package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="quotationcosting")
public class QuotationCosting extends BaseObject {
	
	//Mandatory field's
	private String corpID;
	private Long id;
	private String sequenceNumber;
	private String shipNumber;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	
	
	//For working
	private String revenueCategory;
    private String revenueVendorCode;
    private String revenueName;
    private String revenueChargeCode;
    private String revenueReference;
    private String revenueUnit;
    private BigDecimal revenueRate;
    private BigDecimal revenueExpense;
    private boolean revenuePassThrough;
    private String revenueRevenueAmount;
	
    
	private String insuranceCategory;
    private String insuranceVendorCode;
    private String insuranceName;
    private String insuranceChargeCode;
    private String insuranceReference;
    private String insuranceUnit;
    private BigDecimal insuranceRate;
    private BigDecimal insuranceExpense;
    private boolean insurancePassThrough;
    private String insuranceRevenueAmount;
    
	private String originCategory;
    private String originVendorCode;
    private String originName;
    private String originChargeCode;
    private String originReference;
    private String originUnit;
    private BigDecimal originRate;
    private BigDecimal originExpense;
    private boolean originPassThrough;
    private String originRevenueAmount;
    
	private String freightCategory;
    private String freightVendorCode;
    private String freightName;
    private String freightChargeCode;
    private String freightReference;
    private String freightUnit;
    private BigDecimal freightRate;
    private BigDecimal freightExpense;
    private boolean freightPassThrough;
    private String freightRevenueAmount;
    
	private String destinationCategory;
    private String destinationVendorCode;
    private String destinationName;
    private String destinationChargeCode;
    private String destinationReference;
    private String destinationUnit;
    private BigDecimal destinationRate;
    private BigDecimal destinationExpense;
    private boolean destinationPassThrough;
    private String destinationRevenueAmount;
    
	private String otherCategory;
    private String otherVendorCode;
    private String otherName;
    private String otherChargeCode;
    private String otherReference;
    private String otherUnit;
    private BigDecimal otherRate;
    private BigDecimal otherExpense;
    private boolean otherPassThrough;
    private String otherRevenueAmount;
    
	private String other2Category;
    private String other2VendorCode;
    private String other2Name;
    private String other2ChargeCode;
    private String other2Reference;
    private String other2Unit;
    private BigDecimal other2Rate;
    private BigDecimal other2Expense;
    private boolean other2PassThrough;
    private String other2RevenueAmount;
    
	private String commisionCategory;
    private String commisionVendorCode;
    private String commisionName;
    private String commisionChargeCode;
    private String commisionReference;
    private String commisionUnit;
    private BigDecimal commisionRate;
    private BigDecimal commisionExpense;
    private boolean commisionPassThrough;
    private String commisionRevenueAmount;
    
	private String flatFeeCategory;
    private String flatFeeVendorCode;
    private String flatFeeName;
    private String flatFeeChargeCode;
    private String flatFeeReference;
    private String flatFeeUnit;
    private BigDecimal flatFeeRate;
    private BigDecimal flatFeeExpense;
    private boolean flatFeePassThrough;
    private String flatFeeRevenueAmount;
    
    
    private BigDecimal revenueQuantity;
    private BigDecimal insuranceQuantity;
    private BigDecimal originQuantity;
    private BigDecimal freightQuantity;
    private BigDecimal destinationQuantity;
    private BigDecimal otherQuantity;
    private BigDecimal other2Quantity;
    private BigDecimal commisionQuantity;
    private BigDecimal flatFeeQuantity;
    
    private String revenueDivMult;
    private String bookerDivMult;
    private String originDivMult;
    private String freightDivMult;
    private String destinationDivMult;
    private String otherDivMult;
    private String other2DivMult;
    private String commisionDivMult;
    private String flatFeeDivMult;
    
    
    
    private BigDecimal totalExpense;
    private BigDecimal totalRevenueAmount;
    private BigDecimal grossRevenueAmount;
    
    private BigDecimal grossMarginPer;
    
    private boolean priceApproved;
    
    private String contract;
    private String basis;
    private BigDecimal estimateExpanseTotal;
    private BigDecimal estimateExpanseCommision;
    private BigDecimal estimateExpanseOther;
    private BigDecimal estimateExpanseInsurance;
    private BigDecimal estimateExpanseDestination;
    private BigDecimal estimateExpanseFreights;
    private BigDecimal estimateExpanseOrigin;
    private BigDecimal estimateExpanseRevenue;
    private BigDecimal quantity;
    private BigDecimal rate;
    
    
  

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("sequenceNumber", sequenceNumber).append(
				"shipNumber", shipNumber).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("revenueCategory",
						revenueCategory).append("revenueVendorCode",
						revenueVendorCode).append("revenueName", revenueName)
				.append("revenueChargeCode", revenueChargeCode).append(
						"revenueReference", revenueReference).append(
						"revenueUnit", revenueUnit).append("revenueRate",
						revenueRate).append("revenueExpense", revenueExpense)
				.append("revenuePassThrough", revenuePassThrough).append(
						"revenueRevenueAmount", revenueRevenueAmount).append(
						"insuranceCategory", insuranceCategory).append(
						"insuranceVendorCode", insuranceVendorCode).append(
						"insuranceName", insuranceName).append("insuranceChargeCode",
						insuranceChargeCode).append("insuranceReference",
						insuranceReference).append("insuranceUnit", insuranceUnit)
				.append("insuranceRate", insuranceRate).append("insuranceExpense",
						insuranceExpense).append("insurancePassThrough",
						insurancePassThrough).append("insuranceRevenueAmount",
						insuranceRevenueAmount).append("originCategory", originCategory)
				.append("originVendorCode", originVendorCode).append(
						"originName", originName).append("originChargeCode",
						originChargeCode).append("originReference",
						originReference).append("originUnit", originUnit)
				.append("originRate", originRate).append("originExpense",
						originExpense).append("originPassThrough",
						originPassThrough).append("originRevenueAmount",
						originRevenueAmount).append("freightCategory",
						freightCategory).append("freightVendorCode",
						freightVendorCode).append("freightName", freightName)
				.append("freightChargeCode", freightChargeCode).append(
						"freightReference", freightReference).append(
						"freightUnit", freightUnit).append("freightRate",
						freightRate).append("freightExpense", freightExpense)
				.append("freightPassThrough", freightPassThrough).append(
						"freightRevenueAmount", freightRevenueAmount).append(
						"destinationCategory", destinationCategory).append(
						"destinationVendorCode", destinationVendorCode).append(
						"destinationName", destinationName).append(
						"destinationChargeCode", destinationChargeCode).append(
						"destinationReference", destinationReference).append(
						"destinationUnit", destinationUnit).append(
						"destinationRate", destinationRate).append(
						"destinationExpense", destinationExpense).append(
						"destinationPassThrough", destinationPassThrough)
				.append("destinationRevenueAmount", destinationRevenueAmount).append(
						"otherCategory", otherCategory).append(
						"otherVendorCode", otherVendorCode).append("otherName",
						otherName).append("otherChargeCode", otherChargeCode)
				.append("otherReference", otherReference).append("otherUnit",
						otherUnit).append("otherRate", otherRate).append(
						"otherExpense", otherExpense).append(
						"otherPassThrough", otherPassThrough).append(
						"otherRevenueAmount", otherRevenueAmount).append("other2Category",
						other2Category).append("other2VendorCode",
						other2VendorCode).append("other2Name", other2Name)
				.append("other2ChargeCode", other2ChargeCode).append(
						"other2Reference", other2Reference).append(
						"other2Unit", other2Unit).append("other2Rate",
						other2Rate).append("other2Expense", other2Expense)
				.append("other2PassThrough", other2PassThrough).append(
						"other2RevenueAmount", other2RevenueAmount).append(
						"commisionCategory", commisionCategory).append(
						"commisionVendorCode", commisionVendorCode).append(
						"commisionName", commisionName).append(
						"commisionChargeCode", commisionChargeCode).append(
						"commisionReference", commisionReference).append(
						"commisionUnit", commisionUnit).append("commisionRate",
						commisionRate).append("commisionExpense",
						commisionExpense).append("commisionPassThrough",
						commisionPassThrough).append("commisionRevenueAmount",
						commisionRevenueAmount).append("flatFeeCategory",
						flatFeeCategory).append("flatFeeVendorCode",
						flatFeeVendorCode).append("flatFeeName", flatFeeName)
				.append("flatFeeChargeCode", flatFeeChargeCode).append(
						"flatFeeReference", flatFeeReference).append(
						"flatFeeUnit", flatFeeUnit).append("flatFeeRate",
						flatFeeRate).append("flatFeeExpense", flatFeeExpense)
				.append("flatFeePassThrough", flatFeePassThrough).append(
						"flatFeeRevenueAmount", flatFeeRevenueAmount).append(
						"revenueQuantity", revenueQuantity).append(
						"insuranceQuantity", insuranceQuantity).append(
						"originQuantity", originQuantity).append(
						"freightQuantity", freightQuantity).append(
						"destinationQuantity", destinationQuantity).append(
						"otherQuantity", otherQuantity).append(
						"other2Quantity", other2Quantity).append(
						"commisionQuantity", commisionQuantity).append(
						"flatFeeQuantity", flatFeeQuantity).append(
						"totalExpense", totalExpense).append("totalRevenueAmount",
						totalRevenueAmount).append("grossRevenueAmount", grossRevenueAmount)
				.append("priceApproved", priceApproved).append("contract",
						contract).append("basis",basis).append("estimateExpanseTotal", estimateExpanseTotal).append("estimateExpanseCommision",estimateExpanseCommision ).append("estimateExpanseOther", estimateExpanseOther).append("estimateExpanseInsurance", estimateExpanseInsurance).append("estimateExpanseDestination", estimateExpanseDestination).append("estimateExpanseFreights",estimateExpanseFreights ).append("estimateExpanseOrigin", estimateExpanseOrigin).append("estimateExpanseRevenue",estimateExpanseRevenue).append("quantity",quantity).append("rate",rate).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof QuotationCosting))
			return false;
		QuotationCosting castOther = (QuotationCosting) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(sequenceNumber, castOther.sequenceNumber)
				.append(shipNumber, castOther.shipNumber).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(revenueCategory,
						castOther.revenueCategory).append(revenueVendorCode,
						castOther.revenueVendorCode).append(revenueName,
						castOther.revenueName).append(revenueChargeCode,
						castOther.revenueChargeCode).append(revenueReference,
						castOther.revenueReference).append(revenueUnit,
						castOther.revenueUnit).append(revenueRate,
						castOther.revenueRate).append(revenueExpense,
						castOther.revenueExpense).append(revenuePassThrough,
						castOther.revenuePassThrough).append(revenueRevenueAmount,
						castOther.revenueRevenueAmount).append(insuranceCategory,
						castOther.insuranceCategory).append(insuranceVendorCode,
						castOther.insuranceVendorCode).append(insuranceName,
						castOther.insuranceName).append(insuranceChargeCode,
						castOther.insuranceChargeCode).append(insuranceReference,
						castOther.insuranceReference).append(insuranceUnit,
						castOther.insuranceUnit).append(insuranceRate,
						castOther.insuranceRate).append(insuranceExpense,
						castOther.insuranceExpense).append(insurancePassThrough,
						castOther.insurancePassThrough).append(insuranceRevenueAmount,
						castOther.insuranceRevenueAmount).append(originCategory,
						castOther.originCategory).append(originVendorCode,
						castOther.originVendorCode).append(originName,
						castOther.originName).append(originChargeCode,
						castOther.originChargeCode).append(originReference,
						castOther.originReference).append(originUnit,
						castOther.originUnit).append(originRate,
						castOther.originRate).append(originExpense,
						castOther.originExpense).append(originPassThrough,
						castOther.originPassThrough).append(originRevenueAmount,
						castOther.originRevenueAmount).append(freightCategory,
						castOther.freightCategory).append(freightVendorCode,
						castOther.freightVendorCode).append(freightName,
						castOther.freightName).append(freightChargeCode,
						castOther.freightChargeCode).append(freightReference,
						castOther.freightReference).append(freightUnit,
						castOther.freightUnit).append(freightRate,
						castOther.freightRate).append(freightExpense,
						castOther.freightExpense).append(freightPassThrough,
						castOther.freightPassThrough).append(freightRevenueAmount,
						castOther.freightRevenueAmount).append(destinationCategory,
						castOther.destinationCategory).append(
						destinationVendorCode, castOther.destinationVendorCode)
				.append(destinationName, castOther.destinationName).append(
						destinationChargeCode, castOther.destinationChargeCode)
				.append(destinationReference, castOther.destinationReference)
				.append(destinationUnit, castOther.destinationUnit).append(
						destinationRate, castOther.destinationRate).append(
						destinationExpense, castOther.destinationExpense)
				.append(destinationPassThrough,
						castOther.destinationPassThrough).append(
						destinationRevenueAmount, castOther.destinationRevenueAmount)
				.append(otherCategory, castOther.otherCategory).append(
						otherVendorCode, castOther.otherVendorCode).append(
						otherName, castOther.otherName).append(otherChargeCode,
						castOther.otherChargeCode).append(otherReference,
						castOther.otherReference).append(otherUnit,
						castOther.otherUnit).append(otherRate,
						castOther.otherRate).append(otherExpense,
						castOther.otherExpense).append(otherPassThrough,
						castOther.otherPassThrough).append(otherRevenueAmount,
						castOther.otherRevenueAmount).append(other2Category,
						castOther.other2Category).append(other2VendorCode,
						castOther.other2VendorCode).append(other2Name,
						castOther.other2Name).append(other2ChargeCode,
						castOther.other2ChargeCode).append(other2Reference,
						castOther.other2Reference).append(other2Unit,
						castOther.other2Unit).append(other2Rate,
						castOther.other2Rate).append(other2Expense,
						castOther.other2Expense).append(other2PassThrough,
						castOther.other2PassThrough).append(other2RevenueAmount,
						castOther.other2RevenueAmount).append(commisionCategory,
						castOther.commisionCategory).append(
						commisionVendorCode, castOther.commisionVendorCode)
				.append(commisionName, castOther.commisionName).append(
						commisionChargeCode, castOther.commisionChargeCode)
				.append(commisionReference, castOther.commisionReference)
				.append(commisionUnit, castOther.commisionUnit).append(
						commisionRate, castOther.commisionRate).append(
						commisionExpense, castOther.commisionExpense).append(
						commisionPassThrough, castOther.commisionPassThrough)
				.append(commisionRevenueAmount, castOther.commisionRevenueAmount).append(
						flatFeeCategory, castOther.flatFeeCategory).append(
						flatFeeVendorCode, castOther.flatFeeVendorCode).append(
						flatFeeName, castOther.flatFeeName).append(
						flatFeeChargeCode, castOther.flatFeeChargeCode).append(
						flatFeeReference, castOther.flatFeeReference).append(
						flatFeeUnit, castOther.flatFeeUnit).append(flatFeeRate,
						castOther.flatFeeRate).append(flatFeeExpense,
						castOther.flatFeeExpense).append(flatFeePassThrough,
						castOther.flatFeePassThrough).append(flatFeeRevenueAmount,
						castOther.flatFeeRevenueAmount).append(revenueQuantity,
						castOther.revenueQuantity).append(insuranceQuantity,
						castOther.insuranceQuantity).append(originQuantity,
						castOther.originQuantity).append(freightQuantity,
						castOther.freightQuantity).append(destinationQuantity,
						castOther.destinationQuantity).append(otherQuantity,
						castOther.otherQuantity).append(other2Quantity,
						castOther.other2Quantity).append(commisionQuantity,
						castOther.commisionQuantity).append(flatFeeQuantity,
						castOther.flatFeeQuantity).append(totalExpense,
						castOther.totalExpense).append(totalRevenueAmount,
						castOther.totalRevenueAmount).append(grossRevenueAmount,
						castOther.grossRevenueAmount).append(priceApproved,
						castOther.priceApproved).append(contract,
						castOther.contract).append(basis, castOther.basis).append(estimateExpanseTotal, castOther.estimateExpanseTotal).append(estimateExpanseCommision, castOther.estimateExpanseCommision).append(estimateExpanseOther, castOther.estimateExpanseOther).append(estimateExpanseInsurance, castOther.estimateExpanseInsurance).append(estimateExpanseDestination, castOther.estimateExpanseDestination).append(estimateExpanseFreights, castOther.estimateExpanseFreights).append(estimateExpanseOrigin, castOther.estimateExpanseOrigin).append(estimateExpanseRevenue,castOther.estimateExpanseRevenue).append(quantity,castOther.quantity).append(rate,castOther.rate).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(
				sequenceNumber).append(shipNumber).append(createdBy).append(
				updatedBy).append(createdOn).append(updatedOn).append(
				revenueCategory).append(revenueVendorCode).append(revenueName)
				.append(revenueChargeCode).append(revenueReference).append(
						revenueUnit).append(revenueRate).append(revenueExpense)
				.append(revenuePassThrough).append(revenueRevenueAmount).append(
						insuranceCategory).append(insuranceVendorCode).append(
						insuranceName).append(insuranceChargeCode).append(
						insuranceReference).append(insuranceUnit).append(insuranceRate)
				.append(insuranceExpense).append(insurancePassThrough).append(
						insuranceRevenueAmount).append(originCategory).append(
						originVendorCode).append(originName).append(
						originChargeCode).append(originReference).append(
						originUnit).append(originRate).append(originExpense)
				.append(originPassThrough).append(originRevenueAmount).append(
						freightCategory).append(freightVendorCode).append(
						freightName).append(freightChargeCode).append(
						freightReference).append(freightUnit).append(
						freightRate).append(freightExpense).append(
						freightPassThrough).append(freightRevenueAmount).append(
						destinationCategory).append(destinationVendorCode)
				.append(destinationName).append(destinationChargeCode).append(
						destinationReference).append(destinationUnit).append(
						destinationRate).append(destinationExpense).append(
						destinationPassThrough).append(destinationRevenueAmount)
				.append(otherCategory).append(otherVendorCode)
				.append(otherName).append(otherChargeCode).append(
						otherReference).append(otherUnit).append(otherRate)
				.append(otherExpense).append(otherPassThrough).append(
						otherRevenueAmount).append(other2Category).append(
						other2VendorCode).append(other2Name).append(
						other2ChargeCode).append(other2Reference).append(
						other2Unit).append(other2Rate).append(other2Expense)
				.append(other2PassThrough).append(other2RevenueAmount).append(
						commisionCategory).append(commisionVendorCode).append(
						commisionName).append(commisionChargeCode).append(
						commisionReference).append(commisionUnit).append(
						commisionRate).append(commisionExpense).append(
						commisionPassThrough).append(commisionRevenueAmount).append(
						flatFeeCategory).append(flatFeeVendorCode).append(
						flatFeeName).append(flatFeeChargeCode).append(
						flatFeeReference).append(flatFeeUnit).append(
						flatFeeRate).append(flatFeeExpense).append(
						flatFeePassThrough).append(flatFeeRevenueAmount).append(
						revenueQuantity).append(insuranceQuantity).append(
						originQuantity).append(freightQuantity).append(
						destinationQuantity).append(otherQuantity).append(
						other2Quantity).append(commisionQuantity).append(
						flatFeeQuantity).append(totalExpense).append(
						totalRevenueAmount).append(grossRevenueAmount)
				.append(priceApproved).append(contract).append(basis).append(estimateExpanseTotal).append(estimateExpanseCommision).append(estimateExpanseOther).append(estimateExpanseInsurance).append(estimateExpanseDestination).append(estimateExpanseFreights).append(estimateExpanseOrigin).append(estimateExpanseRevenue).append(quantity).append(rate).toHashCode();
	}
	@Column(length=15)
	public String getInsuranceCategory() {
		return insuranceCategory;
	}
	public void setInsuranceCategory(String bookerCategory) {
		this.insuranceCategory = bookerCategory;
	}
	
	@Column(length=12)
	public String getInsuranceChargeCode() {
		return insuranceChargeCode;
	}
	public void setInsuranceChargeCode(String bookerChargeCode) {
		this.insuranceChargeCode = bookerChargeCode;
	}
	
	@Column(length=20)
	public BigDecimal getInsuranceExpense() {
		return insuranceExpense;
	}
	public void setInsuranceExpense(BigDecimal bookerExpense) {
		this.insuranceExpense = bookerExpense;
	}
	
	@Column(length=30)
	public String getInsuranceName() {
		return insuranceName;
	}
	public void setInsuranceName(String bookerName) {
		this.insuranceName = bookerName;
	}
	
	@Column(length=1)
	public boolean isInsurancePassThrough() {
		return insurancePassThrough;
	}
	public void setInsurancePassThrough(boolean bookerPassThrough) {
		this.insurancePassThrough = bookerPassThrough;
	}
	
	@Column(length=20)
	public BigDecimal getInsuranceRate() {
		return insuranceRate;
	}
	public void setInsuranceRate(BigDecimal bookerRate) {
		this.insuranceRate = bookerRate;
	}
	
	@Column(length=20)
	public String getInsuranceReference() {
		return insuranceReference;
	}
	public void setInsuranceReference(String bookerReference) {
		this.insuranceReference = bookerReference;
	}
	
	@Column(length=20)
	public String getInsuranceRevenueAmount() {
		return insuranceRevenueAmount;
	}
	public void setInsuranceRevenueAmount(String bookerRevenueAmount) {
		this.insuranceRevenueAmount = bookerRevenueAmount;
	}
	
	@Column(length=20)
	public String getInsuranceUnit() {
		return insuranceUnit;
	}
	public void setInsuranceUnit(String bookerUnit) {
		this.insuranceUnit = bookerUnit;
	}
	
	@Column(length=10)
	public String getInsuranceVendorCode() {
		return insuranceVendorCode;
	}
	public void setInsuranceVendorCode(String bookerVendorCode) {
		this.insuranceVendorCode = bookerVendorCode;
	}
	
	@Column(length=15)
	public String getCommisionCategory() {
		return commisionCategory;
	}
	public void setCommisionCategory(String commisionCategory) {
		this.commisionCategory = commisionCategory;
	}
	
	@Column(length=12)
	public String getCommisionChargeCode() {
		return commisionChargeCode;
	}
	public void setCommisionChargeCode(String commisionChargeCode) {
		this.commisionChargeCode = commisionChargeCode;
	}
	
	@Column(length=20)
	public BigDecimal getCommisionExpense() {
		return commisionExpense;
	}
	public void setCommisionExpense(BigDecimal commisionExpense) {
		this.commisionExpense = commisionExpense;
	}
	
	@Column(length=30)
	public String getCommisionName() {
		return commisionName;
	}
	public void setCommisionName(String commisionName) {
		this.commisionName = commisionName;
	}
	
	@Column(length=1)
	public boolean isCommisionPassThrough() {
		return commisionPassThrough;
	}
	public void setCommisionPassThrough(boolean commisionPassThrough) {
		this.commisionPassThrough = commisionPassThrough;
	}
	
	@Column(length=20)
	public BigDecimal getCommisionRate() {
		return commisionRate;
	}
	public void setCommisionRate(BigDecimal commisionRate) {
		this.commisionRate = commisionRate;
	}
	
	@Column(length=20)
	public String getCommisionReference() {
		return commisionReference;
	}
	public void setCommisionReference(String commisionReference) {
		this.commisionReference = commisionReference;
	}
	
	@Column(length=20)
	public String getCommisionRevenueAmount() {
		return commisionRevenueAmount;
	}
	public void setCommisionRevenueAmount(String commisionRevenueAmount) {
		this.commisionRevenueAmount = commisionRevenueAmount;
	}
	
	@Column(length=20)
	public String getCommisionUnit() {
		return commisionUnit;
	}
	public void setCommisionUnit(String commisionUnit) {
		this.commisionUnit = commisionUnit;
	}
	
	@Column(length=10)
	public String getCommisionVendorCode() {
		return commisionVendorCode;
	}
	public void setCommisionVendorCode(String commisionVendorCode) {
		this.commisionVendorCode = commisionVendorCode;
	}
	
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length=25)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	
	@Column(length=15)
	public String getDestinationCategory() {
		return destinationCategory;
	}
	public void setDestinationCategory(String destinationCategory) {
		this.destinationCategory = destinationCategory;
	}
	
	@Column(length=12)
	public String getDestinationChargeCode() {
		return destinationChargeCode;
	}
	public void setDestinationChargeCode(String destinationChargeCode) {
		this.destinationChargeCode = destinationChargeCode;
	}
	
	@Column(length=20)
	public BigDecimal getDestinationExpense() {
		return destinationExpense;
	}
	public void setDestinationExpense(BigDecimal destinationExpense) {
		this.destinationExpense = destinationExpense;
	}
	
	@Column(length=30)
	public String getDestinationName() {
		return destinationName;
	}
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}
	
	@Column(length=1)
	public boolean isDestinationPassThrough() {
		return destinationPassThrough;
	}
	public void setDestinationPassThrough(boolean destinationPassThrough) {
		this.destinationPassThrough = destinationPassThrough;
	}
	
	@Column(length=20)
	public BigDecimal getDestinationRate() {
		return destinationRate;
	}
	public void setDestinationRate(BigDecimal destinationRate) {
		this.destinationRate = destinationRate;
	}
	
	@Column(length=20)
	public String getDestinationReference() {
		return destinationReference;
	}
	public void setDestinationReference(String destinationReference) {
		this.destinationReference = destinationReference;
	}
	
	@Column(length=20)
	public String getDestinationRevenueAmount() {
		return destinationRevenueAmount;
	}
	public void setDestinationRevenueAmount(String destinationRevenueAmount) {
		this.destinationRevenueAmount = destinationRevenueAmount;
	}
	
	@Column(length=20)
	public String getDestinationUnit() {
		return destinationUnit;
	}
	public void setDestinationUnit(String destinationUnit) {
		this.destinationUnit = destinationUnit;
	}
	
	@Column(length=10)
	public String getDestinationVendorCode() {
		return destinationVendorCode;
	}
	public void setDestinationVendorCode(String destinationVendorCode) {
		this.destinationVendorCode = destinationVendorCode;
	}
	
	@Column(length=15)
	public String getFlatFeeCategory() {
		return flatFeeCategory;
	}
	public void setFlatFeeCategory(String flatFeeCategory) {
		this.flatFeeCategory = flatFeeCategory;
	}
	
	@Column(length=12)
	public String getFlatFeeChargeCode() {
		return flatFeeChargeCode;
	}
	public void setFlatFeeChargeCode(String flatFeeChargeCode) {
		this.flatFeeChargeCode = flatFeeChargeCode;
	}
	
	@Column(length=20)
	public BigDecimal getFlatFeeExpense() {
		return flatFeeExpense;
	}
	public void setFlatFeeExpense(BigDecimal flatFeeExpense) {
		this.flatFeeExpense = flatFeeExpense;
	}
	
	@Column(length=30)
	public String getFlatFeeName() {
		return flatFeeName;
	}
	public void setFlatFeeName(String flatFeeName) {
		this.flatFeeName = flatFeeName;
	}
	
	@Column(length=1)
	public boolean isFlatFeePassThrough() {
		return flatFeePassThrough;
	}
	public void setFlatFeePassThrough(boolean flatFeePassThrough) {
		this.flatFeePassThrough = flatFeePassThrough;
	}
	
	@Column(length=20)
	public BigDecimal getFlatFeeRate() {
		return flatFeeRate;
	}
	public void setFlatFeeRate(BigDecimal flatFeeRate) {
		this.flatFeeRate = flatFeeRate;
	}
	
	@Column(length=20)
	public String getFlatFeeReference() {
		return flatFeeReference;
	}
	public void setFlatFeeReference(String flatFeeReference) {
		this.flatFeeReference = flatFeeReference;
	}
	
	@Column(length=20)
	public String getFlatFeeRevenueAmount() {
		return flatFeeRevenueAmount;
	}
	public void setFlatFeeRevenueAmount(String flatFeeRevenueAmount) {
		this.flatFeeRevenueAmount = flatFeeRevenueAmount;
	}
	
	@Column(length=20)
	public String getFlatFeeUnit() {
		return flatFeeUnit;
	}
	public void setFlatFeeUnit(String flatFeeUnit) {
		this.flatFeeUnit = flatFeeUnit;
	}
	
	@Column(length=10)
	public String getFlatFeeVendorCode() {
		return flatFeeVendorCode;
	}
	public void setFlatFeeVendorCode(String flatFeeVendorCode) {
		this.flatFeeVendorCode = flatFeeVendorCode;
	}
	
	@Column(length=15)
	public String getFreightCategory() {
		return freightCategory;
	}
	public void setFreightCategory(String freightCategory) {
		this.freightCategory = freightCategory;
	}
	
	@Column(length=12)
	public String getFreightChargeCode() {
		return freightChargeCode;
	}
	public void setFreightChargeCode(String freightChargeCode) {
		this.freightChargeCode = freightChargeCode;
	}
	
	@Column(length=20)
	public BigDecimal getFreightExpense() {
		return freightExpense;
	}
	public void setFreightExpense(BigDecimal freightExpense) {
		this.freightExpense = freightExpense;
	}
	
	@Column(length=30)
	public String getFreightName() {
		return freightName;
	}
	public void setFreightName(String freightName) {
		this.freightName = freightName;
	}
	
	@Column(length=1)
	public boolean isFreightPassThrough() {
		return freightPassThrough;
	}
	public void setFreightPassThrough(boolean freightPassThrough) {
		this.freightPassThrough = freightPassThrough;
	}
	
	@Column(length=20)
	public BigDecimal getFreightRate() {
		return freightRate;
	}
	public void setFreightRate(BigDecimal freightRate) {
		this.freightRate = freightRate;
	}
	
	@Column(length=20)
	public String getFreightReference() {
		return freightReference;
	}
	public void setFreightReference(String freightReference) {
		this.freightReference = freightReference;
	}
	
	@Column(length=20)
	public String getFreightRevenueAmount() {
		return freightRevenueAmount;
	}
	public void setFreightRevenueAmount(String freightRevenueAmount) {
		this.freightRevenueAmount = freightRevenueAmount;
	}
	
	@Column(length=20)
	public String getFreightUnit() {
		return freightUnit;
	}
	public void setFreightUnit(String freightUnit) {
		this.freightUnit = freightUnit;
	}
	
	@Column(length=10)
	public String getFreightVendorCode() {
		return freightVendorCode;
	}
	public void setFreightVendorCode(String freightVendorCode) {
		this.freightVendorCode = freightVendorCode;
	}
	
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(length=15)
	public String getOriginCategory() {
		return originCategory;
	}
	public void setOriginCategory(String originCategory) {
		this.originCategory = originCategory;
	}
	
	@Column(length=12)
	public String getOriginChargeCode() {
		return originChargeCode;
	}
	public void setOriginChargeCode(String originChargeCode) {
		this.originChargeCode = originChargeCode;
	}
	
	@Column(length=20)
	public BigDecimal getOriginExpense() {
		return originExpense;
	}
	public void setOriginExpense(BigDecimal originExpense) {
		this.originExpense = originExpense;
	}
	
	@Column(length=30)
	public String getOriginName() {
		return originName;
	}
	public void setOriginName(String originName) {
		this.originName = originName;
	}
	
	@Column(length=1)
	public boolean isOriginPassThrough() {
		return originPassThrough;
	}
	public void setOriginPassThrough(boolean originPassThrough) {
		this.originPassThrough = originPassThrough;
	}
	
	@Column(length=20)
	public BigDecimal getOriginRate() {
		return originRate;
	}
	public void setOriginRate(BigDecimal originRate) {
		this.originRate = originRate;
	}
	
	@Column(length=20)
	public String getOriginReference() {
		return originReference;
	}
	public void setOriginReference(String originReference) {
		this.originReference = originReference;
	}
	
	@Column(length=20)
	public String getOriginRevenueAmount() {
		return originRevenueAmount;
	}
	public void setOriginRevenueAmount(String originRevenueAmount) {
		this.originRevenueAmount = originRevenueAmount;
	}
	
	@Column(length=20)
	public String getOriginUnit() {
		return originUnit;
	}
	public void setOriginUnit(String originUnit) {
		this.originUnit = originUnit;
	}
	
	@Column(length=10)
	public String getOriginVendorCode() {
		return originVendorCode;
	}
	public void setOriginVendorCode(String originVendorCode) {
		this.originVendorCode = originVendorCode;
	}
	
	@Column(length=15)
	public String getOther2Category() {
		return other2Category;
	}
	public void setOther2Category(String other2Category) {
		this.other2Category = other2Category;
	}
	
	@Column(length=12)
	public String getOther2ChargeCode() {
		return other2ChargeCode;
	}
	public void setOther2ChargeCode(String other2ChargeCode) {
		this.other2ChargeCode = other2ChargeCode;
	}
	
	@Column(length=20)
	public BigDecimal getOther2Expense() {
		return other2Expense;
	}
	public void setOther2Expense(BigDecimal other2Expense) {
		this.other2Expense = other2Expense;
	}
	
	@Column(length=30)
	public String getOther2Name() {
		return other2Name;
	}
	public void setOther2Name(String other2Name) {
		this.other2Name = other2Name;
	}
	
	@Column(length=1)
	public boolean isOther2PassThrough() {
		return other2PassThrough;
	}
	public void setOther2PassThrough(boolean other2PassThrough) {
		this.other2PassThrough = other2PassThrough;
	}
	
	@Column(length=20)
	public BigDecimal getOther2Rate() {
		return other2Rate;
	}
	public void setOther2Rate(BigDecimal other2Rate) {
		this.other2Rate = other2Rate;
	}
	
	@Column(length=20)
	public String getOther2Reference() {
		return other2Reference;
	}
	public void setOther2Reference(String other2Reference) {
		this.other2Reference = other2Reference;
	}
	
	@Column(length=20)
	public String getOther2RevenueAmount() {
		return other2RevenueAmount;
	}
	public void setOther2RevenueAmount(String other2RevenueAmount) {
		this.other2RevenueAmount = other2RevenueAmount;
	}
	
	@Column(length=20)
	public String getOther2Unit() {
		return other2Unit;
	}
	public void setOther2Unit(String other2Unit) {
		this.other2Unit = other2Unit;
	}
	
	@Column(length=10)
	public String getOther2VendorCode() {
		return other2VendorCode;
	}
	public void setOther2VendorCode(String other2VendorCode) {
		this.other2VendorCode = other2VendorCode;
	}
	
	@Column(length=15)
	public String getOtherCategory() {
		return otherCategory;
	}
	public void setOtherCategory(String otherCategory) {
		this.otherCategory = otherCategory;
	}
	
	@Column(length=12)
	public String getOtherChargeCode() {
		return otherChargeCode;
	}
	public void setOtherChargeCode(String otherChargeCode) {
		this.otherChargeCode = otherChargeCode;
	}
	
	@Column(length=20)
	public BigDecimal getOtherExpense() {
		return otherExpense;
	}
	public void setOtherExpense(BigDecimal otherExpense) {
		this.otherExpense = otherExpense;
	}
	
	@Column(length=30)
	public String getOtherName() {
		return otherName;
	}
	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}
	
	@Column(length=1)
	public boolean isOtherPassThrough() {
		return otherPassThrough;
	}
	public void setOtherPassThrough(boolean otherPassThrough) {
		this.otherPassThrough = otherPassThrough;
	}
	
	@Column(length=20)
	public BigDecimal getOtherRate() {
		return otherRate;
	}
	public void setOtherRate(BigDecimal otherRate) {
		this.otherRate = otherRate;
	}
	
	@Column(length=20)
	public String getOtherReference() {
		return otherReference;
	}
	public void setOtherReference(String otherReference) {
		this.otherReference = otherReference;
	}
	
	@Column(length=20)
	public String getOtherRevenueAmount() {
		return otherRevenueAmount;
	}
	public void setOtherRevenueAmount(String otherRevenueAmount) {
		this.otherRevenueAmount = otherRevenueAmount;
	}
	
	@Column(length=20)
	public String getOtherUnit() {
		return otherUnit;
	}
	public void setOtherUnit(String otherUnit) {
		this.otherUnit = otherUnit;
	}
	
	@Column(length=10)
	public String getOtherVendorCode() {
		return otherVendorCode;
	}
	public void setOtherVendorCode(String otherVendorCode) {
		this.otherVendorCode = otherVendorCode;
	}
	
	@Column(length=15)
	public String getRevenueCategory() {
		return revenueCategory;
	}
	public void setRevenueCategory(String revenueCategory) {
		this.revenueCategory = revenueCategory;
	}
	
	@Column(length=12)
	public String getRevenueChargeCode() {
		return revenueChargeCode;
	}
	public void setRevenueChargeCode(String revenueChargeCode) {
		this.revenueChargeCode = revenueChargeCode;
	}
	
	@Column(length=20)
	public BigDecimal getRevenueExpense() {
		return revenueExpense;
	}
	public void setRevenueExpense(BigDecimal revenueExpense) {
		this.revenueExpense = revenueExpense;
	}
	
	@Column(length=30)
	public String getRevenueName() {
		return revenueName;
	}
	public void setRevenueName(String revenueName) {
		this.revenueName = revenueName;
	}
	
	@Column(length=1)
	public boolean isRevenuePassThrough() {
		return revenuePassThrough;
	}
	public void setRevenuePassThrough(boolean revenuePassThrough) {
		this.revenuePassThrough = revenuePassThrough;
	}
	
	@Column(length=20)
	public BigDecimal getRevenueRate() {
		return revenueRate;
	}
	public void setRevenueRate(BigDecimal revenueRate) {
		this.revenueRate = revenueRate;
	}
	
	
	@Column(length=20)
	public String getRevenueReference() {
		return revenueReference;
	}
	public void setRevenueReference(String revenueReference) {
		this.revenueReference = revenueReference;
	}
	
	@Column(length=20)
	public String getRevenueRevenueAmount() {
		return revenueRevenueAmount;
	}
	public void setRevenueRevenueAmount(String revenueRevenueAmount) {
		this.revenueRevenueAmount = revenueRevenueAmount;
	}
	
	@Column(length=20)
	public String getRevenueUnit() {
		return revenueUnit;
	}
	public void setRevenueUnit(String revenueUnit) {
		this.revenueUnit = revenueUnit;
	}
	
	@Column(length=10)
	public String getRevenueVendorCode() {
		return revenueVendorCode;
	}
	public void setRevenueVendorCode(String revenueVendorCode) {
		this.revenueVendorCode = revenueVendorCode;
	}
	@Column(length=9)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	
	@Column(length=9)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	
	@Column(length=25)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length=20)
	public BigDecimal getGrossRevenueAmount() {
		return grossRevenueAmount;
	}
	public void setGrossRevenueAmount(BigDecimal grossRevenueAmount) {
		this.grossRevenueAmount = grossRevenueAmount;
	}
	
	@Column(length=20)
	public BigDecimal getTotalExpense() {
		return totalExpense;
	}
	public void setTotalExpense(BigDecimal totalExpense) {
		this.totalExpense = totalExpense;
	}
	
	@Column(length=20)
	public BigDecimal getTotalRevenueAmount() {
		return totalRevenueAmount;
	}
	public void setTotalRevenueAmount(BigDecimal totalRevenueAmount) {
		this.totalRevenueAmount = totalRevenueAmount;
	}
	
	@Column(length=1)
	public boolean isPriceApproved() {
		return priceApproved;
	}
	public void setPriceApproved(boolean priceApproved) {
		this.priceApproved = priceApproved;
	}
	
	@Column(length=20)
	public BigDecimal getInsuranceQuantity() {
		return insuranceQuantity;
	}
	public void setInsuranceQuantity(BigDecimal insuranceQuantity) {
		this.insuranceQuantity = insuranceQuantity;
	}
	
	@Column(length=20)
	public BigDecimal getCommisionQuantity() {
		return commisionQuantity;
	}
	public void setCommisionQuantity(BigDecimal commisionQuantity) {
		this.commisionQuantity = commisionQuantity;
	}
	
	@Column(length=20)
	public BigDecimal getDestinationQuantity() {
		return destinationQuantity;
	}
	public void setDestinationQuantity(BigDecimal destinationQuantity) {
		this.destinationQuantity = destinationQuantity;
	}
	
	@Column(length=20)
	public BigDecimal getFlatFeeQuantity() {
		return flatFeeQuantity;
	}
	public void setFlatFeeQuantity(BigDecimal flatFeeQuantity) {
		this.flatFeeQuantity = flatFeeQuantity;
	}
	
	@Column(length=20)
	public BigDecimal getFreightQuantity() {
		return freightQuantity;
	}
	public void setFreightQuantity(BigDecimal freightQuantity) {
		this.freightQuantity = freightQuantity;
	}
	
	@Column(length=20)
	public BigDecimal getOriginQuantity() {
		return originQuantity;
	}
	public void setOriginQuantity(BigDecimal originQuantity) {
		this.originQuantity = originQuantity;
	}
	
	@Column(length=20)
	public BigDecimal getOther2Quantity() {
		return other2Quantity;
	}
	public void setOther2Quantity(BigDecimal other2Quantity) {
		this.other2Quantity = other2Quantity;
	}
	
	@Column(length=20)
	public BigDecimal getOtherQuantity() {
		return otherQuantity;
	}
	public void setOtherQuantity(BigDecimal otherQuantity) {
		this.otherQuantity = otherQuantity;
	}
	
	@Column(length=20)
	public BigDecimal getRevenueQuantity() {
		return revenueQuantity;
	}
	public void setRevenueQuantity(BigDecimal revenueQuantity) {
		this.revenueQuantity = revenueQuantity;
	}
	
	@Column(length=30)
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	
	
	@Column(length=1)
	public String getBookerDivMult() {
		return bookerDivMult;
	}
	public void setBookerDivMult(String bookerDivMult) {
		this.bookerDivMult = bookerDivMult;
	}
	
	@Column(length=1)
	public String getCommisionDivMult() {
		return commisionDivMult;
	}
	public void setCommisionDivMult(String commisionDivMult) {
		this.commisionDivMult = commisionDivMult;
	}
	
	@Column(length=1)
	public String getDestinationDivMult() {
		return destinationDivMult;
	}
	public void setDestinationDivMult(String destinationDivMult) {
		this.destinationDivMult = destinationDivMult;
	}
	
	@Column(length=1)
	public String getFlatFeeDivMult() {
		return flatFeeDivMult;
	}
	public void setFlatFeeDivMult(String flatFeeDivMult) {
		this.flatFeeDivMult = flatFeeDivMult;
	}
	
	@Column(length=1)
	public String getFreightDivMult() {
		return freightDivMult;
	}
	public void setFreightDivMult(String freightDivMult) {
		this.freightDivMult = freightDivMult;
	}
	
	@Column(length=1)
	public String getOriginDivMult() {
		return originDivMult;
	}
	public void setOriginDivMult(String originDivMult) {
		this.originDivMult = originDivMult;
	}
	
	@Column(length=1)
	public String getOther2DivMult() {
		return other2DivMult;
	}
	public void setOther2DivMult(String other2DivMult) {
		this.other2DivMult = other2DivMult;
	}
	
	@Column(length=1)
	public String getOtherDivMult() {
		return otherDivMult;
	}
	public void setOtherDivMult(String otherDivMult) {
		this.otherDivMult = otherDivMult;
	}
	
	@Column(length=1)
	public String getRevenueDivMult() {
		return revenueDivMult;
	}
	public void setRevenueDivMult(String revenueDivMult) {
		this.revenueDivMult = revenueDivMult;
	}
	
	@Column(length=20)
	public BigDecimal getGrossMarginPer() {
		return grossMarginPer;
	}
	public void setGrossMarginPer(BigDecimal grossMarginPer) {
		this.grossMarginPer = grossMarginPer;
	}
	@Column(length=20)
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	@Column(length=20)
	public BigDecimal getEstimateExpanseCommision() {
		return estimateExpanseCommision;
	}
	public void setEstimateExpanseCommision(BigDecimal estimateExpanseCommision) {
		this.estimateExpanseCommision = estimateExpanseCommision;
	}
	@Column(length=20)
	public BigDecimal getEstimateExpanseDestination() {
		return estimateExpanseDestination;
	}
	public void setEstimateExpanseDestination(BigDecimal estimateExpanseDestination) {
		this.estimateExpanseDestination = estimateExpanseDestination;
	}
	@Column(length=20)
	public BigDecimal getEstimateExpanseFreights() {
		return estimateExpanseFreights;
	}
	public void setEstimateExpanseFreights(BigDecimal estimateExpanseFreights) {
		this.estimateExpanseFreights = estimateExpanseFreights;
	}
	@Column(length=20)
	public BigDecimal getEstimateExpanseInsurance() {
		return estimateExpanseInsurance;
	}
	public void setEstimateExpanseInsurance(BigDecimal estimateExpanseInsurance) {
		this.estimateExpanseInsurance = estimateExpanseInsurance;
	}
	@Column(length=20)
	public BigDecimal getEstimateExpanseOrigin() {
		return estimateExpanseOrigin;
	}
	public void setEstimateExpanseOrigin(BigDecimal estimateExpanseOrigin) {
		this.estimateExpanseOrigin = estimateExpanseOrigin;
	}
	@Column(length=20)
	public BigDecimal getEstimateExpanseOther() {
		return estimateExpanseOther;
	}
	public void setEstimateExpanseOther(BigDecimal estimateExpanseOther) {
		this.estimateExpanseOther = estimateExpanseOther;
	}
	@Column(length=20)
	public BigDecimal getEstimateExpanseRevenue() {
		return estimateExpanseRevenue;
	}
	public void setEstimateExpanseRevenue(BigDecimal estimateExpanseRevenue) {
		this.estimateExpanseRevenue = estimateExpanseRevenue;
	}
	@Column(length=20)
	public BigDecimal getEstimateExpanseTotal() {
		return estimateExpanseTotal;
	}
	public void setEstimateExpanseTotal(BigDecimal estimateExpanseTotal) {
		this.estimateExpanseTotal = estimateExpanseTotal;
	}
	@Column(length=20)
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	@Column(length=20)
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	
}
