package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="taskchecklist")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class TaskCheckList extends BaseObject{
	
	private Long id;
	private Long todoRuleId;
	private Long resultRecordId;
	private String resultRecordType;
	private String owner;
	
	private String rolelist;
	private String messagedisplayed;
	private String testdate;
	private String fieldToValidate1;
	private String fieldToValidate2;
	private Long durationAddSub;
	private String duration;
	private String corpID;	
	private String url;
	private String shipper;
	private String checkedUserName;
	private String supportingId;
	private String fieldDisplay;
	private String fileNumber;
	private Boolean isNotesAdded;
	private Long ruleNumber;
	
	private String noteType;
	private String noteSubType;
	private String emailNotification;
	private Date emailOn;
	private String manualEmail;
	private String agentCode;
	private String billToName;
	private String recordToDel;
	private String mailStauts;
	private String reassignedOwner ;
	private String agentName;
	
	private String myfileId;
	private String completedBy;
	private Date completedOn;
	private String transferUser;
	private Date transferDate;
	private Date controlDate;


	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof TaskCheckList))
			return false;
		TaskCheckList castOther = (TaskCheckList) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(todoRuleId, castOther.todoRuleId)
				.append(resultRecordId, castOther.resultRecordId)
				.append(resultRecordType, castOther.resultRecordType)
				.append(owner, castOther.owner)
				.append(rolelist, castOther.rolelist)
				.append(messagedisplayed, castOther.messagedisplayed)
				.append(testdate, castOther.testdate)
				.append(fieldToValidate1, castOther.fieldToValidate1)
				.append(fieldToValidate2, castOther.fieldToValidate2)
				.append(durationAddSub, castOther.durationAddSub)
				.append(duration, castOther.duration)
				.append(corpID, castOther.corpID).append(url, castOther.url)
				.append(shipper, castOther.shipper)
				.append(checkedUserName, castOther.checkedUserName)
				.append(supportingId, castOther.supportingId)
				.append(fieldDisplay, castOther.fieldDisplay)
				.append(fileNumber, castOther.fileNumber)
				.append(isNotesAdded, castOther.isNotesAdded)
				.append(ruleNumber, castOther.ruleNumber)
				.append(noteType, castOther.noteType)
				.append(noteSubType, castOther.noteSubType)
				.append(emailNotification, castOther.emailNotification)
				.append(emailOn, castOther.emailOn)
				.append(manualEmail, castOther.manualEmail)
				.append(agentCode, castOther.agentCode)
				.append(billToName, castOther.billToName)
				.append(recordToDel, castOther.recordToDel)
				.append(mailStauts, castOther.mailStauts)
				.append(reassignedOwner, castOther.reassignedOwner)
				.append("agentName",castOther.agentName)
				.append("myfileId",castOther.myfileId)
				.append("completedBy",castOther.completedBy)
				.append("completedOn",castOther.completedOn)
				.append("transferUser",castOther.transferUser)
				.append("transferDate",castOther.transferDate)
				.append("controlDate",castOther.controlDate)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(todoRuleId)
				.append(resultRecordId).append(resultRecordType).append(owner)
				.append(rolelist).append(messagedisplayed).append(testdate)
				.append(fieldToValidate1).append(fieldToValidate2)
				.append(durationAddSub).append(duration).append(corpID)
				.append(url).append(shipper).append(checkedUserName)
				.append(supportingId).append(fieldDisplay).append(fileNumber)
				.append(isNotesAdded).append(ruleNumber).append(noteType)
				.append(noteSubType).append(emailNotification).append(emailOn)
				.append(manualEmail).append(agentCode).append(billToName)
				.append(recordToDel).append(mailStauts).append(reassignedOwner)
				.append(agentName)
				.append(myfileId)
				.append(completedBy)
				.append(completedOn)
				.append(transferUser)
				.append(transferDate)
				.append(controlDate)
				.toHashCode();
	}
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("todoRuleId", todoRuleId)
				.append("resultRecordId", resultRecordId)
				.append("resultRecordType", resultRecordType)
				.append("owner", owner).append("rolelist", rolelist)
				.append("messagedisplayed", messagedisplayed)
				.append("testdate", testdate)
				.append("fieldToValidate1", fieldToValidate1)
				.append("fieldToValidate2", fieldToValidate2)
				.append("durationAddSub", durationAddSub)
				.append("duration", duration).append("corpID", corpID)
				.append("url", url).append("shipper", shipper)
				.append("checkedUserName", checkedUserName)
				.append("supportingId", supportingId)
				.append("fieldDisplay", fieldDisplay)
				.append("fileNumber", fileNumber)
				.append("isNotesAdded", isNotesAdded)
				.append("ruleNumber", ruleNumber).append("noteType", noteType)
				.append("noteSubType", noteSubType)
				.append("emailNotification", emailNotification)
				.append("emailOn", emailOn).append("manualEmail", manualEmail)
				.append("agentCode", agentCode)
				.append("billToName", billToName)
				.append("recordToDel", recordToDel)
				.append("mailStauts", mailStauts)
				.append("reassignedOwner", reassignedOwner)
				.append("agentName",agentName)
				.append("myfileId",myfileId)
				.append("completedBy",completedBy)
				.append("completedOn",completedOn)
				.append("transferUser",transferUser)
				.append("transferDate",transferDate)
				.append("controlDate",controlDate)
				.toString();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=50)
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	@Column(length=10)
	public Long getResultRecordId() {
		return resultRecordId;
	}
	public void setResultRecordId(Long resultRecordId) {
		this.resultRecordId = resultRecordId;
	}
	@Column(length=50)
	public String getResultRecordType() {
		return resultRecordType;
	}
	public void setResultRecordType(String resultRecordType) {
		this.resultRecordType = resultRecordType;
	}
	@Column(length=10)
	public Long getTodoRuleId() {
		return todoRuleId;
	}
	public void setTodoRuleId(Long todoRuleId) {
		this.todoRuleId = todoRuleId;
	}
	@Column(length=10)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=100)
	public String getMessagedisplayed() {
		return messagedisplayed;
	}
	public void setMessagedisplayed(String messagedisplayed) {
		this.messagedisplayed = messagedisplayed;
	}
	
	@Column(length=100)
	public String getTestdate() {
		return testdate;
	}
	public void setTestdate(String testdate) {
		this.testdate = testdate;
	}
	@Column(length=10)
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	@Column(length=50)
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Column(length=75)
	public String getFieldToValidate1() {
		return fieldToValidate1;
	}
	public void setFieldToValidate1(String fieldToValidate1) {
		this.fieldToValidate1 = fieldToValidate1;
	}
	@Column(length=100)
	public String getShipper() {
		return shipper;
	}
	public void setShipper(String shipper) {
		this.shipper = shipper;
	}
	@Column(length=50)
	public String getRolelist() {
		return rolelist;
	}
	public void setRolelist(String rolelist) {
		this.rolelist = rolelist;
	}
	@Column
	public Long getDurationAddSub() {
		return durationAddSub;
	}
	public void setDurationAddSub(Long durationAddSub) {
		this.durationAddSub = durationAddSub;
	}
	@Column(length=50)
	public String getCheckedUserName() {
		return checkedUserName;
	}
	public void setCheckedUserName(String checkedUserName) {
		this.checkedUserName = checkedUserName;
	}
	@Column(length=50)
	public String getSupportingId() {
		return supportingId;
	}
	public void setSupportingId(String supportingId) {
		this.supportingId = supportingId;
	}
	@Column(length=75)
	public String getFieldDisplay() {
		return fieldDisplay;
	}
	public void setFieldDisplay(String fieldDisplay) {
		this.fieldDisplay = fieldDisplay;
	}
	@Column(length=25)
	public String getFileNumber() {
		return fileNumber;
	}
	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}
	public Boolean getIsNotesAdded() {
		return isNotesAdded;
	}
	public void setIsNotesAdded(Boolean isNotesAdded) {
		this.isNotesAdded = isNotesAdded;
	}
	@Column(length=30)
	public String getNoteSubType() {
		return noteSubType;
	}
	public void setNoteSubType(String noteSubType) {
		this.noteSubType = noteSubType;
	}
	@Column(length=30)
	public String getNoteType() {
		return noteType;
	}
	public void setNoteType(String noteType) {
		this.noteType = noteType;
	}
	@Column(length=75)
	public String getFieldToValidate2() {
		return fieldToValidate2;
	}
	public void setFieldToValidate2(String fieldToValidate2) {
		this.fieldToValidate2 = fieldToValidate2;
	}
	public Long getRuleNumber() {
		return ruleNumber;
	}
	public void setRuleNumber(Long ruleNumber) {
		this.ruleNumber = ruleNumber;
	}
	@Column( length=55 )
	public String getEmailNotification() {
		return emailNotification;
	}
	public void setEmailNotification(String emailNotification) {
		this.emailNotification = emailNotification;
	}
	@Column(length=30)
	public Date getEmailOn() {
		return emailOn;
	}
	public void setEmailOn(Date emailOn) {
		this.emailOn = emailOn;
	}
	public String getManualEmail() {
		return manualEmail;
	}
	public void setManualEmail(String manualEmail) {
		this.manualEmail = manualEmail;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	@Column
	public String getBillToName() {
		return billToName;
	}
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	@Column
	public String getRecordToDel() {
		return recordToDel;
	}
	public void setRecordToDel(String recordToDel) {
		this.recordToDel = recordToDel;
	}
	@Column
	public String getMailStauts() {
		return mailStauts;
	}
	public void setMailStauts(String mailStauts) {
		this.mailStauts = mailStauts;
	}
	@Column
	public String getReassignedOwner() {
		return reassignedOwner;
	}
	public void setReassignedOwner(String reassignedOwner) {
		this.reassignedOwner = reassignedOwner;
	}
	@Column
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getMyfileId() {
		return myfileId;
	}
	public void setMyfileId(String myfileId) {
		this.myfileId = myfileId;
	}
	public String getCompletedBy() {
		return completedBy;
	}
	public void setCompletedBy(String completedBy) {
		this.completedBy = completedBy;
	}
	public Date getCompletedOn() {
		return completedOn;
	}
	public void setCompletedOn(Date completedOn) {
		this.completedOn = completedOn;
	}
	public String getTransferUser() {
		return transferUser;
	}
	public void setTransferUser(String transferUser) {
		this.transferUser = transferUser;
	}
	public Date getTransferDate() {
		return transferDate;
	}
	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}
	public Date getControlDate() {
		return controlDate;
	}
	public void setControlDate(Date controlDate) {
		this.controlDate = controlDate;
	}
	

	
}
