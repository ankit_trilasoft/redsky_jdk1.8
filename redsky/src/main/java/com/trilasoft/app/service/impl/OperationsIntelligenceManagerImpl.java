package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.OperationsIntelligenceDao;
import com.trilasoft.app.model.OperationsIntelligence;
import com.trilasoft.app.service.OperationsIntelligenceManager;

public class OperationsIntelligenceManagerImpl extends GenericManagerImpl<OperationsIntelligence, Long> implements OperationsIntelligenceManager{
	
	OperationsIntelligenceDao operationsIntelligenceDao;
	public OperationsIntelligenceManagerImpl(OperationsIntelligenceDao operationsIntelligenceDao) {
        super(operationsIntelligenceDao);
        this.operationsIntelligenceDao = operationsIntelligenceDao;
    
	}
	
	public void updateOperationsIntelligence(Long id,String fieldName,String fieldValue,String tableName , String shipNumber , String updatedBy)
	{
		operationsIntelligenceDao.updateOperationsIntelligence(id,fieldName,fieldValue,tableName,shipNumber ,updatedBy);
	}
	public List findAllResourcesAjax(String shipNumber,String sessionCorpID,String resouceTemplate, String resourceListForOIStatus)
	{
		return operationsIntelligenceDao.findAllResourcesAjax(shipNumber,sessionCorpID,resouceTemplate,  resourceListForOIStatus);
	}
	public void getDeleteWorkOrder(String shipNumber,String sessionCorpID) {
		operationsIntelligenceDao.getDeleteWorkOrder(shipNumber,sessionCorpID);
	}
	public String getSellRateBuyRate(String type,String shipNumber,String descript,String sessionCorpID){
		return operationsIntelligenceDao.getSellRateBuyRate(type, shipNumber, descript, sessionCorpID);
	}
	public void deletefromOIResorcesList(long id,String shipNumber,String usertype) 
	{
		operationsIntelligenceDao.deletefromOIResorcesList(id, shipNumber,usertype);
	}
	public void deletefromWorkTicketResorcesList(long id,String shipNumber,String ticket, String sessionCorpID) 
	{
		operationsIntelligenceDao.deletefromWorkTicketResorcesList(id, shipNumber,ticket,sessionCorpID);
	}
	
	public List distinctWorkOrderForWT(String shipNumber,String sessionCorpID){
		return operationsIntelligenceDao.distinctWorkOrderForWT(shipNumber,sessionCorpID);
	}
	public List getResourceFromOI(String sessionCorpID ,String shipNumber, String workorder)
	{
		return operationsIntelligenceDao.getResourceFromOI(sessionCorpID,shipNumber,workorder);
	}
	 public List distinctWorkOrderForThirdParty(String shipNumber,String sessionCorpID)
	 {
		 return operationsIntelligenceDao.distinctWorkOrderForThirdParty(shipNumber,sessionCorpID);
	 }
	 public List getResourceFromOIForTP(String sessionCorpID ,String shipNumber, String workorder ,String tpId)
	 {
		 return operationsIntelligenceDao.getResourceFromOIForTP(sessionCorpID,shipNumber,workorder,tpId);
	 }
	 public void updateOperationsIntelligenceTicketNo(Long ticket,String workorder,String shipNumber,String sessionCorpID,String tpId , String date , Long ticketId, String usertype,String targetActual)
	 {
		 operationsIntelligenceDao.updateOperationsIntelligenceTicketNo(ticket,workorder,shipNumber,sessionCorpID ,tpId ,date ,ticketId,usertype, targetActual);
	 }
	 public void updateOperationsIntelligenceForScope(String workorder,String tempScopeOfWo,String shipNumber,Long ticket)
	 {
		 operationsIntelligenceDao.updateOperationsIntelligenceForScope(workorder,tempScopeOfWo,shipNumber,ticket);
	 }
	 
	 public void saveScopeInOperationsIntelligence(String workorder,String tempScopeOfWo,String shipNumber,Long ticket,String numberOfComputer,String numberOfEmployee,Double salesTax,String consumables,String revisionScopeValue,boolean consumablePercentageCheck,String aB5Surcharge,boolean editAB5Surcharge ,BigDecimal consumableFromPP)
	 {
		 operationsIntelligenceDao.saveScopeInOperationsIntelligence(workorder,tempScopeOfWo,shipNumber,ticket,numberOfComputer,numberOfEmployee,salesTax,consumables,revisionScopeValue, consumablePercentageCheck,aB5Surcharge,editAB5Surcharge, consumableFromPP);
	 }
	 public String findScopeOfWorkOrder(Long id,String shipNumber)
	 {
		return operationsIntelligenceDao.findScopeOfWorkOrder(id,shipNumber);
	 }
	 public String checkForResourceTransFerWorkTicket(String sessionCorpID ,String shipNumber, String workorder)
	 {
		 return operationsIntelligenceDao.checkForResourceTransFerWorkTicket(sessionCorpID, shipNumber, workorder);
	 }
	 public void updateOperationsIntelligenceItemsJbkEquipId(Long id ,Long itemsJbkEquipId,String shipNumber,String sessionCorpID)
	 {
		 operationsIntelligenceDao.updateOperationsIntelligenceItemsJbkEquipId(id, itemsJbkEquipId, shipNumber, sessionCorpID);
	 }
	 public List scopeOfWorkOrder(String shipNumber,String sessionCorpID,String resouceTemplate)
	 {
		return operationsIntelligenceDao.scopeOfWorkOrder(shipNumber, sessionCorpID,resouceTemplate);
	 }
	 public String checkForTicketBasedOnWorkOrder(String sessionCorpID ,String shipNumber, String workorder)
	 {
		 return operationsIntelligenceDao.checkForTicketBasedOnWorkOrder(sessionCorpID, shipNumber, workorder);
	 }
	 public List distinctWorkOrderForOI(String shipNumber,String sessionCorpID,String flag )
	 {
		 return operationsIntelligenceDao.distinctWorkOrderForOI(shipNumber, sessionCorpID,flag);
	 }
	 public List findResourcesByWorkOrder(String workorder,String shipNumber)
	 {
		 return operationsIntelligenceDao.findResourcesByWorkOrder(workorder, shipNumber);
	 }
	 public String scopeByWorkOrder(String workorder ,String shipNumber)
	 {
		 return operationsIntelligenceDao.scopeByWorkOrder(workorder, shipNumber);
	 }
	 public void updateCalculationOfOI(String calculateOI,String shipNumber)
	 {
		 operationsIntelligenceDao.updateCalculationOfOI(calculateOI,shipNumber);
	 }

	public void updateSalesTax(Double salesTaxForOI, String shipNumber,String salesTaxFieldName) {
		operationsIntelligenceDao.updateSalesTax(salesTaxForOI, shipNumber, salesTaxFieldName);
	}
	public List showDistinctWorkOrderForCopy(String shipNumber,String sessionCorpID)
	{
		 return operationsIntelligenceDao.showDistinctWorkOrderForCopy(shipNumber, sessionCorpID);
	}
	public List copyWorkOrderToResource(String distinctWorkOrder,String shipNumber)
	{
		 return operationsIntelligenceDao.copyWorkOrderToResource(distinctWorkOrder,shipNumber);
	}
	
	public String FindMaxWorkOrder(String shipNumber)
	{
		 return operationsIntelligenceDao.FindMaxWorkOrder(shipNumber);
	}
	
	public void changeStatusOfWorkOrder(String workOrderGroup, String shipNumber,String updatedBy,String itemsJbkEquipId)
	{
		operationsIntelligenceDao.changeStatusOfWorkOrder(workOrderGroup,shipNumber,updatedBy, itemsJbkEquipId);
	}
	
	public void changeStatusOfWorkTicketFromDel(Long id , String shipNumber,String updatedBy,Long itemsJbkEquipId)
	{
		operationsIntelligenceDao.changeStatusOfWorkTicketFromDel(id,shipNumber,updatedBy, itemsJbkEquipId);	
	}
	
	public void changeStatusOfWorkOrderForThirdParty(String workorder,String shipNumber,String updatedBy, Long ticket)
	{
		operationsIntelligenceDao.changeStatusOfWorkOrderForThirdParty(workorder,shipNumber,updatedBy,  ticket);	
	}
	
	public void updateWorkOrder(String workorder,String shipNumber,String usertype)
	{
		operationsIntelligenceDao.updateWorkOrder(workorder, shipNumber, usertype);
	}
	
	public List distinctWOForInsert(String action,String shipNumber,String distinctWorkOrder)
	{
		return operationsIntelligenceDao.distinctWOForInsert(action,shipNumber, distinctWorkOrder);
	}
	public void updateOIDate(Long ticket,String shipNumber,String date1,String updatedBy,String targetActual){
		operationsIntelligenceDao.updateOIDate(ticket, shipNumber, date1,updatedBy,targetActual);
	}
	public void updateOIResourceTicketAndDate(Long ticket,String shipNumber,String updatedBy){
		operationsIntelligenceDao.updateOIResourceTicketAndDate(ticket, shipNumber, updatedBy);
	}
	
	public List taskDetailsListForOI(String sessionCorpID, Long ticket){
		return operationsIntelligenceDao.taskDetailsListForOI(sessionCorpID, ticket);
	}
	
	public List distinctWOForOI(String shipNumber,String sessionCorpID,String flag,String wo){
		return operationsIntelligenceDao.distinctWOForOI(shipNumber, sessionCorpID, flag, wo);
	}
	
	 public String scopeByWorkOrderInUserPortal(String workorder, String shipNumber){
		 return operationsIntelligenceDao.scopeByWorkOrderInUserPortal(workorder, shipNumber);
	 }

	public String getScopeWithValues(String shipNumber, String workorder,String type) {
		return operationsIntelligenceDao.getScopeWithValues(shipNumber, workorder,type);
	}
	
	public void getScopeWithValues(String shipNumber, String workorder, String type, Long ticket){
		 operationsIntelligenceDao.getScopeWithValues(shipNumber, workorder,type,ticket);
	}

	public List FindValueFromOI(String shipNumber) {
		return operationsIntelligenceDao.FindValueFromOI(shipNumber);
	}
	
	public String valueOfCommentForRevised(Long id, String shipNumber, String commentField){
		return operationsIntelligenceDao.valueOfCommentForRevised(id, shipNumber, commentField);
	}
	
	public void saveCmtForRevised(Long id, String shipNumber,String value, String commentField){
		operationsIntelligenceDao.saveCmtForRevised(id, shipNumber,value,commentField);
	}
	
	public List crewListBYTicket(Long ticket, String sessionCorpID){
	return	operationsIntelligenceDao.crewListBYTicket(ticket, sessionCorpID);
	}
	
	public List addWorkOrderToResource(String shipNumber){
		return operationsIntelligenceDao.addWorkOrderToResource(shipNumber);
	}
	public List findDataForCopyEstimate(String shipNumber,String sessionCorpID){
		return operationsIntelligenceDao.findDataForCopyEstimate(shipNumber,sessionCorpID);
	}

	public List updateSelectiveInvoice(Long id, boolean oIStatus, String sessionCorpID,Long itemsJbkEquipId,String shipNumber,String ticket,String workorder,boolean estConsumCheck,BigDecimal estConsumableFromPP,boolean revConsumCheck,BigDecimal revConsumableFromPP,BigDecimal aB5Surcharge,boolean editAB5Surcharge,BigDecimal revisionScopeOfAB5Surcharge,boolean revisionaeditAB5Surcharge) {
		return operationsIntelligenceDao.updateSelectiveInvoice(id, oIStatus, sessionCorpID, itemsJbkEquipId, shipNumber, ticket, workorder,estConsumCheck, estConsumableFromPP,revConsumCheck, revConsumableFromPP,aB5Surcharge,editAB5Surcharge, revisionScopeOfAB5Surcharge,revisionaeditAB5Surcharge);
	}
	public String findRevisionScopeOfWorkOrder(Long id,String shipNumber){
		return operationsIntelligenceDao.findRevisionScopeOfWorkOrder(id, shipNumber);
	}
	public String findInvoicedWorkTicketNumber(String shipNumber){
		return operationsIntelligenceDao.findInvoicedWorkTicketNumber(shipNumber);
	}
	public void updateOperationIntelligenceRevisionSalesTax(String shipNumber){
		operationsIntelligenceDao.updateOperationIntelligenceRevisionSalesTax(shipNumber);
	}
	public void updateExpenseRevenueConsumables(String shipNumber){
		operationsIntelligenceDao.updateExpenseRevenueConsumables(shipNumber);
	}
	public List findEstimateRevisionExpenseRevenueSum(String shipNumber){
		return operationsIntelligenceDao.findEstimateRevisionExpenseRevenueSum(shipNumber);
	}
	public void updateEstimateAndRevisionConsumables(String shipNumber,String workOrder){
		operationsIntelligenceDao.updateEstimateAndRevisionConsumables(shipNumber,workOrder);
	}
	public String findDistinctWorkOrder(String shipNumber,String sessionCorpID){
		return operationsIntelligenceDao.findDistinctWorkOrder(shipNumber, sessionCorpID);
	}
	public void updateEstimateAndRevisionConsumablesWithParnerPercentage(String shipNumber,String workOrder,String fieldName,BigDecimal consumableFromPP){
		operationsIntelligenceDao.updateEstimateAndRevisionConsumablesWithParnerPercentage(shipNumber,workOrder,fieldName, consumableFromPP);
	}
	public Boolean findConsumableChecked(String shipNumber,String workOrder,String fieldName){
		return operationsIntelligenceDao.findConsumableChecked(shipNumber,workOrder,fieldName);
	}
	public void updateEstimateAndRevisionConsumablesWithOutParnerPercentage(String shipNumber,String workOrder,String fieldName){
		operationsIntelligenceDao.updateEstimateAndRevisionConsumablesWithOutParnerPercentage(shipNumber,workOrder,fieldName);
	}
	
	/*
	 * Bug 14390 - O&I and Ops work flow -- Stage One
	 */
	public String getPresetValue(String contract, String charge, String sessionCorpID,String descript){
		return operationsIntelligenceDao.getPresetValue(contract, charge, sessionCorpID,descript);
	}
	public void updateExpenseRevenueAB5Surcharge(String shipNumber){
		 operationsIntelligenceDao.updateExpenseRevenueAB5Surcharge(shipNumber);
	}
	public void updateEstimateAndRevisionAB5SurchargeWithPercentage(String shipNumber,String workOrder,String fieldName,BigDecimal consumableFromPP){
		operationsIntelligenceDao.updateEstimateAndRevisionAB5SurchargeWithPercentage(shipNumber, workOrder, fieldName, consumableFromPP);
	}

	public void updateAccountLineId(String shipNumber, Long serviceOrderId, String operationsIntelligenceIds,Long accountLineId) {
		operationsIntelligenceDao.updateAccountLineId(shipNumber, serviceOrderId, operationsIntelligenceIds,accountLineId);
		
	}

	public double getHoursWithValues(String shipNumber, String workorder, String type) {
		return operationsIntelligenceDao.getHoursWithValues(shipNumber, workorder, type);
	}
	
	/**
	 * End
	 */
}
