package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.KeySurveyQuestionMap;

public interface KeySurveyQuestionMapDao  extends GenericDao<KeySurveyQuestionMap, Long> {

}

