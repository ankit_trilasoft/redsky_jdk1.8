delimiter $$

CREATE trigger redsky.trigger_add_history_refzipgeocodemap
BEFORE UPDATE on redsky.refzipgeocodemap
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";





   IF (OLD.corpId <> NEW.corpId ) THEN
      select CONCAT(fieldNameList,'refzipgeocodemap.corpId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpId,'~') into newValueList;
  END IF;
    




   IF (OLD.zipcode <> NEW.zipcode ) THEN
      select CONCAT(fieldNameList,'refzipgeocodemap.zipcode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.zipcode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.zipcode,'~') into newValueList;
  END IF;
    




   IF (OLD.stateabbreviation <> NEW.stateabbreviation ) THEN
      select CONCAT(fieldNameList,'refzipgeocodemap.stateabbreviation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.stateabbreviation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.stateabbreviation,'~') into newValueList;
  END IF;
    




   IF (OLD.latitude <> NEW.latitude ) THEN
      select CONCAT(fieldNameList,'refzipgeocodemap.latitude~') into fieldNameList;
      select CONCAT(oldValueList,OLD.latitude,'~') into oldValueList;
      select CONCAT(newValueList,NEW.latitude,'~') into newValueList;
  END IF;
    




   IF (OLD.longitude <> NEW.longitude ) THEN
      select CONCAT(fieldNameList,'refzipgeocodemap.longitude~') into fieldNameList;
      select CONCAT(oldValueList,OLD.longitude,'~') into oldValueList;
      select CONCAT(newValueList,NEW.longitude,'~') into newValueList;
  END IF;
    




   IF (OLD.city <> NEW.city ) THEN
      select CONCAT(fieldNameList,'refzipgeocodemap.city~') into fieldNameList;
      select CONCAT(oldValueList,OLD.city,'~') into oldValueList;
      select CONCAT(newValueList,NEW.city,'~') into newValueList;
  END IF;
    




   IF (OLD.state <> NEW.state ) THEN
      select CONCAT(fieldNameList,'refzipgeocodemap.state~') into fieldNameList;
      select CONCAT(oldValueList,OLD.state,'~') into oldValueList;
      select CONCAT(newValueList,NEW.state,'~') into newValueList;
  END IF;
    






   IF (OLD.primaryRecord <> NEW.primaryRecord ) THEN
      select CONCAT(fieldNameList,'refzipgeocodemap.primaryRecord~') into fieldNameList;
      select CONCAT(oldValueList,OLD.primaryRecord,'~') into oldValueList;
      select CONCAT(newValueList,NEW.primaryRecord,'~') into newValueList;
  END IF;





   IF (OLD.phoneAreaCode <> NEW.phoneAreaCode ) THEN
      select CONCAT(fieldNameList,'refzipgeocodemap.phoneAreaCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.phoneAreaCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.phoneAreaCode,'~') into newValueList;
  END IF;






   IF (OLD.country <> NEW.country ) THEN
      select CONCAT(fieldNameList,'refzipgeocodemap.country~') into fieldNameList;
      select CONCAT(oldValueList,OLD.country,'~') into oldValueList;
      select CONCAT(newValueList,NEW.country,'~') into newValueList;
  END IF;





   IF (OLD.County <> NEW.County ) THEN
      select CONCAT(fieldNameList,'refzipgeocodemap.County~') into fieldNameList;
      select CONCAT(oldValueList,OLD.County,'~') into oldValueList;
      select CONCAT(newValueList,NEW.County,'~') into newValueList;
  END IF;






 CALL add_tblHistory (OLD.id,"refzipgeocodemap", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;