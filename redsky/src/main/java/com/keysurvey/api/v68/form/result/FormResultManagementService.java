/**
 * FormResultManagementService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.result;

public interface FormResultManagementService extends java.rmi.Remote {
    public com.keysurvey.api.v68.form.result.WSQuestionResponse getResponse(long respondentId, long questionId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.result.WSRespondent[] getRespondents(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.result.WSRespondent getRespondent(long respondentId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.result.WSQuestionResponse[] getResponses(long respondentId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
    public com.keysurvey.api.v68.form.result.WSFormStatistics getFormStatistics(long formId) throws java.rmi.RemoteException, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault, com.keysurvey.api.v68.WSFault;
}
