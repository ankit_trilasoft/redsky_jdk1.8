<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>

			<c:choose>
			<c:when test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices=='Separate section'}">
			<table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
			<tr>
				<td style="width:595px;">				
				<fieldset style="margin-top:6px;min-height:150px; ">
				<legend><input type="checkbox" id="INCAll" onclick="checkAllInxExc(this,'INC');" class="lgendIn"/> Services Include</legend>
				<div style="height:145px;width:auto;overflow-x:auto;overflow-y:auto;">
				<div style="max-width:980px; white-space: nowrap;">
				<table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
				<c:if test="${not empty inclusionsList}">
			                                                  <c:forEach var="entry" items="${inclusionsList}" varStatus="rowCounter">
                          										<c:if test="${entry.value!=''}"> 
                          										<c:set var="offUrls" value="${fn:split(entry.value,'~')}" />
																<c:forEach items="${offUrls}" var="link" varStatus="rscont">
																 <c:if test="${rscont.count == 1}">
																 <c:set var="des" value="${link}" />
																    </c:if>
																 <c:if test="${rscont.count == 2}">
																 <c:set var="flag" value="${link}" />
																    </c:if>
																</c:forEach>
                          										   
                          										<td class="listwhitetext" style="">
                        										<c:set var="checkOpt" value="false" />
                        										<c:set var="keyValV" value="${entry.key}:INC" />
                          										<c:forTokens items="${servIncExcDefault}" delims="#" var="kc">
																    <c:if test="${kc == keyValV && !checkOpt}">
																    <c:set var="checkOpt" value="true" />
																    </c:if>
																</c:forTokens>                          										                          										
                          										<c:if test="${checkOpt}">
                          										<input type="checkbox" style="vertical-align:middle;" name="checkVC" id="${entry.key}" value="INC" checked="checked" onclick="pickCheckBoxData('INC');" />
                          										</c:if>
                          										 <c:if test="${!checkOpt}">
                          										<input type="checkbox" style="vertical-align:middle;" name="checkVC" id="${entry.key}" value="INC" onclick="pickCheckBoxData('INC');" />
                          										</c:if>
                          										${des} &nbsp;</td>
                          										</c:if>
                          										<c:choose>
														          <c:when test="${rowCounter.count % 1 == 0 && rowCounter.count != 0}">
														          </tr><tr>
														          </c:when>
														         <c:when test="${rowCounter.count != -1}">
														          </c:when>
															          <c:otherwise>
															          </c:otherwise>
															        </c:choose>
                          										</c:forEach>
                      </c:if>
                 </table>
                 </div>
                 </div>
			</fieldset>
			
				</td>
				<td style="width:595px;">				
				<fieldset style="margin-top:6px;margin-left:6px;min-height:150px; ">
				<legend><input type="checkbox" id="EXCAll" onclick="checkAllInxExc(this,'EXC');" style="vertical-align: middle; margin: 0px 0px 0px 2px;!margin: 0px 0px 0px -8px;"/> Services Exclude</legend>
				<div style="height:145px;width:auto;overflow-x:auto;overflow-y:auto; ">
				<div style="max-width:980px; white-space: nowrap;">
				<table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
				<c:if test="${not empty exclusionsList}"> 
			                                                  <c:forEach var="entry" items="${exclusionsList}" varStatus="rowCounter">
                          										<c:if test="${entry.value!=''}"> 
                          										<c:set var="offUrls" value="${fn:split(entry.value,'~')}" />
																<c:forEach items="${offUrls}" var="link" varStatus="rscont">
																 <c:if test="${rscont.count == 1}">
																 <c:set var="des" value="${link}" />
																    </c:if>
																 <c:if test="${rscont.count == 2}">
																 <c:set var="flag" value="${link}" />
																    </c:if>
																</c:forEach>
                          										   
                          										<td class="listwhitetext" style="">
                        										<c:set var="checkOpt" value="false" />
                        										<c:set var="keyValV" value="${entry.key}:EXC" />
                          										<c:forTokens items="${servIncExcDefault}" delims="#" var="kc">
																    <c:if test="${kc == keyValV && !checkOpt}">
																    <c:set var="checkOpt" value="true" />
																    </c:if>
																</c:forTokens>                          										                          										
                          										
                          										  <c:if test="${checkOpt}">
                          										<input type="checkbox" style="vertical-align:middle;" name="checkVC" id="${entry.key}" value="EXC" checked="checked" onclick="pickCheckBoxData('EXC')" />
                          										</c:if>
                          										<c:if test="${!checkOpt}">
                          										<input type="checkbox" style="vertical-align:middle;" name="checkVC" id="${entry.key}" value="EXC" onclick="pickCheckBoxData('EXC');" />
                          										</c:if>
                          										${des} &nbsp;</td>
                          										</c:if>
                          										<c:choose>
														          <c:when test="${rowCounter.count % 1 == 0 && rowCounter.count != 0}">
														          </tr><tr>
														          </c:when>
														         <c:when test="${rowCounter.count != -1}">
														          </c:when>
															          <c:otherwise>
															          </c:otherwise>
															        </c:choose>
                          										</c:forEach>
                     </c:if>
                 </table>
                 </div>
                 </div>
			</fieldset>
			
				</td>
            </tr>
            </table>			
			</c:when>
			<c:when test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices=='Single section'}">
			<table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
			<tr>
				<td style="width:1295px;">				
				<fieldset style="margin-top:6px;min-height:150px; ">
				<legend>Services</legend>
				<div style="height:145px;width:auto;overflow-x:auto;overflow-y:auto;">
				<div style="max-width:980px; white-space: nowrap;">
				<table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
				<c:if test="${not empty incexcServiceList}">
			                                                  <c:forEach var="entry" items="${incexcServiceList}" varStatus="rowCounter">
                          										<c:if test="${entry.value!=''}"> 
                          										<c:set var="des" value="${entry.value}" />
                          										<td class="listwhitetext" style="">
                        										<c:set var="checkOpt" value="false" />
                          										<c:forTokens items="${servIncExcDefault}" delims="#" var="kc">
																    <c:if test="${kc == entry.key && !checkOpt}">
																    <c:set var="checkOpt" value="true" />
																    </c:if>
																</c:forTokens>                          										                          										
                          										
                          										<c:if test="${checkOpt}">
                          										<input type="checkbox" style="vertical-align:middle;" name="checkVC" id="${entry.key}" checked="checked" onclick="pickCheckBoxDataSec();" />
                          										</c:if>
                          										 <c:if test="${!checkOpt}">
                          										<input type="checkbox" style="vertical-align:middle;" name="checkVC" id="${entry.key}" onclick="pickCheckBoxDataSec();" />
                          										</c:if>
                          										${des} &nbsp;</td>
                          										</c:if>
                          										<c:choose>
														          <c:when test="${rowCounter.count % 2 == 0 && rowCounter.count != 0}">
														          </tr><tr>
														          </c:when>
														         <c:when test="${rowCounter.count != -1}">
														          </c:when>
															          <c:otherwise>
															          </c:otherwise>
															        </c:choose>
                          										</c:forEach>
                      </c:if>
                 </table>
                 </div>
                 </div>
			</fieldset>
				</td>
            </tr>
            </table>
			</c:when>
			<c:otherwise></c:otherwise>
			</c:choose>

<script type="text/JavaScript">

</script>


