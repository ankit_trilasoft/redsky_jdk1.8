<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 <head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>   
<s:form id="partnerDetailsForm" method="post" > 

<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin:0px;width:100%;">
<tr>
 <td valign="top">
 <div style="overflow-y:auto;height:215px;">
 <c:if test="${SearchforAutocompleteListAgentDetails!='[]'}">
 <display:table name="SearchforAutocompleteListAgentDetails" class="table pr-f11" id="SearchforAutocompleteListAgentDetails" style="width:100%;font-size:1em;"> 
   	<display:column title="Code" >
   	<c:set var="d" value="'"/>
   	<c:set var="des" value="${fn:replace(SearchforAutocompleteListAgentDetails.lastName,d, '~')}" />
   	<a onclick="copyAgentDetails('${SearchforAutocompleteListAgentDetails.partnerCode}','${des}','${autocompleteDivId}');">${SearchforAutocompleteListAgentDetails.partnerCode}</a>
   	</display:column>
  	<%-- <display:column  title="Name" ><c:out value="${SearchforAutocompleteListAgentDetails.firstName} ${SearchforAutocompleteListAgentDetails.lastName}" /></display:column> --%>
  	<display:column property="lastName" title="Name" ></display:column>
  	<display:column property="aliasName"   title="Alias Name"></display:column>
  	<c:if test="${(partnerType == 'AG' || partnerType == 'VN')}">  
    <display:column title="Map" style="width:45px; text-align: center;"><a>
    	<img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${SearchforAutocompleteListAgentDetails.terminalAddress1}','${SearchforAutocompleteListAgentDetails.terminalAddress2}','${SearchforAutocompleteListAgentDetails.terminalCity}','${SearchforAutocompleteListAgentDetails.terminalZip}','${SearchforAutocompleteListAgentDetails.terminalState}','${SearchforAutocompleteListAgentDetails.terminalCountry}');"/></a></display:column>  
    <display:column title="Acct Ref #"  titleKey="partner.rank" style="width:35px">
    	<a><img align="middle" title="Acct Ref #" onclick="findUserPermission('${partnerList.partnerCode}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
    </display:column>
    </c:if>
	<display:column property="billingCountry" title="Country" ></display:column>	
	<display:column property="billingState" title="State" ></display:column>
	<display:column property="billingCity" title="City" ></display:column>
</display:table>
</c:if>
<c:if test="${SearchforAutocompleteListAgentDetails=='[]'}">
  <display:table name="SearchforAutocompleteListAgentDetails" class="table pr-f11" id="SearchforAutocompleteListAgentDetails" style="width:100%;font-size:1em;">
	<display:column title="Code" ></display:column>
  	<display:column  title="Name" ></display:column>
  	<display:column  sortable="true"  title="Alias Name"></display:column>
  	<c:if test="${(partnerType == 'AG' || partnerType == 'VN')}">  
    <display:column title="Map" style="width:45px; text-align: center;"></display:column>  
    <display:column title="Acct Ref #" sortable="true" titleKey="partner.rank" style="width:35px">
    	
    </display:column>
    </c:if>
	<display:column  title="Country" ></display:column>	
	<display:column  title="State" ></display:column>
	<display:column  title="City" ></display:column>
	<td  class="listwhitetextWhite pr-f11" colspan="2">Nothing Found To Display.</td>  
</display:table>
<script type="text/javascript">
//closeMyChargeDiv2('${chargeCodeId}','${id}');
</script>
 </c:if>
</div>
</td>
<c:if test="${SearchforAutocompleteListAgentDetails!='[]'}">
 <td  align="right" valign="top"><img align="right" class="openpopup" style="position:absolute;top:1px;right:2px;" onclick="closeMyAgentDiv('','','${autocompleteDivId}');" src="<c:url value='/images/closetooltip.gif'/>" /></td>
 </c:if>
  <c:if test="${SearchforAutocompleteListAgentDetails=='[]'}">
   <td  align="right" valign="top"><img align="right" class="openpopup" style="position:absolute;top:1px;right:2px;" onclick="closeMyAgentDiv('','','${autocompleteDivId}');" src="<c:url value='/images/closetooltip.gif'/>" /></td>
  </c:if>
</tr>
</table> 
<script>
function findUserPermission(name,position) { 
	  var url="findAcctRefNumList.html?ajax=1&decorator=simple&popup=true&code=" + encodeURI(name);
	  ajax_showTooltip(url,position);	
	  }
	  
function openOriginLocation(address1,address2,city,zip,state,country) {
	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address1+','+address2+','+city+','+zip+','+state+','+country);
}
</script> 
</s:form>