<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="accountLineList.title"/></title>   
    <meta name="heading" content="<fmt:message key='accountLineList.heading'/>"/> 
<style>
 span.pagelinks {display:block;font-size:0.85em;margin-bottom:5px;margin-top:-18px;padding:2px 0;text-align:right;width:106%;}
.tdheight-footer {border: 1px solid #e0e0e0;padding: 0;}
form .table{font-size:1em;}
#mainPopup {padding:10px;}
#overlay { filter:alpha(opacity=70); -moz-opacity:0.7; 
-khtml-opacity: 0.7; opacity: 0.7; 
position:fixed; width:100%; height:100%;left:0px;top:0px; 
z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
.prWidth {width:10px;}
 </style>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script> 
</head>
<c:choose>
<c:when test="${discountUserFlag.pricingRevision && !discountUserFlag.pricingActual && pageName=='SO'}">
 <c:set var="entitleWidth" value="1px"/>
 <c:set var="estimateWidth" value="68px"/>
 <c:set var="estimaterevenueWidth" value="10px"/>
 <c:set var="sWidth" value="45%"/>
</c:when>
<c:when test="${!discountUserFlag.pricingRevision && discountUserFlag.pricingActual && pageName=='SO'}">
 <c:set var="entitleWidth" value="1px"/>
 <c:set var="estimateWidth" value="25px"/>
 <c:set var="estimaterevenueWidth" value="10px"/>
 <c:set var="actualExpenseWidth" value="16%"/>
 <c:set var="sWidth" value="27%"/>
</c:when>
<c:when test="${discountUserFlag.pricingActual && discountUserFlag.pricingRevision && pageName=='SO'}">
 <c:set var="entitleWidth" value="1px"/>
 <c:set var="estimateWidth" value="20px"/>
 <c:set var="estimaterevenueWidth" value="100px"/>
 <c:set var="actualExpenseWidth" value="16%"/>
 <c:set var="actualRevenueWidth" value="12%"/>
 <c:set var="sWidth" value="28%"/>
</c:when>
<c:when test="${pageName=='AC'}">
 <c:set var="entitleWidth" value="1px"/>
 <c:set var="estimateWidth" value="20px"/>
 <c:set var="estimaterevenueWidth" value="100px"/>
 <c:set var="actualExpenseWidth" value="16%"/>
 <c:set var="actualRevenueWidth" value="12%"/>
 <c:set var="sWidth" value="26%"/>
</c:when>
<c:otherwise>
 <c:set var="entitleWidth" value="75px"/> 
 <c:set var="estimateWidth" value="1px"/>
 <c:set var="sWidth" value="67%"/>
</c:otherwise>
</c:choose>

<s:set name="checkLHF" id="checkLHF"  value="false" scope="request" />
<s:set name="idForCheckLHF" id="idForCheckLHF" value="0" scope="request" />
<s:set name="bulkUpdateAccountLineList" value="bulkUpdateAccountLineList" scope="request"/>
<s:hidden name="pageName" value="<%=request.getParameter("pageName") %>"/>
<c:set var="pageName"  value="<%=request.getParameter("pageName") %>"/>

<table cellspacing="0" cellpadding="0" style="margin:0px;width:100%;">
	<tr>
 <td style="width:${sWidth}"></td>
 <td align="right" class="listwhitetext"><b>All&nbsp;Entitle</b></td>
 <td style="width:${entitleWidth}"><input type="checkbox" id="selectAllEstimateExpense" onclick="selectAllBulkCompute(this,'entitleCheck');"/></td>
 
 <td class="s-wid-col"></td>
 <td align="right" class="listwhitetext"><b>All&nbsp;Est</b></td>
 <td style="width:${estimateWidth}"><input type="checkbox" id="selectAllEstimateExpense" onclick="selectAllBulkCompute(this,'estimateCheck');"/></td>
 
 <c:if test="${discountUserFlag.pricingRevision && pageName=='SO'}"> 
  <td class="s-wid-col"></td>
  <td align="right" class="listwhitetext w-short"><b>All&nbsp;Rev</b></td>
  <td style="width:${estimaterevenueWidth}"><input type="checkbox" id="selectAllEstimateRevenue" onclick="selectAllBulkCompute(this,'revenueCheck');"/></td>
 </c:if>
 
 <c:if test="${discountUserFlag.pricingActual && pageName=='SO'}"> 
  <td><input type="checkbox" id="selectAllActualExpense" onclick="selectAllBulkCompute(this,'actualExpenseCheck');"/></td>
  <td class="s-wid-col"></td>
  <td style="width:${actualExpenseWidth}" class="listwhitetext"><b>All&nbsp;Act&nbsp;Exp</b></td>
   
  <td><input type="checkbox" id="selectAllActualRevenue" onclick="selectAllBulkCompute(this,'actualRevenueCheck');"/></td>
  <td class="s-wid-col"></td>
  <td align="left" style="width:${actualRevenueWidth}" class="listwhitetext alignR"><b>All&nbsp;Act&nbsp;Rev</b></td> 
 </c:if>
 
 <c:if test="${pageName=='AC'}"> 
  <td class="s-wid-col"></td>
  <td align="right" class="listwhitetext w-short"><b>All&nbsp;Rev</b></td>
  <td style="width:25px"><input type="checkbox" id="selectAllEstimateRevenue" onclick="selectAllBulkCompute(this,'revenueCheck');"/></td>
 </c:if>
 
 <c:if test="${pageName=='AC'}"> 
  <td><input type="checkbox" id="selectAllActualExpense" onclick="selectAllBulkCompute(this,'actualExpenseCheck');"/></td>
  <td class="s-wid-col"></td>
  <td class="listwhitetext aaE-w alignC"><b>All&nbsp;Act&nbsp;Exp</b></td>
   
  <td><input type="checkbox" id="selectAllActualRevenue" onclick="selectAllBulkCompute(this,'actualRevenueCheck');"/></td>
  <td class="s-wid-col"></td>
  <td align="left" class="listwhitetext alignR"><b>All&nbsp;Act&nbsp;Rev</b></td> 
 </c:if>
 
 </tr>
</table>
<s:form name="bulkUpdate" method="post" action="">
<display:table name="bulkUpdateAccountLineList" class="table" requestURI="" id="accountLineList" style="width:100%;!margin-top:1px;" defaultsort="1" pagesize="100" >   
     <display:column sortable="true" sortProperty="accountLineNumber" titleKey="accountLine.accountLineNumber"  style="width:5%;padding:0px;"><c:out value="${accountLineList.accountLineNumber}" /></display:column>
     <display:column  sortable="true" title="Charge Code" style="width:25%;padding:0px;"> <c:out value="${accountLineList.chargeCode}" /></display:column>
     <display:column  sortable="true" title="Entitle" style="width:10%;border-right:medium solid #d1d1d1; text-align:right;padding-right:0em;!important" >
             <c:choose>
             <c:when test="${accountLineList.chargeCode == null || accountLineList.chargeCode == '' || accountLineList.category=='Internal Cost'}">
             <input type="checkbox" id="entitle${accountLineList.id}" name="entitle" disabled="true"/>  
             </c:when>
             <c:otherwise> 
             <input type="checkbox" id="entitle${accountLineList.id}" name="entitle"  onclick=""/> 
             </c:otherwise>     
             </c:choose>
     </display:column>
     <display:column  sortable="true" title="Estimate" style="width:10%;border-right:medium solid #d1d1d1; text-align:right;padding-right:0em;!important" >
            <c:choose>
            <c:when test="${accountLineList.chargeCode == null || accountLineList.chargeCode == '' || accountLineList.category=='Internal Cost'}">
             <input type="checkbox" id="estimate${accountLineList.id}" name="estimate"  disabled="true"/> 
            </c:when>
            <c:otherwise> 
             <input type="checkbox" id="estimate${accountLineList.id}" name="estimate"  onclick=""/> 
            </c:otherwise>
            </c:choose>
     </display:column>
     <c:if test="${discountUserFlag.pricingRevision && pageName=='SO'}">
	     <display:column  sortable="true" title="Revision" style="width:12%;border-right:medium solid #d1d1d1; text-align:right;padding-right:0em;!important">
	            <c:choose>
	            <c:when test="${accountLineList.chargeCode == null || accountLineList.chargeCode == '' || accountLineList.category=='Internal Cost'}">
	            <input type="checkbox" id="revision${accountLineList.id}" name="revision"  disabled="true"/> 
	            </c:when>
	            <c:otherwise> 
	            <input type="checkbox" id="revision${accountLineList.id}" name="revision"  onclick=""/> 
	            </c:otherwise>
	            </c:choose>
	      </display:column>
     </c:if>
     <c:if test="${discountUserFlag.pricingActual && pageName=='SO'}">
     <display:column headerClass="containeralign"  sortable="true"  title="Actual Expense" style="width:20%;border-right:medium solid #d1d1d1;padding-right:0em; !important">
            <c:choose>
            <c:when test="${(accountLineList.chargeCode == null || accountLineList.chargeCode == '' || accountLineList.actgCode == null || accountLineList.actgCode == '' || accountLineList.vendorCode == null || accountLineList.vendorCode == '' || accountLineList.category=='Internal Cost')  || (systemDefaultVatCalculation=='true' && (!accountLineList.VATExclude) && (accountLineList.payVatDescr==''))}">
            <input type="checkbox" id="actualExpense${accountLineList.id}" name="actualExpense"  disabled="true"/> 
             </c:when> 
            <c:otherwise>
            <c:choose>
            <c:when test="${((!trackingStatus.soNetworkGroup && networkAgent && billingCMMContractType) 
            || (!trackingStatus.soNetworkGroup && ((freezeAccValue!='Invoicing' && not empty accountLineList.payAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.invoiceNumber!='' || not empty accountLineList.invoiceNumber)))) 
            || (trackingStatus.soNetworkGroup && (!(trackingStatus.accNetworkGroup)) && networkAgent && billingDMMContractType && ((freezeAccValue!='Invoicing' && not empty accountLineList.payAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.invoiceNumber!='' || not empty accountLineList.invoiceNumber)))) 
            || (trackingStatus.soNetworkGroup && ((trackingStatus.accNetworkGroup)) && !networkAgent && (billingDMMContractType || billingCMMContractType)  &&  ((freezeAccValue!='Invoicing' && not empty accountLineList.payAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.invoiceNumber!='' || not empty accountLineList.invoiceNumber))))) 
               }">
               <input type="checkbox" id="actualExpense${accountLineList.id}" name="actualExpense"  disabled="true"/>
            </c:when>
             <c:otherwise>
            <input type="checkbox" id="actualExpense${accountLineList.id}" name="actualExpense"  /> 
            </c:otherwise>
            </c:choose> 
            </c:otherwise>
            </c:choose>
            <div align="right"  style="margin:2px 0 0;float:right; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${accountLineList.actualExpense}" /></div>
    </display:column>    
    <display:column headerClass="containeralign"  sortable="true"  title="Actual Revenue" style="width:20%;padding-right:0em; !important"> 
           <c:choose>
            <c:when test="${(accountLineList.chargeCode == null || accountLineList.chargeCode == '' || accountLineList.billToCode == null || accountLineList.billToCode == '' || accountLineList.category=='Internal Cost' )|| (systemDefaultVatCalculation=='true' && (!accountLineList.VATExclude) && (accountLineList.recVatDescr==''))}">
             <input type="checkbox" id="actualRevenue${accountLineList.id}" name="actualRevenue"  disabled="true"/>
            </c:when>
            <c:otherwise>
            <c:choose>
            <c:when test="${(!trackingStatus.soNetworkGroup &&  ((freezeAccValue!='Invoicing' && not empty accountLineList.recAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.recInvoiceNumber!='' || not empty accountLineList.recInvoiceNumber)))) 
            || (trackingStatus.soNetworkGroup && (!(trackingStatus.accNetworkGroup)) && networkAgent && billingDMMContractType && ((freezeAccValue!='Invoicing' && not empty accountLineList.recAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.recInvoiceNumber!='' || not empty accountLineList.recInvoiceNumber)))) 
            || (trackingStatus.soNetworkGroup && ((trackingStatus.accNetworkGroup)) && !networkAgent && (billingDMMContractType || billingCMMContractType)  &&  ((freezeAccValue!='Invoicing' && not empty accountLineList.recAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.recInvoiceNumber!='' || not empty accountLineList.recInvoiceNumber)) || accountLineList.utsiRecAccDateFlag || accountLineList.utsiPayAccDateFlag)) 
               }">
               <input type="checkbox" id="actualRevenue${accountLineList.id}" name="actualRevenue"  disabled="true"/>
            </c:when>
             <c:otherwise>
            <input type="checkbox" id="actualRevenue${accountLineList.id}" name="actualRevenue"  />
            </c:otherwise>
            </c:choose> 
            </c:otherwise>
            </c:choose>
            <div align="right"  style="margin:2px 0 0;float:right; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${accountLineList.actualRevenue}" /></div>
    </display:column> 
    </c:if>
         <c:if test="${pageName=='AC'}">
	     <display:column  sortable="true" title="Revision" style="width:12%;border-right:medium solid #d1d1d1; text-align:right;padding-right:0em;!important">
	            <c:choose>
	            <c:when test="${accountLineList.chargeCode == null || accountLineList.chargeCode == '' || accountLineList.category=='Internal Cost'}">
	            <input type="checkbox" id="revision${accountLineList.id}" name="revision"  disabled="true"/> 
	            </c:when>
	            <c:otherwise> 
	            <input type="checkbox" id="revision${accountLineList.id}" name="revision"  onclick=""/> 
	            </c:otherwise>
	            </c:choose>
	      </display:column>
     </c:if>
     <c:if test="${pageName=='AC'}">
     <display:column headerClass="containeralign"  sortable="true"  title="Actual Expense" style="width:20%;border-right:medium solid #d1d1d1;padding-right:0em; !important">
            <c:choose>
            <c:when test="${(accountLineList.chargeCode == null || accountLineList.chargeCode == '' || accountLineList.actgCode == null || accountLineList.actgCode == '' || accountLineList.vendorCode == null || accountLineList.vendorCode == '' || accountLineList.category=='Internal Cost')  || (systemDefaultVatCalculation=='true' && (!accountLineList.VATExclude) && (accountLineList.payVatDescr==''))}">
            <input type="checkbox" id="actualExpense${accountLineList.id}" name="actualExpense"  disabled="true"/> 
             </c:when> 
            <c:otherwise>
            <c:choose>
            <c:when test="${((!trackingStatus.soNetworkGroup && networkAgent && billingCMMContractType) 
            || (!trackingStatus.soNetworkGroup && ((freezeAccValue!='Invoicing' && not empty accountLineList.payAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.invoiceNumber!='' || not empty accountLineList.invoiceNumber)))) 
            || (trackingStatus.soNetworkGroup && (!(trackingStatus.accNetworkGroup)) && networkAgent && billingDMMContractType && ((freezeAccValue!='Invoicing' && not empty accountLineList.payAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.invoiceNumber!='' || not empty accountLineList.invoiceNumber)))) 
            || (trackingStatus.soNetworkGroup && ((trackingStatus.accNetworkGroup)) && !networkAgent && (billingDMMContractType || billingCMMContractType)  &&  ((freezeAccValue!='Invoicing' && not empty accountLineList.payAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.invoiceNumber!='' || not empty accountLineList.invoiceNumber))))) 
               }">
               <input type="checkbox" id="actualExpense${accountLineList.id}" name="actualExpense"  disabled="true"/>
            </c:when>
             <c:otherwise>
            <input type="checkbox" id="actualExpense${accountLineList.id}" name="actualExpense"  /> 
            </c:otherwise>
            </c:choose> 
            </c:otherwise>
            </c:choose>
            <div align="right"  style="margin:2px 0 0;float:right; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${accountLineList.actualExpense}" /></div>
    </display:column>    
    <display:column headerClass="containeralign"  sortable="true"  title="Actual Revenue" style="width:20%;padding-right:0em; !important"> 
           <c:choose>
            <c:when test="${(accountLineList.chargeCode == null || accountLineList.chargeCode == '' || accountLineList.billToCode == null || accountLineList.billToCode == '' || accountLineList.category=='Internal Cost' )|| (systemDefaultVatCalculation=='true' && (!accountLineList.VATExclude) && (accountLineList.recVatDescr==''))}">
             <input type="checkbox" id="actualRevenue${accountLineList.id}" name="actualRevenue"  disabled="true"/>
            </c:when>
            <c:otherwise>
            <c:choose>
            <c:when test="${(!trackingStatus.soNetworkGroup &&  ((freezeAccValue!='Invoicing' && not empty accountLineList.recAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.recInvoiceNumber!='' || not empty accountLineList.recInvoiceNumber)))) 
            || (trackingStatus.soNetworkGroup && (!(trackingStatus.accNetworkGroup)) && networkAgent && billingDMMContractType && ((freezeAccValue!='Invoicing' && not empty accountLineList.recAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.recInvoiceNumber!='' || not empty accountLineList.recInvoiceNumber)))) 
            || (trackingStatus.soNetworkGroup && ((trackingStatus.accNetworkGroup)) && !networkAgent && (billingDMMContractType || billingCMMContractType)  &&  ((freezeAccValue!='Invoicing' && not empty accountLineList.recAccDate) || (freezeAccValue=='Invoicing' && (accountLineList.recInvoiceNumber!='' || not empty accountLineList.recInvoiceNumber)) || accountLineList.utsiRecAccDateFlag || accountLineList.utsiPayAccDateFlag)) 
               }">
               <input type="checkbox" id="actualRevenue${accountLineList.id}" name="actualRevenue"  disabled="true"/>
            </c:when>
             <c:otherwise>
            <input type="checkbox" id="actualRevenue${accountLineList.id}" name="actualRevenue"  />
            </c:otherwise>
            </c:choose> 
            </c:otherwise>
            </c:choose>
            <div align="right"  style="margin:2px 0 0;float:right; text-align:right;"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${accountLineList.actualRevenue}" /></div>
    </display:column> 
    </c:if>
    </display:table> 
        
    <div id="overlay">
    <table cellspacing="0" cellpadding="0" border="0" width="100%" >
   <tr>
   <td align="center">
   <table cellspacing="0" cellpadding="3" align="center">
   <tr>
   <td height="200px"></td>
   </tr>
   <tr>
        <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
            <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait...</font>
        </td>
        </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
   </div>
        
     </s:form>
     
    <c:if test="${bulkUpdateAccountLineList!='[]'}">
     <input type="button" class="cssbuttonA" style="width:115px;"  name="Compute Selection"  value="Compute Selection" onclick="calculateBulkCompute('${pageName}')" />
    </c:if>
    <c:if test="${bulkUpdateAccountLineList=='[]'}">
    <input type="button" class="cssbuttonA" style="width:115px;"  name="Compute Selection"  value="Compute Selection" disabled="disabled" />
    </c:if>
     <input type="button" class="cssbuttonA" style="width:120px;"    value="Clear check boxes" onclick="checkAll(this)" />
     <input type="button" class="cssbuttonA" style="width:100px;"    value="Close Screen" onclick="javascript:window.close()" />    
     
     <script type="text/javascript">
     
     function calculateBulkCompute(pageName){
         var bulkComputeValue='';
         var entitle ='';
         var estimate = '';
         var revision = '';
         var actualExpense = '';
         var actualRevenue = '';
         <c:forEach var="entry" items="${bulkUpdateAccountLineList}">
         var id="${entry.id}";
         var chargeCode = '${entry.chargeCode}';
         var shipNumber = '${entry.shipNumber}';
         var lineNumber = '${entry.accountLineNumber}';
         var category = '${entry.category}';
         var billingContract = '${billing.contract}';
         var sid = '${serviceOrder.id}';
         var comptetive ='${customerFile.comptetive}';
         var companyDivision = '${entry.companyDivision}';
         var job = '${serviceOrder.job}';
        
          entitle = document.getElementById('entitle'+id).checked;
          estimate = document.getElementById('estimate'+id).checked;
          <c:if test="${(pageName=='AC') || (discountUserFlag.pricingRevision && pageName=='SO')}">
          revision = document.getElementById('revision'+id).checked;
          </c:if>
          <c:if test="${(pageName=='AC') || (discountUserFlag.pricingActual && pageName=='SO')}">
          actualExpense = document.getElementById('actualExpense'+id).checked;
          actualRevenue = document.getElementById('actualRevenue'+id).checked; 
          </c:if>
          
         if(entitle == true || estimate == true || revision == true || actualExpense == true || actualRevenue == true )
         {
        	 if(bulkComputeValue != ''){
             bulkComputeValue = bulkComputeValue+"~";    
         }
        	 
         if(entitle == true){
             if(bulkComputeValue == ''){
                 bulkComputeValue ="computeEntitle"
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+"computeEntitle"
             }
         }else{
             if(bulkComputeValue == ''){
                 bulkComputeValue =" "
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+" "
             }
         }
         
          if(estimate == true){
             if(bulkComputeValue == ''){
                 bulkComputeValue ="computeEestimate"
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+"computeEstimate"
             }
         }else{
             if(bulkComputeValue == ''){
                 bulkComputeValue =" "
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+" "
             }
         }
         
         if(revision == true){
             if(bulkComputeValue == ''){
                 bulkComputeValue ="computeRevision"
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+"computeRevision"
             }
         } else {
             if(bulkComputeValue == ''){
                 bulkComputeValue =" "
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+" "
             }
         }
         
         if(actualExpense == true){
             if(bulkComputeValue == ''){
                 bulkComputeValue ="computeExpense"
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+"computeExpense"
             }   
         } else {
             if(bulkComputeValue == ''){
                 bulkComputeValue =" "
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+" "
             }
         }
         
         if(actualRevenue == true){
             if(bulkComputeValue == ''){
                 bulkComputeValue ="computeRevenue"
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+"computeRevenue"
             }   
         } else {
             if(bulkComputeValue == ''){
                 bulkComputeValue =" "
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+" "
             }
         }
         
         if(bulkComputeValue != ''){             
             bulkComputeValue = bulkComputeValue+"@"+lineNumber+"@"+chargeCode+"@"+shipNumber+"@"+companyDivision+"@"+category+"@"+billingContract+"@"+comptetive+"@"+sid+"@"+job+"@"+id;   
         }
         }
         </c:forEach>
         if(bulkComputeValue != ''){
         showOrHide(1);
         $.ajax({
             type: "POST",
             url: "bulkComputeCalulation.html?ajax=1&decorator=simple&popup=true",
             data: {bulkComputeValue:bulkComputeValue,sid:sid},
             success: function (data, textStatus, jqXHR) {
            	if(pageName=="SO"){
            	 window.opener.findSoAllPricing();
            	  window.close();
            	}else{
            	 refreshAndClose();
            	}
             },
           error: function (data, textStatus, jqXHR) {
                       showOrHide(0);
                   }
           });  
         }else{
        	 alert("Please select any check box. ")
         }   
   }
     
     function selectAllBulkCompute(target,type){
         var bulkComputeValue='';
         var entitle ='';
         var estimate = '';
         var revision = '';
         var actualExpense = '';
         var actualRevenue = '';
         <c:forEach var="entry" items="${bulkUpdateAccountLineList}">
         var id="${entry.id}";
         var chargeCode = '${entry.chargeCode}';
         var shipNumber = '${entry.shipNumber}';
         var lineNumber = '${entry.accountLineNumber}';
         var category = '${entry.category}';
         var billingContract = '${billing.contract}';
         var sid = '${serviceOrder.id}';
         var comptetive ='${customerFile.comptetive}';
         var companyDivision = '${entry.companyDivision}';
         var job = '${serviceOrder.job}';
        
         if(type=='entitleCheck'){
		 	    if(target.checked==true){
		 	    	<c:choose>
		             <c:when test="${entry.chargeCode == null || entry.chargeCode == '' || entry.category=='Internal Cost'}">
		             	entitle = document.getElementById('entitle'+id).checked=false;  
		             </c:when>
		             <c:otherwise> 
		             	entitle = document.getElementById('entitle'+id).checked=true; 
		             </c:otherwise>     
		             </c:choose>
		 	    }else if(target.checked==false){
		 	    	entitle = document.getElementById('entitle'+id).checked=false;
		 	    }
          }
       	 if(type=='estimateCheck'){
		 	    if(target.checked==true){
		 	    	<c:choose>
		            <c:when test="${entry.chargeCode == null || entry.chargeCode == '' || entry.category=='Internal Cost'}">
		            	estimate = document.getElementById('estimate'+id).checked=false; 
		            </c:when>
		            <c:otherwise> 
		            	estimate = document.getElementById('estimate'+id).checked=true;
		            </c:otherwise>
		            </c:choose>
		 	    }else if(target.checked==false){
		 	    	estimate = document.getElementById('estimate'+id).checked=false;
		 	    }
     	 }
       	if(type=='revenueCheck'){
	 	    if(target.checked==true){
	 	    	<c:choose>
	            <c:when test="${entry.chargeCode == null || entry.chargeCode == '' || entry.category=='Internal Cost'}">
	            	revision = document.getElementById('revision'+id).checked=false; 
	            </c:when>
	            <c:otherwise> 
	            	revision = document.getElementById('revision'+id).checked=true; 
	            </c:otherwise>
	            </c:choose>
	 	    }else if(target.checked==false){
	 	    	revision = document.getElementById('revision'+id).checked=false;
	 	    }
 	 	}
       	if(type=='actualExpenseCheck'){
	 	    if(target.checked==true){
	 	    	<c:choose>
	            <c:when test="${(entry.chargeCode == null || entry.chargeCode == '' || entry.actgCode == null || entry.actgCode == '' || entry.vendorCode == null || entry.vendorCode == '' || entry.category=='Internal Cost')  || (systemDefaultVatCalculation=='true' && (!entry.VATExclude) && (entry.payVatDescr==''))}">
	            	actualExpense = document.getElementById('actualExpense'+id).checked=false; 
	            </c:when> 
	            <c:otherwise>
	            <c:choose>
	            <c:when test="${((!trackingStatus.soNetworkGroup && networkAgent && billingCMMContractType) || (!trackingStatus.soNetworkGroup && ((freezeAccValue!='Invoicing' && not empty entry.payAccDate) || (freezeAccValue=='Invoicing' && (entry.invoiceNumber!='' || not empty entry.invoiceNumber)))) || (trackingStatus.soNetworkGroup && (!(trackingStatus.accNetworkGroup)) && networkAgent && billingDMMContractType && ((freezeAccValue!='Invoicing' && not empty entry.payAccDate) || (freezeAccValue=='Invoicing' && (entry.invoiceNumber!='' || not empty entry.invoiceNumber)))) || (trackingStatus.soNetworkGroup && ((trackingStatus.accNetworkGroup)) && !networkAgent && (billingDMMContractType || billingCMMContractType)  &&  ((freezeAccValue!='Invoicing' && not empty entry.payAccDate) || (freezeAccValue=='Invoicing' && (entry.invoiceNumber!='' || not empty entry.invoiceNumber)))))}">
	            	actualExpense = document.getElementById('actualExpense'+id).checked=false;
	            </c:when>
	            <c:otherwise>
	            	actualExpense = document.getElementById('actualExpense'+id).checked=true; 
	            </c:otherwise>
	            </c:choose> 
	            </c:otherwise>
	            </c:choose>
	 	    }else if(target.checked==false){
	 	    	actualExpense = document.getElementById('actualExpense'+id).checked=false;
	 	    }
 	 	}
       	if(type=='actualRevenueCheck'){
	 	    if(target.checked==true){
	 	    	<c:choose>
	            <c:when test="${(entry.chargeCode == null || entry.chargeCode == '' || entry.billToCode == null || entry.billToCode == '' || entry.category=='Internal Cost' )|| (systemDefaultVatCalculation=='true' && (!entry.VATExclude) && (entry.recVatDescr==''))}">
	            	actualRevenue = document.getElementById('actualRevenue'+id).checked=false;
	            </c:when>
	            <c:otherwise>
	            <c:choose>
	            <c:when test="${(!trackingStatus.soNetworkGroup &&  ((freezeAccValue!='Invoicing' && not empty entry.recAccDate) || (freezeAccValue=='Invoicing' && (entry.recInvoiceNumber!='' || not empty entry.recInvoiceNumber)))) || (trackingStatus.soNetworkGroup && (!(trackingStatus.accNetworkGroup)) && networkAgent && billingDMMContractType && ((freezeAccValue!='Invoicing' && not empty entry.recAccDate) || (freezeAccValue=='Invoicing' && (entry.recInvoiceNumber!='' || not empty entry.recInvoiceNumber)))) || (trackingStatus.soNetworkGroup && ((trackingStatus.accNetworkGroup)) && !networkAgent && (billingDMMContractType || billingCMMContractType)  &&  ((freezeAccValue!='Invoicing' && not empty entry.recAccDate) || (freezeAccValue=='Invoicing' && (entry.recInvoiceNumber!='' || not empty entry.recInvoiceNumber)) || entry.utsiRecAccDateFlag || entry.utsiPayAccDateFlag))}">
	            	actualRevenue = document.getElementById('actualRevenue'+id).checked=false;
	            </c:when>
	            <c:otherwise>
	            	actualRevenue = document.getElementById('actualRevenue'+id).checked=true;
	            </c:otherwise>
	            </c:choose> 
	            </c:otherwise>
	            </c:choose>
	 	    }else if(target.checked==false){
	 	    	actualRevenue = document.getElementById('actualRevenue'+id).checked=false;
	 	    }
 	 	}
         
         if(entitle == true || estimate == true || revision == true || actualExpense == true || actualRevenue == true )
         {
        	 if(bulkComputeValue != ''){
             bulkComputeValue = bulkComputeValue+"~";    
         }
        	 
         if(entitle == true){
             if(bulkComputeValue == ''){
                 bulkComputeValue ="computeEntitle"
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+"computeEntitle"
             }
         }else{
             if(bulkComputeValue == ''){
                 bulkComputeValue =" "
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+" "
             }
         }
         
          if(estimate == true){
             if(bulkComputeValue == ''){
                 bulkComputeValue ="computeEestimate"
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+"computeEstimate"
             }
         }else{
             if(bulkComputeValue == ''){
                 bulkComputeValue =" "
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+" "
             }
         }
         
         if(revision == true){
             if(bulkComputeValue == ''){
                 bulkComputeValue ="computeRevision"
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+"computeRevision"
             }
         } else {
             if(bulkComputeValue == ''){
                 bulkComputeValue =" "
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+" "
             }
         }
         
         if(actualExpense == true){
             if(bulkComputeValue == ''){
                 bulkComputeValue ="computeExpense"
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+"computeExpense"
             }   
         } else {
             if(bulkComputeValue == ''){
                 bulkComputeValue =" "
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+" "
             }
         }
         
         if(actualRevenue == true){
             if(bulkComputeValue == ''){
                 bulkComputeValue ="computeRevenue"
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+"computeRevenue"
             }   
         } else {
             if(bulkComputeValue == ''){
                 bulkComputeValue =" "
             }else{
                 bulkComputeValue=bulkComputeValue+"@"+" "
             }
         }
         
         if(bulkComputeValue != ''){             
             bulkComputeValue = bulkComputeValue+"@"+lineNumber+"@"+chargeCode+"@"+shipNumber+"@"+companyDivision+"@"+category+"@"+billingContract+"@"+comptetive+"@"+sid+"@"+job+"@"+id;   
         }
         }
         </c:forEach>
   }
     
     function checkAll(ele) {
         var checkboxes = document.getElementsByTagName('input');
         if (ele.checked) {
             for (var i = 0; i < checkboxes.length; i++) {
                 if (checkboxes[i].type == 'checkbox') {
                     checkboxes[i].checked = true;
                 }
             }
         } else {
             for (var i = 0; i < checkboxes.length; i++) {
                 console.log(i)
                 if (checkboxes[i].type == 'checkbox') {
                     checkboxes[i].checked = false;
                 }
             }
         }
     }
     
     function refreshAndClose() {
         window.opener.location.reload(true);
         window.close();
     }
     
     
     function showOrHide(value) {
            if (value == 0) {
                if (document.layers)
                   document.layers["overlay"].visibility='hide';
                else
                   document.getElementById("overlay").style.visibility='hidden';
            }else if (value == 1) {
                if (document.layers)
                  document.layers["overlay"].visibility='show';
                else
                  document.getElementById("overlay").style.visibility='visible';
            }
        }
     
</script>

<script type="text/javascript">
 showOrHide(0);
</script>