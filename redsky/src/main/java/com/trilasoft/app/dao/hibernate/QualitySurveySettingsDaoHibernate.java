package com.trilasoft.app.dao.hibernate;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.QualitySurveySettingsDao;
import com.trilasoft.app.model.QualitySurveySettings;

public class QualitySurveySettingsDaoHibernate extends GenericDaoHibernate<QualitySurveySettings, Long> implements QualitySurveySettingsDao{
	public QualitySurveySettingsDaoHibernate()
	{
		super(QualitySurveySettings.class);
	}
	public List getQualitySurveySettingsDate(String accountId, String sessionCorpID,String mode){
		return getHibernateTemplate().find("from QualitySurveySettings where mode='"+mode+"' and corpID='"+sessionCorpID+"' and accountId=?",accountId);
	}
	public Map<String, String> findByParameter(String corpID, String parameter) { 
		try {
			List list;
			list = getSession().createSQLQuery("select code,description from refmaster where  parameter= '"	+ parameter + "' and code!='RLO' and corpID in ('"+ corpID + "','"+getParentCorpID(corpID)+"')  ORDER BY description")
					.addScalar("code", Hibernate.STRING)
					.addScalar("description", Hibernate.STRING).list();
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				String code = "";
				String description = "";
				Object[] row = (Object[]) iterator.next();
				if (row[0] != null) {
					code = row[0].toString();
				}
				if (row[1] != null) {
					description = row[1].toString();
				}

				parameterMap.put(code, description);
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	public String getParentCorpID(String sessionCorpID){
		String corpId="";
		List al=this.getSession().createSQLQuery("select parentCorpId from company where corpId ='"+sessionCorpID+"' ").list();
		if(al!=null && !al.isEmpty() && al.get(0)!=null && !al.get(0).toString().trim().equalsIgnoreCase("")){
			corpId=al.get(0).toString().trim();
		}
		return corpId;
	}
	public List getEmailConfigrationSoList(String sessionCorpID,String partnerCode,String partnerName){
		if(partnerCode==null)		{
			partnerCode="";
		}
		if(partnerName==null)		{
			partnerName="";
		}
		if(!partnerCode.equalsIgnoreCase(""))		{		
			partnerCode=partnerCode.replaceAll("'", "''").replaceAll(":", "''").replace("_", "''").replace("%", "''");
		}
		if(!partnerName.equalsIgnoreCase(""))		{
		partnerName=partnerName.replaceAll("'", "''").replaceAll(":", "''").replace("_", "''").replace("%", "''");		
		}
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		parameterMap=findByParameter(sessionCorpID,"JOB");
		String query1="select DISTINCT p2.partnerCode,p2.lastName from partnerpublic p1,partnerprivate p2" +
		" where p1.id=p2.partnerpublicid and (p1.isAccount is true OR p1.isAgent is true) and p2.customerFeedback='Quality Survey' and p2.corpid in ('"+sessionCorpID+"','"+getParentCorpID(sessionCorpID)+"') and p1.corpid in ('"+sessionCorpID+"','TSFT','"+getParentCorpID(sessionCorpID)+"') AND p1.status <> 'Inactive' AND p2.status <> 'Inactive' and p2.partnerCode like '"+partnerCode+"%' and p2.lastName like '"+partnerName+"%'";
		List list= this.getSession().createSQLQuery(query1).list();	
		List<SurveyEmailListDTO> surveyEmailList= new LinkedList();
		SurveyEmailListDTO surveyEmailDTO=null;
		Iterator it=list.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			surveyEmailDTO=new SurveyEmailListDTO();
			surveyEmailDTO.setPartnerCode(obj[0]);
			surveyEmailDTO.setLastName(obj[1]);
			surveyEmailDTO.setMode("Air/Sea/Truck");
			surveyEmailList.add(surveyEmailDTO);

			surveyEmailDTO=new SurveyEmailListDTO();
			surveyEmailDTO.setPartnerCode(obj[0]);
			surveyEmailDTO.setLastName(obj[1]);
			surveyEmailDTO.setMode("Relocation");
			surveyEmailList.add(surveyEmailDTO);
			for(Map.Entry<String, String> rec:parameterMap.entrySet()){
				surveyEmailDTO=new SurveyEmailListDTO();
				surveyEmailDTO.setPartnerCode(obj[0]);
				surveyEmailDTO.setLastName(obj[1]);
				surveyEmailDTO.setMode(rec.getKey());		
				surveyEmailList.add(surveyEmailDTO);
			}
		}
		return surveyEmailList;
	}
	
	public class SurveyEmailListDTO
	{
		
		private Object partnerCode;
		private Object lastName;
		private Object mode;
		public Object getPartnerCode() {
			return partnerCode;
		}
		public void setPartnerCode(Object partnerCode) {
			this.partnerCode = partnerCode;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getMode() {
			return mode;
		}
		public void setMode(Object mode) {
			this.mode = mode;
		}
	}
	public Boolean getExcludeFPU(String partnerCode,String corpId){
		Boolean FPU=false;
		List al=null;
		String temp[]=partnerCode.split("~");
		if(!temp[1].equalsIgnoreCase("Relocation")){
		al=getSession().createSQLQuery("select if(excludeFromParentQualityUpdate,'TRUE','FALSE') from partnerprivate where partnerCode='"+temp[0]+"' AND corpID in ('"+corpId+"','"+getParentCorpID(corpId)+"')").list();
		}else{
		al=getSession().createSQLQuery("select if(excludeFromParentQualityUpdateRelo,'TRUE','FALSE') from partnerprivate where partnerCode='"+temp[0]+"' AND corpID in ('"+corpId+"','"+getParentCorpID(corpId)+"')").list();			
		}
		if((al.get(0)!=null)&&(!al.isEmpty()))
		{
			if(al.get(0).toString().equalsIgnoreCase("TRUE"))
			{
				FPU=true;
			}else{
				FPU=false;
			}
		}
		return FPU;
	}
	public List getQualityEmailListByPartnerCode(String partnerCode, String corpID){
		String temp[]=partnerCode.split("~");
		return getHibernateTemplate().find("from QualitySurveySettings where mode='"+temp[1]+"' and corpID='"+corpID+"' and accountId=?",temp[0]);
		
	}
	public int getQualityListReference(String partnerCode, String corpID,String mode){
		return	this.getSession().createSQLQuery("delete from qualitysurveysettings where mode='"+mode+"' and corpID='"+corpID+"' and accountId='"+partnerCode+"'").executeUpdate();
	}
	public class CurrentEmailRecordPartnerDTO
	{
		private Object receiver;
		private Object body;
		private Object subject;
		private Object signature;
		private Object surveyEmailId;
		private Object surveyFrom;
		private Object recipient;
		private Object emailbodyCode;
		private Object subjectCode;
		private Object signatureCode;
		private Object timeSent;
		private Object surveyRecipientId;
		private Object surveyBodyId;
		private Object surveySubjectId;
		private Object surveySignatureId;
		
		public Object getReceiver() {
			return receiver;
		}
		public void setReceiver(Object receiver) {
			this.receiver = receiver;
		}
		public Object getBody() {
			return body;
		}
		public void setBody(Object body) {
			this.body = body;
		}
		public Object getSubject() {
			return subject;
		}
		public void setSubject(Object subject) {
			this.subject = subject;
		}
		public Object getSignature() {
			return signature;
		}
		public void setSignature(Object signature) {
			this.signature = signature;
		}
		public Object getSurveyEmailId() {
			return surveyEmailId;
		}
		public void setSurveyEmailId(Object surveyEmailId) {
			this.surveyEmailId = surveyEmailId;
		}
		public Object getSurveyFrom() {
			return surveyFrom;
		}
		public void setSurveyFrom(Object surveyFrom) {
			this.surveyFrom = surveyFrom;
		}
		public Object getRecipient() {
			return recipient;
		}
		public void setRecipient(Object recipient) {
			this.recipient = recipient;
		}
		public Object getEmailbodyCode() {
			return emailbodyCode;
		}
		public void setEmailbodyCode(Object emailbodyCode) {
			this.emailbodyCode = emailbodyCode;
		}
		public Object getSubjectCode() {
			return subjectCode;
		}
		public void setSubjectCode(Object subjectCode) {
			this.subjectCode = subjectCode;
		}
		public Object getSignatureCode() {
			return signatureCode;
		}
		public void setSignatureCode(Object signatureCode) {
			this.signatureCode = signatureCode;
		}
		public Object getTimeSent() {
			return timeSent;
		}
		public void setTimeSent(Object timeSent) {
			this.timeSent = timeSent;
		}
		public Object getSurveyRecipientId() {
			return surveyRecipientId;
		}
		public void setSurveyRecipientId(Object surveyRecipientId) {
			this.surveyRecipientId = surveyRecipientId;
		}
		public Object getSurveyBodyId() {
			return surveyBodyId;
		}
		public void setSurveyBodyId(Object surveyBodyId) {
			this.surveyBodyId = surveyBodyId;
		}
		public Object getSurveySubjectId() {
			return surveySubjectId;
		}
		public void setSurveySubjectId(Object surveySubjectId) {
			this.surveySubjectId = surveySubjectId;
		}
		public Object getSurveySignatureId() {
			return surveySignatureId;
		}
		public void setSurveySignatureId(Object surveySignatureId) {
			this.surveySignatureId = surveySignatureId;
		}
		
	}
	public class SurveyEmailDTO
	{
		
		private Object partnerCode;
		private Object lastName;
		private Object mode;
		private Object id;
		private Object shipNumber;
		private Object seqNumber;
		private Object test;
		private Object job;
		private Object serviceType;
		private Object originAgent;
		private Object destinationAgent;
		private Object originCountry;
		private Object destinationCountry;
		private Object email;
		private Object localHrEmail;
		private Object moveManager;
		private Object title;
		private Object actualDelivery;
		private Object accountManager;
		private Object accountName;
		
		
		public Object getPartnerCode() {
			return partnerCode;
		}
		public void setPartnerCode(Object partnerCode) {
			this.partnerCode = partnerCode;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getMode() {
			return mode;
		}
		public void setMode(Object mode) {
			this.mode = mode;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		public Object getTest() {
			return test;
		}
		public void setTest(Object test) {
			this.test = test;
		}
		public Object getSeqNumber() {
			return seqNumber;
		}
		public void setSeqNumber(Object seqNumber) {
			this.seqNumber = seqNumber;
		}
		public Object getJob() {
			return job;
		}
		public void setJob(Object job) {
			this.job = job;
		}
		public Object getServiceType() {
			return serviceType;
		}
		public void setServiceType(Object serviceType) {
			this.serviceType = serviceType;
		}
		public Object getOriginAgent() {
			return originAgent;
		}
		public void setOriginAgent(Object originAgent) {
			this.originAgent = originAgent;
		}
		public Object getDestinationAgent() {
			return destinationAgent;
		}
		public void setDestinationAgent(Object destinationAgent) {
			this.destinationAgent = destinationAgent;
		}
		public Object getOriginCountry() {
			return originCountry;
		}
		public void setOriginCountry(Object originCountry) {
			this.originCountry = originCountry;
		}
		public Object getDestinationCountry() {
			return destinationCountry;
		}
		public void setDestinationCountry(Object destinationCountry) {
			this.destinationCountry = destinationCountry;
		}
		public Object getEmail() {
			return email;
		}
		public void setEmail(Object email) {
			this.email = email;
		}
		public Object getLocalHrEmail() {
			return localHrEmail;
		}
		public void setLocalHrEmail(Object localHrEmail) {
			this.localHrEmail = localHrEmail;
		}
		public Object getMoveManager() {
			return moveManager;
		}
		public void setMoveManager(Object moveManager) {
			this.moveManager = moveManager;
		}
		public Object getTitle() {
			return title;
		}
		public void setTitle(Object title) {
			this.title = title;
		}
		public Object getActualDelivery() {
			return actualDelivery;
		}
		public void setActualDelivery(Object actualDelivery) {
			this.actualDelivery = actualDelivery;
		}
		public Object getAccountManager() {
			return accountManager;
		}
		public void setAccountManager(Object accountManager) {
			this.accountManager = accountManager;
		}
		public Object getAccountName() {
			return accountName;
		}
		public void setAccountName(Object accountName) {
			this.accountName = accountName;
		}
	}
}
