<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>


<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    

<script language="javascript" type="text/javascript">
function openOriginLocation(address1,address2,city,zip,state,country) {
 		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address1+','+address2+','+city+','+zip+','+state+','+country);
	}
	
</script>
<script language="javascript" type="text/javascript">
function findUserPermission(name,position) { 
  var url="findAcctRefNumList.html?ajax=1&decorator=simple&popup=true&code=" + encodeURI(name);
  ajax_showTooltip(url,position);	
  }


function clear_fields(){
	document.forms['partnerListForm'].elements['partnerPublic.partnerCode'].value = "";
	document.forms['partnerListForm'].elements['partnerPublic.lastName'].value = "";
	document.forms['partnerListForm'].elements['partnerPublic.terminalCountryCode'].value = "";
	document.forms['partnerListForm'].elements['partnerPublic.terminalCountry'].value = "";
	document.forms['partnerListForm'].elements['partnerPublic.terminalState'].value = "";
	document.forms['partnerListForm'].elements['searchListOfVendorCode'].value = "";
}

</script>
<script type="text/javascript" src="scripts/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="scripts/ajax.js"></script>
	<script type="text/javascript" src="scripts/ajax-tooltip.js"></script>
	<link rel="stylesheet" href="styles/ajax-tooltip.css" media="screen" type="text/css">
	<link rel="stylesheet" href="styles/ajax-tooltip-demo.css" media="screen" type="text/css">
	
<script language="javascript" type="text/javascript">
</script>

<script>

</script>
<style>
span.pagelinks { display:block;font-size:0.85em;margin-bottom:22px;!margin-bottom:2px;margin-top:-38px;!margin-top:-17px;padding:2px 0px;
text-align:right;width:100%;
}
</style>
</head>

  
<c:set var="buttons">   
	<c:if test="${not empty param.popup && partnerType == 'PP'}">  
		<input type="button" class="cssbutton" style="width:55px; height:25px" 
        onclick="location.href='<c:url value="/editPartnerAddFormPopup.html?partnerType=${partnerType}&decorator=popup&popup=true"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	</c:if>
	<c:if test="${empty param.popup}">  
    <input type="button" class="cssbutton" style="width:55px; height:25px" 
        onclick="location.href='<c:url value="/editPartnerAddForm.html?partnerType=${partnerType}"/>'"  
        value="<fmt:message key="button.add"/>"/>
    </c:if>       
     <%--   
    <input type="button" onclick="location.href='<c:url value="/mainMenu.html"/>'"  
        value="<fmt:message key="button.done"/>"/> 
      --%>    
</c:set> 
 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>   

<s:form id="partnerListForm" action='${empty param.popup?"searchPartnerAdmin.html":"searchPartnerRelo.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="findFor" value="<%= request.getParameter("findFor")%>" />
<s:hidden name="popupFrom" value="<%= request.getParameter("popupFrom")%>" />
<s:hidden name="accountLine.id" value="%{accountLine.id}"/>   
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<c:set var="sid" value="<%= request.getParameter("sid")%>" />
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />

<s:hidden name="jobReloName"/>

<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber"/>
	<s:hidden name="serviceOrder.ship"/>
<!--<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<s:hidden  name="fld_seventhDescription" value="${param.fld_seventhDescription}" />
	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" /> 
    <c:set var="fld_seventhDescription" value="${param.fld_seventhDescription}" />	
</c:if>
--><c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>


<div id="layer1" style="width:100%;">
<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" border="0" style="width:100%;">
<thead>
<tr>
<th><fmt:message key="partner.partnerCode"/></th>
<th><fmt:message key="partner.name"/></th>
<th>Country Code</th>
<th>Country Name</th>
<th><fmt:message key="partner.billingState"/></th>
<th>Vendor Type</th>

<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="partnerPublic.partnerCode" size="12" cssClass="input-text"/>
			</td>
			<td>
			    <s:textfield name="partnerPublic.lastName" size="24" cssClass="input-text" />
			    <s:hidden  name="jobRelo" value="${jobRelo}"/>
			</td>
			<c:if test="${partnerType == 'AG' || partnerType == 'VN'  || partnerType == 'CR'|| partnerType == 'DF'}">  
			 			<td>
						    <s:textfield name="partnerPublic.terminalCountryCode" size="15" cssClass="input-text"/>
						</td>
						<td>
						    <s:textfield name="partnerPublic.terminalCountry" size="24" cssClass="input-text"/>
						</td>
						<td>
						    <s:textfield name="partnerPublic.terminalState" size="24" cssClass="input-text"/>
						</td>
			</c:if>
						<td>
						<s:select cssClass="list-menu" name="searchListOfVendorCode" list="%{listOfVendorCode}" cssStyle="width:110px" headerKey="" headerValue=""/>
						</td>
		</tr>
		<tr>
			<td colspan="4"></td>
			<td colspan="2" width="130px" style="border-left: hidden;text-align:right;">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	</div>
	</s:form> 
<div id="layer2" style="width:100%;">
<div id="newmnav">   

</div><div class="spn" style="width:100%">&nbsp;</div><br>

<s:set name="partnerReloList" value="partnerReloList" scope="request"/>
<div id="otabs">
				  <ul>
				    <li><a><span>Vendor List</span></a></li>
				  </ul>
				</div>
				
<display:table name="partnerReloList" class="table" requestURI="" id="partnerList" export="${empty param.popup}" defaultsort="2" pagesize="10" style="width:100%;margin-top:-10px;" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${empty param.popup}">  
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode" href="editPartnerAddForm.html?partnerType=${partnerType}" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>    
    <display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
    <c:if test="${partnerType == 'AG'}"> 
	    <display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${partnerList.terminalAddress1}','${partnerList.terminalAddress2}','${partnerList.terminalCity}','${partnerList.terminalZip}','${partnerList.terminalState}','${partnerList.terminalCountry}');"/></a></display:column>  
    	<display:column title="Acct Ref #" sortable="true" titleKey="partner.rank" style="width:35px">
    	<a><img align="middle" title="Acct Ref #" onclick="findUserPermission('${partnerList.partnerCode}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
    	</display:column>
	    <display:column property="terminalCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
        <display:column property="terminalState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
        <display:column property="terminalCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    </c:if> 
    <display:column property="NPSscore" sortable="true" title="NPS score" style="width:120px"/> 
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:120px"/> 
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/>   
  
    <display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table> 
 
</div>
<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if> 
<c:set var="isTrue" value="false" scope="session"/>



<script type="text/javascript">   
       
</script>