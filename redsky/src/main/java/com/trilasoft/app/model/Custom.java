package com.trilasoft.app.model;

import org.appfuse.model.BaseObject;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "custom")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class Custom extends BaseObject {

	private String corpID;
	private Long id;
	private String movement;
	private Date entryDate;
	private String documentType;
	private String documentRef;
	private Integer pieces;
	private BigDecimal weight;
	private BigDecimal volume;
	private String weightUnit;
	private String volumeUnit;
	private String goods;
	private String createdBy;
	private Date createdOn;
	private Date updatedOn;
	private String updatedBy;
	private Long serviceOrderId;
	private String ticket;
	private Long transactionId; 
	private String status;
	private Long networkId;
	
	private ServiceOrder serviceOrder;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id).append("movement", movement)
				.append("entryDate", entryDate)
				.append("documentType", documentType)
				.append("documentRef", documentRef).append("pieces", pieces)
				.append("weight", weight).append("volume", volume)
				.append("weightUnit", weightUnit)
				.append("volumeUnit", volumeUnit).append("goods", goods)
				.append("createdBy", createdBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("updatedBy", updatedBy)
				.append("serviceOrderId", serviceOrderId)
				.append("ticket", ticket)
				.append("transactionId", transactionId)
				.append("status", status).append("networkId", networkId).toString();
	}


	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Custom))
			return false;
		Custom castOther = (Custom) other;
		return new EqualsBuilder().append(corpID, castOther.corpID)
				.append(id, castOther.id).append(movement, castOther.movement)
				.append(entryDate, castOther.entryDate)
				.append(documentType, castOther.documentType)
				.append(documentRef, castOther.documentRef)
				.append(pieces, castOther.pieces)
				.append(weight, castOther.weight)
				.append(volume, castOther.volume)
				.append(weightUnit, castOther.weightUnit)
				.append(volumeUnit, castOther.volumeUnit)
				.append(goods, castOther.goods)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(ticket, castOther.ticket)
				.append(transactionId, castOther.transactionId)
				.append(status, castOther.status)
				.append(networkId, castOther.networkId)
				.isEquals();
	}


	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(movement)
				.append(entryDate).append(documentType).append(documentRef)
				.append(pieces).append(weight).append(volume)
				.append(weightUnit).append(volumeUnit).append(goods)
				.append(createdBy).append(createdOn).append(updatedOn)
				.append(updatedBy).append(serviceOrderId).append(ticket)
				.append(transactionId).append(status).append(networkId).toHashCode();
	}


	@ManyToOne
	@JoinColumn(name="serviceOrderId", nullable=false, updatable=false,referencedColumnName="id")
			public ServiceOrder getServiceOrder() {
			        return serviceOrder;
			    }
			    
				 public void setServiceOrder(ServiceOrder serviceOrder) {
						this.serviceOrder = serviceOrder;
				}  
    

	@Column(length=20, insertable=false, updatable=false)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=50)
	public String getDocumentRef() {
		return documentRef;
	}
	public void setDocumentRef(String documentRef) {
		this.documentRef = documentRef;
	}
	@Column(length=10)
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	@Column
	public Date getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}
	@Column(length=150)
	public String getGoods() {
		return goods;
	}
	public void setGoods(String goods) {
		this.goods = goods;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=1)
	public String getMovement() {
		return movement;
	}
	public void setMovement(String movement) {
		this.movement = movement;
	}
	@Column(length=5)
	public Integer getPieces() {
		return pieces;
	}
	public void setPieces(Integer pieces) {
		this.pieces = pieces;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=20)
	public BigDecimal getVolume() {
		return volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	@Column(length=10)
	public String getVolumeUnit() {
		return volumeUnit;
	}
	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}
	@Column(length=20)
	public BigDecimal getWeight() {
		return weight;
	}
	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}
	@Column(length=10)
	public String getWeightUnit() {
		return weightUnit;
	}
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}

	@Column(length=15)
	public String getTicket() {
		return ticket;
	}


	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	@Column(length=20)
	public Long getTransactionId() {
		return transactionId;
	}


	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	@Column(length=10)
	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	@Column
	public Long getNetworkId() {
		return networkId;
	}


	public void setNetworkId(Long networkId) {
		this.networkId = networkId;
	}
	
}