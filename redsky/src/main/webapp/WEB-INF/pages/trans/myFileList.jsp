<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
<title><fmt:message key="myFileList.title"/></title>   
<meta name="heading" content="<fmt:message key='myFileList.heading'/>"/>   
<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
</c:if>

<script>

function performAction(targetElement,myFileFor,myFileListId,fileId, fileNameFor,secure,forQuotation,ppType,fileContentType, from){
	var action = targetElement.value;
	if(action=='Edit'){
	
			if(myFileFor!='PO'){
				location.href="editFileUpload.html?fid="+myFileListId+"&id="+fileId+"&myFileFor="+fileNameFor+"&secure="+secure+"&noteFor=${noteFor}&PPID=${PPID}&forQuotation="+forQuotation;
			}
			if(myFileFor=='PO'){
				location.href="editFileUpload.html?fid="+myFileListId+"&id="+fileId+"&myFileFor="+fileNameFor+"&secure="+secure+"&noteFor=${noteFor}&PPID=${PPID}&ppType="+ppType;
			}
		
			return true;
		
	}else if(action=='Split'){
		var agree=confirm("Are you sure you wish to split this file?");
		if (agree){
			if(fileContentType == 'application/pdf' && myFileFor!='PO' && myFileFor!='Truck'){
	           location.href="docSplit.html?fileId="+myFileListId+"&id="+fileId+"&myFileFor="+fileNameFor+"&from=main&relatedDocs=No&secure="+secure+"&noteFor=${noteFor}&PPID=${PPID}&forQuotation="+forQuotation;
			}else{
				alert('Only pdf documents can be split to TransDoc.');
			}
		}else{
			return false;
		}
	}else if(action=='Remove'){
		if(from != 'View'){
			var agree=confirm("Are you sure you wish to remove this file?");
			if (agree){
				confirmSubmit(myFileListId);
			}else{
				return false;
			}
		}
	}else{
		return false;
	}
	
}

function confirmSubmit(targetElement){
	var cid = document.forms['myFileForm'].elements['customerFile.id'].value;
	var sid = document.forms['myFileForm'].elements['serviceOrder.id'].value;
	var pid = document.forms['myFileForm'].elements['partner.id'].value;
	var ppType = document.forms['myFileForm'].elements['ppType'].value;
	var tid = document.forms['myFileForm'].elements['truck.id'].value;
	var did = targetElement;
	if(tid !=''){
    	location.href="deleteDoc.html?id="+tid+"&did="+did+"&myFileFor=Truck&myFileFrom=Truck&relatedDocs=No&active=true&secure=false&ppType="+ppType+"&noteFor=${noteFor}&forQuotation=${forQuotation}&PPID=${PPID}";
    }
       else{
           if(pid !=''){
	    	location.href="deleteDoc.html?id="+pid+"&did="+did+"&myFileFor=PO&myFileFrom=PO&relatedDocs=No&active=true&secure=false&ppType="+ppType+"&noteFor=${noteFor}&forQuotation=${forQuotation}&PPID=${PPID}";
	    }
	    else{
			if(sid == ''){
				location.href="deleteDoc.html?id="+cid+"&did="+did+"&myFileFor=CF&myFileFrom=CF&relatedDocs=No&active=true&secure=false&noteFor=${noteFor}&forQuotation=${forQuotation}&PPID=${PPID}";
			}else{
				networkUnLinkedAgentSO(did, sid);
				location.href="deleteDoc.html?id="+sid+"&did="+did+"&myFileFor=SO&myFileFrom=SO&relatedDocs=No&active=true&secure=false&noteFor=${noteFor}&forQuotation=${forQuotation}&PPID=${PPID}";
			}
		}
       }
}

function generatePortalId() {
		var daReferrer = document.referrer; 
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		
		if(checkBoxId ==''){
			alert('Please select the one or more document to e-mail.');
		}else{
			window.open('attachMail.html?checkBoxId='+checkBoxId+'&decorator=popup&popup=true&from=file','','width=650,height=170');
		}
} 

function checkStatusId(rowId, targetElement) {
		var Status = targetElement.checked;
		var url="updateMyfileStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse1;
        http22.send(null);		
} 

function handleHttpResponse1(){
      if (http22.readyState == 4){
           var result= http22.responseText         
      }
}	

function checkStatusAccId(rowId, targetElement) {
		var Status = targetElement.checked;
		var url="updateMyfileAccStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse2;
        http22.send(null);
} 
function handleHttpResponse2(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}

function checkStatusPartnerId(rowId, targetElement){
		var Status = targetElement.checked;
		var url="updateMyfilePartnerStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse3;
        http22.send(null);       
}
function handleHttpResponse3(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}	
function checkStatusServiceProviderId(rowId, targetElement){
	var Status = targetElement.checked;
	var url="updateMyfileServiceProviderStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
	http22.open("GET", url, true);
    http22.onreadystatechange = handleHttpResponse4;
    http22.send(null);       
}
function handleHttpResponse4(){
 if (http22.readyState == 4){
       var result= http22.responseText         
 }
}
var http22 = getHTTPObject22();
function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}


function checkStatusDriverPortalId(rowId, targetElement)
{

	var Status = targetElement.checked;
	var url="updateMyfileDiverStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
	http22.open("GET", url, true);
    http22.onreadystatechange = handleHttpResponse12;
    http22.send(null);  
}

function handleHttpResponse12(){
    if (http22.readyState == 4){
          var result= http22.responseText         
    }
}	
function networkUnLinkedAgentSO(id, fid){
	var url="networkUnlinkedAgentSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(id)+"&fid=" + encodeURI(fid);
	http221.open("GET", url, true);
	http221.onreadystatechange = handleHttpResponse31;
	http221.send(null);			    	
}

function networkLinkedAgentSO(fid,targetElement){
	var status = targetElement.checked;
	var id = document.forms['myFileForm'].elements['serviceOrder.id'].value;
	var url="";
	if( status == true){
		var agree=confirm("Are you sure want to link this file to network partners?");
		if (agree){
	    	url="networkLinkedAgentSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(id)+"&fid=" + encodeURI(fid);
			http221.open("GET", url, true);
			http221.onreadystatechange = handleHttpResponse31;
			http221.send(null);	
		}else{ 
			targetElement.checked=false;
			return false;}
	   }else{
		   var agree=confirm("Are you sure want to Unlink this file to network partners?");
			if (agree){
		    	url="networkUnlinkedAgentSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(id)+"&fid=" + encodeURI(fid);
				http221.open("GET", url, true);
				http221.onreadystatechange = handleHttpResponse31;
				http221.send(null);	
			}else{ 
				targetElement.checked=true;
				return false;}
      }
}

function handleHttpResponse31(){
    if (http221.readyState == 4){
          var result= http221.responseText ;        
    }
}
function handleHttpResponse32(){
    if (http221.readyState == 4){
          var result= http222.responseText ;        
    }
}
var http221 = getHTTPObject221();
var http222 = getHTTPObject222();
var httpupdateInvoice = getHTTPObject22();
function getHTTPObject221(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function getHTTPObject222(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
// 1 visible, 0 hidden   
function userStatusCheck(target){
		var targetElement = target;
		var tempValue=targetElement.value.split("`");
		var ids=tempValue[0];		
		var fileName=tempValue[1];
		var transDoc = tempValue[2];
		
		if(targetElement.checked){
      		var userCheckStatus = document.forms['myFileForm'].elements['userCheck'].value;
      		if(userCheckStatus == ''){
	  			document.forms['myFileForm'].elements['userCheck'].value = ids;
      		}else{
      			var userCheckStatus=	document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus + ',' + ids;
      			document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      		}
      		
      		var userCheckFileName = document.forms['myFileForm'].elements['userCheckFile'].value;
      		if(userCheckFileName == ''){
	  			document.forms['myFileForm'].elements['userCheckFile'].value = fileName;
      		}else{
      			var userCheckFileName =	document.forms['myFileForm'].elements['userCheckFile'].value = userCheckFileName + ',' + fileName;
      			document.forms['myFileForm'].elements['userCheckFile'].value = userCheckFileName.replace( ',,' , ',' );
      		}
      		var userCheckTransDoc = document.forms['myFileForm'].elements['userTransDocStatus'].value;
      		if(userCheckTransDoc == ''){
	  			document.forms['myFileForm'].elements['userTransDocStatus'].value = transDoc;
      		}else{
      			var userCheckTransDoc =	document.forms['myFileForm'].elements['userTransDocStatus'].value = userCheckTransDoc + ',' + transDoc;
      			document.forms['myFileForm'].elements['userTransDocStatus'].value = userCheckTransDoc.replace( ',,' , ',' );
      		}
      		
    	}
  	 	if(targetElement.checked==false){
     		/* var userCheckStatus = document.forms['myFileForm'].elements['userCheck'].value;
     		var userCheckStatus=document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( ids , '' );
     		document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' ); */
  	 		var userCheckStatus = document.forms['myFileForm'].elements['userCheck'].value;
     		userCheckStatus = userCheckStatus.replace( ids , '' );
     		userCheckStatus = userCheckStatus.replace( ',,' , ',' );
     		var len = userCheckStatus.length-1;
     	    if(len==userCheckStatus.lastIndexOf(",")){
     	    	userCheckStatus=userCheckStatus.substring(0,len);
     	      }
     	      if(userCheckStatus.indexOf(",")==0){
     	    	 userCheckStatus=userCheckStatus.substring(1,userCheckStatus.length);
     	      }
     		document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus;
     		
     		/* var userCheckFileName = document.forms['myFileForm'].elements['userCheckFile'].value;
     		var userCheckFileName = document.forms['myFileForm'].elements['userCheckFile'].value = userCheckFileName.replace( fileName , '' );
     		document.forms['myFileForm'].elements['userCheckFile'].value = userCheckFileName.replace( ',,' , ',' ); */
     		var userCheckFileName = document.forms['myFileForm'].elements['userCheckFile'].value;
     		userCheckFileName = userCheckFileName.replace( fileName , '' );
     		userCheckFileName = userCheckFileName.replace( ',,' , ',' );
     		len = userCheckFileName.length-1;
     	    if(len==userCheckFileName.lastIndexOf(",")){
     	    	userCheckFileName=userCheckFileName.substring(0,len);
     	      }
     	      if(userCheckFileName.indexOf(",")==0){
     	    	 userCheckFileName=userCheckFileName.substring(1,userCheckFileName.length);
     	      }
     		document.forms['myFileForm'].elements['userCheckFile'].value = userCheckFileName;

     		/* var userCheckTransDoc = document.forms['myFileForm'].elements['userTransDocStatus'].value;
     		var userCheckTransDoc = document.forms['myFileForm'].elements['userTransDocStatus'].value = userCheckTransDoc.replace( transDoc , '' );
     		document.forms['myFileForm'].elements['userTransDocStatus'].value = userCheckTransDoc.replace( ',,' , ',' ); */
     		var userCheckTransDoc = document.forms['myFileForm'].elements['userTransDocStatus'].value;
     		userCheckTransDoc = userCheckTransDoc.replace( transDoc , '' );
     		userCheckTransDoc = userCheckTransDoc.replace( ',,' , ',' );
     		var len = userCheckTransDoc.length-1;
     	    if(len==userCheckTransDoc.lastIndexOf(",")){
     	    	userCheckTransDoc=userCheckTransDoc.substring(0,len);
     	      }
     	      if(userCheckTransDoc.indexOf(",")==0){
     	    	 userCheckTransDoc=userCheckTransDoc.substring(1,userCheckTransDoc.length);
     	      }
     		document.forms['myFileForm'].elements['userTransDocStatus'].value = userCheckTransDoc;
     		
   		}
  	 	collectFileId();
   	}

function downloadDoc(){
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		var partnerCode = document.forms['myFileForm'].elements['fileNameFor'].value;
		if(partnerCode=='Truck'){
			var seqNo = document.forms['myFileForm'].elements['truck.localNumber'].value;
			}else{
				if(partnerCode=='PO'){
		var seqNo = document.forms['myFileForm'].elements['partner.partnerCode'].value;
		}else{
		var seqNo = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
		if(seqNo == '' ){
			seqNo = document.forms['myFileForm'].elements['serviceOrder.sequenceNumber'].value;
		}
		}}
		if(checkBoxId =='' || checkBoxId ==','){
			alert('Please select the one or more document to download.');
		}else{
			var url = 'ImageServletAction.html?id='+checkBoxId+'&param=DWNLD&seqNo='+seqNo;
			location.href=url;
		}
		
} 
function emailDoc() {
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		
		if(checkBoxId ==''){
			alert('Please select the one or more document to download.');
		}else{
			var url = 'ImageServletAction.html?id='+checkBoxId+'&param=EMAIL';
			location.href=url;
		}
}

function checkAll(){
	document.forms['myFileForm'].elements['userCheck'].value = "";
	document.forms['myFileForm'].elements['userCheckFile'].value = "";
	document.forms['myFileForm'].elements['userTransDocStatus'].value = "";
	var len = document.forms['myFileForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['myFileForm'].elements['DD'][i].checked = true ;
		userStatusCheck(document.forms['myFileForm'].elements['DD'][i]);
	}
	collectFileId();
}

function uncheckAll(){
	var len = document.forms['myFileForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['myFileForm'].elements['DD'][i].checked = false ;
		userStatusCheck(document.forms['myFileForm'].elements['DD'][i]);
	}
	document.forms['myFileForm'].elements['userCheck'].value="";
	document.forms['myFileForm'].elements['userCheckFile'].value="";
	document.forms['myFileForm'].elements['userTransDocStatus'].value = "";
	collectFileId();
}
function show(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'block';
     }
}
function hide(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'none';
     }else{
          document.getElementById(theTable).style.display = 'none';
     }
}


function conbinedDoc(){
	var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
	var checkBoxFile = document.forms['myFileForm'].elements['userCheckFile'].value;
	
	var seqNo = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
	if(seqNo == '' ){
		seqNo = document.forms['myFileForm'].elements['serviceOrder.sequenceNumber'].value;
	}
	
	var name = document.forms['myFileForm'].elements['customerFile.firstName'].value;
	name +=	' '+document.forms['myFileForm'].elements['customerFile.lastName'].value;
	
	if(checkBoxFile != ''){
		checkBoxFile = checkBoxFile.trim();
		if (checkBoxFile.indexOf(",") == 0) {
			checkBoxFile = checkBoxFile.substring(1);
		}
		if (checkBoxFile.lastIndexOf(",") == checkBoxFile.length - 1) {
			checkBoxFile = checkBoxFile.substring(0, checkBoxFile.length - 1);
		}
		
		var arrayid = checkBoxFile.split(",");
		var arrayLength = arrayid.length;
		
		for (var i = 0; i < arrayLength; i++) {
			var fName = arrayid[i];
			if(fName.substring(fName.lastIndexOf(".")+1, fName.length) != 'pdf'  && fName.substring(fName.lastIndexOf(".")+1, fName.length) != 'PDF'){
				alert('Please select only PDF Files.');
				return false;
			}
		}
	}
	
	if(checkBoxId =='' || checkBoxId ==','){
		alert('Please select one or more document to merge.');
	}else{
		var url = 'conbinedDoc.html?rId='+checkBoxId+'&seqNum='+seqNo+'&name='+name;
		location.href=url;
	}
}

function conbinedDocInPdf(){
    var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
       var checkBoxFile = document.forms['myFileForm'].elements['userCheckFile'].value;
       var seqNo = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
       if(seqNo == '' ){
           seqNo = document.forms['myFileForm'].elements['serviceOrder.sequenceNumber'].value;
       }
       
       var name = document.forms['myFileForm'].elements['customerFile.firstName'].value;
       name += ' '+document.forms['myFileForm'].elements['customerFile.lastName'].value;
       
       if(checkBoxFile != ''){
           checkBoxFile = checkBoxFile.trim();
           if (checkBoxFile.indexOf(",") == 0) {
               checkBoxFile = checkBoxFile.substring(1);
           }
           if (checkBoxFile.lastIndexOf(",") == checkBoxFile.length - 1) {
               checkBoxFile = checkBoxFile.substring(0, checkBoxFile.length - 1);
           }
           
           var arrayid = checkBoxFile.split(",");
           var arrayLength = arrayid.length;
       }
       
       if(checkBoxId =='' || checkBoxId ==','){
           alert('Please select one or more document to convert in PDF file.');
       }else{
           var url = 'conbinedDocInPdf.html?rId='+checkBoxId+'&seqNum='+seqNo+'&name='+name;
           location.href=url;
       }
   
}


function transDoc(){
	var id = document.forms['myFileForm'].elements['fileId'].value;
	var fileNameFor = document.forms['myFileForm'].elements['fileNameFor'].value;
	var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
	var checkBoxFile = document.forms['myFileForm'].elements['userCheckFile'].value;
	var transDocStatus = document.forms['myFileForm'].elements['userTransDocStatus'].value;
	var transDocArray = transDocStatus.split(',');
	var count=0;
	for(var i=0;i<transDocArray.length ; i++)
	{
		if(transDocArray[i]=='UPLOADED'){
			count++;
		}
	}
	if(count>0){
		var agree = confirm("You have chosen to upload files that were previously sent. Do you wish to upload them again?");
		if(agree){
			}else{return false;}
	}
	var seqNo = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
	if(seqNo == '' ){
		seqNo = document.forms['myFileForm'].elements['serviceOrder.sequenceNumber'].value;
	}
	
	var name = document.forms['myFileForm'].elements['customerFile.firstName'].value;
	name +=	' '+document.forms['myFileForm'].elements['customerFile.lastName'].value;
	name=name.trim();
	name = name.replace("%","&#37;");
	if(checkBoxFile != ''){
		checkBoxFile = checkBoxFile.trim();
		if (checkBoxFile.indexOf(",") == 0) {
			checkBoxFile = checkBoxFile.substring(1);
		}
		if (checkBoxFile.lastIndexOf(",") == checkBoxFile.length - 1) {
			checkBoxFile = checkBoxFile.substring(0, checkBoxFile.length - 1);
		}
		
		var arrayid = checkBoxFile.split(",");
		var arrayLength = arrayid.length;
		for (var i = 0; i < arrayLength; i++) {
			var fName = arrayid[i];
			if(fName.substring(fName.lastIndexOf(".")+1, fName.length) != 'pdf' && fName.substring(fName.lastIndexOf(".")+1, fName.length) != 'PDF'){
				alert('Only pdf documents can be transferred to TransDoc.');
				return false;
			}
		}
	}
	
	if(checkBoxId =='' || checkBoxId ==','){
		alert('Please select document to transfer.');
	}else{
		var url = 'docTransfer.html?id='+id+'&myFileFor='+fileNameFor+'&rId='+checkBoxId+'&seqNum='+seqNo+'&name='+name;
		location.href=url;
	}
}
function disabledAll(){
	var elementsLen=document.forms['myFileForm'].elements.length;
	for(i=0;i<=elementsLen-1;i++){
		if(document.forms['myFileForm'].elements[i].type=='text'){
			document.forms['myFileForm'].elements[i].readOnly =true;
			document.forms['myFileForm'].elements[i].className = 'input-textUpper';
			document.forms['myFileForm'].elements[i].onkeydown ="";
		}else if(document.forms['myFileForm'].elements[i].type=='textarea'){
			document.forms['myFileForm'].elements[i].readOnly =true;
			document.forms['myFileForm'].elements[i].className = 'textareaUpper';
		}else{
			document.forms['myFileForm'].elements[i].disabled=true;
		} 
	}
}
function getfilterbycategory(target)
{
	var id = document.forms['myFileForm'].elements['fileId'].value;
	var categoryDetails="categoryResult.html?id="+id+"&myFileFor=${myFileFor}&noteFor=${noteFor}&categorySearchValue="+target+"&active=true&secure=false";
	document.forms['myFileForm'].setAttribute("action",categoryDetails);
	document.forms['myFileForm'].submit();
}
</script>

   
<style>  
.table-fc td, .table th, .tableHeaderTable td {padding:0.3em;font-size:11px;}
#mainPopup {padding-left:10px;padding-right:10px;}
span.pagelinks {display:block;font-size:0.95em;margin-bottom:5px;margin-top:-18px;padding:2px 0;text-align:right;width:100%; }
</style>

</head>
<s:form id="myFileForm" action="searchMyFiles" method="post" >  
<c:set var="fileForId" value="<%=request.getParameter("fileForId") %>"/>
<s:hidden name="fileForId" value="<%=request.getParameter("fileForId") %>"/>
<s:hidden name="fileId" value="<%=request.getParameter("id") %>" />
<c:set var="fileId" value="<%=request.getParameter("id") %>"/>

<s:hidden name="fileNameFor"  value="<%=request.getParameter("myFileFor") %>" />
<c:set var="fileNameFor" value="<%=request.getParameter("myFileFor") %>" />
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("ppType")%>" />
<c:set var="ppType" value="<%= request.getParameter("ppType")%>"/>
<s:hidden name="noteForTemp" value="<%=request.getParameter("noteFor") %>" />
<c:set var="noteForTemp" value="<%=request.getParameter("noteFor") %>"/>
<s:hidden name="PPID" id="PPID" value="<%=request.getParameter("PPID") %>" />
<c:set var="PPID" value="<%=request.getParameter("PPID") %>"/>
<c:set var="idOfTasks" value="" scope="session"/>
<c:set var="tableName" value="" scope="session"/>
<s:hidden name="userCheck"/> 
<s:hidden name="userCheckFile"/>
<s:hidden name="myFileIdCheck" value=""/>
<c:set var="myFileStatusCheck" value="" />  
<s:hidden name="customerFile.id" />
<s:hidden name="workTicket.id" />
<s:hidden name="workTicket.ticket" />
<s:hidden name="partner.id" />
<s:hidden name="truck.id" />
<s:hidden name="truck.localNumber" />
<s:hidden name="partner.partnerCode" />
<s:hidden name="serviceOrder.id" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden id="forQuotation" name="forQuotation" />
<s:hidden name="userTransDocStatus" />
<s:hidden name="myFileForVal" value="<%=request.getParameter("myFileFor")%>" />
<s:hidden name="noteForVal" value="<%=request.getParameter("noteFor")%>" />
<s:hidden name="activeVal" value="<%=request.getParameter("active")%>" />
<s:hidden name="secureVal" value="<%=request.getParameter("secure")%>" />
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="salesPortalAccess" value="false" />
<sec-auth:authComponent componentId="module.script.form.corpSalesScript">
<c:set var="salesPortalAccess" value="true" />
</sec-auth:authComponent>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<c:if test="${myFileFor =='CF'}">
	 <c:set var="idOfWhom" value="" scope="session"/>
	<c:set var="noteID" value="" scope="session"/>
	<c:set var="custID" value="" scope="session"/>
	<c:set var="noteFor" value="" scope="session"/> 
	<c:if test="${empty customerFile.id}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
	<c:if test="${not empty customerFile.id}">
		<c:set var="isTrue" value="true" scope="request"/>
	</c:if>
</c:if>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="buttons"> 
<c:if test="${ppType ==''}">  
	<input type="button" class="cssbutton" style="margin-right: 5px;width:65px;" onclick="location.href='<c:url value="/uploadMyFile!start.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=${secure}&forQuotation=${forQuotation}"/>'" value="<fmt:message key="button.upload"/>"/>
</c:if>
	<c:if test="${ppType !=''}">	
	 <input type="button" class="cssbutton" style="margin-right: 5px;width:65px;" onclick="location.href='<c:url value="/uploadMyFile!start.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=${secure}&ppType=${ppType}&forQuotation=${forQuotation}&PPID=${PPID}"/>'" value="<fmt:message key="button.upload"/>"/>
	</c:if>
</c:set> 
<c:if test="${myFileFor=='PO'}"> 
<c:set var="isTrue" value="true" scope="request"/>
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<s:hidden name="from"  id="from" value="<%=request.getParameter("from") %>" />
</c:if>
<c:if test="${myFileFor!='CF' && myFileFor!='PO'}"> 
<s:hidden name="customerFile.firstName" />
<s:hidden name="customerFile.lastName" />
<s:hidden name="customerFile.sequenceNumber" />
<c:set var="idOfWhom" value="" scope="session"/>
<c:set var="noteID" value="" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${noteFor=='WorkTicket' }">
<c:set var="noteFor" value="" /> 
</c:if>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<c:if test="${myFileFor!='PO' && myFileFor!='Truck'}">
<div id="layer6" style="width:100%; ">

<div id="newmnav" style="float:left; ">
    <c:choose>
	<c:when test="${forQuotation!='QC'}">
            <ul>
				<s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
				<s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
				<c:set var="relocationServicesKey" value="" />
				<c:set var="relocationServicesValue" value="" /> 
			    <c:forEach var="entry" items="${relocationServices}">
					<c:if test="${relocationServicesKey==''}">
					<c:if test="${entry.key==serviceOrder.serviceType}">
					<c:set var="relocationServicesKey" value="${entry.key}" />
					<c:set var="relocationServicesValue" value="${entry.value}" /> 
					</c:if>
					</c:if> 
               </c:forEach>
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
	            	<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	            	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			    </c:if>
			    <c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
	            	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>Quotes</span></a></li>
			    </c:if>
			    </sec-auth:authComponent>
	            
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.billingTab">
		             <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >	
		             	<li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
		             </sec-auth:authComponent>
	            </sec-auth:authComponent>
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">
		             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		             <c:choose>
					    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
					      	<li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
					    </c:when> --%>
					    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
							 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
						</c:when>
					    <c:otherwise> 
					    	<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				        </c:otherwise>
				     </c:choose>
				     </c:if> 
			     </sec-auth:authComponent>
			     <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		             <c:choose> 
					    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
							 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
						</c:when>
					    <c:otherwise> 
					    	<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				        </c:otherwise>
				     </c:choose>
				     </c:if>
			     </sec-auth:authComponent>
			     <sec-auth:authComponent componentId="module.tab.serviceorder.accountingPortalTab">	
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	              <li><a href="accountLineSalesPortalList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	              </c:if>
	              </sec-auth:authComponent>	
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
    	         <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	         <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	          </c:if>
			     <sec-auth:authComponent componentId="module.tab.trackingStatus.forwardingTab">
			   
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			     	<c:if test="${serviceOrder.job !='RLO'}"> 
			     	<c:if test="${forwardingTabVal!='Y'}"> 
			     	
 			     	<c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}"> 
	   					<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	   					</c:if>
	  				</c:if>
	  				<c:if test="${forwardingTabVal=='Y'}">
	  				
 	  				<c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}"> 
	  					<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  					</c:if>
	  				</c:if>
	               </c:if>
	               </c:if>
	             </sec-auth:authComponent>
	             
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.domesticTab">
		             <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
		             <c:if test="${serviceOrder.job !='RLO'}"> 
		             	<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
		             </c:if>
		             </c:if>
	             </sec-auth:authComponent>
	             <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
                 <c:if test="${serviceOrder.job =='INT'}">
                   <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
                 </c:if>
                 </sec-auth:authComponent>
	              <c:if test="${userType!='DRIVER'}">
	              <c:if test="${serviceOrder.job =='RLO'}"> 
                  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
                 </c:if>
                 <c:if test="${serviceOrder.job !='RLO'}"> 
	             <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
	             </c:if>
	            </c:if>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.ticketTab">
	             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	             <c:if test="${serviceOrder.job !='RLO'}"> 
	             	<li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
	             </c:if>
	             </c:if>
	             </sec-auth:authComponent>
	              <configByCorp:fieldVisibility componentId="component.standard.claimTab">
	             <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
	             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	             	<c:if test="${serviceOrder.job !='RLO'}"> 
	             	<li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
	             	</c:if>
	             	</c:if>
	             </sec-auth:authComponent>
	             </configByCorp:fieldVisibility>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.customerfileTab">
	             	<li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
	             </sec-auth:authComponent>
	             
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
	           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
	           	 </sec-auth:authComponent>
	           	 <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		    	<c:if test="${voxmeIntergartionFlag=='true'}">
		    		<li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    	</c:if>
		    	<c:if test="${not empty customerFile.id && mmValidation =='Yes'}"> 
		    		<li><a href="inventoryDetailsForm.html?cid=${customerFile.id}"><span>Inventory Details</span></a></li>
		    	</c:if>
		    </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.customerFile.serviceOrderTab">
		    <c:if test="${ usertype=='AGENT' && surveyTab}">
		  		 <li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    </c:if>
		     </sec-auth:authComponent>
	       </ul>
	       </c:when>
	      <c:otherwise>
		   <ul>
		    <li ><a href="QuotationFileForm.html?id=${serviceOrder.customerFileId}&forQuotation=QC" ><span>Quotation File</span></a></li>
		    <li ><a href="quotationServiceOrders.html?id=${serviceOrder.customerFileId}&forQuotation=QC"><span>Quotes</span></a></li>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
    	         <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	         <li><a href="operationResource.html?id=${serviceOrder.id}&quoteFlag=y"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	          </c:if>
		     <c:if test="${voxmeIntergartionFlag=='true'}">             
		 <li><a href='surveyDetails.html?cid=${customerFile.id}&Quote=y'><span>Survey Details</span></a></li>              
             </c:if>
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li>  	
		   </ul>
		</c:otherwise></c:choose>
</div>
    <table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 4px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
<div class="spn">&nbsp;</div>
<div style="!margin-top:8px; ">
 <c:if test="${userType!='DRIVER'}">
<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
</c:if>
</div>
</div>
</c:if>
 </c:if>
 <c:if test="${noteForTemp!='' && noteForTemp!='WorkTicket' }">
    <c:set var="idOfWhom" value="<%=request.getParameter("id") %>" scope="session"/>
	<c:set var="noteID" value="${TempnotesId}" scope="session"/>
	<c:set var="noteFor" value="<%=request.getParameter("noteFor") %>" scope="session"/>
	<c:if test="${noteFor=='WorkTicket' }">
     <c:set var="noteFor" value="" /> 
    </c:if>
   </c:if>
 <c:if test="${userType!='DRIVER'}">
 <c:if test="${myFileFor=='CF'}"> 
 <div id="Layer5" style="width:95%">	
	<c:choose>
	<c:when test="${forQuotation!='QC'}">
	<div id="newmnav">
		  <ul>
		    <c:if test="${customerFile.controlFlag=='A'}">
		    <li><a href="editOrderManagement.html?id=${customerFile.id}" ><span>Order Detail</span></a></li>
		    </c:if> 
		    <c:if test="${customerFile.controlFlag!='A'}">
		    <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
		    <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		    <li><a href="customerServiceOrders.html?id=${customerFile.id}" ><span>Service Orders</span></a></li>
		    </c:if>
		    <c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
		    <li><a href="customerServiceOrders.html?id=${customerFile.id}" ><span>Quotes</span></a></li>
		    </c:if> 
		    <c:if test="${salesPortalAccess=='false'}">
		    <li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
		    <!-- <li><a href="surveysList.html?id=${customerFile.id} "><span>Surveys</span></a></li> -->
		    <li><a href="showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}" ><span>Account Policy</span></a></li> 
		  	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>  
		    </c:if>
		    </c:if> 
		  	 <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		    	<c:if test="${voxmeIntergartionFlag=='true'}">
		    		<li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    	</c:if>
		    	<c:if test="${not empty customerFile.id && mmValidation =='Yes'}"> 
		    		<li><a href="inventoryDetailsForm.html?cid=${customerFile.id}"><span>Inventory Details</span></a></li>
		    	</c:if>
		    </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.customerFile.serviceOrderTab">
		    <c:if test="${ usertype=='AGENT' && surveyTab}">
		  		 <li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    </c:if>
		     </sec-auth:authComponent>
		  </ul>
		</div>
	 </c:when>
	 <c:otherwise>
		<div id="newmnav">
		  <ul>
		    <li ><a href="QuotationFileForm.html?id=${fileId}&forQuotation=QC" ><span>Quotation File</span></a></li>
		    <li ><a href="quotationServiceOrders.html?id=${fileId}&forQuotation=QC"><span>Quotes</span></a></li>
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li> 
		  	 <c:if test="${voxmeIntergartionFlag=='true'}">             
		  <li><a href='surveyDetails.html?cid=${customerFile.id}&Quote=y'><span>Survey Details</span></a></li>              
             </c:if>
		  </ul>
		</div>
		</c:otherwise></c:choose><div class="spn">&nbsp;</div>
		 <div style="padding-bottom:0px;"></div>
 <div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="!margin-top:3px;"><span></span></div>
   <div class="center-content">
<table class=""  cellspacing="1" cellpadding="0"	border="0" style="width:90%">
	<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
					<tbody>
					<tr>
						<td align="right" class="listwhitebox">Cust#</td>
						<td><s:textfield name="customerFile.sequenceNumber" size="21" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Shipper</td>
						<td><s:textfield name="customerFile.firstName" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.lastName" required="true" size="18" readonly="true" cssClass="input-textUpper"/></td>
						<td align="right" class="listwhitebox">Origin</td>
						<td><s:textfield name="customerFile.originCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td ><s:textfield name="customerFile.originCountryCode" required="true" size="13" readonly="true" cssClass="input-textUpper"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitebox">Type</td>
						<td><s:textfield name="customerFile.job" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Destination</td>
						<td><s:textfield name="customerFile.destinationCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.destinationCountryCode" required="true" size="18" readonly="true" cssClass="input-textUpper" /></td>
						<td align="left" class="listwhitebox"><fmt:message key='customerFile.billToCode'/></td>
						<td colspan="2"><s:textfield name="customerFile.billToName" required="true" size="35" readonly="true" cssClass="input-textUpper" /></td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</c:if>  
</c:if>
<div id="Layer1" style="width:100%;">
		<div id="fc-newmnav">
			<ul>
			    <c:if test="${myFileFor!='PO' && myFileFor!='Truck'}">	
				<li id="fc-newmnav1" style="background:#FFF "><a class="current" href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Document List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				  <c:if test="${userType!='DRIVER'}">
				<li><a href="relatedFiles.html?id=${fileId}&myFileFrom=${fileNameFor}&noteFor=${noteFor}&active=true&secure=${secure}&forQuotation=${forQuotation}"><span>Related Docs</span></a></li>
				<li><a href="basketFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteFor}&relatedDocs=No&active=false&forQuotation=${forQuotation}"><span>Waste Basket</span></a></li>
				</c:if>
				<sec-auth:authComponent componentId="module.tab.myfile.securedocTab">
					<li><a href="secureFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteFor}&active=true&secure=true&forQuotation=${forQuotation}"><span>Secure List</span></a></li>
				</sec-auth:authComponent>
				<c:if test="${userType!='DRIVER'}">
				<li><a href="checkListFiles.html?id=${fileId}&myFileFrom=${fileNameFor}&myFileFor=${fileNameFor}&noteFor=${noteFor}&relatedDocs=No&active=false&forQuotation=${forQuotation}"><span>Check List</span></a></li>
				</c:if>
				<%-- <li><a  href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Document Centre<img src="images/navarrow.gif" align="absmiddle" /></span></a></li> --%>
				<div style="!position:absolute;line-height:28px;"><font style="padding-left:10px;font-family: Arial, Helvetica, sans-serif;font-size: 11px;font-weight: 600;color:#003366; ">Filter By Category:</font>
				<s:select cssClass="list-menu" id="entity" name="filterbycategory" headerKey="" headerValue="All" list="%{categoryList}"   onchange="getfilterbycategory(this.value)"/>
				</div>
				</c:if>
				<c:if test="${myFileFor=='Truck'}">	
					<li id="fc-newmnav1" style="background:#FFF "><a class="current" href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=${secure}&ppType=${ppType}"><span>Document List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					<li><a href="basketFiles.html?id=${fileId}&myFileFor=${fileNameFor}&relatedDocs=No&active=false&ppType=${ppType}"><span>Waste Basket</span></a></li>
					<li><a href="editTruck.html?id=${fileId}" ><span>Truck Details</span></a></li>
				</c:if>
				<c:if test="${myFileFor=='PO'}">	
					<li id="fc-newmnav1" style="background:#FFF "><a class="current" href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=${secure}&ppType=${ppType}"><span>Document List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					<c:if test="${from != 'View'}">
						<li><a href="basketFiles.html?id=${fileId}&myFileFor=${fileNameFor}&relatedDocs=No&active=false&ppType=${ppType}&PPID=${PPID}"><span>Waste Basket</span></a></li>
						<c:if test="${!param.popup && ppType=='AG'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Agent Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='AC'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Account Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='PP'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Private Party Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='CR'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Carrier Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='VN'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Vendor Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='OO'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Owner Ops</span></a></li>
						</c:if>
					</c:if>
					<c:if test="${from == 'View'}">
						<c:if test="${!param.popup && ppType=='AG'}"> 
							<li><a href="findPartnerProfileList.html?from=view&code=${ppCode}&partnerType=${ppType}&id=${fileId}"><span>Agent Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='AC'}"> 
							<li><a href="viewPartner.html?id=${fileId}&partnerType=${ppType}" ><span>Account Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='PP'}"> 
							<li><a href="viewPartner.html?id=${fileId}&partnerType=${ppType}" ><span>Private Party Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='CR'}"> 
							<li><a href="viewPartner.html?id=${fileId}&partnerType=${ppType}" ><span>Carrier Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='VN'}"> 
							<li><a href="viewPartner.html?id=${fileId}&partnerType=${ppType}" ><span>Vendor Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='OO'}"> 
							<li><a href="viewPartner.html?id=${fileId}&partnerType=${ppType}" ><span>Owner Ops</span></a></li>
						</c:if>
					</c:if>
				</c:if>
			</ul>
		</div>
		<sec-auth:authComponent componentId="module.table.partner.docList.Admin">
			<div id="chkAllButton"  class="listwhitetext" style="display:none;" >
				<input type="radio"  name="chk" onClick="checkAll()" /><strong>Check All</strong>
				<input type="radio"  name="chk" onClick="uncheckAll()"  /><strong>Uncheck All</strong>
			</div>
		</sec-auth:authComponent>
		<div class="spn">&nbsp;</div>
		<div style="!padding-top:3px;"></div>



<table class="" cellspacing="0" cellpadding="0" border="0" style="width:1050px;!padding-top:3px;margin-bottom:5px;">
	<tbody>
		<tr>
			<td>
				<s:set name="myFiles" value="myFiles" scope="request"/>  
				
					<display:table name="myFiles" class="table-fc" requestURI="" id="myFileList" export="true"  style="width:100%;" defaultsort="${fieldName}"  defaultorder="${sortOrder}">
					
					 <c:if test="${myFileStatusCheck == ''}"> 
			 	 <c:set var="myFileStatusCheck" value="${myFileList.id}^${myFileList.emailStatus}" /> 
			 	 </c:if> 
			 	 <c:if test="${myFileStatusCheck != ''}">  
				 <c:set var="myFileStatusCheck" value="${myFileStatusCheck}^${myFileList.id}:${myFileList.emailStatus}" />
			 	 </c:if>    
					   	<c:if test="${from != 'View'}">
					    	<display:column title=" " style="width:5px;"><input type="checkbox" id="checkboxId" name="DD" value="${myFileList.id}`${myFileList.fileFileName}`${myFileList.transDocStatus}" onclick="userStatusCheck(this)"/></display:column>
					    </c:if>
					    <display:column property="fileType" sortable="true" maxLength="20" title="Document&nbsp;Type" style="width:60px;"/>
					    <display:column property="documentCategory" sortable="true" maxLength="20" title="Document&nbsp;Category" style="width:60px;"/>
						<display:column  maxLength="40" title="Description" style="width:60px;"  sortable="true" sortProperty="description">
						<a onclick="downloadSelectedFile('${myFileList.id}');">
						<c:out value="${myFileList.description}" escapeXml="false"/></a>
						</display:column>					      
					    <display:column property="fileSize" sortable="true" title="Size" style="width:60px;"></display:column>
					      <c:if test="${from != 'View'}">
					      <c:if test="${fn1:indexOf(transDocSysDefault,transDocJobType)>=0}">
					    	<display:column title="TransDoc&nbsp;Status" sortable="true" sortProperty="transDocStatus" style="width:70px;">
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='DOWNLOADED'}">
					    	Received&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='READY_TO_UPLOAD'}">
					    	Ready&nbsp;to&nbsp;transfer&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />				    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='UPLOADED'}">
					    	Sent&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='UPLOAD_FAILED'}">
					    	Send&nbsp;Failed&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='DOWNLOAD_FAILED'}">
					    	Receive&nbsp;Failed&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	</display:column>
					        </c:if>					    
					        
					    <display:column property="createdOn" sortable="true" title="Uploaded&nbsp;On" format="{0,date,dd-MMM-yyyy}" style="width:80px;"/>
						<display:column property="createdBy" sortable="true" title="Uploaded&nbsp;By" style="width:70px;"/>
						<display:column title="Email&nbsp;Status" style="width:25px;">
						<c:url value="openMailView.html" var="url1" >
							<c:param name="decorator" value="popup"/>
							<c:param name="popup" value="true"/>
							<c:param name="emailStatusVal" value="${myFileList.emailStatus}"/>
						</c:url>
						<c:if test="${myFileList.emailStatus==null || myFileList.emailStatus=='' || myFileList.emailStatus==' '}">
						</c:if>						
						<c:if test="${myFileList.emailStatus!=null && myFileList.emailStatus!=''}">
							<c:forEach var="entry" items="${emailStatusList}">
							<c:if test="${myFileList.emailStatus==entry.key}">
								<c:set var="str" value="${entry.value}" />
							</c:if>
							</c:forEach>
							
						    <c:forEach var="num1" items="${fn:split(str, '^')}" varStatus="count">
						    <c:choose> 
							<c:when test="${count.last}">
 								<c:set var="strqq1" value="${num1}" />
 							</c:when>
							<c:otherwise>
								<c:set var="strqq2" value="${num1}" />
							</c:otherwise>			
							</c:choose>			    
				            </c:forEach>	
				            <c:choose> 
				            <c:when test="${strqq2 < 3}">
     				        <c:set var="strqq" value="Ready For Sending" />
				            <c:set var="emailSt" value="pendingState" />
				            </c:when>
				            <c:when test="${strqq2 > 2 && fn:indexOf(strqq1,'SaveForEmail')>-1}">
				            <c:set var="ste" value="${fn:replace(strqq1,'SaveForEmail','')}"/>
				             <c:set var="strqq" value="Email sending has failed ${strqq2} times,${ste}" />
				             <c:set var="emailSt" value="failed" />
				            </c:when>
				            <c:otherwise>
				            </c:otherwise>
				            </c:choose>
							 <c:set var="str1" value="SaveForEmail"/>
							 <c:if test="${fn:indexOf(str,str1)>-1}">
								 <c:if test="${emailSt=='pendingState'}">
								 <img src="<c:url value='/images/email_small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/ques-small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>
								 </c:if>
								 <c:if test="${emailSt!='pendingState'}">
 								 <img src="<c:url value='/images/email_small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/cancel001.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>				
								 </c:if>
							 </c:if>
							 <c:if test="${fn:indexOf(str,str1)<0}">
 							 <img src="<c:url value='/images/email_small.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/tick01.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>				
                           </c:if>
							 
							 
							 
						</c:if>	
						 &nbsp;<img src="<c:url value='/images/user-cabinet.png'/>" title="Email Status"  onclick="recipientWithEmailStatus(this,'${myFileList.id}');"/> 	
						</display:column>
						<display:column title="Invoice Attachment" >
						<c:if test="${myFileList.invoiceAttachment == true || myFileList.invoiceAttachment=='true'}">
											<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="updateInvoice('${myFileList.id}','${myFileList.fileType }',this)" checked/>
										 </c:if>										
										<c:if test="${myFileList.invoiceAttachment == false || myFileList.invoiceAttachment == null}">
											<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="updateInvoice('${myFileList.id}','${myFileList.fileType }',this)"/>
										</c:if>	
						</display:column>
						    <display:column title="Cust&nbsp;Portal" style="width:25px;">
						    <c:if test="${myFileList.isCportal == true}">
						    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusId(${myFileList.id},this)" checked/>
						    </c:if>
							<c:if test="${myFileList.isCportal == false || myFileList.isCportal == null}">
							<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusId(${myFileList.id},this)"/>
							</c:if>
						    </display:column>
						    
						    <display:column title="Acc&nbsp;Portal" style="width:25px;">
						    <c:if test="${myFileList.isAccportal == true}">
						    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId(${myFileList.id},this)" checked/>
						    </c:if>
							<c:if test="${myFileList.isAccportal == false || myFileList.isAccportal == null}">
							<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId(${myFileList.id},this)"/>
							</c:if>
						    </display:column>
						    
						    <display:column title="Partner&nbsp;Portal" style="width:25px;">
						    <c:if test="${myFileList.isPartnerPortal == true}">
						    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId(${myFileList.id},this);" checked/>
						    </c:if>
							<c:if test="${myFileList.isPartnerPortal == false || myFileList.isPartnerPortal == null}">
							<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId(${myFileList.id},this);"/>
							</c:if>
						    </display:column>
						    <display:column title="Service&nbsp;Provider" style="width:25px;">
						    <c:if test="${myFileList.isServiceProvider == true}">
						    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusServiceProviderId(${myFileList.id},this);" checked/>
						    </c:if>
							<c:if test="${myFileList.isServiceProvider == false || myFileList.isServiceProvider == null}">
							<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusServiceProviderId(${myFileList.id},this);"/>
							</c:if>
						    </display:column>
						    <configByCorp:fieldVisibility componentId="component.field.Resource.DriverPortal">
						      <display:column title="Driver&nbspPortal" style="width:25px;">
						    <c:if test="${myFileList.isDriver == true}">
						   <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusDriverPortalId(${myFileList.id},this);" checked/>
						    </c:if>
							<c:if test="${myFileList.isDriver == false || myFileList.isDriver == null}">
							<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusDriverPortalId(${myFileList.id},this);"/>
							</c:if>
						    </display:column>
						    </configByCorp:fieldVisibility>
						    <c:if test="${customerFile.controlFlag == 'C'}">
						      <display:column title="Booking&nbsp;Agent" style="width:25px;">
						      				<c:set var="ischeckedBA" value="false"/>
											<c:if test="${myFileList.isBookingAgent}">
												<c:set var="ischeckedBA" value="true"/>
											</c:if>
						    				<c:if test="${bookingAgentFlag=='BA'}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}" onclick="" disabled="true" checked/>
										</c:if>
										<c:if test="${bookingAgentFlag==''}">
										<c:if test="${myFileList.isBookingAgent}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}"  onclick="findSoAllPricing(${myFileList.id},this,'checkFlagBA');" checked/>
										</c:if>
										<c:if test="${!myFileList.isBookingAgent}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}" onclick="findSoAllPricing(${myFileList.id},this,'checkFlagBA');" unchecked/>
										</c:if>
										</c:if>																
						    </display:column>
						    
						      <display:column title="Network&nbsp;Agent" style="width:25px;">
						      			<c:set var="ischeckedNA" value="false"/>
											<c:if test="${myFileList.isNetworkAgent}">
												<c:set var="ischeckedNA" value="true"/>
											</c:if>	
						   				<c:if test="${networkAgentFlag=='NA'}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="" disabled="true" checked/>
										</c:if>
										<c:if test="${networkAgentFlag==''}">
										<c:if test="${myFileList.isNetworkAgent}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="findSoAllPricing(${myFileList.id},this,'checkFlagNA');" checked/>
										</c:if>	
										<c:if test="${!myFileList.isNetworkAgent}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="findSoAllPricing(${myFileList.id},this,'checkFlagNA');" unchecked/>
										</c:if>	
										</c:if>											
						    </display:column>
						    
						      <display:column title="Origin&nbsp;Agent" style="width:25px;">
						      			<c:set var="ischeckedOA" value="false"/>
											<c:if test="${myFileList.isOriginAgent}">
												<c:set var="ischeckedOA" value="true"/>
											</c:if>
						     			<c:if test="${originAgentFlag=='OA'}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="" disabled="true" checked/>
										</c:if>
										<c:if test="${originAgentFlag==''}">
										<c:if test="${myFileList.isOriginAgent}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="findSoAllPricing(${myFileList.id},this,'checkFlagOA');" checked/>
										</c:if>
										<c:if test="${!myFileList.isOriginAgent}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="findSoAllPricing(${myFileList.id},this,'checkFlagOA');" unchecked/>
										</c:if>		
										</c:if>
						    </display:column>
						    
						      <display:column title="Sub&nbsp;Origin&nbsp;Agent" style="width:25px;">
						      		<c:set var="ischeckedSOA" value="false"/>
											<c:if test="${myFileList.isSubOriginAgent}">
												<c:set var="ischeckedSOA" value="true"/>
											</c:if>												
						   				 <c:if test="${subOriginAgentFlag=='SOA' }">						   				 
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="" disabled="true" checked/>											
										</c:if>
										<c:if test="${subOriginAgentFlag=='' }">
										<c:if test="${myFileList.isSubOriginAgent}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="findSoAllPricing(${myFileList.id},this,'checkFlagSOA');" checked/>
											</c:if>
										<c:if test="${!myFileList.isSubOriginAgent}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="findSoAllPricing(${myFileList.id},this,'checkFlagSOA');" unchecked/>
											</c:if>
										</c:if>										
						    </display:column>
						    
						      <display:column title="Dest&nbsp;Agent" style="width:25px;">
						      		<c:set var="ischeckedDA" value="false"/>
											<c:if test="${myFileList.isDestAgent}">
												<c:set var="ischeckedDA" value="true"/>
											</c:if>
						    			<c:if test="${destAgentFlag=='DA' }">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="" disabled="true" checked/>
										</c:if>
										<c:if test="${destAgentFlag==''}">
										<c:if test="${myFileList.isDestAgent}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="findSoAllPricing(${myFileList.id},this,'checkFlagDA');" checked/>
										</c:if>
										<c:if test="${!myFileList.isDestAgent}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="findSoAllPricing(${myFileList.id},this,'checkFlagDA');" unchecked/>
										</c:if>
										</c:if>										 
						    </display:column>
						    
						      <display:column title="Sub&nbsp;Dest&nbsp;Agent" style="width:25px;">
						      			<c:set var="ischeckedSDA" value="false"/>
											<c:if test="${myFileList.isSubDestAgent}">
												<c:set var="ischeckedSDA" value="true"/>
											</c:if>	
						    		<c:if test="${subDestAgentFlag=='SDA'}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="" disabled="true" checked/>
										</c:if>
										<c:if test="${subDestAgentFlag==''}">
										<c:if test="${myFileList.isSubDestAgent}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="findSoAllPricing(${myFileList.id},this,'checkFlagSDA');" checked/>
										</c:if>
										<c:if test="${!myFileList.isSubDestAgent}">	
											<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="findSoAllPricing(${myFileList.id},this,'checkFlagSDA');" unchecked/>
										</c:if>
										</c:if>
						    </display:column>
						    </c:if>
						    <display:column title="Action" style="width:45px;font-size:9px;">
						          <s:select cssClass="list-menu" cssStyle="width:72px" name="action" list="{'','Edit','Split','Remove'}" onchange="return performAction(this, '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');" />
						    </display:column>
						    <%--					    
						    <display:column title="Edit" style="width:45px;font-size:9px;">
						     <c:if test="${myFileFor!='PO'}">	
						     	<a href="editFileUpload.html?fid=${myFileList.id}&id=${fileId}&myFileFor=${fileNameFor}&secure=${secure}&forQuotation=${forQuotation}" style="text-decoration: none;"><img align="top" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/><u>Edit</u></a>
						     </c:if>
						     <c:if test="${myFileFor=='PO'}">
						     <a href="editFileUpload.html?fid=${myFileList.id}&id=${fileId}&myFileFor=${fileNameFor}&secure=${secure}&ppType=${ppType}" style="text-decoration: none;"><img align="top" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/><u>Edit</u></a>
						     </c:if>	
					    	</display:column>
					    	<display:column title="Split" style="width:45px;font-size:9px; ">
					    	<c:if test="${myFileList.fileContentType == 'application/pdf' && myFileFor!='PO'}">
					     		<a href="docSplit.html?fileId=${myFileList.id}&id=${fileId}&myFileFor=${fileNameFor}&from=main&relatedDocs=No&secure=${secure}&forQuotation=${forQuotation}" style="text-decoration: none;"><img align="top" style="margin: 0px 0px 0px 2px;" src="images/split-doc.gif"/><u>Split</u></a>
					    	</c:if>
					    	</display:column>	
					    	--%>				    	
					     </c:if>
					    <%--
					    <c:if test="${from != 'View'}">
						    <display:column title="Remove" style="width:40px;">
						    	<a><img align="middle" onclick="confirmSubmit(${myFileList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
						    </display:column>
					    </c:if>					    
					    --%>
					    <display:setProperty name="paging.banner.item_name" value="document"/>   
					    <display:setProperty name="paging.banner.items_name" value="documents"/>   
					  <div style="background:url(images/export-bg.png) repeat-x; height:30px; cursor: pointer;border:1px solid red; ">
					    <display:setProperty name="export.excel.filename" value="Document List.xls"/>   
					    <display:setProperty name="export.csv.filename" value="Document List.csv"/>   
					    <display:setProperty name="export.pdf.filename" value="Document List.pdf"/>   
					   </div>
					</display:table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<c:if test="${from != 'View'}">
    <c:if test="${userType!='DRIVER'}">
<c:out value="${buttons}" escapeXml="false" />

<%-- <input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:65px; font-size: 15" value="Email" onclick="emailDoc();"/> --%>

<input type="button" class="cssbutton" style="margin-right: 5px;width:70px; " name="dwnldBtn"  value="Download" onclick="downloadDoc();"/>
</c:if>
<c:if test="${myFileFor!='PO' && myFileFor!='Truck' && userType!='DRIVER'}">
	<input type="button" class="cssbutton" style="margin-right: 5px;width:70px; " name="combBtn"  value="Merge PDF" onclick="return conbinedDoc();"/>
	<c:if test="${fn1:indexOf(transDocSysDefault,transDocJobType)>=0}">
		<input type="button" class="cssbutton" style="margin-right: 5px;width:95px;" name="mailBtn"  value="TransDoc Xfer" onclick="return transDoc();"/>
	</c:if>
</c:if>
</c:if>
 <!-- <input type="button" class="cssbutton" style="margin-right: 5px;width:85px;" name="combBtn"  value="Covert&nbsp;To&nbsp;PDF" onclick="return conbinedDocInPdf();"/> -->
 
<input type="button" class="cssbutton" style="margin-right: 5px;width:65px;"  value="Email" onclick=" attachDocInEmail();"/>
</s:form>
<script type="text/javascript">
function findSoAllPricing(fid,event,targetValue){
	progressBarAutoSave('1');
	var myFileFor='${fileNameFor}';
	var id="";
   if(myFileFor=='SO'){
	    id='${serviceOrder.id}';
   }else if(myFileFor=='CF'){
	   id='${customerFile.id}';
   }
	var fid=fid;
	var valueTarget="";	
  if(event.checked==true){
	  if(targetValue=='checkFlagBA'){
			valueTarget="BA";
		}else if(targetValue=='checkFlagNA'){
			valueTarget="NA";
		}else if(targetValue=='checkFlagOA'){
			valueTarget="OA";
		}else if(targetValue=='checkFlagSOA'){
			valueTarget="SOA";
		}else if(targetValue=='checkFlagDA'){
			valueTarget="DA";
		}else if(targetValue=='checkFlagSDA'){
			valueTarget="SDA";
		}
	new Ajax.Request('uploadMyFileIsCheckBox.html?ajax=1&decorator=simple&popup=true&'+targetValue+'='+valueTarget+'&myFileFor=${fileNameFor}&fid='+fid+'&id='+id,
				  {
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				 				
				      container.innerHTML = response;
				      //container.update(response);
				       showOrHide(0);
				      container.show();
				     
				    },
				    
				    onLoading: function()
				       {
				    	var loading = document.getElementById("loading").style.display = "block";
				    	
				    	 loading.innerHTML;
				    },
				    
				    onFailure: function(){ 
					    //alert('Something went wrong...')
					     }
				  });
  }else{
	 if (event.checked==false){
	  if(targetValue=='checkFlagBA'){
			valueTarget="NBA";
		}else if(targetValue=='checkFlagNA'){
			valueTarget="NNA";
		}else if(targetValue=='checkFlagOA'){
			valueTarget="NOA";
		}else if(targetValue=='checkFlagSOA'){
			valueTarget="NSOA";
		}else if(targetValue=='checkFlagDA'){
			valueTarget="NDA";
		}else if(targetValue=='checkFlagSDA'){
			valueTarget="NSDA";
		}
	  new Ajax.Request('unCheckMyFileIsCheckBox.html?ajax=1&decorator=simple&popup=true&'+targetValue+'='+valueTarget+'&myFileFor=${fileNameFor}&fid='+fid+'&id='+id,
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			   				
			      container.innerHTML = response;
			      //container.update(response);
			       showOrHide(0);
			      container.show();
			     
			    },
			    onLoading: function()
			       {
			    	var loading = document.getElementById("loading").style.display = "block";
			    	
			    	 loading.innerHTML;
			    },
			    
			    onFailure: function(){ 
				    //alert('Something went wrong...')
				     }
			  });  
  }
  }
  setTimeout("pageReload()",10000);  
		}

function pageReload(){
	//progressBarAutoSave('0');
	location.reload(true);
}



</script>

<script type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.0.3.min.js"></script>

<script>
$(document).ready(function(){
$("#Layer1 div.exportlinks").addClass('CFEXP_left');
$("#Layer1 div.exportlinks").after('<div class="CFEXP_right"></div>');
$("#Layer1 div.exportlinks a").first().css('margin-left','65px');
$("#Layer1 div.exportlinks").css('padding-top','7px'); 
$("#Layer1 div.exportlinks").css('margin-bottom','5px'); 
$("#Layer1 div.exportlinks").css('text-transform','uppercase');
$("#Layer1 div.exportlinks a").css('text-transform','capitalize'); 
})
</script>

<script type="text/javascript"> 
try{
	function validatefields(emailStatus) { 		
		javascript:openWindow('openMailView.html?emailStatusVal='+encodeURI(emailStatus)+'&decorator=popup&popup=true&from=file',650,320) ;
		}
<c:if test="${hitFlag == 5}" >
	alert('The selected document type is not mapped to TransDoc so it will not be transferred.');
	location.href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=false";
</c:if>
<c:if test="${hitFlag == 1}" >
location.href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=false";
</c:if>
 }
 catch(e){}
    try{
    var len = document.forms['myFileForm'].elements['DD'].length;
    }
    catch(e){}
   try{
    if(len>1){
    	show('chkAllButton');
    }
    }
    catch(e){}
</script>
<script type="text/javascript">
try{

	//Created by subrat BUG #7530
	<sec-auth:authComponent componentId="module.tab.workTicket.DRIVER">
	 disabledAll();
	</sec-auth:authComponent>
	window.onload=function(){
	var category='${categorySearchValue}';
	document.forms['myFileForm'].elements['filterbycategory'].value=category;
		//alert(category)
	}
}catch(e){
}
</script>
<script type="text/javascript">
 function recipientWithEmailStatus(position,myFileId) {
	 var myFileFor='${fileNameFor}';
		if(myFileFor=='SO')
	{
	    var jobNumber=document.forms['myFileForm'].elements['serviceOrder.shipNumber'].value;
	}
		if(myFileFor=='CF')
	{
			 var jobNumber=document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
	}
 var url="recipientWithEmailStatus.html?ajax=1&decorator=simple&popup=true&jobNumber="+jobNumber+"&noteFor=${noteFor}&myFileId="+myFileId;
 ajax_showTooltip(url,position);
 }
</script>
<script type="text/javascript">
function attachDocInEmail()
{
	var myFileFor='${fileNameFor}';
	if(myFileFor=='SO')
{
    var jobNumber=document.forms['myFileForm'].elements['serviceOrder.shipNumber'].value;
}
	if(myFileFor=='CF')
{
		 var jobNumber=document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
}
var checkBoxEmailId = document.forms['myFileForm'].elements['userCheck'].value;
var myFileIdCheck = document.forms['myFileForm'].elements['myFileIdCheck'].value;
var checkForInvalid=true;
if(myFileIdCheck!=null && myFileIdCheck!=undefined && myFileIdCheck!=''){
	checkForInvalid=checkForInvalidSelect(myFileIdCheck);
}
if(checkBoxEmailId ==''){
	alert('Please select the one or more document to e-mail.');
}else if(!checkForInvalid){
	alert('You are not allowed to send the attachment of pending Email.');
}else{
	var url = 'sendEmailFile.html?decorator=popup&popup=true&checkBoxEmailId='+checkBoxEmailId+'&jobNumber='+jobNumber+"&noteFor=${noteFor}&companyDivision=${customerFile.companyDivision}&myFileIdCheck="+myFileIdCheck;
	window.openWindow(url,height=570,width=520);
}
}

function collectFileId(){
	document.forms['myFileForm'].elements['myFileIdCheck'].value = "";
	var idList='';
	var temp='';
	if(document.forms['myFileForm'].elements['DD'].length!=undefined){
		var len = document.forms['myFileForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		if(document.forms['myFileForm'].elements['DD'][i].checked){
			temp=document.forms['myFileForm'].elements['DD'][i].value;
			if(idList==''){
				idList=temp.split("`")[0];
			}else{
				idList=idList+","+temp.split("`")[0];
			}
		}
		document.forms['myFileForm'].elements['myFileIdCheck'].value = idList;
	}
	}else{
		
		temp=document.forms['myFileForm'].elements['DD'].value;
		idList=temp.split("`")[0];
		document.forms['myFileForm'].elements['myFileIdCheck'].value = idList;
	}
}
function checkForInvalidSelect(myFileIdCheck){
	var flag =true;
	var idArr= myFileIdCheck.split(",");
	var tempId='';
	var tempVal='';
	for(var i=0;i<idArr.length && flag;i++){
		flag = checkForPendingStatus(idArr[i]);
	}
	return flag;
}
function checkForPendingStatus(id){
	var flag =true;
	var emailId=getEmailSetupId(id);
	var retryCont = getRetryCount(emailId); 
	if(retryCont<=2 && getDescription(emailId)=='SaveForEmail'){
		flag =false;
	}
	return flag;
}
function getDescription(id){
	var tempId='';
	var tempValue='';
	<c:forEach var="entry" items="${emailStatusList}">
		if(tempValue==''){
			tempId="${entry.key}";
			if(tempId==id){
				tempValue="${entry.value}";
				tempValue=tempValue.split('^')[1];
			}
		}
	</c:forEach>
	return tempValue;
}
function getRetryCount(id){
	var tempId='';
	var tempCount='';
	<c:forEach var="entry" items="${emailStatusList}">
		if(tempCount==''){
			tempId="${entry.key}";
			if(tempId==id){
				tempCount="${entry.value}";
				tempCount=tempCount.split('^')[0];
			}
		}
	</c:forEach>
	return tempCount;
}
function getEmailSetupId(id){
	var tempId='';
	var tempValue='';
	<c:forEach var="num1" items="${fn:split(myFileStatusCheck, '^')}" varStatus="count">
	if(tempValue==''){
		tempId="${num1}";
		if(tempId.split(":")[0]==id){
			if(tempId.split(":")[1]!=undefined){
			tempValue=tempId.split(":")[1];
		}
		}
		
	}
	</c:forEach>
	return tempValue;
}
function updateInvoice(rowId,fileType,targetElement) 
{		
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")	
		 targetElement.checked=false;		
	}else{
		var Status = targetElement.checked;
		var url="updateInvoiceAttachment.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		httpupdateInvoice.open("GET", url, true);
       httpupdateInvoice.onreadystatechange = handleHttpResponseupdateInvoice;
       httpupdateInvoice.send(null);	
	     }
} 
function handleHttpResponseupdateInvoice(){
     if (httpupdateInvoice.readyState == 4){
          var result= httpupdateInvoice.responseText         
     }
}
function downloadSelectedFile(id){
	var fileIdVal = document.forms['myFileForm'].elements['fileId'].value;
	var fieldVal = document.forms['myFileForm'].elements['myFileForVal'].value;
	var fieldVal1 = document.forms['myFileForm'].elements['noteForVal'].value;
	var fieldVal2 = document.forms['myFileForm'].elements['activeVal'].value;
	var fieldVal3 = document.forms['myFileForm'].elements['secureVal'].value;
	var myFileJspName = "myFiles";
	var url="";
	if(fieldVal=='SO'){
		var fieldVal4 = document.forms['myFileForm'].elements['serviceOrder.shipNumber'].value;
		url="ImageServletAction.html?id="+id+"&myFileForVal="+fieldVal+"&noteForVal="+fieldVal1+"&activeVal="+fieldVal2+"&secureVal="+fieldVal3+"&shipNumber="+fieldVal4+"&fileIdVal="+fileIdVal+"&myFileJspName="+myFileJspName+"";
	}else if(fieldVal=='CF'){
		var fieldVal5 = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
		url="ImageServletAction.html?id="+id+"&myFileForVal="+fieldVal+"&noteForVal="+fieldVal1+"&activeVal="+fieldVal2+"&secureVal="+fieldVal3+"&sequenceNumber="+fieldVal5+"&fileIdVal="+fileIdVal+"&myFileJspName="+myFileJspName+"";
	}else if(fieldVal=='PO'){
		var ppTypeVal = document.forms['myFileForm'].elements['ppType'].value;
		var ppIdVal = document.forms['myFileForm'].elements['PPID'].value;
		url="ImageServletAction.html?id="+id+"&myFileForVal="+fieldVal+"&noteForVal="+fieldVal1+"&activeVal="+fieldVal2+"&secureVal="+fieldVal3+"&fileIdVal="+fileIdVal+"&myFileJspName="+myFileJspName+"&ppTypeVal="+ppTypeVal+"&ppIdVal="+ppIdVal+"";
	}
	else if(fieldVal=='Truck'){
		var ppTypeVal = document.forms['myFileForm'].elements['ppType'].value;
		var ppIdVal = document.forms['myFileForm'].elements['PPID'].value;
		url="ImageServletAction.html?id="+id+"&myFileForVal="+fieldVal+"&noteForVal="+fieldVal1+"&activeVal="+fieldVal2+"&secureVal="+fieldVal3+"&fileIdVal="+fileIdVal+"&myFileJspName="+myFileJspName+"&ppTypeVal="+ppTypeVal+"&ppIdVal="+ppIdVal+"";
	}
	location.href=url;
}
var fileVal = '${resultType}';
if ((fileVal!=null && fileVal!='') && (fileVal=='errorNoFile')){
	alert('This File is Temporarily Unavailable.')
	var replaceURL = "";
	var myFileForVal =  "<%=request.getAttribute("myFileFor")%>";
	var noteForVal = "<%=request.getAttribute("noteFor")%>";
	var activeVal = "<%=request.getAttribute("active")%>";
	var secureVal = "<%=request.getAttribute("secure")%>";
	var query = window.location.search.substring(1);
	var vars = query.split("=");
	var totalVars= vars[1];
	var idArr = totalVars.split("&");
	var fileIdVal = idArr[0];
	if(myFileForVal=='SO' || myFileForVal=='CF'){
		replaceURL = "myFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"";
	}else if(myFileForVal=='PO'){
		var ppTypeVal = document.forms['myFileForm'].elements['ppType'].value;
		var ppIdVal = document.forms['myFileForm'].elements['PPID'].value;
		replaceURL = "myFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&ppType="+ppTypeVal+"&PPID="+ppIdVal+"";
	}
	window.location.replace(replaceURL);
}
</script>
