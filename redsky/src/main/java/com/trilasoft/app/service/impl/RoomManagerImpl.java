/**
 * 
 */
package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.RoomDao;
import com.trilasoft.app.model.Room;
import com.trilasoft.app.service.RoomManager;

/**
 * @author GVerma
 *
 */
public class RoomManagerImpl extends GenericManagerImpl<Room, Long> implements RoomManager {
	RoomDao roomDao;
	public RoomManagerImpl(RoomDao roomDao){
		super(roomDao);
		this.roomDao = roomDao;
	}
	public List getAllRoom(String sessionCorpID, String shipno) {
		return roomDao.getAllRoom(sessionCorpID, shipno);
	}
	public List checkRoom(String corpID, int roomCode, String shipNumber){
		return roomDao.checkRoom(corpID, roomCode, shipNumber);
	}

}
