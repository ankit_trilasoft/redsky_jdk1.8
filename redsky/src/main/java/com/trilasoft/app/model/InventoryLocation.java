/**
 * 
 */
package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author GVerma
 *
 */

@Entity
@Table(name="inventorylocation")
public class InventoryLocation {
	private Long id;
	private String workTicket;
	private String locationType;
	private String shipNumber;
	private String sequenceNumber;
	private String corpId;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private Long imageId;
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("workTicket", workTicket)
				.append("locationType", locationType)
				.append("shipNumber", shipNumber)
				.append("sequenceNumber", sequenceNumber)
				.append("corpId", corpId).append("createdOn", createdOn)
				.append("createdBy", createdBy).append("updatedOn", updatedOn)
				.append("updatedBy", updatedBy).append("imageId", imageId)
				.toString();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof InventoryLocation))
			return false;
		InventoryLocation castOther = (InventoryLocation) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(workTicket, castOther.workTicket)
				.append(locationType, castOther.locationType)
				.append(shipNumber, castOther.shipNumber)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(corpId, castOther.corpId)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(imageId, castOther.imageId).isEquals();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(workTicket)
				.append(locationType).append(shipNumber).append(sequenceNumber)
				.append(corpId).append(createdOn).append(createdBy)
				.append(updatedOn).append(updatedBy).append(imageId)
				.toHashCode();
	}

	/**
	 * @return the id
	 */
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the workTicket
	 */
	@Column
	public String getWorkTicket() {
		return workTicket;
	}
	/**
	 * @param workTicket the workTicket to set
	 */
	public void setWorkTicket(String workTicket) {
		this.workTicket = workTicket;
	}
	/**
	 * @return the locationType
	 */
	@Column
	public String getLocationType() {
		return locationType;
	}
	/**
	 * @param locationType the locationType to set
	 */
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
	/**
	 * @return the shipNumber
	 */
	@Column
	public String getShipNumber() {
		return shipNumber;
	}
	/**
	 * @param shipNumber the shipNumber to set
	 */
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	/**
	 * @return the sequenceNumber
	 */
	@Column
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	/**
	 * @param sequenceNumber the sequenceNumber to set
	 */
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	/**
	 * @return the corpId
	 */
	@Column
	public String getCorpId() {
		return corpId;
	}
	/**
	 * @param corpId the corpId to set
	 */
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	/**
	 * @return the createdOn
	 */
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the createdBy
	 */
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the updatedOn
	 */
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the updatedBy
	 */
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the imageId
	 */
	public Long getImageId() {
		return imageId;
	}
	/**
	 * @param imageId the imageId to set
	 */
	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}


}
