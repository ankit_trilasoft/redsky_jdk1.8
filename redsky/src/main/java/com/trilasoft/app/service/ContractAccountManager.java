package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ContractAccount;

public interface ContractAccountManager extends GenericDao<ContractAccount,Long>{
	public List findMaximumId();
	public List findContractAccountList(String contract,String corpId);
	public List isExisted(String accountCode,String contract,String corpId);
	public List findContractAccountByContract(String sessionCorpID);
	public List validateAccountcode(String accountCode, String sessionCorpID);
	public List findContractAccountRespectToContract(String contract,String sessionCorpID);
	public List findChildCode(String parentCode,String sessionCorpID );
	public int countPartnerCode(String childCode,String partnerCode,String contract,String sessionCorpID );
	public List findChildAccountCode(String partnerCode,String contract,String sessionCorpID );
	public List checkPublicParentCode(String ChildCode,String partnerCode,String sessionCorpID );
	public void findDeleteContractAccountOfOtherCorpId(String contractName, String agentCorpId);
}
