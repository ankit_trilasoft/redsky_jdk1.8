// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`inlandagent` ADD COLUMN `idNumber` VARCHAR(10) AFTER `chasisfreeDays`;

alter table reports add excludeFromForm bit(1) default false;

ALTER TABLE `redsky`.`itemsjequip` ADD COLUMN `resourceForAccPortal` bit(1) DEFAULT b'0';

ALTER TABLE `redsky`.`standardaddresses` CHANGE COLUMN `residenceNotes` `residenceNotes` TEXT NULL DEFAULT NULL  ;

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('CWMS','task_parameter',50,true,now(),'CWMS_SETUP',now(),'CWMS_SETUP',1,1,'');

INSERT  into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('CWMS', 'category 1', 'category 1', 50, 'task_parameter','en',now(),'CWMS_SETUP',now(),'CWMS_SETUP'),
('CWMS', 'category 2', 'category 2', 50, 'task_parameter','en',now(),'CWMS_SETUP',now(),'CWMS_SETUP'),
('CWMS', 'category 3', 'category 3', 50, 'task_parameter','en',now(),'CWMS_SETUP',now(),'CWMS_SETUP');

alter table partnerpublic add sentToAccounting bit(1) default false;

ALTER TABLE `redsky`.`taskdetails` ADD COLUMN `category` VARCHAR(100); 

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `buyRate` DECIMAL(4,2) DEFAULT 0 AFTER `internalBillingPerson`,
 ADD COLUMN `sellRate` DECIMAL(4,2) DEFAULT 0 AFTER `buyRate`;
 
 ALTER TABLE `redsky`.`accountline` ADD COLUMN `accrueExpenseVariance` DATETIME NULL  AFTER `accrueRevenueVariance` ;
 
  alter table customersurveyfeedback add column `couselorFeedBack` text COLLATE utf8_unicode_ci;
alter table customersurveyfeedback add column `oaFeedBack` text COLLATE utf8_unicode_ci;
alter table customersurveyfeedback add column `daFeedBack` text COLLATE utf8_unicode_ci;
alter table customersurveyfeedback add column `recommendationFeedBack` text COLLATE utf8_unicode_ci;
alter table customersurveyfeedback add column `additionalFeedBack` text COLLATE utf8_unicode_ci;
alter table customersurveyfeedback add column `additionalSecondFeedBack` text COLLATE utf8_unicode_ci;


alter table customersurveyfeedback add column `couselorRating` bigint(20) unsigned DEFAULT NULL;
alter table customersurveyfeedback add column `oaRating` bigint(20) unsigned DEFAULT NULL;
alter table customersurveyfeedback add column `daRating` bigint(20) unsigned DEFAULT NULL;
alter table customersurveyfeedback add column `recommendation` bigint(20) unsigned DEFAULT NULL;