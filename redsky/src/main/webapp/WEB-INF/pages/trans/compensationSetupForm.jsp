<%@ include file="/common/taglibs.jsp"%>  
<head>   
    <title><fmt:message key="compensationList.title"/></title>  
    <meta name="heading" content="CompensationSetUp Form" /> 
 <style>
 span.pagelinks {display:block; font-size:0.95em; margin-bottom:2px;!margin-bottom:0px; margin-top:-18px; !margin-top:-18px;
padding:2px 0px; text-align:right; width:100%; !width:100%;}
div#main {margin:-5px 0 0;!margin:0px;}
</style>
</head>   
<s:form id="compensationForm"  method="post" validate="true">
<s:hidden name="contractid" value="${contractid}"/>
<s:hidden name="contractName" value="${contractName}"/>
<div id="Layer1" style="width: 60%">  
		<div class="spnblk">&nbsp;</div>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<div id="newmnav">
  <ul>
    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>CompensationSetup Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
    <li><a href="searchContracts.html"><span>Contract List</span></a></li>
    <li><a href="editContract.html?id=${contractid}"><span>Contract Details</span></a></li>
    <li><a href="charge23.html?id=${contractid}"><span>Charge List</span></a></li>
		</ul>
</div>		
</tr>
</tbody>
</table>
<s:set name="compensationList" value="compensationList" scope="request"/>
<display:table name="compensationList" defaultsort="1" class="table" requestURI="" id="compensationList" >   
<display:column   title="Compensation&nbsp;Year">
<input type="text" style="width:100px;"  class="input-text pr-f11"  id="compensationyear${compensationList.id}" value="${compensationList.compensationyear}" onchange="ValidationSpecialCharcter(this,'${compensationList.id}');"  onkeydown="return onlyFloatNumsAllowed(event)" onkeyup="return yearValidation(this.value)" onkeypress="isNumber(event,'${compensationList.id}')"/>
</display:column> 
<display:column   title="Commission&nbsp;Percentage" >
<input type="text" style="width:100px;text-align:right"  class="input-text pr-f11"  id="commissionpercentage${compensationList.id}" value="${compensationList.commissionpercentage}"  onchange="onlyFloat2(this,'${compensationList.id}');ValidationSpecialCharcter(this,'${compensationList.id}');" onkeydown="return onlyFloatNumsAllowed(event)"  />
</display:column> 
</display:table>
<input type="button" name="AddLine" value="Add Line" onclick="addLine();" class="cssbuttonA" style="width:65px; height:25px" tabindex=""/> 
</div>
</s:form>
<script type="text/javascript">
function addLine() {
	showOrHide(1);
    document.forms['compensationForm'].action ='addCompensationSetupLine.html';
	var check=true;
 	if(check){
    document.forms['compensationForm'].submit();
    } 
    }
    

function updateCompensationSetUpLine(id){
	showOrHide(1);
	var contractId=document.forms['compensationForm'].elements['contractid'].value;
	var contractName=document.forms['compensationForm'].elements['contractName'].value;
	var Compensationyear = document.getElementById('compensationyear'+id).value;
	var Commissionpercentage = document.getElementById('commissionpercentage'+id).value;
	var url ='';
	if(contractId!=null){
		  url = "updatecompensationSetupListAjax.html?ajax=1&id="+id+"&compensationyear="+Compensationyear+"&commissionpercentage="+Commissionpercentage+"&contractName="+contractName+"&contractid="+contractId;
		  http120.open("POST", url, true); 
		  http120.onreadystatechange = handleHttpResponse120;
		  http120.send(null);
		}
	else{
		
	}
}

function handleHttpResponse120(){
	if (http120.readyState == 4){		
            var results = http120.responseText
            showOrHide(0);
	}
}


function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http120 = getHTTPObject();
    
    
    function showOrHide(value) {
        if (value==0) {
           	if (document.layers)
               document.layers["overlay"].visibility='hide';
            else
               document.getElementById("overlay").style.visibility='hidden';
       	}else if (value==1) {
       		if (document.layers)
              document.layers["overlay"].visibility='show';
           	else
              document.getElementById("overlay").style.visibility='visible';
       	} } 

	function onlyFloatNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==35) || (keyCode==36); 
	}
	
	function onlyFloat2(e,listId) {
	    var t;
	    var n = e.value;
	    var r = 0;
	    for (t = 0; t < n.length; t++) {
	        var i = n.charAt(t);
	        if (i == ".") {
	            r = r + 1
	        }
	        if ((i < "0" || i > "9") && i != "." || r > "1") {
	            document.getElementById(e.id).value ="";
	            alert ("Special characters are not allowed.");
	            document.getElementById(e.id).select();
	            return false;
	        }else{
	        	
	        	return true;
	        }
	    }
	    
	}

	function ValidationSpecialCharcter(x,listId){
		var iChars = "!`@#$%^&*()+=-[]\\\';,/{}|\":<>?~_";   

		var data = document.getElementById(x.id).value;

		for (var i = 0; i < data.length; i++)

		{      
		    if (iChars.indexOf(data.charAt(i)) != -1)
		    {    
		    alert ("Special characters are not allowed.");
		    document.getElementById(x.id).value = "";
		    return false; 
		    }else{
		    	updateCompensationSetUpLine(listId);
	        	return true;	
		    } 
		} 
		}
	
	
	
	function isNumber(evt,listVal) {
	    evt = (evt) ? evt : window.event;
	    var charCode = (evt.which) ? evt.which : evt.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	        return false;
	    }else{
	    	
	    	return true;
	    }
	}
	

	
	 <c:if test ="${hitFlag=='1'}">
 	     showOrHide(0);
	</c:if>
	
	
	
</script>

