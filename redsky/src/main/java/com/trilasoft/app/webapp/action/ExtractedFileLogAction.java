package com.trilasoft.app.webapp.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;


import com.trilasoft.app.model.ExtractedFileLog;
import com.trilasoft.app.service.ExtractedFileLogManager;

public class ExtractedFileLogAction extends BaseAction{
	private String sessionCorpID;
	private List<ExtractedFileLog> extractedFileLogList;
	private Set extractedFileLogSet;
	private ExtractedFileLogManager extractedFileLogManager;
	static final Logger logger = Logger.getLogger(AuditSetupAction.class);
	Date currentdate = new Date();
	private String moduleName;
	private String extractCreatedby;
	private Date fromExtractDate;
	private Date toExtractDate;
	private String extractCreatedbyFirstName;
	private String extractCreatedbyLastName;
	
	public ExtractedFileLogAction(){
		   Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID(); 
	        
		}
	
	@SkipValidation
	public String extractedFileLogList() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		extractedFileLogList = extractedFileLogManager.getAll();
		extractedFileLogSet = new HashSet(extractedFileLogList);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String searchExtractedFileLog() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		 StringBuffer fromExtractDates = new StringBuffer();
		 StringBuffer toExtractDates = new StringBuffer();
		 if (fromExtractDate != null && toExtractDate != null) {
			 fromExtractDates = new StringBuffer(formats.format(fromExtractDate));
			 toExtractDates = new StringBuffer(formats.format(toExtractDate)); 
			} else if (fromExtractDate != null && toExtractDate == null) {
				fromExtractDates = new StringBuffer(formats.format(fromExtractDate));
				toExtractDates = new StringBuffer();
			} else if (fromExtractDate == null && toExtractDate != null) {
				fromExtractDates = new StringBuffer();
				toExtractDates = new StringBuffer(formats.format(toExtractDate));
			} else {
				fromExtractDates = new StringBuffer();
				toExtractDates = new StringBuffer();
			}
		 extractedFileLogList = extractedFileLogManager.searchExtractedFileLog(sessionCorpID,moduleName,extractCreatedbyFirstName,extractCreatedbyLastName,fromExtractDates.toString(),toExtractDates.toString());
		 extractedFileLogSet = new HashSet(extractedFileLogList);
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		 return SUCCESS;
	}
	
	private List fileListByModule;
	@SkipValidation
	public String findExtractedFileLogListByModule() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 fileListByModule = extractedFileLogManager.getFileLogListByModule(sessionCorpID,moduleName);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public List<ExtractedFileLog> getExtractedFileLogList() {
		return extractedFileLogList;
	}

	public void setExtractedFileLogList(List<ExtractedFileLog> extractedFileLogList) {
		this.extractedFileLogList = extractedFileLogList;
	}

	public void setExtractedFileLogManager(
			ExtractedFileLogManager extractedFileLogManager) {
		this.extractedFileLogManager = extractedFileLogManager;
	}

	public Set getExtractedFileLogSet() {
		return extractedFileLogSet;
	}

	public void setExtractedFileLogSet(Set extractedFileLogSet) {
		this.extractedFileLogSet = extractedFileLogSet;
	}
    
	public String getExtractCreatedby() {
		return extractCreatedby;
	}

	public void setExtractCreatedby(String extractCreatedby) {
		this.extractCreatedby = extractCreatedby;
	}

	public Date getFromExtractDate() {
		return fromExtractDate;
	}

	public void setFromExtractDate(Date fromExtractDate) {
		this.fromExtractDate = fromExtractDate;
	}

	public Date getToExtractDate() {
		return toExtractDate;
	}

	public void setToExtractDate(Date toExtractDate) {
		this.toExtractDate = toExtractDate;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getExtractCreatedbyFirstName() {
		return extractCreatedbyFirstName;
	}

	public void setExtractCreatedbyFirstName(String extractCreatedbyFirstName) {
		this.extractCreatedbyFirstName = extractCreatedbyFirstName;
	}

	public String getExtractCreatedbyLastName() {
		return extractCreatedbyLastName;
	}

	public void setExtractCreatedbyLastName(String extractCreatedbyLastName) {
		this.extractCreatedbyLastName = extractCreatedbyLastName;
	}

	public List getFileListByModule() {
		return fileListByModule;
	}

	public void setFileListByModule(List fileListByModule) {
		this.fileListByModule = fileListByModule;
	}
}
