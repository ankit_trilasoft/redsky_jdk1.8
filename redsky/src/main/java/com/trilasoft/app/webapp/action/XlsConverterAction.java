package com.trilasoft.app.webapp.action;
import org.apache.log4j.PropertyConfigurator;

import org.apache.log4j.Logger;

import org.acegisecurity.Authentication;
import java.util.*;

import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.Location;
import com.trilasoft.app.service.DefaultAccountLineManager;
import com.trilasoft.app.service.RateGridManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.XlsConverterManager;

public class XlsConverterAction extends BaseAction {
	private static final long serialVersionUID = 1L;
	private String file;
	private String sessionCorpID;
	private XlsConverterManager xlsConverterManager;
	private Location location;
	private String charge;
	private String contractName;
	private String twoDGridUnit;
	public List rateGridIds;
	private RateGridManager rateGridManager;
	public String fileExt;
	private String hitFlag;
	private RefMasterManager refMasterManager;
	private DefaultAccountLineManager defaultAccountLineManager;
	
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(XlsConverterAction.class);

	 

	
	public XlsConverterAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
			}
	public String importXlsData(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xlsConverterManager.saveData(sessionCorpID,file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage(getText(Key)); }
		    }catch (Exception e)
			{
			e.printStackTrace ();
			}
		    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	
	public String importXlsFileRateGrid(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xlsConverterManager.saveRateGridData(sessionCorpID,file,userName,charge,contractName,fileExt); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage(getText(Key)); }
		    }catch (Exception e)
			{
			e.printStackTrace ();
			}
   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	
	public String uploadReplaceFileRateGrid(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 String tempIds="";
		try
		{   
			rateGridIds=rateGridManager.findGridIds(charge,contractName,sessionCorpID);
			if(!(rateGridIds.isEmpty())){
			 Iterator it=rateGridIds.iterator();
			 while(it.hasNext()){
				tempIds=tempIds+it.next()+",";
			 }
			 tempIds=tempIds.substring(0,(tempIds.length()-1));
			 tempIds=tempIds.trim();
			}
			String userName = getRequest().getRemoteUser();
			String Key= xlsConverterManager.saveDelRateGridData(sessionCorpID,file,userName,charge,contractName,tempIds,fileExt); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage(getText(Key)); }
		 }catch (Exception e){
			 e.printStackTrace ();
		}
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}

	
	public String importStorageData(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= xlsConverterManager.saveStorageData(sessionCorpID,file,userName); 
			if(Key.equalsIgnoreCase("Not proper fromat")){
				saveMessage("File is not in proper format"); 
			}
			else{saveMessage(getText(Key)); }
		    }catch (Exception e)
			{
			e.printStackTrace ();
			}
		   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	private String msgDisplay;
	private Boolean costElementFlag=false;
	private List defaultAccountlineList;
	private String[] deleteTemplateId;
	private String checkListAll="";
	public String importXlsFileDefaultAccountline(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{   
			if(!hitFlag.equals("1")){
			costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
			String userName = getRequest().getRemoteUser();
			String Key= xlsConverterManager.saveXlsFileDefaultAccountline(sessionCorpID,file,userName,fileExt,costElementFlag); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}else{
				xlsConverterManager.updateDefaultAccountlinethroughProcedure(sessionCorpID,costElementFlag,userName);
				defaultAccountlineList=defaultAccountLineManager.getDuplicatedData(sessionCorpID);
					hitFlag="1";
					saveMessage(getText(Key)); 
			}
			if((Key!=null && !Key.equalsIgnoreCase("")) && Key.contains("successfully")){
				msgDisplay = Key;
			}
		}else{
			try{
				if(deleteTemplateId!=null && deleteTemplateId.length>0)
				{
					for(int i=0;i<deleteTemplateId.length;i++)
					{
						if(checkListAll.equals(""))
						{
							checkListAll = ""+deleteTemplateId[i].toString()+"";
						}
						else{
							checkListAll = checkListAll+","+""+deleteTemplateId[i].toString()+"";
						}
					}
					String returnValue = defaultAccountLineManager.deleteDuplicateTemplateData(sessionCorpID,checkListAll);
					if(returnValue ==""){
						saveMessage("Error occured"); 
					}else{
					hitFlag="1";
					saveMessage(getText(returnValue));
					}
				}
		}catch(Exception e){
			e.printStackTrace();
		}
		}}
		catch (Exception e)
			{
			e.printStackTrace ();
			}
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	public String importXlsRateGrid(){
		return SUCCESS; 
	}
	public String importXlsDataPage(){
		return SUCCESS; 
	}
	public String uploadStorageFileForm(){
		return SUCCESS; 
	}
	
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public void setXlsConverterManager(XlsConverterManager xlsConverterManager) {
		this.xlsConverterManager = xlsConverterManager;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public String getCharge() {
		return charge;
	}
	public void setCharge(String charge) {
		this.charge = charge;
	}
	public String getContractName() {
		return contractName;
	}
	public void setContractName(String contractName) {
		this.contractName = contractName;
	}
	public List getRateGridIds() {
		return rateGridIds;
	}
	public void setRateGridIds(List rateGridIds) {
		this.rateGridIds = rateGridIds;
	}
	public void setRateGridManager(RateGridManager rateGridManager) {
		this.rateGridManager = rateGridManager;
	}
	public String getTwoDGridUnit() {
		return twoDGridUnit;
	}
	public void setTwoDGridUnit(String twoDGridUnit) {
		this.twoDGridUnit = twoDGridUnit;
	}
	public String getFileExt() {
		return fileExt;
	}
	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}
	public String getHitFlag() {
		return hitFlag;
	}
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	public Boolean getCostElementFlag() {
		return costElementFlag;
	}
	public void setCostElementFlag(Boolean costElementFlag) {
		this.costElementFlag = costElementFlag;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public List getDefaultAccountlineList() {
		return defaultAccountlineList;
	}
	public void setDefaultAccountlineList(List defaultAccountlineList) {
		this.defaultAccountlineList = defaultAccountlineList;
	}
	public void setDefaultAccountLineManager(
			DefaultAccountLineManager defaultAccountLineManager) {
		this.defaultAccountLineManager = defaultAccountLineManager;
	}
	public String[] getDeleteTemplateId() {
		return deleteTemplateId;
	}
	public void setDeleteTemplateId(String[] deleteTemplateId) {
		this.deleteTemplateId = deleteTemplateId;
	}
	public String getCheckListAll() {
		return checkListAll;
	}
	public void setCheckListAll(String checkListAll) {
		this.checkListAll = checkListAll;
	}
	public String getMsgDisplay() {
		return msgDisplay;
	}
	public void setMsgDisplay(String msgDisplay) {
		this.msgDisplay = msgDisplay;
	}
}
