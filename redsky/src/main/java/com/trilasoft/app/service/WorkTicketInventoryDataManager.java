package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.WorkTicketInventoryData;

public interface WorkTicketInventoryDataManager extends GenericManager<WorkTicketInventoryData, Long> {

	public List getAllInventory(String sessionCorpID, String shipno);

	public List checkInventoryData(String corpID, String barcode,String shipNumber);

}
