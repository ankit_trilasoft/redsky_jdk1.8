package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="soadditionaldate")
public class SoAdditionalDate extends BaseObject{
	private Long id;
	private Long serviceOrderId;
	//For Origin Dates
	private Date outByDate;
	private Date prePackDate;
	private int prePackMen;
	private String prePackHrs;
	private Date wrapDay;
	private int wrapDayMen;
	private String wrapDayHrs;
	private Date preLoadDate;
	private int preLoadMen;
	private String preLoadHrs;
	private Date packDate;
	private int packDayMen;
	private String packDayHrs;
	private Date loadDate;
	private int loadDayMen;
	private String loadDayHrs;
	private String loadMonth;
	private String loadWeek;
	private Boolean loadElevatorAccess = new Boolean(false);
	private Boolean loadElevatorBooking = new Boolean(false);
	private String loadTimeFrom;
	private String loadTimeTo;
	// Origin Dates End
	// Destination Dates Start
	private Date possessionDate;
	private String possessionTime;
	private Date prefferedDelieveryDate;
	private Date ttgFromDate;
	private int ttgFromMen;
	private String ttgFromHrs;
	private Date guaranteedDeliveryDate;
	private int guaranteedDeliveryDateMen;
	private String guaranteedDeliveryDateHrs;
	private Date unpackUnwrapDate;
	private int unpackUnwrapDateMen;
	private String unpackUnwrapDateHrs;
	private Date debrisRemovalDate;
	private int debrisRemovalDateMen;
	private String debrisRemovalDateHrs;
	private Date ttgToDate;
	private int ttgToDateMen;
	private String ttgToDateHrs;
	private Date actualDeliveryDate;
	private int actualDeliveryDateMen;
	private String actualDeliveryDateHrs;
	private Date setUpDate;
	private int setUpDateMen;
	private String setUpDateHrs;
	private String deliveryMonth;
	private String deliveryWeek;
	private Boolean deliveryElevatorAccess = new Boolean(false);
	private Boolean deliveryElevatorBooking  = new Boolean(false);
	private String deliveryTimeFrom;
	private String deliveryTimeTo;
	//  Destination Dates End	
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private String corpID;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("serviceOrderId", serviceOrderId)
				.append("outByDate", outByDate)
				.append("prePackDate", prePackDate)
				.append("prePackMen", prePackMen)
				.append("prePackHrs", prePackHrs).append("wrapDay", wrapDay)
				.append("wrapDayMen", wrapDayMen)
				.append("wrapDayHrs", wrapDayHrs)
				.append("preLoadDate", preLoadDate)
				.append("preLoadMen", preLoadMen)
				.append("preLoadHrs", preLoadHrs).append("packDate", packDate)
				.append("packDayMen", packDayMen)
				.append("packDayHrs", packDayHrs).append("loadDate", loadDate)
				.append("loadDayMen", loadDayMen)
				.append("loadDayHrs", loadDayHrs)
				.append("loadMonth", loadMonth).append("loadWeek", loadWeek)
				.append("loadElevatorAccess", loadElevatorAccess)
				.append("loadElevatorBooking", loadElevatorBooking)
				.append("loadTimeFrom", loadTimeFrom)
				.append("loadTimeTo", loadTimeTo)
				.append("possessionDate", possessionDate)
				.append("possessionTime", possessionTime)
				.append("prefferedDelieveryDate", prefferedDelieveryDate)
				.append("ttgFromDate", ttgFromDate)
				.append("ttgFromMen", ttgFromMen)
				.append("ttgFromHrs", ttgFromHrs)
				.append("guaranteedDeliveryDate", guaranteedDeliveryDate)
				.append("guaranteedDeliveryDateMen", guaranteedDeliveryDateMen)
				.append("guaranteedDeliveryDateHrs", guaranteedDeliveryDateHrs)
				.append("unpackUnwrapDate", unpackUnwrapDate)
				.append("unpackUnwrapDateMen", unpackUnwrapDateMen)
				.append("unpackUnwrapDateHrs", unpackUnwrapDateHrs)
				.append("debrisRemovalDate", debrisRemovalDate)
				.append("debrisRemovalDateMen", debrisRemovalDateMen)
				.append("debrisRemovalDateHrs", debrisRemovalDateHrs)
				.append("ttgToDate", ttgToDate)
				.append("ttgToDateMen", ttgToDateMen)
				.append("ttgToDateHrs", ttgToDateHrs)
				.append("actualDeliveryDate", actualDeliveryDate)
				.append("actualDeliveryDateMen", actualDeliveryDateMen)
				.append("actualDeliveryDateHrs", actualDeliveryDateHrs)
				.append("setUpDate", setUpDate)
				.append("setUpDateMen", setUpDateMen)
				.append("setUpDateHrs", setUpDateHrs)
				.append("deliveryMonth", deliveryMonth)
				.append("deliveryWeek", deliveryWeek)
				.append("deliveryElevatorAccess", deliveryElevatorAccess)
				.append("deliveryElevatorBooking", deliveryElevatorBooking)
				.append("deliveryTimeFrom", deliveryTimeFrom)
				.append("deliveryTimeTo", deliveryTimeTo)
				.append("createdOn", createdOn).append("createdBy", createdBy)
				.append("updatedOn", updatedOn).append("updatedBy", updatedBy)
				.append("corpID", corpID).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof SoAdditionalDate))
			return false;
		SoAdditionalDate castOther = (SoAdditionalDate) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(outByDate, castOther.outByDate)
				.append(prePackDate, castOther.prePackDate)
				.append(prePackMen, castOther.prePackMen)
				.append(prePackHrs, castOther.prePackHrs)
				.append(wrapDay, castOther.wrapDay)
				.append(wrapDayMen, castOther.wrapDayMen)
				.append(wrapDayHrs, castOther.wrapDayHrs)
				.append(preLoadDate, castOther.preLoadDate)
				.append(preLoadMen, castOther.preLoadMen)
				.append(preLoadHrs, castOther.preLoadHrs)
				.append(packDate, castOther.packDate)
				.append(packDayMen, castOther.packDayMen)
				.append(packDayHrs, castOther.packDayHrs)
				.append(loadDate, castOther.loadDate)
				.append(loadDayMen, castOther.loadDayMen)
				.append(loadDayHrs, castOther.loadDayHrs)
				.append(loadMonth, castOther.loadMonth)
				.append(loadWeek, castOther.loadWeek)
				.append(loadElevatorAccess, castOther.loadElevatorAccess)
				.append(loadElevatorBooking, castOther.loadElevatorBooking)
				.append(loadTimeFrom, castOther.loadTimeFrom)
				.append(loadTimeTo, castOther.loadTimeTo)
				.append(possessionDate, castOther.possessionDate)
				.append(possessionTime, castOther.possessionTime)
				.append(prefferedDelieveryDate,
						castOther.prefferedDelieveryDate)
				.append(ttgFromDate, castOther.ttgFromDate)
				.append(ttgFromMen, castOther.ttgFromMen)
				.append(ttgFromHrs, castOther.ttgFromHrs)
				.append(guaranteedDeliveryDate,
						castOther.guaranteedDeliveryDate)
				.append(guaranteedDeliveryDateMen,
						castOther.guaranteedDeliveryDateMen)
				.append(guaranteedDeliveryDateHrs,
						castOther.guaranteedDeliveryDateHrs)
				.append(unpackUnwrapDate, castOther.unpackUnwrapDate)
				.append(unpackUnwrapDateMen, castOther.unpackUnwrapDateMen)
				.append(unpackUnwrapDateHrs, castOther.unpackUnwrapDateHrs)
				.append(debrisRemovalDate, castOther.debrisRemovalDate)
				.append(debrisRemovalDateMen, castOther.debrisRemovalDateMen)
				.append(debrisRemovalDateHrs, castOther.debrisRemovalDateHrs)
				.append(ttgToDate, castOther.ttgToDate)
				.append(ttgToDateMen, castOther.ttgToDateMen)
				.append(ttgToDateHrs, castOther.ttgToDateHrs)
				.append(actualDeliveryDate, castOther.actualDeliveryDate)
				.append(actualDeliveryDateMen, castOther.actualDeliveryDateMen)
				.append(actualDeliveryDateHrs, castOther.actualDeliveryDateHrs)
				.append(setUpDate, castOther.setUpDate)
				.append(setUpDateMen, castOther.setUpDateMen)
				.append(setUpDateHrs, castOther.setUpDateHrs)
				.append(deliveryMonth, castOther.deliveryMonth)
				.append(deliveryWeek, castOther.deliveryWeek)
				.append(deliveryElevatorAccess,
						castOther.deliveryElevatorAccess)
				.append(deliveryElevatorBooking,
						castOther.deliveryElevatorBooking)
				.append(deliveryTimeFrom, castOther.deliveryTimeFrom)
				.append(deliveryTimeTo, castOther.deliveryTimeTo)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(corpID, castOther.corpID).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(serviceOrderId)
				.append(outByDate).append(prePackDate).append(prePackMen)
				.append(prePackHrs).append(wrapDay).append(wrapDayMen)
				.append(wrapDayHrs).append(preLoadDate).append(preLoadMen)
				.append(preLoadHrs).append(packDate).append(packDayMen)
				.append(packDayHrs).append(loadDate).append(loadDayMen)
				.append(loadDayHrs).append(loadMonth).append(loadWeek)
				.append(loadElevatorAccess).append(loadElevatorBooking)
				.append(loadTimeFrom).append(loadTimeTo).append(possessionDate)
				.append(possessionTime).append(prefferedDelieveryDate)
				.append(ttgFromDate).append(ttgFromMen).append(ttgFromHrs)
				.append(guaranteedDeliveryDate)
				.append(guaranteedDeliveryDateMen)
				.append(guaranteedDeliveryDateHrs).append(unpackUnwrapDate)
				.append(unpackUnwrapDateMen).append(unpackUnwrapDateHrs)
				.append(debrisRemovalDate).append(debrisRemovalDateMen)
				.append(debrisRemovalDateHrs).append(ttgToDate)
				.append(ttgToDateMen).append(ttgToDateHrs)
				.append(actualDeliveryDate).append(actualDeliveryDateMen)
				.append(actualDeliveryDateHrs).append(setUpDate)
				.append(setUpDateMen).append(setUpDateHrs)
				.append(deliveryMonth).append(deliveryWeek)
				.append(deliveryElevatorAccess).append(deliveryElevatorBooking)
				.append(deliveryTimeFrom).append(deliveryTimeTo)
				.append(createdOn).append(createdBy).append(updatedOn)
				.append(updatedBy).append(corpID).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	public Date getOutByDate() {
		return outByDate;
	}
	public void setOutByDate(Date outByDate) {
		this.outByDate = outByDate;
	}
	public Date getPrePackDate() {
		return prePackDate;
	}
	public void setPrePackDate(Date prePackDate) {
		this.prePackDate = prePackDate;
	}
	public int getPrePackMen() {
		return prePackMen;
	}
	public void setPrePackMen(int prePackMen) {
		this.prePackMen = prePackMen;
	}
	public String getPrePackHrs() {
		return prePackHrs;
	}
	public void setPrePackHrs(String prePackHrs) {
		this.prePackHrs = prePackHrs;
	}
	public Date getWrapDay() {
		return wrapDay;
	}
	public void setWrapDay(Date wrapDay) {
		this.wrapDay = wrapDay;
	}
	public int getWrapDayMen() {
		return wrapDayMen;
	}
	public void setWrapDayMen(int wrapDayMen) {
		this.wrapDayMen = wrapDayMen;
	}
	public String getWrapDayHrs() {
		return wrapDayHrs;
	}
	public void setWrapDayHrs(String wrapDayHrs) {
		this.wrapDayHrs = wrapDayHrs;
	}
	public Date getPreLoadDate() {
		return preLoadDate;
	}
	public void setPreLoadDate(Date preLoadDate) {
		this.preLoadDate = preLoadDate;
	}
	public int getPreLoadMen() {
		return preLoadMen;
	}
	public void setPreLoadMen(int preLoadMen) {
		this.preLoadMen = preLoadMen;
	}
	public String getPreLoadHrs() {
		return preLoadHrs;
	}
	public void setPreLoadHrs(String preLoadHrs) {
		this.preLoadHrs = preLoadHrs;
	}
	public Date getPackDate() {
		return packDate;
	}
	public void setPackDate(Date packDate) {
		this.packDate = packDate;
	}
	public int getPackDayMen() {
		return packDayMen;
	}
	public void setPackDayMen(int packDayMen) {
		this.packDayMen = packDayMen;
	}
	public String getPackDayHrs() {
		return packDayHrs;
	}
	public void setPackDayHrs(String packDayHrs) {
		this.packDayHrs = packDayHrs;
	}
	public Date getLoadDate() {
		return loadDate;
	}
	public void setLoadDate(Date loadDate) {
		this.loadDate = loadDate;
	}
	public int getLoadDayMen() {
		return loadDayMen;
	}
	public void setLoadDayMen(int loadDayMen) {
		this.loadDayMen = loadDayMen;
	}
	public String getLoadDayHrs() {
		return loadDayHrs;
	}
	public void setLoadDayHrs(String loadDayHrs) {
		this.loadDayHrs = loadDayHrs;
	}
	public String getLoadMonth() {
		return loadMonth;
	}
	public void setLoadMonth(String loadMonth) {
		this.loadMonth = loadMonth;
	}
	public String getLoadWeek() {
		return loadWeek;
	}
	public void setLoadWeek(String loadWeek) {
		this.loadWeek = loadWeek;
	}
	public Boolean getLoadElevatorAccess() {
		return loadElevatorAccess;
	}
	public void setLoadElevatorAccess(Boolean loadElevatorAccess) {
		this.loadElevatorAccess = loadElevatorAccess;
	}
	public Boolean getLoadElevatorBooking() {
		return loadElevatorBooking;
	}
	public void setLoadElevatorBooking(Boolean loadElevatorBooking) {
		this.loadElevatorBooking = loadElevatorBooking;
	}
	public String getLoadTimeFrom() {
		return loadTimeFrom;
	}
	public void setLoadTimeFrom(String loadTimeFrom) {
		this.loadTimeFrom = loadTimeFrom;
	}
	public String getLoadTimeTo() {
		return loadTimeTo;
	}
	public void setLoadTimeTo(String loadTimeTo) {
		this.loadTimeTo = loadTimeTo;
	}
	public Date getPossessionDate() {
		return possessionDate;
	}
	public void setPossessionDate(Date possessionDate) {
		this.possessionDate = possessionDate;
	}
	public String getPossessionTime() {
		return possessionTime;
	}
	public void setPossessionTime(String possessionTime) {
		this.possessionTime = possessionTime;
	}
	public Date getPrefferedDelieveryDate() {
		return prefferedDelieveryDate;
	}
	public void setPrefferedDelieveryDate(Date prefferedDelieveryDate) {
		this.prefferedDelieveryDate = prefferedDelieveryDate;
	}
	public Date getTtgFromDate() {
		return ttgFromDate;
	}
	public void setTtgFromDate(Date ttgFromDate) {
		this.ttgFromDate = ttgFromDate;
	}
	public int getTtgFromMen() {
		return ttgFromMen;
	}
	public void setTtgFromMen(int ttgFromMen) {
		this.ttgFromMen = ttgFromMen;
	}
	public String getTtgFromHrs() {
		return ttgFromHrs;
	}
	public void setTtgFromHrs(String ttgFromHrs) {
		this.ttgFromHrs = ttgFromHrs;
	}
	public Date getGuaranteedDeliveryDate() {
		return guaranteedDeliveryDate;
	}
	public void setGuaranteedDeliveryDate(Date guaranteedDeliveryDate) {
		this.guaranteedDeliveryDate = guaranteedDeliveryDate;
	}
	public int getGuaranteedDeliveryDateMen() {
		return guaranteedDeliveryDateMen;
	}
	public void setGuaranteedDeliveryDateMen(int guaranteedDeliveryDateMen) {
		this.guaranteedDeliveryDateMen = guaranteedDeliveryDateMen;
	}
	public String getGuaranteedDeliveryDateHrs() {
		return guaranteedDeliveryDateHrs;
	}
	public void setGuaranteedDeliveryDateHrs(String guaranteedDeliveryDateHrs) {
		this.guaranteedDeliveryDateHrs = guaranteedDeliveryDateHrs;
	}
	public Date getUnpackUnwrapDate() {
		return unpackUnwrapDate;
	}
	public void setUnpackUnwrapDate(Date unpackUnwrapDate) {
		this.unpackUnwrapDate = unpackUnwrapDate;
	}
	public int getUnpackUnwrapDateMen() {
		return unpackUnwrapDateMen;
	}
	public void setUnpackUnwrapDateMen(int unpackUnwrapDateMen) {
		this.unpackUnwrapDateMen = unpackUnwrapDateMen;
	}
	public String getUnpackUnwrapDateHrs() {
		return unpackUnwrapDateHrs;
	}
	public void setUnpackUnwrapDateHrs(String unpackUnwrapDateHrs) {
		this.unpackUnwrapDateHrs = unpackUnwrapDateHrs;
	}
	public Date getDebrisRemovalDate() {
		return debrisRemovalDate;
	}
	public void setDebrisRemovalDate(Date debrisRemovalDate) {
		this.debrisRemovalDate = debrisRemovalDate;
	}
	public int getDebrisRemovalDateMen() {
		return debrisRemovalDateMen;
	}
	public void setDebrisRemovalDateMen(int debrisRemovalDateMen) {
		this.debrisRemovalDateMen = debrisRemovalDateMen;
	}
	public String getDebrisRemovalDateHrs() {
		return debrisRemovalDateHrs;
	}
	public void setDebrisRemovalDateHrs(String debrisRemovalDateHrs) {
		this.debrisRemovalDateHrs = debrisRemovalDateHrs;
	}
	public Date getTtgToDate() {
		return ttgToDate;
	}
	public void setTtgToDate(Date ttgToDate) {
		this.ttgToDate = ttgToDate;
	}
	public int getTtgToDateMen() {
		return ttgToDateMen;
	}
	public void setTtgToDateMen(int ttgToDateMen) {
		this.ttgToDateMen = ttgToDateMen;
	}
	public String getTtgToDateHrs() {
		return ttgToDateHrs;
	}
	public void setTtgToDateHrs(String ttgToDateHrs) {
		this.ttgToDateHrs = ttgToDateHrs;
	}
	public Date getActualDeliveryDate() {
		return actualDeliveryDate;
	}
	public void setActualDeliveryDate(Date actualDeliveryDate) {
		this.actualDeliveryDate = actualDeliveryDate;
	}
	public int getActualDeliveryDateMen() {
		return actualDeliveryDateMen;
	}
	public void setActualDeliveryDateMen(int actualDeliveryDateMen) {
		this.actualDeliveryDateMen = actualDeliveryDateMen;
	}
	public String getActualDeliveryDateHrs() {
		return actualDeliveryDateHrs;
	}
	public void setActualDeliveryDateHrs(String actualDeliveryDateHrs) {
		this.actualDeliveryDateHrs = actualDeliveryDateHrs;
	}
	public Date getSetUpDate() {
		return setUpDate;
	}
	public void setSetUpDate(Date setUpDate) {
		this.setUpDate = setUpDate;
	}
	public int getSetUpDateMen() {
		return setUpDateMen;
	}
	public void setSetUpDateMen(int setUpDateMen) {
		this.setUpDateMen = setUpDateMen;
	}
	public String getSetUpDateHrs() {
		return setUpDateHrs;
	}
	public void setSetUpDateHrs(String setUpDateHrs) {
		this.setUpDateHrs = setUpDateHrs;
	}
	public String getDeliveryMonth() {
		return deliveryMonth;
	}
	public void setDeliveryMonth(String deliveryMonth) {
		this.deliveryMonth = deliveryMonth;
	}
	public String getDeliveryWeek() {
		return deliveryWeek;
	}
	public void setDeliveryWeek(String deliveryWeek) {
		this.deliveryWeek = deliveryWeek;
	}
	public Boolean getDeliveryElevatorAccess() {
		return deliveryElevatorAccess;
	}
	public void setDeliveryElevatorAccess(Boolean deliveryElevatorAccess) {
		this.deliveryElevatorAccess = deliveryElevatorAccess;
	}
	public Boolean getDeliveryElevatorBooking() {
		return deliveryElevatorBooking;
	}
	public void setDeliveryElevatorBooking(Boolean deliveryElevatorBooking) {
		this.deliveryElevatorBooking = deliveryElevatorBooking;
	}
	public String getDeliveryTimeFrom() {
		return deliveryTimeFrom;
	}
	public void setDeliveryTimeFrom(String deliveryTimeFrom) {
		this.deliveryTimeFrom = deliveryTimeFrom;
	}
	public String getDeliveryTimeTo() {
		return deliveryTimeTo;
	}
	public void setDeliveryTimeTo(String deliveryTimeTo) {
		this.deliveryTimeTo = deliveryTimeTo;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

}
