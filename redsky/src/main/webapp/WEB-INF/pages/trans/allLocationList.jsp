<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="locationList.title" /></title>
<meta name="heading" content="<fmt:message key='locationList.heading'/>" />


<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-10px;
!margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>
</head>
<c:set var="locationacbuttons">
<td style="border-left: hidden;"><s:submit cssClass="cssbutton" method="searchLocationForStorage" cssStyle="width:60px; height:25px;!margin-bottom: 10px;" key="button.search"/>
			    <input type="button" class="cssbutton" value="Clear" style="width:60px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/></td> 
			    </c:set>
<s:form id="locationacList" name="locationacList" action='${empty param.popup?"searchLocationStorage.html":"searchLocationStorage.html?decorator=popup&popup=true"}' method="post">
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<div id="Layer1" style="width:100%">
 <div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">	
	<table class="table" width="98%">
		<thead>
			<tr>
			    <th><fmt:message key="location.warehouse" /></th>
			    <th><fmt:message key="location.locationId" /></th>
				<th><fmt:message key="location.type" /></th>
			</tr>
		</thead>
		<tbody>
			<tr>
			    <td><s:select name="whouse" cssClass="list-menu" cssStyle="width:80px" list="%{house}" headerKey=""  headerValue=""/></td>
			    <td><s:textfield name="locationId1" required="true" cssClass="text medium" /></td>
				<td><s:select name="locationType" cssClass="list-menu" cssStyle="width:162px" list="%{locTYPE}" headerKey=""  headerValue=""/></td>
			</tr>
			<tr>
				<td colspan="2"></td>
				<c:out value="${locationacbuttons}" escapeXml="false" />
			</tr>
		</tbody>
	</table>
 	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	

	<div id="Layer5" style="width:100%;">
	<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Location List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	<s:set name="allLocations" value="allLocations" scope="request" />
	<display:table name="allLocations" class="table" requestURI="" id="locationlibList" export="${empty param.popup}" defaultsort="2" pagesize="10" style="width:99%; margin-left:5px;margin-top: 2px;!margin-top:-5px;" 
		decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="location.locationId" style="width:50px"/>   
    </c:if> 	
	<display:column property="warehouse" sortable="true" titleKey="location.warehouse" style="width:50px"/>
	<display:column property="type" sortable="true" titleKey="location.type" style="width:50px"/>
	<display:column headerClass="containeralign"  property="capacity" sortable="true"	title="Holding Capacity" style="width:50px; text-align: right"/>
	<display:column headerClass="containeralign" sortable="true" title="Capacity Cbm" style="width:80px; text-align: right"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
     groupingUsed="true" value="${locationlibList.cubicMeter}" /></div></display:column>
	<display:column headerClass="containeralign" sortable="true" title="Utilized Cbm" style="width:80px; text-align: right"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
     groupingUsed="true" value="${locationlibList.utilizedVolCbm}" /></div></display:column>
		
    <display:setProperty name="export.excel.filename" value="Location List.xls"/>
	<display:setProperty name="export.csv.filename" value="Location List.csv"/>
	</display:table>
	
	</div>
	</div>
</s:form>
<script language="javascript" type="text/javascript">
	function clear_fields()
	{  		    	
		document.forms['locationacList'].elements['locationType'].value = "";
		document.forms['locationacList'].elements['whouse'].value = "";
	    document.forms['locationacList'].elements['locationId1'].value ="";
	}
</script>
<script type="text/javascript"> 
    highlightTableRows("locationacList"); 
</script>
