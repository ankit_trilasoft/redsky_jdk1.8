package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.DsCrossCulturalTrainingDao;
import com.trilasoft.app.model.DsCrossCulturalTraining;

public class DsCrossCulturalTrainingDaoHibernate extends GenericDaoHibernate<DsCrossCulturalTraining, Long> implements DsCrossCulturalTrainingDao{

	public DsCrossCulturalTrainingDaoHibernate() {
		super(DsCrossCulturalTraining.class);
		}
	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsCrossCulturalTraining where id = "+id+" ");
	}
}
