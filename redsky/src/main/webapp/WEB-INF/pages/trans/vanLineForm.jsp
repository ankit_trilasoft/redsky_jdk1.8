<%@ include file="/common/taglibs.jsp"%> 
<head>
	<meta name="heading" content="<fmt:message key='vanLineForm.title'/>"/> 
	<title><fmt:message key="vanLineForm.title"/></title>
    <style type="text/css"></style> 	
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<script language="JavaScript" type="text/javascript">
//alert("hi load....");
var chkComp;
var chks='${chkcategorypage}';
//alert(chks);
if(chks!=null && chks=='close'){
	//alert("enter.....")
	opener.location.reload();
	window.close();
}

</script>

	<script language="JavaScript" type="text/javascript">
	function checkAgent()
	{
		var agent=document.forms['vanLineForm'].elements['vanLine.agent'].value;
		var the_char = agent.charAt(0);
		var s = document.forms['vanLineForm'].elements['vanLine.agent'].value;
		var count = 0;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if(c == "\'")
	        {
	        	count = count+1
	        }
	     }
	    if(count != 0)
		{
			alert("Do not add ' on agency#");
			document.forms['vanLineForm'].elements['vanLine.agent'].value=""; 
			return false;
		}
		
		if(the_char==':')
		{
			alert("Do not add ':' at first char on agency#");
			document.forms['vanLineForm'].elements['vanLine.agent'].value=""; 
			return false;
		}
		return true;
	}
	function onlyAlphaNumericAllowed(evt)
	{
		//alert(evt.keyCode);
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || 
	  (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || 
	  (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || 
	  (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110)|| (keyCode==18); 
	  
	}
	</script>	
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->

<script>

function setDriverName(partnerCode){
	//alert(partnerCode);
	 var url="ownerLastNameByCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+partnerCode;
	    
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponseForOwnerDetail;
     http2.send(null);
}
function handleHttpResponseForOwnerDetail()
{ 

     if (http2.readyState == 4)
     {
        var results = http2.responseText
        results = results.trim();
       // alert(results);
        document.forms['vanLineForm'].elements['vanLine.driverName'].value=results;
     }
}

function openAccountPopWindow(){
	//var persId = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value;
	//alert("hiiiii");
	javascript:openWindow('vanLineDriverList.html?&partnerType=OO&decorator=popup&popup=true&fld_description=vanLine.driverName&fld_code=vanLine.driverID&fld_eigthDescription=subcontractorCharges.vlCode&fld_seventhDescription=seventhDescription&fld_ninthDescription=ninthDescription&fld_tenthDescription=tenthDescription&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription');
	}
function newWindow(mypage,myname,w,h,features) {
  var winl = (screen.width-w)/2;
  var wint = (screen.height-h)/2;
  if (winl < 0) winl = 0;
  if (wint < 0) wint = 0;
  var settings = 'height=' + h + ',';
  settings += 'width=' + w + ',';
  settings += 'top=' + wint + ',';
  settings += 'left=' + winl + ',';
  settings += features;
  win = window.open(mypage,myname,settings);
  win.window.focus();
}

function saveAuto(clickType){ 

          var agent=document.forms['vanLineForm'].elements['vanLine.agent'].value; 
          var weekEnding=document.forms['vanLineForm'].elements['vanLine.weekEnding'].value ; 
          var reconcileStatusValidate="";
          <c:if test="${vanLine.reconcileStatus=='Approved'}"> 
          reconcileStatusValidate='Approved';
          </c:if>
          if(reconcileStatusValidate!='Approved'){
        	  var saveValidation = checkReconcileStatusValidation('save'); 
          }
          if(reconcileStatusValidate!='Approved' && saveValidation){
     if(!(clickType == 'save')){
     if ('${autoSavePrompt}' == 'No'){
    	var noSaveAction;
		if(document.forms['vanLineForm'].elements['gotoPageString'].value == 'gototab.vanline'){
               noSaveAction ="vanLineDateList.html?agent="+agent+"&weekEnding="+weekEnding; 
        }
		processAutoSave(document.forms['vanLineForm'],'saveSattlement!saveOnTabChange.html', noSaveAction );

    } 
    else{
       var id = document.forms['vanLineForm'].elements['vanLine.id'].value;
       if (document.forms['vanLineForm'].elements['formStatus'].value == '1'){
           var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='vanLineForm.title'/>");
               if(agree){
                   document.forms['vanLineForm'].action = 'saveSattlement!saveOnTabChange.html';
                   document.forms['vanLineForm'].submit();
               }else{
                   if(id != ''){
                          if(document.forms['vanLineForm'].elements['gotoPageString'].value == 'gototab.vanline'){
                         location.href ="vanLineDateList.html?agent="+agent+"&weekEnding="+weekEnding; 
                           }

                       }
                   }
               }else{
               if(id != ''){
                             if(document.forms['vanLineForm'].elements['gotoPageString'].value == 'gototab.vanline'){
                          location.href ="vanLineDateList.html?agent="+agent+"&weekEnding="+weekEnding; 
                           }
                   }
               }
        }
    }
  }else{
	  location.href ="vanLineDateList.html?agent="+agent+"&weekEnding="+weekEnding;
  }
}

function changeStatus(){
   document.forms['vanLineForm'].elements['formStatus'].value = '1';
}

function checkSplitCharges()
{
    var vid=document.forms['vanLineForm'].elements['vanLine.id'].value;  
	var amtDueAgent=document.forms['vanLineForm'].elements['vanLine.amtDueAgent'].value;
    var reconcileStatus=document.forms['vanLineForm'].elements['vanLine.reconcileStatus'].value;
    <c:if test="${vanLine.reconcileStatus!='Approved'}"> 
       alert("Please save reconcile status as approved than click split charges."); 
      </c:if>
     <c:if test="${vanLine.reconcileStatus=='Approved'}">  
       //newWindow('splitCharge.html?id=${vanLine.id}&dueFormAmount=${vanLine.amtDueAgent}&decorator=popup&popup=true','','900','550');
       var chkComp11="";
            	 // alert("hii");
            	   <configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
            	  chkComp11=document.forms['vanLineForm'].elements['glCodeFlag'].value;
            	  </configByCorp:fieldVisibility>
       window.location ="splitCharge.html?SplitButton=yes&glCodeFlag="+chkComp11+"&id="+vid+"&dueFormAmount="+amtDueAgent  
     </c:if>
}

function checkChargeAllocation(){ 
     var reconcileStatus=document.forms['vanLineForm'].elements['vanLine.reconcileStatus'].value;
     <c:if test="${vanLine.reconcileStatus!='Approved'}"> 
       alert("Please save reconcile status as approved then click Charge Allocation.");
     </c:if>
     <c:if test="${vanLine.reconcileStatus=='Approved'}"> 
     
	 var id=document.forms['vanLineForm'].elements['vanLine.id'].value;  
	 var amtDueAgent=document.forms['vanLineForm'].elements['vanLine.amtDueAgent'].value; 
	 var url="checkchargeAllocation.html?ajax=1&decorator=simple&popup=true&id="+id;
    
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
	</c:if>
	}
	function handleHttpResponse2()
        { 
        var vid=document.forms['vanLineForm'].elements['vanLine.id'].value;  
	    var amtDueAgent=document.forms['vanLineForm'].elements['vanLine.amtDueAgent'].value;
         
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();  
             if(results>0)
             { 
            	//alert("hiii");
            	 var chkComp11="";
            	 // alert("hii");
            	   <configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
            	  chkComp11=document.forms['vanLineForm'].elements['glCodeFlag'].value;
            	  </configByCorp:fieldVisibility>
             window.location ="editVanLine.html?id="+vid+"&glCodeFlag="+chkComp11 
             
             }
             
             if(results==0)
             { 
            	 var chkComp111="";
            	 //alert("hii");
            	   <configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
            	  chkComp111=document.forms['vanLineForm'].elements['glCodeFlag'].value;
            	  </configByCorp:fieldVisibility>
             window.location ="chargeAllocationUpdate.html?btntype=yes&glCodeFlag="+chkComp111+"&vid="+vid+"&dueFormAmount="+amtDueAgent+'&jspPageFlag=vanLineForm'; 
             
             }
             
             }
             
             
        }

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
String.prototype.lntrim = function() {
    return this.replace(/^\s+/,"","\n");
}

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var httpSop=getHTTPObject();
    function ownerOperatorListInDomestic()
    {
            //document.forms['vanLineForm'].elements['validateFlagDriver'].value='OK'; 
            var partnerCode=document.forms['vanLineForm'].elements['vanLine.driverID'].value;
    	    var url="findByOwnerListMiscellaneous.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode);
    	    httpSop.open("GET", url, true);
    	    httpSop.onreadystatechange = handleHttpResponseSop;
    	    httpSop.send(null);
      
    }

    function handleHttpResponseSop(){
    		if (httpSop.readyState == 4){
                    var results = httpSop.responseText
                    results = results.trim();
                    results=results.replace("[","")
                    var res=results.replace("]","")  
                    if(res.length>2){
                    var result=res.split("#");
               				document.forms['vanLineForm'].elements['vanLine.driverName'].value = result[0]+" "+result[1];
               				//document.forms['vanLineForm'].elements['miscellaneous.driverCell'].value = result[2];
               				//validateListForDriver();
    	           		}else{
    	           			alert("Driver ID is not valid." ); 
    					    document.forms['vanLineForm'].elements['vanLine.driverName'].value="";
    					    document.forms['vanLineForm'].elements['vanLine.driverID'].value="";
    				 	   //document.forms['vanLineForm'].elements['miscellaneous.driverCell'].value="";
    	           		}
                   	}
           }
</script> 
<script language="JavaScript">
function calculateAmtDue(){
    var formAmt=document.forms['vanLineForm'].elements['dueFormAmount'].value ;
	var totalAmt=document.forms['vanLineForm'].elements['totalAmount'].value ;
	var balanceAmt=formAmt-totalAmt;
	var balanceAmtRound=Math.round(balanceAmt*100)/100;
	document.forms['vanLineForm'].elements['balanceAmount'].value=balanceAmtRound; 
	}
	
function hidsplitButton(){
<c:if test="${subcontractorChargesExt!='[]'|| SplitButton=='yes'}"> 
//document.forms['vanLineForm'].elements['Split_Charges'].disabled=true;
document.forms['vanLineForm'].elements['Charge_Allocation'].disabled=true;  
 </c:if>

}
function onlyNumberAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39); 
	} 
function checkAccountLineNumber() { 
    var accountLineNumber=eval(document.forms['vanLineForm'].elements['vanLine.accountLineNumber'].value); 
    var accountLineNumber1=document.forms['vanLineForm'].elements['vanLine.accountLineNumber'].value;
    if(accountLineNumber1.length<2)  { 
    	document.forms['vanLineForm'].elements['vanLine.accountLineNumber'].value='00'+document.forms['vanLineForm'].elements['vanLine.accountLineNumber'].value;
    }
    else if(accountLineNumber1.length==2)  { 
    	var aa = document.forms['vanLineForm'].elements['vanLine.accountLineNumber'].value;
    	document.forms['vanLineForm'].elements['vanLine.accountLineNumber'].value='0'+aa;
    }  }

function checkReconcileStatusValidation(temp)
{
	var statementCategory="";
	<c:if test="${not empty vanLine.id}">
	statementCategory='${vanLine.statementCategory}'
	</c:if>
	<c:if test="${empty vanLine.id}">
	statementCategory=document.forms['vanLineForm'].elements['vanLine.statementCategory'].value;
	</c:if>
	 
if(document.forms['vanLineForm'].elements['vanLine.reconcileStatus'].value=='Approved' && statementCategory=='MCLM'){
	var driverID=document.forms['vanLineForm'].elements['vanLine.driverID'].value; 
 
	driverID=driverID.trim();  
	if(driverID=='' && temp=='save'){
		var header = '\t\t\t\t\tWarning';
		var type = 'No Charge back to the driver will be applied.';
		var msg = 'Click Ok to save or Cancel to change the value.';
		var agree = confirm(header+"\n"+type+"\n"+msg);
		 //alert("No Charge back to the driver will be applied");
		 //document.forms['vanLineForm'].elements['vanLine.reconcileStatus'].value='${vanLine.reconcileStatus}'
		if(agree){
     		return true;
     	}else{  	   		
     		return false;	
     	} 
	}
	return true;
}else{return true;}
}
</script>	
<style type="text/css">


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;}

</style>

</head>
<body style="background-color:#444444;">
<s:form id="vanLineForm" name="vanLineForm" action="saveSattlement" method="post" validate="true" >
<s:hidden name="secondDescription" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription"/>
	<s:hidden name="sixthDescription" />
	<s:hidden name="firstDescription" />
	<s:hidden name="seventhDescription" />
	<s:hidden name="eigthDescription" />
	<s:hidden name="ninthDescription" />
	<s:hidden name="tenthDescription" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="vanLine.id" value="%{vanLine.id}"/>
<s:hidden name="id" value="%{vanLine.id}"/>
<configByCorp:fieldVisibility componentId="component.field.Alternative.Division">
	<s:hidden name="divisionFlag" value="YES"/>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
	<s:hidden name="glCodeFlag" value="YES"/>
</configByCorp:fieldVisibility>
<c:choose>
<c:when test="${chkcategorypage!=null && chkcategorypage=='yes'}">
<s:hidden name="chkcategorypage" value="close"/>
</c:when>
<c:otherwise>
<s:hidden name="chkcategorypage" value="open"/>
</c:otherwise>
</c:choose>
<div id="Layer1" style="width:100%" onkeydown="changeStatus();" >

<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<s:hidden name="SplitButton" value="<%=request.getParameter("SplitButton") %>"/>
    <c:set var="SplitButton"  value="<%=request.getParameter("SplitButton") %>"/>
<s:text id="batchDateFormattedValue" name="${FormDateValue}">
<s:param name="value" value="vanLine.weekEnding"/></s:text>
<s:hidden id="batchDate1" cssClass="input-text" name="vanLine.weekEnding1" value="%{batchDateFormattedValue}" />


<c:if test="${not empty vanLine.uploaded}">
	<s:text id="vanLineuploadedFormattedValue" name="${FormDateValue}"> <s:param name="value" value="vanLine.uploaded" /></s:text>
	<s:hidden  name="vanLine.uploaded" value="%{vanLineuploadedFormattedValue}" /> 
</c:if>
<c:if test="${empty vanLine.uploaded}">
	<s:hidden   name="vanLine.uploaded"/> 
</c:if>
<c:if test="${not empty vanLine.accCreatedOn}">
		 <s:text id="accCreatedOnValue" name="${FormDateValue}"> <s:param name="value" value="vanLine.accCreatedOn" /></s:text>
			 <s:hidden  name="vanLine.accCreatedOn" value="%{accCreatedOnValue}" /> 
	 </c:if>
	 <c:if test="${empty vanLine.accCreatedOn}">
		 <s:hidden   name="vanLine.accCreatedOn"/> 
	 </c:if> 			
<c:if test="${validateFormNav =='OK'}" >
	<c:choose>
		<c:when test="${gotoPageString == 'gototab.vanline'}">
		<script type="text/javascript"> 
			var weekEnding=document.forms['vanLineForm'].elements['vanLine.weekEnding1'].value ; 
			//alert(weekEnding);
			if(weekEnding=='null')
			{
			window.location ="vanLineDateList.html?agent=${vanLine.agent}&weekEnding="+'';
			}
			else{
			window.location ="vanLineDateList.html?agent=${vanLine.agent}&weekEnding="+weekEnding; 
			}
		</script> 
		</c:when> 
		<c:otherwise>
		</c:otherwise>
	</c:choose> 
</c:if>
<div id="newmnav">
		  <ul>
		  		<li><a onclick="setReturnString('gototab.vanline');return saveAuto('none');" onmouseover="return checkAgent();"><span>Settlement List</span></a></li>
			    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Order Settlement Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
</div><div class="spn" style="!margin-bottom:0px;">&nbsp;</div>

	<div id="content" align="center" >
				<div id="liquid-round">
   			<div class="top"><span></span></div>
   				<div class="center-content">
 <table class="" cellspacing="0" cellpadding="0" border="0" style="" >
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  	
  	<table class="detailTabLabel" border="0">
		  <tbody>  	
		  
		  	<tr>
		  		<td align="left" height="5px"></td>
		  	</tr>
		  	<tr>
		  	<td align="right" width="30px"></td>
		  	<td align="right">Category</td>
		  	<%-- <s:select cssClass="list-menu" name="vanLine.statementCategory" headerKey="" headerValue="" list="%{categoryList}" cssStyle="width:270px"/> --%>
		  	<c:if test="${not empty vanLine.id}">
		  	<td align="left" colspan="2">
		  	<s:select cssClass="list-menu" name="vanLine.statementCategory"  headerKey="" headerValue="" list="%{categoryList}" cssStyle="width:270px" disabled="true"/>
		   <s:hidden name="vanLine.statementCategory"></s:hidden>
		  	<%-- <s:textfield  name="vanLine.statementCategory"   cssClass="input-textUpper" cssStyle="width:140px" readonly="readonly"/> --%>
		  	</td></c:if>
		  	<c:if test="${empty vanLine.id}">
		  			<td align="left" colspan="2"><s:select cssClass="list-menu" name="vanLine.statementCategory" headerKey="" headerValue="" list="%{categoryList}" cssStyle="width:270px"/></td>
		  		</c:if>
		  	<td align="right" width="90px" >Driver ID</td>
		  	<td>
		  	<table cellspacing="0" cellpadding="0" border="0" style="margin:0px;padding:0px;">
		  	<tr>
		  	<td align="left" width="100px"><s:textfield name="vanLine.driverID"  maxlength="25" cssClass="input-text" cssStyle="width:70px" onchange="setDriverName(document.forms['vanLineForm'].elements['vanLine.driverID'].value),ownerOperatorListInDomestic();"/><img class="openpopup" height="20" width="17" align="top" src="images/open-popup.gif" onclick="openAccountPopWindow();"></td>
		  	<td>Driver Name&nbsp;</td>
		  	<td align="left" width="150px"><s:textfield name="vanLine.driverName"  maxlength="25" cssClass="input-textUpper" readonly="true"  cssStyle="width:130px"/></td>
		  	</tr>
		  	</table>
		  	</td>
		  	</tr>		  
		  	<tr>
		  		<td align="right" width="30px"></td>
		  	   	<td align="right" width=""><fmt:message key="vanLine.linea"/></td>
		  	   	<c:if test="${not empty vanLine.id}">
		  			<td align="left" width="150px"><s:textfield name="vanLine.lineNumber"  maxlength="25" size="20"   cssClass="input-textUpper" cssStyle="width:140px" readonly="true"/></td>
		  		</c:if>
		  		<c:if test="${empty vanLine.id}">
		  			<td align="left" width="150px"><s:textfield name="vanLine.lineNumber"  maxlength="25" size="20"   cssClass="input-text" cssStyle="width:140px"/></td>
		  		</c:if>
		  		<td align="center" width="120px">Actg. Agency</td>			
		  	   
		  	   	<td align="right" width="90px"><fmt:message key="vanLine.weekEnding"/><font color="red" size="2">*</font></td>		  	   	
		  	    <c:if test="${not empty vanLine.id}">
		  	    	<c:if test="${not empty vanLine.weekEnding}">
						<s:text id="batchDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="vanLine.weekEnding"/></s:text>
						<td><s:textfield id="batchDate" cssClass="input-textUpper" name="vanLine.weekEnding" value="%{batchDateFormattedValue}" cssStyle="width:70px" maxlength="11" readonly="true"/></td>
					</c:if>
					<c:if test="${empty vanLine.weekEnding}">
						<td><s:textfield id="batchDate" cssClass="input-textUpper" name="vanLine.weekEnding" cssStyle="width:70px" maxlength="11" readonly="true" /></td>
					</c:if>	
		  	    </c:if>
		  	    <c:if test="${empty vanLine.id}">
			  	   	<c:if test="${not empty vanLine.weekEnding}">
						<s:text id="batchDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="vanLine.weekEnding"/></s:text>
						<td><s:textfield id="batchDate" cssClass="input-text" name="vanLine.weekEnding" value="%{batchDateFormattedValue}" cssStyle="width:70px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/><img id="batchDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty vanLine.weekEnding}">
						<td><s:textfield id="batchDate" cssClass="input-text" name="vanLine.weekEnding" cssStyle="width:70px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/><img id="batchDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>	
				</c:if>		  	   	
		  		<td align="right" width="30px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px"></td>
		  	   	<td align="right"><fmt:message key="vanLine.agencya"/><font color="red" size="2">*</font></td>
		  	   	<c:if test="${not empty vanLine.id}">
		  			<td align="left"><s:textfield name="vanLine.agent"  maxlength="15" size="20"   cssClass="input-textUpper" cssStyle="width:140px" onkeydown="return onlyAlphaNumericAllowed(event)" readonly="true"/></td>
		  		</c:if>
		  		<c:if test="${empty vanLine.id}">
		  			<td align="left"><s:textfield name="vanLine.agent"  maxlength="15" size="20"   cssClass="input-text" cssStyle="width:140px" onkeydown="return onlyAlphaNumericAllowed(event)"/></td>
		  		</c:if>
		  		<td align="center"><s:select cssClass="list-menu" name="vanLine.accountingAgent" headerKey="" headerValue="" list="%{vanLineCodeList}" cssStyle="width:110px"/></td>
		  	   	<td align="right"><fmt:message key="vanLine.registrationa"/></td>
		  	   	<c:if test="${not empty vanLine.id}">
		  			<td align="left"><s:textfield name="vanLine.regNum"  maxlength="20" size="20"   cssClass="input-textUpper" cssStyle="width:140px" readonly="true"/></td>
		  		</c:if>
		  		<c:if test="${empty vanLine.id}">
		  			<td align="left"><s:textfield name="vanLine.regNum"  maxlength="20" size="20"   cssClass="input-text" cssStyle="width:140px"/></td>
		  		</c:if>	
		  	</tr>
		  	<c:if test="${ownbillingVanline}">
		  	<tr>
		  	<td align="right" width="30px"></td>
		  	<td align="right" class="listwhitetext" >Bill To Code</td>
		  	<td align="left" class="listwhitetext" nowrap="nowrap"><s:textfield  cssClass="input-textUpper" name="vanLine.billToCode" onselect="changeStatus();" onblur="changeStatus();" onchange="changeStatus();" size="8" maxlength="17" readonly="true" />
			 <img align="top"  class="openpopup" width="17" height="20" onclick="javascript:openWindow('findVanLineBillToCode.html?vanRegNum=${vanLine.regNum}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=vanLine.billToName&fld_code=vanLine.billToCode');document.forms['vanLineForm'].elements['vanLine.billToCode'].select();" id="openpopup8.img" src="<c:url value='/images/open-popup.gif'/>" />		  	
			 <td></td>
			 <td align="right" class="listwhitetext" >Bill To Name</td>
		  	<td align="left" colspan="2"><s:textfield cssClass="input-textUpper" name="vanLine.billToName" cssStyle="width:272px"  readonly="true"  /></td>
		  	</tr>
		  	</c:if>	
		  	 <tr>
		  	<td align="right" width="30px"></td>
		  	<td align="right" class="listwhitetext" >Company&nbsp;Division</td>
            <td><s:select name="vanLine.companyDivision" list="%{companyDivis}" cssStyle="width:141px"  onchange="changeStatus();" headerKey="" headerValue="" cssClass="list-menu"/></td>
		  	<td align="right" width="10px"></td>
		  	<td align="right" class="listwhitetext" >AccountLine&nbsp;#</td>
		  	<td align="left"><s:textfield name="vanLine.accountLineNumber"  maxlength="3" size="20"   cssClass="input-text" cssStyle="width:140px" onkeydown="return onlyNumberAllowed(event);" onchange="checkAccountLineNumber();" /></td>
		  	</tr>
		  	<configByCorp:fieldVisibility componentId="component.field.subcontractorCharges.validation">
		  	<tr> 
		  	  <td align="right" width="30px"></td>
              <td align="right" class="listwhitetext" >Division</td>
              <td><s:select cssClass="list-menu" list="%{division}" name="vanLine.division" headerKey="00" headerValue="" cssStyle="width:60px" /></td>
             </tr> 
            </configByCorp:fieldVisibility> 	
		  	<tr>
		  		<td align="right" width="30px"></td>
		  		<td align="right"><fmt:message key="vanLine.shipper"/></td>
		  		<td align="left"><s:textfield name="vanLine.shipper"  maxlength="25" size="20"   cssClass="input-text" cssStyle="width:140px"/></td>	  		 	   		  			  			  		
		  		
		  		<td align="right" width="10px"></td>	
		  	   	<td align="right"><fmt:message key="vanline.datetran"/></td>
		  	   	<c:if test="${not empty vanLine.id}">
		  	   		<c:if test="${not empty vanLine.tranDate}">
						<s:text id="batchDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="vanLine.tranDate"/></s:text>
						<td><s:textfield id="batchDateSend" cssClass="input-textUpper" name="vanLine.tranDate" value="%{batchDateFormattedValue}" cssStyle="width:70px" maxlength="11" readonly="true"/></td>
					</c:if>
					<c:if test="${empty vanLine.tranDate}">
						<td><s:textfield id="batchDateSend" cssClass="input-textUpper" name="vanLine.tranDate" cssStyle="width:70px" maxlength="11" readonly="true" /></td>
					</c:if>	
		  	   	</c:if>	  	
		  	   	<c:if test="${empty vanLine.id}">   	
			  	   	<c:if test="${not empty vanLine.tranDate}">
						<s:text id="batchDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="vanLine.tranDate"/></s:text>
						<td><s:textfield id="batchDateSend" cssClass="input-text" name="vanLine.tranDate" value="%{batchDateFormattedValue}" cssStyle="width:70px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/><img id="batchDateSend_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty vanLine.tranDate}">
						<td><s:textfield id="batchDateSend" cssClass="input-text" name="vanLine.tranDate" cssStyle="width:70px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/><img id="batchDateSend_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>	
				</c:if>	  	   	
		  		<td align="right" width="30px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px"></td>
		  	    <td align="right" ><fmt:message key="vanLine.reconcileStatus"/></td>
		  	    <td align="left" class="listwhitetext" valign="top"><s:select cssClass="list-menu"   name="vanLine.reconcileStatus" list="{'','Reconcile','Suspense','Approved','Dispute'}" cssStyle="width:141px" onchange="changeStatus();"/></td><td align="right" width="30px">		  	    	  	    
		  		<td align="right"><fmt:message key="vanLine.descriptiona"/></td>
		  			<c:if test="${empty vanLine.id}">  
		  		<c:choose>
		  		<c:when test="${vanLine.description!='' && vanLine.description!=null}">
		  		<td align="left"><s:textfield name="vanLine.description"  maxlength="225" size="20"  cssClass="input-text" cssStyle="width:273px"/></td>
		  	    </c:when>
		  	    <c:otherwise>
		  	    <td align="left"><s:textfield name="vanLine.distributionCodeDescription"  maxlength="225" size="20"  cssClass="input-text" cssStyle="width:273px"/></td>
		  	    </c:otherwise>
		  	    </c:choose>
		  	    </c:if>
		  	    <c:if test="${not empty vanLine.id}">
		  	    <c:choose>
		  		<c:when test="${vanLine.description!='' && vanLine.description!=null}">
		  		<td align="left"><s:textfield name="vanLine.description"  maxlength="225" size="20"  cssClass="input-textUpper" cssStyle="width:273px" readonly="true"/></td>
		  	    </c:when>
		  	    <c:otherwise>
		  	    <td align="left"><s:textfield name="vanLine.distributionCodeDescription"  maxlength="225" size="20"  cssClass="input-textUpper" cssStyle="width:273px" readonly="true"/></td>
		  	    </c:otherwise>
		  	    </c:choose>
		  	    </c:if>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px"></td>
		  	   	<td align="right"><fmt:message key="vanLine.reconcileAmount"/></td>
		  		<td align="left"><s:textfield name="vanLine.reconcileAmount"  maxlength="25" size="20"   cssClass="input-text" cssStyle="width:140px;text-align:right"/></td>
		  		
		  		<!--<td align="right" width="30px">
		  	   	<td align="right"><fmt:message key="vanLine.dueHqa"/></td>
		  		<td align="left"><s:textfield name="vanLine.amtDueHq"  maxlength="25" size="20"   cssClass="input-text" cssStyle="width:110px"/></td>
		  		
		  		-->
		  		<td align="right" width="30px"></td>
		  		 <td align="right" ><fmt:message key="vanLine.glCodea"/></td>
		  		<td align="left" class="listwhitetext" valign="top"><s:select cssClass="list-menu"   name="vanLine.glCode" headerKey=" " headerValue=" " list="%{glCode}" cssStyle="width:275px" onchange="changeStatus();"/></td> 
		
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px"></td>
		  	   	<td align="right"><fmt:message key="vanLine.dueAgenta"/></td>
		  		<td align="left"><s:textfield name="vanLine.amtDueAgent"  maxlength="25" size="20"   cssClass="input-text"  cssStyle="width:140px;text-align:right"/></td>
		  	
		  		
		  		<td align="right" width="30px"></td>
		  	    <td align="right" >Charge Code</td>
		  		<td align="left"><s:select cssClass="list-menu" name="vanLine.glType" headerKey=" " headerValue=" " list="%{chargeCodeList}" cssStyle="width:275px" onchange="changeStatus();"/></td> 
		  	</tr>
		  
		  	
		  	<tr>
		  		
		  		<td align="left" height="4px"></td>
		  		<td align="right" valign="top"><fmt:message key="vanLine.notesa"/></td>		  		
		  		<td align="left" colspan="3" valign="top"><s:textarea cssClass="textarea" label="Description" name="vanLine.notes" cols="44" rows="2"  /></td>
		  		<td>
		  		<table class="detailtabLabel" cellspacing="1" cellpadding="0" border="0"> 
		  		
		   <!--	<tr> 
		  	<td align="left" valign="top">        		
       			<input type="button"  class="cssbuttonA" style="width: 120px;height:25px" value="Split Charges" name="Split_Charges" onclick="checkSplitCharges();"/>
				</td></tr>-->
				 
				<tr>
				<td align="left" style="padding-top:3px;">  
				<input type="button" class="cssbuttonA"  style="width: 120px;height:25px"  value="Charge Allocation" name="Charge_Allocation" onclick="checkChargeAllocation();"/>
				</td>
        	</tr>  
        	</table>
		  		</td>
		  	</tr> 
		  	
		  	<tr>
		  		<td align="left" width="30px">
		  		
		  		
		  	</tr>
		  	</table>
		  	</td>
		  	</tr>
		  
		  	<!--<table>
		  	<c:if test="${not empty vanLine.regNum}">
		  	<tr>	
		  	  	<td align="right" width="80px">	
       			<td align="left" width="40px">        		
       			<input type="button" class="cssbutton1" style="width:110px;" value="Show Line Details" onClick="newWindow('/pages/trans/activeUsers.jsp','','650','250')" /> 
				<input type="button"  class="cssbutton1" style="width: 120px" value="Show Line Details" onclick="newWindow('showLineDetail.html?regNum=${vanLine.regNum}&decorator=popup&popup=true','','width=750,height=400');"/>
				
        		
	        	</td>            	
        	</tr>
        	</c:if>
        	</table>
        	
        	<table> 
		  	<tr>	
		  	  	<td align="right" width="80px">	
       			<td align="left" width="40px">        		
       			<input type="button" class="cssbutton1" style="width:110px;" value="Show Line Details" onClick="newWindow('/pages/trans/activeUsers.jsp','','650','250')" /> 
				<input type="button"  class="cssbuttonA" style="width: 120px;height:25px" value="Split Charges" onclick="checkSplitCharges();"/>
				</td>            	
				<td align="right" width="80px">	
       			<td align="left" width="40px">        		
       			<input type="button" class="cssbutton1" style="width:110px;" value="Show Line Details" onClick="newWindow('/pages/trans/activeUsers.jsp','','650','250')" /> 
				<input type="button"  class="cssbutton1" style="width: 120px"  value="Charge Allocation" onclick="newWindow('chargeAllocation.html?id=${vanLine.id}&decorator=popup&popup=true','','900','550');"/>
				</td>
				<td align="left" width="40px">        		
       			<input type="button" class="cssbutton1" style="width:110px;" value="Show Line Details" onClick="newWindow('/pages/trans/activeUsers.jsp','','650','250')" /> 
				<input type="button" class="cssbuttonA"  style="width: 120px;height:25px"  value="Charge Allocation" onclick="checkChargeAllocation();"/>
				</td>
        	</tr> 
        	</table>-->
        	
        	
<c:if test="${subcontractorChargesExt!='[]'|| SplitButton=='yes'}"> 
 <table class="" cellspacing="0" cellpadding="1" border="0" style="width:100%">
 <tbody>
   <tr>
	 <td> 
    <c:set var="FormDateValue1" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat1" name="dateFormat1" value="dd-NNN-yy"/>
    <s:hidden name="formStatus" /> 
    <c:out value="${searchresults}" escapeXml="false" />  
       <div id="otabs" style="margin-bottom:0px;">
		  <ul>
		    <li><a class="current"><span>Subcontractor Charges List</span></a></li>
		  </ul>
	  </div>
	  <div class="spn" style="!margin-bottom:2px;">&nbsp;</div> 
      <script type="text/javascript">      
      function setUrl(aref,id11){
    	  chkComp="";
    	  <configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
    	  chkComp=document.forms['vanLineForm'].elements['glCodeFlag'].value;
    	  </configByCorp:fieldVisibility>
    	 // var id11='${subcontractorChargesList1.id}';
    	 // alert(chkComp);
    	 // alert(id11);
    	  aref.href='editSplitSubcontractor.html?id='+id11+'&dueFormAmount=${dueFormAmount}&glCodeFlag='+chkComp+'&jspPageFlag=vanLineForm';
    	   
      }
     
      
      </script>
	  <s:set name="subcontractorChargesExt" value="subcontractorChargesExt" scope="request"/>
      <display:table name="subcontractorChargesExt" class="table" requestURI="" id="subcontractorChargesList1" export="false" defaultsort="1" pagesize="10" style="width:100%;margin-bottom:5px;">   
      <display:column sortable="true" titleKey="subcontractorCharges.personIdList" >
		<a onclick="setUrl(this,${subcontractorChargesList1.id})" href="#">
			<c:if test="${subcontractorChargesList1.personId=='' || subcontractorChargesList1.personId==null}">
				<img class="openpopup" height="20" width="100%" align="top" src="images/blank.png">
			</c:if>
	 		<c:if test="${subcontractorChargesList1.personId!='' && subcontractorChargesList1.personId!=null}">
				<c:out value="${subcontractorChargesList1.personId}" />
			</c:if>
		</a>
	 </display:column> 
      <display:column property="personType" sortable="true" titleKey="subcontractorCharges.personTypeList"/>
      <display:column sortable="true" titleKey="subcontractorCharges.amount">
      <div align="right"><fmt:formatNumber type="number" value="${subcontractorChargesList1.amount}"  minFractionDigits="2" maxFractionDigits="2" groupingUsed="true"/></div>
      </display:column>
      <display:column  sortable="true" titleKey="subcontractorCharges.branch">
      <div align="right"><c:out value="${subcontractorChargesList1.branch}" /></div>
      </display:column>
      <display:column property="description" sortable="true" titleKey="subcontractorCharges.description"/>
      <display:column property="regNumber" sortable="true" titleKey="subcontractorCharges.regNumber"/>
      <display:column property="approved" title="Approved" format="{0,date,dd-MMM-yyyy}"/>
     <display:column property="accountSent" title="Sent Ac" format="{0,date,dd-MMM-yyyy}"/>  
      <display:setProperty name="paging.banner.item_name" value="container"/>  
      <display:setProperty name="paging.banner.items_name" value="container"/> 
     <display:setProperty name="export.excel.filename" value="Container List.xls"/>   
     <display:setProperty name="export.csv.filename" value="Container List.csv"/>   
     <display:setProperty name="export.pdf.filename" value="Container List.pdf"/>  
     <display:footer>
			<tr>  
 	  	           <td align="right"><b><div align="right"><fmt:message key="subcontractorCharges.totalAmount"/></div></b></td>
 	  	           <td></td>
		           <td align="left"  width="70px"><div align="right">
		           <fmt:formatNumber type="number"  minFractionDigits="2" maxFractionDigits="2" groupingUsed="true" value="${totalAmount}" /> 
                   </div>
                   </td>
                   <td colspan="5"></td> 
		  	</tr> 
	</display:footer> 
</display:table>
<s:hidden  name="vid" value="<%=request.getParameter("id") %>"/>
<c:set var="vid"  value="<%=request.getParameter("id") %>"/>
<s:hidden name="dueFormAmount" value="<%=request.getParameter("dueFormAmount") %>"/>
<c:set var="dueFormAmount"  value="<%=request.getParameter("dueFormAmount") %>"/>
<s:hidden name="totalAmount" value="${totalAmount}"/>
  <table class="detailTabLabel" border="0" cellpadding="1" cellspacing="0">
	   <tr> 
		   <td  width="135px"> </td>
		   <td align="right" style="font-size:13px;" class="listwhitetext"><b>Balance:</b></td>
		   <td align="left"   class="balanceBox"><s:textfield cssClass="balanceBox" name="balanceAmount"  readonly="true" size="7" cssStyle="text-align:right"/></td>
		   <td width="15px"></td>
		   <c:if test="${not empty vanLine.id}">
		   <td> <input type="button" class="cssbuttonA" style="width:75px; height:25px"   onclick="setUrlForAddSplitBtn(this,${vid},${dueFormAmount });" value="Add Split" /></td>
	      </c:if>
	   </tr>
   </table>   
  </td>
  </tr>
  </tbody>
  </table>
 </c:if>
	<tr>
		 <td align="left" height="0px"></td>
	</tr>
	</tbody>
	</table> 

	
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>	 
 	<table class="detailTabLabel" border="0">	
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='vanLine.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${vanLine.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="vanLine.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${vanLine.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='vanLine.createdBy' /></b></td>
							<c:if test="${not empty vanLine.id}">
								<s:hidden name="vanLine.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{vanLine.createdBy}"/></td>
							</c:if>
							<c:if test="${empty vanLine.id}">
								<s:hidden name="vanLine.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='vanLine.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${vanLine.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="vanLine.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${vanLine.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='vanLine.updatedBy' /></b></td>
							<c:if test="${not empty vanLine.id}">
								<s:hidden name="vanLine.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{vanLine.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty vanLine.id}">
								<s:hidden name="vanLine.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
		
<table border="0">
	<tbody> 	
		<tr>
		  	<td align="left" height="3px"></td>
		</tr>	  	
		<tr>
		<c:choose>
       	<c:when test="${saveValidateForm =='NotOk'}">
       	<td align="left"><s:submit cssClass="cssbuttonA" type="button" cssStyle="width:50px;height:25px" method="save" key="button.save" onmouseover="return checkAgent();"  onclick="return checkReconcileStatusValidation('save');"/></td>
       	</c:when>
       	<c:otherwise>
		<c:if test="${vanLine.reconcileStatus=='Approved'}">
		<td align="left"><input class="cssbuttonA" type="button" style="width:50px;height:25px" method="save" disabled="disabled" name="Save" value="Save" onmouseover="return checkAgent();" onclick="return checkReconcileStatusValidation('save');"  /></td>
		</c:if>
		<c:if test="${vanLine.reconcileStatus!='Approved'}">
		  	<td align="left"><s:submit cssClass="cssbuttonA" type="button" cssStyle="width:50px;height:25px" method="save" key="button.save" onmouseover="return checkAgent();" onclick="return checkReconcileStatusValidation('save');" /></td>
       	</c:if>	
       	</c:otherwise>
       	</c:choose>
       		<td align="right">
        		<c:if test="${not empty vanLine.id}">
        			<input type="button" class="cssbuttonA" style="width:50px;height:25px"  value="Add" onclick="setUrlForAddBtn(this);" /> 
        		 </c:if>
	        </td>
            <td align="right"><s:reset cssClass="cssbuttonA"   cssStyle="width:55px;height:25px"  type="button" key="Reset"/></td>
        </tr>		  	
	</tbody> 
  	</table>		
</div>
   <div id="mydiv" style="position:absolute;margin-top:-45px;" ></div>
</s:form>
<script type="text/javascript">
function setUrlForAddBtn(addbtnref){
	//alert("111");location.href='<c:url value="/editVanLine.html"/>'
	chkComp="";
	<configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
	 chkComp=document.forms['vanLineForm'].elements['glCodeFlag'].value;
	 </configByCorp:fieldVisibility>
	// alert(chkComp);
	 location.href='editVanLine.html?&glCodeFlag='+chkComp;	 
	  
}
function setUrlForAddSplitBtn(addbtnref,vid,amt){	
	chkComp="";
	<configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
	 chkComp=document.forms['vanLineForm'].elements['glCodeFlag'].value;
	 </configByCorp:fieldVisibility>
	 
	 location.href='editSplitSubcontractor.html?vid=${vid}&dueFormAmount=${dueFormAmount}&glCodeFlag='+chkComp+'&jspPageFlag=vanLineForm';
	   
}
</script>
<script type="text/javascript">
try{
calculateAmtDue();
}
catch(e){}
    highlightTableRows("subcontractorChargesList1");
    try{
    hidsplitButton();
    }
    catch(e){}
</script>

<script type="text/javascript">
    var myimage = document.getElementsByTagName("img"); 
   	for (var i = 0; i < myimage.length; i++) {
	   	var idinms=myimage[i].getAttribute('id');
	   	if(idinms != null && (idinms.split('_')[1]=="trigger")){
		   	var args1=idinms.split('_');
		   	RANGE_CAL_1 = new Calendar({
		           inputField: args1[0],
		           dateFormat: "%d-%b-%y",
		           trigger: idinms,
		           bottomBar: true,
		           animation:true,
		           onSelect: function() {                             
		               this.hide();
		       }
		   });
	   	}
   	}
</script>
		