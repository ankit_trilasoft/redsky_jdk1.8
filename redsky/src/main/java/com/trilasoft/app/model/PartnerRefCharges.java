package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name = "partnerrefcharges")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class PartnerRefCharges extends BaseObject{
	
	private Long id;
	private String corpID;
	private BigDecimal value;
	private String  tariffApplicability;
	private String parameter;
	private String countryCode;
	private String grid;
	private String  basis;
	private String label;
	
	
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("value", value).append("tariffApplicability",
				tariffApplicability).append("parameter", parameter).append(
				"countryCode", countryCode).append("grid", grid).append(
				"basis", basis).append("label", label).append("createdBy",
				createdBy).append("updatedBy", updatedBy).append("createdOn",
				createdOn).append("updatedOn", updatedOn).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PartnerRefCharges))
			return false;
		PartnerRefCharges castOther = (PartnerRefCharges) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(value, castOther.value).append(
				tariffApplicability, castOther.tariffApplicability).append(
				parameter, castOther.parameter).append(countryCode,
				castOther.countryCode).append(grid, castOther.grid).append(
				basis, castOther.basis).append(label, castOther.label).append(
				createdBy, castOther.createdBy).append(updatedBy,
				castOther.updatedBy).append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(value)
				.append(tariffApplicability).append(parameter).append(
						countryCode).append(grid).append(basis).append(label)
				.append(createdBy).append(updatedBy).append(createdOn).append(
						updatedOn).toHashCode();
	}
	@Column(length =25)
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	@Column(length = 10)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length = 5)
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	@Column(length = 50)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length = 50)
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	@Column(length = 50)
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	@Column(length = 30)
	public String getTariffApplicability() {
		return tariffApplicability;
	}
	public void setTariffApplicability(String tariffApplicability) {
		this.tariffApplicability = tariffApplicability;
	}
	@Column(length = 50)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length = 50)
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	@Column(length = 5)
	public String getGrid() {
		return grid;
	}
	public void setGrid(String grid) {
		this.grid = grid;
	}

}
