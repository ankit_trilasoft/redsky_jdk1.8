package com.trilasoft.app.model;

 
 
import javax.persistence.Entity;
 
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "companysystemdefault")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class CompanySystemDefault {

	private Long id;
	private String corpID; 
	private Boolean status;
	private Boolean cportalAccessCompanyDivisionLevel;
	private String timeZone;
	private String partnerActionOnExpiry;
	private String userActionOnExpiry;
	private Boolean restrictAccess; 
	private String restrictedMassage;
	private String companyName;
	private String landingPageWelcomeMsg;
	private String excludeIPs;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("corpID", corpID) 
				.append("id", id)
				.append("companyName", companyName) 
				.append("timeZone", timeZone) 
				.append("userActionOnExpiry", userActionOnExpiry) 
				.append("partnerActionOnExpiry", partnerActionOnExpiry) 
				.append("status",status)
				.append("restrictAccess",restrictAccess) 
				.append("cportalAccessCompanyDivisionLevel",cportalAccessCompanyDivisionLevel)
				.append("restrictedMassage",restrictedMassage)
				.append("landingPageWelcomeMsg",landingPageWelcomeMsg)
				.append("excludeIPs",excludeIPs) 
				.toString();
	}
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CompanySystemDefault))
			return false;
		CompanySystemDefault castOther = (CompanySystemDefault) other;
		return new EqualsBuilder()
				.append(corpID, castOther.corpID)
				.append(id, castOther.id)
				.append(companyName, castOther.companyName) 
				.append(timeZone, castOther.timeZone) 
				.append(userActionOnExpiry, castOther.userActionOnExpiry) 
				.append(partnerActionOnExpiry, castOther.partnerActionOnExpiry) 
				.append(cportalAccessCompanyDivisionLevel, castOther.cportalAccessCompanyDivisionLevel) 
		        .append(landingPageWelcomeMsg,castOther.landingPageWelcomeMsg) 
		        .append(status, castOther.status)
		        .append(restrictAccess, castOther.restrictAccess) 
		        .append(restrictedMassage, castOther.restrictedMassage)
		        .append(excludeIPs, castOther.excludeIPs)
                .isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID)
				.append(id)
				.append(companyName) 
				.append(timeZone)
				.append(userActionOnExpiry) 
				.append(partnerActionOnExpiry) 
				.append(cportalAccessCompanyDivisionLevel) 
		        .append(landingPageWelcomeMsg) 
		        .append(status)
		        .append(restrictAccess)
		        .append(restrictedMassage)
		        .append(excludeIPs) 
                .toHashCode();
	} 
	
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Boolean getCportalAccessCompanyDivisionLevel() {
		return cportalAccessCompanyDivisionLevel;
	}
	public void setCportalAccessCompanyDivisionLevel(
			Boolean cportalAccessCompanyDivisionLevel) {
		this.cportalAccessCompanyDivisionLevel = cportalAccessCompanyDivisionLevel;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public String getPartnerActionOnExpiry() {
		return partnerActionOnExpiry;
	}
	public void setPartnerActionOnExpiry(String partnerActionOnExpiry) {
		this.partnerActionOnExpiry = partnerActionOnExpiry;
	}
	public String getUserActionOnExpiry() {
		return userActionOnExpiry;
	}
	public void setUserActionOnExpiry(String userActionOnExpiry) {
		this.userActionOnExpiry = userActionOnExpiry;
	}
	public Boolean getRestrictAccess() {
		return restrictAccess;
	}
	public void setRestrictAccess(Boolean restrictAccess) {
		this.restrictAccess = restrictAccess;
	}
	public String getRestrictedMassage() {
		return restrictedMassage;
	}
	public void setRestrictedMassage(String restrictedMassage) {
		this.restrictedMassage = restrictedMassage;
	}
	public String getExcludeIPs() {
		return excludeIPs;
	}
	public void setExcludeIPs(String excludeIPs) {
		this.excludeIPs = excludeIPs;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getLandingPageWelcomeMsg() {
		return landingPageWelcomeMsg;
	}
	public void setLandingPageWelcomeMsg(String landingPageWelcomeMsg) {
		this.landingPageWelcomeMsg = landingPageWelcomeMsg;
	}

	@Id 
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	 
	
	
}
