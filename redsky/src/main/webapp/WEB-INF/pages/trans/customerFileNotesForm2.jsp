<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title><fmt:message key="customerFileNotesForm.title" /></title>
<meta name="heading"
	content="<fmt:message key='customerFileNotesForm.heading'/>" />

<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
	
<style>
	<%@ include file="/common/calenderStyle.css"%>
</style>

</head>

<s:form id="customerFileNotesForm2" action="saveCustomerFileNotes"
	method="post" validate="true">
<s:hidden name="id" value="<%=request.getParameter("id") %>"/>	
	<s:hidden key="customerFileNotes.id"
		value="<%=request.getParameter("id") %>" />
	<s:hidden key="noteType"
		value="<%=request.getParameter("noteType") %>" />
	<s:hidden key="noteSubType"
		value="<%=request.getParameter("noteSubType") %>" />
	<s:hidden key="forwardStatus"
		value="<%=request.getParameter("forwardStatus") %>" />
		<s:hidden key="forwardToUser"
		value="<%=request.getParameter("forwardToUser") %>" />	
<s:hidden name="customerFile.id" value="<%=request.getParameter("id1") %>"/>
<s:hidden name="id1" value="<%=request.getParameter("id1") %>"/>
		
	<div id="Layer1">
	<table class="detailTabLabel" cellspacing="0" cellpadding="0"
		border="0">
		<tbody>
			<tr>
				<td width="4" align="right"><img width="4" height="20"
					src="<c:url value='/images/tab-left.jpg'/>" /></td>
				<td width="100" class="content-tab"><a href="editCustomerFile.html?id=<%=request.getParameter("id1") %> " >Customer File</a></td>
				<td width="4" align="left"><img width="4" height="20"
					src="<c:url value='/images/tab-right.jpg'/>" /></td>
				<td width="4"></td>
				<td width="7" align="right"><img width="6" height="20"
					src="<c:url value='/images/tab-left.jpg'/>" /></td>
				<td width="100" class="content-tab"><a href="customerServiceOrders.html?id=<%=request.getParameter("id1") %> " >Service Orders</a></td>
				<td width="7" align="left"><img width="6" height="20"
					src="<c:url value='/images/tab-right.jpg'/>" /></td>
				<td width="4"></td>
				<td width="7" align="right"><img width="6" height="20"
					src="<c:url value='/images/tab-left.jpg'/>" /></td>
				<td width="100" class="content-tab"><a href="quotes.html?id=<%=request.getParameter("id1") %> " >Quotations</a></td>
				<td width="7" align="left"><img width="6" height="20"
					src="<c:url value='/images/tab-right.jpg'/>" /></td>
				<td width="4"></td>
				<td width="7" align="right"><img width="6" height="20"
					src="<c:url value='/images/tab-left.jpg'/>" /></td>
				<td width="100" class="content-tab"><a href="surveysList.html">Surveys</a></td>
				<td width="7" align="left"><img width="6" height="20"
					src="<c:url value='/images/tab-right.jpg'/>" /></td>
				<td width="4"></td>
				<td width="7" align="right"><img width="7" height="20"
					src="<c:url value='/images/head-left.jpg'/>" /></td>
				<td width="100" class="detailActiveTabLabel content-tab"><a href="customerFileNotess.html?id=<%=request.getParameter("id1") %> " ><font color="white" >Notes</font></a></td>
				<td width="7" align="left"><img width="7" height="20"
					src="<c:url value='/images/head-right.jpg'/>" /></td>
				<td width="4"></td>
				<td width="7" align="right"><img width="6" height="20"
					src="<c:url value='/images/tab-left.jpg'/>" /></td>
				<td width="100" class="content-tab">Reports</td>
				<td width="7" align="left"><img width="6" height="20"
					src="<c:url value='/images/tab-right.jpg'/>" /></td>
				<td width="4"></td>
				<td width="7" align="right"><img width="6" height="20"
					src="<c:url value='/images/tab-left.jpg'/>" /></td>
				<td width="100" class="content-tab">Account Policy</td>
				<td width="7" align="left"><img width="6" height="20"
					src="<c:url value='/images/tab-right.jpg'/>" /></td>
				<td align="left"></td>
			</tr>
		</tbody>
	</table>
	<table class="mainDetailTable" cellspacing="1" cellpadding="0"
		border="0">
		<tbody>
			<tr>
				<td>
				<table>
					<tr>
						<td align="right" class="listwhite"><fmt:message
							key="customerFileNotes.notesId" /></td>
						<td align="left" class="listwhite" colspan="4"><s:textfield
							name="customerFile.sequenceNumber"
							value="<%=request.getParameter("notesId") %>" size="7"
							maxlength="15" readonly="true" onfocus="setComboValues();" /></td>
					</tr>
					<tr>
						<td align="right" class="listwhite"><fmt:message
							key="customerFileNotes.noteType" /></td>
						<td align="left" class="listwhite" colspan="4"><s:select name="customerFileNotes.noteType" list="%{notetype}" 
							value="<%=request.getParameter("noteType") %>"  /></td>
					</tr>
					<tr>
						<td align="right" class="listwhite"><fmt:message
							key="customerFileNotes.noteSubType" /></td>
						<td align="left" class="listwhite" colspan="4"><s:select name="customerFileNotes.noteSubType" list="%{notesubtype}" 
							value="<%=request.getParameter("noteSubType") %>"  headerKey="" headerValue=": Leave Empty"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhite"><fmt:message
							key="customerFileNotes.subject" /></td>
						<td align="left" class="listwhite" colspan="4"><s:textfield
							name="customerFileNotes.subject"
							value="<%=request.getParameter("subject") %>" size="70"
							maxlength="25" /></td>
					</tr>
					<tr>
						<td align="right" class="listwhite"><fmt:message
							key="customerFileNotes.note" /></td>
						<td align="left" class="listwhite" colspan="5"><s:textarea
							name="customerFileNotes.note"
							value="<%=request.getParameter("note") %>" cols="70" rows="6" /></td>
					</tr>
					<tr>
						<td align="right" class="listwhite"><fmt:message
							key="customerFileNotes.forwardToUser" /></td>
						<td align="left" class="listwhite"><s:select
							name="customerFileNotes.forwardToUser"
							list="%{all_user}" value="<%=request.getParameter("forwardToUser") %>"  headerKey="" headerValue=": Leave Empty" onchange="enableForwardBtn()"/></td>
						<td align="right" class="listwhite"><fmt:message
							key="customerFileNotes.forwardDate" /></td>
						<td align="right" class="listwhite"><s:textfield
							name="customerFileNotes.forwardDate"
							value="<%=request.getParameter("forwardDate") %>" size="20"
							maxlength="10" readonly="true" /></td>
						<td align="right" class="listwhite"><fmt:message
							key="customerFileNotes.forwardStatus" /></td>
						<td align="left" class="listwhite"><s:select name="customerFileNotes.forwardStatus" list="%{notestatus}" 
							value="FWD"  headerKey="NEW" headerValue="New"/></td>
					</tr>
				</table>
				<table>
					<tbody>
						<tr>
							<s:hidden name="customerFileNotes.createdOn" value="<%=request.getParameter("createdOn") %>"/>
							<s:hidden name="customerFileNotes.createdBy" value="<%=request.getParameter("createdBy") %>"/>
							<s:hidden name="customerFileNotes.updatedOn" value="<%=request.getParameter("updatedOn") %>"/>
							<s:hidden name="customerFileNotes.updatedBy" value="<%=request.getParameter("updatedBy") %>" />
							<td width="560px"></td>
							<td align="right" class="listwhite"><s:submit cssClass="button" method="save" key="button.save"/></td>
						</tr>
					</tbody>
				</table>							
				</td>
			</tr>
		</tbody>
	</table>
	</div>

<!-- 
    <li class="buttonBar bottom"> 
    	<s:submit cssClass="button" method="save" key="button.save"/>   
    	<c:if test="${not empty customerFileNotes.id}">    
            <s:submit cssClass="button" method="delete" key="button.delete" onclick="return confirmDelete('customerFileNotes')"/>   
        </c:if>   
        <s:submit cssClass="button" method="cancel" key="button.cancel"/>   
    </li>   
 -->
</s:form>
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td>
			<s:form id="forwardToUserForm"  action="saveForwardedMessage" method="post">
				<s:hidden key="id" value="%{customerFileNotes.id}" />
				<s:hidden key="id1" value="<%=request.getParameter("id1") %>" />
				<s:hidden key="myMessage.fromUser" />
				<s:hidden key="myMessage.toUser"/>
				<s:hidden key="myMessage.subject" />
				<s:hidden key="myMessage.message"  />
				<s:hidden key="myMessage.sentOn" />
				<s:hidden key="notesId" value="%{customerFileNotes.notesId}"/>
				<s:hidden key="noteSubType" value="%{customerFileNotes.noteSubType}"/>
				<s:hidden key="noteType" value="%{customerFileNotes.noteType}"/>
				<s:hidden key="subject"value="%{customerFileNotes.subject}"/>
				<s:hidden key="note"value="%{customerFileNotes.note}"/>
				<s:hidden key="createdBy"value="%{customerFileNotes.createdBy}"/>
				<s:hidden key="createdOn"value="%{customerFileNotes.createdOn}"/>
				<s:hidden key="updatedBy"value="%{customerFileNotes.updatedBy}"/>
				<s:hidden key="updatedOn"value="%{customerFileNotes.updatedOn}"/>
				<s:hidden key="forwardToUser" value="%{customerFileNotes.forwardToUser}"/>
				<s:hidden key="forwardDate"value="%{customerFileNotes.forwardDate}"/>
				<s:hidden key="forwardStatus"value="%{customerFileNotes.forwardStatus}"/>
				<input type="submit" name="forwardBtn" value="Forward" style="width:87px; height:30px" onclick="forwardToMyMessage();" />
			</s:form>   
		</td>
		<td><s:form id="mailToUserForm">
			<input type="button" value="Email" style="width:87px; height:30px" onclick="emailForm();" />
		</s:form></td>
		<td><s:form id="remindUserForm" action="editRemindToForm"
			method="post">
			<s:submit cssClass="button" key="button.remind" cssStyle="width:87px; height:30px"/>
		</s:form></td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript">
	function onFormLoad() {
		if(document.forms['customerFileNotesForm2'].elements['customerFileNotes.forwardDate'].value == "null" ){
			document.forms['customerFileNotesForm2'].elements['customerFileNotes.forwardDate'].value="";
		}
		if( document.forms['customerFileNotesForm2'].elements['customerFileNotes.forwardToUser'].value == "" ) {
			document.forms['forwardToUserForm'].elements['forwardBtn'].disabled = true ;
		}			
	}
</script>
<script type="text/javascript"> 
	function forwardToMyMessage(){ 
		document.forms['forwardToUserForm'].elements['myMessage.fromUser'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.updatedBy'].value;		
		document.forms['forwardToUserForm'].elements['myMessage.subject'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.subject'].value;		
		document.forms['forwardToUserForm'].elements['myMessage.message'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.note'].value;		
		document.forms['forwardToUserForm'].elements['myMessage.toUser'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.forwardToUser'].value; 
		document.forms['forwardToUserForm'].elements['myMessage.sentOn'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.updatedOn'].value;
		document.forms['forwardToUserForm'].elements['notesId'].value=document.forms['customerFileNotesForm2'].elements['customerFile.sequenceNumber'].value;
		document.forms['forwardToUserForm'].elements['noteSubType'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.noteSubType'].value;
		document.forms['forwardToUserForm'].elements['noteType'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.noteType'].value;
		document.forms['forwardToUserForm'].elements['subject'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.subject'].value;
		document.forms['forwardToUserForm'].elements['note'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.note'].value;
		document.forms['forwardToUserForm'].elements['createdBy'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.createdBy'].value;
		document.forms['forwardToUserForm'].elements['createdOn'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.createdOn'].value;
		document.forms['forwardToUserForm'].elements['updatedBy'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.updatedBy'].value;
		document.forms['forwardToUserForm'].elements['updatedOn'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.updatedOn'].value;
		document.forms['forwardToUserForm'].elements['forwardToUser'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.forwardToUser'].value;
		document.forms['forwardToUserForm'].elements['forwardDate'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.forwardDate'].value;
		document.forms['forwardToUserForm'].elements['forwardStatus'].value=document.forms['customerFileNotesForm2'].elements['customerFileNotes.forwardStatus'].value;
	}
</script>
<script language="JavaScript">
		function onlyNumsAllowed(evt){
		  var keyCode = evt.which ? evt.which : evt.keyCode;
		  return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
		}
		function onlyFloatNumsAllowed(evt){
		  var keyCode = evt.which ? evt.which : evt.keyCode;
		  return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
		}
		function onlyCharsAllowed(evt){
		  var keyCode = evt.which ? evt.which : evt.keyCode;
		  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222)  ; 
		}
</script>

<script type="text/javascript">
		function validate_email(field)
		{
		with (field)
		{
		apos=value.indexOf("@")
		dotpos=value.lastIndexOf(".")
		if (apos<1||dotpos-apos<2) 
		  {alert("Not a valid e-mail address!");return false}
		else {return true}
		}
		}
</script>

<script type="text/javascript"> 
		function emailForm(){ 
		
		var daReferrer = document.referrer; 
		var email = document.forms['customerFileNotesForm2'].elements['customerFileNotes.forwardToUser'].value; 
		var errorMsg = "here here here is the error error error error"; 
		var subject = document.forms['customerFileNotesForm2'].elements['customerFileNotes.subject'].value; 
		var name = document.forms['customerFileNotesForm2'].elements['customerFileNotes.createdBy'].value; 
		var formNotes = document.forms['customerFileNotesForm2'].elements['customerFileNotes.note'].value; 
		var body_message = "%0D%0DHi "+name+"%0D%0D "+formNotes; 
		
		var mailto_link = 'mailto:'+email+'?subject='+subject+'&body='+body_message; 
		
		win = window.open(mailto_link,'emailWindow'); 
		if (win && win.open &&!win.closed) win.close(); 
		
		} 
</script>
	
<script type="text/javascript"> 
	function setComboValues(){ 	
		document.forms['customerFileNotesForm2'].elements['customerFileNotes.noteType'].value=document.forms['customerFileNotesForm2'].elements['noteType'].value;
		document.forms['customerFileNotesForm2'].elements['customerFileNotes.noteSubType'].value=document.forms['customerFileNotesForm2'].elements['noteSubType'].value;
		document.forms['customerFileNotesForm2'].elements['customerFileNotes.forwardStatus'].value="FWD";
		document.forms['customerFileNotesForm2'].elements['customerFileNotes.forwardToUser'].value=document.forms['customerFileNotesForm2'].elements['forwardToUser'].value
	}
</script>

<script type="text/javascript">
	function enableForwardBtn() {
		if( document.forms['customerFileNotesForm2'].elements['customerFileNotes.forwardToUser'].value == "" ) {
				document.forms['forwardToUserForm'].elements['forwardBtn'].disabled = true ;
			}else {
				document.forms['forwardToUserForm'].elements['forwardBtn'].disabled = false ;
			}
	}
</script>

<script type="text/javascript">   
    Form.focusFirstElement($("customerFileNotesForm2"));   
</script>
