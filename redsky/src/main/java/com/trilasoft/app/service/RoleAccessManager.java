package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.RoleAccess;

public interface RoleAccessManager extends GenericManager<RoleAccess, Long> { 
	
	public List<RoleAccess> findByCorpID(String corpId);

}
