package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.UserGuidesDao;
import com.trilasoft.app.model.UserGuides;
import com.trilasoft.app.service.UserGuidesManager;



public class UserGuidesManagerImpl extends GenericManagerImpl<UserGuides, Long> implements UserGuidesManager
{
	UserGuidesDao userGuidesDao;
	public UserGuidesManagerImpl(UserGuidesDao userGuidesDao) {
		super(userGuidesDao);
		this.userGuidesDao=userGuidesDao;
		// TODO Auto-generated constructor stub
	}
	
		public List findListByModuleDocStatus(String module,String documentName,Boolean activeStatus){
			return userGuidesDao.findListByModuleDocStatus(module, documentName,activeStatus); 
		}

		public List findListByModule(String module,String corpId) {
			return userGuidesDao.findListByModule(module,corpId);
		}
		
	public List findDistinct() {
		return userGuidesDao.findDistinct();
	}
	 public List<UserGuides> findListByModuleAndDisplayOrder(String module,int displayOrder,Integer prevDisplayOrder){
		 return userGuidesDao.findListByModuleAndDisplayOrder(module,displayOrder,prevDisplayOrder);
	 }
	 
	 public List<String> findListByDistinctModule(String corpId){
		 return userGuidesDao.findListByDistinctModule(corpId);
	 }
	 public List<UserGuides> findListByDocumentName(String fileName,String UserType,Boolean cmmdmmAgent,Boolean vanlineEnabled,String corpId) {
		 return userGuidesDao.findListByDocumentName(fileName,UserType,cmmdmmAgent,vanlineEnabled,corpId);
	 }
	 public List findModuleCount(String corpId){
		 return userGuidesDao.findModuleCount(corpId);		 
		 
	 }
	 
}
