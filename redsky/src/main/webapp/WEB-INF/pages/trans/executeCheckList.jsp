<%@ include file="/common/taglibs.jsp"%> 


<head> 

<title><fmt:message key="executeRules.title"/></title> 

<meta name="heading" content="<fmt:message key='executeRules.heading'/>"/> 
</head> 

<s:form id="executeRules" name="executeRules" action="" method="post" validate="true">   
 <div id="Layer1">
<table class='searchLabel' cellspacing="2" cellpadding="4" border="0">
<tbody>
	<tr>
      <td align="right" class="listwhitetext" width="700px"><h1><b>The Check Lists have been executed successfully.</b></h1></td>
     </tr>
     <tr> 
      <td align="right" class="listwhitetext" width="700px"><font color="#726DB0" size="5"><b>Number of Rules Executed are ${countNumberOfRules}.</b></font></td>
     </tr>
     <tr> 
      <td align="right" class="listwhitetext" width="700px"><font color="#726DB0" size="5"><b>Number of Result Entered in Activity Management are ${countNumberOfResult}.</b></font></td>
     </tr>
</tbody></table>

</div>           
</s:form>


