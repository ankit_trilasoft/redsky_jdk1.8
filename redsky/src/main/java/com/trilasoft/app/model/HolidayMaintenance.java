package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="holidaymaintenance")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID") 

public class HolidayMaintenance extends BaseObject{
	private Long id;
	private String corpID;
	private String updatedBy;
	private String createdBy;
	private Date updatedOn;
	private Date createdOn;

	private Date holiDayDate;
	private Double holiDayRate;
	private String holiDayBranch;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("updatedBy", updatedBy)
				.append("createdBy", createdBy).append("updatedOn", updatedOn)
				.append("createdOn", createdOn)
				.append("holiDayDate", holiDayDate)
				.append("holiDayRate", holiDayRate)
				.append("holiDayBranch", holiDayBranch).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof HolidayMaintenance))
			return false;
		HolidayMaintenance castOther = (HolidayMaintenance) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(createdOn, castOther.createdOn)
				.append(holiDayDate, castOther.holiDayDate)
				.append(holiDayRate, castOther.holiDayRate)
				.append(holiDayBranch, castOther.holiDayBranch).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(updatedBy).append(createdBy).append(updatedOn)
				.append(createdOn).append(holiDayDate).append(holiDayRate)
				.append(holiDayBranch).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getHoliDayDate() {
		return holiDayDate;
	}
	public void setHoliDayDate(Date holiDayDate) {
		this.holiDayDate = holiDayDate;
	}
	@Column
	public Double getHoliDayRate() {
		return holiDayRate;
	}
	public void setHoliDayRate(Double holiDayRate) {
		this.holiDayRate = holiDayRate;
	}
	@Column
	public String getHoliDayBranch() {
		return holiDayBranch;
	}
	public void setHoliDayBranch(String holiDayBranch) {
		this.holiDayBranch = holiDayBranch;
	}

}
