package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name = "resourcecontractcharges")

public class ResourceContractCharges extends BaseObject {

	private Long id;
	
	private String contract;
	
	private String charge;
	
	private Long parentId;
	
	private String corpID;

	private String updatedBy;

	private String createdBy;

	private Date createdOn;

	private Date updatedOn;

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id).append("contract",contract).append("charge",charge).append("parentId",parentId)
				.append("corpID", corpID)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("createdBy", createdBy)
		        .append("createdOn",createdOn).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ResourceContractCharges))
			return false;
		ResourceContractCharges castOther = (ResourceContractCharges) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(contract, castOther.contract)
				.append(charge, castOther.charge)
				.append(parentId, castOther.parentId)
				.append(corpID, castOther.corpID)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(contract)
				.append(charge).append(parentId).append(corpID)
				.append(updatedBy)
				.append(createdBy)
				.append(createdOn).append(updatedOn)
				.toHashCode();
	}

	@Column(name="corpID", length = 10)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	@Column
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}
	@Column
	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}
	@Column
	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	

}
