package com.trilasoft.app.model;

import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="consigneeInstruction")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class ConsigneeInstruction extends BaseObject{
	private String corpID;
	private Long id;
	private Long serviceOrderId;
	private String sequenceNumber;
	private String ship;
	private String shipNumber;
	private String shipperAddress;
	private String consigneeAddress;
	private String consignmentInstructions;
	private String specialInstructions;
	private String notification;
	private String shipmentLocation;
	private Boolean notify;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn; 
	private Boolean expressRelease;
	private Boolean collect;
	private String ugwIntId;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("serviceOrderId", serviceOrderId).append(
				"sequenceNumber", sequenceNumber).append("ship", ship).append(
				"shipNumber", shipNumber).append("shipperAddress",
				shipperAddress).append("consigneeAddress", consigneeAddress)
				.append("consignmentInstructions", consignmentInstructions)
				.append("specialInstructions", specialInstructions).append(
						"notification", notification).append(
						"shipmentLocation", shipmentLocation).append("notify",
						notify).append("createdBy", createdBy).append(
						"updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("expressRelease",
						expressRelease).append("collect", collect).append("ugwIntId",ugwIntId).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ConsigneeInstruction))
			return false;
		ConsigneeInstruction castOther = (ConsigneeInstruction) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(serviceOrderId, castOther.serviceOrderId)
				.append(sequenceNumber, castOther.sequenceNumber).append(ship,
						castOther.ship)
				.append(shipNumber, castOther.shipNumber).append(
						shipperAddress, castOther.shipperAddress).append(
						consigneeAddress, castOther.consigneeAddress).append(
						consignmentInstructions,
						castOther.consignmentInstructions).append(
						specialInstructions, castOther.specialInstructions)
				.append(notification, castOther.notification).append(
						shipmentLocation, castOther.shipmentLocation).append(
						notify, castOther.notify).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(expressRelease,
						castOther.expressRelease).append(collect,
						castOther.collect)
						.append(ugwIntId, castOther.ugwIntId).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(
				serviceOrderId).append(sequenceNumber).append(ship).append(
				shipNumber).append(shipperAddress).append(consigneeAddress)
				.append(consignmentInstructions).append(specialInstructions)
				.append(notification).append(shipmentLocation).append(notify)
				.append(createdBy).append(updatedBy).append(createdOn).append(
						updatedOn).append(expressRelease).append(collect).append(ugwIntId)
				.toHashCode();
	}
	/**
	 * @return the corpID
	 */
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	/**
	 * @param corpID the corpID to set
	 */
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	/**
	 * @return the id
	 */
	@Id
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the sequenceNumber
	 */
	@Column(length=15)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	/**
	 * @param sequenceNumber the sequenceNumber to set
	 */
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	/**
	 * @return the ship
	 */
	@Column(length=2)
	public String getShip() {
		return ship;
	}
	/**
	 * @param ship the ship to set
	 */
	public void setShip(String ship) {
		this.ship = ship;
	}
	/**
	 * @return the shipNumber
	 */
	@Column(length=15)
	public String getShipNumber() {
		return shipNumber;
	}
	/**
	 * @param shipNumber the shipNumber to set
	 */
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	/**
	 * @return the consigneeAddress
	 */
	@Column(length=1000)
	public String getConsigneeAddress() {
		return consigneeAddress;
	}
	/**
	 * @param consigneeAddress the consigneeAddress to set
	 */
	public void setConsigneeAddress(String consigneeAddress) {
		this.consigneeAddress = consigneeAddress;
	}
	/**
	 * @return the consignmentInstructions
	 */
	@Column(length=100)
	public String getConsignmentInstructions() {
		return consignmentInstructions;
	}
	/**
	 * @param consignmentInstructions the consignmentInstructions to set
	 */
	public void setConsignmentInstructions(String consignmentInstructions) {
		this.consignmentInstructions = consignmentInstructions;
	}
	/**
	 * @return the createdBy
	 */
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the notification
	 */
	@Column(length=1000)
	public String getNotification() {
		return notification;
	}
	/**
	 * @param notification the notification to set
	 */
	public void setNotification(String notification) {
		this.notification = notification;
	}
	/**
	 * @return the notify
	 */
	@Column
	public Boolean getNotify() {
		return notify;
	}
	/**
	 * @param notify the notify to set
	 */
	public void setNotify(Boolean notify) {
		this.notify = notify;
	}
	/**
	 * @return the shipperAddress
	 */
	@Column(length=1000)
	public String getShipperAddress() {
		return shipperAddress;
	}
	/**
	 * @param shipperAddress the shipperAddress to set
	 */
	public void setShipperAddress(String shipperAddress) {
		this.shipperAddress = shipperAddress;
	}
	/**
	 * @return the updatedBy
	 */
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the updatedOn
	 */
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the specialInstructions
	 */
	@Column(length=1000)
	public String getSpecialInstructions() {
		return specialInstructions;
	}
	/**
	 * @param specialInstructions the specialInstructions to set
	 */
	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}
	/**
	 * @return the serviceOrderId
	 */
	@Column(length=20)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	/**
	 * @param serviceOrderId the serviceOrderId to set
	 */
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	/**
	 * @return the shipmentLocation
	 */
	@Column(length=1000)
	public String getShipmentLocation() {
		return shipmentLocation;
	}
	/**
	 * @param shipmentLocation the shipmentLocation to set
	 */
	public void setShipmentLocation(String shipmentLocation) {
		this.shipmentLocation = shipmentLocation;
	}
	@Column
	public Boolean getCollect() {
		return collect;
	}
	public void setCollect(Boolean collect) {
		this.collect = collect;
	}
	@Column
	public Boolean getExpressRelease() {
		return expressRelease;
	}
	public void setExpressRelease(Boolean expressRelease) {
		this.expressRelease = expressRelease;
	}
	@Column
	public String getUgwIntId() {
		return ugwIntId;
	}
	public void setUgwIntId(String ugwIntId) {
		this.ugwIntId = ugwIntId;
	}
}