<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>
<title><fmt:message key="refMasterList.viewList" /></title>
<meta name="heading"
	content="<fmt:message key='refMasterListhead.viewList'/>" />
	<script language="javascript" type="text/javascript">
  function clear_fields(){
			    document.forms['searchForm'].elements['refMaster.code'].value = "";
			    document.forms['searchForm'].elements['refMaster.description'].value = "";
			    document.forms['searchForm'].elements['isactiveStatus'].value = "All";
}

/* function confirmSubmit(id){
	var sdHubWarehouseOperational = '${sdHubWarehouseOperationalLimit}';
	var param='${parameter}';
	if(sdHubWarehouseOperational =='Crew' && param =='TCKTSERVC')
		{
		document.getElementById("disableRemove").disable=true;
		}else{
	var agree=confirm("Are you sure you wish to remove it?");
	var did = targetElement;
	if (agree){
		location.href="deleteRefMastermenu.html?id="+did+"&parameter=${parameter}";
	}else{
		return false;
	}
}} */
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var httpCMMAgent = getHTTPObject();
function confirmSubmit(id,target){
	var sdHubWarehouseOperational = '${sdHubWarehouseOperationalLimit}';
	var param='${parameter}';
		if(sdHubWarehouseOperational =='Crew' && param =='TCKTSERVC'){
			document.getElementById("disableRemove").disable=true;
		}else{	
			var isChecked=target.checked;
			var status='Active';
			if(isChecked){
				status='Active';
			}else{
				status='Inactive';
			}
			 if(id!=undefined && id!='' && id!='null' && id!=null){
			    var url="updateRefmasterStatus.html?ajax=1&decorator=simple&popup=true&id="+id+"&status="+encodeURI(status);
			    httpCMMAgent.open("GET", url, true);
			    httpCMMAgent.onreadystatechange = selectiveInvoiceCheckResponse;
			    httpCMMAgent.send(null);
			 }
		}
}
function selectiveInvoiceCheckResponse(){
	  if (httpCMMAgent.readyState == 4)
    {
       var results = httpCMMAgent.responseText  
    }
}

var httpupdateSequence = getHTTPObject();
function updateSequenceNumber(id,target){
			 if(id!=undefined && id!='' && id!='null' && id!=null){
				 var sequence=target.value;
			    var url="updateRefmasterSequence.html?ajax=1&decorator=simple&popup=true&id="+id+"&sequenceNumber="+encodeURI(sequence);
			    httpupdateSequence.open("GET", url, true);
			    httpupdateSequence.onreadystatechange = selectiveSequenceResponse;
			    httpupdateSequence.send(null);
			 }
		
}
function selectiveSequenceResponse(){
	  if (httpupdateSequence.readyState == 4)
  {
     var results = httpupdateSequence.responseText  
  }
}
</script>
<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>
</head>



<s:form id="searchForm" action="searchRefMasters" method="post" >
<div id="layer1" style="width: 100%;">
<s:hidden name="parameter" value="<%=request.getParameter("parameter")%>" />
<c:set var="parameter" value="<%=request.getParameter("parameter")%>" scope="session"/>
<c:set var="refMaster.id" value="<%=request.getParameter("id")%>" scope="session"/>
<c:set var="pmt" value="%{refMaster.parameter}" scope="session"/>
<c:set var="deletedId" value="<%=request.getParameter("id")%>" scope="session"/>
<s:hidden name="sdHubWarehouseOperationalLimit"></s:hidden>

<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="search" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:12px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%"  >
<thead>
<tr>
<th><fmt:message key="refMaster.code"/></th>
<th><fmt:message key="refMaster.description"/></th>
<th>Active Status</th>
<th>&nbsp;</th>
<%-- <th><fmt:message key="refMaster.parameter"/></th>--%>
</tr></thead>	
		<tbody>
		<tr>
			<td width="" align="left">
			    <s:textfield name="refMaster.code" required="true" cssClass="input-text" size="30"/>
			</td>
			<td width="" align="left">
			    <s:textfield name="refMaster.description" required="true" cssClass="input-text" size="30"/>
			</td>
			<td>
			<s:select id="isactiveStatus" name="isactiveStatus" list="{'All','Active','Inactive'}" cssClass="list-menu" />
			</td>
			<td width="" align="center">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
			</tr>
		<%-- 	<td width="20" align="left">
			    <s:textfield name="refMaster.parameter" required="true" cssClass="input-text" size="12"/>
			</td>
		--%>		
		</tbody>
	</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

	<s:set name="refMasters" value="refMasters" scope="request" />
	
	 
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Reference  List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	<display:table name="refMasters" class="table" requestURI="refMastermenus.html" id="refMasterList" export="true" pagesize="50" style="width:100%;">
		<c:set var="lowerStr" value="${refMasterList.parameter}"/>
		 <c:set var="fieldLength" value="${refMasterList.fieldLength}"/>
	<display:column sortable="true" sortProperty="code" titleKey="refMaster.code" >
	     <c:choose>
	     <c:when test="${sdHubWarehouseOperationalLimit eq 'Crew' && parameter eq 'TCKTSERVC' }">
	     <c:out value="${refMasterList.code}" />
	     </c:when>
	      <c:otherwise>
	     <a href="editRefMastermenu.html?parameter=${fn:toUpperCase(lowerStr)}&id=${refMasterList.id}&l=${refMasterList.fieldLength}"><c:out value="${refMasterList.code}" /></a>
	    </c:otherwise>
	    </c:choose>   
	</display:column>		
	<display:column property="description" sortable="true" maxLength="50" titleKey="refMaster.description" style="width:200px;"/>
	<display:column property="bucket" sortable="true" titleKey="refMaster.bucket" />
	<display:column property="bucket2" sortable="true" 	titleKey="refMaster.bucket2" />
	<display:column  sortable="true" titleKey="refMaster.parameter">${fn:toUpperCase(lowerStr)}</display:column>
	<display:column headerClass="containeralign" style="text-align: right; width:110px;"  property="fieldLength" sortable="true" titleKey="refMaster.fieldLength" />
<%-- <display:column headerClass="containeralign" style="text-align: right" property="number" sortable="true" titleKey="refMaster.number" /> 
--%>
    <display:column property="flex1" sortable="true" title="Flex1" />
    <display:column property="flex2" sortable="true" title="Flex2" />
    <display:column property="flex3" sortable="true" title="Flex3" />
    <display:column property="flex4" sortable="true" title="Flex4" />
    <display:column sortable="true" sortProperty="sequenceNo" title="Seq.No." >
    <c:if test="${refMasterList.corpID==sessionCorpID}">
    <select class="list-menu pr-f11"   tabindex="" onchange="updateSequenceNumber(${refMasterList.id},this)">
    	<option value="0"></option>
   			<c:forEach var="varcont" items="${sequenceList}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${varcont == refMasterList.sequenceNo}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
					<option value="<c:out value='${varcont}' />" <c:out value='${selectedInd}' />>
						<c:out value="${varcont}"></c:out>
    				</option>
			</c:forEach>
		</select>
	</c:if>
	<c:if test="${refMasterList.corpID!=sessionCorpID}">
	<select class="list-menu pr-f11"   tabindex="" onchange="updateSequenceNumber(${refMasterList.id},this)" disabled="disabled">
    	<option value=""></option>
   			<c:forEach var="varcont" items="${sequenceList}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${varcont == refMasterList.sequenceNo}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
					<option value="<c:out value='${varcont}' />" <c:out value='${selectedInd}' />>
						<c:out value="${varcont}"></c:out>
    				</option>
			</c:forEach>
		</select>
	</c:if>
    </display:column>
   <display:column title="Status" style="text-align: center; width:50px;">
   <c:if test="${refMasterList.corpID==sessionCorpID}">
	<c:choose>
	<c:when test="${refMasterList.status != '' && refMasterList.status != null && refMasterList.status=='Active'}">
		<input type="checkbox" onclick="confirmSubmit(${refMasterList.id},this)" checked="true"/> 
	</c:when>
	<c:otherwise>
		<input type="checkbox" onclick="confirmSubmit(${refMasterList.id},this)" /> 
	</c:otherwise>
	</c:choose>
	</c:if>
	<c:if test="${refMasterList.corpID!=sessionCorpID}">
	<c:choose>
	<c:when test="${refMasterList.status != '' && refMasterList.status != null && refMasterList.status=='Active'}">
		<input type="checkbox" checked="true" disabled="disabled"/> 
	</c:when>
	<c:otherwise>
		<input type="checkbox" disabled="disabled"/> 
	</c:otherwise>
	</c:choose>	
	</c:if>
	</display:column>
	

		<display:setProperty name="paging.banner.item_name" value="refMaster" />
		<display:setProperty name="paging.banner.items_name" value="refMasters" />
		<display:setProperty name="export.excel.filename"
			value="RefMaster List.xls" />
		<display:setProperty name="export.csv.filename"
			value="RefMaster List.csv" />
		<display:setProperty name="export.pdf.filename"
			value="RefMaster List.pdf" />
	</display:table>
	<%-- 
	<c:set var="buttons">
		<input type="button" style="width:55px; height:25px" class="cssbutton"
			onclick="location.href='<c:url value="/editRefMastermenu.html?parameter=${pmt}"/>'"
			value="<fmt:message key="button.add"/>" />
       
		
	</c:set>
	--%>
									<c:if test="${editable}">
									<c:if test="${empty parameter}" >
						    			<td><input type="button" class="cssbuttonA" value="Add"  style="width:55px; height:25px" onclick="location.href='<c:url value="/editRefMastermenu.html"/>'" /></td>
						     		</c:if>
						     		<c:if test="${not empty parameter}" >
						     		<c:choose>
						     		 <c:when test="${sdHubWarehouseOperationalLimit eq 'Crew' && parameter eq 'TCKTSERVC' }">
						     		 <td><input type="button" class="cssbuttonA" value="Add"  style="width:55px; height:25px" disabled="disabled">
						     		</c:when>
						     		<c:otherwise>
	                     				<td><input type="button" class="cssbuttonA" value="Add"  style="width:55px; height:25px" onclick="location.href='<c:url value="/editRefMastermenu.html?parameter=${parameter}&l=${fieldLength}"/>'" /></td>
						 			</c:otherwise>
						 			</c:choose>
						 			</c:if>
									</c:if>
	<c:out value="${buttons}" escapeXml="false" />
	</div>
</s:form>
<script type="text/javascript"> 
    //highlightTableRows("refMasterList"); 
</script>