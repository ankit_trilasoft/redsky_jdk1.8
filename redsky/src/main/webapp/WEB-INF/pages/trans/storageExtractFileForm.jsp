<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Storage Extract</title>
<meta name="heading" content="Storage Extract" />	

<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style type="text/css">
	.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:4px 3px 1px 5px; height:15px;width:598px; border:1px solid #99BBE8; border-right:none; border-left:none} 
	a.dsphead{text-decoration:none;color:#000000;}
	.dspchar2{padding-left:0px;}
</style>
<style>
	.input-textarea{
		border:1px solid #219DD1;
		color:#000000;
		font-family:arial,verdana;
		font-size:12px;
		height:45px;
		text-decoration:none;
	}

.bgblue{background:url(images/blue_band.jpg); height: 30px; width:630px; background-repeat: no-repeat;font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #007a8d; padding-left: 40px; }
</style>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script>
<script language="javascript" type="text/javascript">
       function fieldValidate(){
       
	       if(document.forms['storageExtractForm'].elements['beginDate'].value=='')
	       {
	       	alert("Please enter the begin date"); 
	       	return false;
	       }
	       if(document.forms['storageExtractForm'].elements['endDate'].value==''){
	       alert("Please enter the end date "); 
	       	return false;
	       	} 
	       else
	       {
	       	document.forms['storageExtractForm'].submit();
	         return true;
	       }
       
       }  
</script>
</head>

<s:form name="storageExtractForm" id="storageExtractForm" action="storageExtract" method="post" validate="true"  cssClass="form_magn">
<configByCorp:fieldVisibility componentId="component.field.Alternative.StorageExtSTLJobNo">
		<s:hidden name="jobSTLFlag" value="No" />
	</configByCorp:fieldVisibility>
	<sec-auth:authComponent componentId="module.script.form.corpSalesScript">
	<s:hidden name="salesPortalFlag" value="Yes" />
	</sec-auth:authComponent>
	<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="dd-NNN-yy"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>

<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="dd-NNN-yy"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>
<div id="Layer1" style="width:100%">
<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Storage Extract</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>


<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0" style="" width="100%">

	  		
  			<!--<tr> 
  			<td></td>
				<td class="listwhitetext" align="right">Begin Date<font color="red" size="2">*</font></td>
					<c:if test="${not empty beginDate}">
						<s:text id="beginDate" name="${FormDateValue}"><s:param name="value" value="beginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="beginDate" name="beginDate" value="%{beginDate}" size="10" maxlength="11" readonly="true" /> <img id="calender3" align="top" src="${pageContext.request.contextPath}/images/calender.png"HEIGHT=20 WIDTH=20onclick=" onclick="cal.select(document.forms['storageExtractForm'].beginDate,'calender3',document.forms['storageExtractForm'].dateFormat.value); return false;" /></td>
					</c:if>
					<c:if test="${empty beginDate}">
						<td><s:textfield cssClass="input-text" id="beginDate" name="beginDate" required="true" size="10" maxlength="11" readonly="true" /> <img id="calender3" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['storageExtractForm'].beginDate,'calender3',document.forms['storageExtractForm'].dateFormat.value); return false;" /></td>
					</c:if>
				      	
				    <td class="listwhitetext" align="right">End Date<font color="red" size="2">*</font></td>
		 			<c:if test="${not empty endDate}">
						<s:text id="endDate" name="${FormDateValue}"> <s:param name="value" value="endDate" /></s:text>
						<td width="400px"><s:textfield cssClass="input-text" id="endDate" name="endDate" value="%{endDate}" size="10" maxlength="11" readonly="true" /> <img id="calender4" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['storageExtractForm'].endDate,'calender4',document.forms['storageExtractForm'].dateFormat.value); return false;" /></td>
					</c:if>
					<c:if test="${empty endDate}" >
						<td width="400px"><s:textfield cssClass="input-text" id="endDate" name="endDate" required="true" size="10" maxlength="11" readonly="true" /> <img id="calender4" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['storageExtractForm'].endDate,'calender4',document.forms['storageExtractForm'].dateFormat.value); return false;" /></td>
					</c:if>		 	
			</tr> 
			--><tr><td height="20px"></td></tr>
	  		<tr>	
	  			<td width=""></td>			
				<td colspan="4"><s:submit cssClass="cssbutton" cssStyle="width:155px; height:25px" align="top" value="Extract"/></td>
	    	</tr>
	    	<tr><td height="20"></td></tr>	 				
	</table>       
</div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>

</div>
<!--<s:text id="customerFileMoveDateFormattedValue" name="FormDateValue"><s:param name="value" value="refMaster.stopDate"/></s:text>
<s:hidden name="refMaster.stopDate" />
	
	

--></s:form>


