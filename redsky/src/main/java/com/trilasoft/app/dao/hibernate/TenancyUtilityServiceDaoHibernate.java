package com.trilasoft.app.dao.hibernate;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.TenancyUtilityServiceDao;
import com.trilasoft.app.model.TenancyUtilityService;


public class TenancyUtilityServiceDaoHibernate extends GenericDaoHibernate<TenancyUtilityService, Long>implements TenancyUtilityServiceDao{
	public TenancyUtilityServiceDaoHibernate() {
		super(TenancyUtilityService.class);
	}
}
