<%@ include file="/common/taglibs.jsp"%>   
  
<head>   
    <title><fmt:message key="manageDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='manageDetail.heading'/>"/>   
</head> 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<s:form id="internationalForm" action="saveInternational" method="post" validate="true">   
<s:hidden name="international.id" value="%{international.id}"/>
<table width="100%" border="0">

<tr>
<td align="left" width="5%"><s:label label="SIT at Origin" /></td>
<td width="18%"><s:select name="international.sitOriginYN" list="{':select','a','b','c','d','e','f'}"/></td>
<td align="left" width="5%"><s:label label="SIT at Destination" /></td>    
<td width="18%"><s:select name="international.sitDestinationYN" list="{':select','a','b','c','d','e','f'}"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Date in" /></td>
<td align="left" width="12%"><s:textfield name="international.sitOrigin" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Date in" /></td>
<td align="left" width="12%"><s:textfield name="international.sitDestinationIN" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Days#" /></td>
<td align="left" width="12%"><s:textfield name="international.sitOriginDays" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Days#" /></td>
<td align="left" width="12%"><s:textfield name="international.sitDestinationDays" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Date Out" /></td>
<td width="18%"><s:select name="international.sitOriginTA" list="{':select','a','b','c','d','e','f'}"/></td>
<td align="left" width="12%"><s:textfield name="international.sitOriginTA" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Date Out" /></td>    
<td width="18%"><s:select name="international.sitDestinationTA" list="{':select','a','b','c','d','e','f'}"/></td>
<td align="left" width="12%"><s:textfield name="international.sitDestinationTA" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Send Let" /></td>
<td width="18%"><s:select name="international.sitOriginLetter" list="{':select','a','b','c','d','e','f'}"/></td>
<td align="left" width="12%"><s:textfield name="international.sitOriginLetter" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Send Let" /></td>    
<td width="18%"><s:select name="international.sitDestinationLetter" list="{':select','a','b','c','d','e','f'}"/></td>
<td align="left" width="12%"><s:textfield name="international.sitDestinationLetter" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Manager OK" /></td>
<td align="left" width="12%"><s:textfield name="international.managerOkInital" required="true" cssClass="text medium"/></td>
<td align="left" width="12%"><s:textfield name="international.managerOkDate" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Packing Day Call" /></td>
<td align="left" width="12%"><s:textfield name="international.packDayConfirmCall" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Shipment OK" /></td>
<td align="left" width="12%"><s:textfield name="international.saleOKDate" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Loading Day Call" /></td>
<td align="left" width="12%"><s:textfield name="international.loadFollowUpCall" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Insurance order" /></td>
<td align="left" width="12%"><s:textfield name="misclanious.insuranceDate" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Delivery Day Call" /></td>
<td align="left" width="12%"><s:textfield name="international.deliveryFollowCall" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Reconfirm shipper" /></td>
<td align="left" width="12%"><s:textfield name="international.reconfirmShipper" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Poast mv Call/email" /></td>
<td align="left" width="12%"><s:textfield name="international.deliveryFollowCall" required="true" cssClass="text medium"/></td>
</tr>

<tr>
<td align="left" width="5%"><s:label label="Car titles" /></td>
<td align="left" width="12%"><s:textfield name="international.moveFollowUp" required="true" cssClass="text medium"/></td>
<td align="left" width="5%"><s:label label="Sent comp email" /></td>
<td align="left" width="12%"><s:textfield name="international.sentcomp" required="true" cssClass="text medium"/></td>
</tr>
</table>
</s:form>
  
<script type="text/javascript">   
    Form.focusFirstElement($("internationalForm"));   
</script>