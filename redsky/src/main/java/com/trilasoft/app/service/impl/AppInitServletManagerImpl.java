package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.AppInitServletDao;
import com.trilasoft.app.dao.AuditSetupDao;
import com.trilasoft.app.service.AppInitServletManager;

public class AppInitServletManagerImpl  extends GenericManagerImpl implements AppInitServletManager {

	
	 public AppInitServletManagerImpl(AppInitServletDao appInitServletDao) { 
	        super(appInitServletDao); 
	        this.appInitServletDao = appInitServletDao; 
	    }
	
	private AppInitServletDao appInitServletDao;
	
	public AppInitServletDao getAppInitServletDao() {
		return appInitServletDao;
	}

	public void setAppInitServletDao(AppInitServletDao appInitServletDao) {
		this.appInitServletDao = appInitServletDao;
	}

	public AppInitServletManagerImpl(GenericDao genericDao) {
		super(genericDao);
	}

	public void populatePermissionCache(Map pageActionUrls) {
		appInitServletDao.populatePermissionCache(pageActionUrls);
	}

	public void populateRolebasedComponentPermissions(Map permissionMap) {
		appInitServletDao.populateRolebasedComponentPermissions(permissionMap);
	}
	public List getListForSurveyCalander(String corpid,String fmdt,String todt,String consult,String surveycity,String job){
		return appInitServletDao.getListForSurveyCalander( corpid, fmdt, todt, consult, surveycity,job);
	}

	public void populatePageFieldVisibilityPermissions( Map pageFieldVisibilityComponentMap) {
		appInitServletDao.populatePageFieldVisibilityPermissions(pageFieldVisibilityComponentMap);
		
	}

}
