package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.PartnerRefChargesDao;
import com.trilasoft.app.model.PartnerRefCharges;
import com.trilasoft.app.service.PartnerRefChargesManager;

public class PartnerRefChargesManagerImpl extends GenericManagerImpl<PartnerRefCharges, Long> implements PartnerRefChargesManager {
	PartnerRefChargesDao partnerRefChargesDao;

	public PartnerRefChargesManagerImpl(PartnerRefChargesDao partnerRefChargesDao) {
		super(partnerRefChargesDao);
		this.partnerRefChargesDao = partnerRefChargesDao;
	}

	public List getRefCharesList(String value, String tariffApplicability, String parameter, String countryCode, String grid, String basis, String label, String sessionCorpID) {
		// TODO Auto-generated method stub
		return partnerRefChargesDao.getRefCharesList(value, tariffApplicability, parameter, countryCode, grid, basis, label, sessionCorpID);
	}

}
