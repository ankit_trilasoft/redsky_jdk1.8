<%@ include file="/common/taglibs.jsp"%>

<tr>
    <td>
    <div id='teste' style="overflow-x:scroll; width:340px; overflow: -moz-scrollbars-horizontal; font-size:11px;">
        <select  name="<c:out value="${param.leftId}" />" multiple="multiple"
            onDblClick="moveSelectedOptions(this,$('<c:out value="${param.rightId}"/>'),true)"
            id="<c:out value="${param.leftId}"/>" size="6" style="width:340px; font-size:11px;" tabindex="32" onclick="" >
            <c:if test="${leftListTruck != null}">
		    	<c:forEach var="list" items="${leftListTruck}" varStatus="status">
		            <option value="${list}">
		                <c:out value="${list}" escapeXml="false" />
		            </option>
        		</c:forEach>
    		</c:if>

        </select> 
        </div>
    </td>
    <td class="moveOptions">
        <button name="moveRight" id="moveRight<c:out value="${param.listCount}"/>" type="button" tabindex="33"
            onclick="moveSelectedOptions($('<c:out value="${param.leftId}"/>'),$('<c:out value="${param.rightId}"/>'),true)">
            &gt;&gt;</button><br />
        <button name="moveLeft" id="moveLeft<c:out value="${param.listCount}"/>" type="button" tabindex="34"
            onclick="moveSelectedOptions($('<c:out value="${param.rightId}"/>'),$('<c:out value="${param.leftId}"/>'),true)">
            &lt;&lt;</button><br />
        </td>
    <td>
    <div id='teste' style="overflow-x:scroll; width:340px; overflow: -moz-scrollbars-horizontal; ">
    
        <select  name="<c:out value="${param.rightId}"/>" multiple="multiple"
            id="<c:out value="${param.rightId}"/>" size="6" style="width:340px; font-size:11px;" tabindex="35" onclick="">
    <c:if test="${rightListTruck != null}">
        <c:forEach var="list" items="${rightListTruck}" varStatus="status">
            <option value="<c:out value="${list}"/>">
                <c:out value="${list}" escapeXml="false"/>
            </option>
        </c:forEach>
    </c:if>
        </select>
        </div>
    </td>
</tr>
<script type='text/javascript'> 
   
</script> 
