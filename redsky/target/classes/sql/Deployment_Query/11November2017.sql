
// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;


CREATE  TABLE `redsky`.`customerRelation` (
  id BIGINT(20) NOT NULL AUTO_INCREMENT ,
  corpId VARCHAR(10) NULL ,
   createdBy varchar(82) null,
    createdOn datetime null,
   updatedBy varchar(82) null,
     updateOn datetime null,
    customerFileId bigint(20) unsigned NOT NULL,
  workArea VARCHAR(10) NULL ,
  position VARCHAR(10) NULL ,
  reasonForTransfer varchar(32) NULL ,
  loadBeginDate datetime NULL ,
  loadEndDate  datetime NULL ,
  frequencyOfMove varchar(20) NULL ,
  serviceExpectations varchar(100) NULL ,
  favColour varchar(10) NULL ,
   favCake varchar(10) NULL ,
  favHobby varchar(10) NULL ,
  specialItem varchar(10) NULL ,
  qualityRS varchar(10) NULL ,
 
  PRIMARY KEY (`id`) );
  
  ALTER TABLE `redsky`.`storage` ADD COLUMN `originalPieces` INT(11) NULL  AFTER `warehouse` , ADD COLUMN `originalMeasQuantity` DECIMAL(19,2) NULL  AFTER `originalPieces` , ADD COLUMN `originalVolume` DECIMAL(19,2) NULL  AFTER `originalMeasQuantity` ;
  ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `sitDestinationInCondition` VARCHAR(1) NULL, ADD COLUMN `sitOriginOnCondition` VARCHAR(1) NULL  AFTER `sitDestinationInCondition` ;
  ALTER TABLE `redsky`.`partnerpublic` ADD COLUMN `PAIMA` VARCHAR(1) NULL     ,
ADD COLUMN `LACMA` VARCHAR(1) NULL   AFTER `PAIMA` ,
ADD COLUMN `eurovanNetwork` VARCHAR(1) NULL  AFTER `LACMA` ;
  ALTER TABLE `redsky`.`genericsurveyquestion` ADD COLUMN `category` VARCHAR(100) NULL  AFTER `updatedOn` ;
ALTER TABLE `redsky`.`surveyanswerbyuser` ADD COLUMN `customerFileId` BIGINT(20) NULL  AFTER `updatedOn` ;
ALTER TABLE `redsky`.`customerRelation` ADD COLUMN `timeOfReturn` VARCHAR(6) NULL  AFTER `qualityRS` , ADD COLUMN `favPlace` VARCHAR(12) NULL  AFTER `timeOfReturn` ;

ALTER TABLE `redsky`.`reports` MODIFY COLUMN `job` VARCHAR(100);
ALTER TABLE `redsky`.`customerRelation` CHANGE COLUMN `ReasonForTransfer` `reasonForTransfer` VARCHAR(20) NULL DEFAULT NULL  ;

DROP VIEW IF EXISTS `redsky`.`partner`;

CREATE OR REPLACE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `partner` AS select `a`.`id` AS `id`,`a`.`parent` AS `parent`,`a`.`middleInitial` AS `middleInitial`,`a`.`billingAddress1` AS `billingAddress1`,`a`.`billingAddress2` AS `billingAddress2`,`a`.`billingAddress3` AS `billingAddress3`,`a`.`billingAddress4` AS `billingAddress4`,`a`.`billingCity` AS `billingCity`,`a`.`billingCountry` AS `billingCountry`,`a`.`billingCountryCode` AS `billingCountryCode`,`a`.`billingEmail` AS `billingEmail`,`a`.`billingFax` AS `billingFax`,`a`.`billingPhone` AS `billingPhone`,`a`.`billingState` AS `billingState`,`a`.`billingTelex` AS `billingTelex`,`a`.`billingZip` AS `billingZip`,`a`.`effectiveDate` AS `effectiveDate`,`a`.`isAccount` AS `isAccount`,`a`.`isAgent` AS `isAgent`,`a`.`isCarrier` AS `isCarrier`,`a`.`isVendor` AS `isVendor`,`a`.`mailingAddress1` AS `mailingAddress1`,`a`.`mailingAddress2` AS `mailingAddress2`,`a`.`mailingAddress3` AS `mailingAddress3`,`a`.`mailingAddress4` AS `mailingAddress4`,`a`.`mailingCity` AS `mailingCity`,`a`.`mailingCountry` AS `mailingCountry`,`a`.`mailingCountryCode` AS `mailingCountryCode`,`a`.`mailingEmail` AS `mailingEmail`,`a`.`mailingFax` AS `mailingFax`,`a`.`mailingPhone` AS `mailingPhone`,`a`.`mailingState` AS `mailingState`,`a`.`mailingTelex` AS `mailingTelex`,`a`.`mailingZip` AS `mailingZip`,`a`.`partnerPrefix` AS `partnerPrefix`,`a`.`partnerSuffix` AS `partnerSuffix`,`a`.`terminalAddress1` AS `terminalAddress1`,`a`.`terminalAddress2` AS `terminalAddress2`,`a`.`terminalAddress3` AS `terminalAddress3`,`a`.`terminalAddress4` AS `terminalAddress4`,`a`.`terminalCity` AS `terminalCity`,`a`.`terminalCountry` AS `terminalCountry`,`a`.`terminalCountryCode` AS `terminalCountryCode`,`a`.`terminalEmail` AS `terminalEmail`,`a`.`terminalFax` AS `terminalFax`,`a`.`terminalPhone` AS `terminalPhone`,`a`.`terminalState` AS `terminalState`,`a`.`terminalTelex` AS `terminalTelex`,`a`.`terminalZip` AS `terminalZip`,`a`.`isPrivateParty` AS `isPrivateParty`,`a`.`air` AS `air`,`a`.`sea` AS `sea`,`a`.`surface` AS `surface`,`a`.`isOwnerOp` AS `isOwnerOp`,`a`.`typeOfVendor` AS `typeOfVendor`,`a`.`agentParent` AS `agentParent`,`a`.`location1` AS `location1`,`a`.`location2` AS `location2`,`a`.`location3` AS `location3`,`a`.`location4` AS `location4`,`a`.`companyProfile` AS `companyProfile`,`a`.`url` AS `url`,`a`.`latitude` AS `latitude`,`a`.`longitude` AS `longitude`,`a`.`trackingUrl` AS `trackingUrl`,`a`.`yearEstablished` AS `yearEstablished`,`a`.`companyFacilities` AS `companyFacilities`,`a`.`companyCapabilities` AS `companyCapabilities`,`a`.`companyDestiantionProfile` AS `companyDestiantionProfile`,`a`.`serviceRangeKms` AS `serviceRangeKms`,`a`.`serviceRangeMiles` AS `serviceRangeMiles`,`a`.`fidiNumber` AS `fidiNumber`,`a`.`OMNINumber` AS `OMNINumber`,`a`.`IAMNumber` AS `IAMNumber`,`a`.`AMSANumber` AS `AMSANumber`,`a`.`WERCNumber` AS `WERCNumber`,`a`.`facilitySizeSQFT` AS `facilitySizeSQFT`,`a`.`qualityCertifications` AS `qualityCertifications`,`a`.`vanLineAffiliation` AS `vanLineAffiliation`,`a`.`serviceLines` AS `serviceLines`,`a`.`facilitySizeSQMT` AS `facilitySizeSQMT`,`b`.`corpid` AS `corpid`,`b`.`warehouse` AS `warehouse`,`a`.`createdBy` AS `createdBy`,`a`.`createdOn` AS `createdOn`,`a`.`updatedBy` AS `updatedBy`,if((`a`.`updatedOn` > `b`.`updatedOn`),`a`.`updatedOn`,`b`.`updatedOn`) AS `updatedOn`,`a`.`firstName` AS `firstName`,`a`.`lastName` AS `lastName`,if((`a`.`isPrivateParty` is true),`a`.`status`,`b`.`status`) AS `status`,`a`.`nextVanLocation` AS `nextVanLocation`,`a`.`nextReportOn` AS `nextReportOn`,`b`.`billingInstruction` AS `billingInstruction`,`b`.`billingInstructionCode` AS `billingInstructionCode`,`b`.`coordinator` AS `coordinator`,`b`.`salesMan` AS `salesMan`,`b`.`qc` AS `qc`,`b`.`abbreviation` AS `abbreviation`,`b`.`billPayOption` AS `billPayOption`,`b`.`billPayType` AS `billPayType`,`b`.`billToGroup` AS `billToGroup`,`b`.`longPercentage` AS `longPercentage`,`b`.`needAuth` AS `needAuth`,`a`.`partnerCode` AS `partnerCode`,`b`.`storageBillingGroup` AS `storageBillingGroup`,`b`.`accountHolder` AS `accountHolder`,`b`.`paymentMethod` AS `paymentMethod`,`b`.`payOption` AS `payOption`,`b`.`multiAuthorization` AS `multiAuthorization`,`b`.`payableUploadCheck` AS `payableUploadCheck`,`b`.`companyDivision` AS `companyDivision`,`b`.`invoiceUploadCheck` AS `invoiceUploadCheck`,`b`.`billingUser` AS `billingUser`,`b`.`payableUser` AS `payableUser`,`b`.`pricingUser` AS `pricingUser`,`b`.`partnerPortalActive` AS `partnerPortalActive`,`b`.`partnerPortalId` AS `partnerPortalId`,`b`.`associatedAgents` AS `associatedAgents`,`b`.`viewChild` AS `viewChild`,`b`.`creditTerms` AS `creditTerms`,`b`.`accountingDefault` AS `accountingDefault`,`b`.`acctDefaultJobType` AS `acctDefaultJobType`,`b`.`stopNotAuthorizedInvoices` AS `stopNotAuthorizedInvoices`,`b`.`doNotCopyAuthorizationSO` AS `doNotCopyAuthorizationSO`,`b`.`partnerPublicId` AS `partnerPublicId`,`b`.`driverAgency` AS `driverAgency`,`b`.`validNationalCode` AS `validNationalCode`,`b`.`cardNumber` AS `cardNumber`,`b`.`cardStatus` AS `cardStatus`,`b`.`accountId` AS `accountId`,`b`.`customerId` AS `customerId`,`b`.`licenseNumber` AS `licenseNumber`,`b`.`licenseState` AS `licenseState`,`b`.`cardSettelment` AS `cardSettelment`,`b`.`directDeposit` AS `directDeposit`,`b`.`fuelPurchase` AS `fuelPurchase`,`b`.`expressCash` AS `expressCash`,`b`.`creditCheck` AS `creditCheck`,`b`.`creditAmount` AS `creditAmount`,`b`.`creditCurrency` AS `creditCurrency`,`b`.`yruAccess` AS `yruAccess`,`b`.`taxId` AS `taxId`,`b`.`taxIdType` AS `taxIdType`,`b`.`extReference` AS `extReference`,`a`.`startDate` AS `startDate`,`b`.`accountManager` AS `accountManager`,`b`.`classcode` AS `classcode`,`a`.`vatNumber` AS `vatNumber`,`a`.`utsNumber` AS `utsNumber`,`a`.`isNetworkPartner` AS `isNetworkPartner`,`a`.`vanLastLocation` AS `vanLastLocation`,`a`.`vanLastReportOn` AS `vanLastReportOn`,`a`.`vanAvailCube` AS `vanAvailCube`,`a`.`vanLastReportTime` AS `vanLastReportTime`,`a`.`currentVanAgency` AS `currentVanAgency`,`a`.`currentVanID` AS `currentVanID`,`a`.`currentTractorAgency` AS `currentTractorAgency`,`a`.`currentTractorID` AS `currentTractorID`,`b`.`driverType` AS `driverType`,`b`.`noDispatch` AS `noDispatch`,`a`.`aliasName` AS `aliasName`,`a`.`networkGroup` AS `networkGroup`,`b`.`insuranceAuthorized` AS `insuranceAuthorized`,`b`.`mc` AS `mc`,`b`.`soAllDrivers` AS `soAllDrivers`,`a`.`contactName` AS `contactName`,`a`.`billingCurrency` AS `billingCurrency`,`a`.`bankcode` AS `bankCode`,`a`.`bankAccountNumber` AS `bankAccountNumber`,`a`.`agentParentName` AS `agentParentName`,`a`.`ugwwNetworkGroup` AS `ugwwNetworkGroup`,`b`.`collection` AS `collection`,`a`.`isPartnerExtract` AS `isPartnerExtract`,`a`.`agentGroup` AS `agentGroup`,`a`.`partnerType` AS `partnerType`,`b`.`noInsurance` AS `noInsurance`,`b`.`lumpSum` AS `lumpSum`,`b`.`detailedList` AS `detailedList`,`b`.`vatBillingGroup` AS `vatBillingGroup`,`b`.`agentClassification` AS `agentClassification`,`b`.`buyRate` AS `buyRate`,`b`.`sellRate` AS `sellRate`,`b`.`doNotInvoice` AS `doNotInvoice`, a.PAIMA as 'PAIMA',a.LACMA as 'LACMA', a.eurovanNetwork as 'eurovanNetwork' from (`partnerpublic` `a` join `partnerprivate` `b`) where (`a`.`id` = `b`.`partnerPublicId`);
ALTER TABLE `redsky`.`company` ADD COLUMN `genericSurveyOnCfOrSo` VARCHAR(2) NULL  AFTER `customerSurveyOnCustomerPortal` ;

ALTER TABLE `redsky`.`billing`
ADD COLUMN `totalLossCoverageIndicator` VARCHAR(1) NULL  AFTER `contractSystem` ,
ADD COLUMN `mechanicalMalfunctionIndicator` VARCHAR(1) NULL  AFTER `totalLossCoverageIndicator` ,
ADD COLUMN `pairsOrSetsIndicator` VARCHAR(1) NULL  AFTER `mechanicalMalfunctionIndicator` ,
ADD COLUMN `moldOrMildewIndicator` VARCHAR(1) NULL  AFTER `pairsOrSetsIndicator` ;

ALTER TABLE `redsky`.`extractedfilelog` ADD COLUMN `messageLog` VARCHAR(5000) NULL  AFTER `module` ;
ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `importPaperOn` DATETIME NULL  AFTER `packingDays` ;