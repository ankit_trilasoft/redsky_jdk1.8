<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
  
<head>   
    <title><fmt:message key="lossDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='lossDetail.heading'/>"/>
    <style type="text/css">h2 {background-color: #FBBFFF}</style>
    <style><%@ include file="/common/calenderStyle.css"%></style>

</head>
<s:form id="lossForm" action="saveLoss" method="post" validate="true">   
<s:hidden name="loss.id" value="%{loss.id}"/>
<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="serviceOrders.html"/>ServiceOrders</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="editServiceOrder.html?id=<%=request.getParameter("id")%>"/>Detail</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>  
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="editBilling.html?id=${serviceOrder.id}"/>Bill</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="editAgent.html?id=${serviceOrder.id}"/>Agent</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="containers.html?id=${serviceOrder.id}"/>Pack</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab">Date</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab">Long</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="editInternational.html?id=${serviceOrder.id}"/>International</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="6" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="workTickets.html?id=${serviceOrder.id}" >Ticket</a></td>
<td width="7" align="left"><img width="6" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab">Invoice</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/head-left.jpg'/>" /></td>
<td width="100" class="detailActiveTabLabel content-tab"><a href="editClaim.html"/>Claim</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/head-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="editCustomerFile.html?id=${serviceOrder.id}"/>CustomerFile</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
</tr>
</tbody>
</table>

<table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
<tbody>
<tr>
<td>
<table cellspacing="0" cellpadding="3" border="0">
<tbody>

											<tr>
											<td align="left" class="listwhite"><fmt:message key='serviceOrder.firstName1'/></td>
    										<td align="left"><s:textfield name="serviceOrder.firstName" size="9" required="true" cssClass="text medium" readonly="true" /></td><s:hidden name="serviceOrder.id"/>
    										<td align="left"><s:textfield name="serviceOrder.lastName" size="9" required="true" cssClass="text medium" readonly="true"/></td>
											<td align="left" class="listwhite"><fmt:message key='serviceOrder.originCountry'/></td>
											<td align="left"><s:textfield name="serviceOrder.originCountry" size="9" required="true" cssClass="text medium" readonly="true"/></td>
											<td align="left"><s:textfield name="serviceOrder.originState" size="1" required="true" cssClass="text medium" readonly="true"/></td>
											<td align="left" class="listwhite"><fmt:message key='serviceOrder.job2'/></td>
    										<td align="left"><s:textfield name="serviceOrder.job" size="5" required="true" cssClass="text medium" readonly="true"/></td>
    										<td align="left" class="listwhite"><fmt:message key='serviceOrder.commodity'/></td>
    										<td align="left"><s:textfield name="serviceOrder.commodity" size="5" required="true" cssClass="text medium" readonly="true"/></td>
    										<td align="left" class="listwhite"><fmt:message key='serviceOrder.billTo1'/></td>
    										<td align="left"><s:textfield name="serviceOrder.billToCode" size="5" required="true" cssClass="text medium" readonly="true"/></td>
											</tr>
</tbody>
</table>
<table cellspacing="0" cellpadding="3" border="0">
<tbody>											
											<tr>
											<td align="left" class="listwhite"><fmt:message key='serviceOrder.shipNumber'/></td>
    										<td align="left"><s:textfield name="serviceOrder.shipNumber" size="10" required="true" cssClass="text medium" readonly="true"/></td>
											<td></td>
											<td align="left" class="listwhite"><fmt:message key='serviceOrder.destinationCountry'/></td>
    										<td align="left"><s:textfield name="serviceOrder.destinationCountry" size="9" required="true" cssClass="text medium" readonly="true"/></td>
    										<td align="left"><s:textfield name="serviceOrder.destinationState" size="2" required="true" cssClass="text medium" readonly="true"/></td>
    										<td align="left" class="listwhite"><fmt:message key='serviceOrder.registrationNumber'/></td>
    										<td align="left"><s:textfield name="serviceOrder.registrationNumber" size="12" required="true" cssClass="text medium" readonly="true"/></td>
    										<td align="left" class="listwhite"><fmt:message key='serviceOrder.mode'/></td>
    										<td align="left"><s:textfield name="serviceOrder.mode" size="5" required="true" cssClass="text medium" readonly="true"/></td>
    										<td align="left" class="listwhite"><fmt:message key='serviceOrder.routing'/></td>
    										<td align="left"><s:textfield name="serviceOrder.routing" size="5" required="true" cssClass="text medium" readonly="true"/></td>
    										</tr>
											
</tbody>
</table>
<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/tab-left.jpg'/>" /></td>
<td width="100" class="content-tab"><a href="editClaim.html?id=${serviceOrder.id}"/>Claims</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/tab-right.jpg'/>" /></td>
<td width="4"></td>
<td width="7" align="right"><img width="7" height="20" src="<c:url value='/images/head-left.jpg'/>" /></td>
<td width="100" class="detailActiveTabLabel content-tab"><a href="losss.html?id=${serviceOrder.id}"/>Paid</td>
<td width="7" align="left"><img width="7" height="20" src="<c:url value='/images/head-right.jpg'/>" /></td>
<td width="4"></td><td></td><td></td>      
</tr>
</tbody>
</table>



<div id="Layer1">
<table cellspacing="0" cellpadding="6" border="0">
<tbody>
<tr><td valign="top" align="left" class="listwhite">
<tr>
<td align="left" class="listwhite"><fmt:message key="claim.claimNumber"/></td>
<td align="left"><s:textfield name="claim.claimNumber" size="9" maxlength="10" onchange="onlyNumeric(this);"/></td>
<td align="left" class="listwhite"><fmt:message key="loss.idNumber1"/></td>
<td align="left"><s:textfield name="loss.idNumber1" size="9" maxlength="10" onchange="onlyNumeric(this);"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="loss.itemDescription"/></td>
<td align="left"><s:textfield name="loss.itemDescription" size="40" maxlength="25" required="true" cssClass="text medium"/></td>
<td align="left" class="listwhite"><fmt:message key="loss.inventory"/></td>
<td align="left"><s:textfield name="loss.inventory" size="25" maxlength="15" required="true" cssClass="text medium"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="loss.lossComment1"/></td>
<td align="left" colspan="3"><s:textfield name="loss.lossComment" size="75" maxlength="65" required="true" cssClass="text medium"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="loss.lossType"/></td>
<td align="left"><s:textfield name="loss.lossType" size="25" maxlength="4" required="true" cssClass="text medium"/></td>
<td align="left" class="listwhite"><fmt:message key="loss.damageByAction1"/></td>
<td align="left"><s:textfield name="loss.damageByAction" size="25" maxlength="4" required="true" cssClass="text medium"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="loss.lossAction"/></td>
<td align="left"><s:textfield name="loss.lossAction" size="20" maxlength="1" required="true" cssClass="text medium"/></td>
<td align="left" class="listwhite"><fmt:message key="loss.warehouse"/></td>
<td align="left"><s:textfield name="loss.warehouse" size="20" maxlength="2" required="true" cssClass="text medium"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="loss.requestedAmount"/></td>
<td align="left"><s:textfield name="loss.requestedAmount" size="25" maxlength="20" required="true" cssClass="text medium"/></td>
<td align="left" class="listwhite"><fmt:message key="loss.weightDamagedItem"/></td>
<td align="left"><s:textfield name="loss.weightDamagedItem" size="25" maxlength="20" required="true" cssClass="text medium"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="loss.paidCompensationCustomer1"/></td>
<td align="left"><s:textfield name="loss.paidCompensationCustomer" size="25" maxlength="20" required="true" cssClass="text medium"/></td>
<td align="left" class="listwhite"><fmt:message key="loss.chequeNumberCustmer"/></td>
<td align="left"><s:textfield name="loss.chequeNumberCustmer" size="25" maxlength="10" required="true" cssClass="text medium"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="loss.paidCompensation3Party1"/></td>
<td align="left"><s:textfield name="loss.paidCompensation3Party1" size="25" maxlength="20" required="true" cssClass="text medium"/></td>
<td align="left" class="listwhite"><fmt:message key="loss.chequeNumber3Party"/></td>
<td align="left"><s:textfield name="loss.chequeNumber3Party" size="25" maxlength="10" required="true" cssClass="text medium"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="loss.whoWorkCDE"/></td>
<td align="left"><s:textfield name="loss.whoWorkCDE" size="25" maxlength="8" required="true" cssClass="text medium"/></td>
<td align="left" colspan="2"><s:textfield name="loss.damageByAction1" size="40" maxlength="30" required="true" cssClass="text medium"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="loss.chargeBackAmount"/></td>
<td align="left"><s:textfield name="loss.chargeBackAmount" size="25" maxlength="20" required="true" cssClass="text medium"/></td>
</tr>
<tr>
<td align="left" class="listwhite"><fmt:message key="loss.notesLoss"/></td>
<td align="left" colspan="3" ><s:textfield name="loss.notesLoss" size="75" maxlength="65" required="true" cssClass="text medium"/></td>
</tr>
</tbody>
</table>	

<tr>
<td height="10" align="left" class="listwhite">
<div class="subcontent-tab"><a href="javascript:void(0)" class="dsphead" onclick="dsp(this)">Ticket&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="dspchar">+</span></a></div>
<div class="dspcont" id="Layer1">
<table border="0">
<tbody>


<tr>
<td align="left" class="listwhite"><fmt:message key="loss.ticket"/></td>
<td align="left"><s:textfield name="loss.ticket" size="25" maxlength="20" required="true" cssClass="text medium"/></td>
</tr>
</tbody>
</table>
<td valign="top" align="left" class="listwhite" />
<td align="left" class="listwhite" />
</td>
</tr>
<table>
<tbody>
					<tr>
						<td align="right" class="listwhite"><fmt:message key='loss.createdOn'/></td>
						<s:hidden name="loss.createdOn" />
						<td><s:date name="loss.createdOn" format="dd-MMM-yyyy"/></td>		
						<td align="right" class="listwhite"><fmt:message key='loss.createdBy' /></td>
						<s:hidden name="createdBy"/>
						<td><s:label name="createdBy" value="%{loss.createdBy}"/></td>
						<td align="right" class="listwhite"><fmt:message key='loss.updatedOn'/></td>
						<s:hidden name="loss.updatedOn"/>
						<td><s:date name="loss.updatedOn" format="dd-MMM-yyyy"/></td>
						<td align="right" class="listwhite"><fmt:message key='loss.updatedBy' /></td>
						<s:hidden name="loss.updatedBy" />
						<td><s:label name="loss.updatedBy" /></td>
					</tr>
</tbody>
</table>

<td valign="top" align="left" class="listwhite" /></td>
<td align="left" class="listwhite" /></td>
</tbody>
</table>
<li class="buttonBar bottom">            
        <s:submit cssClass="button" method="save" key="button.save"/>   
        <s:reset type="button" key="Reset" onmouseout="checkDate();" />   
</li>     
</div>
</s:form>

<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
</script>
<script language="javascript" type="text/javascript">
function checkDate() {
	var i;
	for(i=0;i<=1000;i++)
	{
		if(document.forms['lossForm'].elements[i].value == "null" ){
			document.forms['lossForm'].elements[i].value="";
		}		
	}		
}
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript">   
    Form.focusFirstElement($("lossForm"));
</script>  		