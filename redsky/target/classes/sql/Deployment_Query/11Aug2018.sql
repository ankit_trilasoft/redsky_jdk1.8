// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;


ALTER TABLE `redsky`.`scheduleresourceoperation` ADD COLUMN `createdBy` VARCHAR(82) NULL  AFTER `assignedCrewForTruck` ;
ALTER TABLE `redsky`.`scheduleresourceoperation` ADD COLUMN `createdOn` DATETIME NULL  AFTER `createdBy` ;
ALTER TABLE `redsky`.`scheduleresourceoperation` ADD COLUMN `updatedBy` VARCHAR(82) NULL  AFTER `createdOn` ;
ALTER TABLE `redsky`.`scheduleresourceoperation` ADD COLUMN `updatedOn` DATETIME NULL  AFTER `updatedBy` ;

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,status)values
('TSFT', 'Removal of household goods', 'Removal of household goods', 30, 'BENEFITTYPE','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP','Active');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,status)values
('UTSI', 'International insurance', 'International insurance', 30, 'BENEFITTYPE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Active'),
('UTSI', 'International pension', 'International pension', 30, 'BENEFITTYPE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Active'),
('UTSI', 'Education advice', 'Education advice', 30, 'BENEFITTYPE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Active'),
('UTSI', 'Partner support', 'Partner support', 30, 'BENEFITTYPE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Active'),
('UTSI', 'Tax support', 'Tax support', 30, 'BENEFITTYPE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Active'),
('UTSI', 'Removal of household goods', 'Removal of household goods', 30, 'BENEFITTYPE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Active');

Alter table partnerprivate modify accountHolder varchar(82) ,modify accountManager varchar(82) , modify claimsUser varchar(82) , modify coordinator varchar(82) ;

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby,status)values
('UTSI','INS','International Insurance', 5, 'SERVICE','en','Relo','',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Active'),
('VOER','INS','International Insurance', 5, 'SERVICE','en','Relo','',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Active');

Insert into dspnetworkfield(corpid,createdon,createdby,updatedon,updatedby,tableName,type,fieldName)values
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INS','INS_vendorCode'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INS','INS_vendorName'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INS','INS_vendorInitiation'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INS','INS_vendorCodeEXSO'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INS','INS_serviceStartDate'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INS','INS_vendorContact'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INS','INS_serviceEndDate'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INS','INS_vendorEmail'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INS','INS_comment');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby,status)values
('UTSI','INP','International Pension', 5, 'SERVICE','en','Relo','',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Active'),
('VOER','INP','International Pension', 5, 'SERVICE','en','Relo','',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Active');

Insert into dspnetworkfield(corpid,createdon,createdby,updatedon,updatedby,tableName,type,fieldName)values
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INP','INP_vendorCode'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INP','INP_vendorName'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INP','INP_vendorInitiation'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INP','INP_vendorCodeEXSO'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INP','INP_serviceStartDate'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INP','INP_vendorContact'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INP','INP_serviceEndDate'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INP','INP_vendorEmail'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','INP','INP_comment');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby,status)values
('UTSI','EDA','Education Advice', 5, 'SERVICE','en','Relo','',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Active'),
('VOER','EDA','Education Advice', 5, 'SERVICE','en','Relo','',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Active');

Insert into dspnetworkfield(corpid,createdon,createdby,updatedon,updatedby,tableName,type,fieldName)values
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','EDA','EDA_vendorCode'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','EDA','EDA_vendorName'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','EDA','EDA_vendorInitiation'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','EDA','EDA_vendorCodeEXSO'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','EDA','EDA_serviceStartDate'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','EDA','EDA_vendorContact'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','EDA','EDA_serviceEndDate'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','EDA','EDA_vendorEmail'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','EDA','EDA_comment');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby,status)values
('UTSI','TAS','Tax Support', 5, 'SERVICE','en','Relo','',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Active'),
('VOER','TAS','Tax Support', 5, 'SERVICE','en','Relo','',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Active');

Insert into dspnetworkfield(corpid,createdon,createdby,updatedon,updatedby,tableName,type,fieldName)values
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','TAS','TAS_vendorCode'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','TAS','TAS_vendorName'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','TAS','TAS_vendorInitiation'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','TAS','TAS_vendorCodeEXSO'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','TAS','TAS_serviceStartDate'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','TAS','TAS_vendorContact'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','TAS','TAS_serviceEndDate'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','TAS','TAS_vendorEmail'),
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','TAS','TAS_comment');

Insert into dspnetworkfield(corpid,createdon,createdby,updatedon,updatedby,tableName,type,fieldName)values
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','SPA','SPA_vendorInitiation');

Insert into dspnetworkfield(corpid,createdon,createdby,updatedon,updatedby,tableName,type,fieldName)values
('TSFT', now(),'rchandra', now(),'rchandra','dspdetails','MMG','MMG_vendorInitiation');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,status)values
('TSFT', 'Single with children', 'Single with children', 30, 'FamilySituation','en',now(),'rchandra',now(),'rchandra','Active');


ALTER TABLE `redsky`.`dspdetails` 
ADD COLUMN `INS_vendorCode` VARCHAR(25) NULL  AFTER `TEN_housingRentVatPercent` ,
ADD COLUMN `INS_vendorName` VARCHAR(255) NULL  AFTER `INS_vendorCode` ,
ADD COLUMN `INS_vendorCodeEXSO` VARCHAR(25) NULL  AFTER `INS_vendorName` ,
ADD COLUMN `INS_vendorEmail` VARCHAR(255) NULL  AFTER `INS_vendorCodeEXSO` ,
ADD COLUMN `INS_vendorContact` VARCHAR(255) NULL  AFTER `INS_vendorEmail` ,
ADD COLUMN `INS_comment` MEDIUMTEXT NULL  AFTER `INS_vendorContact` ,
ADD COLUMN `INS_vendorInitiation` DATETIME NULL  AFTER `INS_comment` ,
ADD COLUMN `INS_serviceStartDate` DATETIME NULL  AFTER `INS_vendorInitiation` ,
ADD COLUMN `INS_serviceEndDate` DATETIME NULL  AFTER `INS_serviceStartDate` ,
ADD COLUMN `INP_vendorCode` VARCHAR(25) NULL  AFTER `INS_serviceEndDate` ,
ADD COLUMN `INP_vendorName` VARCHAR(255) NULL  AFTER `INP_vendorCode` ,
ADD COLUMN `INP_vendorInitiation` DATETIME NULL  AFTER `INP_vendorName` ,
ADD COLUMN `INP_vendorCodeEXSO` VARCHAR(25) NULL  AFTER `INP_vendorInitiation` ,
ADD COLUMN `INP_serviceStartDate` DATETIME NULL  AFTER `INP_vendorCodeEXSO` ,
ADD COLUMN `INP_vendorContact` VARCHAR(255) NULL  AFTER `INP_serviceStartDate` ,
ADD COLUMN `INP_serviceEndDate` DATETIME NULL  AFTER `INP_vendorContact` ,
ADD COLUMN `INP_vendorEmail` VARCHAR(255) NULL  AFTER `INP_serviceEndDate` ,
ADD COLUMN `INP_comment` MEDIUMTEXT NULL  AFTER `INP_vendorEmail` ,
ADD COLUMN `EDA_vendorCode` VARCHAR(25) NULL  AFTER `INP_comment` ,
ADD COLUMN `EDA_vendorName` VARCHAR(255) NULL  AFTER `EDA_vendorCode` ,
ADD COLUMN `EDA_vendorInitiation` DATETIME NULL  AFTER `EDA_vendorName` ,
ADD COLUMN `EDA_vendorCodeEXSO` VARCHAR(25) NULL  AFTER `EDA_vendorInitiation` ,
ADD COLUMN `EDA_serviceStartDate` DATETIME NULL  AFTER `EDA_vendorCodeEXSO` ,
ADD COLUMN `EDA_vendorContact` VARCHAR(255) NULL  AFTER `EDA_serviceStartDate` ,
ADD COLUMN `EDA_serviceEndDate` DATETIME NULL  AFTER `EDA_vendorContact` ,
ADD COLUMN `EDA_vendorEmail` VARCHAR(255) NULL  AFTER `EDA_serviceEndDate` ,
ADD COLUMN `EDA_comment` MEDIUMTEXT NULL  AFTER `EDA_vendorEmail` ,
ADD COLUMN `SPA_vendorInitiation` DATETIME NULL  AFTER `EDA_comment` ,
ADD COLUMN `TAS_vendorCode` VARCHAR(25) NULL  AFTER `SPA_vendorInitiation` ,
ADD COLUMN `TAS_vendorName` VARCHAR(255) NULL  AFTER `TAS_vendorCode` ,
ADD COLUMN `TAS_vendorInitiation` DATETIME NULL  AFTER `TAS_vendorName` ,
ADD COLUMN `TAS_vendorCodeEXSO` VARCHAR(25) NULL  AFTER `TAS_vendorInitiation` ,
ADD COLUMN `TAS_serviceStartDate` DATETIME NULL  AFTER `TAS_vendorCodeEXSO` ,
ADD COLUMN `TAS_vendorContact` VARCHAR(255) NULL  AFTER `TAS_serviceStartDate` ,
ADD COLUMN `TAS_serviceEndDate` DATETIME NULL  AFTER `TAS_vendorContact` ,
ADD COLUMN `TAS_vendorEmail` VARCHAR(255) NULL  AFTER `TAS_serviceEndDate` ,
ADD COLUMN `TAS_comment` MEDIUMTEXT NULL  AFTER `TAS_vendorEmail`,
ADD COLUMN `MMG_vendorInitiation` DATETIME NULL  AFTER `TAS_comment` ;

delete from networkdatafields where fieldname ='customerEmployer' and id=12 ;

ALTER TABLE `redsky`.`serviceorder`
ADD COLUMN `INS_vendorCode` VARCHAR(25) NULL ,
ADD COLUMN `INP_vendorCode` VARCHAR(25) NULL ,
ADD COLUMN `EDA_vendorCode` VARCHAR(25) NULL ,
ADD COLUMN `TAS_vendorCode` VARCHAR(25) NULL ;

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,bucket,createdon,createdby,updatedon,updatedby,status)values
('UTSI','serviceorder','serviceorder', 25, 'DATA_SECURITY_TABLE','en','INS_vendorCode',now(),'rchandra',now(),'rchandra','Active'),
('VOER','serviceorder','serviceorder', 25, 'DATA_SECURITY_TABLE','en','INS_vendorCode',now(),'rchandra',now(),'rchandra','Active'),
('UTSI','serviceorder','serviceorder', 25, 'DATA_SECURITY_TABLE','en','INP_vendorCode',now(),'rchandra',now(),'rchandra','Active'),
('VOER','serviceorder','serviceorder', 25, 'DATA_SECURITY_TABLE','en','INP_vendorCode',now(),'rchandra',now(),'rchandra','Active'),
('UTSI','serviceorder','serviceorder', 25, 'DATA_SECURITY_TABLE','en','EDA_vendorCode',now(),'rchandra',now(),'rchandra','Active'),
('VOER','serviceorder','serviceorder', 25, 'DATA_SECURITY_TABLE','en','EDA_vendorCode',now(),'rchandra',now(),'rchandra','Active'),
('UTSI','serviceorder','serviceorder', 25, 'DATA_SECURITY_TABLE','en','TAS_vendorCode',now(),'rchandra',now(),'rchandra','Active'),
('VOER','serviceorder','serviceorder', 25, 'DATA_SECURITY_TABLE','en','TAS_vendorCode',now(),'rchandra',now(),'rchandra','Active');

CREATE  TABLE `redsky`.`userdevice` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `userName` VARCHAR(82) NULL ,
  `deviceId` VARCHAR(50) NULL ,
  `createdOn` DATETIME NULL , 
  `userLogFileId` BIGINT(20) NULL ,
  `corpID` VARCHAR(8) NULL ,
  PRIMARY KEY (`id`) );
  
  ALTER TABLE `redsky`.`customerfile` ADD COLUMN `jimExtract` BIT(1) NULL DEFAULT b'0'  AFTER `surveyObservation` ;
ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `jimExtract` BIT(1) NULL DEFAULT b'0'  AFTER `TAS_vendorCode` ;

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby,status)values
('UTSI','DsSpousalAssistance','DsSpousalAssistance', 20, 'NOTESUBTYPE','en','Service Order',now(),'rchandra',now(),'rchandra','Active'),
('VOER','DsSpousalAssistance','DsSpousalAssistance', 20, 'NOTESUBTYPE','en','Service Order',now(),'rchandra',now(),'rchandra','Active');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby,status)values
('UTSI','DSInternationalInsurance','DSInternationalInsurance', 20, 'NOTESUBTYPE','en','Service Order',now(),'rchandra',now(),'rchandra','Active'),
('VOER','DSInternationalInsurance','DSInternationalInsurance', 20, 'NOTESUBTYPE','en','Service Order',now(),'rchandra',now(),'rchandra','Active');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby,status)values
('UTSI','DsInternationalPension','DsInternationalPension', 20, 'NOTESUBTYPE','en','Service Order',now(),'rchandra',now(),'rchandra','Active'),
('VOER','DsInternationalPension','DsInternationalPension', 20, 'NOTESUBTYPE','en','Service Order',now(),'rchandra',now(),'rchandra','Active');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby,status)values
('UTSI','DsEducationAdvice','DsEducationAdvice', 20, 'NOTESUBTYPE','en','Service Order',now(),'rchandra',now(),'rchandra','Active'),
('VOER','DsEducationAdvice','DsEducationAdvice', 20, 'NOTESUBTYPE','en','Service Order',now(),'rchandra',now(),'rchandra','Active');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby,status)values
('UTSI','DsTaxSupport','DsTaxSupport', 20, 'NOTESUBTYPE','en','Service Order',now(),'rchandra',now(),'rchandra','Active'),
('VOER','DsTaxSupport','DsTaxSupport', 20, 'NOTESUBTYPE','en','Service Order',now(),'rchandra',now(),'rchandra','Active');


ALTER TABLE `redsky`.`customerfile` 
ADD COLUMN `originalCompanyCode` VARCHAR(10) NULL  AFTER `jimExtract` , 
ADD COLUMN `originalCompanyName` VARCHAR(255) NULL  AFTER `originalCompanyCode` , 
ADD COLUMN `originalCompanyHiringDate` DATETIME NULL  AFTER `originalCompanyName` , 
ADD COLUMN `firstInternationalMove` BIT(1) NULL  AFTER `originalCompanyHiringDate` , 
ADD COLUMN `authorizedBy` VARCHAR(30) NULL  AFTER `firstInternationalMove` , 
ADD COLUMN `authorizedPhone` VARCHAR(20) NULL  AFTER `authorizedBy` , 
ADD COLUMN `authorizedEmail` VARCHAR(65) NULL  AFTER `authorizedPhone` ;