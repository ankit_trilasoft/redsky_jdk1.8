package com.trilasoft.app.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;
import org.springframework.dao.DataAccessException;

import com.trilasoft.app.dao.SSOLoginTokenDao;
import com.trilasoft.app.model.Userlogfile;
import com.trilasoft.app.service.ClaimManager;

public class SSOLoginTokenDaoHibernate extends GenericDaoHibernate<Userlogfile, Long> implements SSOLoginTokenDao {
	
	private static Logger logger = Logger.getLogger(SSOLoginTokenDaoHibernate.class);
	private ClaimManager claimManager;
	
	public ClaimManager getClaimManager() {
		return claimManager;
	}

	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}

	public SSOLoginTokenDaoHibernate(){
		super(Userlogfile.class);
	}
	
	public SSOLoginTokenDaoHibernate(Class<Userlogfile> persistentClass) {
		super(persistentClass);
	}
 
	public void registerLoginToken(String sessionId, String loginToken, String username) throws Exception {
		
		try {
			List<Userlogfile> userlogfileList = (List<Userlogfile>) getHibernateTemplate().find("from Userlogfile where sessionId='"+sessionId+"'");
			logger.info("In Dao registering token "+loginToken+" the User Log list is "+userlogfileList);
			if(userlogfileList==null || (userlogfileList!=null && userlogfileList.size()==0)){
				User user = (User) claimManager.findUserDetails(username).get(0);
				Userlogfile userlogfile=new Userlogfile();
				userlogfile.setUserName(user.getUsername());
				userlogfile.setUserType(user.getUserType());
				userlogfile.setCorpID(user.getCorpID()); 
				userlogfile.setIPaddress("127.0.0.1");
				userlogfile.setDNS("127.0.0.1");
				userlogfile.setLogin(new Date());
				userlogfile.setSessionId(sessionId);
				userlogfile.setSsoToken(loginToken);
				save(userlogfile);
				logger.info("User log entry did not exist, creating a new entry for "+username+" login token "+loginToken);
			}else{
				logger.info("User log entry found, creating a new login token "+loginToken);
				Userlogfile userlogfile = userlogfileList.get(0);
				userlogfile.setSsoToken(loginToken);
				save(userlogfile);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			throw new Exception("Error creating login token in Dao for user "+username+" and session id "+sessionId);
		}
		
	}

}
