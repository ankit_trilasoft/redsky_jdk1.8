package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

public interface AppInitServletManager extends GenericManager {

	public void populatePermissionCache(Map  pageActionUrls);
	
	public void populateRolebasedComponentPermissions(Map permissionMap);
	
	public List getListForSurveyCalander(String corpid,String fmdt,String todt,String consult,String surveycity,String job);

	public void populatePageFieldVisibilityPermissions(Map pageFieldVisibilityComponentMap);
}
