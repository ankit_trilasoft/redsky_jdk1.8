<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Print Daily Package Details</title>   
    <meta name="heading" content="Entitlement"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
    <script type="text/javascript">

    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }
   	function validation(){
        return true;
   	}
    	
    </script>
</head>

<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}

#loader {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>


<c:set var="buttons"> 
    <input name="save" id="save" type="submit"" class="cssbutton1" onclick="return validation();" value="Save" style="width:55px; height:25px" tabindex="83"/> 
</c:set>

<s:form id="printDailyForm" action="savePrintDailyPackage.html" method="post" validate="true">
<s:hidden name="printDailyPackage.id" />
<s:hidden name="id" value="${printDailyPackage.id}"/>  
<s:hidden name="tempId1"/>
<s:hidden name="tempId2"/>

<div id="layer1" style="width:100%"> 
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Print Daily Package</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top" >
    <div class="top" style="margin-top:10px;!margin-top:-5px; "><span></span></div>
    <div class="center-content">
    
 <table class="table" style="width:99%;margin-bottom:0.3em;" border="0">
<thead>
<tr>
<th>Module Name<font color="RED">*</font></th>
<th>Form Name<font color="RED">*</font></th>
<th>One per S/O per day</th>
<th>Print Seq<font color="RED">*</font></th>
<th>#&nbsp;of&nbsp;Copies<font color="RED">*</font></th>
<th>WT Service Type</th>
<th>S/O Mode</th>
<th>Military</th>
<configByCorp:fieldVisibility componentId="component.field.PrintDailyPackage.jobBillToCode">
<th>Job Type</th>
<th>Bill To Code</th>
</configByCorp:fieldVisibility>
</tr>
</thead>	
   <tbody>
	<tr>
		<td width="" align="left">
			    <s:select name="printDailyPackage.module" list="%{moduleReport}" onchange="getReportNameList(this.value);" headerKey="" headerValue="" cssStyle="width:100px;" cssClass="list-menu" />
		</td>
			
		<td width="" align="left" >
			    <s:autocompleter name="printDailyPackage.formName" onchange="getBillToCodeList();" cssStyle="width:150px;" cssClass="list-menu" > </s:autocompleter>
			</td>
			<td width="" align="left">
			    <s:select name="printDailyPackage.onePerSOperDay" list="%{yesno}" cssStyle="width:65px;" cssClass="list-menu" />
			</td>
			<td width="" align="left">
			   <s:textfield name="printDailyPackage.printSeq" onchange="checkNumber('printDailyPackage.printSeq');" cssClass="input-text" size="5" maxlength="2"/>
			</td>
			<td width="" align="left">
			    <s:textfield name="printDailyPackage.noOfCopies" onchange="checkNumber('printDailyPackage.noOfCopies');" cssClass="input-text" size="5" maxlength="2"/>
			</td>
			<td width="" align="left">
			    <s:select name="printDailyPackage.wtServiceType" list="%{wtServiceType}" value="%{multiplServiceTypes}" multiple="true" cssClass="list-menu" cssStyle="width:150px; height:60px" headerKey="" headerValue="" />
			</td>
			<td width="" align="left">
			   <s:select name="printDailyPackage.mode" list="%{mode}" cssClass="list-menu" cssStyle="width:50px;"/>
			</td>
			<td width="" align="left">
			    <s:select name="printDailyPackage.military" list="%{yesno}" cssStyle="width:50px;" cssClass="list-menu" />
			</td>
			<configByCorp:fieldVisibility componentId="component.field.PrintDailyPackage.jobBillToCode">
			<td width="" align="left">
			   <s:select name="printDailyPackage.job" list="%{jobs}" value="%{multiplJobType}" multiple="true" cssClass="list-menu" cssStyle="width:120px;height:60px" headerKey="" headerValue=""/>
			</td>
			<td width="" align="left">
			   <s:select name="printDailyPackage.billToCode" list="%{''}" multiple="true" cssClass="list-menu" cssStyle="width:100px; height:50px" headerKey="" headerValue="" />
			</td>
			</configByCorp:fieldVisibility>
		</tr>						
		</tbody>
	</table>
<table id="parameterTable" style="display: none;width:630px;margin:0px;padding:0px;" border="0" >	
	 <tr>
		  <td colspan="11">
		    <fieldset style="margin-bottom:4px;margin-top:8px; padding:5px; width:520px;">
		    <legend>Parameters&nbsp;</legend>
		  	<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
			  <tr>	
				  <td align="right" class="listwhitebox" width="5%" valign="middle">Ware House</td>
				  <td align="left" width="3%" ><s:checkbox id="WH" name="parameter" value="WH" tabindex="3" /></td>
				  <td align="right" class="listwhitebox" width="10%" valign="middle">Begin Date</td>
				  <td align="left" width="3%" ><s:checkbox id="BeginDate" name="parameter" value="BeginDate" tabindex="4" /></td>
				  <td align="right" class="listwhitebox" width="10%" valign="middle">End Date</td>
				  <td align="left" width="3%" ><s:checkbox id="EndDate" name="parameter" value="EndDate" tabindex="5" /></td>
			  </tr>
			  <tr>	  
				  <td align="right" class="listwhitebox" valign="middle">Work Ticket</td>
				  <td align="left"  width="3%"><s:checkbox id="WorkTicket" name="parameter" value="WorkTicket"  tabindex="6" /></td>
				  <td align="right" class="listwhitebox" width="10%" valign="middle">CorpId</td>
				  <td align="left" width="3%" ><s:checkbox id="corpId" name="parameter" value="corpId" tabindex="3" /></td>
				  <td align="right" class="listwhitebox" width="5%" valign="middle">SO#</td>
				  <td align="left"  width="3%"><s:checkbox id="SO" name="parameter" value="SO" tabindex="7" /></td>
				 				 
			 </tr>
			 
		    </table>
		 </fieldset>
		 </td>
		 <td align="right" class="listwhitebox" valign="middle">
		  	<input type="button" class="cssbutton1" onclick="getParameter();" value="Add" style="width:65px; height:25px" />
		  	</td>
		  	<td align="left">
		  	<input type="button" class="cssbutton1" onclick="Divclose();" value="Close" style="width:65px; height:25px" />
		  </td>
     </tr>
  </table> 
  
  <table class="" cellspacing="0" cellpadding="0" border="0" style="margin-top:8px;width:99%;">
	<tbody>
		<tr>
			<td colspan="5">
				<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0" style="width:100%;">
					<tbody>
					<tr><td height="3px"></td></tr>
					<tr><td align="left" colspan="10">
						<display:table name="printDailyPackageChildList" class="table" requestURI="" id="printChildList"  pagesize="15" style="width:100%;margin-bottom:5px;">
						    <display:column title="#"> 
						    	 <c:out value="${printChildList_rowNum}"/>
						    </display:column>
						    <display:column title="JrXml Name"> 
						    	<s:textfield name="jrxmlName${printChildList.id}" id='jrxmlName${printChildList.id}' onkeyup="autoCompleterAjaxCall(this.value,'jrxmlName${printChildList.id}','choices${printChildList.id}');" value="${printChildList.jrxmlName}" size="55" maxlength="100" cssClass="input-text" tabindex="1"/>
						    	<div id="choices${printChildList.id}" class="listwhitetext" onclick="savePrintChild('${printChildList.id}');" ></div>
						    </display:column>
						    <display:column title="Parameters" > 
						    	<s:textfield name="parameters${printChildList.id}" id="parameters${printChildList.id}" onclick="getParameterVisibility(this.value,'${printChildList.id}');" value="${printChildList.parameters}" readonly="true" size="30" maxlength="100" cssClass="input-text" tabindex="1"/>
						    </display:column>
						    <display:column title="Form Description" style="width:100px" >
								<textarea  name="formDesc${printChildList.id}"  id="formDesc${printChildList.id}" onchange="savePrintChild('${printChildList.id}');"  style="overflow:auto;" class="input-text" onselect="discriptionView('formDesc${printChildList.id}')" onblur="discriptionView1('formDesc${printChildList.id}')" value ="${printChildList.formDesc }"  cols="35" rows="1">${printChildList.formDesc}</textarea>
							</display:column>
						    
						    <display:column title="Remove" style="width: 50px;">
								<a><img align="middle" title="User List" onclick="confirmDeleteChild('${printChildList.parentId}','${printChildList.id}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
							</display:column>
						</display:table>
						<input type="button" class="cssbutton1" onclick="addLine();" value="Add Line" style="width:65px; height:25px" />

					</td>
					</tr>
		    		</tbody>
		    	</table>
		    </td>
		</tr>
	</tbody>
</table>
  
</div>
<div class="bottom-header" style="!margin-top:40px;"><span ></span></div>
</div>
</div> 
 
</div>
<s:submit cssClass="cssbutton1" value="Save" onclick="return validate();"></s:submit>
<input type="button" class="cssbutton1" name="back" value="Back" onclick="goBack();" />

<div id="loader" style="text-align:center; display:none">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
		<tr>
			<td align="center">
				<table cellspacing="0" cellpadding="3" align="center">
					<tr><td height="200px"></td></tr>
					<tr>
				       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
				           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Save In Progress...Please Wait</font>
				       </td>
				    </tr>
				    <tr>
				      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
				           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
				       </td>
				    </tr>
		       </table>
		     </td>
	  	</tr>
	</table>
</div>

</s:form> 
<div id="mydiv" style="position:absolute;top:110px;margin-top:-15px;"></div>


<script type="text/javascript">
var http = getHTTPObject();
var http1 = getHTTPObject();
var http2 = getHTTPObject();
var http3 = getHTTPObject();

window.onload = function() {
	<c:if test="${ not empty printDailyPackage.formName}">
		getReportNameList();
		setTimeout("setFormName()",500);
		setTimeout("setjobType()",500);
		setTimeout("getBillToCodeList()",500);

	</c:if>
	<c:if test="${ empty printDailyPackage.formName}">
		document.getElementById('printDailyForm_printDailyPackage_formName').disabled = true;
	</c:if>
}

function validate(){
	var formName = document.forms['printDailyForm'].elements['printDailyPackage.formName'].value;
	var copies = document.forms['printDailyForm'].elements['printDailyPackage.noOfCopies'].value;
	var printSeq = document.forms['printDailyForm'].elements['printDailyPackage.printSeq'].value;
	if(formName.length == 0 || copies.length == 0 || printSeq.length == 0){
		alert('Please Enter Mandatory Fields');
		return false;
	}else if(printSeq == 0){
		alert('Please Enter a valid sequence.');
		return false;
	}
}
function checkNumber(id){
	var copies = document.forms['printDailyForm'].elements[id].value;
	if(parseInt(copies) != copies || copies == 0){
		 alert('Number is not valid');
		 document.forms['printDailyForm'].elements[id].value='';
	}else{
		for(var i = 0;i < copies.length;i++){
			if(copies.charAt(i) == '.' || copies.charAt(i) == ' '){
				 alert('Number is not valid');
				 document.forms['printDailyForm'].elements[id].value='';
			}
		}
	}
}

function savePrintChild(id){
	var jrxml = document.getElementById('jrxmlName'+id).value;
	//getFormComment(jrxml);
	var parameters = document.getElementById('parameters'+id).value;
	var formName = document.getElementById('printDailyForm_printDailyPackage_formName').value;
	document.getElementById('formDesc'+id).value = formName;
	var formDesc = document.getElementById('formDesc'+id).value;
	if(jrxml != '' && parameters != ''){
		showHide("block");
		var url = "savePrintChildAjax.html?ajax=1&childId="+id+"&jrxmlName="+jrxml+"&parameters="+parameters+"&description="+formDesc;
		http.open("GET", url, true);
		http.onreadystatechange = handleHttpResponse;    //function(){ handleHttpResponse(targetField);};
		http.send(null);
	}
}

function handleHttpResponse(){
	if (http.readyState == 4){
		showHide("none");
	}
}
function getFormComment(jrxml){
	var url = "getFormCommentAjax.html?ajax=1&decorator=simple&popup=true&jrxmlName="+jrxml;
	http1.open("GET", url, true);
	http1.onreadystatechange = handleHttpResponseComment;    //function(){ handleHttpResponse(targetField);};
	http1.send(null);
}
function Divclose(){
	document.getElementById('parameterTable').style.display = "none";
}
function addLine(){
	<c:forEach var="childList" items="${printDailyPackageChildList}" varStatus="rowCounter">
		var jrxml = document.getElementById('jrxmlName${childList.id}').value;
		var parameter = document.getElementById('parameters${childList.id}').value;
		if(jrxml.length == 0 || parameter.length == 0){
			alert('Please enter Jrxml Name and parameter for line ${rowCounter.count}');
			return false;
		}
	</c:forEach>
	
	var id = '${printDailyPackage.id}';
	if(id.length == 0){
		alert('Please Save Print Daily Package first before adding parameter & Jrxml Name.');
		return false;
	}else{
		var url = 'addPrintDailyPackageChild.html?id=${printDailyPackage.id}';
		location.href = url;
	}
}

function confirmDeleteChild(parentId,id){
	var agree=confirm("Are you sure you wish to remove ?");
	if (agree){
		var url = "deletePrintPackageChild.html?id="+parentId+"&childId="+id;
		location.href = url;
	 }else{
		return false;
	}
}

function autoCompleterAjaxCall(jrxmlName,id,divId){
	document.getElementById('printDailyForm').setAttribute("AutoComplete","off");
	document.forms['printDailyForm'].elements['tempId1'].value=id;
	document.forms['printDailyForm'].elements['tempId2'].value=divId;
	
	 new Ajax.Request('/redsky/printPackageAutocomplete.html?ajax=1&jrxmlName='+jrxmlName+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
		    
			      var response = transport.responseText || "no response text";
//			      alert(response);
//					alert(divId);
					
                  var mydiv = document.getElementById(divId);
                  document.getElementById(divId).style.display = "block";                   
                  mydiv.innerHTML = response;
                  mydiv.show(); 
			    },
			    onFailure: function(){ 
				    }
			  });
}

function checkValue(result){
	var targetID=document.forms['printDailyForm'].elements['tempId1'].value;
	var targetID2=document.forms['printDailyForm'].elements['tempId2'].value;
	
	document.getElementById(targetID).value=result.innerHTML;
	document.getElementById(targetID2).style.display = "none";
}

var paramID='';
var globalID = '';
function getParameterVisibility(value,id){
	paramID = 'parameters'+id;
	globalID = id;
	document.getElementById('parameterTable').style.display = 'block';
	
	var chk = document.getElementsByName('parameter');
    for(i=0;i<chk.length;i++){
    	document.getElementById(chk[i].id).checked=false;
    }
    
	var str = value.split(";");
	for(i=0;i<str.length;i++){
		if(str[i] != ''){
			document.getElementById(str[i]).checked=true;
		}
    }
}

function getParameter(){
	var param='';
	var chk = document.getElementsByName('parameter');
    for(i=0;i<chk.length;i++){
        if(chk[i].checked){
        	param = chk[i].id+';'+param;
        }
    }
	document.getElementById(paramID).value = param;
	document.getElementById('parameterTable').style.display = 'none';
	if(param != ''){
		savePrintChild(globalID);
	}
}

function goBack(){
	var url = "printSetUp.html";
	location.href = url;
}

function discriptionView(discription){ 
	 document.getElementById(discription).style.height="50px"; 
} 
function discriptionView1(discription){ 
	 document.getElementById(discription).style.height="15px"; 
}
function showHide(action){
	document.getElementById("loader").style.display = action;
}

function getReportNameList(str){
	var module = document.getElementById('printDailyForm_printDailyPackage_module').value;
	if(module.trim() != ''){
		document.getElementById('printDailyForm_printDailyPackage_formName').disabled = false;
		var url = "getReportNamesAjax.html?ajax=1&decorator=simple&popup=true&module="+module;
		http2.open("GET", url, true);
		http2.onreadystatechange = handleHttpResponseNames;
		http2.send(null);
	}else{
		document.getElementById('printDailyForm_printDailyPackage_formName').value = '';
		document.getElementById('printDailyForm_printDailyPackage_formName').disabled = true;
	}
}
function handleHttpResponseNames(){
	if (http2.readyState == 4){
		var results = http2.responseText;
	      results = results.trim();
	      if(results.indexOf(",")>-1){
		      	results="'',"+results;	  
		   }
	      var res = results.split(",");
	      var formName = document.getElementById('printDailyForm_printDailyPackage_formName');
	      formName.length = res.length;
	      document.getElementById('printDailyForm_printDailyPackage_formName').options[0].text = '';
		  document.getElementById('printDailyForm_printDailyPackage_formName').options[0].value = '';
	      for(i=1;i<=res.length;i++){
	    	  if(res[i] != undefined){
	    		  document.getElementById('printDailyForm_printDailyPackage_formName').options[i].text = res[i];
	    		  document.getElementById('printDailyForm_printDailyPackage_formName').options[i].value = res[i];
	    	  }
	      }
	}
}
function setFormName(){
	document.getElementById('printDailyForm_printDailyPackage_formName').value = '${printDailyPackage.formName}';
}
function setjobType(){
	document.getElementById('printDailyForm_printDailyPackage_job').value = '${printDailyPackage.job}';
}

function getBillToCodeList(){
	<configByCorp:fieldVisibility componentId="component.field.PrintDailyPackage.jobBillToCode">
		var formName = document.getElementById('printDailyForm_printDailyPackage_formName').value;
		if(formName.trim() != ''){
			document.getElementById('printDailyForm_printDailyPackage_billToCode').disabled = false;
			var url = "getBillToCodeAjax.html?ajax=1&decorator=simple&popup=true&formName="+formName;
			http3.open("GET", url, true);
			http3.onreadystatechange = handleHttpResponseNames1;
			http3.send(null);
		}else{
			document.getElementById('printDailyForm_printDailyPackage_billToCode').value = '';
			document.getElementById('printDailyForm_printDailyPackage_billToCode').disabled = true;
		}
	</configByCorp:fieldVisibility>
}
function handleHttpResponseNames1(){
	if (http3.readyState == 4){
		var results = http3.responseText;
	      results = results.trim();
	      if(results.indexOf(",")>-1){
	      	results="'',"+results;	  
	      }    
	      var res = results.split(",");
	      var formName = document.getElementById('printDailyForm_printDailyPackage_billToCode');	     
	      if(res.length==1){
		      formName.length = 2;
    		  document.getElementById('printDailyForm_printDailyPackage_billToCode').options[1].text = res;
    		  document.getElementById('printDailyForm_printDailyPackage_billToCode').options[1].value = res;	    	 
    		      	 		      
	      }else{	     
		      formName.length = res.length;				     
		      for(i=1;i<res.length;i++){	    	  
		    		  document.getElementById('printDailyForm_printDailyPackage_billToCode').options[i].text = res[i];
		    		  document.getElementById('printDailyForm_printDailyPackage_billToCode').options[i].value = res[i];			    		  	 
		      }		     
	      }
	      if(res==''){
	    	  document.getElementById('printDailyForm_printDailyPackage_billToCode').disabled = true;
		     }
			
			var bCodeDB = '${printDailyPackage.billToCode}';	
			var bCodeForm = document.getElementById("printDailyForm_printDailyPackage_billToCode");
			  for (var i = 1; i < bCodeForm.options.length; i++) {
				  var temp = bCodeForm.options[i].value;
				  if(bCodeDB.indexOf(temp)>-1){
					  bCodeForm.options[i].selected =true;
			      }
			  }
	}
}
try{	
	<c:if test="${ empty printDailyPackage.billToCode}">
		document.getElementById('printDailyForm_printDailyPackage_billToCode').disabled = true;
	</c:if>
}catch(e){} 
</script>
	