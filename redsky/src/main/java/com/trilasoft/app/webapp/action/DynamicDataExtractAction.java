package com.trilasoft.app.webapp.action;

import static java.lang.System.out;

import java.io.*;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.text.*;
import java.util.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;  

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.DTO;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.DTOStorageAnalysis;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.DTOUserDataSecurity;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.stoExtractDTO;
import com.trilasoft.app.dao.hibernate.dto.HibernateDTO;
import com.trilasoft.app.model.ExtractQueryFile;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.ExtractColumnMgmtManager;
import com.trilasoft.app.service.ExtractQueryFileManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.impl.ExtractQueryFileManagerImpl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Random;
import java.util.zip.*; 

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.appfuse.Constants;
import org.appfuse.util.StringUtil;
import org.appfuse.model.Address;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder; 
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import org.bouncycastle.asn1.ocsp.Request;
import org.displaytag.properties.SortOrderEnum;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CompanyDivision;
import com.trilasoft.app.model.ContractPolicy;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.ExtractedFileLog;
import com.trilasoft.app.model.ImfEntitlements;
import com.trilasoft.app.model.PartnerQuote;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.RefZipGeoCodeMap;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.AddressManager;
import com.trilasoft.app.service.ClaimManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ContractPolicyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.ExtractedFileLogManager;
import com.trilasoft.app.service.ImfEntitlementsManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ReportsManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ToDoRuleManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;
import com.trilasoft.app.webapp.helper.SearchCriterion;

import javax.servlet.*;
import javax.servlet.http.*; 

import org.w3c.dom.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;  
public class DynamicDataExtractAction extends BaseAction {

	private Long id;

	private Date stoRecInvoiceDate;
	
	private Date beginDate;

	private Date endDate;

	private Date createDate;

	private Date toCreateDate;
	
	private Date updateDate;

	private Date toUpdateDate;

	private Date deliveryTBeginDate;

	private Date deliveryTEndDate;

	private Date deliveryActBeginDate;

	private Date deliveryActEndDate;

	private Date loadTgtBeginDate;

	private Date loadTgtEndDate;

	private Date invPostingBeginDate;

	private Date invPostingEndDate;
	
	private Date payPostingBeginDate;
	
	private Date payPostingEndDate;
	
    private Date receivedBeginDate;
	
	private Date receivedEndDate;
	
	private Date postingPeriodBeginDate;
		
    private Date postingPeriodEndDate;
	
	private Date invoicingBeginDate;

	private Date invoicingEndDate;

	private Date revenueRecognitionBeginDate;

	private Date revenueRecognitionEndDate;
	
	private Date storageBeginDate;

	private Date storageEndDate;

	private List list;
	private String routingTypes;
	

	private String loadTargetDatePeriod;
	
	private String loadActualDatePeriod;
	
	private String deliveryTargetDatePeriod;
	
	private String deliveryActualDatePeriod;
	private String invoicePostingDatePeriod;
	private String payablePostingDatePeriod;
	private String revenueRecognitionPeriod;
	private String InvoicingdatePeriod;
	private String storageDatePeriod;
	private String createDatePeriod;
	private String updateDatePeriod;
	private String PostingPeriodDatePeriod;
	private String receivedDatePeriod;
	private String OriginGisOutput;
	private String OriginGisOutputCount;
	private String stingZipCodeOutput;
	private String jobTypes;

	private String billToCodeType;
	
	private String accountCodeCondition;
	
	private String accountCodeType;

	private String bookingCodeType;

	private String modeType;

	private String packModeType;

	private String commodityType;

	private String coordinatorType;
	
	private String consultantType;

	private String salesmanType;

	private String oACountryType;

	private Date createdOnType;

	private String dACountryType;

	private String actualWgtType;

	private String actualVolumeType;

	private String conditionA1;

	private String conditionA2;

	private String conditionA3;

	private String conditionA4;

	private String conditionA5;

	private String beginDateCondition;

	private String endDateCondition;

	private String deliveryTBeginDateCondition;

	private String deliveryTEndDateCondition;

	private String deliveryActBeginDateCondition;

	private String deliveryActEndDateCondition;

	private String loadTgtBeginDateCondition;

	private String loadTgtEndDateCondition;

	private String invPostingBeginDateCondition;

	private String invPostingEndDateCondition;
	
	private String payPostingBeginDateCondition;
	
	private String payPostingEndDateCondition;

    private String receivedBeginDateCondition;
	
	private String receivedEndDateCondition;
	
    private String periodBeginDateCondition;
	
	private String periodEndDateCondition;
	
	private String invoicingBeginDateCondition;

	private String invoicingEndDateCondition;
	
	private String storageBeginDateCondition;
	
	private String storageEndDateCondition;
	
	private String revRecgBeginDateCondition;
	
	private String revRecgEndDateCondition;

	private String jobTypesCondition;

	private String billToCodeCondition;

	private String bookingCodeCondition;

	private String routingCondition;

	private String modeCondition;

	private String packModeCondition;

	private String commodityCondition;

	private String coordinatorCondition;
	
	private String consultantCondition;

	private String salesmanCondition;

	private String oACountryCondition;

	private String dACountryCondition;

	private String conditionC122;

	private String companyCodeCondition;

	private String actualVolumeCondition;
	private String actualWgtCondition; 
	private String createDateCondition;

	private String toCreateDateCondition;
	private String updateDateCondition;

	private String toUpdateDateCondition;

	private String conditionC79;
	
	private String conditionC80;
	private String publicPrivateFlag;
	private String queryName;
	private String modifiedby;
	private String querycreatedBy;
	private Date querycreatedOn;
	private String querycreatedOn1;
	private String conditionC81;
	
	private String conditionC82;
	private String companyCode;
    private String extractVal;
	private String activeStatus;
	
	private String accountInterface;
	private String companyDivision;
	private Date date;

	private List companyCodeList;
	
	private List queryNameList;

	private String companyDivisionFlag;

	private List imfEstimate;

	private String targetActualW;

	private String serviceW;

	private String warehouseW;

	private String track;

	private String balance;
	
	private String selectCondition;
	
	private String reportName;
	
	private String queryScheduler;
	
	private String perWeekScheduler;
	
	private String perMonthScheduler;
	
	private String perQuarterScheduler;
	
	private String email;
	private String lastEmailSent;
	private String emailStatus;
	private String emailMessage;
	private String datePeriod;
	
	private Date emailDateTime;
	private String emailDateTimeString;
	private String useremail;
	private boolean autoGenerator;
	
	private Date beginDateW;

	private Date endDateW;

	private Date storagedate;

	public  Map<String, String> jobtype=new HashMap<String, String>();
	
	public  Map<String, String> periodScheduler =new TreeMap();
	
	public  Map<String, String> querySchedulerList=new TreeMap();
	
	public  Map<String, String> perWeekSchedulerList=new TreeMap();
	
	public  Map<Integer, Integer> perMonthSchedulerList=new TreeMap<Integer, Integer>();
	
	public  Map<String, String> perQuarterSchedulerList;
	public  Map<String, String> routing;

	private  Map<String, String> ocountry;

	private  Map<String, String> dcountry;

	private  Map<String, String> commodit;

	private  Map<String, String> pkmode;

	private  Map<String, String> sale = new LinkedHashMap<String, String>();
	private  Map<String, String> coord = new LinkedHashMap<String, String>();
	
	private  Map<String, String> tcktactn;

	private  Map<String, String> tcktservc;

	private  Map<String, String> house;

	private Long timeDiff;

	//	 private static Map<String, String> mode;
	private  List mode;

	private  List bookingCode;
	
	private  List multiplejobType;

	private  List billToCode;
	private List queryList= new ArrayList();
	private List activeUsOriginGisOutput;
	private List activeUsDestGisOutput; 
	private List columnList;
	
	private List columnList1= new ArrayList();
	private List columnList2= new ArrayList();
	private List columnList3= new ArrayList();
	private List columnList4= new ArrayList();
	private List columnList5= new ArrayList();
	private String sessionCorpID;

	private RefMasterManager refMasterManager;

	private RefMaster refMaster;
	private RefZipGeoCodeMap refZipGeoCodeMap;
	private ServiceOrderManager serviceOrderManager;

	private CustomerFileManager customerFileManager;
	
	private ExtractColumnMgmtManager  extractColumnMgmtManager;

	private CompanyDivisionManager companyDivisionManager;
	private ExtractQueryFile extractQueryFile;
    private ExtractQueryFileManager extractManager;

	private ClaimManager claimManager;
	private User user;

	private List imageURLList=new ArrayList();
	
	private List dataExtractMailInfo=new ArrayList();

	private String mailStatusReturn;
	
	private Date bookingDate;
	private Date toBookingDate;
	private String bookingDatePeriod;
	private String toBookingDateCondition;
	private String bookingDateCondition;
	private String carrierCondition;
	private String driverIdCondition;
	private String carrier;
	private String driverId;
	private Company company;
	private CompanyManager companyManager;
	private ExtractedFileLogManager extractedFileLogManager;
	private ExtractedFileLog extractedFileLog;
	private boolean extractFileAudit=false;
	public final static int BUFFER = 102400000;
	private String originAgentCondition;
	private String destinationAgentCondition;
	private String originAgentCodeType;
	private String destinationAgentCodeType;

	
    public String extractSupport(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		user = (User) auth.getPrincipal();
    	useremail=user.getEmail();
    	getComboList(sessionCorpID);
		return SUCCESS;
	}
    
	public DynamicDataExtractAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	
	public String findDataExtractMailInfo(){
		//System.out.println("\n\n\n\n\n findDataExtractMailInfo id"+id);
		dataExtractMailInfo =extractManager.findDataExtractMailInfo(id) ;  
		return SUCCESS;
	}
	private String agentParentForPortal="";
	private String salesPerson="";
	private boolean armstrongPortal=false;
	public String edit() {

		companyDivisionFlag = companyDivisionManager.findCompanyDivFlag(sessionCorpID).get(0).toString();
		accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString(); 
		companyCodeList = companyDivisionManager.findCompanyCodeList(sessionCorpID);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		activeStatus = user.getExtractJobType();
		agentParentForPortal=user.getParentAgent();
		salesPerson=user.getUsername().toUpperCase();
		armstrongPortal=user.getSalesPortalAccess();
		//System.out.println(activeStatus);

		getComboList(sessionCorpID);
		return SUCCESS;
	} 
	
	public String saveDataExtractQuery(){
		
		if(selectCondition.equals("")||selectCondition.equals("#"))
	  	  {
	  	  }
	  	 else
	  	  {
	  		selectCondition=selectCondition.trim();
	  	    if(selectCondition.indexOf("#")==0)
	  		 {
	  	    	selectCondition=selectCondition.substring(1);
	  		 }
	  		if(selectCondition.lastIndexOf("#")==selectCondition.length()-1)
	  		 {
	  			selectCondition=selectCondition.substring(0, selectCondition.length()-1);
	  		 }
			
	  	  } 
		String selectColumnId=selectCondition;
		selectCondition=selectCondition.replaceAll("#", ",");
		List selectConditionList=new ArrayList();
		selectConditionList= extractColumnMgmtManager.getSelectCondition(selectCondition,reportName,sessionCorpID);
	    String SaveSelectCondition=new String();
	    Iterator iterator =selectConditionList.iterator();
	    while(iterator.hasNext())
	    {
	    	SaveSelectCondition =SaveSelectCondition.concat((String)iterator.next().toString());
	    	SaveSelectCondition=SaveSelectCondition.concat("#");
	    }
	    SaveSelectCondition=SaveSelectCondition.trim();
		    if(SaveSelectCondition.indexOf("#")==0)
			 {
		    	SaveSelectCondition=SaveSelectCondition.substring(1);
			 }
			if(SaveSelectCondition.lastIndexOf("#")==SaveSelectCondition.length()-1)
			 {
				SaveSelectCondition=SaveSelectCondition.substring(0, SaveSelectCondition.length()-1);
			 }
			//String columnSelect=SaveSelectCondition;
			//SaveSelectCondition=SaveSelectCondition.replaceAll("#", ",");
		
			StringBuffer jobBuffer = new StringBuffer("");
			if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
			} else {
				if (jobTypes.indexOf(",") == 0) {
					jobTypes = jobTypes.substring(1);
				}
				if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
					jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
				}
				String[] arrayJobType = jobTypes.split(",");
				int arrayLength = arrayJobType.length;
				for (int i = 0; i < arrayLength; i++) {
					jobBuffer.append("'");
					jobBuffer.append(arrayJobType[i].trim());
					jobBuffer.append("'");
					jobBuffer.append(",");
				}
			}
			String jobtypeMultiple = new String(jobBuffer);
			if (!jobtypeMultiple.equals("")) {
				jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
			} else if (jobtypeMultiple.equals("")) {
				jobtypeMultiple = new String("''");
			}
			SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
			StringBuffer beginDates = new StringBuffer();
			StringBuffer endDates = new StringBuffer();
			StringBuffer deliveryTBeginDates = new StringBuffer();
			StringBuffer deliveryTEndDates = new StringBuffer();
			StringBuffer deliveryActBeginDates = new StringBuffer();
			StringBuffer deliveryActEndDates = new StringBuffer();
			StringBuffer loadTgtBeginDates = new StringBuffer();
			StringBuffer loadTgtEndDates = new StringBuffer();
			StringBuffer invPostingBeginDates = new StringBuffer();
			StringBuffer invPostingEndDates = new StringBuffer();
			StringBuffer revenueRecognitionBeginDates = new StringBuffer();
			StringBuffer revenueRecognitionEndDates = new StringBuffer();
			StringBuffer invoicingBeginDates = new StringBuffer();
			StringBuffer invoicingEndDates = new StringBuffer();
			StringBuffer storageBeginDates = new StringBuffer();
			StringBuffer storageEndDates = new StringBuffer();
			StringBuffer createdDates = new StringBuffer();
			StringBuffer toCreatedDates = new StringBuffer();
			StringBuffer payPostingBeginDates = new StringBuffer();
			StringBuffer payPostingEndDates = new StringBuffer();
			StringBuffer receivedBeginDates = new StringBuffer();
			StringBuffer receivedEndDates = new StringBuffer();
			StringBuffer postingPeriodBeginDates = new StringBuffer();
			StringBuffer postingPeriodEndDates = new StringBuffer();
			StringBuffer bookingDates = new StringBuffer();
			StringBuffer toBookingDates = new StringBuffer();
			StringBuffer updateDates = new StringBuffer();
			StringBuffer toUpdateDates = new StringBuffer();
			StringBuffer query = new StringBuffer();
			boolean isAnd = false;
			if (beginDate != null && endDate != null) {
				beginDates = new StringBuffer(formats.format(beginDate));
				endDates = new StringBuffer(formats.format(endDate));

			} else if (beginDate != null && endDate == null) {
				beginDates = new StringBuffer(formats.format(beginDate));
				endDates = new StringBuffer();
			} else if (beginDate == null && endDate != null) {
				beginDates = new StringBuffer();
				endDates = new StringBuffer(formats.format(endDate));
			} else {
				beginDates = new StringBuffer();
				endDates = new StringBuffer();
			}
			if (createDate != null && toCreateDate != null) {
				createdDates = new StringBuffer(formats.format(createDate));
				toCreatedDates = new StringBuffer(formats.format(toCreateDate));
			} else if (createDate != null && toCreateDate == null) {
				createdDates = new StringBuffer(formats.format(createDate));
				toCreatedDates = new StringBuffer();
			} else if (createDate == null && toCreateDate != null) {
				createdDates = new StringBuffer();
				toCreatedDates = new StringBuffer(formats.format(toCreateDate));
			} else {
				createdDates = new StringBuffer();
				toCreatedDates = new StringBuffer();
			}
			if (updateDate != null && toUpdateDate != null) {
				updateDates = new StringBuffer(formats.format(updateDate));
				toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
			} else if (updateDate != null && toUpdateDate == null) {
				updateDates = new StringBuffer(formats.format(updateDate));
				toUpdateDates = new StringBuffer();
			} else if (updateDate == null && toUpdateDate != null) {
				updateDates = new StringBuffer();
				toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
			} else {
				updateDates = new StringBuffer();
				toUpdateDates = new StringBuffer();
			}
			if (deliveryTBeginDate != null && deliveryTEndDate != null) {
				deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
				deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
			} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
				deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
				deliveryTEndDates = new StringBuffer();
			} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
				deliveryTBeginDates = new StringBuffer();
				deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
			} else {
				deliveryTBeginDates = new StringBuffer();
				deliveryTEndDates = new StringBuffer();
			}
			if (deliveryActBeginDate != null && deliveryActEndDate != null) {
				deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
				deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
			} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
				deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
				deliveryActEndDates = new StringBuffer();
			} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
				deliveryActBeginDates = new StringBuffer();
				deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
			} else {
				deliveryActBeginDates = new StringBuffer();
				deliveryActEndDates = new StringBuffer();
			}
			if (loadTgtBeginDate != null && loadTgtEndDate != null) {
				loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
				loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
			} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
				loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
				loadTgtEndDates = new StringBuffer();
			} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
				loadTgtBeginDates = new StringBuffer();
				loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
			} else {
				loadTgtBeginDates = new StringBuffer();
				loadTgtEndDates = new StringBuffer();
			}
			if (invPostingBeginDate != null && invPostingEndDate != null) {
				invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
				invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
			} else if (invPostingBeginDate != null && invPostingEndDate == null) {
				invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
				invPostingEndDates = new StringBuffer();
			} else if (invPostingBeginDate == null && invPostingEndDate != null) {
				invPostingBeginDates = new StringBuffer();
				invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
			} else {
				invPostingBeginDates = new StringBuffer();
				invPostingEndDates = new StringBuffer();
			}

			if (invoicingBeginDate != null && invoicingEndDate != null) {
				invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
				invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
			} else if (invoicingBeginDate != null && invoicingEndDate == null) {
				invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
				invoicingEndDates = new StringBuffer();
			} else if (invoicingBeginDate == null && invoicingEndDate != null) {
				invoicingBeginDates = new StringBuffer();
				invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
			} else {
				invoicingBeginDates = new StringBuffer();
				invoicingEndDates = new StringBuffer();
			}
			
			if (storageBeginDate != null && storageEndDate != null) {
				storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
				storageEndDates = new StringBuffer(formats.format(storageEndDate));
			} else if (storageBeginDate != null && storageEndDate == null) {
				storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
				storageEndDates = new StringBuffer();
			} else if (storageBeginDate == null && storageEndDate != null) {
				storageBeginDates = new StringBuffer();
				storageEndDates = new StringBuffer(formats.format(storageEndDate));
			} else {
				storageBeginDates = new StringBuffer();
				storageEndDates = new StringBuffer();
			}
			
			if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate != null) {
				revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
				revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
			} else if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate == null) {
				revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
				revenueRecognitionEndDates = new StringBuffer();
			} else if (revenueRecognitionBeginDate == null && revenueRecognitionEndDate != null) {
				revenueRecognitionBeginDates = new StringBuffer();
				revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
			} else {
				revenueRecognitionBeginDates = new StringBuffer();
				revenueRecognitionEndDates = new StringBuffer();
			}
			if (payPostingBeginDate != null && payPostingEndDate != null) {
				payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
				payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
			} else if (payPostingBeginDate != null && payPostingEndDate == null) {
				payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
				payPostingEndDates = new StringBuffer();
			} else if (payPostingBeginDate == null && payPostingEndDate != null) {
				payPostingBeginDates = new StringBuffer();
				payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
			} else {
				payPostingBeginDates = new StringBuffer();
				payPostingEndDates = new StringBuffer();
			}
			if (receivedBeginDate != null && receivedEndDate != null) {
				receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
				receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
			} else if (receivedBeginDate != null && receivedEndDate == null) {
				receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
				receivedEndDates = new StringBuffer();
			} else if (receivedBeginDate == null && receivedEndDate != null) {
				receivedBeginDates = new StringBuffer();
				receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
			} else {
				receivedBeginDates = new StringBuffer();
				receivedEndDates = new StringBuffer();
			}
			if (postingPeriodBeginDate != null && postingPeriodEndDate != null) {
				postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
				postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
			} else if (postingPeriodBeginDate != null && postingPeriodEndDate == null) {
				postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
				postingPeriodEndDates = new StringBuffer();
			} else if (postingPeriodBeginDate == null && postingPeriodEndDate != null) {
				postingPeriodBeginDates = new StringBuffer();
				postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
			} else {
				postingPeriodBeginDates = new StringBuffer();
				postingPeriodEndDates = new StringBuffer();
			}
			if (bookingDate != null && toBookingDate != null) {
				bookingDates = new StringBuffer(formats.format(bookingDate));
				toBookingDates = new StringBuffer(formats.format(toBookingDate));
			} else if (bookingDate != null && toBookingDate == null) {
				bookingDates = new StringBuffer(formats.format(bookingDate));
				toBookingDates = new StringBuffer();
			} else if (bookingDate == null && toBookingDate != null) {
				bookingDates = new StringBuffer();
				toBookingDates = new StringBuffer(formats.format(toBookingDate));
			} else {
				bookingDates = new StringBuffer();
				toBookingDates = new StringBuffer();
			}
			if (jobTypesCondition.equals("") && billToCodeCondition.equals("")&& originAgentCondition.equals("")&& destinationAgentCondition.equals("") && bookingCodeCondition.equals("") && routingCondition.equals("") && modeCondition.equals("") && packModeCondition.equals("")
					&& commodityCondition.equals("") && coordinatorCondition.equals("") && consultantCondition.equals("") && salesmanCondition.equals("") && oACountryCondition.equals("") && dACountryCondition.equals("")
					&& actualWgtCondition.equals("") && companyCodeCondition.equals("") && beginDates.toString().equals("") && endDates.toString().equals("") && actualVolumeCondition.equals("")
					&& createdDates.toString().equals("") && toCreatedDates.toString().equals("") && updateDates.toString().equals("") && toUpdateDates.toString().equals("") && deliveryTBeginDates.toString().equals("") && deliveryTEndDates.toString().equals("") && deliveryActBeginDates.toString().equals("")
					&& deliveryActEndDates.toString().equals("") && loadTgtBeginDates.toString().equals("") && loadTgtEndDates.toString().equals("") && invPostingBeginDates.toString().equals("") && invPostingEndDates.toString().equals("")
					&& invoicingBeginDates.toString().equals("") && invoicingEndDates.toString().equals("") && storageBeginDates.toString().equals("") && storageEndDates.toString().equals("") && revenueRecognitionBeginDates.toString().equals("") && revenueRecognitionEndDates.toString().equals("")&& payPostingBeginDates.toString().equals("") && payPostingEndDates.toString().equals("")&& receivedBeginDates.toString().equals("") && receivedEndDates.toString().equals("")&& postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals("")
					&& bookingDates.toString().equals("") && toBookingDates.toString().equals("") && carrierCondition.equals("") && driverIdCondition.equals("") && accountCodeCondition.equals("")) {
               if(!reportName.equalsIgnoreCase("dynamicFinancialDetails")){
				if (activeStatus.equalsIgnoreCase("Active")) {
					query.append(" # where  s.corpId='" + sessionCorpID + "'  #  and  s.status not in ('CLSD','CNCL','DWND','DWNLD') # and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '')  group by s.shipNumber");
				} else if (activeStatus.equalsIgnoreCase("Inactive")) {
					query.append(" # where  s.corpId='" + sessionCorpID + "'  #  and  s.status in ('CLSD','CNCL','DWND','DWNLD') # and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
				} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
					query.append(" # where  s.corpId='" + sessionCorpID + "' #  and  s.status not in ('CNCL') # and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
				} else if (activeStatus.equalsIgnoreCase("All")) {
					query.append(" #  where  s.corpId='" + sessionCorpID + "'  group by s.shipNumber ");
				}
               } else if(reportName.equalsIgnoreCase("dynamicFinancialDetails")){
            	   if (activeStatus.equalsIgnoreCase("Active")) {
       				query.append(" # where  s.corpId='" + sessionCorpID + "'   and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '')");
       			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
       				query.append(" #   where  s.corpId='" + sessionCorpID + "'   and  s.status in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') ");
       			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
       				query.append(" # where  s.corpId='" + sessionCorpID + "'   and  s.status not in ('CNCL') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') ");
       			} else if (activeStatus.equalsIgnoreCase("All")) {
       				query.append(" # where  s.corpId='" + sessionCorpID + "' and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') ");
       			}
            	   
               }
				//query.append(" where   s.corpId='"+sessionCorpID+"'  and  s.status not in ('CLSD','CNCL') group by s.shipNumber");
			} else {

				query.append("  where  ");

				if (jobTypesCondition.equals("") || jobTypesCondition == null) {

				} else {
					query.append(" # s.job ");
					isAnd = true;
					if (jobTypesCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append(" in  ");
						query.append("(" + jobtypeMultiple + ")");
					} else {
						query.append(" not in  ");
						query.append("(" + jobtypeMultiple + ")");
					}

				}
				if (billToCodeCondition.equals("") || billToCodeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" # and s.billToCode");
					} else {
						query.append(" # s.billToCode");
					}
					if (billToCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + billToCodeType.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + billToCodeType.trim() + "'");
					}
					isAnd = true;

				}
				
				if (originAgentCondition.equals("") || originAgentCondition == null) {

				} else {
					if (isAnd == true) {
						query.append("  #  and t.originAgentCode");
					} else {
						query.append("  #  t.originAgentCode");
					}
					if (originAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + originAgentCodeType.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + originAgentCodeType.trim() + "'");
					}
					isAnd = true;

				}
				if (destinationAgentCondition.equals("") |destinationAgentCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" #  and t.destinationAgentCode");
					} else {
						query.append("  #  t.destinationAgentCode");
					}
					if (destinationAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + destinationAgentCodeType.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + destinationAgentCodeType.trim() + "'");
					}
					isAnd = true;

				}

				
				if (bookingCodeCondition.equals("") || bookingCodeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" # and s.bookingAgentCode");
					} else {
						query.append(" # s.bookingAgentCode");
					}
					if (bookingCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + bookingCodeType.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + bookingCodeType.trim() + "'");
					}
					isAnd = true;

				}
				if (routingCondition.equals("") || routingCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" # and s.routing");
					} else {
						query.append(" #  s.routing");
					}
					if (routingCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + routingTypes + "'");
					} else {
						query.append("<>");
						query.append("'" + routingTypes + "'");
					}
					isAnd = true;
				}
				if (modeCondition.equals("") || modeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" # and s.mode");
					} else {
						query.append(" #  s.mode");
					}
					if (modeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + modeType + "'");
					} else {
						query.append("<>");
						query.append("'" + modeType + "'");
					}
					isAnd = true;
				}
				if (packModeCondition.equals("") || packModeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" # and s.packingMode");
					} else {
						query.append(" #  s.packingMode");
					}
					if (packModeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + packModeType + "'");
					} else {
						query.append("<>");
						query.append("'" + packModeType + "'");
					}
					isAnd = true;
				}
				if (commodityCondition.equals("") || commodityCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" # and s.commodity");
					} else {
						query.append(" #  s.commodity");
					}
					if (commodityCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + commodityType + "'");
					} else {
						query.append("<>");
						query.append("'" + commodityType + "'");
					}
					isAnd = true;
				}
				if (coordinatorCondition.equals("") || coordinatorCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" # and s.coordinator");
					} else {
						query.append(" #  s.coordinator");
					}
					if (coordinatorCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + coordinatorType + "'");
					} else {
						query.append("<>");
						query.append("'" + coordinatorType + "'");
					}
					isAnd = true;
				}
				if (consultantCondition.equals("") || consultantCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" # and s.estimator");
					} else {
						query.append(" #  s.estimator");
					}
					if (consultantCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + consultantType + "'");
					} else {
						query.append("<>");
						query.append("'" + consultantType + "'");
					}
					isAnd = true;
				}
				
				if (salesmanCondition.equals("") || salesmanCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" # and s.salesMan");
					} else {
						query.append(" #  s.salesMan");
					}
					if (salesmanCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + salesmanType + "'");
					} else {
						query.append("<>");
						query.append("'" + salesmanType + "'");
					}
					isAnd = true;
				}
				if (oACountryCondition.equals("") || oACountryCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" #  and s.originCountryCode");
					} else {
						query.append(" #   s.originCountryCode");
					}
					if (oACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + oACountryType + "'");
					} else {
						query.append("<>");
						query.append("'" + oACountryType + "'");
					}
					isAnd = true;
				}
				if (dACountryCondition.equals("") || dACountryCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" # and s.destinationCountryCode");
					} else {
						query.append(" #  s.destinationCountryCode");
					}
					if (dACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + dACountryType + "'");
					} else {
						query.append("<>");
						query.append("'" + dACountryType + "'");
					}
					isAnd = true;
				} 
				if (actualWgtCondition.equals("") || actualWgtCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" # and m.actualNetWeight");
					} else {
						query.append(" # m.actualNetWeight");
					}

					if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + actualWgtType + "'");
					} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
						query.append("<>");
						query.append("'" + actualWgtType + "'");
					} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
						query.append(">");
						query.append("'" + actualWgtType + "'");
					} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + actualWgtType + "'");
					} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
						query.append("<");
						query.append("'" + actualWgtType + "'");
					} else {
						query.append("<=");
						query.append("'" + actualWgtType + "'");
					}
					isAnd = true;
				}
				if (actualVolumeCondition.equals("") || actualVolumeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" # and m.actualCubicFeet");
					} else {
						query.append(" # m.actualCubicFeet");
					}
					if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + actualVolumeType + "'");
					} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
						query.append("<>");
						query.append("'" + actualVolumeType + "'");
					} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
						query.append(">");
						query.append("'" + actualVolumeType + "'");
					} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + actualVolumeType + "'");
					} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
						query.append("<");
						query.append("'" + actualVolumeType + "'");
					} else {
						query.append("<=");
						query.append("'" + actualVolumeType + "'");
					}
					isAnd = true;
				}
				if (beginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
					}
					if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + beginDates + "')");
					} else if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + beginDates + "')");
					} else {

					}
					isAnd = true;
				}

				if ( createdDates.toString().equals("")) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
					}
					if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + createdDates + "')");
					} else if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + createdDates + "')");
					} else {

					}
					isAnd = true;
				}
				if ( toCreatedDates.toString().equals("")) {

				} else {
					if (isAnd == true) {
						query.append(" #  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
					} else {
						query.append("  #  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
					}
					if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + toCreatedDates + "')");
					} else if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + toCreatedDates + "')");
					} else {

					}
					isAnd = true;
				}
				
				if ( updateDates.toString().equals("")) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
					}
					if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + updateDates + "')");
					} else if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + updateDates + "')");
					} else {

					}
					isAnd = true;
				}
				if ( toUpdateDates.toString().equals("")) {

				} else {
					if (isAnd == true) {
						query.append(" #  and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
					} else {
						query.append("  #  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
					}
					if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + toUpdateDates + "')");
					} else if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + toUpdateDates + "')");
					} else {

					}
					isAnd = true;
				}

				if (endDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
					}
					if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + endDates + "')");
					} else if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + endDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}

				if (deliveryTBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" #  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
					} else {
						query.append("  #  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
					}
					if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + deliveryTBeginDates + "')");
					} else if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + deliveryTBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (deliveryTEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" #  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
					}
					if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + deliveryTEndDates + "')");
					} else if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + deliveryTEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (deliveryActBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
					}
					if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + deliveryActBeginDates + "')");
					} else if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + deliveryActBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (deliveryActEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
					}
					if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + deliveryActEndDates + "')");
					} else if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + deliveryActEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (loadTgtBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" #  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
					}
					if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + loadTgtBeginDates + "')");
					} else if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + loadTgtBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (loadTgtEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
					}
					if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + loadTgtEndDates + "')");
					} else if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + loadTgtEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (invPostingBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" #  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
					}
					if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + invPostingBeginDates + "')");
					} else if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + invPostingBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (invPostingEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
					}
					if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + invPostingEndDates + "')");
					} else if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + invPostingEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (payPostingBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					}
					if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + payPostingBeginDates + "')");
					} else if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + payPostingBeginDates + "')");
					} else {
						
					}
					isAnd = true;
				}
				if (payPostingEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" #  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					}
					if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + payPostingEndDates + "')");
					} else if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + payPostingEndDates + "')");
					} else {
						
					}
					isAnd = true;
				}
				if ( receivedBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
					} else {
						query.append(" #    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
					}
					if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + receivedBeginDates + "')");
					} else if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + receivedBeginDates + "')");
					} else {
						
					}
					isAnd = true;
				}
				if (receivedEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
					}
					if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + receivedEndDates + "')");
					} else if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + receivedEndDates + "')");
					} else {
						
					}
					isAnd = true;
				}
				if ( (postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals(""))) {

				} else 
				{
					if(((!( postingPeriodBeginDates.toString().equals(""))))&&!(postingPeriodEndDates.toString().equals(""))) {
					if (isAnd == true) {
						query.append(" # and ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
					} else {
						query.append(" # ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
					}
					if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else {
						
					}
					query.append("  AND  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");

	                  
					  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "')");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "')");
					} else {
						
					}
	                   query.append("  or ( (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					   if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else {
						
					}

	                 query.append("  AND (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "') ) ) )");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "') ) ) ) ");
					} else {
						
					}

					isAnd = true;
				} 
					else if((!( postingPeriodBeginDates.toString().equals("")))&& ((postingPeriodEndDates.toString().equals("")))){
					if (isAnd == true) {
						query.append(" # and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
					} else {
						query.append("  # ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
					}
					if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else {
						
					}
					 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					 if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
							query.append("<=");
							query.append("'" + postingPeriodBeginDates + "') )");
						} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
							query.append(">=");
							query.append("'" + postingPeriodBeginDates + "') )");
						} else {
							
						}
					 isAnd = true;
				}
				else  if((!( postingPeriodEndDates.toString().equals("")))&& ((postingPeriodBeginDates.toString().equals("")))){
					if (isAnd == true) {
						query.append(" # and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
					} else {
						query.append(" # ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
					}
					if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "')");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "')");
					} else {
						
					}
					 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					 if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
							query.append("<=");
							query.append("'" + postingPeriodEndDates + "') )");
						} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
							query.append(">=");
							query.append("'" + postingPeriodEndDates + "') )");
						} else {
							
						}
					 isAnd = true;
				}
				}
				 
				if (invoicingBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
					} else {
						query.append("  #  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
					}

					if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + invoicingBeginDates + "')");
					} else if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + invoicingBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}

				if (invoicingEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
					}

					if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + invoicingEndDates + "')");
					} else if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + invoicingEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				
				
				if (storageBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
					}

					if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + storageBeginDates + "')");
					} else if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + storageBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}

				if (storageEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" # and  (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
					}

					if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + storageEndDates + "')");
					} else if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + storageEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				
				
				if (revenueRecognitionBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" #  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
					}

					if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + revenueRecognitionBeginDates + "')");
					} else if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + revenueRecognitionBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}

				if (revenueRecognitionEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append(" #  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
					} else {
						query.append(" #   (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
					}

					if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + revenueRecognitionEndDates + "')");
					} else if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + revenueRecognitionEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				

				if (companyCodeCondition == null || companyCodeCondition.equals("")) {

				} else {
					if (isAnd == true) {
						query.append(" # and s.companyDivision");
					} else {
						query.append(" # s.companyDivision");
					}
					if (companyCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + companyCode + "'");
					} else {
						query.append("<>");
						query.append("'" + companyCode + "'");
					}
					isAnd = true;
				}
				if ( bookingDates.toString().equals("")) {
				} else {
					if (isAnd == true) {
						query.append(" # and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
					} else {
						query.append(" # (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
					}
					if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + bookingDates + "')");
					} else if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + bookingDates + "')");
					} else {

					}
					isAnd = true;
				}
				if ( toBookingDates.toString().equals("")) {
				} else {
					if (isAnd == true) {
						query.append(" # and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
					} else {
						query.append(" # (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
					}
					if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + toBookingDates + "')");
					} else if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + toBookingDates + "')");
					} else {

					}
					isAnd = true;
				}
				if (carrierCondition.equals("") || carrierCondition == null) {
				} else {
					if (isAnd == true) {
						query.append(" # and m.carrier");
					} else {
						query.append(" #  m.carrier");
					}
					if (carrierCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + carrier + "'");
					} else {
						query.append("<>");
						query.append("'" + carrier + "'");
					}
					isAnd = true;
				}
				if (driverIdCondition.equals("") || driverIdCondition == null) {
				} else {
					if (isAnd == true) {
						query.append(" # and m.driverId");
					} else {
						query.append(" # m.driverId");
					}
					if (driverIdCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + driverId + "'");
					} else {
						query.append("<>");
						query.append("'" + driverId + "'");
					}
					isAnd = true;
				}
				
				if (accountCodeCondition.equals("") || accountCodeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" # and c.accountCode");
					} else {
						query.append(" #  c.accountCode");
					}
					if (accountCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + accountCodeType + "'");
					} else {
						query.append("<>");
						query.append("'" + accountCodeType + "'");
					}
					isAnd = true;
				}
				
				if((!reportName.equalsIgnoreCase("dynamicFinancialDetails"))&& (!reportName.equalsIgnoreCase("storageBillingAnalysis"))){
				if (activeStatus.equalsIgnoreCase("Active")) {
					query.append(" #  and s.corpId='" + sessionCorpID + "' and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
				} else if (activeStatus.equalsIgnoreCase("Inactive")) {
					query.append(" #  and s.corpId='" + sessionCorpID + "' and  s.status  in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
				} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
					query.append(" #  and s.corpId='" + sessionCorpID + "' and  s.status not in ('CNCL') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
				} else if (activeStatus.equalsIgnoreCase("All")) {
					query.append(" # and s.corpId='" + sessionCorpID + "' group by s.shipNumber");
				}
				} else if((reportName.equalsIgnoreCase("dynamicFinancialDetails"))||(reportName.equalsIgnoreCase("storageBillingAnalysis"))){
					if (activeStatus.equalsIgnoreCase("Active")) {
						query.append(" #  and s.corpId='" + sessionCorpID + "' and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') ");
					} else if (activeStatus.equalsIgnoreCase("Inactive")) {
						query.append(" #  and s.corpId='" + sessionCorpID + "' and  s.status  in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') ");
					} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
						query.append(" #  and s.corpId='" + sessionCorpID + "' and  s.status not in ('CNCL') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') ");
					} else if (activeStatus.equalsIgnoreCase("All")) {
						query.append(" #  and s.corpId='" + sessionCorpID + "' and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '')");
					}
					
				}
				//query.append("   and s.corpId='"+sessionCorpID+"'  and  s.status not in ('CLSD','CNCL')   group by s.shipNumber");
			}
	   if(id==null){
		extractQueryFile = new ExtractQueryFile();
		extractQueryFile.setCreatedon(new Date());
		extractQueryFile.setCreatedby(getRequest().getRemoteUser());
	   }
	   else {
		   extractQueryFile =extractManager.get(id) ;
		   try
           {
		   SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
           Date du = new Date();
           du = df.parse(querycreatedOn1);
           df = new SimpleDateFormat("yyyy-MM-dd");
           Date formatedCreatedOn = df.parse(querycreatedOn1);
		   extractQueryFile.setCreatedon(formatedCreatedOn);
           }
		   catch (ParseException e)
           {
             e.printStackTrace();
           } 
		   extractQueryFile.setCreatedby(querycreatedBy);
	   }
		extractQueryFile.setUserId(getRequest().getRemoteUser()); 
		extractQueryFile.setCorpID(sessionCorpID);
		extractQueryFile.setExtractType(reportName);
		extractQueryFile.setPublicPrivateFlag(getPublicPrivateFlag());
		extractQueryFile.setQueryName(queryName);
		extractQueryFile.setQueryCondition(query.toString());
		extractQueryFile.setSelectQuery(SaveSelectCondition);
		extractQueryFile.setSelectColumnId(selectColumnId);
		extractQueryFile.setModifiedby(getRequest().getRemoteUser());
		extractQueryFile.setModifiedon(new Date());
		extractQueryFile.setAutoGenerateReoprt(autoGenerator);
		extractQueryFile.setQueryScheduler(queryScheduler);
		extractQueryFile.setPerWeekScheduler(perWeekScheduler);
		extractQueryFile.setPerMonthScheduler(perMonthScheduler);
		extractQueryFile.setPerQuarterScheduler(perQuarterScheduler); 
		extractQueryFile.setEmail(email); 
		extractQueryFile.setLastEmailSent(lastEmailSent); 
		datePeriod=datePeriod.trim();
		if(!(datePeriod.equals(""))){
	    if(datePeriod.indexOf(",")==0)
		 {
	    	datePeriod=datePeriod.substring(1);
		 }
		}
		if(!(datePeriod.equals(""))){
		if(datePeriod.lastIndexOf(",")==datePeriod.length()-1)
		 {
			datePeriod=datePeriod.substring(0, datePeriod.length()-1);
		 }
		}
		extractQueryFile.setDatePeriod(datePeriod);
		try
        {
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
        Date du = new Date();
        if(emailDateTimeString!=null &&(! emailDateTimeString.trim().equals(""))){
        du = df.parse(emailDateTimeString);
        df = new SimpleDateFormat("yyyy-MM-dd");
        Date formatedemailDateTime = df.parse(emailDateTimeString); 
        extractQueryFile.setEmailDateTime(formatedemailDateTime); 
        }
        else{
        extractQueryFile.setEmailDateTime(null)	;
        }
        }
		catch (ParseException e)
        {
          e.printStackTrace();
        } 
		extractQueryFile.setEmailStatus(emailStatus); 
		extractQueryFile.setEmailMessage(emailMessage); 
		extractManager.save(extractQueryFile);
		String key="";
		if(id==null){
		    key = queryName + " Query has been saved successfully.";
		}
		else{
			key = queryName + " Query has been updated successfully.";
		}
		saveMessage(getText(key)); 
		findOldQueries();
		return SUCCESS;
	}
	
	

	public String findOldQueries(){
		// extractQueryFile.setUserId(getRequest().getRemoteUser());
		queryList = extractManager.oldQueries(getRequest().getRemoteUser());
	
		return SUCCESS;
	}
    
	public String searchExtractQueryFiles(){
		queryList = extractManager.searchExtractQueryFiles(queryName,publicPrivateFlag,modifiedby,getRequest().getRemoteUser());
		return SUCCESS;
	}
	//private List columnList;
	public String editOldDataExtract(){
		companyDivisionFlag = companyDivisionManager.findCompanyDivFlag(sessionCorpID).get(0).toString();
		accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString(); 
		companyCodeList = companyDivisionManager.findCompanyCodeList(sessionCorpID);
		getComboList(sessionCorpID); 
		extractQueryFile = extractManager.get(id);
		multiplejobType= new ArrayList(); 
		String whereCondition = extractQueryFile.getQueryCondition();
		String selectQuery=extractQueryFile.getSelectQuery();
		selectCondition=extractQueryFile.getSelectColumnId();
		reportName=extractQueryFile.getExtractType();
		queryName=extractQueryFile.getQueryName();
		querycreatedBy=extractQueryFile.getCreatedby();
		querycreatedOn=extractQueryFile.getCreatedon();
		publicPrivateFlag=extractQueryFile.getPublicPrivateFlag();
		autoGenerator=extractQueryFile.isAutoGenerateReoprt();
		queryScheduler=extractQueryFile.getQueryScheduler();
		perWeekScheduler=extractQueryFile.getPerWeekScheduler();
		perMonthScheduler=extractQueryFile.getPerMonthScheduler();
		perQuarterScheduler=extractQueryFile.getPerQuarterScheduler();
		email=extractQueryFile.getEmail();
		emailDateTime=extractQueryFile.getEmailDateTime();
		lastEmailSent=extractQueryFile.getLastEmailSent();
		emailStatus=extractQueryFile.getEmailStatus();
		emailMessage=extractQueryFile.getEmailMessage();
		datePeriod=extractQueryFile.getDatePeriod();
		String[] datePeriodArray=datePeriod.split(",");
		int arrayLengthDatePeriod = datePeriodArray.length; 
  		for(int i=0;i<arrayLengthDatePeriod;i++)
  		 { 
  		   String periodDateValue =datePeriodArray[i]; 
	  		   if(!(periodDateValue.indexOf("loadActualDatePeriod")<0)){
	  			 loadActualDatePeriod=periodDateValue.substring(periodDateValue.indexOf("#")+1); 
	  		   }
	  		 if(!(periodDateValue.indexOf("createDatePeriod")<0)){
	  			createDatePeriod=periodDateValue.substring(periodDateValue.indexOf("#")+1); 
	  		   }
	  		if(!(periodDateValue.indexOf("updateDatePeriod")<0)){
	  			updateDatePeriod=periodDateValue.substring(periodDateValue.indexOf("#")+1); 
	  		   }
	  		if(!(periodDateValue.indexOf("deliveryTargetDatePeriod")<0)){
	  			deliveryTargetDatePeriod=periodDateValue.substring(periodDateValue.indexOf("#")+1); 
	 		   }
	  		if(!(periodDateValue.indexOf("deliveryActualDatePeriod")<0)){
	  			deliveryActualDatePeriod=periodDateValue.substring(periodDateValue.indexOf("#")+1); 
	 		   }
	  		if(!(periodDateValue.indexOf("loadTargetDatePeriod")<0)){
	  			loadTargetDatePeriod=periodDateValue.substring(periodDateValue.indexOf("#")+1); 
	  		   }
	  		if(!(periodDateValue.indexOf("invoicePostingDatePeriod")<0)){
	  			invoicePostingDatePeriod=periodDateValue.substring(periodDateValue.indexOf("#")+1); 
	  		   }
	  		if(!(periodDateValue.indexOf("PostingPeriodDatePeriod")<0)){
	  			PostingPeriodDatePeriod=periodDateValue.substring(periodDateValue.indexOf("#")+1); 
	  		   }
	  		if(!(periodDateValue.indexOf("receivedDatePeriod")<0)){
	  			receivedDatePeriod=periodDateValue.substring(periodDateValue.indexOf("#")+1); 
	  		   }
	  		if(!(periodDateValue.indexOf("InvoicingdatePeriod")<0)){
	  			InvoicingdatePeriod=periodDateValue.substring(periodDateValue.indexOf("#")+1); 
	  		   }
	  		if(!(periodDateValue.indexOf("storageDatePeriod")<0)){
	  			storageDatePeriod=periodDateValue.substring(periodDateValue.indexOf("#")+1); 
	  		   }
	  		if(!(periodDateValue.indexOf("revenueRecognitionPeriod")<0)){
	  			revenueRecognitionPeriod=periodDateValue.substring(periodDateValue.indexOf("#")+1); 
	  		   }
	  		if(!(periodDateValue.indexOf("payablePostingDatePeriod")<0)){
	  			payablePostingDatePeriod=periodDateValue.substring(periodDateValue.indexOf("#")+1); 
	  		   }
	  		if(!(periodDateValue.indexOf("bookingDatePeriod")<0)){
	  			bookingDatePeriod=periodDateValue.substring(periodDateValue.indexOf("#")+1); 
	  		   }
         }
		String[] arraywhere=whereCondition.split("#");
		//System.out.println("\n\n\n\n\n\n\n\narrayRecInvoice in Dynamic DataExtract Action>>>>>>>>>>>>>>"+arraywhere);
		int arrayLength = arraywhere.length;
	    
		for(int i=0;i<arrayLength;i++)
		 {	
		   String whereColumn=arraywhere[i]; 
		   if(whereColumn.indexOf("s.job")>0){
			   if(whereColumn.indexOf("not in")>0){
				   jobTypesCondition="Not Equal to";
			   } else {
				   jobTypesCondition="Equal to"; 
			   }
			   whereColumn= whereColumn.substring(whereColumn.indexOf("("));
			   multiplejobType= new ArrayList();
				if (whereColumn == null || whereColumn.equals("") || whereColumn.equals(",")) {
				} else {
					whereColumn=whereColumn.trim();
					if (whereColumn.indexOf(",") == 0) {
						whereColumn = whereColumn.substring(1);
					}
					if (whereColumn.lastIndexOf(",") == whereColumn.length() - 1) {
						whereColumn = whereColumn.substring(0, whereColumn.length() - 1);
					}
					if (whereColumn.indexOf("(") == 0) {
						whereColumn = whereColumn.substring(1);
					}
					if (whereColumn.lastIndexOf(")") == whereColumn.length() - 1) {
						whereColumn = whereColumn.substring(0, whereColumn.length() - 1);
					}
					String[] arrayJobType = whereColumn.split(",");
					int arrayLength1 = arrayJobType.length;
					for (int j = 0; j < arrayLength1; j++) {
						String job=arrayJobType[j];
						if (job.indexOf("(") == 0) {
							job = job.substring(1);
						}
						if (job.lastIndexOf(")") == job.length() - 1) {
							job = job.substring(0, job.length() - 1);
						}
						if (job.indexOf("'") == 0) {
							job = job.substring(1);
						}
						if (job.lastIndexOf("'") == job.length() - 1) {
							job = job.substring(0, job.length() - 1);
						}
						
						multiplejobType.add(job.trim());
						//System.out.println("\n\n\n\n\n\n\n multiplejobType in Edit Dynamic dataExtract"+multiplejobType);
						
					}
				}
			   
		   }
		   if(whereColumn.indexOf("s.billToCode")>0){
			   if(whereColumn.indexOf("=")>0){
				   billToCodeCondition="Equal to";
			   }
			   else{
				   billToCodeCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   billToCodeType=whereColumn;
			   billToCodeType=billToCodeType.trim();
			   if (billToCodeType.indexOf("=") == 0) {
				   billToCodeType = billToCodeType.substring(1);
				}
			    if (billToCodeType.indexOf("'") == 0) {
				   billToCodeType = billToCodeType.substring(1);
				}
				if (billToCodeType.lastIndexOf("'") == billToCodeType.length() - 1) {
					billToCodeType = billToCodeType.substring(0, billToCodeType.length() - 1);
				}
		   }
			   else { 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   billToCodeType=whereColumn;
				   billToCodeType=billToCodeType.trim();
				   if (billToCodeType.indexOf("<>") == 0) {
					   billToCodeType = billToCodeType.substring(2);
					}
				    if (billToCodeType.indexOf("'") == 0) {
					   billToCodeType = billToCodeType.substring(1);
					}
					if (billToCodeType.lastIndexOf("'") == billToCodeType.length() - 1) {
						billToCodeType = billToCodeType.substring(0, billToCodeType.length() - 1);
					}
			   
				   
			   }
		   }
		   if(whereColumn.indexOf("t.originAgentCode")>0){
			   if(whereColumn.indexOf("=")>0){
				   originAgentCondition="Equal to";
			   }
			   else{
				   originAgentCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   originAgentCodeType=whereColumn;
			   originAgentCodeType=originAgentCodeType.trim();
			   if (originAgentCodeType.indexOf("=") == 0) {
				   originAgentCodeType = originAgentCodeType.substring(1);
				}
			    if (originAgentCodeType.indexOf("'") == 0) {
			    	originAgentCodeType =originAgentCodeType.substring(1);
				}
				if (originAgentCodeType.lastIndexOf("'") == originAgentCodeType.length() - 1) {
					originAgentCodeType = originAgentCodeType.substring(0, originAgentCodeType.length() - 1);
				}
		   }
			   else { 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   originAgentCodeType=whereColumn;
				   originAgentCodeType=originAgentCodeType.trim();
				   if (originAgentCodeType.indexOf("<>") == 0) {
					   originAgentCodeType = originAgentCodeType.substring(2);
					}
				    if (originAgentCodeType.indexOf("'") == 0) {
				    	originAgentCodeType = originAgentCodeType.substring(1);
					}
					if (originAgentCodeType.lastIndexOf("'") == originAgentCodeType.length() - 1) {
						originAgentCodeType = originAgentCodeType.substring(0, originAgentCodeType.length() - 1);
					}
			   
				   
			   }
		   }
		   if(whereColumn.indexOf("t.destinationAgentCode")>0){
			   if(whereColumn.indexOf("=")>0){
				   destinationAgentCondition="Equal to";
			   }
			   else{
				   destinationAgentCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   originAgentCodeType=whereColumn;
			   originAgentCodeType=originAgentCodeType.trim();
			   if (destinationAgentCodeType.indexOf("=") == 0) {
				   destinationAgentCodeType = destinationAgentCodeType.substring(1);
				}
			    if (destinationAgentCodeType.indexOf("'") == 0) {
			    	destinationAgentCodeType =destinationAgentCodeType.substring(1);
				}
				if (destinationAgentCodeType.lastIndexOf("'") == destinationAgentCodeType.length() - 1) {
					destinationAgentCodeType = destinationAgentCodeType.substring(0, destinationAgentCodeType.length() - 1);
				}
		   }
			   else { 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   destinationAgentCodeType=whereColumn;
				   destinationAgentCodeType=destinationAgentCodeType.trim();
				   if (destinationAgentCodeType.indexOf("<>") == 0) {
					   destinationAgentCodeType =destinationAgentCodeType.substring(2);
					}
				    if (destinationAgentCodeType.indexOf("'") == 0) {
				    	destinationAgentCodeType = destinationAgentCodeType.substring(1);
					}
					if (destinationAgentCodeType.lastIndexOf("'") == destinationAgentCodeType.length() - 1) {
						destinationAgentCodeType = destinationAgentCodeType.substring(0, destinationAgentCodeType.length() - 1);
					}
			   
				   
			   }
		   }
		   
		   if(whereColumn.indexOf("s.bookingAgentCode")>0){
			   if(whereColumn.indexOf("=")>0){
				   bookingCodeCondition="Equal to";
			   }
			   else{
				   bookingCodeCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   bookingCodeType=whereColumn;
			   bookingCodeType=bookingCodeType.trim();
			   if (bookingCodeType.indexOf("=") == 0) {
				   bookingCodeType = bookingCodeType.substring(1);
				}
			    if (bookingCodeType.indexOf("'") == 0) {
			    	bookingCodeType = bookingCodeType.substring(1);
				}
				if (bookingCodeType.lastIndexOf("'") == bookingCodeType.length() - 1) {
					bookingCodeType = bookingCodeType.substring(0, bookingCodeType.length() - 1);
				}
		       }else
		       { 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   bookingCodeType=whereColumn;
				   bookingCodeType=bookingCodeType.trim();
				   if (bookingCodeType.indexOf("<>") == 0) {
					   bookingCodeType = bookingCodeType.substring(2);
					}
				    if (bookingCodeType.indexOf("'") == 0) {
				    	bookingCodeType = bookingCodeType.substring(1);
					}
					if (bookingCodeType.lastIndexOf("'") == bookingCodeType.length() - 1) {
						bookingCodeType = bookingCodeType.substring(0, bookingCodeType.length() - 1);
					} 
		       }
		   }
		   if(whereColumn.indexOf("s.routing")>0){
			   if(whereColumn.indexOf("=")>0){
				   routingCondition="Equal to";
			   }
			   else{
				   routingCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   routingTypes=whereColumn;
			   routingTypes=routingTypes.trim();
			   if (routingTypes.indexOf("=") == 0) {
				   routingTypes = routingTypes.substring(1);
				}
			    if (routingTypes.indexOf("'") == 0) {
			    	routingTypes = routingTypes.substring(1);
				}
				if (routingTypes.lastIndexOf("'") == routingTypes.length() - 1) {
					routingTypes = routingTypes.substring(0, routingTypes.length() - 1);
				}
		   } 
			   else{ 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   routingTypes=whereColumn;
				   routingTypes=routingTypes.trim();
				   if (routingTypes.indexOf("<>") == 0) {
					   routingTypes = routingTypes.substring(2);
					}
				    if (routingTypes.indexOf("'") == 0) {
				    	routingTypes = routingTypes.substring(1);
					}
					if (routingTypes.lastIndexOf("'") == routingTypes.length() - 1) {
						routingTypes = routingTypes.substring(0, routingTypes.length() - 1);
					}
			   
			   }
		   }
		   if(whereColumn.indexOf("s.mode")>0){
			   if(whereColumn.indexOf("=")>0){
				   modeCondition="Equal to";
			   }
			   else{
				   modeCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   modeType=whereColumn;
			   modeType=modeType.trim();
			   if (modeType.indexOf("=") == 0) {
				   modeType = modeType.substring(1);
				}
			    if (modeType.indexOf("'") == 0) {
			    	modeType = modeType.substring(1);
				}
				if (modeType.lastIndexOf("'") == modeType.length() - 1) {
					modeType = modeType.substring(0, modeType.length() - 1);
				}
			   }
			   else { 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   modeType=whereColumn;
				   modeType=modeType.trim();
				   if (modeType.indexOf("<>") == 0) {
					   modeType = modeType.substring(2);
					}
				    if (modeType.indexOf("'") == 0) {
				    	modeType = modeType.substring(1);
					}
					if (modeType.lastIndexOf("'") == modeType.length() - 1) {
						modeType = modeType.substring(0, modeType.length() - 1);
					}
				   
			   }
		   }
		   if(whereColumn.indexOf("s.packingMode")>0){
			   if(whereColumn.indexOf("=")>0){
				   packModeCondition="Equal to";
			   }
			   else{
				   packModeCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   packModeType=whereColumn;
			   packModeType=packModeType.trim();
			   if (packModeType.indexOf("=") == 0) {
				   packModeType = packModeType.substring(1);
				}
			    if (packModeType.indexOf("'") == 0) {
			    	packModeType = packModeType.substring(1);
				}
				if (packModeType.lastIndexOf("'") == packModeType.length() - 1) {
					packModeType = packModeType.substring(0, packModeType.length() - 1);
				}
			   }
			   else { 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   packModeType=whereColumn;
				   packModeType=packModeType.trim();
				   if (packModeType.indexOf("<>") == 0) {
					   packModeType = packModeType.substring(2);
					}
				    if (packModeType.indexOf("'") == 0) {
				    	packModeType = packModeType.substring(1);
					}
					if (packModeType.lastIndexOf("'") == packModeType.length() - 1) {
						packModeType = packModeType.substring(0, packModeType.length() - 1);
					}
				    
			   }
		   }
		   if(whereColumn.indexOf("s.commodity")>0){
			   if(whereColumn.indexOf("=")>0){
				   commodityCondition="Equal to";
			   }
			   else{
				   commodityCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   commodityType=whereColumn;
			   commodityType=commodityType.trim();
			   if (commodityType.indexOf("=") == 0) {
				   commodityType = commodityType.substring(1);
				}
			    if (commodityType.indexOf("'") == 0) {
			    	commodityType = commodityType.substring(1);
				}
				if (commodityType.lastIndexOf("'") == commodityType.length() - 1) {
					commodityType = commodityType.substring(0, commodityType.length() - 1);
				}
			   }
			   else{

				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   commodityType=whereColumn;
				   commodityType=commodityType.trim();
				   if (commodityType.indexOf("<>") == 0) {
					   commodityType = commodityType.substring(2);
					}
				    if (commodityType.indexOf("'") == 0) {
				    	commodityType = commodityType.substring(1);
					}
					if (commodityType.lastIndexOf("'") == commodityType.length() - 1) {
						commodityType = commodityType.substring(0, commodityType.length() - 1);
					}
				   
			   }
		   }
		   if(whereColumn.indexOf("s.coordinator")>0){
			   if(whereColumn.indexOf("=")>0){
				   coordinatorCondition="Equal to";
			   }
			   else{
				   coordinatorCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   coordinatorType=whereColumn;
			   coordinatorType=coordinatorType.trim();
			   if (coordinatorType.indexOf("=") == 0) {
				   coordinatorType = coordinatorType.substring(1);
				}
			    if (coordinatorType.indexOf("'") == 0) {
			    	coordinatorType = coordinatorType.substring(1);
				}
				if (coordinatorType.lastIndexOf("'") == coordinatorType.length() - 1) {
					coordinatorType = coordinatorType.substring(0, coordinatorType.length() - 1);
				}
			   }
			   else{ 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   coordinatorType=whereColumn;
				   coordinatorType=coordinatorType.trim();
				   if (coordinatorType.indexOf("<>") == 0) {
					   coordinatorType = coordinatorType.substring(2);
					}
				    if (coordinatorType.indexOf("'") == 0) {
				    	coordinatorType = coordinatorType.substring(1);
					}
					if (coordinatorType.lastIndexOf("'") == coordinatorType.length() - 1) {
						coordinatorType = coordinatorType.substring(0, coordinatorType.length() - 1);
					}
				   
			   }
		   } 
		   if(whereColumn.indexOf("s.estimator")>0){
			   if(whereColumn.indexOf("=")>0){
				   consultantCondition="Equal to";
			   }
			   else{
				   consultantCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   consultantType=whereColumn;
			   consultantType=consultantType.trim();
			   if (consultantType.indexOf("=") == 0) {
				   consultantType = consultantType.substring(1);
				}
			    if (consultantType.indexOf("'") == 0) {
			    	consultantType = consultantType.substring(1);
				}
				if (consultantType.lastIndexOf("'") == consultantType.length() - 1) {
					consultantType = consultantType.substring(0, consultantType.length() - 1);
				}
			   }
			   else{ 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   consultantType=whereColumn;
				   consultantType=consultantType.trim();
				   if (consultantType.indexOf("<>") == 0) {
					   consultantType = consultantType.substring(2);
					}
				    if (consultantType.indexOf("'") == 0) {
				    	consultantType = consultantType.substring(1);
					}
					if (consultantType.lastIndexOf("'") == consultantType.length() - 1) {
						consultantType = consultantType.substring(0, consultantType.length() - 1);
					}
				   
			   }
		   } 
		   if(whereColumn.indexOf("s.salesMan")>0){
			   if(whereColumn.indexOf("=")>0){
				   salesmanCondition="Equal to";
			   }
			   else{
				   salesmanCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   salesmanType=whereColumn;
			   salesmanType=salesmanType.trim();
			   if (salesmanType.indexOf("=") == 0) {
				   salesmanType = salesmanType.substring(1);
				}
			    if (salesmanType.indexOf("'") == 0) {
			    	salesmanType = salesmanType.substring(1);
				}
				if (salesmanType.lastIndexOf("'") == salesmanType.length() - 1) {
					salesmanType = salesmanType.substring(0, salesmanType.length() - 1);
				}
			   }
			   else
			   { 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   salesmanType=whereColumn;
				   salesmanType=salesmanType.trim();
				   if (salesmanType.indexOf("<>") == 0) {
					   salesmanType = salesmanType.substring(2);
					}
				    if (salesmanType.indexOf("'") == 0) {
				    	salesmanType = salesmanType.substring(1);
					}
					if (salesmanType.lastIndexOf("'") == salesmanType.length() - 1) {
						salesmanType = salesmanType.substring(0, salesmanType.length() - 1);
					}
				   
			   }
		   }
		   if(whereColumn.indexOf("s.originCountryCode")>0){
			   if(whereColumn.indexOf("=")>0){
				   oACountryCondition="Equal to";
			   }
			   else{
				   oACountryCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   oACountryType=whereColumn;
			   oACountryType=oACountryType.trim();
			   if (oACountryType.indexOf("=") == 0) {
				   oACountryType = oACountryType.substring(1);
				}
			    if (oACountryType.indexOf("'") == 0) {
			    	oACountryType = oACountryType.substring(1);
				}
				if (oACountryType.lastIndexOf("'") == oACountryType.length() - 1) {
					oACountryType = oACountryType.substring(0, oACountryType.length() - 1);
				}
			   }
			   else{ 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   oACountryType=whereColumn;
				   oACountryType=oACountryType.trim();
				   if (oACountryType.indexOf("<>") == 0) {
					   oACountryType = oACountryType.substring(2);
					}
				    if (oACountryType.indexOf("'") == 0) {
				    	oACountryType = oACountryType.substring(1);
					}
					if (oACountryType.lastIndexOf("'") == oACountryType.length() - 1) {
						oACountryType = oACountryType.substring(0, oACountryType.length() - 1);
					}
				   
			   }
		   }
		   if(whereColumn.indexOf("s.destinationCountryCode")>0){
			   if(whereColumn.indexOf("=")>0){
				   dACountryCondition="Equal to";
			   }
			   else{
				   dACountryCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   dACountryType=whereColumn;
			   dACountryType=dACountryType.trim();
			   if (dACountryType.indexOf("=") == 0) {
				   dACountryType = dACountryType.substring(1);
				}
			    if (dACountryType.indexOf("'") == 0) {
			    	dACountryType = dACountryType.substring(1);
				}
				if (dACountryType.lastIndexOf("'") == dACountryType.length() - 1) {
					dACountryType = dACountryType.substring(0, dACountryType.length() - 1);
				}
			   }
			   else{ 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   dACountryType=whereColumn;
				   dACountryType=dACountryType.trim();
				   if (dACountryType.indexOf("<>") == 0) {
					   dACountryType = dACountryType.substring(2);
					}
				    if (dACountryType.indexOf("'") == 0) {
				    	dACountryType = dACountryType.substring(1);
					}
					if (dACountryType.lastIndexOf("'") == dACountryType.length() - 1) {
						dACountryType = dACountryType.substring(0, dACountryType.length() - 1);
					}
				   
				   
			   }
		   }
		   if(whereColumn.indexOf("m.actualNetWeight")>0){
			   String Weightcomdition="";
			   if(whereColumn.indexOf("=")>0){
				   actualWgtCondition="Equal to";
				   Weightcomdition="=";
			   }
			   else if(whereColumn.indexOf("<>")>0){
				   actualWgtCondition="Not Equal to";
				   Weightcomdition="<>";
			   }
			   else if(whereColumn.indexOf(">")>0){
				   actualWgtCondition="Greater than";
				   Weightcomdition=">";
			   }
			   else  if(whereColumn.indexOf(">=")>0){
				   actualWgtCondition="Greater than Equal to";
				   Weightcomdition=">=";
			   }
			   else if(whereColumn.indexOf("<")>0){
				   actualWgtCondition="Less than";
				   Weightcomdition="<";
			   }
			   else  if(whereColumn.indexOf("<=")>0){
				   actualWgtCondition="Less than Equal to";
				   Weightcomdition="<=";
			   } 
			   whereColumn= whereColumn.substring(whereColumn.indexOf(Weightcomdition));
			   actualWgtType=whereColumn;
			   actualWgtType=actualWgtType.trim();
			   if (actualWgtType.indexOf("=") == 0) {
				   actualWgtType = actualWgtType.substring(1);
				}
			    if (actualWgtType.indexOf("<>") == 0) {
				   actualWgtType = actualWgtType.substring(2);
				}
			    if (actualWgtType.indexOf(">") == 0) {
					   actualWgtType = actualWgtType.substring(1);
					}
			    if (actualWgtType.indexOf(">=") == 0) {
					   actualWgtType = actualWgtType.substring(2);
					}
			    if (actualWgtType.indexOf("<") == 0) {
					   actualWgtType = actualWgtType.substring(1);
					}
			    if (actualWgtType.indexOf("<=") == 0) {
					   actualWgtType = actualWgtType.substring(2);
					}
			    if (actualWgtType.indexOf("'") == 0) {
			    	actualWgtType = actualWgtType.substring(1);
				}
				if (actualWgtType.lastIndexOf("'") == actualWgtType.length() - 1) {
					actualWgtType = actualWgtType.substring(0, actualWgtType.length() - 1);
				}
		   }
		   if(whereColumn.indexOf("m.actualCubicFeet")>0){
			   String Weightcomdition="";
			   if(whereColumn.indexOf("=")>0){
				   actualVolumeCondition="Equal to";
				   Weightcomdition="=";
			   }
			   else if(whereColumn.indexOf("<>")>0){
				   actualVolumeCondition="Not Equal to";
				   Weightcomdition="<>";
			   }
			   else if(whereColumn.indexOf(">")>0){
				   actualVolumeCondition="Greater than";
				   Weightcomdition=">";
			   }
			   else if(whereColumn.indexOf(">=")>0){
				   actualVolumeCondition="Greater than Equal to";
				   Weightcomdition=">=";
			   }
			   else if(whereColumn.indexOf("<")>0){
				   actualVolumeCondition="Less than";
				   Weightcomdition="<";
			   }
			   else if(whereColumn.indexOf("<=")>0){
				   actualVolumeCondition="Less than Equal to";
				   Weightcomdition="<=";
			   } 
			   whereColumn= whereColumn.substring(whereColumn.indexOf(Weightcomdition));
			   actualVolumeType=whereColumn;
			   actualVolumeType=actualVolumeType.trim();
			   if (actualVolumeType.indexOf("=") == 0) {
				   actualVolumeType = actualVolumeType.substring(1);
				}
			   if (actualVolumeType.indexOf("<>") == 0) {
				   actualVolumeType = actualVolumeType.substring(2);
				}
			    if (actualVolumeType.indexOf(">") == 0) {
			    	actualVolumeType = actualVolumeType.substring(1);
					}
			    if (actualVolumeType.indexOf(">=") == 0) {
			    	actualVolumeType = actualVolumeType.substring(2);
					}
			    if (actualVolumeType.indexOf("<") == 0) {
			    	actualVolumeType = actualVolumeType.substring(1);
					}
			    if (actualVolumeType.indexOf("<=") == 0) {
			    	actualVolumeType = actualVolumeType.substring(2);
					}
			    if (actualVolumeType.indexOf("'") == 0) {
			    	actualVolumeType = actualVolumeType.substring(1);
				}
				if (actualVolumeType.lastIndexOf("'") == actualVolumeType.length() - 1) {
					actualVolumeType = actualVolumeType.substring(0, actualVolumeType.length() - 1);
				}
		   }
		   if(whereColumn.indexOf("s.companyDivision")>0){
			   if(whereColumn.indexOf("=")>0){
				   companyCodeCondition="Equal to";
			   }
			   else{
				   companyCodeCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   companyCode=whereColumn;
			   companyCode=companyCode.trim();
			   if (companyCode.indexOf("=") == 0) {
				   companyCode = companyCode.substring(1);
				}
			    if (companyCode.indexOf("'") == 0) {
			    	companyCode = companyCode.substring(1);
				}
				if (companyCode.lastIndexOf("'") == companyCode.length() - 1) {
					companyCode = companyCode.substring(0, companyCode.length() - 1);
				}
			   }
			   else { 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   companyCode=whereColumn;
				   companyCode=companyCode.trim();
				   if (companyCode.indexOf("<>") == 0) {
					   companyCode = companyCode.substring(2);
					}
				    if (companyCode.indexOf("'") == 0) {
				    	companyCode = companyCode.substring(1);
					}
					if (companyCode.lastIndexOf("'") == companyCode.length() - 1) {
						companyCode = companyCode.substring(0, companyCode.length() - 1);
					}
				    
			   }
		   }
		   if(whereColumn.indexOf("DATE_FORMAT(t.loadA,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf(">=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					beginDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
			   if(whereColumn.indexOf("<=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					endDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
		   }
		   if(whereColumn.indexOf("(DATE_FORMAT(s.createdon,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf(">=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					createDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
			   if(whereColumn.indexOf("<=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					toCreateDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
		   }
		   if(whereColumn.indexOf("(DATE_FORMAT(s.updatedOn,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf(">=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					updateDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
			   if(whereColumn.indexOf("<=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					toUpdateDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
		   }
		   if(whereColumn.indexOf("(DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf(">=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					deliveryTBeginDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
			   if(whereColumn.indexOf("<=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					deliveryTEndDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
		   }
		   if(whereColumn.indexOf("(DATE_FORMAT(t.deliveryA,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf(">=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					deliveryActBeginDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
			   if(whereColumn.indexOf("<=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					deliveryActEndDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
		   }
		   if(whereColumn.indexOf("(DATE_FORMAT(t.beginLoad,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf(">=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					loadTgtBeginDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
			   if(whereColumn.indexOf("<=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					loadTgtEndDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
		   }
		   if(whereColumn.indexOf("(DATE_FORMAT(a.recPostDate,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf("or")<=0){
			   if(whereColumn.indexOf(">=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					invPostingBeginDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
			   if(whereColumn.indexOf("<=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					invPostingEndDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
		   }
			}
		   if(whereColumn.indexOf("(DATE_FORMAT(a.payPostDate,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf("or")<=0){
			   if(whereColumn.indexOf(">=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					payPostingBeginDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
			   if(whereColumn.indexOf("<=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					payPostingEndDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
		   }
		   }
		   if(whereColumn.indexOf("(DATE_FORMAT(a.receivedDate,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf(">=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					receivedBeginDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
			   if(whereColumn.indexOf("<=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					receivedEndDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
		   }
		   if(whereColumn.indexOf("(DATE_FORMAT(a.recPostDate,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf("or")>0){
			   if(whereColumn.indexOf(">=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					postingPeriodBeginDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
			   if(whereColumn.indexOf("or")>0){
			   if(whereColumn.indexOf("<=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					postingPeriodEndDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
			   }
		   }
		   }
		   if(whereColumn.indexOf("(DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf(">=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					invoicingBeginDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
			   if(whereColumn.indexOf("<=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					invoicingEndDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
		   }
		   if(whereColumn.indexOf("(DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf(">=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					storageBeginDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   } 
		   }
		   if(whereColumn.indexOf("(DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf("<=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					storageEndDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   } 
		   }
		   if(whereColumn.indexOf("(DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf(">=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					revenueRecognitionBeginDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
			   if(whereColumn.indexOf("<=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					revenueRecognitionEndDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
		   }
		   if(whereColumn.indexOf("(DATE_FORMAT(c.bookingDate,'%Y-%m-%d')")>0){
			   if(whereColumn.indexOf(">=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					bookingDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
			   if(whereColumn.indexOf("<=")>0){ 
				   String dateValue=whereColumn;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = whereColumn.substring(0, whereColumn.length() - 1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try{ 
					toBookingDate=sdf.parse(dateValue); 
				}
				catch(ParseException e)
				{
					e.printStackTrace();
				}  
			   }
		   }
		   if(whereColumn.indexOf("m.carrier")>0){
			   if(whereColumn.indexOf("=")>0){
				   carrierCondition="Equal to";
			   }
			   else{
				   carrierCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   carrier=whereColumn;
			   carrier=carrier.trim();
			   if (carrier.indexOf("=") == 0) {
				   carrier = carrier.substring(1);
				}
			    if (carrier.indexOf("'") == 0) {
			    	carrier = carrier.substring(1);
				}
				if (carrier.lastIndexOf("'") == carrier.length() - 1) {
					carrier = carrier.substring(0, carrier.length() - 1);
				}
		     }else { 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   carrier=whereColumn;
				   carrier=carrier.trim();
				   if (carrier.indexOf("<>") == 0) {
					   carrier = carrier.substring(2);
					}
				    if (carrier.indexOf("'") == 0) {
				    	carrier = carrier.substring(1);
					}
					if (carrier.lastIndexOf("'") == carrier.length() - 1) {
						carrier = carrier.substring(0, carrier.length() - 1);
					}				   
			   }
		   }
		   if(whereColumn.indexOf("m.driverId")>0){
			   if(whereColumn.indexOf("=")>0){
				   driverIdCondition="Equal to";
			   }
			   else{
				   driverIdCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   driverId=whereColumn;
			   driverId=driverId.trim();
			   if (driverId.indexOf("=") == 0) {
				   driverId = driverId.substring(1);
				}
			    if (driverId.indexOf("'") == 0) {
			    	driverId = driverId.substring(1);
				}
				if (driverId.lastIndexOf("'") == driverId.length() - 1) {
					driverId = driverId.substring(0, driverId.length() - 1);
				}
		     }else { 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   driverId=whereColumn;
				   driverId=driverId.trim();
				   if (driverId.indexOf("<>") == 0) {
					   driverId = driverId.substring(2);
					}
				    if (driverId.indexOf("'") == 0) {
				    	driverId = driverId.substring(1);
					}
					if (driverId.lastIndexOf("'") == driverId.length() - 1) {
						driverId = driverId.substring(0, driverId.length() - 1);
					}				   
			   }
		   }
		   if(whereColumn.indexOf("s.status")>0){
			   if(whereColumn.indexOf("('CLSD','CNCL','DWND','DWNLD')")>0){
				   activeStatus="Active";
			   }
			   else if(whereColumn.indexOf("('CLSD','CNCL')")>0){
				   activeStatus="Inactive";
			   } 
			   else if(whereColumn.indexOf("('CNCL')")>0){
				   activeStatus="All (Excl. Cancel)";
			   }
			   else {
				   activeStatus="All";
			   }
		   }
		   else {
			   if(whereColumn.indexOf("('CLSD','CNCL','DWND','DWNLD')")>0){
				   activeStatus="Active";
			   }
			   else if(whereColumn.indexOf("('CLSD','CNCL')")>0){
				   activeStatus="Inactive";
			   } 
			   else if(whereColumn.indexOf("('CNCL')")>0){
				   activeStatus="All (Excl. Cancel)";
			   }
			   else {
				   activeStatus="All";
			   }
		   }
		   
		   
		   if(whereColumn.indexOf("c.accountCode")>0){
			   if(whereColumn.indexOf("=")>0){
				   accountCodeCondition="Equal to";
			   }
			   else{
				   accountCodeCondition="Not Equal to"; 
			   }
			   if(whereColumn.indexOf("=")>0){
			   whereColumn= whereColumn.substring(whereColumn.indexOf("="));
			   accountCodeType=whereColumn;
			   accountCodeType=accountCodeType.trim();
			   if (accountCodeType.indexOf("=") == 0) {
				   accountCodeType = accountCodeType.substring(1);
				}
			    if (accountCodeType.indexOf("'") == 0) {
			    	accountCodeType = accountCodeType.substring(1);
				}
				if (accountCodeType.lastIndexOf("'") == accountCodeType.length() - 1) {
					accountCodeType = accountCodeType.substring(0, accountCodeType.length() - 1);
				}
		   }
			   else { 
				   whereColumn= whereColumn.substring(whereColumn.indexOf("<>"));
				   accountCodeType=whereColumn;
				   accountCodeType=accountCodeType.trim();
				   if (accountCodeType.indexOf("<>") == 0) {
					   accountCodeType = accountCodeType.substring(2);
					}
				    if (accountCodeType.indexOf("'") == 0) {
				    	accountCodeType = accountCodeType.substring(1);
					}
					if (accountCodeType.lastIndexOf("'") == accountCodeType.length() - 1) {
						accountCodeType = accountCodeType.substring(0, accountCodeType.length() - 1);
					}
			   
				   
			   }
		   }
		   
		 } 
		columnList=extractColumnMgmtManager.getExtractColum(reportName,sessionCorpID); 
		if(columnList!=null && (!columnList.isEmpty()))
		{
			Iterator it =columnList.iterator();
			int i=0;
			while (it.hasNext()) {
				Object obj=it.next(); 
				if(i==0)
				{
					columnList1.add(obj);
					i=1;
				}
				else if(i==1){
					columnList2.add(obj);
					i=2;
				}
				else if(i==2){
					columnList3.add(obj);
					i=3;
				}
				else if(i==3){
					columnList4.add(obj);
					i=4;
				}
				else {
					columnList5.add(obj);
					i=0;
				}
			}
		}
		return SUCCESS;
	}
	public void showSchedulerDataExtract() throws Exception{ 
		List schedulerQueryList =extractManager.getSchedulerQueryList();
		Iterator itscheduler = schedulerQueryList.iterator();
		while(itscheduler.hasNext()){
			id=(Long)itscheduler.next(); 
		extractQueryFile = extractManager.get(id);
		String whereCondition = extractQueryFile.getQueryCondition();
		whereCondition=whereCondition.replaceAll("#", " ");
		String selectQuery=extractQueryFile.getSelectQuery();
		String extractType=extractQueryFile.getExtractType();
		String createdBy=extractQueryFile.getCreatedby();
		String QueryName=extractQueryFile.getQueryName();
		user = (User) claimManager.findUserDetails(createdBy).get(0);
		String UserEmailId=user.getEmail();
		String columnSelect="";
		if(selectQuery.equals("")||selectQuery.equals(","))
	  	  {
	  	  }
	  	 else
	  	  {
	  		selectQuery=selectQuery.trim();
	  	    if(selectQuery.indexOf("#")==0)
	  		 {
	  	    	selectQuery=selectQuery.substring(1);
	  		 }
	  		if(selectQuery.lastIndexOf("#")==selectQuery.length()-1)
	  		 {
	  			selectQuery=selectQuery.substring(0, selectQuery.length()-1);
	  		 }
	  		columnSelect=selectQuery;
	  		selectQuery=selectQuery.replaceAll("#",",");
	  	  }
		List dataExtractList=new ArrayList();
		File file =new File("");
		String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources");
		uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "/dataExtract" + "/");
		String file_name = "c:/ashish/test.txt";
		if(extractType.equalsIgnoreCase("shipmentAnalysis")){
			dataExtractList = serviceOrderManager.dynamicActiveShipments(selectQuery, whereCondition.toString(),sessionCorpID);
			file = new File("ActiveShipments");
			file_name="ActiveShipments";
		}
		if(extractType.equalsIgnoreCase("dynamicFinancialSummary")){
			dataExtractList = serviceOrderManager.dynamicFinancialSummary(selectQuery, whereCondition.toString(),sessionCorpID);
			file = new File("DynamicFinancialSummary");
			file_name="DynamicFinancialSummary";
		}
		if(extractType.equalsIgnoreCase("dynamicFinancialDetails")){
			dataExtractList = serviceOrderManager.dynamicDataFinanceDetails(selectQuery, whereCondition.toString(),sessionCorpID);
			file = new File("DynamicFinancialDetails");
			file_name="DynamicFinancialDetails";
		}
		if(extractType.equalsIgnoreCase("domesticAnalysis")){
			dataExtractList = serviceOrderManager.dynamicDataDomesticAnalysis(selectQuery, whereCondition.toString(),sessionCorpID);
			file = new File("DomesticAnalysis");
			file_name="DomesticAnalysis";
		}
		if(extractType.equalsIgnoreCase("storageBillingAnalysis")){
			dataExtractList = serviceOrderManager.dynamicStorageAnalysis(selectQuery, whereCondition.toString(),sessionCorpID);
			file = new File("StorageBillingAnalysis");
			file_name="StorageBillingAnalysis";
		}
		if(extractType.equalsIgnoreCase("RelocationAnalysis")){
			dataExtractList = serviceOrderManager.dynamicReloServices(selectQuery, whereCondition.toString(),sessionCorpID);
			file = new File("RelocationAnalysis");
			file_name="RelocationAnalysis";
		}
		//HttpServletResponse response = getResponse();
		//response.setContentType("application/vnd.ms-excel");
		//ServletOutputStream outputStream = response.getOutputStream(); 
		//response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" + "\t The number of line is" + dataExtractList.size() + "extracted");
		//out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + dataExtractList.size() + "\t is extracted");
		String fileName=file_name+".xls";
		file_name=uploadDir+file_name+".xls";
        FileWriter file1 = new FileWriter(file_name);
        BufferedWriter out = new BufferedWriter (file1);  
		String bA_SSCW = "";
		String[] conditionArray=columnSelect.split("#");
	    int arrayLength = conditionArray.length;
		for(int i=0;i<arrayLength;i++)
		 {	
		   String condition=conditionArray[i]; 
		   String []columnArray=condition.split(" as");
		   //condition= condition.substring(condition.indexOf("as")+2);
		   String column=columnArray[1];
		   out.write((column+"\t"));
		 } 
		out.write("\r\n");
		Iterator it = dataExtractList.iterator();

		while(it.hasNext()){
			Object[] row = (Object[]) it.next();

			for(int i=0; i<arrayLength; i++){
				if (row[i] != null) {
					String data = row[i].toString();
					out.write((data + "\t"));
				} else {
					out.write("\t");
				}
			}
			out.write("\n");
	    }
		out.close();
           String host = "mail.trilasoft.com";
		   String from = "support@redskymobility.com";
			String tempRecipient="";
			String tempRecipientArr[]=UserEmailId.split(",");
			for(String str1:tempRecipientArr){
				if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
					if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
					tempRecipient += str1;
				}
			}
			UserEmailId=tempRecipient;			
		   String msgText1 = "Hi "+user.getFirstName()+" " +user.getLastName()+",\n\nPlease check the attached auto generated extract file.\n\nSchedule Details\nFile Name:"+fileName;
		   
		  
		  try{
		      
			  EmailSetUpUtil.sendMail(UserEmailId, "", "", QueryName, msgText1, file_name, from);
		  }
		 
		  catch(Exception e){
			  e.printStackTrace();
		  }
	}
	}
	
	public String dataExtractSendMail() throws Exception{ 
		String UserEmailId="";
	try{
	extractQueryFile = extractManager.get(id);
	String whereCondition = extractQueryFile.getQueryCondition();
	String datePeriodCondition=extractQueryFile.getDatePeriod();
	String[] datePeriodArray=datePeriodCondition.split(",");
	String whereConditionNew="";
	String periodDateValue="";
	String[] arraywhere=whereCondition.split("#"); 
	List<String> wordList = Arrays.asList(arraywhere); 
	Iterator iterator =wordList.iterator();
	if(datePeriodCondition!=null &&(!(datePeriodCondition.equals("")))){
	while(iterator.hasNext()){
		String whereConditionElement= (String)iterator.next();
		//System.out.println("\n\n\n\n\n\n\n\n\n  whereConditionElement   "+whereConditionElement);
		String ifCondition="true";
		if((whereConditionElement.indexOf("DATE_FORMAT(t.loadA,'%Y-%m-%d')")>0) && (datePeriodCondition.indexOf("loadActualDatePeriod")>=0)){
			int arrayLength = datePeriodArray.length; 
	  		for(int i=0;i<arrayLength;i++)
	  		 { 
	  			periodDateValue =datePeriodArray[i]; 
	  		   if(!(periodDateValue.indexOf("loadActualDatePeriod")<0)){
	  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
	  		     break;
	  		   }else{
	  			 periodDateValue=""; 
	  		   }
		          
	         }
	  		if(!periodDateValue.equals("")){
			String  returnValue=   findPeriodDate(periodDateValue);
        	String [] returnValueArray=returnValue.split("#");
            String	Date1 = new String(returnValueArray[0]);
            String	Date2 = new String(returnValueArray[1]);   
			String whereElement=whereConditionElement;
            if(whereConditionElement.indexOf(">=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date1);
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   }
			   if(whereConditionElement.indexOf("<=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date2);
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   } 
	  		}
	  		else{
	  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
	  		}
	  		ifCondition="false";
	  	} else if((whereConditionElement.indexOf("(DATE_FORMAT(s.createdon,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("createDatePeriod")>=0) ){
			int arrayLength = datePeriodArray.length; 
	  		for(int i=0;i<arrayLength;i++)
	  		 { 
	  			periodDateValue =datePeriodArray[i]; 
	  		   if(!(periodDateValue.indexOf("createDatePeriod")<0)){
	  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
	  		     break;
	  		   }else{
	  			 periodDateValue=""; 
	  		   }
		          
	         }
	  		if(!periodDateValue.equals("")){
			String  returnValue=   findPeriodDate(periodDateValue);
        	String [] returnValueArray=returnValue.split("#");
            String	Date1 = new String(returnValueArray[0]);
            String	Date2 = new String(returnValueArray[1]);   
			String whereElement=whereConditionElement;
            if(whereConditionElement.indexOf(">=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date1);
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   }
			   if(whereConditionElement.indexOf("<=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date2); 
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   } 
	  		}
	  		else{
	  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
	  		}
	  		ifCondition="false";
	  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(s.updatedOn,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("updateDatePeriod")>=0) ){
			int arrayLength = datePeriodArray.length; 
	  		for(int i=0;i<arrayLength;i++)
	  		 { 
	  			periodDateValue =datePeriodArray[i]; 
	  		   if(!(periodDateValue.indexOf("updateDatePeriod")<0)){
	  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
	  		     break;
	  		   }else{
	  			 periodDateValue=""; 
	  		   }
		          
	         }
	  		if(!periodDateValue.equals("")){
			String  returnValue=   findPeriodDate(periodDateValue);
        	String [] returnValueArray=returnValue.split("#");
            String	Date1 = new String(returnValueArray[0]);
            String	Date2 = new String(returnValueArray[1]);   
			String whereElement=whereConditionElement;
            if(whereConditionElement.indexOf(">=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date1);
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   }
			   if(whereConditionElement.indexOf("<=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date2); 
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   } 
	  		}
	  		else{
	  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
	  		}
	  		ifCondition="false";
	  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("deliveryTargetDatePeriod")>=0)){
			int arrayLength = datePeriodArray.length; 
	  		for(int i=0;i<arrayLength;i++)
	  		 { 
	  			periodDateValue =datePeriodArray[i]; 
	  		   if(!(periodDateValue.indexOf("deliveryTargetDatePeriod")<0)){
	  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
	  		     break;
	  		   }else{
	  			 periodDateValue=""; 
	  		   }
		          
	         }
	  		if(!periodDateValue.equals("")){
			String  returnValue=   findPeriodDate(periodDateValue);
        	String [] returnValueArray=returnValue.split("#");
            String	Date1 = new String(returnValueArray[0]);
            String	Date2 = new String(returnValueArray[1]);   
			String whereElement=whereConditionElement;
            if(whereConditionElement.indexOf(">=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date1); 
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   }
			   if(whereConditionElement.indexOf("<=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date2); 
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   } 
	  		}
	  		else{
	  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
	  		}
	  		ifCondition="false";
	  	} else if((whereConditionElement.indexOf("(DATE_FORMAT(t.deliveryA,'%Y-%m-%d')")>0 )&&(datePeriodCondition.indexOf("deliveryActualDatePeriod")>=0)){
			int arrayLength = datePeriodArray.length; 
	  		for(int i=0;i<arrayLength;i++)
	  		 { 
	  			periodDateValue =datePeriodArray[i]; 
	  		   if(!(periodDateValue.indexOf("deliveryActualDatePeriod")<0)){
	  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
	  		     break;
	  		   }else{
	  			 periodDateValue=""; 
	  		   }
		          
	         }
	  		if(!periodDateValue.equals("")){
			String  returnValue=   findPeriodDate(periodDateValue);
        	String [] returnValueArray=returnValue.split("#");
            String	Date1 = new String(returnValueArray[0]);
            String	Date2 = new String(returnValueArray[1]);   
			String whereElement=whereConditionElement;
            if(whereConditionElement.indexOf(">=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date1); 
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   }
			   if(whereConditionElement.indexOf("<=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date2); 
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   } 
	  		}
	  		else{
	  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
	  		}
	  		ifCondition="false";
	  	} else if((whereConditionElement.indexOf("(DATE_FORMAT(t.beginLoad,'%Y-%m-%d')")>0) &&(datePeriodCondition.indexOf("loadTargetDatePeriod")>=0) ){
			int arrayLength = datePeriodArray.length; 
	  		for(int i=0;i<arrayLength;i++)
	  		 { 
	  			periodDateValue =datePeriodArray[i]; 
	  		   if(!(periodDateValue.indexOf("loadTargetDatePeriod")<0)){
	  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
	  		     break;
	  		   }else{
	  			 periodDateValue=""; 
	  		   }
		          
	         }
	  		if(!periodDateValue.equals("")){
			String  returnValue=   findPeriodDate(periodDateValue);
        	String [] returnValueArray=returnValue.split("#");
            String	Date1 = new String(returnValueArray[0]);
            String	Date2 = new String(returnValueArray[1]);   
			String whereElement=whereConditionElement;
            if(whereConditionElement.indexOf(">=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date1);
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   }
			   if(whereConditionElement.indexOf("<=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date2); 
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   } 
	  		}
	  		else{
	  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
	  		}
	  		ifCondition="false";
	  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.recPostDate,'%Y-%m-%d')")>0) &&(datePeriodCondition.indexOf("invoicePostingDatePeriod")>=0 || datePeriodCondition.indexOf("PostingPeriodDatePeriod")>=0 )){
			 if(whereConditionElement.indexOf("or")<0){
				 int arrayLength = datePeriodArray.length; 
		  		for(int i=0;i<arrayLength;i++)
		  		 { 
		  			periodDateValue =datePeriodArray[i]; 
		  		   if(!(periodDateValue.indexOf("invoicePostingDatePeriod")<0)){
		  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
		  		     break;
		  		   }else{
		  			 periodDateValue=""; 
		  		   }
			          
		         }
		  		if(!periodDateValue.equals("")){
				String  returnValue=   findPeriodDate(periodDateValue);
	        	String [] returnValueArray=returnValue.split("#");
	            String	Date1 = new String(returnValueArray[0]);
	            String	Date2 = new String(returnValueArray[1]);   
				String whereElement=whereConditionElement;
	            if(whereConditionElement.indexOf(">=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf(">="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date1);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   if(whereConditionElement.indexOf("<=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf("<="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date2); 
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   } 
		  		}else{
		  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
		  		}
		  		}else if(whereConditionElement.indexOf("or")>0){
					 int arrayLength = datePeriodArray.length; 
				  		for(int i=0;i<arrayLength;i++)
				  		 { 
				  			periodDateValue =datePeriodArray[i]; 
				  		   if(!(periodDateValue.indexOf("PostingPeriodDatePeriod")<0)){
				  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
				  		     break;
				  		   }else{
				  			 periodDateValue=""; 
				  		   }
					          
				         }
				  		if(!periodDateValue.equals("")){
						String  returnValue=   findPeriodDate(periodDateValue);
			        	String [] returnValueArray=returnValue.split("#");
			            String	Date1 = new String(returnValueArray[0]);
			            String	Date2 = new String(returnValueArray[1]);   
						String whereElement=whereConditionElement;
			            if(whereConditionElement.indexOf(">=")>0){ 
							   String dateValue=whereConditionElement;
							   dateValue=dateValue.trim();
							   dateValue= dateValue.substring(dateValue.indexOf(">="));
							   dateValue=dateValue.trim();  
							   dateValue = dateValue.substring(2); 
						    if (dateValue.indexOf("'") == 0) {
						    	dateValue = dateValue.substring(1);
							}
						    int j=dateValue.indexOf("'");
						    dateValue= dateValue.substring(0, j);
						    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							whereElement=whereElement.replaceAll(dateValue, Date1); 
							
						   }
						   if(whereConditionElement.indexOf("<=")>0){ 
							   String dateValue=whereConditionElement;
							   dateValue=dateValue.trim();
							   dateValue= dateValue.substring(dateValue.indexOf("<="));
							   dateValue=dateValue.trim();  
							   dateValue = dateValue.substring(2); 
						    if (dateValue.indexOf("'") == 0) {
						    	dateValue = dateValue.substring(1);
							}
						    int j=dateValue.indexOf("'");
						    dateValue= dateValue.substring(0, j);
						    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							whereElement=whereElement.replaceAll(dateValue, Date2);
							
						   }
						   whereConditionNew=whereConditionNew+"#"+whereElement; 
				  		}else{
				  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
				  		}
				  		} 
			 else{
				   whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	   
			   }
			 ifCondition="false";
	  	} else if((whereConditionElement.indexOf("(DATE_FORMAT(a.payPostDate,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("payablePostingDatePeriod")>=0 || datePeriodCondition.indexOf("PostingPeriodDatePeriod")>=0 )){
			   if(whereConditionElement.indexOf("or")<=0){
					 int arrayLength = datePeriodArray.length; 
				  		for(int i=0;i<arrayLength;i++)
				  		 { 
				  			periodDateValue =datePeriodArray[i]; 
				  		   if(!(periodDateValue.indexOf("payablePostingDatePeriod")<0)){
				  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
				  		     break;
				  		   }else{
				  			 periodDateValue=""; 
				  		   }
					          
				         }
				  		if(!periodDateValue.equals("")){
						String  returnValue=   findPeriodDate(periodDateValue);
			        	String [] returnValueArray=returnValue.split("#");
			            String	Date1 = new String(returnValueArray[0]);
			            String	Date2 = new String(returnValueArray[1]);   
						String whereElement=whereConditionElement;
			            if(whereConditionElement.indexOf(">=")>0){ 
							   String dateValue=whereConditionElement;
							   dateValue=dateValue.trim();
							   dateValue= dateValue.substring(dateValue.indexOf(">="));
							   dateValue=dateValue.trim();  
							   dateValue = dateValue.substring(2); 
						    if (dateValue.indexOf("'") == 0) {
						    	dateValue = dateValue.substring(1);
							}
						    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							whereElement=whereElement.replaceAll(dateValue, Date1); 
							whereConditionNew=whereConditionNew+"#"+whereElement; 
						   }
						   if(whereConditionElement.indexOf("<=")>0){ 
							   String dateValue=whereConditionElement;
							   dateValue=dateValue.trim();
							   dateValue= dateValue.substring(dateValue.indexOf("<="));
							   dateValue=dateValue.trim();  
							   dateValue = dateValue.substring(2); 
						    if (dateValue.indexOf("'") == 0) {
						    	dateValue = dateValue.substring(1);
							}
						    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
								dateValue = dateValue.substring(0, dateValue.length() - 1);
							}
							whereElement=whereElement.replaceAll(dateValue, Date2);
							whereConditionNew=whereConditionNew+"#"+whereElement; 
						   }
						   
				  		}else{
				  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
				  		}
				  		}else{
							   whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	   
						   }
			   ifCondition="false";   
	  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.receivedDate,'%Y-%m-%d')")>0)&& (datePeriodCondition.indexOf("receivedDatePeriod")>=0) ){
				int arrayLength = datePeriodArray.length; 
		  		for(int i=0;i<arrayLength;i++)
		  		 { 
		  			periodDateValue =datePeriodArray[i]; 
		  		   if(!(periodDateValue.indexOf("receivedDatePeriod")<0)){
		  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
		  		     break;
		  		   }else{
		  			 periodDateValue=""; 
		  		   }
			          
		         }
		  		if(!periodDateValue.equals("")){
				String  returnValue=   findPeriodDate(periodDateValue);
	        	String [] returnValueArray=returnValue.split("#");
	            String	Date1 = new String(returnValueArray[0]);
	            String	Date2 = new String(returnValueArray[1]);   
				String whereElement=whereConditionElement;
	            if(whereConditionElement.indexOf(">=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf(">="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date1);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   if(whereConditionElement.indexOf("<=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf("<="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date2); 
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   
		  		}
		  		else{
		  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
		  		}
		  		ifCondition="false";	
	  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')")>0)&& (datePeriodCondition.indexOf("InvoicingdatePeriod")>=0)){

			int arrayLength = datePeriodArray.length; 
	  		for(int i=0;i<arrayLength;i++)
	  		 { 
	  			periodDateValue =datePeriodArray[i]; 
	  		   if(!(periodDateValue.indexOf("InvoicingdatePeriod")<0)){
	  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
	  		     break;
	  		   }else{
	  			 periodDateValue=""; 
	  		   }
		          
	         }
	  		if(!periodDateValue.equals("")){
			String  returnValue=   findPeriodDate(periodDateValue);
        	String [] returnValueArray=returnValue.split("#");
            String	Date1 = new String(returnValueArray[0]);
            String	Date2 = new String(returnValueArray[1]);   
			String whereElement=whereConditionElement;
            if(whereConditionElement.indexOf(">=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date1); 
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   }
			   if(whereConditionElement.indexOf("<=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date2);
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   } 
	  		}
	  		else{
	  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
	  		} 
	  		ifCondition="false";
	  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')")>0)&& (datePeriodCondition.indexOf("storageDatePeriod")>=0)){
            int arrayLength = datePeriodArray.length; 
	  		for(int i=0;i<arrayLength;i++)
	  		 { 
	  			periodDateValue =datePeriodArray[i]; 
	  		   if(!(periodDateValue.indexOf("storageDatePeriod")<0)){
	  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
	  		     break;
	  		   }else{
	  			 periodDateValue=""; 
	  		   }
		          
	         }
	  		if(!periodDateValue.equals("")){
			String  returnValue=   findPeriodDate(periodDateValue);
        	String [] returnValueArray=returnValue.split("#");
            String	Date1 = new String(returnValueArray[0]);
            String	Date2 = new String(returnValueArray[1]);   
			String whereElement=whereConditionElement;
            if(whereConditionElement.indexOf(">=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date1);
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   }
			   if(whereConditionElement.indexOf("<=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date2);
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   } 
	  		}
	  		else{
	  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
	  		} 
		
	  		ifCondition="false";
	  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')")>0 )&& (datePeriodCondition.indexOf("storageDatePeriod")>=0)){

            int arrayLength = datePeriodArray.length; 
	  		for(int i=0;i<arrayLength;i++)
	  		 { 
	  			periodDateValue =datePeriodArray[i]; 
	  		   if(!(periodDateValue.indexOf("storageDatePeriod")<0)){
	  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
	  		     break;
	  		   }else{
	  			 periodDateValue=""; 
	  		   }
		          
	         }
	  		if(!periodDateValue.equals("")){
			String  returnValue=   findPeriodDate(periodDateValue);
        	String [] returnValueArray=returnValue.split("#");
            String	Date1 = new String(returnValueArray[0]);
            String	Date2 = new String(returnValueArray[1]);   
			String whereElement=whereConditionElement;
            if(whereConditionElement.indexOf(">=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date1);
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   }
			   if(whereConditionElement.indexOf("<=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date2); 
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   } 
	  		}
	  		else{
	  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
	  		}  
	  		ifCondition="false";
	  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')")>0)&& (datePeriodCondition.indexOf("revenueRecognitionPeriod")>=0)  ){


            int arrayLength = datePeriodArray.length; 
	  		for(int i=0;i<arrayLength;i++)
	  		 { 
	  			periodDateValue =datePeriodArray[i]; 
	  		   if(!(periodDateValue.indexOf("revenueRecognitionPeriod")<0)){
	  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
	  		     break;
	  		   }else{
	  			 periodDateValue=""; 
	  		   }
		          
	         }
	  		if(!periodDateValue.equals("")){
			String  returnValue=   findPeriodDate(periodDateValue);
        	String [] returnValueArray=returnValue.split("#");
            String	Date1 = new String(returnValueArray[0]);
            String	Date2 = new String(returnValueArray[1]);   
			String whereElement=whereConditionElement;
            if(whereConditionElement.indexOf(">=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date1);
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   }
			   if(whereConditionElement.indexOf("<=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date2);
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   } 
	  		}
	  		else{
	  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
	  		} 
	  		ifCondition="false";
		}else if((whereConditionElement.indexOf("(DATE_FORMAT(c.bookingDate,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("bookingDatePeriod")>=0) ){
			int arrayLength = datePeriodArray.length; 
	  		for(int i=0;i<arrayLength;i++)
	  		 { 
	  			periodDateValue =datePeriodArray[i]; 
	  		   if(!(periodDateValue.indexOf("bookingDatePeriod")<0)){
	  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
	  		     break;
	  		   }else{
	  			 periodDateValue=""; 
	  		   }		          
	         }
	  		if(!periodDateValue.equals("")){
			String  returnValue=   findPeriodDate(periodDateValue);
        	String [] returnValueArray=returnValue.split("#");
            String	Date1 = new String(returnValueArray[0]);
            String	Date2 = new String(returnValueArray[1]);   
			String whereElement=whereConditionElement;
            if(whereConditionElement.indexOf(">=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf(">="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date1);
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   }
			   if(whereConditionElement.indexOf("<=")>0){ 
				   String dateValue=whereConditionElement;
				   dateValue=dateValue.trim();
				   dateValue= dateValue.substring(dateValue.indexOf("<="));
				   dateValue=dateValue.trim();  
				   dateValue = dateValue.substring(2); 
			    if (dateValue.indexOf("'") == 0) {
			    	dateValue = dateValue.substring(1);
				}
			    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
					dateValue = dateValue.substring(0, dateValue.length() - 1);
				}
				whereElement=whereElement.replaceAll(dateValue, Date2); 
				whereConditionNew=whereConditionNew+"#"+whereElement; 
			   } 
	  		}
	  		else{
	  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
	  		}
	  		ifCondition="false"; 
		}
	  	else if(ifCondition.equals("true")){
		      whereConditionNew=whereConditionNew+"#"+whereConditionElement; 
		}
	}
	whereCondition=whereConditionNew;
	} 
	whereCondition=whereCondition.replaceAll("#", " ");
	String selectQuery=extractQueryFile.getSelectQuery();
	String extractType=extractQueryFile.getExtractType();
	String createdBy=extractQueryFile.getCreatedby();
	String QueryName=extractQueryFile.getQueryName();
	String corpid=extractQueryFile.getCorpID(); 
	user = (User) claimManager.findUserDetails(createdBy).get(0);
	UserEmailId=extractQueryFile.getEmail(); 
	String columnSelect="";
	if(selectQuery.equals("")||selectQuery.equals(","))
  	  {
  	  }
  	 else
  	  {
  		selectQuery=selectQuery.trim();
  	    if(selectQuery.indexOf("#")==0)
  		 {
  	    	selectQuery=selectQuery.substring(1);
  		 }
  		if(selectQuery.lastIndexOf("#")==selectQuery.length()-1)
  		 {
  			selectQuery=selectQuery.substring(0, selectQuery.length()-1);
  		 }
  		columnSelect=selectQuery;
  		selectQuery=selectQuery.replaceAll("#",",");
  	  }
	List dataExtractList=new ArrayList();
	File file =new File("");
	String uploadDir =("/resources");
	uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "/dataExtract" + "/");
	String file_name = "c:/ashish/test.txt";
	if(extractType.equalsIgnoreCase("shipmentAnalysis")){
		dataExtractList = serviceOrderManager.dynamicActiveShipments(selectQuery, whereCondition.toString(),corpid);
		file = new File("ActiveShipments");
		file_name="ActiveShipments";
	}
	if(extractType.equalsIgnoreCase("dynamicFinancialSummary")){
		dataExtractList = serviceOrderManager.dynamicFinancialSummary(selectQuery, whereCondition.toString(),corpid);
		file = new File("DynamicFinancialSummary");
		file_name="DynamicFinancialSummary";
	}
	if(extractType.equalsIgnoreCase("dynamicFinancialDetails")){
		dataExtractList = serviceOrderManager.dynamicDataFinanceDetails(selectQuery, whereCondition.toString(),corpid);
		file = new File("DynamicFinancialDetails");
		file_name="DynamicFinancialDetails";
	}
	if(extractType.equalsIgnoreCase("domesticAnalysis")){
		dataExtractList = serviceOrderManager.dynamicDataDomesticAnalysis(selectQuery, whereCondition.toString(),corpid);
		file = new File("DomesticAnalysis");
		file_name="DomesticAnalysis";
	}
	if(extractType.equalsIgnoreCase("storageBillingAnalysis")){
		dataExtractList = serviceOrderManager.dynamicStorageAnalysis(selectQuery, whereCondition.toString(),corpid);
		file = new File("StorageBillingAnalysis");
		file_name="StorageBillingAnalysis";
	}  
	if(extractType.equalsIgnoreCase("RelocationAnalysis")){
		dataExtractList = serviceOrderManager.dynamicReloServices(selectQuery, whereCondition.toString(),corpid);
		file = new File("RelocationAnalysis");
		file_name="RelocationAnalysis";
	}
	String fileName=file_name+".xls";
	file_name=uploadDir+file_name+".xls";
    FileWriter file1 = new FileWriter(file_name);
    BufferedWriter out = new BufferedWriter (file1);  
	String bA_SSCW = ""; 
	String[] conditionArray=columnSelect.split("#");
    int arrayLength = conditionArray.length;
	for(int i=0;i<arrayLength;i++)
	 {	
	   String condition=conditionArray[i]; 
	   String []columnArray=condition.split(" as"); 
	   String column=columnArray[1];
	   out.write((column+"\t"));
	 } 
	out.write("\r\n");
	Iterator it2 = dataExtractList.iterator();

	while(it2.hasNext()){
		Object[] row = (Object[]) it2.next();

		for(int i=0; i<arrayLength; i++){
			if (row[i] != null) {
				String data = row[i].toString();
				out.write((data + "\t"));
			} else {
				out.write("\t");
			}
		}
		out.write("\n");
    }
	out.close(); 
    
	   String from = "support@redskymobility.com";
		String tempRecipient="";
		String tempRecipientArr[]=UserEmailId.split(",");
		for(String str1:tempRecipientArr){
			if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
				if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
				tempRecipient += str1;
			}
		}
		UserEmailId=tempRecipient;
	   
	   String msgText1 = "Dear Requester,\n\nThis email was sent by the RedSky."+" \n\nSchedule Details\nFile Name:"+fileName+"\n\n\n\n Regards, "+"\n RedSky Support Team.";
	   
	  
	  try{
	     
		  EmailSetUpUtil.sendMail(UserEmailId, "", "", QueryName, msgText1, file_name, from);
	       extractManager.updateSendMailStatus(id,UserEmailId,"successfully","mail send successfully") ; 
	       String key = "mail send to "+UserEmailId+" successfully.";
	       saveMessage(getText(key));
	       mailStatusReturn="OK";
	       return findOldQueries();
	  }
	  catch(Exception sfe)
	   {
		  String emailmessage=sfe.toString();
	 	 if(emailmessage.length()>200){
	 		emailmessage=emailmessage.substring(200);	 
	 	 }
	 	extractManager.updateSendMailStatus(id,UserEmailId,"error",emailmessage) ;
	 	String key = "Error in  sending mail.";
	 	errorMessage(getText(key)); 
	 	mailStatusReturn="OK";
	 	return findOldQueries();
	   } 
	}catch (Exception e) { 
		e.printStackTrace();
		String emailmessage=e.toString();
	 	 if(emailmessage.length()>200){
	 		emailmessage=emailmessage.substring(200);	 
	 	 }
	 	extractManager.updateSendMailStatus(id,UserEmailId,"error",emailmessage) ;
	 	String key = "Error in  sending mail.";
	 	errorMessage(getText(key)); 
	 	mailStatusReturn="OK";
	 	return findOldQueries();
     }
	
	}
	
	
	public void showOldDataExtract() throws Exception{
		company=companyManager.findByCorpID(sessionCorpID).get(0);
	    if(company!=null){
	      if(company.getExtractedFileAudit() !=null && company.getExtractedFileAudit()){
	    	  extractFileAudit = true;
	  		}
	    }
		extractQueryFile = extractManager.get(id);
		String whereCondition = extractQueryFile.getQueryCondition();
		String datePeriodCondition=extractQueryFile.getDatePeriod();
		String[] datePeriodArray=datePeriodCondition.split(",");
		String whereConditionNew="";
		String periodDateValue="";
		String[] arraywhere=whereCondition.split("#"); 
		List<String> wordList = Arrays.asList(arraywhere); 
		Iterator iterator =wordList.iterator();
		if(datePeriodCondition!=null &&(!(datePeriodCondition.equals("")))){
		while(iterator.hasNext()){
			String whereConditionElement= (String)iterator.next();
			//System.out.println("\n\n\n\n\n\n\n\n\n  whereConditionElement   "+whereConditionElement);
			String ifCondition="true";
			if((whereConditionElement.indexOf("DATE_FORMAT(t.loadA,'%Y-%m-%d')")>0) && (datePeriodCondition.indexOf("loadActualDatePeriod")>=0)){
				int arrayLength = datePeriodArray.length; 
		  		for(int i=0;i<arrayLength;i++)
		  		 { 
		  			periodDateValue =datePeriodArray[i]; 
		  		   if(!(periodDateValue.indexOf("loadActualDatePeriod")<0)){
		  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
		  		     break;
		  		   }else{
		  			 periodDateValue=""; 
		  		   }
			          
		         }
		  		if(!periodDateValue.equals("")){
				String  returnValue=   findPeriodDate(periodDateValue);
	        	String [] returnValueArray=returnValue.split("#");
	            String	Date1 = new String(returnValueArray[0]);
	            String	Date2 = new String(returnValueArray[1]);   
				String whereElement=whereConditionElement;
	            if(whereConditionElement.indexOf(">=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf(">="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date1);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   if(whereConditionElement.indexOf("<=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf("<="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date2);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   } 
		  		}
		  		else{
		  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
		  		}
		  		ifCondition="false";
		  	} else if((whereConditionElement.indexOf("(DATE_FORMAT(s.createdon,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("createDatePeriod")>=0) ){
				int arrayLength = datePeriodArray.length; 
		  		for(int i=0;i<arrayLength;i++)
		  		 { 
		  			periodDateValue =datePeriodArray[i]; 
		  		   if(!(periodDateValue.indexOf("createDatePeriod")<0)){
		  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
		  		     break;
		  		   }else{
		  			 periodDateValue=""; 
		  		   }
			          
		         }
		  		if(!periodDateValue.equals("")){
				String  returnValue=   findPeriodDate(periodDateValue);
	        	String [] returnValueArray=returnValue.split("#");
	            String	Date1 = new String(returnValueArray[0]);
	            String	Date2 = new String(returnValueArray[1]);   
				String whereElement=whereConditionElement;
	            if(whereConditionElement.indexOf(">=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf(">="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date1);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   if(whereConditionElement.indexOf("<=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf("<="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date2); 
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   } 
		  		}
		  		else{
		  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
		  		}
		  		ifCondition="false";
		  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(s.updatedOn,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("updateDatePeriod")>=0) ){
				int arrayLength = datePeriodArray.length; 
		  		for(int i=0;i<arrayLength;i++)
		  		 { 
		  			periodDateValue =datePeriodArray[i]; 
		  		   if(!(periodDateValue.indexOf("updateDatePeriod")<0)){
		  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
		  		     break;
		  		   }else{
		  			 periodDateValue=""; 
		  		   }
			          
		         }
		  		if(!periodDateValue.equals("")){
				String  returnValue=   findPeriodDate(periodDateValue);
	        	String [] returnValueArray=returnValue.split("#");
	            String	Date1 = new String(returnValueArray[0]);
	            String	Date2 = new String(returnValueArray[1]);   
				String whereElement=whereConditionElement;
	            if(whereConditionElement.indexOf(">=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf(">="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date1);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   if(whereConditionElement.indexOf("<=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf("<="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date2); 
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   } 
		  		}
		  		else{
		  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
		  		}
		  		ifCondition="false";
		  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(s.updatedOn,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("updateDatePeriod")>=0) ){
				int arrayLength = datePeriodArray.length; 
		  		for(int i=0;i<arrayLength;i++)
		  		 { 
		  			periodDateValue =datePeriodArray[i]; 
		  		   if(!(periodDateValue.indexOf("updateDatePeriod")<0)){
		  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
		  		     break;
		  		   }else{
		  			 periodDateValue=""; 
		  		   }
			          
		         }
		  		if(!periodDateValue.equals("")){
				String  returnValue=   findPeriodDate(periodDateValue);
	        	String [] returnValueArray=returnValue.split("#");
	            String	Date1 = new String(returnValueArray[0]);
	            String	Date2 = new String(returnValueArray[1]);   
				String whereElement=whereConditionElement;
	            if(whereConditionElement.indexOf(">=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf(">="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date1);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   if(whereConditionElement.indexOf("<=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf("<="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date2); 
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   } 
		  		}
		  		else{
		  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
		  		}
		  		ifCondition="false";
		  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("deliveryTargetDatePeriod")>=0)){
				int arrayLength = datePeriodArray.length; 
		  		for(int i=0;i<arrayLength;i++)
		  		 { 
		  			periodDateValue =datePeriodArray[i]; 
		  		   if(!(periodDateValue.indexOf("deliveryTargetDatePeriod")<0)){
		  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
		  		     break;
		  		   }else{
		  			 periodDateValue=""; 
		  		   }
			          
		         }
		  		if(!periodDateValue.equals("")){
				String  returnValue=   findPeriodDate(periodDateValue);
	        	String [] returnValueArray=returnValue.split("#");
	            String	Date1 = new String(returnValueArray[0]);
	            String	Date2 = new String(returnValueArray[1]);   
				String whereElement=whereConditionElement;
	            if(whereConditionElement.indexOf(">=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf(">="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date1); 
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   if(whereConditionElement.indexOf("<=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf("<="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date2); 
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   } 
		  		}
		  		else{
		  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
		  		}
		  		ifCondition="false";
		  	} else if((whereConditionElement.indexOf("(DATE_FORMAT(t.deliveryA,'%Y-%m-%d')")>0 )&&(datePeriodCondition.indexOf("deliveryActualDatePeriod")>=0)){
				int arrayLength = datePeriodArray.length; 
		  		for(int i=0;i<arrayLength;i++)
		  		 { 
		  			periodDateValue =datePeriodArray[i]; 
		  		   if(!(periodDateValue.indexOf("deliveryActualDatePeriod")<0)){
		  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
		  		     break;
		  		   }else{
		  			 periodDateValue=""; 
		  		   }
			          
		         }
		  		if(!periodDateValue.equals("")){
				String  returnValue=   findPeriodDate(periodDateValue);
	        	String [] returnValueArray=returnValue.split("#");
	            String	Date1 = new String(returnValueArray[0]);
	            String	Date2 = new String(returnValueArray[1]);   
				String whereElement=whereConditionElement;
	            if(whereConditionElement.indexOf(">=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf(">="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date1); 
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   if(whereConditionElement.indexOf("<=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf("<="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date2); 
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   } 
		  		}
		  		else{
		  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
		  		}
		  		ifCondition="false";
		  	} else if((whereConditionElement.indexOf("(DATE_FORMAT(t.beginLoad,'%Y-%m-%d')")>0) &&(datePeriodCondition.indexOf("loadTargetDatePeriod")>=0) ){
				int arrayLength = datePeriodArray.length; 
		  		for(int i=0;i<arrayLength;i++)
		  		 { 
		  			periodDateValue =datePeriodArray[i]; 
		  		   if(!(periodDateValue.indexOf("loadTargetDatePeriod")<0)){
		  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
		  		     break;
		  		   }else{
		  			 periodDateValue=""; 
		  		   }
			          
		         }
		  		if(!periodDateValue.equals("")){
				String  returnValue=   findPeriodDate(periodDateValue);
	        	String [] returnValueArray=returnValue.split("#");
	            String	Date1 = new String(returnValueArray[0]);
	            String	Date2 = new String(returnValueArray[1]);   
				String whereElement=whereConditionElement;
	            if(whereConditionElement.indexOf(">=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf(">="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date1);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   if(whereConditionElement.indexOf("<=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf("<="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date2); 
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   } 
		  		}
		  		else{
		  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
		  		}
		  		ifCondition="false";
		  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.recPostDate,'%Y-%m-%d')")>0) &&(datePeriodCondition.indexOf("invoicePostingDatePeriod")>=0 || datePeriodCondition.indexOf("PostingPeriodDatePeriod")>=0 )){
				 if(whereConditionElement.indexOf("or")<0){
					 int arrayLength = datePeriodArray.length; 
			  		for(int i=0;i<arrayLength;i++)
			  		 { 
			  			periodDateValue =datePeriodArray[i]; 
			  		   if(!(periodDateValue.indexOf("invoicePostingDatePeriod")<0)){
			  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
			  		     break;
			  		   }else{
			  			 periodDateValue=""; 
			  		   }
				          
			         }
			  		if(!periodDateValue.equals("")){
					String  returnValue=   findPeriodDate(periodDateValue);
		        	String [] returnValueArray=returnValue.split("#");
		            String	Date1 = new String(returnValueArray[0]);
		            String	Date2 = new String(returnValueArray[1]);   
					String whereElement=whereConditionElement;
		            if(whereConditionElement.indexOf(">=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf(">="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date1);
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   }
					   if(whereConditionElement.indexOf("<=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf("<="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date2); 
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   } 
			  		}else{
			  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
			  		}
			  		}else if(whereConditionElement.indexOf("or")>0){
						 int arrayLength = datePeriodArray.length; 
					  		for(int i=0;i<arrayLength;i++)
					  		 { 
					  			periodDateValue =datePeriodArray[i]; 
					  		   if(!(periodDateValue.indexOf("PostingPeriodDatePeriod")<0)){
					  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
					  		     break;
					  		   }else{
					  			 periodDateValue=""; 
					  		   }
						          
					         }
					  		if(!periodDateValue.equals("")){
							String  returnValue=   findPeriodDate(periodDateValue);
				        	String [] returnValueArray=returnValue.split("#");
				            String	Date1 = new String(returnValueArray[0]);
				            String	Date2 = new String(returnValueArray[1]);   
							String whereElement=whereConditionElement;
				            if(whereConditionElement.indexOf(">=")>0){ 
								   String dateValue=whereConditionElement;
								   dateValue=dateValue.trim();
								   dateValue= dateValue.substring(dateValue.indexOf(">="));
								   dateValue=dateValue.trim();  
								   dateValue = dateValue.substring(2); 
							    if (dateValue.indexOf("'") == 0) {
							    	dateValue = dateValue.substring(1);
								}
							    int j=dateValue.indexOf("'");
							    dateValue= dateValue.substring(0, j);
							    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
									dateValue = dateValue.substring(0, dateValue.length() - 1);
								}
								if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
									dateValue = dateValue.substring(0, dateValue.length() - 1);
								}
								whereElement=whereElement.replaceAll(dateValue, Date1); 
								
							   }
							   if(whereConditionElement.indexOf("<=")>0){ 
								   String dateValue=whereConditionElement;
								   dateValue=dateValue.trim();
								   dateValue= dateValue.substring(dateValue.indexOf("<="));
								   dateValue=dateValue.trim();  
								   dateValue = dateValue.substring(2); 
							    if (dateValue.indexOf("'") == 0) {
							    	dateValue = dateValue.substring(1);
								}
							    int j=dateValue.indexOf("'");
							    dateValue= dateValue.substring(0, j);
							    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
									dateValue = dateValue.substring(0, dateValue.length() - 1);
								}
								if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
									dateValue = dateValue.substring(0, dateValue.length() - 1);
								}
								whereElement=whereElement.replaceAll(dateValue, Date2);
								
							   }
							   whereConditionNew=whereConditionNew+"#"+whereElement; 
					  		}else{
					  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
					  		}
					  		} 
				 else{
					   whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	   
				   }
				 ifCondition="false";
		  	} else if((whereConditionElement.indexOf("(DATE_FORMAT(a.payPostDate,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("payablePostingDatePeriod")>=0 || datePeriodCondition.indexOf("PostingPeriodDatePeriod")>=0 )){
				   if(whereConditionElement.indexOf("or")<=0){
						 int arrayLength = datePeriodArray.length; 
					  		for(int i=0;i<arrayLength;i++)
					  		 { 
					  			periodDateValue =datePeriodArray[i]; 
					  		   if(!(periodDateValue.indexOf("payablePostingDatePeriod")<0)){
					  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
					  		     break;
					  		   }else{
					  			 periodDateValue=""; 
					  		   }
						          
					         }
					  		if(!periodDateValue.equals("")){
							String  returnValue=   findPeriodDate(periodDateValue);
				        	String [] returnValueArray=returnValue.split("#");
				            String	Date1 = new String(returnValueArray[0]);
				            String	Date2 = new String(returnValueArray[1]);   
							String whereElement=whereConditionElement;
				            if(whereConditionElement.indexOf(">=")>0){ 
								   String dateValue=whereConditionElement;
								   dateValue=dateValue.trim();
								   dateValue= dateValue.substring(dateValue.indexOf(">="));
								   dateValue=dateValue.trim();  
								   dateValue = dateValue.substring(2); 
							    if (dateValue.indexOf("'") == 0) {
							    	dateValue = dateValue.substring(1);
								}
							    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
									dateValue = dateValue.substring(0, dateValue.length() - 1);
								}
								if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
									dateValue = dateValue.substring(0, dateValue.length() - 1);
								}
								whereElement=whereElement.replaceAll(dateValue, Date1); 
								whereConditionNew=whereConditionNew+"#"+whereElement; 
							   }
							   if(whereConditionElement.indexOf("<=")>0){ 
								   String dateValue=whereConditionElement;
								   dateValue=dateValue.trim();
								   dateValue= dateValue.substring(dateValue.indexOf("<="));
								   dateValue=dateValue.trim();  
								   dateValue = dateValue.substring(2); 
							    if (dateValue.indexOf("'") == 0) {
							    	dateValue = dateValue.substring(1);
								}
							    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
									dateValue = dateValue.substring(0, dateValue.length() - 1);
								}
								if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
									dateValue = dateValue.substring(0, dateValue.length() - 1);
								}
								whereElement=whereElement.replaceAll(dateValue, Date2);
								whereConditionNew=whereConditionNew+"#"+whereElement; 
							   }
							   
					  		}else{
					  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
					  		}
					  		}else{
								   whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	   
							   }
				   ifCondition="false";   
		  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.receivedDate,'%Y-%m-%d')")>0)&& (datePeriodCondition.indexOf("receivedDatePeriod")>=0) ){
					int arrayLength = datePeriodArray.length; 
			  		for(int i=0;i<arrayLength;i++)
			  		 { 
			  			periodDateValue =datePeriodArray[i]; 
			  		   if(!(periodDateValue.indexOf("receivedDatePeriod")<0)){
			  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
			  		     break;
			  		   }else{
			  			 periodDateValue=""; 
			  		   }
				          
			         }
			  		if(!periodDateValue.equals("")){
					String  returnValue=   findPeriodDate(periodDateValue);
		        	String [] returnValueArray=returnValue.split("#");
		            String	Date1 = new String(returnValueArray[0]);
		            String	Date2 = new String(returnValueArray[1]);   
					String whereElement=whereConditionElement;
		            if(whereConditionElement.indexOf(">=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf(">="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date1);
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   }
					   if(whereConditionElement.indexOf("<=")>0){ 
						   String dateValue=whereConditionElement;
						   dateValue=dateValue.trim();
						   dateValue= dateValue.substring(dateValue.indexOf("<="));
						   dateValue=dateValue.trim();  
						   dateValue = dateValue.substring(2); 
					    if (dateValue.indexOf("'") == 0) {
					    	dateValue = dateValue.substring(1);
						}
					    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
							dateValue = dateValue.substring(0, dateValue.length() - 1);
						}
						whereElement=whereElement.replaceAll(dateValue, Date2); 
						whereConditionNew=whereConditionNew+"#"+whereElement; 
					   }
					   
			  		}
			  		else{
			  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
			  		}
			  		ifCondition="false";	
		  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')")>0)&& (datePeriodCondition.indexOf("InvoicingdatePeriod")>=0)){

				int arrayLength = datePeriodArray.length; 
		  		for(int i=0;i<arrayLength;i++)
		  		 { 
		  			periodDateValue =datePeriodArray[i]; 
		  		   if(!(periodDateValue.indexOf("InvoicingdatePeriod")<0)){
		  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
		  		     break;
		  		   }else{
		  			 periodDateValue=""; 
		  		   }
			          
		         }
		  		if(!periodDateValue.equals("")){
				String  returnValue=   findPeriodDate(periodDateValue);
	        	String [] returnValueArray=returnValue.split("#");
	            String	Date1 = new String(returnValueArray[0]);
	            String	Date2 = new String(returnValueArray[1]);   
				String whereElement=whereConditionElement;
	            if(whereConditionElement.indexOf(">=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf(">="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date1); 
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   if(whereConditionElement.indexOf("<=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf("<="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date2);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   } 
		  		}
		  		else{
		  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
		  		} 
		  		ifCondition="false";
		  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')")>0)&& (datePeriodCondition.indexOf("storageDatePeriod")>=0)){
                int arrayLength = datePeriodArray.length; 
		  		for(int i=0;i<arrayLength;i++)
		  		 { 
		  			periodDateValue =datePeriodArray[i]; 
		  		   if(!(periodDateValue.indexOf("storageDatePeriod")<0)){
		  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
		  		     break;
		  		   }else{
		  			 periodDateValue=""; 
		  		   }
			          
		         }
		  		if(!periodDateValue.equals("")){
				String  returnValue=   findPeriodDate(periodDateValue);
	        	String [] returnValueArray=returnValue.split("#");
	            String	Date1 = new String(returnValueArray[0]);
	            String	Date2 = new String(returnValueArray[1]);   
				String whereElement=whereConditionElement;
	            if(whereConditionElement.indexOf(">=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf(">="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date1);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   if(whereConditionElement.indexOf("<=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf("<="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date2);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   } 
		  		}
		  		else{
		  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
		  		} 
			
		  		ifCondition="false";
		  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')")>0 )&& (datePeriodCondition.indexOf("storageDatePeriod")>=0)){

                int arrayLength = datePeriodArray.length; 
		  		for(int i=0;i<arrayLength;i++)
		  		 { 
		  			periodDateValue =datePeriodArray[i]; 
		  		   if(!(periodDateValue.indexOf("storageDatePeriod")<0)){
		  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
		  		     break;
		  		   }else{
		  			 periodDateValue=""; 
		  		   }
			          
		         }
		  		if(!periodDateValue.equals("")){
				String  returnValue=   findPeriodDate(periodDateValue);
	        	String [] returnValueArray=returnValue.split("#");
	            String	Date1 = new String(returnValueArray[0]);
	            String	Date2 = new String(returnValueArray[1]);   
				String whereElement=whereConditionElement;
	            if(whereConditionElement.indexOf(">=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf(">="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date1);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   if(whereConditionElement.indexOf("<=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf("<="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date2); 
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   } 
		  		}
		  		else{
		  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
		  		}  
		  		ifCondition="false";
		  	}else if((whereConditionElement.indexOf("(DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')")>0)&& (datePeriodCondition.indexOf("revenueRecognitionPeriod")>=0)  ){


                int arrayLength = datePeriodArray.length; 
		  		for(int i=0;i<arrayLength;i++)
		  		 { 
		  			periodDateValue =datePeriodArray[i]; 
		  		   if(!(periodDateValue.indexOf("revenueRecognitionPeriod")<0)){
		  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
		  		     break;
		  		   }else{
		  			 periodDateValue=""; 
		  		   }
			          
		         }
		  		if(!periodDateValue.equals("")){
				String  returnValue=   findPeriodDate(periodDateValue);
	        	String [] returnValueArray=returnValue.split("#");
	            String	Date1 = new String(returnValueArray[0]);
	            String	Date2 = new String(returnValueArray[1]);   
				String whereElement=whereConditionElement;
	            if(whereConditionElement.indexOf(">=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf(">="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date1);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   if(whereConditionElement.indexOf("<=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf("<="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date2);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   } 
		  		}
		  		else{
		  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
		  		} 
		  		ifCondition="false";
			}  else if((whereConditionElement.indexOf("(DATE_FORMAT(c.bookingDate,'%Y-%m-%d')")>0)&&(datePeriodCondition.indexOf("bookingDatePeriod")>=0) ){
				int arrayLength = datePeriodArray.length; 
		  		for(int i=0;i<arrayLength;i++)
		  		 { 
		  			periodDateValue =datePeriodArray[i]; 
		  		   if(!(periodDateValue.indexOf("bookingDatePeriod")<0)){
		  			 periodDateValue=periodDateValue.substring(periodDateValue.indexOf("#")+1);
		  		     break;
		  		   }else{
		  			 periodDateValue=""; 
		  		   }			          
		         }
		  		if(!periodDateValue.equals("")){
				String  returnValue=   findPeriodDate(periodDateValue);
	        	String [] returnValueArray=returnValue.split("#");
	            String	Date1 = new String(returnValueArray[0]);
	            String	Date2 = new String(returnValueArray[1]);   
				String whereElement=whereConditionElement;
	            if(whereConditionElement.indexOf(">=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf(">="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date1);
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   }
				   if(whereConditionElement.indexOf("<=")>0){ 
					   String dateValue=whereConditionElement;
					   dateValue=dateValue.trim();
					   dateValue= dateValue.substring(dateValue.indexOf("<="));
					   dateValue=dateValue.trim();  
					   dateValue = dateValue.substring(2); 
				    if (dateValue.indexOf("'") == 0) {
				    	dateValue = dateValue.substring(1);
					}
				    if (dateValue.lastIndexOf(")") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					if (dateValue.lastIndexOf("'") == dateValue.length() - 1) {
						dateValue = dateValue.substring(0, dateValue.length() - 1);
					}
					whereElement=whereElement.replaceAll(dateValue, Date2); 
					whereConditionNew=whereConditionNew+"#"+whereElement; 
				   } 
		  		}
		  		else{
		  			whereConditionNew=whereConditionNew+"#"+whereConditionElement; 	
		  		}
		  		ifCondition="false";
			}
		  	else if(ifCondition.equals("true")){
			      whereConditionNew=whereConditionNew+"#"+whereConditionElement; 
			}
		}
		whereCondition=whereConditionNew;
		} 
		whereCondition=whereCondition.replaceAll("#", " ");
		//whereConditionNew=whereConditionNew.replaceAll("#", "  "); 
		String selectQuery=extractQueryFile.getSelectQuery();
		String extractType=extractQueryFile.getExtractType();
		String columnSelect="";
		if(selectQuery.equals("")||selectQuery.equals(","))
	  	  {
	  	  }
	  	 else
	  	  {
	  		selectQuery=selectQuery.trim();
	  	    if(selectQuery.indexOf("#")==0)
	  		 {
	  	    	selectQuery=selectQuery.substring(1);
	  		 }
	  		if(selectQuery.lastIndexOf("#")==selectQuery.length()-1)
	  		 {
	  			selectQuery=selectQuery.substring(0, selectQuery.length()-1);
	  		 }
	  		columnSelect=selectQuery;
	  		selectQuery=selectQuery.replaceAll("#",",");
	  	  }
		List dataExtractList=new ArrayList();
		File file =new File("");
		if(extractType.equalsIgnoreCase("shipmentAnalysis")){
			dataExtractList = serviceOrderManager.dynamicActiveShipments(selectQuery, whereCondition.toString(),sessionCorpID);
			file = new File("ActiveShipments");
		}
		if(extractType.equalsIgnoreCase("dynamicFinancialSummary")){
			dataExtractList = serviceOrderManager.dynamicFinancialSummary(selectQuery, whereCondition.toString(),sessionCorpID);
			file = new File("DynamicFinancialSummary");
		}
		if(extractType.equalsIgnoreCase("dynamicFinancialDetails")){
			dataExtractList = serviceOrderManager.dynamicDataFinanceDetails(selectQuery, whereCondition.toString(),sessionCorpID);
			file = new File("DynamicFinancialDetails");
		}
		if(extractType.equalsIgnoreCase("domesticAnalysis")){
			dataExtractList = serviceOrderManager.dynamicDataDomesticAnalysis(selectQuery, whereCondition.toString(),sessionCorpID);
			file = new File("DomesticAnalysis");
		}
		if(extractType.equalsIgnoreCase("storageBillingAnalysis")){
			dataExtractList = serviceOrderManager.dynamicStorageAnalysis(selectQuery, whereCondition.toString(),sessionCorpID);
			file = new File("StorageBillingAnalysis");
		}
		if(extractType.equalsIgnoreCase("RelocationAnalysis")){
			dataExtractList = serviceOrderManager.dynamicStorageAnalysisForRelo(selectQuery, whereCondition.toString(),sessionCorpID);
			file = new File("RelocationAnalysis");
		}
		HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream(); 
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls");
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		//response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" + "\t The number of line is" + dataExtractList.size() + "extracted");
		out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + dataExtractList.size() + "\t is extracted");
		String bA_SSCW = "";
		String[] conditionArray=columnSelect.split("#");
	    int arrayLength = conditionArray.length;
		for(int i=0;i<arrayLength;i++)
		 {	
		   String condition=conditionArray[i]; 
		   String []columnArray=condition.split(" as");
		   //condition= condition.substring(condition.indexOf("as")+2);
		   String column=columnArray[1];
		   outputStream.write((column+"\t").getBytes());
		 } 
		
		outputStream.write("\r\n".getBytes());
		Iterator it = dataExtractList.iterator();

		while(it.hasNext()){
			Object[] row = (Object[]) it.next();

			for(int i=0; i<arrayLength; i++){
				if (row[i] != null) {
					String data = row[i].toString();
					outputStream.write((data + "\t").getBytes());
				} else {
					outputStream.write("\t".getBytes());
				}
			}
			
			outputStream.write("\n".getBytes());
	    }
		if(extractFileAudit){
			outputStream.close();
			response.setContentType("application/octet-stream");
			//ServletOutputStream outputStream = response.getOutputStream();
			SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
			StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
			String currentyear=yearDate.toString();
			
			String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
			File dirPath = new File(uploadDir);
					if (!dirPath.exists()) {
				dirPath.mkdirs();
			}
			SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
			StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
			String currentDateTime=tempDate.toString().trim();
			String fileName="NoExtract_"+currentDateTime+".xls";
			if(extractType.equalsIgnoreCase("shipmentAnalysis")){
				fileName="ActiveShipments_"+currentDateTime+".xls";
			}
			if(extractType.equalsIgnoreCase("dynamicFinancialSummary")){
				fileName="DynamicFinancialSummary_"+currentDateTime+".xls";
			}
			if(extractType.equalsIgnoreCase("dynamicFinancialDetails")){
				fileName="DynamicFinancialDetails_"+currentDateTime+".xls"; 
			}
			if(extractType.equalsIgnoreCase("domesticAnalysis")){
				fileName="DomesticAnalysis_"+currentDateTime+".xls"; 
			}
			if(extractType.equalsIgnoreCase("storageBillingAnalysis")){
				fileName="StorageBillingAnalysis_"+currentDateTime+".xls"; 
			}
			if(extractType.equalsIgnoreCase("RelocationAnalysis")){
				fileName="RelocationAnalysis_"+currentDateTime+".xls";
				
			}
			String filePath=uploadDir+""+fileName;
			File file1 = new File(filePath); 
			FileOutputStream outputStream1 = new FileOutputStream(file1);
			response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + "");
			conditionArray=columnSelect.split("#");
		    arrayLength = conditionArray.length;
			for(int i=0;i<arrayLength;i++)
			 {	
			   String condition=conditionArray[i]; 
			   String []columnArray=condition.split(" as");
			   //condition= condition.substring(condition.indexOf("as")+2);
			   String column=columnArray[1];
			   outputStream1.write((column+"\t").getBytes());
			 } 
			//outputStream.write("URL".getBytes());
			outputStream1.write("\r\n".getBytes());
			Iterator its = dataExtractList.iterator();

			while(its.hasNext()){
				Object[] row = (Object[]) its.next();

				for(int i=0; i<arrayLength; i++){
					if (row[i] != null) {
						String data = row[i].toString();
						outputStream1.write((data + "\t").getBytes());
					} else {
						outputStream1.write("\t".getBytes());
					}
				}
				//outputStream.write(("https://www.skyrelo.com/redsky/editTrackingStatus.html?id="+row[arrayLength] + "\t").getBytes());
				outputStream1.write("\n".getBytes());
		    }
			extractedFileLog = new ExtractedFileLog();
			extractedFileLog.setCorpID(sessionCorpID);
			extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
			extractedFileLog.setCreatedOn(new Date());
			extractedFileLog.setLocation(filePath);
			extractedFileLog.setFileName(fileName);
			extractedFileLog.setModule("Data Extract");
			extractedFileLog=extractedFileLogManager.save(extractedFileLog);
			}
		
	}
	
	public String getExtractColumn(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		//activeStatus = user.getExtractJobType();
		agentParentForPortal=user.getParentAgent();
		salesPerson=user.getUsername().toUpperCase();
		armstrongPortal=user.getSalesPortalAccess();
		companyDivisionFlag = companyDivisionManager.findCompanyDivFlag(sessionCorpID).get(0).toString();
		accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString(); 
		companyCodeList = companyDivisionManager.findCompanyCodeList(sessionCorpID);
		getComboList(sessionCorpID);
		selectCondition="";
		columnList=extractColumnMgmtManager.getExtractColum(reportName,sessionCorpID);
		if(columnList!=null && (!columnList.isEmpty()))
		{
			Iterator it =columnList.iterator();
			int i=0;
			while (it.hasNext()) {
				Object obj=it.next(); 
				if(i==0)
				{
					columnList1.add(obj);
					i=1;
				}
				else if(i==1){
					columnList2.add(obj);
					i=2;
				}
				else if(i==2){
					columnList3.add(obj);
					i=3;
				}
				else if(i==3){
					columnList4.add(obj);
					i=4;
				}
				else {
					columnList5.add(obj);
					i=0;
				}
			}
		}
		multiplejobType= new ArrayList();
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				
				multiplejobType.add(arrayJobType[i].trim());
				
			}
		}
		//System.out.println("\n\n\n\n\n\n\n\n\n\n  jobTypes >>>>>>>>>>>>>>>in form    "+jobTypes);
		return SUCCESS;
	}
	
	public String getComboList(String corpId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		if(user.getSalesPortalAccess()){
			jobtype.put("INT", "International");
			jobtype.put("STO", "Storage");
		 }else{
		 jobtype = refMasterManager.findByParameter(corpId, "JOB");
		 }
		routing = refMasterManager.findByParameter(corpId, "ROUTING");
		ocountry = refMasterManager.findByParameter(corpId, "COUNTRY");
		dcountry = refMasterManager.findByParameter(corpId, "COUNTRY");
		mode = refMasterManager.findByParameters(corpId, "MODE");
		pkmode = refMasterManager.findByParameter(corpId, "PKMODE");
		commodit = refMasterManager.findByParameter(corpId, "COMMODIT");
		sale = refMasterManager.findUser(corpId, "ROLE_SALE");
		coord = refMasterManager.findUser(corpId, "ROLE_COORD");
		tcktactn = refMasterManager.findByParameter(corpId, "TCKTACTN");
		tcktservc = refMasterManager.findByParameter(corpId, "TCKTSERVC");
		house = refMasterManager.findByParameter(corpId, "HOUSE");
		//bookingCode = customerFileManager.findBookingCode();
		//billToCode = customerFileManager.findBillToCode();
		querySchedulerList = refMasterManager.findByParameter(corpId, "query_Scheduler");
		//perWeekSchedulerList = refMasterManager.findByParameter(corpId, "perWeek_Scheduler"); 
		perWeekSchedulerList.put("2", "Mon");
		perWeekSchedulerList.put("3", "Tue");
		perWeekSchedulerList.put("4", "Wed");
		perWeekSchedulerList.put("5", "Thu");
		perWeekSchedulerList.put("6", "Fri");
		perWeekSchedulerList.put("7", "Sat");
		perWeekSchedulerList.put("1", "Sun");
		Integer i = new Integer(1);
		for (i=1;i<=31;i++){
				perMonthSchedulerList.put(i, i);
		}
		//perMonthSchedulerList = refMasterManager.findByParameter(corpId, "perMonth_Scheduler");
		perQuarterSchedulerList= refMasterManager.findByParameter(corpId, "perQuarter_Scheduler");
		periodScheduler.put("LastYear", "Last Year");
		periodScheduler.put("CurrentYear", "Current Year");
		periodScheduler.put("LastHalfYear", "Last Half Year");
		periodScheduler.put("CurrentHalfYear", "Current Half Year");
		periodScheduler.put("LastQuarter", "Last Quarter");
		periodScheduler.put("CurrentQuarter", "Current Quarter");
		periodScheduler.put("LastMonth", "Last Month");
		periodScheduler.put("CurrentMonth", "Current Month");
		return SUCCESS;
	}
	public String findPeriodDate(String periodName){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd ");
		Date date = new Date();
		String currentDate=dateFormat.format(date); 
		String [] currentDateArray=currentDate.split("-");
		String fromDate="";
		String toDate="";
		String returnValue="";
		String halfyear1="";
		if(periodName.toString().equals("LastYear")){ 
			int lastYear=(Integer.parseInt(currentDateArray[0]))-1;
			fromDate=lastYear+"-0"+1+"-0"+1;
			toDate=lastYear+"-"+12+"-"+31;
			returnValue=fromDate+"#"+toDate; 
		} 
		if(periodName.toString().equals("CurrentYear")){ 
			int currentYear=Integer.parseInt(currentDateArray[0]);
			fromDate=currentYear+"-0"+1+"-0"+1;
			toDate=currentYear+"-"+12+"-"+31;
			returnValue=fromDate+"#"+toDate; 
		} 
		if(periodName.toString().equals("LastHalfYear")){
			int month=Integer.parseInt(currentDateArray[1]);
			int lastYear=(Integer.parseInt(currentDateArray[0]))-1;
			int currentYear=Integer.parseInt(currentDateArray[0]);
			if(month>6){
				fromDate=currentYear+"-0"+1+"-0"+1;	
				toDate=currentYear+"-0"+6+"-"+30;
				returnValue=fromDate+"#"+toDate; 
			}else if(month<6|| month==6){
				fromDate=lastYear+"-0"+7+"-0"+1;	
				toDate=lastYear+"-"+12+"-"+31;
				returnValue=fromDate+"#"+toDate; 
				
			}
		} 
		if(periodName.toString().equals("CurrentHalfYear")){
			int month=Integer.parseInt(currentDateArray[1]);
			int lastYear=(Integer.parseInt(currentDateArray[0]))-1;
			int currentYear=Integer.parseInt(currentDateArray[0]);
			if(month>6){
				fromDate=currentYear+"-0"+7+"-0"+1;	
				toDate=currentYear+"-"+12+"-"+31;
				returnValue=fromDate+"#"+toDate; 
			}else if(month<6|| month==6){
				fromDate=currentYear+"-0"+1+"-0"+1;	
				toDate=currentYear+"-0"+6+"-"+30;
				returnValue=fromDate+"#"+toDate; 
				
			}
		} 
		if(periodName.toString().equals("LastQuarter")){
			int month=Integer.parseInt(currentDateArray[1]);
			int lastYear=(Integer.parseInt(currentDateArray[0]))-1;
			int currentYear=Integer.parseInt(currentDateArray[0]);
			if(month<3 || month==3){
				fromDate=lastYear+"-"+10+"-0"+1;	
				toDate=lastYear+"-"+12+"-"+31;
				returnValue=fromDate+"#"+toDate; 
			}else if((month>3 && month<6)||month==6){
				fromDate=currentYear+"-0"+1+"-0"+1;	
				toDate=currentYear+"-0"+3+"-"+31;
				returnValue=fromDate+"#"+toDate; 
			} else if((month>6&& month<9)||month==9){
				fromDate=currentYear+"-0"+4+"-0"+1;	
				toDate=currentYear+"-0"+6+"-"+30;
				returnValue=fromDate+"#"+toDate; 
			}
			else if((month>9)){
				fromDate=currentYear+"-0"+7+"-0"+1;	
				toDate=currentYear+"-"+"0"+9+"-"+30;
				returnValue=fromDate+"#"+toDate; 
			}
		}
		if(periodName.toString().equals("CurrentQuarter")){
			int month=Integer.parseInt(currentDateArray[1]);
			int lastYear=(Integer.parseInt(currentDateArray[0]))-1;
			int currentYear=Integer.parseInt(currentDateArray[0]);
			if(month<3 || month==3){
				fromDate=currentYear+"-0"+1+"-0"+1;	
				toDate=currentYear+"-0"+3+"-"+31;
				returnValue=fromDate+"#"+toDate; 
			}else if((month>3 && month<6)||month==6){
				fromDate=currentYear+"-0"+4+"-0"+1;	
				toDate=currentYear+"-0"+6+"-"+30;
				returnValue=fromDate+"#"+toDate; 
			} else if((month>6&& month<9)||month==9){
				fromDate=currentYear+"-0"+7+"-0"+1;	
				toDate=currentYear+"-0"+9+"-"+30;
				returnValue=fromDate+"#"+toDate; 
			}
			else if((month>9)){
				fromDate=currentYear+"-"+10+"-0"+1;	
				toDate=currentYear+"-"+12+"-"+31;
				returnValue=fromDate+"#"+toDate; 
			}
		}
		if(periodName.toString().equals("LastMonth")){
			int currentmonth=(Integer.parseInt(currentDateArray[1])); 
			int lastmonth=(Integer.parseInt(currentDateArray[1]))-1; 
			int currentYear=Integer.parseInt(currentDateArray[0]);
			int lastYear=(Integer.parseInt(currentDateArray[0]))-1;
			if(currentmonth==1){
				lastmonth=12;	
				currentYear=lastYear;
			}
			String	Stringlastmonth="";
			if(lastmonth<10){
				Stringlastmonth="0"+lastmonth;
			} else {
				Stringlastmonth=""+lastmonth;
			}
			
			Calendar calendar = Calendar.getInstance(); 
			calendar.set(currentYear,lastmonth-1,1);
		    int lastDate = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
			fromDate=currentYear+"-"+Stringlastmonth+"-0"+1;	
			toDate=currentYear+"-"+Stringlastmonth+"-"+lastDate;
			returnValue=fromDate+"#"+toDate; 
		}
		if(periodName.toString().equals("CurrentMonth")){
			int currentmonth=(Integer.parseInt(currentDateArray[1])); 
			String	Stringlastmonth="";
			if(currentmonth<10){
				Stringlastmonth="0"+currentmonth;
			} else {
				Stringlastmonth=""+currentmonth;
			}
			int currentYear=Integer.parseInt(currentDateArray[0]);
			Calendar calendar = Calendar.getInstance(); 
			calendar.set(currentYear,currentmonth-1,1);
		    int lastDate = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
			fromDate=currentYear+"-"+Stringlastmonth+"-0"+1;	
			toDate=currentYear+"-"+Stringlastmonth+"-"+lastDate;
			returnValue=fromDate+"#"+toDate; 
		}
		
		return returnValue;	
	}
	
	public void dynamicStorageAnalysis() throws Exception{
		
		   company=companyManager.findByCorpID(sessionCorpID).get(0);
		    if(company!=null){
		      if(company.getExtractedFileAudit() !=null && company.getExtractedFileAudit()){
		    	  extractFileAudit = true;
		  		}
		    }
			StringBuffer jobBuffer = new StringBuffer();
			if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
			} else {
				if (jobTypes.indexOf(",") == 0) {
					jobTypes = jobTypes.substring(1);
				}
				if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
					jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
				}
				String[] arrayJobType = jobTypes.split(",");
				int arrayLength = arrayJobType.length;
				for (int i = 0; i < arrayLength; i++) {
					jobBuffer.append("'");
					jobBuffer.append(arrayJobType[i].trim());
					jobBuffer.append("'");
					jobBuffer.append(",");
				}
			}
			String jobtypeMultiple = new String(jobBuffer);
			if (!jobtypeMultiple.equals("")) {
				jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
			} else if (jobtypeMultiple.equals("")) {
				jobtypeMultiple = new String("''");
			}

			SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
			StringBuffer beginDates = new StringBuffer();
			StringBuffer endDates = new StringBuffer();
			StringBuffer deliveryTBeginDates = new StringBuffer();
			StringBuffer deliveryTEndDates = new StringBuffer();
			StringBuffer deliveryActBeginDates = new StringBuffer();
			StringBuffer deliveryActEndDates = new StringBuffer();
			StringBuffer loadTgtBeginDates = new StringBuffer();
			StringBuffer loadTgtEndDates = new StringBuffer();
			StringBuffer invPostingBeginDates = new StringBuffer();
			StringBuffer invPostingEndDates = new StringBuffer();
			StringBuffer invoicingBeginDates = new StringBuffer();
			StringBuffer invoicingEndDates = new StringBuffer();
			StringBuffer storageBeginDates = new StringBuffer();
			StringBuffer storageEndDates = new StringBuffer();
			StringBuffer createdDates = new StringBuffer();
			StringBuffer toCreatedDates = new StringBuffer();
			StringBuffer payPostingBeginDates = new StringBuffer();
			StringBuffer payPostingEndDates = new StringBuffer();
			StringBuffer revenueRecognitionBeginDates = new StringBuffer();
			StringBuffer revenueRecognitionEndDates = new StringBuffer();
			StringBuffer receivedBeginDates = new StringBuffer();
			StringBuffer receivedEndDates = new StringBuffer();
			StringBuffer postingPeriodBeginDates = new StringBuffer();
			StringBuffer postingPeriodEndDates = new StringBuffer();
			StringBuffer bookingDates = new StringBuffer();
			StringBuffer toBookingDates = new StringBuffer();
			StringBuffer updateDates = new StringBuffer();
			StringBuffer toUpdateDates = new StringBuffer();
			StringBuffer query = new StringBuffer();
			boolean isAnd = false;
			if(!revenueRecognitionPeriod.equals("")){
	        	String  returnValue=   findPeriodDate(revenueRecognitionPeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	revenueRecognitionBeginDates = new StringBuffer(returnValueArray[0]);
	        	revenueRecognitionEndDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate != null) {
				revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
				revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
			} else if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate == null) {
				revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
				revenueRecognitionEndDates = new StringBuffer();
			} else if (revenueRecognitionBeginDate == null && revenueRecognitionEndDate != null) {
				revenueRecognitionBeginDates = new StringBuffer();
				revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
			} else {
				revenueRecognitionBeginDates = new StringBuffer();
				revenueRecognitionEndDates = new StringBuffer();
			}
	        }
			if(!loadActualDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(loadActualDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	beginDates = new StringBuffer(returnValueArray[0]);
	        	endDates = new StringBuffer(returnValueArray[1]);
	        }else{
			if (beginDate != null && endDate != null) {
				beginDates = new StringBuffer(formats.format(beginDate));
				endDates = new StringBuffer(formats.format(endDate));

			} else if (beginDate != null && endDate == null) {
				beginDates = new StringBuffer(formats.format(beginDate));
				endDates = new StringBuffer();
			} else if (beginDate == null && endDate != null) {
				beginDates = new StringBuffer();
				endDates = new StringBuffer(formats.format(endDate));
			} else {
				beginDates = new StringBuffer();
				endDates = new StringBuffer();

			}
	        }
			if(!createDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(createDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	createdDates = new StringBuffer(returnValueArray[0]);
	        	toCreatedDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (createDate != null && toCreateDate != null) {
				createdDates = new StringBuffer(formats.format(createDate));
				toCreatedDates = new StringBuffer(formats.format(toCreateDate));
			} else if (createDate != null && toCreateDate == null) {
				createdDates = new StringBuffer(formats.format(createDate));
				toCreatedDates = new StringBuffer();
			} else if (createDate == null && toCreateDate != null) {
				createdDates = new StringBuffer();
				toCreatedDates = new StringBuffer(formats.format(toCreateDate));
			} else {
				createdDates = new StringBuffer();
				toCreatedDates = new StringBuffer();
			}
	        }
			if(!updateDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(updateDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	updateDates  = new StringBuffer(returnValueArray[0]);
	        	toUpdateDates  = new StringBuffer(returnValueArray[1]);
	        }else{
	        	if (updateDate != null && toUpdateDate != null) {
					updateDates = new StringBuffer(formats.format(updateDate));
					toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
				} else if (updateDate != null && toUpdateDate == null) {
					updateDates = new StringBuffer(formats.format(updateDate));
					toUpdateDates = new StringBuffer();
				} else if (updateDate == null && toUpdateDate != null) {
					updateDates = new StringBuffer();
					toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
				} else {
					updateDates = new StringBuffer();
					toUpdateDates = new StringBuffer();
				}
	        }
			if(!deliveryTargetDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(deliveryTargetDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	deliveryTBeginDates = new StringBuffer(returnValueArray[0]);
	        	deliveryTEndDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (deliveryTBeginDate != null && deliveryTEndDate != null) {
				deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
				deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
			} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
				deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
				deliveryTEndDates = new StringBuffer();
			} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
				deliveryTBeginDates = new StringBuffer();
				deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
			} else {
				deliveryTBeginDates = new StringBuffer();
				deliveryTEndDates = new StringBuffer();
			}
	        }
			if(!deliveryActualDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(deliveryActualDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	deliveryActBeginDates = new StringBuffer(returnValueArray[0]);
	        	deliveryActEndDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (deliveryActBeginDate != null && deliveryActEndDate != null) {
				deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
				deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
			} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
				deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
				deliveryActEndDates = new StringBuffer();
			} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
				deliveryActBeginDates = new StringBuffer();
				deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
			} else {
				deliveryActBeginDates = new StringBuffer();
				deliveryActEndDates = new StringBuffer();
			}
	        }
	        if(!loadTargetDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(loadTargetDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	loadTgtBeginDates = new StringBuffer(returnValueArray[0]);
	        	loadTgtEndDates = new StringBuffer(returnValueArray[1]);
	        }else{
			if (loadTgtBeginDate != null && loadTgtEndDate != null) {
				loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
				loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
			} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
				loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
				loadTgtEndDates = new StringBuffer();
			} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
				loadTgtBeginDates = new StringBuffer();
				loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
			} else {
				loadTgtBeginDates = new StringBuffer();
				loadTgtEndDates = new StringBuffer();
			}
	        }
	        if(!invoicePostingDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(invoicePostingDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	invPostingBeginDates = new StringBuffer(returnValueArray[0]);
	        	invPostingEndDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (invPostingBeginDate != null && invPostingEndDate != null) {
				invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
				invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
			} else if (invPostingBeginDate != null && invPostingEndDate == null) {
				invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
				invPostingEndDates = new StringBuffer();
			} else if (invPostingBeginDate == null && invPostingEndDate != null) {
				invPostingBeginDates = new StringBuffer();
				invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
			} else {
				invPostingBeginDates = new StringBuffer();
				invPostingEndDates = new StringBuffer();
			}
	        }
	        if(!InvoicingdatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(InvoicingdatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	invoicingBeginDates = new StringBuffer(returnValueArray[0]);
	        	invoicingEndDates = new StringBuffer(returnValueArray[1]);
	        }else{  
			if (invoicingBeginDate != null && invoicingEndDate != null) {
				invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
				invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
			} else if (invoicingBeginDate != null && invoicingEndDate == null) {
				invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
				invoicingEndDates = new StringBuffer();
			} else if (invoicingBeginDate == null && invoicingEndDate != null) {
				invoicingBeginDates = new StringBuffer();
				invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
			} else {
				invoicingBeginDates = new StringBuffer();
				invoicingEndDates = new StringBuffer();
			}
	        }
	        if(!storageDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(storageDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	storageEndDates = new StringBuffer(returnValueArray[0]);
	        	storageEndDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (storageBeginDate != null && storageEndDate != null) {
				storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
				storageEndDates = new StringBuffer(formats.format(storageEndDate));
			} else if (storageBeginDate != null && storageEndDate == null) {
				storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
				storageEndDates = new StringBuffer();
			} else if (storageBeginDate == null && storageEndDate != null) {
				storageBeginDates = new StringBuffer();
				storageEndDates = new StringBuffer(formats.format(storageEndDate));
			} else {
				storageBeginDates = new StringBuffer();
				storageEndDates = new StringBuffer();
			}
	        }
			if(!payablePostingDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(payablePostingDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	payPostingBeginDates = new StringBuffer(returnValueArray[0]);
	        	payPostingEndDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (payPostingBeginDate != null && payPostingEndDate != null) {
				payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
				payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
			} else if (payPostingBeginDate != null && payPostingEndDate == null) {
				payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
				payPostingEndDates = new StringBuffer();
			} else if (payPostingBeginDate == null && payPostingEndDate != null) {
				payPostingBeginDates = new StringBuffer();
				payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
			} else {
				payPostingBeginDates = new StringBuffer();
				payPostingEndDates = new StringBuffer();
			}
	        }
			if(!receivedDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(receivedDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	receivedBeginDates = new StringBuffer(returnValueArray[0]);
	        	receivedEndDates = new StringBuffer(returnValueArray[1]);
	        }else{  
			if (receivedBeginDate != null && receivedEndDate != null) {
				receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
				receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
			} else if (receivedBeginDate != null && receivedEndDate == null) {
				receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
				receivedEndDates = new StringBuffer();
			} else if (receivedBeginDate == null && receivedEndDate != null) {
				receivedBeginDates = new StringBuffer();
				receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
			} else {
				receivedBeginDates = new StringBuffer();
				receivedEndDates = new StringBuffer();
			}
	        }
			if(!PostingPeriodDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(PostingPeriodDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	postingPeriodBeginDates = new StringBuffer(returnValueArray[0]);
	        	postingPeriodEndDates = new StringBuffer(returnValueArray[1]);
	        }else{  
			if (postingPeriodBeginDate != null && postingPeriodEndDate != null) {
				 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
				 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
			 } else if (postingPeriodBeginDate != null && postingPeriodEndDate == null) {
				 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
				 postingPeriodEndDates = new StringBuffer();
			 } else if (postingPeriodBeginDate == null && postingPeriodEndDate != null) {
				 postingPeriodBeginDates = new StringBuffer();
				 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
			 } else {
				 postingPeriodBeginDates = new StringBuffer();
				 postingPeriodEndDates = new StringBuffer();
			 } 
	        }
			if(!bookingDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(bookingDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	bookingDates = new StringBuffer(returnValueArray[0]);
	        	toBookingDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (bookingDate != null && toBookingDate != null) {
				bookingDates = new StringBuffer(formats.format(bookingDate));
				toBookingDates = new StringBuffer(formats.format(toBookingDate));
			} else if (bookingDate != null && toBookingDate == null) {
				bookingDates = new StringBuffer(formats.format(bookingDate));
				toBookingDates = new StringBuffer();
			} else if (bookingDate == null && toBookingDate != null) {
				bookingDates = new StringBuffer();
				toBookingDates = new StringBuffer(formats.format(toBookingDate));
			} else {
				bookingDates = new StringBuffer();
				toBookingDates = new StringBuffer();
			}
	        }
			if (jobTypesCondition.equals("") && billToCodeCondition.equals("")&& originAgentCondition.equals("")&& destinationAgentCondition.equals("") && bookingCodeCondition.equals("") && routingCondition.equals("") && modeCondition.equals("") && packModeCondition.equals("")
					&& commodityCondition.equals("") && coordinatorCondition.equals("") && consultantCondition.equals("") && salesmanCondition.equals("") && oACountryCondition.equals("") && dACountryCondition.equals("")
					&& actualWgtCondition.equals("") && companyCodeCondition.equals("") && beginDates.toString().equals("") && endDates.toString().equals("") && actualVolumeCondition.equals("")
					&& createdDates.toString().equals("") && toCreatedDates.toString().equals("") && updateDates.toString().equals("") && toUpdateDates.toString().equals("") &&  deliveryTBeginDates.toString().equals("") && deliveryTEndDates.toString().equals("") && deliveryActBeginDates.toString().equals("")
					&& deliveryActEndDates.toString().equals("") && loadTgtBeginDates.toString().equals("") && loadTgtEndDates.toString().equals("") && invPostingBeginDates.toString().equals("") && invPostingEndDates.toString().equals("")
					&& invoicingBeginDates.toString().equals("") && invoicingEndDates.toString().equals("") && storageBeginDates.toString().equals("") && storageEndDates.toString().equals("") && revenueRecognitionBeginDates.toString().equals("") && revenueRecognitionEndDates.toString().equals("")
					&& payPostingBeginDates.toString().equals("") && payPostingEndDates.toString().equals("")&& receivedBeginDates.toString().equals("") && receivedEndDates.toString().equals("") && postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals("")
					&& bookingDates.toString().equals("") && toBookingDates.toString().equals("") && carrierCondition.equals("") && driverIdCondition.equals("") && accountCodeCondition.equals("")) {

				if (activeStatus.equalsIgnoreCase("Active")) {
					query.append("  where  s.corpId='" + sessionCorpID + "'  and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '' )");
				} else if (activeStatus.equalsIgnoreCase("Inactive")) {
					query.append("  where  s.corpId='" + sessionCorpID + "'  and  s.status in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '' )");
				} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
					query.append("  where  s.corpId='" + sessionCorpID + "'  and  s.status not in ('CNCL') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '' )");
				} else if (activeStatus.equalsIgnoreCase("All")) {
					query.append("  where  s.corpId='" + sessionCorpID + "' and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '' )");
				}

			} else {
				query.append("  where  ");
				if (jobTypesCondition.equals("") || jobTypesCondition == null) {

				} else {
					query.append(" s.job  ");
					isAnd = true;
					if (jobTypesCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append(" in  ");
						query.append("(" + jobtypeMultiple + ")");
					} else {
						query.append(" not in  ");
						query.append("(" + jobtypeMultiple + ")");
					}
				}
				// create date
				if ( createdDates.toString().equals("")) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
					}
					if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + createdDates + "')");
					} else if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + createdDates + "')");
					} else {
					}
					isAnd = true;
				}

				if ( toCreatedDates.toString().equals("")) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
					}
					if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + toCreatedDates + "')");
					} else if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + toCreatedDates + "')");
					} else {
					}
					isAnd = true;
				}
				//create date end
				
				if ( updateDates.toString().equals("")) {

				} else {
					if (isAnd == true) {
						query.append("   and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
					}
					if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + updateDates + "')");
					} else if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + updateDates + "')");
					} else {

					}
					isAnd = true;
				}
				if ( toUpdateDates.toString().equals("")) {

				} else {
					if (isAnd == true) {
						query.append("   and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
					}
					if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + toUpdateDates + "')");
					} else if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + toUpdateDates + "')");
					} else {

					}
					isAnd = true;
				}
				
				if (billToCodeCondition.equals("") || billToCodeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and s.billToCode");
					} else {
						query.append("  s.billToCode");
					}
					if (billToCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + billToCodeType.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + billToCodeType.trim() + "'");
					}
					isAnd = true;
				}
				if (originAgentCondition.equals("") || originAgentCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and t.originAgentCode");
					} else {
						query.append("  t.originAgentCode");
					}
					if (originAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + originAgentCodeType.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + originAgentCodeType.trim() + "'");
					}
					isAnd = true;

				}
				if (destinationAgentCondition.equals("") |destinationAgentCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and t.destinationAgentCode");
					} else {
						query.append("  t.destinationAgentCode");
					}
					if (destinationAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + destinationAgentCodeType.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + destinationAgentCodeType.trim() + "'");
					}
					isAnd = true;

				}

				if (bookingCodeCondition.equals("") || bookingCodeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and s.bookingAgentCode");
					} else {
						query.append("  s.bookingAgentCode");
					}
					if (bookingCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + bookingCodeType.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + bookingCodeType.trim() + "'");
					}
					isAnd = true;

				}

				if (routingCondition.equals("") || routingCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and s.routing");
					} else {
						query.append("   s.routing");
					}
					if (routingCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + routingTypes + "'");
					} else {
						query.append("<>");
						query.append("'" + routingTypes + "'");
					}
					isAnd = true;
				}

				if (modeCondition.equals("") || modeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and s.mode");
					} else {
						query.append("  s.mode");
					}
					if (modeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + modeType + "'");
					} else {
						query.append("<>");
						query.append("'" + modeType + "'");
					}
					isAnd = true;
				}
				if (packModeCondition.equals("") || packModeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and s.packingMode");
					} else {
						query.append("   s.packingMode");
					}
					if (packModeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + packModeType + "'");
					} else {
						query.append("<>");
						query.append("'" + packModeType + "'");
					}
					isAnd = true;
				}
				if (commodityCondition.equals("") || commodityCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and s.commodity");
					} else {
						query.append("  s.commodity");
					}
					if (commodityCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + commodityType + "'");
					} else {
						query.append("<>");
						query.append("'" + commodityType + "'");
					}
					isAnd = true;
				}
				if (coordinatorCondition.equals("") || coordinatorCondition == null) {

				} else {
					if (isAnd == true) {
						query.append("  and s.coordinator");
					} else {
						query.append("   s.coordinator");
					}
					if (coordinatorCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + coordinatorType + "'");
					} else {
						query.append("<>");
						query.append("'" + coordinatorType + "'");
					}
					isAnd = true;
				} 
				if (consultantCondition.equals("") || consultantCondition == null) {

				} else {
					if (isAnd == true) {
						query.append("  and s.estimator");
					} else {
						query.append("   s.estimator");
					}
					if (consultantCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + consultantType + "'");
					} else {
						query.append("<>");
						query.append("'" + consultantType + "'");
					}
					isAnd = true;
				} 
				if (salesmanCondition.equals("") || salesmanCondition == null) {

				} else {
					if (isAnd == true) {
						query.append("  and s.salesMan");
					} else {
						query.append("   s.salesMan");
					}
					if (salesmanCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + salesmanType + "'");
					} else {
						query.append("<>");
						query.append("'" + salesmanType + "'");
					}
					isAnd = true;
				}
				if (oACountryCondition.equals("") || oACountryCondition == null) {

				} else {
					if (isAnd == true) {
						query.append("  and s.originCountryCode");
					} else {
						query.append("   s.originCountryCode");
					}
					if (oACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + oACountryType + "'");
					} else {
						query.append("<>");
						query.append("'" + oACountryType + "'");
					}
					isAnd = true;
				}
				if (dACountryCondition.equals("") || dACountryCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and s.destinationCountryCode");
					} else {
						query.append("  s.destinationCountryCode");
					}
					if (dACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + dACountryType + "'");
					} else {
						query.append("<>");
						query.append("'" + dACountryType + "'");
					}
					isAnd = true;
				} 
				if (actualWgtCondition.equals("") || actualWgtCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and m.actualNetWeight");
					} else {
						query.append("  m.actualNetWeight");
					}

					if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + actualWgtType + "'");
					} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
						query.append("<>");
						query.append("'" + actualWgtType + "'");
					} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
						query.append(">");
						query.append("'" + actualWgtType + "'");
					} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + actualWgtType + "'");
					} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
						query.append("<");
						query.append("'" + actualWgtType + "'");
					} else {
						query.append("<=");
						query.append("'" + actualWgtType + "'");
					}
					isAnd = true;
				}
				if (actualVolumeCondition.equals("") || actualVolumeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and m.actualCubicFeet");
					} else {
						query.append("  m.actualCubicFeet");
					}
					if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + actualVolumeType + "'");
					} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
						query.append("<>");
						query.append("'" + actualVolumeType + "'");
					} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
						query.append(">");
						query.append("'" + actualVolumeType + "'");
					} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + actualVolumeType + "'");
					} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
						query.append("<");
						query.append("'" + actualVolumeType + "'");
					} else {
						query.append("<=");
						query.append("'" + actualVolumeType + "'");
					}
					isAnd = true;
				}
				if (beginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
					}
					if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + beginDates + "')");
					} else if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + beginDates + "')");
					} else {

					}
					isAnd = true;
				}
				if (endDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
					}
					if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + endDates + "')");
					} else if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + endDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (deliveryTBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
					}
					if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + deliveryTBeginDates + "')");
					} else if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + deliveryTBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (deliveryTEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
					}
					if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + deliveryTEndDates + "')");
					} else if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + deliveryTEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (deliveryActBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
					}
					if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + deliveryActBeginDates + "')");
					} else if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + deliveryActBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (deliveryActEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
					}
					if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + deliveryActEndDates + "')");
					} else if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + deliveryActEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}

				if (loadTgtBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
					}
					if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + loadTgtBeginDates + "')");
					} else if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + loadTgtBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (loadTgtEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
					}
					if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + loadTgtEndDates + "')");
					} else if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + loadTgtEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (invPostingBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
					}
					if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + invPostingBeginDates + "')");
					} else if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + invPostingBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}

				if (invPostingEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
					}

					if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + invPostingEndDates + "')");
					} else if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + invPostingEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if ( payPostingBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					}
					if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + payPostingBeginDates + "')");
					} else if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + payPostingBeginDates + "')");
					} else {
						
					}
					isAnd = true;
				}
				if (payPostingEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					}
					if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + payPostingEndDates + "')");
					} else if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + payPostingEndDates + "')");
					} else {
						
					}
					isAnd = true;
				}
				if ( receivedBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
					}
					if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + receivedBeginDates + "')");
					} else if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + receivedBeginDates + "')");
					} else {
						
					}
					isAnd = true;
				}
				if ( receivedEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
					}
					if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + receivedEndDates + "')");
					} else if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + receivedEndDates + "')");
					} else {
						
					}
					isAnd = true;
				}
				if ( (postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals(""))) {

				} else 
				{
					if(((!( postingPeriodBeginDates.toString().equals(""))))&& (!(postingPeriodEndDates.toString().equals("")))) {
					if (isAnd == true) {
						query.append("  and ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
					} else {
						query.append("  ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
					}
					if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else {
						
					}
					query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");

	                  
					  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "')");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "')");
					} else {
						
					}
	                   query.append("  or ( (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					   if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else {
						
					}

	                 query.append("  and (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "') ) ) )");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "') ) ) ) ");
					} else {
						
					}

					isAnd = true;
				} 
					else if((!( postingPeriodBeginDates.toString().equals("")))&& ((postingPeriodEndDates.toString().equals("")))){
					if (isAnd == true) {
						query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
					} else {
						query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
					}
					if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else {
						
					}
					 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					 if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
							query.append("<=");
							query.append("'" + postingPeriodBeginDates + "') )");
						} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
							query.append(">=");
							query.append("'" + postingPeriodBeginDates + "') )");
						} else {
							
						}
					 isAnd = true;
				}
				else  if((!( postingPeriodEndDates.toString().equals("")))&& ((postingPeriodBeginDates.toString().equals("")))){
					if (isAnd == true) {
						query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
					} else {
						query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
					}
					if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "')");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "')");
					} else {
						
					}
					 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					 if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
							query.append("<=");
							query.append("'" + postingPeriodEndDates + "') )");
						} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
							query.append(">=");
							query.append("'" + postingPeriodEndDates + "') )");
						} else {
							
						}
					 isAnd = true;
				}
				}
				
				if (invoicingBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
					}

					if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + invoicingBeginDates + "')");
					} else if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + invoicingBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}

				if (invoicingEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
					}

					if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + invoicingEndDates + "')");
					} else if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + invoicingEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				
				if (storageBeginDates.toString().equals("") || storageBeginDateCondition == null) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
					}

					if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + storageBeginDates + "')");
					} else if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + storageBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				
				
				if (storageEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
					}

					if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + storageEndDates + "')");
					} else if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + storageEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				
				if (revenueRecognitionBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
					}

					if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + revenueRecognitionBeginDates + "')");
					} else if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + revenueRecognitionBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}

				if (revenueRecognitionEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
					}

					if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + revenueRecognitionEndDates + "')");
					} else if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + revenueRecognitionEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}

				if (companyCodeCondition == null || companyCodeCondition.equals("")) {

				} else {
					if (isAnd == true) {
						query.append(" and s.companyDivision");
					} else {
						query.append("  s.companyDivision");
					}

					if (companyCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + companyCode + "'");
					} else {
						query.append("<>");
						query.append("'" + companyCode + "'");
					}
					isAnd = true;
				}
				if ( bookingDates.toString().equals("")) {
				} else {
					if (isAnd == true) {
						query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
					} else {
						query.append(" (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
					}
					if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + bookingDates + "')");
					} else if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + bookingDates + "')");
					} else {
					}
					isAnd = true;
				}

				if ( toBookingDates.toString().equals("")) {
				} else {
					if (isAnd == true) {
						query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
					} else {
						query.append(" (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
					}
					if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + toBookingDates + "')");
					} else if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + toBookingDates + "')");
					} else {
					}
					isAnd = true;
				}
				if (carrierCondition.equals("") || carrierCondition == null) {
				} else {
					if (isAnd == true) {
						query.append(" and m.carrier");
					} else {
						query.append("  m.carrier");
					}
					if (carrierCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + carrier.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + carrier.trim() + "'");
					}
					isAnd = true;
				}
				if (driverIdCondition.equals("") || driverIdCondition == null) {
				} else {
					if (isAnd == true) {
						query.append(" and m.driverId");
					} else {
						query.append("  m.driverId");
					}
					if (driverIdCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + driverId.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + driverId.trim() + "'");
					}
					isAnd = true;
				}
				
				if (accountCodeCondition.equals("") || accountCodeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append("  and c.accountCode");
					} else {
						query.append("   c.accountCode");
					}
					if (accountCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + accountCodeType + "'");
					} else {
						query.append("<>");
						query.append("'" + accountCodeType + "'");
					}
					isAnd = true;
				}

				if (activeStatus.equalsIgnoreCase("Active")) {
					query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '')");
				} else if (activeStatus.equalsIgnoreCase("Inactive")) {
					query.append("   and s.corpId='" + sessionCorpID + "' and  s.status  in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '')");
				} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
					query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CNCL') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '')");
				} else if (activeStatus.equalsIgnoreCase("All")) {
					query.append("  and s.corpId='" + sessionCorpID + "' and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '')");
				}
			 }
			if(selectCondition.equals("")||selectCondition.equals("#"))
		  	  {
		  	  }
		  	 else
		  	  {
		  		selectCondition=selectCondition.trim();
		  	    if(selectCondition.indexOf("#")==0)
		  		 {
		  	    	selectCondition=selectCondition.substring(1);
		  		 }
		  		if(selectCondition.lastIndexOf("#")==selectCondition.length()-1)
		  		 {
		  			selectCondition=selectCondition.substring(0, selectCondition.length()-1);
		  		 }
				
		  	  }
			selectCondition=selectCondition.replaceAll("#", ",");
			List selectConditionList=new ArrayList();
			selectConditionList= extractColumnMgmtManager.getSelectCondition(selectCondition,reportName,sessionCorpID);
		    String SaveSelectCondition=new String();
		    Iterator iterator =selectConditionList.iterator();
		    while(iterator.hasNext())
		    {
		    	SaveSelectCondition =SaveSelectCondition.concat((String)iterator.next().toString());
		    	SaveSelectCondition=SaveSelectCondition.concat("#");
		    }
		    SaveSelectCondition=SaveSelectCondition.trim();
			    if(SaveSelectCondition.indexOf("#")==0)
				 {
			    	SaveSelectCondition=SaveSelectCondition.substring(1);
				 }
				if(SaveSelectCondition.lastIndexOf("#")==SaveSelectCondition.length()-1)
				 {
					SaveSelectCondition=SaveSelectCondition.substring(0, SaveSelectCondition.length()-1);
				 }
				String columnSelect=SaveSelectCondition;
				SaveSelectCondition=SaveSelectCondition.replaceAll("#", ",");
			//System.out.println("\n\n\n\n\n\n\n\n query" + query);
			//System.out.println("\n\n\n\n\n\n\n\n selct query    " +SaveSelectCondition+" query    " + query    ); 
			List activeShipments = serviceOrderManager.dynamicStorageAnalysis(SaveSelectCondition, query.toString(),sessionCorpID);
			
			HttpServletResponse response = getResponse();
			response.setContentType("application/vnd.ms-excel");
			ServletOutputStream outputStream = response.getOutputStream();
			File file = new File("StorageBillingAnalysis");
			response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls");
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			//response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" + "\t The number of line is" + activeShipments.size() + "extracted");
			out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + activeShipments.size() + "\t is extracted");
			String bA_SSCW = "";
			String[] conditionArray=columnSelect.split("#");
		    int arrayLength = conditionArray.length;
			for(int i=0;i<arrayLength;i++)
			 {	
			   String condition=conditionArray[i]; 
			   String []columnArray=condition.split(" as");
			   //condition= condition.substring(condition.indexOf("as")+2);
			   String column=columnArray[1];
			   outputStream.write((column+"\t").getBytes());
			 } 
			//outputStream.write("URL".getBytes());
			outputStream.write("\r\n".getBytes());
			Iterator it = activeShipments.iterator();

			while(it.hasNext()){
				Object[] row = (Object[]) it.next();

				for(int i=0; i<arrayLength; i++){
					if (row[i] != null) {
						String data = row[i].toString();
						outputStream.write((data + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
				}
				//outputStream.write(("https://www.skyrelo.com/redsky/editTrackingStatus.html?id="+row[arrayLength] + "\t").getBytes());
				outputStream.write("\n".getBytes());
		    }
			if(extractFileAudit){
			outputStream.close();
			response.setContentType("application/octet-stream");
			//ServletOutputStream outputStream = response.getOutputStream();
			SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
			StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
			String currentyear=yearDate.toString();
			
			String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
			File dirPath = new File(uploadDir);
					if (!dirPath.exists()) {
				dirPath.mkdirs();
			}
			SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
			StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
			String currentDateTime=tempDate.toString().trim();
			String fileName="StorageBillingAnalysis_"+currentDateTime+".xls";
			String filePath=uploadDir+""+fileName;
			File file1 = new File(filePath); 
			FileOutputStream outputStream1 = new FileOutputStream(file1);
			response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + "");
			conditionArray=columnSelect.split("#");
		    arrayLength = conditionArray.length;
			for(int i=0;i<arrayLength;i++)
			 {	
			   String condition=conditionArray[i]; 
			   String []columnArray=condition.split(" as");
			   //condition= condition.substring(condition.indexOf("as")+2);
			   String column=columnArray[1];
			   outputStream1.write((column+"\t").getBytes());
			 } 
			//outputStream.write("URL".getBytes());
			outputStream1.write("\r\n".getBytes());
			Iterator its = activeShipments.iterator();

			while(its.hasNext()){
				Object[] row = (Object[]) its.next();

				for(int i=0; i<arrayLength; i++){
					if (row[i] != null) {
						String data = row[i].toString();
						outputStream1.write((data + "\t").getBytes());
					} else {
						outputStream1.write("\t".getBytes());
					}
				}
				//outputStream.write(("https://www.skyrelo.com/redsky/editTrackingStatus.html?id="+row[arrayLength] + "\t").getBytes());
				outputStream1.write("\n".getBytes());
		    }
			extractedFileLog = new ExtractedFileLog();
			extractedFileLog.setCorpID(sessionCorpID);
			extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
			extractedFileLog.setCreatedOn(new Date());
			extractedFileLog.setLocation(filePath);
			extractedFileLog.setFileName(fileName);
			extractedFileLog.setModule("Data Extract");
			extractedFileLog=extractedFileLogManager.save(extractedFileLog);
			}
	}
	
	public void dynamicDomesticAnalysis() throws Exception{ 
		 company=companyManager.findByCorpID(sessionCorpID).get(0);
		    if(company!=null){
		      if(company.getExtractedFileAudit() !=null && company.getExtractedFileAudit()){
		    	  extractFileAudit = true;
		  		}
		    }	
		StringBuffer jobBuffer = new StringBuffer();
			if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
			} else {
				if (jobTypes.indexOf(",") == 0) {
					jobTypes = jobTypes.substring(1);
				}
				if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
					jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
				}
				String[] arrayJobType = jobTypes.split(",");
				int arrayLength = arrayJobType.length;
				for (int i = 0; i < arrayLength; i++) {
					jobBuffer.append("'");
					jobBuffer.append(arrayJobType[i].trim());
					jobBuffer.append("'");
					jobBuffer.append(",");
				}
			}
			String jobtypeMultiple = new String(jobBuffer);
			if (!jobtypeMultiple.equals("")) {
				jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
			} else if (jobtypeMultiple.equals("")) {
				jobtypeMultiple = new String("''");
			}
			SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
			StringBuffer beginDates = new StringBuffer();
			StringBuffer endDates = new StringBuffer();
			StringBuffer deliveryTBeginDates = new StringBuffer();
			StringBuffer deliveryTEndDates = new StringBuffer();
			StringBuffer deliveryActBeginDates = new StringBuffer();
			StringBuffer deliveryActEndDates = new StringBuffer();
			StringBuffer loadTgtBeginDates = new StringBuffer();
			StringBuffer loadTgtEndDates = new StringBuffer();
			StringBuffer invPostingBeginDates = new StringBuffer();
			StringBuffer invPostingEndDates = new StringBuffer();
			StringBuffer invoicingBeginDates = new StringBuffer();
			StringBuffer invoicingEndDates = new StringBuffer();
			StringBuffer storageBeginDates = new StringBuffer();
			StringBuffer storageEndDates = new StringBuffer();
			StringBuffer createdDates = new StringBuffer();
			StringBuffer toCreatedDates = new StringBuffer();
			StringBuffer payPostingBeginDates = new StringBuffer();
			StringBuffer payPostingEndDates = new StringBuffer();
			StringBuffer receivedBeginDates = new StringBuffer();
			StringBuffer receivedEndDates = new StringBuffer();
			StringBuffer postingPeriodBeginDates = new StringBuffer();
			StringBuffer postingPeriodEndDates = new StringBuffer();
			StringBuffer bookingDates = new StringBuffer();
			StringBuffer toBookingDates = new StringBuffer();
			StringBuffer updateDates = new StringBuffer();
			StringBuffer toUpdateDates = new StringBuffer();
			StringBuffer query = new StringBuffer();
			
			StringBuffer revenueRecognitionBeginDates = new StringBuffer();
			StringBuffer revenueRecognitionEndDates = new StringBuffer();
			
			boolean isAnd = false;
			
			if(!revenueRecognitionPeriod.equals("")){
	        	String  returnValue=   findPeriodDate(revenueRecognitionPeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	revenueRecognitionBeginDates = new StringBuffer(returnValueArray[0]);
	        	revenueRecognitionEndDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate != null) {
				revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
				revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
			} else if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate == null) {
				revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
				revenueRecognitionEndDates = new StringBuffer();
			} else if (revenueRecognitionBeginDate == null && revenueRecognitionEndDate != null) {
				revenueRecognitionBeginDates = new StringBuffer();
				revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
			} else {
				revenueRecognitionBeginDates = new StringBuffer();
				revenueRecognitionEndDates = new StringBuffer();
			}
	        }
			if(!loadActualDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(loadActualDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	beginDates = new StringBuffer(returnValueArray[0]);
	        	endDates = new StringBuffer(returnValueArray[1]);
	        }else{
			if (beginDate != null && endDate != null) {
				beginDates = new StringBuffer(formats.format(beginDate));
				endDates = new StringBuffer(formats.format(endDate));

			} else if (beginDate != null && endDate == null) {
				beginDates = new StringBuffer(formats.format(beginDate));
				endDates = new StringBuffer();
			} else if (beginDate == null && endDate != null) {
				beginDates = new StringBuffer();
				endDates = new StringBuffer(formats.format(endDate));
			} else {
				beginDates = new StringBuffer();
				endDates = new StringBuffer();

			}
	        }
			if(!createDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(createDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	createdDates = new StringBuffer(returnValueArray[0]);
	        	toCreatedDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (createDate != null && toCreateDate != null) {
				createdDates = new StringBuffer(formats.format(createDate));
				toCreatedDates = new StringBuffer(formats.format(toCreateDate));
			} else if (createDate != null && toCreateDate == null) {
				createdDates = new StringBuffer(formats.format(createDate));
				toCreatedDates = new StringBuffer();
			} else if (createDate == null && toCreateDate != null) {
				createdDates = new StringBuffer();
				toCreatedDates = new StringBuffer(formats.format(toCreateDate));
			} else {
				createdDates = new StringBuffer();
				toCreatedDates = new StringBuffer();
			}
	        }
			if(!updateDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(updateDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	updateDates  = new StringBuffer(returnValueArray[0]);
	        	toUpdateDates  = new StringBuffer(returnValueArray[1]);
	        }else{
	        	if (updateDate != null && toUpdateDate != null) {
					updateDates = new StringBuffer(formats.format(updateDate));
					toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
				} else if (updateDate != null && toUpdateDate == null) {
					updateDates = new StringBuffer(formats.format(updateDate));
					toUpdateDates = new StringBuffer();
				} else if (updateDate == null && toUpdateDate != null) {
					updateDates = new StringBuffer();
					toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
				} else {
					updateDates = new StringBuffer();
					toUpdateDates = new StringBuffer();
				}
	        }
			if(!deliveryTargetDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(deliveryTargetDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	deliveryTBeginDates = new StringBuffer(returnValueArray[0]);
	        	deliveryTEndDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (deliveryTBeginDate != null && deliveryTEndDate != null) {
				deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
				deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
			} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
				deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
				deliveryTEndDates = new StringBuffer();
			} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
				deliveryTBeginDates = new StringBuffer();
				deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
			} else {
				deliveryTBeginDates = new StringBuffer();
				deliveryTEndDates = new StringBuffer();
			}
	        }
			if(!deliveryActualDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(deliveryActualDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	deliveryActBeginDates = new StringBuffer(returnValueArray[0]);
	        	deliveryActEndDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (deliveryActBeginDate != null && deliveryActEndDate != null) {
				deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
				deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
			} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
				deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
				deliveryActEndDates = new StringBuffer();
			} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
				deliveryActBeginDates = new StringBuffer();
				deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
			} else {
				deliveryActBeginDates = new StringBuffer();
				deliveryActEndDates = new StringBuffer();
			}
	        }
	        if(!loadTargetDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(loadTargetDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	loadTgtBeginDates = new StringBuffer(returnValueArray[0]);
	        	loadTgtEndDates = new StringBuffer(returnValueArray[1]);
	        }else{
			if (loadTgtBeginDate != null && loadTgtEndDate != null) {
				loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
				loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
			} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
				loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
				loadTgtEndDates = new StringBuffer();
			} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
				loadTgtBeginDates = new StringBuffer();
				loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
			} else {
				loadTgtBeginDates = new StringBuffer();
				loadTgtEndDates = new StringBuffer();
			}
	        }
	        if(!invoicePostingDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(invoicePostingDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	invPostingBeginDates = new StringBuffer(returnValueArray[0]);
	        	invPostingEndDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (invPostingBeginDate != null && invPostingEndDate != null) {
				invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
				invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
			} else if (invPostingBeginDate != null && invPostingEndDate == null) {
				invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
				invPostingEndDates = new StringBuffer();
			} else if (invPostingBeginDate == null && invPostingEndDate != null) {
				invPostingBeginDates = new StringBuffer();
				invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
			} else {
				invPostingBeginDates = new StringBuffer();
				invPostingEndDates = new StringBuffer();
			}
	        }
	        if(!InvoicingdatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(InvoicingdatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	invoicingBeginDates = new StringBuffer(returnValueArray[0]);
	        	invoicingEndDates = new StringBuffer(returnValueArray[1]);
	        }else{  
			if (invoicingBeginDate != null && invoicingEndDate != null) {
				invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
				invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
			} else if (invoicingBeginDate != null && invoicingEndDate == null) {
				invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
				invoicingEndDates = new StringBuffer();
			} else if (invoicingBeginDate == null && invoicingEndDate != null) {
				invoicingBeginDates = new StringBuffer();
				invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
			} else {
				invoicingBeginDates = new StringBuffer();
				invoicingEndDates = new StringBuffer();
			}
	        }
	        if(!storageDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(storageDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	storageEndDates = new StringBuffer(returnValueArray[0]);
	        	storageEndDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (storageBeginDate != null && storageEndDate != null) {
				storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
				storageEndDates = new StringBuffer(formats.format(storageEndDate));
			} else if (storageBeginDate != null && storageEndDate == null) {
				storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
				storageEndDates = new StringBuffer();
			} else if (storageBeginDate == null && storageEndDate != null) {
				storageBeginDates = new StringBuffer();
				storageEndDates = new StringBuffer(formats.format(storageEndDate));
			} else {
				storageBeginDates = new StringBuffer();
				storageEndDates = new StringBuffer();
			}
	        }
			if(!payablePostingDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(payablePostingDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	payPostingBeginDates = new StringBuffer(returnValueArray[0]);
	        	payPostingEndDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (payPostingBeginDate != null && payPostingEndDate != null) {
				payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
				payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
			} else if (payPostingBeginDate != null && payPostingEndDate == null) {
				payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
				payPostingEndDates = new StringBuffer();
			} else if (payPostingBeginDate == null && payPostingEndDate != null) {
				payPostingBeginDates = new StringBuffer();
				payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
			} else {
				payPostingBeginDates = new StringBuffer();
				payPostingEndDates = new StringBuffer();
			}
	        }
			if(!receivedDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(receivedDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	receivedBeginDates = new StringBuffer(returnValueArray[0]);
	        	receivedEndDates = new StringBuffer(returnValueArray[1]);
	        }else{  
			if (receivedBeginDate != null && receivedEndDate != null) {
				receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
				receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
			} else if (receivedBeginDate != null && receivedEndDate == null) {
				receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
				receivedEndDates = new StringBuffer();
			} else if (receivedBeginDate == null && receivedEndDate != null) {
				receivedBeginDates = new StringBuffer();
				receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
			} else {
				receivedBeginDates = new StringBuffer();
				receivedEndDates = new StringBuffer();
			}
	        }
			if(!PostingPeriodDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(PostingPeriodDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	postingPeriodBeginDates = new StringBuffer(returnValueArray[0]);
	        	postingPeriodEndDates = new StringBuffer(returnValueArray[1]);
	        }else{  
			if (postingPeriodBeginDate != null && postingPeriodEndDate != null) {
				 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
				 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
			 } else if (postingPeriodBeginDate != null && postingPeriodEndDate == null) {
				 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
				 postingPeriodEndDates = new StringBuffer();
			 } else if (postingPeriodBeginDate == null && postingPeriodEndDate != null) {
				 postingPeriodBeginDates = new StringBuffer();
				 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
			 } else {
				 postingPeriodBeginDates = new StringBuffer();
				 postingPeriodEndDates = new StringBuffer();
			 } 
	        }
			if(!bookingDatePeriod.equals("")){
	        	String  returnValue=   findPeriodDate(bookingDatePeriod);
	        	String [] returnValueArray=returnValue.split("#");
	        	bookingDates = new StringBuffer(returnValueArray[0]);
	        	toBookingDates = new StringBuffer(returnValueArray[1]);
	        }else{ 
			if (bookingDate != null && toBookingDate != null) {
				bookingDates = new StringBuffer(formats.format(bookingDate));
				toBookingDates = new StringBuffer(formats.format(toBookingDate));
			} else if (bookingDate != null && toBookingDate == null) {
				bookingDates = new StringBuffer(formats.format(bookingDate));
				toBookingDates = new StringBuffer();
			} else if (bookingDate == null && toBookingDate != null) {
				bookingDates = new StringBuffer();
				toBookingDates = new StringBuffer(formats.format(toBookingDate));
			} else {
				bookingDates = new StringBuffer();
				toBookingDates = new StringBuffer();
			}
	        }
			if (jobTypesCondition.equals("") && billToCodeCondition.equals("")&& originAgentCondition.equals("")&& destinationAgentCondition.equals("") && bookingCodeCondition.equals("") && routingCondition.equals("") && modeCondition.equals("") && packModeCondition.equals("")
					&& commodityCondition.equals("") && coordinatorCondition.equals("") && consultantCondition.equals("") && salesmanCondition.equals("") && oACountryCondition.equals("") && dACountryCondition.equals("")
					&& actualWgtCondition.equals("") && companyCodeCondition.equals("") && beginDates.toString().equals("") && endDates.toString().equals("") && actualVolumeCondition.equals("")
					&& createdDates.toString().equals("") && toCreatedDates.toString().equals("") && updateDates.toString().equals("") && toUpdateDates.toString().equals("") && deliveryTBeginDates.toString().equals("") && deliveryTEndDates.toString().equals("") && deliveryActBeginDates.toString().equals("")
					&& deliveryActEndDates.toString().equals("") && loadTgtBeginDates.toString().equals("") && loadTgtEndDates.toString().equals("") && invPostingBeginDates.toString().equals("") && invPostingEndDates.toString().equals("")
					&& invoicingBeginDates.toString().equals("") && invoicingEndDates.toString().equals("")&& storageBeginDates.toString().equals("") && storageEndDates.toString().equals("") && revenueRecognitionBeginDates.toString().equals("") && revenueRecognitionEndDates.toString().equals("")&&payPostingBeginDates.toString().equals("") && payPostingEndDates.toString().equals("")&& receivedBeginDates.toString().equals("") && receivedEndDates.toString().equals("") && postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals("")
					&& bookingDates.toString().equals("") && toBookingDates.toString().equals("") && carrierCondition.equals("") && driverIdCondition.equals("") && accountCodeCondition.equals("")) {

				if (activeStatus.equalsIgnoreCase("Active")) {
					query.append("  where  s.corpId='" + sessionCorpID + "'  and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '' ) group by s.shipNumber");
				} else if (activeStatus.equalsIgnoreCase("Inactive")) {
					query.append("  where  s.corpId='" + sessionCorpID + "'  and  s.status in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '' ) group by s.shipNumber");
				} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
					query.append("  where  s.corpId='" + sessionCorpID + "'  and  s.status not in ('CNCL') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '')  group by s.shipNumber");
				} else if (activeStatus.equalsIgnoreCase("All")) {
					query.append("  where  s.corpId='" + sessionCorpID + "' and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
				}
				//query.append(" where   s.corpId='"+sessionCorpID+"'  and  s.status not in ('CLSD','CNCL') group by s.shipNumber");
			} else {

				query.append("  where  ");

				if (jobTypesCondition.equals("") || jobTypesCondition == null) {

				} else {
					query.append(" s.job  ");
					isAnd = true;
					if (jobTypesCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append(" in  ");
						query.append("(" + jobtypeMultiple + ")");
					} else {
						query.append(" not in  ");
						query.append("(" + jobtypeMultiple + ")");
					}

				}
				// create date
				if (createdDates.toString().equals("")) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
					}
					if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + createdDates + "')");
					} else if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + createdDates + "')");
					} else {

					}
					isAnd = true;
				}
				if ( toCreatedDates.toString().equals("")) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
					}
					if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + toCreatedDates + "')");
					} else if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + toCreatedDates + "')");
					} else {

					}
					isAnd = true;
				}
				//create date end
				if ( updateDates.toString().equals("")) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
					}
					if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + updateDates + "')");
					} else if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + updateDates + "')");
					} else {

					}
					isAnd = true;
				}
				if ( toUpdateDates.toString().equals("")) {

				} else {
					if (isAnd == true) {
						query.append("   and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
					}
					if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + toUpdateDates + "')");
					} else if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + toUpdateDates + "')");
					} else {

					}
					isAnd = true;
				}

				if (billToCodeCondition.equals("") || billToCodeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and s.billToCode");
					} else {
						query.append("  s.billToCode");
					}
					if (billToCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + billToCodeType.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + billToCodeType.trim() + "'");
					}
					isAnd = true;

				}
				if (originAgentCondition.equals("") || originAgentCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and t.originAgentCode");
					} else {
						query.append("  t.originAgentCode");
					}
					if (originAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + originAgentCodeType.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + originAgentCodeType.trim() + "'");
					}
					isAnd = true;

				}
				if (destinationAgentCondition.equals("") |destinationAgentCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and t.destinationAgentCode");
					} else {
						query.append("  t.destinationAgentCode");
					}
					if (destinationAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + destinationAgentCodeType.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + destinationAgentCodeType.trim() + "'");
					}
					isAnd = true;

				}
				
				if (bookingCodeCondition.equals("") || bookingCodeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and s.bookingAgentCode");
					} else {
						query.append("  s.bookingAgentCode");
					}
					if (bookingCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + bookingCodeType.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + bookingCodeType.trim() + "'");
					}
					isAnd = true;

				}
				if (routingCondition.equals("") || routingCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and s.routing");
					} else {
						query.append("   s.routing");
					}
					if (routingCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + routingTypes + "'");
					} else {
						query.append("<>");
						query.append("'" + routingTypes + "'");
					}
					isAnd = true;
				}
				if (modeCondition.equals("") || modeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and s.mode");
					} else {
						query.append("  s.mode");
					}
					if (modeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + modeType + "'");
					} else {
						query.append("<>");
						query.append("'" + modeType + "'");
					}
					isAnd = true;
				}
				if (packModeCondition.equals("") || packModeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and s.packingMode");
					} else {
						query.append("   s.packingMode");
					}
					if (packModeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + packModeType + "'");
					} else {
						query.append("<>");
						query.append("'" + packModeType + "'");
					}
					isAnd = true;
				}
				if (commodityCondition.equals("") || commodityCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and s.commodity");
					} else {
						query.append("  s.commodity");
					}
					if (commodityCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + commodityType + "'");
					} else {
						query.append("<>");
						query.append("'" + commodityType + "'");
					}
					isAnd = true;
				}
				if (coordinatorCondition.equals("") || coordinatorCondition == null) {

				} else {
					if (isAnd == true) {
						query.append("  and s.coordinator");
					} else {
						query.append("   s.coordinator");
					}
					if (coordinatorCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + coordinatorType + "'");
					} else {
						query.append("<>");
						query.append("'" + coordinatorType + "'");
					}
					isAnd = true;
				} 
				if (consultantCondition.equals("") || consultantCondition == null) {

				} else {
					if (isAnd == true) {
						query.append("  and s.estimator");
					} else {
						query.append("   s.estimator");
					}
					if (consultantCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + consultantType + "'");
					} else {
						query.append("<>");
						query.append("'" + consultantType + "'");
					}
					isAnd = true;
				} 
				if (salesmanCondition.equals("") || salesmanCondition == null) {

				} else {
					if (isAnd == true) {
						query.append("  and s.salesMan");
					} else {
						query.append("   s.salesMan");
					}
					if (salesmanCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + salesmanType + "'");
					} else {
						query.append("<>");
						query.append("'" + salesmanType + "'");
					}
					isAnd = true;
				}
				if (oACountryCondition.equals("") || oACountryCondition == null) {

				} else {
					if (isAnd == true) {
						query.append("  and s.originCountryCode");
					} else {
						query.append("   s.originCountryCode");
					}
					if (oACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + oACountryType + "'");
					} else {
						query.append("<>");
						query.append("'" + oACountryType + "'");
					}
					isAnd = true;
				}
				if (dACountryCondition.equals("") || dACountryCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and s.destinationCountryCode");
					} else {
						query.append("  s.destinationCountryCode");
					}
					if (dACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + dACountryType + "'");
					} else {
						query.append("<>");
						query.append("'" + dACountryType + "'");
					}
					isAnd = true;
				}
				
				if (actualWgtCondition.equals("") || actualWgtCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and m.actualNetWeight");
					} else {
						query.append("  m.actualNetWeight");
					}

					if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + actualWgtType + "'");
					} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
						query.append("<>");
						query.append("'" + actualWgtType + "'");
					} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
						query.append(">");
						query.append("'" + actualWgtType + "'");
					} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + actualWgtType + "'");
					} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
						query.append("<");
						query.append("'" + actualWgtType + "'");
					} else {
						query.append("<=");
						query.append("'" + actualWgtType + "'");
					}
					isAnd = true;
				}
				if (actualVolumeCondition.equals("") || actualVolumeCondition == null) {

				} else {
					if (isAnd == true) {
						query.append(" and m.actualCubicFeet");
					} else {
						query.append("  m.actualCubicFeet");
					}
					if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + actualVolumeType + "'");
					} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
						query.append("<>");
						query.append("'" + actualVolumeType + "'");
					} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
						query.append(">");
						query.append("'" + actualVolumeType + "'");
					} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + actualVolumeType + "'");
					} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
						query.append("<");
						query.append("'" + actualVolumeType + "'");
					} else {
						query.append("<=");
						query.append("'" + actualVolumeType + "'");
					}
					isAnd = true;
				}
				if (beginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
					}
					if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + beginDates + "')");
					} else if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + beginDates + "')");
					} else {

					}
					isAnd = true;
				}
				if (endDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
					}
					if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + endDates + "')");
					} else if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + endDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (deliveryTBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
					}
					if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + deliveryTBeginDates + "')");
					} else if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + deliveryTBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (deliveryTEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
					}
					if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + deliveryTEndDates + "')");
					} else if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + deliveryTEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (deliveryActBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
					}
					if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + deliveryActBeginDates + "')");
					} else if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + deliveryActBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (deliveryActEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
					}
					if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + deliveryActEndDates + "')");
					} else if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + deliveryActEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}

				if (loadTgtBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
					}
					if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + loadTgtBeginDates + "')");
					} else if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + loadTgtBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (loadTgtEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
					}
					if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + loadTgtEndDates + "')");
					} else if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + loadTgtEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (invPostingBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
					}
					if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + invPostingBeginDates + "')");
					} else if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + invPostingBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if (invPostingEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
					}
					if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + invPostingEndDates + "')");
					} else if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + invPostingEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				if ( payPostingBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					}
					if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + payPostingBeginDates + "')");
					} else if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + payPostingBeginDates + "')");
					} else {
						
					}
					isAnd = true;
				}
				if (payPostingEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					}
					if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + payPostingEndDates + "')");
					} else if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + payPostingEndDates + "')");
					} else {
						
					}
					isAnd = true;
				}
				if ( receivedBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
					}
					if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + receivedBeginDates + "')");
					} else if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + receivedBeginDates + "')");
					} else {
						
					}
					isAnd = true;
				}
				if (receivedEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
					}
					if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + receivedEndDates + "')");
					} else if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + receivedEndDates + "')");
					} else {
						
					}
					isAnd = true;
				}
				if ((postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals(""))) {

				} else 
				{
					if(((!( postingPeriodBeginDates.toString().equals(""))))&& (!(postingPeriodEndDates.toString().equals("")))) {
					if (isAnd == true) {
						query.append("  and ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
					} else {
						query.append("  ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
					}
					if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else {
						
					}
					query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");

	                  
					  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "')");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "')");
					} else {
						
					}
	                   query.append("  or ( (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					   if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else {
						
					}

	                 query.append("  and (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "') ) ) )");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "') ) ) ) ");
					} else {
						
					}

					isAnd = true;
				} 
					else if((!( postingPeriodBeginDates.toString().equals("")))&& ((postingPeriodEndDates.toString().equals("")))){
					if (isAnd == true) {
						query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
					} else {
						query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
					}
					if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "')");
					} else {
						
					}
					 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					 if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
							query.append("<=");
							query.append("'" + postingPeriodBeginDates + "') )");
						} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
							query.append(">=");
							query.append("'" + postingPeriodBeginDates + "') )");
						} else {
							
						}
					 isAnd = true;
				}
				else  if((!( postingPeriodEndDates.toString().equals("")))&& ((postingPeriodBeginDates.toString().equals("")))){
					if (isAnd == true) {
						query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
					} else {
						query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
					}
					if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "')");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "')");
					} else {
						
					}
					 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
					 if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
							query.append("<=");
							query.append("'" + postingPeriodEndDates + "') )");
						} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
							query.append(">=");
							query.append("'" + postingPeriodEndDates + "') )");
						} else {
							
						}
					 isAnd = true;
				}
				}
				if (invoicingBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
					}

					if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + invoicingBeginDates + "')");
					} else if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + invoicingBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}

				if (invoicingEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
					}

					if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + invoicingEndDates + "')");
					} else if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + invoicingEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				
				if (storageBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
					}

					if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + storageBeginDates + "')");
					} else if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + storageBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}

				if (storageEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
					}

					if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + storageEndDates + "')");
					} else if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + storageEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}
				
				if (revenueRecognitionBeginDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
					}

					if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + revenueRecognitionBeginDates + "')");
					} else if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + revenueRecognitionBeginDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}

				if (revenueRecognitionEndDates.toString().equals("") ) {

				} else {
					if (isAnd == true) {
						query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
					} else {
						query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
					}

					if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + revenueRecognitionEndDates + "')");
					} else if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + revenueRecognitionEndDates + "')");
					} else {
						//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
					}
					isAnd = true;
				}

				if (companyCodeCondition == null || companyCodeCondition.equals("")) {

				} else {
					if (isAnd == true) {
						query.append(" and s.companyDivision");
					} else {
						query.append("  s.companyDivision");
					}
					if (companyCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + companyCode + "'");
					} else {
						query.append("<>");
						query.append("'" + companyCode + "'");
					}
					isAnd = true;
				}
				if (bookingDates.toString().equals("")) {
				} else {
					if (isAnd == true) {
						query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
					} else {
						query.append("(DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
					}
					if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + bookingDates + "')");
					} else if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + bookingDates + "')");
					} else {

					}
					isAnd = true;
				}
				if ( toBookingDates.toString().equals("")) {
				} else {
					if (isAnd == true) {
						query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
					} else {
						query.append("(DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
					}
					if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + toBookingDates + "')");
					} else if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + toBookingDates + "')");
					} else {

					}
					isAnd = true;
				}
				if (carrierCondition.equals("") || carrierCondition == null) {
				} else {
					if (isAnd == true) {
						query.append(" and m.carrier");
					} else {
						query.append("  m.carrier");
					}
					if (carrierCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + carrier.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + carrier.trim() + "'");
					}
					isAnd = true;
				}
				if (driverIdCondition.equals("") || driverIdCondition == null) {
				} else {
					if (isAnd == true) {
						query.append(" and m.driverId");
					} else {
						query.append("  m.driverId");
					}
					if (driverIdCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + driverId.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + driverId.trim() + "'");
					}
					isAnd = true;
				}
				
				if (accountCodeCondition.equals("") || accountCodeCondition == null) {
				} else {
					if (isAnd == true) {
						query.append(" and c.accountCode");
					} else {
						query.append("  c.accountCode");
					}
					if (accountCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
						query.append("=");
						query.append("'" + accountCodeType.trim() + "'");
					} else {
						query.append("<>");
						query.append("'" + accountCodeType.trim() + "'");
					}
					isAnd = true;
				}
				
				if (activeStatus.equalsIgnoreCase("Active")) {
					query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
				} else if (activeStatus.equalsIgnoreCase("Inactive")) {
					query.append("   and s.corpId='" + sessionCorpID + "' and  s.status  in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
				} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
					query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CNCL') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
				} else if (activeStatus.equalsIgnoreCase("All")) {
					query.append("  and s.corpId='" + sessionCorpID + "' and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
				}
				//query.append("   and s.corpId='"+sessionCorpID+"'  and  s.status not in ('CLSD','CNCL')   group by s.shipNumber");
			}
			if(selectCondition.equals("")||selectCondition.equals("#"))
		  	  {
		  	  }
		  	 else
		  	  {
		  		selectCondition=selectCondition.trim();
		  	    if(selectCondition.indexOf("#")==0)
		  		 {
		  	    	selectCondition=selectCondition.substring(1);
		  		 }
		  		if(selectCondition.lastIndexOf("#")==selectCondition.length()-1)
		  		 {
		  			selectCondition=selectCondition.substring(0, selectCondition.length()-1);
		  		 }
				
		  	  }
			selectCondition=selectCondition.replaceAll("#", ",");
			List selectConditionList=new ArrayList();
			selectConditionList= extractColumnMgmtManager.getSelectCondition(selectCondition,reportName,sessionCorpID);
		    String SaveSelectCondition=new String();
		    Iterator iterator =selectConditionList.iterator();
		    while(iterator.hasNext())
		    {
		    	SaveSelectCondition =SaveSelectCondition.concat((String)iterator.next().toString());
		    	SaveSelectCondition=SaveSelectCondition.concat("#");
		    }
		    SaveSelectCondition=SaveSelectCondition.trim();
			    if(SaveSelectCondition.indexOf("#")==0)
				 {
			    	SaveSelectCondition=SaveSelectCondition.substring(1);
				 }
				if(SaveSelectCondition.lastIndexOf("#")==SaveSelectCondition.length()-1)
				 {
					SaveSelectCondition=SaveSelectCondition.substring(0, SaveSelectCondition.length()-1);
				 }
				String columnSelect=SaveSelectCondition;
				SaveSelectCondition=SaveSelectCondition.replaceAll("#", ",");
			//System.out.println("\n\n\n\n\n\n\n\n query" + query);
			//System.out.println("\n\n\n\n\n\n\n\n selct query    " +SaveSelectCondition+" query    " + query    ); 
			List activeShipments = serviceOrderManager.dynamicDataDomesticAnalysis(SaveSelectCondition, query.toString(),sessionCorpID);
			
			HttpServletResponse response = getResponse();
			response.setContentType("application/vnd.ms-excel");
			ServletOutputStream outputStream = response.getOutputStream();
			File file = new File("DomesticAnalysis");
			response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls");
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			//response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" + "\t The number of line is" + activeShipments.size() + "extracted");
			out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + activeShipments.size() + "\t is extracted");
			String bA_SSCW = "";
			String[] conditionArray=columnSelect.split("#");
		    int arrayLength = conditionArray.length;
			for(int i=0;i<arrayLength;i++)
			 {	
			   String condition=conditionArray[i]; 
			   String []columnArray=condition.split(" as");
			   //condition= condition.substring(condition.indexOf("as")+2);
			   String column=columnArray[1];
			   outputStream.write((column+"\t").getBytes());
			 } 
			//outputStream.write("URL".getBytes());
			outputStream.write("\r\n".getBytes());
			Iterator it = activeShipments.iterator();

			while(it.hasNext()){
				Object[] row = (Object[]) it.next();

				for(int i=0; i<arrayLength; i++){
					if (row[i] != null) {
						String data = row[i].toString();
						outputStream.write((data + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
				}
				//outputStream.write(("https://www.skyrelo.com/redsky/editTrackingStatus.html?id="+row[arrayLength] + "\t").getBytes());
				outputStream.write("\n".getBytes());
		    }
			if(extractFileAudit){
				outputStream.close();
				response.setContentType("application/octet-stream");
				//ServletOutputStream outputStream = response.getOutputStream();
				SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
				StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
				String currentyear=yearDate.toString();
				
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
				File dirPath = new File(uploadDir);
						if (!dirPath.exists()) {
					dirPath.mkdirs();
				}
				SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
				StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
				String currentDateTime=tempDate.toString().trim();
				String fileName="DomesticAnalysis_"+currentDateTime+".xls";
				String filePath=uploadDir+""+fileName;
				File file1 = new File(filePath); 
				FileOutputStream outputStream1 = new FileOutputStream(file1);
				response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + "");
				conditionArray=columnSelect.split("#");
			    arrayLength = conditionArray.length;
				for(int i=0;i<arrayLength;i++)
				 {	
				   String condition=conditionArray[i]; 
				   String []columnArray=condition.split(" as");
				   //condition= condition.substring(condition.indexOf("as")+2);
				   String column=columnArray[1];
				   outputStream1.write((column+"\t").getBytes());
				 } 
				//outputStream.write("URL".getBytes());
				outputStream1.write("\r\n".getBytes());
				Iterator its = activeShipments.iterator();

				while(its.hasNext()){
					Object[] row = (Object[]) its.next();

					for(int i=0; i<arrayLength; i++){
						if (row[i] != null) {
							String data = row[i].toString();
							outputStream1.write((data + "\t").getBytes());
						} else {
							outputStream1.write("\t".getBytes());
						}
					}
					//outputStream.write(("https://www.skyrelo.com/redsky/editTrackingStatus.html?id="+row[arrayLength] + "\t").getBytes());
					outputStream1.write("\n".getBytes());
			    }
				extractedFileLog = new ExtractedFileLog();
				extractedFileLog.setCorpID(sessionCorpID);
				extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
				extractedFileLog.setCreatedOn(new Date());
				extractedFileLog.setLocation(filePath);
				extractedFileLog.setFileName(fileName);
				extractedFileLog.setModule("Data Extract");
				extractedFileLog=extractedFileLogManager.save(extractedFileLog);
				}
	}
	
	public void dynamicFinancialDetails() throws Exception{

		 company=companyManager.findByCorpID(sessionCorpID).get(0);
		    if(company!=null){
		      if(company.getExtractedFileAudit() !=null && company.getExtractedFileAudit()){
		    	  extractFileAudit = true;
		  		}
		  }
		//System.out.println("\n\n\n\n jobTypes in activeship extract>>>>>>>>   "+jobTypes);
		
		StringBuffer jobBuffer = new StringBuffer("");
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		}
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		StringBuffer beginDates = new StringBuffer();
		StringBuffer endDates = new StringBuffer();
		StringBuffer deliveryTBeginDates = new StringBuffer();
		StringBuffer deliveryTEndDates = new StringBuffer();
		StringBuffer deliveryActBeginDates = new StringBuffer();
		StringBuffer deliveryActEndDates = new StringBuffer();
		StringBuffer loadTgtBeginDates = new StringBuffer();
		StringBuffer loadTgtEndDates = new StringBuffer();
		StringBuffer invPostingBeginDates = new StringBuffer();
		StringBuffer invPostingEndDates = new StringBuffer();
		StringBuffer revenueRecognitionBeginDates = new StringBuffer();
		StringBuffer revenueRecognitionEndDates = new StringBuffer();
		StringBuffer invoicingBeginDates = new StringBuffer();
		StringBuffer invoicingEndDates = new StringBuffer();
		StringBuffer storageBeginDates = new StringBuffer();
		StringBuffer storageEndDates = new StringBuffer();
		StringBuffer createdDates = new StringBuffer();
		StringBuffer toCreatedDates = new StringBuffer();
		StringBuffer payPostingBeginDates = new StringBuffer();
		StringBuffer payPostingEndDates = new StringBuffer();
		StringBuffer receivedBeginDates = new StringBuffer();
		StringBuffer receivedEndDates = new StringBuffer();
		StringBuffer postingPeriodBeginDates = new StringBuffer();
		StringBuffer postingPeriodEndDates = new StringBuffer();
		StringBuffer bookingDates = new StringBuffer();
		StringBuffer toBookingDates = new StringBuffer();
		StringBuffer updateDates = new StringBuffer();
		StringBuffer toUpdateDates = new StringBuffer();
		StringBuffer query = new StringBuffer();
		boolean isAnd = false;
		if(!revenueRecognitionPeriod.equals("")){
        	String  returnValue=   findPeriodDate(revenueRecognitionPeriod);
        	String [] returnValueArray=returnValue.split("#");
        	revenueRecognitionBeginDates = new StringBuffer(returnValueArray[0]);
        	revenueRecognitionEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate == null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer();
		} else if (revenueRecognitionBeginDate == null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer();
		}
        }
		if(!loadActualDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(loadActualDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	beginDates = new StringBuffer(returnValueArray[0]);
        	endDates = new StringBuffer(returnValueArray[1]);
        }else{
		if (beginDate != null && endDate != null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer(formats.format(endDate));

		} else if (beginDate != null && endDate == null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer();
		} else if (beginDate == null && endDate != null) {
			beginDates = new StringBuffer();
			endDates = new StringBuffer(formats.format(endDate));
		} else {
			beginDates = new StringBuffer();
			endDates = new StringBuffer();

		}
        }
		if(!createDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(createDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	createdDates = new StringBuffer(returnValueArray[0]);
        	toCreatedDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (createDate != null && toCreateDate != null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else if (createDate != null && toCreateDate == null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer();
		} else if (createDate == null && toCreateDate != null) {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer();
		}
        }
		if(!updateDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(updateDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	updateDates  = new StringBuffer(returnValueArray[0]);
        	toUpdateDates  = new StringBuffer(returnValueArray[1]);
        }else{
        	if (updateDate != null && toUpdateDate != null) {
				updateDates = new StringBuffer(formats.format(updateDate));
				toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
			} else if (updateDate != null && toUpdateDate == null) {
				updateDates = new StringBuffer(formats.format(updateDate));
				toUpdateDates = new StringBuffer();
			} else if (updateDate == null && toUpdateDate != null) {
				updateDates = new StringBuffer();
				toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
			} else {
				updateDates = new StringBuffer();
				toUpdateDates = new StringBuffer();
			}
        }
		if(!deliveryTargetDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(deliveryTargetDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	deliveryTBeginDates = new StringBuffer(returnValueArray[0]);
        	deliveryTEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (deliveryTBeginDate != null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer();
		} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer();
		}
        }
		if(!deliveryActualDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(deliveryActualDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	deliveryActBeginDates = new StringBuffer(returnValueArray[0]);
        	deliveryActEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (deliveryActBeginDate != null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer();
		} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer();
		}
        }
        if(!loadTargetDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(loadTargetDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	loadTgtBeginDates = new StringBuffer(returnValueArray[0]);
        	loadTgtEndDates = new StringBuffer(returnValueArray[1]);
        }else{
		if (loadTgtBeginDate != null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer();
		} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer();
		}
        }
        if(!invoicePostingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(invoicePostingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	invPostingBeginDates = new StringBuffer(returnValueArray[0]);
        	invPostingEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (invPostingBeginDate != null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else if (invPostingBeginDate != null && invPostingEndDate == null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer();
		} else if (invPostingBeginDate == null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer();
		}
        }
        if(!InvoicingdatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(InvoicingdatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	invoicingBeginDates = new StringBuffer(returnValueArray[0]);
        	invoicingEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (invoicingBeginDate != null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else if (invoicingBeginDate != null && invoicingEndDate == null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer();
		} else if (invoicingBeginDate == null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer();
		}
        }
        if(!storageDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(storageDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	storageEndDates = new StringBuffer(returnValueArray[0]);
        	storageEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (storageBeginDate != null && storageEndDate != null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else if (storageBeginDate != null && storageEndDate == null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer();
		} else if (storageBeginDate == null && storageEndDate != null) {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer();
		}
        }
		if(!payablePostingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(payablePostingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	payPostingBeginDates = new StringBuffer(returnValueArray[0]);
        	payPostingEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (payPostingBeginDate != null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else if (payPostingBeginDate != null && payPostingEndDate == null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer();
		} else if (payPostingBeginDate == null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer();
		}
        }
		if(!receivedDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(receivedDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	receivedBeginDates = new StringBuffer(returnValueArray[0]);
        	receivedEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (receivedBeginDate != null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else if (receivedBeginDate != null && receivedEndDate == null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer();
		} else if (receivedBeginDate == null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer();
		}
        }
		if(!PostingPeriodDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(PostingPeriodDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	postingPeriodBeginDates = new StringBuffer(returnValueArray[0]);
        	postingPeriodEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (postingPeriodBeginDate != null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else if (postingPeriodBeginDate != null && postingPeriodEndDate == null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer();
		 } else if (postingPeriodBeginDate == null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer();
		 } 
        }
		if(!bookingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(bookingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	bookingDates = new StringBuffer(returnValueArray[0]);
        	toBookingDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (bookingDate != null && toBookingDate != null) {
			bookingDates = new StringBuffer(formats.format(bookingDate));
			toBookingDates = new StringBuffer(formats.format(toBookingDate));
		} else if (bookingDate != null && toBookingDate == null) {
			bookingDates = new StringBuffer(formats.format(bookingDate));
			toBookingDates = new StringBuffer();
		} else if (bookingDate == null && toBookingDate != null) {
			bookingDates = new StringBuffer();
			toBookingDates = new StringBuffer(formats.format(toBookingDate));
		} else {
			bookingDates = new StringBuffer();
			toBookingDates = new StringBuffer();
		}
        }
		if (jobTypesCondition.equals("") && billToCodeCondition.equals("") && originAgentCondition .equals("") && destinationAgentCondition.equals("") && bookingCodeCondition.equals("") && routingCondition.equals("") && modeCondition.equals("") && packModeCondition.equals("")
				&& commodityCondition.equals("") && coordinatorCondition.equals("") && consultantCondition.equals("") && salesmanCondition.equals("") && oACountryCondition.equals("") && dACountryCondition.equals("")
				&& actualWgtCondition.equals("") && companyCodeCondition.equals("") && beginDates.toString().equals("") && endDates.toString().equals("") && actualVolumeCondition.equals("")
				&& createdDates.toString().equals("") && toCreatedDates.toString().equals("") && updateDates.toString().equals("") && toUpdateDates.toString().equals("") &&   deliveryTBeginDates.toString().equals("") && deliveryTEndDates.toString().equals("") && deliveryActBeginDates.toString().equals("")
				&& deliveryActEndDates.toString().equals("") && loadTgtBeginDates.toString().equals("") && loadTgtEndDates.toString().equals("") && invPostingBeginDates.toString().equals("") && invPostingEndDates.toString().equals("")
				&& invoicingBeginDates.toString().equals("") && invoicingEndDates.toString().equals("") && storageBeginDates.toString().equals("") && storageEndDates.toString().equals("") && revenueRecognitionBeginDates.toString().equals("") && revenueRecognitionEndDates.toString().equals("")&&payPostingBeginDates.toString().equals("") && payPostingEndDates.toString().equals("")&& receivedBeginDates.toString().equals("") && receivedEndDates.toString().equals("") && postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals("")
				&& bookingDates.toString().equals("") && toBookingDates.toString().equals("") && carrierCondition.equals("") && driverIdCondition.equals("") && accountCodeCondition.equals("")) {
			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType is  null or s.moveType = '') ");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType is  null or s.moveType = '') ");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status not in ('CNCL') and s.status is not null ");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  where  s.corpId='" + sessionCorpID + "' and (s.moveType='BookedMove' or s.moveType is  null or s.moveType = '') ");
			}
			//query.append("  where  s.corpId='"+sessionCorpID+"'  and  s.status not in ('CLSD','CNCL')");
		} else { 
			query.append("  where  ");

			if (jobTypesCondition.equals("") || jobTypesCondition == null) {

			} else {
				query.append(" s.job  ");
				isAnd = true;
				if (jobTypesCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append(" in  ");
					query.append("(" + jobtypeMultiple + ")");
				} else {
					query.append(" not in  ");
					query.append("(" + jobtypeMultiple + ")");
				}
			}
			if (billToCodeCondition.equals("") || billToCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.billToCode");
				} else {
					query.append("  s.billToCode");
				}

				if (billToCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + billToCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + billToCodeType.trim() + "'");
				}
				isAnd = true;
			}
			if (originAgentCondition.equals("") || originAgentCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and t.originAgentCode");
				} else {
					query.append("  t.originAgentCode");
				}
				if (originAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + originAgentCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + originAgentCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (destinationAgentCondition.equals("") |destinationAgentCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and t.destinationAgentCode");
				} else {
					query.append("  t.destinationAgentCode");
				}
				if (destinationAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + destinationAgentCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + destinationAgentCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (bookingCodeCondition.equals("") || bookingCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.bookingAgentCode");
				} else {
					query.append(" s.bookingAgentCode");
				}
				if (bookingCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + bookingCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + bookingCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (routingCondition.equals("") || routingCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.routing");
				} else {
					query.append("  s.routing");
				}
				if (routingCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + routingTypes + "'");
				} else {
					query.append("<>");
					query.append("'" + routingTypes + "'");
				}
				isAnd = true;
			}
			if (modeCondition.equals("") || modeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.mode");
				} else {
					query.append("  s.mode");
				}
				if (modeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + modeType + "'");
				} else {
					query.append("<>");
					query.append("'" + modeType + "'");
				}
				isAnd = true;
			}
			if (packModeCondition.equals("") || packModeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.packingMode");
				} else {
					query.append("  s.packingMode");
				}
				if (packModeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + packModeType + "'");
				} else {
					query.append("<>");
					query.append("'" + packModeType + "'");
				}
				isAnd = true;
			}
			if (commodityCondition.equals("") || commodityCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.commodity");
				} else {
					query.append("  s.commodity");
				}
				if (commodityCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + commodityType + "'");
				} else {
					query.append("<>");
					query.append("'" + commodityType + "'");
				}
				isAnd = true;
			}
			if (coordinatorCondition.equals("") || coordinatorCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.coordinator");
				} else {
					query.append("   s.coordinator");
				}
				if (coordinatorCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + coordinatorType + "'");
				} else {
					query.append("<>");
					query.append("'" + coordinatorType + "'");
				}
				isAnd = true;
			}
			if (consultantCondition.equals("") || consultantCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.estimator");
				} else {
					query.append("   s.estimator");
				}
				if (consultantCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + consultantType + "'");
				} else {
					query.append("<>");
					query.append("'" + consultantType + "'");
				}
				isAnd = true;
			}
			
			if (salesmanCondition.equals("") || salesmanCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.salesMan");
				} else {
					query.append("   s.salesMan");
				}
				if (salesmanCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + salesmanType + "'");
				} else {
					query.append("<>");
					query.append("'" + salesmanType + "'");
				}
				isAnd = true;
			}
			if (oACountryCondition.equals("") || oACountryCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.originCountryCode");
				} else {
					query.append("   s.originCountryCode");
				}
				if (oACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + oACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + oACountryType + "'");
				}
				isAnd = true;
			}
			if (dACountryCondition.equals("") || dACountryCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.destinationCountryCode");
				} else {
					query.append("  s.destinationCountryCode");
				}
				if (dACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + dACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + dACountryType + "'");
				}
				isAnd = true;
			} 
			if (actualWgtCondition.equals("") || actualWgtCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualNetWeight");
				} else {
					query.append("  m.actualNetWeight");
				}

				if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualWgtType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualWgtType + "'");
				}
				isAnd = true;
			}
			if (actualVolumeCondition.equals("") || actualVolumeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualCubicFeet");
				} else {
					query.append("  m.actualCubicFeet");
				}
				if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualVolumeType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualVolumeType + "'");
				}
				isAnd = true;
			}
			if (beginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + beginDates + "')");
				} else if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + beginDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( createdDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + createdDates + "')");
				} else if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + createdDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toCreatedDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toCreatedDates + "')");
				} else if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toCreatedDates + "')");
				} else {

				}
				isAnd = true;
			}
            
			if ( updateDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				}
				if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + updateDates + "')");
				} else if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + updateDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toUpdateDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("   and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				}
				if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toUpdateDates + "')");
				} else if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toUpdateDates + "')");
				} else {

				}
				isAnd = true;
			}

			if (endDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + endDates + "')");
				} else if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + endDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTBeginDates + "')");
				} else if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTEndDates + "')");
				} else if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActBeginDates + "')");
				} else if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActEndDates + "')");
				} else if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (loadTgtBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtBeginDates + "')");
				} else if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtEndDates + "')");
				} else if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (invPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingBeginDates + "')");
				} else if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (invPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingEndDates + "')");
				} else if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if ( payPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingBeginDates + "')");
				} else if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (payPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingEndDates + "')");
				} else if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingEndDates + "')");
				} else {
					
				}
				isAnd = true;
			} 
			if ( receivedBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedBeginDates + "')");
				} else if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ( receivedEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedEndDates + "')");
				} else if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ((postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals(""))) {

			} else 
			{
				if(((!( postingPeriodBeginDates.toString().equals(""))))&& (!(postingPeriodEndDates.toString().equals("")))) {
				if (isAnd == true) {
					query.append("  and ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append("  ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");

                  
				  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
                   query.append("  or ( (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				   if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}

                 query.append("  and (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "') ) ) )");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "') ) ) ) ");
				} else {
					
				}

				isAnd = true;
			} 
				else if((!( postingPeriodBeginDates.toString().equals("")))&& ((postingPeriodEndDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			else  if((!( postingPeriodEndDates.toString().equals("")))&& ((postingPeriodBeginDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			}
			
			if (invoicingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingBeginDates + "')");
				} else if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (invoicingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingEndDates + "')");
				} else if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			if (storageBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				}

				if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageBeginDates + "')");
				} else if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (storageEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				}

				if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageEndDates + "')");
				} else if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			if (revenueRecognitionBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (revenueRecognitionEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (companyCodeCondition == null || companyCodeCondition.equals("")) {

			} else {
				if (isAnd == true) {
					query.append(" and s.companyDivision");
				} else {
					query.append("  s.companyDivision");
				}
				if (companyCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + companyCode + "'");
				} else {
					query.append("<>");
					query.append("'" + companyCode + "'");
				}
				isAnd = true;
			}
			if ( bookingDates.toString().equals("")) {
			} else {
				if (isAnd == true) {
					query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				} else {
					query.append(" (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				}
				if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + bookingDates + "')");
				} else if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + bookingDates + "')");
				} else {
				}
				isAnd = true;
			}
			if ( toBookingDates.toString().equals("")) {
			} else {
				if (isAnd == true) {
					query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				} else {
					query.append(" (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				}
				if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toBookingDates + "')");
				} else if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toBookingDates + "')");
				} else {
				}
				isAnd = true;
			}
			if (carrierCondition.equals("") || carrierCondition == null) {
			} else {
				if (isAnd == true) {
					query.append(" and m.carrier");
				} else {
					query.append("  m.carrier");
				}
				if (carrierCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + carrier.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + carrier.trim() + "'");
				}
				isAnd = true;
			}
			if (driverIdCondition.equals("") || driverIdCondition == null) {
			} else {
				if (isAnd == true) {
					query.append(" and m.driverId");
				} else {
					query.append("  m.driverId");
				}
				if (driverIdCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + driverId.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + driverId.trim() + "'");
				}
				isAnd = true;
			}
			
			
			if (accountCodeCondition.equals("") || accountCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and c.accountCode");
				} else {
					query.append("   c.accountCode");
				}
				if (accountCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + accountCodeType + "'");
				} else {
					query.append("<>");
					query.append("'" + accountCodeType + "'");
				}
				isAnd = true;
			}

			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') ");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status  in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') ");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CNCL') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') ");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '')");
			}
			//query.append("   and s.corpId='"+sessionCorpID+"' and  s.status not in ('CLSD','CNCL')");			

		}
   
		//String queryExtract=extractManager.saveForExtract(query.toString());
	if(selectCondition.equals("")||selectCondition.equals("#"))
  	  {
  	  }
  	 else
  	  {
  		selectCondition=selectCondition.trim();
  	    if(selectCondition.indexOf("#")==0)
  		 {
  	    	selectCondition=selectCondition.substring(1);
  		 }
  		if(selectCondition.lastIndexOf("#")==selectCondition.length()-1)
  		 {
  			selectCondition=selectCondition.substring(0, selectCondition.length()-1);
  		 }
		
  	  }
	selectCondition=selectCondition.replaceAll("#", ",");
	List selectConditionList=new ArrayList();
	selectConditionList= extractColumnMgmtManager.getSelectCondition(selectCondition,reportName,sessionCorpID);
    String SaveSelectCondition=new String();
    Iterator iterator =selectConditionList.iterator();
    while(iterator.hasNext())
    {
    	SaveSelectCondition =SaveSelectCondition.concat((String)iterator.next().toString());
    	SaveSelectCondition=SaveSelectCondition.concat("#");
    }
    SaveSelectCondition=SaveSelectCondition.trim();
	    if(SaveSelectCondition.indexOf("#")==0)
		 {
	    	SaveSelectCondition=SaveSelectCondition.substring(1);
		 }
		if(SaveSelectCondition.lastIndexOf("#")==SaveSelectCondition.length()-1)
		 {
			SaveSelectCondition=SaveSelectCondition.substring(0, SaveSelectCondition.length()-1);
		 }
		String columnSelect=SaveSelectCondition;
		SaveSelectCondition=SaveSelectCondition.replaceAll("#", ",");
	//System.out.println("\n\n\n\n\n\n\n\n query" + query);
	//System.out.println("\n\n\n\n\n\n\n\n selct query    " +SaveSelectCondition+" query    " + query    ); 
	List activeShipments = serviceOrderManager.dynamicDataFinanceDetails(SaveSelectCondition, query.toString(),sessionCorpID);
	
	HttpServletResponse response = getResponse();
	response.setContentType("application/vnd.ms-excel");
	ServletOutputStream outputStream = response.getOutputStream();
	File file = new File("DynamicFinancialDetails");
	response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls");
	response.setHeader("Pragma", "public");
	response.setHeader("Cache-Control", "max-age=0");
	//response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" + "\t The number of line is" + activeShipments.size() + "extracted");
	out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + activeShipments.size() + "\t is extracted");
	String bA_SSCW = "";
	String[] conditionArray=columnSelect.split("#");
    int arrayLength = conditionArray.length;
	for(int i=0;i<arrayLength;i++)
	 {	
	   String condition=conditionArray[i]; 
	   String []columnArray=condition.split(" as");
	   //condition= condition.substring(condition.indexOf("as")+2);
	   String column=columnArray[1];
	   outputStream.write((column+"\t").getBytes());
	 } 
	//outputStream.write("URL".getBytes());
	outputStream.write("\r\n".getBytes());
	Iterator it = activeShipments.iterator();

	while(it.hasNext()){
		Object[] row = (Object[]) it.next();

		for(int i=0; i<arrayLength; i++){
			if (row[i] != null) {
				String data = row[i].toString();
				outputStream.write((data + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
		}
		//outputStream.write(("https://www.skyrelo.com/redsky/editTrackingStatus.html?id="+row[arrayLength] + "\t").getBytes());
		outputStream.write("\n".getBytes());
    }
	if(extractFileAudit){
		outputStream.close();
		response.setContentType("application/octet-stream");
		//ServletOutputStream outputStream = response.getOutputStream();
		SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
		StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
		String currentyear=yearDate.toString();
		
		String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
		File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
		SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
		StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
		String currentDateTime=tempDate.toString().trim();
		String fileName="DynamicFinancialDetails_"+currentDateTime+".xls";
		String filePath=uploadDir+""+fileName;
		File file1 = new File(filePath); 
		FileOutputStream outputStream1 = new FileOutputStream(file1);
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + "");
		conditionArray=columnSelect.split("#");
	    arrayLength = conditionArray.length;
		for(int i=0;i<arrayLength;i++)
		 {	
		   String condition=conditionArray[i]; 
		   String []columnArray=condition.split(" as");
		   //condition= condition.substring(condition.indexOf("as")+2);
		   String column=columnArray[1];
		   outputStream1.write((column+"\t").getBytes());
		 } 
		//outputStream.write("URL".getBytes());
		outputStream1.write("\r\n".getBytes());
		Iterator its = activeShipments.iterator();

		while(its.hasNext()){
			Object[] row = (Object[]) its.next();

			for(int i=0; i<arrayLength; i++){
				if (row[i] != null) {
					String data = row[i].toString();
					outputStream1.write((data + "\t").getBytes());
				} else {
					outputStream1.write("\t".getBytes());
				}
			}
			//outputStream.write(("https://www.skyrelo.com/redsky/editTrackingStatus.html?id="+row[arrayLength] + "\t").getBytes());
			outputStream1.write("\n".getBytes());
	    }
		extractedFileLog = new ExtractedFileLog();
		extractedFileLog.setCorpID(sessionCorpID);
		extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
		extractedFileLog.setCreatedOn(new Date());
		extractedFileLog.setLocation(filePath);
		extractedFileLog.setFileName(fileName);
		extractedFileLog.setModule("Data Extract");
		extractedFileLog=extractedFileLogManager.save(extractedFileLog);
		}
	} 
	
		
	
	
	public void dynamicFinancialSummary() throws Exception{ 
		if(extractVal == null || (!extractVal.equals("true")) ){
			company=companyManager.findByCorpID(sessionCorpID).get(0);
		    if(company!=null){
		      if(company.getExtractedFileAudit() !=null && company.getExtractedFileAudit()){
		    	  extractFileAudit = true;
		  		}
		    }
		StringBuffer jobBuffer = new StringBuffer("");
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		}
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		StringBuffer beginDates = new StringBuffer();
		StringBuffer endDates = new StringBuffer();
		StringBuffer deliveryTBeginDates = new StringBuffer();
		StringBuffer deliveryTEndDates = new StringBuffer();
		StringBuffer deliveryActBeginDates = new StringBuffer();
		StringBuffer deliveryActEndDates = new StringBuffer();
		StringBuffer loadTgtBeginDates = new StringBuffer();
		StringBuffer loadTgtEndDates = new StringBuffer();
		StringBuffer invPostingBeginDates = new StringBuffer();
		StringBuffer invPostingEndDates = new StringBuffer();
		StringBuffer revenueRecognitionBeginDates = new StringBuffer();
		StringBuffer revenueRecognitionEndDates = new StringBuffer();
		StringBuffer invoicingBeginDates = new StringBuffer();
		StringBuffer invoicingEndDates = new StringBuffer();
		StringBuffer storageBeginDates = new StringBuffer();
		StringBuffer storageEndDates = new StringBuffer();
		StringBuffer createdDates = new StringBuffer();
		StringBuffer toCreatedDates = new StringBuffer();
		StringBuffer payPostingBeginDates = new StringBuffer();
		StringBuffer payPostingEndDates = new StringBuffer();
		StringBuffer receivedBeginDates = new StringBuffer();
		StringBuffer receivedEndDates = new StringBuffer();
		StringBuffer postingPeriodBeginDates = new StringBuffer();
		StringBuffer postingPeriodEndDates = new StringBuffer();
		StringBuffer bookingDates = new StringBuffer();
		StringBuffer toBookingDates = new StringBuffer();
		StringBuffer updateDates = new StringBuffer();
		StringBuffer toUpdateDates = new StringBuffer();
		StringBuffer query = new StringBuffer();
		boolean isAnd = false;
		if(!revenueRecognitionPeriod.equals("")){
        	String  returnValue=   findPeriodDate(revenueRecognitionPeriod);
        	String [] returnValueArray=returnValue.split("#");
        	revenueRecognitionBeginDates = new StringBuffer(returnValueArray[0]);
        	revenueRecognitionEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate == null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer();
		} else if (revenueRecognitionBeginDate == null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer();
		}
        }
		if(!loadActualDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(loadActualDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	beginDates = new StringBuffer(returnValueArray[0]);
        	endDates = new StringBuffer(returnValueArray[1]);
        }else{
		if (beginDate != null && endDate != null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer(formats.format(endDate));

		} else if (beginDate != null && endDate == null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer();
		} else if (beginDate == null && endDate != null) {
			beginDates = new StringBuffer();
			endDates = new StringBuffer(formats.format(endDate));
		} else {
			beginDates = new StringBuffer();
			endDates = new StringBuffer();

		}
        }
		if(!createDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(createDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	createdDates = new StringBuffer(returnValueArray[0]);
        	toCreatedDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (createDate != null && toCreateDate != null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else if (createDate != null && toCreateDate == null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer();
		} else if (createDate == null && toCreateDate != null) {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer();
		}
        }
		if(!updateDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(updateDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	updateDates  = new StringBuffer(returnValueArray[0]);
        	toUpdateDates  = new StringBuffer(returnValueArray[1]);
        }else{
        	if (updateDate != null && toUpdateDate != null) {
				updateDates = new StringBuffer(formats.format(updateDate));
				toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
			} else if (updateDate != null && toUpdateDate == null) {
				updateDates = new StringBuffer(formats.format(updateDate));
				toUpdateDates = new StringBuffer();
			} else if (updateDate == null && toUpdateDate != null) {
				updateDates = new StringBuffer();
				toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
			} else {
				updateDates = new StringBuffer();
				toUpdateDates = new StringBuffer();
			}
        }
		if(!deliveryTargetDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(deliveryTargetDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	deliveryTBeginDates = new StringBuffer(returnValueArray[0]);
        	deliveryTEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (deliveryTBeginDate != null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer();
		} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer();
		}
        }
		if(!deliveryActualDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(deliveryActualDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	deliveryActBeginDates = new StringBuffer(returnValueArray[0]);
        	deliveryActEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (deliveryActBeginDate != null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer();
		} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer();
		}
        }
        if(!loadTargetDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(loadTargetDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	loadTgtBeginDates = new StringBuffer(returnValueArray[0]);
        	loadTgtEndDates = new StringBuffer(returnValueArray[1]);
        }else{
		if (loadTgtBeginDate != null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer();
		} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer();
		}
        }
        if(!invoicePostingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(invoicePostingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	invPostingBeginDates = new StringBuffer(returnValueArray[0]);
        	invPostingEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (invPostingBeginDate != null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else if (invPostingBeginDate != null && invPostingEndDate == null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer();
		} else if (invPostingBeginDate == null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer();
		}
        }
        if(!InvoicingdatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(InvoicingdatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	invoicingBeginDates = new StringBuffer(returnValueArray[0]);
        	invoicingEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (invoicingBeginDate != null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else if (invoicingBeginDate != null && invoicingEndDate == null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer();
		} else if (invoicingBeginDate == null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer();
		}
        }
        if(!storageDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(storageDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	storageEndDates = new StringBuffer(returnValueArray[0]);
        	storageEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (storageBeginDate != null && storageEndDate != null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else if (storageBeginDate != null && storageEndDate == null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer();
		} else if (storageBeginDate == null && storageEndDate != null) {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer();
		}
        }
		if(!payablePostingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(payablePostingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	payPostingBeginDates = new StringBuffer(returnValueArray[0]);
        	payPostingEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (payPostingBeginDate != null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else if (payPostingBeginDate != null && payPostingEndDate == null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer();
		} else if (payPostingBeginDate == null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer();
		}
        }
		if(!receivedDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(receivedDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	receivedBeginDates = new StringBuffer(returnValueArray[0]);
        	receivedEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (receivedBeginDate != null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else if (receivedBeginDate != null && receivedEndDate == null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer();
		} else if (receivedBeginDate == null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer();
		}
        }
		if(!PostingPeriodDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(PostingPeriodDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	postingPeriodBeginDates = new StringBuffer(returnValueArray[0]);
        	postingPeriodEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (postingPeriodBeginDate != null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else if (postingPeriodBeginDate != null && postingPeriodEndDate == null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer();
		 } else if (postingPeriodBeginDate == null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer();
		 } 
        }
		if(!bookingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(bookingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	bookingDates = new StringBuffer(returnValueArray[0]);
        	toBookingDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (bookingDate != null && toBookingDate != null) {
			bookingDates = new StringBuffer(formats.format(bookingDate));
			toBookingDates = new StringBuffer(formats.format(toBookingDate));
		} else if (bookingDate != null && toBookingDate == null) {
			bookingDates = new StringBuffer(formats.format(bookingDate));
			toBookingDates = new StringBuffer();
		} else if (bookingDate == null && toBookingDate != null) {
			bookingDates = new StringBuffer();
			toBookingDates = new StringBuffer(formats.format(toBookingDate));
		} else {
			bookingDates = new StringBuffer();
			toBookingDates = new StringBuffer();
		}
        }
		if (jobTypesCondition.equals("") && billToCodeCondition.equals("")&& originAgentCondition .equals("")&& destinationAgentCondition.equals("") && bookingCodeCondition.equals("") && routingCondition.equals("") && modeCondition.equals("") && packModeCondition.equals("")
				&& commodityCondition.equals("") && coordinatorCondition.equals("") && consultantCondition.equals("") && salesmanCondition.equals("") && oACountryCondition.equals("") && dACountryCondition.equals("")
				&& actualWgtCondition.equals("") && companyCodeCondition.equals("") && beginDates.toString().equals("") && endDates.toString().equals("") && actualVolumeCondition.equals("")
				&& createdDates.toString().equals("") && toCreatedDates.toString().equals("") && updateDates.toString().equals("") && toUpdateDates.toString().equals("") &&  deliveryTBeginDates.toString().equals("") && deliveryTEndDates.toString().equals("") && deliveryActBeginDates.toString().equals("")
				&& deliveryActEndDates.toString().equals("") && loadTgtBeginDates.toString().equals("") && loadTgtEndDates.toString().equals("") && invPostingBeginDates.toString().equals("") && invPostingEndDates.toString().equals("")
				&& invoicingBeginDates.toString().equals("") && invoicingEndDates.toString().equals("") && storageBeginDates.toString().equals("") && storageEndDates.toString().equals("") && revenueRecognitionBeginDates.toString().equals("") && revenueRecognitionEndDates.toString().equals("")&& payPostingBeginDates.toString().equals("") && payPostingEndDates.toString().equals("")&& receivedBeginDates.toString().equals("") && receivedEndDates.toString().equals("")&& postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals("")
				&& bookingDates.toString().equals("") && toBookingDates.toString().equals("") && carrierCondition.equals("") && driverIdCondition.equals("") && accountCodeCondition.equals("")) {

			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status not in ('CNCL') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  where  s.corpId='" + sessionCorpID + "' and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber ");
			}
			//query.append(" where   s.corpId='"+sessionCorpID+"'  and  s.status not in ('CLSD','CNCL') group by s.shipNumber");
		} else {

			query.append("  where  ");

			if (jobTypesCondition.equals("") || jobTypesCondition == null) {

			} else {
				query.append(" s.job ");
				isAnd = true;
				if (jobTypesCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append(" in  ");
					query.append("(" + jobtypeMultiple + ")");
				} else {
					query.append(" not in  ");
					query.append("(" + jobtypeMultiple + ")");
				}

			}
			if (billToCodeCondition.equals("") || billToCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.billToCode");
				} else {
					query.append("  s.billToCode");
				}
				if (billToCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + billToCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + billToCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (originAgentCondition.equals("") || originAgentCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and t.originAgentCode");
				} else {
					query.append("  t.originAgentCode");
				}
				if (originAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + originAgentCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + originAgentCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (destinationAgentCondition.equals("") |destinationAgentCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and t.destinationAgentCode");
				} else {
					query.append("  t.destinationAgentCode");
				}
				if (destinationAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + destinationAgentCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + destinationAgentCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (bookingCodeCondition.equals("") || bookingCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.bookingAgentCode");
				} else {
					query.append("  s.bookingAgentCode");
				}
				if (bookingCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + bookingCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + bookingCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (routingCondition.equals("") || routingCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.routing");
				} else {
					query.append("   s.routing");
				}
				if (routingCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + routingTypes + "'");
				} else {
					query.append("<>");
					query.append("'" + routingTypes + "'");
				}
				isAnd = true;
			}
			if (modeCondition.equals("") || modeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.mode");
				} else {
					query.append("  s.mode");
				}
				if (modeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + modeType + "'");
				} else {
					query.append("<>");
					query.append("'" + modeType + "'");
				}
				isAnd = true;
			}
			if (packModeCondition.equals("") || packModeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.packingMode");
				} else {
					query.append("   s.packingMode");
				}
				if (packModeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + packModeType + "'");
				} else {
					query.append("<>");
					query.append("'" + packModeType + "'");
				}
				isAnd = true;
			}
			if (commodityCondition.equals("") || commodityCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.commodity");
				} else {
					query.append("  s.commodity");
				}
				if (commodityCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + commodityType + "'");
				} else {
					query.append("<>");
					query.append("'" + commodityType + "'");
				}
				isAnd = true;
			}
			if (coordinatorCondition.equals("") || coordinatorCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.coordinator");
				} else {
					query.append("   s.coordinator");
				}
				if (coordinatorCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + coordinatorType + "'");
				} else {
					query.append("<>");
					query.append("'" + coordinatorType + "'");
				}
				isAnd = true;
			}
			if (consultantCondition.equals("") || consultantCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.estimator");
				} else {
					query.append("  s.estimator");
				}
				if (consultantCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + consultantType + "'");
				} else {
					query.append("<>");
					query.append("'" + consultantType + "'");
				}
				isAnd = true;
			} 
			if (salesmanCondition.equals("") || salesmanCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.salesMan");
				} else {
					query.append("   s.salesMan");
				}
				if (salesmanCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + salesmanType + "'");
				} else {
					query.append("<>");
					query.append("'" + salesmanType + "'");
				}
				isAnd = true;
			}
			if (oACountryCondition.equals("") || oACountryCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.originCountryCode");
				} else {
					query.append("   s.originCountryCode");
				}
				if (oACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + oACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + oACountryType + "'");
				}
				isAnd = true;
			}
			if (dACountryCondition.equals("") || dACountryCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.destinationCountryCode");
				} else {
					query.append("  s.destinationCountryCode");
				}
				if (dACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + dACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + dACountryType + "'");
				}
				isAnd = true;
			} 
			if (actualWgtCondition.equals("") || actualWgtCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualNetWeight");
				} else {
					query.append("  m.actualNetWeight");
				}

				if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualWgtType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualWgtType + "'");
				}
				isAnd = true;
			}
			if (actualVolumeCondition.equals("") || actualVolumeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualCubicFeet");
				} else {
					query.append("  m.actualCubicFeet");
				}
				if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualVolumeType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualVolumeType + "'");
				}
				isAnd = true;
			}
			if (beginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + beginDates + "')");
				} else if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + beginDates + "')");
				} else {

				}
				isAnd = true;
			}

			if ( createdDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + createdDates + "')");
				} else if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + createdDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toCreatedDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toCreatedDates + "')");
				} else if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toCreatedDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( updateDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				}
				if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + updateDates + "')");
				} else if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + updateDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toUpdateDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("   and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				}
				if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toUpdateDates + "')");
				} else if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toUpdateDates + "')");
				} else {

				}
				isAnd = true;
			}

			if (endDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + endDates + "')");
				} else if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + endDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (deliveryTBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTBeginDates + "')");
				} else if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTEndDates + "')");
				} else if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActBeginDates + "')");
				} else if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActEndDates + "')");
				} else if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtBeginDates + "')");
				} else if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtEndDates + "')");
				} else if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (invPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingBeginDates + "')");
				} else if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (invPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingEndDates + "')");
				} else if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (payPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingBeginDates + "')");
				} else if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (payPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingEndDates + "')");
				} else if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ( receivedBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedBeginDates + "')");
				} else if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (receivedEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedEndDates + "')");
				} else if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ( (postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals(""))) {

			} else 
			{
				if(((!( postingPeriodBeginDates.toString().equals(""))))&&!(postingPeriodEndDates.toString().equals(""))) {
				if (isAnd == true) {
					query.append("  and ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append("  ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");

                  
				  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
                   query.append("  or ( (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				   if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}

                 query.append("  and (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "') ) ) )");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "') ) ) ) ");
				} else {
					
				}

				isAnd = true;
			} 
				else if((!( postingPeriodBeginDates.toString().equals("")))&& ((postingPeriodEndDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			else  if((!( postingPeriodEndDates.toString().equals("")))&& ((postingPeriodBeginDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			}
			 
			if (invoicingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingBeginDates + "')");
				} else if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (invoicingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingEndDates + "')");
				} else if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			
			if (storageBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				}

				if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageBeginDates + "')");
				} else if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageBeginDates + "')");
				} else { 
				}
				isAnd = true;
			}

			if (storageEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				}

				if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageEndDates + "')");
				} else if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageEndDates + "')");
				} else { 
				}
				isAnd = true;
			}
			
			
			if (revenueRecognitionBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else { 
				}
				isAnd = true;
			}

			if (revenueRecognitionEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}			

			if (companyCodeCondition == null || companyCodeCondition.equals("")) {

			} else {
				if (isAnd == true) {
					query.append(" and s.companyDivision");
				} else {
					query.append("  s.companyDivision");
				}
				if (companyCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + companyCode + "'");
				} else {
					query.append("<>");
					query.append("'" + companyCode + "'");
				}
				isAnd = true;
			}
			if ( bookingDates.toString().equals("")) {
			} else {
				if (isAnd == true) {
					query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				} else {
					query.append(" (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				}
				if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + bookingDates + "')");
				} else if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + bookingDates + "')");
				} else {
				}
				isAnd = true;
			}
			if ( toBookingDates.toString().equals("")) {
			} else {
				if (isAnd == true) {
					query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				} else {
					query.append(" (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				}
				if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toBookingDates + "')");
				} else if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toBookingDates + "')");
				} else {
				}
				isAnd = true;
			}
			
			if (carrierCondition.equals("") || carrierCondition == null) {
			} else {
				if (isAnd == true) {
					query.append(" and m.carrier");
				} else {
					query.append("  m.carrier");
				}
				if (carrierCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + carrier.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + carrier.trim() + "'");
				}
				isAnd = true;
			}
			
			if (driverIdCondition.equals("") || driverIdCondition == null) {
			} else {
				if (isAnd == true) {
					query.append(" and m.driverId");
				} else {
					query.append("  m.driverId");
				}
				if (driverIdCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + driverId.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + driverId.trim() + "'");
				}
				isAnd = true;
			}
			
			
			
			if (accountCodeCondition.equals("") || accountCodeCondition == null) {

			} else {
				if (isAnd == true) {
					//query.append(" and c.customerCode");
					query.append(" and c.accountCode");
				} else {
					//query.append("  c.customerCode");
					query.append(" and c.accountCode");
				}
				if (accountCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + accountCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + accountCodeType.trim() + "'");
				}
				isAnd = true;

			}
			
			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status  in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CNCL') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  and s.corpId='" + sessionCorpID + "' and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			}
			//query.append("   and s.corpId='"+sessionCorpID+"'  and  s.status not in ('CLSD','CNCL')   group by s.shipNumber");
		}
   
		//String queryExtract=extractManager.saveForExtract(query.toString());
	if(selectCondition.equals("")||selectCondition.equals("#"))
  	  {
  	  }
  	 else
  	  {
  		selectCondition=selectCondition.trim();
  	    if(selectCondition.indexOf("#")==0)
  		 {
  	    	selectCondition=selectCondition.substring(1);
  		 }
  		if(selectCondition.lastIndexOf("#")==selectCondition.length()-1)
  		 {
  			selectCondition=selectCondition.substring(0, selectCondition.length()-1);
  		 }
		
  	  }
	selectCondition=selectCondition.replaceAll("#", ",");
	List selectConditionList=new ArrayList();
	selectConditionList= extractColumnMgmtManager.getSelectCondition(selectCondition,reportName,sessionCorpID);
    String SaveSelectCondition=new String();
    Iterator iterator =selectConditionList.iterator();
    while(iterator.hasNext())
    {
    	SaveSelectCondition =SaveSelectCondition.concat((String)iterator.next().toString());
    	SaveSelectCondition=SaveSelectCondition.concat("#");
    }
    SaveSelectCondition=SaveSelectCondition.trim();
	    if(SaveSelectCondition.indexOf("#")==0)
		 {
	    	SaveSelectCondition=SaveSelectCondition.substring(1);
		 }
		if(SaveSelectCondition.lastIndexOf("#")==SaveSelectCondition.length()-1)
		 {
			SaveSelectCondition=SaveSelectCondition.substring(0, SaveSelectCondition.length()-1);
		 }
		String columnSelect=SaveSelectCondition;
		SaveSelectCondition=SaveSelectCondition.replaceAll("#", ",");
	//System.out.println("\n\n\n\n\n\n\n\n query" + query);
	//System.out.println("\n\n\n\n\n\n\n\n selct query    " +SaveSelectCondition+" query    " + query    ); 
	List activeShipments = serviceOrderManager.dynamicFinancialSummary(SaveSelectCondition, query.toString(),sessionCorpID);
	
	HttpServletResponse response = getResponse();
	response.setContentType("application/vnd.ms-excel");
	ServletOutputStream outputStream = response.getOutputStream();
	File file = new File("DynamicFinancialSummary");
	response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls");
	response.setHeader("Pragma", "public");
	response.setHeader("Cache-Control", "max-age=0");
	//response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" + "\t The number of line is" + activeShipments.size() + "extracted");
	out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + activeShipments.size() + "\t is extracted");
	String bA_SSCW = "";
	String[] conditionArray=columnSelect.split("#");
    int arrayLength = conditionArray.length;
	for(int i=0;i<arrayLength;i++)
	 {	
	   String condition=conditionArray[i]; 
	   String []columnArray=condition.split(" as");
	   //condition= condition.substring(condition.indexOf("as")+2);
	   String column=columnArray[1];
	   outputStream.write((column+"\t").getBytes());
	 } 
	//outputStream.write("URL".getBytes());
	outputStream.write("\r\n".getBytes());
	Iterator it = activeShipments.iterator();

	while(it.hasNext()){
		Object[] row = (Object[]) it.next();

		for(int i=0; i<arrayLength; i++){
			if (row[i] != null) {
				String data = row[i].toString();
				outputStream.write((data + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
		}
		//outputStream.write(("https://www.skyrelo.com/redsky/editTrackingStatus.html?id="+row[arrayLength] + "\t").getBytes());
		outputStream.write("\n".getBytes());
    }
	if(extractFileAudit){
		outputStream.close();
		response.setContentType("application/octet-stream");
		//ServletOutputStream outputStream = response.getOutputStream();
		SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
		StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
		String currentyear=yearDate.toString();
		
		String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
		File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
		SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
		StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
		String currentDateTime=tempDate.toString().trim();
		String fileName="DynamicFinancialSummary_"+currentDateTime+".xls";
		String filePath=uploadDir+""+fileName;
		File file1 = new File(filePath); 
		FileOutputStream outputStream1 = new FileOutputStream(file1);
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + "");
		conditionArray=columnSelect.split("#");
	    arrayLength = conditionArray.length;
		for(int i=0;i<arrayLength;i++)
		 {	
		   String condition=conditionArray[i]; 
		   String []columnArray=condition.split(" as");
		   //condition= condition.substring(condition.indexOf("as")+2);
		   String column=columnArray[1];
		   outputStream1.write((column+"\t").getBytes());
		 } 
		//outputStream.write("URL".getBytes());
		outputStream1.write("\r\n".getBytes());
		Iterator its = activeShipments.iterator();

		while(its.hasNext()){
			Object[] row = (Object[]) its.next();

			for(int i=0; i<arrayLength; i++){
				if (row[i] != null) {
					String data = row[i].toString();
					outputStream1.write((data + "\t").getBytes());
				} else {
					outputStream1.write("\t".getBytes());
				}
			}
			//outputStream.write(("https://www.skyrelo.com/redsky/editTrackingStatus.html?id="+row[arrayLength] + "\t").getBytes());
			outputStream1.write("\n".getBytes());
	    }
		extractedFileLog = new ExtractedFileLog();
		extractedFileLog.setCorpID(sessionCorpID);
		extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
		extractedFileLog.setCreatedOn(new Date());
		extractedFileLog.setLocation(filePath);
		extractedFileLog.setFileName(fileName);
		extractedFileLog.setModule("Data Extract");
		extractedFileLog=extractedFileLogManager.save(extractedFileLog);
		}
	}		
	}
	
	public void activeShip() throws Exception { 
		if(extractVal == null || (!extractVal.equals("true")) ){
			company=companyManager.findByCorpID(sessionCorpID).get(0);
		    if(company!=null){
		      if(company.getExtractedFileAudit() !=null && company.getExtractedFileAudit()){
		    	  extractFileAudit = true;
		  		}
		    }
		StringBuffer jobBuffer = new StringBuffer("");
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		}
		
		
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		StringBuffer beginDates = new StringBuffer();
		StringBuffer endDates = new StringBuffer();
		StringBuffer deliveryTBeginDates = new StringBuffer();
		StringBuffer deliveryTEndDates = new StringBuffer();
		StringBuffer deliveryActBeginDates = new StringBuffer();
		StringBuffer deliveryActEndDates = new StringBuffer();
		StringBuffer loadTgtBeginDates = new StringBuffer();
		StringBuffer loadTgtEndDates = new StringBuffer();
		StringBuffer invPostingBeginDates = new StringBuffer();
		StringBuffer invPostingEndDates = new StringBuffer();
		StringBuffer revenueRecognitionBeginDates = new StringBuffer();
		StringBuffer revenueRecognitionEndDates = new StringBuffer();
		StringBuffer invoicingBeginDates = new StringBuffer();
		StringBuffer invoicingEndDates = new StringBuffer();
		StringBuffer storageBeginDates = new StringBuffer();
		StringBuffer storageEndDates = new StringBuffer();
		StringBuffer createdDates = new StringBuffer();
		StringBuffer toCreatedDates = new StringBuffer();
		StringBuffer payPostingBeginDates = new StringBuffer();
		StringBuffer payPostingEndDates = new StringBuffer();
		StringBuffer receivedBeginDates = new StringBuffer();
		StringBuffer receivedEndDates = new StringBuffer();
		StringBuffer postingPeriodBeginDates = new StringBuffer();
		StringBuffer postingPeriodEndDates = new StringBuffer();
		StringBuffer bookingDates = new StringBuffer();
		StringBuffer toBookingDates = new StringBuffer();
		StringBuffer updateDates = new StringBuffer();
		StringBuffer toUpdateDates = new StringBuffer();
		StringBuffer query = new StringBuffer();
		boolean isAnd = false;
		if(!revenueRecognitionPeriod.equals("")){
        	String  returnValue=   findPeriodDate(revenueRecognitionPeriod);
        	String [] returnValueArray=returnValue.split("#");
        	revenueRecognitionBeginDates = new StringBuffer(returnValueArray[0]);
        	revenueRecognitionEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate == null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer();
		} else if (revenueRecognitionBeginDate == null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer();
		}
        }
		if(!loadActualDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(loadActualDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	beginDates = new StringBuffer(returnValueArray[0]);
        	endDates = new StringBuffer(returnValueArray[1]);
        }else{
		if (beginDate != null && endDate != null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer(formats.format(endDate));

		} else if (beginDate != null && endDate == null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer();
		} else if (beginDate == null && endDate != null) {
			beginDates = new StringBuffer();
			endDates = new StringBuffer(formats.format(endDate));
		} else {
			beginDates = new StringBuffer();
			endDates = new StringBuffer();

		}
        }
		if(!createDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(createDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	createdDates = new StringBuffer(returnValueArray[0]);
        	toCreatedDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (createDate != null && toCreateDate != null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else if (createDate != null && toCreateDate == null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer();
		} else if (createDate == null && toCreateDate != null) {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer();
		}
        }
		if(!updateDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(updateDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	updateDates  = new StringBuffer(returnValueArray[0]);
        	toUpdateDates  = new StringBuffer(returnValueArray[1]);
        }else{
        	if (updateDate != null && toUpdateDate != null) {
				updateDates = new StringBuffer(formats.format(updateDate));
				toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
			} else if (updateDate != null && toUpdateDate == null) {
				updateDates = new StringBuffer(formats.format(updateDate));
				toUpdateDates = new StringBuffer();
			} else if (updateDate == null && toUpdateDate != null) {
				updateDates = new StringBuffer();
				toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
			} else {
				updateDates = new StringBuffer();
				toUpdateDates = new StringBuffer();
			}
        }
		if(!deliveryTargetDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(deliveryTargetDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	deliveryTBeginDates = new StringBuffer(returnValueArray[0]);
        	deliveryTEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (deliveryTBeginDate != null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer();
		} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer();
		}
        }
		if(!deliveryActualDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(deliveryActualDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	deliveryActBeginDates = new StringBuffer(returnValueArray[0]);
        	deliveryActEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (deliveryActBeginDate != null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer();
		} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer();
		}
        }
        if(!loadTargetDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(loadTargetDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	loadTgtBeginDates = new StringBuffer(returnValueArray[0]);
        	loadTgtEndDates = new StringBuffer(returnValueArray[1]);
        }else{
		if (loadTgtBeginDate != null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer();
		} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer();
		}
        }
        if(!invoicePostingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(invoicePostingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	invPostingBeginDates = new StringBuffer(returnValueArray[0]);
        	invPostingEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (invPostingBeginDate != null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else if (invPostingBeginDate != null && invPostingEndDate == null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer();
		} else if (invPostingBeginDate == null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer();
		}
        }
        if(!InvoicingdatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(InvoicingdatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	invoicingBeginDates = new StringBuffer(returnValueArray[0]);
        	invoicingEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (invoicingBeginDate != null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else if (invoicingBeginDate != null && invoicingEndDate == null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer();
		} else if (invoicingBeginDate == null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer();
		}
        }
        if(!storageDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(storageDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	storageEndDates = new StringBuffer(returnValueArray[0]);
        	storageEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (storageBeginDate != null && storageEndDate != null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else if (storageBeginDate != null && storageEndDate == null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer();
		} else if (storageBeginDate == null && storageEndDate != null) {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer();
		}
        }
		if(!payablePostingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(payablePostingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	payPostingBeginDates = new StringBuffer(returnValueArray[0]);
        	payPostingEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (payPostingBeginDate != null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else if (payPostingBeginDate != null && payPostingEndDate == null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer();
		} else if (payPostingBeginDate == null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer();
		}
        }
		if(!receivedDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(receivedDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	receivedBeginDates = new StringBuffer(returnValueArray[0]);
        	receivedEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (receivedBeginDate != null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else if (receivedBeginDate != null && receivedEndDate == null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer();
		} else if (receivedBeginDate == null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer();
		}
        }
		if(!PostingPeriodDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(PostingPeriodDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	postingPeriodBeginDates = new StringBuffer(returnValueArray[0]);
        	postingPeriodEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (postingPeriodBeginDate != null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else if (postingPeriodBeginDate != null && postingPeriodEndDate == null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer();
		 } else if (postingPeriodBeginDate == null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer();
		 } 
        }
		if(!bookingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(bookingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	bookingDates = new StringBuffer(returnValueArray[0]);
        	toBookingDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (bookingDate != null && toBookingDate != null) {
			bookingDates = new StringBuffer(formats.format(bookingDate));
			toBookingDates = new StringBuffer(formats.format(toBookingDate));
		} else if (bookingDate != null && toBookingDate == null) {
			bookingDates = new StringBuffer(formats.format(bookingDate));
			toBookingDates = new StringBuffer();
		} else if (bookingDate == null && toBookingDate != null) {
			bookingDates = new StringBuffer();
			toBookingDates = new StringBuffer(formats.format(toBookingDate));
		} else {
			bookingDates = new StringBuffer();
			toBookingDates = new StringBuffer();
		}
        }
		if (jobTypesCondition.equals("") && billToCodeCondition.equals("") && originAgentCondition.equals("")&& destinationAgentCondition.equals("") && bookingCodeCondition.equals("") && routingCondition.equals("") && modeCondition.equals("") && packModeCondition.equals("")
				&& commodityCondition.equals("") && coordinatorCondition.equals("") && consultantCondition.equals("") && salesmanCondition.equals("") && oACountryCondition.equals("") && dACountryCondition.equals("")
				&& actualWgtCondition.equals("") && companyCodeCondition.equals("") && beginDates.toString().equals("") && endDates.toString().equals("") && actualVolumeCondition.equals("")
				&& createdDates.toString().equals("") && toCreatedDates.toString().equals("") && updateDates.toString().equals("") && toUpdateDates.toString().equals("") && deliveryTBeginDates.toString().equals("") && deliveryTEndDates.toString().equals("") && deliveryActBeginDates.toString().equals("")
				&& deliveryActEndDates.toString().equals("") && loadTgtBeginDates.toString().equals("") && loadTgtEndDates.toString().equals("") && invPostingBeginDates.toString().equals("") && invPostingEndDates.toString().equals("")
				&& invoicingBeginDates.toString().equals("") && invoicingEndDates.toString().equals("") && storageBeginDates.toString().equals("") && storageEndDates.toString().equals("") && revenueRecognitionBeginDates.toString().equals("") && revenueRecognitionEndDates.toString().equals("")&& payPostingBeginDates.toString().equals("") && payPostingEndDates.toString().equals("")&& receivedBeginDates.toString().equals("") && receivedEndDates.toString().equals("")&& postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals("")
				&& bookingDates.toString().equals("") && toBookingDates.toString().equals("") && carrierCondition.equals("") && driverIdCondition.equals("") && accountCodeCondition.equals("")) {

			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status not in ('CNCL') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  where  s.corpId='" + sessionCorpID + "' and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber ");
			}
			//query.append(" where   s.corpId='"+sessionCorpID+"'  and  s.status not in ('CLSD','CNCL') group by s.shipNumber");
		} else {

			query.append("  where  ");

			if (jobTypesCondition.equals("") || jobTypesCondition == null) {

			} else {
				query.append(" s.job ");
				isAnd = true;
				if (jobTypesCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append(" in  ");
					query.append("(" + jobtypeMultiple + ")");
				} else {
					query.append(" not in  ");
					query.append("(" + jobtypeMultiple + ")");
				}

			}
			if (billToCodeCondition.equals("") || billToCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.billToCode");
				} else {
					query.append("  s.billToCode");
				}
				if (billToCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + billToCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + billToCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (originAgentCondition.equals("") || originAgentCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and t.originAgentCode");
				} else {
					query.append("  t.originAgentCode");
				}
				if (originAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + originAgentCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + originAgentCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (destinationAgentCondition.equals("") || destinationAgentCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and t.destinationAgentCode");
				} else {
					query.append("  t.destinationAgentCode");
				}
				if (destinationAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + destinationAgentCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + destinationAgentCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (bookingCodeCondition.equals("") || bookingCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.bookingAgentCode");
				} else {
					query.append("  s.bookingAgentCode");
				}
				if (bookingCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + bookingCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + bookingCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (routingCondition.equals("") || routingCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.routing");
				} else {
					query.append("   s.routing");
				}
				if (routingCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + routingTypes + "'");
				} else {
					query.append("<>");
					query.append("'" + routingTypes + "'");
				}
				isAnd = true;
			}
			if (modeCondition.equals("") || modeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.mode");
				} else {
					query.append("  s.mode");
				}
				if (modeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + modeType + "'");
				} else {
					query.append("<>");
					query.append("'" + modeType + "'");
				}
				isAnd = true;
			}
			if (packModeCondition.equals("") || packModeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.packingMode");
				} else {
					query.append("   s.packingMode");
				}
				if (packModeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + packModeType + "'");
				} else {
					query.append("<>");
					query.append("'" + packModeType + "'");
				}
				isAnd = true;
			}
			if (commodityCondition.equals("") || commodityCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.commodity");
				} else {
					query.append("  s.commodity");
				}
				if (commodityCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + commodityType + "'");
				} else {
					query.append("<>");
					query.append("'" + commodityType + "'");
				}
				isAnd = true;
			}
			if (coordinatorCondition.equals("") || coordinatorCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.coordinator");
				} else {
					query.append("   s.coordinator");
				}
				if (coordinatorCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + coordinatorType + "'");
				} else {
					query.append("<>");
					query.append("'" + coordinatorType + "'");
				}
				isAnd = true;
			}
			if (consultantCondition.equals("") || consultantCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.estimator");
				} else {
					query.append("   s.estimator");
				}
				if (consultantCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + consultantType + "'");
				} else {
					query.append("<>");
					query.append("'" + consultantType + "'");
				}
				isAnd = true;
			}
			
			if (salesmanCondition.equals("") || salesmanCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.salesMan");
				} else {
					query.append("   s.salesMan");
				}
				if (salesmanCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + salesmanType + "'");
				} else {
					query.append("<>");
					query.append("'" + salesmanType + "'");
				}
				isAnd = true;
			}
			if (oACountryCondition.equals("") || oACountryCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.originCountryCode");
				} else {
					query.append("   s.originCountryCode");
				}
				if (oACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + oACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + oACountryType + "'");
				}
				isAnd = true;
			}
			if (dACountryCondition.equals("") || dACountryCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.destinationCountryCode");
				} else {
					query.append("  s.destinationCountryCode");
				}
				if (dACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + dACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + dACountryType + "'");
				}
				isAnd = true;
			} 
			if (actualWgtCondition.equals("") || actualWgtCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualNetWeight");
				} else {
					query.append("  m.actualNetWeight");
				}

				if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualWgtType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualWgtType + "'");
				}
				isAnd = true;
			}
			if (actualVolumeCondition.equals("") || actualVolumeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualCubicFeet");
				} else {
					query.append("  m.actualCubicFeet");
				}
				if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualVolumeType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualVolumeType + "'");
				}
				isAnd = true;
			}
			if (beginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + beginDates + "')");
				} else if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + beginDates + "')");
				} else {

				}
				isAnd = true;
			}

			if ( createdDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + createdDates + "')");
				} else if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + createdDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toCreatedDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toCreatedDates + "')");
				} else if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toCreatedDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( updateDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				}
				if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + updateDates + "')");
				} else if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + updateDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toUpdateDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("   and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				}
				if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toUpdateDates + "')");
				} else if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toUpdateDates + "')");
				} else {

				}
				isAnd = true;
			}

			if (endDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + endDates + "')");
				} else if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + endDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (deliveryTBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTBeginDates + "')");
				} else if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTEndDates + "')");
				} else if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActBeginDates + "')");
				} else if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActEndDates + "')");
				} else if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtBeginDates + "')");
				} else if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtEndDates + "')");
				} else if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (invPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingBeginDates + "')");
				} else if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (invPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingEndDates + "')");
				} else if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (payPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingBeginDates + "')");
				} else if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (payPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingEndDates + "')");
				} else if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ( receivedBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedBeginDates + "')");
				} else if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if (receivedEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedEndDates + "')");
				} else if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			if ( (postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals(""))) {

			} else 
			{
				if(((!( postingPeriodBeginDates.toString().equals(""))))&&!(postingPeriodEndDates.toString().equals(""))) {
				if (isAnd == true) {
					query.append("  and ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append("  ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");

                  
				  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
                   query.append("  or ( (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				   if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}

                 query.append("  and (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "') ) ) )");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "') ) ) ) ");
				} else {
					
				}

				isAnd = true;
			} 
				else if((!( postingPeriodBeginDates.toString().equals("")))&& ((postingPeriodEndDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			else  if((!( postingPeriodEndDates.toString().equals("")))&& ((postingPeriodBeginDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			}
			 
			if (invoicingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingBeginDates + "')");
				} else if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (invoicingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingEndDates + "')");
				} else if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			
			
			if (storageBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				}

				if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageBeginDates + "')");
				} else if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageBeginDates + "')");
				} else { 
				}
				isAnd = true;
			}

			if (storageEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				}

				if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageEndDates + "')");
				} else if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageEndDates + "')");
				} else { 
				}
				isAnd = true;
			}
			
			
			if (revenueRecognitionBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else { 
				}
				isAnd = true;
			}

			if (revenueRecognitionEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			

			if (companyCodeCondition == null || companyCodeCondition.equals("")) {

			} else {
				if (isAnd == true) {
					query.append(" and s.companyDivision");
				} else {
					query.append("  s.companyDivision");
				}
				if (companyCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + companyCode + "'");
				} else {
					query.append("<>");
					query.append("'" + companyCode + "'");
				}
				isAnd = true;
			}
			if ( bookingDates.toString().equals("")) {
			} else {
				if (isAnd == true) {
					query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				} else {
					query.append(" (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				}
				if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + bookingDates + "')");
				} else if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + bookingDates + "')");
				} else {
				}
				isAnd = true;
			}
			if ( toBookingDates.toString().equals("")) {
			} else {
				if (isAnd == true) {
					query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				} else {
					query.append(" (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				}
				if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toBookingDates + "')");
				} else if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toBookingDates + "')");
				} else {
				}
				isAnd = true;
			}
			
			if (carrierCondition.equals("") || carrierCondition == null) {
			} else {
				if (isAnd == true) {
					query.append(" and m.carrier");
				} else {
					query.append("  m.carrier");
				}
				if (carrierCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + carrier.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + carrier.trim() + "'");
				}
				isAnd = true;
			}
			if (driverIdCondition.equals("") || driverIdCondition == null) {
			} else {
				if (isAnd == true) {
					query.append(" and m.driverId");
				} else {
					query.append("  m.driverId");
				}
				if (driverIdCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + driverId.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + driverId.trim() + "'");
				}
				isAnd = true;
			}
			
			if (accountCodeCondition.equals("") || accountCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and c.accountCode");
				} else {
					query.append("   c.accountCode");
				}
				if (accountCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + accountCodeType + "'");
				} else {
					query.append("<>");
					query.append("'" + accountCodeType + "'");
				}
				isAnd = true;
			}
			
			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status  in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CNCL') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  and s.corpId='" + sessionCorpID + "' and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			}
			//query.append("   and s.corpId='"+sessionCorpID+"'  and  s.status not in ('CLSD','CNCL')   group by s.shipNumber");
		}
   
		//String queryExtract=extractManager.saveForExtract(query.toString());
	if(selectCondition.equals("")||selectCondition.equals("#"))
  	  {
  	  }
  	 else
  	  {
  		selectCondition=selectCondition.trim();
  	    if(selectCondition.indexOf("#")==0)
  		 {
  	    	selectCondition=selectCondition.substring(1);
  		 }
  		if(selectCondition.lastIndexOf("#")==selectCondition.length()-1)
  		 {
  			selectCondition=selectCondition.substring(0, selectCondition.length()-1);
  		 }
		
  	  }
	selectCondition=selectCondition.replaceAll("#", ",");
	List selectConditionList=new ArrayList();
	selectConditionList= extractColumnMgmtManager.getSelectCondition(selectCondition,reportName,sessionCorpID);
    String SaveSelectCondition=new String();
    Iterator iterator =selectConditionList.iterator();
    while(iterator.hasNext())
    {
    	SaveSelectCondition =SaveSelectCondition.concat((String)iterator.next().toString());
    	SaveSelectCondition=SaveSelectCondition.concat("#");
    }
    SaveSelectCondition=SaveSelectCondition.trim();
	    if(SaveSelectCondition.indexOf("#")==0)
		 {
	    	SaveSelectCondition=SaveSelectCondition.substring(1);
		 }
		if(SaveSelectCondition.lastIndexOf("#")==SaveSelectCondition.length()-1)
		 {
			SaveSelectCondition=SaveSelectCondition.substring(0, SaveSelectCondition.length()-1);
		 }
	String columnSelect=SaveSelectCondition;
	SaveSelectCondition=SaveSelectCondition.replaceAll("#", ",");
	//System.out.println("\n\n\n\n\n\n\n\n query" + query);
	//System.out.println("\n\n\n\n\n\n\n\n selct query    " +SaveSelectCondition+" query    " + query    );
	//List activeShipments=new ArrayList();
	List activeShipments = serviceOrderManager.dynamicActiveShipments(SaveSelectCondition, query.toString(),sessionCorpID);
	
	HttpServletResponse response = getResponse();
	response.setContentType("application/vnd.ms-excel");
	ServletOutputStream outputStream = response.getOutputStream();
	File file = new File("ActiveShipments");
	response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls");
	response.setHeader("Pragma", "public");
	response.setHeader("Cache-Control", "max-age=0");
	//response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" + "\t The number of line is" + activeShipments.size() + "extracted");
	out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + activeShipments.size() + "\t is extracted");
	String bA_SSCW = "";
	String[] conditionArray=columnSelect.split("#");
    int arrayLength = conditionArray.length;
	for(int i=0; i<arrayLength; i++)
	 {	
	   String condition=conditionArray[i]; 
	   String []columnArray=condition.split(" as");
	   //condition= condition.substring(condition.indexOf("as")+2);
	   String column=columnArray[1];
	   outputStream.write((column+"\t").getBytes());
	 } 
	//outputStream.write("URL".getBytes());
	outputStream.write("\r\n".getBytes());
	Iterator it = activeShipments.iterator();

	while(it.hasNext()){
		Object[] row = (Object[]) it.next();

		for(int i=0; i<arrayLength; i++){
			if (row[i] != null) {
				String data = row[i].toString(); 
				outputStream.write((data + "\t").getBytes());
			} else {
				outputStream.write("\t".getBytes());
			}
		}
		//outputStream.write(("https://www.skyrelo.com/redsky/editTrackingStatus.html?id="+row[arrayLength] + "\t").getBytes());
		outputStream.write("\n".getBytes());
    }
	if(extractFileAudit){
		outputStream.close();
		response.setContentType("application/octet-stream");
		//ServletOutputStream outputStream = response.getOutputStream();
		SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
		StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
		String currentyear=yearDate.toString();
		
		String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
		File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
		SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
		StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
		String currentDateTime=tempDate.toString().trim();
		String fileName="ActiveShipments_"+currentDateTime+".xls";
		String filePath=uploadDir+""+fileName;
		File file1 = new File(filePath); 
		FileOutputStream outputStream1 = new FileOutputStream(file1);
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + "");
		conditionArray=columnSelect.split("#");
	    arrayLength = conditionArray.length;
		for(int i=0;i<arrayLength;i++)
		 {	
		   String condition=conditionArray[i]; 
		   String []columnArray=condition.split(" as");
		   //condition= condition.substring(condition.indexOf("as")+2);
		   String column=columnArray[1];
		   outputStream1.write((column+"\t").getBytes());
		 } 
		//outputStream.write("URL".getBytes());
		outputStream1.write("\r\n".getBytes());
		Iterator its = activeShipments.iterator();

		while(its.hasNext()){
			Object[] row = (Object[]) its.next();

			for(int i=0; i<arrayLength; i++){
				if (row[i] != null) {
					String data = row[i].toString();
					outputStream1.write((data + "\t").getBytes());
				} else {
					outputStream1.write("\t".getBytes());
				}
			}
			//outputStream.write(("https://www.skyrelo.com/redsky/editTrackingStatus.html?id="+row[arrayLength] + "\t").getBytes());
			outputStream1.write("\n".getBytes());
	    }
		extractedFileLog = new ExtractedFileLog();
		extractedFileLog.setCorpID(sessionCorpID);
		extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
		extractedFileLog.setCreatedOn(new Date());
		extractedFileLog.setLocation(filePath);
		extractedFileLog.setFileName(fileName);
		extractedFileLog.setModule("Data Extract");
		extractedFileLog=extractedFileLogManager.save(extractedFileLog);
		}
	}
		
	}
	
	
	public void dynamicDataReloAnalysis() throws Exception{
		if(extractVal == null || (!extractVal.equals("true")) ){
			 company=companyManager.findByCorpID(sessionCorpID).get(0);
			    if(company!=null){
			      if(company.getExtractedFileAudit() !=null && company.getExtractedFileAudit()){
			    	  extractFileAudit = true;
			  		}
			    }	
		StringBuffer jobBuffer = new StringBuffer("");
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		}
		
		if(jobtypeMultiple!=null && jobtypeMultiple.contains("RLO")){
		jobtypeMultiple="'RLO'";
		}else{
			jobtypeMultiple = "";
		}
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		//StringBuffer beginDates = new StringBuffer();
		//StringBuffer endDates = new StringBuffer();
		//StringBuffer deliveryTBeginDates = new StringBuffer();
		//StringBuffer deliveryTEndDates = new StringBuffer();
		//StringBuffer deliveryActBeginDates = new StringBuffer();
		//StringBuffer deliveryActEndDates = new StringBuffer();
		//StringBuffer loadTgtBeginDates = new StringBuffer();
		//StringBuffer loadTgtEndDates = new StringBuffer();
		StringBuffer invPostingBeginDates = new StringBuffer();
		StringBuffer invPostingEndDates = new StringBuffer();
		StringBuffer revenueRecognitionBeginDates = new StringBuffer();
		StringBuffer revenueRecognitionEndDates = new StringBuffer();
		StringBuffer invoicingBeginDates = new StringBuffer();
		StringBuffer invoicingEndDates = new StringBuffer();
		StringBuffer storageBeginDates = new StringBuffer();
		StringBuffer storageEndDates = new StringBuffer();
		StringBuffer createdDates = new StringBuffer();
		StringBuffer toCreatedDates = new StringBuffer();
		StringBuffer payPostingBeginDates = new StringBuffer();
		StringBuffer payPostingEndDates = new StringBuffer();
		StringBuffer receivedBeginDates = new StringBuffer();
		StringBuffer receivedEndDates = new StringBuffer();
		StringBuffer postingPeriodBeginDates = new StringBuffer();
		StringBuffer postingPeriodEndDates = new StringBuffer();
		StringBuffer bookingDates = new StringBuffer();
		StringBuffer toBookingDates = new StringBuffer();
		StringBuffer updateDates = new StringBuffer();
		StringBuffer toUpdateDates = new StringBuffer();
		StringBuffer query = new StringBuffer();
		boolean isAnd = false;
		if(!revenueRecognitionPeriod.equals("")){
        	String  returnValue=   findPeriodDate(revenueRecognitionPeriod);
        	String [] returnValueArray=returnValue.split("#");
        	revenueRecognitionBeginDates = new StringBuffer(returnValueArray[0]);
        	revenueRecognitionEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate == null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer();
		} else if (revenueRecognitionBeginDate == null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer();
		}
        }
		/*if(!loadActualDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(loadActualDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	beginDates = new StringBuffer(returnValueArray[0]);
        	endDates = new StringBuffer(returnValueArray[1]);
        }else{
		if (beginDate != null && endDate != null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer(formats.format(endDate));

		} else if (beginDate != null && endDate == null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer();
		} else if (beginDate == null && endDate != null) {
			beginDates = new StringBuffer();
			endDates = new StringBuffer(formats.format(endDate));
		} else {
			beginDates = new StringBuffer();
			endDates = new StringBuffer();

		}
        }*/
		if(!createDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(createDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	createdDates = new StringBuffer(returnValueArray[0]);
        	toCreatedDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (createDate != null && toCreateDate != null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else if (createDate != null && toCreateDate == null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer();
		} else if (createDate == null && toCreateDate != null) {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer();
		}
        }
		if(!updateDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(updateDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	updateDates  = new StringBuffer(returnValueArray[0]);
        	toUpdateDates  = new StringBuffer(returnValueArray[1]);
        }else{
        	if (updateDate != null && toUpdateDate != null) {
				updateDates = new StringBuffer(formats.format(updateDate));
				toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
			} else if (updateDate != null && toUpdateDate == null) {
				updateDates = new StringBuffer(formats.format(updateDate));
				toUpdateDates = new StringBuffer();
			} else if (updateDate == null && toUpdateDate != null) {
				updateDates = new StringBuffer();
				toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
			} else {
				updateDates = new StringBuffer();
				toUpdateDates = new StringBuffer();
			}
        }

		/*if(!deliveryTargetDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(deliveryTargetDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	deliveryTBeginDates = new StringBuffer(returnValueArray[0]);
        	deliveryTEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (deliveryTBeginDate != null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer();
		} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer();
		}
        }*/
		/*if(!deliveryActualDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(deliveryActualDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	deliveryActBeginDates = new StringBuffer(returnValueArray[0]);
        	deliveryActEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (deliveryActBeginDate != null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer();
		} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer();
		}
        }*/
       /* if(!loadTargetDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(loadTargetDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	loadTgtBeginDates = new StringBuffer(returnValueArray[0]);
        	loadTgtEndDates = new StringBuffer(returnValueArray[1]);
        }else{
		if (loadTgtBeginDate != null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer();
		} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer();
		}
        }*/
        if(!invoicePostingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(invoicePostingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	invPostingBeginDates = new StringBuffer(returnValueArray[0]);
        	invPostingEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (invPostingBeginDate != null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else if (invPostingBeginDate != null && invPostingEndDate == null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer();
		} else if (invPostingBeginDate == null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer();
		}
        }
        if(!InvoicingdatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(InvoicingdatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	invoicingBeginDates = new StringBuffer(returnValueArray[0]);
        	invoicingEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (invoicingBeginDate != null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else if (invoicingBeginDate != null && invoicingEndDate == null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer();
		} else if (invoicingBeginDate == null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer();
		}
        }
        if(!storageDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(storageDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	storageEndDates = new StringBuffer(returnValueArray[0]);
        	storageEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (storageBeginDate != null && storageEndDate != null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else if (storageBeginDate != null && storageEndDate == null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer();
		} else if (storageBeginDate == null && storageEndDate != null) {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer();
		}
        }
		if(!payablePostingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(payablePostingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	payPostingBeginDates = new StringBuffer(returnValueArray[0]);
        	payPostingEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (payPostingBeginDate != null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else if (payPostingBeginDate != null && payPostingEndDate == null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer();
		} else if (payPostingBeginDate == null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer();
		}
        }
		if(!receivedDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(receivedDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	receivedBeginDates = new StringBuffer(returnValueArray[0]);
        	receivedEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (receivedBeginDate != null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else if (receivedBeginDate != null && receivedEndDate == null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer();
		} else if (receivedBeginDate == null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer();
		}
        }
		if(!PostingPeriodDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(PostingPeriodDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	postingPeriodBeginDates = new StringBuffer(returnValueArray[0]);
        	postingPeriodEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (postingPeriodBeginDate != null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else if (postingPeriodBeginDate != null && postingPeriodEndDate == null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer();
		 } else if (postingPeriodBeginDate == null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer();
		 } 
        }
		if(!bookingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(bookingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	bookingDates = new StringBuffer(returnValueArray[0]);
        	toBookingDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (bookingDate != null && toBookingDate != null) {
			bookingDates = new StringBuffer(formats.format(bookingDate));
			toBookingDates = new StringBuffer(formats.format(toBookingDate));
		} else if (bookingDate != null && toBookingDate == null) {
			bookingDates = new StringBuffer(formats.format(bookingDate));
			toBookingDates = new StringBuffer();
		} else if (bookingDate == null && toBookingDate != null) {
			bookingDates = new StringBuffer();
			toBookingDates = new StringBuffer(formats.format(toBookingDate));
		} else {
			bookingDates = new StringBuffer();
			toBookingDates = new StringBuffer();
		}
        }
		if (jobTypesCondition.equals("") && billToCodeCondition.equals("")&& originAgentCondition.equals("")&& destinationAgentCondition.equals("") && bookingCodeCondition.equals("") && routingCondition.equals("") && modeCondition.equals("") && packModeCondition.equals("")
				&& commodityCondition.equals("") && coordinatorCondition.equals("") && consultantCondition.equals("") && salesmanCondition.equals("") && oACountryCondition.equals("") && dACountryCondition.equals("")
				&& actualWgtCondition.equals("") && companyCodeCondition.equals("")  && actualVolumeCondition.equals("")
				&& createdDates.toString().equals("") && toCreatedDates.toString().equals("")
				&& updateDates.toString().equals("") && toUpdateDates.toString().equals("") 
				&& invPostingBeginDates.toString().equals("") && invPostingEndDates.toString().equals("")
				&& invoicingBeginDates.toString().equals("") && invoicingEndDates.toString().equals("") && storageBeginDates.toString().equals("") && storageEndDates.toString().equals("") && revenueRecognitionBeginDates.toString().equals("") && revenueRecognitionEndDates.toString().equals("")&& payPostingBeginDates.toString().equals("") && payPostingEndDates.toString().equals("")&& receivedBeginDates.toString().equals("") && receivedEndDates.toString().equals("")&& postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals("")
				&& bookingDates.toString().equals("") && toBookingDates.toString().equals("") && carrierCondition.equals("") && driverIdCondition.equals("") && accountCodeCondition.equals("")) {

			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status not in ('CNCL') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  where  s.corpId='" + sessionCorpID + "' and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber ");
			}
			//query.append(" where   s.corpId='"+sessionCorpID+"'  and  s.status not in ('CLSD','CNCL') group by s.shipNumber");
		} else {

			query.append(" where  ");

			if (jobTypesCondition.equals("") || jobTypesCondition == null) {

			} else {
				if(jobtypeMultiple!=null && (!(jobtypeMultiple.trim().equals("")))){
				query.append(" s.job ");
				isAnd = true;
				if (jobTypesCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append(" in  ");
					query.append("(" + jobtypeMultiple + ")");
				} else {
					query.append(" not in  ");
					query.append("(" + jobtypeMultiple + ")");
				}
				}
			}
			if (billToCodeCondition.equals("") || billToCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.billToCode");
				} else {
					query.append("  s.billToCode");
				}
				if (billToCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + billToCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + billToCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (originAgentCondition.equals("") || originAgentCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and t.originAgentCode");
				} else {
					query.append("  t.originAgentCode");
				}
				if (originAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + originAgentCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + originAgentCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (destinationAgentCondition.equals("") |destinationAgentCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and t.destinationAgentCode");
				} else {
					query.append("  t.destinationAgentCode");
				}
				if (destinationAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + destinationAgentCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + destinationAgentCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (bookingCodeCondition.equals("") || bookingCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.bookingAgentCode");
				} else {
					query.append("  s.bookingAgentCode");
				}
				if (bookingCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + bookingCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + bookingCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (routingCondition.equals("") || routingCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.routing");
				} else {
					query.append("   s.routing");
				}
				if (routingCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + routingTypes + "'");
				} else {
					query.append("<>");
					query.append("'" + routingTypes + "'");
				}
				isAnd = true;
			}
			if (modeCondition.equals("") || modeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.mode");
				} else {
					query.append("  s.mode");
				}
				if (modeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + modeType + "'");
				} else {
					query.append("<>");
					query.append("'" + modeType + "'");
				}
				isAnd = true;
			}
			if (packModeCondition.equals("") || packModeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.packingMode");
				} else {
					query.append("   s.packingMode");
				}
				if (packModeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + packModeType + "'");
				} else {
					query.append("<>");
					query.append("'" + packModeType + "'");
				}
				isAnd = true;
			}
			if (commodityCondition.equals("") || commodityCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.commodity");
				} else {
					query.append("  s.commodity");
				}
				if (commodityCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + commodityType + "'");
				} else {
					query.append("<>");
					query.append("'" + commodityType + "'");
				}
				isAnd = true;
			}
			if (coordinatorCondition.equals("") || coordinatorCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.coordinator");
				} else {
					query.append("   s.coordinator");
				}
				if (coordinatorCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + coordinatorType + "'");
				} else {
					query.append("<>");
					query.append("'" + coordinatorType + "'");
				}
				isAnd = true;
			}
			if (consultantCondition.equals("") || consultantCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.estimator");
				} else {
					query.append("   s.estimator");
				}
				if (consultantCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + consultantType + "'");
				} else {
					query.append("<>");
					query.append("'" + consultantType + "'");
				}
				isAnd = true;
			}
			
			if (salesmanCondition.equals("") || salesmanCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.salesMan");
				} else {
					query.append("   s.salesMan");
				}
				if (salesmanCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + salesmanType + "'");
				} else {
					query.append("<>");
					query.append("'" + salesmanType + "'");
				}
				isAnd = true;
			}
			if (oACountryCondition.equals("") || oACountryCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.originCountryCode");
				} else {
					query.append("   s.originCountryCode");
				}
				if (oACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + oACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + oACountryType + "'");
				}
				isAnd = true;
			}
			if (dACountryCondition.equals("") || dACountryCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.destinationCountryCode");
				} else {
					query.append("  s.destinationCountryCode");
				}
				if (dACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + dACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + dACountryType + "'");
				}
				isAnd = true;
			} 
			if (actualWgtCondition.equals("") || actualWgtCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualNetWeight");
				} else {
					query.append("  m.actualNetWeight");
				}

				if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualWgtType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualWgtType + "'");
				}
				isAnd = true;
			}
			if (actualVolumeCondition.equals("") || actualVolumeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualCubicFeet");
				} else {
					query.append("  m.actualCubicFeet");
				}
				if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualVolumeType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualVolumeType + "'");
				}
				isAnd = true;
			}
			/*if (beginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					//query.append(">=");
					//query.append("'" + beginDates + "')");
				} else if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					//query.append("'" + beginDates + "')");
				} else {

				}
				isAnd = true;
			}*/

			if ( createdDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + createdDates + "')");
				} else if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + createdDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toCreatedDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toCreatedDates + "')");
				} else if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toCreatedDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( updateDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				}
				if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + updateDates + "')");
				} else if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + updateDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toUpdateDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("   and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				}
				if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toUpdateDates + "')");
				} else if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toUpdateDates + "')");
				} else {

				}
				isAnd = true;
			}

			/*if (endDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + endDates + "')");
				} else if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + endDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}*/

			/*if (deliveryTBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTBeginDates + "')");
				} else if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}*/
			
			/*if (deliveryTEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTEndDates + "')");
				} else if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}*/
			/*if (deliveryActBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActBeginDates + "')");
				} else if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}*/
			/*if (deliveryActEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActEndDates + "')");
				} else if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}*/
			/*if (loadTgtBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtBeginDates + "')");
				} else if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}*/
			/*if (loadTgtEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtEndDates + "')");
				} else if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}*/
			/*if (invPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingBeginDates + "')");
				} else if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}*/
			/*if (invPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingEndDates + "')");
				} else if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}*/
			/*if (payPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingBeginDates + "')");
				} else if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}*/
			/*if (payPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingEndDates + "')");
				} else if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}*/
			/*if ( receivedBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedBeginDates + "')");
				} else if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}*/
			/*if (receivedEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedEndDates + "')");
				} else if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}*/
			/*	if ( (postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals(""))) {

			} else 
			{
				if(((!( postingPeriodBeginDates.toString().equals(""))))&&!(postingPeriodEndDates.toString().equals(""))) {
				if (isAnd == true) {
					query.append("  and ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append("  ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");

                  
				  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
                   query.append("  or ( (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				   if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}

                 query.append("  and (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "') ) ) )");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "') ) ) ) ");
				} else {
					
				}

				isAnd = true;
			} 
				else if((!( postingPeriodBeginDates.toString().equals("")))&& ((postingPeriodEndDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			else  if((!( postingPeriodEndDates.toString().equals("")))&& ((postingPeriodBeginDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			}*/
			 
			/*if (invoicingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingBeginDates + "')");
				} else if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}*/

			/*if (invoicingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingEndDates + "')");
				} else if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}*/
			
			/*
			if (storageBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				}

				if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageBeginDates + "')");
				} else if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageBeginDates + "')");
				} else { 
				}
				isAnd = true;
			}*/

			/*if (storageEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				}

				if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageEndDates + "')");
				} else if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageEndDates + "')");
				} else { 
				}
				isAnd = true;
			}*/
			
			
			if (revenueRecognitionBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else { 
				}
				isAnd = true;
			}

			if (revenueRecognitionEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			

			if (companyCodeCondition == null || companyCodeCondition.equals("")) {

			} else {
				if (isAnd == true) {
					query.append(" and s.companyDivision");
				} else {
					query.append("  s.companyDivision");
				}
				if (companyCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + companyCode + "'");
				} else {
					query.append("<>");
					query.append("'" + companyCode + "'");
				}
				isAnd = true;
			}
			if ( bookingDates.toString().equals("")) {
			} else {
				if (isAnd == true) {
					query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				} else {
					query.append(" (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				}
				if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + bookingDates + "')");
				} else if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + bookingDates + "')");
				} else {
				}
				isAnd = true;
			}
			if ( toBookingDates.toString().equals("")) {
			} else {
				if (isAnd == true) {
					query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				} else {
					query.append(" (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				}
				if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toBookingDates + "')");
				} else if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toBookingDates + "')");
				} else {
				}
				isAnd = true;
			}
			
			if (carrierCondition.equals("") || carrierCondition == null) {
			} else {
				if (isAnd == true) {
					query.append(" and m.carrier");
				} else {
					query.append("  m.carrier");
				}
				if (carrierCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + carrier.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + carrier.trim() + "'");
				}
				isAnd = true;
			}
			if (driverIdCondition.equals("") || driverIdCondition == null) {
			} else {
				if (isAnd == true) {
					query.append(" and m.driverId");
				} else {
					query.append("  m.driverId");
				}
				if (driverIdCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + driverId.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + driverId.trim() + "'");
				}
				isAnd = true;
			}
			
			if (accountCodeCondition.equals("") || accountCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and c.accountCode");
				} else {
					query.append("   c.accountCode");
				}
				if (accountCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + accountCodeType + "'");
				} else {
					query.append("<>");
					query.append("'" + accountCodeType + "'");
				}
				isAnd = true;
			}
			
			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status  in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CNCL') and s.status is not null and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  and s.corpId='" + sessionCorpID + "' and (s.moveType='BookedMove' or s.moveType  is null or s.moveType = '') group by s.shipNumber");
			}
			//query.append("   and s.corpId='"+sessionCorpID+"'  and  s.status not in ('CLSD','CNCL')   group by s.shipNumber");
		}
   
		//String queryExtract=extractManager.saveForExtract(query.toString());
	if(selectCondition.equals("")||selectCondition.equals("#"))
  	  {
  	  }
  	 else
  	  {
  		selectCondition=selectCondition.trim();
  	    if(selectCondition.indexOf("#")==0)
  		 {
  	    	selectCondition=selectCondition.substring(1);
  		 }
  		if(selectCondition.lastIndexOf("#")==selectCondition.length()-1)
  		 {
  			selectCondition=selectCondition.substring(0, selectCondition.length()-1);
  		 }
		
  	  }
	selectCondition=selectCondition.replaceAll("#", ",");
	List selectConditionList=new ArrayList();
	selectConditionList= extractColumnMgmtManager.getSelectCondition(selectCondition,reportName,sessionCorpID);
    String SaveSelectCondition=new String();
    Iterator iterator =selectConditionList.iterator();
    while(iterator.hasNext())
    {
    	SaveSelectCondition =SaveSelectCondition.concat((String)iterator.next().toString());
    	SaveSelectCondition=SaveSelectCondition.concat("#");
    }
    SaveSelectCondition=SaveSelectCondition.trim();
	    if(SaveSelectCondition.indexOf("#")==0)
		 {
	    	SaveSelectCondition=SaveSelectCondition.substring(1);
		 }
		if(SaveSelectCondition.lastIndexOf("#")==SaveSelectCondition.length()-1)
		 {
			SaveSelectCondition=SaveSelectCondition.substring(0, SaveSelectCondition.length()-1);
		 }
	String columnSelect=SaveSelectCondition;
	SaveSelectCondition=SaveSelectCondition.replaceAll("#", ",");
	//System.out.println("\n\n\n\n\n\n\n\n query" + query);
	//System.out.println("\n\n\n\n\n\n\n\n selct query    " +SaveSelectCondition+" query    " + query    );
	//List activeShipments=new ArrayList();
	List activeShipments = serviceOrderManager.dynamicReloServices(SaveSelectCondition, query.toString(),sessionCorpID);
	
	try {
		HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file = new File("RelocationAnalysis");
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls");
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		//response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" + "\t The number of line is" + activeShipments.size() + "extracted");
		out.println("filename=" + file.getName() + ".xls" + "\t The number of line  \t" + activeShipments.size() + "\t is extracted");
		String bA_SSCW = "";
		String[] conditionArray=columnSelect.split("#");
		int arrayLength = conditionArray.length;
		for(int i=0; i<arrayLength; i++)
		 {	
		   String condition=conditionArray[i]; 
		   String []columnArray=condition.split(" as ");
		   //condition= condition.substring(condition.indexOf("as")+2);
		   System.out.println(columnArray[0].trim() +"========================="+ columnArray[1].trim());
		   String column=columnArray[1];
		   outputStream.write((column+"\t").getBytes());
		 } 
		//outputStream.write("URL".getBytes());
		outputStream.write("\r\n".getBytes());
		Iterator it = activeShipments.iterator();

		while(it.hasNext()){
			Object[] row = (Object[]) it.next();

			for(int i=0; i<arrayLength; i++){
				if (row[i] != null) {
					String data = row[i].toString(); 
					outputStream.write((data + "\t").getBytes());
				} else {
					outputStream.write("\t".getBytes());
				}
			}
			//outputStream.write(("https://www.skyrelo.com/redsky/editTrackingStatus.html?id="+row[arrayLength] + "\t").getBytes());
			outputStream.write("\n".getBytes());
		}
		if(extractFileAudit){
			outputStream.close();
			response.setContentType("application/octet-stream");
			//ServletOutputStream outputStream = response.getOutputStream();
			SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
			StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
			String currentyear=yearDate.toString();
			
			String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
			File dirPath = new File(uploadDir);
					if (!dirPath.exists()) {
				dirPath.mkdirs();
			}
			SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
			StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
			String currentDateTime=tempDate.toString().trim();
			String fileName="RelocationAnalysis_"+currentDateTime+".xls";
			String filePath=uploadDir+""+fileName;
			File file1 = new File(filePath); 
			FileOutputStream outputStream1 = new FileOutputStream(file1);
			response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + "");
			conditionArray=columnSelect.split("#");
		    arrayLength = conditionArray.length;
			for(int i=0;i<arrayLength;i++)
			 {	
			   String condition=conditionArray[i]; 
			   String []columnArray=condition.split(" as");
			   //condition= condition.substring(condition.indexOf("as")+2);
			   String column=columnArray[1];
			   outputStream1.write((column+"\t").getBytes());
			 } 
			//outputStream.write("URL".getBytes());
			outputStream1.write("\r\n".getBytes());
			Iterator its = activeShipments.iterator();

			while(its.hasNext()){
				Object[] row = (Object[]) its.next();

				for(int i=0; i<arrayLength; i++){
					if (row[i] != null) {
						String data = row[i].toString();
						outputStream1.write((data + "\t").getBytes());
					} else {
						outputStream1.write("\t".getBytes());
					}
				}
				//outputStream.write(("https://www.skyrelo.com/redsky/editTrackingStatus.html?id="+row[arrayLength] + "\t").getBytes());
				outputStream1.write("\n".getBytes());
		    }
			extractedFileLog = new ExtractedFileLog();
			extractedFileLog.setCorpID(sessionCorpID);
			extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
			extractedFileLog.setCreatedOn(new Date());
			extractedFileLog.setLocation(filePath);
			extractedFileLog.setFileName(fileName);
			extractedFileLog.setModule("Data Extract");
			extractedFileLog=extractedFileLogManager.save(extractedFileLog);
			}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
		
	
		
		
		
		
	}
	
	
	
	// for GIS ZipCode placements
   // for GIS ZipCode placements for origin
	public String zipPortsPlacement(){
		return SUCCESS;
	}
//	 for GIS ZipCode placements for destin
	public String zipPortsDestPlacement(){
		return SUCCESS;
	}
	
//	 for GIS ZipCode placements for origin markups
	public InputStream getZipPointMarkup(){
		ByteArrayInputStream output = null;
		
		if(extractVal == null || (!extractVal.equals("true")) ){
		StringBuffer jobBuffer = new StringBuffer("");
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		}
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		StringBuffer beginDates = new StringBuffer();
		StringBuffer endDates = new StringBuffer();
		StringBuffer deliveryTBeginDates = new StringBuffer();
		StringBuffer deliveryTEndDates = new StringBuffer();
		StringBuffer deliveryActBeginDates = new StringBuffer();
		StringBuffer deliveryActEndDates = new StringBuffer();
		StringBuffer loadTgtBeginDates = new StringBuffer();
		StringBuffer loadTgtEndDates = new StringBuffer();
		StringBuffer invPostingBeginDates = new StringBuffer();
		StringBuffer invPostingEndDates = new StringBuffer();
		StringBuffer revenueRecognitionBeginDates = new StringBuffer();
		StringBuffer revenueRecognitionEndDates = new StringBuffer();
		StringBuffer invoicingBeginDates = new StringBuffer();
		StringBuffer invoicingEndDates = new StringBuffer();
		StringBuffer storageBeginDates = new StringBuffer();
		StringBuffer storageEndDates = new StringBuffer();
		StringBuffer createdDates = new StringBuffer();
		StringBuffer toCreatedDates = new StringBuffer();
		StringBuffer payPostingBeginDates = new StringBuffer();
		StringBuffer payPostingEndDates = new StringBuffer();
		StringBuffer receivedBeginDates = new StringBuffer();
		StringBuffer receivedEndDates = new StringBuffer();
		StringBuffer postingPeriodBeginDates = new StringBuffer();
		StringBuffer postingPeriodEndDates = new StringBuffer();
		StringBuffer bookingDates = new StringBuffer();
		StringBuffer toBookingDates = new StringBuffer();
		StringBuffer updateDates = new StringBuffer();
		StringBuffer toUpdateDates = new StringBuffer();
		StringBuffer query = new StringBuffer();
		boolean isAnd = false;
		if(!revenueRecognitionPeriod.equals("")){
        	String  returnValue=   findPeriodDate(revenueRecognitionPeriod);
        	String [] returnValueArray=returnValue.split("#");
        	revenueRecognitionBeginDates = new StringBuffer(returnValueArray[0]);
        	revenueRecognitionEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate == null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer();
		} else if (revenueRecognitionBeginDate == null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer();
		}
        }
		if(!loadActualDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(loadActualDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	beginDates = new StringBuffer(returnValueArray[0]);
        	endDates = new StringBuffer(returnValueArray[1]);
        }else{
		if (beginDate != null && endDate != null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer(formats.format(endDate));

		} else if (beginDate != null && endDate == null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer();
		} else if (beginDate == null && endDate != null) {
			beginDates = new StringBuffer();
			endDates = new StringBuffer(formats.format(endDate));
		} else {
			beginDates = new StringBuffer();
			endDates = new StringBuffer();

		}
        }
		if(!createDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(createDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	createdDates = new StringBuffer(returnValueArray[0]);
        	toCreatedDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (createDate != null && toCreateDate != null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else if (createDate != null && toCreateDate == null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer();
		} else if (createDate == null && toCreateDate != null) {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer();
		}
        }
		if(!updateDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(updateDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	updateDates  = new StringBuffer(returnValueArray[0]);
        	toUpdateDates  = new StringBuffer(returnValueArray[1]);
        }else{
        	if (updateDate != null && toUpdateDate != null) {
				updateDates = new StringBuffer(formats.format(updateDate));
				toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
			} else if (updateDate != null && toUpdateDate == null) {
				updateDates = new StringBuffer(formats.format(updateDate));
				toUpdateDates = new StringBuffer();
			} else if (updateDate == null && toUpdateDate != null) {
				updateDates = new StringBuffer();
				toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
			} else {
				updateDates = new StringBuffer();
				toUpdateDates = new StringBuffer();
			}
        }
		if(!deliveryTargetDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(deliveryTargetDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	deliveryTBeginDates = new StringBuffer(returnValueArray[0]);
        	deliveryTEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (deliveryTBeginDate != null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer();
		} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer();
		}
        }
		if(!deliveryActualDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(deliveryActualDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	deliveryActBeginDates = new StringBuffer(returnValueArray[0]);
        	deliveryActEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (deliveryActBeginDate != null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer();
		} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer();
		}
        }
        if(!loadTargetDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(loadTargetDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	loadTgtBeginDates = new StringBuffer(returnValueArray[0]);
        	loadTgtEndDates = new StringBuffer(returnValueArray[1]);
        }else{
		if (loadTgtBeginDate != null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer();
		} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer();
		}
        }
        if(!invoicePostingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(invoicePostingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	invPostingBeginDates = new StringBuffer(returnValueArray[0]);
        	invPostingEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (invPostingBeginDate != null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else if (invPostingBeginDate != null && invPostingEndDate == null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer();
		} else if (invPostingBeginDate == null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer();
		}
        }
        if(!InvoicingdatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(InvoicingdatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	invoicingBeginDates = new StringBuffer(returnValueArray[0]);
        	invoicingEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (invoicingBeginDate != null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else if (invoicingBeginDate != null && invoicingEndDate == null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer();
		} else if (invoicingBeginDate == null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer();
		}
        }
        if(!storageDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(storageDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	storageEndDates = new StringBuffer(returnValueArray[0]);
        	storageEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (storageBeginDate != null && storageEndDate != null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else if (storageBeginDate != null && storageEndDate == null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer();
		} else if (storageBeginDate == null && storageEndDate != null) {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer();
		}
        }
		if(!payablePostingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(payablePostingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	payPostingBeginDates = new StringBuffer(returnValueArray[0]);
        	payPostingEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (payPostingBeginDate != null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else if (payPostingBeginDate != null && payPostingEndDate == null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer();
		} else if (payPostingBeginDate == null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer();
		}
        }
		if(!receivedDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(receivedDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	receivedBeginDates = new StringBuffer(returnValueArray[0]);
        	receivedEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (receivedBeginDate != null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else if (receivedBeginDate != null && receivedEndDate == null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer();
		} else if (receivedBeginDate == null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer();
		}
        }
		if(!PostingPeriodDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(PostingPeriodDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	postingPeriodBeginDates = new StringBuffer(returnValueArray[0]);
        	postingPeriodEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (postingPeriodBeginDate != null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else if (postingPeriodBeginDate != null && postingPeriodEndDate == null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer();
		 } else if (postingPeriodBeginDate == null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer();
		 } 
        }
		if(!bookingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(bookingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	bookingDates = new StringBuffer(returnValueArray[0]);
        	toBookingDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (bookingDate != null && toBookingDate != null) {
			bookingDates = new StringBuffer(formats.format(bookingDate));
			toBookingDates = new StringBuffer(formats.format(toBookingDate));
		} else if (bookingDate != null && toBookingDate == null) {
			bookingDates = new StringBuffer(formats.format(bookingDate));
			toBookingDates = new StringBuffer();
		} else if (bookingDate == null && toBookingDate != null) {
			bookingDates = new StringBuffer();
			toBookingDates = new StringBuffer(formats.format(toBookingDate));
		} else {
			bookingDates = new StringBuffer();
			toBookingDates = new StringBuffer();
		}
        }
		if (jobTypesCondition.equals("") && billToCodeCondition.equals("")&& originAgentCondition.equals("")&& destinationAgentCondition.equals("") && bookingCodeCondition.equals("") && routingCondition.equals("") && modeCondition.equals("") && packModeCondition.equals("")
				&& commodityCondition.equals("") && coordinatorCondition.equals("") && consultantCondition.equals("") && salesmanCondition.equals("") && oACountryCondition.equals("") && dACountryCondition.equals("")
				&& actualWgtCondition.equals("") && companyCodeCondition.equals("") && beginDates.toString().equals("") && endDates.toString().equals("") && actualVolumeCondition.equals("")
				&& createdDates.toString().equals("") && toCreatedDates.toString().equals("") && updateDates.toString().equals("") && toUpdateDates.toString().equals("") &&  deliveryTBeginDates.toString().equals("") && deliveryTEndDates.toString().equals("") && deliveryActBeginDates.toString().equals("")
				&& deliveryActEndDates.toString().equals("") && loadTgtBeginDates.toString().equals("") && loadTgtEndDates.toString().equals("") && invPostingBeginDates.toString().equals("") && invPostingEndDates.toString().equals("")
				&& invoicingBeginDates.toString().equals("") && invoicingEndDates.toString().equals("") && storageBeginDates.toString().equals("") && storageEndDates.toString().equals("") && revenueRecognitionBeginDates.toString().equals("") && revenueRecognitionEndDates.toString().equals("")&& payPostingBeginDates.toString().equals("") && payPostingEndDates.toString().equals("")&& receivedBeginDates.toString().equals("") && receivedEndDates.toString().equals("")&& postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals("")
				&& bookingDates.toString().equals("") && toBookingDates.toString().equals("") && carrierCondition.equals("") && driverIdCondition.equals("")) {

			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null  ");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status in ('CLSD','CNCL') and s.status is not null  ");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status not in ('CNCL') and s.status is not null  ");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   ");
			}
			//query.append(" where   s.corpId='"+sessionCorpID+"'  and  s.status not in ('CLSD','CNCL') group by s.shipNumber");
		} else {

			query.append("  where  ");

			if (jobTypesCondition.equals("") || jobTypesCondition == null) {

			} else {
				query.append(" s.job ");
				isAnd = true;
				if (jobTypesCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append(" in  ");
					query.append("(" + jobtypeMultiple + ")");
				} else {
					query.append(" not in  ");
					query.append("(" + jobtypeMultiple + ")");
				}

			}
			if (billToCodeCondition.equals("") || billToCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.billToCode");
				} else {
					query.append("  s.billToCode");
				}
				if (billToCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + billToCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + billToCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (originAgentCondition.equals("") || originAgentCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and t.originAgentCode");
				} else {
					query.append("  t.originAgentCode");
				}
				if (originAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + originAgentCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + originAgentCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (destinationAgentCondition.equals("") |destinationAgentCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and t.destinationAgentCode");
				} else {
					query.append("  t.destinationAgentCode");
				}
				if (destinationAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + destinationAgentCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + destinationAgentCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (bookingCodeCondition.equals("") || bookingCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.bookingAgentCode");
				} else {
					query.append("  s.bookingAgentCode");
				}
				if (bookingCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + bookingCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + bookingCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (routingCondition.equals("") || routingCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.routing");
				} else {
					query.append("   s.routing");
				}
				if (routingCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + routingTypes + "'");
				} else {
					query.append("<>");
					query.append("'" + routingTypes + "'");
				}
				isAnd = true;
			}
			if (modeCondition.equals("") || modeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.mode");
				} else {
					query.append("  s.mode");
				}
				if (modeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + modeType + "'");
				} else {
					query.append("<>");
					query.append("'" + modeType + "'");
				}
				isAnd = true;
			}
			if (packModeCondition.equals("") || packModeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.packingMode");
				} else {
					query.append("   s.packingMode");
				}
				if (packModeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + packModeType + "'");
				} else {
					query.append("<>");
					query.append("'" + packModeType + "'");
				}
				isAnd = true;
			}
			if (commodityCondition.equals("") || commodityCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.commodity");
				} else {
					query.append("  s.commodity");
				}
				if (commodityCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + commodityType + "'");
				} else {
					query.append("<>");
					query.append("'" + commodityType + "'");
				}
				isAnd = true;
			}
			if (coordinatorCondition.equals("") || coordinatorCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.coordinator");
				} else {
					query.append("   s.coordinator");
				}
				if (coordinatorCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + coordinatorType + "'");
				} else {
					query.append("<>");
					query.append("'" + coordinatorType + "'");
				}
				isAnd = true;
			}
			if (consultantCondition.equals("") || consultantCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.estimator");
				} else {
					query.append("   s.estimator");
				}
				if (consultantCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + consultantType + "'");
				} else {
					query.append("<>");
					query.append("'" + consultantType + "'");
				}
				isAnd = true;
			}
			
			if (salesmanCondition.equals("") || salesmanCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.salesMan");
				} else {
					query.append("   s.salesMan");
				}
				if (salesmanCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + salesmanType + "'");
				} else {
					query.append("<>");
					query.append("'" + salesmanType + "'");
				}
				isAnd = true;
			}
			if (oACountryCondition.equals("") || oACountryCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.originCountryCode");
				} else {
					query.append("   s.originCountryCode");
				}
				if (oACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + oACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + oACountryType + "'");
				}
				isAnd = true;
			}
			if (dACountryCondition.equals("") || dACountryCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.destinationCountryCode");
				} else {
					query.append("  s.destinationCountryCode");
				}
				if (dACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + dACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + dACountryType + "'");
				}
				isAnd = true;
			} 
			if (actualWgtCondition.equals("") || actualWgtCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualNetWeight");
				} else {
					query.append("  m.actualNetWeight");
				}

				if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualWgtType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualWgtType + "'");
				}
				isAnd = true;
			}
			if (actualVolumeCondition.equals("") || actualVolumeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualCubicFeet");
				} else {
					query.append("  m.actualCubicFeet");
				}
				if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualVolumeType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualVolumeType + "'");
				}
				isAnd = true;
			}
			if (beginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + beginDates + "')");
				} else if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + beginDates + "')");
				} else {

				}
				isAnd = true;
			}

			if ( createdDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + createdDates + "')");
				} else if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + createdDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toCreatedDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toCreatedDates + "')");
				} else if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toCreatedDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( updateDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				}
				if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + updateDates + "')");
				} else if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + updateDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toUpdateDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("   and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				}
				if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toUpdateDates + "')");
				} else if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toUpdateDates + "')");
				} else {

				}
				isAnd = true;
			}

			if (endDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + endDates + "')");
				} else if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + endDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (deliveryTBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTBeginDates + "')");
				} else if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTEndDates + "')");
				} else if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActBeginDates + "')");
				} else if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActEndDates + "')");
				} else if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtBeginDates + "')");
				} else if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtEndDates + "')");
				} else if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			/*if (invPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingBeginDates + "')");
				} else if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}*/
			/*if (invPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingEndDates + "')");
				} else if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}*/
			/*if (payPostingBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingBeginDates + "')");
				} else if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}*/
			/*if (payPostingEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingEndDates + "')");
				} else if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}*/
			/*if ( receivedBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedBeginDates + "')");
				} else if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			}*/
			/*if (receivedEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedEndDates + "')");
				} else if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}*/
			if ( (postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals(""))) {

			} else 
			{/*
				if(((!( postingPeriodBeginDates.toString().equals(""))))&&!(postingPeriodEndDates.toString().equals(""))) {
				if (isAnd == true) {
					query.append("  and ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append("  ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");

                  
				  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
                   query.append("  or ( (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				   if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}

                 query.append("  and (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "') ) ) )");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "') ) ) ) ");
				} else {
					
				}

				isAnd = true;
			} 
				else if((!( postingPeriodBeginDates.toString().equals("")))&& ((postingPeriodEndDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			else  if((!( postingPeriodEndDates.toString().equals("")))&& ((postingPeriodBeginDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			*/}
			 
			if (invoicingBeginDates.toString().equals("") ) {

			} else {/*
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingBeginDates + "')");
				} else if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			*/}

			if (invoicingEndDates.toString().equals("") ) {

			} else {/*
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingEndDates + "')");
				} else if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			*/}
			
			
			if (storageBeginDates.toString().equals("") ) {

			} else {/*
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				}

				if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageBeginDates + "')");
				} else if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageBeginDates + "')");
				} else { 
				}
				isAnd = true;
			*/}

			if (storageEndDates.toString().equals("") ) {

			} else {/*
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				}

				if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageEndDates + "')");
				} else if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageEndDates + "')");
				} else { 
				}
				isAnd = true;
			*/}
			
			
			if (revenueRecognitionBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else { 
				}
				isAnd = true;
			}

			if (revenueRecognitionEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			

			if (companyCodeCondition == null || companyCodeCondition.equals("")) {

			} else {
				if (isAnd == true) {
					query.append(" and s.companyDivision");
				} else {
					query.append("  s.companyDivision");
				}
				if (companyCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + companyCode + "'");
				} else {
					query.append("<>");
					query.append("'" + companyCode + "'");
				}
				isAnd = true;
			}
			if ( bookingDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				} else {
					query.append(" (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				}
				if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + bookingDates + "')");
				} else if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + bookingDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toBookingDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				} else {
					query.append(" (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				}
				if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toBookingDates + "')");
				} else if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toBookingDates + "')");
				} else {

				}
				isAnd = true;
			}
			
			if (carrierCondition.equals("") || carrierCondition == null) {
			} else {
				if (isAnd == true) {
					query.append(" and m.carrier");
				} else {
					query.append("  m.carrier");
				}
				if (carrierCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + carrier.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + carrier.trim() + "'");
				}
				isAnd = true;
			}
			if (driverIdCondition.equals("") || driverIdCondition == null) {
			} else {
				if (isAnd == true) {
					query.append(" and m.driverId");
				} else {
					query.append("  m.driverId");
				}
				if (driverIdCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + driverId.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + driverId.trim() + "'");
				}
				isAnd = true;
			}
			
			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null ");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status  in ('CLSD','CNCL') and s.status is not null ");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CNCL') and s.status is not null ");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  and s.corpId='" + sessionCorpID + "' ");
			}
			
		}
		activeUsOriginGisOutput=serviceOrderManager.usOriginGisOutput(query.toString(),sessionCorpID);
		int count = 0;
		StringBuilder ZipCode = new StringBuilder();
		Iterator it=activeUsOriginGisOutput.iterator();
		  while(it.hasNext())
			{	
			  //String [] str1 =activeUsOriginGisOutput.get(0).toString().split("");
				Object  obj= (Object)it.next();
					if((!((DTO)obj).getLatitude().toString().equalsIgnoreCase("")) && (!((DTO)obj).getLongitude().toString().equalsIgnoreCase(""))){
						count++;
						if (count > 1) ZipCode.append(","); else ZipCode.append("");
						ZipCode.append("{\"originZip\":\"" + ((DTO)obj).getOriginZip() + "\", \"originZipCountNo\": \"" + ((DTO)obj).getOriginZipCountNo() + "\", \"latitude\": \"" + ((DTO)obj).getLatitude() + "\", \"longitude\": \"" + ((DTO)obj).getLongitude() + "\"}");
					}
				
			}
			String prependStr = "{ \"count\":" + count + ", \"ZipCode\":[";
			String appendStr = "]}";
			String s=prependStr + ZipCode.toString() + appendStr;
			output= new ByteArrayInputStream(s.getBytes());
			//System.out.println("\n\n\n\n\n\n\n\n\n\n\n\nactiveUsOriginGisOutput"+activeUsOriginGisOutput);
	}
	return output;	
	
	}
//	 for GIS ZipCode placements for origin markups	
	public InputStream getZipPointDestMarkup(){
		ByteArrayInputStream output = null;
		
		if(extractVal == null || (!extractVal.equals("true")) ){
		StringBuffer jobBuffer = new StringBuffer("");
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		}
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		StringBuffer beginDates = new StringBuffer();
		StringBuffer endDates = new StringBuffer();
		StringBuffer deliveryTBeginDates = new StringBuffer();
		StringBuffer deliveryTEndDates = new StringBuffer();
		StringBuffer deliveryActBeginDates = new StringBuffer();
		StringBuffer deliveryActEndDates = new StringBuffer();
		StringBuffer loadTgtBeginDates = new StringBuffer();
		StringBuffer loadTgtEndDates = new StringBuffer();
		StringBuffer invPostingBeginDates = new StringBuffer();
		StringBuffer invPostingEndDates = new StringBuffer();
		StringBuffer revenueRecognitionBeginDates = new StringBuffer();
		StringBuffer revenueRecognitionEndDates = new StringBuffer();
		StringBuffer invoicingBeginDates = new StringBuffer();
		StringBuffer invoicingEndDates = new StringBuffer();
		StringBuffer storageBeginDates = new StringBuffer();
		StringBuffer storageEndDates = new StringBuffer();
		StringBuffer createdDates = new StringBuffer();
		StringBuffer toCreatedDates = new StringBuffer();
		StringBuffer payPostingBeginDates = new StringBuffer();
		StringBuffer payPostingEndDates = new StringBuffer();
		StringBuffer receivedBeginDates = new StringBuffer();
		StringBuffer receivedEndDates = new StringBuffer();
		StringBuffer postingPeriodBeginDates = new StringBuffer();
		StringBuffer postingPeriodEndDates = new StringBuffer();
		StringBuffer bookingDates = new StringBuffer();
		StringBuffer toBookingDates = new StringBuffer();
		StringBuffer updateDates = new StringBuffer();
		StringBuffer toUpdateDates = new StringBuffer();
		StringBuffer query = new StringBuffer();
		boolean isAnd = false;
		if(!revenueRecognitionPeriod.equals("")){
        	String  returnValue=   findPeriodDate(revenueRecognitionPeriod);
        	String [] returnValueArray=returnValue.split("#");
        	revenueRecognitionBeginDates = new StringBuffer(returnValueArray[0]);
        	revenueRecognitionEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else if (revenueRecognitionBeginDate != null && revenueRecognitionEndDate == null) {
			revenueRecognitionBeginDates = new StringBuffer(formats.format(revenueRecognitionBeginDate));
			revenueRecognitionEndDates = new StringBuffer();
		} else if (revenueRecognitionBeginDate == null && revenueRecognitionEndDate != null) {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer(formats.format(revenueRecognitionEndDate));
		} else {
			revenueRecognitionBeginDates = new StringBuffer();
			revenueRecognitionEndDates = new StringBuffer();
		}
        }
		if(!loadActualDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(loadActualDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	beginDates = new StringBuffer(returnValueArray[0]);
        	endDates = new StringBuffer(returnValueArray[1]);
        }else{
		if (beginDate != null && endDate != null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer(formats.format(endDate));

		} else if (beginDate != null && endDate == null) {
			beginDates = new StringBuffer(formats.format(beginDate));
			endDates = new StringBuffer();
		} else if (beginDate == null && endDate != null) {
			beginDates = new StringBuffer();
			endDates = new StringBuffer(formats.format(endDate));
		} else {
			beginDates = new StringBuffer();
			endDates = new StringBuffer();

		}
        }
		if(!createDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(createDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	createdDates = new StringBuffer(returnValueArray[0]);
        	toCreatedDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (createDate != null && toCreateDate != null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else if (createDate != null && toCreateDate == null) {
			createdDates = new StringBuffer(formats.format(createDate));
			toCreatedDates = new StringBuffer();
		} else if (createDate == null && toCreateDate != null) {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer(formats.format(toCreateDate));
		} else {
			createdDates = new StringBuffer();
			toCreatedDates = new StringBuffer();
		}
        }
		if(!updateDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(updateDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	updateDates  = new StringBuffer(returnValueArray[0]);
        	toUpdateDates  = new StringBuffer(returnValueArray[1]);
        }else{
        	if (updateDate != null && toUpdateDate != null) {
				updateDates = new StringBuffer(formats.format(updateDate));
				toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
			} else if (updateDate != null && toUpdateDate == null) {
				updateDates = new StringBuffer(formats.format(updateDate));
				toUpdateDates = new StringBuffer();
			} else if (updateDate == null && toUpdateDate != null) {
				updateDates = new StringBuffer();
				toUpdateDates = new StringBuffer(formats.format(toUpdateDate));
			} else {
				updateDates = new StringBuffer();
				toUpdateDates = new StringBuffer();
			}
        }
		if(!deliveryTargetDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(deliveryTargetDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	deliveryTBeginDates = new StringBuffer(returnValueArray[0]);
        	deliveryTEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (deliveryTBeginDate != null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else if (deliveryTBeginDate != null && deliveryTEndDate == null) {
			deliveryTBeginDates = new StringBuffer(formats.format(deliveryTBeginDate));
			deliveryTEndDates = new StringBuffer();
		} else if (deliveryTBeginDate == null && deliveryTEndDate != null) {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer(formats.format(deliveryTEndDate));
		} else {
			deliveryTBeginDates = new StringBuffer();
			deliveryTEndDates = new StringBuffer();
		}
        }
		if(!deliveryActualDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(deliveryActualDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	deliveryActBeginDates = new StringBuffer(returnValueArray[0]);
        	deliveryActEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (deliveryActBeginDate != null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else if (deliveryActBeginDate != null && deliveryActEndDate == null) {
			deliveryActBeginDates = new StringBuffer(formats.format(deliveryActBeginDate));
			deliveryActEndDates = new StringBuffer();
		} else if (deliveryActBeginDate == null && deliveryActEndDate != null) {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer(formats.format(deliveryActEndDate));
		} else {
			deliveryActBeginDates = new StringBuffer();
			deliveryActEndDates = new StringBuffer();
		}
        }
        if(!loadTargetDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(loadTargetDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	loadTgtBeginDates = new StringBuffer(returnValueArray[0]);
        	loadTgtEndDates = new StringBuffer(returnValueArray[1]);
        }else{
		if (loadTgtBeginDate != null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else if (loadTgtBeginDate != null && loadTgtEndDate == null) {
			loadTgtBeginDates = new StringBuffer(formats.format(loadTgtBeginDate));
			loadTgtEndDates = new StringBuffer();
		} else if (loadTgtBeginDate == null && loadTgtEndDate != null) {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer(formats.format(loadTgtEndDate));
		} else {
			loadTgtBeginDates = new StringBuffer();
			loadTgtEndDates = new StringBuffer();
		}
        }
        if(!invoicePostingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(invoicePostingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	invPostingBeginDates = new StringBuffer(returnValueArray[0]);
        	invPostingEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (invPostingBeginDate != null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else if (invPostingBeginDate != null && invPostingEndDate == null) {
			invPostingBeginDates = new StringBuffer(formats.format(invPostingBeginDate));
			invPostingEndDates = new StringBuffer();
		} else if (invPostingBeginDate == null && invPostingEndDate != null) {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer(formats.format(invPostingEndDate));
		} else {
			invPostingBeginDates = new StringBuffer();
			invPostingEndDates = new StringBuffer();
		}
        }
        if(!InvoicingdatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(InvoicingdatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	invoicingBeginDates = new StringBuffer(returnValueArray[0]);
        	invoicingEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (invoicingBeginDate != null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else if (invoicingBeginDate != null && invoicingEndDate == null) {
			invoicingBeginDates = new StringBuffer(formats.format(invoicingBeginDate));
			invoicingEndDates = new StringBuffer();
		} else if (invoicingBeginDate == null && invoicingEndDate != null) {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer(formats.format(invoicingEndDate));
		} else {
			invoicingBeginDates = new StringBuffer();
			invoicingEndDates = new StringBuffer();
		}
        }
        if(!storageDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(storageDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	storageEndDates = new StringBuffer(returnValueArray[0]);
        	storageEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (storageBeginDate != null && storageEndDate != null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else if (storageBeginDate != null && storageEndDate == null) {
			storageBeginDates = new StringBuffer(formats.format(storageBeginDate));
			storageEndDates = new StringBuffer();
		} else if (storageBeginDate == null && storageEndDate != null) {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer(formats.format(storageEndDate));
		} else {
			storageBeginDates = new StringBuffer();
			storageEndDates = new StringBuffer();
		}
        }
		if(!payablePostingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(payablePostingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	payPostingBeginDates = new StringBuffer(returnValueArray[0]);
        	payPostingEndDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (payPostingBeginDate != null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else if (payPostingBeginDate != null && payPostingEndDate == null) {
			payPostingBeginDates = new StringBuffer(formats.format(payPostingBeginDate));
			payPostingEndDates = new StringBuffer();
		} else if (payPostingBeginDate == null && payPostingEndDate != null) {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer(formats.format(payPostingEndDate));
		} else {
			payPostingBeginDates = new StringBuffer();
			payPostingEndDates = new StringBuffer();
		}
        }
		if(!receivedDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(receivedDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	receivedBeginDates = new StringBuffer(returnValueArray[0]);
        	receivedEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (receivedBeginDate != null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else if (receivedBeginDate != null && receivedEndDate == null) {
			receivedBeginDates = new StringBuffer(formats.format(receivedBeginDate));
			receivedEndDates = new StringBuffer();
		} else if (receivedBeginDate == null && receivedEndDate != null) {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer(formats.format(receivedEndDate));
		} else {
			receivedBeginDates = new StringBuffer();
			receivedEndDates = new StringBuffer();
		}
        }
		if(!PostingPeriodDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(PostingPeriodDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	postingPeriodBeginDates = new StringBuffer(returnValueArray[0]);
        	postingPeriodEndDates = new StringBuffer(returnValueArray[1]);
        }else{  
		if (postingPeriodBeginDate != null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else if (postingPeriodBeginDate != null && postingPeriodEndDate == null) {
			 postingPeriodBeginDates = new StringBuffer(formats.format(postingPeriodBeginDate));
			 postingPeriodEndDates = new StringBuffer();
		 } else if (postingPeriodBeginDate == null && postingPeriodEndDate != null) {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer(formats.format(postingPeriodEndDate));
		 } else {
			 postingPeriodBeginDates = new StringBuffer();
			 postingPeriodEndDates = new StringBuffer();
		 } 
        }
		if(!bookingDatePeriod.equals("")){
        	String  returnValue=   findPeriodDate(bookingDatePeriod);
        	String [] returnValueArray=returnValue.split("#");
        	bookingDates = new StringBuffer(returnValueArray[0]);
        	toBookingDates = new StringBuffer(returnValueArray[1]);
        }else{ 
		if (bookingDate != null && toBookingDate != null) {
			bookingDates = new StringBuffer(formats.format(bookingDate));
			toBookingDates = new StringBuffer(formats.format(toBookingDate));
		} else if (bookingDate != null && toBookingDate == null) {
			bookingDates = new StringBuffer(formats.format(bookingDate));
			toBookingDates = new StringBuffer();
		} else if (bookingDate == null && toBookingDate != null) {
			bookingDates = new StringBuffer();
			toBookingDates = new StringBuffer(formats.format(toBookingDate));
		} else {
			bookingDates = new StringBuffer();
			toBookingDates = new StringBuffer();
		}
        }
		if (jobTypesCondition.equals("") && billToCodeCondition.equals("")&& originAgentCondition.equals("")&& destinationAgentCondition.equals("") && bookingCodeCondition.equals("") && routingCondition.equals("") && modeCondition.equals("") && packModeCondition.equals("")
				&& commodityCondition.equals("") && coordinatorCondition.equals("") && consultantCondition.equals("") && salesmanCondition.equals("") && oACountryCondition.equals("") && dACountryCondition.equals("")
				&& actualWgtCondition.equals("") && companyCodeCondition.equals("") && beginDates.toString().equals("") && endDates.toString().equals("") && actualVolumeCondition.equals("")
				&& createdDates.toString().equals("") && toCreatedDates.toString().equals("") && updateDates.toString().equals("") && toUpdateDates.toString().equals("") && deliveryTBeginDates.toString().equals("") && deliveryTEndDates.toString().equals("") && deliveryActBeginDates.toString().equals("")
				&& deliveryActEndDates.toString().equals("") && loadTgtBeginDates.toString().equals("") && loadTgtEndDates.toString().equals("") && invPostingBeginDates.toString().equals("") && invPostingEndDates.toString().equals("")
				&& invoicingBeginDates.toString().equals("") && invoicingEndDates.toString().equals("") && storageBeginDates.toString().equals("") && storageEndDates.toString().equals("") && revenueRecognitionBeginDates.toString().equals("") && revenueRecognitionEndDates.toString().equals("")&& payPostingBeginDates.toString().equals("") && payPostingEndDates.toString().equals("")&& receivedBeginDates.toString().equals("") && receivedEndDates.toString().equals("")&& postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals("")
				&& bookingDates.toString().equals("") && toBookingDates.toString().equals("") && carrierCondition.equals("") && driverIdCondition.equals("")) {

			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null  ");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status in ('CLSD','CNCL') and s.status is not null  ");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   and  s.status not in ('CNCL') and s.status is not null  ");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  where  s.corpId='" + sessionCorpID + "'   ");
			}
			//query.append(" where   s.corpId='"+sessionCorpID+"'  and  s.status not in ('CLSD','CNCL') group by s.shipNumber");
		} else {

			query.append("  where  ");

			if (jobTypesCondition.equals("") || jobTypesCondition == null) {

			} else {
				query.append(" s.job ");
				isAnd = true;
				if (jobTypesCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append(" in  ");
					query.append("(" + jobtypeMultiple + ")");
				} else {
					query.append(" not in  ");
					query.append("(" + jobtypeMultiple + ")");
				}

			}
			if (billToCodeCondition.equals("") || billToCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.billToCode");
				} else {
					query.append("  s.billToCode");
				}
				if (billToCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + billToCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + billToCodeType.trim() + "'");
				}
				isAnd = true;

			}
			
			if (originAgentCondition.equals("") || originAgentCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and t.originAgentCode");
				} else {
					query.append("  t.originAgentCode");
				}
				if (originAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + originAgentCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + originAgentCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (destinationAgentCondition.equals("") |destinationAgentCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and t.destinationAgentCode");
				} else {
					query.append("  t.destinationAgentCode");
				}
				if (destinationAgentCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + destinationAgentCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + destinationAgentCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (bookingCodeCondition.equals("") || bookingCodeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.bookingAgentCode");
				} else {
					query.append("  s.bookingAgentCode");
				}
				if (bookingCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + bookingCodeType.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + bookingCodeType.trim() + "'");
				}
				isAnd = true;

			}
			if (routingCondition.equals("") || routingCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.routing");
				} else {
					query.append("   s.routing");
				}
				if (routingCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + routingTypes + "'");
				} else {
					query.append("<>");
					query.append("'" + routingTypes + "'");
				}
				isAnd = true;
			}
			if (modeCondition.equals("") || modeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.mode");
				} else {
					query.append("  s.mode");
				}
				if (modeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + modeType + "'");
				} else {
					query.append("<>");
					query.append("'" + modeType + "'");
				}
				isAnd = true;
			}
			if (packModeCondition.equals("") || packModeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.packingMode");
				} else {
					query.append("   s.packingMode");
				}
				if (packModeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + packModeType + "'");
				} else {
					query.append("<>");
					query.append("'" + packModeType + "'");
				}
				isAnd = true;
			}
			if (commodityCondition.equals("") || commodityCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.commodity");
				} else {
					query.append("  s.commodity");
				}
				if (commodityCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + commodityType + "'");
				} else {
					query.append("<>");
					query.append("'" + commodityType + "'");
				}
				isAnd = true;
			}
			if (coordinatorCondition.equals("") || coordinatorCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.coordinator");
				} else {
					query.append("   s.coordinator");
				}
				if (coordinatorCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + coordinatorType + "'");
				} else {
					query.append("<>");
					query.append("'" + coordinatorType + "'");
				}
				isAnd = true;
			}
			if (consultantCondition.equals("") || consultantCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.estimator");
				} else {
					query.append("  s.estimator");
				}
				if (consultantCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + consultantType + "'");
				} else {
					query.append("<>");
					query.append("'" + consultantType + "'");
				}
				isAnd = true;
			}
			
			if (salesmanCondition.equals("") || salesmanCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.salesMan");
				} else {
					query.append("   s.salesMan");
				}
				if (salesmanCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + salesmanType + "'");
				} else {
					query.append("<>");
					query.append("'" + salesmanType + "'");
				}
				isAnd = true;
			}
			if (oACountryCondition.equals("") || oACountryCondition == null) {

			} else {
				if (isAnd == true) {
					query.append("  and s.originCountryCode");
				} else {
					query.append("   s.originCountryCode");
				}
				if (oACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + oACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + oACountryType + "'");
				}
				isAnd = true;
			}
			if (dACountryCondition.equals("") || dACountryCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and s.destinationCountryCode");
				} else {
					query.append("  s.destinationCountryCode");
				}
				if (dACountryCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + dACountryType + "'");
				} else {
					query.append("<>");
					query.append("'" + dACountryType + "'");
				}
				isAnd = true;
			} 
			if (actualWgtCondition.equals("") || actualWgtCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualNetWeight");
				} else {
					query.append("  m.actualNetWeight");
				}

				if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualWgtType + "'");
				} else if (actualWgtCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualWgtType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualWgtType + "'");
				}
				isAnd = true;
			}
			if (actualVolumeCondition.equals("") || actualVolumeCondition == null) {

			} else {
				if (isAnd == true) {
					query.append(" and m.actualCubicFeet");
				} else {
					query.append("  m.actualCubicFeet");
				}
				if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Not Equal to".trim())) {
					query.append("<>");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than".trim())) {
					query.append(">");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + actualVolumeType + "'");
				} else if (actualVolumeCondition.toUpperCase().trim().equalsIgnoreCase("Less than".trim())) {
					query.append("<");
					query.append("'" + actualVolumeType + "'");
				} else {
					query.append("<=");
					query.append("'" + actualVolumeType + "'");
				}
				isAnd = true;
			}
			if (beginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + beginDates + "')");
				} else if (beginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + beginDates + "')");
				} else {

				}
				isAnd = true;
			}

			if ( createdDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + createdDates + "')");
				} else if (createDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + createdDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toCreatedDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.createdon,'%Y-%m-%d')");
				}
				if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toCreatedDates + "')");
				} else if (toCreateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toCreatedDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( updateDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				}
				if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + updateDates + "')");
				} else if (updateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + updateDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toUpdateDates.toString().equals("")) {

			} else {
				if (isAnd == true) {
					query.append("   and  (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(s.updatedOn,'%Y-%m-%d')");
				}
				if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toUpdateDates + "')");
				} else if (toUpdateDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toUpdateDates + "')");
				} else {

				}
				isAnd = true;
			}

			if (endDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.loadA,'%Y-%m-%d')");
				}
				if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + endDates + "')");
				} else if (endDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + endDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}

			if (deliveryTBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTBeginDates + "')");
				} else if (deliveryTBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryTEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryShipper,'%Y-%m-%d')");
				}
				if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryTEndDates + "')");
				} else if (deliveryTEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryTEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActBeginDates + "')");
				} else if (deliveryActBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (deliveryActEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.deliveryA,'%Y-%m-%d')");
				}
				if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + deliveryActEndDates + "')");
				} else if (deliveryActEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + deliveryActEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtBeginDates + "')");
				} else if (loadTgtBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (loadTgtEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(t.beginLoad,'%Y-%m-%d')");
				}
				if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + loadTgtEndDates + "')");
				} else if (loadTgtEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + loadTgtEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			}
			if (invPostingBeginDates.toString().equals("") ) {

			} else {/*
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingBeginDates + "')");
				} else if (invPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			*/}
			if (invPostingEndDates.toString().equals("") ) {

			} else {/*
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')");
				}
				if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invPostingEndDates + "')");
				} else if (invPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invPostingEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			*/}
			if (payPostingBeginDates.toString().equals("") ) {

			} else {/*
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingBeginDates + "')");
				} else if (payPostingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			*/}
			if (payPostingEndDates.toString().equals("") ) {

			} else {/*
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				}
				if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + payPostingEndDates + "')");
				} else if (payPostingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + payPostingEndDates + "')");
				} else {
					
				}
				isAnd = true;
			*/}
			if ( receivedBeginDates.toString().equals("") ) {

			} else {/*
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedBeginDates + "')");
				} else if (receivedBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedBeginDates + "')");
				} else {
					
				}
				isAnd = true;
			*/}
			if (receivedEndDates.toString().equals("") ) {

			} else {/*
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedDate,'%Y-%m-%d')");
				}
				if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + receivedEndDates + "')");
				} else if (receivedEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + receivedEndDates + "')");
				} else {
					
				}
				isAnd = true;
			*/}
			if ( (postingPeriodBeginDates.toString().equals("") && postingPeriodEndDates.toString().equals(""))) {

			} else 
			{/*
				if(((!( postingPeriodBeginDates.toString().equals(""))))&&!(postingPeriodEndDates.toString().equals(""))) {
				if (isAnd == true) {
					query.append("  and ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append("  ( ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				query.append("  and  (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");

                  
				  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
                   query.append("  or ( (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				   if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}

                 query.append("  and (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				  if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "') ) ) )");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "') ) ) ) ");
				} else {
					
				}

				isAnd = true;
			} 
				else if((!( postingPeriodBeginDates.toString().equals("")))&& ((postingPeriodEndDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodBeginDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else if (periodBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodBeginDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			else  if((!( postingPeriodEndDates.toString().equals("")))&& ((postingPeriodBeginDates.toString().equals("")))){
				if (isAnd == true) {
					query.append("  and ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d')  ");
				} else {
					query.append(" ( (DATE_FORMAT(a.recPostDate,'%Y-%m-%d') ");
				}
				if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + postingPeriodEndDates + "')");
				} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + postingPeriodEndDates + "')");
				} else {
					
				}
				 query.append("  or  (DATE_FORMAT(a.payPostDate,'%Y-%m-%d')");
				 if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
						query.append("<=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else if (periodEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
						query.append(">=");
						query.append("'" + postingPeriodEndDates + "') )");
					} else {
						
					}
				 isAnd = true;
			}
			*/}
			 
			if (invoicingBeginDates.toString().equals("") ) {

			} else {/*
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingBeginDates + "')");
				} else if (invoicingBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingBeginDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			*/}

			if (invoicingEndDates.toString().equals("") ) {

			} else {/*
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.receivedInvoiceDate,'%Y-%m-%d')");
				}

				if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + invoicingEndDates + "')");
				} else if (invoicingEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + invoicingEndDates + "')");
				} else {
					//query.append("  and (DATE_FORMAT(s.createdon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"') or DATE_FORMAT(s.updatedon,'%Y-%m-%d') BETWEEN ('"+beginDates+" ') and ('"+endDates+"'))"+" and s.corpId='"+sessionCorpID+"'");	
				}
				isAnd = true;
			*/}
			
			
			if (storageBeginDates.toString().equals("") ) {

			} else {/*
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeFrom,'%Y-%m-%d')");
				}

				if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageBeginDates + "')");
				} else if (storageBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageBeginDates + "')");
				} else { 
				}
				isAnd = true;
			*/}

			if (storageEndDates.toString().equals("") ) {

			} else {/*
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(a.storageDateRangeTo,'%Y-%m-%d')");
				}

				if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + storageEndDates + "')");
				} else if (storageEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + storageEndDates + "')");
				} else { 
				}
				isAnd = true;
			*/}
			
			
			if (revenueRecognitionBeginDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else if (revRecgBeginDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionBeginDates + "')");
				} else { 
				}
				isAnd = true;
			}

			if (revenueRecognitionEndDates.toString().equals("") ) {

			} else {
				if (isAnd == true) {
					query.append("  and  (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				} else {
					query.append("    (DATE_FORMAT(b.revenueRecognition,'%Y-%m-%d')");
				}

				if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else if (revRecgEndDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + revenueRecognitionEndDates + "')");
				} else {
					
				}
				isAnd = true;
			}
			

			if (companyCodeCondition == null || companyCodeCondition.equals("")) {

			} else {
				if (isAnd == true) {
					query.append(" and s.companyDivision");
				} else {
					query.append("  s.companyDivision");
				}
				if (companyCodeCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + companyCode + "'");
				} else {
					query.append("<>");
					query.append("'" + companyCode + "'");
				}
				isAnd = true;
			}
			if ( bookingDates.toString().equals("")) {
			} else {
				if (isAnd == true) {
					query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				} else {
					query.append(" (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				}
				if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + bookingDates + "')");
				} else if (bookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + bookingDates + "')");
				} else {

				}
				isAnd = true;
			}
			if ( toBookingDates.toString().equals("")) {
			} else {
				if (isAnd == true) {
					query.append(" and (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				} else {
					query.append(" (DATE_FORMAT(c.bookingDate,'%Y-%m-%d')");
				}
				if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Greater than Equal to".trim())) {
					query.append(">=");
					query.append("'" + toBookingDates + "')");
				} else if (toBookingDateCondition.toUpperCase().trim().equalsIgnoreCase("Less than Equal to".trim())) {
					query.append("<=");
					query.append("'" + toBookingDates + "')");
				} else {

				}
				isAnd = true;
			}
			
			if (carrierCondition.equals("") || carrierCondition == null) {
			} else {
				if (isAnd == true) {
					query.append(" and m.carrier");
				} else {
					query.append("  m.carrier");
				}
				if (carrierCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + carrier.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + carrier.trim() + "'");
				}
				isAnd = true;
			}
			if (driverIdCondition.equals("") || driverIdCondition == null) {
			} else {
				if (isAnd == true) {
					query.append(" and m.driverId");
				} else {
					query.append("  m.driverId");
				}
				if (driverIdCondition.toUpperCase().trim().equalsIgnoreCase("Equal to".trim())) {
					query.append("=");
					query.append("'" + driverId.trim() + "'");
				} else {
					query.append("<>");
					query.append("'" + driverId.trim() + "'");
				}
				isAnd = true;
			}
			
			if (activeStatus.equalsIgnoreCase("Active")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CLSD','CNCL','DWND','DWNLD') and s.status is not null ");
			} else if (activeStatus.equalsIgnoreCase("Inactive")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status  in ('CLSD','CNCL') and s.status is not null ");
			} else if (activeStatus.equalsIgnoreCase("All (Excl. Cancel)")) {
				query.append("   and s.corpId='" + sessionCorpID + "' and  s.status not in ('CNCL') and s.status is not null ");
			} else if (activeStatus.equalsIgnoreCase("All")) {
				query.append("  and s.corpId='" + sessionCorpID + "' ");
			}
			
		}
		activeUsDestGisOutput=serviceOrderManager.usDestGisOutput(query.toString(),sessionCorpID);
		int count = 0;
		StringBuilder ZipdestCode = new StringBuilder();
		Iterator it=activeUsDestGisOutput.iterator();
		  while(it.hasNext())
			{
				Object  obj= (Object)it.next();
					if((!((DTO)obj).getLatitude().toString().equalsIgnoreCase("")) && (!((DTO)obj).getLongitude().toString().equalsIgnoreCase(""))){
						count++;
					if (count > 1) ZipdestCode.append(","); else ZipdestCode.append("");
					ZipdestCode.append("{\"destinationZip\":\"" + ((DTO)obj).getDestinationZip() + "\", \"destZipCountNo\": \"" + ((DTO)obj).getDestZipCountNo() + "\", \"latitude\": \"" + ((DTO)obj).getLatitude() + "\", \"longitude\": \"" + ((DTO)obj).getLongitude() + "\"}");
					}
			}
			String prependStr = "{ \"count\":" + count + ", \"ZipdestCode\":[";
			String appendStr = "]}";
			String s=prependStr + ZipdestCode.toString() + appendStr;
			output= new ByteArrayInputStream(s.getBytes());
	}
	return output;	
	
	}
	
	
	public void createXMLLobbyDashboard(){ 
			  try
			  {
			        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			        DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
			        //creating a new instance of a DOM to build a DOM tree.
			        Document doc =   docBuilder.newDocument();
			        createXmlTree(doc,"USActivities.xml","USActivities"); 
			        Document doc1 =   docBuilder.newDocument();
			        createXmlTree(doc1,"globalActivities.xml","globalActivities"); 
			        /*Document doc2 =   docBuilder.newDocument();
			        createXmlTree(doc2,"USPackingLoading.xml","USActivities"); */
			        /*Document doc3 =   docBuilder.newDocument();
			        createXmlTree(doc3,"GlobalPackingLoading.xml","GlobalPackingLoading"); */
			        Document doc4 =   docBuilder.newDocument();
			        createXmlTree(doc4,"truckingLogistics.xml","truckingLogistics"); 
			        /*Document doc5 =   docBuilder.newDocument();
			        createXmlTree(doc5,"LogisiticsTruckingPickUps.xml","LogisiticsTruckingPickUps");*/
			        createZipFile();
			  }
			  catch(Exception e)
			  {
			        System.out.println(e);
			  } 
	}  	   
	String  uploadDir="";
	public void createXmlTree(Document doc,String fileName,String dashboardType) throws Exception {
		// US Deliveries
		int  totalcount=serviceOrderManager.getTotalUSDeliveries(dashboardType);
		List dataList=serviceOrderManager.getUSDeliveriesList(dashboardType);
		Iterator it = dataList.iterator();
		
        Element root = doc.createElement("map_xml");
        doc.appendChild(root);
        double count=0.00;
        Double percent=0.00;
        String code="";
        String lat="";
        String lon="";
        String zipCode="";
        String parameter="";
        if(dashboardType.equals("globalActivities")){
        	parameter="country";
        }else{
        	parameter="state";
        }
        while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			if (row[0] != null && (! row[0].toString().equals(""))) {
				count=Integer.parseInt(row[0].toString());
			} else {
				count=0;
			}
			if (row[1] != null && (! row[1].toString().equals(""))) {
				code=row[1].toString();
			} else {
				code="US";
			}
			if(parameter.trim().equals("state")){
				if (row[2] != null && (! row[2].toString().equals(""))) {
					zipCode=row[2].toString();
				} else {
					zipCode="";
				}	
			}
			if(!(zipCode.trim().equals(""))){ 
				List zipLonLat=serviceOrderManager.getZipLonLat(sessionCorpID,zipCode);
				if(zipLonLat!=null && (!(zipLonLat.isEmpty()))){
					refZipGeoCodeMap=(RefZipGeoCodeMap) zipLonLat.get(0);
				    lat=refZipGeoCodeMap.getLatitude();
				    lon=refZipGeoCodeMap.getLongitude();
				} else{ 
					List LonLat=serviceOrderManager.getLonLat(code,parameter,sessionCorpID);
					if(LonLat!=null && (!(LonLat.isEmpty()))){
					refMaster=(RefMaster) LonLat.get(0);
					lat=refMaster.getflex1();
					lon=refMaster.getflex2();
					}
				
				}
			
			}else{
				List LonLat=serviceOrderManager.getLonLat(code,parameter,sessionCorpID);
				if(LonLat!=null && (!(LonLat.isEmpty()))){
				refMaster=(RefMaster) LonLat.get(0);
				lat=refMaster.getflex1();
				lon=refMaster.getflex2();
				}
			}
		percent=count*100/totalcount;
		if(lat!=null && (!(lat.trim().equals("")))&& lon!=null && (!(lon.trim().equals("")))){
        Element child = doc.createElement("location");
        root.appendChild(child); 
        
        Element child1 = doc.createElement("lat");
        child.appendChild(child1);

        Text text1 = doc.createTextNode(lat);
        child1.appendChild(text1);
        
        Element child2 = doc.createElement("lon");
        child.appendChild(child2);

        Text text2 = doc.createTextNode(lon);
        child2.appendChild(text2);
        
        Element child3 = doc.createElement("percent");
        child.appendChild(child3);

        Text text3 = doc.createTextNode(percent.toString());
        child3.appendChild(text3); 
        } 
        }
        //TransformerFactory instance is used to create Transformer objects. 
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer(); 
        transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
        // create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();
        uploadDir = ServletActionContext.getServletContext().getRealPath("/resources");
		uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/XML" + "/");
		String filePath=""+uploadDir+fileName;
        File file = new File(filePath);
        BufferedWriter bw = new BufferedWriter
                      (new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        String  imageURL = filePath;
        imageURLList.add(filePath);  
      
    }

	public void createZipFile() throws IOException{
        HttpServletResponse response = getResponse();
        ByteArrayOutputStream byteOut=new ByteArrayOutputStream();
        ZipOutputStream zipOut=new ZipOutputStream(byteOut);
        ServletOutputStream out = response.getOutputStream();
             try {  
                  DataInputStream dataInputStream = null;
                  String zip="";
                  Iterator it = imageURLList.iterator();
                    while(it.hasNext()){
                          String filesLocation = (String)it.next();
                          if(!(new File(filesLocation).exists()))continue;
                          FileInputStream file = new FileInputStream(filesLocation);
                          dataInputStream = new DataInputStream(file); 
                            String fileName = "";
                            if(filesLocation.lastIndexOf("\\") != -1){
                                fileName = filesLocation.substring(filesLocation.lastIndexOf("\\")+1, filesLocation.length());
                            }
                            if(filesLocation.lastIndexOf("/") != -1){
                                fileName = filesLocation.substring(filesLocation.lastIndexOf("/")+1, filesLocation.length());
                            }
                            zipOut.putNextEntry(new ZipEntry(fileName));
                            int count;
                            while((count = dataInputStream.read()) != -1) {
                                zipOut.write(count);
                            }
                            file.close();
                            dataInputStream.close();
                 
                     }
                     zipOut.closeEntry();
                     zipOut.finish();
                     zip=byteOut.toString();
                     response.setContentType("application/zip");
                     response.setHeader("Content-Disposition","inline; filename=output.zip;");  
             		 response.setHeader("Pragma", "public");
             		 response.setHeader("Cache-Control", "max-age=0");
                     //out.println(zip);
                     out.write(byteOut.toByteArray());
                     out.flush();
                     zipOut.close();
                     byteOut.close();

              }
            
              catch (Exception e)
              {
                  response.setContentType("text/html");
                  out.println("<html><head><title>Error</title></head>");
                  out.println("<body><b>");
                  out.println("An error has occured while processing <br>");
                  out.println("Here is the exception: <br>"+e+"<br>");
                  e.printStackTrace(new PrintWriter(out));
                  out.println("</body>");
                  out.println("</html>");
             }
     }  
	
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public String getPublicPrivateFlag() {
		return publicPrivateFlag;
	}

	public void setPublicPrivateFlag(String publicPrivateFlag) {
		this.publicPrivateFlag = publicPrivateFlag;
	}

	public String getQueryName() {
		return queryName;
	}

	public void setQueryName(String queryName) {
		this.queryName = queryName;
	}

	public String getCompanyCodeCondition() {
		return companyCodeCondition;
	}

	public void setCompanyCodeCondition(String companyCodeCondition) {
		this.companyCodeCondition = companyCodeCondition;
	}

	public String getBillToCodeCondition() {
		return billToCodeCondition;
	}

	public void setBillToCodeCondition(String billToCodeCondition) {
		this.billToCodeCondition = billToCodeCondition;
	}

	public String getBookingCodeCondition() {
		return bookingCodeCondition;
	}

	public void setBookingCodeCondition(String bookingCodeCondition) {
		this.bookingCodeCondition = bookingCodeCondition;
	}

	public String getRoutingCondition() {
		return routingCondition;
	}

	public void setRoutingCondition(String routingCondition) {
		this.routingCondition = routingCondition;
	}

	public String getModeCondition() {
		return modeCondition;
	}

	public void setModeCondition(String modeCondition) {
		this.modeCondition = modeCondition;
	}

	public String getPackModeCondition() {
		return packModeCondition;
	}

	public void setPackModeCondition(String packModeCondition) {
		this.packModeCondition = packModeCondition;
	}

	public String getCoordinatorCondition() {
		return coordinatorCondition;
	}

	public void setCoordinatorCondition(String coordinatorCondition) {
		this.coordinatorCondition = coordinatorCondition;
	}

	public String getCommodityCondition() {
		return commodityCondition;
	}

	public void setCommodityCondition(String commodityCondition) {
		this.commodityCondition = commodityCondition;
	}

	public String getSalesmanCondition() {
		return salesmanCondition;
	}

	public void setSalesmanCondition(String salesmanCondition) {
		this.salesmanCondition = salesmanCondition;
	}

	public String getOACountryCondition() {
		return oACountryCondition;
	}

	public void setOACountryCondition(String countryCondition) {
		oACountryCondition = countryCondition;
	}

	public String getDACountryCondition() {
		return dACountryCondition;
	}

	public void setDACountryCondition(String countryCondition) {
		dACountryCondition = countryCondition;
	}

	public String getActualWgtCondition() {
		return actualWgtCondition;
	}

	public void setActualWgtCondition(String actualWgtCondition) {
		this.actualWgtCondition = actualWgtCondition;
	}

	public String getLoadTgtBeginDateCondition() {
		return loadTgtBeginDateCondition;
	}

	public void setLoadTgtBeginDateCondition(String loadTgtBeginDateCondition) {
		this.loadTgtBeginDateCondition = loadTgtBeginDateCondition;
	}

	public String getEndDateCondition() {
		return endDateCondition;
	}

	public void setEndDateCondition(String endDateCondition) {
		this.endDateCondition = endDateCondition;
	}

	public String getBeginDateCondition() {
		return beginDateCondition;
	}

	public void setBeginDateCondition(String beginDateCondition) {
		this.beginDateCondition = beginDateCondition;
	}

	public String getDeliveryTBeginDateCondition() {
		return deliveryTBeginDateCondition;
	}

	public void setDeliveryTBeginDateCondition(String deliveryTBeginDateCondition) {
		this.deliveryTBeginDateCondition = deliveryTBeginDateCondition;
	}

	public String getDeliveryTEndDateCondition() {
		return deliveryTEndDateCondition;
	}

	public void setDeliveryTEndDateCondition(String deliveryTEndDateCondition) {
		this.deliveryTEndDateCondition = deliveryTEndDateCondition;
	}

	public String getDeliveryActBeginDateCondition() {
		return deliveryActBeginDateCondition;
	}

	public void setDeliveryActBeginDateCondition(
			String deliveryActBeginDateCondition) {
		this.deliveryActBeginDateCondition = deliveryActBeginDateCondition;
	}

	public String getDeliveryActEndDateCondition() {
		return deliveryActEndDateCondition;
	}

	public void setDeliveryActEndDateCondition(String deliveryActEndDateCondition) {
		this.deliveryActEndDateCondition = deliveryActEndDateCondition;
	}

	public String getPayPostingEndDateCondition() {
		return payPostingEndDateCondition;
	}

	public void setPayPostingEndDateCondition(String payPostingEndDateCondition) {
		this.payPostingEndDateCondition = payPostingEndDateCondition;
	}

	public String getRevRecgBeginDateCondition() {
		return revRecgBeginDateCondition;
	}

	public void setRevRecgBeginDateCondition(String revRecgBeginDateCondition) {
		this.revRecgBeginDateCondition = revRecgBeginDateCondition;
	}

	public String getRevRecgEndDateCondition() {
		return revRecgEndDateCondition;
	}

	public void setRevRecgEndDateCondition(String revRecgEndDateCondition) {
		this.revRecgEndDateCondition = revRecgEndDateCondition;
	}

	public String getInvoicingBeginDateCondition() {
		return invoicingBeginDateCondition;
	}

	public void setInvoicingBeginDateCondition(String invoicingBeginDateCondition) {
		this.invoicingBeginDateCondition = invoicingBeginDateCondition;
	}

	public String getInvoicingEndDateCondition() {
		return invoicingEndDateCondition;
	}

	public void setInvoicingEndDateCondition(String invoicingEndDateCondition) {
		this.invoicingEndDateCondition = invoicingEndDateCondition;
	}

	public String getStorageBeginDateCondition() {
		return storageBeginDateCondition;
	}

	public void setStorageBeginDateCondition(String storageBeginDateCondition) {
		this.storageBeginDateCondition = storageBeginDateCondition;
	}

	public String getStorageEndDateCondition() {
		return storageEndDateCondition;
	}

	public void setStorageEndDateCondition(String storageEndDateCondition) {
		this.storageEndDateCondition = storageEndDateCondition;
	}

	public String getCreateDateCondition() {
		return createDateCondition;
	}

	public void setCreateDateCondition(String createDateCondition) {
		this.createDateCondition = createDateCondition;
	}

	public String getToCreateDateCondition() {
		return toCreateDateCondition;
	}

	public void setToCreateDateCondition(String toCreateDateCondition) {
		this.toCreateDateCondition = toCreateDateCondition;
	}

	public String getPeriodBeginDateCondition() {
		return periodBeginDateCondition;
	}

	public void setPeriodBeginDateCondition(String periodBeginDateCondition) {
		this.periodBeginDateCondition = periodBeginDateCondition;
	}

	public String getPeriodEndDateCondition() {
		return periodEndDateCondition;
	}

	public void setPeriodEndDateCondition(String periodEndDateCondition) {
		this.periodEndDateCondition = periodEndDateCondition;
	}

	public String getReceivedBeginDateCondition() {
		return receivedBeginDateCondition;
	}

	public void setReceivedBeginDateCondition(String receivedBeginDateCondition) {
		this.receivedBeginDateCondition = receivedBeginDateCondition;
	}

	public String getAccountInterface() {
		return accountInterface;
	}

	public void setAccountInterface(String accountInterface) {
		this.accountInterface = accountInterface;
	}

	public String getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}

	public String getActualVolumeCondition() {
		return actualVolumeCondition;
	}

	public void setActualVolumeCondition(String actualVolumeCondition) {
		this.actualVolumeCondition = actualVolumeCondition;
	}

	public String getActualVolumeType() {
		return actualVolumeType;
	}

	public void setActualVolumeType(String actualVolumeType) {
		this.actualVolumeType = actualVolumeType;
	}

	public String getActualWgtType() {
		return actualWgtType;
	}

	public void setActualWgtType(String actualWgtType) {
		this.actualWgtType = actualWgtType;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getBeginDateW() {
		return beginDateW;
	}

	public void setBeginDateW(Date beginDateW) {
		this.beginDateW = beginDateW;
	}

	public List getBillToCode() {
		return billToCode;
	}

	public void setBillToCode(List billToCode) {
		this.billToCode = billToCode;
	}

	public String getBillToCodeType() {
		return billToCodeType;
	}

	public void setBillToCodeType(String billToCodeType) {
		this.billToCodeType = billToCodeType;
	}

	public List getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(List bookingCode) {
		this.bookingCode = bookingCode;
	}

	public String getBookingCodeType() {
		return bookingCodeType;
	}

	public void setBookingCodeType(String bookingCodeType) {
		this.bookingCodeType = bookingCodeType;
	}

	public List getColumnList() {
		return columnList;
	}

	public void setColumnList(List columnList) {
		this.columnList = columnList;
	}

	public Map<String, String> getCommodit() {
		return commodit;
	}

	public void setCommodit(Map<String, String> commodit) {
		this.commodit = commodit;
	}

	public String getCommodityType() {
		return commodityType;
	}

	public void setCommodityType(String commodityType) {
		this.commodityType = commodityType;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public List getCompanyCodeList() {
		return companyCodeList;
	}

	public void setCompanyCodeList(List companyCodeList) {
		this.companyCodeList = companyCodeList;
	}

	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public String getCompanyDivisionFlag() {
		return companyDivisionFlag;
	}

	public void setCompanyDivisionFlag(String companyDivisionFlag) {
		this.companyDivisionFlag = companyDivisionFlag;
	}

	public String getConditionA1() {
		return conditionA1;
	}

	public void setConditionA1(String conditionA1) {
		this.conditionA1 = conditionA1;
	}

	public String getConditionA2() {
		return conditionA2;
	}

	public void setConditionA2(String conditionA2) {
		this.conditionA2 = conditionA2;
	}

	public String getConditionA3() {
		return conditionA3;
	}

	public void setConditionA3(String conditionA3) {
		this.conditionA3 = conditionA3;
	}

	public String getConditionA4() {
		return conditionA4;
	}

	public void setConditionA4(String conditionA4) {
		this.conditionA4 = conditionA4;
	}

	public String getConditionA5() {
		return conditionA5;
	}

	public void setConditionA5(String conditionA5) {
		this.conditionA5 = conditionA5;
	}

	public String getConditionC122() {
		return conditionC122;
	}

	public void setConditionC122(String conditionC122) {
		this.conditionC122 = conditionC122;
	}

	public String getConditionC79() {
		return conditionC79;
	}

	public void setConditionC79(String conditionC79) {
		this.conditionC79 = conditionC79;
	}

	public String getConditionC80() {
		return conditionC80;
	}

	public void setConditionC80(String conditionC80) {
		this.conditionC80 = conditionC80;
	}

	public String getConditionC81() {
		return conditionC81;
	}

	public void setConditionC81(String conditionC81) {
		this.conditionC81 = conditionC81;
	}

	public String getConditionC82() {
		return conditionC82;
	}

	public void setConditionC82(String conditionC82) {
		this.conditionC82 = conditionC82;
	}

	public Map<String, String> getCoord() {
		return coord;
	}

	public void setCoord(Map<String, String> coord) {
		this.coord = coord;
	}

	public String getCoordinatorType() {
		return coordinatorType;
	}

	public void setCoordinatorType(String coordinatorType) {
		this.coordinatorType = coordinatorType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getCreatedOnType() {
		return createdOnType;
	}

	public void setCreatedOnType(Date createdOnType) {
		this.createdOnType = createdOnType;
	}

	public String getDACountryType() {
		return dACountryType;
	}

	public void setDACountryType(String countryType) {
		dACountryType = countryType;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Map<String, String> getDcountry() {
		return dcountry;
	}

	public void setDcountry(Map<String, String> dcountry) {
		this.dcountry = dcountry;
	}

	public Date getDeliveryActBeginDate() {
		return deliveryActBeginDate;
	}

	public void setDeliveryActBeginDate(Date deliveryActBeginDate) {
		this.deliveryActBeginDate = deliveryActBeginDate;
	}

	public Date getDeliveryActEndDate() {
		return deliveryActEndDate;
	}

	public void setDeliveryActEndDate(Date deliveryActEndDate) {
		this.deliveryActEndDate = deliveryActEndDate;
	}

	public Date getDeliveryTBeginDate() {
		return deliveryTBeginDate;
	}

	public void setDeliveryTBeginDate(Date deliveryTBeginDate) {
		this.deliveryTBeginDate = deliveryTBeginDate;
	}

	public Date getDeliveryTEndDate() {
		return deliveryTEndDate;
	}

	public void setDeliveryTEndDate(Date deliveryTEndDate) {
		this.deliveryTEndDate = deliveryTEndDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getEndDateW() {
		return endDateW;
	}

	public void setEndDateW(Date endDateW) {
		this.endDateW = endDateW;
	}

	public ExtractQueryFileManager getExtractManager() {
		return extractManager;
	}

	public void setExtractManager(ExtractQueryFileManager extractManager) {
		this.extractManager = extractManager;
	}

	public ExtractQueryFile getExtractQueryFile() {
		return extractQueryFile;
	}

	public void setExtractQueryFile(ExtractQueryFile extractQueryFile) {
		this.extractQueryFile = extractQueryFile;
	}

	public String getExtractVal() {
		return extractVal;
	}

	public void setExtractVal(String extractVal) {
		this.extractVal = extractVal;
	}

	public Map<String, String> getHouse() {
		return house;
	}

	public void setHouse(Map<String, String> house) {
		this.house = house;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List getImfEstimate() {
		return imfEstimate;
	}

	public void setImfEstimate(List imfEstimate) {
		this.imfEstimate = imfEstimate;
	}

	public Date getInvoicingBeginDate() {
		return invoicingBeginDate;
	}

	public void setInvoicingBeginDate(Date invoicingBeginDate) {
		this.invoicingBeginDate = invoicingBeginDate;
	}

	public Date getInvoicingEndDate() {
		return invoicingEndDate;
	}

	public void setInvoicingEndDate(Date invoicingEndDate) {
		this.invoicingEndDate = invoicingEndDate;
	}

	public Date getInvPostingBeginDate() {
		return invPostingBeginDate;
	}

	public void setInvPostingBeginDate(Date invPostingBeginDate) {
		this.invPostingBeginDate = invPostingBeginDate;
	}

	public String getInvPostingBeginDateCondition() {
		return invPostingBeginDateCondition;
	}

	public void setInvPostingBeginDateCondition(String invPostingBeginDateCondition) {
		this.invPostingBeginDateCondition = invPostingBeginDateCondition;
	}

	public Date getInvPostingEndDate() {
		return invPostingEndDate;
	}

	public void setInvPostingEndDate(Date invPostingEndDate) {
		this.invPostingEndDate = invPostingEndDate;
	}

	public String getInvPostingEndDateCondition() {
		return invPostingEndDateCondition;
	}

	public void setInvPostingEndDateCondition(String invPostingEndDateCondition) {
		this.invPostingEndDateCondition = invPostingEndDateCondition;
	}

	public Map<String, String> getJobtype() {
		return jobtype;
	}

	public void setJobtype(Map<String, String> jobtype) {
		this.jobtype = jobtype;
	}

	public String getJobTypes() {
		return jobTypes;
	}

	public void setJobTypes(String jobTypes) {
		this.jobTypes = jobTypes;
	}

	public String getJobTypesCondition() {
		return jobTypesCondition;
	}

	public void setJobTypesCondition(String jobTypesCondition) {
		this.jobTypesCondition = jobTypesCondition;
	}

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}

	public Date getLoadTgtBeginDate() {
		return loadTgtBeginDate;
	}

	public void setLoadTgtBeginDate(Date loadTgtBeginDate) {
		this.loadTgtBeginDate = loadTgtBeginDate;
	}

	public Date getLoadTgtEndDate() {
		return loadTgtEndDate;
	}

	public void setLoadTgtEndDate(Date loadTgtEndDate) {
		this.loadTgtEndDate = loadTgtEndDate;
	}

	public String getLoadTgtEndDateCondition() {
		return loadTgtEndDateCondition;
	}

	public void setLoadTgtEndDateCondition(String loadTgtEndDateCondition) {
		this.loadTgtEndDateCondition = loadTgtEndDateCondition;
	}

	public List getMode() {
		return mode;
	}

	public void setMode(List mode) {
		this.mode = mode;
	}

	public String getModeType() {
		return modeType;
	}

	public void setModeType(String modeType) {
		this.modeType = modeType;
	}

	public List getMultiplejobType() {
		return multiplejobType;
	}

	public void setMultiplejobType(List multiplejobType) {
		this.multiplejobType = multiplejobType;
	}

	public String getOACountryType() {
		return oACountryType;
	}

	public void setOACountryType(String countryType) {
		oACountryType = countryType;
	}

	public Map<String, String> getOcountry() {
		return ocountry;
	}

	public void setOcountry(Map<String, String> ocountry) {
		this.ocountry = ocountry;
	}

	public String getPackModeType() {
		return packModeType;
	}

	public void setPackModeType(String packModeType) {
		this.packModeType = packModeType;
	}

	public Date getPayPostingBeginDate() {
		return payPostingBeginDate;
	}

	public void setPayPostingBeginDate(Date payPostingBeginDate) {
		this.payPostingBeginDate = payPostingBeginDate;
	}

	public String getPayPostingBeginDateCondition() {
		return payPostingBeginDateCondition;
	}

	public void setPayPostingBeginDateCondition(String payPostingBeginDateCondition) {
		this.payPostingBeginDateCondition = payPostingBeginDateCondition;
	}

	public Date getPayPostingEndDate() {
		return payPostingEndDate;
	}

	public void setPayPostingEndDate(Date payPostingEndDate) {
		this.payPostingEndDate = payPostingEndDate;
	}

	public Map<String, String> getPkmode() {
		return pkmode;
	}

	public void setPkmode(Map<String, String> pkmode) {
		this.pkmode = pkmode;
	}

	public Date getPostingPeriodBeginDate() {
		return postingPeriodBeginDate;
	}

	public void setPostingPeriodBeginDate(Date postingPeriodBeginDate) {
		this.postingPeriodBeginDate = postingPeriodBeginDate;
	}

	public Date getPostingPeriodEndDate() {
		return postingPeriodEndDate;
	}

	public void setPostingPeriodEndDate(Date postingPeriodEndDate) {
		this.postingPeriodEndDate = postingPeriodEndDate;
	}

	public List getQueryList() {
		return queryList;
	}

	public void setQueryList(List queryList) {
		this.queryList = queryList;
	}

	public List getQueryNameList() {
		return queryNameList;
	}

	public void setQueryNameList(List queryNameList) {
		this.queryNameList = queryNameList;
	}

	public Date getReceivedBeginDate() {
		return receivedBeginDate;
	}

	public void setReceivedBeginDate(Date receivedBeginDate) {
		this.receivedBeginDate = receivedBeginDate;
	}

	public Date getReceivedEndDate() {
		return receivedEndDate;
	}

	public void setReceivedEndDate(Date receivedEndDate) {
		this.receivedEndDate = receivedEndDate;
	}

	public String getReceivedEndDateCondition() {
		return receivedEndDateCondition;
	}

	public void setReceivedEndDateCondition(String receivedEndDateCondition) {
		this.receivedEndDateCondition = receivedEndDateCondition;
	}

	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public Date getRevenueRecognitionBeginDate() {
		return revenueRecognitionBeginDate;
	}

	public void setRevenueRecognitionBeginDate(Date revenueRecognitionBeginDate) {
		this.revenueRecognitionBeginDate = revenueRecognitionBeginDate;
	}

	public Date getRevenueRecognitionEndDate() {
		return revenueRecognitionEndDate;
	}

	public void setRevenueRecognitionEndDate(Date revenueRecognitionEndDate) {
		this.revenueRecognitionEndDate = revenueRecognitionEndDate;
	}

	public Map<String, String> getRouting() {
		return routing;
	}

	public void setRouting(Map<String, String> routing) {
		this.routing = routing;
	}

	public String getRoutingTypes() {
		return routingTypes;
	}

	public void setRoutingTypes(String routingTypes) {
		this.routingTypes = routingTypes;
	}

	public Map<String, String> getSale() {
		return sale;
	}

	public void setSale(Map<String, String> sale) {
		this.sale = sale;
	}

	public String getSalesmanType() {
		return salesmanType;
	}

	public void setSalesmanType(String salesmanType) {
		this.salesmanType = salesmanType;
	}

	public String getSelectCondition() {
		return selectCondition;
	}

	public void setSelectCondition(String selectCondition) {
		this.selectCondition = selectCondition;
	}

	public String getServiceW() {
		return serviceW;
	}

	public void setServiceW(String serviceW) {
		this.serviceW = serviceW;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Date getStorageBeginDate() {
		return storageBeginDate;
	}

	public void setStorageBeginDate(Date storageBeginDate) {
		this.storageBeginDate = storageBeginDate;
	}

	public Date getStoragedate() {
		return storagedate;
	}

	public void setStoragedate(Date storagedate) {
		this.storagedate = storagedate;
	}

	public Date getStorageEndDate() {
		return storageEndDate;
	}

	public void setStorageEndDate(Date storageEndDate) {
		this.storageEndDate = storageEndDate;
	}

	public Date getStoRecInvoiceDate() {
		return stoRecInvoiceDate;
	}

	public void setStoRecInvoiceDate(Date stoRecInvoiceDate) {
		this.stoRecInvoiceDate = stoRecInvoiceDate;
	}

	public String getTargetActualW() {
		return targetActualW;
	}

	public void setTargetActualW(String targetActualW) {
		this.targetActualW = targetActualW;
	}

	public Map<String, String> getTcktactn() {
		return tcktactn;
	}

	public void setTcktactn(Map<String, String> tcktactn) {
		this.tcktactn = tcktactn;
	}

	public Map<String, String> getTcktservc() {
		return tcktservc;
	}

	public void setTcktservc(Map<String, String> tcktservc) {
		this.tcktservc = tcktservc;
	}

	public Long getTimeDiff() {
		return timeDiff;
	}

	public void setTimeDiff(Long timeDiff) {
		this.timeDiff = timeDiff;
	}

	public Date getToCreateDate() {
		return toCreateDate;
	}

	public void setToCreateDate(Date toCreateDate) {
		this.toCreateDate = toCreateDate;
	}

	public String getTrack() {
		return track;
	}

	public void setTrack(String track) {
		this.track = track;
	}

	public String getWarehouseW() {
		return warehouseW;
	}

	public void setWarehouseW(String warehouseW) {
		this.warehouseW = warehouseW;
	}

	public void setCompanyDivisionManager(
			CompanyDivisionManager companyDivisionManager) {
		this.companyDivisionManager = companyDivisionManager;
	}

	public void setExtractColumnMgmtManager(
			ExtractColumnMgmtManager extractColumnMgmtManager) {
		this.extractColumnMgmtManager = extractColumnMgmtManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public List getColumnList2() {
		return columnList2;
	}

	public void setColumnList2(List columnList2) {
		this.columnList2 = columnList2;
	}

	public List getColumnList1() {
		return columnList1;
	}

	public void setColumnList1(List columnList1) {
		this.columnList1 = columnList1;
	}

	public List getColumnList3() {
		return columnList3;
	}

	public void setColumnList3(List columnList3) {
		this.columnList3 = columnList3;
	}

	public String getQuerycreatedBy() {
		return querycreatedBy;
	}

	public void setQuerycreatedBy(String querycreatedBy) {
		this.querycreatedBy = querycreatedBy;
	}

	public Date getQuerycreatedOn() {
		return querycreatedOn;
	}

	public void setQuerycreatedOn(Date querycreatedOn) {
		this.querycreatedOn = querycreatedOn;
	}

	public String getQuerycreatedOn1() {
		return querycreatedOn1;
	}

	public void setQuerycreatedOn1(String querycreatedOn1) {
		this.querycreatedOn1 = querycreatedOn1;
	}

	public Map<Integer, Integer> getPerMonthSchedulerList() {
		return perMonthSchedulerList;
	}

	public void setPerMonthSchedulerList(Map<Integer, Integer> perMonthSchedulerList) {
		this.perMonthSchedulerList = perMonthSchedulerList;
	}

	public Map<String, String> getPerWeekSchedulerList() {
		return perWeekSchedulerList;
	}

	public void setPerWeekSchedulerList(Map<String, String> perWeekSchedulerList) {
		this.perWeekSchedulerList = perWeekSchedulerList;
	}

	public Map<String, String> getQuerySchedulerList() {
		return querySchedulerList;
	}

	public void setQuerySchedulerList(Map<String, String> querySchedulerList) {
		this.querySchedulerList = querySchedulerList;
	}

	public String getPerMonthScheduler() {
		return perMonthScheduler;
	}

	public void setPerMonthScheduler(String perMonthScheduler) {
		this.perMonthScheduler = perMonthScheduler;
	}

	

	public String getPerWeekScheduler() {
		return perWeekScheduler;
	}

	public void setPerWeekScheduler(String perWeekScheduler) {
		this.perWeekScheduler = perWeekScheduler;
	}

	public String getQueryScheduler() {
		return queryScheduler;
	}

	public void setQueryScheduler(String queryScheduler) {
		this.queryScheduler = queryScheduler;
	} 
	public boolean isAutoGenerator() {
		return autoGenerator;
	}

	public void setAutoGenerator(boolean autoGenerator) {
		this.autoGenerator = autoGenerator;
	}

	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPerQuarterScheduler() {
		return perQuarterScheduler;
	}

	public void setPerQuarterScheduler(String perQuarterScheduler) {
		this.perQuarterScheduler = perQuarterScheduler;
	}

	public Map<String, String> getPerQuarterSchedulerList() {
		return perQuarterSchedulerList;
	}

	public void setPerQuarterSchedulerList(
			Map<String, String> perQuarterSchedulerList) {
		this.perQuarterSchedulerList = perQuarterSchedulerList;
	}

	public List getColumnList4() {
		return columnList4;
	}

	public void setColumnList4(List columnList4) {
		this.columnList4 = columnList4;
	}

	public List getColumnList5() {
		return columnList5;
	}

	public void setColumnList5(List columnList5) {
		this.columnList5 = columnList5;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUseremail() {
		return useremail;
	}

	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}

	public Map<String, String> getPeriodScheduler() {
		return periodScheduler;
	}

	public void setPeriodScheduler(Map<String, String> periodScheduler) {
		this.periodScheduler = periodScheduler;
	}

	public String getCreateDatePeriod() {
		return createDatePeriod;
	}

	public void setCreateDatePeriod(String createDatePeriod) {
		this.createDatePeriod = createDatePeriod;
	}

	public String getDeliveryActualDatePeriod() {
		return deliveryActualDatePeriod;
	}

	public void setDeliveryActualDatePeriod(String deliveryActualDatePeriod) {
		this.deliveryActualDatePeriod = deliveryActualDatePeriod;
	}

	public String getDeliveryTargetDatePeriod() {
		return deliveryTargetDatePeriod;
	}

	public void setDeliveryTargetDatePeriod(String deliveryTargetDatePeriod) {
		this.deliveryTargetDatePeriod = deliveryTargetDatePeriod;
	}

	public String getInvoicePostingDatePeriod() {
		return invoicePostingDatePeriod;
	}

	public void setInvoicePostingDatePeriod(String invoicePostingDatePeriod) {
		this.invoicePostingDatePeriod = invoicePostingDatePeriod;
	}

	public String getInvoicingdatePeriod() {
		return InvoicingdatePeriod;
	}

	public void setInvoicingdatePeriod(String invoicingdatePeriod) {
		InvoicingdatePeriod = invoicingdatePeriod;
	}

	public String getLoadActualDatePeriod() {
		return loadActualDatePeriod;
	}

	public void setLoadActualDatePeriod(String loadActualDatePeriod) {
		this.loadActualDatePeriod = loadActualDatePeriod;
	}

	public String getLoadTargetDatePeriod() {
		return loadTargetDatePeriod;
	}

	public void setLoadTargetDatePeriod(String loadTargetDatePeriod) {
		this.loadTargetDatePeriod = loadTargetDatePeriod;
	}

	public String getPayablePostingDatePeriod() {
		return payablePostingDatePeriod;
	}

	public void setPayablePostingDatePeriod(String payablePostingDatePeriod) {
		this.payablePostingDatePeriod = payablePostingDatePeriod;
	}

	public String getPostingPeriodDatePeriod() {
		return PostingPeriodDatePeriod;
	}

	public void setPostingPeriodDatePeriod(String postingPeriodDatePeriod) {
		PostingPeriodDatePeriod = postingPeriodDatePeriod;
	}

	public String getReceivedDatePeriod() {
		return receivedDatePeriod;
	}

	public void setReceivedDatePeriod(String receivedDatePeriod) {
		this.receivedDatePeriod = receivedDatePeriod;
	}

	public String getRevenueRecognitionPeriod() {
		return revenueRecognitionPeriod;
	}

	public void setRevenueRecognitionPeriod(String revenueRecognitionPeriod) {
		this.revenueRecognitionPeriod = revenueRecognitionPeriod;
	}

	public String getStorageDatePeriod() {
		return storageDatePeriod;
	}

	public void setStorageDatePeriod(String storageDatePeriod) {
		this.storageDatePeriod = storageDatePeriod;
	}

	/**
	 * @return the originGisOutput
	 */
	public String getOriginGisOutput() {
		return OriginGisOutput;
	}

	/**
	 * @param originGisOutput the originGisOutput to set
	 */
	public void setOriginGisOutput(String originGisOutput) {
		OriginGisOutput = originGisOutput;
	}

	/**
	 * @return the originGisOutputCount
	 */
	public String getOriginGisOutputCount() {
		return OriginGisOutputCount;
	}

	/**
	 * @param originGisOutputCount the originGisOutputCount to set
	 */
	public void setOriginGisOutputCount(String originGisOutputCount) {
		OriginGisOutputCount = originGisOutputCount;
	}

	/**
	 * @return the activeUsOriginGisOutput
	 */
	public List getActiveUsOriginGisOutput() {
		return activeUsOriginGisOutput;
	}

	/**
	 * @param activeUsOriginGisOutput the activeUsOriginGisOutput to set
	 */
	public void setActiveUsOriginGisOutput(List activeUsOriginGisOutput) {
		this.activeUsOriginGisOutput = activeUsOriginGisOutput;
	}

	/**
	 * @return the stingZipCodeOutput
	 */
	public String getStingZipCodeOutput() {
		return stingZipCodeOutput;
	}

	/**
	 * @param stingZipCodeOutput the stingZipCodeOutput to set
	 */
	public void setStingZipCodeOutput(String stingZipCodeOutput) {
		this.stingZipCodeOutput = stingZipCodeOutput;
	}

	/**
	 * @return the activeUsDestGisOutput
	 */
	public List getActiveUsDestGisOutput() {
		return activeUsDestGisOutput;
	}

	/**
	 * @param activeUsDestGisOutput the activeUsDestGisOutput to set
	 */
	public void setActiveUsDestGisOutput(List activeUsDestGisOutput) {
		this.activeUsDestGisOutput = activeUsDestGisOutput;
	}

	public RefMaster getRefMaster() {
		return refMaster;
	}

	public void setRefMaster(RefMaster refMaster) {
		this.refMaster = refMaster;
	}

	public RefZipGeoCodeMap getRefZipGeoCodeMap() {
		return refZipGeoCodeMap;
	}

	public void setRefZipGeoCodeMap(RefZipGeoCodeMap refZipGeoCodeMap) {
		this.refZipGeoCodeMap = refZipGeoCodeMap;
	}

	public Date getEmailDateTime() {
		return emailDateTime;
	}

	public void setEmailDateTime(Date emailDateTime) {
		this.emailDateTime = emailDateTime;
	}

	public String getEmailMessage() {
		return emailMessage;
	}

	public void setEmailMessage(String emailMessage) {
		this.emailMessage = emailMessage;
	}

	public String getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}

	public String getLastEmailSent() {
		return lastEmailSent;
	}

	public void setLastEmailSent(String lastEmailSent) {
		this.lastEmailSent = lastEmailSent;
	}

	public String getEmailDateTimeString() {
		return emailDateTimeString;
	}

	public void setEmailDateTimeString(String emailDateTimeString) {
		this.emailDateTimeString = emailDateTimeString;
	}

	public List getDataExtractMailInfo() {
		return dataExtractMailInfo;
	}

	public void setDataExtractMailInfo(List dataExtractMailInfo) {
		this.dataExtractMailInfo = dataExtractMailInfo;
	}

	public String getMailStatusReturn() {
		return mailStatusReturn;
	}

	public void setMailStatusReturn(String mailStatusReturn) {
		this.mailStatusReturn = mailStatusReturn;
	}

	public String getDatePeriod() {
		return datePeriod;
	}

	public void setDatePeriod(String datePeriod) {
		this.datePeriod = datePeriod;
	}

	/**
	 * @return the modifiedby
	 */
	public String getModifiedby() {
		return modifiedby;
	}

	/**
	 * @param modifiedby the modifiedby to set
	 */
	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Date getToBookingDate() {
		return toBookingDate;
	}

	public void setToBookingDate(Date toBookingDate) {
		this.toBookingDate = toBookingDate;
	}

	public String getBookingDatePeriod() {
		return bookingDatePeriod;
	}

	public void setBookingDatePeriod(String bookingDatePeriod) {
		this.bookingDatePeriod = bookingDatePeriod;
	}

	public String getToBookingDateCondition() {
		return toBookingDateCondition;
	}

	public void setToBookingDateCondition(String toBookingDateCondition) {
		this.toBookingDateCondition = toBookingDateCondition;
	}

	public String getBookingDateCondition() {
		return bookingDateCondition;
	}

	public void setBookingDateCondition(String bookingDateCondition) {
		this.bookingDateCondition = bookingDateCondition;
	}

	public String getCarrierCondition() {
		return carrierCondition;
	}

	public void setCarrierCondition(String carrierCondition) {
		this.carrierCondition = carrierCondition;
	}

	public String getDriverIdCondition() {
		return driverIdCondition;
	}

	public void setDriverIdCondition(String driverIdCondition) {
		this.driverIdCondition = driverIdCondition;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public String getAgentParentForPortal() {
		return agentParentForPortal;
	}

	public void setAgentParentForPortal(String agentParentForPortal) {
		this.agentParentForPortal = agentParentForPortal;
	}

	public String getSalesPerson() {
		return salesPerson;
	}

	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}

	public boolean isArmstrongPortal() {
		return armstrongPortal;
	}

	public void setArmstrongPortal(boolean armstrongPortal) {
		this.armstrongPortal = armstrongPortal;
	}

	public String getAccountCodeCondition() {
		return accountCodeCondition;
	}

	public void setAccountCodeCondition(String accountCodeCondition) {
		this.accountCodeCondition = accountCodeCondition;
	}

	public String getAccountCodeType() {
		return accountCodeType;
	}

	public void setAccountCodeType(String accountCodeType) {
		this.accountCodeType = accountCodeType;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Date getToUpdateDate() {
		return toUpdateDate;
	}

	public void setToUpdateDate(Date toUpdateDate) {
		this.toUpdateDate = toUpdateDate;
	}

	public String getUpdateDateCondition() {
		return updateDateCondition;
	}

	public void setUpdateDateCondition(String updateDateCondition) {
		this.updateDateCondition = updateDateCondition;
	}

	public String getToUpdateDateCondition() {
		return toUpdateDateCondition;
	}

	public void setToUpdateDateCondition(String toUpdateDateCondition) {
		this.toUpdateDateCondition = toUpdateDateCondition;
	}

	public String getUpdateDatePeriod() {
		return updateDatePeriod;
	}

	public void setUpdateDatePeriod(String updateDatePeriod) {
		this.updateDatePeriod = updateDatePeriod;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public boolean isExtractFileAudit() {
		return extractFileAudit;
	}

	public void setExtractFileAudit(boolean extractFileAudit) {
		this.extractFileAudit = extractFileAudit;
	}

	public ExtractedFileLog getExtractedFileLog() {
		return extractedFileLog;
	}

	public void setExtractedFileLog(ExtractedFileLog extractedFileLog) {
		this.extractedFileLog = extractedFileLog;
	}

	public void setExtractedFileLogManager(
			ExtractedFileLogManager extractedFileLogManager) {
		this.extractedFileLogManager = extractedFileLogManager;
	}

	public String getConsultantCondition() {
		return consultantCondition;
	}

	public void setConsultantCondition(String consultantCondition) {
		this.consultantCondition = consultantCondition;
	}

	public String getConsultantType() {
		return consultantType;
	}

	public void setConsultantType(String consultantType) {
		this.consultantType = consultantType;
	}

	public String getOriginAgentCondition() {
		return originAgentCondition;
	}

	public void setOriginAgentCondition(String originAgentCondition) {
		this.originAgentCondition = originAgentCondition;
	}

	public String getDestinationAgentCondition() {
		return destinationAgentCondition;
	}

	public void setDestinationAgentCondition(String destinationAgentCondition) {
		this.destinationAgentCondition = destinationAgentCondition;
	}

	public String getOriginAgentCodeType() {
		return originAgentCodeType;
	}

	public void setOriginAgentCodeType(String originAgentCodeType) {
		this.originAgentCodeType = originAgentCodeType;
	}

	public String getDestinationAgentCodeType() {
		return destinationAgentCodeType;
	}

	public void setDestinationAgentCodeType(String destinationAgentCodeType) {
		this.destinationAgentCodeType = destinationAgentCodeType;
	}

	

}