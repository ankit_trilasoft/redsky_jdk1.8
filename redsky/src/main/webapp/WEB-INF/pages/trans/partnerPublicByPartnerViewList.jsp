<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="partnerPublicList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerPublicList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
<script language="javascript" type="text/javascript">
function clear_fields(){
	     document.forms['partnerListForm'].elements['partnerPublic.aliasName'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.lastName'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.firstName'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.partnerCode'].value = '';
		document.forms['partnerListForm'].elements['countryCodeSearch'].value = '';
		document.forms['partnerListForm'].elements['stateSearch'].value = '';
		document.forms['partnerListForm'].elements['countrySearch'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.status'].value = '';
		document.forms['partnerListForm'].elements['partnerPublic.isPrivateParty'].checked = false;
		document.forms['partnerListForm'].elements['partnerPublic.isAccount'].checked = false;
		document.forms['partnerListForm'].elements['partnerPublic.isAgent'].checked = false;
		document.forms['partnerListForm'].elements['partnerPublic.isVendor'].checked = false;
		document.forms['partnerListForm'].elements['partnerPublic.isCarrier'].checked = false;
		document.forms['partnerListForm'].elements['partnerPublic.isOwnerOp'].checked = false;
		document.forms['partnerListForm'].elements['isIgnoreInactive'].checked = false;
		document.forms['partnerListForm'].elements['partnerPrivate.extReference'].value = '';
}
function activBtn(){
	document.forms['addPartner'].elements['addBtn'].disabled = false;
}

function seachValidate(){
		var pp =  document.forms['partnerListForm'].elements['partnerPublic.isPrivateParty'].checked;
		var ac =  document.forms['partnerListForm'].elements['partnerPublic.isAccount'].checked;
		var ag =  document.forms['partnerListForm'].elements['partnerPublic.isAgent'].checked;
		var vd =  document.forms['partnerListForm'].elements['partnerPublic.isVendor'].checked;
		var cr =  document.forms['partnerListForm'].elements['partnerPublic.isCarrier'].checked;
		var oo =  document.forms['partnerListForm'].elements['partnerPublic.isOwnerOp'].checked;
		if (pp == false && ac == false && ag ==false && vd == false && cr == false && oo == false){
			alert('Please select atleast one partnerPublic type.');
			return false;
		} 
}
</script>
<style>
span.pagelinks {
display:block;
margin-bottom:0px;
!margin-bottom:2px;
margin-top:-10px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:99%;
!width:98%;
font-size:0.85em;
}

.beta {color:#ee0909;font-style:italic;font-size:11px;font-family:arial,verdana;font-weight:bold;}

.radiobtn {
margin:0px 0px 2px 0px;
!padding-bottom:5px;
!margin-bottom:5px;
}

form {
margin-top:-10px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;

}
</style>
</head>
<c:set var="buttons">     
    <input type="button" class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/editPartnerPublic.html"/>'" value="<fmt:message key="button.add"/>"/>         
</c:set> 
 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px; !margin-bottom:10px;" align="top" method="" key="button.search" onclick="return seachValidate();"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/> 
</c:set>   

<s:form id="partnerListForm" action="searchPartnerPublicByPartnerView" method="post" >  
<div id="layer5" style="width:100%">

<div id="newmnav">
	  <ul>
	  	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>	  	
				<li><a href="geoCodeMaps.html?partnerOption=notView" ><span>Geo Search</span></a></li>					  	
	  </ul>
</div>
<div class="spnblk" style="margin-top:-10px;">&nbsp;</div> 
<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:10px;!margin-top:-5px;  "><span></span></div>
<div class="center-content">
<table class="table" border="0" style="width:99%;">
	<thead>
		<tr>
			<th><fmt:message key="partner.partnerCode"/></th>
			<th>External Ref.</th>
			<th><fmt:message key="partner.firstName"/></th>
			<th>Last/Company Name</th>
			<th>Alias Name</th>
			<th>Country Code</th>
			<th>Country Name</th>
			<th><fmt:message key="partner.billingState"/></th>
			<th><fmt:message key="partner.status"/></th>
		</tr>
	</thead>	
	<tbody>
		<tr>
			<td><s:textfield name="partnerPublic.partnerCode" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="partnerPrivate.extReference" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="partnerPublic.firstName" size="15" cssClass="input-text" /></td>
			<td><s:textfield name="partnerPublic.lastName" size="15" cssClass="input-text" /></td>
			<td><s:textfield name="partnerPublic.aliasName" size="15" cssClass="input-text" /></td>
			<td><s:textfield name="countryCodeSearch" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="countrySearch" size="14" cssClass="input-text"/></td>
			<td><s:textfield name="stateSearch" size="5" cssClass="input-text"/></td>
			<td><s:select cssClass="list-menu" name="partnerPublic.status" list="%{partnerStatus}" cssStyle="width:80px" headerKey="" headerValue=""/></td>
		</tr>
		<tr>
			<td colspan="10">
				<table style="margin:0px; padding:0px;">
					<tbody>
						<tr>
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isPrivateParty}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:none;">Private party<s:checkbox key="partnerPublic.isPrivateParty" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>				
							
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isAccount}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:none;">Accounts<s:checkbox key="partnerPublic.isAccount" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="12"/></td>
							
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isAgent}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="right" class="listwhitetext" width="100px" style="border:none;">Agents<s:checkbox key="partnerPublic.isAgent" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="13"/></td>
														
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isVendor}">
							<c:set var="ischecked" value="true"/>
							</c:if>					
							<td align="center" class="listwhitetext" width="100px" style="border:none;">Vendors<s:checkbox key="partnerPublic.isVendor" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="14"/></td>
															
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isCarrier}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext" width="100px" style="border:none;">Carriers<s:checkbox key="partnerPublic.isCarrier" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="15"/></td>
									
							<c:set var="ischecked" value="false"/>
							<c:if test="${partnerPublic.isOwnerOp}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext" width="100px" style="border:none;">Owner Ops<s:checkbox key="partnerPublic.isOwnerOp" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
						
							<c:set var="ischecked" value="false"/>
							<c:if test="${isIgnoreInactive}">
							<c:set var="ischecked" value="true"/>
							</c:if>						
							<td align="center" class="listwhitetext" width="110px" style="border:none;">Ignore Inactive<s:checkbox key="isIgnoreInactive" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
							
							<td width="130px" style="border:none;"><c:out value="${searchbuttons}" escapeXml="false"/></td>
						</tr>
					</tbody>	
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
</s:form>
<div id="layer1" style="width:100%">
<div id="otabs" style="margin-top:-20px; ">
	<ul>
		<li><a class="current"><span>Partner List</span></a></li>
	</ul>
</div><div class="spnblk">&nbsp;</div> 


<s:set name="partnerPublicList" value="partnerPublicList" scope="request"/>  
<display:table name="partnerPublicList" class="table" requestURI="" id="partnerPublicLists" export="false" defaultsort="2" pagesize="10" style="width:99%;margin-left:5px;">  		
	<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode" paramId="id" paramProperty="id" />   
	<display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerPublicLists.firstName} ${partnerPublicLists.lastName}" /></display:column>		
	<display:column property="aliasName" sortable="true" title="Alias Name" style="width:65px"/>
	<display:column property="extReference" sortable="true" titleKey="partner.extReference" style="width:65px"/>
	<display:column title="PParty" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isPrivateParty == true}">  
    		<a href="editPartnerPublic.html?id=${partnerPublicLists.id}&partnerType=PP"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Account" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isAccount == true}">  
    		<a href="editPartnerPublic.html?id=${partnerPublicLists.id}&partnerType=AC"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Agent" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isAgent == true}">  
    		<a href="editPartnerPublic.html?id=${partnerPublicLists.id}&partnerType=AG"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Carrier" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isCarrier == true}">  
    		<a href="editPartnerPublic.html?id=${partnerPublicLists.id}&partnerType=CR"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Vendor" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isVendor == true}">  
    		<a href="editPartnerPublic.html?id=${partnerPublicLists.id}&partnerType=VN"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Owner Ops" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isOwnerOp == true}">  
    		<a href="editPartnerPublic.html?id=${partnerPublicLists.id}&partnerType=OO"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>			     
	
	<display:column property="countryName" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
	<display:column property="stateName" sortable="true" titleKey="partner.billingState" style="width:65px"/>
	<display:column property="cityName" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:100px"/>
    
    <display:setProperty name="paging.banner.item_name" value="Partner Public"/>   
    <display:setProperty name="paging.banner.items_name" value="Partner Public"/>
       
  	<display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>
</div>
<table height="30px" class="mainDetailTable" cellpadding="0" cellspacing="0" style="padding:1px;width: 100%;">
	<tbody>
		<s:form id="addPartner" action="editPartnerPublic.html" method="post" >  
			<tr class="colored_bg" height="30px">
				<td class="listwhitetext-hcrew" style="padding-left:20px;!padding-left:5px;font-size:12px; !padding-bottom:8px;">Add New Partner :
					<s:radio name="partnerType" list="%{actionType}" onclick="activBtn();"  />
				   	<s:submit name="addBtn" cssClass="cssbutton" cssStyle="width:125px; height:25px; margin-left:50px;" align="top" value=" Add Partner"/>   
				</td>
			</tr>	
		</s:form>
	</tbody>
</table>
<c:set var="isTrue" value="false" scope="session"/>
<script type="text/javascript">
try{
	document.forms['addPartner'].elements['addBtn'].disabled = true;
}
catch(e){}
</script>