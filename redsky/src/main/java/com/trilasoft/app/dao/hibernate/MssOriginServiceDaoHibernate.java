package com.trilasoft.app.dao.hibernate;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.MssOriginServiceDao;
import com.trilasoft.app.model.MssOriginService;

public class MssOriginServiceDaoHibernate extends GenericDaoHibernate<MssOriginService, Long>implements MssOriginServiceDao{
	public MssOriginServiceDaoHibernate() {
		super(MssOriginService.class);
	}
}
