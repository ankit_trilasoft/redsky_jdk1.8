package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.CostingDao;
import com.trilasoft.app.model.Costing;



public class CostingDaoHibernate extends GenericDaoHibernate<Costing, Long> implements CostingDao {   
	//private List charges;
    public CostingDaoHibernate() {   
        super(Costing.class);   
    }   
	public List findMaxId() {
		
		return getHibernateTemplate().find("select max(id) from Costing");
	}
    public List checkById(Long id) {
    	return getHibernateTemplate().find("select id from Costing where id=?",id);
    }
    public int genrateInvoiceNumber(Long newInvoiceNumber,String shipNumber) {
    	//System.out.println("\n\n receivable record are :\t"+newInvoiceNumber+"\t"+shipNumber+"\n\n");
    	return getHibernateTemplate().bulkUpdate("update Costing set invoiceNumber="+newInvoiceNumber+",invoiceDate=Now() where shipNumber="+shipNumber+" and invoiceNumber='' and amount <> 0");
    }
    public List findInvoiceNumber()
    {
    	return getHibernateTemplate().find("select max(invoiceNumber) from Costing");
    }
	
}