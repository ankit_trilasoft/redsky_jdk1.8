<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>

<tr>
	<td height="10" width="100%" align="left" >
	 <div  onClick="javascript:animatedcollapse.toggle('sec')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Secondary Billing
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center" >&nbsp;</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
	   <div id="sec">
		<table class="detailTabLabel" border="0" >
 		  <tbody>
 		
			<tr>
				<td align="right" width="120px" class="listwhitetext" width="145px">Bill To</td>
				<td align="left" width="125px"><s:textfield id="billingBillTo2CodeId" name="billing.billTo2Code" required="true" cssClass="input-text" cssStyle="width:65px;" maxlength="8" onchange="changeStatus();getBillTo2Code();secondaryRecVatWithVatGroup();" tabindex=""/><img class="openpopup" style="vertical-align:bottom;" width="17" height="20" onclick="assignSecBillToCode();openPopWindowSec(this);document.forms['billingForm'].elements['billing.billTo2Code'].focus();" id="openpopup2.img"  src="<c:url value='/images/open-popup.gif'/>" /></td>
				<td align="right" class="listwhitetext" width="100px">Bill&nbsp;To&nbsp;Name</td>
				<td align="left" colspan="0">
				<div style="float:left;width:222px">
				<div style="float:left;">
				<s:textfield name="billing.billTo2Name" id="billingBillTo2NameId" cssClass="input-text" onkeyup="findPartnerDetails('billingBillTo2NameId','billingBillTo2CodeId','billingBillTo2NameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true  or isCarrier=true )','${serviceOrder.companyDivision}',event);" onchange="findPartnerDetailsByName('billingBillTo2CodeId','billingBillTo2NameId');" cssStyle="width:189px" maxlength="250" tabindex="" onfocus="changeStatus();"  />
				<div id="billingBillTo2NameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</div>
				<div id="hidBBillTo2" style="vertical-align:middle; width:26px;float:left;"><a>&nbsp;<img style="margin-left:-4px;" class="openpopup" onclick="getPartnerAlert(document.forms['billingForm'].elements['billing.billTo2Code'].value,'hidBBillTo2');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
				</div>
				</td>
				
                <c:if test="${systemDefaultVatCalculation=='true'}">
                <c:choose>
                <c:when test="${!trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup && networkAgent }">
                <td class="listwhitetext" width="" align="right">VAT&nbsp;Desc</td>
				<td class="listwhitetext" width="" ><configByCorp:customDropDown 	listType="map" list="${secEuVatList}" fieldValue="${billing.primaryVatCode}"
		attribute="id=billing.secondaryVatCode class=list-menu name=billing.secondaryVatCode style=width:110px  headerKey='' headerValue='' onchange='changeStatus()';  tabindex='' "/>
<%-- 				<s:select  cssClass="list-menu"  key="billing.secondaryVatCode" id="billing.secondaryVatCode" cssStyle="width:110px" list="%{euVatList}" headerKey="" onchange="changeStatus();" headerValue="" tabindex=""  disabled="true" /> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick=""/></td>
                </c:when>  
                <c:otherwise> 	
				<td class="listwhitetext" width="" align="right">VAT&nbsp;Desc</td>
				<td class="listwhitetext" width="" ><configByCorp:customDropDown 	listType="map" list="${secEuVatList}" fieldValue="${billing.secondaryVatCode}"
		attribute="id=billing.secondaryVatCode class=list-menu name=billing.secondaryVatCode style=width:110px  headerKey='' headerValue='' onchange='changeStatus()';  tabindex='' "/>
<%-- 				<s:select  cssClass="list-menu"  key="billing.secondaryVatCode" id="billing.secondaryVatCode" cssStyle="width:110px" list="%{euVatList}" headerKey="" onchange="changeStatus();" headerValue="" tabindex=""  /> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillRecAccVat('secondaryVatCode');"/></td>
				</c:otherwise>
				</c:choose>
				</c:if>
				<c:if test="${systemDefaultVatCalculation!='true'}">
				<s:hidden name="billing.secondaryVatCode"/>
				</c:if>
				<script type="text/javascript">
					setTimeout("showPartnerAlert('onload','${billing.billTo2Code}','hidBBillTo2')", 1000);
				</script>
				<td/>
			</tr>
		    <tr>
		    	<td align="right" class="listwhitetext" >Authorization&nbsp;Number</td>
		    	<td align="left"><s:textfield cssStyle="width:119px" name="billing.billTo2Authority" onkeydown="return onlyAlphaNumericAllowed(event);changeStatus();" required="true" cssClass="input-text" size="45" maxlength="35" tabindex=""/></td>
		    	<td align="right" class="listwhitetext" >Account&nbsp;Reference</td>
		    	<td align="left" ><s:textfield name="billing.billTo2Reference"  cssStyle="width:189px" onchange="changeStatus();" required="true" cssClass="input-text" size="20" maxlength="50" tabindex=""/></td>
		    	<td align="right" class="listwhitetext" width="64px">Pay&nbsp;Method</td>
		    	<td align="left">
		    	<configByCorp:customDropDown 	listType="map" list="${paytype}" fieldValue="${billing.billTo2Point}"
		attribute="id=billingForm_billing_billTo2Point class=list-menu name=billing.billTo2Point  style=width:110px  headerKey='' headerValue='' onchange='changeStatus();autoLinkCreditCard()'; tabindex='' "/></td>
		    	
		    	<%--  <s:select cssClass="list-menu" name="billing.billTo2Point" list="%{paytype}" cssStyle="width:110px"  headerKey="" headerValue="" onchange="changeStatus();autoLinkCreditCard();" tabindex=""/>--%>
		      <c:if test="${empty billing.id}">
							<td style="width:100px;" colspan="5" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
					</c:if>
					<c:if test="${not empty billing.id}">
							<c:choose>
								<c:when test="${countBillingSecondaryNotes == '0' || countBillingSecondaryNotes == '' || countBillingSecondaryNotes == null}">
								<td style="width:100px;" colspan="5" align="right"><img id="countBillingSecondaryNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingSecondary&imageId=countBillingSecondaryNotesImage&fieldId=countBillingSecondaryNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingSecondary&imageId=countBillingSecondaryNotesImage&fieldId=countBillingSecondaryNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td style="width:100px;" colspan="5" align="right"><img id="countBillingSecondaryNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingSecondary&imageId=countBillingSecondaryNotesImage&fieldId=countBillingSecondaryNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingSecondary&imageId=countBillingSecondaryNotesImage&fieldId=countBillingSecondaryNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
					</c:if>	    
		    </tr>
		     <tr>
	       <td align="right" class="listwhitetext" style="width:118px">Remarks </td>
	       <td align="left" class="listwhitetext" style="width:15px">
	       <s:textfield name="billing.invoiceRemarks2" cssClass="input-text" id="invoiceRemarks2" maxlength="40"/></td>        
	       </tr> 
		    <tr><td align="left" height="5px"></td></tr>
		    </tbody>
 		</table>
		 </div></td>
 		  </tr>  
 		  
 		  <tr>
	<td height="10" align="left" class="listwhitetext">
	 <div  onClick="javascript:animatedcollapse.toggle('private')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Private Party Billing
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center" >&nbsp;</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
		<div id="private">
		<table class="detailTabLabel" border="0">
 		  <tbody>
 		  	<tr>
 		  		<td align="right" width="120px" class="listwhitetext">Bill To</td>
				<td align="left" width="125px"><s:textfield name="billing.privatePartyBillingCode" id="billingBrivatePartyBillingCodeId" required="true" cssClass="input-text" cssStyle="width:65px" maxlength="8"  onchange="getBillTo3Code();changeStatus();privatePartyRecVatWithVatGroup();" tabindex=""/><img class="openpopup" style="vertical-align:bottom;" width="17" height="20" onclick="assignPrivateBillToCode();openPopWindowPrivate(this);document.forms['billingForm'].elements['billing.privatePartyBillingCode'].focus();" id="openpopup3.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
				<td align="right" class="listwhitetext" width="100px">Bill&nbsp;To&nbsp;Name</td>
				<td align="left" width="">
				<div style="float:left;width:235px">
				<div style="float:left;">
				<s:textfield name="billing.privatePartyBillingName" id="billingPrivatePartyBillingNameId" cssClass="input-text" cssStyle="width:190px" size="35" onkeyup="findPartnerDetails('billingPrivatePartyBillingNameId','billingBrivatePartyBillingCodeId','billingPrivatePartyBillingNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isCarrier=true )','${serviceOrder.companyDivision}',event);" onchange="findPartnerDetailsByName('billingBrivatePartyBillingCodeId','billingPrivatePartyBillingNameId');" maxlength="250" tabindex=""  onfocus="changeStatus();"/>
				<div id="billingPrivatePartyBillingNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</div>
				<div id="hidBBillTo3" style="vertical-align:middle; width:30px;float:left;"><a>&nbsp;<img style="margin-left:-4px;" class="openpopup" onclick="getPartnerAlert(document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value,'hidBBillTo3');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
				</div>
				</td>
                <c:if test="${systemDefaultVatCalculation=='true'}"> 
                <c:choose>
                <c:when test="${!trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup && networkAgent }">
               <td class="listwhitetext" width="52" align="right">VAT&nbsp;Desc</td>
				<td class="listwhitetext" width="" ><configByCorp:customDropDown 	listType="map" list="${privateEuVatList}" fieldValue="${billing.privatePartyVatCode}"
		attribute="id=billingForm_billing_privatePartyVatCode class=list-menu name=billing.privatePartyVatCode style=width:110px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/>
		    	
				
				
<%-- 				<s:select  cssClass="list-menu"  key="billing.privatePartyVatCode" id="billing.privatePartyVatCode" cssStyle="width:110px" list="%{euVatList}" headerKey="" onchange="changeStatus();" headerValue="" tabindex="" disabled="true"  /> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick=""/></td>
                </c:when>
                <c:otherwise>
				<td class="listwhitetext" width="52" align="right">VAT&nbsp;Desc</td>
				<td class="listwhitetext" width="" ><configByCorp:customDropDown 	listType="map" list="${privateEuVatList}" fieldValue="${billing.privatePartyVatCode}"
		attribute="id=billing.privatePartyVatCode class=list-menu name=billing.privatePartyVatCode style=width:110px  headerKey='' headerValue=''  onchange='changeStatus()'; tabindex='' "/>
<%-- 				
				<s:select  cssClass="list-menu"  key="billing.privatePartyVatCode" id="billing.privatePartyVatCode" cssStyle="width:110px" list="%{euVatList}" headerKey="" onchange="changeStatus();" headerValue="" tabindex=""  /> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillRecAccVat('privatePartyVatCode');"/></td>
				</c:otherwise>
				</c:choose>
				</c:if>
				<c:if test="${systemDefaultVatCalculation!='true'}">
				<s:hidden name="billing.privatePartyVatCode"/>
				</c:if>
				<script type="text/javascript">
					setTimeout("showPartnerAlert('onload','${billing.privatePartyBillingCode}','hidBBillTo3')", 2000);
				</script> 
			</tr>
			<tr> 
			<td align="right" class="listwhitetext" >Remarks </td>
	       <td align="left" class="listwhitetext" >
	       <s:textfield name="billing.invoiceRemarks3" cssClass="input-text" id="invoiceRemarks3" maxlength="40"/>
	       </td> 
			 <td align="right" class="listwhitetext" width="100px"><fmt:message key="billing.billTo2Point"/></td>
		    	<td align="left">
		    	<configByCorp:customDropDown 	listType="map" list="${paytype}" fieldValue="${billing.payMethod}"
		attribute="id=billingForm_billing_payMethod class=list-menu name=billing.payMethod  style=width:194px  headerKey='' headerValue='' onchange='changeStatus();autoLinkCreditCard()'; tabindex='' "/></td>
		    	
		    	<%--  <s:select cssClass="list-menu" name="billing.payMethod" list="%{paytype}" cssStyle="width:194px"  headerKey="" headerValue="" onchange="changeStatus();autoLinkCreditCard();" tabindex=""/></td>--%>
			   <c:if test="${empty billing.id}">
							<td width="268px" align="right" colspan="2"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
					</c:if>
					<c:if test="${not empty billing.id}">
							<c:choose>
								<c:when test="${countBillingPrivatePartyNotes == '0' || countBillingPrivatePartyNotes == '' || countBillingPrivatePartyNotes == null}">
								<td width="268px" align="right" colspan="2"><img id="countBillingPrivatePartyNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingPrivateParty&imageId=countBillingPrivatePartyNotesImage&fieldId=countBillingPrivatePartyNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingPrivateParty&imageId=countBillingPrivatePartyNotesImage&fieldId=countBillingPrivatePartyNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td width="268px" align="right" colspan="2"><img id="countBillingPrivatePartyNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingPrivateParty&imageId=countBillingPrivatePartyNotesImage&fieldId=countBillingPrivatePartyNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingPrivateParty&imageId=countBillingPrivatePartyNotesImage&fieldId=countBillingPrivatePartyNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
					</c:if>
 		  	</tr> 		   
	      </tbody>
 		</table>
 		</div>
 		</td>
 		 </tr>
 <c:if test="${systemDefaultVatCalculation=='true'}">
 <tr>
<td height="10" align="left" class="listwhitetext">		  
 <div  onClick="javascript:animatedcollapse.toggle('payvat')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Payable Vat Defaults
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center" >&nbsp;</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
<div id="payvat">
<table class="detailTabLabel" border="0" >
	  <tbody>	
	<tr>
	<c:if test="${serviceOrder.bookingAgentCode !='' && not empty serviceOrder.bookingAgentCode}">
	<td width="" align="right" class="listwhitetext">Booking&nbsp;Agent&nbsp;</td>
	<td><s:textfield cssClass="input-textUpper" key="serviceOrderBookingAgentCode" value="${serviceOrder.bookingAgentCode }" readonly="true" size="5" maxlength="10" tabindex=""/></td>
	<td><s:textfield  cssClass="input-textUpper" key="serviceOrder.bookingAgentName" readonly="true" size="31" tabindex="" /></td>
	<td class="listwhitetext" width="" align="right">VAT&nbsp;Desc</td>
	<td class="listwhitetext" width="" >
	
	<configByCorp:customDropDown 	listType="map" list="${bookingAgentVatList}" fieldValue="${billing.bookingAgentVatCode}"
		attribute="id=billing.bookingAgentVatCode class=list-menu name=billing.bookingAgentVatCode style=width:100px  headerKey='' headerValue='' onchange='checkPayAccVat(this,document.forms['billingForm'].elements['serviceOrder.bookingAgentCode'].value);changeStatus();' tabindex='' "/>
	
	<%-- <s:select  cssClass="list-menu"  key="billing.bookingAgentVatCode" id="billing.bookingAgentVatCode" cssStyle="width:100px" list="%{payVatList}" headerKey="" onchange="checkPayAccVat(this,document.forms['billingForm'].elements['serviceOrder.bookingAgentCode'].value);changeStatus();" headerValue="" tabindex=""  /> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillPayAccVat('bookingAgentVatCode');"/></td>
	</c:if>
	<c:if test="${trackingStatus.networkPartnerCode !='' && not empty trackingStatus.networkPartnerCode}">
	<td width="" align="right" class="listwhitetext">Network&nbsp;</td>
	<td><s:textfield cssClass="input-textUpper" key="trackingStatus.networkPartnerCode" readonly="true" size="5" maxlength="10" tabindex=""/></td>
	<td><s:textfield  cssClass="input-textUpper" key="trackingStatus.networkPartnerName" readonly="true" size="31" tabindex="" /></td>
	<td class="listwhitetext" width="" align="right">VAT&nbsp;Desc</td>
	<td class="listwhitetext" width="" >
	<configByCorp:customDropDown 	listType="map" list="${networkAgentVatList}" fieldValue="${billing.networkPartnerVatCode}"
		attribute="id=billing.networkPartnerVatCode class=list-menu name=billing.networkPartnerVatCode style=width:100px  headerKey='' headerValue='' onchange='checkPayAccVat(this,document.forms['billingForm'].elements['trackingStatus.networkPartnerCode'].value);changeStatus();' tabindex='' "/>
	
<img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillPayAccVat('networkPartnerVatCode');"/></td>
	</c:if>
	</tr>
	<tr>
	<c:if test="${trackingStatus.originAgentCode !='' && not empty trackingStatus.originAgentCode}">
	<td align="right" width="" class="listwhitetext">Origin Agent</td>
	<td align="left" width="70px"><s:textfield name="trackingStatus.originAgentCode" required="true" readonly="true" cssClass="input-textUpper" size="5" maxlength="8"  tabindex=""/></td>
	<td align="left" width="100px"><s:textfield name="trackingStatus.originAgent" required="true" readonly="true" cssClass="input-textUpper" size="31" maxlength="8"  tabindex=""/></td>
	<td class="listwhitetext" width="" align="right">VAT&nbsp;Desc</td>
	<td class="listwhitetext" width="" >
	
	<configByCorp:customDropDown 	listType="map" list="${origingAgentVatList}" fieldValue="${billing.originAgentVatCode}"
		attribute="id=billing.originAgentVatCode class=list-menu name=billing.originAgentVatCode style=width:100px  headerKey='' headerValue='' onchange='checkPayAccVat(this,document.forms['billingForm'].elements['trackingStatus.originAgentCode'].value);changeStatus();' tabindex='' "/>
	
<%-- 	<s:select  cssClass="list-menu"  key="billing.originAgentVatCode" id="billing.originAgentVatCode" cssStyle="width:100px" list="%{payVatList}" headerKey="" onchange="checkPayAccVat(this,document.forms['billingForm'].elements['trackingStatus.originAgentCode'].value);changeStatus();" headerValue="" tabindex=""  /> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillPayAccVat('originAgentVatCode');"/></td>		
	</c:if>
	<c:if test="${trackingStatus.destinationAgentCode !='' && not empty trackingStatus.destinationAgentCode}">
	<td align="right" width="" class="listwhitetext">Destination Agent</td>
	<td align="left" width="70px"><s:textfield name="trackingStatus.destinationAgentCode" required="true" readonly="true" cssClass="input-textUpper" size="5" maxlength="8"  tabindex=""/></td>
	<td align="left" width="100px"><s:textfield name="trackingStatus.destinationAgent" required="true" readonly="true" cssClass="input-textUpper" size="31" maxlength="8"  tabindex=""/></td>
	<td class="listwhitetext" width="" align="right">VAT&nbsp;Desc</td>
	<td class="listwhitetext" width="" ><configByCorp:customDropDown 	listType="map" list="${destinationAgentVatList}" fieldValue="${billing.destinationAgentVatCode}"
		attribute="id=billing.destinationAgentVatCode class=list-menu name= billing.destinationAgentVatCode style=width:100px  headerKey='' headerValue='' onchange='checkPayAccVat(this,document.forms['billingForm'].elements['trackingStatus.destinationAgentCode'].value);changeStatus();' tabindex='' "/>
<%-- 	
	<s:select  cssClass="list-menu"  key="billing.destinationAgentVatCode" id="billing.destinationAgentVatCode" cssStyle="width:100px" list="%{payVatList}" headerKey="" onchange="checkPayAccVat(this,document.forms['billingForm'].elements['trackingStatus.destinationAgentCode'].value);changeStatus();" headerValue="" tabindex=""  /> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillPayAccVat('destinationAgentVatCode');"/></td>		
	</c:if>
	</tr>
	<tr>
	<c:if test="${trackingStatus.originSubAgentCode !='' && not empty trackingStatus.originSubAgentCode}">
	<td align="right" class="listwhitetext" width="145px">Sub Origin Agent</td>
	<td align="left" width="70px"><s:textfield name="trackingStatus.originSubAgentCode" required="true" readonly="true" cssClass="input-textUpper" size="5" maxlength="8"  tabindex=""/></td>
	<td align="left" width="100px"><s:textfield name="trackingStatus.originSubAgent" required="true" readonly="true" cssClass="input-textUpper" size="31" maxlength="8"  tabindex=""/></td>
	<td class="listwhitetext" width="60" align="right">VAT&nbsp;Desc</td>
	<td class="listwhitetext" width="" >
	<configByCorp:customDropDown 	listType="map" list="${subOriginAgentVatList}" fieldValue="${billing.originSubAgentVatCode}"
		attribute="id=billing.originSubAgentVatCode class=list-menu name= billing.originSubAgentVatCode style=width:100px  headerKey='' headerValue='' onchange='checkPayAccVat(this,document.forms['billingForm'].elements['trackingStatus.originSubAgentCode'].value);changeStatus();' tabindex='' "/>
<%-- 	
<s:select  cssClass="list-menu"  key="billing.originSubAgentVatCode" id="billing.originSubAgentVatCode" cssStyle="width:100px" list="%{payVatList}" headerKey="" onchange="checkPayAccVat(this,document.forms['billingForm'].elements['trackingStatus.originSubAgentCode'].value);changeStatus();" headerValue="" tabindex=""  /> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillPayAccVat('originSubAgentVatCode');"/></td>		
	</c:if>
	<c:if test="${trackingStatus.destinationSubAgentCode !='' && not empty trackingStatus.destinationSubAgentCode}">
	<td align="right" class="listwhitetext" width="145px">Sub Destination Agent</td>
	<td align="left" width="70px"><s:textfield name="trackingStatus.destinationSubAgentCode" required="true" readonly="true" cssClass="input-textUpper" size="5" maxlength="8"  tabindex=""/></td>
	<td align="left" width="100px"><s:textfield name="trackingStatus.destinationSubAgent" required="true" readonly="true" cssClass="input-textUpper" size="31" maxlength="8"  tabindex=""/></td>
	<td class="listwhitetext" width="" align="right">VAT&nbsp;Desc</td>
	<td class="listwhitetext" width="" ><configByCorp:customDropDown 	listType="map" list="${subDestinationAgentVatList}" fieldValue="${billing.destinationSubAgentVatCode}"
		attribute="id=billing.destinationSubAgentVatCode class=list-menu name= billing.destinationSubAgentVatCode style=width:100px  headerKey='' headerValue='' onchange='checkPayAccVat(this,document.forms['billingForm'].elements['trackingStatus.destinationSubAgentCode'].value);changeStatus();' tabindex='' "/>
<%-- 	<s:select  cssClass="list-menu"  key="billing.destinationSubAgentVatCode" id="billing.destinationSubAgentVatCode" cssStyle="width:100px" list="%{payVatList}" headerKey="" onchange="checkPayAccVat(this,document.forms['billingForm'].elements['trackingStatus.destinationSubAgentCode'].value);changeStatus();" headerValue="" tabindex=""  /> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillPayAccVat('destinationSubAgentVatCode');"/></td>		
	</c:if>
	</tr>
	<tr>
	<c:if test="${trackingStatus.brokerCode !='' && not empty trackingStatus.brokerCode}">
	<td align="right" width="" class="listwhitetext">Broker</td>
	<td align="left" width="70px"><s:textfield name="trackingStatus.brokerCode" required="true" readonly="true" cssClass="input-textUpper" size="5" maxlength="8"  tabindex=""/></td>
	<td align="left" width="100px"><s:textfield name="trackingStatus.brokerName" required="true" readonly="true" cssClass="input-textUpper" size="31" maxlength="8"  tabindex=""/></td>
	<td class="listwhitetext" width="60" align="right">VAT&nbsp;Desc</td>
	
	<td class="listwhitetext" width="" >
	<configByCorp:customDropDown 	listType="map" list="${brokerAgentVatList}" fieldValue="${billing.brokerVatCode}"
		attribute="id=billing.brokerVatCode class=list-menu name= billing.brokerVatCode style=width:100px  headerKey='' headerValue='' onchange='checkPayAccVat(this,document.forms['billingForm'].elements['trackingStatus.brokerCode'].value);changeStatus();' tabindex='' "/>
<%-- 	
	<s:select  cssClass="list-menu"  key="billing.brokerVatCode" id="billing.brokerVatCode" cssStyle="width:100px" list="%{payVatList}" headerKey="" onchange="checkPayAccVat(this,document.forms['billingForm'].elements['trackingStatus.brokerCode'].value);changeStatus();" headerValue="" tabindex=""  /> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillPayAccVat('brokerVatCode');"/></td>		
	</c:if>
	<c:if test="${trackingStatus.forwarderCode !='' && not empty trackingStatus.forwarderCode}">
	<td align="right" width="" class="listwhitetext">Forwarder</td>
	<td align="left" width="70px"><s:textfield name="trackingStatus.forwarderCode" required="true" readonly="true" cssClass="input-textUpper" size="5" maxlength="8"  tabindex=""/></td>
	<td align="left" width="100px"><s:textfield name="trackingStatus.forwarder" required="true" readonly="true" cssClass="input-textUpper" size="31" maxlength="8"  tabindex=""/></td>
	<td class="listwhitetext" width="" align="right">VAT&nbsp;Desc</td>
	<td class="listwhitetext" width="" ><configByCorp:customDropDown 	listType="map" list="${forwarderAgentVatList}" fieldValue="${billing.forwarderVatCode}"
		attribute="id=billing.forwarderVatCode class=list-menu name=billing.forwarderVatCode style=width:100px  headerKey='' headerValue='' onchange='checkPayAccVat(this,document.forms['billingForm'].elements['trackingStatus.forwarderCode'].value);changeStatus();' tabindex='' "/>
<%-- 	
	<s:select  cssClass="list-menu"  key="billing.forwarderVatCode" id="billing.forwarderVatCode" cssStyle="width:100px" list="%{payVatList}" headerKey="" onchange="checkPayAccVat(this,document.forms['billingForm'].elements['trackingStatus.forwarderCode'].value);changeStatus();" headerValue="" tabindex=""  /> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillPayAccVat('forwarderVatCode');"/></td>		
	</c:if>
	</tr>
	<tr>
	<c:if test="${billing.vendorCode !='' && not empty billing.vendorCode}">
	<td align="right" width="" class="listwhitetext">Vendor Code</td>
	<td align="left" width="70px"><s:textfield name="billingVendorCode" value="${billing.vendorCode}" required="true" readonly="true" cssClass="input-textUpper" size="5" maxlength="8"  tabindex=""/></td>
	<td align="left" width="100px"><s:textfield name="billingVendorName" value="${billing.vendorName}" required="true" readonly="true" cssClass="input-textUpper" size="31" maxlength="8"  tabindex=""/></td>
	<td class="listwhitetext" width="60" align="right">VAT&nbsp;Desc</td>
	<td class="listwhitetext" width="" >
	<configByCorp:customDropDown 	listType="map" list="${venderlistpayVatList}" fieldValue="${billing.vendorCodeVatCode}"
		attribute="id=billing.vendorCodeVatCode class=list-menu name=billing.vendorCodeVatCode style=width:100px  headerKey='' headerValue='' onchange='checkPayAccVat(this,document.forms['billingForm'].elements['billingVendorCode'].value);changeStatus();' tabindex='' "/>
	
<%-- 	<s:select  cssClass="list-menu"  key="billing.vendorCodeVatCode" id="billing.vendorCodeVatCode" cssStyle="width:100px" list="%{payVatList}" headerKey="" onchange="checkPayAccVat(this,document.forms['billingForm'].elements['billingVendorCode'].value);changeStatus();" headerValue="" tabindex=""  /> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillPayAccVat('vendorCodeVatCode');"/></td>		
	</c:if>
	<c:if test="${billing.vendorCode1 !='' && not empty billing.vendorCode1}">
	<td align="right" width="" class="listwhitetext">Vendor Code</td>
	<td align="left" width="70px"><s:textfield name="billingVendorCode1" value="${billing.vendorCode1}" required="true" readonly="true" cssClass="input-textUpper" size="5" maxlength="8"  tabindex="" onfocus="changeStatus();"/></td>
	<td align="left" width="100px"><s:textfield name="billingVendorName1" value="${billing.vendorName1}" required="true" readonly="true" cssClass="input-textUpper" size="31" maxlength="8"  tabindex="" onfocus="changeStatus();"/></td>
	<td class="listwhitetext" width="60" align="right">VAT&nbsp;Desc</td>
	<td class="listwhitetext" width="" >
<configByCorp:customDropDown 	listType="map" list="${venderpaylistEuVatList}" fieldValue="${billing.vendorCodeVatCode1}"
		attribute="id=billing.vendorCodeVatCode1 class=list-menu name= billing.vendorCodeVatCode1 style=width:100px  headerKey='' headerValue='' onchange='checkPayAccVat(this,document.forms['billingForm'].elements['billingVendorCode1'].value);changeStatus();' tabindex='' "/>
	
<%-- 	<s:select  cssClass="list-menu"  key="billing.vendorCodeVatCode1" id="billing.vendorCodeVatCode1" cssStyle="width:100px" list="%{payVatList}" headerKey="" onchange="checkPayAccVat(this,document.forms['billingForm'].elements['billingVendorCode1'].value);changeStatus();" headerValue="" tabindex=""  /> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillPayAccVat('vendorCodeVatCode1');"/></td>		
	</c:if>
	</tbody>
	</table>
</div>
</td>
</tr>
</c:if>