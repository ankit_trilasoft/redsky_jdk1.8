<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<head>
<title><fmt:message key="extractColumnMgmtDetail.title" /></title>
<meta name="heading"
	content="<fmt:message key='extractColumnMgmtDetail.heading'/>" />
	
	<script type="text/javascript">
	function getTablesName() {
		var reportName = document.forms['addFieldForm'].elements['extractColumnMgmt.reportName'].value;
		var url ="findTableNameByReportName.html?ajax=1&decorator=simple&popup=true&reportName="+encodeURI(reportName);
		http5.open("GET", url, true);
		http5.onreadystatechange = handleHttpResponse121;
	    http5.send(null);
	}

	function handleHttpResponse121(){
		if (http5.readyState == 4){
                var results = http5.responseText
                results = results.trim();
                res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.split("@");
                targetElementVal=document.forms['addFieldForm'].elements['extractColumnMgmt.tableName'].value;
                targetElement = document.forms['addFieldForm'].elements['extractColumnMgmt.tableName'];
                targetElement.length = res.length;
				for(i=0;i<res.length;i++){
					if(res[i] == ''){
					document.forms['addFieldForm'].elements['extractColumnMgmt.tableName'].options[i].text = '';
					document.forms['addFieldForm'].elements['extractColumnMgmt.tableName'].options[i].value = '';
					}else{
					document.forms['addFieldForm'].elements['extractColumnMgmt.tableName'].options[i].text =res[i];
					document.forms['addFieldForm'].elements['extractColumnMgmt.tableName'].options[i].value =res[i];
					}
				}
				
				if(document.forms['addFieldForm'].elements['formStatus'].value == '1'){
				document.forms['addFieldForm'].elements['extractColumnMgmt.tableName'].value=targetElementVal;
				}
				else{
				document.forms['addFieldForm'].elements['extractColumnMgmt.tableName'].value='${extractColumnMgmt.tableName}';
				}
         }
}
	
	function getHTTPObject()
	{
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}
	
	var http5 = getHTTPObject();
	
	function checkBlank(){
		var val = document.forms['addFieldForm'].elements['extractColumnMgmt.displayName'].value;
		if(val.match(' ')){
			alert("Blank Space is not allowed.");
			document.forms['addFieldForm'].elements['extractColumnMgmt.displayName'].focus();
			//document.getElementById("myAnchor").focus();
			return false;
		}
	}
	
	function clear_fields(){
		document.forms['addFieldForm'].elements['extractColumnMgmt.reportName'].value = "";
		document.forms['addFieldForm'].elements['extractColumnMgmt.tableName'].value = "";
		document.forms['addFieldForm'].elements['extractColumnMgmt.fieldName'].value = "";
		document.forms['addFieldForm'].elements['extractColumnMgmt.displayName'].value = "";
		document.forms['addFieldForm'].elements['extractColumnMgmt.corpID'].value = "";
	}
	
	
	window.onload = function() {
		//var session_corp_id = document.getElementById("h_corpid_login").value;
		var sessionCorpId = document.getElementById("sessionCorpId").value;
		var corpid = document.getElementById("oldCorpId").value;
		if(sessionCorpId!="TSFT" && corpid=="TSFT"){
			document.getElementById("extractColumnMgmt.reportName").disabled = true;
			document.getElementById("extractColumnMgmt.tableName").disabled = true;
			document.getElementById("extractColumnMgmt.fieldName").readOnly = true;
			document.getElementById("extractColumnMgmt.displayName").readOnly = true;
			document.getElementById("mydiv").style.display="none";
		}
	}
	
	function submitFieldForm(){
		var report_name = document.forms['addFieldForm'].elements['extractColumnMgmt.reportName'].value;
		var table_name = document.forms['addFieldForm'].elements['extractColumnMgmt.tableName'].value;
		var field_name = document.forms['addFieldForm'].elements['extractColumnMgmt.fieldName'].value;
		var display_name = document.forms['addFieldForm'].elements['extractColumnMgmt.displayName'].value;
		if(report_name==null || report_name==""){
			alert("Please select Report Name.");
			document.forms['addFieldForm'].elements['extractColumnMgmt.reportName'].focus();
			return false;
		}
		if(table_name==null || table_name==""){
			alert("Please select Table Name.");
			document.forms['addFieldForm'].elements['extractColumnMgmt.tableName'].focus();
			return false;
		}
		if(field_name==null || field_name==""){
			alert("Please select Field Name.");
			document.forms['addFieldForm'].elements['extractColumnMgmt.fieldName'].focus();
			return false;
		}
		if(display_name==null || display_name==""){
			alert("Please select Display Name.");
			document.forms['addFieldForm'].elements['extractColumnMgmt.displayName'].focus();
			return false;
		}
		
		
		
		var sessionCorpId = document.getElementById("sessionCorpId").value;
		var corpid = document.getElementById("oldCorpId").value;
		if(sessionCorpId!="TSFT" && corpid=="TSFT"){
			document.forms['addFieldForm'].action= "columnManagementList.html?hitflag=trues";
		}else{
			document.forms['addFieldForm'].action= "saveColumnManagement.html?id=${id}&popup=false&decorator=popup";
			document.forms['addFieldForm'].submit();
		}
	}
	
	/* function validateForm(){
		var report_name = document.forms['addFieldForm'].elements['extractColumnMgmt.reportName'].value;
		var table_name = document.forms['addFieldForm'].elements['extractColumnMgmt.tableName'].value;
		var field_name = document.forms['addFieldForm'].elements['extractColumnMgmt.fieldName'].value;
		var display_name = document.forms['addFieldForm'].elements['extractColumnMgmt.displayName'].value;
		if(report_name==null || report_name==""){
			alert("Please select Report Name.");
			document.forms['addFieldForm'].elements['extractColumnMgmt.reportName'].focus();
			return false;
		}
		if(table_name==null || table_name==""){
			alert("Please select Table Name.");
			document.forms['addFieldForm'].elements['extractColumnMgmt.tableName'].focus();
			return false;
		}
		if(field_name==null || field_name==""){
			alert("Please select Field Name.");
			document.forms['addFieldForm'].elements['extractColumnMgmt.fieldName'].focus();
			return false;
		}
		if(display_name==null || display_name==""){
			alert("Please select Display Name.");
			document.forms['addFieldForm'].elements['extractColumnMgmt.displayName'].focus();
			return false;
		}
	} */
	</script>
</head>
<%-- <s:form name="addFieldForm" id="addFieldForm" action="saveColumnManagement" method="post"> --%>
<s:form name="addFieldForm" id="addFieldForm" action="" method="post">
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
	 <s:hidden name="extractColumnMgmt.id" />  
	<s:hidden name="extractColumnMgmt.columnSequenceNumber" />
	<c:set var="corp_Id" value="${sessionCorpID}" />
	<s:hidden name="oldCorpId" id="oldCorpId" value="${oldCorpId}" />
	<s:hidden name="sessionCorpId" id="sessionCorpId" value="${sessionCorpID}" />
	
	<div id="Layer1" style="width: 100%;">
	
		<div id="layer4" style="width: 100%;">
			<div id="newmnav">
				<ul>
					<li id="newmnav1" style="background: #FFF"><a class="current"><span>Field Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					<li><a href="columnManagementList.html"><span>Field Detail List</span></a></li>
				</ul>
			</div>
			<div class="spn">&nbsp;</div>
		</div>
		<div id="content" align="center">
			<div id="liquid-round-top">
				<div class="top" style="margin-top: 2px;">
					<span></span>
				</div>
				<div class="center-content">
					<table cellspacing="1" cellpadding="1" border="0">
						<tbody>
							<tr>
								<td align="left" class="listwhitetext">
									<table class="detailTabLabel" cellspacing="2" cellpadding="2"
										border="0">
										<tbody>
											<tr>
												<td align="left" height="5px"></td>
											</tr>
											<tr>
												<td align="right">Report Name<font color="red" size="2">&nbsp;*</font></td>
												<td align="right"><s:select	name="extractColumnMgmt.reportName" id="extractColumnMgmt.reportName" cssClass="list-menu" cssStyle="width:160px" headerKey="" headerValue="" list="%{reportNameList}" onchange="getTablesName();"></s:select></td>
												<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Table Name<font color="red" size="2">&nbsp;*</td>
												<td align="right"><s:select	name="extractColumnMgmt.tableName" id="extractColumnMgmt.tableName" cssClass="list-menu" cssStyle="width:100px" headerKey="" headerValue="" list="%{tableNameList}"></s:select></td>
												<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Field Name<font color="red" size="2">&nbsp;*</td>
												<td><s:textfield name="extractColumnMgmt.fieldName" required="true" id="extractColumnMgmt.fieldName" cssClass="input-text" maxlength="800" cssStyle="width:340px"/></td>
												<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Display Name<font color="red" size="2">&nbsp;*</td>
												<td align="right"><s:textfield name="extractColumnMgmt.displayName" id="extractColumnMgmt.displayName" cssClass="input-text" maxlength="100" onkeyup="checkBlank()" cssStyle="width:140px"/></td>
												<c:if test="${corp_Id == 'TSFT'}">
												<td align="right">&nbsp&nbsp&nbsp&nbsp&nbsp&nbspCorp Id</td>
												<td align="right"><s:select name="extractColumnMgmt.corpID" id="extractColumnMgmt.corpID" cssClass="list-menu" cssStyle="width:60px" headerKey="" headerValue="" list="%{corpIdList}"></s:select></td>
												<%-- <td align="right"><s:select id="corp_id" name="corp_id" value="${corp_id}" cssClass="list-menu" cssStyle="width:140px" headerKey="" headerValue="" list="%{corpIdList}"></s:select></td> --%>
												<%-- <td align="right">
												<select class="list-menu pr-f11" id="corp_id" name="corp_id"  >
												<option value=""></option>
												<c:forEach var="varcont" items="${corpIdList}" varStatus="loopStatus">
													<c:choose>
														<c:when test="${varcont == corp_id}">
															<c:set var="selectedInd" value=" selected"></c:set>
														</c:when>
														<c:otherwise>
															<c:set var="selectedInd" value=""></c:set>
														</c:otherwise>
													</c:choose>
														<option value="<c:out value='${varcont}' />" <c:out value='${selectedInd}' />>
															<c:out value="${varcont}"></c:out>
    													</option>
												</c:forEach>
												</select></td> --%>
												</c:if>
												</tr>
											<tr>
												<td align="left" height="5px"></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>

				</div>
				<div class="bottom-header">
					<span></span>
				</div>
			</div>
		</div>

		<table border="0">
			<tbody>
				<tr>
					<td align="left" class="listwhitetext" width="30px"></td>
					<td colspan="5"></td>
				</tr>
				<tr>
					<td align="left" class="listwhitetext" width="30px"></td>
					<td colspan="5"></td>
				</tr>
				<tr>
					<td align="left" class="listwhitetext" style="width: 75px"><b><fmt:message
								key='extractColumnMgmt.createdOn' /></b></td>
					<td valign="top"></td>
					
					<td style="width: 120px"><fmt:formatDate
							var="extractColumnMgmtCreatedOnFormattedValue"
							value="${extractColumnMgmt.createdOn}"
							pattern="${displayDateTimeEditFormat}" /> <s:hidden
							name="extractColumnMgmt.createdOn"
							value="${extractColumnMgmtCreatedOnFormattedValue}" /> <fmt:formatDate
							value="${extractColumnMgmt.createdOn}"
							pattern="${displayDateTimeFormat}" /></td>
					<td align="left" class="listwhitetext" style="width: 75px"><b><fmt:message
								key='extractColumnMgmt.createdBy' /></b></td>
					<c:if test="${not empty extractColumnMgmt.id}">
						<s:hidden name="extractColumnMgmt.createdBy" />
						<td style="width: 85px"><s:label name="createdBy"
								value="%{extractColumnMgmt.createdBy}" /></td>
					</c:if>
					<c:if test="${empty extractColumnMgmt.id}">
						<s:hidden name="extractColumnMgmt.createdBy"
							value="${pageContext.request.remoteUser}" />
						<td style="width: 100px"><s:label name="createdBy"
								value="${pageContext.request.remoteUser}" /></td>
					</c:if>
					<td align="left" class="listwhitetext" style="width: 75px"><b><fmt:message
								key='extractColumnMgmt.updatedOn' /></b></td>
					<fmt:formatDate var="extractColumnMgmtUpdatedOnFormattedValue"
						value="${extractColumnMgmt.updatedOn}"
						pattern="${displayDateTimeEditFormat}" />
					<s:hidden name="extractColumnMgmt.updatedOn"
						value="${extractColumnMgmtUpdatedOnFormattedValue}" />
					<td style="width: 120px"><fmt:formatDate
							value="${extractColumnMgmt.updatedOn}"
							pattern="${displayDateTimeFormat}" /></td>
					<td align="left" class="listwhitetext" style="width: 75px"><b><fmt:message
								key='extractColumnMgmt.updatedBy' /></b></td>
					<c:if test="${not empty extractColumnMgmt.id}">
						<s:hidden name="extractColumnMgmt.updatedBy" />
						<td style="width: 85px"><s:label name="updatedBy"
								value="%{extractColumnMgmt.updatedBy}" /></td>
					</c:if>
					<c:if test="${empty extractColumnMgmt.id}">
						<s:hidden name="extractColumnMgmt.updatedBy"
							value="${pageContext.request.remoteUser}" />
						<td style="width: 100px"><s:label name="updatedBy"
								value="${pageContext.request.remoteUser}" /></td>
					</c:if>
				</tr>
			</tbody>
		</table>

		<div id="mydiv">
		<table class="detailTabLabel" border="0">
			<tbody>
				<tr>
					<td align="left" height="3px"></td>
				</tr>
				<tr>
					<td align="left"><%-- <s:button cssClass="cssbutton1" type="button"	method="save" key="button.save" onclick="return submitPartner();" tabindex=""/> --%>
									<input id="button.save" type="button" class="cssbutton"  value="Save" name="button.save" style="width:60px;" onclick="return submitFieldForm();" tabindex="" /></td>
					<td align="right"><input class="cssbutton1" type="button" value="Clear" key="Reset" onclick="clear_fields()" style="width:55px;"/></td>
					<td align="right"><c:if test="${not empty extractColumnMgmt.id}">
							<input type="button" class="cssbutton1" value="Add"
								onclick="location.href='<c:url value="/editColumnManagement.html"/>'" />
						</c:if></td>
				</tr>
			</tbody>
		</table>
		</div>
	</div>
</s:form>
<%-- <script type="text/javascript">
	Form.focusFirstElement($("addFieldForm"));
	window.onload = function() {
		document.forms['addFieldForm'].elements['addFieldForm.accAccountPortal'].value = '${systemDefault.accountLineAccountPortalFlag}';
	}
</script> --%>
<script language="javascript" type="text/javascript">
	
<%@ include file="/common/formCalender.js"%>
	
</script>

<script language="JavaScript" type="text/javascript"
	SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript"
	SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value='/styles/redsky/jscal2.css'/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/styles/redsky/border-radius.css'/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/styles/redsky/steel.css'/>" />
<script type="text/javascript">
	setCalendarFunctionality();
</script>
