/**
 * Implementation of <strong>ActionSupport</strong> that contains convenience methods.
 * This class represents the basic actions on "DataExtraction"  in Redsky its use "QUARTZ".
 * @Class Name	DataExtractJob
 * @Author      Ravi Mishra
 * @Version     V01.0
 * @Since       1.0
 * @Date        29-Dec-2008
 */
package com.trilasoft.app.webapp.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.trilasoft.app.dao.CompanyDao;
import com.trilasoft.app.dao.ImfEntitlementsDao;
import com.trilasoft.app.dao.SystemDefaultDao;
import com.trilasoft.app.dao.ToDoRuleDao;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.CheckListManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ToDoRuleManager;
import java.util.Calendar;

public class DataExtractJob extends QuartzJobBean {
	
	private static final String APPLICATION_CONTEXT_KEY = "applicationContext";
	private ApplicationContext appCtx;
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd";
	private Company company;
	private CompanyManager companymanager;
	private List<Company> companyList;
	

	private ApplicationContext getApplicationContext(JobExecutionContext context)
			throws Exception {
		ApplicationContext appCtx = null;
		appCtx = (ApplicationContext) context.getScheduler().getContext().get(
				APPLICATION_CONTEXT_KEY);
		if (appCtx == null) {
			throw new JobExecutionException(
					"No application context available in scheduler context for key \""
							+ APPLICATION_CONTEXT_KEY + "\"");
		}
		return appCtx;
	}
	
	//This method automatically called when you run jetty see applicationContext.xml 
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		try {
			String executionTime="";
			appCtx = getApplicationContext(context);
			
			SystemDefaultDao systemDefaultDao=(SystemDefaultDao)appCtx.getBean("systemDefaultDao");
			//CompanyDao companyDao=(CompanyDao)appCtx.getBean("companyDao");
			Iterator it=systemDefaultDao.getAll().iterator();
			
			while(it.hasNext()){
				 Object systemDefault=it.next();
				 String corpID=((SystemDefault)systemDefault).getCorpID().toString();
				 
				 if(((SystemDefault)systemDefault).getToDoRuleExecutionTime()!=null){
					 executionTime=((SystemDefault)systemDefault).getToDoRuleExecutionTime().toString();
				 }
				 
				 ToDoRuleManager toDoRuleManager=(ToDoRuleManager)appCtx.getBean("toDoRuleManager");
				 
				 CheckListManager checkListManager=(CheckListManager)appCtx.getBean("checkListManager");
				 
				 List timetoExecute=toDoRuleManager.getTimeToExecute(corpID);
				 
				 if(!timetoExecute.isEmpty()){
					 try{
					 toDoRuleManager.executeRules(corpID);
					 toDoRuleManager.updateCompanyDivisionLastRunDate(corpID);
					 }catch(Exception ex){
						 
					 }
					 try{
						 checkListManager.execute(corpID) ;
					 }catch(Exception ex){
						 
					 }
				 }
				 
				 List timeToExecute2=toDoRuleManager.getTimeToExecute2(corpID);
				 
				 if(!timeToExecute2.isEmpty()){
					 try{
					 toDoRuleManager.executeRules(corpID);
					 toDoRuleManager.updateCompanyDivisionLastRunDate(corpID);
					 }catch(Exception ex){
						 
					 }
					 try{
						 checkListManager.execute(corpID) ;
					 }catch(Exception ex){
						 
					 }
				 }
			}
			
			
			/*
			ImfEntitlementsDao imfEntitlementsDao=(ImfEntitlementsDao)appCtx.getBean("imfEntitlementsDao");
			System.out.println("\n\n............Schedular Run...............\n\n");
			
			//First File ssDown.txt extraction
			System.out.println("\n\n imfEstimateDown called....(ssDown).......................\n\n");
			Writer outputSSDown = null;
			File ssdownTXT = new File("c:\\ravi\\ssdown.txt");
			outputSSDown = new BufferedWriter(new FileWriter(ssdownTXT));
			String transShip="";
			List listSSdown=imfEntitlementsDao.getImfEstimateDown();
			if(listSSdown.isEmpty())
			{
				String emptyString="...........................No Record Found...........................";
				System.out.println("\n\n No Record..............\n\n");
				outputSSDown.write(emptyString);
				outputSSDown.close();
			}
			int totalNumberofRecord=listSSdown.size();
			System.out.println("\n\nSize==============::"+totalNumberofRecord);	
			try
			{								
				String textSSdown= new String();
				if(!listSSdown.isEmpty())
				{		
				Iterator it=listSSdown.iterator();	  
			    while(it.hasNext())
			       {
					Object []row= (Object [])it.next();
					
					if(row[0]==null||row[0].toString().trim().equals("null")||row[0].toString().trim().equals(""))				
					{
						row[0]="            ";
					}
					
					if(row[1]==null||row[1].toString().trim().equals("null")||row[1].toString().trim().equals(""))				
					{
						row[1]="      ";
					}
									
					if(row[2]==null||row[2].toString().trim().equals("null")||row[2].toString().trim().equals(""))				
					{
						row[2]="  ";
					}
					
					//billToReference begin
					if(row[3]==null||row[3].toString().trim().equals("null")||row[3].toString().trim().equals(""))				
					{
						row[3]="        ";
					}
									
					
					if(row[4]==null||row[4].toString().trim().equals("null")||row[4].toString().trim().equals(""))				
					{
						row[4]="        ";
					}
					
					if(row[5]==null||row[5].toString().trim().equals("null")||row[5].toString().trim().equals(""))				
					{
						row[5]=" ";
					}
					
					
					//originAddress1 begin
					if(row[6]==null||row[6].toString().trim().equals("null")||row[6].toString().trim().equals(""))				
					{
						row[6]="                                        ";
					}
					
					if(row[7]==null||row[7].toString().trim().equals("null")||row[7].toString().trim().equals(""))				
					{
						row[7]="                                        ";
					}
					
					if(row[8]==null||row[8].toString().trim().equals("null")||row[8].toString().trim().equals(""))				
					{
						row[8]="                                        ";
					}
					
					if(row[9]==null||row[9].toString().trim().equals("null")||row[9].toString().trim().equals(""))				
					{
						row[9]="                         ";
					}
					
					if(row[10]==null||row[10].toString().trim().equals("null")||row[10].toString().trim().equals(""))				
					{
						row[10]="  ";
					}
					if(row[11]==null||row[11].toString().trim().equals("null")||row[11].toString().trim().equals(""))				
					{
						row[11]="         ";
					}
					if(row[12]==null||row[12].toString().trim().equals("null")||row[12].toString().trim().equals(""))				
					{
						row[12]="              ";
					}
					
					if(row[13]==null||row[13].toString().trim().equals("null")||row[13].toString().trim().equals(""))				
					{
						row[13]="              ";
					}
									
					if(row[14]==null||row[14].toString().trim().equals("null")||row[14].toString().trim().equals(""))				
					{
						row[14]="   ";
					}
					
					if(row[15]==null||row[15].toString().trim().equals("null")||row[15].toString().trim().equals(""))				
					{
						row[15]="                                        ";
					}
					
					if(row[16]==null||row[16].toString().trim().equals("null")||row[16].toString().trim().equals(""))				
					{
						row[16]="                                        ";
					}
					
					if(row[17]==null||row[17].toString().trim().equals("null")||row[17].toString().trim().equals(""))				
					{
						row[17]="                                        ";
											
					}
					//destinationAddress3 END
					if(row[18]==null||row[18].toString().trim().equals("null")||row[18].toString().trim().equals(""))				
					{
						row[18]="                         ";
					}
					
					if(row[19]==null||row[19].toString().trim().equals("null")||row[19].toString().trim().equals(""))				
					{
						row[19]="  ";
					}
					
					if(row[20]==null||row[20].toString().trim().equals("null")||row[20].toString().trim().equals(""))				
					{
						row[20]="         ";
					}
					
					if(row[21]==null||row[21]==null||row[21].toString().trim().equals("null")||row[21].toString().trim().equals(""))				
					{
						row[21]="   ";
					}
					
					if(row[22]==null||row[22].toString().trim().equals("null")||row[22].toString().trim().equals(""))				
					{
						row[22]="               ";
					}
					
					if(row[23]==null||row[23].toString().trim().equals("null")||row[23].toString().trim().equals(""))				
					{
						row[23]="              ";
					}
					//destinationCountryPhone END
					if(row[24]==null||row[24].toString().trim().equals("null")||row[24].toString().trim().equals(""))				
					{
						row[24]="        ";
					}
					if(row[25]==null||row[25].toString().trim().equals("null")||row[25].toString().trim().equals(""))				
					{
						row[25]="        ";
					}
					if(row[26]==null||row[26].toString().trim().equals("null")||row[26].toString().trim().equals(""))				
					{
						row[26]="        ";
					}
									
									
					if(row[27]==null||row[27].toString().trim().equals("null")||row[27].toString().trim().equals(""))				
					{
						row[27]=" ";
					}
					
					if(row[28]==null||row[28].toString().trim().equals("null")||row[28].toString().trim().equals(""))				
					{
						row[28]="      ";
					}
					
					if(row[29]==null||row[29].toString().trim().equals("null")||row[29].toString().trim().equals(""))				
					{
						row[29]=" ";
					}
					
					if(row[30]==null||row[30].toString().trim().equals("null")||row[30].toString().trim().equals(""))				
					{
						row[30]="   ";
											
					}
					//estimator END
					if(row[31]==null||row[31].toString().trim().equals("null")||row[31].toString().trim().equals(""))				
					{
						row[31]="     ";
					}
					
					if(row[32]==null||row[32].toString().trim().equals("null")||row[32].toString().trim().equals(""))				
					{
						row[32]="        ";
					}
					
					if(row[33]==null||row[33].toString().trim().equals("null")||row[33].toString().trim().equals(""))				
					{
						row[33]="        ";
					}
					
					if(row[34]==null||row[34].toString().trim().equals("null")||row[34].toString().trim().equals(""))				
					{
						row[34]=" ";
					}
					
					if(row[35]==null||row[35].toString().trim().equals("null")||row[35].toString().trim().equals(""))				
					{
						row[35]="      ";
					}
					
					if(row[36]==null||row[36].toString().trim().equals("null")||row[36].toString().trim().equals(""))				
					{
						row[36]=" ";
					}
					//pieces begin
					if(row[37]==null||row[37].toString().trim().equals("null")||row[37].toString().trim().equals(""))				
					{
						row[37]="      ";
					}
					if(row[38]==null||row[38].toString().trim().equals("null")||row[38].toString().trim().equals(""))				
					{
						row[38]="  ";
					}
					if(row[39]==null||row[39].toString().trim().equals("null")||row[39].toString().trim().equals(""))				
					{
						row[39]="  ";
					}
					
					
					if(row[40]==null||row[40].toString().trim().equals("null")||row[40].toString().trim().equals(""))				
					{
						row[40]=" ";
					}
								
					if(row[41]==null||row[41].toString().trim().equals("null")||row[41].toString().trim().equals(""))				
					{
						row[41]="        ";
					}
					
					if(row[42]==null||row[42].toString().trim().equals("null")||row[42].toString().trim().equals(""))				
					{
						row[42]="        ";
					}
					
					if(row[43]==null||row[43].toString().trim().equals("null")||row[43].toString().trim().equals(""))				
					{
						row[43]="        ";
					}
					
					if(row[44]==null||row[44].toString().trim().equals("null")||row[44].toString().trim().equals(""))				
					{
						row[44]=" ";
											
					}
					
					if(row[45]==null||row[45].toString().trim().equals("null")||row[45].toString().trim().equals(""))				
					{
						row[45]="        ";
					}
					
					if(row[46]==null||row[46].toString().trim().equals("null")||row[46].toString().trim().equals(""))				
					{
						row[46]="                 ";
					}
					
					if(row[47]==null||row[47].toString().trim().equals("null")||row[47].toString().trim().equals(""))				
					{
						row[47]="        ";
					}
					
					if(row[48]==null||row[48].toString().trim().equals("null")||row[48].toString().trim().equals(""))				
					{
						row[48]=" ";
					}
					transShip=row[48].toString();
					
					if(transShip.trim().equals("true"))
					{
						transShip="Y";
					
					}
					else
					{
						transShip=" ";
					
					}
					if(row[49]==null||row[49].toString().trim().equals("null")||row[49].toString().trim().equals(""))				
					{
						row[49]="        ";
					}
					
					if(row[50]==null||row[50].toString().trim().equals("null")||row[50].toString().trim().equals(""))				
					{
						row[50]="                              ";
					}
					
					if(row[51]==null||row[51].toString().trim().equals("null")||row[51].toString().trim().equals(""))				
					{
						row[51]="        ";
					}
					if(row[52]==null||row[52].toString().trim().equals("null")||row[52].toString().trim().equals(""))				
					{
						row[52]=" ";
					}
					if(row[53]==null||row[53].toString().trim().equals("null")||row[53].toString().trim().equals(""))				
					{
						row[53]="                              ";
					}
					
					if(row[54]!=null||row[54]==null||row[54].toString().trim().equals("null")||row[54].toString().trim().equals(""))				
					{
						row[54]="                              ";
					}
									
					if(row[55]!=null||row[55]==null||row[55].toString().trim().equals("null")||row[55].toString().trim().equals(""))				
					{
						row[55]="                            ";
					}
					
					if(row[56]==null||row[56].toString().trim().equals("null")||row[56].toString().trim().equals(""))				
					{
						row[56]="        ";
					}
					
					if(row[57]==null||row[57].toString().trim().equals("null")||row[57].toString().trim().equals(""))				
					{
						row[57]=" ";
					}
					
					if(row[58]==null||row[58].toString().trim().equals("null")||row[58].toString().trim().equals(""))				
					{
						row[58]="        ";
											
					}
					
					if(row[59]==null||row[59].toString().trim().equals("null")||row[59].toString().trim().equals(""))				
					{
						row[59]=" ";
					}
					
					if(row[60]==null||row[60].toString().trim().equals("null")||row[60].toString().trim().equals(""))				
					{
						row[60]=" ";
					}
					
					if(row[61]==null||row[61].toString().trim().equals("null")||row[61].toString().trim().equals(""))				
					{
						row[61]=" ";
					}
					
					if(row[62]==null||row[62].toString().trim().equals("null")||row[62].toString().trim().equals(""))				
					{
						row[62]="        ";
					}
					
					if(row[63]==null||row[63].toString().trim().equals("null")||row[63].toString().trim().equals(""))				
					{
						row[63]="        ";
					}
					
					if(row[64]==null||row[64].toString().trim().equals("null")||row[64].toString().trim().equals(""))				
					{
						row[64]="        ";
					}
					
					if(row[65]==null||row[65].toString().trim().equals("null")||row[65].toString().trim().equals(""))				
					{
						row[65]="        ";
					}
					if(row[66]==null||row[66].toString().trim().equals("null")||row[66].toString().trim().equals(""))				
					{
						row[66]="               ";
					}
					if(row[67]==null||row[67].toString().trim().equals("null")||row[67].toString().trim().equals(""))				
					{
						row[67]="        ";
					}
					
					if(row[68]==null||row[68].toString().trim().equals("null")||row[68].toString().trim().equals(""))				
					{
						row[68]="        ";
					}
								
					if(row[69]==null||row[69].toString().trim().equals("null")||row[69].toString().trim().equals(""))				
					{
						row[69]="                    ";
					}
					
					if(row[70]==null||row[70].toString().trim().equals("null")||row[70].toString().trim().equals(""))				
					{
						row[70]="   ";
					}
					
					if(row[71]==null||row[71].toString().trim().equals("null")||row[71].toString().trim().equals(""))				
					{
						row[71]="        ";       
					}
					if(row[72]==null||row[72].toString().trim().equals("null")||row[72].toString().trim().equals(""))				
					{
						row[72]="    ";       
					}
					if(row[73]==null||row[73].toString().trim().equals("null")||row[73].toString().trim().equals(""))				
					{
						row[73]="                                 ";       
					}
					if(row[74]==null||row[74].toString().trim().equals("null")||row[74].toString().trim().equals(""))				
					{
						row[74]="      ";       
					}	
					if(row[75]==null||row[75].toString().trim().equals("null")||row[75].toString().trim().equals(""))				
					{
						row[75]="      ";       
					}
										
					
		    		String nowYYYYMMDD="         ";
		    		if(row[4]==null||row[4].toString().trim().equals("null")||row[4].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD="        ";
		    		}
		    		if(!row[4].toString().trim().equals("null") && !row[4].toString().trim().equals(""))
		    		{				
		    			Date date=(Date)row[4];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
				         nowYYYYMMDD = new String( dateformatYYYYMMDD.format( date ) );	
				    } 
		    		
		    		
		    		String nowYYYYMMDD1="         ";
		    		if(row[26]==null||row[26].toString().trim().equals("null")||row[26].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD1="        ";
		    		}
		    		if(!row[26].toString().trim().equals("null") && !row[26].toString().trim().equals(""))
		    		{				
		    			Date date=(Date)row[26];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
				         nowYYYYMMDD1 = new String( dateformatYYYYMMDD.format( date ) );	
				    } 
		    		
		    		String nowYYYYMMDD2="          ";
		    		if(row[32]==null||row[32].toString().trim().equals("null")||row[32].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD2="         ";
		    		}
		    		if(!row[32].toString().trim().equals("null") && !row[32].toString().trim().equals(""))
		    		{				
		    			Date date=(Date)row[32];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
				         nowYYYYMMDD2 = new String( dateformatYYYYMMDD.format( date ) );
				         nowYYYYMMDD2=nowYYYYMMDD2+"A";
				    } 
		    		
		    		String nowYYYYMMDD3="          ";
		    		if(row[33]==null||row[33].toString().trim().equals("null")||row[33].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD3="         ";
		    		}
		    		if(!row[33].toString().trim().equals("null") && !row[33].toString().trim().equals(""))
		    		{				
		    			Date date=(Date)row[33];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
				         nowYYYYMMDD3 = new String( dateformatYYYYMMDD.format( date ) );
				         nowYYYYMMDD3=nowYYYYMMDD3+"A";
				    } 
		    		
		    		String nowYYYYMMDD4="         ";
		    		if(row[41]==null||row[41].toString().trim().equals("null")||row[41].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD4="        ";
		    		}
		    		if(!row[41].toString().trim().equals("null") && !row[41].toString().trim().equals(""))
		    		{				
		    			Date date=(Date)row[41];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
						nowYYYYMMDD4 = new String( dateformatYYYYMMDD.format( date ) );	
				    } 
		    		
		    		String nowYYYYMMDD5="         ";
		    		if(row[42]==null||row[42].toString().trim().equals("null")||row[42].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD5="        ";
		    		}
		    		if(!row[42].toString().trim().equals("null") && !row[42].toString().trim().equals(""))
		    		{				
		    			Date date=(Date)row[42];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
						nowYYYYMMDD5 = new String( dateformatYYYYMMDD.format( date ) );	
				    } 
		    		
		    		String nowYYYYMMDD6="         ";
		    		if(row[43]==null||row[43].toString().trim().equals("null")||row[43].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD6="        ";
		    		}
		    		if(!row[43].toString().trim().equals("null") && !row[43].toString().trim().equals(""))
		    		{				
		    			Date date=(Date)row[43];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
						nowYYYYMMDD6 = new String( dateformatYYYYMMDD.format( date ) );	
				    } 
		    		
		    		String nowYYYYMMDD7="          ";
		    		if(row[47]==null||row[47].toString().trim().equals("null")||row[47].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD7="         ";
		    		}
		    		if(!row[47].toString().trim().equals("null") && !row[47].toString().trim().equals(""))
		    		{				
		    			Date date=(Date)row[47];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
						nowYYYYMMDD7 = new String( dateformatYYYYMMDD.format( date ) );	
						nowYYYYMMDD7=nowYYYYMMDD7+"A";
				    } 
		    		
		    		
		    		String nowYYYYMMDD56="          ";
		    		if(row[56]==null||row[56].toString().trim().equals("null")||row[56].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD56="         ";
		    		}
		    		if(!row[56].toString().trim().equals("null") && !row[56].toString().trim().equals(""))
		    		{				
		    			Date date=(Date)row[56];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
						nowYYYYMMDD56 = new String( dateformatYYYYMMDD.format( date ) );
						nowYYYYMMDD56=nowYYYYMMDD56+"A";
				    } 
		    		
		    		String nowYYYYMMDD58="         ";
		    		if(row[58]==null||row[58].toString().trim().equals("null")||row[58].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD58="        ";
		    		}
		    		if(row[58]!=null)
		    		{	    			
		    			nowYYYYMMDD58="        ";
		    		}
		    		
		    		String nowYYYYMMDD62="         ";
		    		if(row[62]==null||row[62].toString().trim().equals("null")||row[62].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD62="        ";
		    		}
		    		if(!row[62].toString().trim().equals("null") && !row[62].toString().trim().equals(""))
		    		{				
		    			Date date=(Date)row[62];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
						nowYYYYMMDD62 = new String( dateformatYYYYMMDD.format( date ) );	
				    } 
		    		String nowYYYYMMDD63="         ";
		    		if(row[63]==null||row[63].toString().trim().equals("null")||row[63].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD63="        ";
		    		}
		    		if(!row[63].toString().trim().equals("null") && !row[63].toString().trim().equals(""))
		    		{				
		    			Date date=(Date)row[63];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
						nowYYYYMMDD63 = new String( dateformatYYYYMMDD.format( date ) );	
				    } 
		    		String nowYYYYMMDD64="          ";
		    		
		    		if(row[64]==null||row[64].toString().trim().equals("null")||row[64].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD64="         ";
		    		}
		    		if(!row[64].toString().trim().equals("null") && !row[64].toString().trim().equals(""))
		    		{				
		    			Date date=(Date)row[64];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
						nowYYYYMMDD64 = new String( dateformatYYYYMMDD.format( date ) );
						nowYYYYMMDD64=nowYYYYMMDD64+"A";
				    } 
		    		
		    		
		    		String nowYYYYMMDD65="         ";
		    		if(row[65]==null||row[65].toString().trim().equals("null")||row[65].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD65="        ";
		    		}
		    		if(!row[65].toString().trim().equals("null") && !row[65].toString().trim().equals(""))
		    		{				
		    			Date date=(Date)row[65];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
						nowYYYYMMDD65 = new String( dateformatYYYYMMDD.format( date ) );	
				    }
		    		
		    		
		    		String nowYYYYMMDD51="          ";
		    		if(row[51]==null||row[51].toString().trim().equals("null")||row[51].toString().trim().equals(""))
		    		{	    			
		    			nowYYYYMMDD51="         ";
		    		}
		    		if(!row[51].toString().trim().equals("null") && !row[51].toString().trim().equals(""))
		    		{				
		    			Date date=(Date)row[51];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
						nowYYYYMMDD51 = new String( dateformatYYYYMMDD.format( date ) );	
						nowYYYYMMDD51=nowYYYYMMDD51+"A";
				    } 
		    		
		    		
		    		if(row[36].toString().trim().equals("S"))
		    		{
		    			textSSdown=textSSdown+row[0].toString()+row[1].toString()+row[2].toString()+row[3].toString()+nowYYYYMMDD+row[5].toString()+row[6].toString()+row[7].toString()+row[8].toString()+row[9].toString()+row[10].toString()+row[11].toString()+row[12].toString()+row[13].toString()+row[14].toString()+row[15].toString()+row[16].toString()+row[17].toString()+row[18].toString()+row[19].toString()+row[20].toString()+row[21].toString()+row[22].toString()+row[23].toString()+"237"+row[24].toString()+row[25].toString()+nowYYYYMMDD1+"A"+row[28].toString()+row[29].toString()+row[30].toString()+row[31].toString()+nowYYYYMMDD2+nowYYYYMMDD3+row[35].toString()+"N"+row[37].toString()+row[38].toString()+row[39].toString()+"                                                            "+row[40].toString()+nowYYYYMMDD4+nowYYYYMMDD5+nowYYYYMMDD6+transShip+row[45].toString()+row[46].toString()+nowYYYYMMDD7+row[49].toString()+row[53].toString()+nowYYYYMMDD51+row[50].toString()+row[73].toString()+row[55].toString()+nowYYYYMMDD56+nowYYYYMMDD58+" "+row[61].toString()+nowYYYYMMDD62+nowYYYYMMDD63+nowYYYYMMDD64+nowYYYYMMDD65+"                               "+row[69].toString()+row[70].toString()+"                                                  "+row[71].toString()+row[72].toString()+"\n";
		    		}
		    		if(row[36].toString().trim().equals("A"))
		    		{
		    			textSSdown=textSSdown+row[0].toString()+row[1].toString()+row[2].toString()+row[3].toString()+nowYYYYMMDD+row[5].toString()+row[6].toString()+row[7].toString()+row[8].toString()+row[9].toString()+row[10].toString()+row[11].toString()+row[12].toString()+row[13].toString()+row[14].toString()+row[15].toString()+row[16].toString()+row[17].toString()+row[18].toString()+row[19].toString()+row[20].toString()+row[21].toString()+row[22].toString()+row[23].toString()+"237"+row[24].toString()+row[25].toString()+nowYYYYMMDD1+"A"+row[28].toString()+row[29].toString()+row[30].toString()+row[31].toString()+nowYYYYMMDD2+nowYYYYMMDD3+row[35].toString()+"G"+row[37].toString()+row[38].toString()+row[39].toString()+"                                                            "+row[40].toString()+nowYYYYMMDD4+nowYYYYMMDD5+nowYYYYMMDD6+transShip+row[45].toString()+row[46].toString()+nowYYYYMMDD7+row[49].toString()+row[53].toString()+nowYYYYMMDD51+row[50].toString()+row[73].toString()+row[55].toString()+nowYYYYMMDD56+nowYYYYMMDD58+" "+row[61].toString()+nowYYYYMMDD62+nowYYYYMMDD63+nowYYYYMMDD64+nowYYYYMMDD65+"                               "+row[69].toString()+row[70].toString()+"                                                  "+row[71].toString()+row[72].toString()+"\n";
		    		}
		    		
		    		if(row[36].toString().trim().equals(""))
		    		{
					textSSdown=textSSdown+row[0].toString()+row[1].toString()+row[2].toString()+row[3].toString()+nowYYYYMMDD+row[5].toString()+row[6].toString()+row[7].toString()+row[8].toString()+row[9].toString()+row[10].toString()+row[11].toString()+row[12].toString()+row[13].toString()+row[14].toString()+row[15].toString()+row[16].toString()+row[17].toString()+row[18].toString()+row[19].toString()+row[20].toString()+row[21].toString()+row[22].toString()+row[23].toString()+"237"+row[24].toString()+row[25].toString()+nowYYYYMMDD1+row[27].toString()+row[28].toString()+row[29].toString()+row[30].toString()+row[31].toString()+nowYYYYMMDD2+nowYYYYMMDD3+row[35].toString()+row[36].toString()+row[37].toString()+row[38].toString()+row[39].toString()+"                                                            "+row[40].toString()+nowYYYYMMDD4+nowYYYYMMDD5+nowYYYYMMDD6+transShip+row[45].toString()+row[46].toString()+nowYYYYMMDD7+row[49].toString()+row[53].toString()+nowYYYYMMDD51+row[50].toString()+row[73].toString()+row[55].toString()+nowYYYYMMDD56+nowYYYYMMDD58+" "+row[61].toString()+nowYYYYMMDD62+nowYYYYMMDD63+nowYYYYMMDD64+nowYYYYMMDD65+"                               "+row[69].toString()+row[70].toString()+"                                                  "+row[71].toString()+row[72].toString()+"\n";
		    		}
					 
					}
					
				
				outputSSDown.write(textSSdown);				
				outputSSDown.close();
				
				}
				System.out.println("\n\nssDown.txt file extraction complete");
			}
			catch(Exception e)
			{
				System.out.println("ERROR:"+e);
			} 
			
			
			//Second File sscosts.txt extraction

			System.out.println("\n\nimfEstimateActualData called...........................\n\n");
			Double  weightSScosts=new Double(0);
			Double  totalWeightSScosts=new Double(0);
			String weSScosts="";
			String textSScosts= new String();
			Writer outputSScosts = null;
			File fileSScosts = new File("c:\\ravi\\sscosts.txt");
			outputSScosts = new BufferedWriter(new FileWriter(fileSScosts));	
			List listSScosts=imfEntitlementsDao.getImfEstimateActual();
			if(listSScosts.isEmpty())
			{
				String emptyString="...........................No Record Found...........................";
				System.out.println("\n\n No Record..............\n\n");
				outputSSDown.write(emptyString);
				outputSSDown.close();
			}
			int totalNumberofRecordSscosts=listSScosts.size();	
			System.out.println("\n\nSIZE::"+totalNumberofRecordSscosts);
			try
			{
				
				if(!listSScosts.isEmpty())
				{		
				Iterator it=listSScosts.iterator();
					
				while(it.hasNext())
			       {
					Object []row= (Object [])it.next();	
					
					if(row[0]==null||row[0].toString().trim().equals("null")||row[0].toString().trim().equals(""))				
					{
						row[0]="      ";
					}
					
					if(row[1]==null||row[1].toString().trim().equals("null")||row[1].toString().trim().equals(""))				
					{
						row[1]="  ";
					}
							
					
					if(row[2]==null||row[2].toString().trim().equals("null")||row[2].toString().trim().equals(""))				
					{
						row[2]="       ";
					}
					
					if(row[3]==null||row[3].toString().trim().equals("null")||row[3].toString().trim().equals(""))				
					{
						row[3]="    ";
					}
					
					if(row[4]==null||row[4].toString().trim().equals("null")||row[4].toString().trim().equals(""))				
					{
						row[4]="               ";
					}
					
					
					if(row[5]==null||row[5].toString().trim().equals("null")||row[5].toString().trim().equals(""))				
					{
						row[5]="         ";
					}
									
					
					if(row[7]==null||row[7].toString().trim().equals("")||row[7].toString().trim().equals("null"))				
					{
						row[7]="               ";
					}
							
					
					if(row[8]==null||row[8].toString().trim().equals("null")||row[8].toString().trim().equals("")||row[8]==null)				
					{
						row[8]=" ";
					}
					
								
					String str=(String)row[4].toString();				
					if(!str.equals("               "))
					{
					weightSScosts =(double) Double.parseDouble(str.trim());  
					totalWeightSScosts=totalWeightSScosts+weightSScosts;
					}
		    		
		    		String nowYYYYMMDD="        ";
		    		if(row[6]==null||row[6].toString().trim().equals("null")||row[6].toString().trim().equals(""))
		    		{	    
		    			nowYYYYMMDD="        ";
		    		}
		    		if(row[6]!=null)
		    		{				
		    			Date date=(Date)row[6];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("ddMMyyyy");
				        nowYYYYMMDD = new String( dateformatYYYYMMDD.format( date ) );	
				    } 
		    		
		    		String nowYYYYMMDD1="        ";
		    		if(row[9]==null||row[9].toString().trim().equals("null")||row[9].toString().trim().equals(""))
		    		{	    
		    			nowYYYYMMDD1="        ";
		    		}
		    		if(row[9]!=null)
		    		{				
		    			Date date=(Date)row[9];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("ddMMyyyy");
				        nowYYYYMMDD1 = new String( dateformatYYYYMMDD.format( date ) );	
				    }
		    		String nowYYYYMMDD2="        ";
		    		if(row[10]==null||row[10].toString().trim().equals("null")||row[10].toString().trim().equals(""))
		    		{	    
		    			nowYYYYMMDD2="        ";
		    		}
		    		if(row[10]!=null)
		    		{				
		    			Date date=(Date)row[10];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("ddMMyyyy");
				        nowYYYYMMDD2 = new String( dateformatYYYYMMDD.format( date ) );	
				    }
		    		
		    		//replacing value (0) of billToReference
		    		int l=48;
		    		String target1=new String("");	    		
		    		String strDescription1 =(String)row[0];
		    		target1=strDescription1;
		    		
		    		
		    		if(target1.charAt(0)==l)
		    		{
		    		if(l==48){
		    			char e=(char)l;
		    		
		    		int i=	strDescription1.indexOf(e);
		    		
		    				if(i<=0)
		    				{
		    					target1=strDescription1;	    					
		    				}else if(i>0)
		    				{
		    					target1=strDescription1.substring(0,i);
		    					strDescription1=target1;
		    				} 
		    				
		    			
		    		target1=target1.replaceFirst("0"," ");
		    		}
		    		}
		    		if(target1.equals(""))
		    		{
		    			target1="       ";
		    		}
		    		
					textSScosts=textSScosts+"0"+target1+" "+row[1].toString()+row[2].toString()+row[3].toString()+row[4].toString()+row[5].toString()+nowYYYYMMDD+row[7].toString()+row[8].toString()+nowYYYYMMDD1+nowYYYYMMDD2+"\n";				
			
			       }
				
				
				String totalFileNumber12="1INV   "+totalNumberofRecordSscosts;
				
				DecimalFormat df1 = new DecimalFormat("0.00");
				weSScosts=df1.format(totalWeightSScosts);
				
				outputSScosts.write(textSScosts);
				outputSScosts.write(totalFileNumber12+"      ");				
				outputSScosts.write(weSScosts);
				System.out.println("\n\n sscosts.txt extraction done successfull......................\n\n");
				outputSScosts.close();
				}
			}
			catch(Exception e)
			{
				System.out.println("ERROR:"+e);
			} 
			
			
		
			
			
		
		*/
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	}	

	}

	public void setCompanymanager(CompanyManager companymanager) {
		this.companymanager = companymanager;
	}

	public List<Company> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

}
