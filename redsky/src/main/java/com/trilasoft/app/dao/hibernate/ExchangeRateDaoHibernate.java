/**
 * This class interacts with Spring's HibernateTemplate to save/delete and
 * retrieve the basic "ExchangeRate" objects in Redsky.
 * @Class Name	ExchangeRateDaoHibernate
 * @Author      Ravi Mishra
 * @Version     V01.0
 * @Since       1.0
 * @Date        08-Dec-2008
 * Extended by GenericDaoHibernate to  implement ExchangeRateDao
 */
package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.trilasoft.app.dao.ExchangeRateDao;
import com.trilasoft.app.model.ExchangeRate;
import com.trilasoft.app.model.HistoryExchangeRate;
import com.trilasoft.app.model.RefMaster;

public class ExchangeRateDaoHibernate extends GenericDaoHibernate<ExchangeRate, Long> implements ExchangeRateDao{
	
	public ExchangeRateDaoHibernate() {
		super(ExchangeRate.class);
	}
	
	public List searchBaseCurrency(String corpID){
		return getHibernateTemplate().find("select baseCurrency from SystemDefault where corpID=?", corpID);
	}	
	public List findMaximumId() {
    	return getHibernateTemplate().find("select max(id) from ExchangeRate");
    }
	public List findAccExchangeRate(String corpId, String country) {
		return this.getSession().createSQLQuery("select baseCurrencyRate from exchangerate e where currency = '"+country+"' and corpID='"+corpId+"' and currencyBaseRate is not null order by valueDate desc limit 1").list();		
	}	
	public List searchAll(String currency){		
		List exCurrency = getHibernateTemplate().find("from ExchangeRate where currency like '" +currency+"%' ");		
		return exCurrency;
	}
	public List searchID(){
		return getHibernateTemplate().find("select id from ExchangeRate");
	}
	public Map<String, String> getExchangeRateWithCurrency(String corpID){
		List<ExchangeRate> list;
		list = getHibernateTemplate().find("from ExchangeRate order by valueDate"); 
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();  
			for (ExchangeRate exchangeRate : list) {
				parameterMap.put(exchangeRate.getCurrency(), exchangeRate.getBaseCurrencyRate()+"");
			} 
		return parameterMap;
	}

	public Map<String, String> getAgentExchangeRateWithCurrency() {
	List corpidList=this.getSession().createSQLQuery("select corpid from company  where cmmdmmAgent is true").list();
	Iterator it=corpidList.iterator();
	String corpID="";
	while(it.hasNext()){
		if(corpID.equals("")){
			corpID=it.next().toString();	
		}else{
			corpID=corpID+"','"+it.next().toString();	
		}
	}
		List<ExchangeRate> list;
		list = this.getSession().createSQLQuery("select corpID,currency,baseCurrencyRate from exchangerate e where corpID in ('"+corpID+"') and currencyBaseRate is not null order by valueDate desc ").list(); 
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();  
		Iterator its=list.iterator();
		while (its.hasNext()) {
		Object[] row = (Object[]) its.next();
				parameterMap.put(row[0].toString().trim()+"~"+row[1].toString().trim(), row[2].toString().trim());
			} 
		return parameterMap;
	
	}
	public List getExchangeHistoryList(String corpID)
	{
		getHibernateTemplate().setMaxResults(500);
	return getHibernateTemplate().find("from HistoryExchangeRate where corpID = '"+corpID+"' ");	
	}
	
	public List searchAll(String currency , String valueDate){
		List exCurrency = getHibernateTemplate().find("from HistoryExchangeRate where currency like '" +currency+"%' and valueDate like '" +valueDate+"%' ");		
	    return exCurrency;
	} 
	public List getHistoryFromid(Long id)
	{
    	Criteria crit = this.getSession().createCriteria(HistoryExchangeRate.class);
    	return  crit.add(Restrictions.eq("id", id)).list();
	}
	public Map<String, String> getExchangeHistoryListByDate(String corpID,String fxValueDate){
		List<ExchangeRate> list;
		list = this.getSession().createSQLQuery("select currency,baseCurrencyRate from history_exchangerate where corpID='"+corpID+"' and valueDate like '"+fxValueDate+"%' ").list(); 
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();  
		Iterator its=list.iterator();
		while (its.hasNext()) {
		Object[] row = (Object[]) its.next();
				parameterMap.put(row[0].toString().trim(), row[1].toString().trim());
			} 
		return parameterMap;
	}
		
}