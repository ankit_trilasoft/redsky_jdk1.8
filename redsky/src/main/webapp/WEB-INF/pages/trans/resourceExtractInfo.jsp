<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<head>
<title>Resource&nbsp;Extract</title>
<meta name="heading" content="Resource Extract" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style> <%@ include file="/common/calenderStyle.css"%> </style>

<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script> 

<script type="text/javascript"> 

</script>
</head>
<s:form cssClass="form_magn" id="ResourceExtractInfoForm" name="ResourceExtractInfoForm" action="resourceExtract" method="post">
<div id="Layer1" style="width:85%;">			
<div id="newmnav" class="nav_tabs">
		  <ul>
		    <li><a href="cPortalResourceMgmts.html"><span>CPortal&nbsp;ResourceMgmt&nbsp;List</span></a></li>
		    <li id="newmnav1" style="background:#FFF"><a class="current"><span>Resource&nbsp;Extract</span></a></li>
		  </ul>
		</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:13px;!margin-top:-12px;"><span></span></div>
   <div class="center-content">
<table  border="0" cellspacing="0" cellpadding="2">
	
	<tr><td height="5"></td></tr>
		<tr>
	    <td width="5px"></td>
		<td align="right" class="listwhitetext" style="width:100px;" >Document Type</td>
		 <td width="150"><s:select name="documentType" cssClass="list-menu" list="%{cPortalResourceDocsList}" onchange="" headerKey="" headerValue="" /></td>
		</tr> 
		<tr><td height="10"></td></tr>
		<tr>
		<td width="5px"></td>
		<td></td> 
        <td align="left"><s:submit cssClass="cssbutton1" type="button" value="Extract" name="Extract"  cssStyle="width:70px;"  />  
        </td>
		</tr> 
		<tr><td height="15"></td></tr>
	</table>
	</div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>
</div>
</s:form>
<script type="text/javascript"> 
 
</script>