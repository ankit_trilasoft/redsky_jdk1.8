package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.ToDoResult;



public interface ToDoResultManager extends GenericManager<ToDoResult, Long>{
	public List findMaximumId();
public void updateToDoResult(String sessionCorpID);

//13674 - Create dashboard
public List getTaskList(String  filenumber,String sessionCorpID,Long id);
//End  dashboard
}
