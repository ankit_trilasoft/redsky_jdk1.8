package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="menu_item")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)

public class MenuItem extends BaseObject
{
	private String corpID;
	private Long id;
	private String parentName;
	private String name;
	private String url;
	private String description;
	private String title;
	private BigDecimal sequenceNum;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private Set<MenuItemPermission> menuItemPermissions;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("parentName", parentName).append("name", name)
				.append("url", url).append("description", description).append(
						"title", title).append("createdBy", createdBy).append(
						"createdOn", createdOn).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof MenuItem))
			return false;
		MenuItem castOther = (MenuItem) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(parentName, castOther.parentName).append(
				name, castOther.name).append(url, castOther.url).append(
				description, castOther.description).append(title,
				castOther.title).append(createdBy, castOther.createdBy).append(
				createdOn, castOther.createdOn).append(updatedBy,
				castOther.updatedBy).append(updatedOn, castOther.updatedOn)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(
				parentName).append(name).append(url).append(description)
				.append(title).append(createdBy).append(createdOn).append(
						updatedBy).append(updatedOn).toHashCode();
	}
	@OneToMany(mappedBy="menuItem", cascade = CascadeType.ALL, fetch = FetchType.EAGER )
	public Set<MenuItemPermission> getMenuItemPermissions() {
		return menuItemPermissions;
	}
	public void setMenuItemPermissions(Set<MenuItemPermission> menuItemPermissions) {
		this.menuItemPermissions = menuItemPermissions;
	}
	
	/*
	 @ManyToMany(fetch = FetchType.EAGER) 
	    @JoinTable(
	           
	            joinColumns = { @JoinColumn() },
	            inverseJoinColumns = @JoinColumn()
	            
	    )  
	public Set<MenuItemPermission> getMenuItemPermissions() {
	        return menuItemPermissions;
	    }
	 public void setMenuItemPermissions(Set<MenuItemPermission> menuItemPermissions) {
			this.menuItemPermissions = menuItemPermissions;
		}
		*/
	@Column
	public String getCorpID() {
		return corpID;
	}
	public String setCorpID(String corpID) {
		return this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	//@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	@Column
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	@Column
	public BigDecimal getSequenceNum() {
		return sequenceNum;
	}
	public void setSequenceNum(BigDecimal sequenceNum) {
		this.sequenceNum = sequenceNum;
	}
	
	
}
