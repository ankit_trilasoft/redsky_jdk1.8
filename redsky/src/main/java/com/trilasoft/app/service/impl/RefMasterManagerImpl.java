package com.trilasoft.app.service.impl;

import com.trilasoft.app.dao.RefMasterDao;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.UserDTO;
import com.trilasoft.app.service.RefMasterManager;

import org.appfuse.service.impl.GenericManagerImpl;

import java.util.*;

public class RefMasterManagerImpl extends GenericManagerImpl<RefMaster, Long> implements RefMasterManager {
	RefMasterDao refMasterDao;

	public RefMasterManagerImpl(RefMasterDao refMasterDao) {
		super(refMasterDao);
		this.refMasterDao = refMasterDao;
	} 

	public List findByParameter(String parameter, String searchCode, String searchDescription, String corpId, String status) {
		return refMasterDao.findByParameter(parameter, searchCode, searchDescription, corpId, status);
	}

	public List findClaimPerson(String corpID, String jobType){
		List list = refMasterDao.findClaimPerson(corpID, jobType);
		return list;
	}

	public Map<String, String> findByParameter(String corpId, String parameter) {
		return refMasterDao.findByParameter(corpId, parameter);
	}
	public Map<String, String> findByParameterWithBucket2(String corpId, String parameter) {
		return refMasterDao.findByParameterWithBucket2(corpId, parameter);
	}
	
	public String findDocCategoryByDocType(String sessionCorpID, String docType){
		return refMasterDao.findDocCategoryByDocType(sessionCorpID,docType);
	}
	
	public Map<String,String> findByParameterCodeDesc(String corpId, String parameter){
		return refMasterDao.findByParameterCodeDesc(corpId, parameter);
	}
	
	public List findCompanyList( String sessionCorpID){
		return refMasterDao.findCompanyList(sessionCorpID);
	}
	
	public Map<String, String> findUser(String corpId, String parameter) {
		return refMasterDao.findUser(corpId, parameter);

	}
	
	public Map<String, String> findUserUGCORP(String parameter){
		return refMasterDao.findUserUGCORP(parameter);
		
	}
	public List<UserDTO> getAllUser(String corpId, String roles){
		return refMasterDao.getAllUser(corpId, roles);
	}
		
	public Map<String, String> findUserNotes(String corpId, String parameter) {
		return refMasterDao.findUserNotes(corpId, parameter);
      }
	public List findRole(String corpId, String parameter) {
		return refMasterDao.findRole(corpId, parameter);
	}

	public List findByRoles(String corpID) {
		return refMasterDao.findByRoles(corpID);
	}

	public List searchUser(String userType, String username, String firstName, String lastName, String email, String supervisor,String enableUser,String parentAgent) {
		return refMasterDao.searchUser(userType, username, firstName, lastName, email, supervisor,enableUser , parentAgent);
	}

	public List findUserDetails(String corpId, String parameter) {
		return refMasterDao.findUserDetails(corpId, parameter);
	}

	public List searchByCodeAndDescription(String searchCode, String searchDescription, String corpId) {
		return refMasterDao.searchByCodeAndDescription(searchCode, searchDescription, corpId);
	}

	public List<RefMaster> getList() {
		return refMasterDao.getList();
	}

	public List<RefMaster> searchList(String parameter, String corpId,String nameonform) {
		return refMasterDao.searchList(parameter, corpId,nameonform);
	}

	public List findMaximumId() {
		return refMasterDao.findMaximumId();
	}

	public List<RefMaster> deletedByParameterAndCode(String parameter, String code, String corpId) {
		return refMasterDao.deletedByParameterAndCode(parameter, code, corpId);
	}

	public List testJoin() {
		return refMasterDao.testJoin();
	}

	public Map<String, String> findDocumentList(String corpID, String Param) {
		return refMasterDao.findDocumentList(corpID, Param);
	}
	
	public Map<String, String> findDocumentListByCategory(String corpID, String Param,String cat) {
		return refMasterDao.findDocumentListByCategory(corpID, Param,cat);
	}
	
	public Map<String, String> findByParameterforVat(String corpId, String parameter) {
		return refMasterDao.findByParameterforVat(corpId, parameter);
	}

	public List findAll(String corpID) {
		return refMasterDao.findAll(corpID);
	}

	public List findAccordingCorpId() {
		return refMasterDao.findAccordingCorpId();
	}

	public List<RefMaster> searchListWithoutState(String parameter, String corpId,String nameonform) {
		return refMasterDao.searchListWithoutState(parameter, corpId,nameonform);
	}

	public List<RefMaster> findByParameterlistWithoutState(String parameter, String corpId) {
		return refMasterDao.findByParameterlistWithoutState(parameter, corpId);
	}

	public List<RefMaster> findByParameterWithoutState(String parameter, String searchCode, String searchDescription, String corpId, String status) {
		return refMasterDao.findByParameterWithoutState(parameter, searchCode, searchDescription, corpId, status);
	}

	public List<RefMaster> findByParameters(String corpId, String parameter) {
		return refMasterDao.findByParameters(corpId, parameter);
	}
       public List<RefMaster> findByParameters_status(String corpId, String parameter) {
		return refMasterDao.findByParameters_status(corpId, parameter);
	}

	public List test() {
		return refMasterDao.test();
	}

	public List getBucket(String code, String corpId) {
		return refMasterDao.getBucket(code, corpId);
	}

	public List findOnlyCode(String code, String corpId) {
		return refMasterDao.findOnlyCode(code, corpId);
	}

	public Map<String, String> findByParameterPortCountry(String corpId, String parameter) {
		return refMasterDao.findByParameterPortCountry(corpId, parameter);
	}

	public Map<String, String> findByParameterStatus(String parameter, String corpId, int statusNumber) {
		return refMasterDao.findByParameterStatus(parameter, corpId, statusNumber);

	}

	public List isExisted(String code, String parameter, String corpId,String language) {
		return refMasterDao.isExisted(code, parameter, corpId,language);
	}

	public List occupied(String parameter, String corpId) {
		return refMasterDao.occupied(parameter, corpId);
	}

	public List findCountryCode(String countryName, String sessionCorpID) {
		return refMasterDao.findCountryCode(countryName, sessionCorpID);
	}
	
	public List findStateCode(String stateName, String sessionCorpID){
		return refMasterDao.findStateCode(stateName, sessionCorpID);
	}

	public Map<String, String> findCountry(String corpId, String parameter) {
		return refMasterDao.findCountry(corpId, parameter);
	}
	public Map<String, String> findContinentList(String corpId, String parameter){
		return refMasterDao.findContinentList(corpId, parameter);
	}
	public List findCountryCodeList(String countryName,String sessionCorpID){
		return refMasterDao.findCountryCodeList(countryName, sessionCorpID);
	}
	public List findCountryList(String countryName,String sessionCorpID){
		return refMasterDao.findCountryList(countryName, sessionCorpID);
	}
	public List findContinent(String countryName,String sessionCorpID){
		return refMasterDao.findContinent(countryName, sessionCorpID);
	}
	public Map<String, String> findCountryForPartner(String corpId, String parameter) {
		return refMasterDao.findCountryForPartner(corpId, parameter);
	}

	public List militaryDesc(String code, String corpId) {
		return refMasterDao.militaryDesc(code, corpId);
	}

	public Map<String, String> findByParameterOrderByCode(String corpId, String parameter) {
		return refMasterDao.findByParameterOrderByCode(corpId, parameter);
	}
	public Map<String, String> findByParameterOrderByFlex1(String corpId, String parameter) {
		return refMasterDao.findByParameterOrderByFlex1(corpId, parameter);
	}


	public Map<String, String> findState(String sessionCorpID) {
		return refMasterDao.findState(sessionCorpID);
	}

	public void updateRefmasterStopDate(Date stopDate, int fieldLength, String bucket2, String bucket, Double number, String address1, String address2, String city, String state, String zip, String branch, String sessionCorpID, String code, Date updatedOn, String updatedBy) {
		refMasterDao.updateRefmasterStopDate(stopDate, fieldLength, bucket2, bucket, number, address1, address2, city, state, zip, branch, sessionCorpID, code, updatedOn, updatedBy);

	}

	public Map<String, String> findByParameterWhareHouse(String corpId, String parameter) {
		return refMasterDao.findByParameterWhareHouse(corpId, parameter);
	}

	public Map<String, String> findGlType(String corpId) {
		return refMasterDao.findGlType(corpId);
	}

	public List getSuperVisor(String corpId) {
		return refMasterDao.getSuperVisor(corpId);
	}

	public Map<String, String> findNoteSubType(String corpId, String noteType, String parameter) {
		return refMasterDao.findNoteSubType(corpId, noteType, parameter);
	}

	public List findNoteSubTypeList(String corpId, String noteType, String parameter) {
		return refMasterDao.findNoteSubTypeList(corpId, noteType, parameter);
	}

	public List findRolesUser(Long id, String sessionCorpID) {
		return refMasterDao.findRolesUser(id, sessionCorpID);
	}

	public List findUserRoles(String sessionCorpID, String name) {
		return refMasterDao.findUserRoles(sessionCorpID, name);
	}

	public Map<String, String> findSaleConsultant(String corpId) {
		return refMasterDao.findSaleConsultant(corpId);
	}

	public Map<String, String> findMapList(String corpID, String Param) {
		return refMasterDao.findMapList(corpID, Param);
	}

	public List getDescription(String originCountry, String parameter) {
		return refMasterDao.getDescription(originCountry, parameter);
	}
	public List<String>  getCustomerServiceAnsList(String corpid, String parameter) {
		return refMasterDao.getCustomerServiceAnsList(corpid, parameter);
	}
	public Map<String, String> getCustomerServiceQuesList(String corpid, String parameter) {
		return refMasterDao.getCustomerServiceQuesList(corpid, parameter);
	}
	public List<String> getCustomerServiceQuesFlex2List(String corpid, String parameter) {
		return refMasterDao.getCustomerServiceQuesFlex2List(corpid, parameter);
	}
	public String findDivisionInBucket2(String corpId, String jobType){
		return refMasterDao.findDivisionInBucket2(corpId, jobType);
	}
	public Map<String, String> findCordinaor(String job, String parameter, String sessionCorpID) {
		// TODO Auto-generated method stub
		return refMasterDao.findCordinaor(job, parameter, sessionCorpID);
	}

	public List findCordinaorList(String custJobType, String parameter, String sessionCorpID) {
		
		return refMasterDao.findCordinaorList(custJobType, parameter, sessionCorpID);
	}

	public List findJobByCompanyDivision(String compDivision, String sessionCorpID) {
		return refMasterDao.findJobByCompanyDivision(compDivision, sessionCorpID);
	}

	public Map<String, String> findByParameterRelo(String corpId, String parameter) {
		return refMasterDao.findByParameterRelo(corpId, parameter);
	}

	public List findJobByCompanyDivisionRelo(String compDivision, String sessionCorpID) {
		return refMasterDao.findJobByCompanyDivisionRelo(compDivision, sessionCorpID);
	}

	public List findJobByCompanyDivisionReloServiceOrder(String compDivision, String sessionCorpID) {
		return refMasterDao.findJobByCompanyDivisionReloServiceOrder(compDivision, sessionCorpID);
	}

	public Map<String, String> findByParameterJob(String corpId, String parameter) {
		return refMasterDao.findByParameterJob(corpId, parameter);
	}

	public Map<String, String> findByParameterJobRelo(String corpId, String parameter) {
		return refMasterDao.findByParameterJobRelo(corpId, parameter);
	}

	public String findReloJob(String job, String sessionCorpID, String parameter) {
		return refMasterDao.findReloJob(job, sessionCorpID, parameter);
	}
	public Map<String, String> findByflexValue(String corpId, String parameter){
		return refMasterDao.findByflexValue(corpId, parameter);
	}
	public Map<String, String> findByRelocationServices(String corpId, String parameter) {
		return refMasterDao.findByRelocationServices(corpId, parameter);
	}
	public Map<String, String> findBySurveyQualityParameter(String corpId, String parameter){
		return refMasterDao.findBySurveyQualityParameter(corpId, parameter);
	}
	public Map<String, String> findServiceByJob(String job, String sessionCorpID, String parameter) {
		return refMasterDao.findServiceByJob(job, sessionCorpID, parameter);
	}

	public List findServiceByJob(String jobRelo, String sessionCorpID) {
		return refMasterDao.findServiceByJob(jobRelo, sessionCorpID);
	}

	public Map<String, String> findVatPercentList(String corpID, String parameter) {
		return refMasterDao.findVatPercentList(corpID, parameter);
	}
	public Map<String, String> findByParameterCustomDoc(String corpId, String parameter, String county) {
		return refMasterDao.findByParameterCustomDoc(corpId, parameter, county);
	}


	public String findCodeByParameter(String sessionCorpID, String param,String storageLibrayType) {
		return refMasterDao.findCodeByParameter(sessionCorpID,param,storageLibrayType);
	}


	public List<RefMaster> findByParameterlist(String parameter, String corpId) {
		// TODO Auto-generated method stub
		return refMasterDao.findByParameterlist(parameter, corpId);
	}

	public String findCountryCodeEU(String originCountryCode, String destinationCountryCode, String sessionCorpID) { 
		return refMasterDao.findCountryCodeEU(originCountryCode, destinationCountryCode,sessionCorpID);
	}

	public List findByParameterFlex3(String corpId, String parameter) {
		return refMasterDao.findByParameterFlex3(corpId, parameter);
	}

	public Map<String, String> findCountryIso2(String corpId, String parameter) {
		return refMasterDao.findCountryIso2(corpId, parameter);
	}

	public Map<String, String> findByParameterFlex4(String corpId, String parameter) {
		return refMasterDao.findByParameterFlex4(corpId, parameter);
	}
	
	public Map<String, String> findByParameter(String corpId, String parameter, String job){
		return refMasterDao.findByParameter(corpId, parameter, job);
		
	}

	public Map<String, String> findByParameterService(String sessionCorpID,String parameter, String serviceGroup) {
		return refMasterDao.findByParameterService(sessionCorpID, parameter, serviceGroup);
	}

	public Map<String, String> findByParameterReloParentAgent(String sessionCorpID, String parameter, String parentAgent) {
		return refMasterDao.findByParameterReloParentAgent(sessionCorpID, parameter, parentAgent);
	}

	public Map<String, String> findByParameterASML(String corpId, String parameter) {
		return refMasterDao.findByParameterASML(corpId, parameter); 
	}

	public Map<String, String> findByParameterWithOutASML(String corpId, String parameter) {
		return refMasterDao.findByParameterWithOutASML(corpId, parameter);
	}

	public Map<String, String> findDocumentListAccountPortal(String sessionCorpID, String Param) {
		return refMasterDao.findDocumentListAccountPortal(sessionCorpID, Param);
	}

	public Map<String, String> findByParameterWithoutParent(String corpID, String parameter) {
		return refMasterDao.findByParameterWithoutParent(corpID, parameter);
	}
	public Map<String, String> findByParameterNotCF(String sessionCorpID,String parameter){
		return refMasterDao.findByParameterNotCF(sessionCorpID,parameter); 
	}
	public List findAvaliableVolume(String defaulVolUnit,String containerSize, String sessionCorpID){
		return refMasterDao.findAvaliableVolume(defaulVolUnit, containerSize, sessionCorpID);
		
	}

	public List typeOfWorkFlex1(String typeofWork, String parameter,String sessionCorpID) {
		return refMasterDao.typeOfWorkFlex1(typeofWork, parameter, sessionCorpID);
	}
    public Boolean getCostElementFlag(String corpID){
    	return refMasterDao.getCostElementFlag(corpID);
    }
    public List findByParameterRadius(String sessionCorpID,String parameter){
    	return refMasterDao.findByParameterRadius(sessionCorpID, parameter);
    }
    public Map<String, String> findDescByParameter(String sessionCorpID,String parameter){
    	return refMasterDao.findDescByParameter(sessionCorpID, parameter);
    }
    public List getTimetypeFlex1List(String actionCode, String parameter, String sessionCorpID){
    	return refMasterDao.getTimetypeFlex1List(actionCode, parameter, sessionCorpID);
    }
    public Map<String, String>findWithDescriptionWithBucket2(String parameter, String sessionCorpID){
    	return refMasterDao.findWithDescriptionWithBucket2(parameter, sessionCorpID);
    }

	public Map<String, String> findCodeOnleByParameter(String corpID,
			String parameter) {
		return refMasterDao.findCodeOnleByParameter(corpID, parameter);
	}

	public String getVatDescrCode(String externalCorpID, String parameter, String VatPercent) {
		return refMasterDao.getVatDescrCode(externalCorpID, parameter,  VatPercent);
	}
	public String findByParameterHouse(String sessionCorpID, String warehouse, String parameter){
		return refMasterDao.findByParameterHouse(sessionCorpID, warehouse, parameter);
	}

	public String getBucket2(String code, String corpId) {
		return refMasterDao.getBucket2(code, corpId);
	}
	public Map<String, String> findByParameteFlex2(String sessionCorpID, String parameter){
		return refMasterDao.findByParameteFlex2(sessionCorpID, parameter);
	}
	public List findByRefParameterlist(String parameter, String sessionCorpID){
		return refMasterDao.findByRefParameterlist(parameter,sessionCorpID);
	}
	public String getVatCodeDetail(String vatCode, String sessionCorpID,String vatParameter){
		return refMasterDao.getVatCodeDetail(vatCode, sessionCorpID, vatParameter);
	}
	
	public List<String> findUserType(String sessionCorpID, String parameter){
		return refMasterDao.findUserType(sessionCorpID, parameter);
	}
	
	public List findBillingEmail(String partnerCode, String sessionCorpID){
		return refMasterDao.findBillingEmail(partnerCode, sessionCorpID);
	}
	public List findReportEmailList(String partnerCode, String sessionCorpID){
		return refMasterDao.findReportEmailList(partnerCode, sessionCorpID);
	}
	public List findReportEmailViaCompanyDivision(String corpId,String bookingAgentCode){
		return refMasterDao.findReportEmailViaCompanyDivision(corpId, bookingAgentCode);
	}
	public Map<String, Map<String, String>> findByMultipleParameter(String corpId, String parameters) {
		return refMasterDao.findByMultipleParameter(corpId, parameters);
	}
	public Map<String, String> findByCategoryCode(String sessionCorpID, String parameter){
		return refMasterDao.findByCategoryCode(sessionCorpID, parameter);
	}
	public Map<String, String> findByCategoryCodeByFlex(String sessionCorpID, String extTaskCode,String parameter){
		return refMasterDao.findByCategoryCodeByFlex(sessionCorpID,extTaskCode,parameter);
	}
	
	public Map<String, String> findByParameterUTSIPayVat(String sessionCorpID, String parameters, String externalCorpID) {
		return refMasterDao.findByParameterUTSIPayVat(sessionCorpID, parameters, externalCorpID);
	}

	public Map<String, String> findByParameterAgetntPayVat(String sessionCorpID, String parameters, String externalCorpID) {
		return refMasterDao.findByParameterAgetntPayVat(sessionCorpID, parameters, externalCorpID);
	}
	public String findDivisionForReconcile(String corpID,String chargeCode,String regNum){
		return refMasterDao.findDivisionForReconcile(corpID,chargeCode,regNum);
	}
	public String checkInvoiceDateForWithReconcile(String corpID,String chargeCode,String RegNum){
		return refMasterDao.checkInvoiceDateForWithReconcile(corpID, chargeCode, RegNum);
	}
	public Map<String, String> findByParameterUserGuideModule(String corpId, String parameter){
		return refMasterDao.findByParameterUserGuideModule(corpId, parameter);
	}

	public Map<String, String> findByProperty(String corpId,String... parameter) {
		return refMasterDao.findByProperty( corpId, parameter);
	}

	public Map<String, String> findByParameterAjax(String sessionCorpID,String parameter){
		return refMasterDao.findByParameterAjax( sessionCorpID, parameter);
	}
	
	public Map<String, String> findServiceByJobForlinkUpNotes(String job, String sessionCorpID, String parameter) {
		return refMasterDao.findServiceByJobForlinkUpNotes(job, sessionCorpID, parameter);
	}
	
	public List<RefMasterDTO> getAllParameterValue(String corpId,String parameters){
		return refMasterDao.getAllParameterValue(corpId,parameters);
	}
	public Map<String, String> findByAnswerParamSurveyQualityParameter(String corpId, String parameter){
		return refMasterDao.findByAnswerParamSurveyQualityParameter(corpId, parameter);
	}

	public Map<String, String> findByParameterVatRecGL(String sessionCorpID, String parameter) {
		return refMasterDao.findByParameterVatRecGL(sessionCorpID, parameter);
	}
	public Map<String, Map<String, String>> findByParameterQstVatGL(String sessionCorpID){
		return refMasterDao.findByParameterQstVatGL(sessionCorpID);
	}
	public Map<String, Map<String, String>> findByParameterQstVatAmtGL(String sessionCorpID){
		return refMasterDao.findByParameterQstVatAmtGL(sessionCorpID);
	}
	public Map<String, String> findByParameterVatPayGL(String sessionCorpID, String parameter) {
		return refMasterDao.findByParameterVatPayGL(sessionCorpID, parameter);
	}
	public List findByParameterFlex5(String corpId, String parameter){
		return refMasterDao.findByParameterFlex5(corpId, parameter);
	}
	public void deletedByParameterAndCodeList(String parameter, String code, String corpId){
		refMasterDao.deletedByParameterAndCodeList(parameter, code, corpId);
	}
	public Map<String, String> getFlex1MapByParameter(String corpId,String parameter){
		return refMasterDao.getFlex1MapByParameter(corpId, parameter);
	}
	public Map<String, List> findReloServiceFieldMap(String serviceType, String corpId){
		return refMasterDao.findReloServiceFieldMap(serviceType, corpId);
	}
	public List customsDocList(String sessionCorpID){
	return refMasterDao.customsDocList(sessionCorpID);
	}
	
	public List searchCustomDoc(String code , String description , String sessionCorpID)
	{
		return refMasterDao.searchCustomDoc(code , description , sessionCorpID);
	}
	public List findCodeList()
	{
		return refMasterDao.findCodeList();
	}

	public Map<String, Map> findByParameterUTSIPayVatMap(String parameters, String sessionCorpID) {
		return refMasterDao.findByParameterUTSIPayVatMap(parameters, sessionCorpID);
	}

	public Map<String, Map> findByParameterAgetntPayVatMap(String parameters, String sessionCorpID) {
		return refMasterDao.findByParameterAgetntPayVatMap(parameters, sessionCorpID);
	}

	public Map<String, Map> findVatPercentListMap(String parameters) {
		return refMasterDao.findVatPercentListMap(parameters);
	}
	
	public List serviceTypeList(String corpId, String parameter )
	{
		return refMasterDao.serviceTypeList(corpId,parameter );
	}
	
	public String getSDHubWarehouseOperationalLimit(String sessionCorpID)
	{
		return refMasterDao.getSDHubWarehouseOperationalLimit(sessionCorpID);
	}
	
	public String findByCrTerms(String sessionCorpID, String jobNumber)
	{
		return refMasterDao.findByCrTerms(sessionCorpID, jobNumber);
	}
	public String surveyEmailListForRelo(String sessionCorpID,String parameters){
		return refMasterDao.surveyEmailListForRelo(sessionCorpID, parameters);
	}

	public Map<String, String> findRefMasterFlex8Map(String sessionCorpID, String parameter) {
		return refMasterDao.findRefMasterFlex8Map(sessionCorpID, parameter);
	}

	public Map<String, String> findRefMasterFlex7Map(String sessionCorpID, String parameter) {
		return refMasterDao.findRefMasterFlex7Map(sessionCorpID, parameter);
	}

	public Map<String, String> findRefMasterFlex6Map(String sessionCorpID, String parameter) {
		return refMasterDao.findRefMasterFlex6Map(sessionCorpID, parameter);
	}

	public Map<String, Map> findVatBillingGroupMap(String sessionCorpID, String parameter) {
		return refMasterDao.findVatBillingGroupMap(sessionCorpID, parameter);
	}
	public Map<String, String> findByParameterHubWithWarehouse(String corpId, String parameter,Map<String, String> hub){
		return refMasterDao.findByParameterHubWithWarehouse(corpId, parameter,hub);
	}
	public Map<String, String> findDocumentByJobList(String corpID, String Param,String job){
		return refMasterDao.findDocumentByJobList(corpID, Param, job);
	}
	public Map<String, String> findMapByJobList(String corpID, String Param,String job){
		return refMasterDao.findMapByJobList(corpID, Param, job);
	}
	public Map<String, String> findByParameterWithCurrency(String corpID, String parameter) {
		return refMasterDao.findByParameterWithCurrency(corpID, parameter);
	}
	public Map<String, Set<String>> getMandatoryFileMap(String sessionCorpID,String parameter){
		return refMasterDao.getMandatoryFileMap(sessionCorpID, parameter);
	}
	public Map<String, String> findDefaultStateIsActiveList(String bucket2, String corpId){
		return refMasterDao.findDefaultStateIsActiveList(bucket2, corpId);
	}
	
	public Map<String, String> findCordinaorForCmmDmm(String job, String parameter, String corpID)
	{
		return refMasterDao.findCordinaorForCmmDmm(job, parameter, corpID);
	}
	public Map<String, String> findCordinaorForCorpId(String job, String parameter, String corpID,String checkCorpId){
		return refMasterDao.findCordinaorForCorpId(job, parameter, corpID, checkCorpId);
	}

	public Map<String, String> findByParameterOnlyDescription(String sessionCorpID, String parameter) {
		return refMasterDao.findByParameterOnlyDescription(sessionCorpID, parameter);
	}

	public List findByParameterFlex1(String sessionCorpID, String parameter) {
		return refMasterDao.findByParameterFlex1(sessionCorpID, parameter);
	}
	
	public Map<String, String> findEuvateDescWithFlex(String sessionCorpID,	String parameter){
		return refMasterDao.findEuvateDescWithFlex(sessionCorpID, parameter);
	}
	public Map<String, String> findByParentParameterValues(String sessionCorpID,	String parameter){
		return refMasterDao.findByParentParameterValues(sessionCorpID, parameter);
	}
	public String getValueFromParameterControl(String sessionCorpID,String parameter){
		return refMasterDao.getValueFromParameterControl(sessionCorpID, parameter);
	}
	public List getParentParameterList(String sessionCorpID,String parameter,String instructionCode){
		return refMasterDao.getParentParameterList(sessionCorpID, parameter,instructionCode);
	}
	public List getParentParameterListAjax(String sessionCorpID,String parameter,String serviceCode,String instructionCode){
		return refMasterDao.getParentParameterListAjax(sessionCorpID, parameter,serviceCode,instructionCode);
	}

	public List findOnlyCodeByOpenperiod(String parameter, String corpId) {
		return refMasterDao.findOnlyCodeByOpenperiod(parameter, corpId);
	}
	public String findBucket2Value(String code, String parameter){
		return refMasterDao.findBucket2Value(code, parameter);
	}
	
	public Map<String, String> getCodeWithDecription(String sessionCorpID){
		return refMasterDao.getCodeWithDecription(sessionCorpID);
	}
	public Map<String, String> findByParameterFromServiceorder(String corpId, String parameter, String job) {
		return refMasterDao.findByParameterFromServiceorder(corpId, parameter, job);
	}
	public List findMaxIdByParameter(String parameter,String sessionCorpID){
		return refMasterDao.findMaxIdByParameter(parameter, sessionCorpID);
	}
	public List findFlex5ByDescription(String countryName,String sessionCorpID,String parameterName){
		return refMasterDao.findFlex5ByDescription(countryName, sessionCorpID,parameterName);
	}
	public List getDescriptionBylanguage(String parameter,String language,String sessionCorpID){
		return refMasterDao.getDescriptionBylanguage(parameter, language, sessionCorpID);
	}
	public Map<String, String> findByParameter_isactive(String corpId, String parameter, String job){
		return refMasterDao.findByParameter_isactive(corpId, parameter, job);
	}
	public List getServiceTypeListByWarehouse(String corpId, String parameter, String wareHouse){
		return refMasterDao.getServiceTypeListByWarehouse(corpId, parameter, wareHouse);
	}
	public List getTaskPlannerListAjax(String parameter,String extTaskCode){
		return refMasterDao.getTaskPlannerListAjax(parameter, extTaskCode);
	}
	public List getAssigneeListAjax(String parameter,String assignee){
		
		return refMasterDao.getAssigneeListAjax(parameter, assignee);
		
	}
	public String getCompanyUUID(String code,String parameter){
		return refMasterDao.getCompanyUUID(code, parameter);
	}
	public String findDescriptionByFlex5(String code,String parameter){
		return refMasterDao.findDescriptionByFlex5(code, parameter);
	}

	public Map findCodeOnleByParameterCountry(String corpID, String parameter) {
		return refMasterDao.findCodeOnleByParameterCountry( corpID,  parameter);
	}

	public List findPartnerVatCodeDescription(String flexcode, String defaultVatDec, String corpId,String flag) {
		// TODO Auto-generated method stub
		return refMasterDao.findPartnerVatCodeDescription(flexcode, defaultVatDec, corpId, flag);
	}

	public String findPartnerVatBillingCode(String code, String corpId,String compDivis) {
		// TODO Auto-generated method stub
		return refMasterDao.findPartnerVatBillingCode(code, corpId,compDivis);
	}

	public Map<String, String> findVatDescriptionList(String corpID, String parameter, String flexcode,
			String defaultVatDec,String partnerVatDec,String flagb) {
		// TODO Auto-generated method stub
		return refMasterDao.findVatDescriptionList(corpID, parameter, flexcode, defaultVatDec, partnerVatDec, flagb);
	}

	public List findAccountLinePayVatCodeDescription(String flexcode, String defaultVatDec, String corpId,String flag) {
		// TODO Auto-generated method stub
		return refMasterDao.findAccountLinePayVatCodeDescription(flexcode, defaultVatDec, corpId, flag);
	}

	public String findAccountLinePayVatBillingCode(String code, String corpId,String compDivis) {
		// TODO Auto-generated method stub
		return refMasterDao.findAccountLinePayVatBillingCode(code, corpId, compDivis);
	}

	public List findAccountLineRecVatCodeDescription(String flexcode, String defaultVatDec,String partnerVatDec, String corpId,String flag) {
		// TODO Auto-generated method stub
		return refMasterDao.findAccountLineRecVatCodeDescription(flexcode, defaultVatDec,partnerVatDec, corpId,flag);
	}

	public Map<String, String> findPayVatDescriptionList(String corpID, String parameter, String flexcode,
			String defaultVatDec,String flag) {
		// TODO Auto-generated method stub
		return refMasterDao.findPayVatDescriptionList(corpID, parameter, flexcode, defaultVatDec, flag);
	}

	public Map<String, String> findPartnerVatDescriptionList(String corpID, String parameter, String flexcode,
			String defaultVatDec, String flagb) {
		// TODO Auto-generated method stub
		return refMasterDao.findPartnerVatDescriptionList(corpID, parameter, flexcode, defaultVatDec, flagb);
	}
	public String findPartnerVatBillingFlexCode(String corpId, String vatdescription){
		return refMasterDao.findPartnerVatBillingFlexCode(corpId,  vatdescription);
	}

	public String findPartnerVatBillingDescription(String corpId,String vatdescription) {
		// TODO Auto-generated method stub
		return refMasterDao.findPartnerVatBillingDescription(corpId, vatdescription);
	}

	public Map<String, String> findPartnerVatDescription(String corpID, String parameter, String flexcode,
			String defaultVatDec, String flagb) {
		// TODO Auto-generated method stub
		return refMasterDao.findPartnerVatDescription(corpID, parameter, flexcode, defaultVatDec, flagb);
	}

	public Map<String, String> findVatDescriptionListForPricing(String corpID, String parameter, String flexcode, String defaultVatDec, String partnerVatDec, String flagb) {
		// TODO Auto-generated method stub
		return refMasterDao.findVatDescriptionListForPricing(corpID, parameter, flexcode, defaultVatDec, partnerVatDec, flagb);
	}

	public Map<String, String> findPayVatDescription(String corpID, String parameter, String flexcode,
			String defaultVatDec, String flag) {
		// TODO Auto-generated method stub
		return refMasterDao.findPayVatDescription(corpID, parameter, flexcode, defaultVatDec, flag);
	}

	public Map<String, String> findPartnerRecVatDescriptionList(String corpID, String parameter) {
		// TODO Auto-generated method stub
		return refMasterDao.findPartnerRecVatDescriptionList(corpID, parameter);
	}

	public Map<String, String> findRecVatDescriptionData(String corpID, String parameter) {
		// TODO Auto-generated method stub
		return refMasterDao.findRecVatDescriptionData(corpID, parameter);
	}

	public Map<String, String> findPayVatDescriptionData(String corpID, String parameter) {
		// TODO Auto-generated method stub
		return refMasterDao.findPayVatDescriptionData(corpID, parameter);
	}

}
