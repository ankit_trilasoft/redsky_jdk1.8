<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="logTimeList.title"/></title>   
    <meta name="heading" content="<fmt:message key='logTimeList.heading'/>"/> 
    <style type="text/css"> 

.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

.table tfoot td {
border:1px solid #E0E0E0;
}
.table2 thead th, table2HeaderTable td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0 0;
border-color:#99BBE8 #99BBE8 #99BBE8 -moz-use-text-color;
border-style:solid;
border-right:medium;
color:#99BBE8;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}
span.pagelinks {
    display: none;
    text-align: right;
  
}
.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

</style> 
<style type="text/css">h2 {background-color: #FBBFFF}</style>  
</head>
<fmt:setTimeZone value="${sessionTimeZone}" scope="session"/>
<c:set var="displayDateFormat" value="dd-MMM-yy" scope="session"/>
<c:set var="displayDateTimeFormat" value="dd-MMM-yyyy HH:mm" scope="session"/>
<c:set var="displayDateTimeEditFormat" value="dd-MMM-yyyy HH:mm" scope="session"/>
<c:set var="corpTimeZone" value="${sessionTimeZone}" scope="session"/>
<c:if test="${logTimeList!='[]'}"> 
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:285px !important;">
<tr valign="top"> 	
	<td align="left" style="font-size:1.2em !important;"><b>Login Logout Time List</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
<div class="spn">&nbsp;</div>
<s:form id="logTimeList" cssStyle="margin-top:0px;" action="" method="post">  
<s:set name="logTimeList" value="logTimeList" scope="request"/> 
<div style="oveflow-y:scroll;overflow-x: hidden; max-height:200px;">
<display:table name="logTimeList" class="table" requestURI="" id="logTimeList" style="width:300px !important"  pagesize="500" >   
 		 <display:column   titleKey="userlogfileList.Login" >
    <fmt:formatDate value="${logTimeList.login}" pattern="${displayDateTimeFormat}"/> 
    </display:column>
    <display:column   titleKey="userlogfileList.Logout"   >
    <fmt:formatDate value="${logTimeList.logout}" pattern="${displayDateTimeFormat}"/> 
    </display:column>
 		 
</display:table>

</div>
</s:form>
</c:if> 
<c:if test="${logTimeList=='[]'}">
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:150px;">
<tr valign="top"> 	
	<td align="left"><b>No List </b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
</c:if>
		  	