package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.service.GenericManager;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.DriverCommissionDao;
import com.trilasoft.app.dao.VanLineDao;
import com.trilasoft.app.model.DriverCommissionPlan;
import com.trilasoft.app.model.VanLine;

public class DriverCommissionDaoHibernate extends GenericDaoHibernate<DriverCommissionPlan, Long> implements DriverCommissionDao {
	public DriverCommissionDaoHibernate() {   
        super(DriverCommissionPlan.class);   
    }
	
	public List getPlanList(String corpId){
	List PlanList=getHibernateTemplate().find("select distinct plan from DriverCommissionPlan");
	return PlanList;
	}
	public List getcontractList(String corpId){
		//String att="select distinct contract from contract where corpID = 'SSCW' and (date_format(ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d') or(ending is null or date_format(ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d'))) and ((date_format(begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d')) or(begin is null or date_format(begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d')))";
		List ContractList=getHibernateTemplate().find("select distinct contract from Contract where corpID = '"+corpId+"' and (date_format(ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d') or(ending is null or date_format(ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d'))) and ((date_format(begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d')) or(begin is null or date_format(begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d'))) order by contract ");
		return ContractList;	
	}
	public List findChargeList(String corpId){
		List chargeList=new ArrayList();
		List list1= getSession().createSQLQuery("select ch.charge,ch.description from charges ch inner JOIN contract c on c.contract=ch.contract and c.corpId='"+corpId+"' where ch.corpId='"+corpId+"' and (date_format(c.ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d') or(c.ending is null or date_format(c.ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d'))) and ((date_format(c.begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d')) or(c.begin is null or date_format(c.begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d')))")
		.addScalar("charge", Hibernate.STRING) 
		.addScalar("description", Hibernate.STRING).list(); 
	 Iterator it=list1.iterator();
	 DTO DTO1=null;
	 while(it.hasNext()){
	 Object []obj=(Object[]) it.next();
	 DTO1=new DTO();
	 DTO1.setCharge(obj[0]);
	 DTO1.setDescription(obj[1]);
	 chargeList.add(DTO1);
	 }
	return chargeList;
	}
	public class DTO{
		private Object charge;
		private Object description;
		private Object contract;
		public Object getCharge() {
			return charge;
		}
		public void setCharge(Object charge) {
			this.charge = charge;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getContract() {
			return contract;
		}
		public void setContract(Object contract) {
			this.contract = contract;
		}
		
	}
	public List getChargeList(String charge,String description,String corpId){
		List chargeList=new ArrayList();
		//String tt="SELECT ch.charge, ch.description, ch.contract from charges ch, contract ct where ch.contract = ct.contract and ch.corpid = '"+corpId+"' and begin <= now() and (ending is null or ending >= now()) and charge like '%" + charge.replaceAll("'", "''") + "%' and description like '%"+description.replaceAll("'", "''") +"%'";
		List list1= getSession().createSQLQuery("SELECT ch.charge, ch.description, ch.contract from charges ch, contract ct where ch.contract = ct.contract and ch.corpid = '"+corpId+"' and ct.corpid = '"+corpId+"' and (begin is null or begin <= now()) and (ending is null or ending >= now()) and ch.charge like '%" + charge.replaceAll("'", "''") + "%' and ch.description like '%"+description.replaceAll("'", "''") +"%'")
		  .addScalar("charge", Hibernate.STRING) 
		  .addScalar("description", Hibernate.STRING) 
		  .addScalar("contract", Hibernate.STRING).list();
		Iterator it=list1.iterator();
		 DTO DTO1=null;
		 while(it.hasNext()){
		 Object []obj=(Object[]) it.next();
		 DTO1=new DTO();
		 DTO1.setCharge(obj[0]);
		 DTO1.setDescription(obj[1]);
		 DTO1.setContract(obj[2]);
		 chargeList.add(DTO1);
		 }
	return chargeList;
	}
	public List getPlanListValue(String planValue,String corpId){
		List planList=getHibernateTemplate().find("select charge from DriverCommissionPlan where plan='"+planValue+"' and corpid='"+corpId+"'");
		return planList;
	}
	public List getChargeListValue(String planValue,String chargeValue,String corpId){
		List list1= getSession().createSQLQuery("select concat(if(description is null ,'',description),'#',if(contract is null,'',contract),'#',if(percent is null,'',percent) ,'#',if(distributionAmount is true,'1','0') ,'#',if (grossRevenue is true,'1','0'))  from drivercommissionplan where plan='"+planValue+"' and charge='"+chargeValue+"'  and corpid='"+corpId+"'").list();
		return list1;
	}
	public List getCommissionPlanList(String corpId){
		return getHibernateTemplate().find("from DriverCommissionPlan");
		
	}
	public List getChargeList(String corpId){
		List list1= getSession().createSQLQuery("select distinct ch.charge from charges ch inner JOIN contract c on c.contract=ch.contract and c.corpId='"+corpId+"' where ch.corpId='"+corpId+"' and (date_format(c.ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d') or(c.ending is null or date_format(c.ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d'))) and ((date_format(c.begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d')) or(c.begin is null or date_format(c.begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d'))) order by ch.charge ").list(); 
		
		return list1;
	}
	public List getCommissionSearchPlanList(String plan,String charge,String description,String contract,String corpId){
		
		return getHibernateTemplate().find("from DriverCommissionPlan where plan like '%"+plan.replaceAll("'", "''") +"%' and description like '%"+description.replaceAll("'", "''") +"%' and contract like '%"+contract.replaceAll("'", "''") +"%' and charge like '%"+charge.replaceAll("'", "''") +"%'");
	}
	public void updateCommissionPercent(BigDecimal percent,String planAmount,String plan,String contract,String charge,String corpId){
	
			getHibernateTemplate().bulkUpdate("update DriverCommissionPlan set percent='"+percent+"',amountBasis='"+planAmount+"' where plan='"+plan+"' and charge='"+charge+"' and contract='"+contract+"' and corpid='"+corpId+"'");
	
			//getHibernateTemplate().bulkUpdate("update DriverCommissionPlan set percent='"+percent+"',grossRevenue=true,distributionAmount=false where plan='"+plan+"' and charge='"+charge+"' and contract='"+contract+"' and corpid='"+corpId+"'");
	
		
	
	}
	 public int delDriverCommission(String delCommissionId){
		 return getHibernateTemplate().bulkUpdate("delete from DriverCommissionPlan where id in("+delCommissionId+")");
	 }
	 public int checkPlan(String plan,String charge,String contract,String corpId){
		 if(plan==null)plan="";
		 if(charge==null)charge="";
		 int counter=0;
		 if(contract==null || contract.equals("")){
			 String temp=this.getSession().createSQLQuery("select count(*) from drivercommissionplan where plan='"+plan+"' and charge='"+charge+"' and contract='' and corpid='"+corpId+"' ").list().get(0).toString();	
			   if(!temp.equals("0") && temp!=null ){
				   counter++; 
			   } 
		 }else{
		 String[] contractList=contract.split(",");
		 for (String sType : contractList) {
			 String contractValue=sType.trim();
			 String temp=this.getSession().createSQLQuery("select count(*) from drivercommissionplan where plan='"+plan+"' and charge='"+charge+"' and contract like '%"+contractValue+"%' and corpid='"+corpId+"' ").list().get(0).toString();	
			   if(!temp.equals("0") && temp!=null ){
				   counter++; 
			   }
		      }
		 }
		 //int countValue=Integer.parseInt(counter);
		 int countValue=counter;
		 return countValue;
		 
	 }
	  public List getChargedriverList(String charge,String corpId){
		  //String arg="SELECT ch.contract from charges ch, contract ct where ch.contract = ct.contract and ch.corpid = '"+corpId+"' and begin <= now() and (ending is null or ending >= now()) and ch.charge ='"+charge+"'";
		  List list1= getSession().createSQLQuery("SELECT ch.contract from charges ch, contract ct where ch.contract = ct.contract and ch.corpid = '"+corpId+"' and ct.corpid = '"+corpId+"' and ( begin is null or begin <= now()) and (ending is null or ending >= now()) and ch.charge ='"+charge+"'").list();
		  return list1;
	  }
	  
}
