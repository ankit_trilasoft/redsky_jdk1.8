package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.Discount;

public interface DiscountDao extends GenericDao<Discount, Long>{
	public List discountList(String contract, String chargeCode,String corpId);
	public List discountMapList(String corpId);
}
