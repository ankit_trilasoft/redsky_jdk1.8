<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>
	<title>Pricing Engine</title>
	<meta name="heading" content="Pricing Engine"/>
    <meta name="menu" content="UserIDHelp"/>
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />
<style>

#content-containertable.override{
width:150px;
}

#content-containertable.override div{
height:460px;
overflow:scroll;
}
.filter-head{color: #484848;font:12px Arial, Helvetica, sans-serif ; font-weight:bold; cursor: pointer; }
.price-bg{background-image:url(images/ng-bg.png); background-repeat:repeat-x;margin:0px;padding:0px;}
.filter-bg{background-image:url(images/border-filter.png); background-repeat:no-repeat; text-align:left;margin:3px; height:77px; width:952px;}
</style>

</head>
<%--<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w" type="text/javascript"></script>
--%>

<style type="text/css">

</style>
<c:set var="buttons"> 
     <input type="button" class="cssbutton1" style="width:120px; height:25px"  
        onclick="recalculatePricing()"  
        value="Recalculate Pricing"/>  

</c:set>
<s:form id="agentPricing" name="agentPricing" action='locateFreight.html' method="post" validate="true">
<s:hidden key="pricingControl.weight"/>
<s:hidden key="pricingControl.mode"/>
<s:hidden key="pricingControl.packing"/>
<s:hidden key="pricingControl.containerSize"/>
<s:hidden key="pricingControl.isOrigin"/>
<s:hidden key="pricingControl.isDestination"/>
<s:hidden key="pricingControl.originLatitude"/>
<s:hidden key="pricingControl.originLongitude"/>
<s:hidden key="pricingControl.originCountry"/>
<s:hidden key="pricingControl.originPOE"/>
<s:hidden key="pricingControl.destinationPOE"/>
<s:hidden key="pricingControl.id"/>
<s:hidden key="pricingControl.pricingID"/>
<s:hidden key="freightId"/>
<s:hidden key="freightCurrency"/>
<s:hidden key="freightPrice"/>
<s:hidden key="freightOriginPortCode"/>
<s:hidden key="freightDestinationPortCode"/>
<s:hidden key="freightOriginPortCodeName"/>
<s:hidden key="freightDestinationPortCodeName"/>

<div id="newmnav" style="!margin-bottom:5px;float:left;">
		  <ul>
		  <c:if test="${!param.popup}">
		 	 	<li><a href="agentPricingBasicInfo.html?id=${pricingControl.id}"><span>Basic Info</span></a></li>
			  	<li><a href="agentPricingOriginInfo.html?id=${pricingControl.id}"><span>Origin Options</span></a></li>
			  	<li id="newmnav1" ><a class="current"><span>Freight Options<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  	<li><a href="agentPricingDestinationInfo.html?id=${pricingControl.id}"><span>Destination Options</span></a></li>
			  	<li><a href="agentPricingSummaryInfo.html?id=${pricingControl.id}"><span>Pricing Summary</span></a></li>
			  	<li><a href="agentPricingAllPricingInfo.html?id=${pricingControl.id}"><span>Pricing Options</span></a></li>
			  	<li><a href="agentPricingList.html"><span>Quote List</span></a></li>
			  	<sec-auth:authComponent componentId="module.tab.pricingWizard.accountingTab">
			  	 <c:if test="${not empty pricingControl.serviceOrderId}">
			  	<li><a onclick="return checkControlFlag();"><span>Accounting</span></a></li>
		   	</c:if>	
		  </sec-auth:authComponent>
		  </c:if>	
		   <c:if test="${param.popup}">
		  	<li><a href="agentPricingList.html?serviceOrderId=${serviceOrderId}&decorator=popup&popup=true"><span>Pricing Engine List</span></a></li>
		  </c:if>
		  	
		  </ul>
		</div>
		<!--<div style="float: left; font-size: 13px; margin-left: 50px;margin-top:-3px;">
		<a href="http://www.sscw.com/movingservices/international_move.html" target="_blank" style="text-decoration:underline;">
		
		<a href="#"  style="color:#330000;" onclick="javascript:window.open('http://www.sscw.com/redsky/agentportal/','help','width=1004,height=500,scrollbars=yes,left=100,top=50');">
		<img src="images/wizard-button.png" border="0"/></a></div>
		--><div class="spn">&nbsp;</div>
<table class="mainDetailTable" cellspacing="0" cellpadding="0" border="0"  style="width: 100%;" >
  <tbody>
  <tr>
 
  </tr>
  <tr>
  <td valign="top" align="center" style="margin:0px;padding: 0px; background-color:#F6F9FA;"><table width="100%" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" >
    
    <tr>
    <td>
    <c:set var="ischecked" value="false"/>
	<c:if test="${pricingControl.freightMarkUpControl}">
	<c:set var="ischecked" value="true"/>
	</c:if>
      <div class="pricetab_bg">&nbsp;<div class="price_tleft"><a href="javascript:;"></a></div><div class="price_tright">&nbsp;Freights Results: 
      <sec-auth:authComponent componentId="module.checkBox.pricingWizard.markUp">
      <span style="padding-left:80px;">Price MarkUp<s:checkbox key="pricingControl.freightMarkUpControl" value="${ischecked}" fieldValue="true" onclick="updateFreightMarkUpControl()"/></span>
     </sec-auth:authComponent>
      </div></div>
      
      </td>
    </tr>
      <tr>
    
      <td align="left" height="1px"></td>
    </tr>
    <tr>
   
     <c:if test="${empty pricingControl.sequenceNumber}">
    <display:table name="agentFreightList" class="table" requestURI="" id="agentFreightListId" defaultsort="8" pagesize="20" style="width:100%" >   
	<display:column property="contractType"  sortable="true" title="Type"/>
	<display:column property="carrier"  sortable="true" title="Carrier"/>
   	<display:column property="origin"  sortable="true" title="Origin"/>
   	<display:column property="originPortCode"  sortable="true" title="Origin-Port"/>
   	<display:column property="destination"  sortable="true" title="Destination"/>
   	<display:column property="destinationPortCode"  sortable="true" title="Dest-Port"/>
   	<display:column property="equipmentType"  sortable="true" title="Equip Type"/>
    <display:column property="totalPrice"  sortable="true" title="Price"/>
    <display:column property="laneComments"  sortable="true" title="Lane Comments"/>
    <display:column property="destTHCValue"  sortable="true" title="Dest THC" style="width:60px"/>
    <display:column property="destTHCDetails"  sortable="true" title="THC Basis"/>
    <display:column property="baseCurrency"  sortable="true" title="Currency"/>
    <display:column   title="" style="width:10px" >
 		<c:if test="${agentFreightListId.id==selectedFreightId}">
 		<input style="vertical-align:bottom;" type="radio" name="radioFreight" checked="checked" onclick="updateFreight('${agentFreightListId.id}','${agentFreightListId.baseCurrency}','${agentFreightListId.totalPrice}','${agentFreightListId.originPortCode}','${agentFreightListId.destinationPortCode}','${agentFreightListId.originPortCodeName}','${agentFreightListId.destinationPortCodeName}')"/>
 		</c:if>
 		
 	</display:column>
   	</display:table>
   </c:if>
    <c:if test="${not empty pricingControl.sequenceNumber}">
    <display:table name="agentFreightList" class="table" requestURI="" id="agentFreightListId" defaultsort="8" pagesize="20" style="width:100%" >   
	<display:column property="contractType"  sortable="true" title="Type"/>
	<display:column property="carrier"  sortable="true" title="Carrier"/>
   	<display:column property="origin"  sortable="true" title="Origin"/>
   	<display:column property="originPortCode"  sortable="true" title="Origin-Port"/>
   	<display:column property="destination"  sortable="true" title="Destination"/>
   	<display:column property="destinationPortCode"  sortable="true" title="Dest-Port"/>
   	<display:column property="equipmentType"  sortable="true" title="Equip Type"/>
    <display:column property="totalPrice"  sortable="true" title="Price"/>
    <display:column property="laneComments"  sortable="true" title="Lane Comments"/>
    <display:column property="destTHCValue"  sortable="true" title="Dest THC" style="width:60px"/>
    <display:column property="destTHCDetails"  sortable="true" title="THC Basis"/>
    <display:column property="baseCurrency"  sortable="true" title="Currency"/>
    <display:column   title="" style="width:10px" >
 		<c:if test="${agentFreightListId.id==selectedFreightId}">
 		<input style="vertical-align:bottom;" type="radio" name="radioFreight" disabled="disabled" checked="checked" onclick="updateFreight('${agentFreightListId.id}','${agentFreightListId.baseCurrency}','${agentFreightListId.totalPrice}','${agentFreightListId.originPortCode}','${agentFreightListId.destinationPortCode}','${agentFreightListId.originPortCodeName}','${agentFreightListId.destinationPortCodeName}')"/>
 		</c:if>
 		
 	</display:column>
   	</display:table>
   </c:if>
    

    
  </table></td>
        
    
        </tr>
        </tbody>
        </table>
<!--<c:out value="${buttons}" escapeXml="false" />   
--></s:form>

<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>"djConfig="parseOnLoad:true, isDebug:false"></script>

<style><%@ include file="/common/calenderStyle.css"%></style>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>

<script language="javascript" type="text/javascript">
<%-- //var geocoder;
//var map;
window.onload = function() { 

      // Create new map object
      //map = new GMap2(document.getElementById("map"));

      // Create new geocoding object
      //geocoder = new GClientGeocoder();
		//addToMapDestination();	
	//locateFreight();
	 
      // Retrieve location information, pass it to addToMap()
     // geocoder.getLocations(document.agentPricing.address.value, addToMap);
   }

--%>

function selectedValue(target, target1){
	var id= target;
	var pricingControlID=target1;
	var url="updatePricingDetail.html?ajax=1&decorator=simple&popup=true&tarrifApplicability=Origin&pricingDetailsId=" + encodeURI(id)+'&pricingControlID='+encodeURI(pricingControlID);
	http2.open("GET", url, true);
  	//http2.onreadystatechange = handleHttpResponseFieldList;
   	http2.send(null);

}

function openPartnerProfile(target)
{
	window.open('findPartnerProfileList.html?id='+target+'&decorator=popup&popup=true','profile','scrollbars=1,width=900,height=450');
	//findPartnerProfileList.html?code=${partner.partnerCode}&partnerType=${partnerType}&id=${partner.id}"
}

String.prototype.trim = function() {
	    return this.replace(/^\s+|\s+$/g,"");
	}
	String.prototype.ltrim = function() {
	    return this.replace(/^\s+/,"");
	}
	String.prototype.rtrim = function() {
	    return this.replace(/\s+$/,"");
	}
	String.prototype.lntrim = function() {
	    return this.replace(/^\s+/,"","\n");
	}
	
	
	function getHTTPObject()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
	}
    var http2 = getHTTPObject();

    
   </script>

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
	animatedcollapse.addDiv('address', 'fade=1,persist=0,hide=0')
	animatedcollapse.addDiv('info', 'fade=1,persist=0,hide=1')
	animatedcollapse.addDiv('pricing', 'fade=1,persist=0,hide=1')
	animatedcollapse.init()
</script>
<script language="JavaScript" type="text/JavaScript">
var namesVec = new Array("130.png", "129.png");
var root='images/';
function swapImg(ima){
// divides the path
nr = ima.getAttribute('src').split('/');
// gets the last part of path, ie name
nr = nr[nr.length-1]
// former was .split('.')[0];
 
if(nr==namesVec[0]){ima.setAttribute('src',root+namesVec[1]);}
else{ima.setAttribute('src',root+namesVec[0]);}
 
}

function updateFreight(target, baseCurrency, freightPrice,originPortCode,destinationPortCode,originPortCodeName,destinationPortCodeName){
	var id = document.forms['agentPricing'].elements['pricingControl.id'].value;
	var freightId=target;
	document.forms['agentPricing'].elements['freightId'].value=freightId;
	document.forms['agentPricing'].elements['freightCurrency'].value=baseCurrency;
	document.forms['agentPricing'].elements['freightPrice'].value=freightPrice;
	document.forms['agentPricing'].elements['freightOriginPortCode'].value=originPortCode;
	document.forms['agentPricing'].elements['freightDestinationPortCode'].value=destinationPortCode;
	document.forms['agentPricing'].elements['freightOriginPortCodeName'].value=originPortCodeName;
	document.forms['agentPricing'].elements['freightDestinationPortCodeName'].value=destinationPortCodeName;
	
	var url="updateFreightDetail.html?ajax=1&decorator=simple&popup=true&pricingControlID=" + encodeURI(id)+'&freightId='+encodeURI(freightId)+'&freightCurrency='+encodeURI(baseCurrency)+'&freightPrice='+encodeURI(freightPrice);
	http2.open("GET", url, true);
   	http2.send(null);
	
}
function checkControlFlag(){
	var id= '${pricingControl.id}';
	var sequenceNumber='${pricingControl.sequenceNumber}';
	var url="checkControlFlag.html?ajax=1&decorator=simple&popup=true&sequenceNumber=" +sequenceNumber;
	http2.open("GET", url, true);
  	http2.onreadystatechange = handleHttpResponseCheckControlFlag;
   	http2.send(null);
}

function handleHttpResponseCheckControlFlag(){
	if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results=='C'){
                var sid='${pricingControl.serviceOrderId}';
                	location.href='accountLineList.html?sid='+sid;
                	return true;
                }else{
                	alert('Quote is still not accepted');
                	return false;
                }
             }
}

function updateFreightMarkUpControl(){
	var freightMarkUpFlag=document.forms['agentPricing'].elements['pricingControl.freightMarkUpControl'].checked;
	var id= '${pricingControl.id}';
	var url="updateFreightMarkUpControl.html?ajax=1&decorator=simple&popup=true&pricingControlID="+id+"&freightMarkUpFlag=" +freightMarkUpFlag;
	http2.open("GET", url, true);
   	http2.send(null);
}

function recalculatePricing(){
	var id= '${pricingControl.id}';
	agree= confirm("Are you sure?");
	if(agree){
		var url = "recalculatePricing.html?id="+id;
		document.forms['agentPricing'].action = url;
		document.forms['agentPricing'].submit();
	}
}

</script>
<script type="text/javascript">   
<c:if test="${hitFlag == 100}" >
		<c:redirect url="/agentPricingSummaryInfo.html?id=${pricingControl.id}"  />
	
</c:if>
</script>  
		  	