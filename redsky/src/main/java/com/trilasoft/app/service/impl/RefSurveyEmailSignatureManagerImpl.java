package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.RefSurveyEmailSignatureDao;
import com.trilasoft.app.model.RefSurveyEmailSignature;
import com.trilasoft.app.service.RefSurveyEmailSignatureManager;

public class RefSurveyEmailSignatureManagerImpl extends GenericManagerImpl<RefSurveyEmailSignature, Long> implements RefSurveyEmailSignatureManager{
	RefSurveyEmailSignatureDao refSurveyEmailSignatureDao;
	public RefSurveyEmailSignatureManagerImpl(RefSurveyEmailSignatureDao refSurveyEmailSignatureDao)
	{
		super(refSurveyEmailSignatureDao);
		this.refSurveyEmailSignatureDao=refSurveyEmailSignatureDao;
	}

}
