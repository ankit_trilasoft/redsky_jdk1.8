package com.trilasoft.app.service;


import org.appfuse.service.GenericManager; 
import com.trilasoft.app.model.Miscellaneous; 

import java.util.Date;
import java.util.List; 
import java.util.Map;
 
public interface MiscellaneousManager extends GenericManager<Miscellaneous, Long>
{ 
    public List<Miscellaneous> findBySequenceNumber(String sequenceNumber);
    public List findMaximumId();
    public List checkById(Long id);
    public void updateTrackingStatus(Date beginPacking, Date endPacking, Date packA, Date beginLoad, Date endLoad, Date loadA, Date deliveryShipper, Date deliveryLastDay, Date deliveryTA, Date deliveryA, Long id,String packATime,String deliveryATime,String deliveryTATime,String loadATime);
    public void updateBilling(Date deliveryA, Long id, String loginUser);
    public void updateTrackingStatusDestination(Date sitDestinationIn,Long id);
    public void updateTrackingStatussitDestinationA(Date sitDestinationA,Long id);
    public void updateTrackingStatusDestinationTA(String sitDestinationTA,Long id);
    public void updateTrackingStatusDestinationDays(int sitDestinationDays,Long id);
	public List findHaulingMgtList(Date fromdate, Date todate,String corpid,String filterdDrivers,String opsHubList,String driverAgency,String selfHaul,String fromArea, String toArea, String fromZipCodes, String toZipCodes, String fromRadius, String toRadius, String job,String filterdTripNumber,String JobStatus,String filterdRegumber);
	public List findByOwner(String partnerCode,String corpId);
	public List findByOwnerAndContact(String partnerCode,String corpId);
	public List findByOwnerFromTruck(String partnerCode,String truckNumber,String corpId);
	public List findByOwnerPerticularTruck(String truckNumber,String corpId);
	public List findByTrailerPerticularTruck(String truckNumber,String corpId);
	public void updateMiscellaneous(Long id,String status);
	public Map<String, String> getDriverAgency(String corpid);
	public void updateTrackingStatusLateReason(String partyResponsible,String lateReason, Long id);
	public void updatePackingDriverId(String partnerCode, String service,String shipNumber, String sessionCorpID);
	public List partyResponsible(String corpId, String spNumber);
	public String weightForAmountDistribution(Long miscId,String grpId, String sessionCorpId);
	public String findMasterOrderWeight(String id,String corpId);
	public List findZipCodeList(String sessionCorpID, String lattitude1,String lattitude2, String longitude1, String longitude2);
	public List findLattLongList(String sessionCorpID, String zipCode);
	public void updateTripNumber(String tripNumber,String tripid);
	public List getTripOrder(String tripNumber, String sessionCorpID);
	public List findmaxTripNumber(String sessionCorpID);
	public List getTripNumber(String tripNumber, String sessionCorpID);
	public void removeTripNumber(String tripid);
	public List findTripName(String sessionCorpID);
	public void updateJobStatus(String sessionCorpID,String soStatus,String orderNo);
	public List findToolTipForVehicle(String orderNo, String sessionCorpID, String job);
	public void updateTargetRevenueRecognition(Date deliveryA, Long id, String loginUser);
}  