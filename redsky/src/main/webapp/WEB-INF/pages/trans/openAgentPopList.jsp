<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix = "c"  uri = "http://java.sun.com/jsp/jstl/core" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Custom&nbsp;Information</title>  
<script type="text/javascript">

</script>
<style type="text/css">
.clear { clear:both;}
body { margin:0px; padding:0px;}

</style>
</head>
<body id="dashboard" style="background-image:url(images/bg.gif);">

<c:set var="noHeader" value="false" scope="session"/> 
<div class="clear"></div>

<tr valign="top"> 	
	<td align="left"><b>Agent Reqest&nbsp;Information</b></td>
	<td align="left">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
	<tr valign="top"><td colspan="2">		
<s:set name="findAgentFeedbackList" value="findAgentFeedbackList" id="findAgentFeedbackList"  scope="request"/>
<s:form id="searchForm"  action="customInfo" method="post" validate="true" >

</s:form>
<div style="width:100%;overflow-x:auto;">
<display:table name="findAgentFeedbackList" class="table" requestURI="" id="findAgentFeedbackList" style="width:100%;border-collapse:collapse;" defaultsort="1"  >
       <c:if test="${not empty findAgentFeedbackList}">
       <c:if test="${findAgentFeedbackList.corpID=='TSFT'}">
       
       <display:column style="background-color:#f2dede" title="Created By"  >
        <div align="left" style="margin: 0px;">
        <c:out value="${findAgentFeedbackList.createdBy}" />
           </div> 
     </display:column>
      <display:column style=" background-color:#f2dede" title="Comment"> 
      <div align="left" style="margin: 0px;">
      <c:out value="${findAgentFeedbackList.comment}" />
           </div> 
    </display:column>
     <display:column style="width:50%;background-color:#f2dede" title="Reason" >
     <div align="left" style="margin: 0px;">
      <c:forEach var="entry" items="${reason}">
        <c:if test="${entry.key==findAgentFeedbackList.reason}">
           <c:out value="${entry.value}" /> 
        </c:if> 
      </c:forEach> 
           </div>  
    </display:column>
    </c:if>
       <c:if test="${findAgentFeedbackList.corpID!='TSFT'}">
       <display:column style=" background-color:#dff0d8" title="createdBy" >
        <div align="left" style="margin: 0px;">  
            <c:out value="${findAgentFeedbackList.createdBy}" />
           </div> 
     </display:column>
      <display:column style="background-color:#dff0d8" title="comment"   > 
      <div align="left" style="margin: 0px;">  
            <c:out value="${findAgentFeedbackList.comment}" />
           </div> 
    </display:column>
     <display:column style="width:50%;background-color:#dff0d8" title="reason">
     <div align="left" style="margin: 0px; ">
       <c:forEach var="entry" items="${reason}">
        <c:if test="${entry.key==findAgentFeedbackList.reason}">
           <c:out value="${entry.value}" /> 
        </c:if> 
       </c:forEach> 
           </div>  
    </display:column>
    </c:if>
    </c:if>
</display:table>
</div>
</td></tr>
</body> 
		  	