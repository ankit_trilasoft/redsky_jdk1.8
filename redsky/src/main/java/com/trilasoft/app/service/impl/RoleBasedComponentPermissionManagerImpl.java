package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.RoleBasedComponentPermissionDao;
import com.trilasoft.app.model.RoleBasedComponentPermission;
import com.trilasoft.app.service.RoleBasedComponentPermissionManager;

public class RoleBasedComponentPermissionManagerImpl extends GenericManagerImpl<RoleBasedComponentPermission, Long> implements RoleBasedComponentPermissionManager{
	
	RoleBasedComponentPermissionDao RoleBasedComponentPermissionDao;
	

    public RoleBasedComponentPermissionManagerImpl(RoleBasedComponentPermissionDao RoleBasedComponentPermissionDao) { 
        super(RoleBasedComponentPermissionDao); 
        this.RoleBasedComponentPermissionDao = RoleBasedComponentPermissionDao; 
    }
    
	public boolean isAccessAllowed(String componentId, String roles) {
		return RoleBasedComponentPermissionDao.isAccessAllowed(componentId, roles);
	}

	public List findMaximumId() {
		return RoleBasedComponentPermissionDao.findMaximumId();
	}

	public List searchcompanyPermission(String componentId, String description,String role) {
		return RoleBasedComponentPermissionDao.searchcompanyPermission(componentId, description,role);
	}

	public List getRole(String corpID) {
		return RoleBasedComponentPermissionDao.getRole(corpID);
	}

	public int getPermission(String componentId, String roles) {
		return RoleBasedComponentPermissionDao.getPermission(componentId, roles);
	}

	public List getModulePermissions(String[] tableList, String roles) {
		return RoleBasedComponentPermissionDao.getModulePermissions(tableList, roles);
	}

	public String getPartnerIdList(String userName) {
			return RoleBasedComponentPermissionDao.getPartnerIdList(userName);
	}

	public String getUserRoleInTransaction(String transId, String partnerIdList) {
		   return RoleBasedComponentPermissionDao.getUserRoleInTransaction(transId, partnerIdList);
	}
	public void updateCurrentRolePermission(String corpId){
		RoleBasedComponentPermissionDao.updateCurrentRolePermission(corpId);
	}
}
