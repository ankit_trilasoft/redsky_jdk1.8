<%@page import="java.util.SortedMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.math.BigDecimal"%>


<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<%@page import="java.util.TreeMap"%>
<head>
    <title>Reciprocity Analysis</title>
    <meta name="heading" content="Reciprocity Analysis "/>
<style>

</style>

</head>
<s:form id="summaryForm" action="" method="post" validate="true">
<div id="newmnav" >
		  <ul>
		  	
		    <li  id="newmnav1" style="background:#FFF "><a class="current"><span>Reciprocity Analysis<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
</div>
<div class="spn" style="width:900px;">&nbsp;</div>
<div style="padding-bottom:3px;"></div>
<table class="mainDetailTable" cellpadding="3" cellspacing="0" border="0" style="margin: 0px; padding: 0px; width: 950px">
	<tr style="margin: 0px; padding: 0px;">
		
		<td class="heading-bg" align="center">AGENT&nbsp;GROUP</td>
		<td class="heading-bg" align="center">AGENT&nbsp;NAME</td>
		<td class="heading-bg" align="center">COUNTRY</td>
		<td class="heading-bg" align="center" >CITY</td>
		<td class="heading-bg" align="center" width="60px"></td>
		<td class="heading-bg" width="20px" align="center" >OA&nbsp;GIVEN</td> 
		<td class="heading-bg" width="20px" align="center">DA&nbsp;GIVEN</td>
		<td class="heading-bg" width="20px" align="center">SUB-TOTAL</td>
		<td class="heading-bg" width="20px" align="center">OA&nbsp;RECVD</td>
		<td class="heading-bg" width="20px" align="center">DA&nbsp;RECVD</td>
		<td class="heading-bg" width="20px" align="center">SUB-TOTAL</td> 
	</tr>
	
	<% 
		SortedMap <String, String> companyMap = (SortedMap <String, String>)request.getAttribute("companyMap");
		Iterator companyIterator = companyMap.entrySet().iterator(); 
		String AgentGroup=""; 
		String agentName="";
		String Country="";
		String City="";
		long DA_Total_Given=0;
	    long OA_Total_Given=0;
	    long Total_Given=0;
	    long DA_Total_Recd=0;
        long OA_Total_Recd=0;
        long Total_Recd=0;
        long DA_Total_WtsGiven=0;
        long OA_Total_WtsGiven=0;
        long Total_WtsGiven=0;
        long DA_Total_WtsRecd=0;
	    long OA_Total_WtsRecd=0;
	    long Total_WtsRecd=0;
		while (companyIterator.hasNext()) {
			int OA_GIVEN=0;
			long OA_TONNAGE_given=0;
			int DA_GIVEN=0;
			long DA_TONNAGE_given=0;
			int OA_RECD=0;
			long OA_TONNAGE_recd=0;
			int DA_RECD=0;
			long DA_TONNAGE_recd=0;
			int subtotal_given=0;
			int subtotal_recd=0;
			long subtotal_TONNAGE_recd=0;
			long subtotal_TONNAGE_given=0;
			Map.Entry entry = (Map.Entry) companyIterator.next();
		    String key = (String) entry.getKey();
		    String[] str1 = key.toString().split("#");
		    AgentGroup=str1[0];
		    if(AgentGroup.equalsIgnoreCase("ZZZ")){
	    		  AgentGroup="Un-Group"; 
	    	 }
		    if(str1[1].toString().equals("a"))
		    {
		    	agentName="";
		    }else {
		    agentName=str1[1];
		    agentName=agentName.replaceAll("~","-");
		    City=str1[2];
		    }
			String detail= companyMap.get(key);
			String[] str2 = detail.toString().split("#");
			if(str2[0].toString().equals("a"))
		    {
				Country="";
		    }else {
		    	Country=str2[0];
		    	if(Country.equalsIgnoreCase("ZZZ")){
	            	  Country="";  
	              }
		    } 
			OA_GIVEN=Integer.parseInt(str2[1])  ; 
			DA_GIVEN=Integer.parseInt(str2[2]); 
			subtotal_given=Integer.parseInt(str2[3]); 
			OA_RECD=Integer.parseInt(str2[4]); 
			DA_RECD=Integer.parseInt(str2[5]); 
			subtotal_recd=Integer.parseInt(str2[6]); 
			OA_TONNAGE_given=Long.parseLong(str2[7]) ; 
			DA_TONNAGE_given=Long.parseLong(str2[8]); 
			subtotal_TONNAGE_given=Long.parseLong(str2[9]); 
			OA_TONNAGE_recd=Long.parseLong(str2[10]); 
			DA_TONNAGE_recd=Long.parseLong(str2[11]); 
			subtotal_TONNAGE_recd=Long.parseLong(str2[12]);
			
			
	%>
	        
	        <%
	        	if(agentName.equals(""))
	        	{
	        		OA_Total_Given=OA_Total_Given+OA_GIVEN;
                	DA_Total_Given=DA_Total_Given+DA_GIVEN;
                	Total_Given=Total_Given+subtotal_given;
                	OA_Total_Recd=OA_Total_Recd+OA_RECD;
                	DA_Total_Recd=DA_Total_Recd+DA_RECD;
                	Total_Recd=Total_Recd+subtotal_recd;
                	OA_Total_WtsGiven=OA_Total_WtsGiven+OA_TONNAGE_given;
                	DA_Total_WtsGiven=DA_Total_WtsGiven+DA_TONNAGE_given;
                	Total_WtsGiven=Total_WtsGiven+subtotal_TONNAGE_given;
                	OA_Total_WtsRecd=OA_Total_WtsRecd+OA_TONNAGE_recd;
                	DA_Total_WtsRecd=DA_Total_WtsRecd+DA_TONNAGE_recd;
                	Total_WtsRecd=Total_WtsRecd+subtotal_TONNAGE_recd;
	        %>
	        		<tr height="20px" style="margin: 0px;  padding: 0px;">
	        		<td class="list-columnmain2" style=""><b><%=AgentGroup%></b></td> 
	        		<td class="list-columnmain2" style=""><b><%=agentName%></b></td>
					<td class="list-columnmain2" style="text-align: right;"><b></b></td>
					<td class="list-columnmain2" style="text-align: right;"></td>
					<td class="list-columnmain2" style="text-align: right;"><b>Agt Total #</b></td>
					<td class="list-columnmain2" style=" text-align: right;"><b>
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=OA_GIVEN%>" /></div>
					</b></td>
					<td class="list-columnmain2" style=" text-align: right;"><b>
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=DA_GIVEN%>" /></div>
					</b></td>
					<td class="list-columnmain2 " style="text-align: right;"><b>
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=subtotal_given%>" /></div>
					</b></td>
					<td class="list-columnmain2" style="text-align: right;"><b>
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=OA_RECD%>" /></div>
					</b></td>
					<td class="list-columnmain2" style="text-align: right;"><b>
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=DA_RECD%>" /></div>
					</b></td>
					<td class="list-columnmain2 " style="text-align: right;"><b>
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=subtotal_recd%>" /></div>
					</b></td>
					</tr>
					<tr height="25px" style="margin: 0px;  padding: 0px;">
					<td class="list-columnmain2" style="text-align: right;" ></td>
					<td class="list-columnmain2" style="text-align: right;" ></td>
					<td class="list-columnmain2" style="text-align: right;" ></td>
					<td class="list-columnmain2" style="text-align: right;" ></td>
					<td class="list-columnmain2" style="text-align: right;" ><b>Agt Total Wts </b></td>
					<td class="list-columnmain2" style=" text-align: right;"><b>
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=OA_TONNAGE_given%>" /></div>
					</b></td>
					<td class="list-columnmain2" style=" text-align: right;"><b>
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=DA_TONNAGE_given%>" /></div>
					</b></td>
					<td class="list-columnmain2 " style="text-align: right;"><b>
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=subtotal_TONNAGE_given%>" /></div>
					</b></td>
					<td class="list-columnmain2" style=" text-align: right;"><b>
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=OA_TONNAGE_recd%>" /></div>
					</b></td>
					<td class="list-columnmain2" style="text-align: right;"><b>
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=DA_TONNAGE_recd%>" /></div>
					</b></td>
					<td class="list-columnmain2 " style="text-align: right;"><b>
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=subtotal_TONNAGE_recd%>" /></div>
					</b></td> 
					</tr>
	        <%
	        if(!companyIterator.hasNext()){%>
	        
	        <c:if test="${agentGroup ==''}">
	        <tr height="20px" style="margin: 0px;  padding: 0px;" >
             <td class="list-columnmain2" width="10%"  ><b>Report Total </b></td>
             <td class="list-columnmain2" width="16%" > <span class="spacer"></span></td>
             <td class="list-columnmain2" width="10%" ></td>
             <td class="list-columnmain2" width="10%" ></td>
             <td class="list-columnmain2" width="10%" style="text-align: right;" ><b>Total #</b></td>
             <td class="list-columnmain2" width="9%" style=" text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=OA_Total_Given%>" /></b>
             </td>
             <td class="list-columnmain2" width="9%" style=" text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=DA_Total_Given%>" /></b>
             </td>
             <td class="list-columnmain2" width="9%" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0" 
              groupingUsed="true" value="<%=Total_Given%>" /></b>
             </td>
             <td class="list-columnmain2" width="9%" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=OA_Total_Recd%>" /></b>
             </td>

             <td class="list-columnmain2" width="9%" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=DA_Total_Recd%>" /></b>
             </td>
             <td class="list-columnmain2" width="9%"  style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=Total_Recd%>" /></b>
             </td>
             </tr>
                                                 <tr height="25px">
             <td class="list-columnmain2"  style="text-align: right;" ></td>
             <td class="list-columnmain2"  style="text-align: right;" ></td>
             <td class="list-columnmain2"  style="text-align: right;" ></td>
             <td class="list-columnmain2"  style="text-align: right;" ></td>
             <td class="list-columnmain2"  style="text-align: right;" ><b>Total Wts</b> </td>
             <td class="list-columnmain2"  style=" text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=OA_Total_WtsGiven%>" /></b>
             </td>
             <td class="list-columnmain2" width="" style=" text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=DA_Total_WtsGiven%>" /></b>
             </td>
             <td class="list-columnmain2" width="" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=Total_WtsGiven%>" /></b>
             </td>
             <td class="list-columnmain2" width="" style=" text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=OA_Total_WtsRecd%>" /></b>
             </td>
             <td class="list-columnmain2" width="" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=DA_Total_WtsRecd%>" /></b>
             </td>
             <td class="list-columnmain2" width="" style="text-align: right;"><b>
             <fmt:formatNumber type="number" maxFractionDigits="0"
              groupingUsed="true" value="<%=Total_WtsRecd%>" /></b>
             </td>
             </tr>
	        </c:if>	
	        	<% }
	        	}
	        	else
	        	{
	        %>
		        	<tr height="20px" style="margin: 0px;  padding: 0px;" >
		        	<td class="rec-columnmain" style=""><%=AgentGroup%></td> 
		        	<td class="rec-columnmain" style=""><%=agentName%></td>
					<td class="rec-columnmain" style=""><%=Country%></td>
					<td class="rec-columnmain" style=""><%=City%></td>
					<td class="rec-columnmain" style="text-align: right;" >#</td>
					<td class="rec-columnmain" style=" text-align: right;">
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=OA_GIVEN%>" /></div>
					</td>
					<td class="rec-columnmain" style=" text-align: right;">
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=DA_GIVEN%>" /></div>
					</td>
					<td class="rec-columnmain " style="text-align: right;">
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=subtotal_given%>" /></div>
					</td>
					<td class="rec-columnmain" style="text-align: right;">
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=OA_RECD%>" /></div>
					</td>
					</td>
					<td class="rec-columnmain" style="text-align: right;">
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=DA_RECD%>" /></div>
					</td>
					<td class="rec-columnmain " style="text-align: right;">
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=subtotal_recd%>" /></div>
					</td>
					</tr>
					<tr height="25px"> 
					<td class="list-columnmain" style="text-align: right;" ></td>
					<td class="list-columnmain" style="text-align: right;" ></td>
					<td class="list-columnmain" style="text-align: right;" ></td>
					<td class="list-columnmain" style="text-align: right;" ></td>
					<td class="list-columnmain" style="text-align: right;" >Wts </td>
					<td class="list-columnmain" style=" text-align: right;">
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=OA_TONNAGE_given%>" /></div>
					</td>
					<td class="list-columnmain" style=" text-align: right;">
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=DA_TONNAGE_given%>" /></div>
					</td>
					<td class="list-columnmain " style="text-align: right;">
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=subtotal_TONNAGE_given%>" /></div>
					</td>
					<td class="list-columnmain" style=" text-align: right;">
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=OA_TONNAGE_recd%>" /></div> 
					</td>
					<td class="list-columnmain" style="text-align: right;">
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=DA_TONNAGE_recd%>" /></div> 
					</td>
					<td class="list-columnmain " style="text-align: right;">
					<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                     groupingUsed="true" value="<%=subtotal_TONNAGE_recd%>" /></div> 
					</td> 
					</tr>
	        <%
	        	}
	        %>
			
			
			
			
				
				<%}%>
			</table></s:form>
			<script type="text/javascript">
			try{
			window.print();
			}
			catch(e){}
			</script>