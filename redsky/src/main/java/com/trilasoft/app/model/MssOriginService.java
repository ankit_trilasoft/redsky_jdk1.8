package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;


@Entity
@Table(name="mssoriginservice")
public class MssOriginService extends BaseObject{
	private Long id;
	private Long mssId;
	private String corpId;
	private String oriItems;
	private Boolean oriAppr=new Boolean(true);
	private Boolean oriCod=new Boolean(false);
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof MssOriginService))
			return false;
		MssOriginService castOther = (MssOriginService) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(mssId, castOther.mssId)
				.append(corpId, castOther.corpId)
				.append(oriItems, castOther.oriItems)
				.append(oriAppr, castOther.oriAppr)
				.append(oriCod, castOther.oriCod)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(mssId).append(corpId)
				.append(oriItems).append(oriAppr).append(oriCod)
				.append(createdOn).append(createdBy).append(updatedOn)
				.append(updatedBy).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("mssId", mssId).append("corpId", corpId)
				.append("oriItems", oriItems).append("oriAppr", oriAppr)
				.append("oriCod", oriCod).append("createdOn", createdOn)
				.append("createdBy", createdBy).append("updatedOn", updatedOn)
				.append("updatedBy", updatedBy).toString();
	}

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Long getMssId() {
		return mssId;
	}

	public void setMssId(Long mssId) {
		this.mssId = mssId;
	}
	@Column
	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}		
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Boolean getOriAppr() {
		return oriAppr;
	}

	public void setOriAppr(Boolean oriAppr) {
		this.oriAppr = oriAppr;
	}
	@Column
	public Boolean getOriCod() {
		return oriCod;
	}

	public void setOriCod(Boolean oriCod) {
		this.oriCod = oriCod;
	}
	@Column
	public String getOriItems() {
		return oriItems;
	}

	public void setOriItems(String oriItems) {
		this.oriItems = oriItems;
	}
	

}
