<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<head>
	<title>Survey Schedule</title>
	<meta name="heading" content="Survey Schedule"/>
    <meta name="menu" content="UserIDHelp"/>
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />
    <script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>

<style>
#overlay11 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
.rounded-top {  
border:1px solid #AFAFAF;
-moz-border-radius-topleft: .8em;
-webkit-border-top-left-radius:.8em;
-moz-border-radius-topright: .8em;
-webkit-border-top-right-radius:.8em;
border-top-left-radius: 0.8em;
border-top-right-radius: 0.8em;
box-shadow: -2px -2px 5px #666; 
border-bottom:none;
   }
.rounded-bottom {   
border:1px solid #AFAFAF;
-moz-border-radius-bottomleft: .8em;
-webkit-border-bottom-left-radius:.8em;
-moz-border-radius-bottomright: .8em;
-webkit-border-bottom-right-radius:.8em;
border-bottom-left-radius: 0.8em;
border-bottom-right-radius: 0.8em;
box-shadow: 0px 3px 3px #666;
border-top:none;  
}
#close a{
background:transparent url(images/close.gif) repeat scroll 0 0;
border:0 none;
display:block;
float:right;
font-size:0;
height:15px;
line-height:0;
position:relative;
text-decoration:none;
width:15px;
z-index:902;
}

#content {
color:#003366;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
text-decoration:none;
z-index:901;
}


#content-containertable.override{
width:212px;
}

#content-containertable.override div{
height:460px;
overflow:scroll;
}

#content-containertable.override1{
width:190px;
}

#content-containertable.override1 div{
height:150px;
overflow:scroll;
}

.listwhitetext-head {
color:#FFFFFF;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
text-decoration:none;
}

#mainPopup {
padding-left:5px;
padding-right:5px;
padding-top:5px;
margin-top:-20px;
}


.inner-tab a, a:link a:active {
color:#FFFFFF;text-decoration:none;font-weight:bold;font-family:arial,verdana; font-size:11px;
}
.price_tleft{float:left;padding:3px 2px 0px 8px;!margin-top:0px;}

.filter-head{color: #484848;font:11px arial,verdana ; font-weight:bold; cursor: pointer; }
.filter-bg{background-image:url(images/border-filter.png); background-repeat:no-repeat; text-align:left;margin:3px; height:77px; width:852px;}
</style>

</head>
<!--<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w" type="text/javascript"></script>
-->
<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghT6f_0fta-IgTNYK12tZ1qaQ6vwTxRZr2mDluVMFpm7o4X5mkZsz5WZKQ" type="text/javascript"></script>

<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>" djConfig="parseOnLoad:true, isDebug:false"></script>



<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->
<script language="javascript" type="text/javascript">
function completeTimeString() {
	
		stime1 = document.forms['surveyScheduleForm'].elements['surveyTime'].value;
		stime2 = document.forms['surveyScheduleForm'].elements['surveyTime2'].value;;
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['surveyScheduleForm'].elements['surveyTime'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['surveyScheduleForm'].elements['surveyTime'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['surveyScheduleForm'].elements['surveyTime'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['surveyScheduleForm'].elements['surveyTime'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['surveyScheduleForm'].elements['surveyTime'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['surveyScheduleForm'].elements['surveyTime'].value = "0" + stime1;
			}
		}
		if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
			if(stime2.length==1 || stime2.length==2){
				if(stime2.length==2){
					document.forms['surveyScheduleForm'].elements['surveyTime2'].value = stime2 + ":00";
				}
				if(stime2.length==1){
					document.forms['surveyScheduleForm'].elements['surveyTime2'].value = "0" + stime2 + ":00";
				}
			}else{
				document.forms['surveyScheduleForm'].elements['surveyTime2'].value = stime2 + "00";
			}
		}else{
			if(stime2.indexOf(":") == -1 && stime2.length==3){
				document.forms['surveyScheduleForm'].elements['surveyTime2'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
			}
			if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
				document.forms['surveyScheduleForm'].elements['surveyTime2'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
			}
			if(stime2.indexOf(":") == 1){
				document.forms['surveyScheduleForm'].elements['surveyTime2'].value = "0" + stime2;
			}
		}
		//IsValidTime();
	
		var testH=document.forms['surveyScheduleForm'].elements['surveyTime'].value;
		testH= testH.substring(0,2);
		var test1H=document.forms['surveyScheduleForm'].elements['surveyTime2'].value;
		test1H= test1H.substring(0,2);
		var testM=document.forms['surveyScheduleForm'].elements['surveyTime'].value;
		testM= testM.substring(3,5);
		var test1M=document.forms['surveyScheduleForm'].elements['surveyTime2'].value;
		test1M= test1M.substring(3,5);
		if(testH > 24){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['surveyTime'].value='';
		return false;
		}
		if(testM > 60){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['surveyTime'].value='';
		return false;
		}
		if(test1H > 24){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['surveyTime2'].value='';
		return false;
		}
		if(test1M > 60){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['surveyTime2'].value='';
		return false;
		}
		var testCheck1 = document.forms['surveyScheduleForm'].elements['surveyTime'].value;
		testCheck1 = testCheck1.substring(0,2);
		var testCheck2 = document.forms['surveyScheduleForm'].elements['surveyTime2'].value;
		testCheck2 = testCheck2.substring(0,2);
		if(testCheck1 > testCheck2 && testCheck2!=00 ){
		alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['surveyTime2'].value='';
		return false;
		}
		
	}

function consultantTimeString() {
	
		stime1 = document.forms['surveyScheduleForm'].elements['consultantTime'].value;
		stime2 = document.forms['surveyScheduleForm'].elements['consultantTime2'].value;;
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['surveyScheduleForm'].elements['consultantTime'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['surveyScheduleForm'].elements['consultantTime'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['surveyScheduleForm'].elements['consultantTime'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['surveyScheduleForm'].elements['consultantTime'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['surveyScheduleForm'].elements['consultantTime'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['surveyScheduleForm'].elements['consultantTime'].value = "0" + stime1;
			}
		}
		if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
			if(stime2.length==1 || stime2.length==2){
				if(stime2.length==2){
					document.forms['surveyScheduleForm'].elements['consultantTime2'].value = stime2 + ":00";
				}
				if(stime2.length==1){
					document.forms['surveyScheduleForm'].elements['consultantTime2'].value = "0" + stime2 + ":00";
				}
			}else{
				document.forms['surveyScheduleForm'].elements['consultantTime2'].value = stime2 + "00";
			}
		}else{
			if(stime2.indexOf(":") == -1 && stime2.length==3){
				document.forms['surveyScheduleForm'].elements['consultantTime2'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
			}
			if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
				document.forms['surveyScheduleForm'].elements['consultantTime2'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
			}
			if(stime2.indexOf(":") == 1){
				document.forms['surveyScheduleForm'].elements['consultantTime2'].value = "0" + stime2;
			}
		}
		//IsValidTime();
		var testH=document.forms['surveyScheduleForm'].elements['consultantTime'].value;
		testH= testH.substring(0,2);
		var test1H=document.forms['surveyScheduleForm'].elements['consultantTime2'].value;
		test1H= test1H.substring(0,2);
		var testM=document.forms['surveyScheduleForm'].elements['consultantTime'].value;
		testM= testM.substring(3,5);
		var test1M=document.forms['surveyScheduleForm'].elements['consultantTime2'].value;
		test1M= test1M.substring(3,5);
		if(testH > 24){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['consultantTime'].value='';
		return false;
		}
		if(testM > 60){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['consultantTime'].value='';
		return false;
		}
		if(test1H > 24){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['consultantTime2'].value='';
		return false;
		}
		if(test1M > 60){
			alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['consultantTime2'].value='';
		return false;
		}
		var testCheck1 = document.forms['surveyScheduleForm'].elements['consultantTime'].value;
		testCheck1 = testCheck1.substring(0,2);
		var testCheck2 = document.forms['surveyScheduleForm'].elements['consultantTime2'].value;
		testCheck2 = testCheck2.substring(0,2);
		if(testCheck1 > testCheck2 && testCheck2!=00 ){
		alert('Please enter correct Time')
		document.forms['surveyScheduleForm'].elements['consultantTime2'].value='';
		return false;
		}
	}


	
function IsValidTime() {

var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['surveyScheduleForm'].elements['surveyTime'].value;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("Time is not in a valid format. Please use HH:MM format");
document.forms['surveyScheduleForm'].elements['surveyTime'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("'Pre Move Survey' time must between 0 to 23 (Hrs)");
document.forms['surveyScheduleForm'].elements['surveyTime'].focus();
return false;
}
if (minute<0 || minute > 59) {
alert ("'Pre Move Survey' time must between 0 to 59 (Min)");
document.forms['surveyScheduleForm'].elements['surveyTime'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Pre Move Survey' time must between 0 to 59 (Sec)");
document.forms['surveyScheduleForm'].elements['surveyTime'].focus();
return false;
}

// **************Check for Survey Time2*************************

var time2Str = document.forms['surveyScheduleForm'].elements['surveyTime2'].value;
var matchTime2Array = time2Str.match(timePat);
if (matchTime2Array == null) {
alert("Time is not in a valid format. please Use HH:MM format");
document.forms['surveyScheduleForm'].elements['surveyTime2'].focus();
return false;
}
hourTime2 = matchTime2Array[1];
minuteTime2 = matchTime2Array[2];
secondTime2 = matchTime2Array[4];
ampmTime2 = matchTime2Array[6];

if (hourTime2 < 0  || hourTime2 > 23) {
alert("'Pre Move Survey' time must between 0 to 23 (Hrs)");
document.forms['surveyScheduleForm'].elements['surveyTime2'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'Pre Move Survey' time must between 0 to 59 (Min)");
document.forms['surveyScheduleForm'].elements['surveyTime2'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'Pre Move Survey' time must between 0 to 59 (Sec)");
document.forms['surveyScheduleForm'].elements['surveyTime2'].focus();
return false;
}
}

function openTargetOriginAddressLocation() {
        var targetAddress=document.forms['surveyScheduleForm'].elements['targetAddress'].value;
        map = new GMap2(document.getElementById("map"));
        geocoder = new GClientGeocoder();
        geocoder.getLocations(targetAddress, addToMapOrigin); 
	}
</script>
<script language="javascript" type="text/javascript">
var geocoder;
var map;
var letter1 = "";
var letter = "";
var letter3 = "";
var flag1="0";
var count1=-1;

window.onload = function() { 
		map = new GMap2(document.getElementById("map"));
		geocoder = new GClientGeocoder();
		openTargetOriginAddressLocation();
			
   }
   var surveyData;
   var toDisplay =new Array();
   var surveyDataMap = new Object();
   var letterMap = new Object();
   var barToProcess = new Object();
   
   function showhide(id){ 
		if (document.getElementById){ 
			obj = document.getElementById(id); 
			if (obj.style.display == "none"){ 
				obj.style.display = ""; 
			} else { 
				obj.style.display = "none"; 
			} 
		} 
	} 
   
   
   var j = 0;
   var addrArray =new Array();
  	function getSurveySchedulePoints(){
		dojo.xhrPost({
	       form: "surveyScheduleForm",
	       timeout: 30000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){
	        		//var jsonData = dojo.toJson(data)
	        var d=document.getElementById("surveyScheduleSOList");
	        d.innerHTML='';	  
  			var i=0;
  			var oldEstimator;
  			
  			
  			if(jsonData.count>=0)
  			{
	           for(i=0; i < jsonData.surveySchedulePoints.length; i++){
	             	
	             	letter1 = jsonData.surveySchedulePoints[i].activityName;
	             	
	             	var newEstimator= jsonData.surveySchedulePoints[i].estimator
	             	if( letter1== 'SO')
	             	{
	             	   letter = String.fromCharCode("A".charCodeAt(0) + j);
	             	   j++;
					   var tempLetter = letter;	             	   
		 	   		   if(flag1=="1")
		 	   		   {
			 	   		   var temp=String.fromCharCode("A".charCodeAt(0) + count1);
			 	   		   letter = temp+letter;
		 	   		   }		 	   		   
	             	}	

	             	//setTimeout("",1000);
	           		document.forms['surveyScheduleForm'].elements['fromDate'].value=jsonData.surveySchedulePoints[i].startDay;
	           		document.forms['surveyScheduleForm'].elements['toDate'].value = jsonData.surveySchedulePoints[i].add7Day;
	           		document.forms['surveyScheduleForm'].elements['surveyTime'].value=jsonData.surveySchedulePoints[i].fromTime;
	           		document.forms['surveyScheduleForm'].elements['surveyTime2'].value = jsonData.surveySchedulePoints[i].toTime;
	           		
	           		 if(newEstimator!=oldEstimator){
	           		 if( letter1== 'SO')
	           		 {
	           		 d.innerHTML+='<table width="198" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;"></td><td><a style="text-decoration: underline; cursor: pointer;" onclick=moveDateBackByOne("refreshAppointments","'+jsonData.surveySchedulePoints[i].estimator+'")>'+jsonData.surveySchedulePoints[i].estimator+'</a></td></tr></table>'+
	           		 '</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" class="listwhitetext" width="200px" >'+jsonData.surveySchedulePoints[i].survey+' - '+
					''+jsonData.surveySchedulePoints[i].surveyTime1+''+
					'-'+jsonData.surveySchedulePoints[i].surveyTime2+''  
					//+'  <a style="text-decoration: underline; cursor: pointer; color:red; font-weight:bold" onclick=markerClickFnCunsltant("'+jsonData.surveySchedulePoints[i].sonumber+'","'+jsonData.surveySchedulePoints[i].customerName+'","'+encodeURI(jsonData.surveySchedulePoints[i].address)+'")>('+letter+')</td></table>';
					+'  <a style="text-decoration: underline; cursor: pointer; color:red; font-weight:bold" onclick=javascript:showhide("'+jsonData.surveySchedulePoints[i].sonumber+'")>('+letter+')</td></table>';
	           		 d.innerHTML+='<table class="listwhitetext" style="margin:0px; paddding:0px; border:1px dotted #8FCDFF;-moz-border-radius:8px; background-color:#efefef; " id="'+jsonData.surveySchedulePoints[i].sonumber+'"><tr><td><b>C/F#:</b> </td><td>'+jsonData.surveySchedulePoints[i].sonumber+' </td><td><a style="text-decoration: underline; cursor: pointer; color:red; font-weight:bold" onclick=javascript:showhide("'+jsonData.surveySchedulePoints[i].sonumber+'")><img src="${pageContext.request.contextPath}/images/cancel.png"/></td></tr><tr><td><b>Name:</b> </td><td>'+jsonData.surveySchedulePoints[i].customerName+'</td></tr><tr><td valign="top"><b>Address:</b> </td><td valign="top" colspan="2">'+jsonData.surveySchedulePoints[i].address+'</td></tr></table>';
	           		 showhide(jsonData.surveySchedulePoints[i].sonumber);
	           		 }
	           		 else
	           		 {
	           		 	d.innerHTML+='<table width="198" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;"></td><td><a style="text-decoration: underline; cursor: pointer;"onclick=moveDateBackByOne("refreshAppointments","'+jsonData.surveySchedulePoints[i].estimator+'")>'+jsonData.surveySchedulePoints[i].estimator+'</a></td></tr></table>'+
	           		 '</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" class="listwhitetext" width="200px" >'+jsonData.surveySchedulePoints[i].survey+' - '+
					''+jsonData.surveySchedulePoints[i].surveyTime1+''+
					'-'+jsonData.surveySchedulePoints[i].surveyTime2+''  
					+' '+jsonData.surveySchedulePoints[i].activityName+' </td></table>';
	           		 }
	           		 
	           		 }else{
	           		 if( letter1== 'SO')
	           		 {
	           		 d.innerHTML+='<table width="198" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"></tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" class="listwhitetext" width="200px" >'+jsonData.surveySchedulePoints[i].survey+' - '+
					''+jsonData.surveySchedulePoints[i].surveyTime1+''+
					'-'+jsonData.surveySchedulePoints[i].surveyTime2+'' 
					+'  <a style="text-decoration: underline; cursor: pointer; color:red; font-weight:bold" onclick=javascript:showhide("'+jsonData.surveySchedulePoints[i].sonumber+'")>('+letter+')</td></table>';
	           		 d.innerHTML+='<table class="listwhitetext" style="margin:0px; paddding:0px; border:1px dotted #8FCDFF;-moz-border-radius:8px; background-color:#efefef;" id="'+jsonData.surveySchedulePoints[i].sonumber+'"><tr><td><b>C/F#:</b> </td><td>'+jsonData.surveySchedulePoints[i].sonumber+' </td><td><a style="text-decoration: underline; cursor: pointer; color:red; font-weight:bold" onclick=javascript:showhide("'+jsonData.surveySchedulePoints[i].sonumber+'")><img src="${pageContext.request.contextPath}/images/cancel.png"/></td></tr><tr><td><b>Name:</b> </td><td>'+jsonData.surveySchedulePoints[i].customerName+'</td></tr><tr><td valign="top"><b>Address:</b> </td><td valign="top" colspan="2">'+jsonData.surveySchedulePoints[i].address+'</td></tr></table>';
	           		 showhide(jsonData.surveySchedulePoints[i].sonumber);
	           		 }
	           		 else
	           		 {
	           		 d.innerHTML+='<table width="198" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"></tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" class="listwhitetext" width="200px" >'+jsonData.surveySchedulePoints[i].survey+' - '+
					''+jsonData.surveySchedulePoints[i].surveyTime1+''+
					'-'+jsonData.surveySchedulePoints[i].surveyTime2+'' 
					+' '+jsonData.surveySchedulePoints[i].activityName+' </td></table>';
	           		 }
	           		 
	           		 }    		
	              						
	                oldEstimator=jsonData.surveySchedulePoints[i].estimator;
	              
	               console.log("address: " + jsonData.surveySchedulePoints[i].address + ", estimator: " +  jsonData.surveySchedulePoints[i].estimator);
	        	   surveyData=jsonData.surveySchedulePoints[i];
	        	   toDisplay[i] = surveyData;	  
	        	   surveyDataMap[jsonData.surveySchedulePoints[i].address] = jsonData.surveySchedulePoints[i];
	        	   letterMap[jsonData.surveySchedulePoints[i].address] = letter;
	        	   barToProcess[jsonData.surveySchedulePoints[i].address] = letter3;
	        	   
			var timeout = i * 225; 
			addrArray[i] = jsonData.surveySchedulePoints[i].address;
			window.setTimeout(function() { geoCodeLookup(); }, timeout);    
      	   
	        	   //alert("Finding Survey Schedule for "+oldEstimator);
	        	   var delme = 0;  
	        	   		if(tempLetter=="Z")
						{
							flag1="1";
							j=0;
							count1++;
							tempLetter="";
						}
	        	   }
	        } 
	        if(jsonData.locCount>0){
	        for(i=0; i < jsonData.surveySchedulePoints.length; i++){
//	        document.forms['surveyScheduleForm'].elements['fromDate'].value=jsonData.surveySchedulePoints[i].startDay;
//	        document.forms['surveyScheduleForm'].elements['toDate'].value = jsonData.surveySchedulePoints[i].add7Day;
	        surveyDataMap[jsonData.surveySchedulePoints[i].consultantHomeAdd] = jsonData.surveySchedulePoints[i];
	        console.log("address: " + jsonData.surveySchedulePoints[i].consultantHomeAdd);
	        geocoder.getLocations(jsonData.surveySchedulePoints[i].consultantHomeAdd, addHomeAddToMap);
	        }
	        }       	  
	        	}       	       
	   });
	
	}
	
	var addrCtr = 0;
	function 	geoCodeLookup() {
		var addr = addrArray[addrCtr];
		addrCtr++;
		geocoder = new GClientGeocoder();
		geocoder.getLocations(addr, addZipCoordsToMap);
	}
	
	function addHomeAddToMap(response){
		if(!response.Placemark) return;
	      place = response.Placemark[0];
          point = new GLatLng(place.Point.coordinates[1],
          place.Point.coordinates[0]);
	      var fn

			var baseIcon = new GIcon(G_DEFAULT_ICON);
			    baseIcon.shadow = "http://www.google.com/mapfiles/shadow50.png";
				baseIcon.image = "${pageContext.request.contextPath}/images/home-map.png";
				baseIcon.iconSize = new GSize(28, 28);
				baseIcon.shadowSize = new GSize(24, 25);
				baseIcon.iconAnchor = new GPoint(9, 34);
				baseIcon.infoWindowAnchor = new GPoint(9, 2);
			
			  //var letter = "";
			  //alert("letter1--- "+ letter1);
			  //if( letter1 == 'CF'){
			  //letter=String.fromCharCode("A".charCodeAt(0) + j);
			  //j++;
			  //}
			  //var letteredIcon = new GIcon(baseIcon);
			  //letteredIcon.iconSize = new GSize(30, 30);
			  //letteredIcon.image = "http://localhost:8080/images/home-map.png";
			  // Set up our GMarkerOptions object
			  
			  
			  
				markerOptions = { icon:baseIcon };
				marker = new GMarker(point, markerOptions);
				map.addOverlay(marker);
				
				map.setUIToDefault();
				map.disableScrollWheelZoom();
				fn = markerClickFnAddLoc(point,surveyDataMap[response.name]);
				GEvent.addListener(marker, "click", fn);
				//showOrHide(0);
	  } 
	function markerClickFnAddLoc(point, surveyResults) {
	   return function() {
		   if (!surveyResults) return;
		   var consultantHomeAdd = surveyResults.consultantHomeAdd;
	       var estimator = surveyResults.estimator;
	       var a1 = consultantHomeAdd.substring(0, 15)
	       var a2 = consultantHomeAdd.substring(15, consultantHomeAdd.length)
	       a2 = a2.replace(',','<br>');
	       var a3 = a1+''+a2;
	       //alert(consultantHomeAdd+' -- '+a3);
	       var infoHtml = '<div style="width:100%;"><h5><B>Consultant: </B>'+estimator+'<BR><B>Address: </B>'+a3+'</h5>'+
	       '<td><a style="text-decoration: underline; cursor: pointer;" </td>';
	            
	       infoHtml += '</div></div>';
	       map.openInfoWindowHtml(point, infoHtml);

	   }
   }  
	  var coordCount = 0;
	function addZipCoordsToMap(response){
		if(!response.Placemark) return;
	      place = response.Placemark[0];
          point = new GLatLng(place.Point.coordinates[1],
          place.Point.coordinates[0]);
          
         
	      var fn;

			var baseIcon = new GIcon(G_DEFAULT_ICON);
				baseIcon.shadow = "http://www.google.com/mapfiles/shadow50.png";
				baseIcon.iconSize = new GSize(20, 34);
				baseIcon.shadowSize = new GSize(37, 34);
				baseIcon.iconAnchor = new GPoint(9, 34);
				baseIcon.infoWindowAnchor = new GPoint(9, 2);
			
			  //var letter = "";
			  //alert("letter1--- "+ letter1);
			  //if( letter1 == 'CF'){
			  //letter=String.fromCharCode("A".charCodeAt(0) + j);
			  //j++;
			  //}
			  var letteredIcon = new GIcon(baseIcon);
			  letteredIcon.image = "http://chart.apis.google.com/chart?chst=d_map_spin&chld=4.5|0|FF0000|80|b|"+letterMap[response.name];
                                    
			  // Set up our GMarkerOptions object
			  
			  
			  
				markerOptions = { icon:letteredIcon };
				marker = new GMarker(point, markerOptions);
				map.addOverlay(marker);
				
				map.setUIToDefault();
				map.disableScrollWheelZoom();
				fn = markerClickFnPorts(point,surveyDataMap[response.name]);
				GEvent.addListener(marker, "click", fn);
				//showOrHide(0);
	  }  
	
	function markerClickFnPorts(point, surveyResults) {
	   return function() {
		   if (!surveyResults) return;
	       var customerName = surveyResults.customerName;
	       var originCity = surveyResults.originCity;
	       var originState = surveyResults.originState;
	       var survey = surveyResults.survey;
	       var surveyTime1 = surveyResults.surveyTime1;
	       var surveyTime2 = surveyResults.surveyTime2;
	       var estimator = surveyResults.estimator;
	       var survey1 = surveyResults.survey1;
	       var sonumber = surveyResults.sonumber;
	       var billtocode="";
	       try{
	       billtocode = surveyResults.billtocode;
	       }catch(e){billtocode="";}
	       
	       var infoHtml = '<div style="width:100%;"><h5><B>C/F #: </B>'+sonumber+' ('+billtocode+')<BR><B>Customer: </B>'+customerName+'<Br><B>Survey: </B>'+survey+' - '+surveyTime1+' - '+surveyTime2+'<Br><B>City: </B>'+originCity+', '+originState+'<Br><B>Consultant: </B>'+estimator+'</h5>'+
	       '<td><a style="text-decoration: underline; cursor: pointer;" onclick=putSelectedEstimator("'+estimator+'","'+encodeURI(survey1)+'")>Book Appt</a></td>';
	            
	       infoHtml += '</div></div>';
	       map.openInfoWindowHtml(point, infoHtml);

	   }
   }  

   
   
// for populating estimator

function putSelectedEstimator(estimator,survey)   
   {
        document.forms['surveyScheduleForm'].elements['consultantDate'].value=survey;
   		document.forms['surveyScheduleForm'].elements['estimatorList'].value=estimator;
   		document.forms['surveyScheduleForm'].elements['consultantDate'].focus();
   
   }
// for target
function addToMapOrigin(response)
   {
      place = response.Placemark[0];
      point = new GLatLng(place.Point.coordinates[1],
      place.Point.coordinates[0]);
      //map.setCenter(point, 10);
	  var yellowIcon = new GIcon(G_DEFAULT_ICON);
	  yellowIcon.image = "${pageContext.request.contextPath}/images/target.gif";
	  markerOptions = { icon:yellowIcon };
	  marker = new GMarker(point, markerOptions); 
	  map.addOverlay(marker, markerOptions);
	  map.setCenter(point, 10);
	  map.setUIToDefault();
	  map.disableScrollWheelZoom();
	  getSurveySchedulePoints();
  }
</script>

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('address', 'fade=1,persist=0,hide=0')
animatedcollapse.addDiv('info', 'fade=1,persist=0,hide=1')
animatedcollapse.addDiv('pricing', 'fade=1,persist=0,hide=1')
animatedcollapse.init()
</script>
<script language="JavaScript" type="text/JavaScript">
var namesVec = new Array("130.png", "129.png");
var root='images/';
function swapImg(ima){
// divides the path
nr = ima.getAttribute('src').split('/');
// gets the last part of path, ie name
nr = nr[nr.length-1]
// former was .split('.')[0];
 
if(nr==namesVec[0]){ima.setAttribute('src',root+namesVec[1]);}
else{ima.setAttribute('src',root+namesVec[0]);}
 
}

function setServiceOrderFile()
{

		var surveyCustomerDate=document.forms['surveyScheduleForm'].elements['consultantDate'].value;
   		var estimatorCustomer=document.forms['surveyScheduleForm'].elements['estimatorList'].value;
   		var consultantTime=document.forms['surveyScheduleForm'].elements['consultantTime'].value;
   		var consultantTime2=document.forms['surveyScheduleForm'].elements['consultantTime2'].value;
   		
        if(surveyCustomerDate !='' && estimatorCustomer !='' & consultantTime !='' & consultantTime !='')
        {   		
   		var url="serviceSurveyShedule.html?ajax=1&decorator=simple&popup=true&surveyCustomerDate="+ encodeURI(surveyCustomerDate)+"&estimatorCustomer="+estimatorCustomer+"&consultantTime="+consultantTime+"&consultantTime2="+consultantTime2;
   		http2.open("GET", url, true);
        http2.onreadystatechange = handleHttpResponse1;
        http2.send(null);
        }
        else{
        alert("Please select all the fields to Confirm Appt.");
        }
}
function handleHttpResponse1()
     {
     	var surveyCustomerDate=document.forms['surveyScheduleForm'].elements['consultantDate'].value;
   		var estimatorCustomer=document.forms['surveyScheduleForm'].elements['estimatorList'].value;
   		var consultantTime=document.forms['surveyScheduleForm'].elements['consultantTime'].value;
   		var consultantTime2=document.forms['surveyScheduleForm'].elements['consultantTime2'].value;
          if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>2)
                {
                	var confirm1 = confirm(estimatorCustomer+" is already scheduled for the same date and time.\nDo you want to confirm this appointment?");
                	if(confirm1)
                	{
	                	window.opener.document.forms['serviceOrderForm'].elements['serviceOrder.survey'].value = surveyCustomerDate;
	                	window.opener.document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value = consultantTime;
	                	window.opener.document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value = consultantTime2;
	                	window.opener.document.forms['serviceOrderForm'].elements['serviceOrder.estimator'].value = estimatorCustomer;
	                	
	                	window.close();
	                	
                	}
                else{
                document.forms['surveyScheduleForm'].elements['consultantTime'].value='00:00';
   				document.forms['surveyScheduleForm'].elements['consultantTime2'].value='00:00';
        			}
                } 
                else
                {       window.opener.document.forms['serviceOrderForm'].elements['serviceOrder.survey'].value = surveyCustomerDate;
	                	window.opener.document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value = consultantTime;
	                	window.opener.document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value = consultantTime2;
	                	window.opener.document.forms['serviceOrderForm'].elements['serviceOrder.estimator'].value = estimatorCustomer;
	                	
	                	window.close();
	                	
                }
    }
    
    }
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["overlay11"].visibility='hide';
        else
           document.getElementById("overlay11").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["overlay11"].visibility='show';
       else
          document.getElementById("overlay11").style.visibility='visible';
   }
} 

function addDate(dateObject, numDays) {

dateObject.setDate(dateObject.getDate() + numDays);

return dateObject;

} 
function subtractDate(dateObject, numDays) {

dateObject.setDate(dateObject.getDate() - numDays);

return dateObject;

}   

function dateChange(myDateValue1,myDateValue2,typeChange)  
{
 var mdate111= new Date();
         var mdate11 = myDateValue1.split("-");         
         var day = mdate11[0];
	     var month = mdate11[1];
		 var year = mdate11[2];			
        if(month == 'Jan'){
		       month = "01";
		   	}
		if(month == 'Feb'){
		       month = "02";
		   	}
		if(month == 'Mar'){
		       month = "03";
		   	}
		if(month == 'Apr'){
		       month = "04";
		   	}
		if(month == 'May'){
		       month = "05";
		   	}
		if(month == 'Jun'){
		       month = "06";
		   	}
		if(month == 'Jul'){
		       month = "07";
		   	}
		if(month == 'Aug'){
		       month = "08";
		   	}
		if(month == 'Sep'){
		       month = "09";
		   	}
		if(month == 'Oct'){
		       month = "10";
		  	}
		if(month == 'Nov'){
		       month = "11";
		   	}
		if(month == 'Dec'){
		       month = "12";
		   	}
		
	     if(parseInt(year)>=00 && parseInt(year)<50)
	     {
	         year=2000+parseInt(year)+"";
	     }
	     
	     if(parseInt(year)>=50)
	     {
	         year=1900+parseInt(year)+"";
	     }

	   		
	    mdate11=year+","+month+","+day;
		mdate111= new Date(mdate11);
		if(typeChange=='lessThanOneDay')
		{ var mydate = subtractDate(mdate111, 1);		   	 	}
		if(typeChange=='greaterThanOneDay')			
	 	{ var mydate = addDate(mdate111, 1);			   	 	}
		var year1=mydate.getFullYear();
		var y=""+year1;
		if (year1 < 1000)
		year1+=1900;
		var month1=mydate.getMonth()+1;
		if(month1 == 1)month1="Jan";
		if(month1 == 2)month1="Feb";
		if(month1 == 3)month1="Mar";
		if(month1 == 4)month1="Apr";
		if(month1 == 5)month1="May";
		if(month1 == 6)month1="Jun";
		if(month1 == 7)month1="Jul";
		if(month1 == 8)month1="Aug";
		if(month1 == 9)month1="Sep";
		if(month1 == 10)month1="Oct";
		if(month1 == 11)month1="Nov";
		if(month1 == 12)month1="Dec";
	    var daym=mydate.getDate();
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month1+"-"+y.substring(2,4);
		document.forms['surveyScheduleForm'].elements['fromDate'].value=datam;
		
		
		
		

         mdate11 = myDateValue2.split("-");         
         day = mdate11[0];
	     month = mdate11[1];
		 year = mdate11[2];			
        if(month == 'Jan'){
		       month = "01";
		   	}
		if(month == 'Feb'){
		       month = "02";
		   	}
		if(month == 'Mar'){
		       month = "03";
		   	}
		if(month == 'Apr'){
		       month = "04";
		   	}
		if(month == 'May'){
		       month = "05";
		   	}
		if(month == 'Jun'){
		       month = "06";
		   	}
		if(month == 'Jul'){
		       month = "07";
		   	}
		if(month == 'Aug'){
		       month = "08";
		   	}
		if(month == 'Sep'){
		       month = "09";
		   	}
		if(month == 'Oct'){
		       month = "10";
		  	}
		if(month == 'Nov'){
		       month = "11";
		   	}
		if(month == 'Dec'){
		       month = "12";
		   	}
		   	
	
	     if(parseInt(year)>=00 && parseInt(year)<50)
	     {
   	         year=2000+parseInt(year)+"";
	     }
	     if(parseInt(year)>=50)
	     {
	         year=1900+parseInt(year)+"";
	     }
	     

	   		
	    mdate11=year+","+month+","+day;
		mdate111= new Date(mdate11);
		if(typeChange=='lessThanOneDay')
		{	mydate = subtractDate(mdate111, 1);				   	 	}
		if(typeChange=='greaterThanOneDay')			
	 	{	mydate = addDate(mdate111, 1);			   	 	}
		year1=mydate.getFullYear();
		 y=""+year1;
		if (year1 < 1000)
		year1+=1900;
		month1=mydate.getMonth()+1;
		if(month1 == 1)month1="Jan";
		if(month1 == 2)month1="Feb";
		if(month1 == 3)month1="Mar";
		if(month1 == 4)month1="Apr";
		if(month1 == 5)month1="May";
		if(month1 == 6)month1="Jun";
		if(month1 == 7)month1="Jul";
		if(month1 == 8)month1="Aug";
		if(month1 == 9)month1="Sep";
		if(month1 == 10)month1="Oct";
		if(month1 == 11)month1="Nov";
		if(month1 == 12)month1="Dec";
	    daym=mydate.getDate();
		if (daym<10)
		daym="0"+daym;
		datam = daym+"-"+month1+"-"+y.substring(2,4);		
		
		document.forms['surveyScheduleForm'].elements['toDate'].value=datam;  
			
}

function moveDateBackByOne(target,consultant){ 
	 var fromDate = document.forms['surveyScheduleForm'].elements['fromDate'].value; 
	 var toDate = document.forms['surveyScheduleForm'].elements['toDate'].value;	
	 var jobTypeSchedule=document.forms['surveyScheduleForm'].elements['jobTypeSchedule'].value;
	 var lessThanOneDay=document.forms['surveyScheduleForm'].elements['lessThanOneDay'].value;
	 var targetAddress=document.forms['surveyScheduleForm'].elements['targetAddress'].value;
	 var surveyTime=document.forms['surveyScheduleForm'].elements['surveyTime'].value;
	 var surveyTime2=document.forms['surveyScheduleForm'].elements['surveyTime2'].value;
	 var lessThanOneDay;
	 
	if(target=='lessThanOneDay')
	{
    	lessThanOneDay=target;	
		if(fromDate!='' && fromDate!='')
		{
        dateChange(fromDate,toDate,'lessThanOneDay');
        fromDate=document.forms['surveyScheduleForm'].elements['fromDate'].value;
        if(document.forms['surveyScheduleForm'].elements['toDate'].value=='NaN-NaN-N')
        	{
        	toDate=document.forms['surveyScheduleForm'].elements['toDate'].value='';
        	}
        else
        	{
        toDate=document.forms['surveyScheduleForm'].elements['toDate'].value;
        
        	}
        }
	}
	else if(target=='greaterThanOneDay'){
		lessThanOneDay=target;
		if(fromDate!='' && fromDate!='')
		{
        dateChange(fromDate,toDate,'greaterThanOneDay');
        fromDate=document.forms['surveyScheduleForm'].elements['fromDate'].value;
        if(document.forms['surveyScheduleForm'].elements['toDate'].value=='NaN-NaN-N')
    	{
    	toDate=document.forms['surveyScheduleForm'].elements['toDate'].value='';
    	}
    else
    	{
    toDate=document.forms['surveyScheduleForm'].elements['toDate'].value;
    
    	}
     
        }

	}
	else if(target=='refreshAppointments'){
	lessThanOneDay=target;
	}
	else if(target=='homeDetails'){
	lessThanOneDay=target;
	}
	if(consultant == 'nan')
	{
		consultant = "";
	}
	location.href = "sendSurveySOSchedule.html?jobTypeSchedule="+jobTypeSchedule+"&lessThanOneDay="+lessThanOneDay+"&fromDate="+fromDate+"&toDate="+toDate+"&targetAddress="+targetAddress+"&surveyTime="+surveyTime+"&surveyTime2="+surveyTime2+"&estimated="+consultant+"&decorator=popup&popup=true","forms","height=600,width=950,top=1, left=200, scrollbars=yes,resizable=yes";
} 

	
function copyFromDate(){
 var date1 = document.forms['surveyScheduleForm'].elements['fromDate'].value; 
 var date2 = document.forms['surveyScheduleForm'].elements['toDate'].value;
 if(date1 !='')
 {
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   
   var finalDate = year+"-"+month+"-"+day;
  document.forms['surveyScheduleForm'].elements['fromDate'].value=finalDate;
  }
  if(date2 !='')
 {
   var mySplitResult = date2.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   
   var finalDate = year+"-"+month+"-"+day;
   document.forms['surveyScheduleForm'].elements['toDate'].value=finalDate;
  }
 }

function changeStatus(){
	document.forms['surveyScheduleForm'].elements['formStatus'].value = '1';
}
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns()
		showOrHide(1);
	</script>
	
<s:form id="surveyScheduleForm" name="surveyScheduleForm" action='surveySOSchedule.html' method="post" validate="true">
<s:hidden key="jobTypeSchedule"  value="<%=request.getParameter("jobTypeSchedule") %>"/>



<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="formStatus" value=""/>
<s:hidden name="lessThanOneDay"/><s:hidden name="estimated"/>
<s:hidden name="targetAddress" value="<%=request.getParameter("targetAddress") %>"/>
		<div id="priceinfotab_bg" style="width: 99.8%"><div class="price_tleft">Survey Schedule</div></div>
<table class="mainDetailTable" cellspacing="0" cellpadding="0" border="0"  style="width: 100%;" >
  <tbody>
  <tr>
  <td colspan="2" align="left" valign="middle" height="82" class="price-bg">

  <table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
  <tr>
  <td width="3">&nbsp;</td> 
  <td valign="top">
  <div class="curved-left">
  <table cellpadding="1" cellspacing="2" border="0" style="margin:0px;padding:0px;">
  <tr>  
   		
		
		<td colspan="9">
		<table cellpadding="2" cellspacing="1" border="0" style="margin:0px;padding:0px;">
  		<tr> 
  		<td width="1">&nbsp;
  		</td>
		<!--<td align="right" class="listwhitetext">
		<a><img align="middle" onclick="javascript:moveDateBackByOne('lessThanOneDay','nan');" HEIGHT="25" WIDTH="112" BORDER="0"  src="images/s-backward.png"/></a>
		</td>
		--><td align="right" class="listwhitetext">
		<a><div class="survey-back" onclick="javascript:moveDateBackByOne('lessThanOneDay','nan');" /></div></a>
		</td>
		<td align="right" class="listwhitetext">From:</td>					
		<td><s:textfield id="fromDates" cssClass="input-text" name="fromDate"  size="7" maxlength="11"  readonly="true"  onkeydown="return onlyDel(event,this)"/></td>
		<td><img id="fromDates_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="changeStatus()"/></td>
	   <td align="right" class="listwhitetext">To:</td>	
	   <td><s:textfield id="toDates" cssClass="input-text" name="toDate" size="7" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this)"/></td>
	   <td><img id="toDates_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="changeStatus()"/></td>
	 	<!--<td align="right" class="listwhitetext">
	 	<a><img align="middle" onclick="javascript:moveDateBackByOne('greaterThanOneDay','nan');" HEIGHT="25" WIDTH="127" BORDER="0"  src="images/s-forward.png"/></a>
	 	</td>
	 	-->
	 	<td align="right" class="listwhitetext">
	 	<a><div class="survey-forw" onclick="javascript:moveDateBackByOne('greaterThanOneDay','nan');" /></div></a>
	 	</td>	 	
	 	</tr>
	 	</table>
	 	
		</td>
	
	</tr>
	
	<tr>
	
	<td colspan="9">
		<table cellpadding="2" cellspacing="1" border="0" style="margin:0px;padding:0px;">
  	<tr> 
  	<td width="1">&nbsp;</td>
  	<td align="right" width="115" class="listwhitetext">
	 <input type="button" class="movebutton" value="Consultant&nbsp;Home" style="width:112px;padding:2px 5px;" onclick="moveDateBackByOne('homeDetails','nan');"/>
	</td>
  	  	
	<td align="right" class="listwhitetext">Time: </td>
  	<td class="listwhitetext"><s:textfield key="surveyTime" cssClass="input-text" size="2" maxlength="5" onchange="completeTimeString();"/>hrs</td>
	<td width="20">&nbsp;</td>
	<td align="right" class="listwhitetext">To: </td>
  	<td class="listwhitetext"><s:textfield key="surveyTime2" cssClass="input-text" size="2" maxlength="5" onchange="completeTimeString();"/>hrs</td>
  	<td width="3">&nbsp;</td>
  	<td align="right"  class="listwhitetext">
	 <input type="button" class="movebutton" value="Refresh Appointments" style="width:153px;" onclick="moveDateBackByOne('refreshAppointments','nan');"/>
	</td>
	</tr>
	</table>
	</div>
	</td>
	</tr>
	</table>
  </td>
  <td width="1"></td>
  <td valign="top">
  <div class="curved-right">
  <table cellpadding="2" cellspacing="2" border="0" style="margin:0px;padding:0px;">
  <tr>
  
  <td colspan="5">
		<table cellpadding="2" cellspacing="1" border="0" style="margin:0px;padding:0px;">
  	<tr>
  <td align="right" class="listwhitetext">Consultant</td>
  <td><s:select cssClass="list-menu" name="estimator" id="estimatorList" list="%{estimatorList}"   cssStyle="width:130px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
	<td align="right" class="listwhitetext">Date: </td>	
	<td><s:textfield id="consultantDates" cssClass="input-text" name="consultantDate"  size="7" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this)"/></td>
	<td><img id="consultantDates_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="changeStatus()"/></td>
 </tr>
 </table>
 </td>
 
  </tr>
  <tr>
  <td colspan="5">
		<table cellpadding="2" cellspacing="1" border="0" style="margin:0px;padding:0px;">
  	<tr>
  <td align="right" class="listwhitetext" width="50">Time: </td>
  	<td class="listwhitetext" width="70"><s:textfield key="consultantTime" cssClass="input-text" size="2" maxlength="5" onchange="consultantTimeString();"/>hrs</td>
    <td align="right" class="listwhitetext">To: </td>
  	<td class="listwhitetext"><s:textfield key="consultantTime2" cssClass="input-text" size="2" maxlength="5" onchange="consultantTimeString();"/>hrs</td>
  	<td><input type="button" class="movebutton" id="btnConvert" value="Confirm Appt" style="width:105px;" />
  	</td>
  	</tr>
  	</table>
  	</td>
  </tr>
  </table>
  </div>
  </td>
  </tr>
  </table>
  
  
  
  
  
  
	
  </td>
  </tr>
  
  
  <tr>
  <td valign="top" align="center" style="margin:0px;padding: 0px; background-color:#F6F9FA;">
  <table width="100%" style="margin:0px;padding:0px;" border="0" cellpadding="0" cellspacing="0" >    
    <tr>
    <td style="padding-left:1px;">
      
      </td>
	</tr>
	  <tr>
      <td align="left" colspan="2">
      <div id="priceinfotab_bg"><div class="price_tleft">Consultants Availability:</div></div>
          
       <div id="content-containertable" class="override">
			 <div id="surveyScheduleSOList"></div>
			 </div>
      </td>
    </tr>
  </table></td>
     <td width="100%" valign="top" style="margin:0px;padding: 0px;">
     
     	<table border="0" cellpadding="2" cellspacing="1" width="100%" style="margin:0px;padding: 0px;">
		  <tbody>  	
		 <tr>      
        	<td valign="top" style="margin:0px;padding: 0px;width:74%;border:1px solid #969694;">
        	
				<div id="map" style="width: 100%; height: 500px;margin:0px;padding: 0px; z-index:1">
            </td>
            <B>Note</B>:-If you are unable to view the map, this may be due to Firefox security. Please
click on the grey shield icon on the left hand corner of the browsers address
bar and accept to view "Mixed Mode Content" for this page.
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        </tbody>
        </table><DIV ID="overlay11">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="250px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait, Map is loading.......</font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
			</div>
        <div id="mydiv" style="position:absolute; z-index:905;"></div>
    
</s:form>
<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
</script>
<script type="text/javascript">
setTimeout("showOrHide(0)",6000);
//copyFromDate();
//openTargetOriginAddressLocation();
$(function() {
$('#btnConvert').click(function() {
var param1 = new Date();
var ddate = $('#fromDates').val();
var todate = $('#toDates').val();
var consultantDate=$('#consultantDates').val();

var date2 = new Date();
var daym;
var year=date2.getFullYear()
var y=""+year;
if (year < 1000)
year+=1900
var day=date2.getDay()
var month=date2.getMonth()+1
if(month == 1)month="Jan";
if(month == 2)month="Feb";
if(month == 3)month="Mar";
	  if(month == 4)month="Apr";
	  if(month == 5)month="May";
	  if(month == 6)month="Jun";
	  if(month == 7)month="Jul";
	  if(month == 8)month="Aug";
	  if(month == 9)month="Sep";
	  if(month == 10)month="Oct";
	  if(month == 11)month="Nov";
	  if(month == 12)month="Dec";
	  var daym=date2.getDate()
	  if (daym<10)
	  daym="0"+daym
	  var datam = daym+"-"+month+"-"+y.substring(2,4); 
	
	  var current=new Date(datam);
	  var from=new Date(ddate);
	  var toDate=new Date(todate);
	  var consultant=new Date(consultantDate);


if (consultantDate!='' && (consultantDate < datam)) {
	alert('Please Enter Consulatnt Date  Greater than Current Date ');
	$('#consultantDates').val("");
	return false;
	}

else
{
setServiceOrderFile();
}
})
})
</script>