package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DiscountDao;
import com.trilasoft.app.model.Discount;
import com.trilasoft.app.service.DiscountManager;

public class DiscountManagerImpl extends GenericManagerImpl<Discount, Long> implements DiscountManager {
	DiscountDao discountDao;
	public DiscountManagerImpl(DiscountDao discountDao) {
		super(discountDao);
		this.discountDao =discountDao;
	}
	public List discountList(String contract, String chargeCode,String corpId) {
		return discountDao.discountList(contract, chargeCode, corpId);
	}
	
	public List discountMapList(String corpId){
		return discountDao.discountMapList(corpId);
	}
}
