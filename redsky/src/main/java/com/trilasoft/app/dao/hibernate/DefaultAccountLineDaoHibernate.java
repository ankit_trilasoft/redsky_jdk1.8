package com.trilasoft.app.dao.hibernate;

import java.sql.CallableStatement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;

import com.trilasoft.app.dao.DefaultAccountLineDao;
import com.trilasoft.app.dao.hibernate.ChargesDaoHibernate.ChargesDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.Claim;
import com.trilasoft.app.model.DefaultAccountLine;
import com.trilasoft.app.model.VanLineGLType;

public class DefaultAccountLineDaoHibernate extends GenericDaoHibernate<DefaultAccountLine, Long> implements DefaultAccountLineDao { 
	//private List defaultAccountLines;
	//private List defaultAccounts;
	private HibernateUtil hibernateUtil;
	private String sessionCorpID;
	
	public DefaultAccountLineDaoHibernate() {   
	        super(DefaultAccountLine.class);   
	    }
	 

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	public List<DefaultAccountLine> findDefaultAccountLineList(){
		getHibernateTemplate().setMaxResults(100000);
		List defaultAccountLines = new ArrayList();
    	String columns = "id,route,jobType,mode,serviceType,categories,basis,quantity,rate,amount";
        defaultAccountLines = getHibernateTemplate().find("select " + columns + " from DefaultAccountLine");
        try {
        	defaultAccountLines= hibernateUtil.convertScalarToComponent(columns, defaultAccountLines, DefaultAccountLine.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
		return defaultAccountLines;
    	}
	/*public List<DefaultAccountLine> findDefaultAccountLineList(){
			List list= new ArrayList();
			String columns = "id,corpID,route,jobType,mode,serviceType,categories,basis,quantity,rate,amount";
			//String columns = "id,corpID,route,jobType,mode,categories,basis,quantity,rate,amount,serviceType";
			list= getHibernateTemplate().find("select " + columns +  " from DefaultAccountLine");
			System.out.println("\n\n\n in dao materialsList it..."+ list);
	        try {
	        	list= hibernateUtil.convertScalarToComponent(columns, list, DefaultAccountLine.class);
	        	System.out.println("\n\n\n in dao materialsList it..."+ list);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return list;
		}*/

	public List getDefaultList(String jobType, String routing, String mode) {
		getHibernateTemplate().setMaxResults(100000);
		return getHibernateTemplate().find("from DefaultAccountLine where jobType='"+jobType+"' AND route='"+routing+"' AND mode='"+mode+"'");
	}


	public List<DefaultAccountLine> findDefaultAccountLines(String jobType, String route, String mode, String serviceType, String categories,String billToCode,String contract,String packMode,String commodity, String compDiv, String oCountry, String dCountry,String equipment){
		getHibernateTemplate().setMaxResults(100000); 
		List defaultAccounts = new ArrayList();
		 //String query="from DefaultAccountLine where (route like '" + route.replaceAll("'", "''").replaceAll(":", "''") + "%' or route is null or route ='')  AND jobType like '" + jobType.replaceAll("'", "''").replaceAll(":", "''") + "%' AND ( mode like '" + mode.replaceAll("'", "''").replaceAll(":", "''") + "%' or mode is null or mode ='' )AND ( commodity like '" + commodity.replaceAll("'", "''").replaceAll(":", "''") + "%' or commodity is null or commodity ='' )  AND ( serviceType like '" + serviceType.replaceAll("'", "''").replaceAll(":", "''") + "%' or serviceType is null or serviceType = ''  ) AND categories like '" + categories.replaceAll("'", "''").replaceAll(":", "''") + "%' and ( billToCode like '"+billToCode+"%' or billToCode = '' or billToCode is null ) and contract like '"+contract+"' and ( packMode like '" + packMode.replaceAll("'", "''").replaceAll(":", "''") + "%' or packMode is null or packMode = '' ) AND ( companyDivision like '" + compDiv.replaceAll("'", "''").replaceAll(":", "''") + "%' or companyDivision is null or companyDivision ='' ) order By orderNumber,id";
		 if(compDiv==null){
			 compDiv="";
		 }
		 String query="from DefaultAccountLine where ";
		 if(jobType.equalsIgnoreCase("")){
		 query += "(jobType like '%' or jobType is null or jobType ='')  ";
		 }else{
		 	query += "(jobType='"+jobType+"')  ";
		 }
		 if(route.equalsIgnoreCase("")){
		 query += " and (route like '%' or route is null or route ='')  ";
		 }else{
		 	query += " and (route='"+route+"'  or route='' or route is null)  ";
		 }
		 if(mode.equalsIgnoreCase("")){
		 query += " and (mode like '%' or mode is null or mode ='')  ";
		 }else{
		 	query += " and (mode='"+mode+"' or mode is null or mode ='' )  ";
		 }
		 if(commodity.equalsIgnoreCase("")){
		 query += " and (commodity like '%' or commodity is null or commodity ='')  ";
		 }else{
		 	query += " and (commodity='"+commodity+"' or commodity is null or commodity ='' )  ";
		 }
		 if(serviceType.equalsIgnoreCase("")){
		 query += " and (serviceType like '%' or serviceType is null or serviceType ='')  ";
		 }else{
		 	query += " and (serviceType='"+serviceType+"' or serviceType is null or serviceType ='' )  ";
		 }
		 if(categories.equalsIgnoreCase("")){
		 query += " and (categories like '%' or categories is null or categories ='')  ";
		 }else{
		 	query += " and (categories='"+categories+"')  ";
		 }
		 if(billToCode.equalsIgnoreCase("")){
		 query += " and (billToCode like '%' or billToCode is null or billToCode ='')  ";
		 }else{
		 	query += " and (billToCode='"+billToCode+"' or billToCode='' or billToCode is null)  ";
		 }
		 if(contract.equalsIgnoreCase("")){
		 query += " and (contract like '%' or contract is null or contract ='')  ";
		 }else{
		 	query += " and (contract='"+contract+"')  ";
		 }
		 if(packMode.equalsIgnoreCase("")){
		 query += " and (packMode like '%' or packMode is null or packMode ='')  ";
		 }else{
		 	query += " and (packMode='"+packMode+"' or packMode is null or packMode ='' )  ";
		 }
		 if(compDiv.equalsIgnoreCase("")){
		 query += " and (companyDivision like '%' or companyDivision is null or companyDivision ='')  ";
		 }else{
		 	query += " and (companyDivision='"+compDiv+"' or companyDivision='' or companyDivision is null)  ";
		 } 
		 if(oCountry.equalsIgnoreCase("")){
			 query += " and (originCountry like '%' or originCountry is null or originCountry ='')  ";
			 }else{
			 	query += " and (originCountry='"+oCountry+"')  ";
			 } 
		 if(dCountry.equalsIgnoreCase("")){
			 query += " and (destinationCountry like '%' or destinationCountry is null or destinationCountry ='')  ";
			 }else{
			 	query += " and (destinationCountry='"+dCountry+"')  ";
			 }
		 if(equipment.equalsIgnoreCase("")){
			 query += " and (equipment like '%' or equipment is null or equipment ='')  ";
			 }else{
			 	query += " and (equipment='"+equipment+"')  ";
			 }
		 query +=" order By orderNumber,id";
		 defaultAccounts = getHibernateTemplate().find(query);
		 return defaultAccounts;		
	}
	public List defaultALUpdate(Long id){
		return getHibernateTemplate().find("delete FROM defaultaccountline where id=?", id);
	}


	public List findAccountInterface(String corpID) {
		return getHibernateTemplate().find("select accountingInterface from SystemDefault where  accountingInterface is not null and corpID=?",corpID);
	}


	public List getDefaultAccountList(String jobType, String routing, String mode, String contract, String billToCode, String packingMode, String commodity, String serviceType, String companyDivision,String originCountry,String destinationCountry,String equipment,String originCity,String destinationCity) {
		getHibernateTemplate().setMaxResults(100000);
		String ServiceValue="";
		String query="";
		
		getSessionCorpID();
		StringBuffer getChargeCode=new StringBuffer() ;
		List empty=new ArrayList();
		
		try{
		 String []res2=serviceType.split(",");
		 for(int l=0;l<res2.length;l++){
			 if(ServiceValue.trim().equals("")){
			 ServiceValue=res2[l]+"','";
			 }else{
				 ServiceValue =ServiceValue+res2[l]+"','";
			 }
		 }
		 if(ServiceValue.lastIndexOf("'") == ServiceValue.length() - 1) 
		   {
			 ServiceValue = ServiceValue.substring(0, ServiceValue.length() - 1);
		    }
		if(ServiceValue.lastIndexOf(",") == ServiceValue.length() - 1) 
		   {
			ServiceValue = ServiceValue.substring(0, ServiceValue.length() - 1);
		    } 
		if(ServiceValue.lastIndexOf("'") == ServiceValue.length() - 1) 
		   {
			ServiceValue= ServiceValue.substring(0, ServiceValue.length() - 1);
		    }
		}catch(Exception e){
			
		}
		
		query="select charge from charges where contract='"+contract+"' and status is false and corpId='"+sessionCorpID+"' or corpId='' or corpId is null ";
		List list= this.getSession().createSQLQuery(query).list();
		if(!list.isEmpty()){
			Iterator chargeCodeIterator=list.iterator();
	        while(chargeCodeIterator.hasNext())
	        {
	        	getChargeCode=getChargeCode.append("'");
	        	String ChargeCode =(String)chargeCodeIterator.next();  
	        	getChargeCode=getChargeCode.append(ChargeCode);
	        	getChargeCode=getChargeCode.append("',");
	        	
	        } 
	        String activeChargeCode=new String(getChargeCode);
	        if(!activeChargeCode.equals(""))
	        {
	        	activeChargeCode=activeChargeCode.substring(0, activeChargeCode.length()-1);
	        }
	        else if(activeChargeCode.equals(""))
	        {
	        	activeChargeCode =new String("''");  
	        }
		
		
		 query="from DefaultAccountLine where chargeCode not in("+activeChargeCode+") and  (jobType='"+jobType+"' or jobType='' or jobType is null )AND (route='"+routing+"' or route='' or route is null) AND (mode='"+mode+"' or mode='' or mode is null) AND contract='"+contract+"' AND (billToCode='"+billToCode+"' or billToCode='' or billToCode is null) and (packMode='"+packingMode+"' or packMode='' or packMode is null) and (commodity ='"+commodity+"' or commodity ='' or commodity is null) and (serviceType in ('"+ServiceValue+"')  or serviceType ='' or serviceType is null) and (companyDivision='"+companyDivision+"' or companyDivision='' or companyDivision is null) and (originCountry='"+originCountry+"' or originCountry='' or originCountry is null) and (destinationCountry='"+destinationCountry+"' or destinationCountry='' or destinationCountry is null) and (equipment='"+equipment+"' or equipment='' or equipment is null) and (originCity='"+originCity.replaceAll("'", "'\'")+"' or originCity='' or originCity is null) and (destinationCity='"+destinationCity.replaceAll("'", "'\'")+"' or destinationCity='' or destinationCity is null) order By orderNumber,id";
		
		return getHibernateTemplate().find(query);
		}else{
			return empty;
		}
		
	}


	public List findBillToCode(String corpID) {
		getHibernateTemplate().setMaxResults(100000);
		return getHibernateTemplate().find("select distinct billToCode FROM DefaultAccountLine where corpID='"+corpID+"' and billToCode<>''");
	}


	public List findContractList(String corpID) {
		return getHibernateTemplate().find("select distinct contract from Contract where (ending is null or date_format(ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d')) and corpID='"+corpID+"' ORDER BY contract");
		
	}


	public List getAcctTemplateList(String contract, String sessionCorpID) {
		getHibernateTemplate().setMaxResults(100000);
		return getHibernateTemplate().find("from DefaultAccountLine where  contract='"+contract+"'and corpID='"+sessionCorpID+"' ");
	}

	public class TemplateDTO
	{
		
		private Object id;
		private Object contract;
		private Object corpID;
		private Object charge;
		private Object description;
		private Object begin;
		private Object ending;
		private Object count;
		
		public Object getBegin() {
			return begin;
		}
		public void setBegin(Object begin) {
			this.begin = begin;
		}
		public Object getCharge() {
			return charge;
		}
		public void setCharge(Object charge) {
			this.charge = charge;
		}
		public Object getContract() {
			return contract;
		}
		public void setContract(Object contract) {
			this.contract = contract;
		}
		public Object getCorpID() {
			return corpID;
		}
		public void setCorpID(Object corpID) {
			this.corpID = corpID;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getEnding() {
			return ending;
		}
		public void setEnding(Object ending) {
			this.ending = ending;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getCount() {
			return count;
		}
		public void setCount(Object count) {
			this.count = count;
		}
		
	}
	public List findChargesByTemplate(String sessionCorpID) {
		
		String query= "select c.id, x.contract, c.begin, c.ending, count(*) from defaultaccountline x, contract c where c.corpid = '"+sessionCorpID+"' and x.corpid=c.corpid and x.contract = c.contract group by x.contract having count(*) > 0 order by x.contract, c.begin ";
		List list= this.getSession().createSQLQuery(query).list();	
		//System.out.println("\n\nQUERY::"+query);
		List templates= new ArrayList();
		TemplateDTO templateDTO;
		Iterator it=list.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			
			templateDTO=new TemplateDTO();
			templateDTO.setId(obj[0]);
			templateDTO.setContract(obj[1]);
			templateDTO.setBegin(obj[2]);
			templateDTO.setEnding(obj[3]);
			templateDTO.setCount(obj[4]);
			templates.add(templateDTO);
		}
		return templates;
}


	public List getChargeTemplate(String contract, String sessionCorpID) {
		getHibernateTemplate().setMaxResults(100000);
		return getHibernateTemplate().find("from DefaultAccountLine where  contract='"+contract+"' and corpID='"+sessionCorpID+"'");
		
	}

	public String saveBulkUpdates(String id,String changedValue ){
		int i= getHibernateTemplate().bulkUpdate("update DefaultAccountLine set orderNumber='"+changedValue+"'where id='"+id+"'");
		return String.valueOf(i);
	}
	public List getAllValues(String sessionCorpID){
		getHibernateTemplate().setMaxResults(100);
		return getHibernateTemplate().find("from DefaultAccountLine where corpID='"+sessionCorpID+"' order by orderNumber,id");
	}


	public List findContracOnJobBasis(String billToCode, String job, Date createdon, String corpID) {
		String query="";
		if(createdon !=null){
		    Date date1Str = createdon;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
			query="select  distinct c.contract from contract c where (c.jobtype like '%"+job+"%' or c.jobtype='' or c.jobtype is null )and c.corpID = '"+corpID+"' and (c.ending is null or (c.ending >=if('"+formatedDate1+"'='null','2001-01-01','"+formatedDate1+"'))) union select distinct c.contract from contract c, contractaccount ca where ca.accountcode = '"+billToCode+"' and ca.contract = c.contract and ca.corpID = '"+corpID+"' and (c.ending is null or (c.ending >=if('"+formatedDate1+"'='null','2001-01-01','"+formatedDate1+"'))) order by 1";
			return getSession().createSQLQuery(query).list();
			}else{
				query="select  distinct c.contract from contract c where (c.jobtype like '%"+job+"%' or c.jobtype='' or c.jobtype is null) and c.corpID = '"+corpID+"' and (c.ending is null or (c.ending >=if('"+createdon+"'='null','2001-01-01','"+createdon+"'))) union select distinct c.contract from contract c, contractaccount ca where ca.accountcode = '"+billToCode+"' and ca.contract = c.contract and ca.corpID = '"+corpID+"' and (c.ending is null or (c.ending >=if('"+createdon+"'='null','2001-01-01','"+createdon+"'))) order by 1";
			   return getSession().createSQLQuery(query).list();  
		   } 
		
	}
	
	public void saveUpdatesList(String allValue,String corpid,String updateby){
		  String[] str1=allValue.split("@");
		String query="update defaultaccountline set contract='"+str1[1]+"',";
				if(str1[2]!=null){
					query=query+ "jobType='"+str1[2]+"',";
				}if(str1[3]!=null){
					query=query+  "mode='"+str1[3]+"',";
				}if(str1[4]!=null){
					query=query+ "billToCode='"+str1[4]+"',";
				}if(str1[5]!=null){
					query=query+"billToName='"+str1[5]+"',";
				}if(str1[6]!=null){
					query=query+"route='"+str1[6]+"',";
				}if(str1[7]!=null){
					query=query+ "packMode='"+str1[7]+"',";
				}if(str1[8]!=null){
					query=query+ "commodity='"+str1[8]+"',";
				}if(str1[9]!=null){
					query=query+ "serviceType='"+str1[9]+"',";
				}if(str1[10]!=null){
					query=query+ "companyDivision='"+str1[10]+"',";
				}if(str1[11]!=null){
					query=query+"equipment='"+str1[11]+"',";
				}if(str1[12]!=null){
					query=query+"originCountry='"+str1[12]+"',";
				}if(str1[13]!=null){
					query=query+"destinationCountry='"+str1[13]+"',";
				}if(str1[14]!=null){
					query=query+ "orderNumber='"+str1[14]+"',";
				}if(str1[15]!=null){
					query=query+ "categories='"+str1[15]+"',";
				}if(str1[16]!=null){
					query=query+ "basis='"+str1[16]+"',";
				}if(str1[17]!=null){
					query=query+ "vendorCode='"+str1[17]+"',";
				}if(str1[18]!=null){
					query=query+ "vendorName='"+str1[18]+"',";
				}if(str1[19]!=null){
					query=query+ "chargeCode='"+str1[19]+"',";
				}if(str1[20]!=null){
					query=query+ "recGl='"+str1[20]+"',";
				}if(str1[21]!=null){
					query=query+ "payGl='"+str1[21]+"',";
				}if(str1[22]!=null){
					query=query+ "quantity="+str1[22]+",";
				}if(str1[23]!=null){
					query=query+ "estVatDescr='"+str1[23]+"',";
				}if(str1[24]!=null){
					query=query+ "estVatPercent='"+str1[24]+"',";
				}if(str1[25]!=null){
					query=query+ "sellRate="+str1[25]+",";
				}if(str1[26]!=null){
					query=query+ "estimatedRevenue='"+str1[26]+"',";
				}if(str1[27]!=null){
					query=query+ "markUp="+str1[27]+",";
				}if(str1[28]!=null){
					query=query+ "estExpVatDescr='"+str1[28]+"',";
				}if(str1[29]!=null){
					query=query+ "estExpVatPercent='"+str1[29]+"',";
				}if(str1[30]!=null){
					query=query+ "rate='"+str1[30]+"',";
				}if(str1[31]!=null){
					query=query+ "amount='"+str1[31]+"',";
				}if(str1[32]!=null){
					query=query+ "description='"+str1[32]+"',";
				}if(str1[33]!=null){
					query=query+ "estVatAmt='"+str1[33]+"',";  
				}
				query=query+ "updatedBy='"+updateby+"', updatedOn=now()  ";
				query=query+"where id='"+str1[0]+"' and corpID='"+corpid+"'";
				
		this.getSession().createSQLQuery(query).executeUpdate();	
	}
	public List findSearchNameAutocompleteListFromDefaultAccountList(String searchName, String corpId,String populatedatafield){
		try
		{ 
			String query="";
			if(populatedatafield.equalsIgnoreCase("Vendor")){
				query="select distinct(CONCAT(firstName,lastname,'#',partnercode))as name   from partnerpublic where isAgent is true and status='Approved' and (firstName like'"+searchName+"%' or lastname like'"+searchName+"%') and  corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"') order by firstName,lastname ";
			}
			if(populatedatafield.equalsIgnoreCase("BillToName")){
				query="select distinct(CONCAT(firstName,lastname,'#',partnercode))as name   from partnerpublic where isAccount is true and status='Approved' and (firstName like'"+searchName+"%' or lastname like'"+searchName+"%') and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"') order by firstName,lastname ";	
			}
		List temp =  this.getSession().createSQLQuery(query).list();
		return temp;
	} catch (Exception e) {
    	 e.printStackTrace();
	}
	return null;
	}
	public void getSessionCorpID()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();	
	}
	public List findSearchChargeCodeAutocompleteList(String searchName, String corpId,String contract,String formtype,String compDivision,String originCountry,String destinCountry,String mode,String category){
		List chargeDetailList1=new ArrayList();
		String query="";
		String Stringquery="";
		try
		{ 
			if(formtype.equalsIgnoreCase("TemplateNonInternalCategory"))
			{
				query="select charge,description from charges where (charge like'%"+searchName+"%' or description like'%"+searchName+"%')  and  corpID ='"+corpId+"' and  contract in ('"+contract.replaceAll("'", "''")+"') and status is true and  charge not in ('DMMFXFEE','DMMFEE') order by charge ";
			}
			if(formtype.equalsIgnoreCase("TemplateInternalCategory"))
			{
				query="select ch.charge,ch.description from  charges ch, contract ct Where ch.contract=ct.contract and ct.companyDivision='"+compDivision+"' and ct.internalCostContract is true and ch.corpID='"+corpId+"' and (ch.charge like'%"+searchName+"%' or ch.description like'%"+searchName+"%') and  ch.status is true order by ch.charge  ";
			}
			if(formtype.equalsIgnoreCase("AccountInternalCategory"))
			{
				/*query="select ch.charge,ch.description from charges ch, contract ct ";
				query=query+" Where ch.contract=ct.contract and ct.companyDivision='"+compDivision+"' ";
				query=query+ " and ct.internalCostContract is true and ch.corpID='"+corpId+"' ";
				query=query+" and (((originCountry like '"+originCountry+"%' or originCountry is null or originCountry='') ";
				query=query+" or (destinationCountry like '"+destinCountry+"%' or destinationCountry is null or destinationCountry='')) ";
				query=query+" and (mode like '"+mode+"%' or mode is null or mode='')) and ch.status is true and (ch.charge like'%"+searchName+"%' or ch.description like'%"+searchName+"%') and ch.charge not in ('DMMFXFEE','DMMFEE') order by ch.charge ";
				
				*/
				query="select ch.charge,ch.description from charges ch, contract ct Where ch.contract=ct.contract ";
				query=query+" and ct.companyDivision='"+compDivision+"' and ct.internalCostContract is true and ";
				query=query+" (ch.charge like'%"+searchName+"%' or ch.description like'%"+searchName+"%') and ";
				query=query+" ch.corpID='"+corpId+"' and (originCountry like '"+originCountry+"%' or ";
				query=query+" originCountry is null or originCountry='')  ";
				query=query+" and (destinationCountry like '"+destinCountry+"%' or destinationCountry is null or ";
				query=query+" destinationCountry='') and (mode like '"+mode+"%' or mode is null or mode='') ";
			}
			if(formtype.equalsIgnoreCase("AccountNonInternalCategory"))
			{
				query="select charge,description from charges where ";
				query=query+" (charge like'%"+searchName+"%' or description like'%"+searchName+"%') and contract ='"+contract+"' ";
				query=query+" and corpID='"+corpId+"'and ((originCountry like '"+originCountry+"%' or originCountry is null or ";
				query=query+" originCountry='')  OR (destinationCountry like '"+destinCountry+"%' or destinationCountry is null or ";
				query=query+" destinationCountry='') and (mode like '"+mode+"%' or mode is null or mode=''))and status is true ";
				
				/*if(category.equalsIgnoreCase("")){
					Stringquery="select id,if(((originCountry is null or originCountry='') " +
				     " and (destinationCountry is null or destinationCountry=''))," +
				     " 'YES',if((originCountry='"+originCountry+"' or " +
				     " destinationCountry ='"+destinCountry+"'),'YES','NO'))," +
				     " originCountry,destinationCountry,countryFlexibility from " +
				     " charges where contract='"+contract+"' and corpID='"+corpId+"' " +
				     " and (mode like '"+mode+"%' or mode is null or mode='') and status is true";
					
				}else {		
					Stringquery="select ch.id,if(((ch.originCountry is null or ch.originCountry='') " +
						  " and (ch.destinationCountry is null or ch.destinationCountry='')),'YES', "+
						  " if((ch.originCountry='"+originCountry+"' or ch.destinationCountry ='"+destinCountry+"'),'YES','NO')), "+
						  " ch.originCountry,ch.destinationCountry,ch.countryFlexibility "+
						  " from charges ch, contract ct, costelement ce "+
						  " where ch.contract=ct.contract and ch.costElement = ce.costElement "+
						  " and ch.contract='"+contract+"' "+
						  " and ch.corpID='"+corpId+"' "+
						  " and ct.corpID='"+corpId+"' "+
						  " and ce.corpID='"+corpId+"' "+
						  " and ce.reportingAlias ='"+category+"' "+
						  " and (ch.mode like '"+mode+"%' or ch.mode is null or ch.mode='') and ch.status is true";		
					
				}
				List lista= this.getSession().createSQLQuery(Stringquery).list(); 
				Iterator it=lista.iterator();
				String idList="";
					while(it.hasNext()){
							Object []obj=(Object[])it.next();				
							if(idList.equalsIgnoreCase("")){
								if(obj[1].toString().equalsIgnoreCase("YES")){
									if((obj[4]!=null)&&(obj[4].toString().equalsIgnoreCase("true"))){
										if(((obj[2]==null)||(obj[2].toString().trim().equalsIgnoreCase("")))&&((obj[3]==null)||(obj[3].toString().trim().equalsIgnoreCase("")))){
											idList=obj[0]+"".toString().trim();
										}else if((obj[2]!=null)&&(obj[2].toString().trim().equalsIgnoreCase(originCountry))&&(obj[3]!=null)&&(obj[3].toString().trim().equalsIgnoreCase(destinCountry))){
											idList=obj[0]+"".toString().trim();
										}else{
											
										}
									}else{
										idList=obj[0]+"".toString().trim();
									}
								}
							}else{
								if(obj[1].toString().equalsIgnoreCase("YES")){
									if((obj[4]!=null)&&(obj[4].toString().equalsIgnoreCase("true"))){
										if(((obj[2]==null)||(obj[2].toString().trim().equalsIgnoreCase("")))&&((obj[3]==null)||(obj[3].toString().trim().equalsIgnoreCase("")))){
											idList=idList+","+obj[0]+"".toString().trim();
										}else if((obj[2]!=null)&&(obj[2].toString().trim().equalsIgnoreCase(originCountry))&&(obj[3]!=null)&&(obj[3].toString().trim().equalsIgnoreCase(destinCountry))){
											idList=idList+","+obj[0]+"".toString().trim();
										}else{
											
										}
									}else{
										idList=idList+","+obj[0]+"".toString().trim();
									}
								}
							}
					}
					if(idList.equals("")){
						idList="'"+idList+"'";
					}*/
					
					/*query="select charge,description from charges where (charge like'%"+searchName+"%' or description like'%"+searchName+"%')  and contract='"+contract+"' and corpID='"+corpId+"' and id in ("+idList+") and (status is true) and charge not in ('DMMFXFEE','DMMFEE') order by charge ";*/
			}  
			
		List chargeDetails =  this.getSession().createSQLQuery(query).list();
		chargeDetailsList dto = null;
		Iterator it = chargeDetails.iterator();
		while (it.hasNext()){
			Object []row= (Object [])it.next();
			dto = new chargeDetailsList();
			dto.setCharge(row[0]);
			dto.setDescription(row[1]);
			chargeDetailList1.add(dto);
		}
		return chargeDetailList1;
		
	} catch (Exception e) {
    	 e.printStackTrace();
	}
	return null;
	}
	public List findContractListNEW(String corpID) {
		String query="select distinct contract from contract where (ending is null or date_format(ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d')) and corpID='"+corpID+"' union select distinct contract from defaultaccountline where corpId='"+corpID+"' ORDER BY contract";
		List temp =  this.getSession().createSQLQuery(query).list();
		return temp;
		//return getHibernateTemplate().find(query);
		
	}
	public List findContractListFromContract(String corpID){
		String query="select distinct contract from contract c where ((ending is null or date_format(ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d'))) and corpid='"+corpID+"' and contract not in (select distinct contract from defaultaccountline where corpID='"+corpID+"') ORDER BY contract";
		List temp =  this.getSession().createSQLQuery(query).list();
		return temp;
	}
	public List findContractListFromTemplate(String corpID){
		String query="select distinct contract from defaultaccountline where corpID='"+corpID+"' ORDER BY contract";
		List temp =  this.getSession().createSQLQuery(query).list();
		return temp;
	}
	
	public List<Charges> findChargeCodeForMissingDefaultAccountLineContract(String fromContract,String corpID,String toContract){
		String query="";
		String id="";
		String query1="";
		String chargeCode="";
		query1="select charge from charges where corpId='"+corpID+"' and contract='"+toContract+"' and status is true";
		List al=this.getSession().createSQLQuery(query1).list();
		if(al.size()>0 && !al.isEmpty() && al!=null){
		Iterator itr = al.iterator();
		while(itr.hasNext()){
			if(chargeCode.equals("")){
				chargeCode = "'"+itr.next().toString()+"'";
			}
			else{
				chargeCode = chargeCode+","+"'"+itr.next().toString()+"'";
			}
		}
		}
		
		if(chargeCode.equals("")){
			chargeCode="'"+chargeCode+"'";
		}
		
		
		query="select id from defaultaccountline where corpId='"+corpID+"' and contract='"+toContract+"' and chargeCode not in("+chargeCode+") ";
		 List templateList=this.getSession().createSQLQuery(query).list();
		if(templateList.size()>0 && !templateList.isEmpty() && templateList!=null){
		Iterator itr = templateList.iterator();
		while(itr.hasNext()){
			if(id.equals("")){
				id = "'"+itr.next().toString()+"'";
			}
			else{
				id = id+","+"'"+itr.next().toString()+"'";
			}
		}
		}
		
		if(id.equals("")){
			id="'"+id+"'";
		}
		String q1="from DefaultAccountLine where contract='"+fromContract.replaceAll("'", "''")+"' and corpID='"+corpID+"' and id not in("+id+") and (chargeCode in("+chargeCode+") or chargeCode='' or chargeCode is null) and chargecode not in (select distinct chargeCode from DefaultAccountLine where contract='"+toContract.replaceAll("'", "''")+"' and corpID='"+corpID+"')";
		return getHibernateTemplate().find(q1);
	}
	public int getChargeTemplateCopy(String contract, String sessionCorpID,String id,String toContract) {
		/*String q1="";
		String chargeCode="";
		q1="select charge from charges where corpId='"+sessionCorpID+"' and contract='"+toContract+"' and status is true";
		List al=this.getSession().createSQLQuery(q1).list();
		if(al.size()>0 && !al.isEmpty() && al!=null){
		Iterator itr = al.iterator();
		while(itr.hasNext()){
			if(chargeCode.equals("")){
				chargeCode = "'"+itr.next().toString()+"'";
			}
			else{
				chargeCode = chargeCode+","+"'"+itr.next().toString()+"'";
			}
		}
		}
		
		if(chargeCode.equals("")){
			chargeCode="'"+chargeCode+"'";
		}
		
		return getHibernateTemplate().find("from DefaultAccountLine where  id='"+id+"' and contract='"+contract+"' and corpID='"+sessionCorpID+"' and (chargeCode in("+chargeCode+") or chargeCode='' or chargeCode is null)");*/
		
		int i=0;
		 try {
			 Query q = this.getSession().createSQLQuery("{ call CopyContractToDefaultAccountLine(?,?,?,?) }");
			 q.setParameter(0,sessionCorpID);
			 q.setParameter(1,contract);
			 q.setParameter(2,toContract);
			 q.setParameter(3,id);
			 i=q.executeUpdate();
			 return i;
		 } catch (Exception e) {
				e.printStackTrace();
		}
		 return i;
		
	}
	public List findSameDefaultAccountLineContract(String fromcontract,String corpID,String toContract){
		List lista=new ArrayList();
		try{
			String query1="SELECT  id from defaultaccountline where corpID='"+corpID+"' and  contract='"+fromcontract+"'";
			 lista= this.getSession().createSQLQuery(query1).list();
			
			String query2="SELECT  id from defaultaccountline where corpID='"+corpID+"' and  contract='"+toContract+"'";
			List listb= this.getSession().createSQLQuery(query2).list();
			if(lista.size()==listb.size()){
				return lista;
			}else{
				lista=new ArrayList();
			}
			}catch(Exception e){}
		return lista;
		
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List findChargeCodeForMisMatchDefaultAccountLine(String fromContract,String corpID,String toContract){
		getHibernateTemplate().setMaxResults(100000);
		String q1="from DefaultAccountLine where contract='"+fromContract.replaceAll("'", "''")+"' and corpID='"+corpID+"' and chargeCode not in (select distinct chargeCode from DefaultAccountLine where contract='"+toContract.replaceAll("'", "''")+"' and corpID='"+corpID+"') )";
		return getHibernateTemplate().find(q1);
	}
	public List findChargeCodeForMisMatchFromCharges(String fromcontract,String corpID,String toContract){
		getHibernateTemplate().setMaxResults(100000);
		String a1="";
		String charge="";
		a1="select chargeCode from defaultaccountline d where d.corPid='"+corpID+"' and d.contract='"+fromcontract+"' ";
		a1=a1+"and  not exists(select id from charges where corpId='"+corpID+"'and status=true ";
		a1=a1+" and d.chargeCode=charge and contract='"+toContract+"') ";
		List al=this.getSession().createSQLQuery(a1).list();
		if(al.size()>0 && !al.isEmpty() && al!=null){
		Iterator itr = al.iterator();
		while(itr.hasNext()){
			if(charge.equals("")){
				charge = "'"+itr.next().toString()+"'";
			}
			else{
				charge = charge+","+"'"+itr.next().toString()+"'";
			}
		}
		}
		
		if(charge.equals("")){
			charge="'"+charge+"'";
		}
		String q1="from DefaultAccountLine where contract='"+fromcontract.replaceAll("'", "''")+"' and corpID='"+corpID+"' and chargeCode in("+charge+") )";
		return getHibernateTemplate().find(q1);
	}
	
	public List getDuplicatedData(String corpID){
		//                      1          2        3          4           5          6         7      8      9
		String query="select contract,chargeCode,billToCode,billToName,vendorCode,vendorName,corpID,amount,quantity,"
				//    10   11   12     13     14          15         16
				+ " rate,basis,mode,jobType,serviceType,chargeBack,categories,"
				//   17     18    19     20
				+ " route,recGl,payGl,description,"
				//     21        22       23        24      25         26                27            28
				+ " commodity,packMode,orderNumber,markUp,sellRate,estimatedRevenue,companyDivision,estVatPercent,"
				//     29          30          31           32               33          34             35
				+ " estVatDescr,estVatAmt,originCountry,destinationCountry,equipment,estExpVatDescr,estExpVatPercent,"
				//    36               37
				+ " count(contract),SUBSTRING(replace(group_concat(id),SUBSTRING_INDEX(group_concat(id),',',1),''),2),"
				//    38
				+ " group_concat(id) from defaultaccountline where corpid='"+corpID+"' "
				+ " group by corpID,amount,quantity,rate,basis,mode,jobType,serviceType,chargeBack,categories,route,"
				+ " vendorCode,vendorName,contract,chargeCode,"
				+ " recGl,payGl,billToCode,billToName,description,commodity,packMode,orderNumber,markUp,sellRate,estimatedRevenue,"
				+ " companyDivision,estVatPercent,estVatDescr,estVatAmt,originCountry,destinationCountry,equipment,estExpVatDescr,"
				+ " estExpVatPercent having count(contract)>1 order by count(contract) desc";		
			
			List list= this.getSession().createSQLQuery(query).list();
			List <Object> defaultAccountLineList = new ArrayList<Object>();
			 Iterator it=list.iterator();
			 DuplicateDefaultAccountLineList dtoDuplicateList=null;
			  while(it.hasNext()){
				  String listVal ="";
				  Object [] row=(Object[])it.next();
				  dtoDuplicateList=new DuplicateDefaultAccountLineList();
				  if(row[0]==null){
					  dtoDuplicateList.setContract("");
				  }else{
					  dtoDuplicateList.setContract(row[0]);  
				  }
				  if(row[1]==null){
					  dtoDuplicateList.setChargeCode("");
				  }else{
					  dtoDuplicateList.setChargeCode(row[1]);  
				  }
				  if(row[3]==null){
					  dtoDuplicateList.setBillToName("");
				  }else{
					  dtoDuplicateList.setBillToName(row[3]);  
				  }
				  if(row[5]==null){
					  dtoDuplicateList.setVendorName("");
				  }else{
					  dtoDuplicateList.setVendorName(row[5]);  
				  }
				  if(row[9]==null){
					  dtoDuplicateList.setRate("");
				  }else{
					  dtoDuplicateList.setRate(row[9]);  
				  }
				  if(row[36]==null){
					  dtoDuplicateList.setId("");
				  }else{
					  dtoDuplicateList.setId(row[36]);  
				  }
				  defaultAccountLineList.add(dtoDuplicateList); 
				  
			  }
			  return defaultAccountLineList;
	}
	
	public String deleteDuplicateTemplateData(String corpID,String listedId){
		int record=0;
		String totRecords=null;
		try{
			 record = this.getSession().createSQLQuery("delete from defaultaccountline where id in ("+listedId+") and corpid='"+corpID+"' ").executeUpdate();
			}catch(Exception e){
				System.out.print("Error occour while deleting  from defaultaccountline"+e);
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	 e.printStackTrace();
			}
		totRecords=record+" Delete(s) successfully.";
		return totRecords;
	}
	
public class DuplicateDefaultAccountLineList{
	private Object contract;
	private Object chargeCode;
	private Object billToCode;
	private Object billToName;
	private Object vendorCode;
	private Object vendorName;
	private Object rate;
	private Object id;
	
	public Object getContract() {
		return contract;
	}
	public void setContract(Object contract) {
		this.contract = contract;
	}
	public Object getChargeCode() {
		return chargeCode;
	}
	public void setChargeCode(Object chargeCode) {
		this.chargeCode = chargeCode;
	}
	public Object getBillToCode() {
		return billToCode;
	}
	public void setBillToCode(Object billToCode) {
		this.billToCode = billToCode;
	}
	public Object getBillToName() {
		return billToName;
	}
	public void setBillToName(Object billToName) {
		this.billToName = billToName;
	}
	public Object getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(Object vendorCode) {
		this.vendorCode = vendorCode;
	}
	public Object getVendorName() {
		return vendorName;
	}
	public void setVendorName(Object vendorName) {
		this.vendorName = vendorName;
	}
	public Object getRate() {
		return rate;
	}
	public void setRate(Object rate) {
		this.rate = rate;
	}
	public Object getId() {
		return id;
	}
	public void setId(Object id) {
		this.id = id;
	}
}
	
	public class chargeDetailsList{
		private Object charge;
		private Object description;
		public Object getCharge() {
			return charge;
		}
		public void setCharge(Object charge) {
			this.charge = charge;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
	}
	public int  findChargeCodeForMissingDefaultAccountLineContractWithoutValidation(String userName,String fromContract,String corpID,String toContract){
		
		int i=0;
		 try {
			 CallableStatement callable = null;
			 CallableStatement q = this.getSession().connection().prepareCall("{ call copyAccountingTemplate(?,?,?,?,?) }");
			 q.setString(1,userName);
			 q.setString(2,fromContract);
			 q.setString(3,corpID);
			 q.setString(4,toContract);
			 q.registerOutParameter(5,java.sql.Types.INTEGER);
			 i=q.executeUpdate();
			 return i;
		 } catch (Exception e) {
				e.printStackTrace();
		}
		 return i;
		
	   
	}


	


	
	
}
