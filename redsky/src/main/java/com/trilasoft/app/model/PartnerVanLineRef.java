package com.trilasoft.app.model;

import org.appfuse.model.BaseObject;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "partnervanlineref")
@FilterDef(name = "partnerCorpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "partnerCorpID", condition = "corpID in (:forCorpID)")
//@Table(appliesTo="location", indexes = { @Index(name="locOcc",columnNames={"occupied","location"} ) } )
public class PartnerVanLineRef extends BaseObject {
	private Long id;

	private String partnerCode;

	private String vanLineCode;

	private String jobType;

	private String corpID;

	private String createdBy;

	private String updatedBy;

	private Date createdOn;

	private Date updatedOn;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("partnerCode", partnerCode).append("vanLineCode", vanLineCode).append("jobType", jobType).append("corpID",
				corpID).append("createdBy", createdBy).append("updatedBy", updatedBy).append("createdOn", createdOn).append("updatedOn", updatedOn).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PartnerVanLineRef))
			return false;
		PartnerVanLineRef castOther = (PartnerVanLineRef) other;
		return new EqualsBuilder().append(id, castOther.id).append(partnerCode, castOther.partnerCode).append(vanLineCode, castOther.vanLineCode).append(jobType,
				castOther.jobType).append(corpID, castOther.corpID).append(createdBy, castOther.createdBy).append(updatedBy, castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn, castOther.updatedOn).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(partnerCode).append(vanLineCode).append(jobType).append(corpID).append(createdBy).append(updatedBy).append(createdOn)
		.append(updatedOn).toHashCode();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length = 3)
	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	@Column(length = 8)
	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	@Column(length = 8)
	public String getVanLineCode() {
		return vanLineCode;
	}

	public void setVanLineCode(String vanLineCode) {
		this.vanLineCode = vanLineCode;
	}
}