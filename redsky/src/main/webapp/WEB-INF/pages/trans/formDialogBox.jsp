<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="org.appfuse.model.User"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	User user = (User)auth.getPrincipal();
	String sessionCorpID = user.getCorpID();
%>


<head>   
<title><fmt:message key="reportsDetail.title"/></title>   
<meta name="heading" content="<fmt:message key='reportsDetail.heading'/>"/>   
<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
</c:if>
<style type="text/css">
	h2 {background-color: #CCCCCC}
	.attach{
	background:url("images/attachIcon.png") right center no-repeat, url("images/btn_bg.gif") right center repeat-x;
	border: 1px solid #CCCCCC; padding:2px 0px;
	}
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>

<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns()
	</script>
	
<script language="javascript" type="text/javascript">
var http125678 = getHTTPObject();
function setDocCat1(targetElement){
	//alert("hi");
	var doctype=targetElement.options[targetElement.selectedIndex].value;
	var url="docCategoryByDocType.html?ajax=1&decorator=simple&popup=true&docType="+encodeURI(doctype);
	//alert(url);
	http125678.open("GET", url, true);
	http125678.onreadystatechange = handleHttpResponseFordocCategory1;
	http125678.send(null);
}
function handleHttpResponseFordocCategory1(){
	//alert("re...");
    if(http125678.readyState == 4){
		var results = http125678.responseText
      results = results.trim();
		//alert(results);
	document.forms['reportForm'].elements['documentCategory'].value = results; 
							     	 
	}
}
function validatefields(val){
	<c:forEach var="reportParameter" items="${reportParameters}" varStatus="status">
	var i;
	i = ${status.index}*1 + 17;
	var ab="${reportParameter.name}";
	  if((document.forms['reportForm'].elements['reportParameter_'+ab].value) == ''){
		  document.forms['reportForm'].elements['reportParameter_'+ab].focus();
			alert('Please enter values for '+"${reportParameter.name}"+'.');
		  return false;  
		} 
		if('${reportValidationFlag}' == 'Yes')
		{
	  	if((document.forms['reportForm'].elements['reportParameter_'+ab].value) == '%' || (document.forms['reportForm'].elements['reportParameter_'+ab].value) == '%%'){
	  		document.forms['reportForm'].elements['reportParameter_'+ab].focus();
			alert("${reportParameter.name}"+' as % or %% not allowed.');
		  return false;  
		}
		} 
		
	</c:forEach>
	selectJobs();
	if(document.forms['reportForm'].elements['formFieldName'].value!='' && document.forms['reportForm'].elements['formValue'].value !=''){
		setTimeout("pick()", 3000);
	}
	<configByCorp:fieldVisibility componentId="component.accountline.Invoice.FileCabinetInvoiceReport">
	document.forms['reportForm'].elements['checkfromfilecabinetinvoicereportflag'].value = 'Y';
	</configByCorp:fieldVisibility>
	document.forms['reportForm'].elements['fileType'].value = val;
	var url = 'viewReportWithParam.html?decorator=popup&popup=true';
	document.forms['reportForm'].action =url;
	document.forms['reportForm'].submit();
	return true;
}



function selectJobs(){
	var selectedJobs = "";
	var selObj = document.getElementById('compareTypes');
	var i;
	var count = 0;
	if(selObj!= null){
	for (i=0; i<selObj.options.length; i++) {
		 if (selObj.options[i].selected) {
			 selectedJobs = selectedJobs +",'"+ selObj.options[i].value+"'";
		    count++;
		  }
		}
	}
	selectedJobs = selectedJobs.trim();
	document.forms['reportForm'].elements['selectedJobs'].value = selectedJobs.substring(1,selectedJobs.length);
	}





function pick() {
	  parent.window.opener.document.location.reload();
}	

function refreshParent() {
	window.close();
}

function mergeReporPdf(val){
	<c:forEach var="reportParameter" items="${reportParameters}" varStatus="status">
	var i;
	i = ${status.index}*1 + 16;
	var ab="${reportParameter.name}";
	  if((document.forms['reportForm'].elements['reportParameter_'+ab].value) == ''){
		  document.forms['reportForm'].elements['reportParameter_'+ab].focus();
			alert('Please enter values for '+"${reportParameter.name}"+'.');
		  return false;  
		} 
		if('${reportValidationFlag}' == 'Yes')
		{
	  	if((document.forms['reportForm'].elements['reportParameter_'+ab].value) == '%' || (document.forms['reportForm'].elements['reportParameter_'+ab].value) == '%%'){
	  		 document.forms['reportForm'].elements['reportParameter_'+ab].focus();
			alert("${reportParameter.name}"+' as % or %% not allowed.');
		  return false;  
		}
		} 
		
	</c:forEach>
	document.forms['reportForm'].elements['fileType'].value = val;
	<c:if test="${empty param.popup}"> 
	  url = 'conbinedAllPdf.html';
	</c:if>		
    <c:if test="${param.popup}">  
				var url = 'conbinedAllPdf.html?decorator=popup&popup=true';		
	</c:if>
		
	document.forms['reportForm'].action =url;
	document.forms['reportForm'].submit();
	return true;
		
    } 
	
</script>
	
<script>
function display(){
	window.onload=displayStatusReason();
}
function displayStatusReason(){
     var statusValue=document.forms['reportForm'].elements['docsxfer'].value;
     if(statusValue=='Yes'){
        document.getElementById('docs').style.display='block';
     }else{
        document.getElementById('docs').style.display='none';
     }
} 
function validateFields1(){
	<c:forEach var="reportParameter" items="${reportParameters}" varStatus="status">
		var i;
		i = ${status.index}*1 + 16;
		var ab="${reportParameter.name}";
		  if((document.forms['reportForm'].elements['reportParameter_'+ab].value) == ''){
			  document.forms['reportForm'].elements['reportParameter_'+ab].focus();
				alert('Please enter values for '+"${reportParameter.name}"+'.');
			  return false;  
			}

		if('${reportValidationFlag}' == 'Yes')
		{
		  if((document.forms['reportForm'].elements['reportParameter_'+ab].value) == '%' || (document.forms['reportForm'].elements['reportParameter_'+ab].value) == '%%'){
			  document.forms['reportForm'].elements['reportParameter_'+ab].focus();
				alert("${reportParameter.name}"+' as % or %% not allowed.');
			  return false;  
			} 
		}
			
	</c:forEach>
	selectJobs();
	if(document.forms['reportForm'].elements['docFileType'].value==""){
		alert("Select a file type");
		return false;
	}	
	document.form['reportForm'].action="/uploadReport.html";	
	document.forms['reportForm'].submit();	
	return true;
}
function documentMap(targetElement){
   	var fileType = targetElement.value;
   	var url="docMapFolder.html?decorator=simple&popup=true&fileType=" + encodeURI(fileType);
   	http2.open("GET", url, true);
   	http2.onreadystatechange = handleHttpResponseMap;
   	http2.send(null);
}

function handleHttpResponseMap(){
      if(http2.readyState == 4){
		var results = http2.responseText
        results = results.trim();
        var res = results.substring(1,results.length-1);
		document.forms['reportForm'].elements['mapFolder'].options[0].text = res; 
		document.forms['reportForm'].elements['mapFolder'].options[0].value = res;
		document.forms['reportForm'].elements['mapFolder'].options[0].selected=true;					     	 
	}
}

var http2 = getHTTPObject();

function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function setDescriptionDuplicate(targetElement){
	document.forms['reportForm'].elements['myFile.description'].value = targetElement.options[targetElement.selectedIndex].text;
}
var form_submitted = false;

		function submit_form()
		{
		  if (form_submitted)
		  {
		    alert ("Your form has already been submitted. Please wait...");
		    return false;
		  }
		  else
		  {
		    form_submitted = true;
		    return true;
		  }
		}
</script>
	
<style type="text/css">
	/* collapse */
	

img {
cursor:pointer;
}
		


#Layer1 {
width:540px;
}
		
</style>
	
	
</head>  
<body style="background-color:#444444;">
<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<s:form id="reportForm" target="_parent" action='${empty param.popup?"viewReportWithParam.html":"viewReportWithParam.html?decorator=popup&popup=true"}' method="post" onsubmit="return submit_form()" validate="true" enctype="multipart/form-data" >
	<s:hidden name="docsxfer" value="<%=request.getParameter("docsxfer") %>" />
	<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
	<s:hidden name="module" value="%{reports.module}"/>
	<s:hidden name="reportName" value="${reports.description}"/>
	<s:hidden name="reportModule" value="${reportModule}"/>
	<s:hidden name="reportSubModule" value="${reportSubModule}"/>
	<s:hidden name="formFieldName" value="${reports.formFieldName}"/>
	<s:hidden name="formValue" value="${reports.formValue}"/>
	<s:hidden name="selectedJobs" />
	<s:hidden name="multipleJobs" />
	<s:hidden name="checkfromfilecabinetinvoicereportflag" value=""/>
    <s:hidden name="jobNumber" value="<%=request.getParameter("customerFile.sequenceNumber") %>"/>
	<c:set var="jobNumber" value="<%=request.getParameter("jobNumber") %>" />	
	<s:set name="reportss" value="reportss" scope="request"/>
	
	<s:hidden name="claimNumber" value="<%=request.getParameter("claimNumber") %>" />
	<c:set var="claimNumber" value="<%=request.getParameter("claimNumber") %>" />
	
	<s:hidden name="bookNumber" value="<%=request.getParameter("bookNumber") %>" />
	<c:set var="bookNumber" value="<%=request.getParameter("bookNumber") %>" />
	
	<s:hidden name="noteID" value="<%=request.getParameter("noteID") %>" />
	<c:set var="noteID" value="<%=request.getParameter("noteID") %>" />
	
	<s:hidden name="custID" value="<%=request.getParameter("custID") %>" />
	<c:set var="custID" value="<%=request.getParameter("custID") %>" />
	
	<s:hidden name="cid" value="<%=request.getParameter("cid") %>" />
	<s:set name="cid" value="cid" scope="session"/>

	<s:hidden name="sid" value="<%=request.getParameter("sid") %>" />
	<s:set name="sid" value="sid" scope="session"/>
		
	<s:hidden name="invoiceNumber" value="<%=request.getParameter("invoiceNumber") %>" />
	<c:set var="invoiceNumber" value="<%=request.getParameter("invoiceNumber") %>" />

	<s:hidden name="regNumber" value="<%=request.getParameter("regNumber")%>"/>
	<c:set var="regNumber" value="<%=request.getParameter("regNumber") %>" />

	<s:hidden name="jobType" value="<%=request.getParameter("jobType") %>"/>
	<c:set var="jobType" value="<%=request.getParameter("jobType") %>" />
	
	<s:hidden name="preferredLanguage" value="<%=request.getParameter("preferredLanguage") %>"/>
	<c:set var="preferredLanguage" value="<%=request.getParameter("preferredLanguage") %>" />		


	
	<div id="Layer1">
	<table cellspacing="0" cellpadding="0" border="0" style="padding-bottom:0px">
		
			<tr>
				<td><img src="<c:url value='/images/form_ico.gif'/>"/></td>
				<td>
					<table cellspacing="0" cellpadding="0">
					<tr><td>&nbsp;</td></tr> 
					<tr><td><b><font style="font-size:16px; font-family:Verdana, Geneva, sans-serif; font-style:italic; color: #EB8234;">Form Manager</font></b></td></tr> 
					<tr style="height: 5px;"><td></td></tr> 
					<tr><td><b><font style="color: #5D5D5D;">Form : ${reports.description}</font></b></td></tr> 
					</table>
				</td>
			</tr>	
		
	</table>
	<c:if test="${list!='main'}">	
	<div id="newmnav">
		  <ul>
		  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Forms Manager<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
		</div>
		</div><div class="spn" style="width:510px;line-height:0.2em;">&nbsp;</div>
</c:if>  
	<table class="notesDetailTable" cellspacing="0" cellpadding="0" border="0" style="width:510px;padding-bottom:0px">
		<tbody>
			<tr>
				<td>
		 			<div class="subcontent-tab">Select Parameters and Output format for Form</div>
		   				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
		    				<tbody>	
		    					<tr><td style="height:10px;"></td></tr> 
								<tr><td><b>&nbsp;&nbsp;Parameter(s):</b></td></tr>
								<tr style="height: 10px;"><td></td></tr> 
								  <c:forEach var="reportParameter" items="${reportParameters}">
									<tr>
										<td width="130px" align="left" valign="top" class="listwhitetext">&nbsp;&nbsp;&nbsp;<c:out value="${reportParameter.name}"/><font color="red" size="2">*</font> :</td>
											<c:if test='${reportParameter.valueClassName == "java.util.Date"}'> 
											  <td class="listwhitetext">
											  	<c:set var="inputParameterName" value='${"reportParameter_"}${reportParameter.name}'/>
											  	<s:textfield cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" maxlength="11" readonly="true"/>
											  </td>
											  	<td align="left"><img id="${inputParameterName}-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
											</c:if>
											
											<c:if test='${reportParameter.valueClassName != "java.util.Date"}'>											
											 <c:set var="inputParameterName" value='${"reportParameter_"}${reportParameter.name}'/>
										      <td class="listwhitetext">
										    	<c:if test="${(inputParameterName == 'reportParameter_User Name')}">
													<s:textfield cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${pageContext.request.remoteUser}" readonly="true" size="15"/>
												</c:if>
												
												<c:if test="${!(inputParameterName == 'reportParameter_User Name')}">
												<c:if test="${jobNumber==null || jobNumber=='' }">
													<c:if test="${(inputParameterName == 'reportParameter_Corporate ID')}">
											  			<s:textfield cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="<%=sessionCorpID %>" readonly="true" size="15"/>
											  		</c:if>
											  		<c:if test="${!(inputParameterName == 'reportParameter_Corporate ID')}">
											  			<c:if test="${!(inputParameterName == 'reportParameter_Customer File Number')}">
											  				<c:if test="${(inputParameterName == 'reportParameter_Warehouse')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{house}" cssStyle="width:105px" headerKey="" headerValue="" value="${house}"/>
			    											</c:if>
			    											<c:if test="${(inputParameterName == 'reportParameter_Bank')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{BANK}" cssStyle="width:150px" />
			    											</c:if>    											
			    											<c:if test="${(inputParameterName == 'reportParameter_Coordinator')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{soList}" cssStyle="width:105px" headerKey="" headerValue="" value="${soList}"/>
			    											</c:if>
			    											<c:if test="${(inputParameterName == 'reportParameter_SO Container')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{containerList}" cssStyle="width:105px" headerKey="%%" headerValue="all" value="${containerList}"/>
			    											</c:if>
			    											<c:if test="${(inputParameterName == 'reportParameter_Payment Status')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{payableStatusList}" cssStyle="width:105px" headerKey="" headerValue="" value="${payableStatusList}"/>
			    											</c:if>
			    											<c:if test="${(inputParameterName == 'reportParameter_Documents')}">
			    								 				<s:textarea  id="${inputParameterName}" name="${inputParameterName}" value="" readonly="" cols="30" label="6" />
															</c:if>		
															<c:if test="${(inputParameterName == 'reportParameter_DTD Service Charge')}">
																<s:textarea   cssClass="textarea"  id="${inputParameterName}" name="${inputParameterName}" value="" readonly="" cols="46"  />
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Print Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Print With Header','Print Without Header'}" cssStyle="width:115px"  value=" "/>
															</c:if>																	
															<c:if test="${(inputParameterName == 'reportParameter_Print Options')}">															
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="%{printOptions}" cssStyle="width:175px"  value="%{printOptionsValue} "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Print_Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'No header / footer','UGRN - ACF'}" cssStyle="width:175px"  value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Print_Options')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'No header / footer','Cadogan Tate'}" cssStyle="width:175px"  value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Print_Method')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'No header / footer','UGRN','Voerman'}" cssStyle="width:175px"  value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Send Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Email','Print'}" cssStyle="width:115px"  value=" "/>
															</c:if>
															
															<c:if test="${(inputParameterName == 'reportParameter_Currency')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Local','Foreign'}" cssStyle="width:100px"  value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Letter')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'PA','DA'}" cssStyle="width:100px"  value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Compare')}">
															   		<s:select cssClass="list-menu" name="${inputParameterName}" id="compareTypes" list="%{compare}" value="%{multipleJobs}" cssStyle="width:130px; height:100px" multiple="true" headerKey="No Compare" headerValue="No Compare"/><td><font style="font-size:9px;"><u>* Use Control + mouse to select multiple ServiceOrder</u></font></td>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Bill To')}" >
			    												<s:textfield cssClass="input-text" id="${inputParameterName}" name="reportParameter_Bill To" size="15"/>
			    										   <img class="openpopup" width="17" height="20" style="vertical-align:bottom;" onclick="window.open('partnersPopup.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=reportParameter_Bill To','billtocode','width=950,height=650');"  src="<c:url value='/images/open-popup.gif'/>" />
															<s:hidden name="secondDescription" />
															<s:hidden name="thirdDescription" />
															<s:hidden name="fourthDescription" />
															<s:hidden name="fifthDescription" />
															<s:hidden name="sixthDescription" />
															<s:hidden name="firstDescription" />
															<s:hidden name="description" />
			    				                     	</c:if>
			    				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Partner Code')}" >
			    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="reportParameter_Partner Code" size="15"/>
			    										   <img class="openpopup" width="17" height="20" style="vertical-align:bottom;" onclick="window.open('partnersPopup.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=reportParameter_Partner Code','billtocode','width=950,height=650');"  src="<c:url value='/images/open-popup.gif'/>" />
															<s:hidden name="secondDescription" />
															<s:hidden name="thirdDescription" />
															<s:hidden name="fourthDescription" />
															<s:hidden name="fifthDescription" />
															<s:hidden name="sixthDescription" />
															<s:hidden name="firstDescription" />
															<s:hidden name="description" />
			    				                     	</c:if>
			    				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Value Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Estimate','Revised'}" cssStyle="width:115px"  value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Partner Name')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{partnerCodeArrayList}" cssStyle="width:250px" headerKey="" headerValue="" value="${partnerCodeArrayList}"/>
			    											</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Predefined Text')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{preDefTxt}" cssStyle="width:105px" headerKey="" headerValue="" value="${preDefTxt}"/>
			    											</c:if>
			    								 			
															<c:if test="${(inputParameterName == 'reportParameter_Remark')}">
			    								 				<s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:60px" value=" "/>
													        </c:if>	
													        <c:if test="${(inputParameterName == 'reportParameter_Rates Exclude')}">
			    								 				<s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:60px" value=" "/>
													        </c:if>
													        <c:if test="${(inputParameterName == 'reportParameter_Rates Include')}">
			    								 				<s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:60px" value=" "/>
													        </c:if>
													        <c:if test="${(inputParameterName == 'reportParameter_Not Include')}">
			    								 				<s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:60px" value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Include')}">
			    								 				<s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:60px" value=" "/>
															 </c:if>		
													        <c:if test="${(inputParameterName == 'reportParameter_Order Details')}">
			    								 				<s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:60px" value=" "/>
													        </c:if>	
													        <c:if test="${(inputParameterName == 'reportParameter_Email')}">
                                                                <s:textfield cssClass="list-menu" name="${inputParameterName}" cssStyle="width:175px;" value=" "/>
                                                            </c:if>													        
													        <c:if test="${(inputParameterName == 'reportParameter_ATTN')}">
                                                                <s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px;" value=" "/>
                                                            </c:if> 
                                                            
                                                            <c:if test="${(inputParameterName == 'reportParameter_Credit Terms')}">
                                                                <s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:100px" value="${crTerms}"/>
                                                            </c:if>	
                                                            <c:if test="${(inputParameterName == 'reportParameter_Additional Information')}">
                                                                <s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:50px" value=" "/>
                                                     		</c:if>
                                                     			<c:if test="${(inputParameterName == 'reportParameter_Special Requirements')}">
                                                                <s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:50px" value=" "/>
                                                     		</c:if>		
			    																								  	
														  	<c:if test="${reports.corpID == 'SSCW'}">
			    											<c:if test="${!(inputParameterName == 'reportParameter_Warehouse') && !(inputParameterName == 'reportParameter_Coordinator') && !(inputParameterName == 'reportParameter_Payment Status') && !(inputParameterName == 'reportParameter_SO Container')  && !(inputParameterName == 'reportParameter_Predefined Text') && !(inputParameterName == 'reportParameter_Documents')  && !(inputParameterName =='reportParameter_DTD Service Charge') && !(inputParameterName == 'reportParameter_Print Option') && !(inputParameterName =='reportParameter_Print_Option') && !(inputParameterName =='reportParameter_Print Options') && !(inputParameterName =='reportParameter_Print_Options') && !(inputParameterName =='reportParameter_Print_Method') && !(inputParameterName == 'reportParameter_Send Option') && !(inputParameterName =='reportParameter_Bank') && !(inputParameterName =='reportParameter_Compare') && !(inputParameterName == 'reportParameter_Currency') && !(inputParameterName == 'reportParameter_Bill To') && !(inputParameterName == 'reportParameter_Partner Code') && !(inputParameterName =='reportParameter_Value Option') && !(inputParameterName =='reportParameter_Partner Name') && !(inputParameterName == 'reportParameter_Remark') && !(inputParameterName =='reportParameter_Rates Exclude') && !(inputParameterName =='reportParameter_Rates Include')  && !(inputParameterName =='reportParameter_Not Include') && !(inputParameterName =='reportParameter_Include') && !(inputParameterName =='reportParameter_Order Details') && !(inputParameterName == 'reportParameter_ATTN') && !(inputParameterName == 'reportParameter_Email') && !(inputParameterName =='reportParameter_Credit Terms') && !(inputParameterName =='reportParameter_Special Requirements') && !(inputParameterName =='reportParameter_Additional Information') && !(inputParameterName == 'reportParameter_Letter')}" >
			    												<s:textfield  cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value=""/>
			    											</c:if>
			    											</c:if>
			    										
			    											<c:if test="${reports.corpID != 'SSCW'}">
			    											<c:if test="${!(inputParameterName == 'reportParameter_Warehouse') && !(inputParameterName == 'reportParameter_Coordinator') && !(inputParameterName == 'reportParameter_Payment Status') && !(inputParameterName == 'reportParameter_SO Container') && !(inputParameterName == 'reportParameter_Predefined Text') && !(inputParameterName == 'reportParameter_Documents') && !(inputParameterName =='reportParameter_DTD Service Charge') && !(inputParameterName == 'reportParameter_Print Option') && !(inputParameterName =='reportParameter_Print_Option') && !(inputParameterName =='reportParameter_Print Options') && !(inputParameterName =='reportParameter_Print_Options') && !(inputParameterName =='reportParameter_Print_Method') && !(inputParameterName == 'reportParameter_Send Option')  && !(inputParameterName == 'reportParameter_Bank') && !(inputParameterName =='reportParameter_Compare') && !(inputParameterName == 'reportParameter_Currency') && !(inputParameterName == 'reportParameter_Bill To') && !(inputParameterName == 'reportParameter_Partner Code')  && !(inputParameterName =='reportParameter_Value Option') && !(inputParameterName =='reportParameter_Partner Name') && !(inputParameterName == 'reportParameter_Remark') && !(inputParameterName =='reportParameter_Rates Exclude') && !(inputParameterName =='reportParameter_Rates Include')  && !(inputParameterName =='reportParameter_Not Include') && !(inputParameterName =='reportParameter_Include')  && !(inputParameterName =='reportParameter_Order Details') && !(inputParameterName == 'reportParameter_ATTN') && !(inputParameterName == 'reportParameter_Email') && !(inputParameterName =='reportParameter_Credit Terms') && !(inputParameterName =='reportParameter_Special Requirements') && !(inputParameterName =='reportParameter_Additional Information') && !(inputParameterName == 'reportParameter_Letter')}" >
			    												<s:textfield  cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value="%%"/>
			    											</c:if>
			    											</c:if>
														  	
											  			</c:if>
											  		</c:if>

											  		<c:if test="${custID != null || custID != ''}">
											  			<c:if test="${(inputParameterName == 'reportParameter_Customer File Number')}">
														  	<s:textfield  cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${custID}" readonly="false" size="15"/>
														</c:if> 
		 											</c:if>
		 											
		 										
								 										
													
											  	</c:if>
											  	
											  	<c:if test="${!(jobNumber==null || jobNumber=='')}">
											  	
											  		<c:if test="${custID != null || custID != ''}">
											  			<c:if test="${(inputParameterName == 'reportParameter_Customer File Number')}">
														  	<s:textfield  cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${custID}" readonly="false" size="15"/>
														</c:if> 
		 											</c:if>
		 											
		 											<c:if test="${custID != null || custID != ''}">
											  			<c:if test="${(inputParameterName == 'reportParameter_Quotation File Number')}">
														  	<s:textfield  cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${jobNumber}" readonly="false" size="15"/>
														</c:if> 
		 											</c:if>
		 											
		 											<c:if test="${invoiceNumber != null || invoiceNumber != ''}">
		 												<c:if test="${(inputParameterName == 'reportParameter_Invoice Number')}">
		 													<s:textfield  cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${invoiceNumber}" readonly="false" size="15"/>
														</c:if> 
		 											</c:if>
		 											
		 											<c:if test="${regNumber != null || regNumber != ''}">
		 												<c:if test="${(inputParameterName == 'reportParameter_Regnum')}">
		 													<s:textfield  cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${regNumber}" readonly="false" size="15"/>
														</c:if> 
		 											</c:if>
		 											
											  		<c:if test="${(inputParameterName == 'reportParameter_Service Order Number')}">
											  			<s:textfield cssStyle="text-align:right" cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" value="${jobNumber}" readonly="true" size="15"/>
											  		</c:if>
											  		
											  		<c:if test="${!(claimNumber==null || claimNumber=='')}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Claim Number')}">
														  	<s:textfield cssStyle="text-align:right" cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${claimNumber}" readonly="true" size="15"/>
														</c:if>
													</c:if>
													<c:if test="${reports.corpID == 'SSCW'}">
													<c:if test="${claimNumber==null || claimNumber==''}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Claim Number')}">
														  	<s:textfield cssStyle="text-align:right" cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value=""/>
														</c:if>
													</c:if>
													</c:if>
													
													<c:if test="${reports.corpID != 'SSCW'}">
													<c:if test="${claimNumber==null || claimNumber==''}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Claim Number')}">
														  	<s:textfield cssStyle="text-align:right" cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value="%%"/>
														</c:if>
													</c:if>
													</c:if>
													
													<c:if test="${!(bookNumber==null || bookNumber=='')}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Book Number')}">
														  	<s:textfield cssStyle="text-align:right" cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${bookNumber}" readonly="true" size="15"/>
														</c:if>
													</c:if>
													
													<c:if test="${reports.corpID == 'SSCW'}">
													<c:if test="${bookNumber==null || bookNumber==''}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Book Number')}">
														  	<s:textfield cssStyle="text-align:right" cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value=""/>
														</c:if>
													</c:if>
													</c:if>
													
													<c:if test="${reports.corpID != 'SSCW'}">
													<c:if test="${bookNumber==null || bookNumber==''}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Book Number')}">
														  	<s:textfield cssStyle="text-align:right" cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value="%%"/>
														</c:if>
													</c:if>
													</c:if>
												  		
												  	<c:if test="${!(noteID==null || noteID=='')}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Work Ticket Number')}">
														  	<s:textfield cssStyle="text-align:right"cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${noteID}" readonly="true" size="15"/>
														</c:if>
													</c:if>
													<c:if test="${reports.corpID == 'SSCW'}">
													<c:if test="${(noteID==null || noteID=='')}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Work Ticket Number')}">
														  	<s:textfield cssStyle="text-align:right"cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value=""/>
														</c:if>
													</c:if>
													</c:if>
													
													<c:if test="${reports.corpID != 'SSCW'}">
													<c:if test="${(noteID==null || noteID=='')}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Work Ticket Number')}">
														  	<s:textfield cssStyle="text-align:right"cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value="%%"/>
														</c:if>
													</c:if>
													</c:if>
													
													<c:if test="${(inputParameterName == 'reportParameter_Bank')}" >
			    											<s:select cssClass="list-menu" name="${inputParameterName}" list="%{BANK}" cssStyle="width:150px" />
			    									</c:if>
													
													<c:if test="${(inputParameterName == 'reportParameter_Documents')}">
														<s:textarea  id="${inputParameterName}" name="${inputParameterName}" value="" readonly="" cols="30" label="6" />
													</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_DTD Service Charge')}">
														<s:textarea   cssClass="textarea"  id="${inputParameterName}" name="${inputParameterName}" value="" readonly="" cols="46"  />
													</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_SO Container')}" >
			    										<s:select cssClass="list-menu" name="${inputParameterName}" list="%{containerList}" cssStyle="width:105px" headerKey="%%" headerValue="all" value="${containerList}"/>
			    									</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Print Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Print With Header','Print Without Header'}" cssStyle="width:115px"  value=" "/>
													</c:if>													
													<c:if test="${(inputParameterName == 'reportParameter_Print Options')}">													
			    								 	<s:select cssClass="list-menu" name="${inputParameterName}" list="%{printOptions}" cssStyle="width:175px"  value="%{printOptionsValue}"/>
													</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Print_Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'No header / footer','UGRN - ACF'}" cssStyle="width:175px"  value=" "/>
													</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Print_Options')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'No header / footer','Cadogan Tate'}" cssStyle="width:175px"  value=" "/>
													</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Print_Method')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'No header / footer','UGRN','Voerman'}" cssStyle="width:175px"  value=" "/>
															</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Send Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Email','Print'}" cssStyle="width:115px"  value=" "/>
													</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Currency')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Local','Foreign'}" cssStyle="width:100px"  value=" "/>
													</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Letter')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'PA','DA'}" cssStyle="width:100px"  value=" "/>
													</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Compare')}">
			    										 <s:select cssClass="list-menu" name="${inputParameterName}" id="compareTypes" list="%{compare}" value="%{multipleJobs}" cssStyle="width:130px; height:100px" multiple="true" headerKey="No Compare" headerValue="No Compare"/><td><font style="font-size:9px;"><u>* Use Control + mouse to select multiple ServiceOrder</u></font></td>
													</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Bill To')}" >
			    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="reportParameter_Bill To" size="15"/>
			    										   <img class="openpopup" width="17" height="20" style="vertical-align:bottom;" onclick="window.open('partnersPopup.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=reportParameter_Bill To','billtocode','width=950,height=650');"  src="<c:url value='/images/open-popup.gif'/>" />
															<s:hidden name="secondDescription" />
															<s:hidden name="thirdDescription" />
															<s:hidden name="fourthDescription" />
															<s:hidden name="fifthDescription" />
															<s:hidden name="sixthDescription" />
															<s:hidden name="firstDescription" />
															<s:hidden name="description" />
			    				                     	</c:if>
			    				                     	
			    				                     	
			    				                     	<c:if test="${(inputParameterName == 'reportParameter_Partner Code')}" >
			    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="reportParameter_Partner Code" size="15"/>
			    										   <img class="openpopup" width="17" height="20" style="vertical-align:bottom;" onclick="window.open('partnersPopup.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=reportParameter_Partner Code','billtocode','width=950,height=650');"  src="<c:url value='/images/open-popup.gif'/>" />
															<s:hidden name="secondDescription" />
															<s:hidden name="thirdDescription" />
															<s:hidden name="fourthDescription" />
															<s:hidden name="fifthDescription" />
															<s:hidden name="sixthDescription" />
															<s:hidden name="firstDescription" />
															<s:hidden name="description" />
			    				                     	</c:if>
			    				                     	
			    				                     	
													<c:if test="${(inputParameterName == 'reportParameter_Value Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Estimate','Revised'}" cssStyle="width:115px"  value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Partner Name')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{partnerCodeArrayList}" cssStyle="width:250px" headerKey="" headerValue="" value="${partnerCodeArrayList}"/>
			    											</c:if>	
													<c:if test="${(inputParameterName == 'reportParameter_Predefined Text')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{preDefTxt}" cssStyle="width:105px" headerKey="" headerValue="" value="${preDefTxt}"/>
			    											</c:if>
			    								 			
													<c:if test="${(inputParameterName == 'reportParameter_Remark')}">
			    								 				<s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:60px" value=" "/>
													</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Rates Exclude')}">
			    								 				<s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:60px" value=" "/>
													</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Rates Include')}">
			    								 				<s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:60px" value=" "/>
													 </c:if>
													 <c:if test="${(inputParameterName == 'reportParameter_Not Include')}">
			    								 				<s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:60px" value=" "/>
													</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Include')}">
			    								 				<s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:60px" value=" "/>
													 </c:if>												
													<c:if test="${(inputParameterName == 'reportParameter_Order Details')}">
			    								 				<s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:60px" value=" "/>
													 </c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Email')}">
                                                                <s:textfield cssClass="list-menu" name="${inputParameterName}" cssStyle="width:175px;" value=" "/>
                                                    </c:if>
													<c:if test="${(inputParameterName == 'reportParameter_ATTN')}">
                                                                <s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px;" value=" "/>
                                                    </c:if>
                                                    
                                                    <c:if test="${(inputParameterName == 'reportParameter_Credit Terms')}">
                                                                <s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:100px" value="${crTerms}"/>
                                                     </c:if>
                                                     <c:if test="${(inputParameterName == 'reportParameter_Additional Information')}">
                                                                <s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:50px" value=" "/>
                                                     </c:if>
                                                     <c:if test="${(inputParameterName == 'reportParameter_Special Requirements')}">
                                                                <s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:50px" value=" "/>
                                                     </c:if>	
                                                     
                                                     
																							      													
											  		<c:if test="${inputParameterName != 'reportParameter_Service Order Number' && inputParameterName != 'reportParameter_Claim Number' && inputParameterName !='reportParameter_Book Number' && inputParameterName !='reportParameter_Work Ticket Number' && inputParameterName !='reportParameter_Customer File Number' && inputParameterName !='reportParameter_Documents' && inputParameterName !='reportParameter_DTD Service Charge' && inputParameterName !='reportParameter_Invoice Number' && inputParameterName !='reportParameter_Regnum' && inputParameterName !='reportParameter_Print Option' && inputParameterName !='reportParameter_Print_Option' && inputParameterName !='reportParameter_Print Options' && inputParameterName !='reportParameter_Print_Options' && inputParameterName !='reportParameter_Print_Method' && inputParameterName !='reportParameter_Send Option' && inputParameterName !='reportParameter_Bank' && inputParameterName !='reportParameter_Compare' && inputParameterName != 'reportParameter_Currency' && inputParameterName != 'reportParameter_Bill To' && inputParameterName != 'reportParameter_Partner Code' && inputParameterName !='reportParameter_Value Option' && inputParameterName !='reportParameter_Partner Name' && inputParameterName !='reportParameter_Predefined Text' && inputParameterName !='reportParameter_Remark' && inputParameterName !='reportParameter_Rates Exclude' && inputParameterName !='reportParameter_Rates Include' && inputParameterName !='reportParameter_Not Include' && inputParameterName !='reportParameter_Include' && inputParameterName !='reportParameter_Order Details' && inputParameterName !='reportParameter_ATTN'  && inputParameterName != 'reportParameter_Email' && inputParameterName !='reportParameter_Credit Terms'  && inputParameterName !='reportParameter_SO Container' && inputParameterName !='reportParameter_Quotation File Number' && inputParameterName !='reportParameter_Special Requirements' && inputParameterName !='reportParameter_Additional Information' && inputParameterName != 'reportParameter_Letter'}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Corporate ID')}">
												  			<s:textfield cssStyle="text-align:right" cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="<%=sessionCorpID %>" readonly="true" size="15" />
												  		</c:if>
												  		<c:if test="${!(inputParameterName == 'reportParameter_Corporate ID')}">
													  		<s:textfield  cssStyle="text-align:right"cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value=""/>
													  	</c:if>
													</c:if>
											  	</c:if>
											  	</c:if>
											 </td>
											</c:if>
										</tr> 
									  <tr><td class="listwhitetext" style="height:10px"></td></tr>  
						           </c:forEach>
						           
						           </tbody>
						           </table>
						           <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
						           <thead>
						           <tr>
							           <td align="left"  colspan="2" style="padding-left:10px">
							           		<s:textarea name="reports.reportComment"  onkeypress="return imposeMaxLength(this,1000);" rows="4" cols="70" readonly="true" cssClass="textarea"/>
							           </td>
						           </tr>
									<tr><td class="listwhitetext" style="height:10px">
									
									</td></tr>  
									<tr><td><b>&nbsp;&nbsp;Output Format : </b></td>&nbsp;
									<td class="listwhitetext" align="right" style="height:10px"><input size="25" type="button" value="Print With" style="width:105px;"  alt="CSV File" class="attach" onclick="return mergeReporPdf('PDF');"/></td></tr> 
									<tr><td class="listwhitetext" style="height:10px"></td>
									</tr> 
									<tr> 
						
									
									<td colspan="2" class="listwhitetext" >&nbsp;&nbsp;
									<c:if test="${(reports.csv == true)}">
									<img alt="CSV File" src="<c:url value='/images/csv1.gif'/>" onclick="return validatefields('CSV');"/><a href="#" onClick="return validatefields('CSV');"> CSV </a>
									</c:if>
									<c:if test="${(reports.xls == true)}">
									&nbsp;&nbsp;<img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validatefields('XLS');"/><a href="#" onClick="return validatefields('XLS');"> XLS </a>
									</c:if>
									<c:if test="${(reports.html == true)}">
									&nbsp;&nbsp;<img alt="HTML File" src="<c:url value='/images/html1.gif'/>" onclick="return validatefields('HTML');"/><a href="#" onClick="return validatefields('HTML');"> HTML</a>
									</c:if>
									<c:if test="${(reports.pdf == true)}">
									&nbsp;&nbsp;<img alt="PDF File" src="<c:url value='/images/pdf1.gif'/>" onclick="return validatefields('PDF');"/> <a href="#" onClick="return validatefields('PDF');"> PDF </a>
									</c:if>
									<c:if test="${(reports.rtf == true)}">
									&nbsp;&nbsp;<img alt="RTF File" src="<c:url value='/images/doc.gif'/>" onclick="return validatefields('RTF');"/> <a href="#" onClick="return validatefields('RTF');"> DOC </a>
									</c:if>
									<c:if test="${(reports.extract == true)}">
									&nbsp;&nbsp;<img alt="EXTRACT" src="<c:url value='/images/extract-para.png'/>" onclick="return validatefields('EXTRACT');"/> <a href="#" onClick="return validatefields('EXTRACT');">EXTRACT </a>
									</c:if>
									<c:if test="${(reports.docx == true)}">
									&nbsp;&nbsp;<img alt="Docx File" src="<c:url value='/images/docx.gif'/>" onclick="return validatefields('DOCX');"/> <a href="#" onClick="return validatefields('DOCX');"> DOCX </a>
									</c:if>
									</td></tr>
							</table>
						</td>	
		  			</tr>
		  		</tbody>
			</table>
            <table cellspacing="0" cellpadding="0">
                  <tr><td>Note: Use % as wildcard in parameter section.</td></tr>
            </table>
            <c:if test="${usertype!='DRIVER'}">
			<div id="docs" class="mainDetailTable" style="width:510px;">
			
				<table id="docs" class="colored_bg" cellspacing="0" cellpadding="0" border="0" style="width:510px;margin: 0px;padding: 0px;">
				<tbody>				
					<tr><td colspan="4"><div class="subcontent-tab">Enter information below to file this report in the File Cabinet</div></td></tr>  
									
									<tr ><td class="listwhitetext" style="height:10px;"></td></tr>  
									<tr>
										<td align="right" class="listwhitetext">Document Type : &nbsp;</td>
										<td align="left" width="150px"><s:select name="docFileType" cssClass="list-menu" list="%{docsList}" headerKey="" headerValue="" cssStyle="width:155px" onclick="setDescriptionDuplicate(this),documentMap(this);" onchange="setDocCat1(this);"/></td>
										<td align="right" class="listwhitetext" width="100px">Map&nbsp;&nbsp;<s:select name="mapFolder" cssClass="list-menu" list="%{mapList}" headerKey="" headerValue="" cssStyle="width:70px" /></td>
									</tr>
									<tr>
						<td align="right" class="listwhitetext" style="width:110px;" height="30px" >Document&nbsp;Category:&nbsp;<!-- <font color="red" size="2">*</font> --></td>
							<td colspan="2"><s:textfield cssClass="input-text" name="documentCategory" value = "${documentCategory}" cssStyle="width:270px" size="25" readonly="true"/></td>
						</tr>
									<tr>
										<td align="left" class="listwhitetext" style="height:0px"></td>
									</tr>
									<tr>
										<td align="right" class="listwhitetext" style="width:110px" >Description : &nbsp;</td>
										<td align="left" colspan="2"><s:textfield name="myFile.description" value="" cssClass="input-text" size="50"/></td>
										<td align="right" style="padding-right:20px;"><s:submit name="upload" key="button.upload" action="uploadForm"  cssClass="cssbutton" cssStyle="width:70px; height:25px" onclick="return validateFields1();"/></td>
									</tr>
									<tr>
										<td align="left" class="listwhitetext" style="height:10px"></td>
									</tr>
									<tr>
										<td align="left" class="listwhitetext" style="height:10px"></td>
									</tr>
								
								</tbody>
					</table> 		
			</div>
			</c:if>


	<table>
		<tbody>
			  
			<tr height="10px;"><td align="right" style="padding-left: 370px;"><strong><a href="#" onClick="return refreshParent();"><font style="color: #EB8234;font-size:11px;"><u>Close this window</u></font></a></strong></td></tr>  		
	  	</tbody>
	</table>
	<s:hidden name="fileType" />
	<div id="mydiv" style="position:absolute"></div>
</s:form>
<script type="text/javascript"> 
	try
	{
	displayStatusReason();
	}
	catch(e){}
</script>
</body>
<script type="text/javascript">   
    Form.focusFirstElement($("reportForm"));  
</script>
 <script type="text/javascript">
 	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
 </script>
