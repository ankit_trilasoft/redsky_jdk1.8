package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.SubcontractorChargesDao; 
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.DtoAccountInvoicePost;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.SubcontractorCharges; 

 

public class SubcontractorChargesDoaHibernate extends GenericDaoHibernate<SubcontractorCharges,Long> implements SubcontractorChargesDao{

	public SubcontractorChargesDoaHibernate() {   
        super(SubcontractorCharges.class);   
    } 
private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}

	public List findMaxId() {
		return getHibernateTemplate().find("select max(id) from SubcontractorCharges ");
	}

	public List<SubcontractorCharges> findAllRecords() {
	 
		 return getHibernateTemplate().find("from SubcontractorCharges");
	  
	}

	public List<SubcontractorCharges> findById(Long id) {
		
		return getHibernateTemplate().find("from SubcontractorCharges where id=?", id);
	}  
	
	  public List<SubcontractorCharges> registrationNo(String regNo)
	  {
		 // System.out.println(getHibernateTemplate().find("select lastName from Partner where partnerCode=?",partnerCode));
		  return getHibernateTemplate().find("select registrationNumber from ServiceOrder where registrationNumber=?",regNo);
	  }
	  
	  public List findServiceOrder(String regNo){
		  return getHibernateTemplate().find("from ServiceOrder where registrationNumber=?",regNo);
		  
	  }
	  public List findRegNo(String serviceOrder){
		  return getHibernateTemplate().find("select concat(registrationNumber,'~', shipNumber) from ServiceOrder where shipNumber=?",serviceOrder);
		  
	  }

	public List findBySysDefault(String corpID) {
		
		return getHibernateTemplate().find("select postDate1 from SystemDefault where postDate1 is not null and corpID=?", corpID);
	}

	public List findBranchList(String corpID) {
		
		return getHibernateTemplate().find("select vanLineCode from CompanyDivision where vanLineCode<>'' and corpID=?", corpID);
	}

	public List getsubcontrCompanyCode(String corpID) {
		return getHibernateTemplate().find("select distinct companyCode from CompanyDivision where closeddivision is false and  corpID=?", corpID);
		
	}
	public  class DTOForSubContcExtract{
		private Object amount; 
		private Object personId;
		private Object regNumber;
		private Object approved;
		private Object accountCrossReference;
		private Object glCode;
		private Object subId;
		private Object serviceOrder;
		private Object accountingCode;
		private Object description;
		private Object job;
		private Object post;
		private Object division;
		private Object companyDivision;
		 
		public Object getAmount() {
			return amount;
		}
		public void setAmount(Object amount) {
			this.amount = amount;
		}
		public Object getPersonId() {
			return personId;
		}
		public void setPersonId(Object personId) {
			this.personId = personId;
		}
		public Object getRegNumber() {
			return regNumber;
		}
		public void setRegNumber(Object regNumber) {
			this.regNumber = regNumber;
		}
		public Object getApproved() {
			return approved;
		}
		public void setApproved(Object approved) {
			this.approved = approved;
		}
		public Object getAccountCrossReference() {
			return accountCrossReference;
		}
		public void setAccountCrossReference(Object accountCrossReference) {
			this.accountCrossReference = accountCrossReference;
		}
		public Object getGlCode() {
			return glCode;
		}
		public void setGlCode(Object glCode) {
			this.glCode = glCode;
		}
		public Object getSubId() {
			return subId;
		}
		public void setSubId(Object subId) {
			this.subId = subId;
		}
		public Object getServiceOrder() {
			return serviceOrder;
		}
		public void setServiceOrder(Object serviceOrder) {
			this.serviceOrder = serviceOrder;
		}
		public Object getAccountingCode() {
			return accountingCode;
		}
		public void setAccountingCode(Object accountingCode) {
			this.accountingCode = accountingCode;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getJob() {
			return job;
		}
		public void setJob(Object job) {
			this.job = job;
		}
		public Object getPost() {
			return post;
		}
		public void setPost(Object post) {
			this.post = post;
		}
		public Object getDivision() {
			return division;
		}
		public void setDivision(Object division) {
			this.division = division;
		}
		public Object getCompanyDivision() {
			return companyDivision;
		}
		public void setCompanyDivision(Object companyDivision) {
			this.companyDivision = companyDivision;
		} 
	} 
	
	
	public  class DTOForSubContcExtractLevel1
	{
		private Object sumamount;
		private Object accountCrossReference;
		private Object regNumber;
		private Object approved;
		public Object getAccountCrossReference() {
			return accountCrossReference;
		}
		public void setAccountCrossReference(Object accountCrossReference) {
			this.accountCrossReference = accountCrossReference;
		}
	
		public Object getApproved() {
			return approved;
		}
		public void setApproved(Object approved) {
			this.approved = approved;
		}
		public Object getRegNumber() {
			return regNumber;
		}
		public void setRegNumber(Object regNumber) {
			this.regNumber = regNumber;
		}
		public Object getSumamount() {
			return sumamount;
		}
		public void setSumamount(Object sumamount) {
			this.sumamount = sumamount;
		}
	}
	public  class DTOForSubContcExtractLevel4{
		private Object amount; 
		private Object regNumber;
		private Object glCode;
		public Object getAmount() {
			return amount;
		}
		public void setAmount(Object amount) {
			this.amount = amount;
		}
		public Object getGlCode() {
			return glCode;
		}
		public void setGlCode(Object glCode) {
			this.glCode = glCode;
		}
		public Object getRegNumber() {
			return regNumber;
		}
		public void setRegNumber(Object regNumber) {
			this.regNumber = regNumber;
		}
		
	}
	
	public List getSubContcExtract(String subcontrCompanyCode,String corpid, String postDate, String startInvoiceDates, String endInvoiceDates) {
		List <Object> subContcExtractList = new ArrayList<Object>(); 
		
		  List list= this.getSession().createSQLQuery("Select s.amount ,s.personId ,s.regNumber, s.approved ,if(pa.accountCrossReference is null,'',pa.accountCrossReference),s.glCode,s.id,s.serviceOrder,(replace(replace(replace((s.description),'\r\n',''),'\t',''),'\"','  '))description,s.job,s.post,s.division  from subcontractorcharges s  INNER JOIN partneraccountref pa on s.personId=pa.partnercode and pa.corpID='"+corpid+"' and (pa.refType='P' or pa.refType is null) and pa.accountCrossReference !='temp' and  pa.companyDivision='"+subcontrCompanyCode+"'   Where  s.corpID='"+corpid+"' and s.amount != 0 and (s.balanceForward=false or s.balanceForward is null) and s.personId<>''  and s.companyDivision='"+subcontrCompanyCode+"' and s.approved is not null and (DATE_FORMAT(s.approved,'%Y-%m-%d') >=  ('"+startInvoiceDates+" ')) and (DATE_FORMAT(s.approved,'%Y-%m-%d') <=  ('"+endInvoiceDates+"')) and s.accountSent is null and s.post is not null and s.post='"+postDate+"'")
		  .addScalar("s.amount", Hibernate.BIG_DECIMAL)
		  .addScalar("s.personId", Hibernate.STRING)  
		  .addScalar("s.regNumber", Hibernate.STRING) 
		  .addScalar("s.approved", Hibernate.DATE)
		  .addScalar("if(pa.accountCrossReference is null,'',pa.accountCrossReference)", Hibernate.STRING) 
		  .addScalar("s.glCode", Hibernate.STRING) 
		  .addScalar("s.id", Hibernate.LONG)
		  .addScalar("s.serviceOrder", Hibernate.STRING)  
		  .addScalar("description", Hibernate.STRING)
		  .addScalar("s.job", Hibernate.STRING) 
		  .addScalar("s.post", Hibernate.DATE)
		  .addScalar("s.division", Hibernate.STRING) 
		  .list();
		  Iterator it=list.iterator();
		  DTOForSubContcExtract dtoSubContcExtract=null;
		  while(it.hasNext())  
		  {
			  Object [] row=(Object[])it.next();
			  dtoSubContcExtract=new DTOForSubContcExtract(); 
			  dtoSubContcExtract.setAmount(row[0]);
			  dtoSubContcExtract.setPersonId(row[1]);
			  dtoSubContcExtract.setRegNumber(row[2]);
			  dtoSubContcExtract.setApproved(row[3]);
			  dtoSubContcExtract.setAccountCrossReference(row[4]);
			  dtoSubContcExtract.setGlCode(row[5]);
			  dtoSubContcExtract.setSubId(row[6]);
			  dtoSubContcExtract.setServiceOrder(row[7]); 
			  dtoSubContcExtract.setDescription(row[8]); 
			  dtoSubContcExtract.setJob(row[9]);
			  dtoSubContcExtract.setPost(row[10]);
			  dtoSubContcExtract.setDivision(row[11]) ;
			  
            subContcExtractList.add(dtoSubContcExtract);
			  
		  } 
		  return subContcExtractList;
	}

	public List getSubContcExtractlevel1(String personId,String corpid ,String subcontrCompanyCode) {
		
		List <Object> subContcExtractList = new ArrayList<Object>(); 
		
		  List list= this.getSession().createSQLQuery("select sum(s.amount),s.regNumber, s.approved ,if(pa.accountCrossReference is null,'',pa.accountCrossReference) from subcontractorcharges s,partner p,companydivision c,partneraccountref pa Where s.branch = c.vanLineCode and c.companyCode = '"+subcontrCompanyCode+"' and c.corpID='"+corpid+"' and s.amount > 0 and s.amount !=0 and (s.balanceForward=false or s.balanceForward is null) and p.partnerCode=s.personId and s.personId='"+personId+"' and s.corpID='"+corpid+"' and s.approved is not null and s.accountSent is null and s.post is not null and pa.partnercode=p.partnercode and pa.corpID IN ('"+corpid+"','"+hibernateUtil.getParentCorpID(corpid)+"') and c.companycode = pa.companydivision group by s.personId")
		  
		  .addScalar("sum(s.amount)", Hibernate.BIG_DECIMAL)
		  .addScalar("s.regNumber", Hibernate.STRING) 
		  .addScalar("s.approved", Hibernate.DATE)
		  .addScalar("if(pa.accountCrossReference is null,'',pa.accountCrossReference)", Hibernate.STRING) 
		  .list(); 
		 Iterator it=list.iterator();
		 DTOForSubContcExtractLevel1 dtoForSubContcExtractLevel1=null;
		  while(it.hasNext()) 
		  {
			  Object [] row=(Object[])it.next();
			  dtoForSubContcExtractLevel1=new DTOForSubContcExtractLevel1(); 
			  dtoForSubContcExtractLevel1.setSumamount(row[0]); 
			  dtoForSubContcExtractLevel1.setRegNumber(row[1]); 
			  dtoForSubContcExtractLevel1.setApproved(row[2]); 
			  dtoForSubContcExtractLevel1.setAccountCrossReference(row[3]); 
			  subContcExtractList.add(dtoForSubContcExtractLevel1);
			  
		  } 
		  return subContcExtractList;
	}

	public List findGlCodeForExtract(String personId, String corpid) {
		
		return getHibernateTemplate().find("select glCode from SubcontractorCharges where personId='"+personId+"' and (balanceForward=false or balanceForward is null) and corpID='"+corpid+"'and amount <> 0 and approved is not null and accountSent is null and post is not null and glCode is not null");
	}

	public List getSubContcExtractlevel4(String personId,String corpid, String glCode,String subcontrCompanyCode) {
		
		List <Object> subContcExtractList = new ArrayList<Object>(); 
		
		  List list= this.getSession().createSQLQuery("select s.amount,s.regNumber,s.glCode from subcontractorcharges s, companydivision c  Where s.branch = c.vanLineCode and c.companyCode = '"+subcontrCompanyCode+"' and c.corpID='"+corpid+"' and s.amount > 0 and s.amount !=0 and s.personId='"+personId+"'and (s.balanceForward=false or s.balanceForward is null) and s.glCode='"+glCode+"' and s.corpID='"+corpid+"' and s.approved is not null and s.accountSent is null and s.post is not null")
		  
		  .addScalar("s.amount", Hibernate.BIG_DECIMAL)
		  .addScalar("s.regNumber", Hibernate.STRING)  
		  .addScalar("s.glCode", Hibernate.STRING) 
		  .list(); 
		 Iterator it=list.iterator();
		 DTOForSubContcExtractLevel4 dtoForSubContcExtractLevel4=null;
		  while(it.hasNext()) 
		  {
			  Object [] row=(Object[])it.next();
			  dtoForSubContcExtractLevel4=new DTOForSubContcExtractLevel4(); 
			  dtoForSubContcExtractLevel4.setAmount(row[0]); 
			  dtoForSubContcExtractLevel4.setRegNumber(row[1]); 
			  dtoForSubContcExtractLevel4.setGlCode(row[2]);  
			  subContcExtractList.add(dtoForSubContcExtractLevel4);
			  
		  } 
		  return subContcExtractList;
	}

	public List getSubContcNivExtractlevel1(String personId ,String corpid,String subcontrCompanyCode) {
		List <Object> subContcExtractList = new ArrayList<Object>(); 
		
		  List list= this.getSession().createSQLQuery("select sum(s.amount),s.regNumber, s.approved ,if(pa.accountCrossReference is null,'',pa.accountCrossReference) from subcontractorcharges s,partner p,companydivision c,partneraccountref pa Where s.branch = c.vanLineCode and c.companyCode = '"+subcontrCompanyCode+"' and c.corpID='"+corpid+"' and s.amount < 0 and s.amount !=0 and (s.balanceForward=false or s.balanceForward is null) and p.partnerCode=s.personId and s.personId='"+personId+"' and s.corpID='"+corpid+"' and s.approved is not null and s.accountSent is null and s.post is not null and pa.partnercode=p.partnercode and pa.corpID IN ('"+corpid+"') and c.companycode = pa.companydivision group by s.personId")
		  
		  .addScalar("sum(s.amount)", Hibernate.BIG_DECIMAL)
		  .addScalar("s.regNumber", Hibernate.STRING) 
		  .addScalar("s.approved", Hibernate.DATE)
		  .addScalar("if(pa.accountCrossReference is null,'',pa.accountCrossReference)", Hibernate.STRING) 
		  .list(); 
		 Iterator it=list.iterator();
		 DTOForSubContcExtractLevel1 dtoForSubContcExtractLevel1=null;
		  while(it.hasNext()) 
		  {
			  Object [] row=(Object[])it.next();
			  dtoForSubContcExtractLevel1=new DTOForSubContcExtractLevel1(); 
			  dtoForSubContcExtractLevel1.setSumamount(row[0]); 
			  dtoForSubContcExtractLevel1.setRegNumber(row[1]); 
			  dtoForSubContcExtractLevel1.setApproved(row[2]); 
			  dtoForSubContcExtractLevel1.setAccountCrossReference(row[3]); 
			  subContcExtractList.add(dtoForSubContcExtractLevel1);
			  
		  } 
		  return subContcExtractList;
	}

	public List getSubContcNivExtractlevel4(String personId ,String corpid,String glCode,String subcontrCompanyCode) {
		List <Object> subContcExtractList = new ArrayList<Object>(); 
		
		  List list= this.getSession().createSQLQuery("select s.amount,s.regNumber,s.glCode from subcontractorcharges s, companydivision c  Where s.branch = c.vanLineCode and c.companyCode = '"+subcontrCompanyCode+"' and c.corpID='"+corpid+"' and s.amount < 0 and s.amount !=0 and s.personId='"+personId+"'and (s.balanceForward=false or s.balanceForward is null) and s.glCode='"+glCode+"' and s.corpID='"+corpid+"' and s.approved is not null and s.accountSent is null and s.post is not null")
		  
		  .addScalar("s.amount", Hibernate.BIG_DECIMAL)
		  .addScalar("s.regNumber", Hibernate.STRING)  
		  .addScalar("s.glCode", Hibernate.STRING) 
		  .list(); 
		 Iterator it=list.iterator();
		 DTOForSubContcExtractLevel4 dtoForSubContcExtractLevel4=null;
		  while(it.hasNext()) 
		  {
			  Object [] row=(Object[])it.next();
			  dtoForSubContcExtractLevel4=new DTOForSubContcExtractLevel4(); 
			  dtoForSubContcExtractLevel4.setAmount(row[0]); 
			  dtoForSubContcExtractLevel4.setRegNumber(row[1]); 
			  dtoForSubContcExtractLevel4.setGlCode(row[2]);  
			  subContcExtractList.add(dtoForSubContcExtractLevel4);
			  
		  } 
		  return subContcExtractList;
	}

	public int subContcFileUpdate(String batchName, String subContcPersonId,String corpid) {
		int i= getHibernateTemplate().bulkUpdate("update SubcontractorCharges set accountSent=now(),sentToAccountVia='"+batchName+"'  where amount <> 0 and personId='"+subContcPersonId+"'and corpID='"+corpid+"' and approved is not null and accountSent is null and post is not null");
		   //System.out.println("\n\n\n\n\n\nThe number of subcontractorcharges line "+i+" is updated");
		  return i; 
	}
	public  class DTONonSettleAmount{
		private Object amount; 
		private Object personId;
		private Object description;
		public Object getAmount() {
			return amount;
		}
		public void setAmount(Object amount) {
			this.amount = amount;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getPersonId() {
			return personId;
		}
		public void setPersonId(Object personId) {
			this.personId = personId;
		}
		
	}
	
	public List getNonsettledAmount(String subContcPersonId,String corpid){
		
		return getHibernateTemplate().find(" from SubcontractorCharges where personId='"+subContcPersonId+"' and corpID='"+corpid+"' and isSettled is not true order by id ");
		
	}
	public List findMiscellaneousStatus(String serviceOrder){
		return getHibernateTemplate().find("from Miscellaneous where shipNumber='"+serviceOrder+"' and status not in ('Ready To Transfer' , 'CLOSED') ");
	}

	public List<SubcontractorCharges> findAmountDesc(String serviceOrder, String corpId,String driver) {
		/*if("ACCOUNT".equals(driver)){
			List<Miscellaneous> mList = getHibernateTemplate().find("from Miscellaneous where shipNumber='"+serviceOrder+"' and corpId='"+corpId+"'");
			for (Miscellaneous miscellaneous : mList) {
				driver= miscellaneous.getDriverId();
			}
		}*/
//		String query = "from SubcontractorCharges where serviceOrder='"+serviceOrder+"' and corpId='"+corpId+"' and personId='"+driver+"' and description not like 'Std. Ded.%' and isSettled is not true";
		String query = "from SubcontractorCharges where serviceOrder='"+serviceOrder+"' and corpId='"+corpId+"' and description not like 'Std. Ded.%' and isSettled is not true";
		List<SubcontractorCharges> list = getHibernateTemplate().find(query);
		return list;
	}
	public  class DTOGPSubContcExtract
   {
		private Object amount; 
		private Object personId;
		private Object regNumber;
		private Object approved;
		private Object accountCrossReference;
		private Object glCode;
		private Object subId;
		private Object serviceOrder;
		private Object accountingCode;
		private Object description;
		private Object job;
		private Object post;
		private Object division;
		
		public Object getAmount() {
			return amount;
		}
		public void setAmount(Object amount) {
			this.amount = amount;
		}
		public Object getPersonId() {
			return personId;
		}
		public void setPersonId(Object personId) {
			this.personId = personId;
		}
		public Object getRegNumber() {
			return regNumber;
		}
		public void setRegNumber(Object regNumber) {
			this.regNumber = regNumber;
		}
		public Object getApproved() {
			return approved;
		}
		public void setApproved(Object approved) {
			this.approved = approved;
		}
		public Object getAccountCrossReference() {
			return accountCrossReference;
		}
		public void setAccountCrossReference(Object accountCrossReference) {
			this.accountCrossReference = accountCrossReference;
		}
		public Object getGlCode() {
			return glCode;
		}
		public void setGlCode(Object glCode) {
			this.glCode = glCode;
		}
		public Object getSubId() {
			return subId;
		}
		public void setSubId(Object subId) {
			this.subId = subId;
		}
		public Object getServiceOrder() {
			return serviceOrder;
		}
		public void setServiceOrder(Object serviceOrder) {
			this.serviceOrder = serviceOrder;
		}
		public Object getAccountingCode() {
			return accountingCode;
		}
		public void setAccountingCode(Object accountingCode) {
			this.accountingCode = accountingCode;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getJob() {
			return job;
		}
		public void setJob(Object job) {
			this.job = job;
		}
		public Object getPost() {
			return post;
		}
		public void setPost(Object post) {
			this.post = post;
		}
		public Object getDivision() {
			return division;
		}
		public void setDivision(Object division) {
			this.division = division;
		}
		 
		
   }
	
	public List getgpSubContcExtract(String sessionCorpID) {
		List <Object> subContcExtractList = new ArrayList<Object>(); 
		
		  List list= this.getSession().createSQLQuery("Select distinct s.amount ,s.personId ,s.regNumber, s.approved ,if(pa.accountCrossReference is null,'',pa.accountCrossReference),s.glCode,s.id,s.serviceOrder,c.accountingCode,(replace(replace(replace((s.description),'\r\n',''),'\t',''),'\"','  '))description,s.job,s.post,s.division  from subcontractorcharges s INNER JOIN companydivision c on s.companyDivision = c.companyCode  and c.corpID='"+sessionCorpID+"'  INNER JOIN partneraccountref pa on s.personId=pa.partnercode and pa.corpID='"+sessionCorpID+"' and (pa.refType='P' or pa.refType is null) Where  s.corpID='"+sessionCorpID+"' and s.amount != 0 and (s.balanceForward=false or s.balanceForward is null) and s.personId<>''  and s.approved is not null and s.accountSent is null and s.post is not null ")
		  .addScalar("s.amount", Hibernate.BIG_DECIMAL)
		  .addScalar("s.personId", Hibernate.STRING)  
		  .addScalar("s.regNumber", Hibernate.STRING) 
		  .addScalar("s.approved", Hibernate.DATE)
		  .addScalar("if(pa.accountCrossReference is null,'',pa.accountCrossReference)", Hibernate.STRING) 
		  .addScalar("s.glCode", Hibernate.STRING) 
		  .addScalar("s.id", Hibernate.LONG)
		  .addScalar("s.serviceOrder", Hibernate.STRING) 
		  .addScalar("c.accountingCode", Hibernate.STRING) 
		  .addScalar("description", Hibernate.STRING)
		  .addScalar("s.job", Hibernate.STRING) 
		  .addScalar("s.post", Hibernate.DATE)
		  .addScalar("s.division", Hibernate.STRING) 
		  .list();
		  Iterator it=list.iterator();
		  DTOGPSubContcExtract dTOGPSubContcExtract=null;
		  while(it.hasNext())  
		  {
			  Object [] row=(Object[])it.next();
			  dTOGPSubContcExtract=new DTOGPSubContcExtract(); 
			  dTOGPSubContcExtract.setAmount(row[0]);
			  dTOGPSubContcExtract.setPersonId(row[1]);
			  dTOGPSubContcExtract.setRegNumber(row[2]);
			  dTOGPSubContcExtract.setApproved(row[3]);
			  dTOGPSubContcExtract.setAccountCrossReference(row[4]);
			  dTOGPSubContcExtract.setGlCode(row[5]);
			  dTOGPSubContcExtract.setSubId(row[6]);
			  dTOGPSubContcExtract.setServiceOrder(row[7]);
			  dTOGPSubContcExtract.setAccountingCode(row[8]);
			  dTOGPSubContcExtract.setDescription(row[9]); 
			  dTOGPSubContcExtract.setJob(row[10]);
			  dTOGPSubContcExtract.setPost(row[11]);
			  dTOGPSubContcExtract.setDivision(row[12]) ;
			  
              subContcExtractList.add(dTOGPSubContcExtract);
			  
		  } 
		  return subContcExtractList;
	}

	public int gpSubContcFileUpdate(String batchName, String subIdList, String sessionCorpID) {
		String s="update SubcontractorCharges set accountSent=now(),sentToAccountVia='"+batchName+"'  where amount <> 0 and id in  ("+subIdList+") and corpID='"+sessionCorpID+"' and approved is not null and accountSent is null and post is not null";
		int i= getHibernateTemplate().bulkUpdate("update SubcontractorCharges set accountSent=now(),sentToAccountVia='"+batchName+"'  where amount <> 0 and id in  ("+subIdList+") and corpID='"+sessionCorpID+"' and approved is not null and accountSent is null and post is not null");
		   //System.out.println("\n\n\n\n\n\nThe number of subcontractorcharges line "+i+" is updated");
		  return i; 
	}	
	public List getCompanyCode(String agentCode, String sessionCorpID){
		List list = getHibernateTemplate().find("from CompanyDivision where vanLineCode='" + agentCode +"'");
		return list;
	}
    public List getAccountInvoicePostDataList(String corpId){
    	List subcontractorPostDateList=new ArrayList();
    	List list= this.getSession().createSQLQuery("Select s.post as recpost,count(s.id) as accID ,if(s.companyDivision is null,'',s.companyDivision)  as compDiv from subcontractorcharges s  INNER JOIN partneraccountref pa on s.personId=pa.partnercode and pa.corpID='"+corpId+"' and (pa.refType='P' or pa.refType is null) and pa.accountCrossReference !='temp' Where  s.corpID='"+corpId+"' and s.amount != 0 and (s.balanceForward=false or s.balanceForward is null) and s.personId<>'' and s.approved is not null and s.accountSent is null and s.post is not null group by  s.post,compDiv").list();
    		Iterator it=list.iterator();
    		while(it.hasNext()){
    			Object []row= (Object [])it.next();
    			DtoAccountInvoicePost dtoAccountInvoicePost = new DtoAccountInvoicePost(); 
    			dtoAccountInvoicePost.setRecPostDate((Date)(row[0]));
    			dtoAccountInvoicePost.setRecPostDateCount(row[1].toString());
    			dtoAccountInvoicePost.setCompanyDivision((row[2]==null?"":row[2].toString()));
    			subcontractorPostDateList.add(dtoAccountInvoicePost);
    		}
    		return subcontractorPostDateList;
    }
	public List getSubContcExtractHoll(String subcontrCompanyCode, String corpid,String postDate) {
		List <Object> subContcExtractList = new ArrayList<Object>(); 
		String query ="";
		if(!subcontrCompanyCode.equalsIgnoreCase("")){
			query ="Select s.amount ,s.personId ,s.regNumber, s.approved ,if(pa.accountCrossReference is null,'',pa.accountCrossReference),s.glCode,s.id,s.serviceOrder,(replace(replace(replace((s.description),'\r\n',''),'\t',''),'\"','  '))description,s.job,s.post,s.division  from subcontractorcharges s  INNER JOIN partneraccountref pa on s.personId=pa.partnercode and pa.corpID='"+corpid+"' and (pa.refType='P' or pa.refType is null) and pa.accountCrossReference !='temp'   Where  s.corpID='"+corpid+"' and s.amount != 0 and (s.balanceForward=false or s.balanceForward is null) and s.personId<>''  and s.companyDivision='"+subcontrCompanyCode+"' and s.approved is not null and s.accountSent is null and s.post is not null and s.post='"+postDate+"'";	
		}else{
			query ="Select s.amount ,s.personId ,s.regNumber, s.approved ,if(pa.accountCrossReference is null,'',pa.accountCrossReference),s.glCode,s.id,s.serviceOrder,(replace(replace(replace((s.description),'\r\n',''),'\t',''),'\"','  '))description,s.job,s.post,s.division  from subcontractorcharges s  INNER JOIN partneraccountref pa on s.personId=pa.partnercode and pa.corpID='"+corpid+"' and (pa.refType='P' or pa.refType is null) and pa.accountCrossReference !='temp'   Where  s.corpID='"+corpid+"' and s.amount != 0 and (s.balanceForward=false or s.balanceForward is null) and s.personId<>''  and (s.companyDivision='' or s.companyDivision is null) and s.approved is not null and s.accountSent is null and s.post is not null and s.post='"+postDate+"'";
		}
	
		  List list= this.getSession().createSQLQuery(query)
		  .addScalar("s.amount", Hibernate.BIG_DECIMAL)
		  .addScalar("s.personId", Hibernate.STRING)  
		  .addScalar("s.regNumber", Hibernate.STRING) 
		  .addScalar("s.approved", Hibernate.DATE)
		  .addScalar("if(pa.accountCrossReference is null,'',pa.accountCrossReference)", Hibernate.STRING) 
		  .addScalar("s.glCode", Hibernate.STRING) 
		  .addScalar("s.id", Hibernate.LONG)
		  .addScalar("s.serviceOrder", Hibernate.STRING)  
		  .addScalar("description", Hibernate.STRING)
		  .addScalar("s.job", Hibernate.STRING) 
		  .addScalar("s.post", Hibernate.DATE)
		  .addScalar("s.division", Hibernate.STRING) 
		  .list();
		  Iterator it=list.iterator();
		  DTOForSubContcExtract dtoSubContcExtract=null;
		  while(it.hasNext())  
		  {
			  Object [] row=(Object[])it.next();
			  dtoSubContcExtract=new DTOForSubContcExtract(); 
			  dtoSubContcExtract.setAmount(row[0]);
			  dtoSubContcExtract.setPersonId(row[1]);
			  dtoSubContcExtract.setRegNumber(row[2]);
			  dtoSubContcExtract.setApproved(row[3]);
			  dtoSubContcExtract.setAccountCrossReference(row[4]);
			  dtoSubContcExtract.setGlCode(row[5]);
			  dtoSubContcExtract.setSubId(row[6]);
			  dtoSubContcExtract.setServiceOrder(row[7]); 
			  dtoSubContcExtract.setDescription(row[8]); 
			  dtoSubContcExtract.setJob(row[9]);
			  dtoSubContcExtract.setPost(row[10]);
			  dtoSubContcExtract.setDivision(row[11]) ;
			  
          subContcExtractList.add(dtoSubContcExtract);
			  
		  } 
		  return subContcExtractList;
	}

	public List getSubcontrCompanyCodeDataList(String sessionCorpID) {
    	
    	List subcontractorPostDateList=new ArrayList();
    	List list= this.getSession().createSQLQuery("Select s.post as recpost,count(s.id) as accID ,if(s.companyDivision is null,'',s.companyDivision)  as compDiv from subcontractorcharges s  INNER JOIN partneraccountref pa on s.personId=pa.partnercode and pa.corpID='"+sessionCorpID+"' and (pa.refType='P' or pa.refType is null) and pa.accountCrossReference !='temp' and  pa.companyDivision=s.companyDivision Where  s.corpID='"+sessionCorpID+"' and s.amount != 0 and (s.balanceForward=false or s.balanceForward is null) and s.personId<>'' and s.companyDivision is not null and s.companyDivision !=''  and s.approved is not null and s.accountSent is null and s.post is not null group by  s.post, compDiv ").list();
    	Iterator it=list.iterator();
    		while(it.hasNext()){
    			Object []row= (Object [])it.next();
    			DtoAccountInvoicePost dtoAccountInvoicePost = new DtoAccountInvoicePost(); 
    			dtoAccountInvoicePost.setRecPostDate((Date)(row[0]));
    			dtoAccountInvoicePost.setRecPostDateCount(row[1].toString());
    			dtoAccountInvoicePost.setCompanyDivision((row[2]==null?"":row[2].toString()));
    			subcontractorPostDateList.add(dtoAccountInvoicePost);
    		}
    		return subcontractorPostDateList;
    }
	public  class DTOSubcontrACCCompanyCodeDataList
	   {
		private Date recPostDate; 
		private String invoiceDate;
		private String invoiceDateEnd;
		private String companyDivision;
		private String recPostDateCount;
		private String companyDivisionGroup;
		public Date getRecPostDate() {
			return recPostDate;
		}
		public void setRecPostDate(Date recPostDate) {
			this.recPostDate = recPostDate;
		}
		
		public String getInvoiceDate() {
			return invoiceDate;
		}
		public void setInvoiceDate(String invoiceDate) {
			this.invoiceDate = invoiceDate;
		}
		public String getInvoiceDateEnd() {
			return invoiceDateEnd;
		}
		public void setInvoiceDateEnd(String invoiceDateEnd) {
			this.invoiceDateEnd = invoiceDateEnd;
		}
		public String getCompanyDivision() {
			return companyDivision;
		}
		public void setCompanyDivision(String companyDivision) {
			this.companyDivision = companyDivision;
		}
		public String getRecPostDateCount() {
			return recPostDateCount;
		}
		public void setRecPostDateCount(String recPostDateCount) {
			this.recPostDateCount = recPostDateCount;
		}
		public String getCompanyDivisionGroup() {
			return companyDivisionGroup;
		}
		public void setCompanyDivisionGroup(String companyDivisionGroup) {
			this.companyDivisionGroup = companyDivisionGroup;
		}
	   }

	public List getSubcontrACCCompanyCodeDataList(String sessionCorpID) {
    	
    	List subcontractorPostDateList=new ArrayList();
    	List list= this.getSession().createSQLQuery("(Select s.post ,DATE_FORMAT(DATE_ADD(DATE_ADD(s.approved, INTERVAL(1-DAYOFWEEK(s.approved)) DAY),INTERVAL if(DAYOFWEEK(s.approved)>5,'5','-2' ) day),'%Y-%m-%d'),DATE_FORMAT(DATE_ADD(DATE_ADD(s.approved, INTERVAL(7-DAYOFWEEK(s.approved )) DAY),INTERVAL if(DAYOFWEEK(s.approved)>5,'5','-2' ) day),'%Y-%m-%d'),s.companyDivision,'subcontractorcharges',s.id from subcontractorcharges s  INNER JOIN partneraccountref pa on s.personId=pa.partnercode and pa.corpID='"+sessionCorpID+"' and (pa.refType='P' or pa.refType is null) and pa.accountCrossReference !='temp' and  pa.companyDivision=s.companyDivision Where  s.corpID='"+sessionCorpID+"' and s.amount != 0 and (s.balanceForward=false or s.balanceForward is null) and s.personId<>'' and s.companyDivision is not null and s.companyDivision !=''  and s.approved is not null and s.accountSent is null and s.post is not null) UNION (select a.paypostdate, DATE_FORMAT(DATE_ADD(DATE_ADD(a.invoiceDate , INTERVAL(1-DAYOFWEEK(a.invoiceDate)) DAY),INTERVAL if(DAYOFWEEK(a.invoiceDate)>5,'5','-2' ) day),'%Y-%m-%d'),DATE_FORMAT(DATE_ADD(DATE_ADD(a.invoiceDate, INTERVAL(7-DAYOFWEEK(a.invoiceDate)) DAY),INTERVAL if(DAYOFWEEK(a.invoiceDate)>5,'5','-2' ) day),'%Y-%m-%d'),a.companyDivision,'accountline',a.id from accountline a,partnerpublic p  where a.corpid='"+sessionCorpID+"' and a.actualexpense <> 0 and a.payingstatus = 'A' and (a.vendorcode is not null or a.vendorcode <> '') and (a.payGl is not null or a.payGl <> '') and a.updatedOn >= '2008-12-31' and a.status=true and  a.vendorcode=p.partnerCode and p.isOwnerOp = true and p.corpid in ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','TSFT') and a.actgCode<>'' and a.actgCode<>'Temp'    and (a.invoiceNumber<>'' or a.invoiceNumber is not null) and invoiceDate is not null and a.payPostDate is not null and a.payAccDate is null) order by 2 desc").list();
    	Iterator it=list.iterator();
    		while(it.hasNext()){
    			
    			Object []row= (Object [])it.next();
    			DTOSubcontrACCCompanyCodeDataList dtoAccountInvoicePost = new DTOSubcontrACCCompanyCodeDataList(); 
    			try{
    			dtoAccountInvoicePost.setRecPostDate((Date)(row[0]));
    			dtoAccountInvoicePost.setInvoiceDate((row[1].toString()));
    			dtoAccountInvoicePost.setInvoiceDateEnd((row[2].toString()));
    			dtoAccountInvoicePost.setCompanyDivision((row[3]==null?"":row[3].toString()));
    			}catch(Exception e){
    				
    			}
    			subcontractorPostDateList.add(dtoAccountInvoicePost);
    		}
    		return subcontractorPostDateList;
    }

	public List getMergeSubcontrACCCompanyCodeDataList(String sessionCorpID) {
    	
    	List subcontractorPostDateList=new ArrayList();
    	List list= this.getSession().createSQLQuery("(Select s.post ,DATE_FORMAT(DATE_ADD(DATE_ADD(s.approved, INTERVAL(1-DAYOFWEEK(s.approved)) DAY),INTERVAL if(DAYOFWEEK(s.approved)>5,'5','-2' ) day),'%Y-%m-%d'),DATE_FORMAT(DATE_ADD(DATE_ADD(s.approved, INTERVAL(7-DAYOFWEEK(s.approved )) DAY),INTERVAL if(DAYOFWEEK(s.approved)>5,'5','-2' ) day),'%Y-%m-%d'),s.companyDivision,'subcontractorcharges',s.id,if(s.companyDivision in ('SGL','FFG'),'A',if(s.companyDivision in ('SSC','DUL','FOR','LAU','LAM','GAI','BAL','INT','WAS','ALE'),'B',if(s.companyDivision in ('BOL','COU','COM'),'C',if(s.companyDivision in ('FMG'),'D',if(s.companyDivision in ('DFC','DFC'),'E',if(s.companyDivision in ('ALP'),'F',if(s.companyDivision in ('DTS'),'G','H'))))))) as compDiv  from subcontractorcharges s  INNER JOIN partneraccountref pa on s.personId=pa.partnercode and pa.corpID='"+sessionCorpID+"' and (pa.refType='P' or pa.refType is null) and pa.accountCrossReference !='temp' and  pa.companyDivision=s.companyDivision Where  s.corpID='"+sessionCorpID+"' and s.companyDivision is not null and s.companyDivision <>'' and s.amount != 0 and (s.balanceForward=false or s.balanceForward is null) and s.personId<>'' and s.companyDivision is not null and s.companyDivision !=''  and s.approved is not null and s.accountSent is null and s.post is not null) UNION (select a.paypostdate, DATE_FORMAT(DATE_ADD(DATE_ADD(a.invoiceDate , INTERVAL(1-DAYOFWEEK(a.invoiceDate)) DAY),INTERVAL if(DAYOFWEEK(a.invoiceDate)>5,'5','-2' ) day),'%Y-%m-%d'),DATE_FORMAT(DATE_ADD(DATE_ADD(a.invoiceDate, INTERVAL(7-DAYOFWEEK(a.invoiceDate)) DAY),INTERVAL if(DAYOFWEEK(a.invoiceDate)>5,'5','-2' ) day),'%Y-%m-%d'),a.companyDivision,'accountline',a.id, if(a.companyDivision in ('SGL','FFG'),'A',if(a.companyDivision in ('SSC','DUL','FOR','LAU','LAM','GAI','BAL','INT','WAS','ALE'),'B',if(a.companyDivision in ('BOL','COU','COM'),'C',if(a.companyDivision in ('FMG'),'D',if(a.companyDivision in ('DFC','DFC'),'E',if(a.companyDivision in ('ALP'),'F',if(a.companyDivision in ('DTS'),'G','H'))))))) as compDiv  from accountline a,partnerpublic p  where a.corpid='"+sessionCorpID+"' and a.companyDivision is not null and a.companyDivision <>'' and a.actualexpense <> 0 and a.payingstatus = 'A' and (a.vendorcode is not null or a.vendorcode <> '') and (a.payGl is not null or a.payGl <> '') and a.updatedOn >= '2008-12-31' and a.status=true and  a.vendorcode=p.partnerCode and p.isOwnerOp = true and p.corpid in ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','TSFT') and a.actgCode<>'' and a.actgCode<>'Temp'    and (a.invoiceNumber<>'' or a.invoiceNumber is not null) and invoiceDate is not null and a.payPostDate is not null and a.payAccDate is null) order by 2 desc").list();
    	Iterator it=list.iterator();
    		while(it.hasNext()){
    			
    			Object []row= (Object [])it.next();
    			DTOSubcontrACCCompanyCodeDataList dtoAccountInvoicePost = new DTOSubcontrACCCompanyCodeDataList(); 
    			try{
    			dtoAccountInvoicePost.setRecPostDate((Date)(row[0]));
    			dtoAccountInvoicePost.setInvoiceDate((row[1].toString()));
    			dtoAccountInvoicePost.setInvoiceDateEnd((row[2].toString()));
    			dtoAccountInvoicePost.setCompanyDivision((row[3]==null?"":row[3].toString()));
    			dtoAccountInvoicePost.setCompanyDivisionGroup((row[6]==null?"":row[6].toString()));
    			}catch(Exception e){
    				
    			}
    			subcontractorPostDateList.add(dtoAccountInvoicePost);
    		}
    		return subcontractorPostDateList;
    }

	public List getSubContcMergeExtract(String subcontrCompanyCode, String corpid, String postDate, String startInvoiceDates, String endInvoiceDates) {
		List <Object> subContcExtractList = new ArrayList<Object>(); 
		
		  List list= this.getSession().createSQLQuery("Select s.amount ,s.personId ,s.regNumber, s.approved ,if(pa.accountCrossReference is null,'',pa.accountCrossReference),s.glCode,s.id,s.serviceOrder,(replace(replace(replace((s.description),'\r\n',''),'\t',''),'\"','  '))description,s.job,s.post,s.division,s.companyDivision  from subcontractorcharges s  INNER JOIN partneraccountref pa on s.personId=pa.partnercode and pa.corpID='"+corpid+"' and (pa.refType='P' or pa.refType is null) and pa.accountCrossReference !='temp' and  pa.companyDivision in  ("+subcontrCompanyCode+")   Where  s.corpID='"+corpid+"' and s.amount != 0 and (s.balanceForward=false or s.balanceForward is null) and s.personId<>''  and s.companyDivision in  ("+subcontrCompanyCode+") and s.approved is not null and (DATE_FORMAT(s.approved,'%Y-%m-%d') >=  ('"+startInvoiceDates+" ')) and (DATE_FORMAT(s.approved,'%Y-%m-%d') <=  ('"+endInvoiceDates+"')) and s.accountSent is null and s.post is not null and s.post='"+postDate+"'")
		  .addScalar("s.amount", Hibernate.BIG_DECIMAL)
		  .addScalar("s.personId", Hibernate.STRING)  
		  .addScalar("s.regNumber", Hibernate.STRING) 
		  .addScalar("s.approved", Hibernate.DATE)
		  .addScalar("if(pa.accountCrossReference is null,'',pa.accountCrossReference)", Hibernate.STRING) 
		  .addScalar("s.glCode", Hibernate.STRING) 
		  .addScalar("s.id", Hibernate.LONG)
		  .addScalar("s.serviceOrder", Hibernate.STRING)  
		  .addScalar("description", Hibernate.STRING)
		  .addScalar("s.job", Hibernate.STRING) 
		  .addScalar("s.post", Hibernate.DATE)
		  .addScalar("s.division", Hibernate.STRING) 
		  .addScalar("s.companyDivision", Hibernate.STRING) 
		  .list();
		  Iterator it=list.iterator();
		  DTOForSubContcExtract dtoSubContcExtract=null;
		  while(it.hasNext())  
		  {
			  Object [] row=(Object[])it.next();
			  dtoSubContcExtract=new DTOForSubContcExtract(); 
			  dtoSubContcExtract.setAmount(row[0]);
			  dtoSubContcExtract.setPersonId(row[1]);
			  dtoSubContcExtract.setRegNumber(row[2]);
			  dtoSubContcExtract.setApproved(row[3]);
			  dtoSubContcExtract.setAccountCrossReference(row[4]);
			  dtoSubContcExtract.setGlCode(row[5]);
			  dtoSubContcExtract.setSubId(row[6]);
			  dtoSubContcExtract.setServiceOrder(row[7]); 
			  dtoSubContcExtract.setDescription(row[8]); 
			  dtoSubContcExtract.setJob(row[9]);
			  dtoSubContcExtract.setPost(row[10]);
			  dtoSubContcExtract.setDivision(row[11]) ;
			  dtoSubContcExtract.setCompanyDivision(row[12]) ;
			  
          subContcExtractList.add(dtoSubContcExtract);
			  
		  } 
		  return subContcExtractList;
	}

	public List getSSCWGPSubcontrACCCompanyCodeDataList(String sessionCorpID) {
    	
    	List subcontractorPostDateList=new ArrayList();
    	List list= this.getSession().createSQLQuery("(Select s.post ,DATE_FORMAT(DATE_ADD(DATE_ADD(s.approved, INTERVAL(1-DAYOFWEEK(s.approved)) DAY),INTERVAL if(DAYOFWEEK(s.approved)>5,'5','-2' ) day),'%Y-%m-%d'),DATE_FORMAT(DATE_ADD(DATE_ADD(s.approved, INTERVAL(7-DAYOFWEEK(s.approved )) DAY),INTERVAL if(DAYOFWEEK(s.approved)>5,'5','-2' ) day),'%Y-%m-%d'),s.companyDivision,'subcontractorcharges',s.id,if(s.companyDivision in ('SGL','FFG'),'A',if(s.companyDivision in ('SSC','DUL','FOR','LAU','LAM','GAI','BAL','INT','WAS','ALE'),'B',if(s.companyDivision in ('BOL','COU','COM'),'C',if(s.companyDivision in ('FMG'),'D',if(s.companyDivision in ('DFC','DFC'),'E',if(s.companyDivision in ('ALP'),'F',if(s.companyDivision in ('DTS'),'G','H'))))))) as compDiv  from subcontractorcharges s  INNER JOIN partneraccountref pa on s.personId=pa.partnercode and pa.corpID='"+sessionCorpID+"' and (pa.refType='P' or pa.refType is null) and pa.accountCrossReference !='temp' and  pa.companyDivision=s.companyDivision Where  s.corpID='"+sessionCorpID+"' and s.companyDivision is not null and s.companyDivision <>'' and s.amount != 0 and (s.balanceForward=false or s.balanceForward is null) and s.personId<>'' and s.companyDivision is not null and s.companyDivision !=''  and s.approved is not null and s.accountSent is null and s.post is not null) UNION (select a.paypostdate, DATE_FORMAT(DATE_ADD(DATE_ADD(a.invoiceDate , INTERVAL(1-DAYOFWEEK(a.invoiceDate)) DAY),INTERVAL if(DAYOFWEEK(a.invoiceDate)>5,'5','-2' ) day),'%Y-%m-%d'),DATE_FORMAT(DATE_ADD(DATE_ADD(a.invoiceDate, INTERVAL(7-DAYOFWEEK(a.invoiceDate)) DAY),INTERVAL if(DAYOFWEEK(a.invoiceDate)>5,'5','-2' ) day),'%Y-%m-%d'),a.companyDivision,'accountline',a.id, if(a.companyDivision in ('SGL','FFG'),'A',if(a.companyDivision in ('SSC','DUL','FOR','LAU','LAM','GAI','BAL','INT','WAS','ALE'),'B',if(a.companyDivision in ('BOL','COU','COM'),'C',if(a.companyDivision in ('FMG'),'D',if(a.companyDivision in ('DFC','DFC'),'E',if(a.companyDivision in ('ALP'),'F',if(a.companyDivision in ('DTS'),'G','H'))))))) as compDiv  from accountline a,partnerpublic p  where a.corpid='"+sessionCorpID+"' and a.companyDivision is not null and a.companyDivision <>'' and a.actualexpense <> 0 and a.payingstatus = 'A' and (a.vendorcode is not null or a.vendorcode <> '') and (a.payGl is not null or a.payGl <> '') and a.updatedOn >= '2008-12-31' and a.status=true and  a.vendorcode=p.partnerCode and p.isOwnerOp = true and p.corpid in ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','TSFT') and a.actgCode<>'' and a.actgCode<>'Temp'    and (a.invoiceNumber<>'' or a.invoiceNumber is not null) and invoiceDate is not null and a.payPostDate is not null and a.payAccDate is null) order by 2 desc").list();
    	Iterator it=list.iterator();
    		while(it.hasNext()){
    			
    			Object []row= (Object [])it.next();
    			DTOSubcontrACCCompanyCodeDataList dtoAccountInvoicePost = new DTOSubcontrACCCompanyCodeDataList(); 
    			try{
    			dtoAccountInvoicePost.setRecPostDate((Date)(row[0]));
    			dtoAccountInvoicePost.setInvoiceDate((row[1].toString()));
    			dtoAccountInvoicePost.setInvoiceDateEnd((row[2].toString()));
    			dtoAccountInvoicePost.setCompanyDivision((row[3]==null?"":row[3].toString()));
    			dtoAccountInvoicePost.setCompanyDivisionGroup((row[6]==null?"":row[6].toString()));
    			}catch(Exception e){
    				
    			}
    			subcontractorPostDateList.add(dtoAccountInvoicePost);
    		}
    		return subcontractorPostDateList;
    }

	public List getSubContcSSCWGPExtract(String subcontrCompanyCode, String corpid, String postDate, String startInvoiceDates, String endInvoiceDates) {
		List <Object> subContcExtractList = new ArrayList<Object>(); 
		
		List list= this.getSession().createSQLQuery("Select s.amount ,s.personId ,s.regNumber, s.approved ,if(pa.accountCrossReference is null,'',pa.accountCrossReference),s.glCode,s.id,s.serviceOrder,(replace(replace(replace((s.description),'\r\n',''),'\t',''),'\"','  '))description,s.job,s.post,s.division,s.companyDivision  from subcontractorcharges s  INNER JOIN partneraccountref pa on s.personId=pa.partnercode and pa.corpID='"+corpid+"' and (pa.refType='P' or pa.refType is null) and pa.accountCrossReference !='temp' and  pa.companyDivision in  ("+subcontrCompanyCode+")   Where  s.corpID='"+corpid+"' and s.amount != 0 and (s.balanceForward=false or s.balanceForward is null) and s.personId<>''  and s.companyDivision in  ("+subcontrCompanyCode+") and s.approved is not null and (DATE_FORMAT(s.approved,'%Y-%m-%d') >=  ('"+startInvoiceDates+" ')) and (DATE_FORMAT(s.approved,'%Y-%m-%d') <=  ('"+endInvoiceDates+"')) and s.accountSent is null and s.post is not null and s.post='"+postDate+"'")
		  .addScalar("s.amount", Hibernate.BIG_DECIMAL)
		  .addScalar("s.personId", Hibernate.STRING)  
		  .addScalar("s.regNumber", Hibernate.STRING) 
		  .addScalar("s.approved", Hibernate.DATE)
		  .addScalar("if(pa.accountCrossReference is null,'',pa.accountCrossReference)", Hibernate.STRING) 
		  .addScalar("s.glCode", Hibernate.STRING) 
		  .addScalar("s.id", Hibernate.LONG)
		  .addScalar("s.serviceOrder", Hibernate.STRING)  
		  .addScalar("description", Hibernate.STRING)
		  .addScalar("s.job", Hibernate.STRING) 
		  .addScalar("s.post", Hibernate.DATE)
		  .addScalar("s.division", Hibernate.STRING) 
		  .addScalar("s.companyDivision", Hibernate.STRING) 
		  .list();
		  Iterator it=list.iterator();
		  DTOForSubContcExtract dtoSubContcExtract=null;
		  while(it.hasNext())  
		  {
			  Object [] row=(Object[])it.next();
			  dtoSubContcExtract=new DTOForSubContcExtract(); 
			  dtoSubContcExtract.setAmount(row[0]);
			  dtoSubContcExtract.setPersonId(row[1]);
			  dtoSubContcExtract.setRegNumber(row[2]);
			  dtoSubContcExtract.setApproved(row[3]);
			  dtoSubContcExtract.setAccountCrossReference(row[4]);
			  dtoSubContcExtract.setGlCode(row[5]);
			  dtoSubContcExtract.setSubId(row[6]);
			  dtoSubContcExtract.setServiceOrder(row[7]); 
			  dtoSubContcExtract.setDescription(row[8]); 
			  dtoSubContcExtract.setJob(row[9]);
			  dtoSubContcExtract.setPost(row[10]);
			  dtoSubContcExtract.setDivision(row[11]) ;
			  dtoSubContcExtract.setCompanyDivision(row[12]) ;
			  
        subContcExtractList.add(dtoSubContcExtract);
			  
		  } 
		  return subContcExtractList; 
	}

	public int subContcSSCWGPFileUpdate(String batchName, String subIdList, String sessionCorpID) {
		String s="update SubcontractorCharges set sentToAccountVia='"+batchName+"_GP'  where amount <> 0 and id in  ("+subIdList+") and corpID='"+sessionCorpID+"' and approved is not null and sentToAccountVia  not like '%_GP%' and post is not null";
		int i= getHibernateTemplate().bulkUpdate("update SubcontractorCharges set accountSent=now(),sentToAccountVia='"+batchName+"'  where amount <> 0 and id in  ("+subIdList+") and corpID='"+sessionCorpID+"' and approved is not null and accountSent is null and post is not null");
		   //System.out.println("\n\n\n\n\n\nThe number of subcontractorcharges line "+i+" is updated");
		  return i; 
	} 
}
