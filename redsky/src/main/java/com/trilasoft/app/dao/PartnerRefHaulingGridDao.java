package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.PartnerRefHaulingGrid;

public interface PartnerRefHaulingGridDao extends GenericDao<PartnerRefHaulingGrid, Long>  {
	public List getHaulingGridList(String lowDistance, String highDistance, String rateMile, String rateFlat, String grid, String unit, String countryCode,String corpID);
	
}
