<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Add Storage</title>
<meta name="heading" content="Add Storage" />

<link href='<s:url value="/css/main.css"/>' rel="stylesheet"
	type="text/css" />
<s:head theme="ajax" />
<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
<script type="text/javascript">
function setFlagValueMo(){
	document.forms['locationaddForm'].elements['hitFlag'].value = '1';
}

function notExists(){
		alert("The Ticket information is not saved yet");
}	

function chkSelect()
	{
		if (checkFloat('locationaddForm','storage.measQuantity','Invalid data in Weight') == false)
           {
              document.forms['locationaddForm'].elements['storage.measQuantity'].focus();
              return false;
           }
           if (checkFloat('locationaddForm','storage.volume','Invalid data in Volume') == false)
           {
              document.forms['locationaddForm'].elements['storage.volume'].focus();
              return false;
           }
           if (checkNumber('locationaddForm','storage.pieces','Invalid data in Pieces') == false)
           {
              document.forms['locationaddForm'].elements['storage.pieces'].focus();
              return false;
           }
           if (checkFloat('locationaddForm','storage.price','Invalid data in Price') == false)
           {
              document.forms['locationaddForm'].elements['storage.price'].focus();
              return false;
           }
           if(document.forms['locationaddForm'].elements['location.locationId'].value==''){
        	   alert('Please select Location.');
        	   return false; 
           }
           document.forms['locationaddForm'].elements['bookStorage.locationId'].value=document.forms['locationaddForm'].elements['location.locationId'].value;
           document.forms['locationaddForm'].elements['storage.locationId'].value=document.forms['locationaddForm'].elements['location.locationId'].value;
}

function forwardToMyMessage(){ 
	document.forms['locationaddForm'].elements['bookStorage.price'].value=document.forms['locationaddForm'].elements['storage.price'].value;		
	document.forms['locationaddForm'].elements['bookStorage.description'].value=document.forms['locationaddForm'].elements['storage.description'].value;		
	document.forms['locationaddForm'].elements['bookStorage.containerId'].value=document.forms['locationaddForm'].elements['storage.containerId'].value;		
	document.forms['locationaddForm'].elements['bookStorage.pieces'].value=document.forms['locationaddForm'].elements['storage.pieces'].value;		
	document.forms['locationaddForm'].elements['bookStorage.locationId'].value=document.forms['locationaddForm'].elements['storage.locationId'].value;	
	document.forms['locationaddForm'].elements['bookStorage.idNum'].value=document.forms['locationaddForm'].elements['storage.idNum'].value;	
	document.forms['locationaddForm'].elements['bookStorage.itemTag'].value=document.forms['locationaddForm'].elements['storage.itemTag'].value;	
	document.forms['locationaddForm'].elements['bookStorage.createdOn'].value=document.forms['locationaddForm'].elements['storage.createdOn'].value;
	document.forms['locationaddForm'].elements['bookStorage.createdBy'].value=document.forms['locationaddForm'].elements['storage.createdBy'].value;
	document.forms['locationaddForm'].elements['bookStorage.updatedOn'].value=document.forms['locationaddForm'].elements['storage.updatedOn'].value;
	document.forms['locationaddForm'].elements['bookStorage.updatedBy'].value=document.forms['locationaddForm'].elements['storage.updatedBy'].value;
	document.forms['locationaddForm'].elements['storage.shipNumber'].value=document.forms['locationaddForm'].elements['shipNumber'].value;	
	document.forms['locationaddForm'].elements['bookStorage.shipNumber'].value=document.forms['locationaddForm'].elements['shipNumber'].value;					
}
	

function findLocationId() {
		      var whouse=document.forms['locationaddForm'].elements['workTicket.warehouse'].value;
		      var storageLibrayType=document.forms['locationaddForm'].elements['storageTypeCode'].value;
		 	  openWindow('findLocation.html?storageLibrayType='+ storageLibrayType +'&whouse='+whouse+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=location.locationId',1024,500);            
		 	  document.forms['locationaddForm'].elements['location.locationId'].select();
}

// fuction for updating the volume in storage lib

function availableVolume()
	{
	var n=0;
	var unit = "";
	var rads =document.forms['locationaddForm'].elements['storage.volUnit'];
	for (var i=0; i < rads.length; i++)
	    {
	    if (rads[i].checked)
	       {
	           unit = rads[i].value;

	       }
	    }
	var volume = document.forms['locationaddForm'].elements['storage.volume'].value;
	var availableVolCft=document.forms['locationaddForm'].elements['storageLibrary.availVolumeCft'].value;
	var availableVolCbm=document.forms['locationaddForm'].elements['storageLibrary.availVolumeCbm'].value;
	if(unit=='Cft')
	{
	var availeLimit = eval(availableVolCft)-eval(volume);
	if(availeLimit<0)
	{
	var agree='This storage library has only '+availableVolCft+' available capacity.';
	alert(agree);
	document.forms['locationaddForm'].elements['subAvailVolCbm'].value = eval(availableVolCbm);
	document.forms['locationaddForm'].elements['subAvailVolCft'].value = eval(availableVolCft);
	document.forms['locationaddForm'].elements['addedUtilizedVolCft'].value = document.forms['locationaddForm'].elements['storageLibrary.usedVolumeCft'].value;
	document.forms['locationaddForm'].elements['addedUtilizedVolCbm'].value = document.forms['locationaddForm'].elements['storageLibrary.usedVolumeCbm'].value;
	document.forms['locationaddForm'].elements['storage.volume'].value=document.forms['locationaddForm'].elements['oldVolume'].value;
	}
	else{
	 var cubicMeter =(0.0283168466) * volume;
	 var cubicMeterValue=cubicMeter.toFixed(2);
	 var utilizedVolCft1=eval(document.forms['locationaddForm'].elements['storageLibrary.usedVolumeCft'].value);
	 var utilizedVolCbm1=eval(document.forms['locationaddForm'].elements['storageLibrary.usedVolumeCbm'].value);
	 var addedUtilizedCft = utilizedVolCft1 + eval(volume);
	 var addedUtilizedCbm = utilizedVolCbm1 + eval(cubicMeterValue);
	 var availeLimitCbm = eval(availableVolCbm)-eval(cubicMeterValue);
	 document.forms['locationaddForm'].elements['subAvailVolCbm'].value=availeLimitCbm;
	 document.forms['locationaddForm'].elements['subAvailVolCft'].value=availeLimit;
	 document.forms['locationaddForm'].elements['addedUtilizedVolCft'].value = addedUtilizedCft;
	 document.forms['locationaddForm'].elements['addedUtilizedVolCbm'].value = addedUtilizedCbm;
	 }
	}
	else{
	var availeLimit =availableVolCbm-volume;
	if(availeLimit<0)
	{
	    var agree='This storage library has only '+availableVolCbm+' available capacity.';
	    alert(agree);
		document.forms['locationaddForm'].elements['subAvailVolCbm'].value = eval(availableVolCbm);
		document.forms['locationaddForm'].elements['subAvailVolCft'].value = eval(availableVolCft);
		document.forms['locationaddForm'].elements['addedUtilizedVolCft'].value = document.forms['locationaddForm'].elements['storageLibrary.usedVolumeCft'].value;
		document.forms['locationaddForm'].elements['addedUtilizedVolCbm'].value = document.forms['locationaddForm'].elements['storageLibrary.usedVolumeCbm'].value;
		document.forms['locationaddForm'].elements['storage.volume'].value=document.forms['locationaddForm'].elements['oldVolume'].value;
	}
	else{
			var cubicFeet =(35.3146) * volume;
			var cubicFeetValue=cubicFeet.toFixed(2);
			var availeLimitCft = eval(availableVolCft)-eval(cubicFeetValue);
			document.forms['locationaddForm'].elements['subAvailVolCbm'].value=availeLimit;
			document.forms['locationaddForm'].elements['subAvailVolCft'].value=availeLimitCft;
			var utilizedVolCbm2=eval(document.forms['locationaddForm'].elements['storageLibrary.usedVolumeCbm'].value);
			var utilizedVolCft2=eval(document.forms['locationaddForm'].elements['storageLibrary.usedVolumeCft'].value);
			var addedUtilizedCbm = utilizedVolCbm2 + eval(volume);
			var addedUtilizedCft = utilizedVolCft2 + eval(cubicFeetValue);
			document.forms['locationaddForm'].elements['addedUtilizedVolCbm'].value = addedUtilizedCbm;
			document.forms['locationaddForm'].elements['addedUtilizedVolCft'].value = addedUtilizedCft;	
	      }
	 }
}
function confirmUser(){
	var locId=document.forms['locationaddForm'].elements['location.locationId'].value;
	var stoId='${locationByStorage}';
	var stoMode='${storageLibrary.storageMode}';
	if(locId!=stoId && stoMode!='5'){
	var agree=confirm("Changing location in this ticket will change the other storage location too.");
	if(agree){
	
	}
	else{
	document.forms['locationaddForm'].elements['location.locationId'].value=stoId;
	}
	}
}
						
</script>


</head>

<s:form id="locationaddForm" action="saveLocationUnit" method="post" validate="true">
<div id="Layer1" >
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="description" />
<s:hidden name="shipNumber" value="${shipNumber}"/>
<s:hidden name="id" value="${id}"/> 
<s:hidden name="ticket" value="${ticket}"/>
<s:hidden name="storageLibrary.storageType" />
<s:hidden name="storageLibrary.usedVolumeCft" />
<s:hidden name="storageLibrary.usedVolumeCbm" />
<s:hidden name="addedUtilizedVolCft" />
<s:hidden name="addedUtilizedVolCbm" />
<s:hidden name="subAvailVolCft" />
<s:hidden name="subAvailVolCbm" />
<s:hidden name="oldVolume" value="${storage.volume}"/>
<s:hidden name="id1" value="${id1}"/>
<s:hidden name="storageTypeCode"/>
<s:hidden name="countTicketLocationNotes" value="<%=request.getParameter("countTicketLocationNotes") %>"/>
<c:set var="countTicketLocationNotes" value="<%=request.getParameter("countTicketLocationNotes") %>" />
<s:hidden name="location.id" value="%{location.id}" />
<s:hidden name="locationByStorage"/>
<s:hidden name="storageModeValue" value="${storageLibrary.storageMode}"/>
	
<div id="newmnav">
		   			<ul>
					  	<li><a href="editWorkTicketUpdate.html?id=<%=request.getParameter("id1") %>"><span>Work Ticket</span></a></li>
						<li><a href="bookStorageLibraries.html?id=<%=request.getParameter("id1") %>"><span>Storage List</span></a></li>
						<li id="newmnav1" style="background:#FFF"><a class="current"><span>Add Storage<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
						<li><a href="storageUnit.html?id=<%=request.getParameter("id1") %>"><span>Access/Release Storage</span></a></li>
					  	<li><a href="storageUnitMove.html?id=<%=request.getParameter("id1") %>"><span>Move Storage Location</span></a></li>					  	
					</ul>
</div>
<div class="spn">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
	<table class="" cellspacing="1" cellpadding="0" border="0" width="100%">
		<tbody>
			<tr>
				<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
					<tbody>

						<tr>
							<td class="listwhite" align="right" width="20px"></td>
							<td class="listwhitetext" align="right" width="65px">Storage Id</td>
							<td><s:textfield id="storage1" name="storageLibrary.storageId" value="%{storageLibrary.storageId}" readonly="true" cssClass="input-text" tabindex="1"/></td>
						
							<c:if test="${storageLibrary.storageMode=='3' || storageLibrary.storageMode=='1'}">
							<c:if test="${locationByStorage==''}">
							<td class="listwhitetext" align="right" width="140px">Location<font color="red" size="2">*</font></td>
							<td width="200px" ><s:textfield name="location.locationId" readonly="true" cssClass="input-text" cssStyle="width:170px;" tabindex="2"/>
							<img align="top" class="openpopup" width="17" height="20" style="float:right;" onclick="findLocationId();changeStatus()" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
							</c:if>
							<c:if test="${locationByStorage!=''}">
							<td class="listwhitetext" align="right" width="140px">Location</td>
							<td width="200px" ><s:textfield name="location.locationId" value="${locationByStorage}" readonly="true" cssClass="input-textUpper" cssStyle="width:170px;" tabindex="2" onblur="confirmUser();"/>
							<img align="top" class="openpopup" width="17" height="20" style="float:right;" onclick="findLocationId();changeStatus()" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
							</c:if>
							</c:if>
							<c:if test="${storageLibrary.storageMode=='5'}">							
							<td class="listwhitetext" align="right" width="140px">Location<font color="red" size="2">*</font></td>
							<td width="200px" ><s:textfield name="location.locationId"  readonly="true" cssClass="input-text" cssStyle="width:170px;" tabindex="2"/>
							<img align="top" class="openpopup" width="17" height="20" style="float:right;" onclick="findLocationId();changeStatus()" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
							</c:if>
							<td></td>
							
							<c:if test="${empty location.id}">
								<td align="right" width="130px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty location.id}">						
							<c:choose>
							
								<c:when test="${countTicketLocationNotes == '0' || countTicketLocationNotes == '' || countTicketLocationNotes == null}">
								
								<td align="right" width="130px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${id1}&notesId=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${id1}&notesId=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);" ></a></td>
								</c:when>
								<c:otherwise>
								
								<td align="right" width="130px"><img src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${id1}&notesId=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${id1}&notesId=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);" ></a></td>
								</c:otherwise>
							</c:choose> 

							</c:if>

						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right" valign="top">Description</td>
							<td colspan="2"><s:textarea name="storage.description" cols="30" rows="4" tabindex="3" cssClass="textarea"/></td>
							
							<c:if test="${not empty storage.id}">
								<td class="listwhitetext" align="right" valign="top">Storage Action</td>
								<td align="left" valign="top"><s:textfield name="bookStorage.what" size="20" maxlength="10" cssClass="input-text" tabindex="4" /></td>
							</c:if>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Container ID</td>
							<td><s:textfield name="storage.containerId" size="20" maxlength="10" cssClass="input-text" tabindex="5" /></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Model</td>
							<td><s:textfield name="storage.model" size="20" maxlength="10" cssClass="input-text" tabindex="6" /></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Serial</td>
							<td><s:textfield name="storage.serial" size="20" maxlength="10" cssClass="input-text" tabindex="7" /></td>
							<td></td>
							<td colspan="3" rowspan="4">						
							<table class="detailTabLabel">
							<tr>
							<td class="listwhitetext">Available&nbsp;Volume</td>
							</tr>
							<tr>							
							<td><s:textfield name="storageLibrary.availVolumeCft" size="10" maxlength="10" readonly="true" cssClass="input-textUpper" cssStyle="text-align:right" /></td>
							<td class="listwhitetext">Cft</td>
							</tr>
							<tr>
							<td><s:textfield name="storageLibrary.availVolumeCbm" size="10" maxlength="10" readonly="true" cssClass="input-textUpper" cssStyle="text-align:right" /></td>
							<td class="listwhitetext">Cbm</td>
							</tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
							</table>							
							</td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Weight</td>
							<td><s:textfield name="storage.measQuantity" size="20" maxlength="10" cssClass="input-text" tabindex="8" cssStyle="text-align:right"/></td>
							<td align="left"  valign="middle" class="listwhitetext"><fmt:message key='labels.units'/><s:radio name="storage.unit" list="%{weightunits}"  onchange="changeStatus();" tabindex="9" cssStyle="vertical-align:bottom;"/></td>
							<td class="listwhitetext" align="right" ></td>
							
							
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Volume</td>
							<td><s:textfield name="storage.volume" size="20" maxlength="10" cssClass="input-text" tabindex="10" cssStyle="text-align:right" onchange="availableVolume();"/></td>
							<td align="left" colspan="" valign="middle" class="listwhitetext"><fmt:message key='labels.units'/><s:radio name="storage.volUnit" list="%{volumeunits}"  onchange="changeStatus();" onclick="availableVolume();" tabindex="11" cssStyle="vertical-align:bottom;"/></td>
						
							<td class="listwhitetext" align="right" ></td>
							
							
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Item Tag</td>
							<td><s:textfield name="storage.itemTag" size="20" maxlength="10" cssClass="input-text" tabindex="12" /></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Pieces</td>
							<td><s:textfield name="storage.pieces" size="20" maxlength="7"  cssClass="input-text" cssStyle="text-align:right" tabindex="13" /></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Price</td>
							<td><s:textfield name="storage.price" size="20" maxlength="7"  cssClass="input-text" tabindex="14" cssStyle="text-align:right"/></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhite" align="right"></td>
							<td><s:hidden name="workTicket.ticket" value="${ticket}" /></td>
							<s:hidden name="workTicket.warehouse"/>
							<s:hidden name="storage.idNum" required="true" cssClass="text medium" />
							<s:hidden name="storage.id" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.id" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.price" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.createdOn" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.createdBy" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.updatedOn" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.updatedBy" required="true" cssClass="text medium" />
							<s:hidden key="bookStorage.description" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.containerId" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.corpID" required="true"   />
							<s:hidden name="bookStorage.type"/>
							<s:hidden name="bookStorage.dated"/>
							<s:hidden name="storage.locationId"/> 
							<s:hidden name="storage.serviceOrderId" value="%{workTicket.serviceOrderId}" />
							<s:hidden name="bookStorage.serviceOrderId" value="%{workTicket.serviceOrderId}" />
							<s:hidden name="storage.storageId" value="%{storageLibrary.storageId}"/>
							<s:hidden name="storage.corpID" />
							<s:hidden name="bookStorage.model" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.itemTag" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.ticket" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.jobNumber" required="true" cssClass="text medium"/>
							<s:hidden name="storage.jobNumber" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.measQuantity" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.idNum" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.itemNumber" required="true" cssClass="text medium" />
							<s:hidden name="storage.itemNumber" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.pieces" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.locationId"/>
							<s:hidden name="bookStorage.oldLocation"  cssClass="text medium" />
							<s:hidden name="storage.shipNumber" required="true" cssClass="text medium"/>
							<s:hidden name="bookStorage.shipNumber" value="${shipNumber}" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.storageId" value="%{storageLibrary.storageId}"/>
							
						</tr>
					</tbody>
				</table>
			  </td>
			</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	<table>
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			
		<tr><fmt:formatDate var="serviceCreatedOnFormattedValue" value="${storage.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='storage.createdOn' /></b></td>
				<s:hidden name="storage.createdOn" value="${serviceCreatedOnFormattedValue}"/>
				<td style="font-size:.90em ;width:130px"><fmt:formatDate value="${storage.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message 	key='storage.createdBy' /></b></td>
				<c:if test="${not empty storage.id}">
					<s:hidden name="storage.createdBy" />
					<td style="font-size:.90em"><s:label name="createdBy"
						value="%{storage.createdBy}" /></td>
				</c:if>
				<c:if test="${empty storage.id}">
					<s:hidden name="storage.createdBy"
						value="${pageContext.request.remoteUser}" />
					<td style="font-size:.90em"><s:label name="createdBy"
						value="${pageContext.request.remoteUser}" /></td>
				</c:if>

				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message
					key='storage.updatedOn' /></b></td>
					<fmt:formatDate var="serviceUpdatedOnFormattedValue" value="${storage.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="storage.updatedOn" value="${serviceUpdatedOnFormattedValue}" />
				<td style="font-size:.90em; width:130px"><fmt:formatDate value="${storage.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message	key='storage.updatedBy' /></b></td>
				<c:if test="${not empty storage.id}"> 
                <s:hidden name="storage.updatedBy"/>

				<td style="width:85px ; font-size:.90em"><s:label name="updatedBy" value="%{storage.updatedBy}"/></td>
				
				</c:if>
				
				<c:if test="${empty storage.id}">
				
				<s:hidden name="storage.updatedBy" value="${pageContext.request.remoteUser}"/>
				
				<td style="width:85px ; font-size:.90em"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
				
				</c:if> 
			</tr>
		</tbody>
	</table>
	
</div>
	<div align="left">
		<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="saveUnit" key="button.save" onclick="return chkSelect();forwardToMyMessage();" tabindex="15"/> 
		<s:reset type="button" key="Reset" cssClass="cssbutton" cssStyle="width:55px; height:25px" tabindex="16"/>
	</div>	

	<c:if test="${hitFlag == 1}" >
		<c:redirect url="/bookStorageLibraries.html?id=${workTicket.id}"  />
	</c:if>
</s:form>
<script type="text/javascript">
try{  
		document.getElementById('storage1').focus();
	}catch(e){}
</script>