<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<head>
    <title>Expirations Details</title>
    <meta name="heading" content="Expirations Details:"/>
    <style><%@ include file="/common/calenderStyle.css"%></style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->
   
<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
	</script>

<script>
function pick() {
	  try {
	   window.opener.replaceLocation();
	   //parent.window.opener.document.location.reload();
	  }catch(e){
	   parent.window.opener.document.location.reload();
	  }
	    window.close();
	 }
	
 function valButton(btn) {
    var cnt = -1;
    for (var i=btn.length-1; i > -1; i--) {
        if (btn[i].checked) {cnt = i; i = -1;}
    }
    if (cnt > -1) return btn[cnt].value;
    else return null;
}  
function validation(){
if(document.forms['ownerOperatorDetailsForm'].elements['controlExpirations.fieldName'].value==''){
   alert("Expiry For is a required field ")
   return false;
 }	
	}
	
	function checkEmail() {
if(document.forms['ownerOperatorDetailsForm'].elements['controlExpirations.email'].value!=''){
var emailFilter2=/^.+@.+\..{2,3}$/ 
var emailValue2=document.forms['ownerOperatorDetailsForm'].elements['controlExpirations.email'].value;
if (!(emailFilter2.test(emailValue2))) {
alert("Please enter valid Email.")
document.forms['ownerOperatorDetailsForm'].elements['controlExpirations.email'].value='';
document.forms['ownerOperatorDetailsForm'].elements['controlExpirations.email'].select();
return false;
}else{
return true;
}
}
}
</script> 	    
</head>

<s:form id="ownerOperatorDetailsForm" action="saveOwnerOperatorDetails.html?btntype=yes&decorator=popup&popup=true" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat1" value="dd-NNN-yy"/>
<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="controlExpirations.parentId" value="<%=request.getParameter("parentId") %>"/>
<s:hidden name="controlExpirations.id" />
<s:hidden name="validateString" />
<s:hidden name="controlExpirations.tableName" /></td>
<s:hidden name="controlExpirations.type"/></td>

<s:hidden name="id" value="${controlExpirations.id}"/>
<div id="layer1" style="width:700px; margin-top: 10px;">
<div id="newmnav">
			<ul>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Expirations Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
           </ul>
       </div><div class="spn" >&nbsp;</div>
      
<div id="Layer5" style="width:95%" onkeydown="changeStatus();">
<table class="" cellspacing="0" cellpadding="0" border="0" width="95%" >
<tbody>
<tr>
<td>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr><td height="5px"></td></tr>

<tr>

<td align="left" width="55px" class="listwhitetext">Expiry For<font color="red" size="2">*</font></td>
<td align="left" class="listwhitetext" WIDTH="190"><s:select cssClass="list-menu" name="controlExpirations.fieldName" list="%{expirations}" headerKey="" headerValue="" cssStyle="width:120px"/></td>
<td align="left" width="40px" class="listwhitetext">Required</td>	
<c:set var="isRequiredFlag" value="false"/>
								<c:if test="${controlExpirations.required}">
									<c:set var="isRequiredFlag" value="true"/>
								</c:if>
								<td align="left" class="listwhitetext"><s:checkbox key="controlExpirations.required" value="${isRequiredFlag}" fieldValue="true" /></td>
							<td align="left" width="55px" class="listwhitetext">Expiry Date</td>
							<c:if test="${not empty controlExpirations.expiryDate}">
								<s:text id="expiryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="controlExpirations.expiryDate"/></s:text>
								<td width="5%"><s:textfield cssClass="input-text" id="expiryDate" name="controlExpirations.expiryDate" value="%{expiryDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td width="20"><img id="expiryDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
								<c:if test="${empty controlExpirations.expiryDate}">
								<td width="5%"><s:textfield cssClass="input-text" id="expiryDate" name="controlExpirations.expiryDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td width="38"><img id="expiryDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
</tr>
 
<tr><td align="left" height="15px"></td></tr>
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
</div>
</td>
</tr>
</tbody>
</table> 
<table width="700px">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${controlExpirations.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="controlExpirations.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${controlExpirations.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						
						
						<c:if test="${not empty controlExpirations.id}">
								<s:hidden name="controlExpirations.createdBy"/>
								<td ><s:label name="createdBy" value="%{controlExpirations.createdBy}"/></td>
							</c:if>
							<c:if test="${empty controlExpirations.id}">
								<s:hidden name="controlExpirations.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${controlExpirations.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="controlExpirations.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${controlExpirations.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty controlExpirations.id}">
							<s:hidden name="controlExpirations.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{controlExpirations.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty controlExpirations.id}">
							<s:hidden name="controlExpirations.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
</div> 
   <table><tr><td>   
        <s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" method="save" key="button.save" theme="simple" onclick="return validation();"/>
        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" />
	</td></tr></table>
</div>
</s:form> 
<div id="mydiv" style="position:absolute;top:110px;"></div>
<script type="text/javascript">
   try{
if(document.forms['ownerOperatorDetailsForm'].elements['btntype'].value=='yes'){
pick();
}
}
catch(e){} 
 </script>
 
<script type="text/javascript">
	RANGE_CAL_1 = new Calendar({
        inputField: "expiryDate",
        dateFormat: "%d-%b-%y",
        trigger: "expiryDate_trigger",
        bottomBar: true,
        animation:true,
        onSelect: function() {                             
            this.hide();
   		 }
	});
 </script>
    