package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.InventoryArticleDao;
import com.trilasoft.app.model.InventoryArticle;
import com.trilasoft.app.service.InventoryArticleManager;

public class InventoryArticleManagerImpl extends GenericManagerImpl<InventoryArticle,Long> implements InventoryArticleManager{
	InventoryArticleDao inventoryArticleDao;
	public InventoryArticleManagerImpl(InventoryArticleDao inventoryArticleDao) {
		super(inventoryArticleDao);
		this.inventoryArticleDao=inventoryArticleDao;
	}
	 public  List	getArticleAutoComplete(String sessionCorpID,String article){
		   return inventoryArticleDao.getArticleAutoComplete(sessionCorpID, article);
	   }
}
