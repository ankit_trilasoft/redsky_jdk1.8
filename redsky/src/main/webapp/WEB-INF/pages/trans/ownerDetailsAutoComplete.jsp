<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 <head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>   
<s:form id="partnerDetailsForm" method="post" > 

<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin:0px;width:100%;">
<tr>
 <td valign="top">
 <div style="overflow-y:auto;height:215px;">
 <display:table name="partnerDetailsAutoComplete" class="table" id="partnerDetailsAutoComplete" style="width:100%;"> 
   	<display:column title="Code" >  
   	<a onclick="copyPartnerDetails('${partnerDetailsAutoComplete.partnercode}','${paertnerCodeId}','${autocompleteDivId}');">${partnerDetailsAutoComplete.partnercode}</a>
   	</display:column>
  	<display:column property="lastname" title="Name" ></display:column>
	<display:column property="billingcountry" title="Country" ></display:column>	
	<display:column property="billingstate" title="State" ></display:column>
	<display:column property="billingcity" title="City" ></display:column>
	<display:column property="partnerType" title="Partner Type" ></display:column>
</display:table>
</div>
</td>
<td  align="right" valign="top"><img align="right" class="openpopup" style="position:absolute;top:1px;right:2px;" onclick="closeMyDiv('${autocompleteDivId}','${paertnerCodeId}');" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
                                <c:if test="${not empty partnerDetailsAutoComplete}">
								<s:hidden id="partnerDetailsAutoCompleteListSize" value="false"/>
								</c:if>
								<c:if test="${empty partnerDetailsAutoComplete}">
								<s:hidden id="partnerDetailsAutoCompleteListSize" value="true"/>
								</c:if>
</s:form>