package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CompanyToDoRuleExecutionUpdateDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.CompanyToDoRuleExecutionUpdate;

public class CompanyToDoRuleExecutionUpdateDaoHibernate extends GenericDaoHibernate<CompanyToDoRuleExecutionUpdate, Long> implements CompanyToDoRuleExecutionUpdateDao {
	private HibernateUtil hibernateUtil;
	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	public CompanyToDoRuleExecutionUpdateDaoHibernate() {
		super(CompanyToDoRuleExecutionUpdate.class);
	}

	public List<CompanyToDoRuleExecutionUpdate> findByCorpID(String corpId) {
		return getSession().createQuery("from CompanyToDoRuleExecutionUpdate where corpID='"+corpId+"'").list();
	}

}
