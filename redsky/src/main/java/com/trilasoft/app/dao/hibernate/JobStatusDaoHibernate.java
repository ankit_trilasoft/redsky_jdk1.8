package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.JobStatusDao;
import com.trilasoft.app.model.JobStatus;

public class JobStatusDaoHibernate extends GenericDaoHibernate<JobStatus, Long> implements JobStatusDao{

	public JobStatusDaoHibernate() {
		super(JobStatus.class);
		
	}

	public List findMaximumId() {
		return null;
	}

	public void increaseToTalCount(Long id, String sessionCorpID) {
		getHibernateTemplate().bulkUpdate("update JobStatus set totalCount=totalCount+1 where corpID='"+sessionCorpID+"' and id='"+id+"'");		
	}

	public void updatePresentCount(Long countTotalNumber, String sessionCorpID, Long id, Long totCount) {
		getHibernateTemplate().bulkUpdate("update JobStatus set presentCount="+countTotalNumber+", status='Processing "+countTotalNumber+" of "+totCount+"' where corpID='"+sessionCorpID+"' and id='"+id+"'");	
	}

	public List getPresentCount(String sessionCorpID, Long id) {
		List maxid=getHibernateTemplate().find("select max(id) from JobStatus");
		List pCount=new ArrayList();
		if(maxid!=null)
		{
			pCount= getHibernateTemplate().find("select presentCount from JobStatus where corpID='"+sessionCorpID+"' and id='"+maxid.get(0)+"'");
			
		}
		return pCount;
	}

	public List getTotalCount(String sessionCorpID, Long id) {
		List maxid=getHibernateTemplate().find("select max(id) from JobStatus");
		List tCount=new ArrayList();
		if(maxid!=null)
		{
			tCount= getHibernateTemplate().find("select totalCount from JobStatus where corpID='"+sessionCorpID+"' and id='"+maxid.get(0)+"'");
			
		}
		return tCount;
	}

	public List getList(String sessionCorpID) {
		return getHibernateTemplate().find("from JobStatus where corpID='"+sessionCorpID+"' and userName is not null order by id desc");
	}

	public List getStatus(String sessionCorpID) {
		List maxid=getHibernateTemplate().find("select max(id) from JobStatus");
		List tCount=new ArrayList();
		if(maxid!=null)
		{
			tCount= getHibernateTemplate().find("select status from JobStatus where corpID='"+sessionCorpID+"' and id='"+maxid.get(0)+"'");
			
		}
		return tCount;
	}

	

}
