ALTER TABLE `redsky`.`commission` CHANGE COLUMN `actualExpence` `actualExpense` DECIMAL(19,2) DEFAULT NULL, ADD COLUMN `chargeCode` VARCHAR(20) DEFAULT NULL AFTER `aid`;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `miscellaneousdefaultdriver` VARCHAR(10) DEFAULT NULL AFTER `POS`;

ALTER TABLE `redsky`.`emailsetup` MODIFY COLUMN `emailStatus` VARCHAR(500);

ALTER TABLE `redsky`.`company` ADD COLUMN `RSBillingInstructions` TEXT AFTER `securityChecked`,ADD COLUMN `payablesXferWithApprovalOnly` BIT(1) DEFAULT b'1' AFTER `RSBillingInstructions`;

ALTER TABLE `redsky`.`partnerpublic`  MODIFY COLUMN `typeOfVendor` VARCHAR(300);

ALTER TABLE `redsky`.`emailsetup` ADD COLUMN `signaturePart` VARCHAR(200) AFTER `updatedBy`;

alter table integrationloginfo add column ugwwOA varchar(15);

alter table integrationloginfo add column ugwwDA varchar(15);

alter table integrationloginfo add column ugwwBA varchar(15);

ALTER TABLE `redsky`.`reports` ADD COLUMN `fieldName` VARCHAR(50) NULL  AFTER `emailBody` , ADD COLUMN `value` VARCHAR(50) NULL  AFTER `fieldName` ;

update company set payablesXferWithApprovalOnly =false where corpid in ('SSCW','CWMS');

ALTER TABLE `reports` CHANGE COLUMN `fieldName` `formFieldName` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL , CHANGE COLUMN `value` `formValue` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL  ;
 
ALTER TABLE `redsky`.`timesheet` MODIFY COLUMN `userName` VARCHAR(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;

ALTER TABLE `redsky`.`company` ADD COLUMN `vanlineEnabled` BIT(1) NULL DEFAULT b'0'  AFTER `payablesXferWithApprovalOnly` ;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `vanlineSettleColourStatus` BIT(1) NULL DEFAULT b'0'  AFTER `miscellaneousdefaultdriver` ;

DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`findcompanydivision` $$
CREATE FUNCTION `findcompanydivision`(code varchar(50)) RETURNS varchar(10) CHARSET utf8
BEGIN
DECLARE Name varchar(10) DEFAULT 0;

SELECT distinct companycode INTO Name FROM companydivision WHERE
bookingagentcode=code;

RETURN Name;

END $$

DELIMITER ;

DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`FindsumofActualRevenueofallserviceorder` $$
CREATE DEFINER=`root`@`%` FUNCTION `FindsumofActualRevenueofallserviceorder`(v1 VARCHAR(50),fromdate DATETIME,todate DATETIME) RETURNS double
    DETERMINISTIC
BEGIN
    DECLARE i1,avg Double ;
         select sum(a.actualrevenue) into i1 from customerfile c Inner Join serviceorder s on c.id=s.customerfileid and s.corpid='UTSI'
Inner Join billing b on s.id=b.id and b.corpid='UTSI'
Left Outer join accountline a on s.id=a.serviceorderid and a.corpid='UTSI'
and a.status is true and a.activateaccportal is true and a.actualrevenue!='0.00'
Inner Join contract co on a.contract=co.contract and co.corpid='UTSI'
Inner Join charges ch on ch.charge=a.chargecode and ch.contract=co.contract and ch.corpid='UTSI'
Inner Join costelement ce on ch.costelement=ce.costelement and ce.corpid='UTSI'
where  ce.reportingalias in ('Accessorials','broker')
 and a.category in ('Accessorials','broker')
and c.corpid='UTSI' and c.sequencenumber=v1 and a.receivedinvoicedate between fromdate and todate;

    SET avg = i1;
      RETURN avg;
   END $$

DELIMITER ;



