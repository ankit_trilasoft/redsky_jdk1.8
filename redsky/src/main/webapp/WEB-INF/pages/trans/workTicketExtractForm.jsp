<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
 
<head>
    <title>Work Ticket Extract</title>
    <meta name="heading" content="Work Ticket Extract"/>	
	<script language="javascript" type="text/javascript">
		<%@ include file="/common/calenderStyle.css"%>
	</script>
	</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
	<script language="javascript" type="text/javascript">
       function CompareDates()
			{
				
			   var str1  = document.forms['workTicketDataExtractsForm'].elements['beginDateW'].value;
			   var str2  = document.forms['workTicketDataExtractsForm'].elements['endDateW'].value;
			   var mySplitResult = str1.split("-");
   				var day = mySplitResult[0];
   				var month = mySplitResult[1];
  				 var year = mySplitResult[2];
		  				if(month == 'Jan')
		  {
		       month = "01";
		   }
		   else if(month == 'Feb')
		   {
		       month = "02";
		   }
		   else if(month == 'Mar')
		   {
		       month = "03"
		   }
		   else if(month == 'Apr')
		   {
		       month = "04"
		   }
		   else if(month == 'May')
		   {
		       month = "05"
		   }
		   else if(month == 'Jun')
		   {
		       month = "06"
		   }
		   else if(month == 'Jul')
		   {
		       month = "07"
		   }
		   else if(month == 'Aug')
		   {
		       month = "08"
		   }
		   else if(month == 'Sep')
		   {
		       month = "09"
		   }
		   else if(month == 'Oct')
		   {
		       month = "10"
		   }
		   else if(month == 'Nov')
		   {
		       month = "11"
		   }
		   else if(month == 'Dec')
		   {
		       month = "12";
		   }
		   var finalDate = month+"-"+day+"-"+year;
		   
		   var mySplitResult2 = str2.split("-");
		   var day2 = mySplitResult2[0];
		   var month2 = mySplitResult2[1];
		   var year2 = mySplitResult2[2];
		   if(month2 == 'Jan')
		   {
		       month2 = "01";
		   }
		   else if(month2 == 'Feb')
		   {
		       month2 = "02";
		   }
		   else if(month2 == 'Mar')
		   {
		       month2 = "03"
		   }
		   else if(month2 == 'Apr')
		   {
		       month2 = "04"
		   }
		   else if(month2 == 'May')
		   {
		       month2 = "05"
		   }
		   else if(month2 == 'Jun')
		   {
		       month2 = "06"
		   }
		   else if(month2 == 'Jul')
		   {
		       month2 = "07"
		   }
		   else if(month2 == 'Aug')
		   {
		       month2 = "08"
		   }
		   else if(month2 == 'Sep')
		   {
		       month2 = "09"
		   }
		   else if(month2 == 'Oct')
		   {
		       month2 = "10"
		   }
		   else if(month2 == 'Nov')
		   {
		       month2 = "11"
		   }
		   else if(month2 == 'Dec')
		   {
		       month2 = "12";
		   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  
  date2 = finalDate2.split("-");
   
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
    var daysApart = Math.round((sDate-eDate)/86400000);
	  
			   
			   
			   
			   
		   if(daysApart>0)
		   {
		      alert("End date cannot be greater than Begin date");
		      return false;
		   }
		}
	</script>
	
	
	<script language="JavaScript">
			
		 function formValidate()
		 {
		 	var actual = document.forms['workTicketDataExtractsForm'].elements['targetActualW'].value;
		 	var serv = document.forms['workTicketDataExtractsForm'].elements['serviceW'].value;
		 	var whouse = document.forms['workTicketDataExtractsForm'].elements['warehouseW'].value;
		 	var bdate = document.forms['workTicketDataExtractsForm'].elements['beginDateW'].value;
		 	var edate = document.forms['workTicketDataExtractsForm'].elements['endDateW'].value;
		 	var billStatus = document.forms['workTicketDataExtractsForm'].elements['billingStatus'].value;
		 	
		 	var a1 = document.forms['workTicketDataExtractsForm'].elements['conditionA1'].value;
		 	var a2 = document.forms['workTicketDataExtractsForm'].elements['conditionA2'].value;
		 	var a3 = document.forms['workTicketDataExtractsForm'].elements['conditionA3'].value;
		 	//var a4 = document.forms['workTicketDataExtractsForm'].elements['conditionA4'].value;
		 	//var a5 = document.forms['workTicketDataExtractsForm'].elements['conditionA5'].value;
		 	if(actual=='' && serv=='' && whouse=='' && bdate=='' && edate=='' && a1=='' && a2=='' && a3=='' && billStatus=='')
		 	{
		 		alert(" Please select minimum one search criteria.");
		 		return false;
		 	}	
		 		
		 	// value checking 
		 	if(a1 != "" && actual == ""){    	
    		alert("Ticket Status  value is blank, Please select.");    		
          	return false; 		
    		}
    		
    		if(a2 != "" && serv == ""){    	
    		alert("Service value is blank, Please select.");    		
          	return false; 		
    		}
    		
    		if(a3 != "" && whouse == ""){    	
    		alert("Warehouse value is blank, Please select.");    		
          	return false; 		
    		}  
    		//condition checking
    		if(actual != "" && a1 == "" ){    	
    		alert("Ticket Status  condition is blank, Please select.");    		
          	return false; 		
    		}
    		
    		if(serv != "" && a2 == ""){    	
    		alert("Service condition is blank, Please select.");    		
          	return false; 		
    		}
    		
    		if(whouse != ""&& a3 == "" ){    	
    		alert("Warehouse condition is blank, Please select.");    		
          	return false; 		
    		}  
		 	if(edate.length == 0 && bdate.length == 0 )
		 	{
		 		alert(" select begin and end date. ");
		 		return false;
		 	}else if(edate.length == 0 && bdate.length > 0 ){
		 		alert(" select end date. ");
		 		return false;
		 	}else if(edate.length > 0 && bdate.length == 0 ){
		 		alert(" select begin date. ");
		 		return false;
		 	}
    		return CompareDates();
		 }
		 
		 function winOpen(){
		 		openWindow('brokerPartners.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=vendorName&fld_code=vendorCode');
		 }
		 
		 function getVendorName(){
		     var vendorId = document.forms['workTicketDataExtractsForm'].elements['vendorCode'].value;
		     if(vendorId==''){
				 document.forms['workTicketDataExtractsForm'].elements['vendorName'].value = "";
			 }
			 if(vendorId!=''){
		     	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
		      	http2.open("GET", url, true);
		      	http2.onreadystatechange = handleHttpResponse5555;
		      	http2.send(null);
			 }
		 }
		 function handleHttpResponse5555(){
		              if (http2.readyState == 4){
		                 var results = http2.responseText
		                 results = results.trim();
		                 var res = results.split("#"); 
		                 if(res.length>2){
		                 	if(res[2] == 'Approved'){
		            				document.forms['workTicketDataExtractsForm'].elements['vendorName'].value = res[1];
		 	           		}else{
		 	           			alert("Vendor Code is not approved" ); 
		 					    document.forms['workTicketDataExtractsForm'].elements['vendorName'].value="";
		 				 		document.forms['workTicketDataExtractsForm'].elements['vendorCode'].value="";
		 	           		}
		                	}else{
		                      alert("Vendor Code not valid" );
		                  	 document.forms['workTicketDataExtractsForm'].elements['vendorName'].value="";
		 				 	 document.forms['workTicketDataExtractsForm'].elements['vendorCode'].value="";
		 			   }
		        }
		 }
		 var http2 = getHTTPObject();
		   function getHTTPObject(){
		    var xmlhttp;
		    if(window.XMLHttpRequest){
		        xmlhttp = new XMLHttpRequest();
		    }else if (window.ActiveXObject){
		        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		        if (!xmlhttp)
		        {
		            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		        }
		    }
		    return xmlhttp;
		}
	</script>
</head>

<s:form id="workTicketDataExtractsForm" name="workTicketDataExtractsForm" cssClass="form_magn" action="workTicketDataExtracts" method="post" validate="true"> 
<configByCorp:fieldVisibility componentId="component.field.Alternative.BillingPersonWorkTtExt">
		<s:hidden name="billingPersonFlag" value="Yes" />
	</configByCorp:fieldVisibility>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:100%">
<div id="otabs">
  <ul>
    <li><a class="current"><span>Extract Parameters</span></a></li>
  </ul>
</div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class=""  cellspacing="0" cellpadding="2" border="0" style="width:100%">
	  		
	  		<tr>
	  			<td  colspan="5">
	  			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px;cursor:pointer;">
				<tr>
					<td class="probg_left"></td>
					<td NOWRAP class="probg_center" style="padding-left:3%;">&nbsp;Field<span style="padding-left:4%;padding-right:10%;">Condition</span><span style="padding-left:10%;">Value</span></td>
					<td class="probg_right"></td>
				</tr>
				</table>
	  			</td>
	  	   </tr>
	  	   <tr>
	  			<td height="5px" colspan="5"></td>
	  		</tr>
	  		<tr>
	  		<td  class="listwhitetext" align="right" width="7%" height="21px">Vendor Code</td>
	  	   	<td  width="1%"></td>
	  		<td align="left"><s:textfield name="vendorCode" id="vendorCode" required="true" cssStyle="width:98px;" cssClass="input-text" size="11" maxlength="8" onchange="getVendorName();"/>
			<img class="openpopup" style="vertical-align:top;" width="17" height="20" onclick="javascript:winOpen()" id="openpopup1.img" src="<c:url value='/images/open-popup.gif'/>" />
			<div style="display: inline-block; float: right; line-height: 20px;" class="listwhitetext">Vendor Name</div>
			</td>
	  		<td align="left" width="" class="listwhitetext">
			<s:textfield name="vendorName" id="vendorName" cssStyle="width:238px;" cssClass="input-text" size="43" maxlength="250" readonly="true" /></td>
	  		</tr>
	  	   <tr>	  	   		
	  	   	    <td  class="listwhitetext" align="right" width="7%" height="21px">Ticket Status</td>
	  	   		<td  width="1%"></td>
	  			<td width="13%"><s:select cssClass="list-menu" cssStyle="width:100px" id="conditionA1" name="conditionA1" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>	  			
	  			
	  			<td ><s:select name="targetActualW" list="%{tcktactn}"  cssStyle="width:240px; !width:100px;" cssClass="list-menu" headerKey="" headerValue=""  tabindex="" /></td>	  				  		
	  		</tr>
	  		<tr>	  			
	  			<td  class="listwhitetext" align="right" height="21px">Service</td>
	  			<td  width="2px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:100px" id="conditionA2" name="conditionA2" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select name="serviceW" list="%{tcktservc}" cssStyle="width:240px; !width:240px;" cssClass="list-menu" headerKey="" headerValue="" /></td>			
	  				  		
				</tr>
	  		<tr>	  			
	  			<td  class="listwhitetext" align="right" height="21px">Warehouse</td>
	  			<td  width="2px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:100px" id="conditionA3" name="conditionA3" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td><s:select name="warehouseW" list="%{house}"  cssStyle="width:240px; !width:240px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
	  		</tr>
	  		<tr>	  			
	  			<td  class="listwhitetext" align="right" height="21px">Billing Status</td>
	  			<td  width="2px"></td>
	  			<td><s:select name="billingStatus" list="{'Billed','Unbilled'}"  cssStyle="width:100px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
	  		</tr>
	  		<tr>	  			
	  			<td  class="listwhitetext" align="right" height="21px">Begin Date from</td>
	  			<td  width="2px"></td>
	  			<s:hidden  id="conditionA4" name="conditionA4" value="Greater than Equal to"  />
	  			
	  			<td align="left" class="listwhitetext" style=""><s:textfield cssClass="input-text" id="beginDateW" name="beginDateW" value="%{beginDateW}" cssStyle="width:73px;" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)"/> 
		  		<img id="beginDateW_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			
	  		</tr>
	  		<tr>	  			
	  			<td  class="listwhitetext" align="right" height="21px">End date till</td>
	  			<td  width="2px"></td>
	  			<s:hidden id="conditionA5" name="conditionA5" value="Less than Equal to" />
	  			
	  			<td align="left" class="listwhitetext" style=""><s:textfield cssClass="input-text" id="endDateW" name="endDateW" value="%{endDateW}" cssStyle="width:73px;" maxlength="11" readonly="true"   onkeydown="return onlyDel(event,this)"/>
				 <img id="endDateW_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  
	  		</tr>
	  		
	  		<tr>	  			
	  			<td  class="listwhitetext" align="right" height="21px">Instruction Code</td>
	  			<td  width="2px"></td>
	  			<td align="left" class="listwhitetext" style="">
	  			<s:select name="instructionCode" list="%{woinstr}" cssStyle="width:322px" cssClass="list-menu" headerKey="" headerValue="" />
				 </td>
		  
	  		</tr>
	  		
	  		<tr>
	  			<td height="5px" colspan="5"></td>
	  		</tr>
	  		<tr>
   	           <td colspan="15" align="left" class="vertlinedata"></td>
   	        </tr>
   	        
   	        <tr><td height="5px"></td></tr>
   	        <tr>
   	       <td align="left" colspan="11" style="padding-left:17.2%">
   	       <s:submit cssClass="cssbutton" cssStyle="width:80px; height:27px" value="Extract" onclick = "return formValidate();"/>
   	       <s:reset cssClass="cssbutton" cssStyle="width:55px; height:27px" key="Reset" />
   	       </td>
   	       
			<tr>
	  			<td height="15px" colspan="4"></td>
	  		</tr>
     </table> 
    </div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>
</div>
<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="seventhDescription" />

</s:form>
 
<script type="text/javascript"> 
     try{
     buttonDisable();
   }
   catch(e){}
    Form.focusFirstElement($("reportForm")); 
</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>