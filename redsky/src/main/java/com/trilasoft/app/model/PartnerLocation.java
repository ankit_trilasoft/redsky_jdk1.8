package com.trilasoft.app.model;

public class PartnerLocation {
	
	private double latitude;
	private double longitude;
	private String name;
	private String address;
	private String phone;
	private String url;
	private String facilityPhoto;
	

	public void setLatitude(double latitude) {
		this.latitude = latitude;
		
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFacilityPhoto() {
		return facilityPhoto;
	}

	public void setFacilityPhoto(String facilityPhoto) {
		this.facilityPhoto = facilityPhoto;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	
}
