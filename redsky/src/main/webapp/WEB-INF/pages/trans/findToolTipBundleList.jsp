<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
<title>Bundle List</title>   
<meta name="heading" content="Tool Tip"/> 
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	<td align="left"><b></b></td>
	<td align="right">
		<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table> 
<display:table name="emailList" class="table" requestURI="" id="emailList" style="min-width:250px;">
	<display:column  title="Bundle&nbsp;Name" style="width:90px;">
		<a><div align="left" onclick="documentBundleWinOpen('${emailList.emailValue}','${emailList.shipNumber}','${emailList.firstName}','${emailList.lastName}','${emailList.formsId}','${emailList.dbId}','${emailList.sequenceNumber}','${emailList.clickedField}');ajax_hideTooltip();" >
			<c:out value="${emailList.bundleName}" /></div></a>
		</display:column>
	<display:column  title="Email" property="emailValue"></display:column>	
</display:table>

