package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.KeySurveyQuestionMap;

public interface KeySurveyQuestionMapManager extends GenericManager<KeySurveyQuestionMap, Long>{

}
