package com.trilasoft.app.webapp.action;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.transaction.UserTransaction;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.service.UserExistsException;
import org.appfuse.webapp.action.BaseAction;
import org.bouncycastle.asn1.ocsp.Request;

import com.trilasoft.app.model.AccountAssignmentType;
import com.trilasoft.app.model.ContractPolicy;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DataSecurityFilter;
import com.trilasoft.app.model.DataSecuritySet;
import com.trilasoft.app.model.Entitlement;
import com.trilasoft.app.model.FrequentlyAskedQuestions;
import com.trilasoft.app.model.MyMessage;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.PolicyDocument;
import com.trilasoft.app.model.QualitySurveySettings;
import com.trilasoft.app.service.AccountAssignmentTypeManager;
import com.trilasoft.app.service.ContractPolicyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DataSecurityFilterManager;
import com.trilasoft.app.service.DataSecuritySetManager;
import com.trilasoft.app.service.EntitlementManager;
import com.trilasoft.app.service.FrequentlyAskedQuestionsManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.PolicyDocumentManager;
import com.trilasoft.app.service.QualitySurveySettingsManager;
import com.trilasoft.app.service.RefMasterManager;

public class ContractPolicyAction extends BaseAction {
	
	private ContractPolicyManager contractPolicyManager;
	private PartnerManager partnerManager;
	private ContractPolicy contractPolicy;
	//private Partner partner;
	private PartnerPublic partnerPolicy;
	private PartnerPublic partner;
	private PartnerPrivate partnerPrivate;
	private PartnerPrivateManager partnerPrivateManager;
	private PartnerPublicManager partnerPublicManager;
	private PolicyDocumentManager policyDocumentManager;
	private Long id;
	private List contractPolicys;
	private String code;
	private List policyFileList;
	private RefMasterManager refMasterManager;

	private  Map<String,String>languageList;
	private  Map<String,String>sectionNameList;
	private List policyDocumentList;
	private Long id1;
	private Long id2;
	private String hitFlag;
	private String userCorpID;
	private String dummsectionName;
	private String dummlanguage;
	private String documentSectionName;
	private String documentLanguage;
	private String checkedPrivateOption;
	private String agentParent;
	private String pagePrivateListType;
	private String pagePrsListType;
	private String pageCPFListType;
	private String pageFaqInfo;
	private DataSecurityFilterManager dataSecurityFilterManager;
	private DataSecuritySetManager dataSecuritySetManager;
	private DataSecuritySet dataSecuritySet;
	private DataSecurityFilter dataSecurityFilter;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(ContractPolicyAction.class);
 

	public void setContractPolicyManager(ContractPolicyManager contractPolicyManager) {
        this.contractPolicyManager = contractPolicyManager;
    }
	
	public void setPartnerManager(PartnerManager partnerManager) {
        this.partnerManager = partnerManager;
    }
	
	 public List getContractPolicys() { 
	    	return contractPolicys;   
	    }
	 
	 public String list() { 
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			contractPolicys = contractPolicyManager.getAll();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS;   
	    }

	 public void setId(Long id) {
			this.id = id;
		}
	 
	 public void setCode(String code) {
			this.code = code;
		}
	 
	public ContractPolicy getContractPolicy() {
		return contractPolicy;
	}

	public void setContractPolicy(ContractPolicy contractPolicy) {
		this.contractPolicy = contractPolicy;
	}
	
	public PartnerPublic getPartner() {   
        return partner;   
    }   
      
    public void setPartner(PartnerPublic partner) {   
        this.partner = partner;   
    }
    
    private String sessionCorpID;
    private String usertype; 
    public ContractPolicyAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		usertype=user.getUserType();
	}
	public String uploadPolicyFile(){
		
		return SUCCESS;	
	}
	public String uploadAdminPolicyFile(){
		
		return SUCCESS;	
	}
    public String getComboList(String corpID){ 

		languageList=refMasterManager.findByParameter(corpID, "LANGUAGE");
    	sectionNameList=refMasterManager.findDocumentList(corpID, "SECTIONNAME");

	 	 return SUCCESS;
	}
    @SkipValidation
    public String editList() {
		getComboList(sessionCorpID);
        policyFileList=contractPolicyManager.policyFilesList(sessionCorpID);
        return SUCCESS;   
    }  
	public String edit() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		User userPolicy = userManager.getUserByUsername(getRequest().getRemoteUser()); 
		userCorpID = userPolicy.getCorpID();
		 if (id != null) { 
        	partner = partnerPublicManager.get(id);
        	PartnerPublic parentCorpId = partnerPublicManager.getPartnerByCode(partner.getAgentParent());
        	partnerPrivate = (PartnerPrivate)partnerPrivateManager.findPartnerCode(partner.getPartnerCode()).get(0);
        	if(partner.getPartnerType()!=null && (!(partner.getPartnerType().toString().trim().equals("")))  && partner.getStatus().equalsIgnoreCase("Approved") && (partner.getPartnerType().equalsIgnoreCase("CMM") || partner.getPartnerType().equalsIgnoreCase("DMM"))){
        		List oldPolicyDocList=null; 
        		if(usertype.equals("ACCOUNT")){        			
        	policyFileList=contractPolicyManager.findPolicyFileNewList(partner.getPartnerCode(),partner.getCorpID()); 			
        	if(!sessionCorpID.equals(partnerPrivate.getCorpID())){	 
   			 oldPolicyDocList = contractPolicyManager.findPolicyFileNewList(partner.getPartnerCode(),sessionCorpID);		 
   			 }	 
   			 if(oldPolicyDocList!=null && !oldPolicyDocList.isEmpty()){
	  	        			Iterator<PolicyDocument> it = oldPolicyDocList.iterator();
	  	        			while(it.hasNext()){
	  	        				policyFileList.add(it.next());
	  	        			}
	       			}		
        		}
        		else{
        			 policyFileList=contractPolicyManager.findPolicyFileList(partner.getPartnerCode(),partner.getCorpID());
        			 if(!sessionCorpID.equals(partnerPrivate.getCorpID())){
        			 oldPolicyDocList = contractPolicyManager.findPolicyFileList(partner.getPartnerCode(),sessionCorpID);
        			 }
        			 if(oldPolicyDocList!=null && !oldPolicyDocList.isEmpty()){
	  	        			Iterator<PolicyDocument> it = oldPolicyDocList.iterator();
	  	        			while(it.hasNext()){
	  	        				policyFileList.add(it.next());
	  	        			}
	       			}
        			
        		}		
        		
        		
        	}
        	else{
        		 //List policyFileList;
        		 List oldPolicyDocList=null;        		
        			 if(usertype.equals("ACCOUNT")){
        			 policyFileList=contractPolicyManager.findPolicyFileNewList(partner.getPartnerCode(),sessionCorpID);
        			 if(!sessionCorpID.equals(partnerPrivate.getCorpID())){	 
        			 oldPolicyDocList = contractPolicyManager.findPolicyFileNewList(partner.getPartnerCode(),parentCorpId.getCorpID());		 
        			 }	 
        			 if(oldPolicyDocList!=null && !oldPolicyDocList.isEmpty()){
  	  	        			Iterator<PolicyDocument> it = oldPolicyDocList.iterator();
  	  	        			while(it.hasNext()){
  	  	        				policyFileList.add(it.next());
  	  	        			}
  	       			}
        			 }
        			 
        			 else{
        			 policyFileList=contractPolicyManager.findPolicyFileList(partner.getPartnerCode(),sessionCorpID);
        			 if(!sessionCorpID.equals(partnerPrivate.getCorpID())){
        			 oldPolicyDocList = contractPolicyManager.findPolicyFileList(partner.getPartnerCode(),parentCorpId.getCorpID());
        			 }
        			 if(oldPolicyDocList!=null && !oldPolicyDocList.isEmpty()){
	  	        			Iterator<PolicyDocument> it = oldPolicyDocList.iterator();
	  	        			while(it.hasNext()){
	  	        				policyFileList.add(it.next());
	  	        			}
	       			}
        			 }
	       			/**if(!oldPolicyDocList.isEmpty()){
	  	        			Iterator<PolicyDocument> it = oldPolicyDocList.iterator();
	  	        			while(it.hasNext()){
	  	        				policyFileList.add(it.next());
	  	        			}
	       			}
        		 }*/
        	}
        	/*if(contractPolicyManager.checkById(partner.getPartnerCode()).isEmpty()){
        		contractPolicy = new ContractPolicy(); 
        		contractPolicy.setId(partner.getId());
        		contractPolicy.setPartnerCode(partner.getPartnerCode());
        		contractPolicy.setCreatedOn(new Date());
        		contractPolicy.setUpdatedOn(new Date());
        		contractPolicy.setCorpID(sessionCorpID);
        		contractPolicy.setUpdatedBy(getRequest().getRemoteUser());
        		contractPolicy.setCreatedBy(getRequest().getRemoteUser());
        	}else{
        		contractPolicy = contractPolicyManager.get(id);   
        		contractPolicy.setCorpID(partner.getCorpID());
        		//contractPolicy.setUpdatedOn(new Date());
        		
        		
        	}*/
        }
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }  
	public String fileDelete()throws Exception{
		contractPolicyManager.deleteChildDocument(id2,sessionCorpID);
		contractPolicyManager.remove(id2);		
	  	contractPolicyManager.deletedDocument(documentSectionName,documentLanguage,partnerCode,sessionCorpID);
	  	String key =" deleted successfully";
	  	saveMessage(getText(key));
	  	edit();
	  	hitFlag="7";
		 return SUCCESS;
	}
	public String fileAdminDelete()throws Exception{
		contractPolicyManager.remove(id2);
		contractPolicyManager.deletedDocument(documentSectionName,documentLanguage,"",sessionCorpID);
	  	String key =" deleted successfully";
	  	saveMessage(getText(key));
		 return SUCCESS;
	}
	public String documentDeleteFile()throws Exception{
		contractPolicyManager.deleteChildDocumentFromPolicy(id2,sessionCorpID);
		policyDocumentManager.remove(id2);
		String key =" deleted successfully";
	  	saveMessage(getText(key));
	  	addPolicyFile();
	  	hitFlag="4";
		 return SUCCESS;
	}
	public String policyAdminDocDelete()throws Exception{
		policyDocumentManager.remove(id2);
		String key =" deleted successfully";
	  	saveMessage(getText(key));
	  	addPolicyAdminFiles();
	  	 hitFlag="9";
		 return SUCCESS;
	}
	private String partnerType;
	private Long dummSeqNumber;
	public Long getDummSeqNumber() {
		return dummSeqNumber;
	}

	public void setDummSeqNumber(Long dummSeqNumber) {
		this.dummSeqNumber = dummSeqNumber;
	}

	public String addPolicyFile(){
		getComboList(sessionCorpID);
		User userPolicy = userManager.getUserByUsername(getRequest().getRemoteUser()); 
		userCorpID = userPolicy.getCorpID();
		partnerType=getRequest().getParameter("partnerType");
		
		    partner = partnerPublicManager.get(id1);
		
              if(id==null){
        		contractPolicy = new ContractPolicy();
        		//policyDocumentList=contractPolicyManager.findDocumentList(partner.getPartnerCode(),contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
        		//contractPolicy.setId(partner.getId());
        		contractPolicy.setPartnerCode(partner.getPartnerCode());
        		contractPolicy.setCreatedOn(new Date());
        		contractPolicy.setUpdatedOn(new Date());
        		contractPolicy.setCorpID(sessionCorpID);
        		contractPolicy.setUpdatedBy(getRequest().getRemoteUser());
        		contractPolicy.setCreatedBy(getRequest().getRemoteUser());
        		
        	}else{
        		contractPolicy = contractPolicyManager.get(id);
        		if(partner.getPartnerType()!=null && (!(partner.getPartnerType().toString().trim().equals("")))  && partner.getStatus().equalsIgnoreCase("Approved") && (partner.getPartnerType().equalsIgnoreCase("CMM") || partner.getPartnerType().equalsIgnoreCase("DMM"))){
        			policyDocumentList=contractPolicyManager.findDocumentList(partner.getPartnerCode(),contractPolicy.getSectionName(),contractPolicy.getLanguage(),partner.getCorpID());
        	     	partnerPrivate = (PartnerPrivate)partnerPrivateManager.findPartnerCode(partner.getPartnerCode()).get(0);
        			if(!sessionCorpID.equals(partnerPrivate.getCorpID())){
	        			List oldPolicyDocList = contractPolicyManager.findDocumentList(partner.getPartnerCode(),contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
		      			if(!oldPolicyDocList.isEmpty()){
		 	        			Iterator it = oldPolicyDocList.iterator();
		 	        			while(it.hasNext()){
		 	        				policyDocumentList.add(it.next());
		 	        			}
		      			}
        			}
	           	}
	           	else{
	           		PartnerPublic parentDocList = partnerPublicManager.getPartnerByCode(partner.getAgentParent());
        	     	partnerPrivate = (PartnerPrivate)partnerPrivateManager.findPartnerCode(partner.getPartnerCode()).get(0);
	           		policyDocumentList=contractPolicyManager.findDocumentList(partner.getPartnerCode(),contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
	           		if(!sessionCorpID.equals(partnerPrivate.getCorpID())){
		           		List oldPolicyDocList = contractPolicyManager.findDocumentList(partner.getPartnerCode(),contractPolicy.getSectionName(),contractPolicy.getLanguage(),parentDocList.getCorpID());
		      			if(!oldPolicyDocList.isEmpty()){
		 	        			Iterator it = oldPolicyDocList.iterator();
		 	        			while(it.hasNext()){
		 	        				policyDocumentList.add(it.next());
		 	        			}
		      			}
	           		}
	           	}
        		dummsectionName=contractPolicy.getSectionName();
        		dummlanguage=contractPolicy.getLanguage();
        		dummSeqNumber=contractPolicy.getDocSequenceNumber();
        		contractPolicy.setLanguage(contractPolicy.getLanguage());
        		contractPolicy.setSectionName(contractPolicy.getSectionName());
        		contractPolicy.setPolicy(contractPolicy.getPolicy());
        		
        	}
        
		return SUCCESS;	
	}
	public String addPolicyAdminFiles(){
		getComboList(sessionCorpID);
		User userPolicy = userManager.getUserByUsername(getRequest().getRemoteUser()); 
		userCorpID = userPolicy.getCorpID();
		partnerType=getRequest().getParameter("partnerType");
		   // partner = partnerPublicManager.get(id1);
              if(id==null){
        		contractPolicy = new ContractPolicy();
        		//policyDocumentList=contractPolicyManager.findDocumentList(partner.getPartnerCode(),contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
        		//contractPolicy.setId(partner.getId());
        		//contractPolicy.setPartnerCode(partner.getPartnerCode());
        		contractPolicy.setCreatedOn(new Date());
        		contractPolicy.setUpdatedOn(new Date());
        		contractPolicy.setCorpID(sessionCorpID);
        		contractPolicy.setUpdatedBy(getRequest().getRemoteUser());
        		contractPolicy.setCreatedBy(getRequest().getRemoteUser());
        	}else{
        		contractPolicy = contractPolicyManager.get(id);
        		dummsectionName=contractPolicy.getSectionName();
        		policyDocumentList=contractPolicyManager.documentFileList(contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
        		dummlanguage=contractPolicy.getLanguage();
        		contractPolicy.setLanguage(contractPolicy.getLanguage());
        		contractPolicy.setSectionName(contractPolicy.getSectionName());
        		contractPolicy.setPolicy(contractPolicy.getPolicy());
        	}
        
		return SUCCESS;	
	}
	public String save() throws Exception {
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		boolean isNew = (contractPolicy.getId() == null);  
		if(contractPolicy.getSectionName().equalsIgnoreCase("")|| contractPolicy.getSectionName().equalsIgnoreCase(null)){
			errorMessage("Section Name is required field.");
			//hitFlag="8";
			return INPUT;	
		}
		 if(contractPolicy.getLanguage().equalsIgnoreCase("")||contractPolicy.getLanguage().equalsIgnoreCase(null)){
			 errorMessage("Language is required field.");
				//hitFlag="8";
				return INPUT; 
		 }
		 if(contractPolicy.getDocSequenceNumber()==null || contractPolicy.getDocSequenceNumber().toString().trim().equals("")){
			 errorMessage("Sequence is required field.");
				//hitFlag="8";
				return INPUT; 
		 }
		if(contractPolicy.getPolicy().equalsIgnoreCase("")|| contractPolicy.getPolicy().equalsIgnoreCase(null)|| contractPolicy.getPolicy().equalsIgnoreCase("<br>")){
			errorMessage("Description is required field.");
			//hitFlag="8";
			return INPUT;
		}
		partnerPolicy = partnerPublicManager.getPartnerByCode(partner.getPartnerCode());
		if(partnerPolicy.getPartnerType()!=null && (!(partnerPolicy.getPartnerType().toString().trim().equals("")))  
				&& partnerPolicy.getStatus().equalsIgnoreCase("Approved") 
				&& (partnerPolicy.getPartnerType().equalsIgnoreCase("CMM") || partnerPolicy.getPartnerType().equalsIgnoreCase("DMM"))){
			sessionCorpID = partnerPolicy.getCorpID();
    	}
		 int i=contractPolicyManager.checkSectionName(contractPolicy.getSectionName(),contractPolicy.getLanguage(),partner.getPartnerCode(),sessionCorpID);
		 int j = contractPolicyManager.checkSequenceNumber(contractPolicy.getDocSequenceNumber(), contractPolicy.getLanguage(), partner.getPartnerCode(), sessionCorpID);
		 if(j>0 && isNew ){
			 errorMessage("Policy with the same language and sequence # already exists.");
    		return INPUT;	
		      }
		 Long seqNumberDumm=contractPolicy.getDocSequenceNumber();
         if(j>0 && !isNew && !dummSeqNumber.equals(seqNumberDumm)){
			 errorMessage("Policy with the same language and sequence # already exists.");
  		return INPUT;	
		      }
		 if(i>0 && isNew ){
        	errorMessage("Section Name and  Language already exist In Info Package ");
        	 policyDocumentList=contractPolicyManager.findDocumentList(partner.getPartnerCode(),contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
    		return INPUT;
                       }
        String sectionNameDumm=contractPolicy.getSectionName();
        String languageDumm=contractPolicy.getLanguage();
		 if((!dummsectionName.equals(sectionNameDumm))||(!dummlanguage.equals(languageDumm))){
		   if(i>0){
			 errorMessage("Section Name and  Language already exist In Info Package ");
        	// hitFlag="8";
        	 //policyDocumentList=contractPolicyManager.findDocumentList(partner.getPartnerCode(),contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
    		return INPUT;	
		     } 
		 }
		 if(contractPolicyManager.checkById(partner.getPartnerCode()).isEmpty()){
        	contractPolicy.setCreatedOn(new Date());
		}
        //contractPolicy.setId(partner.getId());
        contractPolicy.setPartnerCode(partner.getPartnerCode());
        
        if(partnerPolicy.getPartnerType()!=null && (!(partnerPolicy.getPartnerType().toString().trim().equals("")))  && partnerPolicy.getStatus().equalsIgnoreCase("Approved") && (partnerPolicy.getPartnerType().equalsIgnoreCase("CMM") || partnerPolicy.getPartnerType().equalsIgnoreCase("DMM"))){
        	contractPolicy.setCorpID(partnerPolicy.getCorpID());
        }
        else{
        	contractPolicy.setCorpID(sessionCorpID);
        }
        contractPolicy.setUpdatedOn(new Date());
        contractPolicy.setUpdatedBy(getRequest().getRemoteUser());
        contractPolicy=contractPolicyManager.save(contractPolicy);
        contractPolicyManager.updatePolicyDocument(dummsectionName,dummlanguage,partner.getPartnerCode(),contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
        policyDocumentList=contractPolicyManager.findDocumentList(partner.getPartnerCode(),contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
        String key = (isNew) ? "Info Package has been added successfully." : "Info Package has been updated successfully.";   
        saveMessage(getText(key));   
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		hitFlag="1";
		//addPolicyFile();
            return SUCCESS;   
       
    }  
     public String savePolicy() throws Exception {
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		boolean isNew = (contractPolicy.getId() == null);  
		if(contractPolicy.getSectionName().equalsIgnoreCase("")|| contractPolicy.getSectionName().equalsIgnoreCase(null)){
			errorMessage("Section Name is required field.");
			//hitFlag="8";
			return INPUT;	
		}
		 if(contractPolicy.getLanguage().equalsIgnoreCase("")||contractPolicy.getLanguage().equalsIgnoreCase(null)){
			 errorMessage("Language is required field.");
				//hitFlag="8";
				return INPUT; 
		 }
		if(contractPolicy.getPolicy().equalsIgnoreCase("")|| contractPolicy.getPolicy().equalsIgnoreCase(null)|| contractPolicy.getPolicy().equalsIgnoreCase("<br>")){
			errorMessage("Description is required field.");
			//hitFlag="8";
			return INPUT;
		}
	   int i=contractPolicyManager.sectionNameCheck(contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
	   if(i>0 && isNew ){
		  policyDocumentList=contractPolicyManager.documentFileList(contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
    	errorMessage("Section Name and  Language already exist In Info Package ");
		return INPUT;
                   }
	   String sectionNameDumm=contractPolicy.getSectionName();
       String languageDumm=contractPolicy.getLanguage();
       policyDocumentList=contractPolicyManager.documentFileList(contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
		 if((!dummsectionName.equals(sectionNameDumm))||(!dummlanguage.equals(languageDumm))){
		   if(i>0){
			 errorMessage("Section Name and  Language already exist In Info Package ");
       
   		return INPUT;	
		     }
		 }
	
	//contractPolicy.setId(partner.getId());
        //contractPolicy.setPartnerCode(partner.getPartnerCode());
     /*   partnerPolicy = partnerPublicManager.getPartnerByCode(partner.getPartnerCode());
        if(partnerPolicy.getPartnerType()!=null && (!(partnerPolicy.getPartnerType().toString().trim().equals("")))  && partnerPolicy.getStatus().equalsIgnoreCase("Approved") && (partnerPolicy.getPartnerType().equalsIgnoreCase("CMM") || partnerPolicy.getPartnerType().equalsIgnoreCase("DMM"))){
        	contractPolicy.setCorpID(partnerPolicy.getCorpID());
        }
        else{
        	contractPolicy.setCorpID(sessionCorpID);
        }*/
		 contractPolicy.setCorpID(sessionCorpID);
        contractPolicy.setUpdatedOn(new Date());
        contractPolicy.setUpdatedBy(getRequest().getRemoteUser());
        contractPolicy=contractPolicyManager.save(contractPolicy);
        contractPolicyManager.updatePolicyDocument(dummsectionName,dummlanguage," ",contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
        policyDocumentList=contractPolicyManager.documentFileList(contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
        String key = (isNew) ? "Info Package has been added successfully." : "Info Package has been updated successfully.";   
        saveMessage(getText(key));   
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		hitFlag="1";
		//addPolicyFile();
            return SUCCESS;   
       
    }  
	
    private String jobNumber;
    private CustomerFileManager  customerFileManager;
    private String moveTypeVal;
    private CustomerFile customerFile;
    private Long cid;
	public String showAccountPolicy() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(code == null){
			code = "";
		}
        if(contractPolicyManager.getContractPolicyRef(code,sessionCorpID).isEmpty()){
        	contractPolicy = new ContractPolicy(); 
        }else{
        	contractPolicy = contractPolicyManager.get(Long.parseLong(((contractPolicyManager.getContractPolicyRef(code,sessionCorpID)).get(0)).toString()));
        	policyDocumentList=contractPolicyManager.findDocumentList(contractPolicy.getPartnerCode(),contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
        }
        List<CustomerFile> cfList = customerFileManager.findSequenceNumber(jobNumber);
        if(cfList!=null && !cfList.equals("")){
	        for(CustomerFile cf:cfList){
	        	moveTypeVal = cf.getMoveType();
	        }
        }
        if(cid!=null){
	        customerFile=customerFileManager.get(cid);
	    	moveTypeVal = customerFile.getMoveType();
        }
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }
	
	public String showAccountPolicy1() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(code == null){
			code = "";
		}
/*        if(contractPolicyManager.checkById(code).isEmpty()){
        	contractPolicy = new ContractPolicy(); 
        }else{
        	contractPolicy = contractPolicyManager.get(Long.parseLong(((contractPolicyManager.checkById(code)).get(0)).toString()));   
        }*/
        if(contractPolicyManager.getContractPolicyRef(code,sessionCorpID).isEmpty()){
        	contractPolicy = new ContractPolicy(); 
        }else{
        	contractPolicy = contractPolicyManager.get(Long.parseLong(((contractPolicyManager.getContractPolicyRef(code,sessionCorpID)).get(0)).toString()));
        	policyDocumentList=contractPolicyManager.findDocumentList(contractPolicy.getPartnerCode(),contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
        }
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;   
    }
	
	private String gotoPageString;
	private String validateFormNav;
	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	public String getGotoPageString() {

	return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

	this.gotoPageString = gotoPageString;

	}
	private String excludeFPU;
	private String partnerCode;
	private String excludeOption;
	public String updatePartnerPrivate()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
		int rec=partnerPrivateManager.updatePartnerPrivate(excludeFPU,sessionCorpID,partnerCode,excludeOption);
		}catch(Exception e){e.printStackTrace();}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String childAffected;
	private String pageListType;
	private String checkedOption;
	private FrequentlyAskedQuestionsManager frequentlyAskedQuestionsManager;
	private QualitySurveySettingsManager qualitySurveySettingsManager;
	private EntitlementManager entitlementManager;
	private List maxSequenceNumber;
	private Long autoSequenceNumber;
	private AccountAssignmentTypeManager accountAssignmentTypeManager;
	private String partnerId;
	
	public String updateChildFromParent()
	{ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		PartnerPublic partnerPublicChildUpdate = partnerPublicManager.getPartnerByCode(partnerCode);
			try{
				childAffected="No Child account is available for this account";
				  if(pageListType.equalsIgnoreCase("CPF"))
				  {
							  BeanUtilsBean beanUtilsContract = BeanUtilsBean.getInstance();
							  beanUtilsContract.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
							  List partnerCodeList = new ArrayList();
							  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
								  policyFileList=contractPolicyManager.findPolicyFileList(partnerCode,partnerPublicChildUpdate.getCorpID());
								  
							  }
							  else{
								  policyFileList=contractPolicyManager.findPolicyFileList(partnerCode,sessionCorpID);
							  }
							  partnerCodeList=partnerManager.getPartnerIncludeChildAgentsList(partnerCode,sessionCorpID,pageListType);
							  if(!(policyFileList.isEmpty())&&(policyFileList!=null))
							  {
							  Iterator it = partnerCodeList.iterator();
							  while(it.hasNext())
							  {
								  String partnerCode=(String)it.next();
								  
								  /**
								   * delete policy from child then insert new policy records.
								   */
								  int i = contractPolicyManager.deletedPolicyContract(partnerCode, sessionCorpID);
							  }
							  }
							  String parentPartnerCode=partnerCode;
							  if(!(partnerCodeList.isEmpty())&&(partnerCodeList!=null))
							  {
								  if(!(policyFileList.isEmpty())&&(policyFileList!=null))
								  {
									  Iterator itrPolicy = policyFileList.iterator();
									  while(itrPolicy.hasNext())
									  {
										  ContractPolicy contractPolicy=(ContractPolicy)itrPolicy.next();
										  Iterator itrPolicy1 = partnerCodeList.iterator();
										  while(itrPolicy1.hasNext())
										  {
											  //List contractPolicyTempe = new ArrayList();
											  String partnerCode=(String)itrPolicy1.next();
											  
/*											  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
												  contractPolicyTempe=contractPolicyManager.getPolicyReference(partnerCode, partnerPublicChildUpdate.getCorpID(),contractPolicy.getId());	
											  }
											  else{
												  contractPolicyTempe=contractPolicyManager.getPolicyReference(partnerCode, sessionCorpID,contractPolicy.getId());	
											  }
											  										  
											  if((contractPolicyTempe!=null)&&(!contractPolicyTempe.isEmpty()))
											  {
												  if(contractPolicyTempe.get(0)!=null)
												  {		
													  String cid=contractPolicyTempe.get(0).toString();
													  ContractPolicy newContractPolicy=contractPolicyManager.get(Long.parseLong(cid));
													  Long tempId=newContractPolicy.getId();
													  beanUtilsContract.copyProperties(newContractPolicy, contractPolicy);
													  newContractPolicy.setPartnerCode(partnerCode);
													  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
														  newContractPolicy.setCorpID(partnerPublicChildUpdate.getCorpID());
													  }
													  else{
														  newContractPolicy.setCorpID(sessionCorpID);
													  }
													  newContractPolicy.setParentId(contractPolicy.getId());
													  newContractPolicy.setId(tempId);
	 									        	  contractPolicyManager.save(newContractPolicy);
											          childAffected="All Child Accounts have been updated";
											          List policyDocumentList2 = new ArrayList();
											          if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
											        	  policyDocumentList2=contractPolicyManager.findDocumentList(parentPartnerCode,contractPolicy.getSectionName(),contractPolicy.getLanguage(),partnerPublicChildUpdate.getCorpID());
													  }
													  else{
														  policyDocumentList2=contractPolicyManager.findDocumentList(parentPartnerCode,contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
													  }
											          
											          Iterator itrDocPolicy = policyDocumentList2.iterator();
											          while(itrDocPolicy.hasNext())
											          {
											        	 PolicyDocument policyDocument=(PolicyDocument)itrDocPolicy.next();
											        	 List policyDocumentTempe = new ArrayList();
												          if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
												        	  policyDocumentTempe=contractPolicyManager.getPolicyDocReference(partnerCode,partnerPublicChildUpdate.getCorpID(),policyDocument.getId(),policyDocument.getSectionName(),policyDocument.getLanguage());
														  }
														  else{
															  policyDocumentTempe=contractPolicyManager.getPolicyDocReference(partnerCode,sessionCorpID,policyDocument.getId(),policyDocument.getSectionName(),policyDocument.getLanguage());
														  }
														  if((policyDocumentTempe!=null)&&(!policyDocumentTempe.isEmpty()))
														  {
															  if(policyDocumentTempe.get(0)!=null)
															  {
																 String pid=policyDocumentTempe.get(0).toString();
																 PolicyDocument newPolicyDocument=policyDocumentManager.get(Long.parseLong(pid));
																 Long tempId1=newPolicyDocument.getId();
											        			 beanUtilsContract.copyProperties(newPolicyDocument, policyDocument);
											        			 newPolicyDocument.setPartnerCode(partnerCode);
											        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
											        				 newPolicyDocument.setCorpID(partnerPublicChildUpdate.getCorpID());
																  }
																  else{
																	  newPolicyDocument.setCorpID(sessionCorpID);
																  }
											        			 newPolicyDocument.setParentId(policyDocument.getId());
											        			 newPolicyDocument.setId(tempId1);
											        			 newPolicyDocument=policyDocumentManager.save(newPolicyDocument);
												        		 childAffected="All Child Accounts have been updated";
																	
																}
											             }else{
										            	 		 PolicyDocument newPolicyDocument= new PolicyDocument();
											        			 beanUtilsContract.copyProperties(newPolicyDocument, policyDocument);
											        			 newPolicyDocument.setPartnerCode(partnerCode);
											        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
											        				 newPolicyDocument.setCorpID(partnerPublicChildUpdate.getCorpID());
																  }
																  else{
																	  newPolicyDocument.setCorpID(sessionCorpID);
																  }
											        			 newPolicyDocument.setCreatedBy(getRequest().getRemoteUser());
											        			 newPolicyDocument.setUpdatedBy(getRequest().getRemoteUser());
											        			 newPolicyDocument.setCreatedOn(new Date());
											        			 newPolicyDocument.setUpdatedOn(new Date());
											        			 newPolicyDocument.setParentId(policyDocument.getId());
											        			 newPolicyDocument.setId(null);
											        			 newPolicyDocument=policyDocumentManager.save(newPolicyDocument);
												        		 childAffected="All Child Accounts have been updated";
											             }
													 }
													}												  
											  }else{*/
												  ContractPolicy newContractPolicy= new ContractPolicy();
												  beanUtilsContract.copyProperties(newContractPolicy, contractPolicy);
												  newContractPolicy.setPartnerCode(partnerCode);
												  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
													  newContractPolicy.setCorpID(partnerPublicChildUpdate.getCorpID());
												  }
												 else{
													 newContractPolicy.setCorpID(sessionCorpID);
												 }
												  newContractPolicy.setCreatedBy(getRequest().getRemoteUser());
												  newContractPolicy.setUpdatedBy(getRequest().getRemoteUser());
												  newContractPolicy.setCreatedOn(new Date());
												  newContractPolicy.setUpdatedOn(new Date());
												  newContractPolicy.setParentId(contractPolicy.getId());
												  newContractPolicy.setId(null);
 									        	  contractPolicyManager.save(newContractPolicy);
										          childAffected="All Child Accounts have been updated";
										          List policyDocumentList2 = new ArrayList();
										          if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
										        	  policyDocumentList2=contractPolicyManager.findDocumentList(parentPartnerCode,contractPolicy.getSectionName(),contractPolicy.getLanguage(),partnerPublicChildUpdate.getCorpID());
												  }
												  else{
													  policyDocumentList2=contractPolicyManager.findDocumentList(parentPartnerCode,contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
												  }
										          
										          Iterator itrDocPolicy = policyDocumentList2.iterator();
										          while(itrDocPolicy.hasNext())
										          {
										        	 PolicyDocument policyDocument=(PolicyDocument)itrDocPolicy.next();
										        	 List policyDocumentTempe = new ArrayList();
											          if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
											        	  policyDocumentTempe=contractPolicyManager.getPolicyDocReference(partnerCode,partnerPublicChildUpdate.getCorpID(),policyDocument.getId(),policyDocument.getSectionName(),policyDocument.getLanguage());
													  }
													  else{
														  policyDocumentTempe=contractPolicyManager.getPolicyDocReference(partnerCode,sessionCorpID,policyDocument.getId(),policyDocument.getSectionName(),policyDocument.getLanguage());
													  }
										        	 
													  if((policyDocumentTempe!=null)&&(!policyDocumentTempe.isEmpty()))
													  {
														  if(policyDocumentTempe.get(0)!=null)
														  {
															 String pid=policyDocumentTempe.get(0).toString();
															 PolicyDocument newPolicyDocument=policyDocumentManager.get(Long.parseLong(pid));
															 Long tempId1=newPolicyDocument.getId();
										        			 beanUtilsContract.copyProperties(newPolicyDocument, policyDocument);
										        			 newPolicyDocument.setPartnerCode(partnerCode);
										        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
										        				 newPolicyDocument.setCorpID(partnerPublicChildUpdate.getCorpID());
															  }
															 else{
																 newPolicyDocument.setCorpID(sessionCorpID);
															 }
										        			 newPolicyDocument.setParentId(policyDocument.getId());
										        			 newPolicyDocument.setId(tempId1);
											        	     policyDocumentManager.save(newPolicyDocument);
											        		 childAffected="All Child Accounts have been updated";
																
															}
										             }else{
									            	 		 PolicyDocument newPolicyDocument= new PolicyDocument();
										        			 beanUtilsContract.copyProperties(newPolicyDocument, policyDocument);
										        			 newPolicyDocument.setPartnerCode(partnerCode);
										        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
										        				 newPolicyDocument.setCorpID(partnerPublicChildUpdate.getCorpID());
															  }
															 else{
																 newPolicyDocument.setCorpID(sessionCorpID);
															 }
										        			 newPolicyDocument.setCreatedBy(getRequest().getRemoteUser());
										        			 newPolicyDocument.setUpdatedBy(getRequest().getRemoteUser());
										        			 newPolicyDocument.setCreatedOn(new Date());
										        			 newPolicyDocument.setUpdatedOn(new Date());
										        			 newPolicyDocument.setParentId(policyDocument.getId());
										        			 newPolicyDocument.setId(null);
											        	     policyDocumentManager.save(newPolicyDocument);
											        		 childAffected="All Child Accounts have been updated";
										             }
												 }
										          
											  }
										  }
									  /*}*/
							  }else{
								  childAffected="Can not update child as no record is available";
							  }
							  }
				  }
				  if(pageListType.equalsIgnoreCase("FAQ"))
				  {
							  BeanUtilsBean beanUtilsContract = BeanUtilsBean.getInstance();
							  beanUtilsContract.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
							  List faqFileList=new ArrayList();
							  List partnerCodeList = new ArrayList();
							  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
								  faqFileList = frequentlyAskedQuestionsManager.getFAQByPartnerCode(partnerCode,partnerPublicChildUpdate.getCorpID());
								  if(!sessionCorpID.equals(partnerPublicChildUpdate.getCorpID())){
									  List oldFaqList = frequentlyAskedQuestionsManager.getFaqByPartnerCode(partnerCode,sessionCorpID);
						      		  if(!oldFaqList.isEmpty()){
						 	        			Iterator it = oldFaqList.iterator();
						 	        			while(it.hasNext()){
						 	        				faqFileList.add(it.next());
						 	        			}
						      			}
								  }
							  }
							  else{
								  PartnerPublic partnerFaq = partnerPublicManager.getPartnerByCode(partnerPublicChildUpdate.getAgentParent());
								  faqFileList = frequentlyAskedQuestionsManager.getFaqByPartnerCode(partnerCode,sessionCorpID);
								  if(!sessionCorpID.equals(partnerPublicChildUpdate.getCorpID())){
									  List oldFaqList = frequentlyAskedQuestionsManager.getFAQByPartnerCode(partnerCode,partnerFaq.getCorpID());
						      		  if(!oldFaqList.isEmpty()){
						 	        			Iterator it = oldFaqList.iterator();
						 	        			while(it.hasNext()){
						 	        				faqFileList.add(it.next());
						 	        			}
						      			}
								  }
							  }
								  partnerCodeList=partnerManager.getPartnerIncludeChildAgentsList(partnerCode,sessionCorpID,pageListType);
							  if(!(partnerCodeList.isEmpty())&&(partnerCodeList!=null))
							  {
								  if(!(faqFileList.isEmpty())&&(faqFileList!=null))
								  {
									  Iterator itrPolicy = faqFileList.iterator();
									  while(itrPolicy.hasNext())
									  {
										  FrequentlyAskedQuestions frequentlyAskedQuestions=(FrequentlyAskedQuestions)itrPolicy.next();
										  Iterator itrPolicy1 = partnerCodeList.iterator();
										  while(itrPolicy1.hasNext())
										  {
											  String partnerCodeTemp=(String)itrPolicy1.next();
											  String parRec[]=partnerCodeTemp.split("~");
											  String partnerCode=parRec[0];
											  String partnerId1=parRec[1];
											  List frequentlyAskedQuestionsTempe  = new ArrayList();
											  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
												  frequentlyAskedQuestionsTempe=frequentlyAskedQuestionsManager.getFaqReference(partnerCode, partnerPublicChildUpdate.getCorpID(),frequentlyAskedQuestions.getId());
												  if(!sessionCorpID.equals(partnerPublicChildUpdate.getCorpID())){
													  List oldFaqList = frequentlyAskedQuestionsManager.getFaqReference(partnerCode, sessionCorpID,frequentlyAskedQuestions.getId());
										      		  if(!oldFaqList.isEmpty()){
										 	        			Iterator it = oldFaqList.iterator();
										 	        			while(it.hasNext()){
										 	        				frequentlyAskedQuestionsTempe.add(it.next());
										 	        			}
										      			}
												  }
											  }else{
												  PartnerPublic partnerFaq = partnerPublicManager.getPartnerByCode(partnerPublicChildUpdate.getAgentParent());
												  frequentlyAskedQuestionsTempe=frequentlyAskedQuestionsManager.getFaqReference(partnerCode, sessionCorpID,frequentlyAskedQuestions.getId());
												  if(!sessionCorpID.equals(partnerPublicChildUpdate.getCorpID())){
													  List oldFaqList = frequentlyAskedQuestionsManager.getFaqReference(partnerCode, partnerFaq.getCorpID(),frequentlyAskedQuestions.getId());
										      		  if(!oldFaqList.isEmpty()){
										 	        			Iterator it = oldFaqList.iterator();
										 	        			while(it.hasNext()){
										 	        				frequentlyAskedQuestionsTempe.add(it.next());
										 	        			}
										      			}
												  }
											  }
											  if((frequentlyAskedQuestionsTempe!=null)&&(!frequentlyAskedQuestionsTempe.isEmpty()))
											  {
												  if(frequentlyAskedQuestionsTempe.get(0)!=null)
												  {
												  String fid=frequentlyAskedQuestionsTempe.get(0).toString();
												  FrequentlyAskedQuestions newFrequentlyAskedQuestions=frequentlyAskedQuestionsManager.getForOtherCorpid(Long.parseLong(fid));
												  Long tempId3=newFrequentlyAskedQuestions.getId();
												  beanUtilsContract.copyProperties(newFrequentlyAskedQuestions, frequentlyAskedQuestions);
												  newFrequentlyAskedQuestions.setPartnerCode(partnerCode);
												  newFrequentlyAskedQuestions.setPartnerId(Long.parseLong(partnerId1));
								        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
								        				 newFrequentlyAskedQuestions.setCorpId(partnerPublicChildUpdate.getCorpID());
													  }
													 else{
														 newFrequentlyAskedQuestions.setCorpId(sessionCorpID);
													 }
												  newFrequentlyAskedQuestions.setParentId(frequentlyAskedQuestions.getId());
												  newFrequentlyAskedQuestions.setId(tempId3);
										          newFrequentlyAskedQuestions=frequentlyAskedQuestionsManager.save(newFrequentlyAskedQuestions);
										          childAffected="All Child Accounts have been updated";
												  }
											  }else{
												  FrequentlyAskedQuestions newFrequentlyAskedQuestions= new FrequentlyAskedQuestions();
												  beanUtilsContract.copyProperties(newFrequentlyAskedQuestions, frequentlyAskedQuestions);
												  newFrequentlyAskedQuestions.setPartnerCode(partnerCode);
												  newFrequentlyAskedQuestions.setPartnerId(Long.parseLong(partnerId1));
												  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
								        				 newFrequentlyAskedQuestions.setCorpId(partnerPublicChildUpdate.getCorpID());
													  }
													 else{
														 newFrequentlyAskedQuestions.setCorpId(sessionCorpID);
													 }
												  newFrequentlyAskedQuestions.setCreatedBy(getRequest().getRemoteUser());
												  newFrequentlyAskedQuestions.setUpdatedBy(getRequest().getRemoteUser());
												  newFrequentlyAskedQuestions.setCreatedOn(new Date());
												  newFrequentlyAskedQuestions.setUpdatedOn(new Date());
												  newFrequentlyAskedQuestions.setParentId(frequentlyAskedQuestions.getId());
												  newFrequentlyAskedQuestions.setId(null);
												  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
													  maxSequenceNumber = frequentlyAskedQuestionsManager.findMaximum(newFrequentlyAskedQuestions.getLanguage(),partnerCode,partnerPublicChildUpdate.getCorpID());
												  }else{
													  maxSequenceNumber = frequentlyAskedQuestionsManager.findMaximum(newFrequentlyAskedQuestions.getLanguage(),partnerCode,sessionCorpID);
												  }
														if (maxSequenceNumber.get(0) == null) {
															newFrequentlyAskedQuestions.setSequenceNumber(1L);
														} else {
															autoSequenceNumber = Long.parseLong(maxSequenceNumber.get(0).toString()) + 1;
															newFrequentlyAskedQuestions.setSequenceNumber(autoSequenceNumber);
														}
												  newFrequentlyAskedQuestions=frequentlyAskedQuestionsManager.save(newFrequentlyAskedQuestions);
										          childAffected="All Child Accounts have been updated";													  
											  }
										  }
									  }
							  }else{
								  childAffected="Can not update child as no record is available";
							  }
							  }
				  }
				  if(pageListType.equalsIgnoreCase("AIS"))
				  {
					          partnerPrivate=partnerPrivateManager.get(id);
					          PartnerPublic partnerPublic1=partnerPublicManager.get(partnerPrivate.getPartnerPublicId());
							  List partnerCodeList = new ArrayList();
								  partnerCodeList=partnerManager.getPartnerIncludeChildAgentsList(partnerPublic1.getPartnerCode(),sessionCorpID,pageListType);
							  if(!(partnerCodeList.isEmpty())&&(partnerCodeList!=null))
							  {
								  Iterator itrPolicy = partnerCodeList.iterator();
								  while(itrPolicy.hasNext())
								  {
									  String partnerCode=(String)itrPolicy.next();
									  List al=partnerPrivateManager.getPartnerPrivateChildAccount(partnerCode,sessionCorpID);
									  if((al.get(0)!=null)&&(!al.isEmpty()))
									  {
										 PartnerPrivate childPartnerPrivate=(PartnerPrivate)al.get(0);
										 
										 if(checkedOption.indexOf("storageBillingGroup")>-1){
											 childPartnerPrivate.setStorageBillingGroup(partnerPrivate.getStorageBillingGroup());
										 }
										 
										 if(checkedOption.indexOf("controlInfo")>-1)
										 {
											 childPartnerPrivate.setPortalDefaultAccess(partnerPrivate.getPortalDefaultAccess());
											 childPartnerPrivate.setGrpAllowed(partnerPrivate.getGrpAllowed());
										 }
										 if(checkedOption.indexOf("defaultRoles")>-1)
										 {
											 childPartnerPrivate.setAccountHolder(partnerPrivate.getAccountHolder());
											 childPartnerPrivate.setAccountManager(partnerPrivate.getAccountManager());
											 childPartnerPrivate.setPricingUser(partnerPrivate.getPricingUser());
											 childPartnerPrivate.setClaimsUser(partnerPrivate.getClaimsUser());
											 childPartnerPrivate.setBillingUser(partnerPrivate.getBillingUser());
											 childPartnerPrivate.setPayableUser(partnerPrivate.getPayableUser());
											 childPartnerPrivate.setAuditor(partnerPrivate.getAuditor());
											 childPartnerPrivate.setCoordinator(partnerPrivate.getCoordinator());
										 }
										 if(checkedOption.indexOf("defaultRLORoles")>-1)
										 {
											 childPartnerPrivate.setAccountHolderForRelo(partnerPrivate.getAccountHolderForRelo());
											 childPartnerPrivate.setAccountManagerForRelo(partnerPrivate.getAccountManagerForRelo());
											 childPartnerPrivate.setPricingUserForRelo(partnerPrivate.getPricingUserForRelo());
											 childPartnerPrivate.setClaimsUserForRelo(partnerPrivate.getClaimsUserForRelo());
											 childPartnerPrivate.setBillingUserForRelo(partnerPrivate.getBillingUserForRelo());
											 childPartnerPrivate.setPayableUserForRelo(partnerPrivate.getPayableUserForRelo());
											 childPartnerPrivate.setAuditorForRelo(partnerPrivate.getAuditorForRelo());
											 childPartnerPrivate.setCoordinatorForRelo(partnerPrivate.getCoordinatorForRelo());
										 }
										 
	                                      if(checkedOption.indexOf("InsuranceSetUp")>-1){
	                                    	  childPartnerPrivate.setLumpSum(partnerPrivate.getLumpSum()); 
	                                    	  childPartnerPrivate.setLumpSumAmount(partnerPrivate.getLumpSumAmount());
	                                    	  childPartnerPrivate.setLumpPerUnit(partnerPrivate.getLumpPerUnit());
	                                    	  childPartnerPrivate.setMinReplacementvalue(partnerPrivate.getMinReplacementvalue());
	                                    	  childPartnerPrivate.setDetailedList(partnerPrivate.getDetailedList());
	                                    	  childPartnerPrivate.setNoInsurance(partnerPrivate.getNoInsurance());
										 }
										 if(checkedOption.indexOf("paymentMethod")>-1)
										 {
											 childPartnerPrivate.setPaymentMethod(partnerPrivate.getPaymentMethod());
										 }
										 if(checkedOption.indexOf("billingInstruction")>-1)
										 {
											 childPartnerPrivate.setBillingInstruction(partnerPrivate.getBillingInstruction());
										 }
										 if(checkedOption.indexOf("insurance")>-1)
										 {											
											 childPartnerPrivate.setInsuranceAuthorized(partnerPrivate.getInsuranceAuthorized());
										 }
										 if(checkedOption.indexOf("creditTerms")>-1)
										 {
											 childPartnerPrivate.setCreditTerms(partnerPrivate.getCreditTerms());
										 }
										 if(checkedOption.indexOf("qualityMeasurement")>-1)
										 {
											 childPartnerPrivate.setNoTransfereeEvaluation(partnerPrivate.getNoTransfereeEvaluation());
											 childPartnerPrivate.setOneRequestPerCF(partnerPrivate.getOneRequestPerCF());
											 childPartnerPrivate.setRatingScale(partnerPrivate.getRatingScale());
											 childPartnerPrivate.setCustomerFeedback(partnerPrivate.getCustomerFeedback());
											 childPartnerPrivate.setDescription1(partnerPrivate.getDescription1());
											 childPartnerPrivate.setDescription2(partnerPrivate.getDescription2());
											 childPartnerPrivate.setDescription3(partnerPrivate.getDescription3());
											 childPartnerPrivate.setLink1(partnerPrivate.getLink1());
											 childPartnerPrivate.setLink2(partnerPrivate.getLink2());
											 childPartnerPrivate.setLink3(partnerPrivate.getLink3());
										 }
										 if(checkedOption.indexOf("entitlement")>-1)
										 {
											 List entitlementList=entitlementManager.findEntitlementByPartnerId(sessionCorpID, partnerPublic1.getId());
											  BeanUtilsBean beanUtilsContract = BeanUtilsBean.getInstance();
											  beanUtilsContract.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
											  if(!(entitlementList.isEmpty())&&(entitlementList!=null))
											  {
												  Iterator entitlementL = entitlementList.iterator();
												  while(entitlementL.hasNext())
												  {
													  Entitlement entitlement=(Entitlement)entitlementL.next();
													  List entitlementTempe=entitlementManager.getEntitlementReference(childPartnerPrivate.getPartnerCode(), sessionCorpID,entitlement.getId());
													  if((entitlementTempe!=null)&&(!entitlementTempe.isEmpty()))
													  {
														  if(entitlementTempe.get(0)!=null)
														  {
														  String fid=entitlementTempe.get(0).toString();
														  Entitlement newEntitlement=entitlementManager.get(Long.parseLong(fid));
														  Long tempId3=newEntitlement.getId();
														  beanUtilsContract.copyProperties(newEntitlement, entitlement);
														  newEntitlement.setPartnerCode(childPartnerPrivate.getPartnerCode());
														  newEntitlement.setPartnerPrivateId(childPartnerPrivate.getPartnerPublicId());
														  newEntitlement.setCorpID(sessionCorpID);
														  newEntitlement.setParentId(entitlement.getId());
														  newEntitlement.setId(tempId3);
														  newEntitlement=entitlementManager.save(newEntitlement);
												          childAffected="All Child Accounts have been updated";
														  }														  
													  }else{
														  Entitlement newEntitlement= new Entitlement();
														  beanUtilsContract.copyProperties(newEntitlement, entitlement);
														  newEntitlement.setPartnerCode(childPartnerPrivate.getPartnerCode());
														  newEntitlement.setPartnerPrivateId(childPartnerPrivate.getPartnerPublicId());
														  newEntitlement.setCorpID(sessionCorpID);
														  newEntitlement.setCreatedBy(getRequest().getRemoteUser());
														  newEntitlement.setUpdatedBy(getRequest().getRemoteUser());
														  newEntitlement.setCreatedOn(new Date());
														  newEntitlement.setUpdatedOn(new Date());
														  newEntitlement.setParentId(entitlement.getId());
														  newEntitlement.setId(null);
														  newEntitlement=entitlementManager.save(newEntitlement);
												          childAffected="All Child Accounts have been updated";														  
													  }
												  }
											  }
										 }
										 if(checkedOption.indexOf("accountAssignmentType")>-1)
										 {
											 List accountAssignmentTypeList=accountAssignmentTypeManager.getAssignmentTypeList(partnerPublic1.getPartnerCode(), partnerPublic1.getId(), sessionCorpID);
											  BeanUtilsBean beanUtilsContract = BeanUtilsBean.getInstance();
											  beanUtilsContract.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
											  if(!(accountAssignmentTypeList.isEmpty())&&(accountAssignmentTypeList!=null))
											  {
												  Iterator accountAssignmentTypeL = accountAssignmentTypeList.iterator();
												  while(accountAssignmentTypeL.hasNext())
												  {
													  AccountAssignmentType accountAssignmentType=(AccountAssignmentType)accountAssignmentTypeL.next();
													  List accountAssignmentTypeTempe=accountAssignmentTypeManager.getaccountAssignmentTypeReference(childPartnerPrivate.getPartnerCode(), sessionCorpID,accountAssignmentType.getId());
													  if((accountAssignmentTypeTempe!=null)&&(!accountAssignmentTypeTempe.isEmpty()))
													  {
														  if(accountAssignmentTypeTempe.get(0)!=null)
														  {
														  String fid=accountAssignmentTypeTempe.get(0).toString();
														  AccountAssignmentType newAccountAssignmentType=accountAssignmentTypeManager.get(Long.parseLong(fid));
														  Long tempId3=newAccountAssignmentType.getId();
														  beanUtilsContract.copyProperties(newAccountAssignmentType, accountAssignmentType);
														  newAccountAssignmentType.setPartnerCode(childPartnerPrivate.getPartnerCode());
														  newAccountAssignmentType.setPartnerPrivateId(childPartnerPrivate.getPartnerPublicId());
														  newAccountAssignmentType.setCorpId(sessionCorpID);
														  newAccountAssignmentType.setParentId(accountAssignmentType.getId());
														  newAccountAssignmentType.setId(tempId3);
														  newAccountAssignmentType=accountAssignmentTypeManager.save(newAccountAssignmentType);
												          childAffected="All Child Accounts have been updated";
														  }														  
													  }else{
														  AccountAssignmentType newAccountAssignmentType= new AccountAssignmentType();
														  beanUtilsContract.copyProperties(newAccountAssignmentType, accountAssignmentType);
														  newAccountAssignmentType.setPartnerCode(childPartnerPrivate.getPartnerCode());
														  newAccountAssignmentType.setPartnerPrivateId(childPartnerPrivate.getPartnerPublicId());
														  newAccountAssignmentType.setCorpId(sessionCorpID);
														  newAccountAssignmentType.setCreatedBy(getRequest().getRemoteUser());
														  newAccountAssignmentType.setUpdatedBy(getRequest().getRemoteUser());
														  newAccountAssignmentType.setCreatedOn(new Date());
														  newAccountAssignmentType.setUpdatedOn(new Date());
														  newAccountAssignmentType.setParentId(accountAssignmentType.getId());
														  newAccountAssignmentType.setId(null);
														  newAccountAssignmentType=accountAssignmentTypeManager.save(newAccountAssignmentType);
												          childAffected="All Child Accounts have been updated";														  
													  }
												  }
											  }																					 
										 }
										 if(checkedOption.indexOf("contract")>-1){
											 childPartnerPrivate.setContract(partnerPrivate.getContract());
										 }
										 if(checkedOption.indexOf("networkPartnerCode")>-1){
											 childPartnerPrivate.setNetworkPartnerCode(partnerPrivate.getNetworkPartnerCode());
											 String bookingAgentName = contractPolicyManager.findByBookingLastName(partnerPrivate.getNetworkPartnerCode(), sessionCorpID);
											 childPartnerPrivate.setNetworkPartnerName(partnerPrivate.getNetworkPartnerName());
										 }
										 if(checkedOption.indexOf("source")>-1){
											 childPartnerPrivate.setSource(partnerPrivate.getSource());
										 }
										 if(checkedOption.indexOf("vendorCode")>-1){
											 childPartnerPrivate.setVendorCode(partnerPrivate.getVendorCode());
											 String bookingAgentName = contractPolicyManager.findByBookingLastName(partnerPrivate.getVendorCode(), sessionCorpID);
											 childPartnerPrivate.setVendorName(partnerPrivate.getVendorName());
										 }
										 if(checkedOption.indexOf("insuranceHas")>-1){
											 childPartnerPrivate.setInsuranceHas(partnerPrivate.getInsuranceHas());
										 }
										 if(checkedOption.indexOf("insuranceOption")>-1){
											 childPartnerPrivate.setInsuranceOption(partnerPrivate.getInsuranceOption());
										 }
										 if(checkedOption.indexOf("defaultVat")>-1){
											 childPartnerPrivate.setDefaultVat(partnerPrivate.getDefaultVat());
										 }
										 if(checkedOption.indexOf("billToAuthorization")>-1){
											 childPartnerPrivate.setBillToAuthorization(partnerPrivate.getBillToAuthorization());
										 }
										 if(checkedOption.indexOf("rddBased")>-1){
											 childPartnerPrivate.setRddBased(partnerPrivate.getRddBased());
										 }
										 if(checkedOption.indexOf("rddDays")>-1){
											 childPartnerPrivate.setRddDays(partnerPrivate.getRddDays());
										 }
										 if(checkedOption.indexOf("storageEmailType")>-1){
											 childPartnerPrivate.setStorageEmailType(partnerPrivate.getStorageEmailType());
										 }
										 if(checkedOption.indexOf("billingCycle")>-1){
											 childPartnerPrivate.setBillingCycle(partnerPrivate.getBillingCycle());
										 }
										 childPartnerPrivate=partnerPrivateManager.save(childPartnerPrivate);
										 childAffected="All Child Accounts have been updated";
									  }
								  }
							  }
				  }
				  if(pageListType.equalsIgnoreCase("PUB"))				  {

			          partnerPrivate=partnerPrivateManager.get(id);
			          PartnerPublic partnerPublic1=partnerPublicManager.get(partnerPrivate.getPartnerPublicId());
			          List partnerCodeList = new ArrayList();
					  partnerCodeList=partnerManager.getPartnerIncludeChildAgentsList(partnerPublic1.getPartnerCode(),sessionCorpID,pageListType);
					  if(!(partnerCodeList.isEmpty())&&(partnerCodeList!=null))
					  {
						  Iterator itrPolicy = partnerCodeList.iterator();
						  while(itrPolicy.hasNext())
						  {
							  String partnerCode=(String)itrPolicy.next();
							  List al=partnerPublicManager.getPartnerPublicChildAccount(partnerCode,sessionCorpID);
							  if((al.get(0)!=null)&&(!al.isEmpty()))
							  {
								 PartnerPublic childPartnerPublic=(PartnerPublic)al.get(0);
								 if(checkedOption.indexOf("logoPhotographs")>-1)
								 {
									 childPartnerPublic.setLocation1(partnerPublic1.getLocation1());
									 childPartnerPublic.setLocation2(partnerPublic1.getLocation2());
									 childPartnerPublic.setLocation3(partnerPublic1.getLocation3());
									 childPartnerPublic.setLocation4(partnerPublic1.getLocation4());
									
									 childPartnerPublic=partnerPublicManager.save(childPartnerPublic);
								 }
								 if(checkedOption.indexOf("bankCode")>-1)
								 {
									 childPartnerPublic.setBankCode(partnerPublic1.getBankCode());
									 
									 childPartnerPublic=partnerPublicManager.save(childPartnerPublic);
								 }
								 if(checkedOption.indexOf("defaultBillingCurrency")>-1)
								 {
									 childPartnerPublic.setBillingCurrency(partnerPublic1.getBillingCurrency());
									 
									 childPartnerPublic=partnerPublicManager.save(childPartnerPublic);
								 }
								 if(checkedOption.indexOf("Type")>-1){
									 childPartnerPublic.setPartnerType(partnerPublic1.getPartnerType());
									 if(partnerPublic1.getPartnerType()!=null && (!(partnerPublic1.getPartnerType().toString().trim().equals("")))  && partnerPublic1.getStatus().equalsIgnoreCase("Approved") && (partnerPublic1.getPartnerType().equalsIgnoreCase("CMM") || partnerPublic1.getPartnerType().equalsIgnoreCase("DMM"))){
										 childPartnerPublic.setCorpID("TSFT");
									 }
									 childPartnerPublic=partnerPublicManager.save(childPartnerPublic);
								 }
								 if(checkedOption.indexOf("partnerPortalDetail")>-1)
								 {
									 childPartnerPublic.setPartnerPortalActive(partnerPublic1.getPartnerPortalActive());
									 childPartnerPublic.setAssociatedAgents(partnerPublic1.getAssociatedAgents());
									 childPartnerPublic.setViewChild(partnerPublic1.getViewChild());
									 try {
											if(childPartnerPublic.getPartnerPortalActive()!=null)
											{
											boolean portalIdActive = (childPartnerPublic.getPartnerPortalActive() == (true));
											if (portalIdActive) {
												String prefix = "";
												String prefixDataSet = "";
												if(childPartnerPublic.getIsAccount()){
													prefix = "DATA_SECURITY_FILTER_SO_BILLTOCODE_" + childPartnerPublic.getPartnerCode();
													prefixDataSet = "DATA_SECURITY_SET_SO_BILLTOCODE_" + childPartnerPublic.getPartnerCode();
												}else{
													prefix = "DATA_SECURITY_FILTER_AGENT_" + childPartnerPublic.getPartnerCode();
													prefixDataSet = "DATA_SECURITY_SET_AGENT_" + childPartnerPublic.getPartnerCode();
												}
												List agentFilterList = dataSecurityFilterManager.getAgentFilterList(prefix, sessionCorpID);
												List agentFilterSetList = dataSecuritySetManager.getDataSecuritySet(prefixDataSet, sessionCorpID);
												if ((agentFilterList ==null || agentFilterList.isEmpty()) && (agentFilterSetList == null || agentFilterSetList.isEmpty())) {
													List nameList = new ArrayList();
													if(childPartnerPublic.getIsAccount()){
														nameList.add("BILLTOCODE");
													}else{
														nameList.add("BOOKINGAGENTCODE");
														nameList.add("BROKERCODE");
														nameList.add("ORIGINAGENTCODE");
														nameList.add("ORIGINSUBAGENTCODE");
														nameList.add("DESTINATIONAGENTCODE");
														nameList.add("DESTINATIONSUBAGENTCODE");
													}
													dataSecuritySet = new DataSecuritySet();
													Iterator it = nameList.iterator();
													while (it.hasNext()) {
														String fieldName = it.next().toString();
														String postFix = prefix + "_" + fieldName;
														dataSecurityFilter = new DataSecurityFilter();
														if(childPartnerPublic.getIsAccount()){
															dataSecurityFilter.setName(prefix);
														}else{
															dataSecurityFilter.setName(postFix);
														}
														dataSecurityFilter.setTableName("serviceorder");
														dataSecurityFilter.setFieldName(fieldName.toLowerCase());
														dataSecurityFilter.setFilterValues(childPartnerPublic.getPartnerCode().toString());
														dataSecurityFilter.setCorpID(sessionCorpID);
														dataSecurityFilter.setCreatedOn(new Date());
														dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
														dataSecurityFilter.setUpdatedOn(new Date());
														dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
														dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
														dataSecuritySet.addFilters(dataSecurityFilter);
													}
													dataSecuritySet.setName(prefixDataSet);
													dataSecuritySet.setDescription(prefixDataSet);
													dataSecuritySet.setCorpID(sessionCorpID);
													dataSecuritySet.setCreatedOn(new Date());
													dataSecuritySet.setCreatedBy(getRequest().getRemoteUser());
													dataSecuritySet.setUpdatedOn(new Date());
													dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
													dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
												}
											}
									
								             }
											}catch(Exception e){
												e.printStackTrace();
											}
									 childPartnerPublic=partnerPublicManager.save(childPartnerPublic);
								 }
								 childAffected="All Child Accounts have been updated";
							  }
						  }
					  }		  
				  }
				  if(pageListType.equalsIgnoreCase("PRS"))			  {

			          PartnerPublic partnerPublic1=partnerPublicManager.get(id);
			          List partnerCodeList = new ArrayList();
						  partnerCodeList=partnerManager.getPartnerIncludeChildAgentsList(partnerPublic1.getPartnerCode(),sessionCorpID,pageListType);
					  if(!(partnerCodeList.isEmpty())&&(partnerCodeList!=null))
					  {
						  Iterator itrPolicy = partnerCodeList.iterator();
						  while(itrPolicy.hasNext())
						  {
							  String partnerCode=(String)itrPolicy.next();
							  List al=partnerPublicManager.getPartnerPublicChildAccount(partnerCode,sessionCorpID);
							  if((al.get(0)!=null)&&(!al.isEmpty()))
							  {
								 PartnerPublic childPartnerPublic=(PartnerPublic)al.get(0);
								 childPartnerPublic.setReloContact(partnerPublic1.getReloContact());
								 childPartnerPublic.setReloContactEmail(partnerPublic1.getReloContactEmail());
								 childPartnerPublic.setReloServices(partnerPublic1.getReloServices());
								 childPartnerPublic.setUpdatedOn(new Date());
								 childPartnerPublic.setUpdatedBy(getRequest().getRemoteUser());
								 childPartnerPublic.setTransPort(partnerPublic1.getTransPort());
								 childPartnerPublic=partnerPublicManager.save(childPartnerPublic);
								 childAffected="All Child Accounts have been updated";
							  }
						  }
					  }					  
				  }
				  if(pageListType.equalsIgnoreCase("QSE"))
				  {
					  BeanUtilsBean beanUtilsContract = BeanUtilsBean.getInstance();
					  beanUtilsContract.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					  List faqFileList=new ArrayList();
					  List partnerCodeList = new ArrayList();
					  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
						  faqFileList=qualitySurveySettingsManager.getQualityEmailListByPartnerCode(partnerCode,partnerPublicChildUpdate.getCorpID());
						  partnerCodeList=partnerManager.getPartnerIncludeChildAgentsList(partnerCode,partnerPublicChildUpdate.getCorpID(),pageListType);
					  }
					  else{
						  faqFileList=qualitySurveySettingsManager.getQualityEmailListByPartnerCode(partnerCode,sessionCorpID);
						  partnerCodeList=partnerManager.getPartnerIncludeChildAgentsList(partnerCode,sessionCorpID,pageListType);
					  }
					  String tempAbc[]=partnerCode.split("~");
					  String flag="";
					  if(!(partnerCodeList.isEmpty())&&(partnerCodeList!=null))
					  {
						  if(!(faqFileList.isEmpty())&&(faqFileList!=null))
						  {
							  Iterator itrPolicy = faqFileList.iterator();
							  while(itrPolicy.hasNext())
							  {
								  QualitySurveySettings qualitySurveySettings=(QualitySurveySettings)itrPolicy.next();
								  Iterator itrPolicy1 = partnerCodeList.iterator();
								  while(itrPolicy1.hasNext())
								  {
									  String partnerCode=(String)itrPolicy1.next();
									  if(!(flag.indexOf(partnerCode)>-1)) {
									  int i=qualitySurveySettingsManager.getQualityListReference(partnerCode, sessionCorpID,tempAbc[1]);
									  }
									  flag=flag+" "+partnerCode;
									  QualitySurveySettings newQualitySurveySettings= new QualitySurveySettings();
										  beanUtilsContract.copyProperties(newQualitySurveySettings, qualitySurveySettings);
										  newQualitySurveySettings.setAccountId(partnerCode);
										  newQualitySurveySettings.setCorpId(sessionCorpID);
										  newQualitySurveySettings.setCreatedBy(getRequest().getRemoteUser());
										  newQualitySurveySettings.setUpdatedBy(getRequest().getRemoteUser());
										  newQualitySurveySettings.setCreatedOn(new Date());
										  newQualitySurveySettings.setUpdatedOn(new Date());
										  newQualitySurveySettings.setMode(tempAbc[1]);
										  newQualitySurveySettings.setId(null);
										  newQualitySurveySettings=qualitySurveySettingsManager.save(newQualitySurveySettings);
								          childAffected="All Child Accounts have been updated";									  
								  }
							  }
					  }else{
						  childAffected="Can not update child as no record is available";
					  }
					  }					  
				  }
				  if(pagePrsListType.equalsIgnoreCase("PRS")){
					  partnerPrivate=partnerPrivateManager.get(id);
			          PartnerPublic partnerPublic1=partnerPublicManager.get(partnerPrivate.getPartnerPublicId());
			          List partnerCodeList = new ArrayList();
						  partnerCodeList=partnerManager.getPartnerIncludeChildAgentsList(partnerPublic1.getPartnerCode(),sessionCorpID,"PRS");
					  if(!(partnerCodeList.isEmpty())&&(partnerCodeList!=null))
					  {
						  Iterator itrPolicy = partnerCodeList.iterator();
						  while(itrPolicy.hasNext())
						  {
							  String partnerCode=(String)itrPolicy.next();
							  List al=partnerPublicManager.getPartnerPublicChildAccount(partnerCode,sessionCorpID);
							  if((al.get(0)!=null)&&(!al.isEmpty()))
							  {
								 PartnerPublic childPartnerPublic=(PartnerPublic)al.get(0);
								 childPartnerPublic.setReloContact(partnerPublic1.getReloContact());
								 childPartnerPublic.setReloContactEmail(partnerPublic1.getReloContactEmail());
								 childPartnerPublic.setReloServices(partnerPublic1.getReloServices());
								 childPartnerPublic.setUpdatedOn(new Date());
								 childPartnerPublic.setUpdatedBy(getRequest().getRemoteUser());
								 childPartnerPublic.setTransPort(partnerPublic1.getTransPort());
								 childPartnerPublic=partnerPublicManager.save(childPartnerPublic);
								 childAffected="All Child Accounts have been updated";
							  }
						  }
					  }					  
				  }
				
				if(pageCPFListType.equalsIgnoreCase("CPF"))
				  {
					  BeanUtilsBean beanUtilsContract = BeanUtilsBean.getInstance();
					  beanUtilsContract.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					  List partnerCodeList = new ArrayList();
					  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
						  policyFileList=contractPolicyManager.findPolicyFileList(partnerCode,partnerPublicChildUpdate.getCorpID());
						  
					  }
					  else{
						  policyFileList=contractPolicyManager.findPolicyFileList(partnerCode,sessionCorpID);
					  }
					  partnerCodeList=partnerManager.getPartnerIncludeChildAgentsList(partnerCode,sessionCorpID,"CPF");
					  if(!(policyFileList.isEmpty())&&(policyFileList!=null))
					  {
					  Iterator it = partnerCodeList.iterator();
					  while(it.hasNext())
					  {
						  String partnerCode=(String)it.next();
						  
						  /**
						   * delete policy from child then insert new policy records.
						   */
						  int i = contractPolicyManager.deletedPolicyContract(partnerCode, sessionCorpID);
					  }
					  }
					  String parentPartnerCode=partnerCode;
					  if(!(partnerCodeList.isEmpty())&&(partnerCodeList!=null))
					  {
						  if(!(policyFileList.isEmpty())&&(policyFileList!=null))
						  {
							  Iterator itrPolicy = policyFileList.iterator();
							  while(itrPolicy.hasNext())
							  {
								  ContractPolicy contractPolicy=(ContractPolicy)itrPolicy.next();
								  Iterator itrPolicy1 = partnerCodeList.iterator();
								  while(itrPolicy1.hasNext())
								  {
									  //List contractPolicyTempe = new ArrayList();
									  String partnerCode=(String)itrPolicy1.next();
									  
/*											  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
										  contractPolicyTempe=contractPolicyManager.getPolicyReference(partnerCode, partnerPublicChildUpdate.getCorpID(),contractPolicy.getId());	
									  }
									  else{
										  contractPolicyTempe=contractPolicyManager.getPolicyReference(partnerCode, sessionCorpID,contractPolicy.getId());	
									  }
									  										  
									  if((contractPolicyTempe!=null)&&(!contractPolicyTempe.isEmpty()))
									  {
										  if(contractPolicyTempe.get(0)!=null)
										  {		
											  String cid=contractPolicyTempe.get(0).toString();
											  ContractPolicy newContractPolicy=contractPolicyManager.get(Long.parseLong(cid));
											  Long tempId=newContractPolicy.getId();
											  beanUtilsContract.copyProperties(newContractPolicy, contractPolicy);
											  newContractPolicy.setPartnerCode(partnerCode);
											  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
												  newContractPolicy.setCorpID(partnerPublicChildUpdate.getCorpID());
											  }
											  else{
												  newContractPolicy.setCorpID(sessionCorpID);
											  }
											  newContractPolicy.setParentId(contractPolicy.getId());
											  newContractPolicy.setId(tempId);
								        	  contractPolicyManager.save(newContractPolicy);
									          childAffected="All Child Accounts have been updated";
									          List policyDocumentList2 = new ArrayList();
									          if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
									        	  policyDocumentList2=contractPolicyManager.findDocumentList(parentPartnerCode,contractPolicy.getSectionName(),contractPolicy.getLanguage(),partnerPublicChildUpdate.getCorpID());
											  }
											  else{
												  policyDocumentList2=contractPolicyManager.findDocumentList(parentPartnerCode,contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
											  }
									          
									          Iterator itrDocPolicy = policyDocumentList2.iterator();
									          while(itrDocPolicy.hasNext())
									          {
									        	 PolicyDocument policyDocument=(PolicyDocument)itrDocPolicy.next();
									        	 List policyDocumentTempe = new ArrayList();
										          if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
										        	  policyDocumentTempe=contractPolicyManager.getPolicyDocReference(partnerCode,partnerPublicChildUpdate.getCorpID(),policyDocument.getId(),policyDocument.getSectionName(),policyDocument.getLanguage());
												  }
												  else{
													  policyDocumentTempe=contractPolicyManager.getPolicyDocReference(partnerCode,sessionCorpID,policyDocument.getId(),policyDocument.getSectionName(),policyDocument.getLanguage());
												  }
												  if((policyDocumentTempe!=null)&&(!policyDocumentTempe.isEmpty()))
												  {
													  if(policyDocumentTempe.get(0)!=null)
													  {
														 String pid=policyDocumentTempe.get(0).toString();
														 PolicyDocument newPolicyDocument=policyDocumentManager.get(Long.parseLong(pid));
														 Long tempId1=newPolicyDocument.getId();
									        			 beanUtilsContract.copyProperties(newPolicyDocument, policyDocument);
									        			 newPolicyDocument.setPartnerCode(partnerCode);
									        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
									        				 newPolicyDocument.setCorpID(partnerPublicChildUpdate.getCorpID());
														  }
														  else{
															  newPolicyDocument.setCorpID(sessionCorpID);
														  }
									        			 newPolicyDocument.setParentId(policyDocument.getId());
									        			 newPolicyDocument.setId(tempId1);
									        			 newPolicyDocument=policyDocumentManager.save(newPolicyDocument);
										        		 childAffected="All Child Accounts have been updated";
															
														}
									             }else{
								            	 		 PolicyDocument newPolicyDocument= new PolicyDocument();
									        			 beanUtilsContract.copyProperties(newPolicyDocument, policyDocument);
									        			 newPolicyDocument.setPartnerCode(partnerCode);
									        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
									        				 newPolicyDocument.setCorpID(partnerPublicChildUpdate.getCorpID());
														  }
														  else{
															  newPolicyDocument.setCorpID(sessionCorpID);
														  }
									        			 newPolicyDocument.setCreatedBy(getRequest().getRemoteUser());
									        			 newPolicyDocument.setUpdatedBy(getRequest().getRemoteUser());
									        			 newPolicyDocument.setCreatedOn(new Date());
									        			 newPolicyDocument.setUpdatedOn(new Date());
									        			 newPolicyDocument.setParentId(policyDocument.getId());
									        			 newPolicyDocument.setId(null);
									        			 newPolicyDocument=policyDocumentManager.save(newPolicyDocument);
										        		 childAffected="All Child Accounts have been updated";
									             }
											 }
											}												  
									  }else{*/
										  ContractPolicy newContractPolicy= new ContractPolicy();
										  beanUtilsContract.copyProperties(newContractPolicy, contractPolicy);
										  newContractPolicy.setPartnerCode(partnerCode);
										  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
											  newContractPolicy.setCorpID(partnerPublicChildUpdate.getCorpID());
										  }
										 else{
											 newContractPolicy.setCorpID(sessionCorpID);
										 }
										  newContractPolicy.setCreatedBy(getRequest().getRemoteUser());
										  newContractPolicy.setUpdatedBy(getRequest().getRemoteUser());
										  newContractPolicy.setCreatedOn(new Date());
										  newContractPolicy.setUpdatedOn(new Date());
										  newContractPolicy.setParentId(contractPolicy.getId());
										  newContractPolicy.setId(null);
							        	  contractPolicyManager.save(newContractPolicy);
								          childAffected="All Child Accounts have been updated";
								          List policyDocumentList2 = new ArrayList();
								          if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
								        	  policyDocumentList2=contractPolicyManager.findDocumentList(parentPartnerCode,contractPolicy.getSectionName(),contractPolicy.getLanguage(),partnerPublicChildUpdate.getCorpID());
										  }
										  else{
											  policyDocumentList2=contractPolicyManager.findDocumentList(parentPartnerCode,contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
										  }
								          
								          Iterator itrDocPolicy = policyDocumentList2.iterator();
								          while(itrDocPolicy.hasNext())
								          {
								        	 PolicyDocument policyDocument=(PolicyDocument)itrDocPolicy.next();
								        	 List policyDocumentTempe = new ArrayList();
									          if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
									        	  policyDocumentTempe=contractPolicyManager.getPolicyDocReference(partnerCode,partnerPublicChildUpdate.getCorpID(),policyDocument.getId(),policyDocument.getSectionName(),policyDocument.getLanguage());
											  }
											  else{
												  policyDocumentTempe=contractPolicyManager.getPolicyDocReference(partnerCode,sessionCorpID,policyDocument.getId(),policyDocument.getSectionName(),policyDocument.getLanguage());
											  }
								        	 
											  if((policyDocumentTempe!=null)&&(!policyDocumentTempe.isEmpty()))
											  {
												  if(policyDocumentTempe.get(0)!=null)
												  {
													 String pid=policyDocumentTempe.get(0).toString();
													 PolicyDocument newPolicyDocument=policyDocumentManager.get(Long.parseLong(pid));
													 Long tempId1=newPolicyDocument.getId();
								        			 beanUtilsContract.copyProperties(newPolicyDocument, policyDocument);
								        			 newPolicyDocument.setPartnerCode(partnerCode);
								        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
								        				 newPolicyDocument.setCorpID(partnerPublicChildUpdate.getCorpID());
													  }
													 else{
														 newPolicyDocument.setCorpID(sessionCorpID);
													 }
								        			 newPolicyDocument.setParentId(policyDocument.getId());
								        			 newPolicyDocument.setId(tempId1);
									        	     policyDocumentManager.save(newPolicyDocument);
									        		 childAffected="All Child Accounts have been updated";
														
													}
								             }else{
							            	 		 PolicyDocument newPolicyDocument= new PolicyDocument();
								        			 beanUtilsContract.copyProperties(newPolicyDocument, policyDocument);
								        			 newPolicyDocument.setPartnerCode(partnerCode);
								        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
								        				 newPolicyDocument.setCorpID(partnerPublicChildUpdate.getCorpID());
													  }
													 else{
														 newPolicyDocument.setCorpID(sessionCorpID);
													 }
								        			 newPolicyDocument.setCreatedBy(getRequest().getRemoteUser());
								        			 newPolicyDocument.setUpdatedBy(getRequest().getRemoteUser());
								        			 newPolicyDocument.setCreatedOn(new Date());
								        			 newPolicyDocument.setUpdatedOn(new Date());
								        			 newPolicyDocument.setParentId(policyDocument.getId());
								        			 newPolicyDocument.setId(null);
									        	     policyDocumentManager.save(newPolicyDocument);
									        		 childAffected="All Child Accounts have been updated";
								             }
										 }
								          
									  }
								  }
							  /*}*/
					  }else{
						  childAffected="Can not update child as no record is available";
					  }
					  }
		  }
				if(pageFaqInfo.equalsIgnoreCase("FAQ"))
				  {
					  BeanUtilsBean beanUtilsContract = BeanUtilsBean.getInstance();
					  beanUtilsContract.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					  List faqFileList=new ArrayList();
					  List partnerCodeList = new ArrayList();
					  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
						  faqFileList = frequentlyAskedQuestionsManager.getFAQByPartnerCode(partnerCode,partnerPublicChildUpdate.getCorpID());
						  if(!sessionCorpID.equals(partnerPublicChildUpdate.getCorpID())){
							  List oldFaqList = frequentlyAskedQuestionsManager.getFaqByPartnerCode(partnerCode,sessionCorpID);
				      		  if(!oldFaqList.isEmpty()){
				 	        			Iterator it = oldFaqList.iterator();
				 	        			while(it.hasNext()){
				 	        				faqFileList.add(it.next());
				 	        			}
				      			}
						  }
					  }
					  else{
						  PartnerPublic partnerFaq = partnerPublicManager.getPartnerByCode(partnerPublicChildUpdate.getAgentParent());
						  faqFileList = frequentlyAskedQuestionsManager.getFaqByPartnerCode(partnerCode,sessionCorpID);
						  if(!sessionCorpID.equals(partnerPublicChildUpdate.getCorpID())){
							  List oldFaqList = frequentlyAskedQuestionsManager.getFAQByPartnerCode(partnerCode,partnerFaq.getCorpID());
				      		  if(!oldFaqList.isEmpty()){
				 	        			Iterator it = oldFaqList.iterator();
				 	        			while(it.hasNext()){
				 	        				faqFileList.add(it.next());
				 	        			}
				      			}
						  }
					  }
						  partnerCodeList=partnerManager.getPartnerIncludeChildAgentsList(partnerCode,sessionCorpID,"FAQ");
						  if((faqFileList!=null) && (!(faqFileList.isEmpty())))
						  {
						  Iterator it = partnerCodeList.iterator();
						  while(it.hasNext())
						  { 
							  String partnerCodeTemp=(String)it.next();
							  String parRec[]=partnerCodeTemp.split("~");
							  String partnerCode=parRec[0];
							  String partnerId1=parRec[1];
							  /**
							   * delete FAQ from child then insert new FAQ records.
							   */
							  int i = frequentlyAskedQuestionsManager.deletedChildFAQ(partnerCode, sessionCorpID);
						  }
						  }
					  if(!(partnerCodeList.isEmpty())&&(partnerCodeList!=null))
					  {
						  if(!(faqFileList.isEmpty())&&(faqFileList!=null))
						  {
							  Iterator itrPolicy = faqFileList.iterator();
							  while(itrPolicy.hasNext())
							  {
								  FrequentlyAskedQuestions frequentlyAskedQuestions=(FrequentlyAskedQuestions)itrPolicy.next();
								  Iterator itrPolicy1 = partnerCodeList.iterator();
								  while(itrPolicy1.hasNext())
								  {
									  String partnerCodeTemp=(String)itrPolicy1.next();
									  String parRec[]=partnerCodeTemp.split("~");
									  String partnerCode=parRec[0];
									  String partnerId1=parRec[1];
									  List frequentlyAskedQuestionsTempe  = new ArrayList();
									  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
										  frequentlyAskedQuestionsTempe=frequentlyAskedQuestionsManager.getFaqReference(partnerCode, partnerPublicChildUpdate.getCorpID(),frequentlyAskedQuestions.getId());
										  if(!sessionCorpID.equals(partnerPublicChildUpdate.getCorpID())){
											  List oldFaqList = frequentlyAskedQuestionsManager.getFaqReference(partnerCode, sessionCorpID,frequentlyAskedQuestions.getId());
								      		  if(!oldFaqList.isEmpty()){
								 	        			Iterator it = oldFaqList.iterator();
								 	        			while(it.hasNext()){
								 	        				frequentlyAskedQuestionsTempe.add(it.next());
								 	        			}
								      			}
										  }
									  }else{
										  PartnerPublic partnerFaq = partnerPublicManager.getPartnerByCode(partnerPublicChildUpdate.getAgentParent());
										  frequentlyAskedQuestionsTempe=frequentlyAskedQuestionsManager.getFaqReference(partnerCode, sessionCorpID,frequentlyAskedQuestions.getId());
										  if(!sessionCorpID.equals(partnerPublicChildUpdate.getCorpID())){
											  List oldFaqList = frequentlyAskedQuestionsManager.getFaqReference(partnerCode, partnerFaq.getCorpID(),frequentlyAskedQuestions.getId());
								      		  if(!oldFaqList.isEmpty()){
								 	        			Iterator it = oldFaqList.iterator();
								 	        			while(it.hasNext()){
								 	        				frequentlyAskedQuestionsTempe.add(it.next());
								 	        			}
								      			}
										  }
									  }
									  if((frequentlyAskedQuestionsTempe!=null)&&(!frequentlyAskedQuestionsTempe.isEmpty()))
									  {
										  if(frequentlyAskedQuestionsTempe.get(0)!=null)
										  {
										  String fid=frequentlyAskedQuestionsTempe.get(0).toString();
										  FrequentlyAskedQuestions newFrequentlyAskedQuestions=frequentlyAskedQuestionsManager.getForOtherCorpid(Long.parseLong(fid));
										  Long tempId3=newFrequentlyAskedQuestions.getId();
										  beanUtilsContract.copyProperties(newFrequentlyAskedQuestions, frequentlyAskedQuestions);
										  newFrequentlyAskedQuestions.setPartnerCode(partnerCode);
										  newFrequentlyAskedQuestions.setPartnerId(Long.parseLong(partnerId1));
						        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
						        				 newFrequentlyAskedQuestions.setCorpId(partnerPublicChildUpdate.getCorpID());
											  }
											 else{
												 newFrequentlyAskedQuestions.setCorpId(sessionCorpID);
											 }
										  newFrequentlyAskedQuestions.setParentId(frequentlyAskedQuestions.getId());
										  newFrequentlyAskedQuestions.setId(tempId3);
								          newFrequentlyAskedQuestions=frequentlyAskedQuestionsManager.save(newFrequentlyAskedQuestions);
								          childAffected="All Child Accounts have been updated";
										  }
									  }else{
										  FrequentlyAskedQuestions newFrequentlyAskedQuestions= new FrequentlyAskedQuestions();
										  beanUtilsContract.copyProperties(newFrequentlyAskedQuestions, frequentlyAskedQuestions);
										  newFrequentlyAskedQuestions.setPartnerCode(partnerCode);
										  newFrequentlyAskedQuestions.setPartnerId(Long.parseLong(partnerId1));
										  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
						        				 newFrequentlyAskedQuestions.setCorpId(partnerPublicChildUpdate.getCorpID());
											  }
											 else{
												 newFrequentlyAskedQuestions.setCorpId(sessionCorpID);
											 }
										  newFrequentlyAskedQuestions.setCreatedBy(getRequest().getRemoteUser());
										  newFrequentlyAskedQuestions.setUpdatedBy(getRequest().getRemoteUser());
										  newFrequentlyAskedQuestions.setCreatedOn(new Date());
										  newFrequentlyAskedQuestions.setUpdatedOn(new Date());
										  newFrequentlyAskedQuestions.setParentId(frequentlyAskedQuestions.getId());
										  newFrequentlyAskedQuestions.setId(null);
										  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
											  maxSequenceNumber = frequentlyAskedQuestionsManager.findMaximum(newFrequentlyAskedQuestions.getLanguage(),partnerCode,partnerPublicChildUpdate.getCorpID());
										  }else{
											  maxSequenceNumber = frequentlyAskedQuestionsManager.findMaximum(newFrequentlyAskedQuestions.getLanguage(),partnerCode,sessionCorpID);
										  }
												if (maxSequenceNumber.get(0) == null) {
													newFrequentlyAskedQuestions.setSequenceNumber(1L);
												} else {
													autoSequenceNumber = Long.parseLong(maxSequenceNumber.get(0).toString()) + 1;
													newFrequentlyAskedQuestions.setSequenceNumber(autoSequenceNumber);
												}
										  newFrequentlyAskedQuestions=frequentlyAskedQuestionsManager.save(newFrequentlyAskedQuestions);
								          childAffected="All Child Accounts have been updated";													  
									  }
								  }
							  }
					  }else{
						  childAffected="Can not update child as no record is available";
					  }
					  }
		  }
			}catch(Exception e){e.printStackTrace();}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");			
	return SUCCESS;
	}
	public String updateDefaultChildPartner(){
		try{
			String oldAgentParent="";
			List al1=partnerPublicManager.getPartnerPublicDefaultAccount(partnerCode,sessionCorpID);
			 if(al1!=null &&(!al1.isEmpty()) &&(al1.get(0)!=null))
			  {
				 PartnerPublic childPartnerPublic=(PartnerPublic)al1.get(0);
				 oldAgentParent=childPartnerPublic.getAgentParent();
			  } 
		if(pageListType.equalsIgnoreCase("PUB")){
			List DefaultChildPartner= partnerPublicManager.getPartnerPublicDefaultAccount(agentParent,sessionCorpID);
			          if(DefaultChildPartner!=null && ! DefaultChildPartner.isEmpty()){
			        	  PartnerPublic partnerPublic1= (PartnerPublic) DefaultChildPartner.get(0);
						  List al=partnerPublicManager.getPartnerPublicDefaultAccount(partnerCode,sessionCorpID);
						  if(al!=null &&(!al.isEmpty()) &&(al.get(0)!=null))
						  {
							 PartnerPublic childPartnerPublic=(PartnerPublic)al.get(0);
							 if(checkedOption.indexOf("Type")>-1){
								 childPartnerPublic.setPartnerType(partnerPublic1.getPartnerType());
								 if(partnerPublic1.getPartnerType()!=null && (!(partnerPublic1.getPartnerType().toString().trim().equals("")))  && partnerPublic1.getStatus().equalsIgnoreCase("Approved") && (partnerPublic1.getPartnerType().equalsIgnoreCase("CMM") || partnerPublic1.getPartnerType().equalsIgnoreCase("DMM"))){
									 childPartnerPublic.setCorpID("TSFT");
								 }
							 }
							 if(checkedOption.indexOf("logoPhotographs")>-1)
							 {
								 childPartnerPublic.setLocation1(partnerPublic1.getLocation1());
								 childPartnerPublic.setLocation2(partnerPublic1.getLocation2());
								 childPartnerPublic.setLocation3(partnerPublic1.getLocation3());
								 childPartnerPublic.setLocation4(partnerPublic1.getLocation4());
							 }
							 if(checkedOption.indexOf("bankCode")>-1)
							 {
								 childPartnerPublic.setBankCode(partnerPublic1.getBankCode());
							 }
							 if(checkedOption.indexOf("defaultBillingCurrency")>-1)
							 {
								 childPartnerPublic.setBillingCurrency(partnerPublic1.getBillingCurrency());
								
							 }
							if(checkedOption.indexOf("partnerPortalDetail")>-1)
							 {
								 childPartnerPublic.setPartnerPortalActive(partnerPublic1.getPartnerPortalActive());
								 childPartnerPublic.setAssociatedAgents(partnerPublic1.getAssociatedAgents());
								 childPartnerPublic.setViewChild(partnerPublic1.getViewChild());
								 try {
										if(childPartnerPublic.getPartnerPortalActive()!=null)
										{
										boolean portalIdActive = (childPartnerPublic.getPartnerPortalActive() == (true));
										if (portalIdActive) {
											String prefix = "";
											String prefixDataSet = "";
											if(childPartnerPublic.getIsAccount()){
												prefix = "DATA_SECURITY_FILTER_SO_BILLTOCODE_" + childPartnerPublic.getPartnerCode();
												prefixDataSet = "DATA_SECURITY_SET_SO_BILLTOCODE_" + childPartnerPublic.getPartnerCode();
											}else{
												prefix = "DATA_SECURITY_FILTER_AGENT_" + childPartnerPublic.getPartnerCode();
												prefixDataSet = "DATA_SECURITY_SET_AGENT_" + childPartnerPublic.getPartnerCode();
											}
											List agentFilterList = dataSecurityFilterManager.getAgentFilterList(prefix, sessionCorpID);
											List agentFilterSetList = dataSecuritySetManager.getDataSecuritySet(prefixDataSet, sessionCorpID);
											if ((agentFilterList ==null || agentFilterList.isEmpty()) && (agentFilterSetList == null || agentFilterSetList.isEmpty())) {
												List nameList = new ArrayList();
												if(childPartnerPublic.getIsAccount()){
													nameList.add("BILLTOCODE");
												}else{
													nameList.add("BOOKINGAGENTCODE");
													nameList.add("BROKERCODE");
													nameList.add("ORIGINAGENTCODE");
													nameList.add("ORIGINSUBAGENTCODE");
													nameList.add("DESTINATIONAGENTCODE");
													nameList.add("DESTINATIONSUBAGENTCODE");
												}
												dataSecuritySet = new DataSecuritySet();
												Iterator it = nameList.iterator();
												while (it.hasNext()) {
													String fieldName = it.next().toString();
													String postFix = prefix + "_" + fieldName;
													dataSecurityFilter = new DataSecurityFilter();
													if(childPartnerPublic.getIsAccount()){
														dataSecurityFilter.setName(prefix);
													}else{
														dataSecurityFilter.setName(postFix);
													}
													dataSecurityFilter.setTableName("serviceorder");
													dataSecurityFilter.setFieldName(fieldName.toLowerCase());
													dataSecurityFilter.setFilterValues(childPartnerPublic.getPartnerCode().toString());
													dataSecurityFilter.setCorpID(sessionCorpID);
													dataSecurityFilter.setCreatedOn(new Date());
													dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
													dataSecurityFilter.setUpdatedOn(new Date());
													dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
													dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
													dataSecuritySet.addFilters(dataSecurityFilter);
												}
												dataSecuritySet.setName(prefixDataSet);
												dataSecuritySet.setDescription(prefixDataSet);
												dataSecuritySet.setCorpID(sessionCorpID);
												dataSecuritySet.setCreatedOn(new Date());
												dataSecuritySet.setCreatedBy(getRequest().getRemoteUser());
												dataSecuritySet.setUpdatedOn(new Date());
												dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
												dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
											}
										}
								
							             }
										}catch(Exception e){
											e.printStackTrace();
										}
							 }
							
							 childPartnerPublic=partnerPublicManager.save(childPartnerPublic);
							 childAffected="Child account has been updated";
						  }else{
							  childAffected="Can not update child as no record is available";    
						  }
			          }
			          
		          }
		                    if(pagePrivateListType.equalsIgnoreCase("AIS")){
			                List Ais = partnerPrivateManager.getPartnerPrivateDefaultAccount(agentParent,sessionCorpID);
			                        if(Ais!=null && !Ais.isEmpty()){
			                	   partnerPrivate=(PartnerPrivate) Ais.get(0);
								  List al=partnerPrivateManager.getPartnerPrivateDefaultAccount(partnerCode,sessionCorpID);
								  if(al!=null &&(!al.isEmpty()) &&(al.get(0)!=null))
								  {
									 PartnerPrivate childPartnerPrivate=(PartnerPrivate)al.get(0);
									 if(checkedPrivateOption.indexOf("storageBillingGroup")>-1 || checkedPrivateOption.indexOf("controlInfo")>-1 || checkedPrivateOption.indexOf("defaultRoles")>-1 || checkedPrivateOption.indexOf("paymentMethod")>-1 || checkedPrivateOption.indexOf("insurance")>-1 || checkedPrivateOption.indexOf("creditTerms")>-1 || checkedPrivateOption.indexOf("qualityMeasurement")>-1 || checkedPrivateOption.indexOf("defaultRLORoles")>-1 || checkedPrivateOption.indexOf("billingCycle")>-1){
										 if(checkedPrivateOption.indexOf("controlInfo")>-1)
										 {
											 childPartnerPrivate.setPortalDefaultAccess(partnerPrivate.getPortalDefaultAccess());
											 childPartnerPrivate.setGrpAllowed(partnerPrivate.getGrpAllowed());
											
										 }
										 if(checkedPrivateOption.indexOf("defaultRoles")>-1)
										 {
											 childPartnerPrivate.setAccountHolder(partnerPrivate.getAccountHolder());
											 childPartnerPrivate.setAccountManager(partnerPrivate.getAccountManager());
											 childPartnerPrivate.setPricingUser(partnerPrivate.getPricingUser());
											 childPartnerPrivate.setClaimsUser(partnerPrivate.getClaimsUser());
											 childPartnerPrivate.setBillingUser(partnerPrivate.getBillingUser());
											 childPartnerPrivate.setPayableUser(partnerPrivate.getPayableUser());
											 childPartnerPrivate.setAuditor(partnerPrivate.getAuditor());
											 childPartnerPrivate.setCoordinator(partnerPrivate.getCoordinator());
										 }
										 if(checkedPrivateOption.indexOf("defaultRLORoles")>-1)
										 {
											 childPartnerPrivate.setAccountHolderForRelo(partnerPrivate.getAccountHolderForRelo());
											 childPartnerPrivate.setAccountManagerForRelo(partnerPrivate.getAccountManagerForRelo());
											 childPartnerPrivate.setPricingUserForRelo(partnerPrivate.getPricingUserForRelo());
											 childPartnerPrivate.setClaimsUserForRelo(partnerPrivate.getClaimsUserForRelo());
											 childPartnerPrivate.setBillingUserForRelo(partnerPrivate.getBillingUserForRelo());
											 childPartnerPrivate.setPayableUserForRelo(partnerPrivate.getPayableUserForRelo());
											 childPartnerPrivate.setAuditorForRelo(partnerPrivate.getAuditorForRelo());
											 childPartnerPrivate.setCoordinatorForRelo(partnerPrivate.getCoordinatorForRelo());
											 childPartnerPrivate=partnerPrivateManager.save(childPartnerPrivate);
										 }
										 
										 if(checkedPrivateOption.indexOf("paymentMethod")>-1)
										 {
											 childPartnerPrivate.setPaymentMethod(partnerPrivate.getPaymentMethod());
										
										 }
										 if(checkedPrivateOption.indexOf("billingInstruction")>-1)
										 {
											 childPartnerPrivate.setBillingInstruction(partnerPrivate.getBillingInstruction());
										
										 }
										 if(checkedPrivateOption.indexOf("insurance")>-1)
										 {
											 childPartnerPrivate.setInsuranceAuthorized(partnerPrivate.getInsuranceAuthorized());
										
										 }
										 if(checkedPrivateOption.indexOf("creditTerms")>-1)
										 {
											 childPartnerPrivate.setCreditTerms(partnerPrivate.getCreditTerms());
											 											 
										 }
										 if(checkedPrivateOption.indexOf("storageBillingGroup")>-1){
											 childPartnerPrivate.setStorageBillingGroup(partnerPrivate.getStorageBillingGroup());
										 }
										 if(checkedPrivateOption.indexOf("billingCycle")>-1){
											 childPartnerPrivate.setBillingCycle(partnerPrivate.getBillingCycle());
										 }
										 if(checkedPrivateOption.indexOf("storageEmailType")>-1){
											 childPartnerPrivate.setStorageEmailType(partnerPrivate.getStorageEmailType());
										 }
										 if(checkedPrivateOption.indexOf("qualityMeasurement")>-1)
										 {
											 childPartnerPrivate.setNoTransfereeEvaluation(partnerPrivate.getNoTransfereeEvaluation());
											 childPartnerPrivate.setOneRequestPerCF(partnerPrivate.getOneRequestPerCF());
											 childPartnerPrivate.setRatingScale(partnerPrivate.getRatingScale());
											 childPartnerPrivate.setCustomerFeedback(partnerPrivate.getCustomerFeedback());
											 childPartnerPrivate.setDescription1(partnerPrivate.getDescription1());
											 childPartnerPrivate.setDescription2(partnerPrivate.getDescription2());
											 childPartnerPrivate.setDescription3(partnerPrivate.getDescription3());
											 childPartnerPrivate.setLink1(partnerPrivate.getLink1());
											 childPartnerPrivate.setLink2(partnerPrivate.getLink2());
											 childPartnerPrivate.setLink3(partnerPrivate.getLink3());
										 }
										 childPartnerPrivate=partnerPrivateManager.save(childPartnerPrivate); 
										 childAffected="Child account has been updated";
									 }
									 
									 if(checkedPrivateOption.indexOf("entitlement")>-1)
									 {
										 if(!oldAgentParent.equalsIgnoreCase(agentParent)&& !(oldAgentParent.equalsIgnoreCase(partnerCode))){
											 List Ais1 = partnerPrivateManager.getPartnerPrivateDefaultAccount(oldAgentParent,sessionCorpID);
						                        if(Ais1!=null && !Ais1.isEmpty()){
						                        PartnerPrivate partnerPrivate1=(PartnerPrivate) Ais1.get(0);
						                        partnerPublicManager.deleteAgentParent(partnerPrivate1.getPartnerPublicId(), partnerCode ,sessionCorpID);
						                        } 
										 }
										
										 List entitlementList=entitlementManager.findEntitlementByPartnerId(sessionCorpID, partnerPrivate.getPartnerPublicId());
										  BeanUtilsBean beanUtilsContract = BeanUtilsBean.getInstance();
										  beanUtilsContract.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
										  if(!(entitlementList.isEmpty())&&(entitlementList!=null))
										  {
											  Iterator entitlementL = entitlementList.iterator();
											  while(entitlementL.hasNext())
											  {
												  Entitlement entitlement=(Entitlement)entitlementL.next();
												  
												  List entitlementTempe=entitlementManager.getEntitlementReference( partnerCode , sessionCorpID,entitlement.getId());
												  if((entitlementTempe!=null)&&(!entitlementTempe.isEmpty()))
												  {
													  if(entitlementTempe.get(0)!=null)
													  {
													  String fid=entitlementTempe.get(0).toString();
													  Entitlement newEntitlement=entitlementManager.get(Long.parseLong(fid));
													  Long tempId3=newEntitlement.getId();
													  beanUtilsContract.copyProperties(newEntitlement, entitlement);
													  newEntitlement.setPartnerCode(childPartnerPrivate.getPartnerCode());
													  newEntitlement.setPartnerPrivateId(childPartnerPrivate.getPartnerPublicId());
													  newEntitlement.setCorpID(sessionCorpID);
													  newEntitlement.setUpdatedBy(getRequest().getRemoteUser());
													  newEntitlement.setUpdatedOn(new Date());	
													  newEntitlement.setParentId(entitlement.getId());
													  newEntitlement.setId(tempId3);
													  newEntitlement=entitlementManager.save(newEntitlement);
											          childAffected="Child account has been updated";
													  }														  
												  }else{
													  Entitlement newEntitlement= new Entitlement();
													  beanUtilsContract.copyProperties(newEntitlement, entitlement);
													  newEntitlement.setPartnerCode(childPartnerPrivate.getPartnerCode());
													  newEntitlement.setPartnerPrivateId(childPartnerPrivate.getPartnerPublicId());
													  newEntitlement.setCorpID(sessionCorpID);
													  newEntitlement.setCreatedBy(getRequest().getRemoteUser());
													  newEntitlement.setUpdatedBy(getRequest().getRemoteUser());
													  newEntitlement.setCreatedOn(new Date());
													  newEntitlement.setUpdatedOn(new Date());
													  newEntitlement.setParentId(entitlement.getId());
													  newEntitlement.setId(null);
													  newEntitlement=entitlementManager.save(newEntitlement);
											          childAffected="Child account has been updated";														  
												  }
											  }
										  }
									 }
									 if(checkedPrivateOption.indexOf("accountAssignmentType")>-1)
									 {
										 List accountAssignmentTypeList=accountAssignmentTypeManager.getAssignmentTypeList(agentParent, partnerPrivate.getPartnerPublicId(), sessionCorpID);
										  BeanUtilsBean beanUtilsContract = BeanUtilsBean.getInstance();
										  beanUtilsContract.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
										  if(!(accountAssignmentTypeList.isEmpty())&&(accountAssignmentTypeList!=null))
										  {
											  Iterator accountAssignmentTypeL = accountAssignmentTypeList.iterator();
											  while(accountAssignmentTypeL.hasNext())
											  {
												  AccountAssignmentType accountAssignmentType=(AccountAssignmentType)accountAssignmentTypeL.next();
												  List accountAssignmentTypeTempe=accountAssignmentTypeManager.getaccountAssignmentTypeReference(partnerCode, sessionCorpID,accountAssignmentType.getId());
												  if((accountAssignmentTypeTempe!=null)&&(!accountAssignmentTypeTempe.isEmpty()))
												  {
													  if(accountAssignmentTypeTempe.get(0)!=null)
													  {
													  String fid=accountAssignmentTypeTempe.get(0).toString();
													  AccountAssignmentType newAccountAssignmentType=accountAssignmentTypeManager.get(Long.parseLong(fid));
													  Long tempId3=newAccountAssignmentType.getId();
													  beanUtilsContract.copyProperties(newAccountAssignmentType, accountAssignmentType);
													  newAccountAssignmentType.setPartnerCode(childPartnerPrivate.getPartnerCode());
													  newAccountAssignmentType.setPartnerPrivateId(childPartnerPrivate.getPartnerPublicId());
													  newAccountAssignmentType.setCorpId(sessionCorpID);
													  newAccountAssignmentType.setUpdatedBy(getRequest().getRemoteUser());
													  newAccountAssignmentType.setUpdatedOn(new Date());	
													  newAccountAssignmentType.setParentId(accountAssignmentType.getId());
													  newAccountAssignmentType.setId(tempId3);
													  newAccountAssignmentType=accountAssignmentTypeManager.save(newAccountAssignmentType);
											          childAffected="Child account has been updated";
													  }														  
												  }else{
													  AccountAssignmentType newAccountAssignmentType= new AccountAssignmentType();
													  beanUtilsContract.copyProperties(newAccountAssignmentType, accountAssignmentType);
													  newAccountAssignmentType.setPartnerCode(childPartnerPrivate.getPartnerCode());
													  newAccountAssignmentType.setPartnerPrivateId(childPartnerPrivate.getPartnerPublicId());
													  newAccountAssignmentType.setCorpId(sessionCorpID);
													  newAccountAssignmentType.setCreatedBy(getRequest().getRemoteUser());
													  newAccountAssignmentType.setUpdatedBy(getRequest().getRemoteUser());
													  newAccountAssignmentType.setCreatedOn(new Date());
													  newAccountAssignmentType.setUpdatedOn(new Date());
													  newAccountAssignmentType.setParentId(accountAssignmentType.getId());
													  newAccountAssignmentType.setId(null);
													  newAccountAssignmentType=accountAssignmentTypeManager.save(newAccountAssignmentType);
											          childAffected="Child account has been updated";														  
												  }
											  }
										  }																					 
									 }
									
									 if(checkedPrivateOption.indexOf("InsuranceSetUp")>-1){
                                   	  childPartnerPrivate.setLumpSum(partnerPrivate.getLumpSum()); 
                                   	  childPartnerPrivate.setLumpSumAmount(partnerPrivate.getLumpSumAmount());
                                   	  childPartnerPrivate.setLumpPerUnit(partnerPrivate.getLumpPerUnit());
                                   	  childPartnerPrivate.setMinReplacementvalue(partnerPrivate.getMinReplacementvalue());
                                   	  childPartnerPrivate.setDetailedList(partnerPrivate.getDetailedList());
                                   	  childPartnerPrivate.setNoInsurance(partnerPrivate.getNoInsurance());
									 }
									 if(checkedPrivateOption.indexOf("contract")>-1){
										 childPartnerPrivate.setContract(partnerPrivate.getContract());
									 }
									 if(checkedPrivateOption.indexOf("networkPartnerCode")>-1){
										 childPartnerPrivate.setNetworkPartnerCode(partnerPrivate.getNetworkPartnerCode());
										 String bookingAgentName = contractPolicyManager.findByBookingLastName(partnerPrivate.getNetworkPartnerCode(), sessionCorpID);
										 childPartnerPrivate.setNetworkPartnerName(partnerPrivate.getNetworkPartnerName());
									 }
									 if(checkedPrivateOption.indexOf("source")>-1){
										 childPartnerPrivate.setSource(partnerPrivate.getSource());
									 }
									 if(checkedPrivateOption.indexOf("vendorCode")>-1){
										 childPartnerPrivate.setVendorCode(partnerPrivate.getVendorCode());
										 String bookingAgentName = contractPolicyManager.findByBookingLastName(partnerPrivate.getVendorCode(), sessionCorpID);
										 childPartnerPrivate.setVendorName(partnerPrivate.getVendorName());
									 }
									 if(checkedPrivateOption.indexOf("insuranceHas")>-1){
										 childPartnerPrivate.setInsuranceHas(partnerPrivate.getInsuranceHas());
									 }
									 if(checkedPrivateOption.indexOf("insuranceOption")>-1){
										 childPartnerPrivate.setInsuranceOption(partnerPrivate.getInsuranceOption());
									 }
									 if(checkedPrivateOption.indexOf("defaultVat")>-1){
										 childPartnerPrivate.setDefaultVat(partnerPrivate.getDefaultVat());
									 }
									 if(checkedPrivateOption.indexOf("billToAuthorization")>-1){
										 childPartnerPrivate.setBillToAuthorization(partnerPrivate.getBillToAuthorization());
									 }
									 if(checkedPrivateOption.indexOf("rddBased")>-1){
										 childPartnerPrivate.setRddBased(partnerPrivate.getRddBased());
									 }
									 if(checkedPrivateOption.indexOf("rddDays")>-1){
										 childPartnerPrivate.setRddDays(partnerPrivate.getRddDays());
									 }
									 childPartnerPrivate=partnerPrivateManager.save(childPartnerPrivate); 

									 childAffected="Child account has been updated";
								  }else{
									  childAffected="Can not update child as no record is available";    
								  }
			                }
			       
		                }
		  if(pagePrsListType.equalsIgnoreCase("PRS")){
			  List DefaultChild = partnerPublicManager.getPartnerPublicDefaultAccount(agentParent,sessionCorpID);
			   if(DefaultChild!=null && !DefaultChild.isEmpty()){
				   PartnerPublic partnerPublic1= (PartnerPublic) DefaultChild.get(0);
			       /*   List partnerCodeList = new ArrayList();
						  partnerCodeList=partnerPublicManager.getPartnerPublicChildAccount(partnerCode,sessionCorpID);
					  if(!(partnerCodeList.isEmpty())&&(partnerCodeList!=null))
					  {
						  Iterator itrPolicy = partnerCodeList.iterator();
						  while(itrPolicy.hasNext())
						  {*/
							  //String partnerCode=(String)itrPolicy.next();
							  List al=partnerPublicManager.getPartnerPublicDefaultAccount(partnerCode,sessionCorpID);
							  if(al!=null &&(!al.isEmpty()) &&(al.get(0)!=null))
							  {
								 PartnerPublic childPartnerPublic=(PartnerPublic)al.get(0);
								 childPartnerPublic.setReloContact(partnerPublic1.getReloContact());
								 childPartnerPublic.setReloContactEmail(partnerPublic1.getReloContactEmail());
								 childPartnerPublic.setReloServices(partnerPublic1.getReloServices());
								 childPartnerPublic.setTransPort(partnerPublic1.getTransPort());
								 childPartnerPublic=partnerPublicManager.save(childPartnerPublic);
								 childAffected="Child account has been updated";
							  }else{
								  childAffected="Child account has been updated";  
							  }
					/*	  }
					  }*/			  
			   }
			  		  
		  }
		
		if(pageCPFListType.equalsIgnoreCase("CPF"))
		  {
			
			        PartnerPublic partnerPublicChildUpdate = partnerPublicManager.getPartnerByCode(agentParent);
					  BeanUtilsBean beanUtilsContract = BeanUtilsBean.getInstance();
					  beanUtilsContract.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					  List partnerCodeList = new ArrayList();
					  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
						  policyFileList=contractPolicyManager.findPolicyFileList(agentParent,partnerPublicChildUpdate.getCorpID());
						  
					  }
					  else{
						  policyFileList=contractPolicyManager.findPolicyFileList(agentParent,sessionCorpID);
					  }
					  if(!oldAgentParent.equalsIgnoreCase(agentParent) && !(oldAgentParent.equalsIgnoreCase(partnerCode)) ){
						  contractPolicyManager.deleteagentParentPolicyFile(oldAgentParent,partnerCode,sessionCorpID);
						}
					  partnerCodeList=partnerPublicManager.getPartnerPublicDefaultAccount(partnerCode,sessionCorpID);
					  //String parentPartnerCode=partnerCode;
					  if(!(partnerCodeList.isEmpty())&&(partnerCodeList!=null))
					  {
						  if(!(policyFileList.isEmpty())&&(policyFileList!=null))
						  {
							  Iterator itrPolicy = policyFileList.iterator();
							  while(itrPolicy.hasNext())
							  {
								  ContractPolicy contractPolicy=(ContractPolicy)itrPolicy.next();
								 /* Iterator itrCodPolicy = partnerCodeList.iterator();
								  while(itrCodPolicy.hasNext())
								  {*/
									  List contractPolicyTempe = new ArrayList();
								     // PartnerPublic pp=(PartnerPublic)itrCodPolicy.next();
									  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
										  contractPolicyTempe=contractPolicyManager.getPolicyReference(partnerCode, partnerPublicChildUpdate.getCorpID(),contractPolicy.getId());	
									  }
									  else{
										  contractPolicyTempe=contractPolicyManager.getPolicyReference(partnerCode, sessionCorpID,contractPolicy.getId());	
									  }
									  										  
									  if((contractPolicyTempe!=null)&&(!contractPolicyTempe.isEmpty()))
									  {
										  if(contractPolicyTempe.get(0)!=null)
										  {		
											  String cid=contractPolicyTempe.get(0).toString();
											  ContractPolicy newContractPolicy=contractPolicyManager.get(Long.parseLong(cid));
											  Long tempId=newContractPolicy.getId();
											  beanUtilsContract.copyProperties(newContractPolicy, contractPolicy);
											  newContractPolicy.setPartnerCode(partnerCode);
											  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
												  newContractPolicy.setCorpID(partnerPublicChildUpdate.getCorpID());
											  }
											  else{
												  newContractPolicy.setCorpID(sessionCorpID);
											  }
											  newContractPolicy.setUpdatedBy(getRequest().getRemoteUser());
											  newContractPolicy.setUpdatedOn(new Date());
											  newContractPolicy.setParentId(contractPolicy.getId());
											  newContractPolicy.setId(tempId);
									        	  contractPolicyManager.save(newContractPolicy);
									          childAffected="Child Account have been updated";
									          List policyDocumentList2 = new ArrayList();
									          if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
									        	  policyDocumentList2=contractPolicyManager.findDocumentList(agentParent,contractPolicy.getSectionName(),contractPolicy.getLanguage(),partnerPublicChildUpdate.getCorpID());
											  }
											  else{
												  policyDocumentList2=contractPolicyManager.findDocumentList(agentParent,contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
											  }
									          
									          Iterator itrDocPolicy = policyDocumentList2.iterator();
									          while(itrDocPolicy.hasNext())
									          {
									        	 PolicyDocument policyDocument=(PolicyDocument)itrDocPolicy.next();
									        	 List policyDocumentTempe = new ArrayList();
										          if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
										        	  policyDocumentTempe=contractPolicyManager.getPolicyDocReference(partnerCode,partnerPublicChildUpdate.getCorpID(),policyDocument.getId(),policyDocument.getSectionName(),policyDocument.getLanguage());
												  }
												  else{
													  policyDocumentTempe=contractPolicyManager.getPolicyDocReference(partnerCode,sessionCorpID,policyDocument.getId(),policyDocument.getSectionName(),policyDocument.getLanguage());
												  }
												  if((policyDocumentTempe!=null)&&(!policyDocumentTempe.isEmpty()))
												  {
													  if(policyDocumentTempe.get(0)!=null)
													  {
														 String pid=policyDocumentTempe.get(0).toString();
														 PolicyDocument newPolicyDocument=policyDocumentManager.get(Long.parseLong(pid));
														 Long tempId1=newPolicyDocument.getId();
									        			 beanUtilsContract.copyProperties(newPolicyDocument, policyDocument);
									        			 newPolicyDocument.setPartnerCode(partnerCode);
									        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
									        				 newPolicyDocument.setCorpID(partnerPublicChildUpdate.getCorpID());
														  }
														  else{
															  newPolicyDocument.setCorpID(sessionCorpID);
														  }
									        			 newPolicyDocument.setUpdatedBy(getRequest().getRemoteUser());
									        			 newPolicyDocument.setUpdatedOn(new Date());
									        			 newPolicyDocument.setParentId(policyDocument.getId());
									        			 newPolicyDocument.setId(tempId1);
									        			 newPolicyDocument=policyDocumentManager.save(newPolicyDocument);
										        		 childAffected="Child account has been updated";
															
														}
									             }else{
								            	 		 PolicyDocument newPolicyDocument= new PolicyDocument();
									        			 beanUtilsContract.copyProperties(newPolicyDocument, policyDocument);
									        			 newPolicyDocument.setPartnerCode(partnerCode);
									        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
									        				 newPolicyDocument.setCorpID(partnerPublicChildUpdate.getCorpID());
														  }
														  else{
															  newPolicyDocument.setCorpID(sessionCorpID);
														  }
									        			 newPolicyDocument.setCreatedBy(getRequest().getRemoteUser());
									        			 newPolicyDocument.setUpdatedBy(getRequest().getRemoteUser());
									        			 newPolicyDocument.setCreatedOn(new Date());
									        			 newPolicyDocument.setUpdatedOn(new Date());
									        			 newPolicyDocument.setParentId(policyDocument.getId());
									        			 newPolicyDocument.setId(null);
									        			 newPolicyDocument=policyDocumentManager.save(newPolicyDocument);
										        		 childAffected="Child account has been updated";
									             }
											 }
											}												  
									  }else{
										  ContractPolicy newContractPolicy= new ContractPolicy();
										  beanUtilsContract.copyProperties(newContractPolicy, contractPolicy);
										  newContractPolicy.setPartnerCode(partnerCode);
										  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
											  newContractPolicy.setCorpID(partnerPublicChildUpdate.getCorpID());
										  }
										 else{
											 newContractPolicy.setCorpID(sessionCorpID);
										 }
										  newContractPolicy.setCreatedBy(getRequest().getRemoteUser());
										  newContractPolicy.setUpdatedBy(getRequest().getRemoteUser());
										  newContractPolicy.setCreatedOn(new Date());
										  newContractPolicy.setUpdatedOn(new Date());
										  newContractPolicy.setParentId(contractPolicy.getId());
										  newContractPolicy.setId(null);
								        	  contractPolicyManager.save(newContractPolicy);
								          childAffected="Child account has been updated";
								          List policyDocumentList2 = new ArrayList();
								          if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
								        	  policyDocumentList2=contractPolicyManager.findDocumentList(agentParent,contractPolicy.getSectionName(),contractPolicy.getLanguage(),partnerPublicChildUpdate.getCorpID());
										  }
										  else{
											  policyDocumentList2=contractPolicyManager.findDocumentList(agentParent,contractPolicy.getSectionName(),contractPolicy.getLanguage(),sessionCorpID);
										  }
								          
								          Iterator itrDocPolicy = policyDocumentList2.iterator();
								          while(itrDocPolicy.hasNext())
								          {
								        	 PolicyDocument policyDocument=(PolicyDocument)itrDocPolicy.next();
								        	 List policyDocumentTempe = new ArrayList();
									          if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
									        	  policyDocumentTempe=contractPolicyManager.getPolicyDocReference(partnerCode,partnerPublicChildUpdate.getCorpID(),policyDocument.getId(),policyDocument.getSectionName(),policyDocument.getLanguage());
											  }
											  else{
												  policyDocumentTempe=contractPolicyManager.getPolicyDocReference(partnerCode,sessionCorpID,policyDocument.getId(),policyDocument.getSectionName(),policyDocument.getLanguage());
											  }
								        	 
											  if((policyDocumentTempe!=null)&&(!policyDocumentTempe.isEmpty()))
											  {
												  if(policyDocumentTempe.get(0)!=null)
												  {
													 String pid=policyDocumentTempe.get(0).toString();
													 PolicyDocument newPolicyDocument=policyDocumentManager.get(Long.parseLong(pid));
													 Long tempId1=newPolicyDocument.getId();
								        			 beanUtilsContract.copyProperties(newPolicyDocument, policyDocument);
								        			 newPolicyDocument.setPartnerCode(partnerCode);
								        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
								        				 newPolicyDocument.setCorpID(partnerPublicChildUpdate.getCorpID());
													  }
													 else{
														 newPolicyDocument.setCorpID(sessionCorpID);
													 }
								        			 newPolicyDocument.setUpdatedBy(getRequest().getRemoteUser());
								        			 newPolicyDocument.setUpdatedOn(new Date());
								        			 newPolicyDocument.setParentId(policyDocument.getId());
								        			 newPolicyDocument.setId(tempId1);
									        	     policyDocumentManager.save(newPolicyDocument);
									        		 childAffected="Child account has been updated";
														
													}
								             }else{
							            	 		 PolicyDocument newPolicyDocument= new PolicyDocument();
								        			 beanUtilsContract.copyProperties(newPolicyDocument, policyDocument);
								        			 newPolicyDocument.setPartnerCode(partnerCode);
								        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
								        				 newPolicyDocument.setCorpID(partnerPublicChildUpdate.getCorpID());
													  }
													 else{
														 newPolicyDocument.setCorpID(sessionCorpID);
													 }
								        			 newPolicyDocument.setCreatedBy(getRequest().getRemoteUser());
								        			 newPolicyDocument.setUpdatedBy(getRequest().getRemoteUser());
								        			 newPolicyDocument.setCreatedOn(new Date());
								        			 newPolicyDocument.setUpdatedOn(new Date());
								        			 newPolicyDocument.setParentId(policyDocument.getId());
								        			 newPolicyDocument.setId(null);
									        	     policyDocumentManager.save(newPolicyDocument);
									        		 childAffected="Child account has been updated";
								             }
										 }
								          
									  }
								/*  }*/
							  }
					  }else{
						  childAffected="Child account has been updated";
					  }
					  }
		  }
		if(pageFaqInfo.equalsIgnoreCase("FAQ"))
		  {
			
			PartnerPublic partnerPublicChildUpdate = partnerPublicManager.getPartnerByCode(agentParent);
					  BeanUtilsBean beanUtilsContract = BeanUtilsBean.getInstance();
					  beanUtilsContract.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					  List faqFileList=new ArrayList();
					  List partnerCodeList = new ArrayList();
					  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
						  faqFileList = frequentlyAskedQuestionsManager.getFAQByPartnerCode(agentParent,partnerPublicChildUpdate.getCorpID());
						  if(!sessionCorpID.equals(partnerPublicChildUpdate.getCorpID())){
							  List oldFaqList = frequentlyAskedQuestionsManager.getFaqByPartnerCode(agentParent,sessionCorpID);
				      		  if(!oldFaqList.isEmpty()){
				 	        			Iterator it = oldFaqList.iterator();
				 	        			while(it.hasNext()){
				 	        				faqFileList.add(it.next());
				 	        			}
				      			}
						  }
					  }
					  else{
						  PartnerPublic partnerFaq = partnerPublicManager.getPartnerByCode(partnerPublicChildUpdate.getAgentParent());
						  faqFileList = frequentlyAskedQuestionsManager.getFaqByPartnerCode(agentParent,sessionCorpID);
						  if(!sessionCorpID.equals(partnerPublicChildUpdate.getCorpID())){
							  List oldFaqList = frequentlyAskedQuestionsManager.getFAQByPartnerCode(agentParent,partnerFaq.getCorpID());
				      		  if(!oldFaqList.isEmpty()){
				 	        			Iterator it = oldFaqList.iterator();
				 	        			while(it.hasNext()){
				 	        				faqFileList.add(it.next());
				 	        			}
				      			}
						  }
					  }
					  if(!oldAgentParent.equalsIgnoreCase(agentParent) && !(oldAgentParent.equalsIgnoreCase(partnerCode)) ){
						  frequentlyAskedQuestionsManager.deleteagentParentPolicyFile(oldAgentParent,partnerCode,sessionCorpID);
						}
					partnerCodeList=partnerPublicManager.getPartnerPublicDefaultAccount(partnerCode,sessionCorpID);
					  if(!(partnerCodeList.isEmpty())&&(partnerCodeList!=null))
					  {
						  if(!(faqFileList.isEmpty())&&(faqFileList!=null))
						  {
							  Iterator itrPolicy = faqFileList.iterator();
							  while(itrPolicy.hasNext())
							  {
								  FrequentlyAskedQuestions frequentlyAskedQuestions=(FrequentlyAskedQuestions)itrPolicy.next();
								/*  Iterator itrPolicy1 = partnerCodeList.iterator();
								  while(itrPolicy1.hasNext())
								  
									  String partnerCodeTemp=(String)itrPolicy1.next();
									  String parRec[]=partnerCodeTemp.split("~");
									  //String partnerCode=parRec[0];
									  String partnerId1=parRec[1];{*/
								  PartnerPublic partnerPublic1= (PartnerPublic) partnerCodeList.get(0);
								 
									  List frequentlyAskedQuestionsTempe  = new ArrayList();
									  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
										  frequentlyAskedQuestionsTempe=frequentlyAskedQuestionsManager.getFaqReference(partnerCode, partnerPublicChildUpdate.getCorpID(),frequentlyAskedQuestions.getId());
										  if(!sessionCorpID.equals(partnerPublicChildUpdate.getCorpID())){
											  List oldFaqList = frequentlyAskedQuestionsManager.getFaqReference(partnerCode, sessionCorpID,frequentlyAskedQuestions.getId());
								      		  if(!oldFaqList.isEmpty()){
								 	        			Iterator it = oldFaqList.iterator();
								 	        			while(it.hasNext()){
								 	        				frequentlyAskedQuestionsTempe.add(it.next());
								 	        			}
								      			}
										  }
									  }else{
										  PartnerPublic partnerFaq = partnerPublicManager.getPartnerByCode(partnerPublicChildUpdate.getAgentParent());
										  frequentlyAskedQuestionsTempe=frequentlyAskedQuestionsManager.getFaqReference(partnerCode, sessionCorpID,frequentlyAskedQuestions.getId());
										  if(!sessionCorpID.equals(partnerPublicChildUpdate.getCorpID())){
											  List oldFaqList = frequentlyAskedQuestionsManager.getFaqReference(partnerCode, partnerFaq.getCorpID(),frequentlyAskedQuestions.getId());
								      		  if(!oldFaqList.isEmpty()){
								 	        			Iterator it = oldFaqList.iterator();
								 	        			while(it.hasNext()){
								 	        				frequentlyAskedQuestionsTempe.add(it.next());
								 	        			}
								      			}
										  }
									  }
									  if((frequentlyAskedQuestionsTempe!=null)&&(!frequentlyAskedQuestionsTempe.isEmpty()))
									  {
										  if(frequentlyAskedQuestionsTempe.get(0)!=null)
										  {
										  String fid=frequentlyAskedQuestionsTempe.get(0).toString();
										  FrequentlyAskedQuestions newFrequentlyAskedQuestions=frequentlyAskedQuestionsManager.getForOtherCorpid(Long.parseLong(fid));
										  Long tempId3=newFrequentlyAskedQuestions.getId();
										  beanUtilsContract.copyProperties(newFrequentlyAskedQuestions, frequentlyAskedQuestions);
										  newFrequentlyAskedQuestions.setPartnerCode(partnerCode);
										  newFrequentlyAskedQuestions.setPartnerId(partnerPublic1.getId());
						        			 if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
						        				 newFrequentlyAskedQuestions.setCorpId(partnerPublicChildUpdate.getCorpID());
											  }
											 else{
												 newFrequentlyAskedQuestions.setCorpId(sessionCorpID);
											 }
										  newFrequentlyAskedQuestions.setUpdatedBy(getRequest().getRemoteUser());
										  newFrequentlyAskedQuestions.setUpdatedOn(new Date());	
										  newFrequentlyAskedQuestions.setParentId(frequentlyAskedQuestions.getId());
										  newFrequentlyAskedQuestions.setId(tempId3);
								          newFrequentlyAskedQuestions=frequentlyAskedQuestionsManager.save(newFrequentlyAskedQuestions);
								          childAffected="Child account has been updated";
										  }
									  }else{
										  FrequentlyAskedQuestions newFrequentlyAskedQuestions= new FrequentlyAskedQuestions();
										  beanUtilsContract.copyProperties(newFrequentlyAskedQuestions, frequentlyAskedQuestions);
										  newFrequentlyAskedQuestions.setPartnerCode(partnerCode);
										  newFrequentlyAskedQuestions.setPartnerId(partnerPublic1.getId());
										  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
						        				 newFrequentlyAskedQuestions.setCorpId(partnerPublicChildUpdate.getCorpID());
											  }
											 else{
												 newFrequentlyAskedQuestions.setCorpId(sessionCorpID);
											 }
										  newFrequentlyAskedQuestions.setCreatedBy(getRequest().getRemoteUser());
										  newFrequentlyAskedQuestions.setUpdatedBy(getRequest().getRemoteUser());
										  newFrequentlyAskedQuestions.setCreatedOn(new Date());
										  newFrequentlyAskedQuestions.setUpdatedOn(new Date());
										  newFrequentlyAskedQuestions.setParentId(frequentlyAskedQuestions.getId());
										  newFrequentlyAskedQuestions.setId(null);
										  if(partnerPublicChildUpdate.getPartnerType()!=null && (!(partnerPublicChildUpdate.getPartnerType().toString().trim().equals("")))  && partnerPublicChildUpdate.getStatus().equalsIgnoreCase("Approved") && (partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicChildUpdate.getPartnerType().equalsIgnoreCase("DMM"))){
											  maxSequenceNumber = frequentlyAskedQuestionsManager.findMaximum(newFrequentlyAskedQuestions.getLanguage(),partnerCode,partnerPublicChildUpdate.getCorpID());
										  }else{
											  maxSequenceNumber = frequentlyAskedQuestionsManager.findMaximum(newFrequentlyAskedQuestions.getLanguage(),partnerCode,sessionCorpID);
										  }
												if (maxSequenceNumber.get(0) == null) {
													newFrequentlyAskedQuestions.setSequenceNumber(1L);
												} else {
													autoSequenceNumber = Long.parseLong(maxSequenceNumber.get(0).toString()) + 1;
													newFrequentlyAskedQuestions.setSequenceNumber(autoSequenceNumber);
												}
										  newFrequentlyAskedQuestions=frequentlyAskedQuestionsManager.save(newFrequentlyAskedQuestions);
								          childAffected="Child account has been updated";													  
									  }
								/*  }*/
							  }
					  }else{
						  childAffected="Child account has been updated";
					  }
					  }
		  }
		List DefaultChildPartner= partnerPublicManager.getPartnerPublicDefaultAccount(agentParent,sessionCorpID);
        if(DefaultChildPartner!=null && ! DefaultChildPartner.isEmpty()){
        	PartnerPublic partnerPublic1= (PartnerPublic) DefaultChildPartner.get(0);	
       	 List al=partnerPublicManager.getPartnerPublicDefaultAccount(partnerCode,sessionCorpID);
		 if(al!=null &&(!al.isEmpty()) &&(al.get(0)!=null))
		  {
			 PartnerPublic childPartnerPublic=(PartnerPublic)al.get(0);
			 childPartnerPublic.setAgentParent(agentParent);
			 childPartnerPublic.setAgentParentName(partnerPublic1.getLastName());
			 childPartnerPublic.setUpdatedOn(new Date());
			 childPartnerPublic.setUpdatedBy(getRequest().getRemoteUser());
			 childPartnerPublic=partnerPublicManager.save(childPartnerPublic);
		  }
        }
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;	
	}
	public String editorHyperlink(){
		return SUCCESS;	
	}
	public String editorTable(){
		return SUCCESS;	
	}
	public String editorImage(){
		return SUCCESS;	
	}
	public String editorColor(){
		return SUCCESS;	
	}
	public String saveOnTabChange() throws Exception {
	    //if (option enabled for this company in the system parameters table then call save) {
			validateFormNav = "OK";
	        String s = save();    // else simply navigate to the requested page)
	        return gotoPageString;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public PartnerPrivate getPartnerPrivate() {
		return partnerPrivate;
	}

	public void setPartnerPrivate(PartnerPrivate partnerPrivate) {
		this.partnerPrivate = partnerPrivate;
	}

	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public List getPolicyFileList() {
		return policyFileList;
	}

	public void setPolicyFileList(List policyFileList) {
		this.policyFileList = policyFileList;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getLanguageList() {
		return languageList;
	}

	public void setLanguageList(Map<String, String> languageList) {
		this.languageList = languageList;
	}

	public PartnerManager getPartnerManager() {
		return partnerManager;
	}

	public List getPolicyDocumentList() {
		return policyDocumentList;
	}

	public void setPolicyDocumentList(List policyDocumentList) {
		this.policyDocumentList = policyDocumentList;
	}

	public Long getId1() {
		return id1;
	}

	public void setId1(Long id1) {
		this.id1 = id1;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public Long getId() {
		return id;
	}

	public Map<String, String> getSectionNameList() {
		return sectionNameList;
	}

	public void setSectionNameList(Map<String, String> sectionNameList) {
		this.sectionNameList = sectionNameList;
	}

	public void setPolicyDocumentManager(PolicyDocumentManager policyDocumentManager) {
		this.policyDocumentManager = policyDocumentManager;
	}

	public Long getId2() {
		return id2;
	}

	public void setId2(Long id2) {
		this.id2 = id2;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getDummsectionName() {
		return dummsectionName;
	}
	public void setDummsectionName(String dummsectionName) {
		this.dummsectionName = dummsectionName;
	}

	public String getDummlanguage() {
		return dummlanguage;
	}

	public void setDummlanguage(String dummlanguage) {
		this.dummlanguage = dummlanguage;
	}

	public String getExcludeFPU() {
		return excludeFPU;
	}

	public void setExcludeFPU(String excludeFPU) {
		this.excludeFPU = excludeFPU;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getChildAffected() {
		return childAffected;
	}

	public void setChildAffected(String childAffected) {
		this.childAffected = childAffected;
	}

	public String getPageListType() {
		return pageListType;
	}

	public void setPageListType(String pageListType) {
		this.pageListType = pageListType;
	}

	public void setFrequentlyAskedQuestionsManager(
			FrequentlyAskedQuestionsManager frequentlyAskedQuestionsManager) {
		this.frequentlyAskedQuestionsManager = frequentlyAskedQuestionsManager;
	}

	public String getCheckedOption() {
		return checkedOption;
	}

	public void setCheckedOption(String checkedOption) {
		this.checkedOption = checkedOption;
	}

	public String getExcludeOption() {
		return excludeOption;
	}

	public void setExcludeOption(String excludeOption) {
		this.excludeOption = excludeOption;
	}

	public List getMaxSequenceNumber() {
		return maxSequenceNumber;
	}

	public void setMaxSequenceNumber(List maxSequenceNumber) {
		this.maxSequenceNumber = maxSequenceNumber;
	}

	public Long getAutoSequenceNumber() {
		return autoSequenceNumber;
	}

	public void setAutoSequenceNumber(Long autoSequenceNumber) {
		this.autoSequenceNumber = autoSequenceNumber;
	}

	public String getDocumentSectionName() {
		return documentSectionName;
	}

	public void setDocumentSectionName(String documentSectionName) {
		this.documentSectionName = documentSectionName;
	}

	public String getDocumentLanguage() {
		return documentLanguage;
	}

	public void setDocumentLanguage(String documentLanguage) {
		this.documentLanguage = documentLanguage;
	}

	public void setQualitySurveySettingsManager(
			QualitySurveySettingsManager qualitySurveySettingsManager) {
		this.qualitySurveySettingsManager = qualitySurveySettingsManager;
	}

	public void setEntitlementManager(EntitlementManager entitlementManager) {
		this.entitlementManager = entitlementManager;
	}

	public PartnerPublic getPartnerPolicy() {
		return partnerPolicy;
	}

	public void setPartnerPolicy(PartnerPublic partnerPolicy) {
		this.partnerPolicy = partnerPolicy;
	}

	public String getUserCorpID() {
		return userCorpID;
	}

	public void setUserCorpID(String userCorpID) {
		this.userCorpID = userCorpID;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public void setAccountAssignmentTypeManager(
			AccountAssignmentTypeManager accountAssignmentTypeManager) {
		this.accountAssignmentTypeManager = accountAssignmentTypeManager;
	}

	public String getCheckedPrivateOption() {
		return checkedPrivateOption;
	}

	public void setCheckedPrivateOption(String checkedPrivateOption) {
		this.checkedPrivateOption = checkedPrivateOption;
	}

	public String getAgentParent() {
		return agentParent;
	}

	public void setAgentParent(String agentParent) {
		this.agentParent = agentParent;
	}

	public String getPagePrivateListType() {
		return pagePrivateListType;
	}

	public void setPagePrivateListType(String pagePrivateListType) {
		this.pagePrivateListType = pagePrivateListType;
	}

	public String getPagePrsListType() {
		return pagePrsListType;
	}

	public void setPagePrsListType(String pagePrsListType) {
		this.pagePrsListType = pagePrsListType;
	}

	public String getPageCPFListType() {
		return pageCPFListType;
	}

	public void setPageCPFListType(String pageCPFListType) {
		this.pageCPFListType = pageCPFListType;
	}

	public String getPageFaqInfo() {
		return pageFaqInfo;
	}

	public void setPageFaqInfo(String pageFaqInfo) {
		this.pageFaqInfo = pageFaqInfo;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public String getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getMoveTypeVal() {
		return moveTypeVal;
	}

	public void setMoveTypeVal(String moveTypeVal) {
		this.moveTypeVal = moveTypeVal;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}


	public void setDataSecurityFilterManager(
			DataSecurityFilterManager dataSecurityFilterManager) {
		this.dataSecurityFilterManager = dataSecurityFilterManager;
	}

	public DataSecuritySet getDataSecuritySet() {
		return dataSecuritySet;
	}

	public void setDataSecuritySet(DataSecuritySet dataSecuritySet) {
		this.dataSecuritySet = dataSecuritySet;
	}

	public DataSecurityFilter getDataSecurityFilter() {
		return dataSecurityFilter;
	}

	public void setDataSecurityFilter(DataSecurityFilter dataSecurityFilter) {
		this.dataSecurityFilter = dataSecurityFilter;
	}

	public void setDataSecuritySetManager(
			DataSecuritySetManager dataSecuritySetManager) {
		this.dataSecuritySetManager = dataSecuritySetManager;
	}

}
