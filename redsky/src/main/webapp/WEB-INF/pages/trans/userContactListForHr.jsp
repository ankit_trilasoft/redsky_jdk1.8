<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head> 
    <title>Agent Directory</title>   
    <meta name="heading" content="Agent Directory"/>  
<script language="javascript" type="text/javascript">
function getValue(name, mail, phone){
	var fileFor='${myFileFor}';
	if(fileFor=='CF'){
	window.opener.document.forms['customerFileForm'].elements['customerFile.localHr'].value = name;
	window.opener.document.forms['customerFileForm'].elements['customerFile.localHrEmail'].value = mail;
	window.opener.document.forms['customerFileForm'].elements['customerFile.localHrPhone'].value = phone;
	}
	if(fileFor=='OI'){
	window.opener.document.forms['customerFileForm'].elements['customerFile.orderBy'].value = name;
	window.opener.document.forms['customerFileForm'].elements['customerFile.orderEmail'].value = mail;
	window.opener.document.forms['customerFileForm'].elements['customerFile.orderPhone'].value = phone;
	}
	if(fileFor=='ASMLOr'){
		window.opener.document.forms['customerFileForm'].elements['customerFile.orderBy'].value = name;
		window.opener.document.forms['customerFileForm'].elements['customerFile.orderEmail'].value = mail;
		window.opener.document.forms['customerFileForm'].elements['customerFile.orderPhone'].value = phone;
		}
	window.close();
}

function clear_fields(){
	document.getElementsByName('userName')[0].value='';
	document.getElementsByName('userEmailId')[0].value='';
}

</script>
<s:form name="searchUserContactCf" id="searchUserContactCf" action="searchUserContactCf" method="post">
<s:hidden name="decorator" value="popup"/>
<s:hidden name="popup" value="true"/>
<s:hidden name="codeForHr" value="${codeForHr}"/>
<s:hidden name="myFileFor" value="${myFileFor}"/>
<div id="Layer1" style="width:100%;">
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
	<div id="liquid-round-top">
	   <div class="top" style="margin-top:10px;!margin-top:0px;"><span></span></div>
	   <div class="center-content">		
		<table class="table">
		<thead>
		<tr>
		<th>Name</th>
		<th>Email ID</th>
		<th>&nbsp;</th>
		</tr>
		</thead>	
		<tbody>
			<tr>	
				<td width="" align="left"><s:textfield name="userName" size="30" required="true" cssClass="input-text" /></td>
				<td width="" align="left"><s:textfield name="userEmailId" size="30" required="true" cssClass="input-text" /></td>		
				<td>
	       		<s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;!margin-bottom:10px;" key="button.search"/>  
	       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/>       
	   			</td>
				 </tr>			 
			</tbody>
		</table>
		</div>
		<div class="bottom-header"><span></span></div>
	</div>
</div> 
<style>
span.pagelinks {
display:block;
font-size:0.85em;
margin-left:385px;
margin-bottom:22px;
!margin-bottom:2px;
margin-top:-38px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:60%;
!width:98%;
}
</style>
</head>

</div><div class="spn" style="width:100%">&nbsp;</div><br><br><br>
<s:set name="userContactList" value="userContactList" scope="request"/> 
<display:table name="userContactList" class="table" requestURI="" id="userContactList" defaultsort="2" pagesize="10" style="width:100%;margin-top:-10px;" >   
	  <display:column sortable="true" title="Full Name" style="width:65px"> 
	   <a href="javascript: void(0)" onclick= "getValue('${userContactList.full_name}','${userContactList.email}','${userContactList.contact}');"> 	 
	     <c:out value="${userContactList.full_name}" /></a>
	    </display:column>
	    <display:column sortable="true" title="Email Address" style="width:65px"> 
	    	 <c:out value="${userContactList.email}" /></a>
	    </display:column>
	    <display:column sortable="true" title="Phone&nbsp;Number" style="width:65px"> 
	    	  	 <c:out value="${userContactList.contact}" /></a>
	    </display:column>
</display:table> 
 
</div>
</s:form>

