<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>

<head>   
    <title><fmt:message key="containerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='containerList.heading'/>"/> 
 <style type="text/css">
span.pagelinks {display:block;font-size:0.85em;margin-bottom:5px;!margin-bottom:-1px;margin-top:-31px;!margin-top:-28px;padding:2px 0px;width:100%;}
.table thead th, .tableHeaderTable td {color:#343434;background:url("images/bg_listheader.png") repeat-x scroll 0 0 #bcd2ef;}
.table-fc thead th, .tableHeaderTable td {background:url("images/bgListHeader-forward-new.jpg") repeat-x scroll 0 0 #bcd2ef;height:26px;}
.table tfoot th, tfoot td {color:#434242; background:url("images/footer-fowrd-blueshade-new.jpg") repeat-x scroll 0 0 #bcd9eb; height:auto;}
@media screen and (-webkit-min-device-pixel-ratio:0) {
.table td, .table th, .tableHeaderTable td{ height:5px }
}} 
 div#content {padding:0px 0px; min-height:50px; margin-left:0px;} 
</style> 
<script>
 <sec-auth:authComponent componentId="module.script.form.agentScript">
	window.onload = function() { 
	 trap();
		<sec-auth:authScript tableList="serviceOrder" formNameList="serviceForm1" transIdList='${serviceOrder.shipNumber}'> 
		</sec-auth:authScript> 
		}
 </sec-auth:authComponent> 
<sec-auth:authComponent componentId="module.script.form.corpAccountScript"> 
	window.onload = function() {  
		//trap(); 
	}
</sec-auth:authComponent> 
 function trap() { 
		  if(document.images)   { 
		    	for(i=0;i<document.images.length;i++)
		      { 
		      	if(document.images[i].src.indexOf('nav')>0)
						{
							document.images[i].onclick= right; 
		        			document.images[i].src = 'images/navarrow.gif';  
						}    }    }   }
		  

</script>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="scripts/jquery.slimscroll.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('container', 'fade=1,persist=0,hide=0');
animatedcollapse.addDiv('carton', 'fade=1,persist=0,hide=0');
animatedcollapse.addDiv('vehicle', 'fade=1,persist=0,hide=0');
animatedcollapse.addDiv('routing', 'fade=1,persist=0,hide=0');
animatedcollapse.addDiv('custom', 'fade=1,persist=0,hide=0');
animatedcollapse.addDiv('consignee', 'fade=1,persist=0,hide=0');
animatedcollapse.addDiv('inLandAgent', 'fade=1,persist=0,hide=0');
animatedcollapse.addDiv('broker', 'fade=1,persist=0,hide=0');
animatedcollapse.init();
</script>

<script type="text/javascript">
function openadd2(){	
	animatedcollapse.toggle('routing')
	window.open('http://www.track-trace.com/','height=500,width=950,top=0, scrollbars=yes,resizable=yes').focus();	
}
</script>
</head>   
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="noteFor" id="noteFor" value="ServiceOrder"></s:hidden>

<s:form id="serviceForm1" name="serviceForm1" >
 <configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="checkPropertyAmountComponent" value="N" />
<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
	<c:set var="checkPropertyAmountComponent" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="checkMsgClickedValue" value="<%=request.getParameter("msgClicked") %>" />
<s:hidden name="msgClicked" id="msgClicked"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="sid" value="<%=request.getParameter("id")%>"/>
<div id="Layer1" style="width:100%; ">
<div id="newmnav" style="float: left;margin-bottom:0px;">
  <ul>
  <li>

	        <configByCorp:fieldVisibility componentId="component.Dynamic.DashBoard">
	          <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
          <c:if test="${(fn1:indexOf(dashBoardHideJobsList,serviceOrder.job)==-1)}">
	   <a href="redskyDashboard.html?sid=${serviceOrder.id}" /><span>Dashboard</span></a>
		</c:if>
	</sec-auth:authComponent>
	</configByCorp:fieldVisibility>

  <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
  	<c:if test="${checkPropertyAmountComponent!='Y'}">
	   <a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>S/O Details</span></a>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<a href="editServiceOrderUpdate.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}" /><span>S/O Details</span></a>
	</c:if>
  </sec-auth:authComponent>
  </li>
  <sec-auth:authComponent componentId="module.tab.container.billingTab">
  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
  	<c:if test="${checkPropertyAmountComponent!='Y'}">
	   	<li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<li><a href="editBilling.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}" /><span>Billing</span></a></li>
	</c:if>
  </sec-auth:authComponent>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.accountingTab">
  <c:choose>
	<%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
	   <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
	</c:when> --%>
	<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
	</c:when>
	<c:otherwise> 
		<c:if test="${checkPropertyAmountComponent!='Y'}">
		   	<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		</c:if>
		<c:if test="${checkPropertyAmountComponent=='Y'}">
			<li><a href="accountLineList.html?sid=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Accounting</span></a></li>
		</c:if>
	</c:otherwise>
  </c:choose> 
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
  <c:choose> 
	<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
	</c:when>
	<c:otherwise> 
		<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	</c:otherwise>
  </c:choose> 
  </sec-auth:authComponent>
 <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
    	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <c:if test="${checkPropertyAmountComponent!='Y'}">
			   	<li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
			</c:if>
			<c:if test="${checkPropertyAmountComponent=='Y'}">
				<li><a href="operationResource.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>O&I</span></a></li>
			</c:if>
	         </sec-auth:authComponent>
	</c:if>
  <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
       <li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.forwardingTab">
  <li id="newmnav1" style="background:#FFF "><a href="containersAjaxList.html?id=${serviceOrder.id}" class="current" ><span>Forwarding</span></a></li>
   </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.domesticTab">
  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
  	<c:if test="${checkPropertyAmountComponent!='Y'}">
	   	<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<li><a href="editMiscellaneous.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Domestic</span></a></li>
	</c:if>
  </c:if>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
    <c:if test="${serviceOrder.job =='INT'}">
      <c:if test="${checkPropertyAmountComponent!='Y'}">
	   	<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<li><a href="editMiscellaneous.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Domestic</span></a></li>
	</c:if>
    </c:if>
  </sec-auth:authComponent>  
  <sec-auth:authComponent componentId="module.tab.container.statusTab">
   <c:if test="${serviceOrder.job =='RLO'}"> 
	 <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
	</c:if>
	<c:if test="${serviceOrder.job !='RLO'}">
		<c:choose>
			<c:when test="${checkPropertyAmountComponent=='Y'}">
				<li><a href="editTrackingStatus.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Status</span></a></li>
			</c:when>
			<c:otherwise>
				<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:otherwise>
		</c:choose> 
			
	</c:if>
	 <sec-auth:authComponent componentId="module.tab.summary.summaryTab">
		<li><a href="findSummaryList.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
	</sec-auth:authComponent>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.ticketTab">
  	<c:if test="${checkPropertyAmountComponent!='Y'}">
	   	<li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<li><a href="customerWorkTickets.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}" ><span>Ticket</span></a></li>
	</c:if>
  </sec-auth:authComponent>
   <configByCorp:fieldVisibility componentId="component.standard.claimTab">
  <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
   <c:if test="${checkPropertyAmountComponent!='Y'}">
	   	<li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<li><a href="claims.html?id=${serviceOrder.id}&msgClicked=${checkMsgClickedValue}"><span>Claims</span></a></li>
	</c:if>
   </sec-auth:authComponent>
   </configByCorp:fieldVisibility>
 <%--   <sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.ticketTab">
	<c:if test="${serviceOrder.job =='RLO'}"> 
		 <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
	</c:if>
	</sec-auth:authComponent>	
	<sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.claimsTab">
	<c:if test="${serviceOrder.job =='RLO'}"> 	
		 <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
	</c:if> 
	</sec-auth:authComponent> --%>
	<c:set var="checkTab" value="Agent" />
    <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
    <c:set var="checkTab" value="NotAgent" />
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			 <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 <li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 </configByCorp:fieldVisibility>
			</sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
			      <c:if test="${ usertype=='AGENT' && surveyTab}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			</c:if>
			</sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.customerFileTab">
  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
  </sec-auth:authComponent>
    <sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
  		<li><a href="soAdditionalDateDetails.html?sid=${serviceOrder.id}"><span>Critical Dates</span></a></li>
	</sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.reportTab">
  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Container&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
  </sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.auditTab">			
			<li><a onclick="window.open('auditListForForwardingTab.html?id=${serviceOrder.id}&tableName=container,carton,vehicle,servicepartner,consigneeInstruction&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
	</sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
    <li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
	</sec-auth:authComponent>
	<c:if test="${usertype=='USER'}">
		<configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
	  		<li><a href="findEmailSetupTemplateByModuleNameSO.html?sid=${serviceOrder.id}"><span>View Emails</span></a></li>
	  	</configByCorp:fieldVisibility>
  	</c:if>
</div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;width: 60px; float: left;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<c:if test="${countShip != 1}" >
		<td width="20px" align="left" valign="top" style="vertical-align:top;!padding-top:1px;">		
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</td>
		</c:if>
		<c:if test="${countShip == 1}" >
		<td width="20px" align="left" style="vertical-align:top;!padding-top:1px;">
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</td>
  		</c:if>
  		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 0px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>		
		</c:if></tr></table>
<div class="spn" style="!margin-top:7px;">&nbsp;</div>
 <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
 
  <s:hidden name="shipNumber" id ="fileID" value="%{serviceOrder.shipNumber}" />
  <s:hidden name="regNumber" id ="fileID" value="%{serviceOrder.registrationNumber}" />
  <s:hidden name="packingMode" id ="fileID" value="%{serviceOrder.packingMode}" />
  <c:set var="shipNumber" value="${serviceOrder.shipNumber}" scope="session"/>
  <c:set var="packingMode" value="${serviceOrder.packingMode}" scope="session"/>
  <c:set var="ServiceOrderID" value="${serviceOrder.id}" scope="session"/>
  <c:set var="buttons">     
	        
</c:set> 
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="customerFile.id" />
<s:hidden name="serviceOrder.id" />
<s:hidden name="hitFlagVal"/>

<tr>
	<td height="10" width="100%" align="left" style="margin:0px">
<div onClick="javascript:animatedcollapse.toggle('container');" style="margin:0px">
     <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left"></td>
<td NOWRAP class="headtab_center">&nbsp;&nbsp;SS&nbsp;Container</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;</td>
<td class="headtab_right"></td>
</tr>
</table>
     </div>
		<div id="container" class="switchgroup1">
		<div id="containerDetailsAjax">		
		<jsp:include flush="true" page="containerAjaxList.jsp"></jsp:include>		
		</div>
	</div></td></tr>

<tr>						
		<td height="10" width="100%" align="left" style="margin: 0px">
 				<div onClick="javascript:animatedcollapse.toggle('carton');" style="margin: 0px">
	   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
	<tr>
	<td class="headtab_left"></td>
	<td NOWRAP class="headtab_center">&nbsp;&nbsp;Piece Count/LCL/Air</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;</td>
	<td class="headtab_right"></td>
	</tr>
	</table>
	  </div>
     		<div id="carton" class="switchgroup1">
     		<div id="cartonDetailsAjax">
     		<jsp:include flush="true" page="cartonAjaxList.jsp"></jsp:include>
     		</div>
			</div></td></tr>

<tr>						
		<td height="10" width="100%" align="left" style="margin: 0px">
 				<div onClick="javascript:animatedcollapse.toggle('vehicle');" style="margin: 0px">
	   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
	<tr>
	<td class="headtab_left"></td>
	<td NOWRAP class="headtab_center">&nbsp;&nbsp;Vehicle</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;</td>
	<td class="headtab_right"></td>
	</tr>
	</table>
	  </div>
     		<div id="vehicle" class="switchgroup1">
     		<div id="vehicleDetailsAjax">
     		<jsp:include flush="true" page="vehicleAjaxList.jsp"></jsp:include>
     		</div>			
			</div></td></tr>
	
<tr>						
		<td height="10" width="100%" align="left" style="margin: 0px">
 				<div onClick="javascript:animatedcollapse.toggle('routing');" style="margin: 0px">
	   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
	<tr>
	<td class="headtab_left"></td>
	<td NOWRAP class="headtab_center">&nbsp;&nbsp;<font style="line-height:19px;">Routing</font> &nbsp;
	<a><img onclick="openadd2();" style="vertical-align:middle;" src="${pageContext.request.contextPath}/images/track_trace.png" alt="Track & Trace" title="Track & Trace" /></a>
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;</td>
	<td class="headtab_right"></td>
	</tr>
	</table>
	  </div>
     		<div id="routing" class="switchgroup1">
			<div id="routingDetailsAjax">
			<jsp:include flush="true" page="servicePartnerAjaxList.jsp"></jsp:include>
			</div>
			</div></td></tr>
<c:if test="${countBondedGoods >= 0}" >
<tr>						
		<td height="10" width="100%" align="left" style="margin: 0px">
 				<div onClick="javascript:animatedcollapse.toggle('custom');" style="margin: 0px">
	   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
	<tr>
	<td class="headtab_left"></td>
	<td NOWRAP class="headtab_center">&nbsp;&nbsp;Customs</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;</td>
	<td class="headtab_right"></td>
	</tr>
	</table>
	  </div>
     		<div id="custom" class="switchgroup1">
			<div id="customDetailsAjax">
			<jsp:include flush="true" page="customAjaxList.jsp"></jsp:include>
			</div>
			</div></td></tr>
			</c:if>
		
		
<configByCorp:fieldVisibility componentId="component.section.forwarding.InLandAgent">
	<tr>						
		<td width="100%" align="left" style="margin: 0px">
 				<div onClick="javascript:animatedcollapse.toggle('inLandAgent');" style="margin: 0px">
	   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
	<tr>
	<td class="headtab_left"></td>
	<td NOWRAP class="headtab_center">&nbsp;&nbsp;Inland Agent</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;</td>
	<td class="headtab_right"></td>
	</tr>
	</table>
	  </div>
			<div id="inLandAgent" class="switchgroup1">
			<div id="inlandAgentAjax">
			<jsp:include flush="true" page="inLandAgentAjaxList.jsp"></jsp:include>
		</div>
		</div></td></tr>

</configByCorp:fieldVisibility>
<s:hidden name="typeFormPlace" id="typeFormPlace" value=""/>
</s:form>


<s:form id="brokerDetailsForm" name="brokerDetailsForm" action="saveBrokerDetailsAjax" method="post" validate="true">
<configByCorp:fieldVisibility componentId="component.section.forwarding.brokerDetails">
				<tr>
					<td height="10" width="100%" align="left" style="margin: 0px">
  						<div  onClick="javascript:animatedcollapse.toggle('broker')" style="margin: 0px">
      						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Broker Details
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
						</div>
						<div id="broker" class="switchgroup1">
							<jsp:include flush="true" page="brokerDetailsAjaxForm.jsp"></jsp:include>
						<div id="brokerExamListAjax">
						<jsp:include flush="true" page="brokerExamdetailsAjaxList.jsp"></jsp:include>
					</div>
					</div>
					</td>
				</tr>

</configByCorp:fieldVisibility>
</s:form>

	
	<s:form id="consigneeInstructionForm" action="saveConsigneeAjax" method="post" validate="true">
	<sec-auth:authComponent componentId="module.tab.container.consigneeInstructionsTab">
				<tr>
					<td height="10" width="100%" align="left" style="margin: 0px">
  				<div  onClick="javascript:animatedcollapse.toggle('consignee')" style="margin: 0px">
      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Consignee Instructions
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
						</div>
						<div id="consignee" class="switchgroup1">
			<jsp:include flush="true" page="consigneeInstructionAjaxForm.jsp"></jsp:include>
										</div>
										</td>
									</tr>
									 </sec-auth:authComponent>

</div>
     <c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
    <c:set var="idOfTasks" value="${serviceOrder.id}" scope="session"/>
     <c:set var="tableName" value="serviceorder" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if> 
<s:hidden name="serviceOrder.grpID"/>  
<s:hidden name="serviceOrder.grpStatus"/>
</s:form>
<script language="javascript" type="text/javascript">
function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
	}
</script> 

<script> 
		  function right(e) {  
		if (navigator.appName == 'Netscape' && e.which == 1) { 
		return false;
		} 
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) { 
		return false;
		} 
		else return true;
		}
function goToCustomerDetail(targetValue){
        document.forms['serviceForm1'].elements['id'].value = targetValue;
        document.forms['serviceForm1'].action = 'editContainer.html?from=list';
        document.forms['serviceForm1'].submit();
} 

  function showOrHide(value) {
	    if (value==0) {
	       	if (document.layers)
	           document.layers["overlay"].visibility='hide';
	        else
	           document.getElementById("overlay").style.visibility='hidden';
	   	}else if (value==1) {
	   		if (document.layers)
	          document.layers["overlay"].visibility='show';
	       	else
	          document.getElementById("overlay").style.visibility='visible';
	   	} } 
  showOrHide(0);
  
  var httpContainerDetails = getHTTPObject();
	function getContainerDetails(){
		showOrHide(1);
		var url="findContainerDetailsAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}";
		httpContainerDetails.open("POST", url, true);
		httpContainerDetails.onreadystatechange = function(){handleHttpResponseForContainerDetails()};
		httpContainerDetails.send(null);
	}
	function handleHttpResponseForContainerDetails(){
		if (httpContainerDetails.readyState == 4){
			showOrHide(0);
			refreshWeights1();
	        var results= httpContainerDetails.responseText; 
	 	    var containerDetailsDiv = document.getElementById("containerDetailsAjax");
	 	   		containerDetailsDiv.innerHTML = results;	 	  
	 	}
	}
  
  var httpCartonDetails = getHTTPObject();
	function getCartonDetails(){
		showOrHide(1);
		var url="findCartonDetailsAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}";
		httpCartonDetails.open("POST", url, true);
		httpCartonDetails.onreadystatechange = function(){handleHttpResponseForCartonDetails()};
		httpCartonDetails.send(null);
	}
	function handleHttpResponseForCartonDetails(){
		if (httpCartonDetails.readyState == 4){
			showOrHide(0);
			refreshWeights1();
	        var results= httpCartonDetails.responseText; 
	 	    var cartonDetailsDiv = document.getElementById("cartonDetailsAjax");
	 	   		cartonDetailsDiv.innerHTML = results;	 	  
	 	}
	}
  
  var httpVehicleDetails = getHTTPObject();
  function getVehicleDetails(){
		showOrHide(1);
		var url="findVehicleDetailsAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}";
		httpVehicleDetails.open("POST", url, true);
		httpVehicleDetails.onreadystatechange = function(){handleHttpResponseForVehicleDetails()};
		httpVehicleDetails.send(null);
	}
	function handleHttpResponseForVehicleDetails(){
		if (httpVehicleDetails.readyState == 4){
			showOrHide(0);
	        var results= httpVehicleDetails.responseText; 
	 	    var vehicleDetailsDiv = document.getElementById("vehicleDetailsAjax");
	 	   		vehicleDetailsDiv.innerHTML = results;	 	  
	 	}
	}
	
	var httpRoutingDetails = getHTTPObject();
	function getRoutingDetails(){
		showOrHide(1);
		var url="findRoutingDetailsAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}";
		httpRoutingDetails.open("POST", url, true);
		httpRoutingDetails.onreadystatechange = function(){handleHttpResponseForRoutingDetails()};
		httpRoutingDetails.send(null);
	}
	function handleHttpResponseForRoutingDetails(){
		if (httpRoutingDetails.readyState == 4){
			showOrHide(0);
	        var results= httpRoutingDetails.responseText; 
	 	    var routingDetailsDiv = document.getElementById("routingDetailsAjax");
	 	   		routingDetailsDiv.innerHTML = results;
	 	   	setOnSelectBasedMethods(["calcDays(),calcDays1()"]);
	 	   	setCalendarFunctionality();
	 	}
	}
	var httpCustomDetails = getHTTPObject();
	function getCustomDetails(){
		showOrHide(1);
		var url="findCustomDetailsAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}";
		httpCustomDetails.open("POST", url, true);
		httpCustomDetails.onreadystatechange = function(){handleHttpResponseForCustomDetails()};
		httpCustomDetails.send(null);
	}
	function handleHttpResponseForCustomDetails(){
		if (httpCustomDetails.readyState == 4){
			showOrHide(0);
	        var results= httpCustomDetails.responseText; 
	 	    var customDetailsDiv = document.getElementById("customDetailsAjax");
	 	   customDetailsDiv.innerHTML = results;
	 	}
	}
	
	
	var httpInlandAgentDetails = getHTTPObject();
	  function getInlandAgentDetails(){
			showOrHide(1);
			var url="findInlandAgentDetailsAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}";
			httpInlandAgentDetails.open("POST", url, true);
			httpInlandAgentDetails.onreadystatechange = function(){handleHttpResponseForInlandAgent()};
			httpInlandAgentDetails.send(null);
		}
		function handleHttpResponseForInlandAgent(){
			if (httpInlandAgentDetails.readyState == 4){
				showOrHide(0);
		        var InlandAgentDetails= httpInlandAgentDetails.responseText; 
		 	    var inlandAgentsDiv = document.getElementById("inlandAgentAjax");
		 	   inlandAgentsDiv.innerHTML = InlandAgentDetails;	
		 	  <configByCorp:fieldVisibility componentId="component.section.forwarding.InLandAgent">
		 	  	setOnSelectBasedMethods(["calcDays(),calcDays1(),updateDateDetailsInlandAgent()"]);
		 	 	setCalendarFunctionality();
		 	 </configByCorp:fieldVisibility>
		 	}
		}
		
		var httpBrokerExamListDetails = getHTTPObject();
		  function getBrokerExamListDetails(){
				showOrHide(1);
				var url="findBrokerExamListDetailsAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}";
				httpBrokerExamListDetails.open("POST", url, true);
				httpBrokerExamListDetails.onreadystatechange = function(){handleHttpResponseForBrokerExamList()};
				httpBrokerExamListDetails.send(null);
			}
			function handleHttpResponseForBrokerExamList(){
				if (httpBrokerExamListDetails.readyState == 4){
					showOrHide(0);
			        var BrokerExamListDetails= httpBrokerExamListDetails.responseText; 
			 	    var inlandAgentsDiv = document.getElementById("brokerExamListAjax");
			 	   inlandAgentsDiv.innerHTML = BrokerExamListDetails;	
			 	  <configByCorp:fieldVisibility componentId="component.section.forwarding.brokerDetails">
			 	  	setOnSelectBasedMethods(["calcDays(),calcDays1(),updateDateExamDetails()"]);
			 	 	setCalendarFunctionality();
			 	 </configByCorp:fieldVisibility>
			 	}
			}
	
</script>
<script type="text/javascript">   
	try{
	 refreshWeights1();
	 }
	 catch(e){}
</script> 
<script type="text/javascript">   
		try{
		<c:if test="${hitFlag == 1}" >
			<c:if test="${checkPropertyAmountComponent!='Y'}">
				<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}" ></c:redirect>
			</c:if>
			<c:if test="${checkPropertyAmountComponent=='Y'}">
				<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}&msgClicked=${msgClicked}" ></c:redirect>
			</c:if>
		</c:if>
		}
		catch(e){}
		
		try{
			var msgClicked='No';
			<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
			if('${serviceOrder.job}' =='INT'){
			var checkMsgClickedValue = "<%=request.getParameter("msgClicked") %>";
			var checkMsgClickedDBValue = "${msgClicked}";
			if((checkMsgClickedValue=='No' || checkMsgClickedValue==null || checkMsgClickedValue=='null' || checkMsgClickedValue=='') || (checkMsgClickedDBValue=='No' || checkMsgClickedDBValue==null || checkMsgClickedDBValue=='null' || checkMsgClickedDBValue=='')){
				var hasValuation = '${billing.insuranceHas}';
				if(hasValuation==null || hasValuation==''){
					var soCreatedOn = '${serviceOrder.createdOn}';
					var todayDate = new Date();
					if(soCreatedOn!=null && soCreatedOn!=''){
					   var mySplitResult = soCreatedOn.split("-");
					   var day = mySplitResult[2];
					   day = day.substring(0,2)
					   var month = mySplitResult[1];
					   var year = mySplitResult[0];
					   if(month == 'Jan'){month = "01";
					   }else if(month == 'Feb'){month = "02";
					   }else if(month == 'Mar'){month = "03";
					   }else if(month == 'Apr'){month = "04";
					   }else if(month == 'May'){month = "05";
					   }else if(month == 'Jun'){month = "06";
					   }else if(month == 'Jul'){month = "07";
					   }else if(month == 'Aug'){month = "08";
					   }else if(month == 'Sep'){month = "09";
					   }else if(month == 'Oct'){month = "10";
					   }else if(month == 'Nov'){month = "11";
					   }else if(month == 'Dec'){month = "12";
					   }
					   var finalDate = month+"-"+day+"-"+year;
					   date1 = finalDate.split("-");
					   var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
					   var daysApart = Math.round((todayDate-sDate)/86400000);
					   if(daysApart > 7){
				  			alert('Enter insurance requirements');
				  			msgClicked='Yes';
					   }
					}
				}
				var certificateNo = '${billing.certnum}';
				if((hasValuation=='Y') && (certificateNo==null || certificateNo=='')){
					var tsTargetPackDate = '${trackingStatus.beginPacking}';
					var tsTargetLoadDate = '${trackingStatus.beginLoad}';
					var soCreatedOn = '${serviceOrder.createdOn}';
					var soDate = null;
					var tsPackDate = null;
					var tsLoadDate = null;
					
					if(tsTargetPackDate!=null && tsTargetPackDate!=''){
						   var mySplitResult = tsTargetPackDate.split("-");
						   var day = mySplitResult[2];
						   day = day.substring(0,2)
						   var month = mySplitResult[1];
						   var year = mySplitResult[0];
						   if(month == 'Jan'){month = "01";
						   }else if(month == 'Feb'){month = "02";
						   }else if(month == 'Mar'){month = "03";
						   }else if(month == 'Apr'){month = "04";
						   }else if(month == 'May'){month = "05";
						   }else if(month == 'Jun'){month = "06";
						   }else if(month == 'Jul'){month = "07";
						   }else if(month == 'Aug'){month = "08";
						   }else if(month == 'Sep'){month = "09";
						   }else if(month == 'Oct'){month = "10";
						   }else if(month == 'Nov'){month = "11";
						   }else if(month == 'Dec'){month = "12";
						   }
						   var finalDate = month+"-"+day+"-"+year;
						   var date1 = finalDate.split("-");
						   tsPackDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
						   
					}else if(tsTargetLoadDate!=null && tsTargetLoadDate!=''){
						   var mySplitResult = tsTargetLoadDate.split("-");
						   var day = mySplitResult[2];
						   day = day.substring(0,2)
						   var month = mySplitResult[1];
						   var year = mySplitResult[0];
						   if(month == 'Jan'){month = "01";
						   }else if(month == 'Feb'){month = "02";
						   }else if(month == 'Mar'){month = "03";
						   }else if(month == 'Apr'){month = "04";
						   }else if(month == 'May'){month = "05";
						   }else if(month == 'Jun'){month = "06";
						   }else if(month == 'Jul'){month = "07";
						   }else if(month == 'Aug'){month = "08";
						   }else if(month == 'Sep'){month = "09";
						   }else if(month == 'Oct'){month = "10";
						   }else if(month == 'Nov'){month = "11";
						   }else if(month == 'Dec'){month = "12";
						   }
						   var finalDate = month+"-"+day+"-"+year;
						   var date1 = finalDate.split("-");
						   tsLoadDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
					}
					if(soCreatedOn!=null && soCreatedOn!=''){
						   var mySplitResult = soCreatedOn.split("-");
						   var day = mySplitResult[2];
						   day = day.substring(0,2)
						   var month = mySplitResult[1];
						   var year = mySplitResult[0];
						   if(month == 'Jan'){month = "01";
						   }else if(month == 'Feb'){month = "02";
						   }else if(month == 'Mar'){month = "03";
						   }else if(month == 'Apr'){month = "04";
						   }else if(month == 'May'){month = "05";
						   }else if(month == 'Jun'){month = "06";
						   }else if(month == 'Jul'){month = "07";
						   }else if(month == 'Aug'){month = "08";
						   }else if(month == 'Sep'){month = "09";
						   }else if(month == 'Oct'){month = "10";
						   }else if(month == 'Nov'){month = "11";
						   }else if(month == 'Dec'){month = "12";
						   }
						   var finalDate = month+"-"+day+"-"+year;
						   var date1 = finalDate.split("-");
						   soDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
					}
					if((tsPackDate!=null && tsPackDate!='') && (soDate!=null && soDate!='')){
						var daysApart = Math.round((tsPackDate-soDate)/86400000);
						if(daysApart < 7){
				  			alert('Need cert#');
				  			msgClicked='Yes';
					   	}
					}else if((tsLoadDate!=null && tsLoadDate!='') && (soDate!=null && soDate!='')){
						var daysApart = Math.round((tsLoadDate-soDate)/86400000);
						if(daysApart < 7){
				  			alert('Need cert#');
				  			msgClicked='Yes';
					   	}
					}
				}
				checkMsgClickedValue = msgClicked;
				document.forms['consigneeInstructionForm'].elements['msgClicked'].value = checkMsgClickedValue;
			}
		}
		</configByCorp:fieldVisibility>
		}catch(e){}
</script>