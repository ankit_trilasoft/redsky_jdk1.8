package com.trilasoft.app.service;

import java.util.List;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.RuleActions;



public interface RuleActionsManager extends GenericManager<RuleActions, Long>{
	
	
	public List getFilterName(String sessionCorpID ,Long userId);
	public List getActionList(Long ruleId) ;
	public <RuleActions> List getToDoRuleByRuleNumber(Long ruleId, String corpId);
	public void updateToDoRole(String messageDisplayed,String roleList, Long id, String corpId);
	public void updateStatus(String activestatus,Long ruleId,Long id);
}