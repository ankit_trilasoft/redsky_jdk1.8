package com.trilasoft.app.model;



import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.directwebremoting.annotations.Auth;


@Entity
@Table(name="commission")

public class Commission extends BaseObject{

	private Long id;
	private String shipNumber;
	private String corpId;
	private String salesPerson;
	private String commissionLine1Id;
	private BigDecimal  salesPersonAmount = new BigDecimal("0.00");
	private BigDecimal  salesPersonPercentage = new BigDecimal("0.00");
	private Date settledDate;
	private String consultant;
	private String commissionLine2Id;
	private BigDecimal  consultantAmount = new BigDecimal("0.00");
	private BigDecimal  consultantPercentage = new BigDecimal("0.00");
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String commissionType;
	private Long aid;
	private BigDecimal  actualRevenue = new BigDecimal("0.00");
	private BigDecimal  actualExpense = new BigDecimal("0.00");
	private String chargeCode;
	private String companyDivision;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("shipNumber", shipNumber).append("corpId", corpId)
				.append("salesPerson", salesPerson)
				.append("commissionLine1Id", commissionLine1Id)
				.append("salesPersonAmount", salesPersonAmount)
				.append("salesPersonPercentage", salesPersonPercentage)
				.append("settledDate", settledDate)
				.append("consultant", consultant)
				.append("commissionLine2Id", commissionLine2Id)
				.append("consultantAmount", consultantAmount)
				.append("consultantPercentage", consultantPercentage)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("commissionType", commissionType).append("aid", aid)
				.append("actualRevenue", actualRevenue)
				.append("actualExpense", actualExpense)
				.append("chargeCode", chargeCode)
				.append("companyDivision", companyDivision).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Commission))
			return false;
		Commission castOther = (Commission) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(shipNumber, castOther.shipNumber)
				.append(corpId, castOther.corpId)
				.append(salesPerson, castOther.salesPerson)
				.append(commissionLine1Id, castOther.commissionLine1Id)
				.append(salesPersonAmount, castOther.salesPersonAmount)
				.append(salesPersonPercentage, castOther.salesPersonPercentage)
				.append(settledDate, castOther.settledDate)
				.append(consultant, castOther.consultant)
				.append(commissionLine2Id, castOther.commissionLine2Id)
				.append(consultantAmount, castOther.consultantAmount)
				.append(consultantPercentage, castOther.consultantPercentage)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(commissionType, castOther.commissionType)
				.append(aid, castOther.aid)
				.append(actualRevenue, castOther.actualRevenue)
				.append(actualExpense, castOther.actualExpense)
				.append(chargeCode, castOther.chargeCode)
				.append(companyDivision, castOther.companyDivision).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(shipNumber)
				.append(corpId).append(salesPerson).append(commissionLine1Id)
				.append(salesPersonAmount).append(salesPersonPercentage)
				.append(settledDate).append(consultant)
				.append(commissionLine2Id).append(consultantAmount)
				.append(consultantPercentage).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(commissionType).append(aid).append(actualRevenue)
				.append(actualExpense).append(chargeCode)
				.append(companyDivision).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	
	@Column
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	
	@Column
	public String getSalesPerson() {
		return salesPerson;
	}
	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}
	
	@Column
	public String getCommissionLine1Id() {
		return commissionLine1Id;
	}
	public void setCommissionLine1Id(String commissionLine1Id) {
		this.commissionLine1Id = commissionLine1Id;
	}
	
	@Column
	public BigDecimal getSalesPersonAmount() {
		return salesPersonAmount;
	}
	public void setSalesPersonAmount(BigDecimal salesPersonAmount) {
		this.salesPersonAmount = salesPersonAmount;
	}
	
	@Column
	public BigDecimal getSalesPersonPercentage() {
		return salesPersonPercentage;
	}
	public void setSalesPersonPercentage(BigDecimal salesPersonPercentage) {
		this.salesPersonPercentage = salesPersonPercentage;
	}
	
	@Column
	public Date getSettledDate() {
		return settledDate;
	}
	public void setSettledDate(Date settledDate) {
		this.settledDate = settledDate;
	}
	
	@Column
	public String getConsultant() {
		return consultant;
	}
	public void setConsultant(String consultant) {
		this.consultant = consultant;
	}
	
	@Column
	public String getCommissionLine2Id() {
		return commissionLine2Id;
	}
	public void setCommissionLine2Id(String commissionLine2Id) {
		this.commissionLine2Id = commissionLine2Id;
	}
	
	@Column
	public BigDecimal getConsultantAmount() {
		return consultantAmount;
	}
	public void setConsultantAmount(BigDecimal consultantAmount) {
		this.consultantAmount = consultantAmount;
	}
	
	@Column
	public BigDecimal getConsultantPercentage() {
		return consultantPercentage;
	}
	public void setConsultantPercentage(BigDecimal consultantPercentage) {
		this.consultantPercentage = consultantPercentage;
	}
	
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Column
	public String getCommissionType() {
		return commissionType;
	}
	public void setCommissionType(String commissionType) {
		this.commissionType = commissionType;
	}
	
	@Column
	public Long getAid() {
		return aid;
	}
	public void setAid(Long aid) {
		this.aid = aid;
	}
	
	@Column
	public BigDecimal getActualRevenue() {
		return actualRevenue;
	}
	public void setActualRevenue(BigDecimal actualRevenue) {
		this.actualRevenue = actualRevenue;
	}
	
	@Column
	public BigDecimal getActualExpense() {
		return actualExpense;
	}
	public void setActualExpense(BigDecimal actualExpense) {
		this.actualExpense = actualExpense;
	}
	
	@Column
	public String getChargeCode() {
		return chargeCode;
	}
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}
	
	@Column
	public String getCompanyDivision() {
		return companyDivision;
	}
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
  
}