package com.trilasoft.app.service;

import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.OperationsResourceLimits;

public interface OperationsResourceLimitsManager  extends GenericManager<OperationsResourceLimits, Long> {
	
	public List isExisted(Date workDate, String hub, String sessionCorpID);

	public List getResourceLimitsByHub(String hub, String category);

	public List getItemListByCategory(String category, String resource, String sessionCorpID);
	
	public List checkResource(String resource, String sessionCorpID);

}
