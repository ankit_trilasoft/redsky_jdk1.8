<%@ include file="/common/taglibs.jsp"%>
<table class="" cellspacing="0" cellpadding="0" style="margin:0px;width:100%;">
<tr>
<td>
   <table class="HMtable" cellspacing="1" style="margin:0px;">
        <tr>
            <td class="head">Type</td>
            <td class="head">Rate(USD)</td>
            <td class="head">Rate Basis</td>
        </tr>
    </table>
      <div class="wrap">
    <div class="inner_table">
        <table class="HMtable" style="margin:0px;border:2px solid #74B3DC;border-top:none;">
         <c:if test="${supplementQuotesList!='[]'}"> 
			<c:forEach var="individualItem" items="${supplementQuotesList}" varStatus="rowCounter">
			<c:if test="${rowCounter.count % 2 == 0}">
			 	<tr class="even">
			</c:if>
			<c:if test="${rowCounter.count % 2 != 0}">
				<tr class="odd">
			</c:if>
			 		<td>					
						<c:out value="${individualItem.type}"/>
					</td>
			 		<td><c:out value="${individualItem.rate}"/></td>
			 		<td><c:out value="${individualItem.basis}"/></td>
				</tr>
			</c:forEach>
		 </c:if>
	     <c:if test="${supplementQuotesList=='[]'}"> 
	     <div class="EmptyHM">Nothing found to display.</div>
	     </c:if>
    </table>
    </div>
</div>
</td>
</tr>
</table>
