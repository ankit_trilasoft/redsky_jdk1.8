<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
 <head>   
    <title>Lead File</title>   
    <meta name="heading" content="Lead File"/> 
      
   <script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" /> 
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}

div.error, span.error, li.error, div.message {
width:450px;
margin-top:0px; 
}
form {
margin-top:-15px;
!margin-top:-10px;
}

div#main {
margin:-5px 0 0;
}

 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>


</head>   
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px;"  
        onclick="location.href='<c:url value="/editLeadCapture.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:52px; height:25px;" key="button.search" />   
    <input type="button" class="cssbutton1" value="Clear" style="width:50px; height:25px;" onclick="clear_fields();"/> 
</c:set>   
   
<s:form id="searchForm" action="leadCaptureSearch" method="post" >  
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content" style="padding-left:15px;">
<table class="table" style="width:100%"  >
<thead>
<tr>
<th>Lead File#</th>
<th><fmt:message key="customerFile.lastName"/></th>
<th><fmt:message key="customerFile.firstName"/></th>
<th>Created On</th>
<th><fmt:message key="customerFile.job"/></th>
<th>Sales Person</th>
<th>Lead Status</th>
<th>Move By</th>

</tr></thead>	
		<tbody>
		<tr>
			<td width="" align="left">
			    <s:textfield name="customerFile.sequenceNumber" required="true" cssClass="input-text" cssStyle="width:90px" size="15" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td width="" align="left">
			    <s:textfield name="customerFile.lastName" required="true" cssClass="input-text" size="16" />
			</td>
			<td width="" align="left">
			    <s:textfield name="customerFile.firstName" required="true" cssClass="input-text" size="10" />
			</td>
			
			<c:if test="${not empty customerFile.createdOn}">
			<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.createdOn"/></s:text>
			<td><s:textfield cssClass="input-text" id="createdOn" name="customerFile.createdOn" value="%{customerFileMoveDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="();" onkeydown="return onlyDel(event,this)"/>
			<img id="createdOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty customerFile.createdOn}">
			<td><s:textfield cssClass="input-text" id="createdOn" name="customerFile.createdOn" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="();" onkeydown="return onlyDel(event,this)" />
			<img id="createdOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
					
			<td width="" align="left">
			   <s:select name="customerFile.job" list="%{jobs}" cssClass="list-menu" cssStyle="width:150px;" headerKey="" headerValue=""/>
			</td>
			<td width="" align="left">
			   <s:select name="customerFile.salesMan" list="%{sale}" cssClass="list-menu" cssStyle="width:100px;" headerKey="" headerValue=""/>
			</td>
				<td width="" align="left">
			  <s:select name="customerFile.leadStatus" list="%{leadCaptuteStatus}" cssClass="list-menu" cssStyle="width:90px" headerKey="" headerValue=""/>			
			</td>
			<c:if test="${not empty customerFile.moveDate}">
			<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.moveDate"/></s:text>
			<td><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" value="%{customerFileMoveDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="();" onkeydown="return onlyDel(event,this)"/>
			<img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty customerFile.moveDate}">
			<td><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="();" onkeydown="return onlyDel(event,this)" />
			<img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			</tr>
			<tr><td style="text-align:left;border-right:none;margin:0px;padding:0px;" class="listwhitetext" colspan="8">
			<table style="margin:0px;padding:0px;border:none; float:right;" class="listwhitetext">
				<tr>			
			<td style="border:none;" class="listwhitetext">		
			Search Options<br>
			<s:select name="serviceOrderSearchVal" list="%{serviceOrderSearchType}" cssClass="list-menu" cssStyle="margin-top:2px;"/>
			</td>
			<td style="border:none;vertical-align:bottom;">						
			    <c:out value="${searchbuttons}" escapeXml="false" />
			</td>
			</tr>
			</table>  
			       
			</td>
			
		</tr>

		</tbody>
	</table>
</div>
<div class="bottom-header" style="margin-top:31px;!margin-top:49px;"><span></span></div>
</div>
</div> 

<c:out value="${searchresults}" escapeXml="false" />  
		<div id="otabs" style="margin-top: -10px;">
		  <ul>
		    <li><a class="current"><span>Lead File List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<s:set name="leadCaptureList" value="leadCaptureList" scope="request"/> 

<display:table name="leadCaptureList" class="table" requestURI="" id="leadCaptureList" export=""  pagesize="15" style="width:100%;" > 
  
    <display:column property="sequenceNumber" sortable="true" title="Lead File#" url="/editLeadCapture.html" paramId="id" paramProperty="id" />     
    <display:column property="lastName" sortable="true" titleKey="customerFile.lastName" style="width:90px" />
    <display:column property="firstName" sortable="true" titleKey="customerFile.firstName" />
     <display:column property="job" sortable="true" titleKey="customerFile.job"/>
     <display:column property="salesMan" sortable="true" titleKey="customerFile.salesMan"/>
    <display:column property="statusDate" sortable="true" titleKey="customerFile.statusDate" style="width:100px"  format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="status" sortable="true" title="Lead Status"/>    
    <display:column property="createdOn" sortable="true" titleKey="customerFile.createdOn" style="width:100px" format="{0,date,dd-MMM-yyyy}"/>      
   
</display:table>
  
<c:out value="${buttons}" escapeXml="false" /> 

</s:form>

<script language="javascript" type="text/javascript">
function clear_fields(){
		document.forms['searchForm'].elements['customerFile.job'].value  = "";
		document.forms['searchForm'].elements['customerFile.sequenceNumber'].value  = ""; 
		document.forms['searchForm'].elements['customerFile.lastName'].value  = "";
		document.forms['searchForm'].elements['customerFile.firstName'].value  = "";
		document.forms['searchForm'].elements['customerFile.createdOn'].value = "";
		document.forms['searchForm'].elements['customerFile.salesMan'].value = "";
		document.forms['searchForm'].elements['customerFile.leadStatus'].value = "";
		document.forms['searchForm'].elements['customerFile.moveDate'].value = "";
}
</script>
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();	
</script>
