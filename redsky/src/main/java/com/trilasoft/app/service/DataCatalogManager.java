package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.DataCatalog;

public interface DataCatalogManager extends GenericManager<DataCatalog, Long>{
	
	public List findById(Long id);
	public List findMaximumId();
	public List configurableTable(boolean b);
    public List configurableField(String tableName,boolean b);
    public List getList();
	public List<DataCatalog> searchdataCatalog(String tableName, String fieldName, String auditable, String description, String usDomestic, String defineByToDoRule, String visible, String charge);
	
	public List getCorpAuthoritiesForComponent(String userCorpID, String componentId);
	public boolean getFieldVisibilityAttrbute(String userCorpID, String componentId);
	public List checkData(String tableName, String fieldName, String sessionCorpID);
	public void mergeDataCatalog(DataCatalog dc);
}
