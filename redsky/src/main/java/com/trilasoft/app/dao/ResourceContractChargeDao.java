package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ResourceContractCharges;

public interface ResourceContractChargeDao extends GenericDao<ResourceContractCharges, Long> {
	
	public List findResourceByPartnerId(String corpId,Long partnerId);
	public void deleteAll(Long parentId);
	
	public List createPrice(String corpId, String contract,String shipNumber,Long serviceOrderId,String divisionFlag);

}
