<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title><fmt:message key="roleBasedPermissionList.title"/></title> 
<meta name="heading" content="<fmt:message key='roleBasedPermissionList.heading'/>"/>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:2px;
margin-top:-17px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
}</style>

<script language="javascript" type="text/javascript">

function clear_fields(){
	var i;
			for(i=0;i<=1;i++)
			{
					document.forms['roleBasedPermissionList'].elements[i].value = "";
			}
}
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Role Permission");
	var did = targetElement;
		if (agree){
			location.href="deleteRolePermission.html?id="+did;
		}else{
			return false;
		}
}
</script>



</head>



<c:set var="buttons">  

     <input type="button" class="cssbutton1" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/roleBasedPermissionForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	
</c:set>

	<s:form id="roleBasedPermissionList" name="roleBasedPermissionList" action="searchRoleBasedPermissionList" method="post" validate="true">   
	<div id="Layer1" style="width:100%;">
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px; !margin-top:-4px;"><span></span></div>
   <div class="center-content">		
	<table class="table">
	<thead>
	<tr>
	<th><fmt:message key="companyPermission.componentId"/></th>
	<th><fmt:message key="companyPermission.description"/></th>
	<th>Role</th>
	</tr></thead>
	
	<tbody>
		<tr>
		    <td width="20" align="left"><s:textfield name="roleBasedComponentPermission.componentId" required="true" cssClass="input-text" size="66"/></td>
			<td width="20" align="left"><s:textfield name="roleBasedComponentPermission.description" required="true" cssClass="input-text" size="66"/></td>
		  	 <td align="left"><s:select cssClass="list-menu"   name="roleBasedComponentPermission.role" list="%{role}" headerKey="" headerValue="" cssStyle="width:350px" /></td>			 
			 <tr>
			 <td></td>
			  <td></td>
			 <td style="border-left: hidden;">
       		<s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;!margin-bottom:10px;" key="button.search"/>  
       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/> 
       
   			</td>
		</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<c:out value="${searchresults}" escapeXml="false" /> 
	
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Role Permission List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	
	<display:table name="roleBasedPermissionList" class="table" requestURI="" id="roleBasedPermissionList" export="true" defaultsort="1" pagesize="10" style="width:100%" >   
	<display:column property="componentId"  href="roleBasedPermissionForm.html" paramId="id" paramProperty="id" sortable="true" titleKey="companyPermission.componentId"/>
   	<display:column property="description"  sortable="true" titleKey="companyPermission.description"/>
   	<display:column property="role"  sortable="true" title="Role"/>
   	<display:column property="corpID"  sortable="true" title="CorpID"/>
   	<display:column headerClass="containeralign" property="mask"  sortable="true" title="Mask"/>
   	 	<%--<display:column title="Remove" style="width:50px">
			<a><img title="Remove" align="middle" onclick="confirmSubmit(${roleBasedPermissionList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
	</display:column>  	--%>
 </display:table>
	
	<c:out value="${buttons}" escapeXml="false" />   
	<c:set var="isTrue" value="false" scope="session"/>
	</div>
</s:form> 

<script type="text/javascript"> 

highlightTableRows("roleBasedPermissionList"); 

</script> 
		
		