<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="NotesReport"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="2065"
		 pageHeight="842"
		 columnWidth="2005"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Begin_Date" isForPrompting="true" class="java.util.Date"/>
	<parameter name="End_Date" isForPrompting="true" class="java.util.Date"/>
	<queryString><![CDATA[SELECT DISTINCT "NotesReport-R-EXTRACT-BOUR.jrxml",
	n.createdBy AS 'Created by',
	DATE_FORMAT(n.createdOn,'%d/%m/%Y') AS 'Created on',
	n.updatedBy AS 'Modified by',
	DATE_FORMAT(n.updatedOn,'%d/%m/%Y') AS 'Modified on',
	n.noteType AS 'Type',
	n.noteSubType AS 'Sub-Type',
	n.noteStatus AS 'Status',
	n.subject AS 'Subject',
	LEFT(replace(replace(replace(replace(replace(n.note,'<p>',' '),'</p>',' '),'/n',' '),'\r\n',' '),'\n',' '),250) AS 'Details',
	n.notesId AS 'S/O number',
	c.firstName AS 'Shipper`s first name',
	c.lastName AS 'Shipper`s surname',
	DATE_FORMAT(n.forwardDate,'%d/%m/%Y') AS 'Follow up date',
	n.followUpFor AS 'Follow up for',
	if(n.notesId=c.sequenceNumber,c.job,s.job) AS 'Job Type',
	if(n.notesId=c.sequenceNumber,c.coordinator,s.coordinator) AS 'Coordinator',
	if(n.notesId=c.sequenceNumber,c.personPricing,b.personPricing) AS 'Pricing Person',
	DATE_FORMAT(c.moveDate,'%d/%m/%Y') AS 'Move By Date',
	c.billToName AS 'Bill to Name',
	DATE_FORMAT(c.bookingDate,'%d/%m/%Y') AS 'Booking date',
	c.originCity AS 'Origin City',
	c.originCountry AS 'Origin Country',
	c.destinationCity AS 'Destination City',
	c.destinationCountry AS 'Destination Country',
	if(d.RNT_leaseStartDate is null || d.RNT_leaseStartDate='','',DATE_FORMAT(d.RNT_leaseStartDate,'%d/%m/%Y')) AS 'Lease start date'
FROM notes n
	LEFT JOIN customerfile c ON n.customernumber = c.sequenceNumber  AND n.corpId = c.corpId
	LEFT JOIN serviceorder s ON c.id = s.customerFileId AND c.corpid = s.corpid AND s.shipNumber=n.notesId AND n.corpId = s.corpId
	LEFT JOIN billing b ON s.id = b.id AND s.corpid = b.corpid
	LEFT JOIN dspdetails d ON d.id=s.id AND d.corpid = s.corpid
WHERE n.corpId=$P{Corporate ID}
	AND (n.createdOn between $P{Begin_Date} AND $P{End_Date})
ORDER BY n.notesId]]></queryString>

	<field name="NotesReport-R-EXTRACT-BOUR.jrxml" class="java.lang.String"/>
	<field name="Created by" class="java.lang.String"/>
	<field name="Created on" class="java.lang.String"/>
	<field name="Modified by" class="java.lang.String"/>
	<field name="Modified on" class="java.lang.String"/>
	<field name="Type" class="java.lang.String"/>
	<field name="Sub-Type" class="java.lang.String"/>
	<field name="Status" class="java.lang.String"/>
	<field name="Subject" class="java.lang.String"/>
	<field name="Details" class="java.lang.String"/>
	<field name="S/O number" class="java.lang.String"/>
	<field name="Shipper`s first name" class="java.lang.String"/>
	<field name="Shipper`s surname" class="java.lang.String"/>
	<field name="Follow up date" class="java.lang.String"/>
	<field name="Follow up for" class="java.lang.String"/>
	<field name="Job Type" class="java.lang.String"/>
	<field name="Coordinator" class="java.lang.String"/>
	<field name="Pricing Person" class="java.lang.String"/>
	<field name="Move By Date" class="java.lang.String"/>
	<field name="Bill to Name" class="java.lang.String"/>
	<field name="Booking date" class="java.lang.String"/>
	<field name="Origin City" class="java.lang.String"/>
	<field name="Origin Country" class="java.lang.String"/>
	<field name="Destination City" class="java.lang.String"/>
	<field name="Destination Country" class="java.lang.String"/>
	<field name="Lease start date" class="java.lang.String"/>

		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="61"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="34"
						width="73"
						height="27"
						key="staticText-2"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Created by]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="73"
						y="34"
						width="60"
						height="27"
						key="staticText-3"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Created on]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="133"
						y="34"
						width="75"
						height="27"
						key="staticText-4"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Modified by]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="208"
						y="34"
						width="65"
						height="27"
						key="staticText-5"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Modified on]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="273"
						y="34"
						width="72"
						height="27"
						key="staticText-6"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Type]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="345"
						y="34"
						width="71"
						height="27"
						key="staticText-7"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Sub-Type]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="416"
						y="34"
						width="55"
						height="27"
						key="staticText-8"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Status]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="471"
						y="34"
						width="110"
						height="27"
						key="staticText-9"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Subject]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="1"
						width="2005"
						height="24"
						key="staticText-11"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="13" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Notes Report]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="581"
						y="34"
						width="100"
						height="27"
						key="staticText-12"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Details]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="681"
						y="34"
						width="84"
						height="27"
						key="staticText-13"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[S/O number]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="765"
						y="34"
						width="85"
						height="27"
						key="staticText-14"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Shipper`s
First Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="850"
						y="34"
						width="104"
						height="27"
						key="staticText-15"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Shipper`s
Surname]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="935"
						y="34"
						width="60"
						height="27"
						key="staticText-16"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Follow up
date]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="995"
						y="34"
						width="90"
						height="27"
						key="staticText-17"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Follow up for]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="1085"
						y="34"
						width="45"
						height="27"
						key="staticText-18"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Job Type]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="1130"
						y="34"
						width="98"
						height="27"
						key="staticText-19"/>
					<box leftPadding="2"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Coordinator]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="1228"
						y="34"
						width="86"
						height="27"
						key="staticText-20"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Pricing Person]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="1384"
						y="34"
						width="86"
						height="27"
						key="staticText-21"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Bill to Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="1314"
						y="34"
						width="70"
						height="27"
						key="staticText-22"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Move By Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="1470"
						y="34"
						width="86"
						height="27"
						key="staticText-23"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Booking date]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="1556"
						y="34"
						width="86"
						height="27"
						key="staticText-24"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Origin City]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="1642"
						y="34"
						width="86"
						height="27"
						key="staticText-25"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Origin Country]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="1728"
						y="34"
						width="86"
						height="27"
						key="staticText-26"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Destination City]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="1814"
						y="34"
						width="103"
						height="27"
						key="staticText-27"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Destination Country]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="1917"
						y="34"
						width="88"
						height="27"
						key="staticText-28"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Lease start date]]></text>
				</staticText>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="19"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="1"
						width="73"
						height="18"
						key="textField"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Created by}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="73"
						y="1"
						width="60"
						height="18"
						key="textField"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Created on}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="133"
						y="1"
						width="75"
						height="18"
						key="textField"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Modified by}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="208"
						y="1"
						width="65"
						height="18"
						key="textField"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Modified on}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="273"
						y="1"
						width="72"
						height="18"
						key="textField"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Type}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="345"
						y="1"
						width="71"
						height="18"
						key="textField"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Sub-Type}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="416"
						y="1"
						width="55"
						height="18"
						key="textField"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Status}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="471"
						y="1"
						width="110"
						height="18"
						key="textField"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Subject}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="581"
						y="1"
						width="100"
						height="18"
						key="textField-4"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Details}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="681"
						y="1"
						width="84"
						height="18"
						key="textField-5"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{S/O number}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="765"
						y="1"
						width="85"
						height="18"
						key="textField-6"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Shipper`s first name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="850"
						y="1"
						width="85"
						height="18"
						key="textField-7"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Shipper`s surname}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="935"
						y="1"
						width="60"
						height="18"
						key="textField-8"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Follow up date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="995"
						y="1"
						width="90"
						height="18"
						key="textField-9"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Follow up for}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1085"
						y="1"
						width="45"
						height="18"
						key="textField-10"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Job Type}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1130"
						y="1"
						width="98"
						height="18"
						key="textField-11"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Coordinator}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1228"
						y="1"
						width="86"
						height="18"
						key="textField-12"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Pricing Person}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1384"
						y="1"
						width="86"
						height="18"
						key="textField-13"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Bill to Name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1314"
						y="1"
						width="70"
						height="18"
						key="textField-14"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Move By Date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1470"
						y="1"
						width="86"
						height="18"
						key="textField-15"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Booking date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1556"
						y="1"
						width="86"
						height="18"
						key="textField-16"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Origin City}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1642"
						y="1"
						width="86"
						height="18"
						key="textField-17"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Origin Country}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1728"
						y="1"
						width="86"
						height="18"
						key="textField-18"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Destination City}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1814"
						y="1"
						width="103"
						height="18"
						key="textField-19"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Destination Country}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1917"
						y="1"
						width="88"
						height="18"
						key="textField-20"
						stretchType="RelativeToTallestObject"/>
					<box></box>
					<textElement>
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Lease start date}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="17"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1810"
						y="2"
						width="130"
						height="15"
						key="textField-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1940"
						y="2"
						width="65"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="CP1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="2"
						width="142"
						height="15"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="859"
						y="2"
						width="287"
						height="15"
						key="staticText-10"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" size="8"/>
					</textElement>
				<text><![CDATA[NotesReport.jrxml]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="1"
						width="2005"
						height="0"
						key="line-1"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
