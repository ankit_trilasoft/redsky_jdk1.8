<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix = "c"  uri = "http://java.sun.com/jsp/jstl/core" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Custom&nbsp;Information</title>  
<script type="text/javascript">
function poptastic(url)
{
    if(url.length>0)
	{
	newwindow=window.open(url,'name','height=400,width=200,scrollbars=yes,resizable=yes');
	if (window.focus) {newwindow.focus()}
	}
	else
	{    
		//window.open('openPopList.html','name','height=400,width=600,scrollbars=yes');
	}
}

function loadMainWindow(url,add1) 
{  
   var win = window.self; 
   win.opener = window.self; 
   if(add1 !=""){
   var newloc = "omniPopUp.html?countryName="+url; 
   
   
   var ih = (window.window.screen.availHeight - 60) - 100; 
   var iw = (window.window.screen.availWidth - 10) - 100; 
   var it = ((window.window.screen.availHeight - 60) - ih) / 2; 
   var il = ((window.window.screen.availWidth - 10) - iw) / 2; 
   
   var features = 
"directories=no,menubar=no,location=no,url=no,toolbar=no,status=no,scrollbars=yes,resizable=yes" 
+ ",width=" + iw + ",height=" + ih + ",top=" + it + ",left=" + il + ""; 
   v1 = "custom"; 
   v2 = "pg" + v1; 
   
   var oNewWindow = window.open(newloc, v2, features); 
  

   oNewWindow.focus(); 

}
   else{
   alert("No details available for "+url);
   }
   

} 

function popUpDocuument() 
{ 
   try 
   { 
      loadMainWindow("omniPopUp.html?countryName="+url); 
   } 
   catch(e) 
   { 
      popUpMessage.style.display = 'block'; 
   } 
}


</script>
<style type="text/css">
.clear { clear:both;}
body { margin:0px; padding:0px;}

</style>
</head>
<body id="dashboard" style="background-image:url(images/bg.gif);">

<c:set var="noHeader" value="false" scope="session"/>
	
			<script type="text/javascript">function setInitTabCol() {
			        if (document.getElementById('idGroupTab') != null) {
			            document.getElementById('idGroupTab').className = "group";
			        }
			        if (document.getElementById('idIndividualTab') != null) {
			            document.getElementById('idIndividualTab').className = "current";
			        }
			    }
			</script>
			<form name="dashboardActionForm" method="post" action="">
				<input name="reportActionEvent" value="" type="hidden">
				<input name="dashboardActionEvent" value="" type="hidden">
				<input name="clickedCtn" value="" type="hidden">
		
			</form>
			<div class="clear"></div>
		
			
			
		<script type="text/javascript">setInitTabCol();
		</script>
		<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>Custom&nbsp;Information</b></td>
	<td align="right">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
	<tr valign="top"><td colspan="2">		
<s:set name="findAddressList" value="findAddressList" id="findAddressList"  scope="request"/>
<s:form id="searchForm"  action="customInfo" method="post" validate="true" >
<display:table name="findAddressList" class="table" requestURI="" id="findAddressList" style="width:100%;border-collapse:collapse;" defaultsort="1" pagesize="100" >
      <display:column style="width:10%">
     To view customs Information for <b>${findAddressList.countryName}</b> 
     <c:if test="${findAddressList.address1 !=''}">
     <a href="#" onclick="window.open('${findAddressList.address1}')"><I>Click Here</I></a>
     </c:if>
     <c:if test="${findAddressList.address1 ==''}">
     <a href="#" onclick="viewcustom(this)"><I>Click Here</I></a>
          </c:if>
    </display:column>
</display:table>

</s:form>
</td></tr></table>
</body> 
		  	