package com.trilasoft.app.service;


import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.UserGuides;

public interface UserGuidesManager extends GenericManager<UserGuides, Long>{
	
	 public List findListByModuleDocStatus(String module,String documentName,Boolean activeStatus);
	 public List findListByModule(String module,String corpId);
	 public List findDistinct();
	 public List<UserGuides> findListByModuleAndDisplayOrder(String module,int displayOrder,Integer prevOrder);
	 public List<String> findListByDistinctModule(String corpId);
	 public List<UserGuides> findListByDocumentName(String fileName,String UserType,Boolean cmmdmmAgent,Boolean vanlineEnabled,String corpId);
	 public List findModuleCount(String corpId);

}
