<%@ include file="/common/taglibs.jsp"%>
<title><fmt:message key="activeWorkTicketList.title" /></title>
<meta name="heading" content="<fmt:message key='activeWorkTicketList.heading'/>" />

<style type="text/css">
form{margin-top:-20px;}
span.pagelinks {display:block;font-size:0.95em;margin-bottom:2px;margin-top:-18px;padding:2px 0;text-align:right;width:100%;}
.listwhitetext-hcrew {color:#003366;font-family:arial,verdana;font-size:11px;font-weight:bold;padding:0 0 2px;text-decoration:none;}
</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns()
	</script>
<script type="text/javascript">  

 function invoiceStatusCheck(rowId,targetElement,foreignAmt) 
   { 
   if(targetElement.checked)
     {
      var userCheckStatus = document.forms['paidInvoiceForm'].elements['invoiceCheck'].value;
      var totalActualForeignAmt = document.forms['paidInvoiceForm'].elements['totalActualForeignAmt'].value;
      if(userCheckStatus == '')
      {
	  	document.forms['paidInvoiceForm'].elements['invoiceCheck'].value = rowId;
	  	document.forms['paidInvoiceForm'].elements['totalActualForeignAmt'].value = foreignAmt;
      }
      else
      {
       	var userCheckStatus = document.forms['paidInvoiceForm'].elements['invoiceCheck'].value = userCheckStatus + ',' + rowId;
      	document.forms['paidInvoiceForm'].elements['invoiceCheck'].value = userCheckStatus.replace( ',,' , ',' );
      }
      
      if(totalActualForeignAmt == '')
      {
	  	document.forms['paidInvoiceForm'].elements['totalActualForeignAmt'].value = foreignAmt;
      }
      else
      {
      	document.forms['paidInvoiceForm'].elements['totalActualForeignAmt'].value = (parseFloat(document.forms['paidInvoiceForm'].elements['totalActualForeignAmt'].value) + parseFloat(foreignAmt))
      }
      
    }
   if(targetElement.checked==false)
    {
     var userCheckStatus = document.forms['paidInvoiceForm'].elements['invoiceCheck'].value;
     var userCheckStatus=document.forms['paidInvoiceForm'].elements['invoiceCheck'].value = userCheckStatus.replace( rowId , '' );
     document.forms['paidInvoiceForm'].elements['invoiceCheck'].value = userCheckStatus.replace( ',,' , ',' );
     
     document.forms['paidInvoiceForm'].elements['totalActualForeignAmt'].value = (parseFloat(document.forms['paidInvoiceForm'].elements['totalActualForeignAmt'].value) - parseFloat(foreignAmt))
     } 
      invoiceCheck();
    }
    
    
    function invoiceCheck(){
    var invoiceCheck = document.forms['paidInvoiceForm'].elements['invoiceCheck'].value;
    invoiceCheck=invoiceCheck.trim(); 
    if(invoiceCheck=='')
    {
    document.forms['paidInvoiceForm'].elements['button.save'].disabled=true;
    }
    else if(invoiceCheck==',')
    {
    document.forms['paidInvoiceForm'].elements['button.save'].disabled=true;
    }
    else if(invoiceCheck!='')
    {
      document.forms['paidInvoiceForm'].elements['button.save'].disabled=false;
    }
    }
    function winOpen(){
    openWindow('partnersPopup.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=notPaidBillToName&fld_code=notPaidBillToCode');
    }
    function clear_fields(){
			document.forms['paidInvoiceForm1'].elements['notPaidInvoiceNumber'].value = "";
			document.forms['paidInvoiceForm1'].elements['notPaidBillToCode'].value = "";
			document.forms['paidInvoiceForm1'].elements['notPaidBillToName'].value = "";
			
}
</script>
</head>

<s:form id="paidInvoiceForm1" name="paidInvoiceForm1" action="searchPaidInvoice" method="post">
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="firstDescription" />
<s:hidden name="seventhDescription" />
<div id="Layer3" style="width:95%;"> 
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		
<table class="table" border="1">
<thead>
<tr>
<th>Invoice Number</th>
<th>Bill To Code </th>
<th>Bill To Name</th>
<th></th>
</tr></thead> 
<tbody>

  <tr>
  <td ><s:textfield name="notPaidInvoiceNumber" required="true" cssClass="input-text" size="18" /> </td> 
   <td width=""><s:textfield cssClass="input-text" name="notPaidBillToCode" readonly="false" size="8" maxlength="10" />
   <img  align="top" class="openpopup" width="17" height="20" onclick="javascript:winOpen()" src="<c:url value='/images/open-popup.gif'/>" />
   </td> 
   <td><s:textfield cssClass="input-textUpper" name="notPaidBillToName" cssStyle="width:255px;" readonly="true" /></td>   
  <td>
       <s:submit cssClass="cssbutton1" cssStyle="width:55px;" key="button.search"/>  
       <input type="reset" class="cssbutton1" value="Clear" style="width:55px;"/>        
   </td>
   </tr>   
  </tbody>
 </table>	
 </div>	
</s:form>
<s:form id="paidInvoiceForm" name="paidInvoiceForm" action="updatePaidInvoice" method="post">
<div id="Layer1" style="width:95%;"> 
    
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Please select the Invoice for Fully Paid</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	
	<s:set name="paidInvoiceList" value="paidInvoiceList" scope="request"/> 
<display:table name="paidInvoiceList" class="table" requestURI="" id="paidInvoiceList" style="width:100%" defaultsort="1"  >   
 		
		<display:column property="recInvoiceNumber" sortable="true" title="Invoice#" style="width:70px" /> 
	    <display:column property="billToCode" sortable="true" title="BillToCode" maxLength="30"  style="width:70px"/>  
        <display:column property="billToName" sortable="true" title="BillToName"  style="width:170px"/>
       <display:column property="actualRevenueForeignSum" sortable="true" title="Invoice Total Amount" headerClass="containeralign" style="width:70px;text-align:right;"/>
        <display:column property="recRateCurrency" sortable="true" title="Currency"  style="width:10px"/>
        <sec-auth:authComponent componentId="module.billing.section.roleFinanceUnpaidInvoice.edit">
	<%;
	String billingComplete="true";
	int permissionDate  = (Integer)request.getAttribute("module.billing.section.roleFinanceUnpaidInvoice.edit" + "Permission");
 	if (permissionDate > 2 ){
 		billingComplete = "false"; 		
 	} 	
  %>
       <display:column title=""   style="width:10px"> 
	    <% if (billingComplete.equalsIgnoreCase("false")){%>
	    	<input type="checkbox"  style="margin-left:10px;"  onclick="invoiceStatusCheck('${paidInvoiceList.recInvoiceNumber}',this,'${paidInvoiceList.actualRevenueForeignSum}')" />
	    <%} %>
	      <% if (billingComplete.equalsIgnoreCase("true")){%>
	    	<input type="checkbox"  style="margin-left:10px;" disabled="disabled" onclick="invoiceStatusCheck('${paidInvoiceList.recInvoiceNumber}',this,'${paidInvoiceList.actualRevenueForeignSum}')" />
	    <%} %>
        </display:column>
   </sec-auth:authComponent>
</display:table> 
<table cellpadding="0" cellspacing="2" height="30px" style="border: 2px solid rgb(153, 187, 232); padding: 1px; background: rgb(216, 228, 253) none repeat scroll 0%; ">
<tr><td class="listwhitetext-hcrew" style="padding-left: 10px;padding-right: 10px; font-size: 12px;">
Total Invoice Amount:&nbsp; 
<s:textfield name="totalActualForeignAmt" cssClass="input-text" size="18" readonly="true"/>
<s:hidden name="invoiceCheck" /> 
</td></tr></table>
</div>
<sec-auth:authComponent componentId="module.billing.section.roleFinanceUnpaidInvoice.edit">
<%;
	String billingComplete="true";
	int permissionDate  = (Integer)request.getAttribute("module.billing.section.roleFinanceUnpaidInvoice.edit" + "Permission");
 	if (permissionDate > 2 ){
 		billingComplete = "false";
 		
 	}
 	
  %>
<% if (billingComplete.equalsIgnoreCase("false")){%>
<s:submit cssClass="cssbuttonA" key="button.save"  name="button.save" cssStyle="width:55px; height:25px" tabindex="23" />
<s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" onclick="setTimeout('invoiceCheck()',300);" />
	<%} %>
	</sec-auth:authComponent>
</s:form>
<script type="text/javascript">
//document.forms['paidInvoiceForm'].elements['invoiceCheck'].value='';
try{
invoiceCheck(); 
}
catch(e){}
</script>