<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
<title><fmt:message key="documentAccessControl.title"/></title>   
<meta name="heading" content="<fmt:message key='documentAccessControl.heading'/>"/>   
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();
</script>
<script>

function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove this document type?");
	var did = targetElement;
	if (agree){
		location.href="deleteDocAccess.html?id="+did;
	}else{
		return false;
	}
}

function checkStatusId(rowId, targetElement) {
		var Status = targetElement.checked;
		var url="updateMyfileStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse1;
        http22.send(null);		
} 

function handleHttpResponse1(){
      if (http22.readyState == 4){
           var result= http22.responseText         
      }
}	

function checkStatusAccId(rowId, targetElement) {
		var Status = targetElement.checked;
		var url="updateMyfileAccStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse2;
        http22.send(null);
} 
function handleHttpResponse2(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}

function checkStatusPartnerId(rowId, targetElement){
		var Status = targetElement.checked;
		var url="updateMyfilePartnerStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse3;
        http22.send(null);
}
function handleHttpResponse3(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}	


var http22 = getHTTPObject22();

function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

</script>

   
<style>
    .table th.sorted {
   		color:#15428B;
}

#mainPopup {
padding-left:10px;
padding-right:10px;
}


.table td, .table th,
.tableHeaderTable td {
border:1px solid #E0E0E0;
padding:0.3em;
}
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-20px;
padding:2px 0;
text-align:right;
width:100%;
}
</style>
</head>


<s:form cssClass="form_magn" id="docAccessForm" action="searchMyFiles" method="post" >

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="userCheck"/> 
 
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px" onclick="location.href='<c:url value="/editDocControl.html"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set>     

<div id="Layer1" style="width:100%;">

<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Document Access List</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%">
	<tbody>
		<tr>
			<td>
				<s:set name="documentAccessControls" value="documentAccessControls" scope="request"/>  
				<display:table name="documentAccessControls" class="table" requestURI="" id="documentAccessControlsList" export="true" defaultsort="9" defaultorder="descending" pagesize="25" style="width:100%" >   
				    <display:column property="fileType" sortable="true" title="Document Type" url="/editDocControl.html?from=list" paramId="id" paramProperty="id"/>
					<display:column property="fileDescription" sortable="true" title="Document Description" />
					<display:column  sortable="true" title="Customer Portal"   style="width:100px"> 
						<c:if test="${documentAccessControlsList.isCportal==true}">
					  		<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					 	</c:if>
					 	<c:if test="${documentAccessControlsList.isCportal==false}">
							<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					 	</c:if>
					</display:column>
					
					<display:column  sortable="true" title="Account Portal"   style="width:100px"> 
						<c:if test="${documentAccessControlsList.isAccportal==true}">
					  		<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					 	</c:if>
					 	<c:if test="${documentAccessControlsList.isAccportal==false}">
							<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					 	</c:if>
					</display:column>
					
					<display:column  sortable="true" title="Partner Portal"   style="width:100px"> 
						<c:if test="${documentAccessControlsList.isPartnerPortal==true}">
					  		<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					 	</c:if>
					 	<c:if test="${documentAccessControlsList.isPartnerPortal==false}">
							<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					 	</c:if>
					</display:column>
					
					<display:column  sortable="true" title="Service Provider"   style="width:100px"> 
						<c:if test="${documentAccessControlsList.isServiceProvider==true}">
					  		<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					 	</c:if>
					 	<c:if test="${documentAccessControlsList.isServiceProvider==false || documentAccessControlsList.isServiceProvider == null}">
							<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					 	</c:if>
					</display:column>
					
					<display:column  sortable="true" title="Partner Code"   style="width:100px"> 
						<c:if test="${documentAccessControlsList.isvendorCode==true}">
					  		<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					 	</c:if>
					 	<c:if test="${documentAccessControlsList.isvendorCode==false || documentAccessControlsList.isServiceProvider == null}">
							<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					 	</c:if>
					</display:column>
					
					<display:column  sortable="true" title="Paid"   style="width:100px"> 
						<c:if test="${documentAccessControlsList.isPaymentStatus==true}">
					  		<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					 	</c:if>
					 	<c:if test="${documentAccessControlsList.isPaymentStatus==false || documentAccessControlsList.isServiceProvider == null}">
							<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					 	</c:if>
					</display:column>
				    				    
				    <display:column title="Remove" style="width:45px;">
				    	<a><img align="middle" onclick="confirmSubmit(${documentAccessControlsList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
				    </display:column>
				   
				    <display:setProperty name="paging.banner.item_name" value="document"/>   
				    <display:setProperty name="paging.banner.items_name" value="documents"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Document List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Document List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Document List.pdf"/>   
				</display:table>  
  			</td>
		</tr>
	</tbody>
</table>
</div>  
<c:out value="${buttons}" escapeXml="false" />
</s:form>
<script type="text/javascript"> 
	try{
	<c:if test="${hitFlag == 1}" >
		<c:redirect url="/docControl.html"/>
	</c:if>
	}
	catch(e){}
</script>
