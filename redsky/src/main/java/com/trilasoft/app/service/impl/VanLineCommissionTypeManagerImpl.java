package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.service.VanLineCommissionTypeManager;
import com.trilasoft.app.dao.VanLineCommissionTypeDao;
import com.trilasoft.app.model.VanLineCommissionType;

public class VanLineCommissionTypeManagerImpl  extends GenericManagerImpl<VanLineCommissionType, Long> implements VanLineCommissionTypeManager {

	VanLineCommissionTypeDao vanLineCommissionTypeDao; 

    public VanLineCommissionTypeManagerImpl(VanLineCommissionTypeDao vanLineCommissionTypeDao) { 
        super(vanLineCommissionTypeDao); 
        this.vanLineCommissionTypeDao = vanLineCommissionTypeDao; 
    } 
    
    public List<VanLineCommissionType> vanLineCommissionTypeList(){
    	return vanLineCommissionTypeDao.vanLineCommissionTypeList();   
    }
    
    public List<VanLineCommissionType> searchVanLineCommissionType(String code, String type, String description, String glCode){
		return vanLineCommissionTypeDao.searchVanLineCommissionType(code, type, description, glCode);
	}
    
    public List isExisted(String code, String corpId){
    	return vanLineCommissionTypeDao.isExisted(code, corpId);
    }
	public List<VanLineCommissionType> getGLCodePopupList(String chargesGl){
    	return vanLineCommissionTypeDao.getGLCodePopupList(chargesGl);
    }
	public  List glList(String chargesGl){
		return vanLineCommissionTypeDao.glList(chargesGl);
	}

	public List searchVanLineCommissionPopup(String code, String type, String description, String glCode) {
		return vanLineCommissionTypeDao.searchVanLineCommissionPopup(code, type, description, glCode);
	}
}
