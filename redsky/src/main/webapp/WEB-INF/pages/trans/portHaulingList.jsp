<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="portList.title"/></title>   
    <meta name="heading" content="<fmt:message key='portList.heading'/>"/>   
    <script language="javascript" type="text/javascript">
  function clear_fields(){ 
			    document.forms['searchForm'].elements['portCode'].value = "";
			    document.forms['searchForm'].elements['portName'].value = ""; 
			      document.forms['searchForm'].elements['country'].value = ""; 
}
</script> 
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:3px;
margin-top:-18px;
!margin-top:-15px;
padding:2px 0px;
text-align:right;
width:99%;
}
</style> 
<script>
function goToCustomerDetail(targetValue){
        document.forms['searchForm'].elements['id'].value = targetValue;
        document.forms['searchForm'].action = 'editPort.html?from=list';
        document.forms['searchForm'].submit();
}
</script>  
</head>
<c:set var="fld_code" value="<%=request.getParameter("fld_code") %>" />
<c:set var="fld_description" value="<%=request.getParameter("fld_description") %>" />


<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px" onclick="location.href='<c:url value="editPort.html"/>'" value="<fmt:message key="button.add"/>"/> 
</c:set>  
<s:form id="searchForm" action='${empty param.popup?"searchHaulingPortList.html":"searchHaulingPortList.html?decorator=popup&popup=true"}' method="post" validate="true" >
<div id="layer1" style="width:100%; ">

<div id="otabs" style="">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		  <div class="spnblk">&nbsp;</div>
		</div>
		
		<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom:10px;" align="top"  key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/> 
</c:set>
<div id="content" align="center">
<div id="liquid-round-top" >
    <div class="top" style="margin-top:-20px;!margin-top:-2px; "><span></span></div>
    <div class="center-content">
		<table class="table" style="width:100%"  >
		<thead>
		<tr>
		<th><fmt:message key="port.portCode"/></th>
		<th><fmt:message key="port.portName"/></th> 
		<th>Country</th>
		</tr></thead>	
				<tbody>
				<tr>
				<td width="" align="left">
					    <s:textfield name="portCode" required="true" cssClass="input-text" size="30"/>
					</td>
					<td width="" align="left">
					    <s:textfield name="portName" required="true" cssClass="input-text" size="30"/>
					</td>
					<td width="" align="left">
					<s:select cssClass="list-menu" name="country" id="country" list="%{ocountry}" cssStyle="width:162px"  headerKey="" headerValue="" />
					</td>
		
					 <!--<s:hidden name="country" value="${country}"/>
					  --><s:hidden name="modeType" value="SEA"/>
					</tr>
					<tr>
					<td colspan="2"></td>
					<td width="" align="center" style="border-left: hidden;">
					    <c:out value="${searchbuttons}" escapeXml="false" />   
					</td>
				</tr>
				</tbody>
			</table>
			
				</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
<s:set name="ports" value="ports" scope="request"/>  

<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Port&nbsp;List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		
<display:table name="ports" class="table" requestURI="" id="portList" export="${empty param.popup}" defaultsort="2" pagesize="10" style="width:99%; margin-left:5px;margin-top: 2px;!margin-top:-5px;" 
		decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
    <c:if test="${empty param.popup}">  
    <display:column sortable="true" titleKey="port.portCode"><a onclick="goToCustomerDetail(${portList.id});" style="cursor:pointer"><c:out value="${portList.portCode}" /></a></display:column>
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="port.portCode"/>   
    </c:if>    
    <display:column property="portName" sortable="true" titleKey="port.portName"/>
    <display:column property="country" sortable="true" titleKey="port.country"/>
    <display:column property="mode" sortable="true" titleKey="port.modeType"/>  
    <display:setProperty name="paging.banner.item_name" value="port"/>   
    <display:setProperty name="paging.banner.items_name" value="ports"/>  
    </display:table>  
 <c:if test="${empty param.popup}"> 
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
<s:hidden name="id"/>
</div>
</s:form>
<script type="text/javascript">   
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/ports.html" ></c:redirect>
		</c:if>
		}
		catch(e){}
</script>  
