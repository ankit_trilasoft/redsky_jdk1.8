package com.trilasoft.app; 
import java.io.EOFException;
import java.io.IOException; 
import java.io.PrintWriter;  
import java.util.Date; 
import java.util.Random;

import javax.servlet.ServletException;  
import javax.servlet.http.HttpServlet; 
import javax.servlet.http.HttpServletRequest; 
import javax.servlet.http.HttpServletResponse; 

import org.springframework.transaction.IllegalTransactionStateException;

public class AppExceptionHandler extends HttpServlet {

    private static final long serialVersionUID = 1L; 
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
        processError(request, response); 
    } 

    protected void doPost(HttpServletRequest request,

            HttpServletResponse response) throws ServletException, IOException {

        processError(request, response);

    } 

    private void processError(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException { 
    	
        Throwable throwable = (Throwable) request .getAttribute("javax.servlet.error.exception");

        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");

        String servletName = (String) request .getAttribute("javax.servlet.error.servlet_name");

        if (servletName == null) {

            servletName = "Unknown";

        } 
        if(throwable!=null && (throwable instanceof EOFException || throwable instanceof IllegalTransactionStateException))
        throwable.printStackTrace();
        String requestUri = (String) request .getAttribute("javax.servlet.error.request_uri");

        if (requestUri == null) {

            requestUri = "Unknown";

        } 
        String errorMessage= (String) request .getAttribute("javax.servlet.error.message"); 
        Random rnd = new Random();
        String autoErroCode = String.format("%06x", rnd.nextInt(0x1000000));
        System.out.println("\n\n\n Error Code " + statusCode + "\n" +errorMessage);
        System.out.println("\n\n\n Error from Exception handler with Correlation ID " + autoErroCode); 
          response.setContentType("text/html");  
          request.setAttribute("autoErroCode", autoErroCode);
          request.setAttribute("statusCode", statusCode);
          request.getRequestDispatcher("/appErrorExceptionHandler.jsp").forward(request, response); 
    }

}