package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.CreditCard;
import com.trilasoft.app.model.MyMessage;



public interface CreditCardManager extends GenericManager<CreditCard, Long> {
    public List findMaxId();
    public List findByShipNumber(String shipNumber);
	public List getPrimaryFlagStatus(String shipNumber, String sessionCorpID);
	public List getCreditCardMasking(String number);
	public List getCreditcardNumber(Long id);
	public List getServiceOrder(String shipNumber, String sessionCorpID);
}
