<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="org.appfuse.model.User"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	User user = (User)auth.getPrincipal();
	String sessionCorpID = user.getCorpID();
%>
<head>   
<title><fmt:message key="reportsDetail.title"/></title>   
<meta name="heading" content="<fmt:message key='reportsDetail.heading'/>"/>   
<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
</c:if>
<style type="text/css">
	h2 {background-color: #CCCCCC}
	.attach{
	background:url("images/attachIcon.png") right center no-repeat, url("images/btn_bg.gif") right center repeat-x;
	border: 1px solid #CCCCCC; padding:2px 0px;
	}
#loader {
position:absolute; z-index:999; margin:0px auto -1px 250px;
}
 input[type="radio"] {margin:0px;}
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns()
	</script>
<script language="javascript" type="text/javascript">
function validateFields(){

	var partnerCode=document.forms['reportForm'].elements['partnerCode'].value;
	var recipientTo=document.forms['reportForm'].elements['recipientTo'].value;
	var reportSubject=document.forms['reportForm'].elements['reportSubject'].value;
	var reportBody=document.forms['reportForm'].elements['reportBody'].value;
	var typedoc=document.getElementsByName('typedoc');
	var docFileType=document.getElementById('docFileType').value;
	var invoiceIncludeFlag="FALSE";
	var checkBoxData=pickCheckBoxData();
	try{
		if(document.getElementById('includeInvoiceAttachment').checked){
			invoiceIncludeFlag="TRUE";
		}
	}catch(e){}
	var tp="";
	if(typedoc != null && typedoc != undefined && typedoc.length > 0){
		for(var i=0; i<typedoc.length; i++){
			if(typedoc[i].checked == true)
				tp="found";
		}
	}
	 if((recipientTo.trim()==null)||(recipientTo.trim()=='')){
		alert('Please enter values for Recipient To.');
		  return false;  
	}
	if((reportSubject.trim()==null)||(reportSubject.trim()=='')){
		alert('Please enter values for Subject To.');
		  return false;  
	}
	if((reportBody.trim()==null)||(reportBody.trim()=='')){
		alert('Please enter values for Body.');
		  return false;  
	}
	 if((tp.trim()=='')||(tp.trim()==null)){
		alert('Please select one option in Preview Form.');
		  return false;  
	}
	 if((docFileType==null)||(docFileType.trim()=='')){
			alert('Please select Document Type First.');
			  return false;  
		}
		
	<c:forEach var="reportParameter" items="${reportParameters}" varStatus="status">
	var ab="${reportParameter.name}";
	  if(((document.forms['reportForm'].elements['reportParameter_'+ab].value) == '')&&(ab != 'Remark')){
		  document.forms['reportForm'].elements['reportParameter_'+ab].focus();
			alert('Please enter values for '+"${reportParameter.name}"+'.');
		  return false;  
		} 
		if('${reportValidationFlag}' == 'Yes')
		{
	  	if(((document.forms['reportForm'].elements['reportParameter_'+ab].value) == '%' || (document.forms['reportForm'].elements['reportParameter_'+ab].value) == '%%')&&(ab != 'Remark')){
	  		 document.forms['reportForm'].elements['reportParameter_'+ab].focus();
			alert("${reportParameter.name}"+' as % or %% not allowed.');
		  return false;  
		}
		} 
	</c:forEach>
	var url = 'emailViewReportWithParam.html?decorator=popup&popup=true&invoiceIncludeFlag='+invoiceIncludeFlag+'&checkBoxData='+checkBoxData;
	document.forms['reportForm'].action =url;
	document.forms['reportForm'].submit();
	return true;
}

function refreshParent() {
	window.close();
}

</script>
<script>
function display(){
	window.onload=displayStatusReason();
}
function displayStatusReason(){
     var statusValue=document.forms['reportForm'].elements['docsxfer'].value;
     if(statusValue=='Yes'){
        document.getElementById('docs').style.display='block';
     }else{
        document.getElementById('docs').style.display='none';
     }
} 
function validateFieldsTT(val){
	<c:forEach var="reportParameter" items="${reportParameters}" varStatus="status">
		var ab="${reportParameter.name}";
		  if((document.forms['reportForm'].elements['reportParameter_'+ab].value) == ''){
			  document.forms['reportForm'].elements['reportParameter_'+ab].focus();
				alert('Please enter values for '+"${reportParameter.name}"+'.');
			  return false;  
			} 
			if('${reportValidationFlag}' == 'Yes')
			{
		  	if((document.forms['reportForm'].elements['reportParameter_'+ab].value) == '%' || (document.forms['reportForm'].elements['reportParameter_'+ab].value) == '%%'){
		  		 document.forms['reportForm'].elements['reportParameter_'+ab].focus();
				alert("${reportParameter.name}"+' as % or %% not allowed.');
			  return false;  
			}
			} 		
	</c:forEach>	
	document.forms['reportForm'].elements['fileType'].value = val;
	var url = 'viewReportWithParam.html?decorator=popup&popup=true';
	document.forms['reportForm'].action =url;
	document.forms['reportForm'].submit();
	return true;
}
function documentMap(targetElement){
   	var fileType = targetElement.value;
   	var url="docMapFolder.html?ajax=1&decorator=simple&popup=true&fileType=" + encodeURI(fileType);
   	http2.open("GET", url, true);
   	http2.onreadystatechange = handleHttpResponseMap;
   	http2.send(null);
}
function handleHttpResponseMap(){
      if(http2.readyState == 4){
		var results = http2.responseText
        results = results.trim();
        var res = results.substring(1,results.length-1);
		document.forms['reportForm'].elements['mapFolder'].options[0].text = res; 
		document.forms['reportForm'].elements['mapFolder'].options[0].value = res;
		document.forms['reportForm'].elements['mapFolder'].options[0].selected=true;					     	 
	}
}
var http3 = getHTTPObject();
var http2 = getHTTPObject();
var http331 = getHTTPObject();
function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var httpMergeSO = getHTTPObject();

function showSO(target){ 
	if(document.forms['reportForm'].elements['recipientTo'].value=='${defaultBillingEmail}'){
		document.forms['reportForm'].elements['recipientTo'].value='';
	}
	 var soDiv = document.getElementById("soList");
	 var partnerNumber = target.value;	 
	 if(partnerNumber!=''){
		 soDiv.style.display = 'none';
		 	document.getElementById("loader").style.display = "block";
		 	var reportModule=document.forms['reportForm'].elements['reportModule'].value;
		 	var jobNo='${jobNumber}';
		 	var url="linkingUserList.html?ajax=1&decorator=simple&popup=true&partnerCode="+partnerNumber+"&reportModule="+reportModule+"&jobNumber="+jobNo;
		 	httpMergeSO.open("GET", url, true);
		 	httpMergeSO.onreadystatechange =  handleHttpResponseMergeSoList;
			httpMergeSO.send(null);	
	 	}else{
		 	soDiv.style.display = 'none';
	 	}
	 	var url="defaultStationary.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerNumber);
	   	http3.open("GET", url, true);
	   	http3.onreadystatechange = handleHttpResponseStationary;
	   	http3.send(null);
}
function handleHttpResponseStationary(){
	if (http3.readyState == 4){
	    var results= http3.responseText;
	    results = results.trim();	          
	    if(results!=null && results!=''){
		    try{
		    document.forms['reportForm'].elements['reportParameter_Print Options'].options[i].text = results; 
			document.forms['reportForm'].elements['reportParameter_Print Options'].options[i].value = results;
			document.forms['reportForm'].elements['reportParameter_Print Options'].options[i].selected=true;
		    }catch(e){}
	    }else{
	    	  try{		    	
	  	    	document.forms['reportForm'].elements['reportParameter_Print Options'].options[0].text = "No header / footer"; 
	  			document.forms['reportForm'].elements['reportParameter_Print Options'].options[0].value = "No header / footer";
	  			document.forms['reportForm'].elements['reportParameter_Print Options'].options[0].selected=true;
	  		    }catch(e){}
	    } 	    
	}
}
function handleHttpResponseMergeSoList(){
	if (httpMergeSO.readyState == 4){
	    var results= httpMergeSO.responseText; 
	    var soDiv = document.getElementById("soList");      
	    soDiv.style.display = 'block';
	    document.getElementById("loader").style.display = "none";
	    soDiv.innerHTML = results;
	    }
}
function setDescriptionDuplicate(targetElement){
	var fileType=document.forms['reportForm'].elements['docFileType'].value;
	if(fileType=='Credit Card Authorization'){		 
		    document.getElementById('isServiceProvider').checked=false;
		    document.getElementById('isPartnerPortal').checked=false;
		    document.getElementById('isAccportal').checked=false;
		    document.getElementById('isCportal').checked=false;
		    document.getElementById('isBookingAgent').checked=false;
			document.getElementById('isNetworkAgent').checked=false;
			document.getElementById('isOriginAgent').checked=false;
			document.getElementById('isSubOriginAgent').checked=false;
			document.getElementById('isDestAgent').checked=false;
			document.getElementById('isSubDestAgent').checked=false;
		 }
	document.forms['reportForm'].elements['myFile.description'].value = targetElement.options[targetElement.selectedIndex].text;
}
var form_submitted = false;
		function submit_form()
		{
		  if (form_submitted)
		  {
		    alert ("Your form has already been submitted. Please wait...");
		    return false;
		  }
		  else
		  {
		    form_submitted = true;
		    return true;
		  }
		}
		function setCostingDirectLink(target){
			if(target.checked){
				var reportBody=document.forms['reportForm'].elements['reportBody'].value;
				try{
					var ss="${linkCostingPage}";
					ss="\n<a href="+ss+">"+ss+"</a>";
				document.forms['reportForm'].elements['reportBody'].value=reportBody+ss;
				}catch(e){}
			}else{
				var field= document.getElementById('reportBody');
			    field.value= field.defaultValue;
			}
		}
		function documentAccessControlAjax(){
		    var fileType = document.forms['reportForm'].elements['docFileType'].value;
		    var url="docAccessControl.html?ajax=1&decorator=simple&popup=true&fileType=" + encodeURI(fileType);
		    http331.open("GET", url, true);
		    http331.onreadystatechange = handleHttpResponse331;
		    http331.send(null);
			
		}
		function handleHttpResponse331(){
			 if (http331.readyState == 4){
		         var results = http331.responseText
		         results = results.trim();
		         var res = results.replace("[",'');
		         res = res.replace("]",'');
		         if(res !='' && res.length>2){
		        	 res = res.split("#");
		        		 if(res[0]=='true'){
		        			 <configByCorp:fieldVisibility componentId="component.standard.cPortalActivation">
		            			document.getElementById('isCportal').checked=true;
		            			</configByCorp:fieldVisibility>
		            		}else{
		            			<configByCorp:fieldVisibility componentId="component.standard.cPortalActivation">
		            			document.getElementById('isCportal').checked=false;
		            			</configByCorp:fieldVisibility>
		            		}
			        	 if(res[1]=='true'){
			        		 <configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">
			         			document.getElementById('isAccportal').checked=true;
			         			</configByCorp:fieldVisibility>
			         		}else{
			         			<configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">
			        			document.getElementById('isAccportal').checked=false;
			        			</configByCorp:fieldVisibility>
			        		}
			        	 if(res[2]=='true'){
			         			document.getElementById('isPartnerPortal').checked=true;
			         		}else{
			        			document.getElementById('isPartnerPortal').checked=false;
			        		}
			        	 if(res[9]=='true'){
			         			document.getElementById('isServiceProvider').checked=true;
			         		}else{
			        			document.getElementById('isServiceProvider').checked=false;
			        		}
			        	 if(res[3]=='true'){
			         			document.getElementById('isBookingAgent').checked=true;
			         		}else{
			        			document.getElementById('isBookingAgent').checked=false;
			        		}
			        	 if(res[4]=='true'){
			         			document.getElementById('isNetworkAgent').checked=true;
			         		}else{
			        			document.getElementById('isNetworkAgent').checked=false;
			        		}
			        	 if(res[5]=='true'){
			         			document.getElementById('isOriginAgent').checked=true;
			         		}else{
			        			document.getElementById('isOriginAgent').checked=false;
			        		}
			        	 if(res[6]=='true'){
			         			document.getElementById('isSubOriginAgent').checked=true;
			         		}else{
			        			document.getElementById('isSubOriginAgent').checked=false;
			        		}
			        	 if(res[7]=='true'){
			         			document.getElementById('isDestAgent').checked=true;
			         		}else{
			        			document.getElementById('isDestAgent').checked=false;
			        		}
			        	 if(res[8]=='true'){
			         			document.getElementById('isSubDestAgent').checked=true;
			         		}else{
			        			document.getElementById('isSubDestAgent').checked=false;
			        		}			 
				        	 if(res[12]=='true'){
				         			document.getElementById('isPaymentStatus').value="true";
				         		}else{
				        			document.getElementById('isPaymentStatus').value="false";
				        		}			        		
			        		
			        	 checkCheckbox2();		        					        		
		         }
			 }
		}
		function checkCheckbox2(){
			var i=0;
			var j=0;
			if(!document.getElementById('isBookingAgent').checked){ i++; }
			if(!document.getElementById('isNetworkAgent').checked){ i++; }
			if(!document.getElementById('isOriginAgent').checked){ i++; }
			if(!document.getElementById('isSubOriginAgent').checked){ i++; }
			if(!document.getElementById('isDestAgent').checked){ i++; }
			if(!document.getElementById('isSubDestAgent').checked){ i++; }
			if(document.getElementById('isBookingAgent').checked){ j++; }
			if(document.getElementById('isNetworkAgent').checked){ j++; }
			if(document.getElementById('isOriginAgent').checked){ j++; }
			if(document.getElementById('isSubOriginAgent').checked){ j++; }
			if(document.getElementById('isDestAgent').checked){ j++; }
			if(document.getElementById('isSubDestAgent').checked){ j++; }
			if(i>=6){
				document.getElementById('checkbox2').checked=false;
			}
			if(j>=6){
				document.getElementById('checkbox2').checked=true;
			}			
		}
		function checkAll(target){
			var fileType=document.forms['reportForm'].elements['docFileType'].value;
			if(fileType=='Credit Card Authorization'){
				 alert("Secure Authorized Document")
				 target.checked=false;
			}else{				
			if(target.checked){
         			document.getElementById('isBookingAgent').checked=true;
         			document.getElementById('isNetworkAgent').checked=true;
         			document.getElementById('isOriginAgent').checked=true;
         			document.getElementById('isSubOriginAgent').checked=true;
         			document.getElementById('isDestAgent').checked=true;
         			document.getElementById('isSubDestAgent').checked=true;
			}else{
        			document.getElementById('isBookingAgent').checked=false;
        			document.getElementById('isNetworkAgent').checked=false;
        			document.getElementById('isOriginAgent').checked=false;
        			document.getElementById('isSubOriginAgent').checked=false;
        			document.getElementById('isDestAgent').checked=false;
        			document.getElementById('isSubDestAgent').checked=false;
			}
			 }
		}
		function pickCheckBoxData(){
			var isCportal="F";
			var isAccportal="F";
			var isPartnerPortal="F";
			var isServiceProvider="F";

			var isBookingAgent="F";
			var isNetworkAgent="F";
			var isOriginAgent="F";
			var isSubOriginAgent="F";
			var isDestAgent="F";
			var isSubDestAgent="F";

			var includeInvoiceAttachment="F";
			var isPaymentStatus="F";
			var finalVal="";
			<configByCorp:fieldVisibility componentId="component.standard.cPortalActivation">
			if(document.getElementById('isCportal').checked){
				isCportal="T";
			}
			</configByCorp:fieldVisibility>
			<configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">
			if(document.getElementById('isAccportal').checked){
				isAccportal="T";
			}
			</configByCorp:fieldVisibility>
			if(document.getElementById('isPartnerPortal').checked){
				isPartnerPortal="T";
			}
			if(document.getElementById('isServiceProvider').checked){
				isServiceProvider="T";
			}
			if(document.getElementById('isBookingAgent').checked){
				isBookingAgent="T";
			}
			if(document.getElementById('isNetworkAgent').checked){
				isNetworkAgent="T";
			}
			if(document.getElementById('isOriginAgent').checked){
				isOriginAgent="T";
			}
			if(document.getElementById('isSubOriginAgent').checked){
				isSubOriginAgent="T";
			}
			if(document.getElementById('isDestAgent').checked){
				isDestAgent="T";
			}
			if(document.getElementById('isSubDestAgent').checked){
				isSubDestAgent="T";
			}			
			try{
			if(document.getElementById('includeInvoiceAttachment').checked){
				includeInvoiceAttachment="T";
			}
			}catch(e){}
			if(document.getElementById('isPaymentStatus').value=="true"){
				isPaymentStatus="T";
			}			
			finalVal=isCportal+"~"+isAccportal+"~"+isPartnerPortal+"~"+isServiceProvider+"~"+isBookingAgent+"~"+isNetworkAgent+"~"+isOriginAgent+"~"+isSubOriginAgent+"~"+isDestAgent+"~"+isSubDestAgent+"~"+includeInvoiceAttachment+"~"+isPaymentStatus;
			return finalVal;
		}
		function checkSecureDoc(target){
			var fileType=document.forms['reportForm'].elements['docFileType'].value;
			if(fileType=='Credit Card Authorization'){
				 alert("Secure Authorized Document")
				 target.checked=false;
				 }
		}		
</script>
<style type="text/css">
img {
cursor:pointer;
}
#Layer1 {
width:540px;
}
</style>
</head>  
<body style="background-color:#444444;">
<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>
<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>
<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>
<s:form id="reportForm" target="_parent" action='${empty param.popup?"viewReportWithParam.html":"viewReportWithParam.html?decorator=popup&popup=true"}' method="post" onsubmit="return submit_form()" validate="true" enctype="multipart/form-data" >
	<s:hidden name="formFieldName" value="${reports.formFieldName}"/>
	<s:hidden name="formValue" value="${reports.formValue}"/>
	<s:hidden name="docsxfer" value="<%=request.getParameter("docsxfer") %>" />
	<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
	<s:hidden name="module" value="%{reports.module}"/>
	<s:hidden name="reportName" value="${reports.description}"/>
	<s:hidden name="reportModule" value="${reportModule}"/>
	<s:hidden name="reportSubModule" value="${reportSubModule}"/>
    <s:hidden name="jobNumber" value="<%=request.getParameter("customerFile.sequenceNumber") %>"/>
	<c:set var="jobNumber" value="<%=request.getParameter("jobNumber") %>" />	
	<s:set name="reportss" value="reportss" scope="request"/>
	<s:hidden name="claimNumber" value="<%=request.getParameter("claimNumber") %>" />
	<c:set var="claimNumber" value="<%=request.getParameter("claimNumber") %>" />
	<s:hidden name="bookNumber" value="<%=request.getParameter("bookNumber") %>" />
	<c:set var="bookNumber" value="<%=request.getParameter("bookNumber") %>" />
	<s:hidden name="noteID" value="<%=request.getParameter("noteID") %>" />
	<c:set var="noteID" value="<%=request.getParameter("noteID") %>" />
	<s:hidden name="custID" value="<%=request.getParameter("custID") %>" />
	<c:set var="custID" value="<%=request.getParameter("custID") %>" />
	<s:hidden name="cid" value="<%=request.getParameter("cid") %>" />
	<s:set name="cid" value="cid" scope="session"/>
	<s:hidden name="sid" value="<%=request.getParameter("sid") %>" />
	<s:set name="sid" value="sid" scope="session"/>
	<s:hidden name="invoiceNumber" value="<%=request.getParameter("invoiceNumber") %>" />
	<c:set var="invoiceNumber" value="<%=request.getParameter("invoiceNumber") %>" />
	<s:hidden name="regNumber" value="<%=request.getParameter("regNumber")%>"/>
	<c:set var="regNumber" value="<%=request.getParameter("regNumber") %>" />
	<s:hidden name="jobType" value="<%=request.getParameter("jobType") %>"/>
	<c:set var="jobType" value="<%=request.getParameter("jobType") %>" />
	<s:hidden name="preferredLanguage" value="<%=request.getParameter("preferredLanguage") %>"/>
	<c:set var="preferredLanguage" value="<%=request.getParameter("preferredLanguage") %>" />	
	<s:hidden name="reportSignature" />
	<div id="Layer1">
	<table cellspacing="0" cellpadding="0" border="0" style="padding-bottom:0px">
		
			<tr>
				<td><img src="<c:url value='/images/form_ico.gif'/>"/></td>
				<td>
					<table cellspacing="0" cellpadding="0">
					<tr><td>&nbsp;</td></tr> 
					<tr><td><b><font style="font-size:16px; font-family:Verdana, Geneva, sans-serif; font-style:italic; color: #EB8234;">Form Manager</font></b></td></tr> 
					<tr style="height: 5px;"><td></td></tr> 
					<tr><td><b><font style="color: #5D5D5D;">Form : ${reports.description}</font></b></td></tr> 
					</table>
				</td>
			</tr>	
	</table>
	<c:if test="${list!='main'}">	
	<div id="newmnav">
		  <ul>
		  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Forms Manager<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
		</div>
		<div class="spn" style="width:510px;line-height:0.2em;">&nbsp;</div>
</c:if> 
</div>
<table class="notesDetailTable" cellspacing="0" cellpadding="0" border="0" style="width:510px;padding-bottom:0px">
		<tbody>
			<tr>
				<td>
		 			<div class="subcontent-tab">Preview Email</div>
		   				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
		    				<tbody>	
		    					<tr><td style="height:10px;"></td></tr> 
								<tr>
                                <td align="right" class="listwhitetext" width="100px">
                                Partner :&nbsp;&nbsp;</td><td><s:select name="partnerCode" cssClass="list-menu" list="%{partnerCodeArrayList}" headerKey="" headerValue="" cssStyle="width:170px" onchange="showSO(this);" />
                                </td>
                                <td style="height:10px;"></td>
								<c:if test="${linkCostingPage!=''}">                                
		                        <td align="left" class="listwhitetext" width="">
                                Add costing URL:&nbsp;<s:checkbox id="costingDirectLink" cssStyle="vertical-align:sub;" name="costingDirectLink" onclick="setCostingDirectLink(this);" />
                                </td> 
                                </c:if>                
								</tr>
								<tr style="height: 10px;"><td></td></tr>
								<tr>
                                <td align="right" class="listwhitetext" width="100px">
                                Recipients :&nbsp;To :<font color="red" size="2">*</font>&nbsp;&nbsp;
                                </td>
                                <td>
                                <s:textfield name="recipientTo" cssClass="input-text" value="${defaultBillingEmail}" cssStyle="width:170px" />
                                </td>
                                <td align="right" class="listwhitetext" width="45px">
                                &nbsp;&nbsp;&nbsp;&nbsp;CC :&nbsp;&nbsp;
                                </td>
                                <td>
                                <s:textfield name="recipientCC" cssClass="input-text" cssStyle="width:170px" />
                                </td>
								</tr>
						     </tbody>
						</table>
						<div style="text-align:center; display:none" id="loader"><img src="/images/ajax-loader.gif" /></div>
						<div id="soList"></div>
						<table class="detailTabLabel">
						<tr>
			  			<td class="listwhitetext" width="93" align="right">Subject : </td>
			  			<td colspan="5">
			            <s:textfield name="reportSubject" cssClass="input-text"  cssStyle="width:387px;" />
			            </td>
			  			</tr>
						<tr>
			  			<td class="listwhitetext" width="93" align="right">Body : </td>
			  			<td colspan="5">
			            <s:textarea id="reportBody" name="reportBody" cssClass="input-text"  cssStyle="width:387px;height:50px" />
			            </td>
			  			</tr>
						<tr>
			  			<td class="listwhitetext" width="93" align="right">Signature : </td>
			  			<td colspan="5">
			            <s:textarea name="reportSignaturePart" cssClass="input-text"  cssStyle="width:387px;height:50px" />
			            </td>			            
			  			</tr>
						</table>
				</td>	
  			</tr>  			
  		</tbody>
</table>			
	<table class="notesDetailTable" cellspacing="0" cellpadding="0" border="0" style="width:510px;padding-bottom:0px">
		<tbody>
			<tr>
				<td>
		 			<div class="subcontent-tab">Select Parameters and Output format for Form</div>
		   				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
		    				<tbody>	
		    					<tr><td style="height:10px;"></td></tr> 
								<tr><td><b>&nbsp;&nbsp;Parameter(s):</b></td></tr>
								<tr style="height: 10px;"><td></td></tr> 
								  <c:forEach var="reportParameter" items="${reportParameters}">
									<tr>
										<td width="150px" align="right" class="listwhitetext">&nbsp;&nbsp;&nbsp;<c:out value="${reportParameter.name}"/> <c:if test="${reportParameter.name!='Remark'}"><font color="red" size="2">*</font></c:if> :&nbsp;&nbsp;</td>
											<c:if test='${reportParameter.valueClassName == "java.util.Date"}'> 
											  <td class="listwhitetext">
											  	<c:set var="inputParameterName" value='${"reportParameter_"}${reportParameter.name}'/>
											  	<s:textfield cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" maxlength="11" readonly="true"/>
											  </td>
											  	<td align="left"><img id="${inputParameterName}-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
											</c:if>
											<c:if test='${reportParameter.valueClassName != "java.util.Date"}'>											
											 <c:set var="inputParameterName" value='${"reportParameter_"}${reportParameter.name}'/>
										      <td class="listwhitetext">
										    	<c:if test="${(inputParameterName == 'reportParameter_User Name')}">
													<s:textfield cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${pageContext.request.remoteUser}" readonly="true" size="15"/>
												</c:if>
												<c:if test="${!(inputParameterName == 'reportParameter_User Name')}">
												<c:if test="${jobNumber==null || jobNumber=='' }">
													<c:if test="${(inputParameterName == 'reportParameter_Corporate ID')}">
											  			<s:textfield cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="<%=sessionCorpID %>" readonly="true" size="15"/>
											  		</c:if>
											  		<c:if test="${!(inputParameterName == 'reportParameter_Corporate ID')}">
											  			<c:if test="${!(inputParameterName == 'reportParameter_Customer File Number')}">
											  				<c:if test="${(inputParameterName == 'reportParameter_Warehouse')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{house}" cssStyle="width:105px" headerKey="" headerValue="" value="${house}"/>
			    											</c:if>
			    											<c:if test="${(inputParameterName == 'reportParameter_Coordinator')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{soList}" cssStyle="width:105px" headerKey="" headerValue="" value="${soList}"/>
			    											</c:if>
			    											<c:if test="${(inputParameterName == 'reportParameter_Payment Status')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{payableStatusList}" cssStyle="width:105px" headerKey="" headerValue="" value="${payableStatusList}"/>
			    											</c:if>
			    											
			    											<c:if test="${(inputParameterName == 'reportParameter_Documents')}">
			    								 				<s:textarea  id="${inputParameterName}" name="${inputParameterName}" value="" readonly="" cols="30" label="6" />
															</c:if>		
															<c:if test="${(inputParameterName == 'reportParameter_Print Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Print With Header','Print Without Header'}" cssStyle="width:115px"  value=" "/>
															</c:if>	
															<c:if test="${(inputParameterName == 'reportParameter_Print Options')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="%{printOptions}" cssStyle="width:175px"  value="%{printOptionsValue}"/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Print_Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'No header / footer','UGRN - ACF'}" cssStyle="width:175px"  value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Print_Options')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'No header / footer','Cadogan Tate'}" cssStyle="width:175px"  value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Print_Method')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'No header / footer','UGRN','Voerman'}" cssStyle="width:175px"  value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Send Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Email','Print'}" cssStyle="width:115px"  value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Partner Code')}" >
			    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="reportParameter_Partner Code" size="15"/>
			    										   <img class="openpopup" width="17" height="20" style="vertical-align:bottom;" onclick="window.open('partnersPopup.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=reportParameter_Partner Code','billtocode','width=950,height=650');"  src="<c:url value='/images/open-popup.gif'/>" />
															<s:hidden name="secondDescription" />
															<s:hidden name="thirdDescription" />
															<s:hidden name="fourthDescription" />
															<s:hidden name="fifthDescription" />
															<s:hidden name="sixthDescription" />
															<s:hidden name="firstDescription" />
															<s:hidden name="description" />
			    				                     	</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Value Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Estimate','Revised'}" cssStyle="width:115px"  value=" "/>
															</c:if>
															
															<c:if test="${(inputParameterName == 'reportParameter_Partner Name')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{partnerCodeArrayList}" cssStyle="width:250px" headerKey="" headerValue="" value="${partnerCodeArrayList}"/>
			    											</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Predefined Text')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{preDefTxt}" cssStyle="width:105px" headerKey="" headerValue="" value="${preDefTxt}"/>
			    											</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Remark')}">
			    								 				<s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:60px" value=" "/>
													        </c:if>					
														  	<c:if test="${reports.corpID == 'SSCW'}">
			    											<c:if test="${!(inputParameterName == 'reportParameter_Warehouse') && !(inputParameterName == 'reportParameter_Coordinator') && !(inputParameterName == 'reportParameter_Payment Status')  && !(inputParameterName == 'reportParameter_Predefined Text') && !(inputParameterName == 'reportParameter_Documents') && !(inputParameterName == 'reportParameter_Print Option') && !(inputParameterName == 'reportParameter_Print Options') && !(inputParameterName =='reportParameter_Print_Option') && !(inputParameterName =='reportParameter_Print_Options') && !(inputParameterName =='reportParameter_Print_Method') && !(inputParameterName == 'reportParameter_Send Option')  && !(inputParameterName == 'reportParameter_Value Option') && !(inputParameterName == 'reportParameter_Partner Name') && !(inputParameterName == 'reportParameter_Partner Code') && !(inputParameterName == 'reportParameter_Remark')}" >
			    												<s:textfield  cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value=""/>
			    											</c:if>
			    											</c:if>
			    											<c:if test="${reports.corpID != 'SSCW'}">
			    											<c:if test="${!(inputParameterName == 'reportParameter_Warehouse') && !(inputParameterName == 'reportParameter_Coordinator') && !(inputParameterName == 'reportParameter_Payment Status') && !(inputParameterName == 'reportParameter_Predefined Text') && !(inputParameterName == 'reportParameter_Documents') && !(inputParameterName == 'reportParameter_Print Option') && !(inputParameterName == 'reportParameter_Print Options') && !(inputParameterName =='reportParameter_Print_Option') && !(inputParameterName =='reportParameter_Print_Options') && !(inputParameterName =='reportParameter_Print_Method') && !(inputParameterName == 'reportParameter_Send Option') && !(inputParameterName == 'reportParameter_Value Option') && !(inputParameterName == 'reportParameter_Partner Code') && !(inputParameterName == 'reportParameter_Remark')}" >
			    												<s:textfield  cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value="%%"/>
			    											</c:if>
			    											</c:if>
											  			</c:if>
											  		</c:if>
											  		<c:if test="${custID != null || custID != ''}">
											  			<c:if test="${(inputParameterName == 'reportParameter_Customer File Number')}">
														  	<s:textfield  cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${custID}" readonly="false" size="15"/>
														</c:if> 
		 											</c:if>
											  	</c:if>
											  	<c:if test="${!(jobNumber==null || jobNumber=='')}">
											  	
											  		<c:if test="${custID != null || custID != ''}">
											  			<c:if test="${(inputParameterName == 'reportParameter_Customer File Number')}">
														  	<s:textfield  cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${custID}" readonly="false" size="15"/>
														</c:if> 
		 											</c:if>
		 											
		 											<c:if test="${custID != null || custID != ''}">
											  			<c:if test="${(inputParameterName == 'reportParameter_Quotation File Number')}">
														  	<s:textfield  cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${jobNumber}" readonly="false" size="15"/>
														</c:if> 
		 											</c:if>
		 											
		 											<c:if test="${invoiceNumber != null || invoiceNumber != ''}">
		 												<c:if test="${(inputParameterName == 'reportParameter_Invoice Number')}">
		 													<s:textfield  cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${invoiceNumber}" readonly="false" size="15"/>
														</c:if> 
		 											</c:if>
		 											
		 											<c:if test="${regNumber != null || regNumber != ''}">
		 												<c:if test="${(inputParameterName == 'reportParameter_Regnum')}">
		 													<s:textfield  cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${regNumber}" readonly="false" size="15"/>
														</c:if> 
		 											</c:if>
		 											
											  		<c:if test="${(inputParameterName == 'reportParameter_Service Order Number')}">
											  			<s:textfield cssStyle="text-align:right" cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" value="${jobNumber}" readonly="true" size="15"/>
											  		</c:if>
											  		
											  		<c:if test="${!(claimNumber==null || claimNumber=='')}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Claim Number')}">
														  	<s:textfield cssStyle="text-align:right" cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${claimNumber}" readonly="true" size="15"/>
														</c:if>
													</c:if>
													<c:if test="${reports.corpID == 'SSCW'}">
													<c:if test="${claimNumber==null || claimNumber==''}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Claim Number')}">
														  	<s:textfield cssStyle="text-align:right" cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value=""/>
														</c:if>
													</c:if>
													</c:if>
													
													<c:if test="${reports.corpID != 'SSCW'}">
													<c:if test="${claimNumber==null || claimNumber==''}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Claim Number')}">
														  	<s:textfield cssStyle="text-align:right" cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value="%%"/>
														</c:if>
													</c:if>
													</c:if>
													
													<c:if test="${!(bookNumber==null || bookNumber=='')}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Book Number')}">
														  	<s:textfield cssStyle="text-align:right" cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${bookNumber}" readonly="true" size="15"/>
														</c:if>
													</c:if>
													
													<c:if test="${reports.corpID == 'SSCW'}">
													<c:if test="${bookNumber==null || bookNumber==''}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Book Number')}">
														  	<s:textfield cssStyle="text-align:right" cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value=""/>
														</c:if>
													</c:if>
													</c:if>
													
													<c:if test="${reports.corpID != 'SSCW'}">
													<c:if test="${bookNumber==null || bookNumber==''}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Book Number')}">
														  	<s:textfield cssStyle="text-align:right" cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value="%%"/>
														</c:if>
													</c:if>
													</c:if>
												  		
												  	<c:if test="${!(noteID==null || noteID=='')}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Work Ticket Number')}">
														  	<s:textfield cssStyle="text-align:right"cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="${noteID}" readonly="true" size="15"/>
														</c:if>
													</c:if>
													<c:if test="${reports.corpID == 'SSCW'}">
													<c:if test="${(noteID==null || noteID=='')}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Work Ticket Number')}">
														  	<s:textfield cssStyle="text-align:right"cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value=""/>
														</c:if>
													</c:if>
													</c:if>
													
													<c:if test="${reports.corpID != 'SSCW'}">
													<c:if test="${(noteID==null || noteID=='')}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Work Ticket Number')}">
														  	<s:textfield cssStyle="text-align:right"cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value="%%"/>
														</c:if>
													</c:if>
													</c:if>
													
													<c:if test="${(inputParameterName == 'reportParameter_Documents')}">
														<s:textarea  id="${inputParameterName}" name="${inputParameterName}" value="" readonly="" cols="30" label="6" />
													</c:if>
													
													<c:if test="${(inputParameterName == 'reportParameter_Print Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Print With Header','Print Without Header'}" cssStyle="width:115px"  value=" "/>
															</c:if>	
															<c:if test="${(inputParameterName == 'reportParameter_Print Options')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="%{printOptions}" cssStyle="width:175px"  value="%{printOptionsValue}"/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Print_Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'No header / footer','UGRN - ACF'}" cssStyle="width:175px"  value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Print_Options')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'No header / footer','Cadogan Tate'}" cssStyle="width:175px"  value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Print_Method')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'No header / footer','UGRN','Voerman'}" cssStyle="width:175px"  value=" "/>
															</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Send Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Email','Print'}" cssStyle="width:115px"  value=" "/>
													</c:if>	
													<c:if test="${(inputParameterName == 'reportParameter_Value Option')}">
			    								 				<s:select cssClass="list-menu" name="${inputParameterName}" list="{'Estimate','Revised'}" cssStyle="width:115px"  value=" "/>
															</c:if>
															<c:if test="${(inputParameterName == 'reportParameter_Partner Name')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{partnerCodeArrayList}" cssStyle="width:250px" headerKey="" headerValue="" value="${partnerCodeArrayList}"/>
			    											</c:if>
															
															<c:if test="${(inputParameterName == 'reportParameter_Partner Code')}" >
			    											<s:textfield cssClass="input-text" id="${inputParameterName}" name="reportParameter_Partner Code" size="15"/>
			    										   <img class="openpopup" width="17" height="20" style="vertical-align:bottom;" onclick="window.open('partnersPopup.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=reportParameter_Partner Code','billtocode','width=950,height=650');"  src="<c:url value='/images/open-popup.gif'/>" />
															<s:hidden name="secondDescription" />
															<s:hidden name="thirdDescription" />
															<s:hidden name="fourthDescription" />
															<s:hidden name="fifthDescription" />
															<s:hidden name="sixthDescription" />
															<s:hidden name="firstDescription" />
															<s:hidden name="description" />
			    				                     	</c:if>
													<c:if test="${(inputParameterName == 'reportParameter_Predefined Text')}" >
			    												<s:select cssClass="list-menu" name="${inputParameterName}" list="%{preDefTxt}" cssStyle="width:105px" headerKey="" headerValue="" value="${preDefTxt}"/>
			    											</c:if>
			    								 			
													<c:if test="${(inputParameterName == 'reportParameter_Remark')}">
			    								 				<s:textarea cssClass="list-menu" name="${inputParameterName}" cssStyle="width:300px; height:60px" value=" "/>
													</c:if>
																							      													
											  		<c:if test="${inputParameterName != 'reportParameter_Service Order Number' && inputParameterName != 'reportParameter_Claim Number' && inputParameterName !='reportParameter_Book Number' && inputParameterName !='reportParameter_Work Ticket Number' && inputParameterName !='reportParameter_Customer File Number' && inputParameterName !='reportParameter_Documents' && inputParameterName !='reportParameter_Invoice Number' && inputParameterName !='reportParameter_Regnum' && inputParameterName !='reportParameter_Print Option' && inputParameterName !='reportParameter_Print Options' && inputParameterName !='reportParameter_Print_Option' && inputParameterName !='reportParameter_Print_Options' && inputParameterName !='reportParameter_Print_Method' && inputParameterName !='reportParameter_Send Option' && inputParameterName != 'reportParameter_Value Option' && inputParameterName != 'reportParameter_Partner Name' && inputParameterName != 'reportParameter_Partner Code' && inputParameterName !='reportParameter_Predefined Text' && inputParameterName !='reportParameter_Remark' && inputParameterName !='reportParameter_Quotation File Number'}">
												  		<c:if test="${(inputParameterName == 'reportParameter_Corporate ID')}">
												  			<s:textfield cssStyle="text-align:right" cssClass="input-textUpper" id="${inputParameterName}" name="${inputParameterName}" value="<%=sessionCorpID %>" readonly="true" size="15" />
												  		</c:if>
												  		<c:if test="${!(inputParameterName == 'reportParameter_Corporate ID')}">
													  		<s:textfield  cssStyle="text-align:right"cssClass="input-text" id="${inputParameterName}" name="${inputParameterName}" size="15" value=""/>
													  	</c:if>
													</c:if>
											  	</c:if>
											  	</c:if>
											 </td>
											</c:if>
										</tr> 
									  <tr><td class="listwhitetext" style="height:10px"></td></tr>  
						           </c:forEach>
						           </tbody>
						           </table>
						           <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
									<tr> 
									<td width="45"></td><td class="listwhitetext" width="94" ><b>Preview Form </b><font color="red" size="2">*</font>&nbsp;:&nbsp;</td>
									<td colspan="0" class="listwhitetext" style="font-size:11px;">
									<c:if test="${(reports.csv == true)}">
									<c:if test="${typedoc=='csv'}">
									<input type="radio" name="typedoc" checked="checked" id="typedoc" value="csv" />
									</c:if>
									<c:if test="${typedoc!='csv'}">
									<input type="radio" name="typedoc" id="typedoc" value="csv" />
									</c:if>
									<img alt="CSV File" src="<c:url value='/images/csv1.gif'/>" onclick="return validateFieldsTT('CSV');"/>CSV
									</c:if>
									<c:if test="${(reports.xls == true)}">
									<c:if test="${typedoc=='xls'}">
									<input type="radio" name="typedoc" checked="checked" id="typedoc" value="xls" />
									</c:if>
									<c:if test="${typedoc!='xls'}">
									<input type="radio" name="typedoc" id="typedoc" value="xls" />
									</c:if>
									<img alt="XLS File" src="<c:url value='/images/xls1.gif'/>" onclick="return validateFieldsTT('XLS');"/>XLS
									</c:if>
									<c:if test="${(reports.html == true)}">
									<c:if test="${typedoc=='htm'}">
									<input type="radio" name="typedoc" checked="checked" id="typedoc" value="htm" />
									</c:if>
									<c:if test="${typedoc!='htm'}">
									<input type="radio" name="typedoc" id="typedoc" value="htm" />
									</c:if>
									<img alt="HTML File" src="<c:url value='/images/html1.gif'/>" onclick="return validateFieldsTT('HTML');"/>HTML
									</c:if>
									<c:if test="${(reports.pdf == true)}">
									<c:if test="${typedoc=='pdf'}">
									<input type="radio" name="typedoc" checked="checked" id="typedoc" value="pdf" />
									</c:if>
									<c:if test="${typedoc!='pdf'}">
									<input type="radio" name="typedoc" id="typedoc" value="pdf"/>
									</c:if>
									<img alt="PDF File" src="<c:url value='/images/pdf1.gif'/>" onclick="return validateFieldsTT('PDF');"/>PDF
									</c:if>
									<c:if test="${(reports.rtf == true)}">
									<c:if test="${typedoc=='doc'}">
									<input type="radio" name="typedoc" checked="checked" id="typedoc" value="doc" />
									</c:if>
									<c:if test="${typedoc!='doc'}">
									<input type="radio" name="typedoc" id="typedoc" value="doc" />
									</c:if>
									<img alt="RTF File" src="<c:url value='/images/doc.gif'/>" onclick="return validateFieldsTT('RTF');"/>DOC
									</c:if>
									<c:if test="${(reports.extract == true)}">
									<c:if test="${typedoc=='ext'}">
									<input type="radio" name="typedoc" checked="checked" id="typedoc" value="ext" />
									</c:if>
									<c:if test="${typedoc!='ext'}">
									<input type="radio" name="typedoc" id="typedoc" value="ext" />
									</c:if>
									<img alt="EXTRACT" src="<c:url value='/images/extract-para.png'/>" onclick="return validateFieldsTT('EXTRACT');"/>EXTRACT
									</c:if>
									</td>
									<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.invoiceAttachment">
									<td class="listwhitetext"><s:checkbox id="includeInvoiceAttachment" cssStyle="vertical-align:sub;" name="includeInvoiceAttachment" onclick="" />Include&nbsp;invoice&nbsp;attachments</td>
									</configByCorp:fieldVisibility>	
									</tr>
							</table>
						</td>	
		  			</tr>
		  		</tbody>
			</table>
			<div id="docs" class="mainDetailTable" style="width:510px;">
				<table id="docs" class="colored_bg" cellspacing="0" cellpadding="0" border="0" style="width:510px;margin: 0px;padding: 0px;">
				<tbody>				
					<tr><td colspan="4"><div class="subcontent-tab">Enter information below to file this email in the File Cabinet</div></td></tr>  
									
									<tr ><td class="listwhitetext" style="height:20px;"></td></tr>  
									<tr>
										<td align="right" class="listwhitetext">Document&nbsp;Type&nbsp;:&nbsp;</td>
										<td align="left" width="184px"><s:select name="docFileType" id="docFileType" cssClass="list-menu" list="%{docsList}" headerKey="" headerValue="" cssStyle="width:155px" onclick="setDescriptionDuplicate(this),documentMap(this);" onchange="documentAccessControlAjax();"/></td>
										<td align="left" class="listwhitetext" width="">Map&nbsp;&nbsp;<s:select name="mapFolder" cssClass="list-menu" list="%{mapList}" headerKey="" headerValue="" cssStyle="width:70px" /></td>
									</tr>
									<tr>
										<td align="left" class="listwhitetext" style="height:10px"></td>
									</tr>
									<tr>
										<td align="right" class="listwhitetext" style="width:130px" >Description : &nbsp;</td>
										<td align="left" colspan="2"><s:textfield name="myFile.description" value="" cssClass="input-text" size="51"/></td>
										
									</tr>
									<tr>
									
							<td colspan="10" style="padding-left: 44px; padding-top: 5px;">
								<table class="detailTabLabelFC-PO" border="0" cellspacing="0" cellpadding="0" style="width:365px">
                                    <tr>
                                    <td colspan="5">
									 <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
                                    <tr>
                                    <td class="CFCtrl_left"></td>
                                    <td NOWRAP class="CFCtrl_right">&nbsp;&nbsp;Portal Access Control </td>
                                    </tr>
                                    </table>                                  
                                    </td>
                                    </tr>
                                    <tr><td height="10"></td></tr>									
										<tr>
											<td width="13"></td><configByCorp:fieldVisibility componentId="component.standard.cPortalActivation">	<td align="left" class="listwhitetextFC" width="52px">
											<s:checkbox id="isCportal" name="isCportal" onclick="checkSecureDoc(this);" />&nbsp;Customer
											</td></configByCorp:fieldVisibility>
											<configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">
										    <td class="listwhitetextFC" align="left" width="52px">
										    <s:checkbox id="isAccportal" name="isAccportal" onclick="checkSecureDoc(this);" />&nbsp;Account
										    </td>
										    </configByCorp:fieldVisibility>
										</tr>
										 <tr><td height="5"></td></tr>		
										<tr>
											<td width=""></td><td class="listwhitetextFC" align="left" width="52px">
											<s:checkbox id="isPartnerPortal" name="isPartnerPortal" onclick="checkSecureDoc(this);" />&nbsp;Partner
											</td>	

											<td class="listwhitetextFC" align="left" width="52px">
											<s:checkbox id="isServiceProvider" name="isServiceProvider" onclick="checkSecureDoc(this);" />Service&nbsp;Provider
											</td>	
										</tr>
										<tr><td height="10"></td></tr>		
									</table>
								
								
								<table class="detailTabLabelFC-NW" border="0" cellspacing="0" cellpadding="0" style="width:365px">
                                    <tr>
                                    <td colspan="5">
									 <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
                                    <tr>
                                    <td class="CFCtrl_left"></td>
                                    <td NOWRAP class="CFCtrl_right">&nbsp;&nbsp;Network Access Control </td>
                                    </tr>
                                    </table>                                  
                                    </td>
                                    </tr>
                                    <tr><td height="7"></td></tr>					
								
										<tr>
											
											<td height="32" style="!width:10px;"></td><td align="left" class="listwhitetextFC" width="220px" colspan="4">
											<s:checkbox id="checkbox2" name="checkbox2" onclick="checkAll(this);"/><b>Check/Uncheck All</b></td> 
											<s:hidden name="isPaymentStatus" id="isPaymentStatus" />               
										</tr>
										<tr><td height="1"></td></tr>		
										<tr>
											
											<td width="32" style="!width:10px;"></td><td align="left" class="listwhitetextFC" width="120px">
											<s:checkbox name="isBookingAgent"  id="isBookingAgent" onchange="checkCheckbox2();" onclick="checkSecureDoc(this);" />&nbsp;Booking Agent
											</td>
											
											<td width="5"></td><td align="left" class="listwhitetextFC" width="">
											<s:checkbox name="isNetworkAgent"  id="isNetworkAgent" onchange="checkCheckbox2();" onclick="checkSecureDoc(this);" />&nbsp;Network Agent
											</td>
											
										</tr>
										<tr><td height="5"></td></tr>		
										<tr>
											
											<td width="32" style="!width:10px;"></td><td class="listwhitetextFC" align="left" width="">
											<s:checkbox name="isOriginAgent"  id="isOriginAgent" onchange="checkCheckbox2();" onclick="checkSecureDoc(this);" />&nbsp;Origin Agent
											</td>
												
											
											<td width="5"></td><td align="left" class="listwhitetextFC" width="">
											<s:checkbox name="isSubOriginAgent" id="isSubOriginAgent" onchange="checkCheckbox2();" onclick="checkSecureDoc(this);" />&nbsp;Sub Origin Agent
											</td>
											
											
										</tr>
										<tr><td height="5"></td></tr>		
										<tr>
											<td width="32" style="!width:10px;"></td><td class="listwhitetextFC" align="left" width="">
											<s:checkbox name="isDestAgent"  id="isDestAgent" onchange="checkCheckbox2();" onclick="checkSecureDoc(this);" />&nbsp;Destination Agent
											</td>
											<td width="5"></td><td align="left" class="listwhitetextFC" width="">
											<s:checkbox name="isSubDestAgent"  id="isSubDestAgent" onchange="checkCheckbox2();" onclick="checkSecureDoc(this);" />&nbsp;Sub Destination Agent
											</td>
										</tr>
										<tr><td height="10"></td></tr>		
									</table>								
							</td>
						
									</tr>
									<tr>
										<td align="left" class="listwhitetext" style="height:10px"></td>
									</tr>
									<tr>
										<td align="left" class="listwhitetext" style="height:10px"></td>
										<td align="left" style="padding-right:20px;" colspan="3" ><input type="button"" name="send" value="Send" style="width:60px;" class="cssbutton"  onclick="return validateFields();"/>
										<s:submit name="cancel" value="Cancel"  cssClass="cssbutton" cssStyle="width:60px; height:25px;margin-left:10px;" onclick="return refreshParent();"/>
										</td>										
									</tr>
									<tr>
										<td align="left" class="listwhitetext" style="height:10px"></td>
									</tr>
								</tbody>
					</table> 		
			</div>	
	<s:hidden name="fileType" />
	<div id="mydiv" style="position:absolute"></div>
</s:form>
<script language="javascript" type="text/javascript">
function fillUserEmail(target){
	try{
		if(target.checked==true){ 
		var newIds=document.forms['reportForm'].elements['recipientTo'].value;
		 if(newIds==''){   
		     newIds=target.value;
		 }else{
		     newIds=newIds + ',' + target.value;
		 }
			document.forms['reportForm'].elements['recipientTo'].value=newIds;
		}else{
			newIds=document.forms['reportForm'].elements['recipientTo'].value;
		    if(newIds.indexOf(target.value)>-1){
		        newIds=newIds.replace(target.value,"");
		        newIds=newIds.replace(",,",",");
		    }
		    var len=newIds.length-1;
		    if(len==newIds.lastIndexOf(",")){
		    newIds=newIds.substring(0,len);
		    }
		    if(newIds.indexOf(",")==0){
		    newIds=newIds.substring(1,newIds.length);
		    }
		    document.forms['reportForm'].elements['recipientTo'].value=newIds;  			
		}
	}catch(e){}	
}
function fillUserEmail1(target){
		try{
			if(target.checked==true){ 
			var newIds=document.forms['reportForm'].elements['recipientCC'].value;
			 if(newIds==''){   
			     newIds=target.value;
			 }else{
			     newIds=newIds + ',' + target.value;
			 }
				document.forms['reportForm'].elements['recipientCC'].value=newIds;
			}else{
				newIds=document.forms['reportForm'].elements['recipientCC'].value;
			    if(newIds.indexOf(target.value)>-1){
			        newIds=newIds.replace(target.value,"");
			        newIds=newIds.replace(",,",",");
			    }
			    var len=newIds.length-1;
			    if(len==newIds.lastIndexOf(",")){
			    newIds=newIds.substring(0,len);
			    }
			    if(newIds.indexOf(",")==0){
			    newIds=newIds.substring(1,newIds.length);
			    }
			    document.forms['reportForm'].elements['recipientCC'].value=newIds;  			
			}
		}catch(e){}			
}

</script>
<script type="text/javascript">
try{
<c:if test="${hitFlag=='1'}"> 
if(document.forms['reportForm'].elements['formFieldName'].value!='' && document.forms['reportForm'].elements['formValue'].value !=''){
	var urlcheck = encodeURI(window.opener.location);
	if(urlcheck.indexOf("searchOurInviceList")>-1){		
		window.close();
	}else{
		window.opener.location.reload(true);
	}
}
window.close();
</c:if>	
}
catch(e){}
	try
	{
	document.getElementById("soList").style.display = 'none';
	displayStatusReason();
	}
	catch(e){}
	try{
	var typedoc1=document.getElementsByName('typedoc');
	if(typedoc1 != null && typedoc1 != undefined && typedoc1.length > 0){
		var flagCheck=false;
		for(var i=0; i<typedoc1.length; i++){
			if(typedoc1[i].value == "pdf"){
				typedoc1[i].checked=true;
				flagCheck=true;
			}
		}
		if(!flagCheck){
			typedoc1[0].checked=true;
		}	
	}
	}catch(e){}
</script>
</body>
<script type="text/javascript">   
    Form.focusFirstElement($("reportForm"));  
</script>
 <script type="text/javascript">
 	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
 </script>