package com.trilasoft.app.webapp.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import org.apache.struts2.ServletActionContext;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import com.trilasoft.app.model.ExtractedFileLog;
import com.trilasoft.app.service.ExtractedFileLogManager;
import com.trilasoft.app.service.RefMasterManager;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.SQLExtract;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.SQLExtractManager;

public class SQLExtractAction extends BaseAction{
	
	private Long id;
	private List extractList;
	private List corpIDList;
	private String sessionCorpID;
	private SQLExtract sqlExtract;
	private String btn;
	private String fileName;
	private String corpID;
	private SQLExtractManager sqlExtractManager;
	private String whereClause1;
	private String whereClause2;
	private String whereClause3;
	private String whereClause4;
	private String whereClause5;
	private Boolean isdateWhere1;
	private Boolean isdateWhere2;
	private Boolean isdateWhere3;
	private Boolean isdateWhere4;
	private Boolean isdateWhere5;
	private String sqlText;
	private String displayLabel1;
	private String displayLabel2;
	private String displayLabel3;
	private String displayLabel4;
	private String displayLabel5;
	private String defaultCondition1;
	private String defaultCondition2;
	private String defaultCondition3;
	private String defaultCondition4;
	private String defaultCondition5;
	private String mailStatus;
	private String emailTo;
	private CompanyManager companyManager;
	private Company company;
	private UserManager userManager;
    public  Map<String, String> perWeekSchedulerList=new TreeMap();
    public  Map<Integer, Integer> perMonthSchedulerList=new TreeMap<Integer, Integer>();
	
	static final Logger logger = Logger.getLogger(SQLExtractAction.class);
	public String getDefaultCondition1() {
		return defaultCondition1;
	}

	public void setDefaultCondition1(String defaultCondition1) {
		this.defaultCondition1 = defaultCondition1;
	}

	public String getDefaultCondition2() {
		return defaultCondition2;
	}

	public void setDefaultCondition2(String defaultCondition2) {
		this.defaultCondition2 = defaultCondition2;
	}

	public String getDefaultCondition3() {
		return defaultCondition3;
	}

	public void setDefaultCondition3(String defaultCondition3) {
		this.defaultCondition3 = defaultCondition3;
	}

	public String getDefaultCondition4() {
		return defaultCondition4;
	}

	public void setDefaultCondition4(String defaultCondition4) {
		this.defaultCondition4 = defaultCondition4;
	}

	public String getDefaultCondition5() {
		return defaultCondition5;
	}

	public void setDefaultCondition5(String defaultCondition5) {
		this.defaultCondition5 = defaultCondition5;
	}

	public String getDisplayLabel1() {
		return displayLabel1;
	}

	public void setDisplayLabel1(String displayLabel1) {
		this.displayLabel1 = displayLabel1;
	}

	public String getDisplayLabel2() {
		return displayLabel2;
	}

	public void setDisplayLabel2(String displayLabel2) {
		this.displayLabel2 = displayLabel2;
	}

	public String getDisplayLabel3() {
		return displayLabel3;
	}

	public void setDisplayLabel3(String displayLabel3) {
		this.displayLabel3 = displayLabel3;
	}

	public String getDisplayLabel4() {
		return displayLabel4;
	}

	public void setDisplayLabel4(String displayLabel4) {
		this.displayLabel4 = displayLabel4;
	}

	public String getDisplayLabel5() {
		return displayLabel5;
	}

	public void setDisplayLabel5(String displayLabel5) {
		this.displayLabel5 = displayLabel5;
	}

	public String getSqlText() {
		return sqlText;
	}

	public void setSqlText(String sqlText) {
		this.sqlText = sqlText;
	}

	public Boolean getIsdateWhere1() {
		return isdateWhere1;
	}

	public void setIsdateWhere1(Boolean isdateWhere1) {
		this.isdateWhere1 = isdateWhere1;
	}

	public Boolean getIsdateWhere2() {
		return isdateWhere2;
	}

	public void setIsdateWhere2(Boolean isdateWhere2) {
		this.isdateWhere2 = isdateWhere2;
	}

	public Boolean getIsdateWhere3() {
		return isdateWhere3;
	}

	public void setIsdateWhere3(Boolean isdateWhere3) {
		this.isdateWhere3 = isdateWhere3;
	}

	public Boolean getIsdateWhere4() {
		return isdateWhere4;
	}

	public void setIsdateWhere4(Boolean isdateWhere4) {
		this.isdateWhere4 = isdateWhere4;
	}

	public Boolean getIsdateWhere5() {
		return isdateWhere5;
	}

	public void setIsdateWhere5(Boolean isdateWhere5) {
		this.isdateWhere5 = isdateWhere5;
	}

	public String getWhereClause1() {
		return whereClause1;
	}

	public void setWhereClause1(String whereClause1) {
		this.whereClause1 = whereClause1;
	}

	public String getWhereClause2() {
		return whereClause2;
	}

	public void setWhereClause2(String whereClause2) {
		this.whereClause2 = whereClause2;
	}

	public String getWhereClause3() {
		return whereClause3;
	}

	public void setWhereClause3(String whereClause3) {
		this.whereClause3 = whereClause3;
	}

	public String getWhereClause4() {
		return whereClause4;
	}

	public void setWhereClause4(String whereClause4) {
		this.whereClause4 = whereClause4;
	}

	public String getWhereClause5() {
		return whereClause5;
	}

	public void setWhereClause5(String whereClause5) {
		this.whereClause5 = whereClause5;
	}

	public SQLExtractAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	
	public String list(){
		extractList = sqlExtractManager.getAllListForTSFT();
		return SUCCESS;
	}
	@SkipValidation	
	public String listEachUser(){
		extractList = sqlExtractManager.getAllUserList(sessionCorpID);
		return SUCCESS;
	}
	@SkipValidation
	public String listUser(){
		company =  companyManager.findByCorpID(sessionCorpID).get(0);
		extractList = sqlExtractManager.getAllList(sessionCorpID);
		return SUCCESS;
	}
	
	@SkipValidation
	public String listUserSearch(){
		extractList = sqlExtractManager.getSqlExtraxtSearch(sqlExtract.getFileName(), sqlExtract.getDescription(),sessionCorpID);
		return SUCCESS;
	}
	
	private List conditionList;
	public String edit() {
		getComboList();
		corpIDList = sqlExtractManager.getAllCorpId();
		if (id != null) {
			
			List sqlExtract1=sqlExtractManager.getSqlExtractByID(id,sessionCorpID);
			sqlExtract= (SQLExtract)sqlExtract1.get(0);
			//sqlExtract = sqlExtractManager.get(id);
		} else {
			sqlExtract = new SQLExtract();
			sqlExtract.setCorpID(sessionCorpID);
			
			sqlExtract.setCreatedOn(new Date());
			sqlExtract.setUpdatedOn(new Date());
		}
		return SUCCESS;
	}
	String queryString = "";
	String where ="where";
	String and   ="and";
	String like=  "like";
	String orderby="order By";
	String groupby ="group by";
	 
	private List sqlExtractUname;
	@SkipValidation
	public String save() throws Exception {
		getComboList();
		corpIDList = sqlExtractManager.getAllCorpId();
		if(sqlExtract.getWhereClause1().trim().equalsIgnoreCase("")  || sqlExtract.getWhereClause1() == null)
			{
				queryString = sqlExtract.getSqlText();
			}
		
		else if(sqlExtract.getWhereClause2().trim().equalsIgnoreCase("") || sqlExtract.getWhereClause2()== null)
		{
			
				if(sqlExtract.getSqlText().contains("where"))
				{
					queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat("=").concat("''");	
				}
				else
				{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat("=").concat("''");	
				}
		}

		else if(sqlExtract.getWhereClause3().trim().equalsIgnoreCase("") || sqlExtract.getWhereClause3()== null)
		{
			
				if(sqlExtract.getSqlText().contains("where"))
				{
					queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat("=").concat("''");
				}
				else
				{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat("=").concat("''");
				}
		}
		
		else if(sqlExtract.getWhereClause4().trim().equalsIgnoreCase("") || sqlExtract.getWhereClause4() == null)
			{
				if(sqlExtract.getSqlText().contains("where"))
				{
					queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat("=").concat("''");
								
				}
				else
				{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat("=").concat("''");
				}
			}
	else if(sqlExtract.getWhereClause5().trim().equalsIgnoreCase("") || sqlExtract.getWhereClause5() == null)
			{
				if(sqlExtract.getSqlText().contains("where"))
				{
					queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat("=").concat("''");
				}
				else
				{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat("=").concat("''");
				}
			}
	else{
		if(sqlExtract.getSqlText().contains("where"))
			{	
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat("=").concat("''");
							
			}
			else
			{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat("=").concat("''").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat("=").concat("''");
				
			}
		}


			/*if(!sqlExtract.getSqlText().contains("join"))
			{
	  							if(sessionCorpID.equalsIgnoreCase("TSFT"))
								{
									queryString = queryString;
								}
								else
								{
									if(queryString.contains("where")){
									
										String CorpID ="'"+sessionCorpID+"'";
										queryString = queryString.concat(" ").concat(and).concat(" ").concat("corpid").concat(" = ").concat(CorpID);
									}
									else
									{
									String CorpID ="'"+sessionCorpID+"'";
									queryString = queryString.concat(" ").concat(where).concat(" ").concat("corpid").concat(" = ").concat(CorpID);
									}
								}
			}*/
					if(sqlExtract.getOrderBy().trim().equalsIgnoreCase("")|| sqlExtract.getOrderBy() == null)
					{
						if(sqlExtract.getGroupBy().trim().equalsIgnoreCase("")|| sqlExtract.getGroupBy()== null)
							{
								queryString=queryString;
							}
							else
							{
							queryString=queryString.concat(" ").concat(groupby).concat(" ").concat(sqlExtract.getGroupBy());
	
							}
					}
					else if (sqlExtract.getGroupBy().trim().equalsIgnoreCase("")|| sqlExtract.getGroupBy()== null)
					{
					
							queryString=queryString.concat(" ").concat(orderby).concat(" ").concat(sqlExtract.getOrderBy());
	
					}
					else{
							queryString=queryString.concat(" ").concat(groupby).concat(" ").concat(sqlExtract.getGroupBy()).concat(" ").concat(orderby).concat(" ").concat(sqlExtract.getOrderBy());
	
						}
		
		//System.out.println(queryString);
		String executeTest = sqlExtractManager.testSQLQuery(queryString);
		
		if(btn.equalsIgnoreCase("Test Query")){
			
			if(executeTest == null){
				sqlExtract.setTested("Tested");
				sqlExtract= sqlExtractManager.save(sqlExtract);
				saveMessage("Query has been executed successfully.");
			}else{
				String sqlError = "";
				try{
					sqlError = executeTest;
				}catch(Exception ex){
					ex.printStackTrace();
					StringBuffer buffer = new StringBuffer();
					buffer.append(executeTest);
					buffer.append(" ");
					sqlError = buffer.toString();
				}
				sqlExtract.setTested("Error");
				saveMessage("Query cannot be executed. ");
				sqlExtract= sqlExtractManager.save(sqlExtract);
			}
			return INPUT;	
		}else{
			if(executeTest==null){
					sqlExtract.setTested("Tested");
					
				}else{
					String sqlError = "";
					try{
						sqlError = executeTest;
					}catch(Exception ex){
						ex.printStackTrace();
						StringBuffer buffer = new StringBuffer();
						buffer.append(executeTest);
						buffer.append(" ");
						sqlError = buffer.toString();
					}
					sqlExtract.setTested("Error");
					}
				
			boolean isNew = (sqlExtract.getId() == null); 
			if(isNew){
				sqlExtract.setCreatedOn(new Date());
			}
		      
			sqlExtract.setUpdatedOn(new Date());
			sqlExtract.setUpdatedBy(getRequest().getRemoteUser());
			sqlExtractUname=sqlExtractManager.getUniquename(sqlExtract.getFileName());
			int count =0;
			if(isNew){
					
				
					Iterator it= sqlExtractUname.iterator();
						while(it.hasNext()){
							Object  row =(Object)it.next();	
							count =count+1;
						}
				
					//System.out.println(count);
						if(count==0)
						{
							sqlExtract= sqlExtractManager.save(sqlExtract);
							saveMessage("Sql Extract has been successfully added.");

							if(sessionCorpID.equalsIgnoreCase("TSFT"))
							{
							return SUCCESS;
							}
							else
							{
							return "SUCCESS1";
							}

						}
							else
							{
								//System.out.println("count>>>>>>>>>"+count);
								saveMessage("fileName already existed,choose another fileName.");	
								return INPUT;
							}
			}
			else
			{
				sqlExtract= sqlExtractManager.save(sqlExtract);
				saveMessage("Sql Extract has been successfully updated.");
				if(sessionCorpID.equalsIgnoreCase("TSFT"))
				{
				return SUCCESS;
				}
				else
				{
				return "SUCCESS1";
				}
			}
			
		}
	}
	
	@SkipValidation
	public String Search()
	{
		
		extractList=sqlExtractManager.getSearch(fileName,corpID);
		
		return SUCCESS;
	}
	
	public String delete() {
		String returnString="SUCCESS";
		sqlExtractManager.deleteSqlExtract(id);
		saveMessage(getText("sqlExtract.deleted"));
		if(sessionCorpID.equalsIgnoreCase("TSFT"))
		{
			extractList = sqlExtractManager.getAll();
		}
		else
		{
			extractList = sqlExtractManager.getAllUserList(sessionCorpID);
			returnString="INPUT1";
		}
		return returnString;
	}

	
	private String whereClause1Value;
	private String whereClause2Value;
	private String whereClause3Value;
	private String whereClause4Value;
	private String whereClause5Value;
	private String orderByValue;
	private String groupByValue;
	private String orderBy;
	private String groupBy;
	private String condition1;
	private String condition2;
	private String condition3;
	private String condition4;
	private String condition5;
	private String condition6;
	private String hitFlag;
	private Date payAccDateToFormat;
	private boolean extractFileAudit=false;
	private ExtractedFileLogManager extractedFileLogManager;
	private ExtractedFileLog extractedFileLog;
	public Date getPayAccDateToFormat() {
		return payAccDateToFormat;
	}

	public void setPayAccDateToFormat(Date payAccDateToFormat) {
		this.payAccDateToFormat = payAccDateToFormat;
	}

	@SkipValidation
	public void extractSQLData() throws IOException, ParseException{
		String header = new String();
		company=companyManager.findByCorpID(sessionCorpID).get(0);
	    if(company!=null){
	      if(company.getExtractedFileAudit() !=null && company.getExtractedFileAudit()){
	    	  extractFileAudit = true;
	  		}
	    }
		List sqlExtract1=sqlExtractManager.getSqlExtractByID(id,sessionCorpID);
		sqlExtract= (SQLExtract)sqlExtract1.get(0);
		
		/*defaultCondition1 = sqlExtract.getDefaultCondition1();
		defaultCondition2 = sqlExtract.getDefaultCondition2();
		defaultCondition3 = sqlExtract.getDefaultCondition3();
		defaultCondition4 = sqlExtract.getDefaultCondition4();
		defaultCondition5 = sqlExtract.getDefaultCondition5();*/
		
		if(sqlExtract.getIsdateWhere1()==true && !whereClause1Value.equals("")){
			try {
					SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
		              Date du = new Date();
		              du = df.parse(whereClause1Value);
		              df = new SimpleDateFormat("yyyy-MM-dd");
		              whereClause1Value = df.format(du);
		             
			}catch (Exception e) {
					e.printStackTrace();
				}
		}
		if(sqlExtract.getIsdateWhere2()==true && !whereClause2Value.equals("")){
			try {
					SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
		              Date du = new Date();
		              du = df.parse(whereClause2Value);
		              df = new SimpleDateFormat("yyyy-MM-dd");
		              whereClause2Value = df.format(du);
		            
			 	}catch (Exception e) {
					e.printStackTrace();
				}
		}
		if(sqlExtract.getIsdateWhere3()==true && !whereClause3Value.equals("")){
			try {
					SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
		              Date du = new Date();
		              du = df.parse(whereClause3Value);
		              df = new SimpleDateFormat("yyyy-MM-dd");
		              whereClause3Value = df.format(du);
		             
			 	}catch (Exception e) {
					e.printStackTrace();
				}
		}
		if(sqlExtract.getIsdateWhere4()==true && !whereClause4Value.equals("")){
			try {
					SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
		              Date du = new Date();
		              du = df.parse(whereClause4Value);
		              df = new SimpleDateFormat("yyyy-MM-dd");
		              whereClause4Value = df.format(du);
		             
			 	}catch (Exception e) {
					e.printStackTrace();
				}
		}
		if(sqlExtract.getIsdateWhere5()==true && !whereClause5Value.equals("")){
			try {
					SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
		              Date du = new Date();
		              du = df.parse(whereClause5Value);
		              df = new SimpleDateFormat("yyyy-MM-dd");
		              whereClause5Value = df.format(du);
		            }catch (Exception e) {
					e.printStackTrace();
				}
		}
		if(defaultCondition1!=null && defaultCondition1.trim().equalsIgnoreCase("Like")){
			whereClause1Value=whereClause1Value.replaceAll("'", "");
			whereClause1Value="%"+whereClause1Value+"%";
		}
		if(defaultCondition2!=null && defaultCondition2.trim().equalsIgnoreCase("Like")){
			whereClause2Value=whereClause2Value.replaceAll("'", "");
			whereClause2Value="%"+whereClause2Value+"%";
		}
		if(defaultCondition3!=null && defaultCondition3.trim().equalsIgnoreCase("Like")){
			whereClause3Value=whereClause3Value.replaceAll("'", "");
			whereClause3Value="%"+whereClause3Value+"%";
		}
		if(defaultCondition4!=null && defaultCondition4.trim().equalsIgnoreCase("Like")){
			whereClause4Value=whereClause4Value.replaceAll("'", "");
			whereClause4Value="%"+whereClause4Value+"%";
		}
		if(defaultCondition5!=null && defaultCondition5.trim().equalsIgnoreCase("Like")){
			whereClause5Value=whereClause5Value.replaceAll("'", "");
			whereClause5Value="%"+whereClause5Value+"%";
		}	
	if(sqlExtract.getWhereClause1() == null || sqlExtract.getWhereClause1().trim().equalsIgnoreCase("")   )
			{
				queryString = sqlExtract.getSqlText();
			}
		
		else if(sqlExtract.getWhereClause2()== null||  sqlExtract.getWhereClause2().trim().equalsIgnoreCase("") )
		{
			if(whereClause1Value.equalsIgnoreCase("'null'")){
				whereClause1Value=whereClause1Value.replaceAll("'", "");
			}
			if(whereClause1Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere1()))
			{
				whereClause1Value= "null";	
				defaultCondition1= " is ";
			}			
				if(sqlExtract.getSqlText().toLowerCase().contains("where"))
				{
					if(whereClause1Value.equalsIgnoreCase("null"))
					{
						queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value);
					}else{
						queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'");					
					}
				}
				else
				{
					if(whereClause1Value.equalsIgnoreCase("null"))
					{
						queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value);
					}else{
						queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'");					
					}
				}
					
		}

		else if(sqlExtract.getWhereClause3()== null || sqlExtract.getWhereClause3().trim().equalsIgnoreCase(""))
		{
			if(whereClause1Value.equalsIgnoreCase("'null'")){
				whereClause1Value.replace("'","" );
			}
			if(whereClause2Value.equalsIgnoreCase("'null'")){
				whereClause2Value.replace("'","" );
			}
			if(whereClause1Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere1()))
			{
				whereClause1Value= "null";	
				defaultCondition1= " is ";
			}
			if(whereClause2Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere2()))
			{
				whereClause2Value= "null";	
				defaultCondition2= " is ";
			}				
			if(sqlExtract.getSqlText().toLowerCase().contains("where"))
			{
				if((whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'");
				}
				if((!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value);
				}
				if((whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value);
				}
				if((!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'");
				}
			}
			else
			{
				if((whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'");
				}
				if((!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value);
				}
				if((whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value);
				}
				if((!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'");
				}
			}
		}
					
		
		
		else if( sqlExtract.getWhereClause4() == null || sqlExtract.getWhereClause4().trim().equalsIgnoreCase(""))
				
			
			{
			if(whereClause1Value.equalsIgnoreCase("'null'")){
				whereClause1Value.replace("'","" );
			}
			if(whereClause2Value.equalsIgnoreCase("'null'")){
				whereClause2Value.replace("'","" );
			}
			if(whereClause3Value.equalsIgnoreCase("'null'")){
				whereClause3Value.replace("'","" );
			}

			if(whereClause1Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere1()))
			{
				whereClause1Value= "null";	
				defaultCondition1= " is ";
			}
			if(whereClause2Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere2()))
			{
				whereClause2Value= "null";	
				defaultCondition2= " is ";
			}				
			if(whereClause3Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere3()))
			{
				whereClause3Value= "null";	
				defaultCondition3= " is ";
			}				

			if(sqlExtract.getSqlText().toLowerCase().contains("where"))
			{				
				if((whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value);				
				}
				if((whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'");								
				}
				if((whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value);				
				}
				if((whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'");				
				}
				if((!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value);		
				}
				if((!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'");
				}
				if((!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value);
				}
				if((!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'");
				}
			}
			else
			{
				if((whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value);				
				}
				if((whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'");								
				}
				if((whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value);				
				}
				if((whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'");				
				}
				if((!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value);		
				}
				if((!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'");
				}
				if((!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value);
				}
				if((!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'");
				}

			}
		}
						
	
		
		else if( sqlExtract.getWhereClause5() == null || sqlExtract.getWhereClause5().trim().equalsIgnoreCase(""))
		{
			if(whereClause1Value.equalsIgnoreCase("'null'")){
				whereClause1Value.replace("'","" );
			}
			if(whereClause2Value.equalsIgnoreCase("'null'")){
				whereClause2Value.replace("'","" );
			}
			if(whereClause3Value.equalsIgnoreCase("'null'")){
				whereClause3Value.replace("'","" );
			}
			if(whereClause4Value.equalsIgnoreCase("'null'")){
				whereClause4Value.replace("'","" );
			}
			if(whereClause1Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere1()))
			{
				whereClause1Value= "null";	
				defaultCondition1= " is ";
			}
			if(whereClause2Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere2()))
			{
				whereClause2Value= "null";	
				defaultCondition2= " is ";
			}				
			if(whereClause3Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere3()))
			{
				whereClause3Value= "null";	
				defaultCondition3= " is ";
			}				
			if(whereClause4Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere4()))
			{
				whereClause4Value= "null";	
				defaultCondition4= " is ";
			}
			if(sqlExtract.getSqlText().toLowerCase().contains("where"))
			{	
				if((whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);
				}
				if((whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);								
				}
				if((whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);				
				}
				if((whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);				
				}
				if((whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);		
				}
				if((whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);
				}
				if((whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);
				}
				if((whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");								
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");				
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");				
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");		
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");
				}
			}
			else
			{
				if((whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);
				}
				if((whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);								
				}
				if((whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);				
				}
				if((whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);				
				}
				if((whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);		
				}
				if((whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);
				}
				if((whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);
				}
				if((whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value);
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");								
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");				
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");				
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");		
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");
				}
				if((!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");
				}
				
			}
		}
		else{
			if(whereClause1Value.equalsIgnoreCase("'null'")){
				whereClause1Value.replaceAll("'", "");
			}
			if(whereClause2Value.equalsIgnoreCase("'null'")){
				whereClause2Value.replaceAll("'", "");
			}
			if(whereClause3Value.equalsIgnoreCase("'null'")){
				whereClause3Value.replaceAll("'", "");
			}
			if(whereClause4Value.equalsIgnoreCase("'null'")){
				whereClause4Value.replaceAll("'", "");
			}
			if(whereClause5Value.equalsIgnoreCase("'null'")){
				whereClause5Value.replaceAll("'", "");
			}
			if(whereClause1Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere1()))
			{
				whereClause1Value= "null";	
				defaultCondition1= " is ";
			}
			if(whereClause2Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere2()))
			{
				whereClause2Value= "null";	
				defaultCondition2= " is ";
			}				
			if(whereClause3Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere3()))
			{
				whereClause3Value= "null";	
				defaultCondition3= " is ";
			}				
			if(whereClause4Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere4()))
			{
				whereClause4Value= "null";	
				defaultCondition4= " is ";
			}
			if(whereClause5Value.equalsIgnoreCase("")&&(sqlExtract.getIsdateWhere5()))
			{
				whereClause5Value= "null";	
				defaultCondition5= " is ";
			}

			if(sqlExtract.getSqlText().toLowerCase().contains("where"))
				{	
				if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);				
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);								
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);				
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);				
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);	
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);							
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);				
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);			
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);	
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);
				}
				if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");		
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");							
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");		
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");				
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");					
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");		
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");		
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
				}
				if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
				{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
				}				
								
				}
				else
				{
					if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);				
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);								
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);				
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);				
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);	
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);							
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);				
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);			
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);	
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);
					}
					if((whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat(whereClause5Value);
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");		
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");							
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");		
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");				
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat(whereClause4Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");					
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");		
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat(whereClause1Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");		
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat(whereClause2Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat(whereClause3Value).concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
					}
					if((!whereClause5Value.equalsIgnoreCase("null"))&&(!whereClause4Value.equalsIgnoreCase("null"))&&(!whereClause1Value.equalsIgnoreCase("null"))&&(!whereClause2Value.equalsIgnoreCase("null"))&&(!whereClause3Value.equalsIgnoreCase("null")))
					{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
					}				

					
				}
			}
	
		if(sqlExtract.getOrderBy() == null || sqlExtract.getOrderBy().trim().equalsIgnoreCase("") )
			{
				if( sqlExtract.getGroupBy()== null || sqlExtract.getGroupBy().trim().equalsIgnoreCase(""))
					{
						queryString=queryString;
					}
					else
						{
						queryString=queryString.concat(" ").concat(groupby).concat(" ").concat(sqlExtract.getGroupBy());
						}
					}
					else if (  sqlExtract.getGroupBy()== null || sqlExtract.getGroupBy().trim().equalsIgnoreCase(""))
					{
							if(!orderByValue.equalsIgnoreCase("") && !orderByValue.equalsIgnoreCase("Ascending")){
								orderByValue="desc";
							}
							else{
								orderByValue="asc";
							}
						queryString=queryString.concat(" ").concat(orderby).concat(" ").concat(sqlExtract.getOrderBy().concat(" ").concat(orderByValue));
					}
					else{
						if(!orderByValue.equalsIgnoreCase("") && !orderByValue.equalsIgnoreCase("Ascending")){
							orderByValue="desc";
						}
						else{
							orderByValue="asc";
						}
						
						queryString=queryString.concat(" ").concat(groupby).concat(" ").concat(sqlExtract.getGroupBy()).concat(" ").concat(orderby).concat(" ").concat(sqlExtract.getOrderBy().concat(" ").concat(orderByValue));
	
						}
		try{
			String executeTest = sqlExtractManager.testSQLQuery(queryString);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		String sqlError = "";
		
		
		List extractList=new ArrayList();
				try{
				
					extractList = sqlExtractManager.sqlDataExtract(queryString);
				}catch(Exception ex)
				{
					ex.printStackTrace();
					extractList.add("Error");
				}
				if(!extractList.contains("Error"))
				{
				
				if (!extractList.isEmpty()) 
							{
								if(sqlExtract.getFileType().toLowerCase().equals("xls"))
								{
								HttpServletResponse response = getResponse();
								ServletOutputStream outputStream = response.getOutputStream();
								File file1 = new File(sqlExtract.getFileName());
								response.setContentType("application/vnd.ms-excel; charset=UTF-8");
								//response.setHeader("Cache-Control", "no-cache");
								response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + "." + sqlExtract.getFileType().toLowerCase() );
								response.setHeader("Pragma", "public");
								response.setHeader("Cache-Control", "max-age=0");
								String query = queryString.replaceAll("\r\n", " ");
								
								int starValue = query.toLowerCase().indexOf(" as")+3;
								int endValue = query.toLowerCase().indexOf(" from ");
								String subQuery = query.substring(starValue,endValue);
								subQuery = subQuery+",";
								String [] temp = null;
						       
								temp = subQuery.toLowerCase().split(" as '");
								int totalLength = temp.length;
								String finalOP ="";
								for (int i = 0 ; i < totalLength ; i++) {
						     	   String tmp = temp[i];
						     	   finalOP += tmp.substring(0,tmp.indexOf(",")-1)+"\t";
								}
								header = finalOP.substring(0,finalOP.length()-1)+"\n";
								outputStream.write(header.getBytes());
				
								Iterator it = extractList.iterator();
								while (it.hasNext()) 
									{
										try{
										Object[] row = (Object[]) it.next();
										
		
										for(int i=0; i<totalLength; i++)
											{
												if (row[i] != null) 
													{
														String data = row[i].toString();
														data=data.trim();
														if(data.equalsIgnoreCase(":")){
															data=data.replaceAll(":", "");
														}
														outputStream.write((data + "\t").getBytes());
													} 
												else 
													{
														outputStream.write("\t".getBytes());
													}
											}
										}catch(Exception ex)
										{
											ex.printStackTrace();
											for(int i=0; i<totalLength; i++)
											{
												if ( it.next() != null) 
													{
														String data =  it.next().toString();
														data=data.trim();
														if(data.equalsIgnoreCase(":")){
														data=data.replaceAll(":", "");
														}
														outputStream.write((data + "\t").getBytes());
													} 
												else 
													{
														outputStream.write("\t".getBytes());
													}
											}
										}
										outputStream.write("\n".getBytes());
									}
								
								if(extractFileAudit){
									outputStream.close();
									response.setContentType("application/octet-stream");
									//ServletOutputStream outputStream = response.getOutputStream();
									SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
									StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
									String currentyear=yearDate.toString();
									
									String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
									uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
									File dirPath = new File(uploadDir);
											if (!dirPath.exists()) {
										dirPath.mkdirs();
									}
									SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
									StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
									String currentDateTime=tempDate.toString().trim();
									String fileName=sqlExtract.getFileName()+"_"+currentDateTime+".xls";
									String filePath=uploadDir+""+fileName;
									File file2= new File(filePath); 
									FileOutputStream outputStream1 = new FileOutputStream(file2);
									response.setHeader("Content-Disposition", "attachment; filename=" + file2.getName() + "");
								    query = queryString.replaceAll("\r\n", " ");
									
									starValue = query.toLowerCase().indexOf(" as")+3;
									endValue = query.toLowerCase().indexOf(" from ");
									subQuery = query.substring(starValue,endValue);
									subQuery = subQuery+",";
									String [] temp1 = null;
							       
									temp1 = subQuery.toLowerCase().split(" as '");
									totalLength = temp1.length;
									finalOP ="";
									for (int i = 0 ; i < totalLength ; i++) {
							     	   String tmp = temp[i];
							     	   finalOP += tmp.substring(0,tmp.indexOf(",")-1)+"\t";
									}
									header = finalOP.substring(0,finalOP.length()-1)+"\n";
									outputStream1.write(header.getBytes());
					
									Iterator its = extractList.iterator();
									while (its.hasNext()) 
										{
											try{
											Object[] row = (Object[]) its.next();
											
			
											for(int i=0; i<totalLength; i++)
												{
													if (row[i] != null) 
														{
															String data = row[i].toString();
															data=data.trim();
															if(data.equalsIgnoreCase(":")){
																data=data.replaceAll(":", "");
															}
															outputStream1.write((data + "\t").getBytes());
														} 
													else 
														{
														outputStream1.write("\t".getBytes());
														}
												}
											}catch(Exception ex)
											{
												ex.printStackTrace();
												for(int i=0; i<totalLength; i++)
												{
													if ( it.next() != null) 
														{
															String data =  it.next().toString();
															data=data.trim();
															if(data.equalsIgnoreCase(":")){
															data=data.replaceAll(":", "");
															}
															outputStream1.write((data + "\t").getBytes());
														} 
													else 
														{
														outputStream1.write("\t".getBytes());
														}
												}
											}
											outputStream1.write("\n".getBytes());
										}
									extractedFileLog = new ExtractedFileLog();
									extractedFileLog.setCorpID(sessionCorpID);
									extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
									extractedFileLog.setCreatedOn(new Date());
									extractedFileLog.setLocation(filePath);
									extractedFileLog.setFileName(fileName);
									extractedFileLog.setModule("SQL Extract");
									extractedFileLog=extractedFileLogManager.save(extractedFileLog);
									}
								
								}
								if(sqlExtract.getFileType().toLowerCase().equals("csv"))
								{
								HttpServletResponse response = getResponse();
								ServletOutputStream outputStream = response.getOutputStream();
								File file1 = new File(sqlExtract.getFileName());
								response.setContentType("application/vnd.ms-excel; charset=UTF-8");
								//response.setHeader("Cache-Control", "no-cache");
								response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + "." + sqlExtract.getFileType().toLowerCase() );
								response.setHeader("Pragma", "public");
								response.setHeader("Cache-Control", "max-age=0");
								String query = queryString.replaceAll("\r\n", " ");
								
								int starValue = query.toLowerCase().indexOf(" as")+3;
								int endValue = query.toLowerCase().indexOf(" from ");
								String subQuery = query.substring(starValue,endValue);
								subQuery = subQuery+",";
								String [] temp = null;
						       
								temp = subQuery.toLowerCase().split(" as '");
								int totalLength = temp.length;
								String finalOP ="";
								for (int i = 0 ; i < totalLength ; i++) {
						     	   String tmp = temp[i];
						     	   String tmp1=tmp.substring(0,tmp.indexOf(",")-1);
						     	   String tmp2=tmp1.replace("''", "");
						     	  String tmp5=tmp2.replace("'", "");
						     	  String tmp3=tmp5.replace("?","");
						     	  finalOP += tmp3+",";
								}
								//header = finalOP.substring(0,finalOP.length()-1)+"\n";
								//outputStream.write(new String("\"header"+"\",").getBytes());
								header = finalOP.substring(0,finalOP.length()-1);
								outputStream.write(header.getBytes());
								outputStream.write("\r\n".getBytes());
				
								Iterator it = extractList.iterator();
								while (it.hasNext()) 
									{
										try{
										Object[] row = (Object[]) it.next();
										
		
										for(int i=0; i<totalLength; i++)
											{
												if (row[i] != null) 
													{
														String data = row[i].toString();
														data=data.trim();
														if(data.equalsIgnoreCase(":")){ 
														data=data.replaceAll(":", "");
														}
														 //String tmp2=data.replace("''", "\\'");
												     	 //String tmp3=tmp2.replace("?","");
												     	 //String tmp5=tmp3.replace("'", "\\'");
												     	// data = data.replace("''", "\\'");
												     	// data = data.replace("?","");
												     	// data = data.replace("'", "\\'");
												     	// data = data.replace(",", " ");
												     	 if(i==totalLength-1){
												     		outputStream.write((data).getBytes()); 
												     		
												     	 }else{
												     		outputStream.write((data + ",").getBytes());	 
												     	 }
														
													} 
												else 
													{
													 if(i==totalLength-1){
														 
													 }
													 else{
														outputStream.write(",".getBytes());
													 }
													}
											}
										}catch(Exception ex)
										{
											ex.printStackTrace();
											for(int i=0; i<totalLength; i++)
											{
												if ( it.next() != null) 
													{
														String data =  it.next().toString();
														data=data.trim();
														if(data.equalsIgnoreCase(":")){
                                                        data=data.replaceAll(":", "");
														}
														 if(i==totalLength-1){
														outputStream.write((data).getBytes());
														 }else{
														outputStream.write((data + ",").getBytes());	 
														 }
													} 
												else 
													{
														outputStream.write(" ".getBytes());
													}
											}
										}
										outputStream.write("\r\n".getBytes());
									}

								if(extractFileAudit){
									outputStream.close();
									response.setContentType("application/octet-stream");
									//ServletOutputStream outputStream = response.getOutputStream();
									SimpleDateFormat dateformats = new SimpleDateFormat("yyyy-MMM");
									StringBuilder yearDate = new StringBuilder(dateformats.format(new Date()));
									String currentyear=yearDate.toString();
									
									String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
									uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "extractedFileLog" + "/" + sessionCorpID + "/"+currentyear+"/" + getRequest().getRemoteUser() + "/");
									File dirPath = new File(uploadDir);
											if (!dirPath.exists()) {
										dirPath.mkdirs();
									}
									SimpleDateFormat recformats = new SimpleDateFormat("ddHHmmss");
									StringBuilder tempDate = new StringBuilder(recformats.format(new Date()));
									String currentDateTime=tempDate.toString().trim();
									String fileName=sqlExtract.getFileName()+"_"+currentDateTime+"." + sqlExtract.getFileType();
									String filePath=uploadDir+""+fileName;
									File file2= new File(filePath); 
									FileOutputStream outputStream1 = new FileOutputStream(file2);
									response.setHeader("Content-Disposition", "attachment; filename=" + file2.getName() + "");
								    query = queryString.replaceAll("\r\n", " ");
									
								    starValue = query.toLowerCase().indexOf(" as")+3;
									endValue = query.toLowerCase().indexOf(" from ");
									subQuery = query.substring(starValue,endValue);
									subQuery = subQuery+",";
									String [] temp1 = null;
							       
									temp1 = subQuery.toLowerCase().split(" as '");
									totalLength = temp1.length;
									finalOP ="";
									for (int i = 0 ; i < totalLength ; i++) {
							     	   String tmp = temp1[i];
							     	   String tmp1=tmp.substring(0,tmp.indexOf(",")-1);
							     	   String tmp2=tmp1.replace("''", "");
							     	  String tmp5=tmp2.replace("'", "");
							     	  String tmp3=tmp5.replace("?","");
							     	  finalOP += tmp3+",";
									}
									//header = finalOP.substring(0,finalOP.length()-1)+"\n";
									//outputStream.write(new String("\"header"+"\",").getBytes());
									header = finalOP.substring(0,finalOP.length()-1);
									outputStream1.write(header.getBytes());
									outputStream1.write("\r\n".getBytes());
					
									Iterator its = extractList.iterator();
									while (its.hasNext()) 
										{
											try{
											Object[] row = (Object[]) its.next();
											
			
											for(int i=0; i<totalLength; i++)
												{
													if (row[i] != null) 
														{
															String data = row[i].toString();
															data=data.trim();
															if(data.equalsIgnoreCase(":")){ 
															data=data.replaceAll(":", "");
															}
															 //String tmp2=data.replace("''", "\\'");
													     	 //String tmp3=tmp2.replace("?","");
													     	 //String tmp5=tmp3.replace("'", "\\'");
													     	// data = data.replace("''", "\\'");
													     	// data = data.replace("?","");
													     	// data = data.replace("'", "\\'");
													     	// data = data.replace(",", " ");
													     	 if(i==totalLength-1){
													     		outputStream1.write((data).getBytes()); 
													     		
													     	 }else{
													     		outputStream1.write((data + ",").getBytes());	 
													     	 }
															
														} 
													else 
														{
														 if(i==totalLength-1){
															 
														 }
														 else{
															 outputStream1.write(",".getBytes());
														 }
														}
												}
											}catch(Exception ex)
											{
												ex.printStackTrace();
												for(int i=0; i<totalLength; i++)
												{
													if ( it.next() != null) 
														{
															String data =  it.next().toString();
															data=data.trim();
															if(data.equalsIgnoreCase(":")){
	                                                        data=data.replaceAll(":", "");
															}
															 if(i==totalLength-1){
																 outputStream1.write((data).getBytes());
															 }else{
																 outputStream1.write((data + ",").getBytes());	 
															 }
														} 
													else 
														{
														outputStream1.write(" ".getBytes());
														}
												}
											}
											outputStream1.write("\r\n".getBytes());
										}
									extractedFileLog = new ExtractedFileLog();
									extractedFileLog.setCorpID(sessionCorpID);
									extractedFileLog.setCreatedBy(getRequest().getRemoteUser());
									extractedFileLog.setCreatedOn(new Date());
									extractedFileLog.setLocation(filePath);
									extractedFileLog.setFileName(fileName);
									extractedFileLog.setModule("SQL Extract");
									extractedFileLog=extractedFileLogManager.save(extractedFileLog);
									}
								
								}
							}
				else{
					HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					File file1 = new File(sqlExtract.getFileName());
					response.setContentType("application/vnd.ms-excel");
					//response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + "." + sqlExtract.getFileType().toLowerCase() );
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					String query = queryString.replaceAll("\r\n", " ");
					
					int starValue = query.toLowerCase().indexOf(" as")+3;
					int endValue = query.toLowerCase().indexOf(" from ");
					String subQuery = query.substring(starValue,endValue);
					subQuery = subQuery+",";
					String [] temp = null;
			       
					temp = subQuery.toLowerCase().split(" as '");
					int totalLength = temp.length;
					String finalOP ="";
					for (int i = 0 ; i < totalLength ; i++) {
			     	   String tmp = temp[i];
			     	   String tmp1=tmp.substring(0,tmp.indexOf(",")-1);
			     	   String tmp2=tmp1.replace("''", "");
			     	  String tmp5=tmp2.replace("'", "");
			     	  String tmp3=tmp5.replace("?","");
			     	  finalOP += tmp3+",";
					}
					header = finalOP.substring(0,finalOP.length()-1);
					outputStream.write(header.getBytes());
					outputStream.write("\r\n".getBytes());
					/*header = "NO Record Found , thanks";
					outputStream.write(header.getBytes());*/
					
				}
				}
						else{
								HttpServletResponse response = getResponse();
								ServletOutputStream outputStream = response.getOutputStream();
								File file1 = new File(sqlExtract.getFileName());
								response.setContentType("application/vnd.ms-excel");
								//response.setHeader("Cache-Control", "no-cache");
								response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + "." + sqlExtract.getFileType().toLowerCase() );
								response.setHeader("Pragma", "public");
								response.setHeader("Cache-Control", "max-age=0");
								header = "Error In Extract, thanks";
								outputStream.write(header.getBytes());
								
							}}
			/*else
				{
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1 = new File("StorageExtract");
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + "." + sqlExtract.getFileType().toLowerCase() + "\t The number of line is" + extractList.size() + "extracted");
				header = "NO Record Found , thanks";
				outputStream.write(header.getBytes());
						
			}*/
		//}
		//catch(Exception e) 
		//{
			
			//saveMessage("sqlError:" + e);	
		
		//}
					//inputQueryClauses();
					//saveMessage("Query cannot be executed. ");
					//inputQueryClauses();
					//return SUCCESS;
	
	@SkipValidation
	public String sqlExtractSendMail() throws IOException, ParseException{
		String header = new String();
		sqlExtract = sqlExtractManager.get(id);
		defaultCondition1 = sqlExtract.getDefaultCondition1();
		defaultCondition2 = sqlExtract.getDefaultCondition2();
		defaultCondition3 = sqlExtract.getDefaultCondition3();
		defaultCondition4 = sqlExtract.getDefaultCondition4();
		defaultCondition5 = sqlExtract.getDefaultCondition5(); 
		
	if(sqlExtract.getWhereClause1() == null || sqlExtract.getWhereClause1().trim().equalsIgnoreCase("")   )
			{
				queryString = sqlExtract.getSqlText();
			}
		
		else if(sqlExtract.getWhereClause2()== null||  sqlExtract.getWhereClause2().trim().equalsIgnoreCase("") )
		{ 
			if(sqlExtract.getSqlText().contains("where"))
				{ 
				     queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'");	
				 }
				else
				{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'");	
				} 	
		} 
		else if(sqlExtract.getWhereClause3()== null || sqlExtract.getWhereClause3().trim().equalsIgnoreCase(""))
		{ 
			if(sqlExtract.getSqlText().contains("where"))
			{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'");
			}
			else
			{
			queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'");
			}
		} 
		else if( sqlExtract.getWhereClause4() == null || sqlExtract.getWhereClause4().trim().equalsIgnoreCase(""))
		{ 	
			if(sqlExtract.getSqlText().contains("where"))
			{				
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'");
			}
			else
			{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'");
				
			}
		}  
		else if( sqlExtract.getWhereClause5() == null || sqlExtract.getWhereClause5().trim().equalsIgnoreCase(""))
		{ 	
			if(sqlExtract.getSqlText().contains("where"))
			{	
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");
			 }
			else
			{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");
			 }
		}
		else{ 
			if(sqlExtract.getSqlText().contains("where"))
				{	
					queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
				}
				else
				{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
				}
			} 
				if(sqlExtract.getOrderBy() == null || sqlExtract.getOrderBy().trim().equalsIgnoreCase("") )
					{
						if( sqlExtract.getGroupBy()== null || sqlExtract.getGroupBy().trim().equalsIgnoreCase(""))
							{
								queryString=queryString;
							}
							else
							{
							queryString=queryString.concat(" ").concat(groupby).concat(" ").concat(sqlExtract.getGroupBy());
	
							}
					}
					else if (  sqlExtract.getGroupBy()== null || sqlExtract.getGroupBy().trim().equalsIgnoreCase(""))
					{
					
							if(!orderByValue.equalsIgnoreCase("") && !orderByValue.equalsIgnoreCase("Ascending")){
								orderByValue="desc";
							}
							else{
								orderByValue="asc";
							}
						queryString=queryString.concat(" ").concat(orderby).concat(" ").concat(sqlExtract.getOrderBy().concat(" ").concat(orderByValue));
	
					} 
		//System.out.println("\n\n\n\n\n\n\n\n\n sql query           "+queryString);
		try{
			String executeTest = sqlExtractManager.testSQLQuery(queryString);
		}catch(Exception ex){ 
			ex.printStackTrace();
		} 
		String sqlError = ""; 
		List extractList=new ArrayList();
				try{
				
					extractList = sqlExtractManager.sqlDataExtract(queryString);
				}catch(Exception ex)
				{
					ex.printStackTrace();
					extractList.add("Error");
				}
				try
				{
				if(sqlExtract.getFileType().toLowerCase().equals("xls"))
				{
				String uploadDir =("/resources");
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "/dataExtract" + "/");
				File file = new File(sqlExtract.getFileName());
				String file_name = sqlExtract.getFileName();
				String fileName=sqlExtract.getFileName()+"."+sqlExtract.getFileType().toLowerCase();
				file_name=uploadDir+file_name+"."+sqlExtract.getFileType().toLowerCase();
		        FileWriter file1 = new FileWriter(file_name);
		        BufferedWriter out = new BufferedWriter (file1);  
				if(!extractList.contains("Error"))
				{
					
				if (!extractList.isEmpty()) 
							{ 	
								String query = queryString.replaceAll("\r\n", " ");
								int starValue = query.indexOf(" as")+3;
								int endValue = query.indexOf(" from ");
								String subQuery = query.substring(starValue,endValue);
								subQuery = subQuery+",";
								String [] temp = null;
						       
								temp = subQuery.split(" as '");
								int totalLength = temp.length;
								String finalOP ="";
								for (int i = 0 ; i < totalLength ; i++) {
						     	   String tmp = temp[i];
						     	   finalOP += tmp.substring(0,tmp.indexOf(",")-1)+"\t";
								}
								header = finalOP.substring(0,finalOP.length()-1)+"\n";
								 out.write(header);
				
								Iterator it = extractList.iterator();
								while (it.hasNext()) 
									{
										try{
										Object[] row = (Object[]) it.next();
										
		
										for(int i=0; i<totalLength; i++)
											{
												if (row[i] != null) 
													{
														String data = row[i].toString();
														out.write((data + "\t"));
													} 
												else 
													{
													out.write("\t");
													}
											}
										}catch(Exception ex)
										{
											ex.printStackTrace();
											for(int i=0; i<totalLength; i++)
											{
												if ( it.next() != null) 
													{
														String data =  it.next().toString();
														out.write((data + "\t"));
													} 
												else 
													{
													out.write("\t");
													}
											}
										}
										out.write("\n");
									}
							
								
							}
				else{  
					header = "NO Record Found , thanks";
					out.write(header); 
				}
				}
						else{ 
							header = "NO Record Found , thanks";
							out.write(header);
								
							}
				out.close(); 
				try {
		
				   String from = "support@redskymobility.com"; 
				   String UserEmailId=sqlExtract.getEmail();
					String tempRecipient="";
					String tempRecipientArr[]=UserEmailId.split(",");
					for(String str1:tempRecipientArr){
						if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
							if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
							tempRecipient += str1;
						}
					}
					UserEmailId=tempRecipient;
				   emailTo=UserEmailId;
				   
				   String msgText1 = "Hi "+",\n\nPlease check the attached auto generated extract file.\n\nSchedule Details\nFile Name:"+fileName;
				  
				  
				  try{
					
					  EmailSetUpUtil.sendMail(emailTo, "", "", "SQL Extract", msgText1, file_name, from); 
				 
				       mailStatus = "sent";
				  }
				  catch(Exception sfe)
				   {
					  sfe.printStackTrace();
				 	mailStatus = "notSent";
				   
				   } 
				  }catch (Exception e) {
					  e.printStackTrace();
						// TODO Auto-generated catch block
						e.printStackTrace();
						mailStatus = "notSent";
				 }
				}
				}
				
	catch(Exception e)
	{
		
		
	}
				if(sqlExtract.getFileType().toLowerCase().equals("csv"))
				{
					String uploadDir =("/resources");
					uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "/dataExtract" + "/");
					File file = new File(sqlExtract.getFileName());
					String file_name = sqlExtract.getFileName();
					String fileName=sqlExtract.getFileName()+"."+sqlExtract.getFileType().toLowerCase();
					file_name=uploadDir+file_name+"."+sqlExtract.getFileType().toLowerCase();
			        FileWriter file1 = new FileWriter(file_name);
			        //BufferedWriter out = new BufferedWriter (file1);  
			        PrintWriter out = new PrintWriter(file1);
					if(!extractList.contains("Error"))
					{
						
					if (!extractList.isEmpty()) 
								{ 	
						String query = queryString.replaceAll("\r\n", " ");
						int starValue = query.toLowerCase().indexOf(" as")+3;
						int endValue = query.toLowerCase().indexOf(" from ");
						String subQuery = query.substring(starValue,endValue);
						subQuery = subQuery+",";
						String [] temp = null;
				       
						temp = subQuery.toLowerCase().split(" as '");
						int totalLength = temp.length;
						String finalOP ="";
						for (int i = 0 ; i < totalLength ; i++) {
				     	   String tmp = temp[i];
				     	   String tmp1=tmp.substring(0,tmp.indexOf(",")-1);
				     	   String tmp2=tmp1.replace("''", "");
				     	  String tmp5=tmp2.replace("'", "");
				     	  String tmp3=tmp5.replace("?","");
				     	  finalOP += tmp3+",";
						}
						//header = finalOP.substring(0,finalOP.length()-1)+"\n";
						//outputStream.write(new String("\"header"+"\",").getBytes());
						header = finalOP.substring(0,finalOP.length()-1);
						out.print(header);
						out.print("\n");
					
									Iterator it = extractList.iterator();
									while (it.hasNext()) 
										{
											try{
											Object[] row = (Object[]) it.next();
											
			
											for(int i=0; i<totalLength; i++)
												{
													if (row[i] != null) 
														{
														   String data = row[i].toString();
														   data=data.replaceAll(",", "");
														   if(i==totalLength-1){
														   out.print((data + ""));
														}else{ 
															out.print((data + ","));
														}
														} 
													else 
														{
														if(i==totalLength-1){
															
														}else{
														out.print(",");
														}
														}
												}
											}catch(Exception ex)
											{
												ex.printStackTrace();
												for(int i=0; i<totalLength; i++)
												{
													if ( it.next() != null) 
														{
															String data =  it.next().toString();
															data=data.replaceAll(",", "");
															if(i==totalLength-1){
																out.print((data + ""));	
															}else{
															out.print((data + ","));
															}
														} 
													else 
														{
														if(i==totalLength-1){
															
														}else{
														out.print(",");
														}
														}
												}
											}
											out.print("\n");
										}
								
									
								}
					else{  
						header = "NO Record Found , thanks";
						out.print(header); 
					}
					}
							else{ 
								header = "NO Record Found , thanks";
								out.print(header);
									
								}
					out.close(); 
					try {
			
					   String from = "support@redskymobility.com"; 
					   String UserEmailId=sqlExtract.getEmail();
						String tempRecipient="";
						String tempRecipientArr[]=UserEmailId.split(",");
						for(String str1:tempRecipientArr){
							if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
								if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
								tempRecipient += str1;
							}
						}
						UserEmailId=tempRecipient;
					   emailTo=UserEmailId;
					   
					   String msgText1 = "Hi "+",\n\nPlease check the attached auto generated extract file.\n\nSchedule Details\nFile Name:"+fileName;
					  
					  
					  try{
						
						  EmailSetUpUtil.sendMail(emailTo, "", "", "SQL Extract", msgText1, file_name, from); 
					 
					       mailStatus = "sent";
					  }
					  catch(Exception sfe)
					   {
						  sfe.printStackTrace();
					 	mailStatus = "notSent";
					   
					   } 
					  }catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							mailStatus = "notSent";
					 }
					}
				else{
					String uploadDir =("/resources");
					uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "/dataExtract" + "/");
					File file = new File(sqlExtract.getFileName());
					String file_name = sqlExtract.getFileName();
					String fileName=sqlExtract.getFileName()+"."+sqlExtract.getFileType().toLowerCase();
					file_name=uploadDir+file_name+"."+sqlExtract.getFileType().toLowerCase();
			        FileWriter file1 = new FileWriter(file_name);
			        BufferedWriter out = new BufferedWriter (file1);
                    header = "NO Record Found , thanks";
					out.write(header); 
					out.close(); 
					try {
			
					   String from = "support@redskymobility.com"; 
					   String UserEmailId=sqlExtract.getEmail();
						String tempRecipient="";
						String tempRecipientArr[]=UserEmailId.split(",");
						for(String str1:tempRecipientArr){
							if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
								if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
								tempRecipient += str1;
							}
						}
						UserEmailId=tempRecipient;
					   emailTo=UserEmailId;
					   
					   String msgText1 = "Hi "+",\n\nPlease check the attached auto generated extract file.\n\nSchedule Details\nFile Name:"+fileName;
					  
					  
					  try{
						
						  EmailSetUpUtil.sendMail(emailTo, "", "", "SQL Extract", msgText1, file_name, from); 
					 
					       mailStatus = "sent";
					  }
					  catch(Exception sfe)
					   {
						  sfe.printStackTrace();
					 	mailStatus = "notSent";
					   
					   } 
					  }catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							mailStatus = "notSent";
					 }  

				}
				  return SUCCESS; 
			}
			

			
	private RefMasterManager refMasterManager;
	public  Map<String, String> sqlQuerySchedulerList=new TreeMap();
	public String  getComboList(){
		conditionList= new ArrayList<String>();
		conditionList.add(" = ");
		conditionList.add(" <> ");
		conditionList.add(" < ");
		conditionList.add(" > ");
		conditionList.add(" <= ");
		conditionList.add(" >= ");
		conditionList.add(" is not ");
		conditionList.add(" Like ");
		
		sqlQuerySchedulerList = refMasterManager.findByParameter(sessionCorpID, "query_Scheduler");
		perWeekSchedulerList.put("2", "Mon");
		perWeekSchedulerList.put("3", "Tue");
		perWeekSchedulerList.put("4", "Wed");
		perWeekSchedulerList.put("5", "Thu");
		perWeekSchedulerList.put("6", "Fri");
		perWeekSchedulerList.put("7", "Sat");
		perWeekSchedulerList.put("1", "Sun");
		Integer i = new Integer(1);
		for (i=1;i<=31;i++){
		 perMonthSchedulerList.put(i, i);
		}
		
		return SUCCESS;
	}
	
	@SkipValidation
	public String inputQueryClauses(){
		String isBlank = getRequest().getParameter("isBlank");
		if(isBlank != null){
			isBlank = isBlank.toString();
		}
		List sqlExtract1=sqlExtractManager.getSqlExtractByID(id,sessionCorpID);
		if(!(sqlExtract1.isEmpty())){
		sqlExtract= (SQLExtract)sqlExtract1.get(0);
		}
		getComboList();
		if("yes".equalsIgnoreCase(isBlank)){
			return "withOutPopup";
		}else{
			return SUCCESS;
		}
	}
	
	
	
	@SkipValidation
	public String sqlExtractListSendMail() throws IOException, ParseException{
		String header = new String();
		sqlExtract = sqlExtractManager.getForOtherCorpid(id);
		defaultCondition1 = sqlExtract.getDefaultCondition1();
		defaultCondition2 = sqlExtract.getDefaultCondition2();
		defaultCondition3 = sqlExtract.getDefaultCondition3();
		defaultCondition4 = sqlExtract.getDefaultCondition4();
		defaultCondition5 = sqlExtract.getDefaultCondition5(); 
		String Subject = sqlExtract.getFileName();
	if(sqlExtract.getWhereClause1() == null || sqlExtract.getWhereClause1().trim().equalsIgnoreCase("")   )
			{
				queryString = sqlExtract.getSqlText();
			}
		
		else if(sqlExtract.getWhereClause2()== null||  sqlExtract.getWhereClause2().trim().equalsIgnoreCase("") )
		{ 
			if(sqlExtract.getSqlText().contains("where"))
				{ 
				     queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'");	
				 }
				else
				{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'");	
				} 	
		} 
		else if(sqlExtract.getWhereClause3()== null || sqlExtract.getWhereClause3().trim().equalsIgnoreCase(""))
		{ 
			if(sqlExtract.getSqlText().contains("where"))
			{
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'");
			}
			else
			{
			queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'");
			}
		} 
		else if( sqlExtract.getWhereClause4() == null || sqlExtract.getWhereClause4().trim().equalsIgnoreCase(""))
		{ 	
			if(sqlExtract.getSqlText().contains("where"))
			{				
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'");
			}
			else
			{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'");
				
			}
		}  
		else if( sqlExtract.getWhereClause5() == null || sqlExtract.getWhereClause5().trim().equalsIgnoreCase(""))
		{ 	
			if(sqlExtract.getSqlText().contains("where"))
			{	
				queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");
			 }
			else
			{
				queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'");
			 }
		}
		else{ 
			if(sqlExtract.getSqlText().contains("where"))
				{	
					queryString = sqlExtract.getSqlText().concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
				}
				else
				{
					queryString = sqlExtract.getSqlText().concat(" ").concat(where).concat(" ").concat(sqlExtract.getWhereClause1()).concat(defaultCondition1).concat("'").concat(whereClause1Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause2()).concat(defaultCondition2).concat("'").concat(whereClause2Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause3()).concat(defaultCondition3).concat("'").concat(whereClause3Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause4()).concat(defaultCondition4).concat("'").concat(whereClause4Value).concat("'").concat(" ").concat(and).concat(" ").concat(sqlExtract.getWhereClause5()).concat(defaultCondition5).concat("'").concat(whereClause5Value).concat("'");
				}
			} 
				if(sqlExtract.getOrderBy() == null || sqlExtract.getOrderBy().trim().equalsIgnoreCase("") )
					{
						if( sqlExtract.getGroupBy()== null || sqlExtract.getGroupBy().trim().equalsIgnoreCase(""))
							{
								queryString=queryString;
							}
							else
							{
							queryString=queryString.concat(" ").concat(groupby).concat(" ").concat(sqlExtract.getGroupBy());
	
							}
					}
					else if (  sqlExtract.getGroupBy()== null || sqlExtract.getGroupBy().trim().equalsIgnoreCase(""))
					{
					
							if(!orderByValue.equalsIgnoreCase("") && !orderByValue.equalsIgnoreCase("Ascending")){
								orderByValue="desc";
							}
							else{
								orderByValue="asc";
							}
						queryString=queryString.concat(" ").concat(orderby).concat(" ").concat(sqlExtract.getOrderBy().concat(" ").concat(orderByValue));
	
					} 
		//System.out.println("\n\n\n\n\n\n\n\n\n sql query           "+queryString);
		try{
			String executeTest = sqlExtractManager.testSQLQuery(queryString);
		}catch(Exception ex){ 
			ex.printStackTrace();
		} 
		String sqlError = ""; 
		List extractList=new ArrayList();
				try{
				
					extractList = sqlExtractManager.sqlDataExtract(queryString);
				}catch(Exception ex)
				{
					ex.printStackTrace();
					extractList.add("Error");
				}
				if(sqlExtract.getFileType().toLowerCase().equals("xls"))
				{
				String uploadDir =("/resources");
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "/dataExtract" + "/");
				File file = new File(sqlExtract.getFileName());
				String file_name = sqlExtract.getFileName();
				String fileName=sqlExtract.getFileName()+"."+sqlExtract.getFileType().toLowerCase();
				file_name=uploadDir+file_name+"."+sqlExtract.getFileType().toLowerCase();
		        FileWriter file1 = new FileWriter(file_name);
		        BufferedWriter out = new BufferedWriter (file1);  
				if(!extractList.contains("Error"))
				{
					
				if (!extractList.isEmpty()) 
							{ 	
								String query = queryString.replaceAll("\r\n", " ");
								int starValue = query.toLowerCase().indexOf(" as")+3;
								int endValue = query.toLowerCase().indexOf(" from ");
								String subQuery = query.substring(starValue,endValue);
								subQuery = subQuery+",";
								String [] temp = null;
						       
								temp = subQuery.split(" as '");
								int totalLength = temp.length;
								String finalOP ="";
								for (int i = 0 ; i < totalLength ; i++) {
						     	   String tmp = temp[i];
						     	   finalOP += tmp.substring(0,tmp.indexOf(",")-1)+"\t";
								}
								header = finalOP.substring(0,finalOP.length()-1)+"\n";
								 out.write(header);
				
								Iterator it = extractList.iterator();
								while (it.hasNext()) 
									{
										try{
										Object[] row = (Object[]) it.next();
										
		
										for(int i=0; i<totalLength; i++)
											{
												if (row[i] != null) 
													{
														String data = row[i].toString();
														out.write((data + "\t"));
													} 
												else 
													{
													out.write("\t");
													}
											}
										}catch(Exception ex)
										{
											ex.printStackTrace();
											for(int i=0; i<totalLength; i++)
											{
												if ( it.next() != null) 
													{
														String data =  it.next().toString();
														out.write((data + "\t"));
													} 
												else 
													{
													out.write("\t");
													}
											}
										}
										out.write("\n");
									}
							
								
							}
				else{  
					header = "NO Record Found , thanks";
					out.write(header); 
				}
				}
						else{ 
							header = "NO Record Found , thanks";
							out.write(header);
								
							}
				out.close(); 
				try {
		
				   String from = "support@redskymobility.com"; 
				   String UserEmailId=sqlExtract.getEmail();
					String tempRecipient="";
					String tempRecipientArr[]=UserEmailId.split(",");
					for(String str1:tempRecipientArr){
						if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
							if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
							tempRecipient += str1;
						}
					}
					UserEmailId=tempRecipient;
				   emailTo=UserEmailId;
				   
				   String msgText1 = "Hi "+",\n\nPlease check the attached auto generated extract file.\n\nSchedule Details\nFile Name:"+fileName;
				  
				  
				  try{
					
					  EmailSetUpUtil.sendMail(emailTo, "", "", Subject, msgText1, file_name, from); 
				 
				       mailStatus = "sent";
				  }
				  catch(Exception sfe)
				   {
					  sfe.printStackTrace();
				 	mailStatus = "notSent";
				 	logger.warn("Error Message : "+sfe);
				   } 
				  }catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						logger.warn("Error Message : "+e);
						mailStatus = "notSent";
				 }
				}
				if(sqlExtract.getFileType().toLowerCase().equals("csv"))
				{

					String uploadDir =("/resources");
					uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "/dataExtract" + "/");
					File file = new File(sqlExtract.getFileName());
					String file_name = sqlExtract.getFileName();
					String fileName=sqlExtract.getFileName()+"."+sqlExtract.getFileType().toLowerCase();
					file_name=uploadDir+file_name+"."+sqlExtract.getFileType().toLowerCase();
			        FileWriter file1 = new FileWriter(file_name);
			        //BufferedWriter out = new BufferedWriter (file1);  
			        PrintWriter out = new PrintWriter(file1);
			        if(!extractList.contains("Error"))
					{
						
					if (!extractList.isEmpty()) 
								{ 	
									String query = queryString.replaceAll("\r\n", " ");
									int starValue = query.toLowerCase().indexOf(" as")+3;
									int endValue = query.toLowerCase().indexOf(" from ");
									String subQuery = query.substring(starValue,endValue);
									subQuery = subQuery+",";
									String [] temp = null;
							       
									temp = subQuery.split(" as '");
									int totalLength = temp.length;
									String finalOP ="";
									for (int i = 0 ; i < totalLength ; i++) {
								     	   String tmp = temp[i];
								     	   String tmp1=tmp.substring(0,tmp.indexOf(",")-1);
								     	   String tmp2=tmp1.replace("''", "");
								     	  String tmp5=tmp2.replace("'", "");
								     	  String tmp3=tmp5.replace("?","");
								     	  finalOP += tmp3+",";
										}
										//header = finalOP.substring(0,finalOP.length()-1)+"\n";
										//outputStream.write(new String("\"header"+"\",").getBytes());
										header = finalOP.substring(0,finalOP.length()-1);
										out.print(header);
										out.print("\n");
					
									Iterator it = extractList.iterator();
									while (it.hasNext()) 
										{
											try{
											Object[] row = (Object[]) it.next();
											
			
											for(int i=0; i<totalLength; i++)
												{
													if (row[i] != null) 
														{
														    String data = row[i].toString();
														    data=data.replaceAll(",", "");
														    if(i==totalLength-1){
														    out.print((data + ""));
														}else{ 
															out.print((data + ","));
														}
														} 
													else 
														{
														if(i==totalLength-1){
															
														}else{
														out.print(",");
														}
														}
												}
											}catch(Exception ex)
											{
												ex.printStackTrace();
												for(int i=0; i<totalLength; i++)
												{
													if ( it.next() != null) 
														{
															String data =  it.next().toString();
															data=data.replaceAll(",", "");
															if(i==totalLength-1){
																out.print((data + ""));
															}else{
															out.print((data + ","));
															}
														} 
													else 
														{
														if(i==totalLength-1){
															
														}else{
														out.print(",");
														}
														}
												}
											}
											out.print("\n");
										}
								
									
								}
					else{  
						header = "NO Record Found , thanks";
						out.print(header); 
					}
					}
							else{ 
								header = "NO Record Found , thanks";
								out.print(header);
									
								}
					out.close(); 
					try {
			
					   String from = "support@redskymobility.com"; 
					   String UserEmailId=sqlExtract.getEmail();
						String tempRecipient="";
						String tempRecipientArr[]=UserEmailId.split(",");
						for(String str1:tempRecipientArr){
							if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
								if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
								tempRecipient += str1;
							}
						}
						UserEmailId=tempRecipient;
					   emailTo=UserEmailId;
					   
					   String msgText1 = "Hi "+",\n\nPlease check the attached auto generated extract file.\n\nSchedule Details\nFile Name:"+fileName;
					  
					  
					  try{
						
						  EmailSetUpUtil.sendMail(emailTo, "", "", Subject, msgText1, file_name, from); 
					 
					       mailStatus = "sent";
					  }
					  catch(Exception sfe)
					   {
						  sfe.printStackTrace();
					 	mailStatus = "notSent";
					 	logger.warn("Error Message : "+sfe);
					   } 
					  }catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							logger.warn("Error Message : "+e);
							mailStatus = "notSent";
					 }
						
				}
				  return SUCCESS; 
			}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List getExtractList() {
		return extractList;
	}

	public void setExtractList(List extractList) {
		this.extractList = extractList;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public void setSqlExtractManager(SQLExtractManager sqlExtractManager) {
		this.sqlExtractManager = sqlExtractManager;
	}

	public SQLExtract getSqlExtract() {
		return sqlExtract;
	}

	public void setSqlExtract(SQLExtract sqlExtract) {
		this.sqlExtract = sqlExtract;
	}

	public String getBtn() {
		return btn;
	}

	public void setBtn(String btn) {
		this.btn = btn;
	}

	public List getCorpIDList() {
		return corpIDList;
	}

	public void setCorpIDList(List corpIDList) {
		this.corpIDList = corpIDList;
	}

	public List getSqlExtractUname() {
		return sqlExtractUname;
	}

	public void setSqlExtractUname(List sqlExtractUname) {
		this.sqlExtractUname = sqlExtractUname;
	}

	

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	public String getWhereClause1Value() {
		return whereClause1Value;
	}

	public void setWhereClause1Value(String whereClause1Value) {
		this.whereClause1Value = whereClause1Value;
	}

	public String getWhereClause2Value() {
		return whereClause2Value;
	}

	public void setWhereClause2Value(String whereClause2Value) {
		this.whereClause2Value = whereClause2Value;
	}

	public String getWhereClause3Value() {
		return whereClause3Value;
	}

	public void setWhereClause3Value(String whereClause3Value) {
		this.whereClause3Value = whereClause3Value;
	}

	public String getWhereClause4Value() {
		return whereClause4Value;
	}

	public void setWhereClause4Value(String whereClause4Value) {
		this.whereClause4Value = whereClause4Value;
	}

	public String getWhereClause5Value() {
		return whereClause5Value;
	}

	public void setWhereClause5Value(String whereClause5Value) {
		this.whereClause5Value = whereClause5Value;
	}

	public String getCondition1() {
		return condition1;
	}

	public void setCondition1(String condition1) {
		this.condition1 = condition1;
	}

	public String getCondition2() {
		return condition2;
	}

	public void setCondition2(String condition2) {
		this.condition2 = condition2;
	}

	public String getCondition3() {
		return condition3;
	}

	public void setCondition3(String condition3) {
		this.condition3 = condition3;
	}

	public String getCondition4() {
		return condition4;
	}

	public void setCondition4(String condition4) {
		this.condition4 = condition4;
	}

	public String getGroupBy() {
		return groupBy;
	}

	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getCondition5() {
		return condition5;
	}

	public void setCondition5(String condition5) {
		this.condition5 = condition5;
	}

	public String getOrderByValue() {
		return orderByValue;
	}

	public void setOrderByValue(String orderByValue) {
		this.orderByValue = orderByValue;
	}

	public List getConditionList() {
		return conditionList;
	}

	public void setConditionList(List conditionList) {
		this.conditionList = conditionList;
	}

	public String getMailStatus() {
		return mailStatus;
	}

	public void setMailStatus(String mailStatus) {
		this.mailStatus = mailStatus;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public ExtractedFileLog getExtractedFileLog() {
		return extractedFileLog;
	}

	public void setExtractedFileLog(ExtractedFileLog extractedFileLog) {
		this.extractedFileLog = extractedFileLog;
	}

	public void setExtractedFileLogManager(
			ExtractedFileLogManager extractedFileLogManager) {
		this.extractedFileLogManager = extractedFileLogManager;
	}
    

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getSqlQuerySchedulerList() {
		return sqlQuerySchedulerList;
	}

	public void setSqlQuerySchedulerList(Map<String, String> sqlQuerySchedulerList) {
		this.sqlQuerySchedulerList = sqlQuerySchedulerList;
	}

	public Map<String, String> getPerWeekSchedulerList() {
		return perWeekSchedulerList;
	}

	public void setPerWeekSchedulerList(Map<String, String> perWeekSchedulerList) {
		this.perWeekSchedulerList = perWeekSchedulerList;
	}

	public Map<Integer, Integer> getPerMonthSchedulerList() {
		return perMonthSchedulerList;
	}

	public void setPerMonthSchedulerList(Map<Integer, Integer> perMonthSchedulerList) {
		this.perMonthSchedulerList = perMonthSchedulerList;
	}


	

}
