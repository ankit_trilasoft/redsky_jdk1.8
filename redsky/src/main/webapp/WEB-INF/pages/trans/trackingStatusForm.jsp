<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<head>   
    <title><fmt:message key="trackingStatusDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='trackingStatusDetail.heading'/>"/>
    <style type="text/css">h2 {background-color: #FBBFFF}</style>
<style type="text/css">
@media only screen and (max-width:1024px) {
    #scrollable { overflow-x: scroll !important; width:920px;
    } }
	div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
<script language="javascript" type="text/javascript"><%@ include file="/common/formCalender.js"%></script>    
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script language="javascript" type="text/javascript"> 
<c:set var="isItemList" value="Y" />
<configByCorp:fieldVisibility componentId="component.workTicket.itemListTab">
<c:set var="isItemList" value="F" />
</configByCorp:fieldVisibility>
function showPartnerAlert(display, code, position){
     if(code == ''){ 
     	document.getElementById(position).style.display="none";
     	ajax_hideTooltip();
     }else if(code != ''){
     	getBAPartner(display,code,position);
     }  }
function getBAPartner(display,code,position){var url = "findPartnerAlertList.html?ajax=1&decorator=simple&popup=true&notesId=" + encodeURI(code);
     http.open("GET", url, true); http.onreadystatechange = function() {handleHttpResponsePartner(display,code,position)};
     http.send(null);
}
function handleHttpResponsePartner(display,code,position){
 	if (http.readyState == 4){var results = http.responseText
        results = results.trim();
        if(results.length > 555){document.getElementById(position).style.display="block"; 
          	if(display != 'onload'){getPartnerAlert(code,position);}
      	}else{document.getElementById(position).style.display="none";ajax_hideTooltip();} } }
</script> 
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('agentroles', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('agentReciprocity', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('weights', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('initiation', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('orgpack', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('sitorigin', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('interstate', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('orgfwd', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('military','fade=0,persist=1,hide=1')
animatedcollapse.addDiv('gstfwd', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('transport', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('destination', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('sitdestination', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('storage', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('customs', 'fade=0,persist=1,hide=0')
animatedcollapse.addDiv('weightnvolumetracsub', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('weightnvolumetracsub2', 'fade=0,persist=1,hide=1')
animatedcollapse.init()
</script>
<style><%@ include file="/common/calenderStyle.css"%></style>   
<script language="JavaScript" type="text/javascript">
  <sec-auth:authComponent componentId="module.script.form.agentScript">
  window.onload = function() { trap();
		try{ var linkup_Network = $('#linkup_Network');
			 $(linkup_Network).hide(); var linkup_originAgentExSO = $('#linkup_originAgentExSO');
			 $(linkup_originAgentExSO).hide(); var linkup_destinationAgentExSO = $('#linkup_destinationAgentExSO');
			 $(linkup_destinationAgentExSO).hide(); var linkup_originSubAgentExSO = $('#linkup_originSubAgentExSO');
			 $(linkup_originSubAgentExSO).hide(); var linkup_destinationSubAgentExSO = $('#linkup_destinationSubAgentExSO');
			 $(linkup_destinationSubAgentExSO).hide(); var linkup_bookingAgent = $('#linkup_bookingAgent');
			 $(linkup_bookingAgent).hide();	 
      }catch(e){}
		 $('#trackingStatusForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
		 $('#trackingStatusForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper').attr('onkeyup','false');	 
		 if(document.forms['trackingStatusForm'].elements['saveButton']) {
				document.forms['trackingStatusForm'].elements['saveButton'].disabled=false;
			}
			if(document.forms['trackingStatusForm'].elements['Reset']) {
				document.forms['trackingStatusForm'].elements['Reset'].disabled=false;
			}
			<sec-auth:authScript tableList="trackingStatus,miscellaneous" formNameList="trackingStatusForm,trackingStatusForm,trackingStatusForm" transIdList='${serviceOrder.shipNumber}'>
	        </sec-auth:authScript>
			trap1();
				}	
 </sec-auth:authComponent>
<sec-auth:authComponent componentId="module.script.form.corpAccountScript">
window.onload = function() { 
	trap();
	try{var linkup_Network = $('#linkup_Network');$(linkup_Network).hide();
		 var linkup_originAgentExSO = $('#linkup_originAgentExSO'); $(linkup_originAgentExSO).hide();			 
		 var linkup_destinationAgentExSO = $('#linkup_destinationAgentExSO'); $(linkup_destinationAgentExSO).hide();
		 var linkup_originSubAgentExSO = $('#linkup_originSubAgentExSO'); $(linkup_originSubAgentExSO).hide();
		 var linkup_destinationSubAgentExSO = $('#linkup_destinationSubAgentExSO'); $(linkup_destinationSubAgentExSO).hide();
		 var linkup_bookingAgent = $('#linkup_bookingAgent'); $(linkup_bookingAgent).hide();	
    }catch(e){}
	 $('#trackingStatusForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
	 $('#trackingStatusForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper').attr('onkeyup','false');
	 trap1();		
 }
</sec-auth:authComponent>
<sec-auth:authComponent componentId="module.script.form.vendettiScript">
window.onload = function() { 
	trap();
	try{var linkup_Network = $('#linkup_Network');
		 $(linkup_Network).hide();
		 var linkup_originAgentExSO = $('#linkup_originAgentExSO');
		 $(linkup_originAgentExSO).hide();			 
		 var linkup_destinationAgentExSO = $('#linkup_destinationAgentExSO');
		 $(linkup_destinationAgentExSO).hide();
		 var linkup_originSubAgentExSO = $('#linkup_originSubAgentExSO');
		 $(linkup_originSubAgentExSO).hide();
		 var linkup_destinationSubAgentExSO = $('#linkup_destinationSubAgentExSO');
		 $(linkup_destinationSubAgentExSO).hide();
		 var linkup_bookingAgent = $('#linkup_bookingAgent');
		 $(linkup_bookingAgent).hide();	
   }catch(e){}
	 $('#trackingStatusForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
	 $('#trackingStatusForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper').attr('onkeyup','false');	 
	 if(document.forms['trackingStatusForm'].elements['saveButton']){
		document.forms['trackingStatusForm'].elements['saveButton'].disabled=false;
	}
	if(document.forms['trackingStatusForm'].elements['Reset']){
			document.forms['trackingStatusForm'].elements['Reset'].disabled=false;
	}
	<sec-auth:enableScript tableList="trackingStatus,miscellaneous" formNameList="trackingStatusForm,trackingStatusForm,trackingStatusForm" ></sec-auth:enableScript>
	trap1();
}
 </sec-auth:authComponent>
 <sec-auth:authComponent componentId="module.script.form.partnerScript">
 window.onload = function() { 
		trap();
		try{
			 var linkup_Network = $('#linkup_Network');
			 $(linkup_Network).hide();
			 var linkup_originAgentExSO = $('#linkup_originAgentExSO');
			 $(linkup_originAgentExSO).hide();			 
			 var linkup_destinationAgentExSO = $('#linkup_destinationAgentExSO');
			 $(linkup_destinationAgentExSO).hide();
			 var linkup_originSubAgentExSO = $('#linkup_originSubAgentExSO');
			 $(linkup_originSubAgentExSO).hide();
			 var linkup_destinationSubAgentExSO = $('#linkup_destinationSubAgentExSO');
			 $(linkup_destinationSubAgentExSO).hide();
			 var linkup_bookingAgent = $('#linkup_bookingAgent');
			 $(linkup_bookingAgent).hide();	
    }catch(e){}
		 $('#trackingStatusForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
		 $('#trackingStatusForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper').attr('onkeyup','false');	 
			if(document.forms['trackingStatusForm'].elements['saveButton']){
				document.forms['trackingStatusForm'].elements['saveButton'].disabled=false;
			}if(document.forms['trackingStatusForm'].elements['Reset']){
				document.forms['trackingStatusForm'].elements['Reset'].disabled=false;
			}
			<sec-auth:enableScript tableList="trackingStatus,miscellaneous" formNameList="trackingStatusForm,trackingStatusForm,trackingStatusForm" >
			</sec-auth:enableScript>
			trap1();			
	}
 </sec-auth:authComponent>
	function enableAll() { 
	var elementsLen=document.forms['trackingStatusForm'].elements.length;
			for(i=0;i<=elementsLen-1;i++) { 
						document.forms['trackingStatusForm'].elements[i].disabled=false;
			} }
function forDays(){
 document.forms['trackingStatusForm'].elements['checkSitDestinationDaysClick'].value = '1';
}
function calcDays() {
 var date2 = document.forms['trackingStatusForm'].elements['trackingStatus.sitDestinationIn'].value;	 
 var date1 = document.forms['trackingStatusForm'].elements['trackingStatus.sitDestinationA'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')  { month = "01";
   }  else if(month == 'Feb') { month = "02";
   } else if(month == 'Mar') { month = "03"
   }   else if(month == 'Apr')  { month = "04"
   }  else if(month == 'May') { month = "05"
   }  else if(month == 'Jun') { month = "06"
   }  else if(month == 'Jul') { month = "07"
   } else if(month == 'Aug') {month = "08"
   } else if(month == 'Sep')  {month = "09"
   } else if(month == 'Oct')  {month = "10"
   }  else if(month == 'Nov')  { month = "11"
   } else if(month == 'Dec')  {month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')  { month2 = "01";
   }  else if(month2 == 'Feb') { month2 = "02";
   } else if(month2 == 'Mar') {month2 = "03"
   } else if(month2 == 'Apr') {month2 = "04"
   }  else if(month2 == 'May')  { month2 = "05"
   }  else if(month2 == 'Jun') { month2 = "06"
   } else if(month2 == 'Jul') { month2 = "07"
   } else if(month2 == 'Aug')  {month2 = "08"
   } else if(month2 == 'Sep')  { month2 = "09"
   } else if(month2 == 'Oct')  { month2 = "10"
   }  else if(month2 == 'Nov') { month2 = "11"
   } else if(month2 == 'Dec')  {month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = (Math.round((sDate-eDate)/86400000))+1;
  document.forms['trackingStatusForm'].elements['trackingStatus.sitDestinationDays'].value = daysApart; 
  if(daysApart<1) {
    alert("SIT Destination In Date Range must be less than to SIT Destination Out");
    document.forms['trackingStatusForm'].elements['trackingStatus.sitDestinationDays'].value='';
    document.forms['trackingStatusForm'].elements['trackingStatus.sitDestinationA'].value='';
  }
  if(document.forms['trackingStatusForm'].elements['trackingStatus.sitDestinationDays'].value=='NaN')   {
     document.forms['trackingStatusForm'].elements['trackingStatus.sitDestinationDays'].value = '';
   }
 	document.forms['trackingStatusForm'].elements['checkSitDestinationDaysClick'].value = '';
}
function forOriDays(){
 document.forms['trackingStatusForm'].elements['checkSitOriginDaysClick'].value = '1';
}
function calcDayOri() {
 var date2 = document.forms['trackingStatusForm'].elements['trackingStatus.sitOriginOn'].value;	
 var date1 = document.forms['trackingStatusForm'].elements['trackingStatus.sitOriginA'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan') { month = "01";
   }  else if(month == 'Feb') { month = "02";
   } else if(month == 'Mar')  { month = "03"
   } else if(month == 'Apr') { month = "04"
   }  else if(month == 'May') {month = "05"
   } else if(month == 'Jun')  {month = "06"
   }  else if(month == 'Jul') {month = "07"
   } else if(month == 'Aug')  { month = "08"
   } else if(month == 'Sep')  { month = "09"
   }  else if(month == 'Oct')  {month = "10"
   }  else if(month == 'Nov') {month = "11"
   } else if(month == 'Dec') { month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')  { month2 = "01";
   }  else if(month2 == 'Feb')  { month2 = "02";
   } else if(month2 == 'Mar')  {month2 = "03"
   } else if(month2 == 'Apr') { month2 = "04"
   } else if(month2 == 'May') {month2 = "05"
   }  else if(month2 == 'Jun') { month2 = "06"
   }  else if(month2 == 'Jul') { month2 = "07"
   } else if(month2 == 'Aug')  {  month2 = "08"
   } else if(month2 == 'Sep')  {month2 = "09"
   }  else if(month2 == 'Oct')  { month2 = "10"
   } else if(month2 == 'Nov') {month2 = "11"
   } else if(month2 == 'Dec') { month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = (Math.round((sDate-eDate)/86400000))+1;
  document.forms['trackingStatusForm'].elements['trackingStatus.sitOriginDays'].value = daysApart; 
  if(daysApart<1) {
    alert("SIT Origin In Date Range must be less than to SIT Origin Out");
    document.forms['trackingStatusForm'].elements['trackingStatus.sitOriginDays'].value='';
    document.forms['trackingStatusForm'].elements['trackingStatus.sitOriginA'].value='';
  }
  if(document.forms['trackingStatusForm'].elements['trackingStatus.sitOriginDays'].value=='NaN')  {
     document.forms['trackingStatusForm'].elements['trackingStatus.sitOriginDays'].value = '';
   } 
 	document.forms['trackingStatusForm'].elements['checkSitOriginDaysClick'].value = '';
 }
function checkDatePre() {
 	var etDepart =document.forms['trackingStatusForm'].elements['trackingStatus.preliminaryNotification'].value;
 	var id=document.forms['trackingStatusForm'].elements['id'].value;
    var url="checkDatePre.html?ajax=1&decorator=simple&popup=true&etDepart=" + encodeURI(etDepart)+"&id="+encodeURI(id);
    http2.open("GET", url, true);
    http2.onreadystatechange = httpResponseCheckDate;
    http2.send(null);
}
function httpResponseCheckDate()  { 
             if (http2.readyState == 4) {
                var results = http2.responseText
                results = results.trim();
                var less='[Pre-Advise date cannot be Greater than the Carrier ETD]' ;
                if(results ==less.trim()){
                	alert('Pre-Advise date cannot be Greater than the Carrier ETD');
                	document.forms['trackingStatusForm'].elements['trackingStatus.preliminaryNotification'].value='';
                }  }  }
 function findWeight(){
     var sid = document.forms['trackingStatusForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     } else{
     var url="containerWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse3;
     http2.send(null);
} }
function findNetWeight(){
     var sid = document.forms['trackingStatusForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }else{
     var url="containerNetWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
     } } 
function findAgent(position,agentType) {
	var partnerCode = "";
	var shipNumberA="";
	var originalCorpID = document.forms['trackingStatusForm'].elements['trackingStatus.corpID'].value
	if(agentType=='OA') {
		partnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value; 
		var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&originalCorpID=" + encodeURI(originalCorpID);
		ajax_showTooltip(url,position); 
	}
	if(agentType=='DA') {
		partnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value; 
		var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&originalCorpID=" + encodeURI(originalCorpID);
		ajax_showTooltip(url,position);	
	}
	if(agentType=='BA') {	
		shipNumberA = document.forms['trackingStatusForm'].elements['shipNumberCode'].value; 
		var url="bookerAddress.html?ajax=1&decorator=simple&popup=true&shipNumberA=" + encodeURI(shipNumberA);		
		ajax_showTooltip(url,position);	
	} 
	if(agentType=='BK') {	
		partnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].value; 
		var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&originalCorpID=" + encodeURI(originalCorpID);
		ajax_showTooltip(url,position);	
	}
	if(agentType=='SOA') {	
		partnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value; 
		var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&originalCorpID=" + encodeURI(originalCorpID);
		ajax_showTooltip(url,position);	
	}
	if(agentType=='SDA') {	
		partnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value; 
		var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&originalCorpID=" + encodeURI(originalCorpID);
		ajax_showTooltip(url,position);	
	}
	if(agentType=='FWD') {	
		partnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].value; 
		var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&originalCorpID=" + encodeURI(originalCorpID);
		ajax_showTooltip(url,position);		}
	if(agentType=='NT') {	
		partnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value; 
		var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&originalCorpID=" + encodeURI(originalCorpID);
		ajax_showTooltip(url,position);		}
	return false
}
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
function handleHttpResponse2()  {
             if (http2.readyState == 4)  {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")  {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 }else{
                 	document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value = '${miscellaneous.actualNetWeight}';
                 } }  }  }  
function handleHttpResponse7()  {
             if (http2.readyState == 4) {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
             }    }             
function handleHttpResponse6()  {
             if (http2.readyState == 4)  {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00") {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 }else{ 
                 	document.forms['trackingStatusForm'].elements['miscellaneous.actualTareWeight'].value = '${miscellaneous.actualTareWeight}';
                 	calcNetWeight3();
                 } }  }    }     
        function handleHttpResponse5()  {
             if (http2.readyState == 4)  {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00") {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){ 
                 }else{
                 	document.forms['trackingStatusForm'].elements['miscellaneous.actualCubicFeet'].value = '${miscellaneous.actualCubicFeet}';
                 	document.forms['trackingStatusForm'].elements['miscellaneous.actualCubicMtr'].value = '${miscellaneous.actualCubicMtr}';
                 } }  }  }     
function handleHttpResponse3()  {
              if (http2.readyState == 4) {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00") {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){ 
                 }else{
                 	document.forms['trackingStatusForm'].elements['miscellaneous.actualGrossWeight'].value = '${miscellaneous.actualGrossWeight}';
                 	document.forms['trackingStatusForm'].elements['miscellaneous.actualNetWeight'].value = '${miscellaneous.actualNetWeight}';
                 calcNetWeight3();
                 } }  }  }     
 function change() {
		       if( document.forms['trackingStatusForm'].elements['serviceOrder.commodity'].value == "BOAT" || document.forms['trackingStatusForm'].elements['serviceOrder.commodity'].value == "AUTO" || document.forms['trackingStatusForm'].elements['serviceOrder.commodity'].value == "HHG/A") {
		        	document.forms['trackingStatusForm'].elements['miscellaneous.entitleNumberAuto'].disabled = false ;
				document.forms['trackingStatusForm'].elements['miscellaneous.estimateAuto'].disabled = false ;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualAuto'].disabled = false ;
			  }else{
			  	document.forms['trackingStatusForm'].elements['miscellaneous.entitleNumberAuto'].disabled = true ;
				document.forms['trackingStatusForm'].elements['miscellaneous.estimateAuto'].disabled = true ;
				document.forms['trackingStatusForm'].elements['miscellaneous.actualAuto'].disabled = true ; 
			  } }
function autoPopulate_trackingStatus_approvedDate(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['trackingStatus.managerOkOn'].value=datam;	}
function autoPopulate_miscellaneous_approvedDate1(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['trackingStatus.packDone'].value=datam1;
	}	
function validate_email(field) {
with (field)
{
apos=value.indexOf("@")       
dotpos=value.lastIndexOf(".")
if (apos<1||dotpos-apos<2) 
  {alert("Not a valid e-mail address!");return false}
else {return true}
}
if(document.forms['trackingStatusForm'].elements['miscellaneous.estimateCubicFeet'].value == '0.0' ){
		document.forms['trackingStatusForm'].elements['miscellaneous.estimateCubicFeet'].value="";
	}
	if(document.forms['trackingStatusForm'].elements['miscellaneous.entitleCubicFeet'].value == '0.0' ){
		document.forms['trackingStatusForm'].elements['miscellaneous.entitleCubicFeet'].value="";
	}
	if(document.forms['trackingStatusForm'].elements['miscellaneous.actualCubicFeet'].value == '0.0' ){
		document.forms['trackingStatusForm'].elements['miscellaneous.actualCubicFeet'].value="";
	}
	if(document.forms['trackingStatusForm'].elements['miscellaneous.netEstimateCubicFeet'].value == '0.0' ){
		document.forms['trackingStatusForm'].elements['miscellaneous.netEstimateCubicFeet'].value="";
	}
	if(document.forms['trackingStatusForm'].elements['miscellaneous.netEntitleCubicFeet'].value == '0.0' ){
		document.forms['trackingStatusForm'].elements['miscellaneous.netEntitleCubicFeet'].value="";
	} 
	if(document.forms['trackingStatusForm'].elements['miscellaneous.netActualCubicFeet'].value == '0.0' ){
		document.forms['trackingStatusForm'].elements['miscellaneous.netActualCubicFeet'].value="";
	} } 
	function checkdate(clickType){
	var elementsLen=document.forms['trackingStatusForm'].elements.length;
			for(i=0;i<=elementsLen-1;i++) { 
				 document.forms['trackingStatusForm'].elements[i].disabled=false;
			}
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
			<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
	<c:set var="checkPropertyAmountComponent" value="N" />
	<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
		<c:set var="checkPropertyAmountComponent" value="Y" />
	</configByCorp:fieldVisibility>
	progressBarAutoSave('1');
		if ('${autoSavePrompt}' == 'No'){ 
	        var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
      		var id1 = document.forms['trackingStatusForm'].elements['serviceOrder.id'].value;
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
               //noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
               <c:if test="${checkPropertyAmountComponent!='Y'}">
               		noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  			noSaveAction = 'editServiceOrderUpdate.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
           }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.billing'){
				//noSaveAction = 'editBilling.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	          		noSaveAction = 'editBilling.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  			noSaveAction = 'editBilling.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
			}    
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.accounting'){
               //noSaveAction = 'accountLineList.html?sid='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	         		noSaveAction = 'accountLineList.html?sid='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  			noSaveAction = 'accountLineList.html?sid='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
           }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
               noSaveAction = 'pricingList.html?sid='+id1;
               }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
	        	   <c:if test="${forwardingTabVal!='Y'}">
		  				noSaveAction = 'containers.html?id='+id1;
		  			</c:if>
			  		<c:if test="${forwardingTabVal=='Y'}">
			  			//noSaveAction = 'containersAjaxList.html?id='+id1;
			  			<c:if test="${checkPropertyAmountComponent!='Y'}">
			  				noSaveAction = 'containersAjaxList.html?id='+id1;
						</c:if>
					  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				  			noSaveAction = 'containersAjaxList.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				  		</c:if>
			  		</c:if>
               }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.domestic'){
               //noSaveAction = 'editMiscellaneous.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	        		noSaveAction = 'editMiscellaneous.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  			noSaveAction = 'editMiscellaneous.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
            }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.OI'){
               //noSaveAction = 'operationResource.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	       			noSaveAction = 'operationResource.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  			noSaveAction = 'operationResource.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
           }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.claims'){
               //noSaveAction = 'claims.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	      			noSaveAction = 'claims.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  			noSaveAction = 'claims.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
           }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.ticket'){
               //noSaveAction = 'customerWorkTickets.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	     			noSaveAction = 'customerWorkTickets.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  			noSaveAction = 'customerWorkTickets.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
           }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
        	   var cidVal='${customerFile.id}';
               noSaveAction = 'editCustomerFile.html?id='+cidVal;
               } 
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value =='gototab.criticaldate')  {
        	   noSaveAction ='soAdditionalDateDetails.html?sid='+id1;
           }
          processAutoSave(document.forms['trackingStatusForm'], 'saveTrackingStatus!saveOnTabChange.html', noSaveAction);
       
}else{
     if(!(clickType == 'save')){
      var id1 = document.forms['trackingStatusForm'].elements['serviceOrder.id'].value;
   var jobNumber = document.forms['trackingStatusForm'].elements['serviceOrder.shipNumber'].value;
      if (document.forms['trackingStatusForm'].elements['formStatus'].value == '1'){
      var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='trackingStatusDetail.heading'/>");
         if(agree){
           document.forms['trackingStatusForm'].action = 'saveTrackingStatus!saveOnTabChange.html';
           document.forms['trackingStatusForm'].submit();
       }else{
           if(id1 != ''){
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
               //location.href = 'editServiceOrderUpdate.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
        	   		location.href = 'editServiceOrderUpdate.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'editServiceOrderUpdate.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
               }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.billing'){
				//location.href = 'editBilling.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	   	   			location.href = 'editBilling.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'editBilling.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
			}    
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.accounting'){
               //location.href = 'accountLineList.html?sid='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	  	   			location.href = 'accountLineList.html?sid='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'accountLineList.html?sid='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
           }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
               location.href = 'pricingList.html?sid='+id1;
               }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
               <c:if test="${forwardingTabVal!='Y'}">
               		location.href = 'containers.html?id='+id1;
 				</c:if>
	  			<c:if test="${forwardingTabVal=='Y'}">
	  				//location.href = 'containersAjaxList.html?id='+id1;
  					<c:if test="${checkPropertyAmountComponent!='Y'}">
  						location.href = 'containersAjaxList.html?id='+id1;
					</c:if>
				  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				  		location.href = 'containersAjaxList.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  		</c:if>
	  			</c:if>
               }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.domestic'){
               //location.href = 'editMiscellaneous.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
        	   		location.href = 'editMiscellaneous.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'editMiscellaneous.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
           }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.OI'){
        	   //location.href = 'operationResource.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
        	   		location.href = 'operationResource.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'operationResource.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
           }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.claims'){
               //location.href = 'claims.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	   	   			location.href = 'claims.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'claims.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
           }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.ticket'){
               //location.href = 'customerWorkTickets.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	  	   			location.href = 'customerWorkTickets.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'customerWorkTickets.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
           }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
        	   var cidVal='${customerFile.id}';
               location.href = 'editCustomerFile.html?id='+cidVal;
               }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value =='gototab.criticaldate')  {
        	   location.href ='soAdditionalDateDetails.html?sid='+id1;
           	   }  }  }
   }else{
   if(id1 != ''){
       if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
               //location.href = 'editServiceOrderUpdate.html?id='+id1;
	    	   	<c:if test="${checkPropertyAmountComponent!='Y'}">
		   			location.href = 'editServiceOrderUpdate.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'editServiceOrderUpdate.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		 		</c:if>
               }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.billing'){
				//location.href = 'editBilling.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
		   			location.href = 'editBilling.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'editBilling.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		 		</c:if>
			} 
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.accounting'){
               //location.href = 'accountLineList.html?sid='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
		   			location.href = 'accountLineList.html?sid='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'accountLineList.html?sid='+id1+'&msgClicked='+checkMsgClickedValue;
		 		</c:if>
           }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
               location.href = 'pricingList.html?sid='+id1;
               }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
        	   <c:if test="${forwardingTabVal!='Y'}">
          			location.href = 'containers.html?id='+id1;
				</c:if>
 				<c:if test="${forwardingTabVal=='Y'}">
 					//location.href = 'containersAjaxList.html?id='+id1;
 					<c:if test="${checkPropertyAmountComponent!='Y'}">
	  						location.href = 'containersAjaxList.html?id='+id1;
						</c:if>
					  	<c:if test="${checkPropertyAmountComponent=='Y'}">
					  		location.href = 'containersAjaxList.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				  		</c:if>
 				</c:if>
               }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.domestic'){
               //location.href = 'editMiscellaneous.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
		   			location.href = 'editMiscellaneous.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'editMiscellaneous.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		 		</c:if>
           }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.OI'){
        	   //location.href = 'operationResource.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
		   			location.href = 'operationResource.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'operationResource.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		 		</c:if>
           }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.claims'){
               //location.href = 'claims.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
		   			location.href = 'claims.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'claims.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		 		</c:if>
           }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.ticket'){
               //location.href = 'customerWorkTickets.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
		   			location.href = 'customerWorkTickets.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'customerWorkTickets.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		 		</c:if>
            }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
        	   var cidVal='${customerFile.id}';
               location.href = 'editCustomerFile.html?id='+cidVal;
               }
           if(document.forms['trackingStatusForm'].elements['gotoPageString'].value =='gototab.criticaldate')  {
        	   location.href ='soAdditionalDateDetails.html?sid='+id1;
           }  }  }  } } }
function changeStatus(){
   document.forms['trackingStatusForm'].elements['formStatus'].value = '1';
}
function storageDisplay(){
	var storageSectionId = document.getElementById('storageVolMain');
	if(storageSectionId != null && storageSectionId != undefined){
		document.getElementById('storageVolMain').style.display = 'none';
	} }
</script> 	
<style type="text/css">
.bluefont{font-family: Arial, Helvetica, sans-serif;font-size: 13px;font-weight: bold;color: #012f60;}
</style>
</head>
<script language="JavaScript">
var requiredDeliveryDateTracker='';
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++) {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value=""; 
	        return false;
	        }  }
	    return true;
	}
	function onlyTimeFormatAllowed(evt) {
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	} 
function checkDateStatus() {
var i;
for(i=0;i<=150;i++) {
	if(document.forms['trackingStatusForm'].elements[i].value == "null" ){
		document.forms['trackingStatusForm'].elements[i].value="";
	} }	 }
var vendorChangeStatus='';

function checkVendorName(){
	<c:if test="${isNetworkBookingAgent==true || networkAgent}">
	 var savedOriginAgentExSO='${trackingStatus.originAgentExSO}';
	 var originAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value;
	 var savedAgent='${trackingStatus.originAgentCode}';
	 var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	 if(originAgentExSo != '' && savedOriginAgentExSO == originAgentExSo && !sameCompanyCodeflag){
			agree= confirm("Changing the agent will remove the current RedSky Network partner link");
			if(agree){
				vendorChangeStatus='YES';
				var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
			    if(vendorId == ''){
			    	document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value='';
			    }
				var job = document.forms['trackingStatusForm'].elements['serviceOrder.job'].value; 
			    if(job=='DOM'||job=='UVL'||job=='MVL'){    
			    	document.forms['trackingStatusForm'].elements['trackingStatus.originAgentVanlineCode'].value='';
			    }
			    if(vendorId!=''){ 
				    <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
					var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode=" + encodeURI(vendorId);
				    </configByCorp:fieldVisibility>
				    <configByCorp:fieldVisibility componentId="component.field.vanline">
					<c:if test="${agentSearchValidation}">
				    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=CRNOTALLOWED&partnerCode=" + encodeURI(vendorId);
				    	 </c:if>
				    	 <c:if test="${!agentSearchValidation}">
					    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
					    	 </c:if>
				    </configByCorp:fieldVisibility>
				    xhttp.open("GET", url, true);
				    xhttp.onreadystatechange = handleHttpResponse8;
				    xhttp.send(null);
			    }
			}else{
				document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value='${trackingStatus.originAgentCode}';
				document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value="${trackingStatus.originAgent}";
				document.forms["trackingStatusForm"].elements["trackingStatus.originAgentContact"].value="${trackingStatus.originAgentContact}";
				document.forms["trackingStatusForm"].elements["trackingStatus.originAgentEmail"].value="${trackingStatus.originAgentEmail}";
				document.forms["trackingStatusForm"].elements["trackingStatus.originAgentPhoneNumber"].value='${trackingStatus.originAgentPhoneNumber}';
			}
			}else{
				if(!sameCompanyCodeflag){
				document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value='';
				}
				var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
			    if(vendorId == ''){
			    	document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value='';
			    }
				var job = document.forms['trackingStatusForm'].elements['serviceOrder.job'].value; 
			    if(job=='DOM'||job=='UVL'||job=='MVL'){    
			    	document.forms['trackingStatusForm'].elements['trackingStatus.originAgentVanlineCode'].value='';
			    }
			    if(vendorId!=''){ 
				    <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
				    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode=" + encodeURI(vendorId);
				    </configByCorp:fieldVisibility>
				    <configByCorp:fieldVisibility componentId="component.field.vanline">
					<c:if test="${agentSearchValidation}">
				    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=CRNOTALLOWED&partnerCode=" + encodeURI(vendorId);
				    	 </c:if>
				    	 <c:if test="${!agentSearchValidation}">
					    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
					    	 </c:if>
				    </configByCorp:fieldVisibility>
				    xhttp.open("GET", url, true);
				    xhttp.onreadystatechange = handleHttpResponse8;
				    xhttp.send(null);
			    } 	 }
	</c:if>
	<c:if test="${isNetworkBookingAgent!=true && !networkAgent}">
    var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
    if(vendorId == ''){
    	document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value='';
    }
	var job = document.forms['trackingStatusForm'].elements['serviceOrder.job'].value; 
    if(job=='DOM'||job=='UVL'||job=='MVL'){    
    	document.forms['trackingStatusForm'].elements['trackingStatus.originAgentVanlineCode'].value='';
    }
    if(vendorId!=''){
	    <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode=" + encodeURI(vendorId);
	    </configByCorp:fieldVisibility>
	    <configByCorp:fieldVisibility componentId="component.field.vanline">
		<c:if test="${agentSearchValidation}">
    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=CRNOTALLOWED&partnerCode=" + encodeURI(vendorId);
    	 </c:if>
    	 <c:if test="${!agentSearchValidation}">
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    	 </c:if>
	    </configByCorp:fieldVisibility>
	    xhttp.open("GET", url, true);
	    xhttp.onreadystatechange = handleHttpResponse8;
	    xhttp.send(null);
    }
    </c:if>
} 
function  checkNetworkPartnerName(){
	
<c:if test="${isNetworkBookingAgent==true || networkAgent}">

	 var networkAgentExSO=document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value;
		if(networkAgentExSO!=''){
			var shipnumberTS='${trackingStatus.shipNumber}'	
			var url="getAllInvoicedLinkedAccountLines.html?ajax=1&decorator=simple&popup=true&shipNumber="+shipnumberTS+"&linkedshipnumberTS="+networkAgentExSO+"&contractTypeTS=${trackingStatus.contractType}"; 
		    http2.open("GET", url, true);
		    http2.onreadystatechange = handleHttpResponseAllInvoicedLinkedAccountLines;
		    http2.send(null); 
			} 
   else{ 		
           var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
			    if(vendorId == ''){
			    	document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerName'].value='';
			    } 
			    if(vendorId!=''){ 
				    var url="networkPartnerName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode=" + encodeURI(vendorId); 
				    http2.open("GET", url, true);
				    http2.onreadystatechange = handleHttpResponseNetworkPartnerName;
				    http2.send(null);
			    } }
    </c:if>
	<c:if test="${isNetworkBookingAgent!=true && !networkAgent}">
	var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
    if(vendorId == ''){
    	document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerName'].value='';
    } 
    if(vendorId!=''){ 
	    var url="networkPartnerName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode=" + encodeURI(vendorId); 
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponseNetworkPartnerName;
	    http2.send(null);
    }
	</c:if>
	} 
function handleHttpResponseAllInvoicedLinkedAccountLines(){
	if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim(); 
                if(results=='true'){ 
                	agree= confirm("Changing the Network will remove the current RedSky Network partner link");
        			if(agree){
        				var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
        			    if(vendorId == ''){
        			    	document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerName'].value='';
        			    } if(vendorId!=''){ 
        				    var url="networkPartnerName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode=" + encodeURI(vendorId); 
        				    http2.open("GET", url, true);
        				    http2.onreadystatechange = handleHttpResponseNetworkPartnerName;
        				    http2.send(null);
        			    }
               }else{
                  document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value='${trackingStatus.networkPartnerCode}';
                  document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerName'].value="${trackingStatus.networkPartnerName}";
               } 
               	}else {
               	     alert("You can not de-link the order as account line already invoiced in Harmony instance." ); 
               	  document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value='${trackingStatus.networkPartnerCode}';
                  document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerName'].value="${trackingStatus.networkPartnerName}";
			   }  } }
function handleHttpResponseNetworkPartnerName(){
	if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
           				document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerName'].value = res[1];
           				document.forms['trackingStatusForm'].elements['trackingStatus.networkEmail'].value = res[4]; 
	           		}else{
	           			alert("Network code is not approved" ); 
					    document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerName'].value="";
					    document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value="";
					    document.forms['trackingStatusForm'].elements['trackingStatus.networkEmail'].value ="";
				 	    document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].select(); 
	           		}
               	}else {
               	     alert("Network code is not valid" ); 
                 	 document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerName'].value="";
                 	 document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value = "";
                 	 document.forms['trackingStatusForm'].elements['trackingStatus.networkEmail'].value = "";
				 	 document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select(); 
			   }  } }

function vendorCodeForActgCode(){
    var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
    if(vendorId==''){
    	document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value="";
    }
    if(vendorId!=''){ 
     var url="vendorNameForActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse222;
     http2.send(null);
    } } 
function handleHttpResponse222(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.size() >= 2){ 
                	if(res[3] == 'Approved'){
                	if(res[2] != "null" && res[2] !=''){
                	document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value = res[1];
                	document.forms['trackingStatusForm'].elements['trackingStatus.actgCode'].value = res[2]; 
                	document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
                	} 
                	else            	{
                	alert("Origin Agent code Approved but missing Accounting Code" ); 
                	document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value="";
           			document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value = "";
                	document.forms['trackingStatusForm'].elements['trackingStatus.actgCode'].value = ""; 
                	document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
				 	showAddressImage();
                	}
           		}else{
           			alert("Origin Agent code is not approved" ); 
				    document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value="";
				 	document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value="";
				 	document.forms['trackingStatusForm'].elements['trackingStatus.actgCode'].value="";
				 	document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
				 	showAddressImage();
           		}
       }else{
            alert("Origin Agent code not valid" );
			document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value="";
			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value="";
			document.forms['trackingStatusForm'].elements['trackingStatus.actgCode'].value="";
			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
			showAddressImage();
			copyOADADetail("OA");
	  } } }
function destCodeForActgCode(){
var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
    if(vendorId==''){
     	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="";
     }
    if(vendorId!=''){ 
	    var url="vendorNameForActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId);
	    http3.open("GET", url, true);
	    http3.onreadystatechange = handleHttpResponse22;
	    http3.send(null);
    } } 
function handleHttpResponse22(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.size() >= 2){
	                if(res[3] == 'Approved'){
	               if(res[2] != "null" && res[2] !=''){
	                     document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value = res[1];
                	     document.forms['trackingStatusForm'].elements['trackingStatus.actgCodeForDis'].value = res[2];
                	     document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
	                }  else{
	                    alert("Destination Agent code Approved but missing Accounting Code" ); 
	                    document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value="";
	           			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value = "";
                		document.forms['trackingStatusForm'].elements['trackingStatus.actgCodeForDis'].value = ""; 
                		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
                		showAddressImage1();
                		}
	           		}else{
	           			alert("Destination Agent code is not approved" ); 
					    document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value=""; 
				 		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value="";
				 		document.forms['trackingStatusForm'].elements['trackingStatus.actgCodeForDis'].value="";
				 		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
				 		showAddressImage1();
	           		}  
                }else{
                    alert("Destination Agent code not valid" );
				 	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value=""; 
				 	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value="";
				 	document.forms['trackingStatusForm'].elements['trackingStatus.actgCodeForDis'].value="";
				 	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
				    showAddressImage1();
				    copyOADADetail('DA');
			   }  }}
function checkVendorNameDest(){
	 <c:if test="${isNetworkBookingAgent==true || networkAgent}"> 
	 var savedDestinationAgentExSo='${trackingStatus.destinationAgentExSO}';
	 var destinationAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value;
		var savedAgent='${trackingStatus.destinationAgentCode}';
		var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
	 
		var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}

		if(destinationAgentExSo!='' && destinationAgentExSo == savedDestinationAgentExSo && !sameCompanyCodeflag){
			agree= confirm("Changing the agent will remove the current RedSky Network partner link");
			if(agree){
				vendorChangeStatus='YES';
				var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
			    if(vendorId == ''){
			    	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="";
			    }
			    var job = document.forms['trackingStatusForm'].elements['serviceOrder.job'].value; 
			    if(job=='DOM'||job=='UVL'||job=='MVL'){ 
			    	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentVanlineCode'].value="";
			    }	
			    if(vendorId != ''){
			        <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
				    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode=" + encodeURI(vendorId);
				    </configByCorp:fieldVisibility>
				    <configByCorp:fieldVisibility componentId="component.field.vanline">
				    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
				    </configByCorp:fieldVisibility>
				    http3.open("GET", url, true);
				    http3.onreadystatechange = handleHttpResponse9;
				    http3.send(null);
				}
			}else{
				document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value='${trackingStatus.destinationAgentCode}';
				document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="${trackingStatus.destinationAgent}";
				document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentContact"].value="${trackingStatus.destinationAgentContact}";
				document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentEmail"].value="${trackingStatus.destinationAgentEmail}";
				document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentPhoneNumber"].value='${trackingStatus.destinationAgentPhoneNumber}';
			}
		}else{
			if(!sameCompanyCodeflag){
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value='';
			}
			var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
		    if(vendorId == ''){
		    	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="";
		    }
		    var job = document.forms['trackingStatusForm'].elements['serviceOrder.job'].value; 
		    if(job=='DOM'||job=='UVL'||job=='MVL'){ 
		    	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentVanlineCode'].value="";
		    }	
		    if(vendorId != ''){
		        <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
			    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode=" + encodeURI(vendorId);
			    </configByCorp:fieldVisibility>
			    <configByCorp:fieldVisibility componentId="component.field.vanline">
				<c:if test="${agentSearchValidation}">
			    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=CRNOTALLOWED&partnerCode=" + encodeURI(vendorId);
			    	  </c:if> 
			    	  <c:if test="${!agentSearchValidation}">
				    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
				    	  </c:if>
			    </configByCorp:fieldVisibility>
			    http3.open("GET", url, true);
			    http3.onreadystatechange = handleHttpResponse9;
			    http3.send(null);
		} }
	  </c:if> 
	  <c:if test="${isNetworkBookingAgent==false && !networkAgent}">
  var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
   if(vendorId == ''){
   	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="";
   }
   var job = document.forms['trackingStatusForm'].elements['serviceOrder.job'].value; 
   if(job=='DOM'||job=='UVL'||job=='MVL'){ 
   	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentVanlineCode'].value="";
   }	
   if(vendorId != ''){
       <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode=" + encodeURI(vendorId);
	    </configByCorp:fieldVisibility>
	    <configByCorp:fieldVisibility componentId="component.field.vanline">
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    </configByCorp:fieldVisibility>
	    http3.open("GET", url, true);
	    http3.onreadystatechange = handleHttpResponse9;
	    http3.send(null);
	}	
   </c:if>  
}
function checkBroker(){
    var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].value;
    if(vendorId ==''){
    	//document.forms['trackingStatusForm'].elements['trackingStatus.brokerName'].value="";
    }
    if(vendorId !=''){
    	<configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=VN&partnerCode=" + encodeURI(vendorId);
	    </configByCorp:fieldVisibility>
	    <configByCorp:fieldVisibility componentId="component.field.vanline">
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    </configByCorp:fieldVisibility>
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse90;
	    http2.send(null);
	}	 } 
function getContact(agentType,partnerCode){
	if(agentType.length > 0 && partnerCode.length >0){
		openWindow('userContacts.html?agentType='+agentType+'&partnerCode='+partnerCode+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=trackingStatus.originAgentVanlineCode&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode',1000,550);
	} } 

function winOpenOriginNew(){
	<c:if test="${isNetworkBookingAgent==true || networkAgent}">
	 var savedOriginAgentExSO='${trackingStatus.originAgentExSO}';
	 var originAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value;

		if(originAgentExSo !='' && savedOriginAgentExSO == originAgentExSo){
			agree= confirm("Changing the agent will remove the current RedSky Network partner link");
			if(agree){
				vendorChangeStatus='YES';
				<configByCorp:fieldVisibility componentId="component.field.vanline">
				if('${serviceOrder.job}' =='DOM' || '${serviceOrder.job}' =='UVL' || '${serviceOrder.job}'=='MVL'){
			 		openWindow('originPartnersVanlineNew.html?flag=6&origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=trackingStatus.originAgentVanlineCode&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode&billToCode=${billing.billToCode}',1000,550);	
			    }else{
			    	openWindow('originPartnersNew.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode&billToCode=${billing.billToCode}');	
			    } 
			    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
		    </configByCorp:fieldVisibility>
		    <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
		    	openWindow('originPartnersNew.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode&billToCode=${billing.billToCode}');	
		 
		    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
		    </configByCorp:fieldVisibility>
			} 
		}else{
			searchFlag='TRUE';
			<configByCorp:fieldVisibility componentId="component.field.vanline">
			if('${serviceOrder.job}' =='DOM' || '${serviceOrder.job}' =='UVL' || '${serviceOrder.job}'=='MVL'){
		 		openWindow('originPartnersVanlineNew.html?flag=6&origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=trackingStatus.originAgentVanlineCode&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode&billToCode=${billing.billToCode}',1000,550);	
		    }else{
		    	openWindow('originPartnersNew.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode&billToCode=${billing.billToCode}');	
		    } 
		    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
	    </configByCorp:fieldVisibility>
	    <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
	    	openWindow('originPartnersNew.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode&billToCode=${billing.billToCode}');	
	 
	    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
	    </configByCorp:fieldVisibility>
		}
	</c:if>
	 <c:if test="${isNetworkBookingAgent!=true &&  !networkAgent}">
	<configByCorp:fieldVisibility componentId="component.field.vanline">
		if('${serviceOrder.job}' =='DOM' || '${serviceOrder.job}' =='UVL' || '${serviceOrder.job}'=='MVL'){
	 		openWindow('originPartnersVanlineNew.html?flag=6&origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=trackingStatus.originAgentVanlineCode&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode&billToCode=${billing.billToCode}',1000,550);	
	    }else{
	    	openWindow('originPartnersNew.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode&billToCode=${billing.billToCode}');	
	    } 
	    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
  </configByCorp:fieldVisibility>
  <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
  	openWindow('originPartnersNew.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode&billToCode=${billing.billToCode}');	
  document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
  </configByCorp:fieldVisibility>
  </c:if>
}
function winOpenOriginGiven(){
	openWindow('originPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originGivenName&fld_code=trackingStatus.originGivenCode');	
} 

function winOpenOriginReceived(){
	openWindow('originPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originReceivedName&fld_code=trackingStatus.originReceivedCode');	
}
function winOpenDestinationGiven(){
	openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fourthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationGivenName&fld_code=trackingStatus.destinationGivenCode');	
}
function winOpenDestinationReceived(){
	openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fourthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationReceivedName&fld_code=trackingStatus.destinationReceivedCode');	
}

function winOpenPartnerNetwork (){
<c:if test="${isNetworkBookingAgent==true || networkAgent}">
	var savedNetworkAgentExSO='${trackingStatus.networkAgentExSO}';
	 var networkAgentExSO=document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value;
		if(networkAgentExSO!='' && networkAgentExSO == savedNetworkAgentExSO){
			agree= confirm("Changing the Network will remove the current RedSky Network partner link");
			if(agree){
			 openWindow('networkPartnersPopUp.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.networkEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.networkPartnerName&fld_code=trackingStatus.networkPartnerCode');
			 }
        } else{
            openWindow('networkPartnersPopUp.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.networkEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.networkPartnerName&fld_code=trackingStatus.networkPartnerCode');
        }
 </c:if>
 <c:if test="${isNetworkBookingAgent!=true && !networkAgent}">
      openWindow('networkPartnersPopUp.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.networkEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.networkPartnerName&fld_code=trackingStatus.networkPartnerCode');
  </c:if>
} 
 function winOpenOriginActg(){ 
 <sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
 	openWindow('originForActgCode.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode&fld_seventhDescription=trackingStatus.actgCode');	
    document.forms['trackingStatusForm'].elements['trackingStatus.actgCode'].select();
   </sec-auth:authComponent>  
    } 

function winOpenDestActg(){ 
 <sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
	openWindow('destinationForActgCode.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode&fld_seventhDescription=trackingStatus.actgCodeForDis');
	document.forms['trackingStatusForm'].elements['trackingStatus.actgCodeForDis'].select();
     </sec-auth:authComponent>
    }

function winOpenBroker(){
 <sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
	openWindow('vendorName.html?partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.brokerEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.brokerName&fld_code=trackingStatus.brokerCode');
	document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].select();
	 </sec-auth:authComponent>
}
var httpValidate = getHTTPObjectValidate();
function getHTTPObjectValidate(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}       
function validateListForOrigingAgent(){ 
 if(document.forms['trackingStatusForm'].elements['validateFlagTrailer'].value=='OK'){
        var parentId=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
        var deliveryLastDay=document.forms['trackingStatusForm'].elements['trackingStatus.deliveryLastDay'].value;                
	    var url="controlExpirationsListInDomestic.html?ajax=1&decorator=simple&popup=true&parentId=" + encodeURI(parentId)+"&deliveryLastDay=" + encodeURI(deliveryLastDay);
	    httpValidate.open("GET", url, true);
	    httpValidate.onreadystatechange = handleHttpResponse;
	    httpValidate.send(null);	    
	      } }
function handleHttpResponse(){
		if (httpValidate.readyState == 4){
                var results = httpValidate.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                               
                if(res !=''){        
                var totalElement= res.indexOf(',');                
                if(totalElement>0)  {
                var res = res.split(",");
                var res11 = res[0].split("#");
                var res22 = res[1].split("#");  
                var res33 = "";
                if(res[2] != undefined)  {
                	res33 = res[2].split("#");
                }
                var ExpiryFor = "";
                 if(res11[1] != "") {
                 	ExpiryFor = ExpiryFor+"\n"+res11[1]+":   "+res11[2];
                 }
                 if(res22[1] != "") {
                 	ExpiryFor = ExpiryFor+"\n"+res22[1]+":   "+res22[2];
                 }
                 if(res33[1] != "" &&  res33[1] != undefined) {
                 	ExpiryFor = ExpiryFor+"\n"+res33[1]+":   "+res33[2];
                 }
                if(res11[0] =='Yes' || res22[0].trim() =='Yes') {
                 alert("This Origin Agent cannot be selected due to the folllowing expirations: "+ExpiryFor);
                 document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value="";
                 document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value="";
			     document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
                 }else{
                 var agree =confirm("This Origin Agent has the following expirations: "+ExpiryFor+"\nDo you wish to proceed?");                 
                 if(agree) 
			     {
			     }  else  { 
			         document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value="";	
			         document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value="";		        
			         document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
			     } }  } 
                 else{
                 var res = res.split("#");
                 var ExpiryFor = ""; 
                 ExpiryFor = ExpiryFor+"\n"+res[1]+":   "+res[2];               
                 if(res[0] =='Yes') {
                 alert("This Origin Agent cannot be selected due to the expirations: "+ExpiryFor);
                 document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value="";	
                 document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value="";		     
			     document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();			     
                 }else{                 
                 var agree =confirm("This Origin Agent has expirations: "+ExpiryFor+"\nDo you wish to proceed?");
			     if(agree) 
			     { } else { 
			         document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value="";
			         document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value="";
			         document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();			         
			     } } }}  }
      document.forms['trackingStatusForm'].elements['validateFlagTrailer'].value='';
      }
	function okValidateListFromTrailer(){
	document.forms['trackingStatusForm'].elements['validateFlagTrailer'].value='OK';
	}
	function copyOADADetail(agentType){
		<configByCorp:fieldVisibility componentId="component.trackingStatus.copyOADADetail">
		if(document.getElementById("hidCopyOADetailImage")!=null || document.getElementById("hidCopyDADetailImage")!=null){
		if(agentType=="OA"){
			var originAgentCode=document.forms["trackingStatusForm"].elements["trackingStatus.originAgentCode"].value;
		if(originAgentCode==""){
			document.getElementById("hidCopyOADetailImage").style.display="none";
		}
		else{
			document.getElementById("hidCopyOADetailImage").style.display="block";
				}
		}
		else if(agentType=="DA"){
			var destinationAgentCode=document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentCode"].value;
		if(destinationAgentCode==""){
			document.getElementById("hidCopyDADetailImage").style.display="none";
				}
		else{
			document.getElementById("hidCopyDADetailImage").style.display="block";
				}
		}
		}
		</configByCorp:fieldVisibility>
		//getEmailFromPortalUser(agentType);
		}
	function handleHttpResponse8(){
		<configByCorp:fieldVisibility componentId="component.field.trackingstatus.classifiedAgent">		
			<c:set var="classifiedAgentOriginValidation" value="Y" />
		</configByCorp:fieldVisibility>	
		if (xhttp.readyState == 4){
	                var results = xhttp.responseText
	                results = results.trim();
	                var res = results.split("#");
	                if(res.length>2){
	                	if(res[2] == 'Approved'){
	                		<c:if test="${classifiedAgentOriginValidation=='Y'}">
	                		if(res[11]=='' && res[8]=='T'){
	                			alert("This Agent cannot be processed as it is not classified.")
	                			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].value ='';
			           			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value ='';
							    document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value="";
							    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value="";
						 	    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
	                			return false;
	                		}else if(res[11]!='' && res[8]=='T' && res[12]!=''){
	                			var check = res[12].trim().indexOf('${serviceOrder.companyDivision}');
	                	 		if(check > -1){
	                	 			document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value = res[1];
			           				document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value = res[4];
			           				document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
			           				validateListForOrigingAgent();
			           				setContactEmail('OA');
			           				<configByCorp:fieldVisibility componentId="component.trackingstatus.field.agentsearch.clientdirective">
			           				validPreferredAgentOrigin();
			           				</configByCorp:fieldVisibility>
	                	 		}else{
			           				alert("This Agent cannot be processed as it is not classified for '${serviceOrder.companyDivision}' Company division.")
		                			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].value ='';
				           			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value ='';
								    document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value="";
								    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value="";
							 	    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
		                			return false;
	                	 		}
	                		}else{
	                			document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value = res[1];
		           				document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value = res[4];
		           				document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
		           				validateListForOrigingAgent();
		           				setContactEmail('OA');
		           				<configByCorp:fieldVisibility componentId="component.trackingstatus.field.agentsearch.clientdirective">
		           				validPreferredAgentOrigin();
		           				</configByCorp:fieldVisibility>
	                		}
	                		</c:if>
	                		<c:if test="${classifiedAgentOriginValidation!='Y'}">
	                			document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value = res[1];
		           				document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value = res[4];
		           				document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
		           				validateListForOrigingAgent();
		           				setContactEmail('OA');
		           			</c:if>
	                		
		           		}else{
		           			alert("Origin Agent code is not approved" ); 
		           			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].value ='';
		           			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value ='';
						    document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value="";
						    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value="";
					 	    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
					 	    showAddressImage();
		           		}
	               	}else{
	                     alert("Origin Agent code not valid" );
	                     document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].value ='';
	                     document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value ='';
	                 	 document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value="";
	                 	 document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value="";
	                 	document.getElementById("hidActiveAP").style.display="none";
	                 	if(document.getElementById("hidAP")!=null)	{
	        				document.getElementById("hidAP").style.display="none"
	        			}
	        			if(document.getElementById("hidRUC")!=null)	{
	        			document.getElementById("hidRUC").style.display="none";
	        			}
	                	document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContactImgFlag'].value='';
					 	 document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
					 	 showAddressImage();
					 	copyOADADetail("OA");
				   }    } }
	function handleHttpResponse9(){
		<configByCorp:fieldVisibility componentId="component.field.trackingstatus.classifiedAgent">		
		<c:set var="classifiedAgentDestValidation" value="Y" />
		</configByCorp:fieldVisibility>		
		if (http3.readyState == 4){
	                var results = http3.responseText
	                results = results.trim();
	                var res = results.split("#"); 
	                if(res.length>2){
	                	if(res[2] == 'Approved'){
	                		<c:if test="${classifiedAgentDestValidation=='Y'}">
	                		if(res[11]=='' && res[8]=='T'){
	                			alert("This Agent cannot be processed as it is not classified.")
	                			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="";
							    document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value="";
						 		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
	                			return false;
	                		}else if(res[11]!='' && res[8]=='T' && res[12]!=''){
	                			var check = res[12].trim().indexOf('${serviceOrder.companyDivision}');
	                	 		if(check > -1){
	                	 			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value = res[1];
				           			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].value = res[4];
				           			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
			           				validateListForDestinAgent();
				           			setContactEmail('DA');
				           			<configByCorp:fieldVisibility componentId="component.trackingstatus.field.agentsearch.clientdirective">
			           				validPreferredAgentDestin();
			           				</configByCorp:fieldVisibility>
	                	 		}else{
			           				alert("This Agent cannot be processed as it is not classified for '${serviceOrder.companyDivision}' Company division.")
		                			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="";
							    	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value="";
						 			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
		                			return false;
	                	 		}
	                		}else{
	                			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value = res[1];
			           			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].value = res[4];
			           			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
		           				validateListForDestinAgent();
			           			setContactEmail('DA');
			           			<configByCorp:fieldVisibility componentId="component.trackingstatus.field.agentsearch.clientdirective">
		           				validPreferredAgentDestin();
		           				</configByCorp:fieldVisibility>
	                		}
	                		</c:if>
	                		<c:if test="${classifiedAgentDestValidation!='Y'}">
	                		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value = res[1];
		           			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].value = res[4];
		           			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
		           			validateListForDestinAgent();
		           			setContactEmail('DA');
		           			</c:if>
	                	}else{
		           			alert("Destination Agent code is not approved" ); 
						    document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="";
						    document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value="";
					 		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
					 		showAddressImage1();
		           		}  
	               	}else{
	                     alert("Destination Agent code not valid" );
	                     document.getElementById("hidDestinActiveAP").style.display="none";	
	                     if(document.getElementById("hidDestAP")!=null)	{
	         				document.getElementById("hidDestAP").style.display="none"
	         			}
	          			if(document.getElementById("hidDestRUC")!=null)	{
	         			document.getElementById("hidDestRUC").style.display="none";
	         			}                              
	                 	 document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContactImgFlag'].value='';
	                 	 document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="";
	                 	 document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value="";
					 	 document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
					 	 showAddressImage1();
					 	copyOADADetail("DA");
				   }   }  }
	function handleHttpResponse90(){
				 
		if (http2.readyState == 4){
	                var results = http2.responseText
	                results = results.trim();
	                var res = results.split("#"); 
	                if(res.length>2){
	                	if(res[2] == 'Approved'){
	                		document.forms['trackingStatusForm'].elements['trackingStatus.brokerName'].value = res[1];
		           			document.forms['trackingStatusForm'].elements['trackingStatus.brokerEmail'].value = res[4];
	                   		document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].select();
	                   		validateListForBroker();
	                	}else{
		           			alert("Broker code is not approved" ); 
						    document.forms['trackingStatusForm'].elements['trackingStatus.brokerName'].value="";
						    document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].value="";
						    document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].select();
						    showAddressImage2();
		           		}  
	               	}else{
	                     alert("Broker code not valid" );
	                 	 document.forms['trackingStatusForm'].elements['trackingStatus.brokerName'].value="";
						 document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].value="";
						 document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].select();
						 showAddressImage2();
	                } } } 
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
    var xhttp = getHTTPObject();
    var xhttp2 = getHTTPObject();
    var http2 = getHTTPObject();
    var http55 = getHTTPObject();
    var http5 = getHTTPObject(); 
    var http22 = getHTTPObject1(); 
var http = getHTTPObjectPA();
function getHTTPObjectPA(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
} 
function getHTTPObject1() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
    var http3 = getHTTPObject1();

function getHTTPObject4() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
    var http4 = getHTTPObject4();
 function showAddressImage(){
     var agentCode=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value; 
     if(agentCode==''){ 
     document.getElementById("hidImage").style.display="none";
     document.getElementById("hidImagePlus").style.display="none";
     } else if(agentCode!='') {
      document.getElementById("hidImage").style.display="block";
      document.getElementById("hidImagePlus").style.display="block";
     } }
  function showEmailImage() {
     var agentEmailCode=document.forms['trackingStatusForm'].elements['trackingStatus.bookingAgentEmail'].value; 
     if(agentEmailCode==''){ 
     document.getElementById("hidEmailImage").style.display="none";
     }  else if(agentEmailCode!=''){
      document.getElementById("hidEmailImage").style.display="block";
     } }
 function showOriginAgentEmailImage() {
     var agentEmailCode=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value; 
     var flagtr=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContactImgFlag'].value; 
     if(flagtr=='true' && document.getElementById("hidActiveAP").style.display!=null){
    	 document.getElementById("hidActiveAP").style.display="block";
     }
     if((flagtr==null || flagtr=='') && document.getElementById("hidActiveAP").style.display!=null){
    	 document.getElementById("hidActiveAP").style.display="none";
     } 
     if(agentEmailCode=='') { 
     document.getElementById("hidOriginAgentEmailImage").style.display="none";
     }
     else if(agentEmailCode!=''){
      document.getElementById("hidOriginAgentEmailImage").style.display="block";
     } }
 function showNetworkEmailImage() {
     var agentEmailCode=document.forms['trackingStatusForm'].elements['trackingStatus.networkEmail'].value;  
     if(agentEmailCode=='') { 
     	document.getElementById("hidNetworkEmailImage").style.display="none";
     } else if(agentEmailCode!=''){
      document.getElementById("hidNetworkEmailImage").style.display="block";
     }   }
 function showDestinationAgentEmailImage() { 
     var agentEmailCode=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].value; 
     var flagtr1=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContactImgFlag'].value;    
     if(flagtr1=='true' && document.getElementById("hidDestinActiveAP")!=null){        
     	 document.getElementById("hidDestinActiveAP").style.display="block";
      }
      if((flagtr1==null || flagtr1=='') && document.getElementById("hidDestinActiveAP")!=null){         
      	 document.getElementById("hidDestinActiveAP").style.display="none";
       }
      if(agentEmailCode==''){ 
     document.getElementById("hidDestinationAgentEmailImage").style.display="none";
     }
     else if(agentEmailCode!='')
     {
      document.getElementById("hidDestinationAgentEmailImage").style.display="block";
     }   }
  function showSubOriginAgentEmailImage() {
     var agentEmailCode=document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentEmail'].value; 
     var flagtr2=document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContactImgFlag'].value;
     if(flagtr2=='true' && document.getElementById("hidSubOrigActiveAP")!=null){
    	 document.getElementById("hidSubOrigActiveAP").style.display="block";
     }
     if((flagtr2==null || flagtr2=='') && document.getElementById("hidSubOrigActiveAP")!=null){
    	 document.getElementById("hidSubOrigActiveAP").style.display="none";
     }
     if(agentEmailCode=='')  { 
     document.getElementById("hidSubOriginAgentEmailImage").style.display="none";
     }  else if(agentEmailCode!='') {
      document.getElementById("hidSubOriginAgentEmailImage").style.display="block";
     } }
  function showSubDestinationAgentEmailImage() {
     var showSubDestinationAgentEmail=document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value;
     var flagtr3=document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentImgFlag'].value;
      if(flagtr3=='true' && document.getElementById("hidSubDestinActiveAP")!=null){
    	 document.getElementById("hidSubDestinActiveAP").style.display="block";
     }
     if((flagtr3==null || flagtr3=='') && document.getElementById("hidSubDestinActiveAP")!=null){
    	 document.getElementById("hidSubDestinActiveAP").style.display="none";
     } 
     if(showSubDestinationAgentEmail=='')  { 
     document.getElementById("hidSubDestinationAgentEmailImage").style.display="none";
     }
     else if(showSubDestinationAgentEmail!='')  {
      document.getElementById("hidSubDestinationAgentEmailImage").style.display="block";
     } }
  function showBrokerEmailImage() {
     var agentEmailCode=document.forms['trackingStatusForm'].elements['trackingStatus.brokerEmail'].value; 
     if(agentEmailCode=='') { 
     document.getElementById("hidBrokerEmailImage").style.display="none";
     }
     else if(agentEmailCode!='') {
      document.getElementById("hidBrokerEmailImage").style.display="block";
     }   }
 function showForwarderEmailImage() {
     var agentEmailCode=document.forms['trackingStatusForm'].elements['trackingStatus.forwarderEmail'].value; 
     if(agentEmailCode=='') { 
     document.getElementById("hidForwarderEmailImage").style.display="none";
     }
     else if(agentEmailCode!='') {
      document.getElementById("hidForwarderEmailImage").style.display="block";
     } }
 function showAddressForwarderCodeImage(){
 	 var agentCode=document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].value;
     if(agentCode==''){ 
     document.getElementById("hidImageforwarderCode").style.display="none";
     document.getElementById("hidImageforwarderCodePlus").style.display="none";
     }else if(agentCode!=''){
      document.getElementById("hidImageforwarderCode").style.display="block";
      document.getElementById("hidImageforwarderCodePlus").style.display="block";
     } }
  function showAddressImage1()  {
     var agentCode=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
     if(agentCode=='') { 
     document.getElementById("hidImage1").style.display="none";
     document.getElementById("hidImagePlus1").style.display="none";
     }
     else if(agentCode!='') {
      document.getElementById("hidImage1").style.display="block";
      document.getElementById("hidImagePlus1").style.display="block";
     } }
  function showAddressImage2() {
     var agentCode=document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].value;
     if(agentCode=='')  { 
     document.getElementById("hidImage2").style.display="none";
     document.getElementById("hidImagePlus2").style.display="none";
     } else if(agentCode!='') {
      document.getElementById("hidImage2").style.display="block";
      document.getElementById("hidImagePlus2").style.display="block";
     } }
 function showAddressImageSub1() {
     var agentCode=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
     if(agentCode=='') { 
     document.getElementById("hidImageSub1").style.display="none";
     document.getElementById("hidImageSubPlus1").style.display="none";
     } else if(agentCode!='')  {
      document.getElementById("hidImageSub1").style.display="block";
      document.getElementById("hidImageSubPlus1").style.display="block";
     } }
 function showAddressImageSub2(){
     var agentCode=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
     if(agentCode==''){ 
     document.getElementById("hidImageSub2").style.display="none";
     document.getElementById("hidImageSubPlus2").style.display="none";
     }else if(agentCode!=''){
      document.getElementById("hidImageSub2").style.display="block";
      document.getElementById("hidImageSubPlus2").style.display="block";
     } }
 function checkVendorNameSub(){
	 <c:if test="${isNetworkBookingAgent==true || isNetworkOriginAgent==true || networkAgent}">
	 var savedOriginSubAgentExSo='${trackingStatus.originSubAgentExSO}';
	 var originSubAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value;
		var savedAgent='${trackingStatus.originSubAgentCode}';
		var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
	 
		var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}
		if(originSubAgentExSo!='' && originSubAgentExSo == savedOriginSubAgentExSo && !sameCompanyCodeflag){
			agree= confirm("Changing the agent will remove the current RedSky Network partner link");
			if(agree){
				vendorChangeStatus='YES';
				var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
			    if(vendorId == ''){
			    	document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value='';
			    }
			    if(vendorId!=''){
					<c:if test="${agentSearchValidation}">
                 var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=CRNOTALLOWED&partnerCode=" + encodeURI(vendorId);
             	</c:if>
            	<c:if test="${!agentSearchValidation}">
                var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
            	</c:if>
				    xhttp2.open("GET", url, true);
				    xhttp2.onreadystatechange = handleHttpResponse88;
				    xhttp2.send(null);
			    }
			}else{
				document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value='${trackingStatus.originSubAgentCode}';
				document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value="${trackingStatus.originSubAgent}";
				document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContact'].value="${trackingStatus.subOriginAgentContact}";
				document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentEmail'].value="${trackingStatus.subOriginAgentEmail}";
				document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentPhoneNumber'].value='${trackingStatus.subOriginAgentPhoneNumber}';
			}
		}else{
			if(!sameCompanyCodeflag){
			document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value='';
			}
			var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
		    if(vendorId == ''){
		    	document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value='';
		    }
		    if(vendorId!=''){ 
		    	<c:if test="${agentSearchValidation}">
                 var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=CRNOTALLOWED&partnerCode=" + encodeURI(vendorId);
             	</c:if>
            	<c:if test="${!agentSearchValidation}">
                var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
            	</c:if>
			    xhttp2.open("GET", url, true);
			    xhttp2.onreadystatechange = handleHttpResponse88;
			    xhttp2.send(null);
		    } }
	</c:if>
	<c:if test="${isNetworkBookingAgent==false && isNetworkOriginAgent!=true && !networkAgent}">
    var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
    if(vendorId == ''){
    	document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value='';
    }
    if(vendorId!=''){ 
    	<c:if test="${agentSearchValidation}">
        var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=CRNOTALLOWED&partnerCode=" + encodeURI(vendorId);
    	</c:if>
   	<c:if test="${!agentSearchValidation}">
       var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
   	</c:if>
	    xhttp2.open("GET", url, true);
	    xhttp2.onreadystatechange = handleHttpResponse88;
	    xhttp2.send(null);
    }
	</c:if>
}
function handleHttpResponse88(){
	<configByCorp:fieldVisibility componentId="component.field.trackingstatus.classifiedAgent">		
			<c:set var="classifiedAgentSubValidation" value="Y" />
	</configByCorp:fieldVisibility>		
	
	if (xhttp2.readyState == 4){
                var results = xhttp2.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
                		<c:if test="${classifiedAgentSubValidation=='Y'}">
                		if(res[11]=='' && res[8]=='T'){
                			alert("This Agent cannot be processed as it is not classified.")
                			document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value="";
    					    document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value="";
    					    document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
    					    return false;
                		}else if(res[11]!='' && res[8]=='T' && res[12]!=''){
                			var check = res[12].trim().indexOf('${serviceOrder.companyDivision}');
                	 		if(check > -1){
                	 			document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value = res[1];
                   				document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentEmail'].value = res[4];
                   				document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
                   				validateListForSubOaAg();
                   				setContactEmail('SOA');
                   			 	<configByCorp:fieldVisibility componentId="component.trackingstatus.field.agentsearch.clientdirective">
    	           				validPreferredAgentSubOA();
    	           				</configByCorp:fieldVisibility>
                	 		}else{
		           				alert("This Agent cannot be processed as it is not classified for '${serviceOrder.companyDivision}' Company division.")
	                			document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value="";
    					    	document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value="";
    					    	document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
	                			return false;
                	 		}
                		}else{
                			document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value = res[1];
               				document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentEmail'].value = res[4];
               				document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
               				validateListForSubOaAg();
               				setContactEmail('SOA');
               			 	<configByCorp:fieldVisibility componentId="component.trackingstatus.field.agentsearch.clientdirective">
	           				validPreferredAgentSubOA();
	           				</configByCorp:fieldVisibility>
                		}
                		</c:if>
                		<c:if test="${classifiedAgentSubValidation!='Y'}">
                		document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value = res[1];
           				document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentEmail'].value = res[4];
           				document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
           				validateListForSubOaAg();
           				setContactEmail('SOA');
           				</c:if>
	           		}else{
	           			alert("Sub Origin Agent code is not approved" ); 
					    document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value="";
					    document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value="";
					    document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
					    showAddressImageSub1();
	           		}
               	}else{
                     alert("Sub Origin Agent code not valid" );
                     document.getElementById("hidSubOrigActiveAP").style.display="none";
                     if(document.getElementById("hidsubOAP")!=null)	{
         				document.getElementById("hidsubOAP").style.display="none"
         			}
          			 if(document.getElementById("hidsubORUC")!=null)	{
         			document.getElementById("hidsubORUC").style.display="none";	
         			}
                 	document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContactImgFlag'].value='';
                 	 document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value="";
                 	 document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value="";
                 	 document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
                 	 showAddressImageSub1();
			   }  } }
function checkVendorNameDestSub(){ 
	 <c:if test="${isNetworkBookingAgent==true || isNetworkDestinationAgent==true || networkAgent}"> 
	 var savedDestinationSubAgentExSo='${trackingStatus.destinationSubAgentExSO}';
	 var destinationSubAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value;
		var savedAgent='${trackingStatus.destinationSubAgentCode}';
		var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
	 
		var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}
		if(destinationSubAgentExSo!='' && destinationSubAgentExSo == savedDestinationSubAgentExSo && !sameCompanyCodeflag){
			agree= confirm("Changing the agent will remove the current RedSky Network partner link");
			if(agree){
				vendorChangeStatus='YES';
				var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
			    if(vendorId == ''){
			    	document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value="";
			    }
			    if(vendorId != ''){
			    	  <c:if test="${agentSearchValidation}">
			            var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=CRNOTALLOWED&partnerCode=" + encodeURI(vendorId);
			             </c:if>
			             <c:if test="${!agentSearchValidation}">
			             var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
			               </c:if>
				    http3.open("GET", url, true);
				    http3.onreadystatechange = handleHttpResponse99;
				    http3.send(null);
				}
			}else{
				document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value='${trackingStatus.destinationSubAgentCode}';
				document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value="${trackingStatus.destinationSubAgent}";
				document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentAgentContact'].value="${trackingStatus.subDestinationAgentAgentContact}";
				document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value="${trackingStatus.subDestinationAgentEmail}";
				document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentPhoneNumber'].value='${trackingStatus.subDestinationAgentPhoneNumber}';
			}
		}else{
			if(!sameCompanyCodeflag){
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value='';
			}
			var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
		    if(vendorId == ''){
		    	document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value="";
		    }
		    if(vendorId != ''){
		    	  <c:if test="${agentSearchValidation}">
	            var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=CRNOTALLOWED&partnerCode=" + encodeURI(vendorId);
	             </c:if>
	             <c:if test="${!agentSearchValidation}">
	             var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	               </c:if>
			    http3.open("GET", url, true);
			    http3.onreadystatechange = handleHttpResponse99;
			    http3.send(null);
			} }
</c:if>
<c:if test="${isNetworkBookingAgent==false && isNetworkDestinationAgent!=true && !networkAgent}">
   var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
   if(vendorId == ''){
   	document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value="";
   }
   if(vendorId != ''){
	   <c:if test="${agentSearchValidation}">
       var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=CRNOTALLOWED&partnerCode=" + encodeURI(vendorId);
        </c:if>
        <c:if test="${!agentSearchValidation}">
        var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
          </c:if>
	    http3.open("GET", url, true);
	    http3.onreadystatechange = handleHttpResponse99;
	    http3.send(null);
	}	
 </c:if>
}
function handleHttpResponse99(){
	<configByCorp:fieldVisibility componentId="component.field.trackingstatus.classifiedAgent">		
	<c:set var="classifiedAgentDestSubValidation" value="Y" />
	</configByCorp:fieldVisibility>	
	if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
	           			
                		<c:if test="${classifiedAgentDestSubValidation=='Y'}">
                		if(res[11]=='' && res[8]=='T'){
                		alert("This Agent cannot be processed as it is not classified.")
                			document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value="";
    					    document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value="";
    					    document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value = "";
    					    document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
    					    showAddressImageSub2();
                			return false;
                		}else if(res[11]!='' && res[8]=='T' && res[12]!=''){
                			var check = res[12].trim().indexOf('${serviceOrder.companyDivision}');
                	 		if(check > -1){
                	 			document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value = res[1];
        	           			document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value = res[4];
        	           			document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
        	           			validateListForSubDaAg();
        	           			setContactEmail('SDA');
        	           			<configByCorp:fieldVisibility componentId="component.trackingstatus.field.agentsearch.clientdirective">
    	           				validPreferredAgentSubDA();
    	           				</configByCorp:fieldVisibility>
                	 		}else{
		           				alert("This Agent cannot be processed as it is not classified for '${serviceOrder.companyDivision}' Company division.")
	                			document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value="";
    					    	document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value="";
    					    	document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value = "";
    					    	document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
    					    	showAddressImageSub2();
	                			return false;
                	 		}
                		}else{
                			document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value = res[1];
    	           			document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value = res[4];
    	           			document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
    	           			validateListForSubDaAg();
    	           			setContactEmail('SDA');
    	           			<configByCorp:fieldVisibility componentId="component.trackingstatus.field.agentsearch.clientdirective">
	           				validPreferredAgentSubDA();
	           				</configByCorp:fieldVisibility>
                		}
                		</c:if>
                		<c:if test="${classifiedAgentDestSubValidation!='Y'}">
                		document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value = res[1];
	           			document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value = res[4];
	           			document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
	           			validateListForSubDaAg();
	           			setContactEmail('SDA');
	           			</c:if>
                	}else{
	           			alert("Sub Destination Agent code is not approved" ); 
					    document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value="";
					    document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value="";
					    document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
					    showAddressImageSub2();
	           		}  
               	}else{
                     alert("Sub Destination Agent code not valid" );
                     document.getElementById("hidSubDestinActiveAP").style.display="none";	
                     if(document.getElementById("hidsubDAP")!=null)	{
         				document.getElementById("hidsubDAP").style.display="none"
         			}
         			if(document.getElementById("hidsubDRUC")!=null)	{
         				document.getElementById("hidsubDRUC").style.display="none"
         			}
                  	 document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentImgFlag'].value='';
                 	 document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value="";
                 	 document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value="";
                 	 document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
                 	 showAddressImageSub2();
			   }  }  }
			   
			   
function checkOriginGiven(){
    var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.originGivenCode'].value;
    if(vendorId == ''){
    	document.forms['trackingStatusForm'].elements['trackingStatus.originGivenName'].value="";
    }
    if(vendorId != ''){
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse9110;
	    http2.send(null);
	}	 
}

function handleHttpResponse9110(){
	 if (http2.readyState == 4){
       var results = http2.responseText
       results = results.trim();
       var res = results.split("#");
       if(res.length>2){
       	if(res[2] == 'Approved'){
      			document.forms['trackingStatusForm'].elements['trackingStatus.originGivenName'].value = res[1];
          		document.forms['trackingStatusForm'].elements['trackingStatus.originGivenCode'].select();
       	}else{
      			alert("Origin Given code is not approved" ); 
			    document.forms['trackingStatusForm'].elements['trackingStatus.originGivenName'].value="";
			    document.forms['trackingStatusForm'].elements['trackingStatus.originGivenCode'].value="";
			    document.forms['trackingStatusForm'].elements['trackingStatus.originGivenCode'].select();
      		}  
      	}else{
	            alert("Origin Given code not valid" );
	        	document.forms['trackingStatusForm'].elements['trackingStatus.originGivenName'].value="";
				document.forms['trackingStatusForm'].elements['trackingStatus.originGivenCode'].value="";
				document.forms['trackingStatusForm'].elements['trackingStatus.originGivenCode'].select();
       } } }
			   
function checkOriginReceived(){
    var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.originReceivedCode'].value;
    
    if(vendorId == ''){
    	document.forms['trackingStatusForm'].elements['trackingStatus.originReceivedName'].value="";
    }
    if(vendorId != ''){
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse9120;
	    http2.send(null);
    }	 
    } 
    
function handleHttpResponse9120(){
	 if (http2.readyState == 4){
      var results = http2.responseText
      results = results.trim();
      var res = results.split("#"); 
      if(res.length>2){
      	if(res[2] == 'Approved'){
     			document.forms['trackingStatusForm'].elements['trackingStatus.originReceivedName'].value = res[1];
         		document.forms['trackingStatusForm'].elements['trackingStatus.originReceivedCode'].select();
      	}else{
     			alert("Origin Received Code  is not approved" ); 
			    document.forms['trackingStatusForm'].elements['trackingStatus.originReceivedName'].value="";
			    document.forms['trackingStatusForm'].elements['trackingStatus.originReceivedCode'].value="";
			    document.forms['trackingStatusForm'].elements['trackingStatus.originReceivedCode'].select();
     		}  
     	}else{
           alert("Origin Received Code not valid" );
       	 document.forms['trackingStatusForm'].elements['trackingStatus.originReceivedName'].value="";
			 document.forms['trackingStatusForm'].elements['trackingStatus.originReceivedCode'].value="";
			 document.forms['trackingStatusForm'].elements['trackingStatus.originReceivedCode'].select();
      } } }

function checkDestinationGiven(){
    var vendorId=document.forms['trackingStatusForm'].elements['trackingStatus.destinationGivenCode'].value;
    
    if(vendorId == ''){
    	document.forms['trackingStatusForm'].elements['trackingStatus.destinationGivenName'].value="";
    }
    if(vendorId != ''){
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse9130;
	    http2.send(null);
    }	 
    } 
    
function handleHttpResponse9130(){
	 if (http2.readyState == 4){
     var results = http2.responseText;
     results = results.trim();
     var res = results.split("#"); 
     if(res.length>2){
     	if(res[2] == 'Approved'){
    			document.forms['trackingStatusForm'].elements['trackingStatus.destinationGivenName'].value = res[1];
        		document.forms['trackingStatusForm'].elements['trackingStatus.destinationGivenCode'].select();
     	}else{
    			alert("Destination Given Code  is not approved" ); 
			    document.forms['trackingStatusForm'].elements['trackingStatus.destinationGivenName'].value="";
			    document.forms['trackingStatusForm'].elements['trackingStatus.destinationGivenCode'].value="";
			    document.forms['trackingStatusForm'].elements['trackingStatus.destinationGivenCode'].select();
    		}  
    	}else{
          alert("Destination Given Code not valid" );
      	 document.forms['trackingStatusForm'].elements['trackingStatus.destinationGivenName'].value="";
			 document.forms['trackingStatusForm'].elements['trackingStatus.destinationGivenCode'].value="";
			 document.forms['trackingStatusForm'].elements['trackingStatus.destinationGivenCode'].select();
     } } }

function checkDestinationReceived(){
    var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.destinationReceivedCode'].value;
    if(vendorId == ''){
    	document.forms['trackingStatusForm'].elements['trackingStatus.destinationReceivedName'].value="";
    }		
    
    if(vendorId !=''){
    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponse9140;
    http2.send(null);
    }	 
     }  
     
function handleHttpResponse9140(){
	 if (http2.readyState == 4){
    var results = http2.responseText
    results = results.trim();
    var res = results.split("#"); 
    if(res.length>2){
    	if(res[2] == 'Approved'){
   			document.forms['trackingStatusForm'].elements['trackingStatus.destinationReceivedName'].value = res[1];
       		document.forms['trackingStatusForm'].elements['trackingStatus.destinationReceivedCode'].select();
    	}else{
   			alert("Destination Received Code  is not approved" ); 
			    document.forms['trackingStatusForm'].elements['trackingStatus.destinationReceivedName'].value="";
			    document.forms['trackingStatusForm'].elements['trackingStatus.destinationReceivedCode'].value="";
			    document.forms['trackingStatusForm'].elements['trackingStatus.destinationReceivedCode'].select();
   		}  
   	}else{
         alert("Destination Received Code not valid" );
     	 document.forms['trackingStatusForm'].elements['trackingStatus.destinationReceivedName'].value="";
			 document.forms['trackingStatusForm'].elements['trackingStatus.destinationReceivedCode'].value="";
			 document.forms['trackingStatusForm'].elements['trackingStatus.destinationReceivedCode'].select();
    } } }
			   
function checkForwarderName(){
    var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].value;
    if(vendorId == ''){
    	document.forms['trackingStatusForm'].elements['trackingStatus.forwarder'].value="";
    }
    if(vendorId != ''){
        <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=VN&partnerCode=" + encodeURI(vendorId);
	    </configByCorp:fieldVisibility>
	    <configByCorp:fieldVisibility componentId="component.field.vanline">
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    </configByCorp:fieldVisibility>
	    http3.open("GET", url, true);
	    http3.onreadystatechange = handleHttpResponseForwarderCode;
	    http3.send(null);
	} }
function handleHttpResponseForwarderCode(){
			
	if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
                			document.forms['trackingStatusForm'].elements['trackingStatus.forwarder'].value = res[1];
    	           			document.forms['trackingStatusForm'].elements['trackingStatus.forwarderEmail'].value = res[4];
    	           			document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].select();
    	           			validateListForForwarder();
                		
                	}else{
	           			alert("Forwarder code is not approved" ); 
					    document.forms['trackingStatusForm'].elements['trackingStatus.forwarder'].value="";
					    document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].value="";
					    document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].select();
					    showAddressForwarderCodeImage();
	           		}  
               	}else{
                     alert("Forwarder code not valid" );
                 	 document.forms['trackingStatusForm'].elements['trackingStatus.forwarder'].value="";
                 	 document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].value="";
                 	 document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].select();
                 	 showAddressForwarderCodeImage();
			   }   } }
function completeTimeString() {
	stime3 = document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].value;	
	<configByCorp:fieldVisibility componentId="trackingStatus.surveyDate">
		stime1 = document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].value;
		stime2 = document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].value;
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].value = "0" + stime1;
			}
		}
		if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
			if(stime2.length==1 || stime2.length==2){
				if(stime2.length==2){
					document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].value = stime2 + ":00";
				}
				if(stime2.length==1){
					document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].value = "0" + stime2 + ":00";
				}
			}else{
				document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].value = stime2 + "00";
			}
		}else{
			if(stime2.indexOf(":") == -1 && stime2.length==3){
				document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
			}
			if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
				document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
			}
			if(stime2.indexOf(":") == 1){
				document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].value = "0" + stime2;
			} }
		</configByCorp:fieldVisibility>
			if(stime3.substring(stime3.indexOf(":")+1,stime3.length) == "" || stime3.length==1 || stime3.length==2){
			if(stime3.length==1 || stime3.length==2){
				if(stime3.length==2){
					document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].value = stime3 + ":00";
				}
				if(stime3.length==1){
					document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].value = "0" + stime3 + ":00";
				}
			}else{
				document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].value = stime3 + "00";
			}
		}else{
			if(stime3.indexOf(":") == -1 && stime3.length==3){
				document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].value = "0" + stime3.substring(0,1) + ":" + stime3.substring(1,stime3.length);
			}
			if(stime3.indexOf(":") == -1 && (stime3.length==4 || stime3.length==5) ){
				document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].value = stime3.substring(0,2) + ":" + stime3.substring(2,4);
			}
			if(stime3.indexOf(":") == 1){
				document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].value = "0" + stime3;
			} } 
			<configByCorp:fieldVisibility componentId="component.tab.trackingStatus.docCutOffTimeTo">
			stime4 = document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].value;
			if(stime4.substring(stime4.indexOf(":")+1,stime4.length) == "" || stime4.length==1 || stime4.length==2){
				if(stime4.length==1 || stime4.length==2){
					if(stime4.length==2){
						document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].value = stime4 + ":00";
					}
					if(stime4.length==1){
						document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].value = "0" + stime4 + ":00";
					}
				}else{
					document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].value = stime4 + "00";
				}
			}else{
				if(stime4.indexOf(":") == -1 && stime4.length==3){
					document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].value = "0" + stime4.substring(0,1) + ":" + stime4.substring(1,stime4.length);
				}
				if(stime4.indexOf(":") == -1 && (stime4.length==4 || stime4.length==5) ){
					document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].value = stime4.substring(0,2) + ":" + stime4.substring(2,4);
				}
				if(stime4.indexOf(":") == 1){
					document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].value = "0" + stime4;
				} }
			</configByCorp:fieldVisibility>
			}
/* function IsValidTime(clickType) {
	if(${isNetworkBookingAgent==true || isNetworkOriginAgent==true ||  isNetworkDestinationAgent==true || networkAgent}){
		document.forms['trackingStatusForm'].elements['buttonType'].value=clickType; 
		var savedDestinationAgent='${trackingStatus.destinationAgentCode}';
		var presentDestinationAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
		var destinationAgentExSo='${trackingStatus.destinationAgentExSO}';
		var presentDestinationEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value;
		
		var findKey=findCorpIdOfCode(savedDestinationAgent);
		var sameCompanyCodeflag=false;
		if(presentDestinationAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentDestinationAgent)>-1){
			sameCompanyCodeflag=true;
		}
		
		if(destinationAgentExSo!='' && presentDestinationEXSO !='' && !sameCompanyCodeflag){
		 if(savedDestinationAgent!=presentDestinationAgent && savedDestinationAgent != '' && savedDestinationAgent != null){
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value='remove'+destinationAgentExSo;
			} }
		var savedDestinationSubAgent='${trackingStatus.destinationSubAgentCode}';
		var presentDestinationSubAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
		var destinationSubAgentExSo='${trackingStatus.destinationSubAgentExSO}';
		var presentDestinationSubAgentEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value;
		
		 findKey=findCorpIdOfCode(savedDestinationSubAgent);
		 sameCompanyCodeflag=false;
		if(presentDestinationSubAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentDestinationSubAgent)>-1){
			sameCompanyCodeflag=true;
		}
		
		if(destinationSubAgentExSo!='' && presentDestinationSubAgentEXSO != '' && !sameCompanyCodeflag){
		 if(savedDestinationSubAgent!=presentDestinationSubAgent && savedDestinationSubAgent != '' && savedDestinationSubAgent != null){
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value='remove'+destinationSubAgentExSo;
			} }
		var savedOriginAgent='${trackingStatus.originAgentCode}';
		var presentOriginAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
		var originAgentExSo='${trackingStatus.originAgentExSO}';
		var presentOriginAgentEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value;
		
		 findKey=findCorpIdOfCode(savedOriginAgent);
		 sameCompanyCodeflag=false;
		if(presentOriginAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentOriginAgent)>-1){
			sameCompanyCodeflag=true;
		}
		
		if(originAgentExSo!='' && presentOriginAgentEXSO != '' && !sameCompanyCodeflag){
		 if(savedOriginAgent!=presentOriginAgent && savedOriginAgent != '' && savedOriginAgent != null){
			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value='remove'+originAgentExSo;
			} }
		var savedOriginSubAgent='${trackingStatus.originSubAgentCode}';
		var presentOriginSubAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
		var originSubAgentExSo='${trackingStatus.originSubAgentExSO}';
		var presentOriginSubAgentEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value;

		findKey=findCorpIdOfCode(savedOriginSubAgent);
		 sameCompanyCodeflag=false;
		if(presentOriginSubAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentOriginSubAgent)>-1){
			sameCompanyCodeflag=true;
		}
		
		if(originSubAgentExSo!='' && presentOriginSubAgentEXSO != '' && !sameCompanyCodeflag){
		 if(savedOriginSubAgent!=presentOriginSubAgent && savedOriginSubAgent != '' && savedOriginSubAgent != null){
			document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value='remove'+originSubAgentExSo;
			} }
		var savedNetworkPartnerCode='${trackingStatus.networkPartnerCode}';
		var presentNetworkPartnerCode=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
		var networkAgentExSO='${trackingStatus.networkAgentExSO}';
		var presentnetworkAgentExSO = document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value;
		
		 findKey=findCorpIdOfCode(savedNetworkPartnerCode);
		 sameCompanyCodeflag=false;
		if(presentNetworkPartnerCode!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentNetworkPartnerCode)>-1){
			sameCompanyCodeflag=true;
		}
		
		if(networkAgentExSO!='' && presentnetworkAgentExSO != '' && !sameCompanyCodeflag){
		 if(savedNetworkPartnerCode!=presentNetworkPartnerCode && savedNetworkPartnerCode != '' && savedNetworkPartnerCode != null){
			document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value='remove'+networkAgentExSO;
			} } }
try{	
<configByCorp:fieldVisibility componentId="trackingStatus.surveyDate">
var timeStr = document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].value;
var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
if(timeStr != "")
alert("Time is not in a valid format. Please use HH:MM format");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6]; 
if (second=="") { second = null; }
if (ampm=="") { ampm = null } 
if (hour < 0  || hour > 23) {
alert("'Survey' time must between 0 to 23 (Hrs)");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].focus();
return false;
}
if (minute<0 || minute > 59) {
alert ("'Survey' time must between 0 to 59 (Min)");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Survey' time must between 0 to 59 (Sec)");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].focus();
return false;
}
var time2Str = document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].value;
var matchTime2Array = time2Str.match(timePat);
if (matchTime2Array == null) {
alert("Time is not in a valid format. please Use HH:MM format");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].focus();
return false;
}
hourTime2 = matchTime2Array[1];
minuteTime2 = matchTime2Array[2];
secondTime2 = matchTime2Array[4];
ampmTime2 = matchTime2Array[6];
if (hourTime2 < 0  || hourTime2 > 23) {
alert("'Survey' time must between 0 to 23 (Hrs)");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'Survey' time must between 0 to 59 (Min)");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'Survey' time must between 0 to 59 (Sec)");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].focus();
return false;
}
</configByCorp:fieldVisibility>
var timeStr = document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].value;
var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
if(timeStr != "")
alert("Time is not in a valid format. Please use HH:MM format");
document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];
if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if(hour != null && (hour < 0  || hour > 23)) {
alert("'Cut off time' must be between 0 to 23 (Hrs)");
document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].focus();
return false;
}
if(minute != null &&  (minute<0 || minute > 59)) {
alert ("'Cut off time' must be between 0 to 59 (Min)");
document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Cut off time' must be between 0 to 59 (Sec)");
document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].focus();
return false;
}
<configByCorp:fieldVisibility componentId="component.tab.trackingStatus.docCutOffTimeTo">
var doctimeStr = document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].value;
var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var matchArray = doctimeStr.match(timePat);
if (matchArray == null) {
if(doctimeStr != "")
alert("Time is not in a valid format. Please use HH:MM format");
document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];
if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if(hour != null && (hour < 0  || hour > 23)) {
alert("'Cut off time' must be between 0 to 23 (Hrs)");
document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].focus();
return false;
}
if(minute != null &&  (minute<0 || minute > 59)) {
alert ("'Cut off time' must be between 0 to 59 (Min)");
document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Cut off time' must be between 0 to 59 (Sec)");
document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].focus();
return false;
}
</configByCorp:fieldVisibility>
}catch(e){}
} */
function right(e) {		
			if (navigator.appName == 'Netscape' && e.which == 1) {
		return false;
		}		
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
		return false;
		}		
		else return true;
		}
		function test()
		{
			return false;
		}
		function test1(){
			var img = this;
			img.onclick = false;
			return false;
		}
		function trap1(){
			 if(document.images)  {
			      var totalImages = document.images.length;
			      	for (var i=0;i<totalImages;i++) {
				      	try{
							if(document.images[i].src.indexOf('images/navarrow.gif')>0) { 
								var el = document.getElementById(document.images[i].id);
								if((el.getAttribute("id")).indexOf('-trigger')>0){ 
									 el.style.display="none";   
									
								} } }
			      	catch(e){}
						} }  
			 try{	 
				 var fieldName = document.forms['trackingStatusForm'].elements['field'].value;
				 if(fieldName=='trackingStatus.loadA'){
				  	fieldName=document.forms['trackingStatusForm'].elements['loadA'].name;
				 }else if(fieldName=='trackingStatus.packA'){
				 	fieldName = document.forms['trackingStatusForm'].elements['packA'].name;
				  }else if(fieldName=='trackingStatus.deliveryA'){
					fieldName = document.forms['trackingStatusForm'].elements['deliveryA'].name; 
				 }else{
					fieldName = document.forms['trackingStatusForm'].elements['field'].value;
				 }
				 var fieldName1 = document.forms['trackingStatusForm'].elements['field1'].value;
				 if(fieldName1=='trackingStatus.loadA'){
				 	fieldName1=document.forms['trackingStatusForm'].elements['loadA'].name;
				 }else if(fieldName1=='trackingStatus.packA'){
				 	fieldName1=document.forms['trackingStatusForm'].elements['packA'].name;
				 }else if(fieldName1=='trackingStatus.deliveryA'){
					 fieldName1=document.forms['trackingStatusForm'].elements['deliveryA'].name;
				 }else{
					fieldName1 = document.forms['trackingStatusForm'].elements['field1'].value;
				 }
				 if(fieldName!=''){
					 document.forms['trackingStatusForm'].elements[fieldName].className = 'rules-textUpper';
					 animatedcollapse.addDiv('agentroles', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('weights', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('initiation', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('orgpack', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('sitorigin', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('interstate', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('orgfwd', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('military','fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('gstfwd', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('transport', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('destination', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('sitdestination', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('storage', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('customs', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('weightnvolumetracsub', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('weightnvolumetracsub2', 'fade=0,persist=0,show=1')
					 animatedcollapse.init()
				 }
				 if(fieldName1!=''){
					 document.forms['trackingStatusForm'].elements[fieldName1].className = 'rules-textUpper';
					 animatedcollapse.addDiv('agentroles', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('weights', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('initiation', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('orgpack', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('sitorigin', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('interstate', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('orgfwd', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('military','fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('gstfwd', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('transport', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('destination', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('sitdestination', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('storage', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('customs', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('weightnvolumetracsub', 'fade=0,persist=0,show=1')
					 animatedcollapse.addDiv('weightnvolumetracsub2', 'fade=0,persist=0,show=1')
					 animatedcollapse.init()
				 }
			     }catch(e){}
				}
		function trap()   {
		  if(document.images) {
		      var totalImages = document.images.length;
		      	for (var i=0;i<totalImages;i++) {
		    		$('#trackingStatusForm').find('img[src="${pageContext.request.contextPath}/images/calender.png"]').attr('src','images/navarrow.gif');
		    		$('#trackingStatusForm').find('img[src="${pageContext.request.contextPath}/images/notes_empty1.jpg"]').attr('src','images/navarrow.gif');
		    		$('#trackingStatusForm').find('img[src="${pageContext.request.contextPath}/images/notes_open1.jpg"]').attr('src','images/navarrow.gif');
		    		$('#trackingStatusForm').find('img[src="${pageContext.request.contextPath}/images/open-popup.gif"]').attr('src','images/navarrow.gif');		    	
		    		$('#trackingStatusForm').find('img[src="images/navarrows_03.png"]').attr('src','images/navarrow.gif');
		    		$('#trackingStatusForm').find('img[src="images/navarrows_04.png"]').attr('src','images/navarrow.gif');
		    		$('#trackingStatusForm').find('img[src="${pageContext.request.contextPath}/images/navarrows_05.png"]').attr('src','images/navarrow.gif');		    		
		    		$('#trackingStatusForm').find('img[src="images/navdisable_03.png"]').attr('src','images/navarrow.gif');
		    		$('#trackingStatusForm').find('img[src="images/navdisable_04.png"]').attr('src','images/navarrow.gif');
		    		$('#trackingStatusForm').find('img[src="images/navdisable_05.png"]').attr('src','images/navarrow.gif');		    		
		    		$('#trackingStatusForm').find('img[src="${pageContext.request.contextPath}/images/address2.png"]').attr('src','images/navarrow.gif');
		    		$('#trackingStatusForm').find('img[src="${pageContext.request.contextPath}/images/plus-small.png"]').attr('src','images/navarrow.gif');
		    		$('#trackingStatusForm').find('img[src="${pageContext.request.contextPath}/images/email_small.gif"]').attr('src','images/navarrow.gif');

		    		var imageList = document.getElementsByTagName('img');
   				 for(var i = 0 ; i < imageList.length ; i++){
   					 if(imageList[i].nodeName == 'IMG' || imageList[i].nodeName == 'img'){
   						imageList[i].onclick =  null;
   					 }
   				 }			    	
	 			} } }
		  
		  function changeCalOpenarvalue()  {
		  	document.forms['trackingStatusForm'].elements['calOpener'].value='open';
		  }	  
 function checkMultiAuthorization() {  
    var billCode ='${serviceOrder.billToCode}';
   var url="checkMultiAuthorization.html?ajax=1&decorator=simple&popup=true&accPartnerCode="+billCode;  
     http55.open("GET", url, true); 
     http55.onreadystatechange = handleHttpResponse41; 
     http55.send(null);  
}
function handleHttpResponse41() {    
               if (http55.readyState == 4) {
                var results = http55.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results=='true') {
                document.getElementById("hid").style.display="block";
                }  else{
                document.getElementById("hid").style.display="none";                
                }} }
  
   function sendEmail(target) {
   		var originEmail = target;
   		var subject = 'S/O# ${serviceOrder.shipNumber}';
   		subject = subject+" ${serviceOrder.firstName}";
   		subject = subject+" ${serviceOrder.lastName}"; 
		var mailto_link = "mailto:"+encodeURI(originEmail)+"?subject="+encodeURI(subject);		
		win = window.open(mailto_link,'emailWindow'); 
		if (win && win.open &&!win.closed) win.close();		
		} 
  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['trackingStatusForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['trackingStatusForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   } 
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['trackingStatusForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['trackingStatusForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }   
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4) {
          		 var results = http5.responseText
                 results = results.trim();
				 var id1=results;	 
	             findOtherServiceType(id1);
             }   }     
	function findOtherServiceType(id1)  {
		  var soIdNum=id1;
		  var url="findOtherServiceType.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum);
		  http10.open("GET", url, true); 
          http10.onreadystatechange =function(){ handleHttpResponseOtherShipType(id1);}; 
          http10.send(null); 
	}
  function handleHttpResponseOtherShipType(id1){
             if (http10.readyState == 4) {
             			  var results = http10.responseText
			               results = results.trim();
						if(results=="")	{
						location.href = 'editTrackingStatus.html?id='+id1;
						}
						else{
						location.href = 'editDspDetails.html?id='+id1;
						}    } }	
    var http10 = getHTTPObject();         
  function goToUrl(id) {
	findOtherServiceType(id);
	}
function findCustomerOtherSO(position) {
 var sid=document.forms['trackingStatusForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['trackingStatusForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  }
function check(targetElement){
  var t = targetElement.value;
  if(t=='lbscft'){
  document.forms['trackingStatusForm'].elements['miscellaneous.unit1'].value='Lbs';
  document.forms['trackingStatusForm'].elements['miscellaneous.unit2'].value='Cft';
  }
  if(t=='kgscbm'){
  document.forms['trackingStatusForm'].elements['miscellaneous.unit1'].value='Kgs';
  document.forms['trackingStatusForm'].elements['miscellaneous.unit2'].value='Cbm';
  } } 
function getVanlineCode(targetField, position) {
	var job = document.forms['trackingStatusForm'].elements['serviceOrder.job'].value; 
    if(job=='DOM'||job=='UVL'||job=='MVL'){
		if(targetField == 'OA'){
	        var partnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
	        if(partnerCode != ''){
	        	position = document.getElementById('navigationOA'); 
	        	var url="findVanLineList.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode) +"&agentType="+ encodeURI(targetField);
	  			ajax_showTooltip(url,position);
	        }   }
	    if(targetField == 'DA'){
	    	var partnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
	    	if(partnerCode != ''){
		      	position = document.getElementById('navigationDA'); 
		        var url="findVanLineList.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode) +"&agentType="+ encodeURI(targetField);
		  		ajax_showTooltip(url,position);
	  		}  } } }

function goToUrlVanline(id, targetField){
	if(targetField == 'OA'){
        	document.forms['trackingStatusForm'].elements['trackingStatus.originAgentVanlineCode'].value = id;
        }
        if(targetField == 'DA'){
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentVanlineCode'].value = id;
        } }

function getVanlineAgent(targetField){
	if(targetField == 'OA'){
        var vanlineCode = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentVanlineCode'].value;
    }
    if(targetField == 'DA'){
        var vanlineCode = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentVanlineCode'].value;
    }
    if(vanlineCode != ''){
    	var url="findVanLineAgent.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vanlineCode);
    	http22.open("GET", url, true);
    	http22.onreadystatechange = function() {handleHttpResponseVanlineAgent(targetField);};
    	http22.send(null);
    } }
function handleHttpResponseVanlineAgent(targetField){
	if (http22.readyState == 4){
		var results = http22.responseText
        results = results.trim(); 
        if(results.length>1){
	        res = results.split("@"); 
	        if(targetField == 'OA'){
	        	targetElement = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'];
	        }
	        if(targetField == 'DA'){
	        	targetElement = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'];
	        }  
	        targetElement.length = res.length;
			for(i=0;i<res.length;i++){
				if(targetField == 'OA'){
					if(res.length == 2){
						stateVal = res[i].split("~");
						document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value = '';
						document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value = '';
					}else{
						stateVal = res[i].split("~");
						document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value = stateVal[0];
						document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value = stateVal[1];
					} }
				if(targetField == 'DA'){
					if(res.length == 2){
						stateVal = res[i].split("~");
						document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value = '';
						document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value = '';
					}else{
						stateVal = res[i].split("~");
						document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value = stateVal[0];
						document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value = stateVal[1];
					} } }
        }else{
        	alert("Invalid vanline code");
        	if(targetField == 'OA'){
		        document.forms['trackingStatusForm'].elements['trackingStatus.originAgentVanlineCode'].value='';
		    }
		    if(targetField == 'DA'){
		        document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentVanlineCode'].value='';
		    } 	} } } 
function openPricingList(){
	var serviceOrderId='${serviceOrder.id}';
	javascript:openWindow("agentPricingList.html?serviceOrderId="+serviceOrderId+"&decorator=popup&popup=true",1000,550);
}	
function downloadXMLFile(){
var customerFileNumber=document.forms['trackingStatusForm'].elements['customerFileNumber'].value;
var sentToKSD=document.forms['trackingStatusForm'].elements['trackingStatus.sentToKSD'].value;
if(sentToKSD==''){
	var mydate=new Date();
	var daym;
	var year=mydate.getFullYear()
	var y=""+year;
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec";
	 var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = daym+"-"+month+"-"+y.substring(2,4);
	     	   document.forms['trackingStatusForm'].elements['trackingStatus.sentToKSD'].value=datam;
	     	   var fileNumber=customerFileNumber.replace('-','');
          	   location.href='downloadXMLFile.html?fileNumber='+encodeURIComponent(fileNumber)
}
else{
alert('Shipment already sent to KSD.');
} }
function trapCalendar(rddStatus){
	requiredDeliveryDateTracker = rddStatus;
}
function autoPopulateActualDeliveryDate(){
	var soServiceType="";
	try{
		soServiceType='${serviceOrder.serviceType}';
	}catch(e){}	
	var deliveryA='';
	try{
	deliveryA='${trackingStatus.deliveryA}';
	}catch(e){}	
	var leftWHOn='';
	try{
		leftWHOn=document.forms['trackingStatusForm'].elements['trackingStatus.leftWHOn'].value;
	}catch(e){}		
	if(((soServiceType!='')&&(soServiceType!=null)&&(soServiceType!= undefined)&&(soServiceType=='D/OP'))&&((deliveryA=='')||(deliveryA==null)||(deliveryA == undefined))&&((leftWHOn!='')&&(leftWHOn!=null)&&(leftWHOn != undefined))){
		document.forms['trackingStatusForm'].elements['deliveryA'].value=leftWHOn;
		document.forms['trackingStatusForm'].elements['autoPopulateActualDeliveryDateFlag'].value="YES";
	} }
function calculateDeliveryDate(){
var MILLIS_IN_DAY = 1000 * 60 * 60 * 24;
if(document.forms['trackingStatusForm'].elements['trackingStatus.requiredDeliveryDays'].value!=""){ 
if(requiredDeliveryDateTracker != 'C'){
var days=Number(document.forms['trackingStatusForm'].elements['trackingStatus.requiredDeliveryDays'].value);
if(document.forms['trackingStatusForm'].elements['trackingStatus.sitOriginOn'].value!=""){
   var date2 = document.forms['trackingStatusForm'].elements['trackingStatus.sitOriginA'].value;	 
   if(date2!=""){
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = 2000+Number(mySplitResult2[2]);
   if(month2 == 'Jan') {
       month2 = "01";
   }  else if(month2 == 'Feb') {
       month2 = "02";
   }  else if(month2 == 'Mar') {
       month2 = "03"
   }  else if(month2 == 'Apr') {
       month2 = "04"
   }  else if(month2 == 'May') {
       month2 = "05"
   }  else if(month2 == 'Jun') {
       month2 = "06"
   }   else if(month2 == 'Jul') {
       month2 = "07"
   }  else if(month2 == 'Aug') {
       month2 = "08"
   }
   else if(month2 == 'Sep') {
       month2 = "09"
   }  else if(month2 == 'Oct') {
       month2 = "10"
   }  else if(month2 == 'Nov') {
       month2 = "11"
   }  else if(month2 == 'Dec') {
       month2 = "12";
   }
       var finalDate2 = month2+"-"+day2+"-"+year2;
       date2 = finalDate2.split("-");
       var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
       var date =new Date();
	   date.setTime((eDate.getTime())+MILLIS_IN_DAY*days);
	   document.forms['trackingStatusForm'].elements['trackingStatus.requiredDeliveryDate'].value=formatDate(date,'dd-NNN-yy');
    }else{document.forms['trackingStatusForm'].elements['trackingStatus.requiredDeliveryDate'].value="";}
    }else{
     var date2 = document.forms['trackingStatusForm'].elements['loadA'].value;	 
      if(date2!=""){
      var mySplitResult2 = date2.split("-");
     var day2 = mySplitResult2[0];
     var month2 = mySplitResult2[1];
     var year2 = 2000+Number(mySplitResult2[2]);
     if(month2 == 'Jan') {
       month2 = "01";
     }  else if(month2 == 'Feb') {
        month2 = "02";
     }  else if(month2 == 'Mar') {
       month2 = "03"
     }  else if(month2 == 'Apr')  {
       month2 = "04"
     }  else if(month2 == 'May') {
       month2 = "05"
     }  else if(month2 == 'Jun') {
       month2 = "06"
     }   else if(month2 == 'Jul') {
       month2 = "07"
     }  else if(month2 == 'Aug') {
       month2 = "08"
     }
     else if(month2 == 'Sep') {
        month2 = "09"
    }  else if(month2 == 'Oct') {
       month2 = "10"
    }  else if(month2 == 'Nov') {
       month2 = "11"
    }  else if(month2 == 'Dec') {
       month2 = "12";
    }
       var finalDate2 = month2+"-"+day2+"-"+year2;
       date2 = finalDate2.split("-");
       var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
       var date =new Date();
	   date.setTime((eDate.getTime())+MILLIS_IN_DAY*days);
	   document.forms['trackingStatusForm'].elements['trackingStatus.requiredDeliveryDate'].value=formatDate(date,'dd-NNN-yy');
     }else{
     document.forms['trackingStatusForm'].elements['trackingStatus.requiredDeliveryDate'].value="";
    	 }   } }
   }else{document.forms['trackingStatusForm'].elements['trackingStatus.requiredDeliveryDate'].value="";}
}
function noCTRL(e) {
var code = (document.all) ? event.keyCode:e.which;
var ctrl = (document.all) ? event.ctrlKey:e.modifiers & Event.CONTROL_MASK;
var msg = "Sorry, this functionality is disabled.";
if (document.all) {
if (ctrl && code==86) {
window.event.returnValue = false;
}
else if (ctrl && code==67) {
window.event.returnValue = false;
}
else if(event.button==2){
window.event.returnValue = false;
}
}
else {
if (ctrl==2)   
{
 window.event.returnValue = false;
} } }
function onlyNumsAllowedWithoutNegative(evt, targetElement, w) {
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  var shiftkey=evt.shiftKey;
	  var isValid = (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36);
	  if(shiftkey){
	    isValid=false;
	  }
	  targetElement.value = targetElement.value.replace(r[w],'');
	  return isValid; 
}
function checkForwarder(){
	 <configByCorp:fieldVisibility componentId="component.button.CWMS.commission">
	var saleManType=document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].value;
		if(document.forms['trackingStatusForm'].elements['invoiceCount'].value!='0'){
			var oldForwarder='${trackingStatus.forwarderCode}';
			<c:if test="${serviceOrder.companyDivision == 'CAJ' || serviceOrder.companyDivision == 'AIF'}">
			if(oldForwarder!=saleManType){
		      	   alert("Invoice Already Generated.Cannot change the Forwarder.") 
		 	 	document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].value='${trackingStatus.forwarderCode}';
		      	document.forms['trackingStatusForm'].elements['trackingStatus.forwarder'].value='${trackingStatus.forwarder}';
			}
			</c:if>
 	}
	</configByCorp:fieldVisibility> 
	}
	
function calculateRddDays(name){
	var fieldName='';
	if(name=='packA'){
		fieldName ="trackingStatus.packA";
	}else if(name=='loadA'){
		fieldName ="trackingStatus.loadA";
	}else{
		
	}
	var soBillToCode = '${serviceOrder.billToCode}';
	 new Ajax.Request('/redsky/findRddDaysFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&fieldName='+fieldName+'&soBillToCode='+soBillToCode,
			  {
			    method:'get',
			    onSuccess: function(transport){
		    
			      var response = transport.responseText || "no response text";
			      if(response!=null && response!=''){
			    	  var res = response.split("~");
			    	if(fieldName.trim()==res[0].trim()){
				    	if(document.forms['trackingStatusForm'].elements['trackingStatus.requiredDeliveryDays'].value==null || document.forms['trackingStatusForm'].elements['trackingStatus.requiredDeliveryDays'].value==''){
				    		document.forms['trackingStatusForm'].elements['trackingStatus.requiredDeliveryDays'].value = res[1].trim();
				    	}
			    	var date1 = '';
			       	date1 = document.forms['trackingStatusForm'].elements[name].value;
			      	var finalDate='';
			      	var daysToAdd = document.forms['trackingStatusForm'].elements['trackingStatus.requiredDeliveryDays'].value;
			      if ((date1 != '') && (daysToAdd!=null && daysToAdd!='')){
			    	   var mySplitResult = date1.split("-");
			    	   var day = mySplitResult[0];
			    	   var month = mySplitResult[1];
			    	   var year = mySplitResult[2];
			    	  if(month == 'Jan'){
			    	       month = "01";
			    	   }else if(month == 'Feb'){
			    	       month = "02";
			    	   }else if(month == 'Mar'){
			    	       month = "03"
			    	   }else if(month == 'Apr'){
			    	       month = "04"
			    	   }else if(month == 'May'){
			    	       month = "05"
			    	   }else if(month == 'Jun'){
			    	       month = "06"
			    	   }else if(month == 'Jul'){
			    	       month = "07"
			    	   }else if(month == 'Aug'){
			    	       month = "08"
			    	   }else if(month == 'Sep'){
			    	       month = "09"
			    	   }else if(month == 'Oct'){
			    	       month = "10"
			    	   }else if(month == 'Nov'){
			    	       month = "11"
			    	   }else if(month == 'Dec'){
			    	       month = "12";
			    	   }
			    	   
			    	    finalDate = month+"-"+day+"-"+year;
					 	date1 = finalDate.split("-");
					  	var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
					  	var date =new Date();
					  	var MILLIS_IN_DAY = 1000 * 60 * 60 * 24;
						date.setTime((sDate.getTime())+MILLIS_IN_DAY*daysToAdd);
					   document.forms['trackingStatusForm'].elements['trackingStatus.requiredDeliveryDate'].value=formatDate(date,'dd-NNN-yy');
			      	}
			      }
			      }
			    },
			    onFailure: function(){ 
				    }
			  });
	
}
</script>

<s:hidden name="usGovJobs"  value="${usGovJobs}"/>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="agentSearchValidation" />
<s:hidden name="fileID" id ="fileID" value="%{trackingStatus.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="noteFor" value="ServiceOrder" />
<s:form id="trackingStatusForm" action="saveTrackingStatus" onsubmit="return submit_form()" method="post" validate="true" >
<div id="Layer1"  onkeydown="changeStatus();" style="width:100%;">
<s:hidden name="serviceOrder.bookingAgentCode" value="${serviceOrder.bookingAgentCode}" />
<s:hidden name="autoPopulateActualDeliveryDateFlag" value="" />
<s:hidden name="weightTab"  id= "weightTab" value="1"/>
<s:hidden name="weightTabSub1"  id= "weightTabSub1" value="1"/>
<s:hidden name="weightTabSub2"  id= "weightTabSub2" value="1"/> 
<s:hidden name="componentId" value="module.trackingStatus" /> 
<s:hidden name="trackingStatus.shipNumber" />
<s:hidden name="billingContractType"  value="${billingContractType}"/>
<s:hidden name="trackingStatus.ugwIntId" />
<s:hidden name="trackingStatus.actgCode" />
<s:hidden name="trackingStatus.soNetworkGroup" value ="${trackingStatus.soNetworkGroup}" />
<s:hidden name="trackingStatus.contractType" value ="${trackingStatus.contractType}" />
<s:hidden name="trackingStatus.accNetworkGroup" value ="${trackingStatus.accNetworkGroup}" /> 
<s:hidden name="trackingStatus.utsiNetworkGroup" value ="${trackingStatus.utsiNetworkGroup}" />
<s:hidden name="trackingStatus.agentNetworkGroup" value ="${trackingStatus.agentNetworkGroup}" />
<s:hidden name="trackingStatus.actgCodeForDis" />
<s:hidden name="trackingStatus.sequenceNumber" />
<s:hidden name="miscellaneous.status"  value="%{miscellaneous.status}" />
<s:hidden name="trackingStatus.corpID" />
<s:hidden name="corpIDform" value="${trackingStatus.corpID}" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.registrationNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="miscellaneous.unit1"/>
<s:hidden name="miscellaneous.unit2"/>
<s:hidden name="miscellaneous.g11" />
<s:hidden id="checkSitOriginDaysClick" name="checkSitOriginDaysClick" />
<s:hidden id="checkSitDestinationDaysClick" name="checkSitDestinationDaysClick" />
<s:hidden name="moveType"  value="${moveType}"/>
<s:hidden name="quotationAccessCheck"  value="${checkAccessQuotation}"/>
<s:hidden name="calOpener" value="notOPen" />
		<s:hidden name="shipSize" />
		<s:hidden name="minShip" />
		<s:hidden name="countShip"/>
		<configByCorp:fieldVisibility componentId="component.field.Alternative.Division">
	<s:hidden name="divisionFlag" value="YES"/>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.SO.Extarct">
	<s:hidden name="isDecaExtract" value="1"/>
	</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.QualitySurvey.CoordinatorEval.validation">
<s:hidden name="coordinatorEvalValidation" id="coordinatorEvalValidation" value="Y"/>
</configByCorp:fieldVisibility>	
<c:set var="dateColumnShow" value="N" />
<configByCorp:fieldVisibility componentId="component.field.trackingstatus.BeginEndDate">
	<c:set var="dateColumnShow" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>

<c:set var="agentClassificationShow" value="N"/>
<configByCorp:fieldVisibility componentId="component.partner.agentClassification.show">
 <c:set var="agentClassificationShow" value="Y"/>
</configByCorp:fieldVisibility>
<s:hidden name="checkMsgClickedValue" value="<%=request.getParameter("msgClicked") %>" />
<s:hidden name="msgClicked" id="msgClicked"/>
<s:hidden name="validateFlagTrailer"/>
<s:hidden name="validateFlagDestin"/>
<s:hidden name="validateFlagBroker"/>
<s:hidden name="validateFlagForwarder"/>
<s:hidden name="validateFlagSubOaAg"/>
<s:hidden name="validateFlagSubDaAg"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<s:hidden name="serviceType"/>
<s:hidden name="documentIdListCF"/>
<s:hidden name="documentIdListSO"/>
<s:hidden name="allIdOfMyFile"/>
<s:hidden name="myFileFieldName"/>
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.serviceorder' }">
   <c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.billing' }">
   <c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accounting' }">
   <c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.newAccounting' }">
   <c:redirect url="/pricingList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.forwarding' }">
  <c:if test="${forwardingTabVal!='Y'}">
	<c:redirect url="/containers.html?id=${serviceOrder.id}"/>
</c:if>
<c:if test="${forwardingTabVal=='Y'}">
	<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}"/>
</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.OI' }">
<c:redirect url="/operationResource.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.domestic' }">
   <c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.status' }">
<c:if test="${serviceOrder.job =='RLO'}"> 
	 <c:redirect url="/editDspDetails.html?id=${serviceOrder.id}" />
</c:if>
        <c:if test="${serviceOrder.job !='RLO'}"> 
	<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}" />
</c:if>   
</c:when>
<c:when test="${gotoPageString == 'gototab.ticket' }">
   <c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.claims' }">
   <c:redirect url="/claims.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.customerfile' }">
   <c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.criticaldate' }">
	<sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
		<c:redirect url="/soAdditionalDateDetails.html?sid=${serviceOrder.id}" />
	</sec-auth:authComponent>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
<s:hidden name="isSOExtract" id="isSOExtract"/>
 <s:hidden name="enableRadio" value="${enableRadio}"/>
<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="customerFile.id"  />
<s:hidden name="serviceOrder.customerFile.id" value="<%=request.getParameter("id") %>" />
<s:hidden name="countStorage" value="<%=request.getParameter("countStorage") %>"/>
<c:set var="countStorage" value="<%=request.getParameter("countStorage") %>" />
<s:hidden name="invoiceCount" value="${invoiceCount}"/>
<s:hidden id="countSurveyNotes" name="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>"/>
<s:hidden id="countOriginDetailNotes" name="countOriginDetailNotes" value="<%=request.getParameter("countOriginDetailNotes") %>"/>
<s:hidden id="countDestinationDetailNotes" name="countDestinationDetailNotes" value="<%=request.getParameter("countDestinationDetailNotes") %>"/>
<s:hidden id="countWeightDetailNotes" name="countWeightDetailNotes" value="<%=request.getParameter("countWeightDetailNotes") %>"/>
<s:hidden id="countOriginPackoutNotes" name="countOriginPackoutNotes" value="<%=request.getParameter("countOriginPackoutNotes") %>"/>
<s:hidden id="countSitOriginNotes" name="countSitOriginNotes" value="<%=request.getParameter("countSitOriginNotes") %>"/>
<s:hidden id="countOriginForwardingNotes" name="countOriginForwardingNotes" value="<%=request.getParameter("countOriginForwardingNotes") %>"/>
<s:hidden id="countGSTForwardingNotes" name="countGSTForwardingNotes" value="<%=request.getParameter("countGSTForwardingNotes") %>"/>
<s:hidden id="countDestinationStatusNotes" name="countDestinationStatusNotes" value="<%=request.getParameter("countDestinationStatusNotes") %>"/>
<s:hidden id="countSitDestinationNotes" name="countSitDestinationNotes" value="<%=request.getParameter("countSitDestinationNotes") %>"/>
<s:hidden id="countDestinationImportNotes" name="countDestinationImportNotes" value="<%=request.getParameter("countDestinationImportNotes") %>"/>
<s:hidden id="countInterStateNotes" name="countInterStateNotes" value="<%=request.getParameter("countInterStateNotes") %>"/>
<s:hidden id="countServiceOrderNotes" name="countServiceOrderNotes" value="<%=request.getParameter("countServiceOrderNotes") %>"/>
<c:set var="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>" />
<c:set var="countOriginDetailNotes" value="<%=request.getParameter("countOriginDetailNotes") %>" />
<c:set var="countDestinationDetailNotes" value="<%=request.getParameter("countDestinationDetailNotes") %>" />
<c:set var="countWeightDetailNotes" value="<%=request.getParameter("countWeightDetailNotes") %>" />
<c:set var="countOriginPackoutNotes" value="<%=request.getParameter("countOriginPackoutNotes") %>" />
<c:set var="countSitOriginNotes" value="<%=request.getParameter("countSitOriginNotes") %>" />
<c:set var="countOriginForwardingNotes" value="<%=request.getParameter("countOriginForwardingNotes") %>" />
<c:set var="countGSTForwardingNotes" value="<%=request.getParameter("countGSTForwardingNotes") %>" />
<c:set var="countDestinationStatusNotes" value="<%=request.getParameter("countDestinationStatusNotes") %>" />
<c:set var="countSitDestinationNotes" value="<%=request.getParameter("countSitDestinationNotes") %>" />
<c:set var="countDestinationImportNotes" value="<%=request.getParameter("countDestinationImportNotes") %>" />
<c:set var="countInterStateNotes" value="<%=request.getParameter("countInterStateNotes") %>" />
<c:set var="countServiceOrderNotes" value="<%=request.getParameter("countServiceOrderNotes") %>" />
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<c:set var="field" value="<%=request.getParameter("field") %>"/>
<s:hidden name="field" value="<%=request.getParameter("field") %>" />
<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
<c:set var="field1" value="<%=request.getParameter("field1") %>"/>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="layer6" style="width:100%; ">
<div id="newmnav" style="float:left; ">
            <ul>
      
	        <configByCorp:fieldVisibility componentId="component.Dynamic.DashBoard">
	               <sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">
	                 <c:if test="${(fn1:indexOf(dashBoardHideJobsList,serviceOrder.job)==-1)}">
          <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">  
		<li><a  href="redskyDashboard.html?sid=${serviceOrder.id}" ><span>Dashboard</span></a></li>
		</c:if>
		</c:if>
		</sec-auth:authComponent>
		</configByCorp:fieldVisibility>
		
            <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
		       <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		       <li><a onmouseover="completeTimeString();return IsValidTime('save');"  onclick="setReturnString('gototab.serviceorder');return checkdate('none');"><span>S/O Details</span></a></li>
            </c:if>
            <c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
            <li><a onmouseover="completeTimeString();return IsValidTime('save');"  onclick="setReturnString('gototab.serviceorder');return checkdate('none');"><span>Quotes</span></a></li>
            </c:if>
            </sec-auth:authComponent> 
            <sec-auth:authComponent componentId="module.tab.trackingStatus.billingTab">
             <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >	
             	<li><a onmouseover="completeTimeString();return IsValidTime('save');" onclick="setReturnString('gototab.billing');return checkdate('none');"><span>Billing</span></a></li>
             </sec-auth:authComponent>
            </sec-auth:authComponent>
            <sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">
            <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
             <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		          <li><a onmouseover="completeTimeString();return IsValidTime('save');" onclick="setReturnString('gototab.accounting');return checkdate('none');"><span>Accounting</span></a></li>
		        </c:otherwise>
		     </c:choose>
		     </c:if> 
		     </sec-auth:authComponent>
		     <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		     <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
             <c:choose> 
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		          <li><a onmouseover="completeTimeString();return IsValidTime('save');" onclick="setReturnString('gototab.newAccounting');return checkdate('none');"><span>Accounting</span></a></li>
		        </c:otherwise>
		     </c:choose>
		     </c:if> 
		     </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.serviceorder.accountingPortalTab">	
	                 <li><a href="accountLineSalesPortalList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	                </sec-auth:authComponent>	
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
      	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		<li><a onclick="setReturnString('gototab.OI');return checkdate('none');"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
		       <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
           	  </sec-auth:authComponent>
		     <sec-auth:authComponent componentId="module.tab.trackingStatus.forwardingTab">
		     <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		     <c:if test="${usertype!='ACCOUNT'}">
 <%--  <c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)>=0 && serviceOrder.corpID=='CWMS')}">  --%>
		      <c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}"> 			
             		<li><a onmouseover="completeTimeString();return IsValidTime('save');" onclick="setReturnString('gototab.forwarding');return checkdate('none');"><span>Forwarding</span></a></li>
             		</c:if>
			 </c:if>
			 <c:if test="${usertype=='ACCOUNT'}">
	  		 <c:if test="${serviceOrder.job !='RLO'}">
	  		 <li><li><a href="servicePartnerss.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li></li>
	  		 </c:if>
			 </c:if>
			 </c:if>
             </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.trackingStatus.domesticTab">
             <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
               <li><a onmouseover="completeTimeString();return IsValidTime('save');" onclick="setReturnString('gototab.domestic');return checkdate('none');"><span>Domestic</span></a></li>
             </c:if>
             </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
	          <c:if test="${serviceOrder.job =='INT'}">
	            <li><a onmouseover="completeTimeString();return IsValidTime('save');" onclick="setReturnString('gototab.domestic');return checkdate('none');"><span>Domestic</span></a></li> 
	          </c:if>
	          </sec-auth:authComponent>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Status</span></a></li>
            <sec-auth:authComponent componentId="module.tab.summary.summaryTab">
			  	<li><a href="findSummaryList.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
			  </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.trackingStatus.ticketTab">
             <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
             	<li><a onmouseover="completeTimeString();return IsValidTime('save');" onclick="setReturnString('gototab.ticket');return checkdate('none');"><span>Ticket</span></a></li>
             </c:if>
             </sec-auth:authComponent> 
              <configByCorp:fieldVisibility componentId="component.standard.claimTab">
              <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
              <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
             	<li><a onmouseover="completeTimeString();return IsValidTime('save');" onclick="setReturnString('gototab.claims');return checkdate('none');"><span>Claims</span></a></li>
             </c:if>
             </sec-auth:authComponent> 
             </configByCorp:fieldVisibility>
             <c:set var="checkTab" value="Agent" />
             <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
             <c:set var="checkTab" value="NotAgent" />
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}" ><span>Survey Details</span></a></li>
			 </c:if>
			 <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>
			</sec-auth:authComponent> 
			  <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
			       <c:if test="${ usertype=='AGENT' && surveyTab}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			</c:if>
			</sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.trackingStatus.customerfileTab">
             <li><a onmouseover="completeTimeString();return IsValidTime('save');" onclick="setReturnString('gototab.customerfile');return checkdate('none');"><span>Customer File</span></a></li>
             </sec-auth:authComponent> 
              <sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
             <li><a onmouseover="completeTimeString();return IsValidTime('save');" onclick="setReturnString('gototab.criticaldate');return checkdate('none');"><span>Critical Dates</span></a></li>
             </sec-auth:authComponent> 
              <sec-auth:authComponent componentId="module.tab.trackingStatus.reportTab">
             <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Status&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=120, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
             </sec-auth:authComponent>
             <%--<li><a onclick="openWindow('auditList.html?id=${serviceOrder.id}&tableName=trackingstatus,miscellaneous&decorator=popup&popup=true',750,400)"><span>Audit</span></a></li>--%>
           	  <sec-auth:authComponent componentId="module.tab.trackingStatus.auditTab">
           	 <li><a onclick="window.open('auditList.html?id=${serviceOrder.id}&tableName=trackingstatus,miscellaneous&decorator=popup&popup=true','audit','height=400,width=770,top=100, left=120, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
           	  </sec-auth:authComponent>
           	  <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
           	 	<li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
           	  </sec-auth:authComponent>
           	  <c:if test="${usertype=='USER'}">
	           	  <configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
			  		<li><a href="findEmailSetupTemplateByModuleNameSO.html?sid=${serviceOrder.id}"><span>View Emails</span></a></li>
			  	  </configByCorp:fieldVisibility>
		  	  </c:if>
           </ul>
       </div> 
       	<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;height:22px;float: none; "><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}"  >
  		<a><img align="middle" id="navigation1" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" id="navigation2" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" id="navigation3" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" id="navigation4" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>		
		<c:if test="${countShip != 1}" >
		<td width="20px" align="left" style="!padding-top:1px;">
		<a><img class="openpopup" id="navigation5" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</td>
		</c:if>		
		<c:if test="${countShip == 1}" >
		<td width="20px" align="left" style="!vertical-align:top;">
  		<a><img align="middle" id="navigation6" src="images/navdisable_05.png"/></a>
  		</td>
  		</c:if>
  		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 1px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400)" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>	
		</c:if>
		</tr>
		</table>
	  <div class="spn">&nbsp;</div>
     <div style="!margin-top:8px; ">
      <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
     </div>
      </div>
<s:hidden name="serviceOrder.id"/>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content"> 
<table cellspacing="0" cellpadding="0" border="0" class="detailTabLabel" align="center" style="width:100%; ">
<tbody>
<tr>
<td style="width: 100%;">
<table cellpadding="0" cellspacing="0" width="100%" style="margin:0px;" >
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Status Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center" style="cursor:default; " >
<a href="javascript:animatedcollapse.hide(['

','agentReciprocity', 'weights', 'initiation', 'orgpack','sitorigin','interstate', 'orgfwd','military','gstfwd', 'transport', 'destination','sitdestination', 'storage','weightnvolumetracsub','weightnvolumetracsub2'])">Collapse All<img src="${pageContext.request.contextPath}/images/section_contract.png" HEIGHT=14 WIDTH=14 align="top"></a> | <a href="javascript:animatedcollapse.show(['agentroles','agentReciprocity', 'weights', 'initiation', 'orgpack','sitorigin','interstate', 'orgfwd','military','gstfwd', 'transport', 'destination','sitdestination', 'storage'])">Expand All<img src="${pageContext.request.contextPath}/images/section_expanded.png" HEIGHT=14 WIDTH=14 align="top"></a></span>
</td>
<td class="headtab_right">
</td>
</tr>
</table>
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" >
<fmt:message key='labels.onHoldUntil'/></a></td>
<c:if test="${not empty serviceOrder.nextCheckOn}">
<s:text id="trackingStatusActivateFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.nextCheckOn"/></s:text>
<td><s:textfield cssClass="input-textUpper" id="activate" name="serviceOrder.nextCheckOn" value="%{trackingStatusActivateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="2"/></td>
<td></td>
</c:if>
<c:if test="${empty serviceOrder.nextCheckOn}">
<td><s:textfield cssClass="input-textUpper" id="activate" name="serviceOrder.nextCheckOn" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="2"/></td>
<td></td>
</c:if>
<td align="right" class="listwhitetext" style="width:70px; !width:81px; padding-right:2px; !padding-right:1px;"><fmt:message key='serviceOrder.coordinator'/>&nbsp;</td>
<td align="left" style="padding-left:2px;"><s:textfield cssClass="input-textUpper"  name="serviceOrder.coordinator" cssStyle="width:145px;" required="true" readonly="true" tabindex="3"/></td>

<td align="right" class="listwhitetext" width="45px"><fmt:message key='customerFile.status'/></td>
<td align="left" class="listwhitetext"> <s:textfield  cssClass="input-textUpper" name="soStatus" value="%{serviceOrder.status}" cssStyle="width:54px;" readonly="true" tabindex="4"/> </td>
<td align="right" class="listwhitetext" width="70px"><fmt:message key='customerFile.statusDate'/></td>
<c:if test="${not empty serviceOrder.statusDate}">
<s:text id="statusDate" name="${FormDateValue}"><s:param name="value" value="serviceOrder.statusDate" /></s:text>
<td><s:textfield cssClass="input-textUpper" name="serviceOrder.statusDate" value="%{statusDate}" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="5"/></td>
</c:if>
<c:if test="${empty serviceOrder.statusDate}">
<td align="left" style="width:5px"></td>
<td><s:textfield cssClass="input-textUpper" name="serviceOrder.statusDate" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="5" /></td>
</c:if> 
<td align="right" class="listwhitetext" width="120px">Service&nbsp;Complete&nbsp;Date</td>
<c:if test="${not empty trackingStatus.serviceCompleteDate}">
<s:text id="trackingStatusServiceCompleteDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.serviceCompleteDate"/></s:text>
	<c:if test="${not empty completeDateFlag}">
		<td>
		<s:textfield name="trackingStatus.serviceCompleteDate" value="%{trackingStatusServiceCompleteDateFormattedValue}"  cssClass="input-textUpper" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="5" />  
		</td>
	</c:if>
	<c:if test="${empty completeDateFlag}">
	<td>
		<s:textfield id="trackingStatusForm_trackingStatus_ServiceCompleteDate" name="trackingStatus.serviceCompleteDate" value="%{trackingStatusServiceCompleteDateFormattedValue}" onkeydown="return onlyDel(event,this)" cssClass="input-text" cssStyle="width:65px" maxlength="11"/></td>
	</c:if>
</c:if>
<c:if test="${empty trackingStatus.serviceCompleteDate}">
<c:if test="${not empty completeDateFlag}">
		<td>
		<s:textfield name="trackingStatus.serviceCompleteDate" value="%{trackingStatusServiceCompleteDateFormattedValue}"  cssClass="input-textUpper" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="5" />
		</td>
	</c:if>
	<c:if test="${empty completeDateFlag}">
		<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_ServiceCompleteDate" name="trackingStatus.serviceCompleteDate" value="%{trackingStatusServiceCompleteDateFormattedValue}" onkeydown="return onlyDel(event,this)" cssStyle="width:65px" maxlength="11"/></td>
	</c:if>
</c:if>
<c:if test="${empty completeDateFlag}">
<td><img id="trackingStatusForm_trackingStatus_ServiceCompleteDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_ServiceCompleteDate" HEIGHT=20 WIDTH=20/></td>
</c:if> 
	<c:if test="${empty serviceOrder.id}">
<td  align="left" style="width:195px;!width:190px;"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty serviceOrder.id}">
<c:choose>
<c:when test="${countServiceOrderNotes == '0' || countServiceOrderNotes == '' || countServiceOrderNotes == null}">
<td  align="right" style="width:195px;!width:190px;"><img id="countServiceOrderNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=ServiceOrder&imageId=countServiceOrderNotesImage&fieldId=countServiceOrderNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=ServiceOrder&imageId=countServiceOrderNotesImage&fieldId=countServiceOrderNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:195px;!width:100px;"><img id="countServiceOrderNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=ServiceOrder&imageId=countServiceOrderNotesImage&fieldId=countServiceOrderNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=ServiceOrder&imageId=countServiceOrderNotesImage&fieldId=countServiceOrderNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
<td align="right" width="66" valign="middle" class="listwhitetext" rowspan="2">
	<c:if test="${serviceOrder.vip}">
			<img id="vipImage" src="${pageContext.request.contextPath}/images/vip_icon.png" />
	</c:if>
</td>
</tr>
</tbody>
</table>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:0px;">
<tbody>
<tr>
   <td height="10" width="100%" align="left" >
           <div  onClick="javascript:animatedcollapse.toggle('agentroles')"  style="margin: 0px;">
      <c:if test="${from=='rule'}">
      <c:if test="${field=='trackingStatus.originAgentCode' || field=='trackingStatus.destinationAgentCode'}">
          <table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;" border="0">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center"><font color="red">&nbsp;Agent Roles</font>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
      </c:if>    </c:if>
      <c:if test="${from=='rule'}">
      <c:if test="${field!='trackingStatus.originAgentCode' && field!='trackingStatus.destinationAgentCode'}">
          <table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;" border="0">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Agent Roles
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
      </c:if></c:if>
      <c:if test="${from!='rule'}">
          <table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;" border="0">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Agent Roles 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
      </c:if>
      </div>
    
      <jsp:include flush="true" page="trackingStatusFormsSec.jsp"></jsp:include>
	  <jsp:include flush="true" page="trackingStatusScriptForm.jsp"></jsp:include>
<!--Start  Agent Reciprocity -->
 <c:if test="${fn:indexOf(systemDefaultReciprocityJobType,serviceOrder.job)>=0}">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:0px;">
<tbody>
  <tr>
	<td height="10" width="100%" align="left" style="margin:0px">
	<div onClick="javascript:animatedcollapse.toggle('agentReciprocity')" style="margin:0px">
     <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Agent&nbsp;Reciprocity
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
     </div>
		<div id="agentReciprocity" class="switchgroup1">
		<table class="detailTabLabel" border="0" width="927px">
 		  <tbody>
 		  		<tr><td align="left" height="5px"></td></tr>     
				<tr>
				<td align="right" class="listwhitetext" width="152">Origin&nbsp;Given</td>
				<td colspan="4">
				<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
    			<tbody>   
     			 <tr>
				<td><s:textfield cssClass="input-text" id="trackingOriginGivenCode" key="trackingStatus.originGivenCode" cssStyle="width:45px;" maxlength="8"  onchange="valid(this,'special');checkOriginGiven();changeStatus();"  tabindex="" /></td>
				<td><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpenOriginGiven();changeStatus(); document.forms['trackingStatusForm'].elements['trackingStatus.originGivenCode'].focus();" id="openpopup7.img" src="<c:url value='/images/open-popup.gif'/>" /></td>			
				<td><s:textfield cssStyle="width:145px"  cssClass="input-text" id="trackingOriginGivenName" key="trackingStatus.originGivenName" onkeyup="findPartnerDetails('trackingOriginGivenName','trackingOriginGivenCode','trackingOriginOriginGivenDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true)','',event);"required="true" tabindex="" />
				<div id="trackingOriginOriginGivenDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</td>
				</tr>
				</tbody>
				</table>				
				</td>
		 
				<td align="right" class="listwhitetext">Destination&nbsp;Given</td>
				<td colspan="4">
				<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
    			<tbody>
     			 <tr>
				<td><s:textfield cssClass="input-text" id="trackingDestinationGivenCode" key="trackingStatus.destinationGivenCode" cssStyle="width:45px;" maxlength="8" onchange="valid(this,'special');checkDestinationGiven();changeStatus();" tabindex="" /></td>
				<td><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpenDestinationGiven();changeStatus(); document.forms['trackingStatusForm'].elements['trackingStatus.destinationGivenCode'].focus();" id="openpopup7.img" src="<c:url value='/images/open-popup.gif'/>" /></td>			
				<td><s:textfield cssStyle="width:145px"  cssClass="input-text" id="trackingdestinationGivenName" key="trackingStatus.destinationGivenName" onkeyup="findPartnerDetails('trackingdestinationGivenName','trackingDestinationGivenCode','trackingOriginDestinationGivenDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true)','',event);" required="true" tabindex="" />
				<div id="trackingOriginDestinationGivenDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</td>
				</tr>
				</tbody>
				</table>
				</td>
				</tr>
				 <tr>
				<td align="right" class="listwhitetext">Origin&nbsp;Received</td>
				<td colspan="4">
				<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
    			<tbody>
     			 <tr>
				<td><s:textfield cssClass="input-text" id="trackingOriginReceivedCode" key="trackingStatus.originReceivedCode" cssStyle="width:45px;" maxlength="8" onchange="valid(this,'special');checkOriginReceived();changeStatus();"  tabindex="" /></td>
				<td><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpenOriginReceived();changeStatus(); document.forms['trackingStatusForm'].elements['trackingStatus.originReceivedCode'].focus();" id="openpopup7.img" src="<c:url value='/images/open-popup.gif'/>" /></td>			
				<td><s:textfield cssStyle="width:145px"  cssClass="input-text" id="trackingOriginReceivedName" key="trackingStatus.originReceivedName" onkeyup="findPartnerDetails('trackingOriginReceivedName','trackingOriginReceivedCode','trackingOriginOriginReceivedDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true)','',event);" onchange="checkOriginReceived();changeStatus();" onblur="checkOriginReceived();" required="true" tabindex="" />
				<div id="trackingOriginOriginReceivedDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</td>
				</tr>
				</tbody>
				</table>
				</td>
				
				<td align="right" class="listwhitetext">Destination&nbsp;Received</td>
				<td colspan="4">
				<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
    			<tbody>
     			 <tr>
				<td><s:textfield cssClass="input-text" id="trackingDestinationReceivedCode" key="trackingStatus.destinationReceivedCode" cssStyle="width:45px;" maxlength="8" onchange="valid(this,'special'); checkDestinationReceived();changeStatus();"   tabindex="" /></td>
				<td><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpenDestinationReceived();changeStatus(); document.forms['trackingStatusForm'].elements['trackingStatus.destinationReceivedCode'].focus();" id="openpopup7.img" src="<c:url value='/images/open-popup.gif'/>" /></td>			
				<td><s:textfield cssStyle="width:145px"  cssClass="input-text" id="trackingDestinationReceivedName" key="trackingStatus.destinationReceivedName" onkeyup="findPartnerDetails('trackingDestinationReceivedName','trackingDestinationReceivedCode','trackingOriginDestinationReceivedDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true)','',event);"required="true" tabindex="" />
				<div id="trackingOriginDestinationReceivedDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</td>
				</tr>
				</tbody>
				</table>
				</td>
				</tr>			 
				</tbody>
				</table>
				</div>
				</td>
				</tr>				 
</tbody>

</table>
</c:if>         
<!--End  Agent Reciprocity -->
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:0px;">
<tbody>
<tr>
  <td height="10" width="100%" align="left" style="margin: 0px;">
      <div  onClick="javascript:animatedcollapse.toggle('weights'),checkMetricOrPoundUnitTab();" style="margin:0px;">
      <c:if test="${from=='rule'}">
      <c:if test="${field=='miscellaneous.estimateGrossWeight' || field=='miscellaneous.actualGrossWeight' || field=='miscellaneous.estimatedNetWeight' || field=='miscellaneous.actualNetWeight' || field=='miscellaneous.estimateCubicFeet' || field=='miscellaneous.netEstimateCubicFeet' || field=='miscellaneous.netActualCubicFeet' || field=='miscellaneous.netActualCubicMtr' || field=='miscellaneous.actualCubicMtr' || field=='miscellaneous.estimateCubicMtr' || field=='miscellaneous.netEstimateCubicMtr'}">               
      <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center"><font color="red">&nbsp;Weights & Volume</font>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
      </c:if></c:if>
      <c:if test="${from=='rule'}">
      <c:if test="${field!='miscellaneous.estimateGrossWeight' && field!='miscellaneous.actualGrossWeight' && field!='miscellaneous.estimatedNetWeight' && field!='miscellaneous.actualNetWeight' && field!='miscellaneous.estimateCubicFeet' && field!='miscellaneous.netEstimateCubicFeet' && field!='miscellaneous.netActualCubicFeet' && field!='miscellaneous.netActualCubicMtr' && field!='miscellaneous.actualCubicMtr' && field!='miscellaneous.estimateCubicMtr' && field!='miscellaneous.netEstimateCubicMtr'}">               
      <table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Weights & Volume
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
      </c:if></c:if>
      <c:if test="${from!='rule'}">
          <table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Weights & Volume
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
      </c:if>
          </div>
			<div id="weights" class="switchgroup1">
				  <table width="100%" border="0" class="detailTabLabel"  style="margin:3px 0 0 20px;" >
					   
					   <tr> <td width="30px"></td><td width="95px" align="right" class="listwhitetext"><a
										href="#" style="cursor:help"
										onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.equipment',this);return false"
										>Planned&nbsp;Equipment</a></td>
									<td style="width:340px;"><s:select cssClass="list-menu"
										name="miscellaneous.equipment" list="%{EQUIP}"
										cssStyle="width:222px" headerKey="" headerValue=" "
										onchange="changeStatus();" tabindex="21" /></td>
									 <td class="listwhitetext" align="left" colspan="0">Excess Weight Billing
							          <configByCorp:customDropDown 	listType="map" list="${excessWeightBillingList}" fieldValue="${miscellaneous.excessWeightBilling}"      attribute="id='serviceOrderForm_miscellaneous_excessWeightBilling' class=list-menu name='miscellaneous.excessWeightBilling'  style='width:224px;height:19px;' onchange='' tabindex=''  headerKey='' headerValue=''"/>
						              </td>
										</tr></table>
           	<table class="detailTabLabel" border="0" style="padding-left: 25px;">
							<tbody>
							<c:if test="${serviceOrder.commodity =='HHG/A' || serviceOrder.commodity =='AUTO' ||serviceOrder.commodity =='BOAT'}">
								<tr>
									<td colspan="2"></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.entitled' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.estimated' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.actual1' /></td></tr>										
<tr><td></td>
<td align="right" class="listwhitetext" width="120px"><fmt:message key='labels.autoboat'/></td>
<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleNumberAuto" size="6" maxlength="2" required="true" onchange="onlyNumeric(this);" tabindex="73"/></td>
<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimateAuto" size="6" maxlength="2" required="true" onchange="onlyNumeric(this);" tabindex="74"/></td>
<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualAuto" size="6" maxlength="2" required="true" onchange="onlyNumeric(this);" tabindex="75"/></td>
<td></td>
</tr>
<tr><td></td>
<td align="right" class="listwhitetext" width="100px"><fmt:message key='labels.vehicleweight'/></td>
<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleAutoWeight" size="6" maxlength="10" required="true" onchange="onlyFloat(this);" tabindex="76" /></td>
<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimatedAutoWeight" size="6" maxlength="10" required="true" onchange="onlyFloat(this);" tabindex="77"/></td>
<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualAutoWeight" size="6" maxlength="10" required="true" onchange="onlyFloat(this);" tabindex="78"/></td>
<td></td>
</tr>
<tr><td height="2px"></tr>
</c:if>
              <c:if test="${serviceOrder.commodity !='HHG/A' && serviceOrder.commodity !='AUTO'  && serviceOrder.commodity !='BOAT'}">
              <s:hidden name="miscellaneous.entitleNumberAuto"/>
              <s:hidden name="miscellaneous.estimateAuto"/>
               <s:hidden name="miscellaneous.actualAuto" />
               </c:if>
							</tbody>
						</table>
						<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;">
						<tbody>
 		  <tr>
 		  <td>
 		  <div  onClick="javascript:animatedcollapse.toggle('weightnvolumetracsub'),toIncrementWeightTabSub1();" style="margin: 0px"  >
						<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
						<tr>
						<td class="headtab_left" width="100px">
						</td>
						<td class="headtab_center" >&nbsp;(Pounds & Cft)
						<c:if test="${weightType == 'lbscft'}">			
						<INPUT type="radio" name="weightType" checked="checked" id="weightnVolumeRadio" value="lbscft" onclick="check(this);changeStatus();toCheckWeightnVolumeRadio1();">
						</c:if>
						<c:if test="${weightType != 'lbscft'}">			
						<INPUT type="radio" name="weightType" value="lbscft" id="weightnVolumeRadio" onclick="check(this);changeStatus();toCheckWeightnVolumeRadio1();">
						</c:if>
						</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_special" >&nbsp;
						</td>
						<td class="headtab_right">
						</td>
						</tr>
						</table>
						</div>		
                      <div id="weightnvolumetracsub" >
                     <table border="0" class="detailTabLabel" style="margin-left:22px;" >
<tr>
<td></td>
<td align="right" class="listwhitetext"><fmt:message key='labels.entitled'/></td>
<td align="right" class="listwhitetext"><fmt:message key='labels.estimated'/></td>
<td align="right" class="listwhitetext"><fmt:message key='labels.actual1'/></td>
<td align="right" class="listwhitetext"><fmt:message key='labels.chargeableWeight'/></td>
<td align="right" class="listwhitetext"><fmt:message key='labels.rewgh/fnl$'/></td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="128px"><fmt:message key='labels.grossweight'/></td>
<td align="right" ><s:textfield cssClass="input-text" cssStyle="text-align:right"  name="miscellaneous.entitleGrossWeight" size="6" required="true" maxlength="10"  onchange="onlyFloat(this); calcNetWeight1();" tabindex="22"/></td>
<td align="right" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimateGrossWeight" size="6" required="true" maxlength="10" onchange="onlyFloat(this);calcNetWeight2();" tabindex="27"/></td>
<td align="right" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualGrossWeight" size="6" required="true" maxlength="10" onchange="onlyFloat(this);findWeight(),calcNetWeight3();" tabindex="32"/></td>
<td align="right" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableGrossWeight" size="6" required="true" maxlength="10"  onchange="onlyFloat(this);calcNetWeight5();" tabindex="37"/></td>
<td align="right" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghGross" size="6" required="true" maxlength="10" onchange="onlyFloat(this);calcNetWeight4();" tabindex="42"/></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='labels.tareweight'/></td>
<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleTareWeight" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcNetWeight1();"  tabindex="23"/></td>
<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimateTareWeight" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcNetWeight2();"  tabindex="28"/></td>
<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualTareWeight" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcNetWeight3(),findTareWeight();" tabindex="33"/></td>
<td align="right" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableTareWeight" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcNetWeight5();" tabindex="38"/></td>
<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghTare" size="6" required="true" maxlength="10" onchange="onlyFloat(this);calcNetWeight4();" tabindex="43"/></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='labels.netweight'/></td>
<td align="right" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleNetWeight" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcNetWeightLbs1();" tabindex="24"/></td>
<td align="right" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimatedNetWeight" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcNetWeightLbs2();" tabindex="29"/></td>
<td align="right" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualNetWeight" size="6" required="true" maxlength="10" onchange="onlyFloat(this);findNetWeight();calcNetWeightLbs3();" tabindex="34"/></td>
<td align="right" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableNetWeight" size="6" required="true" maxlength="10" onchange="onlyNumeric(this);calcNetWeightLbs4();" tabindex="39"/></td>
<td align="right" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghNet" size="6" required="true" maxlength="10" onchange="onlyFloat(this);calcNetWeightLbs5();" tabindex="44"/></td>
<td>&nbsp;</td>
<td><c:if test="${empty serviceOrder.id}">
<td align="right" width="75px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty serviceOrder.id}">
<c:choose>
<c:when test="${countWeightDetailNotes == '0'|| countWeightDetailNotes == ''|| countWeightDetailNotes == null}">
<td align="right" width="430px"><img id="countWeightDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.sequenceNumber }&noteFor=ServiceOrder&subType=WeightDetail&imageId=countWeightDetailNotesImage&fieldId=countWeightDetailNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.sequenceNumber }&noteFor=ServiceOrder&subType=WeightDetail&imageId=countWeightDetailNotesImage&fieldId=countWeightDetailNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td align="right" width="430px"><img id="countWeightDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.sequenceNumber }&noteFor=ServiceOrder&subType=WeightDetail&imageId=countWeightDetailNotesImage&fieldId=countWeightDetailNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.sequenceNumber }&noteFor=ServiceOrder&subType=WeightDetail&imageId=countWeightDetailNotesImage&fieldId=countWeightDetailNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if></td>
</td></tr>
<tr>
<td align="left" colspan="10">
</td>
</tr>
<table class="detailTabLabel" border="0" width="100%">
<tr>
<td align="left" class="vertlinedata"></td></tr>
</table>
<table class="detailTabLabel" style="margin-left:21px;" >
<tr>
<td align="right" class="listwhitetext" width="130px"><fmt:message key='labels.volume1'/></td>
<td align="right"><s:textfield  cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleCubicFeet" size="6" maxlength="10" required="true"  onchange="onlyFloat(this);calcGrossVolume1();" tabindex="25"/></td>
<td align="right"><s:textfield  cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimateCubicFeet" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcGrossVolume2();" tabindex="30"/></td>
<td align="right"><s:textfield  cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualCubicFeet" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcGrossVolume3();findVolumeWeight();" tabindex="35"/></td>
<td align="right"><s:textfield  cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableCubicFeet" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcGrossVolume4();" tabindex="40"/></td>
<td align="right"><s:textfield  cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghCubicFeet" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcGrossVolume5();" tabindex="45"/></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='labels.volume2'/></td>
<td align="right"><s:textfield  cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netEntitleCubicFeet" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcNetVolume1();" tabindex="26"/></td>
<td align="right"><s:textfield  cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netEstimateCubicFeet" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcNetVolume2();" tabindex="31"/></td>
<td align="right"><s:textfield  cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netActualCubicFeet" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcNetVolume3();" tabindex="36"/></td>
<td align="right"><s:textfield  cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableNetCubicFeet" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcNetVolume4();" tabindex="41"/></td>
<td align="right"><s:textfield  cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghNetCubicFeet" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcNetVolume5();" tabindex="47"/></td>
</tr>
</table>
</td>
</tr>
<tr>
<td colspan="10">
<div  onClick="javascript:animatedcollapse.toggle('weightnvolumetracsub2'),toIncrementWeightTabSub2();" style="margin: 0px"  >
						<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
						<tr>
						<td class="headtab_left" width="100px">
						</td>
						<td class="headtab_center" >&nbsp;(Metric Units)
						<c:if test="${weightType == 'kgscbm'}">
                         <INPUT type="radio" name="weightType" value="kgscbm" checked="checked" id="weightnVolumeRadio2" onclick="check(this);changeStatus();toCheckWeightnVolumeRadio1();">
                        </c:if>
						<c:if test="${weightType != 'kgscbm'}">
                        <INPUT type="radio" name="weightType" value="kgscbm" id="weightnVolumeRadio2" onclick="check(this);changeStatus();toCheckWeightnVolumeRadio1();">
                        </c:if>
						
						</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_special" >&nbsp;
						</td>
						<td class="headtab_right">
						</td>
						</tr>
						</table>
						</div>	
<div id="weightnvolumetracsub2" > 
						<table class="detailTabLabel" border="0" style="margin-left:21px;"  >
							<tbody>
							<tr><td height="5px"></td></tr>
								<tr>
									<td></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.entitled' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.estimated' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.actual1' /></td>
									<td align="right" class="listwhitetext"><fmt:message
										key='labels.chargeableWeight' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.rewgh/fnl$' /></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext" width="130px"><fmt:message
										key='labels.grossweight' /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.entitleGrossWeightKilo" size="6"
										maxlength="10" required="true"
										 onchange="onlyFloat(this); calcNetWeightKgs1();" tabindex="48" /></td>
											<td align="right" colspan="1"><s:textfield
											cssClass="input-text" cssStyle="text-align:right"
											name="miscellaneous.estimateGrossWeightKilo" size="6"
											maxlength="10" required="true"											
											onchange="onlyFloat(this); calcNetWeightKgs2();" tabindex="53" /></td>
								
										<td align="right" colspan="1"><s:textfield
											cssClass="input-text" cssStyle="text-align:right"
											name="miscellaneous.actualGrossWeightKilo" size="6"
											maxlength="10" required="true"										
											onchange="onlyFloat(this);findWeightKilo(); calcNetWeightKgs3();" tabindex="58" /></td>
								
									<td align="right"><s:textfield cssClass="input-text"
										cssStyle="text-align:right"
										name="miscellaneous.chargeableGrossWeightKilo" size="6"
										maxlength="10" required="true"									
										onchange="onlyFloat(this); calcNetWeightKgs5();" tabindex="63" /></td>
									<td align="right"><s:textfield cssClass="input-text"
										cssStyle="text-align:right" name="miscellaneous.rwghGrossKilo"
										size="6" required="true" maxlength="10"										
										onchange="onlyFloat(this); calcNetWeightKgs4();" tabindex="68" /></td>									
								</tr>
								<tr>
									<td align="right" class="listwhitetext"><fmt:message
										key='labels.tareweight' /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.entitleTareWeightKilo" size="6" maxlength="8"
										required="true" 
										 onchange="onlyFloat(this); calcNetWeightKgs1();" tabindex="49" /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.estimateTareWeightKilo" size="6" maxlength="8"
										required="true" 
										onchange="onlyFloat(this); calcNetWeightKgs2();" tabindex="54" /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.actualTareWeightKilo" size="6" maxlength="8"
										required="true" 
										onchange="onlyFloat(this); findTareWeightKilo();calcNetWeightKgs3();" tabindex="59" /></td>
									<td align="right"><s:textfield cssClass="input-text"
										cssStyle="text-align:right"
										name="miscellaneous.chargeableTareWeightKilo" size="6"
										maxlength="8" required="true"										
										onchange="onlyFloat(this); calcNetWeightKgs5();" tabindex="64" /></td>
									<td align="right"><s:textfield cssClass="input-text"
										cssStyle="text-align:right" name="miscellaneous.rwghTareKilo"
										size="6" required="true" maxlength="8"										
										onchange="onlyFloat(this); calcNetWeightKgs4();" tabindex="69" /></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext" width=""><fmt:message
										key='labels.netweight' /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.entitleNetWeightKilo" size="6" maxlength="10"
										required="true"
										onchange="onlyFloat(this);calcNetWeightOnlyKgs1();" tabindex="50" /></td>								
										<td align="right" colspan="1"><s:textfield
											cssClass="input-text" cssStyle="text-align:right"
											name="miscellaneous.estimatedNetWeightKilo" size="6"
											maxlength="10" required="true"
											onchange="onlyFloat(this);calcNetWeightOnlyKgs2();" tabindex="55" /></td>									
										<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualNetWeightKilo" onchange="onlyFloat(this);findNetWeightKilo();calcNetWeightOnlyKgs3();" size="6" maxlength="10" required="true" 
											 tabindex="60" /></td>								
									<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableNetWeightKilo" onchange="onlyFloat(this);calcNetWeightOnlyKgs4();" size="6" maxlength="10" required="true"  tabindex="65" /></td>
									<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghNetKilo" size="6" onchange="onlyFloat(this);calcNetWeightOnlyKgs5();" maxlength="10" required="true"  tabindex="70" /></td>
									
								</tr>
							</tbody>
						</table>
						<table class="detailTabLabel" border="0" width="100%">
							<tbody>
								<tr>
									<td align="left" class="vertlinedata"></td>	</tr>
							</tbody>
						</table>
						<table class="detailTabLabel" border="0" style="margin-left:21px;" >
							<tbody>
								<tr>
									<td align="right" class="listwhitetext" width="130px"><fmt:message key='labels.volume1' /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleCubicMtr" onchange="onlyFloat(this); calcGrossVolumeMtr1();" size="6" maxlength="10" required="true"  tabindex="51" /></td>
										<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimateCubicMtr" onchange="onlyFloat(this); calcGrossVolumeMtr2();" size="6" maxlength="10" required="true"  tabindex="56" /></td>
										<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualCubicMtr" onchange="onlyFloat(this);findVolumeWeightCbm(); calcGrossVolumeMtr3();" size="6" maxlength="10" required="true"  tabindex="61" /></td>

									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableCubicMtr" onchange="onlyFloat(this); calcGrossVolumeMtr4();" size="6" maxlength="10" required="true"  tabindex="66" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right"name="miscellaneous.rwghCubicMtr" onchange="onlyFloat(this); calcGrossVolumeMtr5();" size="6" maxlength="10" required="true"  tabindex="71" /></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext"><fmt:message key='labels.volume2' /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netEntitleCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr1();" size="6" maxlength="10" required="true"  tabindex="52" /></td>
										<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netEstimateCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr2();" size="6" maxlength="10" required="true"  tabindex="57" /></td>
							
										<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netActualCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr3();" size="6" maxlength="10" required="true"  tabindex="62" /></td>
								
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableNetCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr4();" size="6" maxlength="10" required="true"  tabindex="67" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghNetCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr5();" size="6" maxlength="10" required="true" 
										tabindex="72" /></td>
								</tr>
								<tr>
									<td align="left" height="5px"></td>
								</tr>
							</tbody>
						</table>
						</div></td>
</tr>
</table>
</div>						
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
	<td height="10" width="100%" align="left" style="margin: 0px">
  <div  onClick="javascript:animatedcollapse.toggle('initiation')" style="margin: 0px">
      <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Initiation
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
	</div>
		<div id="initiation" class="switchgroup1">
	
		<table class="detailTabLabel" border="0" width="">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>	
<tr>
<td align="right" class="listwhitetext" width="148px"><fmt:message key='labels.booked'/></td>
<c:if test="${not empty customerFile.bookingDate}">
<s:text id="customerFileBookingDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.bookingDate"/></s:text>
<td><s:textfield cssClass="input-textUpper" id="createdOn" name="customerFile.bookingDate" value="%{customerFileBookingDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="72"/></td><%-- <td><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['trackingStatusForm'].createdOn,'calender',document.forms['trackingStatusForm'].dateFormat.value); return false;"/></td>--%>
</c:if>
<c:if test="${empty customerFile.bookingDate}">
<td><s:textfield cssClass="input-textUpper" id="createdOn" name="customerFile.bookingDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="72"/></td><%--<td><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['trackingStatusForm'].createdOn,'calender',document.forms['trackingStatusForm'].dateFormat.value); return false;"/></td>--%>
</c:if>
<td align="right" class="listwhitetext" width="93"><fmt:message key='trackingStatus.initialContact'/></td>
<c:if test="${not empty customerFile.initialContactDate}">
<s:text id="customerFileInitialFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.initialContactDate"/></s:text>
<td><s:textfield cssClass="input-textUpper" id="initialContactDate" name="customerFile.initialContactDate" value="%{customerFileInitialFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="72"/></td><%--<td><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['trackingStatusForm'].initialContact,'calender',document.forms['trackingStatusForm'].dateFormat.value); return false;"/></td>--%>
</c:if>
<c:if test="${empty customerFile.initialContactDate}">
<td><s:textfield cssClass="input-textUpper" id="initialContactDate" name="customerFile.initialContactDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="72"/></td><%--<td><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['trackingStatusForm'].initialContact,'calender',document.forms['trackingStatusForm'].dateFormat.value); return false;"/></td>--%>
</c:if>
<configByCorp:fieldVisibility componentId="trackingStatus.surveyDate">
<td align="right" class="listwhitetext" width="80"><fmt:message key='trackingStatus.survey'/></td>
<c:if test="${not empty trackingStatus.surveyDate}">
<s:text id="trackingStatusSurveyDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.surveyDate"/></s:text>
<td><s:textfield cssClass="input-text" id="surveyDate" name="trackingStatus.surveyDate" value="%{trackingStatusSurveyDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td><td><img id="surveyDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.surveyDate}">
<td><s:textfield cssClass="input-text" id="surveyDate" name="trackingStatus.surveyDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td><td><img id="surveyDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td align="right" class="listwhitetext" width="103"><b><fmt:message key='labels.surveyTime'/></b><img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" ></td>
<td align="right" class="listwhitetext"><fmt:message key='customerFile.surveyTime'/></td>
<td><s:textfield cssClass="input-text" name="trackingStatus.surveyTimeFrom" size="2" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" onchange = "completeTimeString();" tabindex="72"/> </td>
<td align="right" class="listwhitetext"><fmt:message key='customerFile.surveyTime2'/></td>
<td width="100px"><s:textfield cssClass="input-text" name="trackingStatus.surveyTimeTo" size="2" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" onchange = "completeTimeString();" tabindex="72" /><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" ></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="customerFile.survey">
<td align="right" class="listwhitetext" width="80"><fmt:message key='trackingStatus.survey'/></td>
<c:if test="${not empty customerFile.survey}">
<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.survey"/></s:text>
<td><s:textfield cssClass="input-textUpper" id="survey" name="customerFile.survey" value="%{customerFileSurveyFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="72"/></td><%--<td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['customerFileForm'].survey,'calender2',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>--%>
</c:if>
<c:if test="${empty customerFile.survey}">
<td><s:textfield cssClass="input-textUpper" id="survey" name="customerFile.survey" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="72"/></td><%--<td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['customerFileForm'].survey,'calender2',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>--%>
</c:if>
<td align="right" class="listwhitetext" width="103"><b><fmt:message key='labels.surveyTime'/></b><img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" ></td>
<td align="right" class="listwhitetext"><fmt:message key='customerFile.surveyTime'/></td>
<td><s:textfield cssClass="input-textUpper" name="customerFile.surveyTime" id="surveyTime" size="3" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" readonly="true" tabindex="72"/> </td>
<td align="right" class="listwhitetext" width="17"><fmt:message key='customerFile.surveyTime2'/></td>
<td width="100px"><s:textfield cssClass="input-textUpper" name="customerFile.surveyTime2" id="surveyTime2" size="3" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" readonly="true" tabindex="72"/><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" ></td>
</configByCorp:fieldVisibility>
<c:if test="${empty serviceOrder.customerFile.id}">
<td align="right" style="width:50px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty serviceOrder.customerFile.id}">
<c:choose>
<c:when test="${countSurveyNotes == '0' || countSurveyNotes == '' || countSurveyNotes == null}">
<td align="right" style="width:50px"><img id="countSurveyNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.customerFile.id }&notesId=${serviceOrder.customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.customerFile.id }&notesId=${serviceOrder.customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td align="right" style="width:50px"><img id="countSurveyNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.customerFile.id }&notesId=${serviceOrder.customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.customerFile.id }&notesId=${serviceOrder.customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr><td align="left" height="5px"></td></tr>
</tbody>
</table>
</div></td>
</tr> 
<tr>
   <td height="10" width="100%" align="left" >
   <c:if test="${from=='rule'}">
   <c:if test="${field=='trackingStatus.sitOriginTA'|| field=='trackingStatus.leftWHon'|| field=='trackingStatus.ISFSubmitted'|| field=='trackingStatus.managerOkOn' || field=='trackingStatus.loadFollowUpCall' || field=='trackingStatus.loadA' || field=='trackingStatus.beginLoad' || field=='trackingStatus.packDayConfirmCall' || field=='trackingStatus.reconfirmShipper' || field=='trackingStatus.packA' || field=='trackingStatus.beginPacking' || field=='trackingStatus.packDone' || field=='miscellaneous.confirmOriginAgent' || field=='trackingStatus.orderDate' || field=='trackingStatus.contractReceived' || field=='trackingStatus.packConfirmCallPriorOneDay' || field=='trackingStatus.packConfirmCallPriorThreeDay'}">
    <div  onClick="javascript:animatedcollapse.toggle('orgpack')">
     <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center"><font color="red">&nbsp;Origin Packout</font>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>
</c:if></c:if>
<c:if test="${from=='rule'}">
  <c:if test="${field!='trackingStatus.sitOriginTA' && field!='trackingStatus.ISFSubmitted' && field!='trackingStatus.managerOkOn' && field!='trackingStatus.loadFollowUpCall' && field!='trackingStatus.loadA' && field!='trackingStatus.beginLoad' && field!='trackingStatus.packDayConfirmCall' && field!='trackingStatus.reconfirmShipper' && field!='trackingStatus.packA' && field!='trackingStatus.beginPacking' && field!='trackingStatus.packDone' && field!='miscellaneous.confirmOriginAgent' && field!='trackingStatus.orderDate' && field!='trackingStatus.leftWHon' && field!='trackingStatus.packConfirmCallPriorOneDay' && field!='trackingStatus.packConfirmCallPriorThreeDay' || field=='trackingStatus.contractReceived'}">
  <div  onClick="javascript:animatedcollapse.toggle('orgpack')">
          <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Origin Packout
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
          </div>
  </c:if></c:if>
      <c:if test="${from!='rule'}">
      <div onClick="javascript:animatedcollapse.toggle('orgpack')">
          <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Origin Packout
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>
  </c:if>			
		<div id="orgpack" class="switchgroup1">	
		<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" width="100%">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>	
<tr>
<td>
<table class="detailTabLabel" border="0" style="margin-left:14px;">
<tbody>
<tr>
<td colspan="15">
<table  border="0" style="margin: 0px;padding: 0px;">
<tr>
<td align="right" class="listwhitetext" width="116"><fmt:message key='trackingStatus.reconfirmShipper'/></td>
<c:if test="${not empty trackingStatus.reconfirmShipper}">
<s:text id="trackingStatusReconfirmShipperFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.reconfirmShipper"/></s:text>
<td width=""><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_reconfirmShipper" name="trackingStatus.reconfirmShipper" value="%{trackingStatusReconfirmShipperFormattedValue}" required="true" cssStyle="width:65px; margin-left:0px;" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_reconfirmShipper-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_reconfirmShipper" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.reconfirmShipper}">
<td width=""><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_reconfirmShipper" name="trackingStatus.reconfirmShipper" required="true" cssStyle="width:65px;margin-left:3px;" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_reconfirmShipper-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_reconfirmShipper" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${dateColumnShow=='N'}">
<td width="91"></td>
</c:if>
<c:if test="${dateColumnShow=='Y'}">
<td width="75"></td>
</c:if>
<td align="right" class="listwhitetext" colspan="3">
<table  border="0" style="margin: 0px;padding: 0px;">
<tr>
<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.confirmOriginAgent'/></td>
<c:if test="${not empty miscellaneous.confirmOriginAgent}">
<s:text id="miscellaneousConfirmOriginAgentFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.confirmOriginAgent"/></s:text>
<td colspan="2" width="95"><s:textfield cssClass="input-text" id="trackingStatusForm_miscellaneous_confirmOriginAgent" name="miscellaneous.confirmOriginAgent" value="%{miscellaneousConfirmOriginAgentFormattedValue}" required="true" cssStyle="width:65px;margin-left:2px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="trackingStatusForm_miscellaneous_confirmOriginAgent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_miscellaneous_confirmOriginAgent" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.confirmOriginAgent}">
<td colspan="2" width="95"><s:textfield cssClass="input-text" id="trackingStatusForm_miscellaneous_confirmOriginAgent" name="miscellaneous.confirmOriginAgent" required="true" cssStyle="width:65px;margin-left:2px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="trackingStatusForm_miscellaneous_confirmOriginAgent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_miscellaneous_confirmOriginAgent" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
</table>
</td>
<td align="right" class="listwhitetext" width="136"><fmt:message key='trackingStatus.orderDate'/></td>
<c:if test="${not empty trackingStatus.orderDate}">
<s:text id="trackingStatusOrderDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.orderDate"/></s:text>
<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_orderDate" name="trackingStatus.orderDate" value="%{trackingStatusOrderDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_orderDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_orderDate" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.orderDate}">
<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_orderDate" name="trackingStatus.orderDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_orderDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_orderDate" HEIGHT=20 WIDTH=20/></td>
</c:if>
<configByCorp:fieldVisibility componentId="component.button.trackingStatusContractReceived">
 <td align="right" class="listwhitetext">Contract&nbsp;Received</td>
          
			 <c:if test="${not empty trackingStatus.contractReceived}"> 
			  <s:text id="accountLineFormattedEstimateContractValueDate" name="${FormDateValue}"><s:param name="value" value="trackingStatus.contractReceived"/></s:text>
			  <td colspan="2"><s:textfield id="contractReceived" name="trackingStatus.contractReceived" value="%{accountLineFormattedEstimateContractValueDate}" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" cssStyle="width:65px" />
			  <img id="contractReceived_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
			 </td>
			 </c:if>
			     <c:if test="${empty trackingStatus.contractReceived}">
			   <td colspan="2"><s:textfield id="contractReceived" name="trackingStatus.contractReceived" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" cssStyle="width:65px" />
			   <img id="contractReceived_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
			   </td>
   </c:if>
	</configByCorp:fieldVisibility>
</tr>
</table>
</td>
</tr>



<tr>
<td width="118"></td>
<td align="center" class="listwhitetext"><b><fmt:message key='labels.target'/></b></td>
<td></td>
<c:if test="${dateColumnShow=='Y'}">
<td class="listwhitetext"><b><fmt:message key='labels.targetPackEnding'/></b></td>

<td width="5"></td>
</c:if>
<td align="center" width="60" class="listwhitetext"><b><fmt:message key='labels.actual'/></b></td>
<c:if test="${dateColumnShow=='Y'}">
<td class="listwhitetext"><b><fmt:message key='labels.beginPackingEnd'/></b></td>
</c:if>
<c:if test="${dateColumnShow=='N'}">
<td></td>
</c:if>
<td width="10" colspan="5"></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.beginPacking'/></td>
<c:if test="${not empty trackingStatus.beginPacking}">
<s:text id="trackingStatusBeginPackingFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.beginPacking"/></s:text>
<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_beginPacking" name="trackingStatus.beginPacking" value="%{trackingStatusBeginPackingFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onchange="isUpdaterFlag();"/></td><td width="20"><img id="trackingStatusForm_trackingStatus_beginPacking-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_beginPacking" HEIGHT=20 WIDTH=20 /></td>
</c:if>
<c:if test="${empty trackingStatus.beginPacking}">
<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_beginPacking" name="trackingStatus.beginPacking" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onchange="isUpdaterFlag();"/></td><td width="20"><img id="trackingStatusForm_trackingStatus_beginPacking-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_beginPacking" HEIGHT=20 WIDTH=20 /></td>
</c:if>
<c:if test="${dateColumnShow=='Y'}">
<c:if test="${not empty trackingStatus.targetPackEnding}">
<s:text id="trackingStatustargetPackEndingFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.targetPackEnding"/></s:text>
<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_targetPackEnding" name="trackingStatus.targetPackEnding" value="%{trackingStatustargetPackEndingFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onchange="isUpdaterFlag();"/></td><td><img id="trackingStatusForm_trackingStatus_targetPackEnding-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_targetPackEnding" HEIGHT=20 WIDTH=20 /></td>
</c:if>
<c:if test="${empty trackingStatus.targetPackEnding}">
<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_targetPackEnding" name="trackingStatus.targetPackEnding" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onchange="isUpdaterFlag();"/></td><td><img id="trackingStatusForm_trackingStatus_targetPackEnding-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_targetPackEnding" HEIGHT=20 WIDTH=20 /></td>
</c:if>
<td colspan="1">
<table style="margin:0px;margin-bottom:-2px;padding:0px;" cellpadding="0" cellspacing="3">
<tr>
<c:if test="${not empty trackingStatus.actualPackBegin}">
<s:text id="trackingStatusactualPackBeginFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.actualPackBegin"/></s:text>
<td width="65px"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_actualPackBegin" name="actualPackBegin" value="%{trackingStatusactualPackBeginFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_actualPackBegin-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png"  textelementname="trackingStatusForm_actualPackBegin" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test='${serviceOrder.status == "CNCL"}'>
<td><img id="trackingStatusForm_trackingStatus_actualPackBegin-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_actualPackBegin" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
<c:if test='${serviceOrder.status == "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_actualPackBegin-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_actualPackBegin" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
<fmt:formatDate var="trackingStatusactualPackBegin" value="${trackingStatus.actualPackBegin}" pattern="${displayDateTimeEditFormat}"/>
<s:hidden name="trackingStatus.actualPackBegin" value="${trackingStatusactualPackBegin}"/>
</c:if>
<c:if test="${empty trackingStatus.actualPackBegin}">
<td width="65px"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_actualPackBegin" name="actualPackBegin" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_actualPackBegin-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_actualPackBegin" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test='${serviceOrder.status == "CNCL"}'>
<td><img id="trackingStatusForm_trackingStatus_actualPackBegin-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_actualPackBegin" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
<c:if test='${serviceOrder.status == "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_actualPackBegin-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_actualPackBegin" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
</c:if>
</tr>
</table>
</td>
</c:if>
<c:if test="${not empty trackingStatus.packA}">
<s:text id="trackingStatusPackAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.packA"/></s:text>
<td width="65px"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_packA" name="packA" value="%{trackingStatusPackAFormattedValue}" onselect="calculateRddDays()" onchange="calculateRddDays();" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_packA-trigger" name="packA" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName(this.name);" textelementname="trackingStatusForm_packA" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test='${serviceOrder.status == "CNCL"}'>
<td><img id="trackingStatusForm_trackingStatus_packA-trigger" name="packA" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_packA" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
<c:if test='${serviceOrder.status == "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_packA-trigger" name="packA" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_packA" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
<fmt:formatDate var="trackingStatusPackA" value="${trackingStatus.packA}" pattern="${displayDateTimeEditFormat}"/>
<s:hidden name="trackingStatus.packA" value="${trackingStatusPackA}"/>
</c:if>
<c:if test="${empty trackingStatus.packA}">
<td width="65px"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_packA" name="packA" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calculateRddDays()" onchange="calculateRddDays();"/></td>
<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_packA-trigger" name="packA" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_packA" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test='${serviceOrder.status == "CNCL"}'>
<td><img id="trackingStatusForm_trackingStatus_packA-trigger" name="packA" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_packA" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
<c:if test='${serviceOrder.status == "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_packA-trigger" name="packA" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_packA" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
</c:if>
<c:if test="${dateColumnShow=='N'}">
<td colspan="4"></td>
</c:if>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.packDayConfirmCall'/></td>
<c:if test="${not empty trackingStatus.packDayConfirmCall}">
<s:text id="trackingStatusPackDayConfirmCallFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.packDayConfirmCall"/></s:text>
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_packDayConfirmCall" name="trackingStatus.packDayConfirmCall" value="%{trackingStatusPackDayConfirmCallFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_packDayConfirmCall-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_packDayConfirmCall" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.packDayConfirmCall}">
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_packDayConfirmCall" name="trackingStatus.packDayConfirmCall" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_packDayConfirmCall-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_packDayConfirmCall" HEIGHT=20 WIDTH=20/></td>
</c:if>
<configByCorp:fieldVisibility componentId="component.field.AccountLine.payRate">
<td align="right" class="listwhitetext">Packing&nbsp;1Day&nbsp;Prior&nbsp;Call</td>
<c:if test="${not empty trackingStatus.packConfirmCallPriorOneDay}">
<s:text id="trackingStatusPackConfirmCallPriorOneDayFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.packConfirmCallPriorOneDay"/></s:text>
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_packConfirmCallPriorOneDay" name="trackingStatus.packConfirmCallPriorOneDay" value="%{trackingStatusPackConfirmCallPriorOneDayFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_packConfirmCallPriorOneDay-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_packConfirmCallPriorOneDay" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.packConfirmCallPriorOneDay}">
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_packConfirmCallPriorOneDay" name="trackingStatus.packConfirmCallPriorOneDay" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_packConfirmCallPriorOneDay-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_packConfirmCallPriorOneDay" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td align="right" class="listwhitetext">Packing&nbsp;3Day&nbsp;Prior&nbsp;Call</td>
<c:if test="${not empty trackingStatus.packConfirmCallPriorThreeDay}">
<s:text id="trackingStatusPackConfirmCallPriorThreeDayFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.packConfirmCallPriorThreeDay"/></s:text>
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_packConfirmCallPriorThreeDay" name="trackingStatus.packConfirmCallPriorThreeDay" value="%{trackingStatusPackConfirmCallPriorThreeDayFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_packConfirmCallPriorThreeDay-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_packConfirmCallPriorThreeDay" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.packConfirmCallPriorThreeDay}">
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_packConfirmCallPriorThreeDay" name="trackingStatus.packConfirmCallPriorThreeDay" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_packConfirmCallPriorThreeDay-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_packConfirmCallPriorThreeDay" HEIGHT=20 WIDTH=20/></td>
</c:if>
</configByCorp:fieldVisibility>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.beginLoad'/></td>
<c:if test="${not empty trackingStatus.beginLoad}">
<s:text id="trackingStatusBeginLoadFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.beginLoad"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_beginLoad" name="trackingStatus.beginLoad" value="%{trackingStatusBeginLoadFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td><td><img id="trackingStatusForm_trackingStatus_beginLoad-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_beginLoad" HEIGHT=20 WIDTH=20 /></td>
</c:if>
<c:if test="${empty trackingStatus.beginLoad}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_beginLoad" name="trackingStatus.beginLoad" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td><td><img id="trackingStatusForm_trackingStatus_beginLoad-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_beginLoad" HEIGHT=20 WIDTH=20 /></td>
</c:if>
<c:if test="${dateColumnShow=='Y'}">
<c:if test="${not empty trackingStatus.targetLoadEnding}">
<s:text id="trackingStatustargetLoadEndingFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.targetLoadEnding"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_targetLoadEnding" name="trackingStatus.targetLoadEnding" value="%{trackingStatustargetLoadEndingFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td><td><img id="trackingStatusForm_trackingStatus_targetLoadEnding-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_targetLoadEnding" HEIGHT=20 WIDTH=20 /></td>
</c:if>
<c:if test="${empty trackingStatus.targetLoadEnding}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_targetLoadEnding" name="trackingStatus.targetLoadEnding" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td><td><img id="trackingStatusForm_trackingStatus_targetLoadEnding-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_targetLoadEnding" HEIGHT=20 WIDTH=20 /></td>
</c:if>
<td colspan="1">
<table style="margin:0px;margin-bottom:-2px;padding:0px;" cellpadding="0" cellspacing="3">
<tr>
<c:if test="${not empty trackingStatus.actualLoadBegin}">
<s:text id="trackingStatusactualLoadBeginFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.actualLoadBegin"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_actualLoadBegin" name="actualLoadBegin"    value="%{trackingStatusactualLoadBeginFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_actualLoadBegin-trigger"   name="actualLoadBegin" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_actualLoadBegin" HEIGHT=20 WIDTH=20 onclick="forOriDays(); return false;"/></td>
</c:if>
<c:if test='${serviceOrder.status == "CNCL"}'>
<td><img id="trackingStatusForm_trackingStatus_actualLoadBegin-trigger"  name="actualLoadBegin" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_actualLoadBegin" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
<c:if test='${serviceOrder.status == "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_actualLoadBegin-trigger"  name="actualLoadBegin" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_actualLoadBegin" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
<fmt:formatDate var="trackingStatusactualLoadBegin" value="${trackingStatus.actualLoadBegin}" pattern="${displayDateTimeEditFormat}"/>
<s:hidden name="trackingStatus.actualLoadBegin" value="${trackingStatusactualLoadBegin}"/>
</c:if>
<c:if test="${empty trackingStatus.actualLoadBegin}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_actualLoadBegin" name="actualLoadBegin"   required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_actualLoadBegin-trigger"  name="actualLoadBegin" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_actualLoadBegin" HEIGHT=20 WIDTH=20 onclick="forOriDays();return false;"/></td>
</c:if>
<c:if test='${serviceOrder.status == "CNCL"}'>
<td><img id="trackingStatusForm_trackingStatus_actualLoadBegin-trigger"  name="actualLoadBegin" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_actualLoadBegin" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
<c:if test='${serviceOrder.status == "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_actualLoadBegin-trigger"  name="actualLoadBegin" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_actualLoadBegin" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
</c:if>
</tr>
</table>
</td>
</c:if>
<c:if test="${not empty trackingStatus.loadA}">
<s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.loadA"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_loadA" name="loadA" onselect="calculateDeliveryDate();calculateRddDays()" onchange="calculateDeliveryDate(); calculateRddDays()"  onblur="calculateDeliveryDate()" value="%{trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_loadA-trigger"   name="loadA" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_loadA" HEIGHT=20 WIDTH=20 onclick="forOriDays(); return false;"/></td>
</c:if>
<c:if test='${serviceOrder.status == "CNCL"}'>
<td><img id="trackingStatusForm_trackingStatus_loadA-trigger"  name="loadA" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_loadA" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
<c:if test='${serviceOrder.status == "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_loadA-trigger"  name="loadA" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_loadA" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
<fmt:formatDate var="trackingStatusLoadA" value="${trackingStatus.loadA}" pattern="${displayDateTimeEditFormat}"/>
<s:hidden name="trackingStatus.loadA" value="${trackingStatusLoadA}"/>
</c:if>
<c:if test="${empty trackingStatus.loadA}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_loadA" name="loadA" onchange="calculateDeliveryDate();calculateRddDays()" onblur="calculateDeliveryDate();calculateRddDays()" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_loadA-trigger"  name="loadA" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_loadA" HEIGHT=20 WIDTH=20 onclick="forOriDays();return false;"/></td>
</c:if>
<c:if test='${serviceOrder.status == "CNCL"}'>
<td><img id="trackingStatusForm_trackingStatus_loadA-trigger"  name="loadA" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_loadA" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
<c:if test='${serviceOrder.status == "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_loadA-trigger"  name="loadA" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_loadA" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
</c:if>
<c:if test="${dateColumnShow=='N'}">
<td colspan="4"></td>
</c:if>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.loadFollowUpCall'/></td>
<c:if test="${not empty trackingStatus.loadFollowUpCall}">
<s:text id="trackingStatusLoadFollowUpCallFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.loadFollowUpCall"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_loadFollowUpCall" name="trackingStatus.loadFollowUpCall" value="%{trackingStatusLoadFollowUpCallFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_loadFollowUpCall-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_loadFollowUpCall" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.loadFollowUpCall}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_loadFollowUpCall" name="trackingStatus.loadFollowUpCall" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_loadFollowUpCall-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_loadFollowUpCall" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.packRoomA'/></td>
<c:if test="${not empty trackingStatus.packRoomA}">
<s:text id="trackingStatusPackRoomAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.packRoomA"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_packRoomA" name="trackingStatus.packRoomA" value="%{trackingStatusPackRoomAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_packRoomA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_packRoomA" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.packRoomA}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_packRoomA" name="trackingStatus.packRoomA" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_packRoomA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_packRoomA" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${dateColumnShow=='Y'}">
<td></td>
<td></td>
</c:if>
<c:if test="${dateColumnShow=='Y'}">
<c:if test="${not empty trackingStatus.packDone}">
<s:text id="trackingStatusPackDoneFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.packDone"/></s:text>
<td style="padding-left:4px;">
<s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_packDone" name="trackingStatus.packDone" value="%{trackingStatusPackDoneFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/>
<img id="trackingStatusForm_trackingStatus_packDone-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_packDone" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.packDone}">
<td style="padding-left:4px;"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_packDone" name="trackingStatus.packDone" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/>
<img id="trackingStatusForm_trackingStatus_packDone-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_packDone" HEIGHT=20 WIDTH=20/></td>
</c:if>
</c:if>
<c:if test="${dateColumnShow=='N'}">
<c:if test="${not empty trackingStatus.packDone}">
<s:text id="trackingStatusPackDoneFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.packDone"/></s:text>
<td>
<s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_packDone" name="trackingStatus.packDone" value="%{trackingStatusPackDoneFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
<td><img id="trackingStatusForm_trackingStatus_packDone-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_packDone" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.packDone}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_packDone" name="trackingStatus.packDone" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
<td><img id="trackingStatusForm_trackingStatus_packDone-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_packDone" HEIGHT=20 WIDTH=20/></td>
</c:if>
</c:if>
<c:if test="${dateColumnShow=='N'}">
<td colspan="2"></td>
<c:set var="dynaColSpan1" value="2" />
</c:if>
<c:if test="${dateColumnShow=='Y'}">
<c:set var="dynaColSpan1" value="2" />
</c:if>
<td colspan="${dynaColSpan1}"></td>
<td align="right" class="listwhitetext" width=""><fmt:message key='trackingStatus.leftWHOn'/></td>
<c:if test="${not empty trackingStatus.leftWHOn}">
<s:text id="trackingStatusLeftWHOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.leftWHOn"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_leftWHOn" name="trackingStatus.leftWHOn" value="%{trackingStatusLeftWHOnFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_leftWHOn-trigger" name="leftWHOn" onclick="setFieldName(this.name)" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_leftWHOn" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.leftWHOn}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_leftWHOn" name="trackingStatus.leftWHOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_leftWHOn-trigger" name="leftWHOn" onclick="setFieldName(this.name)" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_leftWHOn" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.managerOkInital'/></td>
<td align="left" ><s:textfield cssClass="input-text"  name="trackingStatus.managerOkInital" required="true" cssStyle="width:65px" maxlength="2" onchange="autoPopulate_trackingStatus_approvedDate(this);" tabindex="79"/></td>
<td></td>
<c:if test="${dateColumnShow=='Y'}">
<td></td>
<td></td>
</c:if>
<c:if test="${dateColumnShow=='Y'}">
<c:if test="${from=='rule'}">
<c:if test="${field=='trackingStatus.managerOkOn'}">
<c:if test="${not empty trackingStatus.managerOkOn}">
<s:text id="trackingStatusOkDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.managerOkOn"/></s:text>
<td style="padding-left:4px;"><s:textfield cssClass="rules-textUpper" id="trackingStatusForm_trackingStatus_managerOkOn" name="trackingStatus.managerOkOn" value="%{trackingStatusOkDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="trackingStatusForm_trackingStatus_managerOkOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_managerOkOn" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.managerOkOn}">
<td style="padding-left:4px;"><s:textfield cssClass="rules-textUpper" id="trackingStatusForm_trackingStatus_managerOkOn" name="trackingStatus.managerOkOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="trackingStatusForm_trackingStatus_managerOkOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_managerOkOn" HEIGHT=20 WIDTH=20/></td>
</c:if>
</c:if></c:if>
<c:if test="${from=='rule'}">
<c:if test="${field!='trackingStatus.managerOkOn'}">
<c:if test="${not empty trackingStatus.managerOkOn}">
<s:text id="trackingStatusOkDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.managerOkOn"/></s:text>
<td style="padding-left:4px;"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_managerOkOn" name="trackingStatus.managerOkOn" value="%{trackingStatusOkDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="trackingStatusForm_trackingStatus_managerOkOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_managerOkOn" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.managerOkOn}">
<td style="padding-left:4px;"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_managerOkOn" name="trackingStatus.managerOkOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="trackingStatusForm_trackingStatus_managerOkOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_managerOkOn" HEIGHT=20 WIDTH=20/></td>
</c:if>
</c:if></c:if>
<c:if test="${from!='rule'}">
<c:if test="${not empty trackingStatus.managerOkOn}">
<s:text id="trackingStatusOkDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.managerOkOn"/></s:text>
<td style="padding-left:4px;"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_managerOkOn" name="trackingStatus.managerOkOn" value="%{trackingStatusOkDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="trackingStatusForm_trackingStatus_managerOkOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_managerOkOn" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.managerOkOn}">
<td style="padding-left:4px;"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_managerOkOn" name="trackingStatus.managerOkOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="trackingStatusForm_trackingStatus_managerOkOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_managerOkOn" HEIGHT=20 WIDTH=20/></td>
</c:if>
</c:if>
</c:if>
<c:if test="${dateColumnShow=='N'}">
<c:if test="${from=='rule'}">
<c:if test="${field=='trackingStatus.managerOkOn'}">
<c:if test="${not empty trackingStatus.managerOkOn}">
<s:text id="trackingStatusOkDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.managerOkOn"/></s:text>
<td ><s:textfield cssClass="rules-textUpper" id="trackingStatusForm_trackingStatus_managerOkOn" name="trackingStatus.managerOkOn" value="%{trackingStatusOkDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_managerOkOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_managerOkOn" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.managerOkOn}">
<td><s:textfield cssClass="rules-textUpper" id="trackingStatusForm_trackingStatus_managerOkOn" name="trackingStatus.managerOkOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_managerOkOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_managerOkOn" HEIGHT=20 WIDTH=20/></td>
</c:if>
</c:if></c:if>
<c:if test="${from=='rule'}">
<c:if test="${field!='trackingStatus.managerOkOn'}">
<c:if test="${not empty trackingStatus.managerOkOn}">
<s:text id="trackingStatusOkDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.managerOkOn"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_managerOkOn" name="trackingStatus.managerOkOn" value="%{trackingStatusOkDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_managerOkOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_managerOkOn" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.managerOkOn}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_managerOkOn" name="trackingStatus.managerOkOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_managerOkOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_managerOkOn" HEIGHT=20 WIDTH=20/></td>
</c:if>
</c:if></c:if>
<c:if test="${from!='rule'}">
<c:if test="${not empty trackingStatus.managerOkOn}">
<s:text id="trackingStatusOkDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.managerOkOn"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_managerOkOn" name="trackingStatus.managerOkOn" value="%{trackingStatusOkDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_managerOkOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_managerOkOn" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.managerOkOn}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_managerOkOn" name="trackingStatus.managerOkOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_managerOkOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_managerOkOn" HEIGHT=20 WIDTH=20/></td>
</c:if>
</c:if>
</c:if>
<c:if test="${dateColumnShow=='N'}">
<c:set var="dynaColSpan" value="4" />
</c:if>
<c:if test="${dateColumnShow=='Y'}">
<c:set var="dynaColSpan" value="3" />
</c:if>
<td align="left" class="listwhitetext" colspan="${dynaColSpan}">
<table  border="0" style="margin:0px 0px 0px -62px;padding: 0px;">
<tr>
<c:if test="${dateColumnShow=='N'}">
<td width="5"></td>
</c:if>
<c:if test="${serviceOrder.commodity =='HHG/A' || serviceOrder.commodity =='AUTO' ||serviceOrder.commodity =='BOAT'}">
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.vehicleRecived'/></td>
<c:if test="${not empty trackingStatus.vehicleRecived}">
<s:text id="trackingStatusVehicleRecivedFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.vehicleRecived"/></s:text>
<td><s:textfield cssClass="input-text" id="vehicleRecived" name="trackingStatus.vehicleRecived" value="%{trackingStatusVehicleRecivedFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="vehicleRecived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.vehicleRecived}">
<td><s:textfield cssClass="input-text" id="vehicleRecived" name="trackingStatus.vehicleRecived" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="vehicleRecived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</c:if>
</tr>
</table>
</td>
<td align="left" class="listwhitetext" colspan="6">
<table  border="0" style="margin: 0px;padding: 0px;margin-left:-50px;">
<tr>
<c:if test="${(serviceOrder.destinationCountryCode =='USA' || serviceOrder.destinationCountryCode =='VIR') && (serviceOrder.routing =='IMP' || serviceOrder.routing =='NRI' || serviceOrder.routing =='EXP') && serviceOrder.mode =='Sea'}">
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.ISFSubmitted'/></td>
<c:if test="${not empty trackingStatus.ISFSubmitted}">
<s:text id="trackingStatusISFSubmittedFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.ISFSubmitted"/></s:text>
<td><s:textfield cssClass="input-text" id="ISFSubmitted" name="trackingStatus.ISFSubmitted" value="%{trackingStatusISFSubmittedFormattedValue}" required="true" cssStyle="width:66px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="ISFSubmitted-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.ISFSubmitted}">
<td><s:textfield cssClass="input-text" id="ISFSubmitted" name="trackingStatus.ISFSubmitted" required="true" cssStyle="width:66px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="ISFSubmitted-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td></td>
<td align="right" class="listwhitetext">ISF&nbsp;Confirmation&nbsp;#</td>
<td align="left" ><s:textfield cssClass="input-text"  name="trackingStatus.ISFConfirmationNo" required="true" cssStyle="width:90px;" maxlength="20" tabindex="79" /></td>
</c:if>
<c:if test="${(serviceOrder.destinationCountryCode !='USA' || serviceOrder.destinationCountryCode !='VIR') && (serviceOrder.routing !='IMP' && serviceOrder.routing !='NRI' && serviceOrder.routing !='EXP') && serviceOrder.mode !='Sea'}">
<s:hidden name="trackingStatus.ISFConfirmationNo" />
</c:if>

   <configByCorp:fieldVisibility componentId="component.trackingStatus.packingConfirmationSent">
<td align="right" class="listwhitetext" width="">Packing Confirmation Sent</td>
<td ><s:select cssStyle="width:70px" cssClass="list-menu" name="trackingStatus.PackingConfirmationSent" onchange="changeStatus();" list="{'','N','Y'}" /></td>
</configByCorp:fieldVisibility>
</tr></table></td>









</tr>
<tr>
<td align="right" class="listwhitetext"><configByCorp:fieldVisibility componentId="trackingStatus.documentSentClient"><fmt:message key='trackingStatus.documentSentClient'/></configByCorp:fieldVisibility></td>
<c:if test="${not empty trackingStatus.documentSentClient}">
<s:text id="trackingStatusDocumentSentClientFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.documentSentClient"/></s:text>
<td><configByCorp:fieldVisibility componentId="trackingStatus.documentSentClient"><s:textfield cssClass="input-text" id="documentSentClient" name="trackingStatus.documentSentClient" value="%{trackingStatusDocumentSentClientFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.documentSentClient"><img id="documentSentClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td></configByCorp:fieldVisibility></td>
</c:if>
<c:if test="${empty trackingStatus.documentSentClient}">
<td><configByCorp:fieldVisibility componentId="trackingStatus.documentSentClient"><s:textfield cssClass="input-text" id="documentSentClient" name="trackingStatus.documentSentClient" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.documentSentClient"><img id="documentSentClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
</c:if>


<td colspan="9">
<table style="margin:0px;">
<tr>
<c:if test="${dateColumnShow=='Y'}">
<td width="34"></td>
</c:if>
<td align="right" class="listwhitetext" style="padding-left:61px;"><configByCorp:fieldVisibility componentId="trackingStatus.documentReceivedClient"><fmt:message key='trackingStatus.documentReceivedClient'/></configByCorp:fieldVisibility></td>
<c:if test="${not empty trackingStatus.documentReceivedClient}">
<s:text id="trackingStatusDocumentReceivedClientFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.documentReceivedClient"/></s:text>
<td><configByCorp:fieldVisibility componentId="trackingStatus.documentReceivedClient"><s:textfield cssClass="input-text" id="documentReceivedClient" name="trackingStatus.documentReceivedClient" value="%{trackingStatusDocumentReceivedClientFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.documentReceivedClient"><img id="documentReceivedClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
</c:if>
<c:if test="${empty trackingStatus.documentReceivedClient}">
<td><configByCorp:fieldVisibility componentId="trackingStatus.documentReceivedClient"><s:textfield cssClass="input-text" id="documentReceivedClient" name="trackingStatus.documentReceivedClient" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.documentReceivedClient"><img id="documentReceivedClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
</c:if>







			
</tr>
</table>
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" style="padding-left:10px;">Crew Arrival Date</td>
       
<c:if test="${not empty trackingStatus.crewArrivalDate}">
						<s:text id="trackingStatuscrewArrivalDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.crewArrivalDate"/></s:text>
						<td ><s:textfield cssClass="input-text" id="crewArrivalDate" name="trackingStatus.crewArrivalDate" value="%{trackingStatuscrewArrivalDateFormattedValue}" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="crewArrivalDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.crewArrivalDate}">
						<td ><s:textfield cssClass="input-text" id="crewArrivalDate" name="trackingStatus.crewArrivalDate" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="crewArrivalDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
<td > <s:textfield cssClass="input-text" id="crewArrivalTime" name="crewArrivalTime" required="true" cssStyle="width:40px;" size="4" maxlength="5" onchange="completecrewArrivalTimeString(this);return IsCrewValidTime(this);" value="${crewArrivalTime }" tabindex="1" /></td>						
						
						<td align="right" class="listwhitetext" style="padding-left:10px;">Start of Customs Clearance</td>
						<c:if test="${not empty trackingStatus.customClearance}">
						<s:text id="trackingStatuscustomClearanceFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.customClearance"/></s:text>
						<td ><s:textfield cssClass="input-text" id="customClearance" name="trackingStatus.customClearance" value="%{trackingStatuscustomClearanceFormattedValue}" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="customClearance_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.customClearance}">
						<td ><s:textfield cssClass="input-text" id="customClearance" name="trackingStatus.customClearance" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
						<img id="customClearance_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						
						<td align="right" class="listwhitetext" style="padding-left:10px;">End of Customs Clearance</td>
						<c:if test="${not empty trackingStatus.endCustomClearance}">
						<s:text id="trackingStatuscustomClearanceEndFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.endCustomClearance"/></s:text>
						<td ><s:textfield cssClass="input-text" id="endCustomClearance" name="trackingStatus.endCustomClearance" required="true" size="7" value="%{trackingStatuscustomClearanceEndFormattedValue}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
						<img id="endCustomClearance_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.endCustomClearance}">
						<td ><s:textfield cssClass="input-text" id="endCustomClearance" name="trackingStatus.endCustomClearance" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
						<img id="endCustomClearance_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						
</tr>
<tr>

<td align="right" class="listwhitetext">Packing/Weight report</td>
<c:if test="${not empty trackingStatus.packingWeightOn}">
<s:text id="trackingStatusPackingWeightOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.packingWeightOn"/></s:text>
<td><s:textfield cssClass="input-text" id="packingWeightOn" name="trackingStatus.packingWeightOn" value="%{trackingStatusPackingWeightOnFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><td><img id="packingWeightOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.packingWeightOn}">
<td><s:textfield cssClass="input-text" id="packingWeightOn" name="trackingStatus.packingWeightOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><td><img id="packingWeightOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td></td>
<td align="right" class="listwhitetext">Confirmation&nbsp;report</td>
<c:if test="${not empty trackingStatus.confirmationOn}">
<s:text id="trackingStatusConfirmationOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.confirmationOn"/></s:text>
<td><s:textfield cssClass="input-text" id="confirmationOn" name="trackingStatus.confirmationOn" value="%{trackingStatusConfirmationOnFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="confirmationOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.confirmationOn}">
<td><s:textfield cssClass="input-text" id="confirmationOn" name="trackingStatus.confirmationOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="confirmationOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td colspan="2" style="width:36px;"></td>
 <td align="right" class="listwhitetext">PRO#</td>
<td align="left" colspan="2"><s:textfield cssClass="input-text"  name="trackingStatus.proNumber" required="true" cssStyle="width:105px;" maxlength="40" /></td>


<td></td>
<c:if test="${empty serviceOrder.id}">
<td align="right" width="115px" colspan="3"><img style="padding-left:23px;" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty serviceOrder.id}">
<c:choose>
<c:when test="${countOriginPackoutNotes == '0' || countOriginPackoutNotes == '' || countOriginPackoutNotes == null}">
<td align="right" width="115px" colspan="3"><img style="padding-left:23px;" id="countOriginPackoutNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=OriginPackout&imageId=countOriginPackoutNotesImage&fieldId=countOriginPackoutNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=OriginPackout&imageId=countOriginPackoutNotesImage&fieldId=countOriginPackoutNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td align="right" width="115px" colspan="3"><img style="padding-left:23px;" id="countOriginPackoutNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=OriginPackout&imageId=countOriginPackoutNotesImage&fieldId=countOriginPackoutNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=OriginPackout&imageId=countOriginPackoutNotesImage&fieldId=countOriginPackoutNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<configByCorp:fieldVisibility componentId="component.field.AccountLine.payRate">	
<c:if test="${serviceOrder.job =='INT'}"> 	
<td align="right" class="listwhitetext">Location Type</td>
<td align="left" colspan="5"><configByCorp:customDropDown 	listType="map" list="${locationTypes}" fieldValue="${trackingStatus.originLocationType}"
		attribute="id=trackingStatusForm_trackingStatus_originLocationType class=list-menu name=trackingStatus.originLocationType  style=width:235px  headerKey='' headerValue=''  tabindex='' "/></td>
		</c:if>
		</configByCorp:fieldVisibility>	
</tr>


</tbody>
</table>
</td>
</tr>
<tr>
   <td height="10" width="100%" align="left" >
   <div  onClick="javascript:animatedcollapse.toggle('sitorigin')" style="margin: 0px;">
    <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;SIT @ Origin
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table></div>
		<div id="sitorigin" class="switchgroup1">		
		<table class="detailTabLabel" border="0" width="" style="margin-left:93px;">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.sitAuthorize'/></td>
<td align="left" ><s:textfield cssClass="input-text"  name="trackingStatus.sitAuthorizeOrigin" required="true" cssStyle="width:65px;" maxlength="10" tabindex="80" /></td>
<td align="right" class="listwhitetext" colspan="2"><fmt:message key='trackingStatus.sitOriginDaysAllowed'/></td>
<td align="left" colspan="3"><s:textfield cssClass="input-text"  name="trackingStatus.sitOriginDaysAllowed" required="true" cssStyle="width:65px;" maxlength="10" onchange="onlyNumeric(this);" tabindex="81" /></td>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.sitOriginYN'/></td>
<td align="left"><s:select cssClass="list-menu" name="trackingStatus.sitOriginYN" list="%{yesno}" cssStyle="width:70px" onchange="changeStatus();" tabindex="82" /></td>
<td></td>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.sitOriginLetterTA'/></td>
<td align="left" colspan="2"><s:select cssClass="list-menu" name="trackingStatus.sitOriginLetterTA" list="%{tan}" cssStyle="width:69px" onchange="changeStatus();" tabindex="83" /></td>
<td></td>
<td colspan="2">
<table cellspacing="0" cellpadding="0" class="detailTabLabel" width="92px">
<tr>
<c:if test="${not empty trackingStatus.sitOriginLetter}">
<s:text id="trackingStatusSitOriginLetterFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.sitOriginLetter"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sitOriginLetter" name="trackingStatus.sitOriginLetter" value="%{trackingStatusSitOriginLetterFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
<td><img id="trackingStatusForm_trackingStatus_sitOriginLetter-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitOriginLetter" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.sitOriginLetter}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sitOriginLetter" name="trackingStatus.sitOriginLetter" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
<td><img id="trackingStatusForm_trackingStatus_sitOriginLetter-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitOriginLetter" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
</table>
</td>
</tr> 
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.sitOriginOn'/></td>
<td align="left" colspan="3"><s:select cssClass="list-menu" name="trackingStatus.sitOriginOnCondition" list="%{tan}" cssStyle="width:69px" onchange="changeStatus();"  /></td>
<c:if test="${not empty trackingStatus.sitOriginOn}">
<s:text id="trackingStatusSitOriginOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.sitOriginOn"/></s:text>
<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sitOriginOn" name="trackingStatus.sitOriginOn" value="%{trackingStatusSitOriginOnFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onblur="calcDayOri();calculateDeliveryDate()" onchange="calculateDeliveryDate()"/></td>
<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_sitOriginOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitOriginOn" HEIGHT=20 WIDTH=20 onclick="forOriDays(); return false;"/></td>
</c:if>
<c:if test='${serviceOrder.status == "CNCL"}'>
<td><img id="trackingStatusForm_trackingStatus_sitOriginOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitOriginOn" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
<c:if test='${serviceOrder.status == "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_sitOriginOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitOriginOn" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
</c:if>
<c:if test="${empty trackingStatus.sitOriginOn}">
<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sitOriginOn" name="trackingStatus.sitOriginOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onblur="calcDayOri();calculateDeliveryDate()" onchange="calculateDeliveryDate()" /></td>
<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_sitOriginOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitOriginOn" HEIGHT=20 WIDTH=20 onclick="forOriDays(); return false;"/></td>
</c:if>
<c:if test='${serviceOrder.status == "CNCL"}'>
<td><img id="trackingStatusForm_trackingStatus_sitOriginOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitOriginOn" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
<c:if test='${serviceOrder.status == "HOLD"}'>
<td><img id="trackingStatusForm_trackingStatus_sitOriginOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitOriginOn" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
</c:if>
</c:if>
<td align="right" colspan="2" class="listwhitetext"><fmt:message key='trackingStatus.sitOriginA'/></td>
<td align="left" colspan="2"><s:select cssClass="list-menu" name="trackingStatus.sitOriginTA" list="%{tan}" cssStyle="width:70px" onchange="changeStatus();" tabindex="84" /></td>
 <td></td>
<c:if test="${not empty trackingStatus.sitOriginA}">
<s:text id="trackingStatusSitOriginAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.sitOriginA"/></s:text>
<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sitOriginA" name="trackingStatus.sitOriginA" value="%{trackingStatusSitOriginAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDayOri();"/></td>
<td><img id="trackingStatusForm_trackingStatus_sitOriginA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitOriginA" HEIGHT=20 WIDTH=20 onclick="forOriDays(); return false;"/></td>
</c:if>
<c:if test="${empty trackingStatus.sitOriginA}">
<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sitOriginA" name="trackingStatus.sitOriginA" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDayOri()"/></td><td><img id="trackingStatusForm_trackingStatus_sitOriginA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitOriginA" HEIGHT=20 WIDTH=20 onclick="forOriDays(); return false;"/></td>
</c:if>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.sitOriginDays'/></td>
<td align="left" ><s:textfield cssClass="input-text" id="sitOriDays" name="trackingStatus.sitOriginDays" required="true" cssStyle="width:65px" maxlength="5" tabindex="85" onselect="calcDayOri();calculateDeliveryDate();" onchange="calcDayOri();"/></td>
<td align="right" class="listwhitetext" width="62px">SIT&nbsp;Reason</td>
<td><s:select name="trackingStatus.originSITReason" cssClass="list-menu" list="%{sitOriginReason}" headerKey="" headerValue="" cssStyle="width:70px" tabindex="85"/></td>
<c:if test="${empty serviceOrder.id}">
<td align="right" width="100px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty serviceOrder.id}">
<c:choose>
<c:when test="${countSitOriginNotes == '0' || countSitOriginNotes == '' || countSitOriginNotes == null}">
<td align="right" width="100px"><img id="countSitOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SitOrigin&imageId=countSitOriginNotesImage&fieldId=countSitOriginNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SitOrigin&imageId=countSitOriginNotesImage&fieldId=countSitOriginNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td align="right" width="100px"><img id="countSitOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SitOrigin&imageId=countSitOriginNotesImage&fieldId=countSitOriginNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SitOrigin&imageId=countSitOriginNotesImage&fieldId=countSitOriginNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<td  class="listwhitetext" align="right" >RequestSIT</td>
<c:if test="${not empty trackingStatus.requestOriginSit}">
<s:text id="FormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.requestOriginSit"/></s:text>
<td style="width:65px;"><s:textfield id="requestOriginSit" cssClass="input-text" name="trackingStatus.requestOriginSit" value="%{FormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"/></td><td><img id="requestOriginSit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${ empty trackingStatus.requestOriginSit}">
<td style="width:65px;"><s:textfield id="requestOriginSit" cssClass="input-text" name="trackingStatus.requestOriginSit" value="%{FormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"/></td><td><img id="requestOriginSit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td  class="listwhitetext" align="right" >GrantedSIT</td>
<c:if test="${not empty trackingStatus.receivedOriginSit}">
<s:text id="FormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.receivedOriginSit"/></s:text>
<td style="width:100px;" colspan="2"><s:textfield id="receivedOriginSit" cssClass="input-text" name="trackingStatus.receivedOriginSit" value="%{FormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"/>
<img id="receivedOriginSit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${ empty trackingStatus.requestOriginSit}">
<td style="width:100px;" colspan="2"><s:textfield id="receivedOriginSit" cssClass="input-text" name="trackingStatus.receivedOriginSit" value="%{FormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"/>
<img id="receivedOriginSit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr><td height="2px"></td></tr>
</tbody>
</table></div></td>
</tr>
</tbody>
</table></td>
</tr> 
<configByCorp:fieldVisibility componentId="component.section.trackingStatus.OriginForwarding">
<c:if test="${serviceOrder.job !='DOM'}">
<tr>
   <td height="10" width="100%" align="left" >
       <c:if test="${from=='rule'}">
       <c:if test="${field=='trackingStatus.billLading' || field=='trackingStatus.preliminaryNotification'||field=='trackingStatus.instruction2FF' || field=='trackingStatus.finalNotification'}">
        <div  onClick="javascript:animatedcollapse.toggle('orgfwd')">
      <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center"><font color="red">&nbsp;Origin Forwarding</font>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
    </div>
      </c:if></c:if>
      <c:if test="${from=='rule'}">
      <c:if test="${field!='trackingStatus.billLading' && field!='trackingStatus.preliminaryNotification' && field!='trackingStatus.instruction2FF' && field!='trackingStatus.finalNotification' }">
      <div onClick="javascript:animatedcollapse.toggle('orgfwd')">
     <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Origin Forwarding
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
      </div>
      </c:if></c:if>
      <c:if test="${from!='rule'}">
      <div onClick="javascript:animatedcollapse.toggle('orgfwd')" style="margin: 0px">
      <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Origin Forwarding
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table></div>
   </c:if>
		<div id="orgfwd" class="switchgroup1">
  		
		<table class="detailTabLabel" border="0" width="">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.pickUp2PierWH'/></td>
<td align="left"><s:select cssClass="list-menu" name="trackingStatus.pickUp2PierWHTA" list="%{tan}" cssStyle="width:75px" onchange="changeStatus();" tabindex="86"/></td>
<c:if test="${not empty trackingStatus.pickUp2PierWH}">
<s:text id="trackingStatusPickUp2PierWHFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.pickUp2PierWH"/></s:text>
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_pickUp2PierWH" name="trackingStatus.pickUp2PierWH" value="%{trackingStatusPickUp2PierWHFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_pickUp2PierWH-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_pickUp2PierWH" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.pickUp2PierWH}">
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_pickUp2PierWH" name="trackingStatus.pickUp2PierWH" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_pickUp2PierWH-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_pickUp2PierWH" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td align="right" class="listwhitetext"><fmt:message key='servicePartner.blNumber'/></td>


<c:if test="${from=='rule'}">
<c:if test="${field=='trackingStatus.billLading'}">
<td align="left" colspan="3"><s:textfield cssClass="rules-textUpper"  name="trackingStatus.billLading" required="true" cssStyle="width:180px" maxlength="30" tabindex="87" /></td>
</c:if></c:if>
<c:if test="${from=='rule'}">
<c:if test="${field!='trackingStatus.billLading'}">
 <td align="left" colspan="3"><s:textfield cssClass="input-text"  name="trackingStatus.billLading" required="true" cssStyle="width:180px" maxlength="30" tabindex="87" /></td>
</c:if></c:if>
<c:if test="${from!='rule'}">
<td align="left" colspan="3"><s:textfield cssClass="input-text"  name="trackingStatus.billLading" required="true" cssStyle="width:180px" maxlength="30" tabindex="87" /></td>
</c:if>
 <configByCorp:fieldVisibility componentId="component.section.forwarding.Housebilloflading">
<td align="right" class="listwhitetext"><fmt:message key='servicePartner.hblNumber'/></td>
</configByCorp:fieldVisibility>
<c:if test="${from=='rule'}">
<c:if test="${field=='trackingStatus.hbillLading'}">
 <configByCorp:fieldVisibility componentId="component.section.forwarding.Housebilloflading">
<td align="left" colspan="3"><s:textfield cssClass="input-text"  name="trackingStatus.hbillLading" required="true" cssStyle="width:175px;" maxlength="30" tabindex="87" /></td>
</configByCorp:fieldVisibility>
</c:if></c:if>
<c:if test="${from=='rule'}">
<c:if test="${field!='trackingStatus.hbillLading'}">
 <configByCorp:fieldVisibility componentId="component.section.forwarding.Housebilloflading">
<td align="left" colspan="3"><s:textfield cssClass="input-text"  name="trackingStatus.hbillLading" required="true" cssStyle="width:175px;" maxlength="30" tabindex="87" /></td>
</configByCorp:fieldVisibility>
</c:if></c:if>
 <c:if test="${from!='rule'}">
 <configByCorp:fieldVisibility componentId="component.section.forwarding.Housebilloflading">
<td align="left" colspan="3"><s:textfield cssClass="input-text"  name="trackingStatus.hbillLading" required="true" cssStyle="width:175px;" maxlength="30" tabindex="87" /></td>
</configByCorp:fieldVisibility>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.cutOffDate'/></td>
<td align="left"><s:select cssClass="list-menu" name="trackingStatus.cutOffDateTA" list="%{tan}" cssStyle="width:75px" onchange="changeStatus();" tabindex="88" /></td>
<c:if test="${not empty trackingStatus.cutOffDate}">
<s:text id="trackingStatusCutOffDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.cutOffDate"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_cutOffDate" name="trackingStatus.cutOffDate" value="%{trackingStatusCutOffDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_cutOffDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_cutOffDate" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.cutOffDate}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_cutOffDate" name="trackingStatus.cutOffDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_cutOffDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_cutOffDate" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td align="right" class="listwhitetext">Cut&nbsp;Off&nbsp;Time</td>
<td colspan="2"><s:textfield cssClass="input-text" name="trackingStatus.cutOffTimeTo" size="3" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event,this)" onchange = "completeTimeString();" /><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" ></td>

  <c:if test="${trackingStatus.corpID == 'SSCW' ||trackingStatus.corpID =='JOHN' || trackingStatus.corpID =='CWMS'|| trackingStatus.corpID == 'UGWW' ||trackingStatus.corpID =='HOLL' || trackingStatus.corpID =='AATL' || countryIdName =='SUDD'|| trackingStatus.corpID == 'STVF' ||trackingStatus.corpID =='SDCI' || trackingStatus.corpID =='STIN' ||trackingStatus.corpID =='WEST' || trackingStatus.corpID =='JKMS' ||trackingStatus.corpID =='DAFL' || trackingStatus.corpID =='NWVL'}">
<c:if test="${serviceOrder.job == 'GST' || serviceOrder.job =='UVL' || serviceOrder.job =='MVL'||serviceOrder.job =='INT'}">
	<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.itNumber'/></td>
	<td align="left"><s:textfield cssClass="input-text"  name="trackingStatus.itNumber" required="true" size="8" maxlength="19" tabindex="88" /></td>
</c:if> 
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.returnContainer'/></td>
<td align="left" ><s:select cssClass="list-menu" name="trackingStatus.returnContainer" list="%{tan}" cssStyle="width:75px" onchange="changeStatus();" tabindex="89" /></td>
<c:if test="${not empty trackingStatus.returnContainerDateA}">
<s:text id="trackingStatusReturnContainerDateAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.returnContainerDateA"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_returnContainerDateA" name="trackingStatus.returnContainerDateA" value="%{trackingStatusReturnContainerDateAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_returnContainerDateA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_returnContainerDateA" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.returnContainerDateA}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_returnContainerDateA" name="trackingStatus.returnContainerDateA" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_returnContainerDateA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_returnContainerDateA" HEIGHT=20 WIDTH=20/></td>
</c:if>


  <c:if test="${trackingStatus.corpID == 'SSCW' ||trackingStatus.corpID =='JOHN' || trackingStatus.corpID =='CWMS'|| trackingStatus.corpID == 'UGWW' ||trackingStatus.corpID =='HOLL' || trackingStatus.corpID =='AATL' || countryIdName =='SUDD'|| trackingStatus.corpID == 'STVF' ||trackingStatus.corpID =='SDCI' || trackingStatus.corpID =='STIN' ||trackingStatus.corpID =='WEST' || trackingStatus.corpID =='JKMS' ||trackingStatus.corpID =='DAFL' || trackingStatus.corpID =='NWVL'}">
<c:if test="${serviceOrder.job == 'GST' || serviceOrder.job =='UVL' || serviceOrder.job =='MVL'||serviceOrder.job =='INT'}">
	<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.itNumber2'/></td>
<td align="left" colspan="2"><s:textfield cssClass="input-text"  name="trackingStatus.itNumber2" required="true" size="8" maxlength="19" tabindex="90"/></td>
</c:if>
</c:if>
	<td align="right" class="listwhitetext">Passport&nbsp;Received</td>
	<c:if test="${not empty trackingStatus.passportReceived}">
	<s:text id="trackingStatusPassportReceivedFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.passportReceived"/></s:text>
	<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_passportReceived" name="trackingStatus.passportReceived" value="%{trackingStatusPassportReceivedFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="checkDatePre()"/></td>
	<td><img id="trackingStatusForm_trackingStatus_passportReceived-trigger"  textelementname="trackingStatusForm_trackingStatus_passportReceived" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	<c:if test="${empty trackingStatus.passportReceived}">
	<td width=""><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_passportReceived" name="trackingStatus.passportReceived" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="checkDatePre()"/></td>
	<td><img id="trackingStatusForm_trackingStatus_passportReceived-trigger"  textelementname="trackingStatusForm_trackingStatus_passportReceived" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
</tr>
<tr>
	<td align="right" class="listwhitetext">Documentation&nbsp;Cut&nbsp;Off&nbsp;Date</td>
	<td colspan="3">
	<table style="margin:0px;padding:0px;" cellpadding="0" cellspacing="0">
	<tr>	
	<c:if test="${not empty trackingStatus.documentationCutOffDate}">
	<s:text id="trackingStatusDocumentationCutOffDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.documentationCutOffDate"/></s:text>
	<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_documentationCutOffDate" name="trackingStatus.documentationCutOffDate" value="%{trackingStatusDocumentationCutOffDateFormattedValue}" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td>&nbsp;<img id="trackingStatusForm_trackingStatus_documentationCutOffDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_documentationCutOffDate" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	<c:if test="${empty trackingStatus.documentationCutOffDate}">
	<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_documentationCutOffDate" name="trackingStatus.documentationCutOffDate" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td>&nbsp;<img id="trackingStatusForm_trackingStatus_documentationCutOffDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_documentationCutOffDate" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	<configByCorp:fieldVisibility componentId="component.tab.trackingStatus.docCutOffTimeTo">
	<td align="right" class="listwhitetext">&nbsp;Cut&nbsp;Off&nbsp;Time&nbsp;</td>
	<td ><s:textfield cssClass="input-text" name="trackingStatus.docCutOffTimeTo" size="3" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" onchange = "completeTimeString();" /><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" ></td>
	</configByCorp:fieldVisibility>
	</tr>
	</table>
	</td>	
	<td align="right" class="listwhitetext">EIN#&nbsp;Received</td>
	<c:if test="${not empty trackingStatus.einReceived}">
	<s:text id="trackingStatusEINReceivedFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.einReceived"/></s:text>
	<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_einReceived" name="trackingStatus.einReceived" value="%{trackingStatusEINReceivedFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="checkDatePre()"/></td>
	<td><img id="trackingStatusForm_trackingStatus_einReceived-trigger"  textelementname="trackingStatusForm_trackingStatus_einReceived" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	<c:if test="${empty trackingStatus.einReceived}">
	<td width=""><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_einReceived" name="trackingStatus.einReceived" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="checkDatePre()"/></td>
	<td><img id="trackingStatusForm_trackingStatus_einReceived-trigger"  textelementname="trackingStatusForm_trackingStatus_einReceived" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
</tr>
<tr>
<configByCorp:fieldVisibility componentId="component.tab.trackingStatus.norwegianCustomsTab">
<c:choose>
<c:when test="${serviceOrder.originCountryCode=='NOR' && serviceOrder.destinationCountryCode=='NOR'}">
<td colspan="3" align="right"><input type="button" value="Norwegian Customs" class="cssbutton" onclick="" disabled="disabled"/></td>
</c:when>
<c:when test="${serviceOrder.originCountryCode!='NOR' && serviceOrder.destinationCountryCode!='NOR'}">
<td colspan="3" align="right"><input type="button" value="Norwegian Customs" class="cssbutton" onclick="" disabled="disabled"/></td>
</c:when>
<c:otherwise>
<td colspan="3" align="right"><input type="button" value="Norwegian Customs" class="cssbutton" onclick="downloadXMLFile();"/></td>
</c:otherwise>
</c:choose> 
<td colspan="2" align="right" class="listwhitetext">Sent to KSD</td>
<c:if test="${not empty trackingStatus.sentToKSD}">
<s:text id="FormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.sentToKSD"/></s:text>
<td style="width:100px;"><s:textfield id="trackingStatusSentToKSD" cssClass="input-text" name="trackingStatus.sentToKSD" value="%{FormattedValue}" cssStyle="width:65px" maxlength="11"/>
</td><td><img id="trackingStatusSentToKSD-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if> 
<c:if test="${ empty trackingStatus.sentToKSD}">
<td style="width:100px;"><s:textfield id="trackingStatusSentToKSD" cssClass="input-text" name="trackingStatus.sentToKSD" value="%{FormattedValue}" cssStyle="width:65px" maxlength="11" /></td><td><img id="trackingStatusSentToKSD-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</configByCorp:fieldVisibility>
</tr>
<tr>
<td align="right" class="listwhitetext" width="148px"><fmt:message key='trackingStatus.instruction2FF'/></td>
<c:if test="${from=='rule'}">
<c:if test="${field=='trackingStatus.instruction2FF'}">
<c:if test="${not empty trackingStatus.instruction2FF}">
<s:text id="trackingStatusInstruction2FFFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.instruction2FF"/></s:text>
<td width="60"><s:textfield cssClass="rules-textUpper" id="trackingStatusForm_trackingStatus_instruction2FF" name="trackingStatus.instruction2FF" value="%{trackingStatusInstruction2FFFormattedValue}" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_instruction2FF-trigger"  textelementname="trackingStatusForm_trackingStatus_instruction2FF" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.instruction2FF}">
<td width="60"><s:textfield cssClass="rules-textUpper" id="trackingStatusForm_trackingStatus_instruction2FF" name="trackingStatus.instruction2FF" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_instruction2FF-trigger"  textelementname="trackingStatusForm_trackingStatus_instruction2FF" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if></c:if></c:if>
<c:if test="${from=='rule'}">
<c:if test="${field!='trackingStatus.instruction2FF'}">
<c:if test="${not empty trackingStatus.instruction2FF}">
<s:text id="trackingStatusInstruction2FFFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.instruction2FF"/></s:text>
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_instruction2FF" name="trackingStatus.instruction2FF" value="%{trackingStatusInstruction2FFFormattedValue}" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_instruction2FF-trigger"  textelementname="trackingStatusForm_trackingStatus_instruction2FF" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.instruction2FF}">
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_instruction2FF" name="trackingStatus.instruction2FF" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_instruction2FF-trigger"  textelementname="trackingStatusForm_trackingStatus_instruction2FF" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if></c:if></c:if>
<c:if test="${from!='rule'}">
<c:if test="${not empty trackingStatus.instruction2FF}">
<s:text id="trackingStatusInstruction2FFFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.instruction2FF"/></s:text>
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_instruction2FF" name="trackingStatus.instruction2FF" value="%{trackingStatusInstruction2FFFormattedValue}" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_instruction2FF-trigger"  textelementname="trackingStatusForm_trackingStatus_instruction2FF" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.instruction2FF}">
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_instruction2FF" name="trackingStatus.instruction2FF" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_instruction2FF-trigger"  textelementname="trackingStatusForm_trackingStatus_instruction2FF" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if></c:if>
<td></td>
<td align="right" class="listwhitetext" width="305px"><fmt:message key='trackingStatus.preliminaryNotification'/></td>
<c:if test="${not empty trackingStatus.preliminaryNotification}">
<s:text id="trackingStatusPreliminaryNotificationFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.preliminaryNotification"/></s:text>
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_preliminaryNotification" name="trackingStatus.preliminaryNotification" value="%{trackingStatusPreliminaryNotificationFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="checkDatePre()"/></td>
<td><img id="trackingStatusForm_trackingStatus_preliminaryNotification-trigger"  textelementname="trackingStatusForm_trackingStatus_preliminaryNotification" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.preliminaryNotification}">
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_preliminaryNotification" name="trackingStatus.preliminaryNotification" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="checkDatePre()"/></td>
<td><img id="trackingStatusForm_trackingStatus_preliminaryNotification-trigger"  textelementname="trackingStatusForm_trackingStatus_preliminaryNotification" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.lastFree'/></td>
<c:if test="${not empty trackingStatus.lastFree}">
<s:text id="trackingStatusLastFreeFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.lastFree"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_lastFree" name="trackingStatus.lastFree" value="%{trackingStatusLastFreeFormattedValue}" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_lastFree-trigger"  textelementname="trackingStatusForm_trackingStatus_lastFree" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.lastFree}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_lastFree" name="trackingStatus.lastFree" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_lastFree-trigger"  textelementname="trackingStatusForm_trackingStatus_lastFree" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td></td>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.finalNotification'/></td>
<c:if test="${from=='rule'}">
<c:if test="${field=='trackingStatus.finalNotification'}">
<c:if test="${not empty trackingStatus.finalNotification}">
<s:text id="trackingStatusFinalNotificationFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.finalNotification"/></s:text>
<td><s:textfield cssClass="rules-textUpper" id="finalNotification" name="trackingStatus.finalNotification" value="%{trackingStatusFinalNotificationFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="finalNotification-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.finalNotification}">
<td><s:textfield cssClass="rules-textUpper" id="finalNotification" name="trackingStatus.finalNotification" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="finalNotification-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</c:if></c:if>
<c:if test="${from=='rule'}">
<c:if test="${field!='trackingStatus.finalNotification'}">
<c:if test="${not empty trackingStatus.finalNotification}">
<s:text id="trackingStatusFinalNotificationFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.finalNotification"/></s:text>
<td><s:textfield cssClass="input-text" id="finalNotification" name="trackingStatus.finalNotification" value="%{trackingStatusFinalNotificationFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="finalNotification-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.finalNotification}">
<td><s:textfield cssClass="input-text" id="finalNotification" name="trackingStatus.finalNotification" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="finalNotification-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</c:if></c:if>
<c:if test="${from!='rule'}">
<c:if test="${not empty trackingStatus.finalNotification}">
<s:text id="trackingStatusFinalNotificationFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.finalNotification"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_finalNotification" name="trackingStatus.finalNotification" value="%{trackingStatusFinalNotificationFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_finalNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.finalNotification}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_finalNotification" name="trackingStatus.finalNotification" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_finalNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.returnLocation'/></td>
<td align="left" colspan="3"><s:textfield cssClass="input-text"  name="trackingStatus.returnLocationOrigin" required="true" cssStyle="width:170px" maxlength="20" tabindex="91" /></td>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.flagCarrier'/>
</td>
<td align="left" colspan="2"><s:select cssClass="list-menu"   list="%{flagCarrierList}" name="trackingStatus.flagCarrier"  headerKey="" headerValue="" cssStyle="width:70px" onchange="changeStatus();" tabindex="92"/></td>
<c:if test="${empty serviceOrder.id}">
<td align="right" width="100px" colspan="3"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty serviceOrder.id}">
<c:choose>
<c:when test="${countOriginForwardingNotes == '0' || countOriginForwardingNotes == '' || countOriginForwardingNotes == null}">
<td align="right" width="100px" colspan="3"><img id="countOriginForwardingNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=OriginForwarding&imageId=countOriginForwardingNotesImage&fieldId=countOriginForwardingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=OriginForwarding&subType=Origin&imageId=countOriginForwardingNotesImage&fieldId=countOriginForwardingNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td align="right" width="100px" colspan="3"><img id="countOriginForwardingNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=OriginForwarding&imageId=countOriginForwardingNotesImage&fieldId=countOriginForwardingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=OriginForwarding&subType=Origin&imageId=countOriginForwardingNotesImage&fieldId=countOriginForwardingNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><configByCorp:fieldVisibility componentId="trackingStatus.documentCHA"><fmt:message key='trackingStatus.documentCHA'/></configByCorp:fieldVisibility></td>
<c:if test="${not empty trackingStatus.documentCHA}">
<s:text id="trackingStatusDocumentCHAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.documentCHA"/></s:text>
<td><configByCorp:fieldVisibility componentId="trackingStatus.documentCHA"><s:textfield cssClass="input-text" id="documentCHA" name="trackingStatus.documentCHA" value="%{trackingStatusDocumentCHAFormattedValue}" required="true" cssStyle="width:73px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.documentCHA"><img id="documentCHA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
</c:if>
<c:if test="${empty trackingStatus.documentCHA}">
<td><configByCorp:fieldVisibility componentId="trackingStatus.documentCHA"><s:textfield cssClass="input-text" id="documentCHA" name="trackingStatus.documentCHA" required="true" cssStyle="width:73px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.documentCHA"><img id="documentCHA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
</c:if>
<td></td>
<td align="right" class="listwhitetext"><configByCorp:fieldVisibility componentId="trackingStatus.clearCustomOrigin"><fmt:message key='trackingStatus.clearCustomOrigin'/></configByCorp:fieldVisibility></td>
<c:if test="${not empty trackingStatus.clearCustomOrigin}">
<s:text id="trackingStatusClearCustomOriginFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.clearCustomOrigin"/></s:text>
<td><configByCorp:fieldVisibility componentId="trackingStatus.clearCustomOrigin"><s:textfield cssClass="input-text" id="clearCustomOrigin" name="trackingStatus.clearCustomOrigin" value="%{trackingStatusClearCustomOriginFormattedValue}" required="true" cssStyle="width:73px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.clearCustomOrigin"><img id="clearCustomOrigin-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
</c:if>
<c:if test="${empty trackingStatus.clearCustomOrigin}">
<td><configByCorp:fieldVisibility componentId="trackingStatus.clearCustomOrigin"><s:textfield cssClass="input-text" id="clearCustomOrigin" name="trackingStatus.clearCustomOrigin" required="true" cssStyle="width:73px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.clearCustomOrigin"><img id="clearCustomOrigin-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
</c:if>
</tr>
<tr><td height="2px"></td></tr>
</tbody>
</table></div>
</td>
<td background="<c:url value='/images/bg-right.jpg'/>" align="left" class="listwhite"></td></tr>
</c:if>
</configByCorp:fieldVisibility> 
<configByCorp:fieldVisibility componentId="component.section.trackingStatus.instruction2FF">
<tr> 
   <td height="10" width="100%" align="left" >
       <c:if test="${from=='rule'}">
       <c:if test="${field=='trackingStatus.billLading' || field=='trackingStatus.hbillLading' || field=='trackingStatus.instruction2FF' || field=='trackingStatus.finalNotification'}">
        <div  onClick="javascript:animatedcollapse.toggle('orgfwd')">
      <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px"> 
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center"><font color="red">&nbsp;Origin Forwarding</font>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
    </div>
      </c:if></c:if>
      <c:if test="${from=='rule'}">
      <c:if test="${field!='trackingStatus.billLading' && field!='trackingStatus.instruction2FF' && field!='trackingStatus.finalNotification' }">
      <div onClick="javascript:animatedcollapse.toggle('orgfwd')">
     <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Origin Forwarding
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
      </div>
      </c:if></c:if>

      <c:if test="${from!='rule'}">
      <div onClick="javascript:animatedcollapse.toggle('orgfwd')" style="margin: 0px">
      <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Origin Forwarding
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table></div>
   </c:if>
		<div id="orgfwd" class="switchgroup1">
  		
		<table class="detailTabLabel" border="0" width="">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.pickUp2PierWH'/></td>
<td align="left"><s:select cssClass="list-menu" name="trackingStatus.pickUp2PierWHTA" list="%{tan}" cssStyle="width:75px" onchange="changeStatus();" tabindex="86"/></td>
<c:if test="${not empty trackingStatus.pickUp2PierWH}">
<s:text id="trackingStatusPickUp2PierWHFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.pickUp2PierWH"/></s:text>
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_pickUp2PierWH" name="trackingStatus.pickUp2PierWH" value="%{trackingStatusPickUp2PierWHFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_pickUp2PierWH-trigger"  textelementname="trackingStatusForm_trackingStatus_pickUp2PierWH" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.pickUp2PierWH}">
<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_pickUp2PierWH" name="trackingStatus.pickUp2PierWH" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_pickUp2PierWH-trigger"  textelementname="trackingStatusForm_trackingStatus_pickUp2PierWH" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td align="right" class="listwhitetext"><fmt:message key='servicePartner.blNumber'/></td>
 <td align="left" colspan="3"><s:textfield cssClass="input-text"  name="trackingStatus.billLading" required="true" cssStyle="width:175px;" maxlength="30" tabindex="87" /></td>
  <configByCorp:fieldVisibility componentId="component.section.forwarding.Housebilloflading">
<td align="right" class="listwhitetext"><fmt:message key='servicePartner.hblNumber'/></td>
 <td align="left" colspan="3"><s:textfield cssClass="input-text"  name="trackingStatus.hbillLading" required="true" cssStyle="width:175px;" maxlength="30" tabindex="87" /></td>
</configByCorp:fieldVisibility>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.cutOffDate'/></td>
<td align="left"><s:select cssClass="list-menu" name="trackingStatus.cutOffDateTA" list="%{tan}" cssStyle="width:75px" onchange="changeStatus();" tabindex="88" /></td>
<c:if test="${not empty trackingStatus.cutOffDate}">
<s:text id="trackingStatusCutOffDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.cutOffDate"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_cutOffDate" name="trackingStatus.cutOffDate" value="%{trackingStatusCutOffDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_cutOffDate-trigger"  textelementname="trackingStatusForm_trackingStatus_cutOffDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.cutOffDate}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_cutOffDate" name="trackingStatus.cutOffDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_cutOffDate-trigger"  textelementname="trackingStatusForm_trackingStatus_cutOffDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td align="right" class="listwhitetext">Cut&nbsp;Off&nbsp;Time</td>
<td colspan="2"><s:textfield cssClass="input-text" name="trackingStatus.cutOffTimeTo" size="2" maxlength="5"  cssStyle="width:65px;" onkeydown="return onlyTimeFormatAllowed(event)" onchange = "completeTimeString();" /><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" ></td>
  <c:if test="${trackingStatus.corpID == 'SSCW' ||trackingStatus.corpID =='JOHN' || trackingStatus.corpID =='CWMS'|| trackingStatus.corpID == 'UGWW' ||trackingStatus.corpID =='HOLL' || trackingStatus.corpID =='AATL' || countryIdName =='SUDD'|| trackingStatus.corpID == 'STVF' ||trackingStatus.corpID =='SDCI' || trackingStatus.corpID =='STIN' ||trackingStatus.corpID =='WEST' || trackingStatus.corpID =='JKMS' ||trackingStatus.corpID =='DAFL' || trackingStatus.corpID =='NWVL'}">
<c:if test="${serviceOrder.job == 'GST' || serviceOrder.job =='UVL' || serviceOrder.job =='MVL'||serviceOrder.job =='INT'}">
	<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.itNumber'/></td>
	<td align="left"><s:textfield cssClass="input-text"  name="trackingStatus.itNumber" required="true" size="8" maxlength="19" tabindex="88" /></td>
</c:if>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.returnContainer'/></td>
<td align="left" ><s:select cssClass="list-menu" name="trackingStatus.returnContainer" list="%{tan}" cssStyle="width:75px" onchange="changeStatus();" tabindex="89" /></td>
<c:if test="${not empty trackingStatus.returnContainerDateA}">
<s:text id="trackingStatusReturnContainerDateAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.returnContainerDateA"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_returnContainerDateA" name="trackingStatus.returnContainerDateA" value="%{trackingStatusReturnContainerDateAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_returnContainerDateA-trigger"  textelementname="trackingStatusForm_trackingStatus_returnContainerDateA" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.returnContainerDateA}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_returnContainerDateA" name="trackingStatus.returnContainerDateA" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_returnContainerDateA-trigger"  textelementname="trackingStatusForm_trackingStatus_returnContainerDateA" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if> 
  <c:if test="${trackingStatus.corpID == 'SSCW' ||trackingStatus.corpID =='JOHN' || trackingStatus.corpID =='CWMS'|| trackingStatus.corpID == 'UGWW' ||trackingStatus.corpID =='HOLL' || trackingStatus.corpID =='AATL' || countryIdName =='SUDD'|| trackingStatus.corpID == 'STVF' ||trackingStatus.corpID =='SDCI' || trackingStatus.corpID =='STIN' ||trackingStatus.corpID =='WEST' || trackingStatus.corpID =='JKMS' ||trackingStatus.corpID =='DAFL' || trackingStatus.corpID =='NWVL'}">
<c:if test="${serviceOrder.job == 'GST' || serviceOrder.job =='UVL' || serviceOrder.job =='MVL'||serviceOrder.job =='INT'}">
	<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.itNumber2'/></td>
	<td align="left" colspan="2"><s:textfield cssClass="input-text"  name="trackingStatus.itNumber2" required="true" size="8" maxlength="19" tabindex="90"/></td>
</c:if> 
</c:if>
<c:if test="${serviceOrder.job != 'GST' || serviceOrder.job !='UVL' || serviceOrder.job !='MVL'}">
	<td align="right" class="listwhitetext"></td><td align="right" class="listwhitetext"></td>
</c:if> 
</tr>
<tr>
<td align="right" class="listwhitetext" width="148px"><fmt:message key='trackingStatus.instruction2FF'/></td>
<c:if test="${not empty trackingStatus.instruction2FF}">
<s:text id="trackingStatusInstruction2FFFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.instruction2FF"/></s:text>
<td width="6%"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_instruction2FF" name="trackingStatus.instruction2FF" value="%{trackingStatusInstruction2FFFormattedValue}" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_instruction2FF-trigger"  textelementname="trackingStatusForm_trackingStatus_instruction2FF" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.instruction2FF}">
<td width="6%"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_instruction2FF" name="trackingStatus.instruction2FF" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_instruction2FF-trigger"  textelementname="trackingStatusForm_trackingStatus_instruction2FF" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if> 
<td></td>
<td align="right" class="listwhitetext" width="240px"><fmt:message key='trackingStatus.preliminaryNotification'/></td>
<c:if test="${not empty trackingStatus.preliminaryNotification}">
<s:text id="trackingStatusPreliminaryNotificationFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.preliminaryNotification"/></s:text>
<td width="6%"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_preliminaryNotification" name="trackingStatus.preliminaryNotification" value="%{trackingStatusPreliminaryNotificationFormattedValue}" required="true" cssStyle="width:68px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="checkDatePre()"/></td>
<td><img id="trackingStatusForm_trackingStatus_preliminaryNotification-trigger"  textelementname="trackingStatusForm_trackingStatus_preliminaryNotification" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.preliminaryNotification}">
<td width="6%"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_preliminaryNotification" name="trackingStatus.preliminaryNotification" required="true" cssStyle="width:68px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="checkDatePre()"/></td>
<td><img id="trackingStatusForm_trackingStatus_preliminaryNotification-trigger"  textelementname="trackingStatusForm_trackingStatus_preliminaryNotification" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.lastFree'/></td>
<c:if test="${not empty trackingStatus.lastFree}">
<s:text id="trackingStatusLastFreeFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.lastFree"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_lastFree" name="trackingStatus.lastFree" value="%{trackingStatusLastFreeFormattedValue}" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_lastFree-trigger"  textelementname="trackingStatusForm_trackingStatus_lastFree" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.lastFree}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_lastFree" name="trackingStatus.lastFree" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_lastFree-trigger"  textelementname="trackingStatusForm_trackingStatus_lastFree" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td></td>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.finalNotification'/></td>
<c:if test="${not empty trackingStatus.finalNotification}">
<s:text id="trackingStatusFinalNotificationFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.finalNotification"/></s:text>
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_finalNotification" name="trackingStatus.finalNotification" value="%{trackingStatusFinalNotificationFormattedValue}" required="true" cssStyle="width:68px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_finalNotification-trigger"  textelementname="trackingStatusForm_trackingStatus_finalNotification" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.finalNotification}">
<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_finalNotification" name="trackingStatus.finalNotification" required="true" cssStyle="width:68px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_finalNotification-trigger"  textelementname="trackingStatusForm_trackingStatus_finalNotification" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.returnLocation'/></td>
<td align="left" colspan="3"><s:textfield cssClass="input-text"  name="trackingStatus.returnLocationOrigin" required="true" size="23" maxlength="20" tabindex="91" /></td>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.flagCarrier'/></td>
<td align="left" colspan="2"><s:select cssClass="list-menu"  list="%{flagCarrierList}" name="trackingStatus.flagCarrier" headerKey="" headerValue="" cssStyle="width:70px" /></td>
<c:if test="${empty serviceOrder.id}">
<td align="right" width="230px" colspan="3"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty serviceOrder.id}">
<c:choose>
<c:when test="${countOriginForwardingNotes == '0' || countOriginForwardingNotes == '' || countOriginForwardingNotes == null}">
<td align="right" width="230px" colspan="3"><img id="countOriginForwardingNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=OriginForwarding&imageId=countOriginForwardingNotesImage&fieldId=countOriginForwardingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=OriginForwarding&subType=Origin&imageId=countOriginForwardingNotesImage&fieldId=countOriginForwardingNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td align="right" width="230px" colspan="3"><img id="countOriginForwardingNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=OriginForwarding&imageId=countOriginForwardingNotesImage&fieldId=countOriginForwardingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=OriginForwarding&subType=Origin&imageId=countOriginForwardingNotesImage&fieldId=countOriginForwardingNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><configByCorp:fieldVisibility componentId="trackingStatus.documentCHA"><fmt:message key='trackingStatus.documentCHA'/></configByCorp:fieldVisibility></td>
<c:if test="${not empty trackingStatus.documentCHA}">
<s:text id="trackingStatusDocumentCHAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.documentCHA"/></s:text>
<td><configByCorp:fieldVisibility componentId="trackingStatus.documentCHA"><s:textfield cssClass="input-text" id="documentCHA" name="trackingStatus.documentCHA" value="%{trackingStatusDocumentCHAFormattedValue}" required="true" cssStyle="width:73px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.documentCHA"><img id="documentCHA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td></configByCorp:fieldVisibility></td>
</c:if>
<c:if test="${empty trackingStatus.documentCHA}">
<td><configByCorp:fieldVisibility componentId="trackingStatus.documentCHA"><s:textfield cssClass="input-text" id="documentCHA" name="trackingStatus.documentCHA" required="true" cssStyle="width:73px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.documentCHA"><img id="documentCHA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td></configByCorp:fieldVisibility></td>
</c:if>
<td></td>
<td align="right" class="listwhitetext"><configByCorp:fieldVisibility componentId="trackingStatus.clearCustomOrigin"><fmt:message key='trackingStatus.clearCustomOrigin'/></configByCorp:fieldVisibility></td>
<c:if test="${not empty trackingStatus.clearCustomOrigin}">
<s:text id="trackingStatusClearCustomOriginFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.clearCustomOrigin"/></s:text>
<td><configByCorp:fieldVisibility componentId="trackingStatus.clearCustomOrigin"><s:textfield cssClass="input-text" id="clearCustomOrigin" name="trackingStatus.clearCustomOrigin" value="%{trackingStatusClearCustomOriginFormattedValue}" required="true" cssStyle="width:73px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.clearCustomOrigin"><img id="clearCustomOrigin-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td></configByCorp:fieldVisibility></td>
</c:if>
<c:if test="${empty trackingStatus.clearCustomOrigin}">
<td><configByCorp:fieldVisibility componentId="trackingStatus.clearCustomOrigin"><s:textfield cssClass="input-text" id="clearCustomOrigin" name="trackingStatus.clearCustomOrigin" required="true" cssStyle="width:73px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.clearCustomOrigin"><img id="clearCustomOrigin-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td></configByCorp:fieldVisibility></td>
</c:if>
</tr>
<tr><td height="2px"></td></tr>
</tbody>
</table></div>
</td>
<td background="<c:url value='/images/bg-right.jpg'/>" align="left" class="listwhite"></td></tr>
</configByCorp:fieldVisibility> 
<c:if test="${fn1:indexOf(usGovJobs,serviceOrder.job)>=0}">  
<tr>
<td height="10" width="100%" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('gstfwd')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<c:if test="${from=='rule'}">
       				<c:if test="${field=='trackingStatus.gbl'|| field=='trackingStatus.mailClaim'|| field=='trackingStatus.rating'||field=='trackingStatus.paymentRecived'}">
					<td NOWRAP class="headtab_center"><font color="red">&nbsp;&nbsp;US Government</font>
					</c:if></c:if>
					<c:if test="${from=='rule'}">
       				<c:if test="${field!='trackingStatus.gbl'&& field!='trackingStatus.mailClaim' && field !='trackingStatus.rating' && field!='trackingStatus.paymentRecived'}">
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;US Government
					</c:if></c:if>
					<c:if test="${from!='rule'}">
       				<td NOWRAP class="headtab_center">&nbsp;&nbsp;US Government
					</c:if>
					</td>
				<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
					  </div>
					<div id="gstfwd" class="switchgroup1">
		<table class="detailTabLabel" border="0">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
<tr>
<td width="6px"></td>
<td align="right" class="listwhitetext" width="140px"><fmt:message key='trackingStatus.requestGBL'/></td>
<c:if test="${not empty trackingStatus.requestGBL}">
<s:text id="trackingStatusRequestGBLFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.requestGBL"/></s:text>
<td><s:textfield cssClass="input-text" id="requestGBL" name="trackingStatus.requestGBL" value="%{trackingStatusRequestGBLFormattedValue}" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="requestGBL-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.requestGBL}">
<td><s:textfield cssClass="input-text" id="requestGBL" name="trackingStatus.requestGBL" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="requestGBL-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td align="right" class="listwhitetext" width="110px"><fmt:message key='trackingStatus.recivedGBL'/></td>
<c:if test="${not empty trackingStatus.recivedGBL}">
<s:text id="trackingStatusRecivedGBLFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.recivedGBL"/></s:text>
<td><s:textfield cssClass="input-text" id="recivedGBL" name="trackingStatus.recivedGBL" value="%{trackingStatusRecivedGBLFormattedValue}" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="recivedGBL-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.recivedGBL}">
<td><s:textfield cssClass="input-text" id="recivedGBL" name="trackingStatus.recivedGBL" required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="recivedGBL-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td align="right" class="listwhitetext" width="166px"><fmt:message key='trackingStatus.gbl'/></td>
<td align="left">
<s:textfield cssClass="input-text"  name="trackingStatus.gbl" cssStyle="width:180px;" required="true"  maxlength="20" tabindex="93" />
</td>
<td align="right" class="listwhitetext" width="166px">Supplemental GBL#</td>
<td align="left">
<s:textfield cssClass="input-text"  name="supplementGBL" value="${serviceOrder.supplementGBL}"  cssStyle="width:180px;" required="true"  maxlength="20" tabindex="93" />


</td></tr>
<tr>
<td align="right" colspan="8" class="listwhitetext"><fmt:message key='trackingStatus.daShipmentNum'/></td>
<td align="left"><s:textfield cssClass="input-text"  name="trackingStatus.daShipmentNum" required="true"  cssStyle="width:180px;" maxlength="20" tabindex="93"/>
</td>
<c:if test="${empty serviceOrder.id}">
<td align="right" width="90px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty serviceOrder.id}">
<c:choose>
<c:when test="${countGSTForwardingNotes == '0' || countGSTForwardingNotes =='' || countGSTForwardingNotes == null}">
<td align="right" width="90px"><img id="countGSTForwardingNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=USGovernment&imageId=countGSTForwardingNotesImage&fieldId=countGSTForwardingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=USGovernment&imageId=countGSTForwardingNotesImage&fieldId=countGSTForwardingNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td align="right" width="90px"><img id="countGSTForwardingNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=USGovernment&imageId=countGSTForwardingNotesImage&fieldId=countGSTForwardingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=USGovernment&imageId=countGSTForwardingNotesImage&fieldId=countGSTForwardingNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
</tbody>
</table>
<div class="vertlinedata" style="width:100%;"></div>
<table class="detailTabLabel" border="0" >
<c:set var="gsaiffFundingFlag" value="false" />
<c:if test="${trackingStatus.gsaiffFundingFlag}">
<c:set var="gsaiffFundingFlag" value="true" />
</c:if>
<tbody>
<tr><td align="left" height="5px"></td></tr>
<tr>
<td></td>
<td></td>
<td align="center" class="listwhitetext"><b><fmt:message key='labels.ITGBLPost'/></b></td>
<td></td>
<td></td>
<td width="2"></td>
<td align="right" colspan="2" class="listwhitetext"><b>Ext.System Notification</b></td>
</tr>
<tr>
<td width="32px"></td>
<td align="right" class="listwhitetext" width="115"><fmt:message key='trackingStatus.mailEvaluation'/></td>
<c:if test="${not empty trackingStatus.mailEvaluation}">
<s:text id="trackingStatusMailEvaluationFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.mailEvaluation"/></s:text>
<td width="65"><s:textfield cssClass="input-text" id="mailEvaluation" name="trackingStatus.mailEvaluation" value="%{trackingStatusMailEvaluationFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="mailEvaluation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.mailEvaluation}">
<td width="65"><s:textfield cssClass="input-text" id="mailEvaluation" name="trackingStatus.mailEvaluation" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="mailEvaluation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td width="15"></td>
<td align="right" class="listwhitetext" colspan="2"><fmt:message key='trackingStatus.rating'/></td>
<c:if test="${not empty trackingStatus.rating}">
<s:text id="trackingStatusRatingFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.rating"/></s:text>
<td width="65"><s:textfield cssClass="input-text" id="rating" name="trackingStatus.rating" value="%{trackingStatusRatingFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="rating-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.rating}">
<td width="65"><s:textfield cssClass="input-text" id="rating" name="trackingStatus.rating" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="rating-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td width="65" align="right" class="listwhitetext">TMSS%</td>
<td >
<s:textfield id="tmss" cssClass="input-text" name="trackingStatus.tmss" required="true" cssStyle="width:85px" maxlength="30"  onchange="changeStatus();" tabindex=""/>
</td>
</tr>
<tr>
<td ></td>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.recivedEvaluation'/></td>
<c:if test="${not empty trackingStatus.recivedEvaluation}">
<s:text id="trackingStatusRecivedEvaluationFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.recivedEvaluation"/></s:text>
<td><s:textfield cssClass="input-text" id="recivedEvaluation" name="trackingStatus.recivedEvaluation" value="%{trackingStatusRecivedEvaluationFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="recivedEvaluation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.recivedEvaluation}">
<td><s:textfield cssClass="input-text" id="recivedEvaluation" name="trackingStatus.recivedEvaluation" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="recivedEvaluation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td></td>
<td width="35"></td>
<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.paymentRecived'/></td>
<c:if test="${not empty trackingStatus.paymentRecived}">
<s:text id="trackingPaymentRecivedFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.paymentRecived"/></s:text>
<td><s:textfield cssClass="input-text" id="paymentRecived" name="trackingStatus.paymentRecived" value="%{trackingPaymentRecivedFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="paymentRecived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.paymentRecived}">
<td><s:textfield cssClass="input-text" id="paymentRecived" name="trackingStatus.paymentRecived" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="paymentRecived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>
<td></td>
<configByCorp:fieldVisibility componentId="component.section.trackingStatus.Reason">
<td align="right" class="listwhitetext">Missed&nbsp;RDD&nbsp;Notification </td>
<c:if test="${not empty trackingStatus.missedRDDNotification}">
<s:text id="trackingStatusMissedRDDNotificationFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.missedRDDNotification"/></s:text>
<td><s:textfield cssClass="input-text" id="missedRDDNotification" name="trackingStatus.missedRDDNotification" value="%{trackingStatusMissedRDDNotificationFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="missedRDDNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.missedRDDNotification}">
<td><s:textfield cssClass="input-text" id="missedRDDNotification" name="trackingStatus.missedRDDNotification" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="missedRDDNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if> 
<td align="right" class="listwhitetext" width="118">Reason</td>
<td align="left" colspan=""><s:select cssClass="list-menu"    name="trackingStatus.reason" list="%{reasonList}" headerKey="" headerValue="" cssStyle="width:106px" tabindex="98"/></td>
</configByCorp:fieldVisibility>
<td align="right" colspan="" class="listwhitetext">Made&nbsp;Available&nbsp;for&nbsp;Delivery</td>
<c:if test="${not empty trackingStatus.SITNotify}">
<s:text id="trackingStatusSITNotifyFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.SITNotify"/></s:text>
<td><s:textfield cssClass="input-text" id="SITNotify" name="trackingStatus.SITNotify" value="%{trackingStatusSITNotifyFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="SITNotify-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.SITNotify}">
<td><s:textfield cssClass="input-text" id="SITNotify" name="trackingStatus.SITNotify" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="SITNotify-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>
	 <td></td>
	 <td align="right" class="listwhitetext"><fmt:message key='trackingStatus.gsaiffFundingFlag'/></td>
<td><s:checkbox name="trackingStatus.gsaiffFundingFlag" value="${gsaiffFundingFlag}"  fieldValue="true" onclick="changeStatus();" cssStyle="margin:0px;" tabindex="93" />

    <td colspan="3"></td>
	<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.mailClaim'/></td>
<c:if test="${not empty trackingStatus.mailClaim}">
<s:text id="trackingStatusMailClaimFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.mailClaim"/></s:text>
<td><s:textfield cssClass="input-text" id="mailClaim" name="trackingStatus.mailClaim" value="%{trackingStatusMailClaimFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="mailClaim-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.mailClaim}">
<td><s:textfield cssClass="input-text" id="mailClaim" name="trackingStatus.mailClaim" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="mailClaim-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if> 
</tr>
<tr> 
	 <td></td>
	<td align="right" class="listwhitetext">Required&nbsp;Delivery&nbsp;Days</td>
	 <td ><s:textfield id="requiredDeliveryDays" cssClass="input-text" name="trackingStatus.requiredDeliveryDays" required="true" cssStyle="width:65px" maxlength="3"  onkeydown="return onlyNumsAllowedWithoutNegative(event, this, '');return noCTRL(event);" onmousedown="return noCTRL(event);" onblur="calculateDeliveryDate();trapCalendar('B');" onchange="trapCalendar('B');" tabindex="93"/></td>
	<td colspan="3"></td>
	<td align="right" style="width:160px;" class="listwhitetext"><fmt:message key='trackingStatus.requiredDeliveryDate'/></td>
	<c:if test="${not empty trackingStatus.requiredDeliveryDate}">
		<s:text id="trackingStatusRequiredDeliveryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.requiredDeliveryDate"/></s:text>
		<td><s:textfield cssClass="input-text" id="requiredDeliveryDate" name="trackingStatus.requiredDeliveryDate" value="%{trackingStatusRequiredDeliveryDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="requiredDeliveryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="trapCalendar('C');"/></td>
	</c:if>
	<c:if test="${empty trackingStatus.requiredDeliveryDate}">
		<td><s:textfield cssClass="input-text" id="requiredDeliveryDate" name="trackingStatus.requiredDeliveryDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="requiredDeliveryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="trapCalendar('C');"/></td>
	</c:if>
</tr>
<tr> 
	 <td></td>
	<td align="right" class="listwhitetext">FFW &nbsp;Requested</td>
<c:if test="${not empty trackingStatus.ffwRequested}">
		<s:text id="ffwRequestedDeliveryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.ffwRequested"/></s:text>
		<td><s:textfield cssClass="input-text" id="ffwRequested" name="trackingStatus.ffwRequested" value="%{ffwRequestedDeliveryDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="ffwRequested-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/></td>
	</c:if>
	<c:if test="${empty trackingStatus.ffwRequested}">
		<td><s:textfield cssClass="input-text" id="ffwRequested" name="trackingStatus.ffwRequested" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="ffwRequested-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/></td>
	</c:if>	<td colspan="2"></td>
	<td align="right" style="width:160px;" class="listwhitetext">FFW &nbsp;Received</td>
	<c:if test="${not empty trackingStatus.ffwReceived}">
		<s:text id="ffwReceivedDeliveryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.ffwReceived"/></s:text>
		<td><s:textfield cssClass="input-text" id="ffwReceived" name="trackingStatus.ffwReceived" value="%{ffwReceivedDeliveryDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="ffwReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/></td>
	</c:if>
	<c:if test="${empty trackingStatus.ffwReceived}">
		<td><s:textfield cssClass="input-text" id="ffwReceived" name="trackingStatus.ffwReceived" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="ffwReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/></td>
	</c:if>
</tr>
</tbody>
</table> 
<c:if test="${miscellaneous.millitaryShipment=='Y'}">  
  <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="width:100%;"><tr>
	<td height="10" width="100%" align="left" >
	<div  onClick="javascript:animatedcollapse.toggle('military')" style="margin: 0px">
 		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
		<c:if test="${from=='rule'}">
         <c:if test="${field=='miscellaneous.printGbl'}"> 
		<tr>
		<td class="headtab_left">
		</td>
		<td NOWRAP class="headtab_center"><font color="red">&nbsp;Military
		</td>
		<td width="28" valign="top" class="headtab_bg"></td>
		<td class="headtab_bg_center">&nbsp;
		</td>
		<td class="headtab_right">
		</td>
		</tr>
		</c:if>
		</c:if>
		<c:if test="${from=='rule'}">
         <c:if test="${field!='miscellaneous.printGbl'}">
         <tr>
		<td class="headtab_left">
		</td>
		<td NOWRAP class="headtab_center">&nbsp;Military
		</td>
		<td width="28" valign="top" class="headtab_bg"></td>
		<td class="headtab_bg_center">&nbsp;
		</td>
		<td class="headtab_right">
		</td>
		</tr>
		</c:if>
		</c:if>
		<c:if test="${from!='rule'}">
		<tr>
		<td class="headtab_left">
		</td>
		<td NOWRAP class="headtab_center">&nbsp;Military
		</td>
		<td width="28" valign="top" class="headtab_bg"></td>
		<td class="headtab_bg_center">&nbsp;
		</td>
		<td class="headtab_right">
		</td>
		</tr>
		</c:if>
	</table>
		</div>
	<div  id="military" class="switchgroup1">
	<table class="detailTabLabel" cellspacing="3" cellpadding="0" border="0" style="width: 100%">
	<tbody>	
	<tr>
		<td align="right" class="listwhitetext" width="150"><fmt:message key='miscellaneous.millitaryShipment'/></td>
		<td width="70px"><s:select cssClass="list-menu" name="miscellaneous.millitaryShipment" list="%{yesno}" cssStyle="width:67px;" onchange="changeStatus();" /></td>
		<td/>
		<td align="right" class="listwhitetext" width="150px"><fmt:message key='miscellaneous.dp3'/></td>
		<c:set var="isVipFlag" value="false"/>
		<c:if test="${miscellaneous.dp3}">
			<c:set var="isVipFlag" value="true"/>
		</c:if>
		<td width="20"><s:checkbox key="miscellaneous.dp3" value="${isVipFlag}" fieldValue="true" onclick="changeStatus()"/></td>
		<td width="25px"/>		
		<td align="right" class="listwhitetext" width=""><fmt:message key='miscellaneous.shortFuse'/></td>
		<c:set var="isShortFuseFlag" value="false"/>
		<c:if test="${miscellaneous.shortFuse}">
			<c:set var="isShortFuseFlag" value="true"/>
		</c:if>
		<td width="20px"><s:checkbox key="miscellaneous.shortFuse" value="${isShortFuseFlag}" fieldValue="true" onclick="changeStatus()"/></td>					
		<td width="250px"/>	
	</tr>	
					<tr>
						<td align="right" class="listwhitetext">From Non-Temp Storage</td>
						<td><s:select cssClass="list-menu" name="miscellaneous.fromNonTempStorage" list="%{yesno}" cssStyle="width:67px" onchange="changeStatus();" tabindex="1" /></td>
							<td></td>
						<td align="right" class="listwhitetext" width="100px">Weapons Included</td>
						<td><s:select cssClass="list-menu" name="miscellaneous.weaponsIncluded" list="%{yesno}" cssStyle="width:65px" onchange="changeStatus();" tabindex="1" /></td>
					</tr>
					<tr>
<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.originDocsToBase'/></td>
<c:if test="${not empty miscellaneous.originDocsToBase}">
<s:text id="miscellaneousoriginDocsToBaseFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.originDocsToBase"/></s:text>
<td><s:textfield cssClass="input-text" id="originDocsToBase" name="miscellaneous.originDocsToBase" value="%{miscellaneousoriginDocsToBaseFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="originDocsToBase-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.originDocsToBase}">
<td><s:textfield cssClass="input-text" id="originDocsToBase" name="miscellaneous.originDocsToBase" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="originDocsToBase-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>		
					
<td align="right" class="listwhitetext" width="100px"><fmt:message key='miscellaneous.dd1840'/></td>
<c:if test="${not empty miscellaneous.dd1840}">
<s:text id="miscellaneousDd1840FormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.dd1840"/></s:text>
<td width="70px"><s:textfield cssClass="input-text" id="dd1840" name="miscellaneous.dd1840" value="%{miscellaneousDd1840FormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="dd1840-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.dd1840}">
<td width="70px"><s:textfield cssClass="input-text" id="dd1840" name="miscellaneous.dd1840" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="dd1840-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>		<td align="right" class="listwhitetext">Origin Base</td>
		<td colspan="2"><s:textfield cssClass="input-textUpper" name="serviceOrder.originMilitary" size="7" readonly="true"/></td>
			
		<td align="right" class="listwhitetext" width="100px">1780&nbsp;Score&nbsp;%</td>
		<td><s:textfield cssClass="input-text" name="miscellaneous.originScore" size="7" maxlength="7" onkeydown="onlyFloat(this);" onchange="CheckTotalPercentage();changeStatus();"  /></td>
</tr>
<tr><td  height="2px"></td></tr>
<tr>
<td class="subcontenttabChild" colspan="12"><font color="black"><b>&nbsp;DPS Entry</b></font></td>
</tr>
<tr><td height="2px"></td></tr>
<tr>					
<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.millitarySurveyDate'/></td>
<c:if test="${not empty miscellaneous.millitarySurveyDate}">
<s:text id="miscellaneousmillitarySurveyDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.millitarySurveyDate"/></s:text>
<td><s:textfield cssClass="input-text" id="millitarySurveyDate" name="miscellaneous.millitarySurveyDate" value="%{miscellaneousmillitarySurveyDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="millitarySurveyDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.millitarySurveyDate}">
<td><s:textfield cssClass="input-text" id="millitarySurveyDate" name="miscellaneous.millitarySurveyDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="millitarySurveyDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>					
<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.millitarySurveyResults'/></td>
<c:if test="${not empty miscellaneous.millitarySurveyResults}">
<s:text id="miscellaneousmillitarySurveyResultsFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.millitarySurveyResults"/></s:text>
<td><s:textfield cssClass="input-text" id="millitarySurveyResults" name="miscellaneous.millitarySurveyResults" value="%{miscellaneousmillitarySurveyResultsFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="millitarySurveyResults-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.millitarySurveyResults}">
<td><s:textfield cssClass="input-text" id="millitarySurveyResults" name="miscellaneous.millitarySurveyResults" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="millitarySurveyResults-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr> 
<tr>
<td align="right" class="listwhitetext"></td>
<c:if test="${not empty miscellaneous.preApprovalDate1}">
<s:text id="miscellaneouspreApprovalDate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.preApprovalDate1"/></s:text>
<td><s:textfield cssClass="input-text" id="preApprovalDate1" name="miscellaneous.preApprovalDate1" value="%{miscellaneouspreApprovalDate1FormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="preApprovalDate1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.preApprovalDate1}">
<td><s:textfield cssClass="input-text" id="preApprovalDate1" name="miscellaneous.preApprovalDate1" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="preApprovalDate1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.preApprovalRequest1'/></td>
	<td colspan="6"><s:textfield cssClass="input-text" name="miscellaneous.preApprovalRequest1" size="67" maxlength="100"/></td>
</tr>
<tr>
<td align="right" class="listwhitetext"></td>
<c:if test="${not empty miscellaneous.preApprovalDate2}">
<s:text id="miscellaneouspreApprovalDate2FormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.preApprovalDate2"/></s:text>
<td><s:textfield cssClass="input-text" id="preApprovalDate2" name="miscellaneous.preApprovalDate2" value="%{miscellaneouspreApprovalDate2FormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="preApprovalDate2-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.preApprovalDate2}">
<td><s:textfield cssClass="input-text" id="preApprovalDate2" name="miscellaneous.preApprovalDate2" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="preApprovalDate2-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.preApprovalRequest1'/></td>
	<td colspan="6"><s:textfield cssClass="input-text" name="miscellaneous.preApprovalRequest2" size="67" maxlength="100"/></td>	
</tr>
<tr>
<td align="right" class="listwhitetext"></td>
<c:if test="${not empty miscellaneous.preApprovalDate3}">
<s:text id="miscellaneouspreApprovalDate3FormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.preApprovalDate3"/></s:text>
<td><s:textfield cssClass="input-text" id="preApprovalDate3" name="miscellaneous.preApprovalDate3" value="%{miscellaneouspreApprovalDate3FormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="preApprovalDate3-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.preApprovalDate3}">
<td><s:textfield cssClass="input-text" id="preApprovalDate3" name="miscellaneous.preApprovalDate3" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="preApprovalDate3-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.preApprovalRequest1'/></td>
	<td colspan="6"><s:textfield cssClass="input-text" name="miscellaneous.preApprovalRequest3" size="67" maxlength="100"/></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.printGbl'/></td>
<c:if test="${not empty miscellaneous.printGbl}">
<s:text id="miscellaneousprintGblFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.printGbl"/></s:text>
<td><s:textfield cssClass="input-text" id="printGbl" name="miscellaneous.printGbl" value="%{miscellaneousprintGblFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="printGbl-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.printGbl}">
<td><s:textfield cssClass="input-text" id="printGbl" name="miscellaneous.printGbl" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="printGbl-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.actualPickUp'/></td>
<c:if test="${not empty miscellaneous.actualPickUp}">
<s:text id="miscellaneousactualPickUpFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.actualPickUp"/></s:text>
<td><s:textfield cssClass="input-text" id="actualPickUp" name="miscellaneous.actualPickUp" value="%{miscellaneousactualPickUpFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="actualPickUp-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.actualPickUp}">
<td><s:textfield cssClass="input-text" id="actualPickUp" name="miscellaneous.actualPickUp" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="actualPickUp-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.millitaryActualWeightDate'/></td>
<c:if test="${not empty miscellaneous.millitaryActualWeightDate}">
<s:text id="miscellaneousmillitaryActualWeightDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.millitaryActualWeightDate"/></s:text>
<td><s:textfield cssClass="input-text" id="millitaryActualWeightDate" name="miscellaneous.millitaryActualWeightDate" value="%{miscellaneousmillitaryActualWeightDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="millitaryActualWeightDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.millitaryActualWeightDate}">
<td><s:textfield cssClass="input-text" id="millitaryActualWeightDate" name="miscellaneous.millitaryActualWeightDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="millitaryActualWeightDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.millitarySitDestinationDate'/></td>
<c:if test="${not empty miscellaneous.millitarySitDestinationDate}">
<s:text id="miscellaneousmillitarySitDestinationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.millitarySitDestinationDate"/></s:text>
<td><s:textfield cssClass="input-text" id="millitarySitDestinationDate" name="miscellaneous.millitarySitDestinationDate" value="%{miscellaneousmillitarySitDestinationDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="millitarySitDestinationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.millitarySitDestinationDate}">
<td><s:textfield cssClass="input-text" id="millitarySitDestinationDate" name="miscellaneous.millitarySitDestinationDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="millitarySitDestinationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.millitaryDeliveryDate'/></td>
<c:if test="${not empty miscellaneous.millitaryDeliveryDate}">
<s:text id="miscellaneousmillitaryDeliveryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.millitaryDeliveryDate"/></s:text>
<td><s:textfield cssClass="input-text" id="millitaryDeliveryDate" name="miscellaneous.millitaryDeliveryDate" value="%{miscellaneousmillitaryDeliveryDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="millitaryDeliveryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.millitaryDeliveryDate}">
<td><s:textfield cssClass="input-text" id="millitaryDeliveryDate" name="miscellaneous.millitaryDeliveryDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="millitaryDeliveryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
</tbody></table>
	</div>	
	</td>
	</tr>
	</tbody>
	</table>
</c:if>
</div></td></tr>
</c:if>
<tr>
	<td height="10" width="100%" align="left" style="margin:0px">
	<div onClick="javascript:animatedcollapse.toggle('transport')" style="margin:0px">
     <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Transport
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
     </div>
		<div id="transport" class="switchgroup1">
		
		<table class="detailTabLabel" border="0" width="100%">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
<tr><td align="left" colspan="10">
<div id="scrollable" style="height:120px;overflow-y:auto;">
<s:set name="servicePartnerss" value="servicePartnerss" scope="request"/>
<display:table name="servicePartnerss" class="table" requestURI="" id="servicePartnerss"  defaultsort="1" pagesize="10" style="width:100%;">
   <sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
   	 <display:column titleKey="servicePartner.carrierNumber" style="width:15px">
   	 	<c:if test="${forwardingTabVal!='Y'}">
				<a href="editServicePartner.html?id=${servicePartnerss.id}" >
				<c:out value="${servicePartnerss.carrierNumber}" />
				</a>
		</c:if>
		<c:if test="${forwardingTabVal=='Y'}">
				<a href="containersAjaxList.html?id=${serviceOrder.id}" >
				<c:out value="${servicePartnerss.carrierNumber}" />
				</a>
		</c:if>
	</display:column>
    </sec-auth:authComponent>
     <display:column property="carrierName"  titleKey="servicePartner.carrierName" style="width:100px"/>
    <display:column property="carrierDeparture"  titleKey="servicePartner.carrierDeparture" style="width:75px"/>   
    <display:column property="etDepart"  titleKey="servicePartner.etDepart" format="{0,date,dd-MMM-yyyy}" style="width:60px"/>
    <display:column property="atDepart" titleKey="servicePartner.atDepart" format="{0,date,dd-MMM-yyyy}" style="width:60px"/>
    <display:column property="carrierArrival" titleKey="servicePartner.carrierArrival" style="width:75px" /> 
    <display:column property="etArrival" titleKey="servicePartner.etArrival" format="{0,date,dd-MMM-yyyy}" style="width:60px"/>
	<display:column property="atArrival" titleKey="servicePartner.atArrival" format="{0,date,dd-MMM-yyyy}" style="width:60px"/>
	<display:column property="omni"  titleKey="servicePartner.omni" style="width:100px"/>
	<display:column property="bookNumber" titleKey="servicePartner.bookNumber" style="width:15px"/>
	</display:table>
</div>
</td></tr> 
</tbody>
</table></div></td>
<td background="<c:url value='/images/bg-right.jpg'/>" align="left" class="listwhite"></td></tr>  
 <td height="10" width="100%" align="left" >
  <c:if test="${from=='rule'}">
   <c:if test="${field=='trackingStatus.deliveryFollowCall' || field=='trackingStatus.deliveryA' || field=='trackingStatus.deliveryShipper' || field=='trackingStatus.sitDestinationA' || field=='trackingStatus.clearCustom' || field=='trackingstatus.sent3299ToBroker' || field=='trackingStatus.sitDestinationTA'|| field =='trackingStatus.deliveryReceiptDate'}">
    <div  onClick="javascript:animatedcollapse.toggle('destination')" >
     <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center"><font color="red">&nbsp;Destination</font>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
  </div>
     </c:if>
     </c:if>
  <c:if test="${from=='rule'}">
  <c:if test="${field!='trackingStatus.deliveryFollowCall' && field!='trackingStatus.deliveryA' && field!='trackingStatus.deliveryShipper' && field!='trackingStatus.sitDestinationA' && field!='trackingStatus.clearCustom' && field!='trackingstatus.sent3299ToBroker' && field!='trackingStatus.sitDestinationTA'&& field!='trackingStatus.deliveryReceiptDate'}">
  <div  onClick="javascript:animatedcollapse.toggle('destination')" >
   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Destination
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
      </div>
  </c:if></c:if>
     <c:if test="${from!='rule'}">
  <div  onClick="javascript:animatedcollapse.toggle('destination')" >
    <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Destination
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
    </div>
      </c:if>
	<div id="destination" class="switchgroup1">
	<jsp:include flush="true" page="trackingStatusFormDestinationSection.jsp"></jsp:include>
</div></td>
<td></td></tr>
<sec-auth:authComponent componentId="module.tab.trackingStatus.ticketTab">
<tr>
<td colspan="20">
<div id="storageVolMain">
<table table cellspacing="0" cellpadding="0" style="margin: 0px; padding:0px;width:100%">
<c:choose>
<c:when test="${serviceOrder.job =='STO' || serviceOrder.job =='SSC' || serviceOrder.job =='STL'}">
<tr>
	<td height="10" width="100%" align="left" style="margin: 0px">
     <div onClick="javascript:animatedcollapse.toggle('storage')" style="margin: 0px">
   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Storage
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
      </div>
		<div id="storage" class="switchgroup1">	
		<table class="detailTabLabel" border="0" width="98%">
 		  <tbody>
<tr><td height="5px"></td></tr>
<tr><td align="left" colspan="10">
<c:if test="${isItemList == 'Y'}">
<s:set name="storages" value="storages" scope="request"/>
<display:table name="storages" class="table" requestURI=""  id="storageList" pagesize="10" style="margin:0px;padding:0px;" >
<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
<display:column property="ticket" sortable="true"  titleKey="storage.ticket" url="/editWorkTicketUpdate.html" paramId="id" paramProperty="id" /> 
</sec-auth:authComponent>
<display:column property="description" sortable="true"  titleKey="storage.description"/> 
<display:column property="locationId"  titleKey="storage.locationId"/>
<display:column property="pieces" headerClass="containeralign" style="text-align:right;" titleKey="storage.pieces"/>
<display:column property="releaseDate"  titleKey="storage.releaseDate" format="{0,date,dd-MMM-yyyy}"/> 
<display:column property="containerId" headerClass="containeralign" style="text-align:right;" titleKey="storage.containerId"/> 
<display:column property="itemNumber"  titleKey="storage.itemNumber" />
<configByCorp:fieldVisibility componentId="component.field.Alternative.showForCorpID">
<display:column property="storageId"  title="Storage Id"/>
</configByCorp:fieldVisibility>
<display:column property="measQuantity"  title="Weight" style="text-align:right;"/>
<display:column property="unit"  title="Unit"/>
<display:column property="volume"  title="Volume" style="text-align:right;"/>
<display:column property="volUnit"  title="Unit"/>
<configByCorp:fieldVisibility componentId="component.field.WareHouse.AddItemToLocation">
<display:column property="warehouse"  title="WareHouse"/>
</configByCorp:fieldVisibility>
</display:table>
</c:if>
<c:if test="${isItemList == 'F'}">
<s:set name="handOutList" value="handOutList" scope="request" />
<display:table name="handOutList" class="table" style="margin:3px 0px 5px 0px" requestURI="" id="handOutList">
    <display:column title="HO">
        <div style="text-align: center">
            <c:if test="${handOutList.hoTicket !=null && handOutList.hoTicket !=''}">
                <c:if test="${handOutList.released == null || handOutList.released == ''}">
                    <s:checkbox id="dd${handOutList.id}" name="dd" value="true" disabled="true" onclick="updateHoTicketByHoClick(this,'${handOutList.id}','${handOutList.shipNumber}');" />
                </c:if>
                <c:if test="${handOutList.released != null && handOutList.released != ''}">
                    <s:checkbox id="dd${handOutList.id}" name="dd" value="true" disabled="true" onclick="updateHoTicketByHoClick(this,'${handOutList.id}','${handOutList.shipNumber}');" />
                </c:if> </c:if>
            <c:if test="${handOutList.hoTicket ==null || handOutList.hoTicket ==''}">
                <s:checkbox id="dd${handOutList.id}" name="dd" disabled="true" onclick="updateHoTicketByHoClick(this,'${handOutList.id}','${handOutList.shipNumber}');" />
            </c:if>
        </div>
    </display:column>
    <display:column property="line" title="Line" headerClass="containeralign" style="width:40px;text-align:right;" />
    <display:column property="ticket" title="Ticket#" headerClass="containeralign" />
    <display:column property="itemDesc" style="width:300px;" title="Item Description" maxLength="15" />
    <display:column style="width:250px;" property="warehouse" title="Warehouse" />
    <display:column property="released" title="Released" style="width:100px" format="{0,date,dd-MMM-yyyy}" />
    <display:column property="quantityReceived" headerClass="containeralign" style="width:100px;text-align:right;" title="Actual&nbsp;Qty" />
    <display:column property="quantityShipped" headerClass="containeralign" style="width:100px;text-align:right;" title="Qty&nbsp;Shipped" />
    <display:column property="packId" headerClass="containeralign" style="width:150px;text-align:right" maxLength="10" title="PackId" />
    <display:column property="shippingPackId"  style="width:50px;" headerClass="setwidth" maxLength="10" title="New&nbsp;Shipping&nbsp;PackID" />
    <display:column property="weightReceived" headerClass="containeralign" style="width:80px;text-align:right;" title="Actual&nbsp;Weight" />
    <display:column property="unitOfWeight" style="width:50px;" title="Unit&nbsp;Weight" />
    <display:column property="volumeReceived" headerClass="containeralign" style="width:80px;text-align:right;" title="Actual Volume" />
    <display:column style="width:50px;" property="unitOfVolume" title="Unit&nbsp;Volume" />
    <display:column property="notes" title="Notes" maxLength="10" style="width:300px;" />
    </display:table>
</c:if>
</td></tr>
</tbody>
</table>
<table cellpadding="0" cellspacing="0" width="100%" style="margin:0px;padding:0px;">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Customs
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td></tr>
</table>
<table class="detailTabLabel" border="0" width="98%">
<tbody>
<tr><td align="left" colspan="10">
<s:set name="customs" value="customs" scope="request"/> 
<display:table name="customs" class="table" requestURI="" id="customList" pagesize="10">
   	 	<c:if test="${forwardingTabVal!='Y'}">
   	 	<display:column property="movement" sortable="true" title="Movement" href="editCustom.html?sid=${serviceOrder.id}" paramId="id" paramProperty="id" style="width:100px" />
		</c:if>
	<c:if test="${forwardingTabVal == 'Y'}">
		<c:if test="${customList.movement=='Out'}">
     <c:if test="${not empty customList.transactionId}"> 
     <display:column sortable="true" titleKey="custom.movement"  style="width:80px">
     <c:out value="${customList.movement}"/>
     <a href="javascript:openWindow('editCustomAjax.html?sid=${serviceOrder.id}&id=${customList.transactionId}&decorator=popup&popup=true','sqlExtractInputForm','height=500,width=950,top=0, scrollbars=yes,resizable=yes')"> (#<c:out value="${customList.transactionId}"></c:out>)</a>
     </display:column> 
     </c:if>
     </c:if>
     <c:if test="${customList.movement =='In'}">
     <display:column sortable="true" titleKey="custom.movement" style="width:80px">
      <a href="javascript:openWindow('editCustomAjax.html?sid=${serviceOrder.id}&id=${customList.id}&decorator=popup&popup=true','sqlExtractInputForm','height=500,width=950,top=0, scrollbars=yes,resizable=yes')"> <c:out value="${customList.movement}"></c:out></a>
     </display:column>
     </c:if>
   </c:if>
     <display:column property="ticket" sortable="true" title="Ticket#" />
     <display:column property="entryDate" sortable="true" title="Entry Date" format="{0,date,dd-MMM-yyyy}" /> 
     <display:column property="documentType" sortable="true" title="Document Type"/> 
     <display:column property="documentRef" sortable="true" title="Document Ref"/> 
     <display:column property="pieces" sortable="true" title="Pieces"/> 
     <display:column property="volume" sortable="true" style="text-align:right;" title="Volume"/> 
     <display:column property="weight" sortable="true" style="text-align:right;" title="Weight"/> 
     <display:column property="goods" sortable="true" title="Goods"/>       
</display:table> 
</td></tr>
</tbody>
</table>
</div></td>
<td></td></tr>  
</c:when>
<c:otherwise>
<c:choose>
<c:when test="${countStorage == '0'}">
</c:when>
<c:otherwise>
<tr>
   <td height="10" width="100%" align="left" style="margin: 0px">
     <div onClick="javascript:animatedcollapse.toggle('storage')" style="margin: 0px">
   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Storage
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
      </div>
	<div class="dspcont" id="storage" >
		<table class="detailTabLabel" border="0" width="98%">
 		  <tbody>
<tr><td height="5px"></td></tr>
<tr><td align="left" colspan="10">
<c:if test="${isItemList == 'Y'}">
<s:set name="storages" value="storages" scope="request"/>
<display:table name="storages" class="table" requestURI=""  id="storageList"  pagesize="10" style="margin:0px;padding:0px;">
<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
<display:column property="ticket"  titleKey="storage.ticket" url="/editWorkTicketUpdate.html" paramId="id" paramProperty="id"/> 
</sec-auth:authComponent>
<display:column property="description"   titleKey="storage.description"/> 
<display:column property="locationId"   titleKey="storage.locationId"/>
<configByCorp:fieldVisibility componentId="component.field.Alternative.showForCorpID">
<display:column property="storageId"  title="Storage Id"/>
</configByCorp:fieldVisibility>
<display:column property="pieces"  titleKey="storage.pieces"/>
<display:column property="releaseDate"   titleKey="storage.releaseDate" format="{0,date,dd-MMM-yyyy}"/> 
<display:column property="containerId"  titleKey="storage.containerId"/> 
<display:column property="itemNumber"   titleKey="storage.itemNumber"/>
<display:column property="measQuantity" style="text-align:right;" title="Weight"/>
<display:column property="unit"  title="Unit"/>
<display:column property="volume" style="text-align:right;" title="Volume"/>
<display:column property="volUnit"  title="Unit"/>
<configByCorp:fieldVisibility componentId="component.field.WareHouse.AddItemToLocation">
<display:column property="warehouse"  title="WareHouse"/>
</configByCorp:fieldVisibility>
</display:table>
</c:if>
<c:if test="${isItemList == 'F'}">
<s:set name="handOutList" value="handOutList" scope="request" />
<display:table name="handOutList" class="table" style="margin:3px 0px 5px 0px" requestURI="" id="handOutList">
    <display:column title="HO">
        <div style="text-align: center">
            <c:if test="${handOutList.hoTicket !=null && handOutList.hoTicket !=''}">
                <c:if test="${handOutList.released == null || handOutList.released == ''}">
                    <s:checkbox id="dd${handOutList.id}" name="dd" value="true" disabled="true" onclick="updateHoTicketByHoClick(this,'${handOutList.id}','${handOutList.shipNumber}');" />
                </c:if>
                <c:if test="${handOutList.released != null && handOutList.released != ''}">
                    <s:checkbox id="dd${handOutList.id}" name="dd" value="true" disabled="true" onclick="updateHoTicketByHoClick(this,'${handOutList.id}','${handOutList.shipNumber}');" />
                </c:if> </c:if>
            <c:if test="${handOutList.hoTicket ==null || handOutList.hoTicket ==''}">
                <s:checkbox id="dd${handOutList.id}" name="dd" disabled="true" onclick="updateHoTicketByHoClick(this,'${handOutList.id}','${handOutList.shipNumber}');" />
            </c:if>
        </div>
    </display:column>
    <display:column property="line" title="Line" headerClass="containeralign" style="width:40px;text-align:right;" />
    <display:column property="ticket" title="Ticket#" headerClass="containeralign" />
    <display:column property="itemDesc" style="width:300px;" title="Item Description" maxLength="15" />
    <display:column style="width:250px;" property="warehouse" title="Warehouse" />
    <display:column property="released" title="Released" style="width:100px" format="{0,date,dd-MMM-yyyy}" />
    <display:column property="quantityReceived" headerClass="containeralign" style="width:100px;text-align:right;" title="Actual&nbsp;Qty" />
    <display:column property="quantityShipped" headerClass="containeralign" style="width:100px;text-align:right;" title="Qty&nbsp;Shipped" />
    <display:column property="packId" headerClass="containeralign" style="width:150px;text-align:right" maxLength="10" title="PackId" />
    <display:column property="shippingPackId"  style="width:50px;" headerClass="setwidth" maxLength="10" title="New&nbsp;Shipping&nbsp;PackID" />
    <display:column property="weightReceived" headerClass="containeralign" style="width:80px;text-align:right;" title="Actual&nbsp;Weight" />
    <display:column property="unitOfWeight" style="width:50px;" title="Unit&nbsp;Weight" />
    <display:column property="volumeReceived" headerClass="containeralign" style="width:80px;text-align:right;" title="Actual Volume" />
    <display:column style="width:50px;" property="unitOfVolume" title="Unit&nbsp;Volume" />
    <display:column property="notes" title="Notes" maxLength="10" style="width:300px;" />
    </display:table>
</c:if>
</td></tr>
</tbody>
</table>
<table cellpadding="0" cellspacing="0" width="100%" style="margin:0px;padding:0px;">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Customs
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
<table class="detailTabLabel" border="0" width="98%">
 		  <tbody>
<tr><td align="left" colspan="10">
<s:set name="customs" value="customs" scope="request"/> 
<display:table name="customs" class="table" requestURI="" id="customList" pagesize="10"> 
	 <c:if test="${forwardingTabVal!='Y'}">
   	 	<display:column property="movement" sortable="true" title="Movement" href="editCustom.html?sid=${serviceOrder.id}" paramId="id" paramProperty="id" style="width:100px" />
		</c:if>
	<c:if test="${forwardingTabVal == 'Y'}">
		<c:if test="${customList.movement=='Out'}">
     <c:if test="${not empty customList.transactionId}"> 
     <display:column sortable="true" titleKey="custom.movement"  style="width:80px">
     <c:out value="${customList.movement}"/>
     <a href="javascript:openWindow('editCustomAjax.html?sid=${serviceOrder.id}&id=${customList.transactionId}&decorator=popup&popup=true','sqlExtractInputForm','height=500,width=950,top=0, scrollbars=yes,resizable=yes')"> (#<c:out value="${customList.transactionId}"></c:out>)</a>
     </display:column> 
     </c:if>
     </c:if>
     <c:if test="${customList.movement =='In'}">
     <display:column sortable="true" titleKey="custom.movement" style="width:80px">
      <a href="javascript:openWindow('editCustomAjax.html?sid=${serviceOrder.id}&id=${customList.id}&decorator=popup&popup=true','sqlExtractInputForm','height=500,width=950,top=0, scrollbars=yes,resizable=yes')"> <c:out value="${customList.movement}"></c:out></a>
     </display:column>
     </c:if>
   </c:if>
     <display:column property="ticket" sortable="true" title="Ticket#" />
     <display:column property="entryDate" sortable="true" title="Entry Date" format="{0,date,dd-MMM-yyyy}" /> 
     <display:column property="documentType" sortable="true" title="Document Type"/> 
     <display:column property="documentRef" sortable="true" title="Document Ref"/> 
     <display:column property="pieces" sortable="true" title="Pieces"/> 
     <display:column property="volume" sortable="true" style="text-align:right;" title="Volume"/> 
     <display:column property="weight" sortable="true" style="text-align:right;" title="Weight"/> 
     <display:column property="goods" sortable="true" title="Goods"/>       
</display:table> 
</td></tr>
</tbody>
</table>
</div>
</td>
<td></td></tr>  
</c:otherwise>
</c:choose> 
</c:otherwise>
</c:choose>
</table>
</div>
</td>
</tr>
</sec-auth:authComponent>
</tr>
<tr height="30px"></tr>
	</tbody>
	</table>
	</td>
	</tr>
	</tbody>
	</table>
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<table style="margin-left: 30px;" border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='trackingStatus.createdOn'/></b></td> 
							<td style="width:120px">
								<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${trackingStatus.createdOn}" pattern="${displayDateTimeEditFormat}"/>
								<s:hidden name="trackingStatus.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
								<fmt:formatDate value="${trackingStatus.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='trackingStatus.createdBy' /></b></td>
							<c:if test="${not empty trackingStatus.id}">
								<s:hidden name="trackingStatus.createdBy"/>
								<td style="width:85px; font-size:1em;"><s:label name="createdBy" value="%{trackingStatus.createdBy}"/></td>
							</c:if>
							<c:if test="${empty trackingStatus.id}">
								<s:hidden name="trackingStatus.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:85px; font-size:1em;"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='trackingStatus.updatedOn'/></b></td>
								<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${trackingStatus.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
								<s:hidden name="trackingStatus.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${trackingStatus.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='trackingStatus.updatedBy' /></b></td>
							<c:if test="${not empty trackingStatus.id}">
								<s:hidden name="trackingStatus.updatedBy"/>
								<td style="width:85px ;font-size:1em;"><s:label name="updatedBy" value="%{trackingStatus.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty trackingStatus.id}">
								<s:hidden name="trackingStatus.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px;font-size:1em;"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
</div>
	<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
	  <s:submit cssClass="cssbutton" method="save" id="saveButton" key="button.save" onmouseover="completeTimeString();"  onclick="enableAll();return IsValidTime('save');calculateDeliveryDate();" cssStyle="width:55px; height:25px" tabindex="105" />
	  <s:reset cssClass="cssbutton" key="Reset"  onmousemove="checkDateStatus();" onclick="resetImage();resetImage1();resetImage3();resetImage4();openDiv();" cssStyle="width:55px; height:25px " tabindex="106"/>
	</sec-auth:authComponent>
<s:hidden name="shipNumberCode" value="${serviceOrder.sequenceNumber}${serviceOrder.ship}"/>
<s:hidden name="trackingStatus.id" value="%{trackingStatus.id}"/>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:set var="idOfTasks" value="${serviceOrder.id}" scope="session"/>
 <c:set var="tableName" value="trackingstatus" scope="session"/>
 <s:hidden name="SNumber" value="%{serviceOrder.shipNumber}"/>
 <c:set var="SNumber" value="${serviceOrder.shipNumber}" />
        <%
        String value=(String)pageContext.getAttribute("SNumber") ;
        Cookie cookie = new Cookie("TempnotesId",value);
        cookie.setMaxAge(3600);
        response.addCookie(cookie);
        %>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
	<s:hidden name="miscellaneous.id" />
	<s:hidden name="miscellaneous.corpID" />
	<s:hidden name="miscellaneous.sequenceNumber" />
	<s:hidden name="miscellaneous.ship" />
	<s:hidden name="miscellaneous.shipNumber" />
	<s:hidden name="miscellaneous.listLotNumber" />
	<s:hidden name="miscellaneous.bookerSelfPacking" />
	<s:hidden name="miscellaneous.bookerOwnAuthority" />
	<s:hidden name="miscellaneous.packAuthorize" />
	<s:hidden name="miscellaneous.unPack" />
	<s:hidden name="miscellaneous.packingBulky" />
	<s:hidden name="miscellaneous.codeHauling" />
	<s:hidden name="miscellaneous.tarif" />
	<s:hidden name="miscellaneous.section" />
	<s:hidden name="miscellaneous.applicationNumber3rdParty" />
	<s:hidden name="miscellaneous.needWaiveEstimate" />
	<s:hidden name="miscellaneous.needWaiveSurvey" />
	<s:hidden name="miscellaneous.needWaivePeakRate" />
	<s:hidden name="miscellaneous.needWaiveSRA" />
	<s:hidden name="miscellaneous.vanLineOrderNumber" />
	<s:hidden name="miscellaneous.vanLineNationalCode" />
	<s:hidden name="miscellaneous.originCountyCode" />
	<s:hidden name="miscellaneous.originStateCode" />
	<s:hidden name="miscellaneous.originCounty" />
	<s:hidden name="miscellaneous.originState" />
	<s:hidden name="miscellaneous.destinationCountyCode" />
	<s:hidden name="miscellaneous.destinationStateCode" />
	<s:hidden name="miscellaneous.destinationCounty" />
	<s:hidden name="miscellaneous.destinationState" />
	<s:hidden name="miscellaneous.extraStopYN" />
	<s:hidden name="miscellaneous.mile" />
	<s:hidden name="miscellaneous.ratePSTG" />
	<s:hidden name="miscellaneous.createdBy"/>
	<s:hidden name="miscellaneous.updatedBy"/>
	<c:if test="${not empty miscellaneous.updatedOn}">
	<fmt:formatDate var="updatedOnFormattedValue" value="${miscellaneous.updatedOn}" pattern="${displayDateTimeFormat}"/>
	<s:hidden name="miscellaneous.updatedOn" value="${updatedOnFormattedValue}"/>
	</c:if>
	<s:hidden name="miscellaneous.haulingAgentCode"/> 
	<s:hidden name="miscellaneous.haulerName"/> 
	<s:hidden name="miscellaneous.driverId"/> 
	<s:hidden name="miscellaneous.driverName"/> 
	<s:hidden name="miscellaneous.driverCell"/> 
	<s:hidden name="miscellaneous.selfHaul"/>
	<c:if test="${not empty trackingStatus.endPacking}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="trackingStatus.endPacking" /></s:text>
			 <s:hidden  name="trackingStatus.endPacking" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty trackingStatus.endPacking}">
		 <s:hidden   name="trackingStatus.endPacking"/> 
	 </c:if>
	 <c:if test="${not empty miscellaneous.assignDate}">
		 <s:text id="trackingStatusSitDestinationAFormattedValueassignDate" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.assignDate" /></s:text>
			 <s:hidden  name="miscellaneous.assignDate" value="%{trackingStatusSitDestinationAFormattedValueassignDate}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.assignDate}">
		 <s:hidden   name="miscellaneous.assignDate"/> 
	 </c:if>
	<c:if test="${not empty trackingStatus.endLoad}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="trackingStatus.endLoad" /></s:text>
			 <s:hidden  name="trackingStatus.endLoad" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty trackingStatus.endLoad}">
		 <s:hidden   name="trackingStatus.endLoad"/>
	 </c:if>
	 <c:if test="${not empty trackingStatus.deliveryLastDay}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="trackingStatus.deliveryLastDay" /></s:text>
			 <s:hidden  name="trackingStatus.deliveryLastDay" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty trackingStatus.deliveryLastDay}">
		 <s:hidden   name="trackingStatus.deliveryLastDay"/> 
	 </c:if>
	 <c:if test="${not empty trackingStatus.deliveryTA}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="trackingStatus.deliveryTA" /></s:text>
			 <s:hidden  name="deliveryTA" value="%{customerFileSurveyFormattedValue}" /> 
			 <fmt:formatDate var="trackingStatusDeliveryTA" value="${trackingStatus.deliveryTA}" pattern="${displayDateTimeEditFormat}"/>
			<s:hidden name="trackingStatus.deliveryTA" value="${trackingStatusDeliveryTA}"/>
	 </c:if>
	 <c:if test="${empty trackingStatus.deliveryTA}">
		 <s:hidden   name="trackingStatus.deliveryTA"/> 
	 </c:if>
	<s:hidden name="miscellaneous.estimatedRevenue" /><s:hidden name="miscellaneous.totalActualRevenue" /><s:hidden name="miscellaneous.actualLineHaul" /><s:hidden name="miscellaneous.totalDiscountPercentage" />
	<s:hidden name="miscellaneous.warehouse" />
	<s:hidden name="miscellaneous.tag" />
	<s:hidden name="miscellaneous.actualHouseHoldGood" />
	<s:hidden name="miscellaneous.actualRevenueDollor" />
	<s:hidden name="miscellaneous.agentPerformNonperform" />
	<s:hidden name="miscellaneous.destination24HR" />
	<s:hidden name="miscellaneous.origin24Hr" />
	<s:hidden name="miscellaneous.vanLinePickupNumber" />
	<s:hidden name="miscellaneous.actualBoat" />
	<s:hidden name="miscellaneous.trailerVolumeWeight" />
	<s:hidden name="miscellaneous.gate" />
	<s:hidden name="miscellaneous.trailer" />
	<s:hidden name="miscellaneous.pendingNumber" />
	<s:hidden name="miscellaneous.vanLineCODAmount" />
	<s:hidden name="miscellaneous.weightTicket" />
	<s:hidden name="miscellaneous.domesticInstruction" />
	<s:hidden name="miscellaneous.paperWork" />
	<s:hidden name="miscellaneous.tripNumber"/>
					 <c:if test="${not empty miscellaneous.recivedPO}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.recivedPO" /></s:text>
			 <s:hidden  name="miscellaneous.recivedPO" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.recivedPO}">
		 <s:hidden   name="miscellaneous.recivedPO"/> 
	 </c:if>
	  <c:if test="${not empty miscellaneous.recivedHVI}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.recivedHVI" /></s:text>
			 <s:hidden  name="miscellaneous.recivedHVI" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.recivedHVI}">
		 <s:hidden   name="miscellaneous.recivedHVI"/> 
	 </c:if>
	  
	  <c:if test="${not empty miscellaneous.settledDate}">
		 <s:text id="customerFileSettledDateFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.settledDate" /></s:text>
			 <s:hidden  name="miscellaneous.settledDate" value="%{customerFileSettledDateFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.settledDate}">
		 <s:hidden   name="miscellaneous.settledDate"/> 
	 </c:if>
	  <c:if test="${not empty miscellaneous.quoteOn}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.quoteOn" /></s:text>
			 <s:hidden  name="miscellaneous.quoteOn" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.quoteOn}">
		 <s:hidden   name="miscellaneous.quoteOn"/> 
	 </c:if>
	  <c:if test="${not empty miscellaneous.recivedPayment}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.recivedPayment" /></s:text>
			 <s:hidden  name="miscellaneous.recivedPayment" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.recivedPayment}">
		 <s:hidden   name="miscellaneous.recivedPayment"/> 
	 </c:if>
	<s:hidden name="miscellaneous.peakRate" />
		<c:if test="${not empty miscellaneous.transDocument}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.transDocument" /></s:text>
			 <s:hidden  name="miscellaneous.transDocument" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.transDocument}">
		 <s:hidden   name="miscellaneous.transDocument"/> 
	 </c:if>
	<s:hidden name="miscellaneous.carrier" />
	<s:hidden name="miscellaneous.carrierName"/> 	
		<c:if test="${not empty miscellaneous.atlasDown}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.atlasDown" /></s:text>
			 <s:hidden  name="miscellaneous.atlasDown" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.atlasDown}">
		 <s:hidden   name="miscellaneous.atlasDown"/> 
	 </c:if>
	 		<c:if test="${not empty miscellaneous.shipmentRegistrationDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.shipmentRegistrationDate" /></s:text>
			 <s:hidden  name="miscellaneous.shipmentRegistrationDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.shipmentRegistrationDate}">
		 <s:hidden   name="miscellaneous.shipmentRegistrationDate"/> 
	 </c:if>
	 
	 <c:if test="${not empty trackingStatus.ediDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="trackingStatus.ediDate" /></s:text>
			 <s:hidden name="trackingStatus.ediDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty trackingStatus.ediDate}">
		 <s:hidden name="trackingStatus.ediDate"/> 
	 </c:if>
	 
	<s:hidden  name="miscellaneous.packingDriverId" />
<s:hidden  name="miscellaneous.deliveryDriverId" />
<s:hidden  name="miscellaneous.loadingDriverId" />
<s:hidden  name="miscellaneous.sitDestinationDriverId" />
	<s:hidden name="secondDescription" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription" />
	<s:hidden name="sixthDescription" />
	<s:hidden name="firstDescription" />
	<s:hidden name="seventhDescription" />
	<s:hidden name="miscellaneous.haulingAgentVanlineCode"/> 
<s:hidden name="trackingStatus.lateReason"/> 
<s:hidden name="trackingStatus.partyResponsible"/> 
<s:hidden name="miscellaneous.loadCount"/> 
<s:hidden name="miscellaneous.loadSignature"/> 
<s:hidden name="miscellaneous.deliveryCount"/> 
<s:hidden name="miscellaneous.deliverySignature"/> 
<s:hidden name="miscellaneous.tractorAgentVanLineCode"/> 
<s:hidden name="miscellaneous.tractorAgentName"/> 
<s:hidden name="miscellaneous.vanAgentVanLineCode"/>
<s:hidden name="miscellaneous.vanAgentName"/>
<s:hidden name="miscellaneous.vanID"/>
<s:hidden name="miscellaneous.vanName"/>
<div id="overlayForAgentMarket" style="display:none">
<div id="layerLoading">
<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr><td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr><td height="200px"></td></tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
       <img src="<c:url value='/images/ajax-loader-hr.gif'/>" />       
       </td>
       </tr>
     </table>
   </td>
  </tr>
</table>  
   </div>   
   </div>
</s:form>
<script type="text/javascript"> 
function findCorpIdOfCode(key1){
	var key='';
	var value='';
	var findKey='';
	<c:forEach var="entry" items="${comDivCodePerCorpIdMap}">
	if(findKey==''){
		key="${entry.key}";	
		value="${entry.value}";
		if(value.indexOf(key1)>-1){
			findKey=key+"^"+value;
		}
	}	
	</c:forEach>	
	return findKey;
}
function IsValidTime(clickType) {
	if(${isNetworkBookingAgent==true || isNetworkOriginAgent==true ||  isNetworkDestinationAgent==true || networkAgent}){
		document.forms['trackingStatusForm'].elements['buttonType'].value=clickType; 
		var savedDestinationAgent='${trackingStatus.destinationAgentCode}';
		var presentDestinationAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
		var destinationAgentExSo='${trackingStatus.destinationAgentExSO}';
		var presentDestinationEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value;
		
		var findKey=findCorpIdOfCode(savedDestinationAgent);
		var sameCompanyCodeflag=false;
		if(presentDestinationAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentDestinationAgent)>-1){
			sameCompanyCodeflag=true;
		}
		
		if(destinationAgentExSo!='' && presentDestinationEXSO !='' && !sameCompanyCodeflag){
		 if(savedDestinationAgent!=presentDestinationAgent && savedDestinationAgent != '' && savedDestinationAgent != null){
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value='remove'+destinationAgentExSo;
			} }
		var savedDestinationSubAgent='${trackingStatus.destinationSubAgentCode}';
		var presentDestinationSubAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
		var destinationSubAgentExSo='${trackingStatus.destinationSubAgentExSO}';
		var presentDestinationSubAgentEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value;
		
		 findKey=findCorpIdOfCode(savedDestinationSubAgent);
		 sameCompanyCodeflag=false;
		if(presentDestinationSubAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentDestinationSubAgent)>-1){
			sameCompanyCodeflag=true;
		}
		
		if(destinationSubAgentExSo!='' && presentDestinationSubAgentEXSO != '' && !sameCompanyCodeflag){
		 if(savedDestinationSubAgent!=presentDestinationSubAgent && savedDestinationSubAgent != '' && savedDestinationSubAgent != null){
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value='remove'+destinationSubAgentExSo;
			} }
		var savedOriginAgent='${trackingStatus.originAgentCode}';
		var presentOriginAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
		var originAgentExSo='${trackingStatus.originAgentExSO}';
		var presentOriginAgentEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value;
		
		 findKey=findCorpIdOfCode(savedOriginAgent);
		 sameCompanyCodeflag=false;
		if(presentOriginAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentOriginAgent)>-1){
			sameCompanyCodeflag=true;
		}
		
		if(originAgentExSo!='' && presentOriginAgentEXSO != '' && !sameCompanyCodeflag){
		 if(savedOriginAgent!=presentOriginAgent && savedOriginAgent != '' && savedOriginAgent != null){
			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value='remove'+originAgentExSo;
			} }
		var savedOriginSubAgent='${trackingStatus.originSubAgentCode}';
		var presentOriginSubAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
		var originSubAgentExSo='${trackingStatus.originSubAgentExSO}';
		var presentOriginSubAgentEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value;

		findKey=findCorpIdOfCode(savedOriginSubAgent);
		 sameCompanyCodeflag=false;
		if(presentOriginSubAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentOriginSubAgent)>-1){
			sameCompanyCodeflag=true;
		}
		
		if(originSubAgentExSo!='' && presentOriginSubAgentEXSO != '' && !sameCompanyCodeflag){
		 if(savedOriginSubAgent!=presentOriginSubAgent && savedOriginSubAgent != '' && savedOriginSubAgent != null){
			document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value='remove'+originSubAgentExSo;
			} }
		var savedNetworkPartnerCode='${trackingStatus.networkPartnerCode}';
		var presentNetworkPartnerCode=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
		var networkAgentExSO='${trackingStatus.networkAgentExSO}';
		var presentnetworkAgentExSO = document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value;
		
		 findKey=findCorpIdOfCode(savedNetworkPartnerCode);
		 sameCompanyCodeflag=false;
		if(presentNetworkPartnerCode!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentNetworkPartnerCode)>-1){
			sameCompanyCodeflag=true;
		}
		
		if(networkAgentExSO!='' && presentnetworkAgentExSO != '' && !sameCompanyCodeflag){
		 if(savedNetworkPartnerCode!=presentNetworkPartnerCode && savedNetworkPartnerCode != '' && savedNetworkPartnerCode != null){
			document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value='remove'+networkAgentExSO;
			} } }
try{	
<configByCorp:fieldVisibility componentId="trackingStatus.surveyDate">
var timeStr = document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].value;
var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
if(timeStr != "")
alert("Time is not in a valid format. Please use HH:MM format");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6]; 
if (second=="") { second = null; }
if (ampm=="") { ampm = null } 
if (hour < 0  || hour > 23) {
alert("'Survey' time must between 0 to 23 (Hrs)");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].focus();
return false;
}
if (minute<0 || minute > 59) {
alert ("'Survey' time must between 0 to 59 (Min)");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Survey' time must between 0 to 59 (Sec)");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeFrom'].focus();
return false;
}
var time2Str = document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].value;
var matchTime2Array = time2Str.match(timePat);
if (matchTime2Array == null) {
alert("Time is not in a valid format. please Use HH:MM format");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].focus();
return false;
}
hourTime2 = matchTime2Array[1];
minuteTime2 = matchTime2Array[2];
secondTime2 = matchTime2Array[4];
ampmTime2 = matchTime2Array[6];
if (hourTime2 < 0  || hourTime2 > 23) {
alert("'Survey' time must between 0 to 23 (Hrs)");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'Survey' time must between 0 to 59 (Min)");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'Survey' time must between 0 to 59 (Sec)");
document.forms['trackingStatusForm'].elements['trackingStatus.surveyTimeTo'].focus();
return false;
}
</configByCorp:fieldVisibility>
var timeStr = document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].value;
var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
if(timeStr != "")
alert("Time is not in a valid format. Please use HH:MM format");
document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];
if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if(hour != null && (hour < 0  || hour > 23)) {
alert("'Cut off time' must be between 0 to 23 (Hrs)");
document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].focus();
return false;
}
if(minute != null &&  (minute<0 || minute > 59)) {
alert ("'Cut off time' must be between 0 to 59 (Min)");
document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Cut off time' must be between 0 to 59 (Sec)");
document.forms['trackingStatusForm'].elements['trackingStatus.cutOffTimeTo'].focus();
return false;
}
<configByCorp:fieldVisibility componentId="component.tab.trackingStatus.docCutOffTimeTo">
var doctimeStr = document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].value;
var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var matchArray = doctimeStr.match(timePat);
if (matchArray == null) {
if(doctimeStr != "")
alert("Time is not in a valid format. Please use HH:MM format");
document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];
if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if(hour != null && (hour < 0  || hour > 23)) {
alert("'Cut off time' must be between 0 to 23 (Hrs)");
document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].focus();
return false;
}
if(minute != null &&  (minute<0 || minute > 59)) {
alert ("'Cut off time' must be between 0 to 59 (Min)");
document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Cut off time' must be between 0 to 59 (Sec)");
document.forms['trackingStatusForm'].elements['trackingStatus.docCutOffTimeTo'].focus();
return false;
}
</configByCorp:fieldVisibility>
}catch(e){}
}
<c:if test="${hitFlag=='1'}">
	 <c:if test="${checkPropertyAmountComponent!='Y'}">
	 <c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}"/>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}&msgClicked=${msgClicked}"/>
	</c:if>
</c:if>
	try{
	var f = document.getElementById('trackingStatusForm'); 
	f.setAttribute("autocomplete", "off");
    }
    catch(e){} 
    function resetImage(){
    	vendorChangeStatus='';
    	try{
		   	setTimeout(function(){showAddressImage();}, 20);  		
	       }catch(e){}
	}
	function resetImage1(){
	try{
		   	setTimeout(function(){showAddressImage1();}, 20);  		
	       }	catch(e){}
	}
	function resetImage3(){
	try{
		   	setTimeout(function(){showAddressImageSub1();}, 20);  		
	       }    catch(e){}
    }
    function resetImage4(){
    try{
		   	setTimeout(function(){showAddressImageSub2();}, 20);  		
	 }	 catch(e){}
	 }    try{
	    checkMultiAuthorization();
	    }	    catch(e){}
	    try{
	    showEmailImage();        
	    }	    catch(e){}
	    try{
	    showOriginAgentEmailImage();
	    }	    catch(e){}
	    try{
	    showDestinationAgentEmailImage();
	    }
	    catch(e){}
	    try{
	    showSubOriginAgentEmailImage();
	    }	    catch(e){}
	   try{
	    showSubDestinationAgentEmailImage();
	    }	    catch(e){}
	    try{
	    showBrokerEmailImage();
	    }	    catch(e){}
	    try{
	    showForwarderEmailImage();
	    }	    catch(e){}
	    	    <sec-auth:authComponent componentId="module.script.form.agentScript">
window.onload = function() { 
		trap();
		try{ 
			 var linkup_Network = $('#linkup_Network');
			 $(linkup_Network).hide();
			 var linkup_originAgentExSO = $('#linkup_originAgentExSO');
			 $(linkup_originAgentExSO).hide();			 
			 var linkup_destinationAgentExSO = $('#linkup_destinationAgentExSO');
			 $(linkup_destinationAgentExSO).hide();
			 var linkup_originSubAgentExSO = $('#linkup_originSubAgentExSO');
			 $(linkup_originSubAgentExSO).hide();
			 var linkup_destinationSubAgentExSO = $('#linkup_destinationSubAgentExSO');
			 $(linkup_destinationSubAgentExSO).hide();
			 var linkup_bookingAgent = $('#linkup_bookingAgent');
			 $(linkup_bookingAgent).hide(); 
        }catch(e){}
        try{
			 var linkup_hidCopyOADetailImage = $('#hidCopyOADetailImage');
			 $(linkup_hidCopyOADetailImage).hide();            
			 var linkup_hidCopyDADetailImage = $('#hidCopyDADetailImage');
			 $(linkup_hidCopyDADetailImage).hide();
      }catch(e){}
       
		 $('#trackingStatusForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
		 $('#trackingStatusForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper');	 
		 if(document.forms['trackingStatusForm'].elements['saveButton'])
			{
				document.forms['trackingStatusForm'].elements['saveButton'].disabled=false;
			}
			if(document.forms['trackingStatusForm'].elements['Reset'])
			{
				document.forms['trackingStatusForm'].elements['Reset'].disabled=false;
			}
			<sec-auth:authScript tableList="trackingStatus,miscellaneous" formNameList="trackingStatusForm,trackingStatusForm,trackingStatusForm" transIdList='${serviceOrder.shipNumber}'>
	
			</sec-auth:authScript>
			trap1();
				}		
 </sec-auth:authComponent> 
<sec-auth:authComponent componentId="module.script.form.corpAccountScript">
	window.onload = function() { 
		trap();
		try{
			 var linkup_Network = $('#linkup_Network');
			 $(linkup_Network).hide();
			 var linkup_originAgentExSO = $('#linkup_originAgentExSO');
			 $(linkup_originAgentExSO).hide();			 
			 var linkup_destinationAgentExSO = $('#linkup_destinationAgentExSO');
			 $(linkup_destinationAgentExSO).hide();
			 var linkup_originSubAgentExSO = $('#linkup_originSubAgentExSO');
			 $(linkup_originSubAgentExSO).hide();
			 var linkup_destinationSubAgentExSO = $('#linkup_destinationSubAgentExSO');
			 $(linkup_destinationSubAgentExSO).hide();
			 var linkup_bookingAgent = $('#linkup_bookingAgent');
			 $(linkup_bookingAgent).hide();	
        }catch(e){}
        try{
			 var linkup_hidCopyOADetailImage = $('#hidCopyOADetailImage');
			 $(linkup_hidCopyOADetailImage).hide();            
			 var linkup_hidCopyDADetailImage = $('#hidCopyDADetailImage');
			 $(linkup_hidCopyDADetailImage).hide();
      }catch(e){}
		 $('#trackingStatusForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
		 $('#trackingStatusForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper');		
	 }
</sec-auth:authComponent>
<sec-auth:authComponent componentId="module.script.form.vendettiScript">
window.onload = function() { 
		trap();
		try{
			 var linkup_Network = $('#linkup_Network');
			 $(linkup_Network).hide();
			 var linkup_originAgentExSO = $('#linkup_originAgentExSO');
			 $(linkup_originAgentExSO).hide();			 
			 var linkup_destinationAgentExSO = $('#linkup_destinationAgentExSO');
			 $(linkup_destinationAgentExSO).hide();
			 var linkup_originSubAgentExSO = $('#linkup_originSubAgentExSO');
			 $(linkup_originSubAgentExSO).hide();
			 var linkup_destinationSubAgentExSO = $('#linkup_destinationSubAgentExSO');
			 $(linkup_destinationSubAgentExSO).hide();
			 var linkup_bookingAgent = $('#linkup_bookingAgent');
			 $(linkup_bookingAgent).hide();	
       }catch(e){}
       try{
			 var linkup_hidCopyOADetailImage = $('#hidCopyOADetailImage');
			 $(linkup_hidCopyOADetailImage).hide();            
			 var linkup_hidCopyDADetailImage = $('#hidCopyDADetailImage');
			 $(linkup_hidCopyDADetailImage).hide();
     }catch(e){}
		 $('#trackingStatusForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
		 $('#trackingStatusForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper').attr('onkeyup','false'); 
		 if(document.forms['trackingStatusForm'].elements['saveButton']){
			document.forms['trackingStatusForm'].elements['saveButton'].disabled=false;
		}
		if(document.forms['trackingStatusForm'].elements['Reset']){
				document.forms['trackingStatusForm'].elements['Reset'].disabled=false;
		}
		<sec-auth:enableScript tableList="trackingStatus,miscellaneous" formNameList="trackingStatusForm,trackingStatusForm,trackingStatusForm" ></sec-auth:enableScript>
		trap1();
	}		
 </sec-auth:authComponent>
 <sec-auth:authComponent componentId="module.script.form.partnerScript">
window.onload = function() { 
		trap();
		try{
			 var linkup_Network = $('#linkup_Network');
			 $(linkup_Network).hide();
			 var linkup_originAgentExSO = $('#linkup_originAgentExSO');
			 $(linkup_originAgentExSO).hide();			 
			 var linkup_destinationAgentExSO = $('#linkup_destinationAgentExSO');
			 $(linkup_destinationAgentExSO).hide();
			 var linkup_originSubAgentExSO = $('#linkup_originSubAgentExSO');
			 $(linkup_originSubAgentExSO).hide();
			 var linkup_destinationSubAgentExSO = $('#linkup_destinationSubAgentExSO');
			 $(linkup_destinationSubAgentExSO).hide();
			 var linkup_bookingAgent = $('#linkup_bookingAgent');
			 $(linkup_bookingAgent).hide();	
       }catch(e){}
       try{
			 var linkup_hidCopyOADetailImage = $('#hidCopyOADetailImage');
			 $(linkup_hidCopyOADetailImage).hide();            
			 var linkup_hidCopyDADetailImage = $('#hidCopyDADetailImage');
			 $(linkup_hidCopyDADetailImage).hide();
     }catch(e){}
		 $('#trackingStatusForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
		 $('#trackingStatusForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper').attr('onkeyup','false');			
			if(document.forms['trackingStatusForm'].elements['saveButton']){
				document.forms['trackingStatusForm'].elements['saveButton'].disabled=false;
			}
			if(document.forms['trackingStatusForm'].elements['Reset']){
				document.forms['trackingStatusForm'].elements['Reset'].disabled=false;
			}
			<sec-auth:enableScript tableList="trackingStatus,miscellaneous" formNameList="trackingStatusForm,trackingStatusForm,trackingStatusForm" >
			</sec-auth:enableScript>
			trap1();			
	}		
 </sec-auth:authComponent>
 <sec-auth:authComponent componentId="module.script.form.corpSalesScript">
 window.onload = function() { 	
 	trap();
 	trap1();
 	try{
 		 var linkup_Network = $('#linkup_Network');
 		 $(linkup_Network).hide();
 		 var linkup_originAgentExSO = $('#linkup_originAgentExSO');
 		 $(linkup_originAgentExSO).hide();			 
 		 var linkup_destinationAgentExSO = $('#linkup_destinationAgentExSO');
 		 $(linkup_destinationAgentExSO).hide();
 		 var linkup_originSubAgentExSO = $('#linkup_originSubAgentExSO');
 		 $(linkup_originSubAgentExSO).hide();
 		 var linkup_destinationSubAgentExSO = $('#linkup_destinationSubAgentExSO');
 		 $(linkup_destinationSubAgentExSO).hide();
 		 var linkup_bookingAgent = $('#linkup_bookingAgent');
 		 $(linkup_bookingAgent).hide();	
    }catch(e){}
    try{
		 var linkup_hidCopyOADetailImage = $('#hidCopyOADetailImage');
		 $(linkup_hidCopyOADetailImage).hide();            
		 var linkup_hidCopyDADetailImage = $('#hidCopyDADetailImage');
		 $(linkup_hidCopyDADetailImage).hide();
  }catch(e){}
 	 $('#trackingStatusForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
 	 $('#trackingStatusForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper').attr('onkeyup','false'); 
 }
 </sec-auth:authComponent>
 </script>
 <script type="text/javascript">
 var fieldName = document.forms['trackingStatusForm'].elements['field'].value;
 if(fieldName=='trackingStatus.loadA'){
  	fieldName=document.forms['trackingStatusForm'].elements['loadA'].name;
 }else if(fieldName=='trackingStatus.packA'){
 	fieldName = document.forms['trackingStatusForm'].elements['packA'].name;
  }else if(fieldName=='trackingStatus.deliveryA'){
	fieldName = document.forms['trackingStatusForm'].elements['deliveryA'].name;	
 }else{
	fieldName = document.forms['trackingStatusForm'].elements['field'].value;
 }
 var fieldName1 = document.forms['trackingStatusForm'].elements['field1'].value;
 if(fieldName1=='trackingStatus.loadA'){
 	fieldName1=document.forms['trackingStatusForm'].elements['loadA'].name;
 }else if(fieldName1=='trackingStatus.packA'){
 	fieldName1=document.forms['trackingStatusForm'].elements['packA'].name;
 }else if(fieldName1=='trackingStatus.deliveryA'){
	 fieldName1=document.forms['trackingStatusForm'].elements['deliveryA'].name;
	  }else{
	fieldName1 = document.forms['trackingStatusForm'].elements['field1'].value; }
 if(fieldName!=''){
	 document.forms['trackingStatusForm'].elements[fieldName].className = 'rules-textUpper';
	 animatedcollapse.addDiv('agentroles', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('weights', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('initiation', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('orgpack', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('sitorigin', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('interstate', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('orgfwd', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('military','fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('gstfwd', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('transport', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('destination', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('sitdestination', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('storage', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('customs', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('weightnvolumetracsub', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('weightnvolumetracsub2', 'fade=0,persist=0,show=1')
	 animatedcollapse.init() }
 if(fieldName1!=''){
	 document.forms['trackingStatusForm'].elements[fieldName1].className = 'rules-textUpper';
	 animatedcollapse.addDiv('agentroles', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('weights', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('initiation', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('orgpack', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('sitorigin', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('interstate', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('orgfwd', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('military','fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('gstfwd', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('transport', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('destination', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('sitdestination', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('storage', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('customs', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('weightnvolumetracsub', 'fade=0,persist=0,show=1')
	 animatedcollapse.addDiv('weightnvolumetracsub2', 'fade=0,persist=0,show=1')
	 animatedcollapse.init() }        
</script>
<script type="text/javascript">
var elementNameT;
function setFieldName(elementName){
	elementNameT = elementName;
}
function calcDelDate(){
	if(elementNameT == 'loadA'){
		calculateDeliveryDate();
	}
	if(elementNameT == 'leftWHOn'){
		<c:if test="${serviceOrder.isNetworkRecord!=true}" >		
		autoPopulateActualDeliveryDate();
		</c:if>
	}}
	function calcRddDate(){
		 var str="0";
		   <configByCorp:fieldVisibility componentId="component.field.Alternative.rddDaysTS">
		   	str="1";
		   </configByCorp:fieldVisibility>
		if((str=="1") && (elementNameT == 'loadA' || elementNameT == 'packA')){
			calculateRddDays(elementNameT);
		}
	}
	setOnSelectBasedMethods(["calcDays(),calcDelDate(),calcDayOri(),calcRddDate()"]);
	setCalendarFunctionality();
	
	function winOpenOrigin(){
		 <c:if test="${isNetworkBookingAgent==true || networkAgent}">
		 var savedOriginAgentExSO='${trackingStatus.originAgentExSO}';
		 var originAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value;

			if(originAgentExSo !='' && savedOriginAgentExSO == originAgentExSo){
				agree= confirm("Changing the agent will remove the current RedSky Network partner link");
				if(agree){
					vendorChangeStatus='YES';
					<configByCorp:fieldVisibility componentId="component.field.vanline">
					if('${serviceOrder.job}' =='DOM' || '${serviceOrder.job}' =='UVL' || '${serviceOrder.job}'=='MVL'){
				 		openWindow('originPartnersVanline.html?flag=6&origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=trackingStatus.originAgentVanlineCode&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode',1000,550);	
				    }else{
				    	<c:if test="${agentClassificationShow=='Y'}">
				        	openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode'); 
				        </c:if>
				        <c:if test="${agentClassificationShow=='N'}">
				        	openWindow('originPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode'); 
				        </c:if>
				    	
				    } 
				    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
			    </configByCorp:fieldVisibility>
			    <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
			    	<c:if test="${agentClassificationShow=='Y'}">
			    		openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode'); 
			        </c:if>
			        <c:if test="${agentClassificationShow=='N'}">
			        	openWindow('originPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode'); 
			        </c:if>
			    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
			    </configByCorp:fieldVisibility>
				} 
			}else{
				searchFlag='TRUE';
				<configByCorp:fieldVisibility componentId="component.field.vanline">
				if('${serviceOrder.job}' =='DOM' || '${serviceOrder.job}' =='UVL' || '${serviceOrder.job}'=='MVL'){
			 		openWindow('originPartnersVanline.html?flag=6&origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=trackingStatus.originAgentVanlineCode&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode',1000,550);	
			    }else{
			    	<c:if test="${agentClassificationShow=='Y'}">
			    		openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode'); 
			        </c:if>
			        <c:if test="${agentClassificationShow=='N'}">
			        	openWindow('originPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode'); 
			        </c:if>
			    		
			    } 
			    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
		    </configByCorp:fieldVisibility>
		    <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
		    <c:if test="${agentClassificationShow=='Y'}">
		    	openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode'); 
	        </c:if>
	        <c:if test="${agentClassificationShow=='N'}">
	        	openWindow('originPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode'); 
	        </c:if>
		    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
		    </configByCorp:fieldVisibility>
			}
		</c:if>
		 <c:if test="${isNetworkBookingAgent!=true &&  !networkAgent}">
		<configByCorp:fieldVisibility componentId="component.field.vanline">
			if('${serviceOrder.job}' =='DOM' || '${serviceOrder.job}' =='UVL' || '${serviceOrder.job}'=='MVL'){
		 		openWindow('originPartnersVanline.html?flag=6&origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=trackingStatus.originAgentVanlineCode&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode',1000,550);	
		    }else{
		    	<c:if test="${agentClassificationShow=='Y'}">
		    		openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode'); 
		        </c:if>
		        <c:if test="${agentClassificationShow=='N'}">
		        	openWindow('originPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode'); 
		        </c:if>
		    		
		    } 
		    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
	   </configByCorp:fieldVisibility>
	   <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
	   <c:if test="${agentClassificationShow=='Y'}">
	   		openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode'); 
	   </c:if>
	   <c:if test="${agentClassificationShow=='N'}">
	   		openWindow('originPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode'); 
	   </c:if>
	   document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
	   </configByCorp:fieldVisibility>
	   </c:if>
	}

	function winOpenDest(){
		 <c:if test="${isNetworkBookingAgent==true || networkAgent}">
		 var savedDestinationAgentExSo='${trackingStatus.destinationAgentExSO}';
		 var destinationAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value;
			if(destinationAgentExSo!='' && destinationAgentExSo == savedDestinationAgentExSo){
				agree= confirm("Changing the agent will remove the current RedSky Network partner link");
				if(agree){
					<configByCorp:fieldVisibility componentId="component.field.vanline">
					 if('${serviceOrder.job}' =='DOM' || '${serviceOrder.job}' =='UVL' || '${serviceOrder.job}' == 'MVL'){
						openWindow('destinationPartnersVanline.html?flag=7&destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=trackingStatus.destinationAgentVanlineCode&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode',1000,550);
				     }else{
				    	<c:if test="${agentClassificationShow=='Y'}"> 
				    		openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode');
				    	</c:if>
				    	<c:if test="${agentClassificationShow=='N'}"> 
				    		openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode');
				    	</c:if>
				     }
				     document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
			     </configByCorp:fieldVisibility>
			   <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
			   	<c:if test="${agentClassificationShow=='Y'}"> 
			   		openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode');
		    	</c:if>
		    	<c:if test="${agentClassificationShow=='N'}"> 
		    		openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode');
		    	</c:if>	   
			   </configByCorp:fieldVisibility>
				}
			}else{
				searchFlag='TRUE';
				<configByCorp:fieldVisibility componentId="component.field.vanline">
				 if('${serviceOrder.job}' =='DOM' || '${serviceOrder.job}' =='UVL' || '${serviceOrder.job}' == 'MVL'){
					openWindow('destinationPartnersVanline.html?flag=7&destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=trackingStatus.destinationAgentVanlineCode&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode',1000,550);
			     }else{
			    	<c:if test="${agentClassificationShow=='Y'}"> 
			    		openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode');
			    	</c:if>
			    	<c:if test="${agentClassificationShow=='N'}"> 
			    		openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode');
			    	</c:if>
			     }
			     document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
		     </configByCorp:fieldVisibility>
		   <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
		   	<c:if test="${agentClassificationShow=='Y'}"> 
		   		openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode');
	    	</c:if>
	    	<c:if test="${agentClassificationShow=='N'}"> 
	    		openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode');
	    	</c:if>
		   </configByCorp:fieldVisibility>
			}
	   </c:if>
	   <c:if test="${isNetworkBookingAgent==false && !networkAgent}">
						<configByCorp:fieldVisibility componentId="component.field.vanline">
					 if('${serviceOrder.job}' =='DOM' || '${serviceOrder.job}' =='UVL' || '${serviceOrder.job}' == 'MVL'){
						openWindow('destinationPartnersVanline.html?flag=7&destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=trackingStatus.destinationAgentVanlineCode&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode',1000,550);
				     }else{
				    	<c:if test="${agentClassificationShow=='Y'}"> 
				    		openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode');
			  	    	</c:if>
			  	    	<c:if test="${agentClassificationShow=='N'}"> 
			  	    		openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode');
			  	    	</c:if>
				     }
				 document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
			     </configByCorp:fieldVisibility>
			   <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
		    	<c:if test="${agentClassificationShow=='Y'}"> 
		    		openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode');
	  	    	</c:if>
	  	    	<c:if test="${agentClassificationShow=='N'}"> 
	  	    		openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode');
	  	    	</c:if>
			   </configByCorp:fieldVisibility>		
	 </c:if> 
	}
	function winOpenDestSub(){
			<c:if test="${isNetworkBookingAgent==true || isNetworkDestinationAgent==true  || networkAgent}">
			var savedDestinationSubAgentExSO='${trackingStatus.destinationSubAgentExSO}';
			 var destinationSubAgentExSO=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value;
				if(destinationSubAgentExSO!='' && destinationSubAgentExSO == savedDestinationSubAgentExSO){
					agree= confirm("Changing the agent will remove the current RedSky Network partner link");
					if(agree){
						<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
						<c:if test="${agentClassificationShow=='Y'}"> 
			    			openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subDestinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationSubAgent&fld_code=trackingStatus.destinationSubAgentCode');
			  	    	</c:if>
			  	    	<c:if test="${agentClassificationShow=='N'}"> 
			  	    		openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subDestinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationSubAgent&fld_code=trackingStatus.destinationSubAgentCode');
			  	    	</c:if>
						document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
					 </sec-auth:authComponent>
						}
				}else{
					searchFlag='TRUE';
					<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
					<c:if test="${agentClassificationShow=='Y'}"> 
	    				openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subDestinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationSubAgent&fld_code=trackingStatus.destinationSubAgentCode');
		  	    	</c:if>
		  	    	<c:if test="${agentClassificationShow=='N'}"> 
		  	    		openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subDestinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationSubAgent&fld_code=trackingStatus.destinationSubAgentCode');
		  	    	</c:if>
					document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
				 </sec-auth:authComponent>
					}
			</c:if>	
			<c:if test="${isNetworkBookingAgent==false && isNetworkDestinationAgent!=true  && !networkAgent}">
			<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
				<c:if test="${agentClassificationShow=='Y'}"> 
					openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subDestinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationSubAgent&fld_code=trackingStatus.destinationSubAgentCode');
  	    		</c:if>
  	    		<c:if test="${agentClassificationShow=='N'}"> 
  	    			openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subDestinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationSubAgent&fld_code=trackingStatus.destinationSubAgentCode');
  	    		</c:if>
				document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
			 </sec-auth:authComponent>
			 </c:if>
		  }
	function winOpenOriginSub(){
			<c:if test="${isNetworkBookingAgent==true || isNetworkOriginAgent==true  || networkAgent}">
			var savedOriginSubAgentExSo='${trackingStatus.originSubAgentExSO}';
			 var originSubAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value;
				if(originSubAgentExSo!='' && originSubAgentExSo == savedOriginSubAgentExSo){
					agree= confirm("Changing the agent will remove the current RedSky Network partner link");
					if(agree){
						<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
					 	<c:if test="${agentClassificationShow=='Y'}">
				   			openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subOriginAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originSubAgent&fld_code=trackingStatus.originSubAgentCode'); 
					   	</c:if>
					   	<c:if test="${agentClassificationShow=='N'}">
					   		openWindow('originPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subOriginAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originSubAgent&fld_code=trackingStatus.originSubAgentCode'); 
					   	</c:if>
					 	document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
					  </sec-auth:authComponent>
					}
				}else{
					searchFlag='TRUE';
					<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
					<c:if test="${agentClassificationShow=='Y'}">
		   				openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subOriginAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originSubAgent&fld_code=trackingStatus.originSubAgentCode'); 
				   	</c:if>
				   	<c:if test="${agentClassificationShow=='N'}">
				   		openWindow('originPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subOriginAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originSubAgent&fld_code=trackingStatus.originSubAgentCode'); 
				   	</c:if>	
					document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
					</sec-auth:authComponent>
				}
		</c:if>
		<c:if test="${isNetworkBookingAgent==false && isNetworkOriginAgent!=true  && !networkAgent}">
		<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
			<c:if test="${agentClassificationShow=='Y'}">
				openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subOriginAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originSubAgent&fld_code=trackingStatus.originSubAgentCode'); 
   			</c:if>
		   	<c:if test="${agentClassificationShow=='N'}">
		   		openWindow('originPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subOriginAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originSubAgent&fld_code=trackingStatus.originSubAgentCode'); 
		   	</c:if>	
		  document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
		  </sec-auth:authComponent>
		  </c:if>
		}
</script>
<script type="text/JavaScript">
try{
	var disableChk = '${disableChk}';
	 if(disableChk == 'N') {		
		document.getElementById('weightnVolumeRadio').disabled = true;	
		document.getElementById('weightnVolumeRadio2').disabled = true;	  		
	 }
}catch(e){}
try{
	try{		
		<sec-auth:authComponent componentId="module.button.trackingStatus.showForStorage">  
		storageDisplay();
	    </sec-auth:authComponent>
	}catch(e){}
	try{
	<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
	var temp=document.forms['trackingStatusForm'].elements['serviceOrder.mode'].value;
	if(((temp=='Air') || (temp=='Sea'))){
		document.getElementById('chkflag').style.display='inline';	
	}else{
		if(document.forms['trackingStatusForm'].elements['trackingStatus.flagCarrier'].value ==null || document.forms['trackingStatusForm'].elements['trackingStatus.flagCarrier'].value==''){
			document.forms['trackingStatusForm'].elements['trackingStatus.flagCarrier'].value='';
			document.getElementById('chkflag').style.display='none'		
		} }
	</configByCorp:fieldVisibility>
	}catch(e){ }
}catch(e){} 
if('${isAgentPortal}'=='AG' && '${isRedSky}'!='RUC'){	
	document.getElementById("hidActiveAP").style.display="block";
	document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContactImgFlag'].value='true';	
}else{
	document.getElementById("hidActiveAP").style.display="none";
	document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContactImgFlag'].value='';		
} 
if('${isDestinAgentPortal}'=='AG' && '${isRedSkyDestinAg}'!='RUC'){		
	document.getElementById("hidDestinActiveAP").style.display="block";	
	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContactImgFlag'].value='true';	
}else{
	document.getElementById("hidDestinActiveAP").style.display="none";	
	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContactImgFlag'].value='';	
}
if('${isSubOrigAgentPortal}'=='AG' && '${isRedSkySubOrigAg}'!='RUC'){	
	document.getElementById("hidSubOrigActiveAP").style.display="block";
	document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContactImgFlag'].value='true';		
}else{
	document.getElementById("hidSubOrigActiveAP").style.display="none";
	document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContactImgFlag'].value='';	
}
if('${isSubDestinAgentPortal}'=='AG'   && '${isRedSkySubDestinAg}'!='RUC'){	
	document.getElementById("hidSubDestinActiveAP").style.display="block";
	document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentImgFlag'].value='true';		
}else{
	document.getElementById("hidSubDestinActiveAP").style.display="none";	
	document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentImgFlag'].value='';		
}
function isSoExtFlag(){
  document.forms['trackingStatusForm'].elements['isSOExtract'].value="yes";
}
function isUpdaterFlag(){
	  document.forms['trackingStatusForm'].elements['updaterIntegration'].value="yes";
	}try{
	<c:if test="${ usertype=='USER'}">
	var  permissionTest  = "";
	<sec-auth:authComponent componentId="module.script.form.corpSalesScript">
	permissionTest="14";
	</sec-auth:authComponent>
	if(permissionTest==''){
	window.onload =function ModifyPlaceHolder () {		
        var input = document.getElementById ("trackingOriginAgentNameId");
	     input.placeholder = "Origin Agent Search";
	     var input1 = document.getElementById ("networkPartnerNameId");
	     input1.placeholder = "Network Agent Search";
	     var input2 = document.getElementById ("trackingDestinationAgentNameId");
	     input2.placeholder = "Destination Agent Search";
	     var input3 = document.getElementById ("trackingOriginSubAgentNameId");
	     input3.placeholder = "Sub Origin Agent Search";
	     var input4 = document.getElementById ("trackingDestinationSubAgentNameId");
	     input4.placeholder = "Sub Destination Agent Search";
	     var input5 = document.getElementById ("trackingBrokerNameId");
	     input5.placeholder = "Broker Search";
	     var input6 = document.getElementById ("trackingForwarderId");
	     input6.placeholder = "Forwarder Search";
		}
    }
	</c:if>
}catch(e){} 
function updateAllAgentDetails(){
	var type="Please confirm that you want to update all agent roles with role information from the current service order.";
	var agree = confirm(type+"\n"+"Click OK to proceed or cancel to avoid updating.");
	if(agree){
	var soId =document.forms['trackingStatusForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['trackingStatusForm'].elements['customerFile.id'].value;
	var url="updateAgentDetails.html?ajax=1&decorator=simple&popup=true&seqNm="+encodeURI(seqNm)+"&soId="+encodeURI(soId);
	 http238.open("GET", url, true); 
	 http238.onreadystatechange = handleHttpResponse238; 
	 http238.send(null);
	}else{
		return false;
	}
}
function handleHttpResponse238(){
	if (http238.readyState == 4){		
            var results = http238.responseText
            results = results.trim();                                
	}
}
var http238 = getHTTPObject();
function onlyFloatNumsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==35) || (keyCode==36); 
	}
	function validateListForOriginAgentClassified(){
		<configByCorp:fieldVisibility componentId="component.field.trackingstatus.classifiedAgent">
			checkVendorName();
		</configByCorp:fieldVisibility>	
	}
	function validateListForDestinAgentClassified(){
		<configByCorp:fieldVisibility componentId="component.field.trackingstatus.classifiedAgent">
		checkVendorNameDest();
		</configByCorp:fieldVisibility>
	}
	function validateListForSubOaAgClassified(){
		<configByCorp:fieldVisibility componentId="component.field.trackingstatus.classifiedAgent">
		checkVendorNameSub();
		</configByCorp:fieldVisibility>
	}
	function validateListForSubDaAgClassified(){
		<configByCorp:fieldVisibility componentId="component.field.trackingstatus.classifiedAgent">
		checkVendorNameDestSub();
		</configByCorp:fieldVisibility>
	}
	function winOpenDestNew(){
		 <c:if test="${isNetworkBookingAgent==true || networkAgent}">
		 var savedDestinationAgentExSo='${trackingStatus.destinationAgentExSO}';
		 var destinationAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value;
			if(destinationAgentExSo!='' && destinationAgentExSo == savedDestinationAgentExSo){
				agree= confirm("Changing the agent will remove the current RedSky Network partner link");
				if(agree){
					<configByCorp:fieldVisibility componentId="component.field.vanline">
					 if('${serviceOrder.job}' =='DOM' || '${serviceOrder.job}' =='UVL' || '${serviceOrder.job}' == 'MVL'){
						openWindow('destinationPartnersVanlineNew.html?flag=7&destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=trackingStatus.destinationAgentVanlineCode&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode&billToCode=${billing.billToCode}',1000,550);
				     }else{
				    	openWindow('destinationPartnersNew.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode&billToCode=${billing.billToCode}');
				     }
				     document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
			     </configByCorp:fieldVisibility>
			   <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
			       	openWindow('destinationPartnersNew.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode&billToCode=${billing.billToCode}');
			   </configByCorp:fieldVisibility>
				}
			}else{
				searchFlag='TRUE';
				<configByCorp:fieldVisibility componentId="component.field.vanline">
				 if('${serviceOrder.job}' =='DOM' || '${serviceOrder.job}' =='UVL' || '${serviceOrder.job}' == 'MVL'){
					openWindow('destinationPartnersVanlineNew.html?flag=7&destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=trackingStatus.destinationAgentVanlineCode&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode&billToCode=${billing.billToCode}',1000,550);
			     }else{
			    	openWindow('destinationPartnersNew.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode&billToCode=${billing.billToCode}');
			     }
			     document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
		     </configByCorp:fieldVisibility>
		   <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
		       	openWindow('destinationPartnersNew.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode&billToCode=${billing.billToCode}');
		   </configByCorp:fieldVisibility>
			}
	   </c:if>
	   <c:if test="${isNetworkBookingAgent==false && !networkAgent}">
						<configByCorp:fieldVisibility componentId="component.field.vanline">
					 if('${serviceOrder.job}' =='DOM' || '${serviceOrder.job}' =='UVL' || '${serviceOrder.job}' == 'MVL'){
						openWindow('destinationPartnersVanlineNew.html?flag=7&destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=trackingStatus.destinationAgentVanlineCode&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode&billToCode=${billing.billToCode}',1000,550);
				     }else{
				    	openWindow('destinationPartnersNew.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode&billToCode=${billing.billToCode}');
				     }
				     document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
			     </configByCorp:fieldVisibility>
			   <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
			       	openWindow('destinationPartnersNew.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.destinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationAgent&fld_code=trackingStatus.destinationAgentCode&billToCode=${billing.billToCode}');
			   </configByCorp:fieldVisibility>		
	 </c:if> 
	}
	function winOpenOriginSubNew(){
		<c:if test="${isNetworkBookingAgent==true || isNetworkOriginAgent==true  || networkAgent}">
		var savedOriginSubAgentExSo='${trackingStatus.originSubAgentExSO}';
		 var originSubAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value;
			if(originSubAgentExSo!='' && originSubAgentExSo == savedOriginSubAgentExSo){
				agree= confirm("Changing the agent will remove the current RedSky Network partner link");
				if(agree){
					<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
				 	openWindow('originPartnersNew.html?subOrigin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subOriginAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originSubAgent&fld_code=trackingStatus.originSubAgentCode&billToCode=${billing.billToCode}');	
				  	document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
				  </sec-auth:authComponent>
				}
			}else{
				searchFlag='TRUE';
				<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
			 	openWindow('originPartnersNew.html?subOrigin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subOriginAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originSubAgent&fld_code=trackingStatus.originSubAgentCode&billToCode=${billing.billToCode}');	
			  	document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
			  </sec-auth:authComponent>
			}
	</c:if>
	<c:if test="${isNetworkBookingAgent==false && isNetworkOriginAgent!=true  && !networkAgent}">
	<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
	 	openWindow('originPartnersNew.html?subOrigin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subOriginAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.originSubAgent&fld_code=trackingStatus.originSubAgentCode&billToCode=${billing.billToCode}');	
	  	document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
	  </sec-auth:authComponent>
	  </c:if>
	}

	function winOpenDestSubNew(){
		<c:if test="${isNetworkBookingAgent==true || isNetworkDestinationAgent==true  || networkAgent}">
		var savedDestinationSubAgentExSO='${trackingStatus.destinationSubAgentExSO}';
		 var destinationSubAgentExSO=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value;
			if(destinationSubAgentExSO!='' && destinationSubAgentExSO == savedDestinationSubAgentExSO){
				agree= confirm("Changing the agent will remove the current RedSky Network partner link");
				if(agree){
					<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
					openWindow('destinationPartnersNew.html?subDestin=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subDestinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationSubAgent&fld_code=trackingStatus.destinationSubAgentCode&billToCode=${billing.billToCode}');
					document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
				 </sec-auth:authComponent>
					}
			}else{
				searchFlag='TRUE';
				<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
				openWindow('destinationPartnersNew.html?subDestin=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subDestinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationSubAgent&fld_code=trackingStatus.destinationSubAgentCode&billToCode=${billing.billToCode}');
				document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
			 </sec-auth:authComponent>
				}
		</c:if>	
		<c:if test="${isNetworkBookingAgent==false && isNetworkDestinationAgent!=true  && !networkAgent}">
		<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
			openWindow('destinationPartnersNew.html?subDestin=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.subDestinationAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.destinationSubAgent&fld_code=trackingStatus.destinationSubAgentCode&billToCode=${billing.billToCode}');
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
		 </sec-auth:authComponent>
		 </c:if>
	  }
	var http2Origin = getHTTPObject();
	var http2Destin = getHTTPObject();
	var http2SubOA = getHTTPObject();
	var http2SubDA = getHTTPObject();
	function validPreferredAgentOrigin(){
	    var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
	    if(vendorId == ''){
	    	document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value='';
	    }
	    if(vendorId!=''){ 
		    var url="checkPreferredAgentCode.html?ajax=1&decorator=simple&popup=true&billToCodeAcc=${billing.billToCode}&partnerCode=" + encodeURI(vendorId);
		    http2Origin.open("GET", url, true);
		    http2Origin.onreadystatechange = handleHttpResponse831;
		    http2Origin.send(null);
	    }
	} 
	
	function handleHttpResponse831(){
		if (http2Origin.readyState == 4){
	                var results = http2Origin.responseText
	                results = results.trim();
	                var res = results.split("#");
	                if(res.length>1){
	                	if(res[2] == 'Y'){
		           				document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
		           		}else{
		           			/* alert("Selected Origin Agent is not part of Preferred  agent list. Kindly select another agent." ); 
		           			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].value ='';
		           			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value ='';
						    document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value="";
						    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value="";
					 	    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select(); */
		           		}
	               	}else{
	                     alert("Origin Agent code not valid" );
	                     document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].value ='';
	                     document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value ='';
	                 	 document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].value="";
	                 	 document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value="";
					 	 document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
				   }    } }
	function validPreferredAgentDestin(){
	    var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
	    if(vendorId == ''){
	    	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value='';
	    }
	    if(vendorId!=''){ 
		    var url="checkPreferredAgentCode.html?ajax=1&decorator=simple&popup=true&billToCodeAcc=${billing.billToCode}&partnerCode=" + encodeURI(vendorId);
		    http2Destin.open("GET", url, true);
		    http2Destin.onreadystatechange = handleHttpResponsedestin;
		    http2Destin.send(null);
	    }	} 
	function handleHttpResponsedestin(){
		if (http2Destin.readyState == 4){
	                var results = http2Destin.responseText
	                results = results.trim();
	                var res = results.split("#");
	                if(res.length>1){
	                	if(res[2] == 'Y'){
		           				document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
	                		
		           		}else{
		           			/* alert("Selected Destination Agent is not part of Preferred  agent list. Kindly select another agent." ); 
		           			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="";
						    document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value="";
					 		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select(); */
		           		}
	               	}else{
	                     alert("Destination Agent code not valid" );
	                     document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].value="";
						    document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value="";
					 		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
				   }    } }
	function validPreferredAgentSubOA(){
	    var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
	    if(vendorId == ''){
	    	document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value='';
	    }
	    if(vendorId!=''){ 
		    var url="checkPreferredAgentCode.html?ajax=1&decorator=simple&popup=true&billToCodeAcc=${billing.billToCode}&partnerCode=" + encodeURI(vendorId);
		    http2SubOA.open("GET", url, true);
		    http2SubOA.onreadystatechange = handleHttpResponseSubOA;
		    http2SubOA.send(null);
	    }	} 
	function handleHttpResponseSubOA(){
		if (http2SubOA.readyState == 4){
	                var results = http2SubOA.responseText
	                results = results.trim();
	                var res = results.split("#");
	                if(res.length>1){
	                	if(res[2] == 'Y'){
		           				document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
	                		
		           		}else{
		           			/* alert("Selected Sub Origin Agent is not part of Preferred  agent list. Kindly select another agent." ); 
		           			document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value="";
						    document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value="";
						    document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select(); */

		           		}
	               	}else{
	               		alert("Sub Origin Agent code is not valid." ); 
	                     document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgent'].value="";
						    document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value="";
						    document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();

				   }    } }
	
	try{
		var msgClicked='No';
			<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
			if('${serviceOrder.job}' =='INT'){
			var checkMsgClickedValue = "<%=request.getParameter("msgClicked") %>";
			var checkMsgClickedDBValue = "${msgClicked}";
			if((checkMsgClickedValue=='No' || checkMsgClickedValue==null || checkMsgClickedValue=='null' || checkMsgClickedValue=='') || (checkMsgClickedDBValue=='No' || checkMsgClickedDBValue==null || checkMsgClickedDBValue=='null' || checkMsgClickedDBValue=='')){
				var hasValuation = '${billing.insuranceHas}';
				if(hasValuation==null || hasValuation==''){
					var soCreatedOn = '${serviceOrder.createdOn}';
					var todayDate = new Date();
					if(soCreatedOn!=null && soCreatedOn!=''){
					   var mySplitResult = soCreatedOn.split("-");
					   var day = mySplitResult[2];
					   day = day.substring(0,2)
					   var month = mySplitResult[1];
					   var year = mySplitResult[0];
					   if(month == 'Jan'){month = "01";
					   }else if(month == 'Feb'){month = "02";
					   }else if(month == 'Mar'){month = "03";
					   }else if(month == 'Apr'){month = "04";
					   }else if(month == 'May'){month = "05";
					   }else if(month == 'Jun'){month = "06";
					   }else if(month == 'Jul'){month = "07";
					   }else if(month == 'Aug'){month = "08";
					   }else if(month == 'Sep'){month = "09";
					   }else if(month == 'Oct'){month = "10";
					   }else if(month == 'Nov'){month = "11";
					   }else if(month == 'Dec'){month = "12";
					   }
					   var finalDate = month+"-"+day+"-"+year;
					   date1 = finalDate.split("-");
					   var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
					   var daysApart = Math.round((todayDate-sDate)/86400000);
					   if(daysApart > 7){
				  			alert('Enter insurance requirements');
				  			msgClicked='Yes';
					   }
					}
				}
				var certificateNo = '${billing.certnum}';
				if((hasValuation=='Y') && (certificateNo==null || certificateNo=='')){
					var tsTargetPackDate = '${trackingStatus.beginPacking}';
					var tsTargetLoadDate = '${trackingStatus.beginLoad}';
					var soCreatedOn = '${serviceOrder.createdOn}';
					var soDate = null;
					var tsPackDate = null;
					var tsLoadDate = null;
					
					if(tsTargetPackDate!=null && tsTargetPackDate!=''){
						   var mySplitResult = tsTargetPackDate.split("-");
						   var day = mySplitResult[2];
						   day = day.substring(0,2)
						   var month = mySplitResult[1];
						   var year = mySplitResult[0];
						   if(month == 'Jan'){month = "01";
						   }else if(month == 'Feb'){month = "02";
						   }else if(month == 'Mar'){month = "03";
						   }else if(month == 'Apr'){month = "04";
						   }else if(month == 'May'){month = "05";
						   }else if(month == 'Jun'){month = "06";
						   }else if(month == 'Jul'){month = "07";
						   }else if(month == 'Aug'){month = "08";
						   }else if(month == 'Sep'){month = "09";
						   }else if(month == 'Oct'){month = "10";
						   }else if(month == 'Nov'){month = "11";
						   }else if(month == 'Dec'){month = "12";
						   }
						   var finalDate = month+"-"+day+"-"+year;
						   var date1 = finalDate.split("-");
						   tsPackDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
						   
					}else if(tsTargetLoadDate!=null && tsTargetLoadDate!=''){
						   var mySplitResult = tsTargetLoadDate.split("-");
						   var day = mySplitResult[2];
						   day = day.substring(0,2)
						   var month = mySplitResult[1];
						   var year = mySplitResult[0];
						   if(month == 'Jan'){month = "01";
						   }else if(month == 'Feb'){month = "02";
						   }else if(month == 'Mar'){month = "03";
						   }else if(month == 'Apr'){month = "04";
						   }else if(month == 'May'){month = "05";
						   }else if(month == 'Jun'){month = "06";
						   }else if(month == 'Jul'){month = "07";
						   }else if(month == 'Aug'){month = "08";
						   }else if(month == 'Sep'){month = "09";
						   }else if(month == 'Oct'){month = "10";
						   }else if(month == 'Nov'){month = "11";
						   }else if(month == 'Dec'){month = "12";
						   }
						   var finalDate = month+"-"+day+"-"+year;
						   var date1 = finalDate.split("-");
						   tsLoadDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
					}
					if(soCreatedOn!=null && soCreatedOn!=''){
						   var mySplitResult = soCreatedOn.split("-");
						   var day = mySplitResult[2];
						   day = day.substring(0,2)
						   var month = mySplitResult[1];
						   var year = mySplitResult[0];
						   if(month == 'Jan'){month = "01";
						   }else if(month == 'Feb'){month = "02";
						   }else if(month == 'Mar'){month = "03";
						   }else if(month == 'Apr'){month = "04";
						   }else if(month == 'May'){month = "05";
						   }else if(month == 'Jun'){month = "06";
						   }else if(month == 'Jul'){month = "07";
						   }else if(month == 'Aug'){month = "08";
						   }else if(month == 'Sep'){month = "09";
						   }else if(month == 'Oct'){month = "10";
						   }else if(month == 'Nov'){month = "11";
						   }else if(month == 'Dec'){month = "12";
						   }
						   var finalDate = month+"-"+day+"-"+year;
						   var date1 = finalDate.split("-");
						   soDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
					}
					if((tsPackDate!=null && tsPackDate!='') && (soDate!=null && soDate!='')){
						var daysApart = Math.round((tsPackDate-soDate)/86400000);
						if(daysApart < 7){
				  			alert('Need cert#');
				  			msgClicked='Yes';
					   	}
					}else if((tsLoadDate!=null && tsLoadDate!='') && (soDate!=null && soDate!='')){
						var daysApart = Math.round((tsLoadDate-soDate)/86400000);
						if(daysApart < 7){
				  			alert('Need cert#');
				  			msgClicked='Yes';
					   	}
					}
				}
				checkMsgClickedValue = msgClicked;
				document.forms['trackingStatusForm'].elements['msgClicked'].value = checkMsgClickedValue;
			}
		}
		</configByCorp:fieldVisibility>
	}catch(e){}
	function completecrewArrivalTimeString(txtValue) {
		var stime1 = txtValue.value;
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					txtValue.value = stime1 + ":00";
				}
				if(stime1.length==1){
					txtValue.value = "0" + stime1 + ":00";
				}
			}else{
				txtValue.value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				txtValue.value  = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				txtValue.value  = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				txtValue.value = "0" + stime1;
			}
		}
		
	}
	function IsCrewValidTime(clickType) {
		var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
		var timeStr = clickType.value;
		var matchArray = timeStr.match(timePat);
		if (matchArray == null) {
		alert("Time is not in a valid format. Please use HH:MM format");
		clickType.value='00:00'
		clickType.focus();
		return false;
		}
		hour = matchArray[1];
		minute = matchArray[2];
		second = matchArray[4];
		ampm = matchArray[6];
		if (second=="") { second = null; }
		if (ampm=="") { ampm = null }
		if (hour < 0  || hour > 23) {
		alert("Time must between 0 to 23:59 (Hrs)");
		clickType.value='00:00'
		clickType.focus();
		return false;
		}
		if (minute<0 || minute > 59) {
		alert ("Time must between 0 to 59 (Min)");
		clickType.value='00:00'
		clickType.focus();
		return false;
		}
		if (second != null && (second < 0 || second > 59)) {
		alert ("Time must between 0 to 59 (Sec)");
		clickType.value='00:00'
		clickType.focus();
		return false;
		}
	}

	
</script>
