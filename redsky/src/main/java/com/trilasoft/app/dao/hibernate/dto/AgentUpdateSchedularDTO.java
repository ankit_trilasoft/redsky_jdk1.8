package com.trilasoft.app.dao.hibernate.dto;

public class AgentUpdateSchedularDTO {
	private String partnerCode;
	private String lastName;
	private String updatedOn;
	private String updatedBy;
	
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Override
	public String toString() {
		return  partnerCode + "seprator" + lastName + "seprator" + updatedOn + "seprator" + updatedBy;
	}
	
	
}
