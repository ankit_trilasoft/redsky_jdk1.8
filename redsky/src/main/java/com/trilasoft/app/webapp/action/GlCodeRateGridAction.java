package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.webapp.action.BaseAction;
import org.appfuse.model.User;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.GlCodeRateGrid;
import com.trilasoft.app.model.TableCatalog;
import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CostElementManager;
import com.trilasoft.app.service.GlCodeRateGridManager;
import com.trilasoft.app.service.RefMasterManager;

public class GlCodeRateGridAction extends BaseAction implements Preparable {
	private Long id;
	private String sessionCorpID;
	private GlCodeRateGrid glCodeRateGrid;
	private List glcodes;
	private GlCodeRateGridManager glCodeRateGridManager;
	private CompanyDivisionManager companyDivisionManager;
	private List glCodeRateGridList;
	 private Map<String, String> job;
	 private Map<String, String> routing;
	 private List companyDivis = new ArrayList(); 
	private RefMasterManager refMasterManager;
	private String jobType; 
	private String routing1;
	private String recGl;
	private String payGl;
	private String costId;
	private String costEle;
	 
	public void prepare() throws Exception { 
		companyDivis=companyDivisionManager.findCompanyCodeList(sessionCorpID);
	}
	public GlCodeRateGridAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}	
	public String list(){
		job = refMasterManager.findByParameter(sessionCorpID, "JOB");
		routing = refMasterManager.findByParameter(sessionCorpID, "ROUTING");
		glcodes=refMasterManager.findOnlyCode("GLCODES", sessionCorpID);
		if(jobType==null)		{
			jobType="";
		}
		if(routing1==null)		{
			routing1="";
		}
		if(recGl==null)		{
			recGl="";
		}
		if(payGl==null)		{
			payGl="";
		}
		if((costId==null)||(costId.equals(""))){
			costId="0";
		}
		glCodeRateGridList=glCodeRateGridManager.getAllRateGridList(sessionCorpID,costId,jobType,routing1,recGl,payGl);
		return SUCCESS;
	}
	private String oldJobType;
	private String oldRouting;
	private String oldRecGl;
	private String oldPayGl;
	public String edit() { 
		job = refMasterManager.findByParameter(sessionCorpID, "JOB");
		routing = refMasterManager.findByParameter(sessionCorpID, "ROUTING");
		glcodes=refMasterManager.findOnlyCode("GLCODES", sessionCorpID);
		if (id != null) {
			glCodeRateGrid = glCodeRateGridManager.get(id);
			 oldJobType=glCodeRateGrid.getGridJob();
			 oldRouting=glCodeRateGrid.getGridRouting();
			 oldRecGl=glCodeRateGrid.getGridRecGl();
			 oldPayGl=glCodeRateGrid.getGridPayGl();
		} else {
			glCodeRateGrid = new GlCodeRateGrid(); 
			glCodeRateGrid.setCreatedOn(new Date());
			glCodeRateGrid.setUpdatedOn(new Date());
		}
		return SUCCESS;
	}
	private String hitflag2;
	private String successMsg;
	private String associatedAcc;
	private CostElementManager costElementManager;
	private ChargesManager chargesManager;
		public String save() throws Exception {
			job = refMasterManager.findByParameter(sessionCorpID, "JOB");
			routing = refMasterManager.findByParameter(sessionCorpID, "ROUTING");
			glcodes=refMasterManager.findOnlyCode("GLCODES", sessionCorpID);
			hitflag2="0";
		boolean isNew = (glCodeRateGrid.getId() == null);  
				if (isNew) { 
					glCodeRateGrid.setCreatedOn(new Date());
					glCodeRateGrid.setCreatedBy(getRequest().getRemoteUser());
		        }
				glCodeRateGrid.setUpdatedOn(new Date());
				glCodeRateGrid.setUpdatedBy(getRequest().getRemoteUser()); 
				glCodeRateGrid.setCorpID(sessionCorpID);
				String flag="0";
				String tempjob=glCodeRateGrid.getGridJob();
				String tempRout=glCodeRateGrid.getGridRouting();
				String tempRec=glCodeRateGrid.getGridRecGl();
				String tempPay=glCodeRateGrid.getGridPayGl();	
				String tempcomDiv=glCodeRateGrid.getCompanyDivision();
				if(((tempjob==null)||(tempjob.equals("")))&&((tempRout==null)||(tempRout.equals("")))&&((tempcomDiv==null)||(tempcomDiv.equals("")))&&((tempRec!=null)&&(!tempRec.equals("")))&&((tempPay!=null)&&(!tempPay.equals("")))){
					flag="1";
				}
				if(((tempjob!=null)&&(!tempjob.equals("")))&&((tempRout!=null)&&(!tempRout.equals("")))&&((tempcomDiv!=null)&&(!tempcomDiv.equals("")))&&((tempRec==null)||(tempRec.equals("")))&&((tempPay==null)||(tempPay.equals("")))){
					flag="1";
				}
				if(((tempjob==null)||(tempjob.equals("")))&&((tempRout==null)||(tempRout.equals("")))&&((tempcomDiv==null)||(tempcomDiv.equals("")))&&((tempRec==null)||(tempRec.equals("")))&&((tempPay!=null)&&(!tempPay.equals("")))){
					flag="1";
				}
				if(((tempjob==null)||(tempjob.equals("")))&&((tempRout==null)||(tempRout.equals("")))&&((tempcomDiv==null)||(tempcomDiv.equals("")))&&((tempRec!=null)&&(!tempRec.equals("")))&&((tempPay==null)||(tempPay.equals("")))){
					flag="1";
				}
				if(flag.equalsIgnoreCase("1"))
				{
					costId=glCodeRateGrid.getCostElementId()+"";
					hitflag2="1";
					return INPUT;
				}else{
					try{
				glCodeRateGrid=glCodeRateGridManager.save(glCodeRateGrid);  
				if((associatedAcc!=null)&&(associatedAcc.equalsIgnoreCase("YES"))){
				try{
						List chargesList = costElementManager.getCharges(costElementManager.get(glCodeRateGrid.getCostElementId()).getCostElement(),sessionCorpID);
						if(!(chargesList.isEmpty()) && chargesList.size()!=0){
							Iterator itr = chargesList.iterator();
							while(itr.hasNext()){
								Charges charges = chargesManager.get(new Long((Long) itr.next()));
								try{
									String recGlVal=(((glCodeRateGrid.getGridRecGl()!=null)&&(!glCodeRateGrid.getGridRecGl().equalsIgnoreCase("")))?glCodeRateGrid.getGridRecGl():costElementManager.get(glCodeRateGrid.getCostElementId()).getRecGl());
									String payGlVal=(((glCodeRateGrid.getGridPayGl()!=null)&&(!glCodeRateGrid.getGridPayGl().equalsIgnoreCase("")))?glCodeRateGrid.getGridPayGl():costElementManager.get(glCodeRateGrid.getCostElementId()).getPayGl());
									if((recGlVal!=null)&&(!recGlVal.equalsIgnoreCase(""))&&(payGlVal!=null)&&(!payGlVal.equalsIgnoreCase(""))){
										int rec=costElementManager.updateAccountlineGLByGLRateGrid(charges.getCharge(),charges.getContract(),sessionCorpID,glCodeRateGrid.getGridJob(),glCodeRateGrid.getGridRouting(),recGlVal,payGlVal,glCodeRateGrid.getCompanyDivision());
										System.out.print(charges.getCharge()+"-->"+rec);
									}
								}catch(Exception e){e.printStackTrace();}
							}
						}
					}catch(Exception e){e.printStackTrace();}
				}
		    	String key = (isNew) ? "Gl Code Rate Grid has been added successfully." : "Gl Code Rate Grid has been updated successfully.";
		    	saveMessage(key);
				return SUCCESS;
					}catch(Exception e){
						e.printStackTrace();
						costId=glCodeRateGrid.getCostElementId()+"";
						glCodeRateGrid.setGridJob(oldJobType);
						glCodeRateGrid.setGridRouting(oldRouting);
						glCodeRateGrid.setGridRecGl(oldRecGl);
						glCodeRateGrid.setGridPayGl(oldPayGl);
						hitflag2="2";
						return INPUT;
					}
				}
	     }
		public String delete()
		{		
			glCodeRateGrid = glCodeRateGridManager.get(id);
			costId=glCodeRateGrid.getCostElementId()+"";
			glCodeRateGridManager.remove(id);  
	    	String key ="Selected Rate Grid Details has been deleted successfully.";
	    	saveMessage(key);
	      return SUCCESS;		    
		}
		public List getGlcodes() {
			return glcodes;
		}
		public void setGlcodes(List glcodes) {
			this.glcodes = glcodes;
		}

		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getSessionCorpID() {
			return sessionCorpID;
		}
		public void setSessionCorpID(String sessionCorpID) {
			this.sessionCorpID = sessionCorpID;
		}
		public GlCodeRateGrid getGlCodeRateGrid() {
			return glCodeRateGrid;
		}
		public void setGlCodeRateGrid(GlCodeRateGrid glCodeRateGrid) {
			this.glCodeRateGrid = glCodeRateGrid;
		}
		public Map<String, String> getJob() {
			return job;
		}
		public void setJob(Map<String, String> job) {
			this.job = job;
		}
		public Map<String, String> getRouting() {
			return routing;
		}
		public void setRouting(Map<String, String> routing) {
			this.routing = routing;
		}
		public void setGlCodeRateGridManager(GlCodeRateGridManager glCodeRateGridManager) {
			this.glCodeRateGridManager = glCodeRateGridManager;
		}
		public String getJobType() {
			return jobType;
		}
		public void setJobType(String jobType) {
			this.jobType = jobType;
		}
		public String getRouting1() {
			return routing1;
		}
		public void setRouting1(String routing1) {
			this.routing1 = routing1;
		}
		public String getRecGl() {
			return recGl;
		}
		public void setRecGl(String recGl) {
			this.recGl = recGl;
		}
		public String getPayGl() {
			return payGl;
		}
		public void setPayGl(String payGl) {
			this.payGl = payGl;
		}
		public List getGlCodeRateGridList() {
			return glCodeRateGridList;
		}
		public void setGlCodeRateGridList(List glCodeRateGridList) {
			this.glCodeRateGridList = glCodeRateGridList;
		}
		public String getCostId() {
			return costId;
		}
		public void setCostId(String costId) {
			this.costId = costId;
		}
		public String getCostEle() {
			return costEle;
		}
		public void setCostEle(String costEle) {
			this.costEle = costEle;
		}
		public void setRefMasterManager(RefMasterManager refMasterManager) {
			this.refMasterManager = refMasterManager;
		}
		public String getHitflag2() {
			return hitflag2;
		}
		public void setHitflag2(String hitflag2) {
			this.hitflag2 = hitflag2;
		}
		public String getSuccessMsg() {
			return successMsg;
		}
		public void setSuccessMsg(String successMsg) {
			this.successMsg = successMsg;
		}
		public String getOldJobType() {
			return oldJobType;
		}
		public void setOldJobType(String oldJobType) {
			this.oldJobType = oldJobType;
		}
		public String getOldRouting() {
			return oldRouting;
		}
		public void setOldRouting(String oldRouting) {
			this.oldRouting = oldRouting;
		}
		public String getOldRecGl() {
			return oldRecGl;
		}
		public void setOldRecGl(String oldRecGl) {
			this.oldRecGl = oldRecGl;
		}
		public String getOldPayGl() {
			return oldPayGl;
		}
		public void setOldPayGl(String oldPayGl) {
			this.oldPayGl = oldPayGl;
		}
		public String getAssociatedAcc() {
			return associatedAcc;
		}
		public void setAssociatedAcc(String associatedAcc) {
			this.associatedAcc = associatedAcc;
		}
		public void setCostElementManager(CostElementManager costElementManager) {
			this.costElementManager = costElementManager;
		}
		public void setChargesManager(ChargesManager chargesManager) {
			this.chargesManager = chargesManager;
		}
		public List getCompanyDivis() {
			return companyDivis;
		}
		public void setCompanyDivis(List companyDivis) {
			this.companyDivis = companyDivis;
		}
		public void setCompanyDivisionManager(
				CompanyDivisionManager companyDivisionManager) {
			this.companyDivisionManager = companyDivisionManager;
		}
		
}
