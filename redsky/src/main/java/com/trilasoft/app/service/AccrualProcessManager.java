package com.trilasoft.app.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.AccountLine;

public interface AccrualProcessManager extends GenericManager<AccountLine, Long> {		
	public List payableReverseAccruedList(Date endDate, String sessionCorpID);
	public List payableNewAccruals(Date endDate, String sessionCorpID);
	public List payableBalance(String sessionCorpID);
	public List receivableReverseAccruedList(Date endDate, String sessionCorpID);
	public List receivableNewAccruals(Date endDate, String sessionCorpID);
	public List receivableBalance(String sessionCorpID);	
}