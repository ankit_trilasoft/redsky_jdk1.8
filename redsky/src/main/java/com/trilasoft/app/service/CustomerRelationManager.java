package com.trilasoft.app.service;


import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CustomerRelation; 
public interface CustomerRelationManager extends GenericManager<CustomerRelation, Long>{

public CustomerRelation getCrmDetails(Long cid,String sessionCorpID);	
}
