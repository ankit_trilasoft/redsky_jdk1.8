<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title><fmt:message key="healthWelfareExtractDetail.title" /></title>
<meta name="heading" content="<fmt:message key='healthWelfareExtractDetail.heading'/>" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="javascript" type="text/javascript">
function fieldValidate(){
 	if(document.forms['healthWelfareExtractForm'].elements['beginDate'].value==''){
 		alert("Please enter the begin date"); 
 		return false;
 	}
 	if(document.forms['healthWelfareExtractForm'].elements['endDate'].value==''){
 		alert("Please enter the end date "); 
 		return false;
 	}
 	else{
 		document.forms['healthWelfareExtractForm'].action = 'healthWelfareExtracts.html';
    	document.forms['healthWelfareExtractForm'].submit();
 		return true;
 	}
} 
       
</script>
</head>
<div id="Layer1" style="width:85%;">
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top:10px;"><span></span></div>
<div class="center-content">
<s:form name="healthWelfareExtractForm" id="healthWelfareExtractForm" action="healthWelfareExtracts" method="post" validate="true">
	<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>

<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>
<table class="" cellspacing="1" cellpadding="3" border="0" style="margin-bottom:2px">

<tr>
<td width="5px"></td>
<td colspan="7" class="bgblue">Health & Welfare Data Extract </td></tr>
			<tr><td height="5px"></td></tr>
  			<tr> 
  				<td></td>
					<td class="listwhitetext" align="right">Begin Date<font color="red" size="2">*</font></td>
					<c:if test="${not empty beginDate}">
						<s:text id="beginDate" name="${FormDateValue}"><s:param name="value" value="beginDate" /></s:text>
						<td width="100px"><s:textfield cssClass="input-text" id="beginDate" name="beginDate" value="%{beginDate}" size="8" maxlength="11" readonly="true" /><img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty beginDate}">
						<td width="100px"><s:textfield cssClass="input-text" id="beginDate" name="beginDate" required="true" size="8" maxlength="11" readonly="true" /><img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
				      	
					<td class="listwhitetext" align="right">End Date<font color="red" size="2">*</font></td>
		 			<c:if test="${not empty endDate}">
						<s:text id="endDate" name="${FormDateValue}"> <s:param name="value" value="endDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="endDate" name="endDate" value="%{endDate}" size="8" maxlength="11" readonly="true" /><img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty endDate}" >
						<td><s:textfield cssClass="input-text" id="endDate" name="endDate" required="true" size="8" maxlength="11" readonly="true" /><img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>	
			</tr> 
			<tr><td width="" height="10px"></td></tr> 
	  		<tr>	
	  			<td width=""></td>			
				<td colspan="4" style="padding-left: 20px"><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" value="Extract" onclick=" return fieldValidate();"/></td>
	    	</tr>	
	    	<tr><td width="" height="10px"></td></tr> 				
	</table>  
</s:form>
</div>
<div class="bottom-header"><span></span></div> 
</div>
</div>
</div>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>