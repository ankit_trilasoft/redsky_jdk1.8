package com.trilasoft.app.model;

import java.util.Date;
import org.appfuse.model.BaseObject;   
import javax.persistence.Column;
import javax.persistence.Entity;   
import javax.persistence.GenerationType;   
import javax.persistence.Id;   
import javax.persistence.GeneratedValue;   
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder; 
@Entity
@Table(name="partnerrates")
public class PartnerRates extends BaseObject{
	private String corpID;
	private Long id;
	private String type;
	private Date effective;
	private Date endDate;
	private BigDecimal l1To999;                                             
	private BigDecimal l1000To1999;       
	private BigDecimal l2000To2999;       
	private BigDecimal l3000To3999;       
	private BigDecimal l4000To4999;      
	private BigDecimal l5000To5999;       
	private BigDecimal l6000To6999;
	
	private BigDecimal l7000To7999;       
	private BigDecimal l8000To8999;       
	private BigDecimal l9000To9999;       
	private BigDecimal l10000To10999;                               
	private BigDecimal l11000To11999;                                
	private BigDecimal l12000To12999;                               
	private BigDecimal l13000To13999;
	
	private BigDecimal l14000To14999;                               
	private BigDecimal l15000To15999;                                
	private BigDecimal l16000To16999;                               
	private BigDecimal l17000To17999;                                
	private BigDecimal l18000To18999;                                
	private BigDecimal l19000To19999;                                
	private BigDecimal l20000;
	
	private BigDecimal v1To999;           
	private BigDecimal v1000To1999;       
	private BigDecimal v2000To2999;       
	private BigDecimal v3000To3999;       
	private BigDecimal v4000To4999;       
	private BigDecimal v5000To5999;       
	private BigDecimal v6000To6999;  
	
	private BigDecimal v7000To7999;       
	private BigDecimal v8000To8999;       
	private BigDecimal v9000To9999;       
	private BigDecimal v10000To10999;                                
	private BigDecimal v11000To11999;                                
	private BigDecimal v12000To12999;                                
	private BigDecimal v13000To13999;
	
	private BigDecimal v14000To14999;                                
	private BigDecimal v15000To15999;                                
	private BigDecimal v16000To16999;                                
	private BigDecimal v17000To17999;                                
	private BigDecimal v18000To18999;                                
	private BigDecimal v19000To19999;                                
	private BigDecimal v20000; 
	
	private BigDecimal c20;             
	private BigDecimal c40;              
	private BigDecimal lclToVan;          
	private BigDecimal aROROToRsd;        
	private BigDecimal aROROToCustomer;       
	private BigDecimal aContainerToRsd;       
	private BigDecimal aContainerToCustomer;       
	private BigDecimal aContainerHHGToRS;                                
	private BigDecimal aContainerHHGToCU;                                
	private BigDecimal lcl499;           
	private BigDecimal lcl500To999;       
	private BigDecimal lcl1000To1499;                                
	private BigDecimal lcl1500To1999;                                
	private BigDecimal lcl2000To2999;                                
	private BigDecimal lcl3000To3999;                                
	private BigDecimal lcl4000;          
	private BigDecimal air499;           
	private BigDecimal air500To999;       
	private BigDecimal air1000To1499;                                
	private BigDecimal air1500To1999;                                
	private BigDecimal air2000To2999;                                
	private BigDecimal air3000To3999;                                
	private BigDecimal air4000;          
	private BigDecimal warehouseRate;          
	private String warehouseUnit;                                           
	private BigDecimal storageRate;         
	private String storageUnit;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String partnerCode;
	private String weightUnit;
	
	private Partner partner;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("type", type).append("effective", effective).append(
				"endDate", endDate).append("l1To999", l1To999).append(
				"l1000To1999", l1000To1999).append("l2000To2999", l2000To2999)
				.append("l3000To3999", l3000To3999).append("l4000To4999",
						l4000To4999).append("l5000To5999", l5000To5999).append(
						"l6000To6999", l6000To6999).append("l7000To7999",
						l7000To7999).append("l8000To8999", l8000To8999).append(
						"l9000To9999", l9000To9999).append("l10000To10999",
						l10000To10999).append("l11000To11999", l11000To11999)
				.append("l12000To12999", l12000To12999).append("l13000To13999",
						l13000To13999).append("l14000To14999", l14000To14999)
				.append("l15000To15999", l15000To15999).append("l16000To16999",
						l16000To16999).append("l17000To17999", l17000To17999)
				.append("l18000To18999", l18000To18999).append("l19000To19999",
						l19000To19999).append("l20000", l20000).append(
						"v1To999", v1To999).append("v1000To1999", v1000To1999)
				.append("v2000To2999", v2000To2999).append("v3000To3999",
						v3000To3999).append("v4000To4999", v4000To4999).append(
						"v5000To5999", v5000To5999).append("v6000To6999",
						v6000To6999).append("v7000To7999", v7000To7999).append(
						"v8000To8999", v8000To8999).append("v9000To9999",
						v9000To9999).append("v10000To10999", v10000To10999)
				.append("v11000To11999", v11000To11999).append("v12000To12999",
						v12000To12999).append("v13000To13999", v13000To13999)
				.append("v14000To14999", v14000To14999).append("v15000To15999",
						v15000To15999).append("v16000To16999", v16000To16999)
				.append("v17000To17999", v17000To17999).append("v18000To18999",
						v18000To18999).append("v19000To19999", v19000To19999)
				.append("v20000", v20000).append("c20", c20).append("c40", c40)
				.append("lclToVan", lclToVan).append("aROROToRsd", aROROToRsd)
				.append("aROROToCustomer", aROROToCustomer).append(
						"aContainerToRsd", aContainerToRsd).append(
						"aContainerToCustomer", aContainerToCustomer).append(
						"aContainerHHGToRS", aContainerHHGToRS).append(
						"aContainerHHGToCU", aContainerHHGToCU).append(
						"lcl499", lcl499).append("lcl500To999", lcl500To999)
				.append("lcl1000To1499", lcl1000To1499).append("lcl1500To1999",
						lcl1500To1999).append("lcl2000To2999", lcl2000To2999)
				.append("lcl3000To3999", lcl3000To3999).append("lcl4000",
						lcl4000).append("air499", air499).append("air500To999",
						air500To999).append("air1000To1499", air1000To1499)
				.append("air1500To1999", air1500To1999).append("air2000To2999",
						air2000To2999).append("air3000To3999", air3000To3999)
				.append("air4000", air4000).append("warehouseRate",
						warehouseRate).append("warehouseUnit", warehouseUnit)
				.append("storageRate", storageRate).append("storageUnit",
						storageUnit).append("createdBy", createdBy).append(
						"createdOn", createdOn).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("partnerCode",
						partnerCode).append("weightUnit", weightUnit)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PartnerRates))
			return false;
		PartnerRates castOther = (PartnerRates) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(type, castOther.type).append(effective,
				castOther.effective).append(endDate, castOther.endDate).append(
				l1To999, castOther.l1To999).append(l1000To1999,
				castOther.l1000To1999).append(l2000To2999,
				castOther.l2000To2999).append(l3000To3999,
				castOther.l3000To3999).append(l4000To4999,
				castOther.l4000To4999).append(l5000To5999,
				castOther.l5000To5999).append(l6000To6999,
				castOther.l6000To6999).append(l7000To7999,
				castOther.l7000To7999).append(l8000To8999,
				castOther.l8000To8999).append(l9000To9999,
				castOther.l9000To9999).append(l10000To10999,
				castOther.l10000To10999).append(l11000To11999,
				castOther.l11000To11999).append(l12000To12999,
				castOther.l12000To12999).append(l13000To13999,
				castOther.l13000To13999).append(l14000To14999,
				castOther.l14000To14999).append(l15000To15999,
				castOther.l15000To15999).append(l16000To16999,
				castOther.l16000To16999).append(l17000To17999,
				castOther.l17000To17999).append(l18000To18999,
				castOther.l18000To18999).append(l19000To19999,
				castOther.l19000To19999).append(l20000, castOther.l20000)
				.append(v1To999, castOther.v1To999).append(v1000To1999,
						castOther.v1000To1999).append(v2000To2999,
						castOther.v2000To2999).append(v3000To3999,
						castOther.v3000To3999).append(v4000To4999,
						castOther.v4000To4999).append(v5000To5999,
						castOther.v5000To5999).append(v6000To6999,
						castOther.v6000To6999).append(v7000To7999,
						castOther.v7000To7999).append(v8000To8999,
						castOther.v8000To8999).append(v9000To9999,
						castOther.v9000To9999).append(v10000To10999,
						castOther.v10000To10999).append(v11000To11999,
						castOther.v11000To11999).append(v12000To12999,
						castOther.v12000To12999).append(v13000To13999,
						castOther.v13000To13999).append(v14000To14999,
						castOther.v14000To14999).append(v15000To15999,
						castOther.v15000To15999).append(v16000To16999,
						castOther.v16000To16999).append(v17000To17999,
						castOther.v17000To17999).append(v18000To18999,
						castOther.v18000To18999).append(v19000To19999,
						castOther.v19000To19999).append(v20000,
						castOther.v20000).append(c20, castOther.c20).append(
						c40, castOther.c40)
				.append(lclToVan, castOther.lclToVan).append(aROROToRsd,
						castOther.aROROToRsd).append(aROROToCustomer,
						castOther.aROROToCustomer).append(aContainerToRsd,
						castOther.aContainerToRsd).append(aContainerToCustomer,
						castOther.aContainerToCustomer).append(
						aContainerHHGToRS, castOther.aContainerHHGToRS).append(
						aContainerHHGToCU, castOther.aContainerHHGToCU).append(
						lcl499, castOther.lcl499).append(lcl500To999,
						castOther.lcl500To999).append(lcl1000To1499,
						castOther.lcl1000To1499).append(lcl1500To1999,
						castOther.lcl1500To1999).append(lcl2000To2999,
						castOther.lcl2000To2999).append(lcl3000To3999,
						castOther.lcl3000To3999).append(lcl4000,
						castOther.lcl4000).append(air499, castOther.air499)
				.append(air500To999, castOther.air500To999).append(
						air1000To1499, castOther.air1000To1499).append(
						air1500To1999, castOther.air1500To1999).append(
						air2000To2999, castOther.air2000To2999).append(
						air3000To3999, castOther.air3000To3999).append(air4000,
						castOther.air4000).append(warehouseRate,
						castOther.warehouseRate).append(warehouseUnit,
						castOther.warehouseUnit).append(storageRate,
						castOther.storageRate).append(storageUnit,
						castOther.storageUnit).append(createdBy,
						castOther.createdBy).append(createdOn,
						castOther.createdOn).append(updatedBy,
						castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(partnerCode,
						castOther.partnerCode).append(weightUnit,
						castOther.weightUnit).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(type)
				.append(effective).append(endDate).append(l1To999).append(
						l1000To1999).append(l2000To2999).append(l3000To3999)
				.append(l4000To4999).append(l5000To5999).append(l6000To6999)
				.append(l7000To7999).append(l8000To8999).append(l9000To9999)
				.append(l10000To10999).append(l11000To11999).append(
						l12000To12999).append(l13000To13999).append(
						l14000To14999).append(l15000To15999).append(
						l16000To16999).append(l17000To17999).append(
						l18000To18999).append(l19000To19999).append(l20000)
				.append(v1To999).append(v1000To1999).append(v2000To2999)
				.append(v3000To3999).append(v4000To4999).append(v5000To5999)
				.append(v6000To6999).append(v7000To7999).append(v8000To8999)
				.append(v9000To9999).append(v10000To10999)
				.append(v11000To11999).append(v12000To12999).append(
						v13000To13999).append(v14000To14999).append(
						v15000To15999).append(v16000To16999).append(
						v17000To17999).append(v18000To18999).append(
						v19000To19999).append(v20000).append(c20).append(c40)
				.append(lclToVan).append(aROROToRsd).append(aROROToCustomer)
				.append(aContainerToRsd).append(aContainerToCustomer).append(
						aContainerHHGToRS).append(aContainerHHGToCU).append(
						lcl499).append(lcl500To999).append(lcl1000To1499)
				.append(lcl1500To1999).append(lcl2000To2999).append(
						lcl3000To3999).append(lcl4000).append(air499).append(
						air500To999).append(air1000To1499)
				.append(air1500To1999).append(air2000To2999).append(
						air3000To3999).append(air4000).append(warehouseRate)
				.append(warehouseUnit).append(storageRate).append(storageUnit)
				.append(createdBy).append(createdOn).append(updatedBy).append(
						updatedOn).append(partnerCode).append(weightUnit)
				.toHashCode();
	}
	@ManyToOne
	@JoinColumn(name="partnerCode", nullable=false, updatable=false,
	referencedColumnName="partnerCode")
	public Partner getPartner() {
	        return partner;
	    }
	    
		 public void setPartner(Partner partner) {
				this.partner = partner;
		} 
	
	@Column(precision=15, scale=5)
	public BigDecimal getAContainerHHGToCU() {
		return aContainerHHGToCU;
	}
	public void setAContainerHHGToCU(BigDecimal containerHHGToCU) {
		aContainerHHGToCU = containerHHGToCU;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getAContainerHHGToRS() {
		return aContainerHHGToRS;
	}
	public void setAContainerHHGToRS(BigDecimal containerHHGToRS) {
		aContainerHHGToRS = containerHHGToRS;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getAContainerToCustomer() {
		return aContainerToCustomer;
	}
	public void setAContainerToCustomer(BigDecimal containerToCustomer) {
		aContainerToCustomer = containerToCustomer;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getAContainerToRsd() {
		return aContainerToRsd;
	}
	public void setAContainerToRsd(BigDecimal containerToRsd) {
		aContainerToRsd = containerToRsd;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getAir1000To1499() {
		return air1000To1499;
	}
	public void setAir1000To1499(BigDecimal air1000To1499) {
		this.air1000To1499 = air1000To1499;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getAir1500To1999() {
		return air1500To1999;
	}
	public void setAir1500To1999(BigDecimal air1500To1999) {
		this.air1500To1999 = air1500To1999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getAir2000To2999() {
		return air2000To2999;
	}
	public void setAir2000To2999(BigDecimal air2000To2999) {
		this.air2000To2999 = air2000To2999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getAir3000To3999() {
		return air3000To3999;
	}
	public void setAir3000To3999(BigDecimal air3000To3999) {
		this.air3000To3999 = air3000To3999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getAir4000() {
		return air4000;
	}
	public void setAir4000(BigDecimal air4000) {
		this.air4000 = air4000;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getAir499() {
		return air499;
	}
	public void setAir499(BigDecimal air499) {
		this.air499 = air499;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getAir500To999() {
		return air500To999;
	}
	public void setAir500To999(BigDecimal air500To999) {
		this.air500To999 = air500To999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getAROROToCustomer() {
		return aROROToCustomer;
	}
	public void setAROROToCustomer(BigDecimal toCustomer) {
		aROROToCustomer = toCustomer;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getAROROToRsd() {
		return aROROToRsd;
	}
	public void setAROROToRsd(BigDecimal toRsd) {
		aROROToRsd = toRsd;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getC20() {
		return c20;
	}
	public void setC20(BigDecimal c20) {
		this.c20 = c20;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getC40() {
		return c40;
	}
	public void setC40(BigDecimal c40) {
		this.c40 = c40;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL10000To10999() {
		return l10000To10999;
	}
	public void setL10000To10999(BigDecimal to10999) {
		l10000To10999 = to10999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL1000To1999() {
		return l1000To1999;
	}
	public void setL1000To1999(BigDecimal to1999) {
		l1000To1999 = to1999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL11000To11999() {
		return l11000To11999;
	}
	public void setL11000To11999(BigDecimal to11999) {
		l11000To11999 = to11999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL12000To12999() {
		return l12000To12999;
	}
	public void setL12000To12999(BigDecimal to12999) {
		l12000To12999 = to12999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL13000To13999() {
		return l13000To13999;
	}
	public void setL13000To13999(BigDecimal to13999) {
		l13000To13999 = to13999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL14000To14999() {
		return l14000To14999;
	}
	public void setL14000To14999(BigDecimal to14999) {
		l14000To14999 = to14999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL15000To15999() {
		return l15000To15999;
	}
	public void setL15000To15999(BigDecimal to15999) {
		l15000To15999 = to15999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL16000To16999() {
		return l16000To16999;
	}
	public void setL16000To16999(BigDecimal to16999) {
		l16000To16999 = to16999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL17000To17999() {
		return l17000To17999;
	}
	public void setL17000To17999(BigDecimal to17999) {
		l17000To17999 = to17999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL18000To18999() {
		return l18000To18999;
	}
	public void setL18000To18999(BigDecimal to18999) {
		l18000To18999 = to18999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL19000To19999() {
		return l19000To19999;
	}
	public void setL19000To19999(BigDecimal to19999) {
		l19000To19999 = to19999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL1To999() {
		return l1To999;
	}
	public void setL1To999(BigDecimal to999) {
		l1To999 = to999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL20000() {
		return l20000;
	}
	public void setL20000(BigDecimal l20000) {
		this.l20000 = l20000;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL2000To2999() {
		return l2000To2999;
	}
	public void setL2000To2999(BigDecimal to2999) {
		l2000To2999 = to2999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL3000To3999() {
		return l3000To3999;
	}
	public void setL3000To3999(BigDecimal to3999) {
		l3000To3999 = to3999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL4000To4999() {
		return l4000To4999;
	}
	public void setL4000To4999(BigDecimal to4999) {
		l4000To4999 = to4999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL5000To5999() {
		return l5000To5999;
	}
	public void setL5000To5999(BigDecimal to5999) {
		l5000To5999 = to5999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL6000To6999() {
		return l6000To6999;
	}
	public void setL6000To6999(BigDecimal to6999) {
		l6000To6999 = to6999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL7000To7999() {
		return l7000To7999;
	}
	public void setL7000To7999(BigDecimal to7999) {
		l7000To7999 = to7999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL8000To8999() {
		return l8000To8999;
	}
	public void setL8000To8999(BigDecimal to8999) {
		l8000To8999 = to8999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getL9000To9999() {
		return l9000To9999;
	}
	public void setL9000To9999(BigDecimal to9999) {
		l9000To9999 = to9999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getLcl1000To1499() {
		return lcl1000To1499;
	}
	public void setLcl1000To1499(BigDecimal lcl1000To1499) {
		this.lcl1000To1499 = lcl1000To1499;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getLcl1500To1999() {
		return lcl1500To1999;
	}
	public void setLcl1500To1999(BigDecimal lcl1500To1999) {
		this.lcl1500To1999 = lcl1500To1999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getLcl2000To2999() {
		return lcl2000To2999;
	}
	public void setLcl2000To2999(BigDecimal lcl2000To2999) {
		this.lcl2000To2999 = lcl2000To2999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getLcl3000To3999() {
		return lcl3000To3999;
	}
	public void setLcl3000To3999(BigDecimal lcl3000To3999) {
		this.lcl3000To3999 = lcl3000To3999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getLcl4000() {
		return lcl4000;
	}
	public void setLcl4000(BigDecimal lcl4000) {
		this.lcl4000 = lcl4000;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getLcl499() {
		return lcl499;
	}
	public void setLcl499(BigDecimal lcl499) {
		this.lcl499 = lcl499;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getLcl500To999() {
		return lcl500To999;
	}
	public void setLcl500To999(BigDecimal lcl500To999) {
		this.lcl500To999 = lcl500To999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getLclToVan() {
		return lclToVan;
	}
	public void setLclToVan(BigDecimal lclToVan) {
		this.lclToVan = lclToVan;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getStorageRate() {
		return storageRate;
	}
	public void setStorageRate(BigDecimal storageRate) {
		this.storageRate = storageRate;
	}
	@Column(length=10)
	public String getStorageUnit() {
		return storageUnit;
	}
	public void setStorageUnit(String storageUnit) {
		this.storageUnit = storageUnit;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV10000To10999() {
		return v10000To10999;
	}
	public void setV10000To10999(BigDecimal to10999) {
		v10000To10999 = to10999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV1000To1999() {
		return v1000To1999;
	}
	public void setV1000To1999(BigDecimal to1999) {
		v1000To1999 = to1999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV11000To11999() {
		return v11000To11999;
	}
	public void setV11000To11999(BigDecimal to11999) {
		v11000To11999 = to11999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV12000To12999() {
		return v12000To12999;
	}
	public void setV12000To12999(BigDecimal to12999) {
		v12000To12999 = to12999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV13000To13999() {
		return v13000To13999;
	}
	public void setV13000To13999(BigDecimal to13999) {
		v13000To13999 = to13999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV14000To14999() {
		return v14000To14999;
	}
	public void setV14000To14999(BigDecimal to14999) {
		v14000To14999 = to14999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV15000To15999() {
		return v15000To15999;
	}
	public void setV15000To15999(BigDecimal to15999) {
		v15000To15999 = to15999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV16000To16999() {
		return v16000To16999;
	}
	public void setV16000To16999(BigDecimal to16999) {
		v16000To16999 = to16999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV17000To17999() {
		return v17000To17999;
	}
	public void setV17000To17999(BigDecimal to17999) {
		v17000To17999 = to17999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV18000To18999() {
		return v18000To18999;
	}
	public void setV18000To18999(BigDecimal to18999) {
		v18000To18999 = to18999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV19000To19999() {
		return v19000To19999;
	}
	public void setV19000To19999(BigDecimal to19999) {
		v19000To19999 = to19999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV1To999() {
		return v1To999;
	}
	public void setV1To999(BigDecimal to999) {
		v1To999 = to999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV20000() {
		return v20000;
	}
	public void setV20000(BigDecimal v20000) {
		this.v20000 = v20000;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV2000To2999() {
		return v2000To2999;
	}
	public void setV2000To2999(BigDecimal to2999) {
		v2000To2999 = to2999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV3000To3999() {
		return v3000To3999;
	}
	public void setV3000To3999(BigDecimal to3999) {
		v3000To3999 = to3999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV4000To4999() {
		return v4000To4999;
	}
	public void setV4000To4999(BigDecimal to4999) {
		v4000To4999 = to4999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV5000To5999() {
		return v5000To5999;
	}
	public void setV5000To5999(BigDecimal to5999) {
		v5000To5999 = to5999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV6000To6999() {
		return v6000To6999;
	}
	public void setV6000To6999(BigDecimal to6999) {
		v6000To6999 = to6999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV7000To7999() {
		return v7000To7999;
	}
	public void setV7000To7999(BigDecimal to7999) {
		v7000To7999 = to7999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV8000To8999() {
		return v8000To8999;
	}
	public void setV8000To8999(BigDecimal to8999) {
		v8000To8999 = to8999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getV9000To9999() {
		return v9000To9999;
	}
	public void setV9000To9999(BigDecimal to9999) {
		v9000To9999 = to9999;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getWarehouseRate() {
		return warehouseRate;
	}
	public void setWarehouseRate(BigDecimal warehouseRate) {
		this.warehouseRate = warehouseRate;
	}
	@Column(length=10)
	public String getWarehouseUnit() {
		return warehouseUnit;
	}
	public void setWarehouseUnit(String warehouseUnit) {
		this.warehouseUnit = warehouseUnit;
	}
	@Column
	public Date getEffective() {
		return effective;
	}
	public void setEffective(Date effective) {
		this.effective = effective;
	}
	@Column
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	@Column(length=1)
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Column(length=8, insertable = false, updatable=false)
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	@Column( length=10 )
	public String getWeightUnit() {
		return weightUnit;
	}
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}
	
	

}
