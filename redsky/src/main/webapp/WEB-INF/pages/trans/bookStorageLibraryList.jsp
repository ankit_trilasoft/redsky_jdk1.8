<%@ include file="/common/taglibs.jsp"%>

<head>
<title>Storage List</title>
<meta name="heading" content="Storage List" />
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:3px;margin-top:-18px;padding:2px 0;text-align:right;width:100%;}
</style>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="storageListForm" action="searchBookStorageLibraries" method="post">
<div id="Layer1" style="width:100%">
<s:hidden name="serviceOrder.id" />
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
			<div id="newmnav" style="!margin-bottom:6px;">
		    <ul>
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			  <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
			      <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			   </c:when>
			    <c:otherwise> 
		          <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose>  
		      </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		      <c:choose> 
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
			      <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			   </c:when>
			    <c:otherwise> 
		          <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose> 
		      </sec-auth:authComponent>
			  	<c:if test="${forwardingTabVal!='Y'}"> 
					<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
				</c:if>
				<c:if test="${forwardingTabVal=='Y'}">
					<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
				</c:if>
			  
			  <c:if test="${serviceOrder.job !='INT'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
			  
			  			 			  		   <c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>	
			  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Ticket</span></a></li>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </configByCorp:fieldVisibility>
			    <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			</ul>
		</div><div class="spn">&nbsp;</div>
	
	<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
		<div id="newmnav">
		   			 <ul>
					  	<li><a href="editWorkTicketUpdate.html?id=${workTicket.id}"><span>Work Ticket</span></a></li>
					  	<li id="newmnav1" style="background:#FFF"><a class="current"><span>Storage List</span></a></li>
						<li><a href="searchUnitLocates.html?locationId=&type=&occupied=&warehouse=${workTicket.warehouse}&id=${workTicket.id}"><span>Add Storage</span></a></li>
					  	<li><a href="storageUnit.html?id=${workTicket.id}"><span>Access/Release Storage</span></a></li>
					  	<li><a href="storageUnitMove.html?id=${workTicket.id}"><span>Move Storage Location</span></a></li>
					  	<li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
					  	
					</ul>
		</div><div class="spn">&nbsp;</div>	
	<table width="100%" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
			<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
				<table>
					<tbody>
						<tr>
							<td class="listwhitetext">Ticket Number</td>
							<td><s:textfield name="workTicket.ticket" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
							<td width="10px"></td>
							<td class="listwhitetext">Warehouse</td>
							<td><s:textfield name="workTicket.warehouse" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
						</tr>
					</tbody>
				</table>
				</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	<s:set name="bookStorages" value="bookStorages" scope="request" />
	<div id="otabs">
				  <ul>
				    <li><a class="current"><span>List</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
				
	<display:table name="bookStorages" class="table" requestURI=" " id="bookStorageLibraryList" export="true" pagesize="10" style="width:100%">
		<display:column property="storageId" sortable="true" title="Storage Id" url="/editBookStorageLibraries.html?id1=${workTicket.id}&ticket=${workTicket.ticket}" paramId="id" paramProperty="id"/>
		<display:column property="locationId" sortable="true" titleKey="bookStorage.locationId" />
		<display:column property="what" sortable="true" titleKey="bookStorage.what" />
 		<display:column property="description" sortable="true" titleKey="bookStorage.description" />
        <display:column property="pieces" headerClass="containeralign" style="text-align: right;" sortable="true" titleKey="bookStorage.pieces" />
		<display:column property="containerId" sortable="true" titleKey="bookStorage.containerId" />
		<display:column property="itemTag" sortable="true" titleKey="bookStorage.itemTag" />
		<display:column property="oldLocation" sortable="true" titleKey="bookStorage.oldLocation" />
		<display:column property="oldStorage" sortable="true" title="Old Storage" />		
		<display:column sortable="true" title="Weight" headerClass="containeralign" >
				<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="0"
                  groupingUsed="true" value="${bookStorageLibraryList.measQuantity}" /></div>
         </display:column>
		<display:column property="unit" sortable="true" title="Unit" />
		<display:column sortable="true" title="Volume" headerClass="containeralign">
				<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="0"
                  groupingUsed="true" value="${bookStorageLibraryList.volume}" /></div>
         </display:column>
		<display:column property="volUnit" sortable="true" title="Unit" />

		<display:setProperty name="paging.banner.item_name" value="bookStorage" />
		<display:setProperty name="paging.banner.items_name" value="bookStorage" />
		<display:setProperty name="export.excel.filename" value="BookStorage List.xls" />
		<display:setProperty name="export.csv.filename" value="BookStorage List.csv" />
		<display:setProperty name="export.pdf.filename" value="BookStorage List.pdf" />
	</display:table>
	</td>
		</tr>
	</tbody>
</table>
</div>   
	
	<c:set var="buttons">	
	</c:set>
	<c:out value="${buttons}" escapeXml="false" />

</s:form>

<script type="text/javascript"> 
    highlightTableRows("bookStorageLibraryList");  
</script>
<script language="JavaScript" type="text/javascript">

function Validate(form){
try{
	if(confirm("Are You Ready to assign These Item")){
	  	form.action="bookStorageLibraries.html";
		form.submit();
	}else{
	  	form.action="bookStorageLibraries.html";
	  	form.submit();
	}  
}
catch(e){}
}
function Validate12(form) {
try{
	if(confirm("Are You Ready to mark  These Item to release")) {
  		form.action="bookStorageLibraries.html";
 		form.submit();
	}else{
	  form.action="bookStorageLibraries.html";
	  form.submit();
	}   
}
catch(e){}
}
function Validate13(form){
try{
	if(confirm("Are You Ready to move  These Item to new location")){
  		form.action="locationacs.html";
		form.submit();
	}else{
	  form.action="locationacs.html";
	  form.submit();
	}   
}
catch(e){}
}
try{
<script type="text/javascript">   
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/bookStorageLibraries.html?id=${workTicket.id}" ></c:redirect>
		</c:if>
}
catch(e){}		
		
</script>
