package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.DsLanguageDao;
import com.trilasoft.app.model.DsLanguage;
import com.trilasoft.app.service.DsLanguageManager;

public class DsLanguageManagerImpl extends GenericManagerImpl<DsLanguage, Long>  implements DsLanguageManager{
     DsLanguageDao dsLanguageDao;
	public DsLanguageManagerImpl(DsLanguageDao dsLanguageDao) {
		super(dsLanguageDao);
		this.dsLanguageDao=dsLanguageDao;
	}
	public List getListById(Long id) {
		return  dsLanguageDao.getListById(id);
	}
}
