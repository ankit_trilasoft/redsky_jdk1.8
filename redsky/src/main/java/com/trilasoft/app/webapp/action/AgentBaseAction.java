package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.trilasoft.app.model.AgentBase;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.service.AgentBaseManager;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.RefMasterManager;

public class AgentBaseAction extends BaseAction{

	    
	    private List baseAgent;
		private Long id;
		private Long partnerId;
		private Long maxId;
		private String baseCode;
		
		private AgentBase agentBase;
		private AgentBaseManager agentBaseManager;
		
		private List refMasters;
	    private RefMasterManager refMasterManager;
	    
	    private PartnerPublic partner;
	    private PartnerPublicManager partnerPublicManager;
	    
	    private static Map<String, String> military;
	    
	    private String sessionCorpID;
	     
	    private String gotoPageString;
		private String validateFormNav;
		private String hitflag;
		private List accountLineList;
		Date currentdate = new Date();

		static final Logger logger = Logger.getLogger(AgentBaseAction.class);

		 

		public String saveOnTabChange() throws Exception {
		    	String s = save(); 
		        validateFormNav = "OK";
		        return gotoPageString;
		}
	    
	    public AgentBaseAction(){
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
		}
	    
	    public String edit() {
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	getComboList(sessionCorpID);
			partner = partnerPublicManager.get(partnerId);
	        if (id != null) {   
	        	agentBase = agentBaseManager.get(id);
	        } else {   
	        	agentBase = new AgentBase();
	        	if(sessionCorpID.equalsIgnoreCase("VORU") || sessionCorpID.equalsIgnoreCase("VOCZ") || sessionCorpID.equalsIgnoreCase("VOAO") || sessionCorpID.equalsIgnoreCase("BONG") ){
	        		agentBase.setCorpID("VOER");
				}else{
					agentBase.setCorpID(sessionCorpID);
				}
	        	agentBase.setPartnerCode(partner.getPartnerCode());
	           
	        	agentBase.setCreatedOn(new Date());
	        	agentBase.setUpdatedOn(new Date());
	        }   
	        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS;   
	    }   
	    
	    public String save() throws Exception { 
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	getComboList(sessionCorpID);
	    	partner = partnerPublicManager.get(partnerId);
	        if (cancel != null) {   
	            return "cancel";   
	        }   
	      
	        if(sessionCorpID.equalsIgnoreCase("VORU") || sessionCorpID.equalsIgnoreCase("VOCZ") || sessionCorpID.equalsIgnoreCase("VOAO") || sessionCorpID.equalsIgnoreCase("BONG") ){
        		agentBase.setCorpID("VOER");
			}else{
				agentBase.setCorpID(sessionCorpID);
			}
	        boolean isNew = (agentBase.getId() == null); 
	        List isexisted=agentBaseManager.isExisted(agentBase.getBaseCode(), sessionCorpID, partner.getPartnerCode());
			if(!isexisted.isEmpty() && isNew){
				errorMessage("Base code already exist.");
				return INPUT; 
			}else{ 
				if(isNew){
		        	agentBase.setCreatedOn(new Date());
		        }
		        agentBase.setUpdatedOn(new Date());
		        agentBaseManager.save(agentBase);   
		        hitflag = "1";
		        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		        String key = (isNew) ? "agentBase.added" : "agentBase.updated";   
		        saveMessage(getText(key));   
		        }
		        if (!isNew) { 
		        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		            return INPUT;   
		        }else{
		        	baseAgent = agentBaseManager.getbaseListByPartnerCode(partner.getPartnerCode());
		        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		          return SUCCESS; 
		        }
			} 
	    } 
	    
	    public String deleteBase() {
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	agentBase = agentBaseManager.get(id);
	    	agentBaseManager.deleteBaseSCAC(agentBase.getPartnerCode(), agentBase.getBaseCode(), sessionCorpID);
	    	agentBaseManager.remove(id);
			saveMessage(getText("agentBase.deleted"));
			/*
				partner = partnerManager.get(partnerId);
	    		baseAgent = agentBaseManager.getbaseListByPartnerCode(partner.getPartnerCode());
			*/	
			id = partnerId;
			list();
	    	hitflag = "1";
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	    
	    @SkipValidation 
	    public String list() {
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	partner = partnerPublicManager.get(id);
	    	baseAgent = agentBaseManager.getbaseListByPartnerCode(partner.getPartnerCode());
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS;   
	    }
	    
	    @SkipValidation
	    public String getComboList(String corpId){
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	military = refMasterManager.findByParameter(sessionCorpID, "MILITARY");
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return SUCCESS;
	    }
	    
	    public String baseName(){
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	if(baseCode!=null){
				accountLineList=refMasterManager.militaryDesc(baseCode, sessionCorpID);
	        }else{
	        	String message="The Base Name is not exist";
	        	errorMessage(getText(message));
	        }
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS; 
		}
	    
	    public void setId(Long id) {   
	        this.id = id;   
	    }  
	    
	    public void setPartnerId(Long partnerId) {
			this.partnerId = partnerId;
		}
		
		public void setRefMasterManager(RefMasterManager refMasterManager) {
	        this.refMasterManager = refMasterManager;
	    }
	    public List getRefMasters(){
	    	return refMasters;
	    }
	    
	    public List getBaseAgent() {
			return baseAgent;
		}

		public void setBaseAgent(List baseAgent) {
			this.baseAgent = baseAgent;
		}

		public AgentBase getAgentBase() {
			return agentBase;
		}

		public void setAgentBase(AgentBase agentBase) {
			this.agentBase = agentBase;
		}

		public void setAgentBaseManager(AgentBaseManager agentBaseManager) {
			this.agentBaseManager = agentBaseManager;
		}

		public static Map<String, String> getMilitary() {
			return military;
		}

		public static void setMilitary(Map<String, String> military) {
			AgentBaseAction.military = military;
		}

		
		public String getValidateFormNav() {
			return validateFormNav;
		}

		public void setValidateFormNav(String validateFormNav) {
			this.validateFormNav = validateFormNav;
		}
		
		public String getGotoPageString() {
			return gotoPageString;
		}

		public void setGotoPageString(String gotoPageString) {
			this.gotoPageString = gotoPageString;
		}

		public Long getMaxId() {
			return maxId;
		}

		public void setMaxId(Long maxId) {
			this.maxId = maxId;
		}

		public String getSessionCorpID() {
			return sessionCorpID;
		}

		public void setSessionCorpID(String sessionCorpID) {
			this.sessionCorpID = sessionCorpID;
		}

		public Long getId() {
			return id;
		}

		public Long getPartnerId() {
			return partnerId;
		}

		public void setRefMasters(List refMasters) {
			this.refMasters = refMasters;
		}

		public PartnerPublic getPartner() {
			return partner;
		}

		public void setPartner(PartnerPublic partner) {
			this.partner = partner;
		}

		public List getAccountLineList() {
			return accountLineList;
		}

		public void setAccountLineList(List accountLineList) {
			this.accountLineList = accountLineList;
		}

		public String getBaseCode() {
			return baseCode;
		}

		public void setBaseCode(String baseCode) {
			this.baseCode = baseCode;
		}

		/**
		 * @return the hitflag
		 */
		public String getHitflag() {
			return hitflag;
		}

		/**
		 * @param hitflag the hitflag to set
		 */
		public void setHitflag(String hitflag) {
			this.hitflag = hitflag;
		}

		
		public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
			this.partnerPublicManager = partnerPublicManager;
		}
	    
	}