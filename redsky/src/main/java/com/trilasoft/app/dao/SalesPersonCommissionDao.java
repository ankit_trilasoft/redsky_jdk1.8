package com.trilasoft.app.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.SalesPersonCommission;

public interface SalesPersonCommissionDao extends GenericDao<SalesPersonCommission, Long>{
	public List getCommPersentAcRange(String corpId,String contractName,String chargeCode);
	public Map<Double,BigDecimal > getCommPersentAcMap(String corpId,String contractName,String chargeCode,String companyDiv);
}
