package com.trilasoft.app.webapp.action;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.commons.beanutils.converters.BigIntegerConverter;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;   

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.ClaimLossTicket;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Loss;
import com.trilasoft.app.model.Claim;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.ClaimLossTicketManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.LossManager;
import com.trilasoft.app.service.ClaimManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.WorkTicketManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List; 
import java.util.Map;
import java.util.Set;

public class LossAction extends BaseAction implements Preparable
{   
	private List losss;
	private Long i;
	private ClaimManager claimManager;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private Claim claim;
	private Long id;
	private Long  soId;
	private int autoIdNumber1;
	private String idNumber1;
	private LossManager lossManager;
	private Loss loss;
	private ClaimLossTicketManager claimLossTicketManager;
	private ClaimLossTicket claimLossTicket;
	private WorkTicketManager workTicketManager;
	private  TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private  Miscellaneous  miscellaneous;
	private CustomerFileManager customerFileManager;
	private  CustomerFile customerFile;
	private MiscellaneousManager miscellaneousManager ;
	private WorkTicket workTicket;
	private List maxIdNumber1;
	private List refMasters;
	private RefMasterManager refMasterManager;
	private List claimLossTickets;
	private List lossPictures;
	private String sessionCorpID;
	private String requestedCLT;
	private String requestedACLT;
	private Long claimNum;
	private String lossNum;
	private String reqWT;
	private String requestedWT;
	private String countLossNotes;
	private NotesManager notesManager;
	private String countLossTicketNotes;
	private String shipNum;
	private String shipnum;
	private Long claimNumberCopy;
	private String chequeNumberCustmer;
	private String chequeNumber3Party;
	private List ls;
	private String seqNum;
	private boolean isAppliedToAllLoss;
	private Long ticket;
	private String cLPercent;
	private String cLChargeBack;
	private List lossListByAct;
	private String btnType;
	private int lossChequeNumberCustmer;
	private int lossChequeNumber3Party;
	private Long minLoss;
	private Long maxLoss;
	private String countLoss;
	private Long tempLoss;
	private String idNumber;
	private Long autoId;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(LossAction.class);
	private CompanyManager companyManager;
	private Company company;
	private String voxmeIntergartionFlag;
	private List pertainingloss=new ArrayList();
	private String itmClmsItem;
    private String oiJobList;
	 
	public LossAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	private  Map<String, String> clm_actn;
	private  Map<String, String> house;
	private  Map<String, String> clm_loss;
	private  Map<String, String> damaged;
	private  Map<String, String> yesno;
	private  Map<String, String> currency;	
	private  Map<String, String> denialReason;
	private  Map<String, String> respExpCode;
	
	public void setLossManager(LossManager lossManager) {
		this.lossManager = lossManager;
	}

	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}
	public void setClaimLossTicketManager(ClaimLossTicketManager claimLossTicketManager) {
		this.claimLossTicketManager = claimLossTicketManager;
	}
	public Claim getClaim() {   
		return claim;   
	}   
	public void setClaim(Claim claim  ) {   
		this.claim  = claim ;   
	} 
	public WorkTicket getWorkTicket() {   
		return workTicket;   
	}   

	public void setWorkTicket(WorkTicket workTicket) {   
		this.workTicket= workTicket;   
	}
	public ClaimLossTicket getClaimLossTicket() {
		return claimLossTicket;
	}

	public void setClaimLossTicket(ClaimLossTicket claimLossTicket) {
		this.claimLossTicket = claimLossTicket;
	}
	public List getLosss() {   
		return losss;   
	}   
	public String list() {
		//getComboList(sessionCorpID);
		losss = (lossManager.findLossList()); 
		return SUCCESS;   
	} 
	public void setId(Long id) {   
		this.id = id;   
	}   

	public void setRequestedACLT(String requestedACLT) {
		this.requestedACLT = requestedACLT;
	}

	public void setRequestedCLT(String requestedCLT) {
		this.requestedCLT = requestedCLT;
	}
	public void setClaimNum(Long claimNum) {
		this.claimNum = claimNum;
	}

	public void setLossNum(String lossNum) {
		this.lossNum = lossNum;
	}

	public void setShipNum(String shipNum) {
		this.shipNum = shipNum;
	}

	public void setSeqNum(String seqNum) {
		this.seqNum = seqNum;
	}

	public Loss getLoss() {   
		return loss;   
	}   

	public List getLs() {
		return ls;
	}

	public void setLs(List ls) {
		this.ls = ls;
	}

	public void setLoss(Loss loss) {   
		this.loss = loss;   
	}   
	public ServiceOrder getServiceOrder() {   
		return serviceOrder;   
	}   

	public boolean isAppliedToAllLoss() {
		return isAppliedToAllLoss;
	}

	public void setAppliedToAllLoss(boolean isAppliedToAllLoss) {
		this.isAppliedToAllLoss = isAppliedToAllLoss;
	}

	public Long getTicket() {
		return ticket;
	}

	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}

	public void setServiceOrder(ServiceOrder serviceOrder  ) {   
		this.serviceOrder = serviceOrder;   
	}   
	public String delete() {   
		lossManager.remove(loss.getId());   
		saveMessage(getText("loss.deleted"));   

		return SUCCESS;   
	} 
	
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			getComboList(sessionCorpID);
			company=companyManager.findByCorpID(sessionCorpID).get(0);
            if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }
			if(company!=null){
				if(company.getVoxmeIntegration()!=null){
					voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
					}
				}
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} 
	
	private List dataFromLossPicture;
	public String edit() {
		//getComboList(sessionCorpID);
		if (id != null)
		{
			loss=lossManager.getForOtherCorpid(id);
			loss.setCorpID(sessionCorpID);
			claim=loss.getClaim();
			serviceOrder=claim.getServiceOrder();
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus=trackingStatusManager.get(serviceOrder.getId());
			billing=billingManager.get(serviceOrder.getId());
			customerFile = serviceOrder.getCustomerFile(); 
			
			maxLoss = Long.parseLong(lossManager.findMaximumLoss(loss.getClaimNumber(), sessionCorpID).get(0).toString());
			minLoss=  Long.parseLong(lossManager.findMinimumLoss(loss.getClaimNumber(), sessionCorpID).get(0).toString());
			countLoss =  lossManager.findCountLoss(loss.getClaimNumber(), sessionCorpID).get(0).toString();
			lossPictures = lossManager.getLossPictureList(loss.getId().toString(), loss.getClaimId());
			
		} 
		claimLossTickets = claimLossTicketManager.findClaimLossTickets(String.valueOf(loss.getIdNumber1()),loss.getClaimNumber(), sessionCorpID);
		loss.setShipNumber(claim.getServiceOrder().getShipNumber());
		loss.setSequenceNumber(claim.getServiceOrder().getSequenceNumber());
		serviceOrder=claim.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus=trackingStatusManager.get(serviceOrder.getId());
		billing=billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile(); 
		dataFromLossPicture = lossManager.getLossPictureData(loss.getId(),loss.getCorpID());
		getNotesForIconChange();
		return SUCCESS;   
	} 
	
	@SkipValidation
	   public String removeCLTByAction() { 
		//System.out.println("n\n\n\n\n\n\nlossNumAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"+lossNum);
		//System.out.println("n\n\n\n\n\n\nlsessionCorpIDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"+sessionCorpID);
		
		 claimLossTicketManager.removeCLTByAction(lossNum, sessionCorpID); 
		
	    //System.out.println("n\n\n\n\n\n\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"+claimLossTicketManager.removeCLTByAction(lossNum, sessionCorpID));	      //  return SUCCESS;   
	    return SUCCESS;  
	} 
	private Billing billing;
	private BillingManager billingManager;
	public String editNewLoss() {
		//getComboList(sessionCorpID);
		if (id != null) {
			claim = claimManager.get(id);
			serviceOrder=claim.getServiceOrder();
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus=trackingStatusManager.get(serviceOrder.getId());
			billing=billingManager.get(serviceOrder.getId());
			customerFile = serviceOrder.getCustomerFile(); 
			loss= new Loss();
			maxIdNumber1 = lossManager.findMaximum();
			if ( maxIdNumber1.get(0) == null ) {          
				idNumber1 = "01";
			}else {
				autoIdNumber1 = Integer.parseInt(maxIdNumber1.get(0).toString()) + 1;
			}
			loss.setIdNumber1(autoIdNumber1);
			loss.setCreatedOn(new Date());
			loss.setUpdatedOn(new Date());
			loss.setCorpID(sessionCorpID);
			
			List list =lossManager.findWH(claim.getClaimNumber(), sessionCorpID);
			if(!list.isEmpty()&& list!=null ){
	            	int size=list.size();
	            	Object[] obj=(Object[])list.get(size-1);
	            	loss.setWarehouse(obj[0].toString());
	            	loss.setDamageByAction(obj[1].toString());
			}
		}
		loss.setShipNumber(claim.getServiceOrder().getShipNumber());
		loss.setSequenceNumber(claim.getServiceOrder().getSequenceNumber());
		List cLTickets = claimLossTicketManager.findCustClmLossTickets(loss.getSequenceNumber(), sessionCorpID);
		getNotesForIconChange();
		return SUCCESS;   
	}

	private String gotoPageString;

	public String getGotoPageString() {

		return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

		this.gotoPageString = gotoPageString;

	}

	public String saveOnTabChange() throws Exception {
		String s = save(); 
		return gotoPageString;
	}

	@SkipValidation
    public String lossChequeNumberCustmer(){
		lossChequeNumberCustmer=lossManager.updateLossChequeNumberCustmer(chequeNumberCustmer,claimNumberCopy, shipnum);
    	return SUCCESS;
    }
	@SkipValidation
    public String lossChequeNumber3Party(){
		lossChequeNumber3Party=lossManager.updateLossChequeNumber3Party(chequeNumber3Party,claimNumberCopy, shipnum);
    	return SUCCESS;
    }
	
	@SkipValidation 
	public String getNotesForIconChange() {
		List cl = notesManager.countLossNotes(claim.getClaimNumber());
		List lt = notesManager.countLossTicketNotes(claim.getClaimNumber());


		if( cl.isEmpty() ){
			countLossNotes = "0";
		}else {
			countLossNotes = ((cl).get(0)).toString() ;
		}
		if(lt.isEmpty() ){
			countLossTicketNotes = "0";
		}else {
			countLossTicketNotes = ((lt).get(0)).toString() ;
		}

		return SUCCESS;   
	}
	public String save() throws Exception {
		loss.setClaim(claim);
		boolean isNew = (loss.getId() == null);
		if(isNew)
		{
			loss.setCreatedOn(new Date());
		}
		loss.setClaimId(claim.getId().toString());
		loss.setClaimNumber(claim.getClaimNumber());
		loss.setUpdatedOn(new Date());
		loss.setUpdatedBy(getRequest().getRemoteUser());
		loss.setCorpID(sessionCorpID);
		/*List lossList = lossManager.findLossByIdNumber(loss.getIdNumber1());
		if(isNew && !(lossList == null || lossList.isEmpty())){
			return SUCCESS;
		}*/
		List maxIdNumber = lossManager.findMaximumIdNumber(claim.getId());
   	    if ( maxIdNumber.get(0) == null  ) {          
           	 idNumber = "01";
           }else {
        	   if( maxIdNumber.get(0).toString().equals("")){
        		   idNumber = "01";   
        	   }else{
        		   autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
        		   //System.out.println(autoId);
        		   if((autoId.toString()).length() == 1) {
        			   idNumber = "0"+(autoId.toString());
        		   }else {
        			   idNumber=autoId.toString();
        		   }
        	   }
           }
   	 if (isNew) { 
   		loss.setIdNumber(idNumber);
		}else{
		loss.setIdNumber(loss.getIdNumber());
		}
		loss = lossManager.save(loss);
		claim=loss.getClaim(); 
		serviceOrder=claim.getServiceOrder();
		trackingStatus=trackingStatusManager.get(serviceOrder.getId());
		 try{
			 if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup())	{
		        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					synchornizeLoss(serviceOrderRecords,claim,isNew,serviceOrder,loss);
					
			   }
		  }catch(Exception ex){
				 ex.printStackTrace();
		   }
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){

			String key = (isNew) ? "Loss has been added successfully" : "Loss has been updated successfully";

			saveMessage(getText(key));

		}
		assignTicketsToLoss();
		getNotesForIconChange();
		if (!isNew) {   
			btnType="save";
			return INPUT;   
		}else {   
			i = Long.parseLong(lossManager.findMaximumId().get(0).toString());
			loss = lossManager.get(i);
			btnType="save";
			return SUCCESS;   
		}   
	} 
	 @SkipValidation
	 private void synchornizeLoss(List<Object> serviceOrderRecords,Claim claim, boolean isNew,ServiceOrder serviceOrderold,Loss lossFrom) { 
	    	Iterator  it=serviceOrderRecords.iterator();
	    	while(it.hasNext()){
	    		ServiceOrder serviceOrderObj=(ServiceOrder)it.next();
	    		TrackingStatus trackingStatusToRecod=trackingStatusManager.getForOtherCorpid(serviceOrderObj.getId());
				if(trackingStatusToRecod.getSoNetworkGroup()){
	    		if(!(serviceOrderObj.getShipNumber().equals(serviceOrderold.getShipNumber()))){
	    		try{
	    			
	    	    	   Long claimId =claimManager.findRemoteClaim(claim.getIdNumber(),serviceOrderObj.getId()); 
	    			   Claim claimTo= claimManager.getForOtherCorpid(claimId); 
	    			   if(isNew){

							Loss lossNew= new Loss();
							List  maxIdNumber1 = lossManager.findMaximumIdNumber1(serviceOrderObj.getCorpID());
							if (maxIdNumber1== null ||  maxIdNumber1.get(0) == null ) {          
							String	idNumber1 = "01";
							}else {
								autoIdNumber1 = Integer.parseInt(maxIdNumber1.get(0).toString()) + 1;
							}
							BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
							beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
							beanUtilsBean2.getConvertUtils().register( new BigIntegerConverter(null), BigInteger.class);
						    beanUtilsBean2.copyProperties(lossNew, lossFrom);
						    lossNew.setIdNumber1(autoIdNumber1);
						    
						    lossNew.setCorpID(serviceOrderObj.getCorpID());
						    lossNew.setId(null);
							lossNew.setShipNumber(serviceOrderObj.getShipNumber());
							lossNew.setSequenceNumber(serviceOrderObj.getSequenceNumber());
							lossNew.setCreatedBy("Networking");
							lossNew.setUpdatedBy("Networking");
							lossNew.setCreatedOn(new Date());
						    lossNew.setUpdatedOn(new Date()); 
						    lossNew.setWarehouse("");
						    lossNew.setClaimId(claimId.toString());
						    lossNew.setClaimNumber(claimTo.getClaimNumber());
						    lossNew.setClaim(claimTo);
						    lossNew= lossManager.save(lossNew);	
							  
	    			   }else{
	    				    Long lossId =lossManager.findRemoteLoss(lossFrom.getIdNumber(),claimTo.getId()); 
	    				    Loss lossTo= lossManager.getForOtherCorpid(lossId);  
	           			    Long id=lossTo.getId();
		           			String createdBy=lossTo.getCreatedBy();
		           			Date createdOn=lossTo.getCreatedOn(); 
		           			Long claimNum = lossTo.getClaimNumber();
		           			String idNum = lossTo.getIdNumber();
		           			String lossClaimId = lossTo.getClaimId();
		           			Long claimNumber= lossTo.getClaimNumber();
		           			String Warehouse= lossTo.getWarehouse();
		           			
		           			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
		       				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
		       				beanUtilsBean.getConvertUtils().register( new BigIntegerConverter(null), BigInteger.class);
		       				beanUtilsBean.copyProperties(lossTo, lossFrom);
		       				
		       				lossTo.setId(id);
		       				lossTo.setCreatedBy(createdBy);
		       				lossTo.setCreatedOn(createdOn);
		       				lossTo.setCorpID(serviceOrderObj.getCorpID());
		       				lossTo.setShipNumber(serviceOrderObj.getShipNumber());
		       				lossTo.setSequenceNumber(serviceOrderObj.getSequenceNumber()); 
		       				lossTo.setUpdatedBy(claim.getCorpID()+":"+getRequest().getRemoteUser());
		       				lossTo.setUpdatedOn(new Date()); 
		       				lossTo.setClaimNumber(claimNum);
		       				lossTo.setIdNumber(idNum);  
		       				lossTo.setClaimId(lossClaimId);
		       				lossTo.setClaimNumber(claimNumber);
		       				lossTo.setWarehouse(Warehouse);
		       				lossTo.setClaim(claimTo);
		       				lossManager.save(lossTo);
	    			   }
	        			
	    		}catch(Exception ex){
	    			 ex.printStackTrace();
	    		}
	    		 
	    	 }
			}	
	     }
	    	 
			
		}
	
	@SkipValidation
	private List findLinkedShipNumber(ServiceOrder serviceOrder) { 
		List linkedShipNumberList= new ArrayList();
    	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
    	}else{
    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
    	} 
    	return linkedShipNumberList;
    }	

	@SkipValidation
    private List<Object> findServiceOrderRecords(List linkedShipNumber,ServiceOrder serviceOrderLocal) { 
	List<Object> recordList= new ArrayList();
	Iterator it =linkedShipNumber.iterator();
	while(it.hasNext()){
		String shipNumber= it.next().toString();
		if(!serviceOrderLocal.getShipNumber().equalsIgnoreCase(shipNumber)){
			ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
			recordList.add(serviceOrderRemote);
		}
	} 
	return recordList;
   }
	
	
	@SkipValidation     
	public String assignTicketsToLoss() throws Exception {
		if(requestedCLT == null || requestedCLT.equalsIgnoreCase("")){

		}
		else{  	   	  	

			if(requestedCLT == null){
				requestedCLT = "";
			}
			String[] result = requestedCLT.split("\\,");
			String i;
			for (int x=0; x<result.length; x++)
			{
				boolean exists = claimLossTicketManager.findTicketbyLoss(Long.parseLong(result[x]),seqNum,lossNum);
				if(!exists){
					claimLossTicket = new ClaimLossTicket();
					claimLossTicket.setTicket(Long.parseLong(result[x]));
					claimLossTicket.setClaimNumber(claimNum);
					claimLossTicket.setLossNumber(lossNum);
					claimLossTicket.setShipNumber(loss.getShipNumber());
					claimLossTicket.setSequenceNumber(loss.getSequenceNumber());
					claimLossTicket.setCorpID(sessionCorpID);
					claimLossTicket.setCreatedBy(getRequest().getRemoteUser());
					claimLossTicket.setCreatedOn(new Date());
					claimLossTicket.setUpdatedBy(getRequest().getRemoteUser());
					claimLossTicket.setUpdatedOn(new Date());
					claimLossTicketManager.save(claimLossTicket);
				}
			}
			if ( save != null ) {
			}
		}
		getNotesForIconChange();
		return SUCCESS;
	} 


	@SkipValidation     
	public String assignTicketsToAllLoss() throws Exception {
		save();
		if(requestedACLT == null){
			requestedACLT = "";
		}
		String[] result = requestedACLT.split("\\,");
		for (int x=0; x<result.length; x++)
		{
			claimLossTicketManager.applyTicketToAllLoss(Long.parseLong(result[x]),seqNum,lossNum);
			List<Loss> lossList = claimLossTicketManager.findLossByClaimNum(claimNum, sessionCorpID);
			for(Loss loss : lossList){
				boolean ticketExists = claimLossTicketManager.findLossTicket(Long.parseLong(result[x]),seqNum,claim.getClaimNumber(),String.valueOf(loss.getIdNumber1()));
				if(!ticketExists){
					claimLossTicket = new ClaimLossTicket();
					claimLossTicket.setTicket(Long.parseLong(result[x]));
					claimLossTicket.setClaimNumber(claim.getClaimNumber());
					claimLossTicket.setLossNumber(String.valueOf(loss.getIdNumber1()));
					claimLossTicket.setShipNumber(loss.getShipNumber());
					claimLossTicket.setSequenceNumber(loss.getSequenceNumber());
					claimLossTicket.setIsAppliedToAllLoss(true);
					claimLossTicket.setCorpID(sessionCorpID);
					claimLossTicket.setChargeBack(claimLossTicket.getChargeBack());
					claimLossTicket.setPercent(claimLossTicket.getPercent());
					claimLossTicket.setCreatedBy(getRequest().getRemoteUser());
					claimLossTicket.setCreatedOn(new Date());
					claimLossTicket.setUpdatedBy(getRequest().getRemoteUser());
					claimLossTicket.setUpdatedOn(new Date());
					claimLossTicketManager.save(claimLossTicket);
				}
			}
			claimLossTicketManager.applyTicketToAllLossOldAlso((Long.parseLong(result[x])), claim.getClaimNumber(), String.valueOf(loss.getIdNumber1()), sessionCorpID);

		}
		boolean isNew = (loss.getId() == null);
		if(isNew)
		{
			loss.setCreatedOn(new Date());
		}
		
		loss.setCorpID(sessionCorpID);
		getNotesForIconChange();
		return SUCCESS;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public List getRefMasters(){
		return refMasters;
	}
	private List selfBlameValue;
	public String getComboList(String corpId){
		clm_actn = refMasterManager.findByParameter(corpId, "CLM_ACTN");
		clm_loss=refMasterManager.findByParameter(corpId, "LOSS");
		house=refMasterManager.findByParameter(corpId, "HOUSE");
		damaged=refMasterManager.findByParameter(corpId, "DAMAGED");
		yesno = refMasterManager.findByParameter(corpId, "YESNO");
		currency = refMasterManager.findCodeOnleByParameter(corpId, "CURRENCY");
		denialReason = refMasterManager.findByParameter(corpId, "DENIALREASON");
		respExpCode = refMasterManager.findByParameter(corpId, "RESPEXPCODE");
		selfBlameValue = new ArrayList();
		selfBlameValue.add("Yes");
		selfBlameValue.add("No");
		
		return SUCCESS;
	}
	@SkipValidation 
	public String pertaininglossList(){
		pertainingloss=lossManager.getpertaininglossvalue(claimNum,itmClmsItem,sessionCorpID); 
		return SUCCESS;
	}
	
	@SkipValidation 
	public String editNextLoss(){
	 	tempLoss=Long.parseLong((lossManager.goNextLossChild(claimNum,id,sessionCorpID)).get(0).toString());
	 	return SUCCESS;   
	}
	@SkipValidation 
	public String editPrevLoss(){
	    	tempLoss=Long.parseLong((lossManager.goPrevLossChild(claimNum,id,sessionCorpID)).get(0).toString());
	    	return SUCCESS;   
	} 
	
	@SkipValidation 
	public String lossOther(){
		losss = lossManager.goLossChild(claimNum,id,sessionCorpID);
	    	return SUCCESS;   
	}
	
	
	private InputStream fileInputStream1;

	public InputStream getFileInputStream1() {
		return fileInputStream1;
	}
	
	private String photoUri;
	private String fileFileName;
	 @SkipValidation
		public String showLossDocumentImageServletAction() throws Exception{
		 List list = lossManager.getLossDocumentImageServletAction(id, sessionCorpID);
		 if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase(""))
			 photoUri =	list.get(0).toString().split("~")[0];
			 fileFileName=list.get(0).toString().split("~")[1];
		 System.out.println(photoUri+"==============================================="+fileFileName);
			 fileInputStream1 = new FileInputStream(new File(photoUri));
		 return SUCCESS;
		}
	

	public  Map<String, String> getClm_actn() {
		return clm_actn;
	}
	public  Map<String, String> getClm_loss() {
		return clm_loss;
	}
	public  Map<String, String> getHouse() {
		return house;
	}
	public  Map<String, String> getDamaged() {
		return damaged;
	}
	public  Map<String, String> getYesno() {
		return yesno;
	}

	public String getCountLossNotes() {
		return countLossNotes;
	}

	public void setCountLossNotes(String countLossNotes) {
		this.countLossNotes = countLossNotes;
	}

	public String getCountLossTicketNotes() {
		return countLossTicketNotes;
	}

	public void setCountLossTicketNotes(String countLossTicketNotes) {
		this.countLossTicketNotes = countLossTicketNotes;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public int getAutoIdNumber1() {
		return autoIdNumber1;
	}

	public void setAutoIdNumber1(int autoIdNumber1) {
		this.autoIdNumber1 = autoIdNumber1;
	}
	public List getMaxIdNumber1() {
		return maxIdNumber1;
	}

	public void setMaxIdNumber1(List maxIdNumber1) {
		this.maxIdNumber1 = maxIdNumber1;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public void setLosss(List losss) {
		this.losss = losss;
	}

	public List getClaimLossTickets() {
		return claimLossTickets;
	}

	public void setClaimLossTickets(List claimLossTickets) {
		this.claimLossTickets = claimLossTickets;
	}

	public  Miscellaneous getMiscellaneous() {
		return( miscellaneous !=null )?miscellaneous :miscellaneousManager.get(soId);
	}

	public  void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public  TrackingStatus getTrackingStatus() {
		return( trackingStatus!=null )?trackingStatus:trackingStatusManager.get(soId);
	}

	public  void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public  CustomerFile getCustomerFile() {
		return customerFile;
	}

	public  void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public String getCLPercent() {
		return cLPercent;
	}

	public void setCLPercent(String percent) {
		cLPercent = percent;
	}

	public String getCLChargeBack() {
		return cLChargeBack;
	}

	public void setCLChargeBack(String chargeBack) {
		cLChargeBack = chargeBack;
	}

	public List getLossListByAct() {
		return lossListByAct;
	}

	public void setLossListByAct(List lossListByAct) {
		this.lossListByAct = lossListByAct;
	}

	public Long getSoId() {
		return soId;
	}

	public void setSoId(Long soId) {
		this.soId = soId;
	}

	public void setClm_actn(Map<String, String> clm_actn) {
		this.clm_actn = clm_actn;
	}

	public void setHouse(Map<String, String> house) {
		this.house = house;
	}

	public void setClm_loss(Map<String, String> clm_loss) {
		this.clm_loss = clm_loss;
	}

	public void setDamaged(Map<String, String> damaged) {
		this.damaged = damaged;
	}

	public void setYesno(Map<String, String> yesno) {
		this.yesno = yesno;
	}

	public String getBtnType() {
		return btnType;
	}

	public void setBtnType(String btnType) {
		this.btnType = btnType;
	}

	public String getCountLoss() {
		return countLoss;
	}

	public void setCountLoss(String countLoss) {
		this.countLoss = countLoss;
	}

	public Long getMaxLoss() {
		return maxLoss;
	}

	public void setMaxLoss(Long maxLoss) {
		this.maxLoss = maxLoss;
	}

	public Long getMinLoss() {
		return minLoss;
	}

	public void setMinLoss(Long minLoss) {
		this.minLoss = minLoss;
	}

	public Long getTempLoss() {
		return tempLoss;
	}

	public void setTempLoss(Long tempLoss) {
		this.tempLoss = tempLoss;
	}

	/**
	 * @return the shipnum
	 */
	public String getShipnum() {
		return shipnum;
	}

	/**
	 * @param shipnum the shipnum to set
	 */
	public void setShipnum(String shipnum) {
		this.shipnum = shipnum;
	}

	

	/**
	 * @return the chequeNumberCustmer
	 */
	public String getChequeNumberCustmer() {
		return chequeNumberCustmer;
	}

	/**
	 * @param chequeNumberCustmer the chequeNumberCustmer to set
	 */
	public void setChequeNumberCustmer(String chequeNumberCustmer) {
		this.chequeNumberCustmer = chequeNumberCustmer;
	}

	/**
	 * @return the claimNumberCopy
	 */
	public Long getClaimNumberCopy() {
		return claimNumberCopy;
	}

	/**
	 * @param claimNumberCopy the claimNumberCopy to set
	 */
	public void setClaimNumberCopy(Long claimNumberCopy) {
		this.claimNumberCopy = claimNumberCopy;
	}

	/**
	 * @return the chequeNumber3Party
	 */
	public String getChequeNumber3Party() {
		return chequeNumber3Party;
	}

	/**
	 * @param chequeNumber3Party the chequeNumber3Party to set
	 */
	public void setChequeNumber3Party(String chequeNumber3Party) {
		this.chequeNumber3Party = chequeNumber3Party;
	}

	/**
	 * @return the lossChequeNumberCustmer
	 */
	public int getLossChequeNumberCustmer() {
		return lossChequeNumberCustmer;
	}

	/**
	 * @param lossChequeNumberCustmer the lossChequeNumberCustmer to set
	 */
	public void setLossChequeNumberCustmer(int lossChequeNumberCustmer) {
		this.lossChequeNumberCustmer = lossChequeNumberCustmer;
	}

	/**
	 * @return the lossChequeNumber3Party
	 */
	public int getLossChequeNumber3Party() {
		return lossChequeNumber3Party;
	}

	/**
	 * @param lossChequeNumber3Party the lossChequeNumber3Party to set
	 */
	public void setLossChequeNumber3Party(int lossChequeNumber3Party) {
		this.lossChequeNumber3Party = lossChequeNumber3Party;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public Map<String, String> getCurrency() {
		return currency;
	}

	public void setCurrency(Map<String, String> currency) {
		this.currency = currency;
	}

	public List getSelfBlameValue() {
		return selfBlameValue;
	}

	public void setSelfBlameValue(List selfBlameValue) {
		this.selfBlameValue = selfBlameValue;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public Long getAutoId() {
		return autoId;
	}

	public void setAutoId(Long autoId) {
		this.autoId = autoId;
	}

	public CompanyManager getCompanyManager() {
		return companyManager;
	}

	public Company getCompany() {
		return company;
	}

	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}

	public Map<String, String> getDenialReason() {
		return denialReason;
	}

	public void setDenialReason(Map<String, String> denialReason) {
		this.denialReason = denialReason;
	}

	public Map<String, String> getRespExpCode() {
		return respExpCode;
	}

	public void setRespExpCode(Map<String, String> respExpCode) {
		this.respExpCode = respExpCode;
	}

	public List getPertainingloss() {
		return pertainingloss;
	}

	public void setPertainingloss(List pertainingloss) {
		this.pertainingloss = pertainingloss;
	}

	public String getItmClmsItem() {
		return itmClmsItem;
	}

	public void setItmClmsItem(String itmClmsItem) {
		this.itmClmsItem = itmClmsItem;
	}

	public List getLossPictures() {
		return lossPictures;
	}

	public void setLossPictures(List lossPictures) {
		this.lossPictures = lossPictures;
	}

	public List getDataFromLossPicture() {
		return dataFromLossPicture;
	}

	public void setDataFromLossPicture(List dataFromLossPicture) {
		this.dataFromLossPicture = dataFromLossPicture;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}


}      