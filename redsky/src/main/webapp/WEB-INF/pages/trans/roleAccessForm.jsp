<%@ include file="/common/taglibs.jsp"%>   
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
</script>

<head>   
    <title><fmt:message key="roleAccessDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='roleAccessDetail.heading'/>"/>   
</head>  

<s:form id="roleAccessForm" name="roleAccessForm" action="saveRoleAccess" method="post" validate="true">      
<s:hidden name="roleAccess.corpID" /> 
<s:hidden name="roleAccess.id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<div id="Layer1">
<div>
  <ul id="newmnav">
   	<li><a href="roleAccesss.html"><span>Role Access List</span></a></li>
   <li id="newmnav1" style="background:#FFF"><a class="current""><span>Role Access Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
</ul></div>

<div class="spn">&nbsp;</div>
<div style="padding-bottom:3px;"></div>


 <table class="mainDetailTable" cellspacing="1" cellpadding="1" border="0">
	<tbody>
 		   <tr><td align="left" height="5px"></td></tr>
		   <tr class="listwhitetext">
		   		<td align="right"><fmt:message key="roleAccess.role"/></td>
		   		<%--
		   		<td align="left"><s:textfield name="roleAccess.role" maxlength="15" required="true" cssClass="input-text" size="15"/></td>
		   		--%>
		   		<td align="left"><s:select cssClass="list-menu" name="roleAccess.role" list="%{role_action}" cssStyle="width:100px" headerKey="" headerValue="  "/></td>
		   		
		   		<td align="right"><fmt:message key="roleAccess.action"/></td>
		   		<td align="left"><s:select cssClass="list-menu"  name="roleAccess.action" list="%{role_action}" cssStyle="width:100px" headerKey="" headerValue="  "/></td>
		   </tr>
		   <tr class="listwhitetext">
		   		<td align="right"><fmt:message key="roleAccess.menu1"/></td>
		   		<td align="left"><s:select cssClass="list-menu" name="roleAccess.menu1" list="%{menu_s}" required="true"  cssStyle="width:250px"/></td>
		   		<td align="right"><fmt:message key="roleAccess.menu2"/></td>
		   		<td align="left"><s:select cssClass="list-menu" name="roleAccess.menu2" list="%{menu_s}" required="true" cssStyle="width:250px"/></td>
		  </tr>
		   <tr class="listwhitetext"> 		
		   		<td align="right"><fmt:message key="roleAccess.menu3"/></td>
		   		<td align="left"><s:select cssClass="list-menu" name="roleAccess.menu3" list="%{menu_s}" required="true" cssStyle="width:250px"/></td>
		   		<td align="right"><fmt:message key="roleAccess.menu4"/></td>
		   		<td align="left"><s:select cssClass="list-menu" name="roleAccess.menu4" list="%{menu_s}" required="true" cssStyle="width:250px"/></td>
		   </tr>
		   <tr class="listwhitetext">	
		   		<td align="right"><fmt:message key="roleAccess.menu5"/></td>
		   		<td align="left"><s:select cssClass="list-menu" name="roleAccess.menu5" list="%{menu_s}" required="true" cssStyle="width:250px"/></td>
		   	</tr>
		  <tr><td align="left" height="5px"></td></tr>
	</tbody>
 </table></td>
 		  <td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td></tr>   
 		  
   
  </tbody>
  </table>
  
 </div>
 
 <table>
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='roleAccess.createdOn'/></b></td>
							<s:hidden name="roleAccess.createdOn" />
							<td style="width:120px"><s:date name="roleAccess.createdOn" format="dd-MMM-yyyy HH:mm"/></td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='roleAccess.createdBy' /></b></td>
							<c:if test="${not empty roleAccess.id}">
								<s:hidden name="roleAccess.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{roleAccess.createdBy}"/></td>
							</c:if>
							<c:if test="${empty roleAccess.id}">
								<s:hidden name="roleAccess.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='roleAccess.updatedOn'/></b></td>
							<s:hidden name="roleAccess.updatedOn"/>
							<td style="width:120px"><s:date name="roleAccess.updatedOn" format="dd-MMM-yyyy HH:mm"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='roleAccess.updatedBy' /></b></td>
							<s:hidden name="roleAccess.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:85px"><s:label name="roleAccess.updatedBy" value="${pageContext.request.remoteUser}" /></td>
						</tr>
					</tbody>
				</table> 
 
        <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="save" key="button.save"/>
        <td></td>   
        <s:reset type="button" cssClass="cssbutton" cssStyle="width:55px; height:25px" key="button.reset"/>

		
</s:form>   
  
<script type="text/javascript">   
    Form.focusFirstElement($("roleAccessForm"));   
</script>
