package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DataSecurityFilter;

public interface DataSecurityFiltersDao extends GenericDao<DataSecurityFilter, Long> {
	public List findMaximumId();
	
	public List findFilters(String filter);
	
	public List getTableList(String sessionCorpID);
	
	public List getFieldList(String tableName);
	
	public List getAgentFilterList(String prefix, String sessionCorpID);
	
	public void deleteFilters(String prefix, String sessionCorpID);
	
	public List search(String name, String tableName, String fieldName, String filterValues);
	
	public List getOtherCorpidFilterList(String prefix, String corpidList);
}
