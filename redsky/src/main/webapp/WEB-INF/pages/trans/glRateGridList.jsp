<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title>Gl Rate Grid List</title>   
    <meta name="heading" content="Gl Rate Grid List"/> 
<style>
 

 </style>
<script type="text/javascript">
function clear_fields(){
	document.forms['GlRateGridList'].elements['jobType'].value = '';
	document.forms['GlRateGridList'].elements['routing1'].value = '';
	document.forms['GlRateGridList'].elements['recGl'].value = '';
	document.forms['GlRateGridList'].elements['payGl'].value = '';
}
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove it?");
	var id = targetElement;
	var costId="${costId}";
	if (agree){
		location.href="deleteGlRateGrid.html?costId="+costId+"&id="+id+"&decorator=popup&popup=true";		
	}else{
		return false;
	}
} 
</script>
 </head>
<s:form name="GlRateGridList" action="searchGlRateGridListPopUp.html?costId=${costId}&decorator=popup&popup=true" method="post" validate="true">

<c:set var="costId" value="${costId}"/>

<div id="layer1" style="width:100%;"><!--
<div id="newmnav" style="!padding-bottom:5px;">
	  <ul>
	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>	  	
	  </ul>
</div>
<div class="spn">&nbsp;</div> 	
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/>     
</c:set>
<div id="content" align="center">
<div id="liquid-round" style="margin:0px;">
    <div class="top"><span></span></div>
    <div class="center-content">
		<table class="table" border="0">
		<thead>
		<tr>
		<th>Job Type</th>
		<th>Routing</th>		
		<th>Receivable GL</th>
		<th>Payable GL</th>		
		</tr>
		</thead>	
		<tbody>
				<tr>
  				    <td align="left">
					    <s:select name="jobType" list="%{job}" headerKey="" headerValue="" cssClass="list-menu"/>
					</td>
					<td align="left">
					    <s:select name="routing1" list="%{routing}" headerKey="" headerValue="" cssClass="list-menu"/>					    
					</td>					
  				    <td align="left">
					    <s:select name="recGl" list="%{glcodes}" headerKey="" headerValue="" cssClass="list-menu"/>
					</td>
					<td align="left">
   					    <s:select name="payGl" list="%{glcodes}" headerKey="" headerValue="" cssClass="list-menu"/>
					</td>					
				</tr>		
				<tr><td ></td><td ></td><td ></td><td align="right" colspan="0"><c:out value="${searchbuttons}" escapeXml="false"/></td></tr>			
		</tbody>
		</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
--><div id="newmnav">
	  <ul>
	  	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Gl Rate Grid List</span></a></li>	  	
				<li><a href="editGlRateGrid.html?costId=${costId}&decorator=popup&popup=true" ><span>Gl Rate Grid Details</span></a></li>					  	
	  </ul>
</div>
<div style="width: 100%" >
<div class="spn">&nbsp;</div>
</div>
<div id="content" align="center">
<div id="Layer1" style="width: 100%;">

<table  width="100%" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>				
<display:table name="glCodeRateGridList" class="table" id="glCodeRateGridList" requestURI="" pagesize="25" style="width:100%;"> 
            <display:column title="Company Division">
			<c:if test="${glCodeRateGridList.companyDivision==''}">
			<a style="color:#fff; width:100%"  onclick="location.href='<c:url value="/editGlRateGrid.html?costId=${costId}&decorator=popup&popup=true&id=${glCodeRateGridList.id}"/>'"><div style="color:#fff; width:100% ; height: 100%" /></a>
			</c:if>
			<c:if test="${glCodeRateGridList.companyDivision!=''}">
			<a onclick="location.href='<c:url value="/editGlRateGrid.html?costId=${costId}&decorator=popup&popup=true&id=${glCodeRateGridList.id}"/>'"><c:out value="${glCodeRateGridList.companyDivision}"/></a>
			</c:if>
			</display:column>

			<display:column title="Job Type">
			<c:if test="${glCodeRateGridList.gridJob==''}">
			<a style="color:#fff; width:100%"  onclick="location.href='<c:url value="/editGlRateGrid.html?costId=${costId}&decorator=popup&popup=true&id=${glCodeRateGridList.id}"/>'"><div style="color:#fff; width:100% ; height: 100%" /></a>
			</c:if>
			<c:if test="${glCodeRateGridList.gridJob!=''}">
			<a onclick="location.href='<c:url value="/editGlRateGrid.html?costId=${costId}&decorator=popup&popup=true&id=${glCodeRateGridList.id}"/>'"><c:out value="${glCodeRateGridList.gridJob}"/></a>
			</c:if>
			</display:column>
			<display:column title="Routing">
			<c:if test="${glCodeRateGridList.gridRouting==''}">
			<a style="color:#fff; width:100%"  onclick="location.href='<c:url value="/editGlRateGrid.html?costId=${costId}&decorator=popup&popup=true&id=${glCodeRateGridList.id}"/>'"><div style="color:#fff; width:100% ; height: 100%" /></a>
			</c:if>
			<c:if test="${glCodeRateGridList.gridRouting!=''}">
			<a onclick="location.href='<c:url value="/editGlRateGrid.html?costId=${costId}&decorator=popup&popup=true&id=${glCodeRateGridList.id}"/>'"><c:out value="${glCodeRateGridList.gridRouting}"/></a>
			</c:if>
			</display:column>			
				
	<display:column property="gridRecGl" title="Receivable GL"/>
		<display:column property="gridPayGl" title="Payable GL"/>
		<display:column title="Remove" style="text-align:center; width:20px;">
		<a><img align="middle" onclick="confirmSubmit(${glCodeRateGridList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
		</display:column>
		</display:table>
			</td>
		</tr>
	</tbody>
</table> 
<c:set var="buttons"> 
    <input type="button" class="cssbutton" style="width:55px; height:25px;" onclick="location.href='<c:url value="editGlRateGrid.html?costId=${costId}&decorator=popup&popup=true	"/>'" value="<fmt:message key="button.add"/>"/> 
</c:set> 
<c:out value="${buttons}" escapeXml="false" /> 
</div>
</div>

</s:form>
<script type="text/javascript">
 
try{
	<c:if test="${successMsg!=null && successMsg!=''}">
	alert('${successMsg}');
	</c:if>
}catch(e){}
</script>
