<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ include file="/common/taglibs.jsp"%>

<style>
body {padding-bottom:100px !important;
} 
.pager {
			padding: 0 20px 0px 20px;
		}
		
		/* define table skin */
		table.grid {
			margin: 0;
			padding: 0;
			border-collapse: collapse;
			border-spacing: 0;
			width: 100%;
		}
		
		table.grid thead, table.grid .collapsible {
			background-color: rgba(0, 0, 0, 0) linear-gradient(to bottom, #b4e7fb 0%, #c6e8f8 51%, #a8d8f4 100%) repeat scroll 0 0;
		}

		table.grid th {
		background: #b4e7fb; /* Old browsers */
		background: -moz-linear-gradient(top, #b4e7fb 0%, #c6e8f8 51%, #a8d8f4 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #b4e7fb 0%,#c6e8f8 51%,#a8d8f4 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #b4e7fb 0%,#c6e8f8 51%,#a8d8f4 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4e7fb', endColorstr='#a8d8f4',GradientType=0 ); /* IE6-9 */
	    border-color: #3dafcb;
	    border-style: solid;
	    border-width: 1px;
	    color: #15428b;
	    font-family: arial,verdana;
	    font-size: 11px;
	    font-weight: bold;
	    height: 16px;
	    padding: 1px 2px;
	    text-decoration: none;
		cursor: pointer;
		} 
		
		/* th.headerSortUp span{background: url("images/arrow_up.png") right center no-repeat;}
		th.headerSortDown span{background: url("images/arrow_down.png") right center no-repeat;} */
		
		table.grid th span {
			padding: 4px 0px 4px 2px;
			font-weight: bold;
		}
		
		table.grid tr.even {
			background-color: #fff;
		}
		
		/* expand/collapse */
		table.grid .collapsible {
			padding: 0 0 3px 0;
		}

		.collapsible a.collapsed {
			margin: 2px;
			display: block;
			width: 15px;
			height: 15px;
			background: url(images/arrow_down.png) no-repeat 3px 3px;
			outline: 0;
		}
		
		.collapsible a.expanded {
			margin: 2px;
			display: block;
			width: 15px;
			height: 15px;
			background: url(images/arrow_up.png) no-repeat 3px 3px;
			outline: 0;
		}
		a.fxAcc{
			color: #0380e1;
		    cursor: pointer;
		    font-family: arial,helvetica,serif;
		    text-decoration: underline;
		    font-size:11px;
		}
		.loading-indicator {
		  position: absolute;
		  background-color: #fff;
		  padding:20px;
		  border-radius:5px;
		  left: 45%;
		  top: 40%;
		}
</style>
<script type="text/javascript">

</script>

<div id="accountLineRows" style="margin-top:20px;">
<s:hidden name="accountLineStatus" value="${accountLineStatus}"/>
<c:set var="SIFieldLockingFlag" value="N" />
<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount"> 
<c:set var="SIFieldLockingFlag" value="Y" />
</configByCorp:fieldVisibility>
<s:hidden name="chargeCodeValidationVal"  id= "chargeCodeValidationVal" />
<s:hidden name="chargeCodeValidationMessage"  id= "chargeCodeValidationMessage" />
<s:hidden name="accId"  id= "accId" value=""/>
<s:hidden name="accountLineSortValue" value="${accountLineSortValue}"/>
<s:hidden name="accountLineId"  id= "accountLineId" value=""/>
<tr>  
<td>
	<div id="pnav" style="margin-left:12px;">
  	<ul>
		<c:if test="${accountLineStatus=='true'}">
	    	<li><a onclick="findAccountLineActiveList();"><span><font color="#ff8000">Active List</font></span></a></li>
	    </c:if>
	    <c:if test="${accountLineStatus!='true'}">
	    	<li><a onclick="findAccountLineActiveList();"><span>Active List</span></a></li>
	    </c:if>
	    <c:if test="${accountLineStatus=='allStatus'}">
	    	<li><a onclick="findAccountlineAllList();"><span><font color="#ff8000">All List</font></span></a></li>
	    </c:if>
	    <c:if test="${accountLineStatus!='allStatus'}">
	    	<li><a onclick="findAccountlineAllList();"><span>All List</span></a></li>
	    </c:if>
			</ul>
	</div>
	<c:if test="${accountInterface=='Y'}">
	<div class="key_acc_price" style="width:50%;margin-left:150px;">&nbsp;</div>
	</c:if>
</td>
</tr>

<div id="pager" class="pager" style="text-align: right; width: 96%; margin-top:-23px;">
			<input type="button" value="&lt;&lt;" class="pageFirst pg-normal" id="pageFirst"/>
			<input type="button" value="&lt;" class="pagePrev pg-normal" id="pagePrev"/>
			<input type="text" class="pagedisplay input-textUpper pr-f11" style="text-align: center;width:30px" id="pagedisplay"/>
			<input type="button" value="&gt;" class="pageNext pg-normal" id="pageNext"/>
			<input type="button" value="&gt;&gt;" class="pageLast pg-normal" id="pageLast"/>
			<select class="pagesize list-menu pr-f11" id="pagesize">
				<option value="10">10</option>
				<option value="25" selected="selected">25</option>
				<option value="50">50</option>
				<option value="100">100</option>
				<option value="150">150</option>
				<option value="200">200</option>
				<option value="250">250</option>
				<option value="250">300</option>
				<option value="250">350</option>
				<option value="250">400</option>
			</select>
</div>
<div style="text-align: right; width: 81%; margin-top:-19px;">
<a id="expandList">
<img id="show" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP /></a>
<a id="collapseList">
<img id="hide" src="${pageContext.request.contextPath}/images/minus1-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP /></a>
<font class="listwhitetext"><b>Filter By :</b></font>		
<s:select cssClass="list-menu pr-f11" id="fieldName" name="fieldName" list="%{fieldNameList}" cssStyle="width:70px" onchange="getFieldValueList(this)"/>	 
<font class="listwhitetext"><b>Value</font>		
<s:select cssClass="list-menu pr-f11" id="fieldValue" name="fieldValue" list="%{fieldList}" cssStyle="width:70px" onchange="getAccountLineList(this)" headerKey="" headerValue=""/>
</div>
<div class="spn" style="!margin-bottom:8px;"></div>
<c:if test="${accountLineList!='[]'}">
	<table  class="table-price grid tablesorter" id="dataTableD" style="width:98%;margin-top:0px;margin-bottom:0px;" border="0" cellpadding="0" cellspacing="0"> 
			<thead> 
				<tr>
					<th style="width:18px;"></th>
					<th>Audit&nbsp;&nbsp;&nbsp;</th>
					<th onclick="sortAccountLineList('accountLineNumber')">&nbsp;#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Active&nbsp;&nbsp;&nbsp;</th>
					<th onclick="sortAccountLineList('category')">Category<font color="red" size="2">*</font>&nbsp;&nbsp;&nbsp;</th>
					<c:if  test="${compDivFlag == 'Yes'}">
						<th onclick="sortAccountLineList('companyDivision')">Division<font color="red" size="2">*</font>&nbsp;&nbsp;&nbsp;&nbsp;</th>
					</c:if>
					<c:if test="${checkContractChargesMandatory=='1'}">
						<th onclick="sortAccountLineList('chargeCode')">Charge&nbsp;Code<font color="red" size="2">*</font>&nbsp;&nbsp;&nbsp;</th>
					</c:if>
					<c:if test="${checkContractChargesMandatory!='1'}">
						<th onclick="sortAccountLineList('chargeCode')">Charge&nbsp;Code&nbsp;&nbsp;&nbsp;</th>
					</c:if>
					<th onclick="sortAccountLineList('vendorCode')">Vendor&nbsp;Code&nbsp;&nbsp;&nbsp;</th>
					<th onclick="sortAccountLineList('estimateVendorName')">Vendor&nbsp;Name&nbsp;&nbsp;&nbsp;</th>
					<th onclick="sortAccountLineList('billToCode')">Bill&nbsp;To&nbsp;Code&nbsp;&nbsp;&nbsp;</th>
					<th onclick="sortAccountLineList('billToName')">Bill&nbsp;To&nbsp;Name&nbsp;&nbsp;&nbsp;</th>
					<th onclick="sortAccountLineList('basis')">Basis&nbsp;&nbsp;&nbsp;&nbsp;</th>
					<c:if test="${systemDefaultVatCalculationNew=='Y'}">
						<th  onclick="sortAccountLineList('recVatDescr')">Rec&nbsp;VAT&nbsp;Desc&nbsp;&nbsp;&nbsp;</th>
						<th  onclick="sortAccountLineList('recVatPercent')">VAT&nbsp;%&nbsp;&nbsp;&nbsp;</th>
						<th  onclick="sortAccountLineList('payVatDescr')">Pay&nbsp;VAT&nbsp;Desc&nbsp;&nbsp;&nbsp;</th>
						<th  onclick="sortAccountLineList('payVatPercent')">VAT&nbsp;%&nbsp;&nbsp;&nbsp;</th>
					</c:if>
				</tr>
			</thead>
		<tbody>
        				
			<c:forEach items="${accountLineList}" var="accountLineList">
			<c:if test="${accountLineList.status}">
				<tr class="even">
					 <td style="width:15px;" rowspan="1" class="collapsible"></td>
					 <td style="width:62px; text-align: center;">
					 	<a><img align="middle" onclick="window.open('auditList.html?id=${accountLineList.id}&tableName=accountLine&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')" style="margin: 0px 0px 0px 0px;" src="images/report-ext.png"></a>
					 </td>
			
					 <td style="width:65px;">
					 	<input type="text" style="text-align:left" name="accountLineNumber${accountLineList.id}" value="${accountLineList.accountLineNumber }" size="1" maxlength="3" class="input-text pr-f11" onkeypress="return isNumber(event)" onchange="checkAccountLineNumber('accountLineNumber${accountLineList.id}');getId('${accountLineList.id}');" />
		    			<img id="target" style="margin-left:8px;" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
		 					
					 </td>
			
					 <td>
						 <select name ="category${accountLineList.id}" id="category${accountLineList.id}" style="width:70px" onchange="selectPricingMarketPlace('category${accountLineList.id}','${accountLineList.id}');getId('${accountLineList.id}');" class="list-menu pr-f11"> 
							  <c:forEach var="chrms" items="${category}" varStatus="loopStatus">
								   <c:choose>
									    <c:when test="${chrms.key == accountLineList.category}">
									        <c:set var="selectedInd" value=" selected"></c:set>
									    </c:when>
									    <c:otherwise>
										 <c:set var="selectedInd" value=""></c:set>
										</c:otherwise>
								    </c:choose>
								    <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
								     <c:out value="${chrms.value}"></c:out>
								    </option>
							  </c:forEach> 
						 </select>
					 </td>
					 
					 <c:if  test="${compDivFlag == 'Yes'}">
						 <td>
							 <select name ="companyDivision${accountLineList.id}" id="companyDivision${accountLineList.id}" onchange="getId('${accountLineList.id}');" style="width:57px" class="list-menu pr-f11"> 
								  <c:forEach var="chrms" items="${companyDivisionMap}" varStatus="loopStatus">
									   <c:choose>
										    <c:when test="${chrms.key == accountLineList.companyDivision}">
										        <c:set var="selectedInd" value=" selected"></c:set>
										    </c:when>
										    <c:otherwise>
											 <c:set var="selectedInd" value=""></c:set>
											</c:otherwise>
									    </c:choose>
									    <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
									     <c:out value="${chrms.value}"></c:out>
									    </option>
								  </c:forEach> 
							 </select>
						 </td>
			 		</c:if>
			 		
					 <td>
						 <c:choose>
						 <c:when test="${(not empty accountLineList.accrueRevenue || not empty accountLineList.accrueRevenueVariance || not empty accountLineList.accruePayable || not empty accountLineList.accrueExpenseVariance) && (SIFieldLockingFlag=='Y')}">
						 <input  class="input-textUpper pr-f11" type="text" id="chargeCode${accountLineList.id}" name="chargeCode${accountLineList.id}"  value="${accountLineList.chargeCode }" title="${accountLineList.chargeCode}" size="10" maxlength="25" readonly="readonly"/>								
						 </c:when>
						 <c:otherwise>
						 <c:if test="${accountInterface!='Y'}">
					   		<input  class="input-text pr-f11" type="text" id="chargeCode${accountLineList.id}" name="chargeCode${accountLineList.id}"  value="${accountLineList.chargeCode }" title="${accountLineList.chargeCode}" size="10" maxlength="25" class="input-text pr-f11" onkeyup="autoCompleterAjaxCallChargeCode(${accountLineList.id},'ChargeNameDivId${accountLineList.id}','${accountLineList.contract}')" onblur="updateContractChangeCharge('${accountLineList.id}','${accountLineList.contract}');fillRevQtyLocalAmounts('${accountLineList.id}');driverCommission('${accountLineList.id}')" onchange="checkChargeCode('${accountLineList.id}');getId('${accountLineList.id}');"  onkeydown="return noQuotes(event);"/>								
							<img id="chargeCodeOpenpopup${accountLineList.id}" class="openpopup" style="text-align:right;vertical-align:top;" width="17" height="20" onclick="chk('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');findInternalCostVendorCode('${accountLineList.id}');"  src="<c:url value='/images/open-popup.gif'/>" />
							<div id="ChargeNameDivId${accountLineList.id}" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
					   	</c:if>
					   <c:if test="${accountInterface=='Y'}">
					   <c:if test="${!accountLineList.utsiRecAccDateFlag && !accountLineList.utsiPayAccDateFlag}"> 
					   <c:if test="${( (accNonEditFlag && (accountLineList.recInvoiceNumber=='' || empty accountLineList.recInvoiceNumber )) || (!accNonEditFlag && empty accountLineList.recAccDate)) && empty accountLineList.payAccDate}">
						   <input  class="input-text pr-f11" type="text" id="chargeCode${accountLineList.id}" name="chargeCode${accountLineList.id}"  value="${accountLineList.chargeCode }" title="${accountLineList.chargeCode}" size="10" maxlength="25" class="input-text pr-f11" onkeyup="autoCompleterAjaxCallChargeCode(${accountLineList.id},'ChargeNameDivId${accountLineList.id}','${accountLineList.contract}')" onblur="updateContractChangeCharge('${accountLineList.id}','${accountLineList.contract}');fillRevQtyLocalAmounts('${accountLineList.id}');driverCommission('${accountLineList.id}')" onchange="checkChargeCode('${accountLineList.id}');getId('${accountLineList.id}');"  onkeydown="return noQuotes(event);"/>								
							<img id="chargeCodeOpenpopup${accountLineList.id}" class="openpopup" style="text-align:right;vertical-align:top;" width="17" height="20" onclick="chk('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');findInternalCostVendorCode('${accountLineList.id}');"  src="<c:url value='/images/open-popup.gif'/>" />
							<div id="ChargeNameDivId${accountLineList.id}" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div> 
					   </c:if>
					   <c:if test="${(accNonEditFlag  && (accountLineList.recInvoiceNumber!='' && not empty accountLineList.recInvoiceNumber ))  || not empty accountLineList.recAccDate || not empty accountLineList.payAccDate}">
					     	<input  class="input-textUpper pr-f11" readonly="true"  type="text" id="chargeCode${accountLineList.id}" name="chargeCode${accountLineList.id}"  value="${accountLineList.chargeCode }" title="${accountLineList.chargeCode}" size="10" maxlength="25" />								
							<c:if test="${(!accNonEditFlag)}">
							<img id="chargeCodeOpenpopup${accountLineList.id}" class="openpopup" style="text-align:right;vertical-align:top;" width="17" height="20" onclick="javascript:alert('You can not change the Charge Code as Sent To Acc has been already filled.')"  src="<c:url value='/images/open-popup.gif'/>" />
							</c:if>
							<c:if test="${(accNonEditFlag)}">
							<img id="chargeCodeOpenpopup${accountLineList.id}" class="openpopup" style="text-align:right;vertical-align:top;" width="17" height="20" onclick="javascript:alert('You can not change the Charge Code as invoice has been generated.)"  src="<c:url value='/images/open-popup.gif'/>" />
							</c:if>
					   </c:if> 
					   </c:if>
					   <c:if test="${accountLineList.utsiRecAccDateFlag || accountLineList.utsiPayAccDateFlag}">
						   <input  class="input-textUpper pr-f11" readonly="true"  type="text" id="chargeCode${accountLineList.id}" name="chargeCode${accountLineList.id}"  value="${accountLineList.chargeCode }" title="${accountLineList.chargeCode}" size="10" maxlength="25" />
						   <img id="chargeCodeOpenpopup${accountLineList.id}" align="top" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Charge Code as sent to Accounting and/or already Invoiced in UTSI Instance')" id="openpopup7.img" src="<c:url value='/images/open-popup.gif'/>" /> 
					   </c:if> 
					   </c:if>
						</c:otherwise></c:choose> 
					  </td>
					  
					  <td> 
					    <c:choose>
						    <c:when test="${empty accountLineList.payAccDate}">
							  	<input type="text" name="vendorCode${accountLineList.id}" id="vendorCode${accountLineList.id}" value="${accountLineList.vendorCode }" size="6" class="input-text pr-f11" onchange="checkPurchaseInvoiceProcessing(this,'VEN','${accountLineList.id}');driverCommission('${accountLineList.id}');getId('${accountLineList.id}');" onfocus="enablePreviousFunction('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}','${accountLineList.contract}');"/>
							  	<img id="vendorCodeOpenpopup${accountLineList.id}" class="openpopup" style="text-align:right;vertical-align:top;" width="17" height="20" onclick="javascript:winOpenForActgCode('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');" src="<c:url value='/images/open-popup.gif'/>" />
							  	<img  class="openpopup"  id="hidMarketPlace${accountLineList.id}" style="text-align:right;vertical-align:top;display:none;" onclick="javascript:marketPlaceCode('vendorCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}','estimateVendorName${accountLineList.id}','estimateRate${accountLineList.id}','${contractType}');"   src="<c:url value='/images/market-areaPP1.png'/>" />
								<c:if test="${accountLineList.category=='Destin' || accountLineList.category=='Origin' }">
									<c:if test="${pricePointFlag}">
										<c:if test="${accountLineList.externalIntegrationReference=='PP'}">
										 	<img  class="openpopup"  id="hidMarketPlace11${accountLineList.id}" style="text-align:right;vertical-align:top;" onclick="javascript:marketPlaceCode('vendorCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}','estimateVendorName${accountLineList.id}','estimateRate${accountLineList.id}','${contractType}');"   src="<c:url value='/images/market-areaPP1.png'/>" />
									    </c:if>
									    <c:if test="${accountLineList.externalIntegrationReference!='PP'}">
											<img  class="openpopup"  id="hidMarketPlace11${accountLineList.id}" style="text-align:right;vertical-align:top;" onclick="javascript:marketPlaceCode('vendorCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}','estimateVendorName${accountLineList.id}','estimateRate${accountLineList.id}','${contractType}');"   src="<c:url value='/images/market-areaPP1.png'/>" />
									    </c:if>
								    </c:if>
							    </c:if>
						    </c:when>
						    <c:otherwise>
						      <input type="text" id="vendorCode${accountLineList.id}" name="vendorCode${accountLineList.id}"  value="${accountLineList.vendorCode }" size="6" class="input-textUpper pr-f11" readonly="true"/>
						    </c:otherwise>
					    </c:choose>
					    <!-- Colour Coding For Amount And Invoice No. Starts -->
					    <c:choose>
							    <c:when test="${accountLineList.actualExpense != '0.00' && accountLineList.actualExpense != '0'}">
									<c:if test="${accountInterface=='Y'}">
									<c:if test="${!payablesXferWithApprovalOnly}">
											<c:choose>
												<c:when test="${not empty accountLineList.payPostDate && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
													<font class="invorange"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${not empty accountLineList.payPostDate && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
													<font class="invgreen"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${not empty accountLineList.payPostDate && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount !='0' && accountLineList.payPayableAmount !='0.00' && accountLineList.payPayableAmount != null && accountLineList.payPayableAmount != '')}">
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${empty accountLineList.payPostDate && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '')}">
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${empty accountLineList.payPostDate && empty accountLineList.payAccDate && accountLineList.invoiceNumber!='' && fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && accountLineList.payingStatus=='A' && (accountLineList.payPayableAmount !='0' && accountLineList.payPayableAmount !='0.00' && accountLineList.payPayableAmount != null && accountLineList.payPayableAmount != '')}">
													<font class="invorange"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:otherwise>
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:otherwise>
											</c:choose>
										</c:if>
										<c:if test="${payablesXferWithApprovalOnly}">
											<c:choose>
												<c:when test="${(not empty accountLineList.payPostDate ) && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
													<font class="invorange"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${(accountLineList.payingStatus=='A'|| not empty accountLineList.payPostDate) && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
													<font class="invgreen"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${(accountLineList.payingStatus=='A'|| not empty accountLineList.payPostDate) && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount !='0' && accountLineList.payPayableAmount !='0.00' && accountLineList.payPayableAmount != null && accountLineList.payPayableAmount != '')}">
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${(accountLineList.payingStatus=='A' || not empty accountLineList.payPostDate) && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '')}">
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${empty accountLineList.payPostDate && empty accountLineList.payAccDate && accountLineList.invoiceNumber!='' && fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && accountLineList.payingStatus=='A' && (accountLineList.payPayableAmount !='0' && accountLineList.payPayableAmount !='0.00' && accountLineList.payPayableAmount != null && accountLineList.payPayableAmount != '')}">
													<font class="invorange"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:otherwise>
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:otherwise>
											</c:choose>
										</c:if>
									</c:if>
								</c:when>
								<c:when test="${accountLineList.revisionExpense != '0.00' && accountLineList.revisionExpense != '0'}">
									<font class="invblue"><b><c:out value="Rev ${accountLineList.revisionExpense}" /></b></font>
								</c:when>
								<c:when test="${accountLineList.estimateExpense != '0.00' && accountLineList.estimateExpense != '0'}">
									<font class="invblue"><b><c:out value="Est ${accountLineList.estimateExpense}" /></b></font>
								</c:when>
								<c:when test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
									<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
								</c:when>
								<c:otherwise></c:otherwise>
						</c:choose>
						<!-- Colour Coding For Amount And Invoice No. Ends --> 
					 </td>
					  
					  <td>
						<c:choose>
						    <c:when test="${empty accountLineList.payAccDate}">
								<input type="text" name="estimateVendorName${accountLineList.id}" id="estimateVendorName${accountLineList.id}" value="${accountLineList.estimateVendorName }" title="${accountLineList.estimateVendorName}" onkeyup="findPartnerDetailsPriceing('estimateVendorName${accountLineList.id}','vendorCode${accountLineList.id}','estimateVendorNameDivId${accountLineList.id}',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','${accountLineList.id}',event);" onchange="findPartnerDetailsByName('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}');getId('${accountLineList.id}');" size="15" class="input-text pr-f11" style="width:90%;"/> 
								<div id="estimateVendorNameDivId${accountLineList.id}" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
							</c:when>
							<c:otherwise>
								<input type="text" name="estimateVendorName${accountLineList.id}" id="estimateVendorName${accountLineList.id}" value="${accountLineList.estimateVendorName }" title="${accountLineList.estimateVendorName}" readonly="true" size="15" class="input-textUpper pr-f11" style="width:90%;" />
							</c:otherwise>
						</c:choose>
					</td>
					  
					  <td> 
						<input type="text" id="billToCode${accountLineList.id}" name="billToCode${accountLineList.id}"  value="${accountLineList.billToCode }" size="6" class="input-textUpper pr-f11 " readonly="true" />
							<c:if test="${accountInterface=='Y'}">
								<c:if test="${!accountLineList.utsiRecAccDateFlag  && !accountLineList.utsiPayAccDateFlag}"> 
									<c:if test="${(!accNonEditFlag && empty accountLineList.recAccDate) || (accNonEditFlag && (accountLineList.recInvoiceNumber=='' || empty accountLineList.recInvoiceNumber))}"> 
										  <c:if test="${receivableSectionDetail==true}"> 
										   		<img align="top" id="accountLineBillToCodeOpenpopup${accountLineList.id}"  class="openpopup" width="17" height="20" onclick="javascript:openWindow('accountLineBillToCode.html?id=${serviceOrder.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billToName${accountLineList.id}&fld_code=billToCode${accountLineList.id}');" id="openpopup8.img" src="<c:url value='/images/open-popup.gif'/>" />
										   </c:if>
									 </c:if>
									<c:if test="${(!accNonEditFlag && not empty accountLineList.recAccDate) || (accNonEditFlag && (accountLineList.recInvoiceNumber!='' && not empty accountLineList.recInvoiceNumber))}"> 
										<c:if test="${receivableSectionDetail==true}"> 
										   <c:if test="${(!accNonEditFlag)}">
										  		<img align="top" id="accountLineBillToCodeOpenpopup${accountLineList.id}" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Billing Party as Sent To Acc has been already filled.')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
											</c:if>
											<c:if test="${(accNonEditFlag)}">
										  		<img align="top" id="accountLineBillToCodeOpenpopup${accountLineList.id}" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Billing Party as invoice has been generated.')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
											</c:if>
										</c:if>
									</c:if>
								</c:if>
								<c:if test="${accountLineList.utsiRecAccDateFlag || accountLineList.utsiPayAccDateFlag}">
									<c:if test="${receivableSectionDetail==true}"> 
									  	<img align="top" id="accountLineBillToCodeOpenpopup${accountLineList.id}" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Billing Party as sent to Accounting and/or already Invoiced in UTSI Instance')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
									</c:if> 
								</c:if>
							</c:if>
							<c:if test="${accountInterface!='Y'}">
								<c:if test="${accountLineList.recInvoiceNumber==''|| accountLineList.recInvoiceNumber==null}"> 
									  <c:if test="${receivableSectionDetail==true}"> 
									   		<img align="top" id="accountLineBillToCodeOpenpopup${accountLineList.id}" class="openpopup" width="17" height="20" onclick="checkBilltoCode();" id="openpopup10.img" src="<c:url value='/images/open-popup.gif'/>" />
									   </c:if>
								 </c:if>
								<c:if test="${accountLineList.recInvoiceNumber!=''&& accountLineList.recInvoiceNumber!=null}"> 
									<c:if test="${receivableSectionDetail==true}"> 
									  		<img align="top" id="accountLineBillToCodeOpenpopup${accountLineList.id}" class="openpopup" width="17" height="20" onclick="checkBilltoCode();" id="openpopup11.img" src="<c:url value='/images/open-popup.gif'/>" />  
									</c:if>
								</c:if>
							</c:if>
							<!-- Colour Coding For Amount And Invoice No. Starts -->
							<c:choose>
							    <c:when test="${accountLineList.actualRevenue != '0.00' && accountLineList.actualRevenue != '0'}">
									<c:if test="${accountInterface=='Y'}">
									       <c:choose> 
										       <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
											       <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
												   <font class="invorange"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
												   <c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
												</c:if>
											       <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
												   <font class="invred"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
												</c:if>
											</c:when>
											<c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
												<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
													<font class="invgreen"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
												</c:if>
												<c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
													<font class="invred"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
											   </c:if>
											</c:when> 
											<c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid')}">
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
											</c:when>
											<c:when test="${empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
												<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
											   </c:if>
											   <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
													<font class="invred"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
											   </c:if>
											</c:when>
											<c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid') && fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && (accountLineList.receivedAmount !='0' && accountLineList.receivedAmount !='0.00' && accountLineList.receivedAmount != null && accountLineList.receivedAmount != '')}">
													<font class="invred"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
											 </c:when>
											 <c:otherwise>
												<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
											    </c:if>
											    <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
													<font class="invred"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
											    </c:if>
											 </c:otherwise>
										</c:choose>
									</c:if> 
									<c:if test="${accountInterface!='Y'}">
										 <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
											   <c:choose>
												    <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid') && fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && (accountLineList.receivedAmount !='0' && accountLineList.receivedAmount !='0.00' && accountLineList.receivedAmount != null && accountLineList.receivedAmount != '')}">
														<font class="invorange"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													    <c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
															<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
														</c:if>
												  </c:when>
												  <c:otherwise>
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>          
												  </c:otherwise>
											   </c:choose>
										   </c:if>
										    <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
													<font class="invred"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
										    </c:if>
									</c:if>
								</c:when>
								<c:when test="${((accountLineList.distributionAmount != '0.00' && accountLineList.distributionAmount != '0') && (accountLineList.actualRevenue == '0.00' || accountLineList.actualRevenue == '0'))}">
									<font class="invblue"><b><c:out value="Dis ${accountLineList.distributionAmount}"/></b></font>
									<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
										<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
									</c:if>
								</c:when>
								<c:when test="${accountLineList.revisionRevenueAmount != '0.00' && accountLineList.revisionRevenueAmount != '0'}">
									<font class="invblue"><b><c:out value="Rev ${accountLineList.revisionRevenueAmount}"/></b></font>
								</c:when>
								<c:when test="${accountLineList.estimateRevenueAmount != '0.00' && accountLineList.estimateRevenueAmount != '0'}">
									<font class="invblue"><b><c:out value="Est ${accountLineList.estimateRevenueAmount}"/></b></font>
								</c:when>
								<c:when test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber !=''}">
									<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>
							<!-- Colour Coding For Amount And Invoice No. End -->
					 </td>
			
					 <td>
						<input type="text" id="billToName${accountLineList.id}" name="billToName${accountLineList.id}" readonly="true" value="${accountLineList.billToName }" title="${accountLineList.billToName}" size="15" class="input-textUpper pr-f11"  /> 
					</td>
			
					<td style="width:2%"> 
						<c:choose>
						 <c:when test="${(not empty accountLineList.accrueRevenue || not empty accountLineList.accrueRevenueVariance || not empty accountLineList.accruePayable || not empty accountLineList.accrueExpenseVariance) && (SIFieldLockingFlag=='Y')}">
						<input type="text" name ="basis${accountLineList.id}"  value="${accountLineList.basis}" style="width:50px;text-align:left;" class="input-textUpper pr-f11" readonly="readonly"> 
						</c:when>
						<c:otherwise>
						<select id="basis${accountLineList.id}" name ="basis${accountLineList.id}" style="width:54px" class="list-menu pr-f11" onchange="changeBasis('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}',this);getId('${accountLineList.id}');"> 
							<c:forEach var="chrms" items="${basis}" varStatus="loopStatus">
			                     <c:choose>
				                      <c:when test="${chrms.value == accountLineList.basis}">
				                      		<c:set var="selectedInd" value=" selected"></c:set>
				                      </c:when>
				                      <c:otherwise>
				                       		<c:set var="selectedInd" value=""></c:set>
				                      </c:otherwise>
			                      </c:choose>
				                     <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
				                    		<c:out value="${chrms.value}"></c:out>
				                    </option>
						     </c:forEach> 
						</select>
						</c:otherwise>
						</c:choose>
						<c:if test="${systemDefaultVatCalculationNew!='Y'}">
							<img id="rateImage"  class="openpopup" width="20" height="" align="top" src="${pageContext.request.contextPath}/images/acc_ctrl.png" onclick="showCheckBoxesInACList('${accountLineList.id}',this);"/>
					 	</c:if>
					 </td>
					 
					 <c:if test="${systemDefaultVatCalculationNew=='Y'}">
					 	<td>
							<c:if test="${empty accountLineList.recAccDate}">
								<select name ="recVatDescr${accountLineList.id}" style="width:75px" class="list-menu pr-f11" onchange="calculateVatSectionAll('REVENUE','${accountLineList.id}','BAS');getId('${accountLineList.id}');">																 								
									<c:forEach var="chrms" items="${estVatList}" varStatus="loopStatus">
		                                 <c:choose>
			                                  <c:when test="${chrms.key == accountLineList.recVatDescr}">
			                                  	<c:set var="selectedInd" value="selected"></c:set>
			                                  </c:when>
			                                 <c:otherwise>
			                                  	<c:set var="selectedInd" value=""></c:set>
			                                 </c:otherwise>
		                                 </c:choose>
			                                <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
			                               		<c:out value="${chrms.value}"></c:out>
			                               </option>
		                               </c:forEach> 
								</select>
							</c:if>
								<c:if test="${not empty accountLineList.recAccDate}">
									<c:set var="selectedVat" value=""></c:set>
										<c:forEach var="chrms" items="${estVatList}" varStatus="loopStatus">
			                                  <c:choose>
				                                  <c:when test="${chrms.key == accountLineList.recVatDescr}">
				                                  		<c:set var="selectedVat" value="${chrms.value}"></c:set>
				                                  </c:when>
				                                 <c:otherwise>
				                                 </c:otherwise>
			                                  </c:choose>
		                                </c:forEach>
		                                <input type="text" name ="recVatDescr${accountLineList.id}" value="${selectedVat}" style="width:70px" class="input-textUpper pr-f11 " readonly="true"> 
								</c:if>
						</td>
						<td>
							<input type="text" name ="recVatPercent${accountLineList.id}"  value="${accountLineList.recVatPercent}" style="width:20px;text-align:right;" class="input-text pr-f11" onchange="getId('${accountLineList.id}');"> 
						</td>
					</c:if>
					
					<c:if test="${systemDefaultVatCalculationNew=='Y'}">
						<td>	
							<c:if test="${empty accountLineList.payAccDate}">
								<select name ="payVatDescr${accountLineList.id}" style="width:75px" class="list-menu pr-f11"  onchange="calculateVatSectionAll('EXPENSE','${accountLineList.id}','BAS');getId('${accountLineList.id}');">																 								
									<c:forEach var="chrms" items="${payVatList}" varStatus="loopStatus">
		                                <c:choose>
			                                 <c:when test="${chrms.key == accountLineList.payVatDescr}">
			                                 	<c:set var="selectedInd" value="selected"></c:set>
			                                 </c:when>
		                                	<c:otherwise>
		                                 		<c:set var="selectedInd" value=""></c:set>
		                                	</c:otherwise>
		                                </c:choose>
			                               <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
			                              		<c:out value="${chrms.value}"></c:out>
			                              </option>
			                        </c:forEach> 
								</select>
							</c:if>
							<c:if test="${not empty accountLineList.payAccDate}">
								<c:set var="selectedVat" value=""></c:set>
									<c:forEach var="chrms" items="${payVatList}" varStatus="loopStatus">
		                                <c:choose>
			                                 <c:when test="${chrms.key == accountLineList.payVatDescr}">
			                                 	<c:set var="selectedVat" value="${chrms.value}"></c:set>
			                                 </c:when>
			                                <c:otherwise>
			                                </c:otherwise>
		                                </c:choose>
			                        </c:forEach>
			                        <input type="text" name ="payVatDescr${accountLineList.id}" value="${selectedVat}" style="width:70px" class="input-textUpper pr-f11 " readonly="true"> 
							</c:if>
					 	</td>
					 	<td>
							<input type="text" name ="payVatPercent${accountLineList.id}"  value="${accountLineList.payVatPercent}" style="width:20px;text-align:right;" class="input-text pr-f11" onchange="getId('${accountLineList.id}');"> 
							<img id="rateImage"  class="openpopup" width="20" height="" align="top" src="${pageContext.request.contextPath}/images/acc_ctrl.png" onclick="showCheckBoxesInACList('${accountLineList.id}',this);"/>
						</td>
					 </c:if>
				  			
					</tr>
					</c:if>
				<c:if test="${accountLineList.status==false}">
					<tr class="even">
					<td style="width:15px;" rowspan="1" class="collapsible"></td>
					 <td style="width:62px; text-align: center;">
					 	<a><img align="middle" onclick="window.open('auditList.html?id=${accountLineList.id}&tableName=accountLine&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')" style="margin: 0px 0px 0px 0px;" src="images/report-ext.png"></a>
					 </td>
			
					 <td style="">
					 	<input type="text" style="text-align:left" name="accountLineNumber${accountLineList.id}" value="${accountLineList.accountLineNumber }" size="1" maxlength="3" class="input-text pr-f11" disabled="disabled" />
						<img id="target" style="margin-left:8px;" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					 </td>
			
					 <td>
						 <select name ="category${accountLineList.id}" id="category${accountLineList.id}" style="width:70px" disabled="disabled" class="list-menu pr-f11"> 
							  <c:forEach var="chrms" items="${category}" varStatus="loopStatus">
								   <c:choose>
									    <c:when test="${chrms.key == accountLineList.category}">
									        <c:set var="selectedInd" value=" selected"></c:set>
									    </c:when>
									    <c:otherwise>
										 <c:set var="selectedInd" value=""></c:set>
										</c:otherwise>
								    </c:choose>
								    <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
								     <c:out value="${chrms.value}"></c:out>
								    </option>
							  </c:forEach> 
						 </select>
					 </td>
					 
					 <c:if  test="${compDivFlag == 'Yes'}">
						 <td>
							 <select name ="companyDivision${accountLineList.id}" id="companyDivision${accountLineList.id}" style="width:57px" class="list-menu pr-f11" disabled="disabled"> 
								  <c:forEach var="chrms" items="${companyDivisionMap}" varStatus="loopStatus">
									   <c:choose>
										    <c:when test="${chrms.key == accountLineList.companyDivision}">
										        <c:set var="selectedInd" value=" selected"></c:set>
										    </c:when>
										    <c:otherwise>
											 <c:set var="selectedInd" value=""></c:set>
											</c:otherwise>
									    </c:choose>
									    <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
									     <c:out value="${chrms.value}"></c:out>
									    </option>
								  </c:forEach> 
							 </select>
						 </td>
			 		</c:if>
			 		
					 <td>
						<input  class="input-text pr-f11" type="text" name="chargeCode${accountLineList.id}"  value="${accountLineList.chargeCode}" title="${accountLineList.chargeCode}" size="10"  maxlength="25" disabled="disabled"/>
					  </td>
			  
			  			<td> 
						    <c:choose>
							    <c:when test="${empty accountLineList.payAccDate}">
								  	<input type="text" name="vendorCode${accountLineList.id}" id="vendorCode${accountLineList.id}" value="${accountLineList.vendorCode }" size="6" class="input-text pr-f11" disabled="disabled"/>
									<c:if test="${accountLineList.category=='Destin' || accountLineList.category=='Origin' }">
										<c:if test="${pricePointFlag}">
											<c:if test="${accountLineList.externalIntegrationReference=='PP'}">
											 	<img  class="openpopup"  id="hidMarketPlace11${accountLineList.id}" style="text-align:right;vertical-align:top;" src="<c:url value='/images/market-areaPP1.png'/>" />
										    </c:if>
										    <c:if test="${accountLineList.externalIntegrationReference!='PP'}">
												<img  class="openpopup"  id="hidMarketPlace11${accountLineList.id}" style="text-align:right;vertical-align:top;" src="<c:url value='/images/market-areaPP1.png'/>" />
										    </c:if>
									    </c:if>
								    </c:if>
							    </c:when>
							    <c:otherwise>
							      <input type="text" id="vendorCode${accountLineList.id}" name="vendorCode${accountLineList.id}"  value="${accountLineList.vendorCode }" size="6" class="input-textUpper pr-f11 " disabled="disabled" />
							    </c:otherwise>
						    </c:choose>
						<!-- Colour Coding For Amount And Invoice No. Starts -->
					    	<c:choose>
							    <c:when test="${accountLineList.actualExpense != '0.00' && accountLineList.actualExpense != '0'}">
									<c:if test="${accountInterface=='Y'}">
									<c:if test="${!payablesXferWithApprovalOnly}">
											<c:choose>
												<c:when test="${not empty accountLineList.payPostDate && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
													<font class="invorange"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${not empty accountLineList.payPostDate && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
													<font class="invgreen"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${not empty accountLineList.payPostDate && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount !='0' && accountLineList.payPayableAmount !='0.00' && accountLineList.payPayableAmount != null && accountLineList.payPayableAmount != '')}">
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${empty accountLineList.payPostDate && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '')}">
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${empty accountLineList.payPostDate && empty accountLineList.payAccDate && accountLineList.invoiceNumber!='' && fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && accountLineList.payingStatus=='A' && (accountLineList.payPayableAmount !='0' && accountLineList.payPayableAmount !='0.00' && accountLineList.payPayableAmount != null && accountLineList.payPayableAmount != '')}">
													<font class="invorange"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:otherwise>
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:otherwise>
											</c:choose>
										</c:if>
										<c:if test="${payablesXferWithApprovalOnly}">
											<c:choose>
												<c:when test="${(not empty accountLineList.payPostDate ) && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
													<font class="invorange"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${(accountLineList.payingStatus=='A'|| not empty accountLineList.payPostDate) && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
													<font class="invgreen"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${(accountLineList.payingStatus=='A'|| not empty accountLineList.payPostDate) && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount !='0' && accountLineList.payPayableAmount !='0.00' && accountLineList.payPayableAmount != null && accountLineList.payPayableAmount != '')}">
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${(accountLineList.payingStatus=='A' || not empty accountLineList.payPostDate) && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '')}">
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:when test="${empty accountLineList.payPostDate && empty accountLineList.payAccDate && accountLineList.invoiceNumber!='' && fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && accountLineList.payingStatus=='A' && (accountLineList.payPayableAmount !='0' && accountLineList.payPayableAmount !='0.00' && accountLineList.payPayableAmount != null && accountLineList.payPayableAmount != '')}">
													<font class="invorange"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:when>
												<c:otherwise>
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualExpense}" /></b></font>
													<c:if test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
													</c:if>
												</c:otherwise>
											</c:choose>
										</c:if>
									</c:if>
								</c:when>
								<c:when test="${accountLineList.revisionExpense != '0.00' && accountLineList.revisionExpense != '0'}">
									<font class="invblue"><b><c:out value="Rev ${accountLineList.revisionExpense}" /></b></font>
								</c:when>
								<c:when test="${accountLineList.estimateExpense != '0.00' && accountLineList.estimateExpense != '0'}">
									<font class="invblue"><b><c:out value="Est ${accountLineList.estimateExpense}" /></b></font>
								</c:when>
								<c:when test="${accountLineList.invoiceNumber != null && accountLineList.invoiceNumber != ''}">
									<font class="invMain"><b>#<c:out value="${accountLineList.invoiceNumber}" /></b></font>
								</c:when>
								<c:otherwise></c:otherwise>
						</c:choose>
						<!-- Colour Coding For Amount And Invoice No. Ends -->
					 	</td>
					 <td >
						<c:choose>
						    <c:when test="${empty accountLineList.payAccDate}">
								<input type="text" name="estimateVendorName${accountLineList.id}" id="estimateVendorName${accountLineList.id}" value="${accountLineList.estimateVendorName }" title="${accountLineList.estimateVendorName}" readonly="true" size="15" class="input-text pr-f11" style="width:90%;" disabled="disabled"/> 
							</c:when>
							<c:otherwise>
								<input type="text" name="estimateVendorName${accountLineList.id}" id="estimateVendorName${accountLineList.id}" value="${accountLineList.estimateVendorName }" title="${accountLineList.estimateVendorName}" readonly="true" size="15" class="input-text pr-f11" style="width:90%;" disabled="disabled"/>
							</c:otherwise>
						</c:choose>
					</td>
			  
					  <td > 
						<input type="text" id="billToCode${accountLineList.id}" name="billToCode${accountLineList.id}"  value="${accountLineList.billToCode }" size="6" class="input-text pr-f11 " disabled="disabled" />
					 		<!-- Colour Coding For Amount And Invoice No. Starts -->
							<c:choose>
							    <c:when test="${accountLineList.actualRevenue != '0.00' && accountLineList.actualRevenue != '0'}">
									<c:if test="${accountInterface=='Y'}">
									       <c:choose> 
										       <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
											       <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
												   <font class="invorange"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
												   <c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
												</c:if>
											       <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
												   <font class="invred"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
												</c:if>
											</c:when>
											<c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
												<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
													<font class="invgreen"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
												</c:if>
												<c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
													<font class="invred"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
											   </c:if>
											</c:when> 
											<c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid')}">
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
											</c:when>
											<c:when test="${empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
												<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
											   </c:if>
											   <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
													<font class="invred"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
											   </c:if>
											</c:when>
											<c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid') && fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && (accountLineList.receivedAmount !='0' && accountLineList.receivedAmount !='0.00' && accountLineList.receivedAmount != null && accountLineList.receivedAmount != '')}">
													<font class="invred"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
											 </c:when>
											 <c:otherwise>
												<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
											    </c:if>
											    <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
													<font class="invred"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
											    </c:if>
											 </c:otherwise>
										</c:choose>
									</c:if> 
									<c:if test="${accountInterface!='Y'}">
										 <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
											   <c:choose>
												    <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid') && fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && (accountLineList.receivedAmount !='0' && accountLineList.receivedAmount !='0.00' && accountLineList.receivedAmount != null && accountLineList.receivedAmount != '')}">
														<font class="invorange"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													    <c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
															<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
														</c:if>
												  </c:when>
												  <c:otherwise>
													<font class="invblue"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>          
												  </c:otherwise>
											   </c:choose>
										   </c:if>
										    <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
													<font class="invred"><b><c:out value="Act ${accountLineList.actualRevenue}"/></b></font>
													<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
														<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
													</c:if>
										    </c:if>
									</c:if>
								</c:when>
								<c:when test="${((accountLineList.distributionAmount != '0.00' && accountLineList.distributionAmount != '0') && (accountLineList.actualRevenue == '0.00' || accountLineList.actualRevenue == '0'))}">
									<font class="invblue"><b><c:out value="Dis ${accountLineList.distributionAmount}"/></b></font>
									<c:if test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber != ''}">
										<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
									</c:if>
								</c:when>
								<c:when test="${accountLineList.revisionRevenueAmount != '0.00' && accountLineList.revisionRevenueAmount != '0'}">
									<font class="invblue"><b><c:out value="Rev ${accountLineList.revisionRevenueAmount}"/></b></font>
								</c:when>
								<c:when test="${accountLineList.estimateRevenueAmount != '0.00' && accountLineList.estimateRevenueAmount != '0'}">
									<font class="invblue"><b><c:out value="Est ${accountLineList.estimateRevenueAmount}"/></b></font>
								</c:when>
								<c:when test="${accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber !=''}">
									<font class="invMain"><b>#<c:out value="${accountLineList.recInvoiceNumber}" /></b></font>
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>
							<!-- Colour Coding For Amount And Invoice No. End -->
					 </td>
			
					 <td>
						<input type="text" id="billToName${accountLineList.id}" name="billToName${accountLineList.id}" readonly="true" value="${accountLineList.billToName }" title="${accountLineList.billToName}" size="15" class="input-text pr-f11"  disabled="disabled"/> 
					</td>
			
					<td style="width:2%"> 
					 <c:choose>
					 <c:when test="${(not empty accountLineList.accrueRevenue || not empty accountLineList.accrueRevenueVariance || not empty accountLineList.accruePayable || not empty accountLineList.accrueExpenseVariance) && (SIFieldLockingFlag=='Y')}">
					 <input type="text" name ="basis${accountLineList.id}"  value="${accountLineList.basis}" style="width:50px;text-align:left;" class="input-textUpper pr-f11" readonly="readonly"> 
					 </c:when>
					 <c:otherwise>
						<select id="basis${accountLineList.id}" name ="basis${accountLineList.id}" style="width:54px" class="list-menu pr-f11" disabled="disabled"> 
							<c:forEach var="chrms" items="${basis}" varStatus="loopStatus">
			                     <c:choose>
				                      <c:when test="${chrms.value == accountLineList.basis}">
				                      		<c:set var="selectedInd" value=" selected"></c:set>
				                      </c:when>
				                      <c:otherwise>
				                       		<c:set var="selectedInd" value=""></c:set>
				                      </c:otherwise>
			                      </c:choose>
				                     <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
				                    		<c:out value="${chrms.value}"></c:out>
				                    </option>
						     </c:forEach> 
						</select>
						</c:otherwise>
						</c:choose>
						<c:if test="${systemDefaultVatCalculationNew!='Y'}">
							<img id="rateImage"  class="openpopup" width="20" height="" align="top" src="${pageContext.request.contextPath}/images/acc_ctrl.png" onclick="showCheckBoxesInACList('${accountLineList.id}',this);"/>
					 	</c:if>
					 </td>
					 
					 <c:if test="${systemDefaultVatCalculationNew=='Y'}">
					 	<td>
							<c:if test="${empty accountLineList.recAccDate}">
								<select name ="recVatDescr${accountLineList.id}" style="width:75px" class="list-menu pr-f11" disabled="disabled">
									<c:forEach var="chrms" items="${estVatList}" varStatus="loopStatus">
		                                 <c:choose>
			                                  <c:when test="${chrms.key == accountLineList.recVatDescr}">
			                                  	<c:set var="selectedInd" value="selected"></c:set>
			                                  </c:when>
			                                 <c:otherwise>
			                                  	<c:set var="selectedInd" value=""></c:set>
			                                 </c:otherwise>
		                                 </c:choose>
			                                <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
			                               		<c:out value="${chrms.value}"></c:out>
			                               </option>
		                               </c:forEach> 
								</select>
							</c:if>
								<c:if test="${not empty accountLineList.recAccDate}">
									<c:set var="selectedVat" value=""></c:set>
										<c:forEach var="chrms" items="${estVatList}" varStatus="loopStatus">
			                                  <c:choose>
				                                  <c:when test="${chrms.key == accountLineList.recVatDescr}">
				                                  		<c:set var="selectedVat" value="${chrms.value}"></c:set>
				                                  </c:when>
				                                 <c:otherwise>
				                                 </c:otherwise>
			                                  </c:choose>
		                                </c:forEach>
		                                <input type="text" value="${selectedVat}" style="width:70px" class="input-textUpper pr-f11 " readonly="true"> 
								</c:if>
						</td>
						<td>
							<input type="text" name ="recVatPercent${accountLineList.id}"  value="${accountLineList.recVatPercent}" style="width:20px;text-align:right;" class="input-text pr-f11" disabled="disabled"> 
						</td>
					</c:if>
					
					<c:if test="${systemDefaultVatCalculationNew=='Y'}">
						<td>	
							<c:if test="${empty accountLineList.payAccDate}">
								<select name ="payVatDescr${accountLineList.id}" style="width:75px" class="list-menu pr-f11"  disabled="disabled">
									<c:forEach var="chrms" items="${payVatList}" varStatus="loopStatus">
		                                <c:choose>
			                                 <c:when test="${chrms.key == accountLineList.payVatDescr}">
			                                 	<c:set var="selectedInd" value="selected"></c:set>
			                                 </c:when>
		                                	<c:otherwise>
		                                 		<c:set var="selectedInd" value=""></c:set>
		                                	</c:otherwise>
		                                </c:choose>
			                               <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
			                              		<c:out value="${chrms.value}"></c:out>
			                              </option>
			                        </c:forEach> 
								</select>
							</c:if>
							<c:if test="${not empty accountLineList.payAccDate}">
								<c:set var="selectedVat" value=""></c:set>
									<c:forEach var="chrms" items="${payVatList}" varStatus="loopStatus">
		                                <c:choose>
			                                 <c:when test="${chrms.key == accountLineList.payVatDescr}">
			                                 	<c:set var="selectedVat" value="${chrms.value}"></c:set>
			                                 </c:when>
			                                <c:otherwise>
			                                </c:otherwise>
		                                </c:choose>
			                        </c:forEach>
			                        <input type="text" value="${selectedVat}" style="width:70px" class="input-textUpper pr-f11 " readonly="true"> 
							</c:if>
					 	</td>
					 	<td>
							<input type="text" name ="payVatPercent${accountLineList.id}"  value="${accountLineList.payVatPercent}" style="width:20px;text-align:right;" class="input-text pr-f11" disabled="disabled"> 
							<img id="rateImage"  class="openpopup" width="20" height="" align="top" src="${pageContext.request.contextPath}/images/acc_ctrl.png" onclick="showCheckBoxesInACList('${accountLineList.id}',this);"/>
						</td>
					 </c:if>
			  			 
					</tr>
					</c:if>
					<tr class="expand-child">
					 	<td></td>
					 	<td align="right">
						 		<table cellspacing="0" cellpadding="0" style="margin:0px;margin-top:20px;">
						 			<tr>
						 			<td style="border:none !important;padding:0px !important;">
						 			<img id="rateImage"  style="cursor:default;" align="top" src="${pageContext.request.contextPath}/images/est-btn.png" />
						 			</td>
						 			</tr>
						 		<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">						 		
						 			<tr>
						 			<td style="border:none !important;padding:0px !important;padding-top:3px;">
						 			<img id="rateImage"  style="cursor: default; position: relative; top: 3px;" align="top" src="${pageContext.request.contextPath}/images/rev-btn.png" />
						 			</td>
						 			</tr>
						 		</c:if>
						 		<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">						 		
						 			<tr>
						 			<td style="border:none !important;padding:0px !important;">
						 			<img id="rateImage"  style="cursor: default; position: relative; top: 4px;" align="top" src="${pageContext.request.contextPath}/images/act-btn.png" />
						 			</td>
						 			</tr>
						 		</c:if>
						 		</table>
					 	</td>
					 	<c:if test="${systemDefaultVatCalculationNew!='Y'}">
							<td colspan="9" style="padding-top:0px; padding-bottom:0px;padding-right:2px;">
						</c:if> 
						<c:if test="${systemDefaultVatCalculationNew=='Y'}">
							<td colspan="13" style="padding-top:0px; padding-bottom:0px;padding-right:2px;">
						</c:if> 
								  <table class="table-price" style="width:100%;margin-top:2px;margin-bottom:2px;" border="0" cellpadding="0" cellspacing="0">
								     <thead>
									       <tr>  
										        <th >Quantity</th>
										        <th >FX</th>
										        <th >Buy Rate</th>
										        <configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
										        	<th>Variance</th>
										        </configByCorp:fieldVisibility>
										       	<th style="color: rgb(204, 0, 0);">Expense</th>
										       	<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
										        	<th>Invoice#</th>
										        	<th >Description</th>
										        </c:if>
										        <th >Mk%</th>
										        <c:if test="${chargeDiscountFlag}">	
													<th>Disc %</th>
												</c:if>
												<c:if test="${contractType}">
													<th >Rec Quantity</th>
												</c:if>
										        <th>Sell Rate</th>
										        <configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
										        	<th>Adjustment</th>
										        </configByCorp:fieldVisibility>
										        <c:if test="${multiCurrency=='Y'}">
													<th >FX</th>
												</c:if>
										        
										        <th style="color: green;">Revenue</th>
										        <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
										        	<th>Distribution Amt</th>
										        </c:if>
										        <c:if test="${systemDefaultVatCalculationNew=='Y'}">
													<th >Pay VAT Amt</th>
													<th >Rec VAT Amt</th>
												</c:if>
												<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
													<th>Invoice#</th>
												</c:if>
												<th >Description</th>
									       </tr>
									    
									 </thead>
									 <tbody>
			    <!-- Estimate Active Status Starts -->
									    <c:if test="${accountLineList.status}">
									       <tr style="background-color: #f0f0f0;">
										       <td>
										       		<input type="text" style="text-align:right" id="estimateQuantity${accountLineList.id}"  name="estimateQuantity${accountLineList.id}"  value="${accountLineList.estimateQuantity }" size="4" class="input-text pr-f11" onkeydown="return onlyFloatNumsAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}','','','');calculateExpense('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}','estSellLocalAmountNew${accountLineList.id}');getId('${accountLineList.id}');"/>
													<img id="compute${accountLineList.id}"  class="openpopup" width="16" height="17" align="top" src="${pageContext.request.contextPath}/images/calc_new1.png" onclick="findRevisedEstimateQuantitys('${accountLineList.id}');getId('${accountLineList.id}');"/>			
												</td>
												<td>
													<c:choose>
								                        <c:when test="${accountLineList.estCurrency!='' && accountLineList.estCurrency !=null }">			                           
								                           <%-- <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','EST',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
								                           <a onmouseover="showBillingDatails('${accountLineList.id}','EST',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                           	<c:out value="${accountLineList.estCurrency}" />
                                                           </a>
								                        </c:when> 
								                        <c:otherwise>
								                          <%-- <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','EST',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
								                          <a onmouseover="showBillingDatails('${accountLineList.id}','EST',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                           <c:out value="${baseCurrency}" />
                                                          </a>
								                        </c:otherwise>
							                        </c:choose>
												</td>
												<td>
													<input type="text" style="text-align:right"  id="estimateRate${accountLineList.id}"  name="estimateRate${accountLineList.id}" value="${accountLineList.estimateRate }" size="5" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateExpense('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');getId('${accountLineList.id}');"/>
												</td>
												<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
													<td></td>
												</configByCorp:fieldVisibility>
												 <td class="exprevbg">
													<input type="text" style="text-align:right" name="estimateExpense${accountLineList.id}" value="${accountLineList.estimateExpense }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
												</td>
												<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
													<td></td>
													<td></td>
												</c:if>
												<td>
													<input type="text" style="text-align:right" name="estimatePassPercentage${accountLineList.id}" value="${accountLineList.estimatePassPercentage }" maxlength="4"  size="2" class="input-text pr-f11" onchange="changeEstimatePassPercentage('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}');calculateRevenueNew('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}','estSellLocalAmountNew${accountLineList.id}');getId('${accountLineList.id}');"  onkeydown="return onlyNumsAllowedPersent(event)" />
												</td>
												
												<c:if test="${chargeDiscountFlag}">
													<td>
													     <c:forEach items="${discountMap}" var="current">   
					       									   <c:if test="${current.key==accountLineList.id}"> 
					       									   <c:set value="${current.value}" var="disMap"/>
					       									   </c:if>
					    								 </c:forEach>  
														<select name ="estimateDiscount${accountLineList.id}" id="estimateDiscount${accountLineList.id}" style="width:70px" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}','','','');getId('${accountLineList.id}');" class="list-menu pr-f11" > 
															<c:forEach var="chrms" items="${disMap}" varStatus="loopStatus">
							                                  <c:choose>
								                                  	<c:when test="${chrms.key == accountLineList.estimateDiscount}">
								                                  		<c:set var="selectedInd" value=" selected"></c:set>
								                                  	</c:when>
								                                 	<c:otherwise>
								                                  		<c:set var="selectedInd" value=""></c:set>
								                                 	</c:otherwise>
							                                  </c:choose>
							                                 	<option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
							                                		<c:out value="${chrms.value}"></c:out>
							                                	</option>
							                                </c:forEach> 
														</select>
													</td>
												</c:if>
												<c:if test="${contractType}">
													<td>
														<input type="text" style="text-align:right" id="estimateSellQuantity${accountLineList.id}"  name="estimateSellQuantity${accountLineList.id}"  value="${accountLineList.estimateSellQuantity }" size="4" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}','','','');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}','estSellLocalAmountNew${accountLineList.id}');getId('${accountLineList.id}');"/>
													</td>
												</c:if>
												<td>
													<input type="text" style="text-align:right" name="estimateSellRate${accountLineList.id}" value="${accountLineList.estimateSellRate }" size="5" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}','estSellCurrencyNew${accountLineList.id}~estimateContractCurrencyNew${accountLineList.id}','estSellValueDateNew${accountLineList.id}~estimateContractValueDateNew${accountLineList.id}','estSellExchangeRateNew${accountLineList.id}~estimateContractExchangeRateNew${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}','estSellLocalAmountNew${accountLineList.id}');getId('${accountLineList.id}');"/>
												</td>
												<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
													<td></td>
												</configByCorp:fieldVisibility>
												<c:if test="${multiCurrency=='Y'}">	
													<td>
														<c:choose>
									                        <c:when test="${accountLineList.estSellCurrency!='' && accountLineList.estSellCurrency!=null}">			                           
									                           <%-- <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','ESTSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
									                        <a onmouseover="showBillingDatails('${accountLineList.id}','ESTSELL',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                             <c:out value="${accountLineList.estSellCurrency}" />
                                                            </a>
									                        </c:when> 
									                        <c:otherwise>
									                          <%-- <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','ESTSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
									                        <a onmouseover="showBillingDatails('${accountLineList.id}','ESTSELL',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                             <c:out value="${baseCurrency}" />
                                                            </a>
									                        </c:otherwise>
								                        </c:choose> 
													</td>
												</c:if>
												
												<td class="exprevbg">
													<input type="text" style="text-align:right" name="estimateRevenueAmount${accountLineList.id}" value="${accountLineList.estimateRevenueAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
												</td>
												<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
													<td></td>
												</c:if>
												<c:if test="${systemDefaultVatCalculationNew=='Y'}">
													<td>
														<input type="text" name ="estExpVatAmt${accountLineList.id}"  value="${accountLineList.estExpVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true"> 
													</td>
													<td>
														<input type="text" name ="vatAmt${accountLineList.id}"  value="${accountLineList.estVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
													</td>
												</c:if>
												<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
													<td></td>
												</c:if>
												<td>
													<input type="text" style="text-align:left;width:180px" name="quoteDescription${accountLineList.id}" value="${accountLineList.quoteDescription }" title="${accountLineList.quoteDescription}" size="15" class="input-text pr-f11" onchange="getId('${accountLineList.id}');" />
												</td>
									       </tr>
									      </c:if>
				      <!-- Estimate Active Status Ends -->
				      <!-- Estimate InActive Status Starts -->
									      <c:if test="${accountLineList.status==false}">
									       <tr style="background-color: #f0f0f0;">
										       <td>
										       		<input type="text" style="text-align:right" id="estimateQuantity${accountLineList.id}"  name="estimateQuantity${accountLineList.id}"  value="${accountLineList.estimateQuantity }" size="4" class="input-text pr-f11" disabled="disabled"/>
													<%-- <img id="rateImage"  class="openpopup" width="16" height="17" align="top" src="${pageContext.request.contextPath}/images/calc_new1.png" onclick="findRevisedEstimateQuantitys('chargeCode${accountLineList.id}','category${accountLineList.id}','estimateExpense${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','deviation${accountLineList.id}','estimateDeviation${accountLineList.id}','estimateSellDeviation${accountLineList.id}','${accountLineList.id}');"/> --%>			
												</td>
												<td>
													<c:choose>
								                        <c:when test="${accountLineList.estCurrency!='' && accountLineList.estCurrency!=null }">			                           
								                          <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','EST',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
								                        <a onmouseover="showBillingDatails('${accountLineList.id}','EST',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                             <c:out value="${accountLineList.estCurrency}" />
                                                        </a>
								                        </c:when> 
								                        <c:otherwise>
								                         <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','EST',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
								                        <a onmouseover="showBillingDatails('${accountLineList.id}','EST',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                             <c:out value="${baseCurrency}" />
                                                        </a>
								                        </c:otherwise>
							                        </c:choose>
												</td>
												<td>
													<input type="text" style="text-align:right"  id="estimateRate${accountLineList.id}"  name="estimateRate${accountLineList.id}" value="${accountLineList.estimateRate }" size="5" class="input-text pr-f11" disabled="disabled""/>
												</td>
												<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
													<td></td>
												</configByCorp:fieldVisibility>
												<td class="exprevbg">
													<input type="text" style="text-align:right" name="estimateExpense${accountLineList.id}" value="${accountLineList.estimateExpense }" size="6" class="input-text pr-f11 " disabled="disabled"/>
												</td>
												<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
													<td></td>
													<td></td>
												</c:if>
												<td>
													<input type="text" style="text-align:right" name="estimatePassPercentage${accountLineList.id}" value="${accountLineList.estimatePassPercentage }" maxlength="4"  size="2" class="input-text pr-f11" disabled="disabled" />
												</td>
												
												<c:if test="${chargeDiscountFlag}">
													<td>
													     <c:forEach items="${discountMap}" var="current">   
					       									   <c:if test="${current.key==accountLineList.id}"> 
					       									   <c:set value="${current.value}" var="disMap"/>
					       									   </c:if>
					    								 </c:forEach>  
														<select name ="estimateDiscount${accountLineList.id}" id="estimateDiscount${accountLineList.id}" style="width:70px" disabled="disabled" class="list-menu pr-f11" > 
															<c:forEach var="chrms" items="${disMap}" varStatus="loopStatus">
							                                  <c:choose>
								                                  	<c:when test="${chrms.key == accountLineList.estimateDiscount}">
								                                  		<c:set var="selectedInd" value=" selected"></c:set>
								                                  	</c:when>
								                                 	<c:otherwise>
								                                  		<c:set var="selectedInd" value=""></c:set>
								                                 	</c:otherwise>
							                                  </c:choose>
							                                 	<option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
							                                		<c:out value="${chrms.value}"></c:out>
							                                	</option>
							                                </c:forEach> 
														</select>
													</td>
												</c:if>
												<c:if test="${contractType}">
													<td>
														<input type="text" style="text-align:right" id="estimateSellQuantity${accountLineList.id}"  name="estimateSellQuantity${accountLineList.id}"  value="${accountLineList.estimateSellQuantity }" size="4" class="input-text pr-f11" disabled="disabled"/>
													</td>
												</c:if>
												<td>
													<input type="text" style="text-align:right" name="estimateSellRate${accountLineList.id}" value="${accountLineList.estimateSellRate }" size="5" class="input-text pr-f11" disabled="disabled"/>
												</td>
												<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
													<td></td>
												</configByCorp:fieldVisibility>
												<c:if test="${multiCurrency=='Y'}">	
													<td>
														<c:choose>
									                        <c:when test="${accountLineList.estSellCurrency!='' && accountLineList.estSellCurrency !=null }">			                           
									                          <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','ESTSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
									                        <a onmouseover="showBillingDatails('${accountLineList.id}','ESTSELL',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                             <c:out value="${accountLineList.estSellCurrency}" />
                                                            </a>
									                        </c:when> 
									                        <c:otherwise>
									                         <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','ESTSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
									                        <a onmouseover="showBillingDatails('${accountLineList.id}','ESTSELL',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                             <c:out value="${baseCurrency}" />
                                                            </a>
									                        </c:otherwise>
								                        </c:choose> 
													</td>
												</c:if>
												
												<td class="exprevbg">
													<input type="text" style="text-align:right" name="estimateRevenueAmount${accountLineList.id}" value="${accountLineList.estimateRevenueAmount }" size="6" class="input-text pr-f11 " disabled="disabled"/>
												</td>
												<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
													<td></td>
												</c:if>
												<c:if test="${systemDefaultVatCalculationNew=='Y'}">
													<td>
														<input type="text" name ="estExpVatAmt${accountLineList.id}"  value="${accountLineList.estExpVatAmt}" style="width:50px;text-align:right;" class="input-text pr-f11 " disabled="disabled"> 
													</td>
													<td>
														<input type="text" name ="vatAmt${accountLineList.id}"  value="${accountLineList.estVatAmt}" style="width:50px;text-align:right;" class="input-text pr-f11 " disabled="disabled">
													</td>
												</c:if>
												<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
													<td></td>
												</c:if>
												<td>
													<input type="text" style="text-align:left;width:180px" name="quoteDescription${accountLineList.id}" value="${accountLineList.quoteDescription }" title="${accountLineList.quoteDescription}" size="15" class="input-text pr-f11"  disabled="disabled"/>
												</td>
									       </tr>
									      </c:if>
				      <!-- Estimate InActive Status Ends -->
				      <!-- Revision Active Status Starts -->
									     <c:if test="${accountLineList.status}"> 
								       		<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
										       <tr>
										       		<td>
										       		<c:choose>
						                            <c:when test="${(not empty accountLineList.accrueRevenue || not empty accountLineList.accrueRevenueVariance || not empty accountLineList.accruePayable || not empty accountLineList.accrueExpenseVariance) && (SIFieldLockingFlag=='Y')}">
										       		<input type="text" style="text-align:right" id="revisionQuantity${accountLineList.id}"  name="revisionQuantity${accountLineList.id}"  value="${accountLineList.revisionQuantity }" size="4" class="input-textUpper pr-f11" readonly="readonly" />
										       		</c:when>
										       		<c:otherwise>
										       			<input type="text" style="text-align:right" id="revisionQuantity${accountLineList.id}"  name="revisionQuantity${accountLineList.id}"  value="${accountLineList.revisionQuantity }" size="4" class="input-text pr-f11" onkeydown="return onlyFloatNumsAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevisionBlock('${accountLineList.id}','','','','');getId('${accountLineList.id}');"/>
														<img id="compute${accountLineList.id}"  class="openpopup" width="16" height="17" align="top" src="${pageContext.request.contextPath}/images/calc_new1.png" onclick="findRevisedQuantitys('${accountLineList.id}');getId('${accountLineList.id}');"/>
										       		</c:otherwise>	
										       		</c:choose>
										       		</td>
										       		<td>
										       			<c:choose>
									                        <c:when test="${accountLineList.revisionCurrency!='' && accountLineList.revisionCurrency!=null}">		                           
									                           <%-- <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REV',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRev('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
									                       <a onmouseover="showBillingDatails('${accountLineList.id}','REV',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRev('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                             <c:out value="${accountLineList.revisionCurrency}" />
                                                           </a>
									                        </c:when> 
									                        <c:otherwise>
									                          <%-- <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REV',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRev('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
									                      <a onmouseover="showBillingDatails('${accountLineList.id}','REV',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRev('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                             <c:out value="${baseCurrency}" />
                                                           </a> 
                                                          </c:otherwise>
									                     </c:choose> 
										       		</td>
										       		<td>
										       		<c:choose>
						                            <c:when test="${(not empty accountLineList.accruePayable || not empty accountLineList.accrueExpenseVariance) && (SIFieldLockingFlag=='Y')}">
						                            <input type="text" style="text-align:right"  id="revisionRate${accountLineList.id}"  name="revisionRate${accountLineList.id}" value="${accountLineList.revisionRate }" size="5" class="input-textUpper pr-f11" readonly="readonly"/>
						                            </c:when>
						                            <c:otherwise>
										       			<input type="text" style="text-align:right"  id="revisionRate${accountLineList.id}"  name="revisionRate${accountLineList.id}" value="${accountLineList.revisionRate }" size="5" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevisionBlock('${accountLineList.id}','','','','');getId('${accountLineList.id}');"/>
										       		</c:otherwise>
										       		</c:choose>
										       		</td>
										       		<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
											       		<td>
														<c:choose>
	                                                    <c:when test="${(empty accountLineList.accrueExpenseVariance)}">
	                                                    	<input type="text" style="text-align:right" id="varianceExpenseAmount${accountLineList.id}" name="varianceExpenseAmount${accountLineList.id}" value="${accountLineList.varianceExpenseAmount }"  size="6" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onchange="getId('${accountLineList.id}');"/>
	                                                    </c:when>
	                                                    <c:otherwise>
															<input type="text" style="text-align:right" id="varianceExpenseAmount${accountLineList.id}" name="varianceExpenseAmount${accountLineList.id}" value="${accountLineList.varianceExpenseAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
														</c:otherwise>
														</c:choose>
														</td>
													</configByCorp:fieldVisibility>
										       		<td class="exprevbg">
										       			<input type="text" style="text-align:right" name="revisionExpense${accountLineList.id}" value="${accountLineList.revisionExpense }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
										       		</td>
										       		<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
										       			<td></td>
										       			<td></td>
										       		</c:if>
										       		<td>
										       			<input type="text" style="text-align:right" name="revisionPassPercentage${accountLineList.id}" value="${accountLineList.revisionPassPercentage }" maxlength="4"  size="2" class="input-text pr-f11" onchange="changeRevisionPassPercentage('basis${accountLineList.id}','revisionQuantity${accountLineList.id}','revisionRate${accountLineList.id}','revisionExpense${accountLineList.id}','revisionSellRate${accountLineList.id}','revisionRevenueAmount${accountLineList.id}','revisionPassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','${accountLineList.id}');getId('${accountLineList.id}');"  onkeydown="return onlyNumsAllowedPersent(event)" />
										       		</td>
										       		
										       		<c:if test="${chargeDiscountFlag}">
										       			<td>
														     <c:forEach items="${discountMap}" var="current">   
						       									   <c:if test="${current.key==accountLineList.id}"> 
						       									   <c:set value="${current.value}" var="disMap"/>
						       									   </c:if>
						    								  </c:forEach>  
															<select name ="revisionDiscount${accountLineList.id}" id="revisionDiscount${accountLineList.id}" style="width:70px" onchange="calculateRevisionBlock('${accountLineList.id}','','','','');getId('${accountLineList.id}');" class="list-menu pr-f11" > 
																<c:forEach var="chrms" items="${disMap}" varStatus="loopStatus">
								                                  <c:choose>
									                                  <c:when test="${chrms.key == accountLineList.revisionDiscount}">
									                                  <c:set var="selectedInd" value=" selected"></c:set>
									                                  </c:when>
									                                 <c:otherwise>
									                                  <c:set var="selectedInd" value=""></c:set>
									                                 </c:otherwise>
								                                  </c:choose>
								                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
								                                <c:out value="${chrms.value}"></c:out>
								                                </option>
								                                </c:forEach> 
															</select>
										       			</td>
										       		</c:if>
										       		<c:if test="${contractType}">
										       			<td>
															<input type="text" style="text-align:right" id="revisionSellQuantity${accountLineList.id}"  name="revisionSellQuantity${accountLineList.id}"  value="${accountLineList.revisionSellQuantity }" size="4" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevisionBlock('${accountLineList.id}','','','','');getId('${accountLineList.id}');"/>
										       			</td>
										       		</c:if>
										       		<td>
										       		<c:choose>
						                            <c:when test="${(not empty accountLineList.accrueRevenue || not empty accountLineList.accrueRevenueVariance) && (SIFieldLockingFlag=='Y')}">
						                            <input type="text" style="text-align:right" name="revisionSellRate${accountLineList.id}" value="${accountLineList.revisionSellRate }" size="5" class="input-textUpper pr-f11" readonly="readonly"/>
						                            </c:when>
						                            <c:otherwise>
										       			<input type="text" style="text-align:right" name="revisionSellRate${accountLineList.id}" value="${accountLineList.revisionSellRate }" size="5" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevisionBlock('${accountLineList.id}','revisionSellCurrency${accountLineList.id}~revisionContractCurrency${accountLineList.id}','revisionSellValueDate${accountLineList.id}~revisionContractValueDate${accountLineList.id}','revisionSellExchangeRate${accountLineList.id}~revisionContractExchangeRate${accountLineList.id}');getId('${accountLineList.id}');"/>
										       		</c:otherwise></c:choose>
										       		</td>
										       		<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
											       		<td>
											       		<c:choose>
	                                                    <c:when test="${(empty accountLineList.accrueRevenueVariance)}">
	                                                    	<input type="text" style="text-align:right" id="varianceRevenueAmount${accountLineList.id}" name="varianceRevenueAmount${accountLineList.id}" value="${accountLineList.varianceRevenueAmount }"  size="6" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onchange="getId('${accountLineList.id}');"/>
	                                                    </c:when>
	                                                    <c:otherwise>
															<input type="text" style="text-align:right" id="varianceRevenueAmount${accountLineList.id}" name="varianceRevenueAmount${accountLineList.id}" value="${accountLineList.varianceRevenueAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
														</c:otherwise>
														</c:choose>
														</td>
													</configByCorp:fieldVisibility>
										       		<c:if test="${multiCurrency=='Y'}">
										       			<td>
															<c:choose>
										                        <c:when test="${accountLineList.revisionSellCurrency!='' && accountLineList.revisionSellCurrency!=null }">			                           
										                          <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REVSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRevNew('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
										                          <a onmouseover="showBillingDatails('${accountLineList.id}','REVSELL',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRevNew('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                     <c:out value="${accountLineList.revisionSellCurrency}" />
                                                                  </a>
										                        </c:when> 
										                        <c:otherwise>
										                         <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REVSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRevNew('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
										                        <a onmouseover="showBillingDatails('${accountLineList.id}','REVSELL',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRevNew('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                     <c:out value="${baseCurrency}" />
                                                                </a>
										                        </c:otherwise>
									                        </c:choose> 
										       			</td>
										       		</c:if>
										       		
										       		<td class="exprevbg">
										       			<c:if test="${accountLineList.accrueRevenueManual == true}"> 
                  											<input type="text" style="text-align:right;color:#E3170D;background-color: #CCCCCC" id="revisionRevenueAmount${accountLineList.id}" name="revisionRevenueAmount${accountLineList.id}" value="${accountLineList.revisionRevenueAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
        												</c:if>
														<c:if test="${accountLineList.accrueRevenueManual == false || accountLineList.accrueRevenueManual == null}"> 
														   <input type="text" style="text-align:right" id="revisionRevenueAmount${accountLineList.id}" name="revisionRevenueAmount${accountLineList.id}" value="${accountLineList.revisionRevenueAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
												        </c:if>
										       		</td>
										       		<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
														<td></td>
													</c:if>
										       		<c:if test="${systemDefaultVatCalculationNew=='Y'}">
										       			<td>
															<input type="text" name ="revisionExpVatAmt${accountLineList.id}"  value="${accountLineList.revisionExpVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">								 
														</td>
														<td>
															<input type="text" name ="revisionVatAmt${accountLineList.id}"  value="${accountLineList.revisionVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true"> 
														</td>
													</c:if>
													<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
														<td></td>
													</c:if>
													<td>
														<input type="text" style="text-align:left;width:180px" name="revisionDescription${accountLineList.id}" value="${accountLineList.revisionDescription}" size="15" class="input-text pr-f11"  onchange="getId('${accountLineList.id}');"/>
													</td>
										       </tr>
								       		</c:if>
								       	</c:if>
			       	<!-- Revision Active Status Ends -->
			       	<!-- Revision InActive Status Starts -->
								       	<c:if test="${accountLineList.status==false}"> 
								       		<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
										       <tr>
										       		<td>
										       			<input type="text" style="text-align:right" id="revisionQuantity${accountLineList.id}"  name="revisionQuantity${accountLineList.id}"  value="${accountLineList.revisionQuantity }" size="4" class="input-text pr-f11" disabled="disabled"/>
														<%-- <img id="rateImage"  class="openpopup" width="16" height="17" align="top" src="${pageContext.request.contextPath}/images/calc_new1.png" onclick="findRevisedQuantitys('${accountLineList.id}');"/> --%>
										       		</td>
										       		<td>
										       			<c:choose>
									                        <c:when test="${accountLineList.revisionCurrency!='' && accountLineList.revisionCurrency !=null }">		                           
									                          <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REV',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRev('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
									                        <a onmouseover="showBillingDatails('${accountLineList.id}','REV',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRev('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                             <c:out value="${accountLineList.revisionCurrency}" />
                                                             </a>
									                        </c:when> 
									                        <c:otherwise>
									                         <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REV',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRev('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
									                        <a onmouseover="showBillingDatails('${accountLineList.id}','REV',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRev('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                             <c:out value="${baseCurrency}" />
                                                             </a>
									                        </c:otherwise>
									                     </c:choose> 
										       		</td>
										       		<td>
										       			<input type="text" style="text-align:right"  id="revisionRate${accountLineList.id}"  name="revisionRate${accountLineList.id}" value="${accountLineList.revisionRate }" size="5" class="input-text pr-f11" disabled="disabled"/>
										       		</td>
										       		<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
											       		<td>
															<input type="text" style="text-align:right" id="varianceExpenseAmount${accountLineList.id}" name="varianceExpenseAmount${accountLineList.id}" value="${accountLineList.varianceExpenseAmount }" size="6" class="input-text pr-f11 " disabled="disabled"/>
														</td>
													</configByCorp:fieldVisibility>
										       		<td class="exprevbg">
										       			<input type="text" style="text-align:right" name="revisionExpense${accountLineList.id}" value="${accountLineList.revisionExpense }" size="6" class="input-text pr-f11 " disabled="disabled"/>
										       		</td>
										       		<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
										       			<td></td>
										       			<td></td>
										       		</c:if>
										       		<td>
										       			<input type="text" style="text-align:right" name="revisionPassPercentage${accountLineList.id}" value="${accountLineList.revisionPassPercentage }" maxlength="4"  size="2" class="input-text pr-f11" disabled="disabled" />
										       		</td>
										       		
										       		<c:if test="${chargeDiscountFlag}">
										       			<td>
														     <c:forEach items="${discountMap}" var="current">   
						       									   <c:if test="${current.key==accountLineList.id}"> 
						       									   <c:set value="${current.value}" var="disMap"/>
						       									   </c:if>
						    								  </c:forEach>  
															<select name ="revisionDiscount${accountLineList.id}" id="revisionDiscount${accountLineList.id}" style="width:70px" disabled="disabled" class="list-menu pr-f11" > 
																<c:forEach var="chrms" items="${disMap}" varStatus="loopStatus">
								                                  <c:choose>
									                                  <c:when test="${chrms.key == accountLineList.revisionDiscount}">
									                                  <c:set var="selectedInd" value=" selected"></c:set>
									                                  </c:when>
									                                 <c:otherwise>
									                                  <c:set var="selectedInd" value=""></c:set>
									                                 </c:otherwise>
								                                  </c:choose>
								                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
								                                <c:out value="${chrms.value}"></c:out>
								                                </option>
								                                </c:forEach> 
															</select>
										       			</td>
										       		</c:if>
										       		<c:if test="${contractType}">
										       			<td>
															<input type="text" style="text-align:right" id="revisionSellQuantity${accountLineList.id}"  name="revisionSellQuantity${accountLineList.id}"  value="${accountLineList.revisionSellQuantity }" size="4" class="input-text pr-f11" disabled="disabled"/>
										       			</td>
										       		</c:if>
										       		<td>
										       			<input type="text" style="text-align:right" name="revisionSellRate${accountLineList.id}" value="${accountLineList.revisionSellRate }" size="5" class="input-text pr-f11" disabled="disabled"/>
										       		</td>
										       		<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
											       		<td>
															<input type="text" style="text-align:right" id="varianceRevenueAmount${accountLineList.id}" name="varianceRevenueAmount${accountLineList.id}" value="${accountLineList.varianceRevenueAmount }" size="6" class="input-text pr-f11 " disabled="disabled"/>
														</td>
													</configByCorp:fieldVisibility>
										       		<c:if test="${multiCurrency=='Y'}">
										       			<td>
															<c:choose>
										                        <c:when test="${accountLineList.revisionSellCurrency!='' && accountLineList.revisionSellCurrency!=null }">			                           
										                           <%-- <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REVSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRevNew('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
										                        <a onmouseover="showBillingDatails('${accountLineList.id}','REVSELL',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRevNew('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                     <c:out value="${accountLineList.revisionSellCurrency}" />
                                                                </a>
										                        </c:when> 
										                        <c:otherwise>
										                         <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REVSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRevNew('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
										                        <a onmouseover="showBillingDatails('${accountLineList.id}','REVSELL',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRevNew('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                     <c:out value="${baseCurrency}" />
                                                                </a>
										                        </c:otherwise>
									                        </c:choose> 
										       			</td>
										       		</c:if>
										       		
										       		<td class="exprevbg">
										       			<input type="text" style="text-align:right" name="revisionRevenueAmount${accountLineList.id}" value="${accountLineList.revisionRevenueAmount }" size="6" class="input-text pr-f11 " disabled="disabled"/>
										       		</td>
										       		<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
														<td></td>
													</c:if>
										       		<c:if test="${systemDefaultVatCalculationNew=='Y'}">
										       			<td>
															<input type="text" name ="revisionExpVatAmt${accountLineList.id}"  value="${accountLineList.revisionExpVatAmt}" style="width:50px;text-align:right;" class="input-text pr-f11 " disabled="disabled"> 
														</td>
														<td>
															<input type="text" name ="revisionVatAmt${accountLineList.id}"  value="${accountLineList.revisionVatAmt}" style="width:50px;text-align:right;" class="input-text pr-f11 " disabled="disabled"> 
														</td>
													</c:if>
													<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
														<td></td>
													</c:if>
													<td>
														<input type="text" style="text-align:left;width:180px" name="revisionDescription${accountLineList.id}" value="${accountLineList.revisionDescription}" title="${accountLineList.revisionDescription}" size="15" class="input-text pr-f11"  disabled="disabled"/>
													</td>
										       </tr>
								       		</c:if>
								       	</c:if>
			       	<!-- Revision InActive Status Ends -->
			       	<!-- Actual Active Status Starts -->
								       <c:if test="${accountLineList.status}">	
									       <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
									       		<tr style="background-color: #f0f0f0;">
									       			<td>
									       				<c:if test="${empty accountLineList.recAccDate}">	
															<input type="text" style="text-align:right" name="recQuantity${accountLineList.id}"  value="${accountLineList.recQuantity}" size="4" class="input-text pr-f11" onkeydown="return onlyFloatNumsAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateActualRevenue('${accountLineList.id}',this,'REC','','','');getId('${accountLineList.id}');"/>
														</c:if>
														<c:if test="${not empty accountLineList.recAccDate}">	
															<input type="text" style="text-align:right" name="recQuantity${accountLineList.id}"  value="${accountLineList.recQuantity}" size="4" class="input-textUpper pr-f11 " readonly="true"/>
														</c:if>
									       			</td>
									       			<td>
									       				<c:if test="${empty accountLineList.payAccDate}">
															<c:choose>
										                        <c:when test="${accountLineList.country!='' && accountLineList.country!=null }">
										                          <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
										                        <a onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                  <c:out value="${accountLineList.country}" />
                                                                </a>
										                        </c:when> 
										                        <c:otherwise>
										                         <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
										                        <a onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                  <c:out value="${baseCurrency}" />
                                                                </a>
										                        </c:otherwise>
										                        </c:choose> 
									                    </c:if>
														<c:if test="${not empty accountLineList.payAccDate}">
															<c:choose>
										                        <c:when test="${accountLineList.country!='' && accountLineList.country!=null}">
										                           <%-- <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
										                       <a onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                  <c:out value="${accountLineList.country}" />
                                                                </a>
										                        </c:when> 
										                        <c:otherwise>
										                         <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
										                        <a onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                  <c:out value="${baseCurrency}" />
                                                                </a>
										                        </c:otherwise>
									                        </c:choose> 								
														</c:if>
									       			</td>
									       			<td></td>
									       			<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
									       			<td></td>
									       			</configByCorp:fieldVisibility>
									       			
									       			<td class="exprevbg">
									       				<c:if test="${accountInterface=='Y'}">
									       					<c:if test="${!payablesXferWithApprovalOnly}">
		  														<c:choose>
		  															<c:when test="${not empty accountLineList.payPostDate && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
		  																<input type="text" style="text-align:right;background-color: #fed676;border:1px solid #ef8319;" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" class="input-text pr-f11" size="6"  onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateActualRevenue('${accountLineList.id}',this,'PAY','','','');getId('${accountLineList.id}');populateVarienceAmount('${accountLineList.id}');"/>
		  															</c:when>
		  															<c:when test="${not empty accountLineList.payPostDate && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
		  																<input type="text" style="text-align:right;background-color: #a5e362;border: 1px solid #00bc0c;" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" class="input-text pr-f11" readonly="true" size="6"  />
		  															</c:when>
		  															<c:when test="${not empty accountLineList.payPostDate && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount !='0' && accountLineList.payPayableAmount !='0.00' && accountLineList.payPayableAmount != null && accountLineList.payPayableAmount != '')}">
		  																<input type="text" style="text-align:right;background-color: #9cd1fd;border:1px solid #3e88c6;" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" class="input-text pr-f11" readonly="true" size="6"  />
		  															</c:when>
		  															<c:when test="${empty accountLineList.payPostDate && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '')}">
		  																<input type="text" style="text-align:right" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" class="input-text pr-f11" size="6"  onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateActualRevenue('${accountLineList.id}',this,'PAY','','','');getId('${accountLineList.id}');populateVarienceAmount('${accountLineList.id}');"/>
		  															</c:when>
		  															<c:when test="${empty accountLineList.payPostDate && empty accountLineList.payAccDate && accountLineList.invoiceNumber!='' && fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && accountLineList.payingStatus=='A' && (accountLineList.payPayableAmount !='0' && accountLineList.payPayableAmount !='0.00' && accountLineList.payPayableAmount != null && accountLineList.payPayableAmount != '')}">
		  																<input type="text" style="text-align:right;background-color: #fed676;border:1px solid #ef8319;" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" class="input-text pr-f11" size="6"  onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateActualRevenue('${accountLineList.id}',this,'PAY','','','');getId('${accountLineList.id}');populateVarienceAmount('${accountLineList.id}');"/>
		  															</c:when>
		  															<c:otherwise>
		  																<input type="text" style="text-align:right" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" class="input-text pr-f11" size="6"  onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateActualRevenue('${accountLineList.id}',this,'PAY','','','');getId('${accountLineList.id}');populateVarienceAmount('${accountLineList.id}');"/>
		  															</c:otherwise>
		  														</c:choose>
		  													</c:if>
		  													<c:if test="${payablesXferWithApprovalOnly}">
		  														<c:choose>
		  															<c:when test="${(not empty accountLineList.payPostDate ) && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
		  																<input type="text" style="text-align:right;background-color: #fed676;border:1px solid #ef8319;" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" class="input-text pr-f11" size="6"  onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateActualRevenue('${accountLineList.id}',this,'PAY','','','');getId('${accountLineList.id}');populateVarienceAmount('${accountLineList.id}');"/>
		  															</c:when>
		  															<c:when test="${(accountLineList.payingStatus=='A'|| not empty accountLineList.payPostDate) && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '') }">
		  																<input type="text" style="text-align:right;background-color: #a5e362;border: 1px solid #00bc0c;" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" class="input-text pr-f11" size="6" readonly="true" />
		  															</c:when>
		  															<c:when test="${(accountLineList.payingStatus=='A'|| not empty accountLineList.payPostDate) && not empty accountLineList.payAccDate && (accountLineList.payPayableAmount !='0' && accountLineList.payPayableAmount !='0.00' && accountLineList.payPayableAmount != null && accountLineList.payPayableAmount != '')}">
		  																<input type="text" style="text-align:right;background-color: #9cd1fd;border:1px solid #3e88c6;" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" class="input-text pr-f11" size="6" readonly="true" />
		  															</c:when>
		  															<c:when test="${(accountLineList.payingStatus=='A' || not empty accountLineList.payPostDate) && empty accountLineList.payAccDate && (accountLineList.payPayableAmount =='0' || accountLineList.payPayableAmount =='0.00' || accountLineList.payPayableAmount == null || accountLineList.payPayableAmount == '')}">
		  																<input type="text" style="text-align:right" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" class="input-text pr-f11" size="6"  onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateActualRevenue('${accountLineList.id}',this,'PAY','','','');getId('${accountLineList.id}');populateVarienceAmount('${accountLineList.id}');"/>
		  															</c:when>
		  															<c:when test="${empty accountLineList.payPostDate && empty accountLineList.payAccDate && accountLineList.invoiceNumber!='' && fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && accountLineList.payingStatus=='A' && (accountLineList.payPayableAmount !='0' && accountLineList.payPayableAmount !='0.00' && accountLineList.payPayableAmount != null && accountLineList.payPayableAmount != '')}">
		  																<input type="text" style="text-align:right;background-color: #fed676;border:1px solid #ef8319;" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" class="input-text pr-f11" size="6"  onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateActualRevenue('${accountLineList.id}',this,'PAY','','','');getId('${accountLineList.id}');populateVarienceAmount('${accountLineList.id}');"/>
		  															</c:when>
		  															<c:otherwise>
		  																<input type="text" style="text-align:right" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" class="input-text pr-f11" size="6"  onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateActualRevenue('${accountLineList.id}',this,'PAY','','','');getId('${accountLineList.id}');populateVarienceAmount('${accountLineList.id}');"/>
		  															</c:otherwise>
		  														</c:choose>
		  													</c:if>
														</c:if>
														<c:if test="${accountInterface!='Y'}">
															<input type="text" style="text-align:right" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" class="input-text pr-f11" size="6"  onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateActualRevenue('${accountLineList.id}',this,'PAY','','','');getId('${accountLineList.id}');populateVarienceAmount('${accountLineList.id}');"/>
														</c:if>
							
														<div id="payableDetails${accountLineList.id}" class="modal fade">
															<div class="loading-indicator">
																Loading...<br><i class="fa fa-circle-o-notch fa-spin" style="font-size:54px;color:grey;" id="loading-indicator${accountLineList.id}"></i>
															</div>
														</div>
														<img id="rateImage"  class="openpopup" align="top" src="${pageContext.request.contextPath}/images/popup_icon14.png" onclick="getReceivableFieldDetails('${accountLineList.id}','Pay')"/>
														<%-- <img id="rateImage"  class="openpopup" align="top" src="${pageContext.request.contextPath}/images/popup_icon14.png" onclick="getPayableSectionDetails('${accountLineList.id}','Pay');"/> --%>
														
														<c:if test="${empty accountLineList.payAccDate}">
															<img id="compute${accountLineList.id}"  class="openpopup" width="16" height="17" align="top" src="${pageContext.request.contextPath}/images/calc_new1.png" onclick="findRevisedQuantityLocalAmounts('${accountLineList.id}');getId('${accountLineList.id}');"/>
														</c:if>
														<c:choose>								
													         <c:when test="${accountLineList.payingStatus=='A' || accountLineList.payingStatus=='C'}">
													         	<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
													        </c:when>
													        <c:when test="${accountLineList.payingStatus=='N'}">
													        	<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
													        </c:when>
													        <c:when test="${accountLineList.payingStatus=='P'}">
													        	<img id="target" src="${pageContext.request.contextPath}/images/q1.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
													        </c:when>
													        <c:when test="${accountLineList.payingStatus=='S'}">
													         	<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
													        </c:when>
													         <c:when test="${accountLineList.payingStatus=='I'}">
													         	<img id="active" src="${pageContext.request.contextPath}/images/internal-Cost.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
													         </c:when>
													        <c:otherwise>
													        </c:otherwise>						         
														</c:choose>
									       			</td>
									       			
									       			<td>
														<input type="text" style="text-align:left" name="invoiceNumber${accountLineList.id}" value="${accountLineList.invoiceNumber }" title="${accountLineList.invoiceNumber}" readonly="readonly" size="6" class="input-textUpper pr-f11 " disabled="disabled"/>
													</td>
													<td>
														<input type="text" style="text-align:left" name="note${accountLineList.id}" value="${accountLineList.note}" title="${accountLineList.note}" size="15" class="input-text pr-f11"  onchange="getId('${accountLineList.id}');"/>
													</td>
									       			<td></td>
									       			
									       			<c:if test="${chargeDiscountFlag}">	
														<td> 
															<c:if test="${empty accountLineList.recAccDate}">	
								 								<c:forEach items="${discountMap}" var="current">   
								       									   <c:if test="${current.key==accountLineList.id}"> 
								       									   <c:set value="${current.value}" var="disMap"/>
								       									   </c:if>
								    						    </c:forEach>  
															<select name ="actualDiscount${accountLineList.id}" id="actualDiscount${accountLineList.id}" style="width:70px" onchange="calculateActualRevenue('${accountLineList.id}',this,'REC','','','');getId('${accountLineList.id}');" class="list-menu pr-f11" > 
																<c:forEach var="chrms" items="${disMap}" varStatus="loopStatus">
								                                  <c:choose>
									                                  <c:when test="${chrms.key == accountLineList.actualDiscount}">
									                                  <c:set var="selectedInd" value=" selected"></c:set>
									                                  </c:when>
									                                 <c:otherwise>
									                                  <c:set var="selectedInd" value=""></c:set>
									                                 </c:otherwise>
								                                  </c:choose>
								                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
								                                <c:out value="${chrms.value}"></c:out>
								                                </option>
								                                </c:forEach> 
															</select>
															</c:if>
															<c:if test="${not empty accountLineList.recAccDate}">	
															<input type="text" name ="actualDiscount${accountLineList.id}"  value="${accountLineList.actualDiscount}" style="width:50px" class="input-textUpper pr-f11 " readonly="true">
															</c:if>
														</td>
													</c:if>
													<c:if test="${contractType}">
									       				<td></td>
									       			</c:if>
									       			<td>
									       				<c:if test="${empty accountLineList.recAccDate}">	
															<input type="text" style="text-align:right" name="recRate${accountLineList.id}" value="${accountLineList.recRate}" size="5" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateActualRevenue('${accountLineList.id}',this,'REC','recRateCurrency${accountLineList.id}~contractCurrency${accountLineList.id}','racValueDate${accountLineList.id}~contractValueDate${accountLineList.id}','recRateExchange${accountLineList.id}~contractExchangeRate${accountLineList.id}');getId('${accountLineList.id}');"/>
															<img id="compute${accountLineList.id}"  class="openpopup" width="16" height="17" align="top" src="${pageContext.request.contextPath}/images/calc_new1.png" onclick="findRevisedReceivableQuantitys('${accountLineList.id}');"/>
														</c:if>
														<c:if test="${not empty accountLineList.recAccDate}">	
															<input type="text" style="text-align:right" name="recRate${accountLineList.id}" value="${accountLineList.recRate}" size="5" class="input-textUpper pr-f11 " readonly="true"/>
														</c:if>	
									       			</td>
									       			<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
									       			<td></td>
									       			</configByCorp:fieldVisibility>
									       			<c:if test="${multiCurrency=='Y'}">
									       				<td>
															<c:if test="${empty accountLineList.recAccDate}">	
																<c:choose>
											                        <c:when test="${accountLineList.recRateCurrency!='' && accountLineList.recRateCurrency !=null }">
											                          <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
											                         <a onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                       <c:out value="${accountLineList.recRateCurrency}" />
                                                                     </a>
											                        </c:when> 
											                        <c:otherwise>
											                         <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
											                        <a onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                       <c:out value="${baseCurrency}" />
                                                                     </a>
											                        </c:otherwise>
										                        </c:choose> 
										                     </c:if>
										                     <c:if test="${not empty accountLineList.recAccDate}">	 
											                        <c:choose>
												                        <c:when test="${accountLineList.recRateCurrency!='' && accountLineList.recRateCurrency!=null  }"> 
												                          <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
												                        <a  onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                           <c:out value="${accountLineList.recRateCurrency}" />
                                                                         </a>
												                        </c:when> 
												                        <c:otherwise>
												                         <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
												                        <a  onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                           <c:out value="${baseCurrency}" />
                                                                         </a>
												                        </c:otherwise>
											                        </c:choose>
										                     </c:if>
										                 </td>
													</c:if>
									       			
									       			
									       			<td class="exprevbg">
														<c:if test="${accountInterface=='Y'}">
													       <c:choose> 
														       <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
															       <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
															           <input type="text" style="text-align:right;background-color: #fed676;border:1px solid #ef8319;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
															        </c:if>
															       <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
															           <input type="text" style="text-align:right;background-color:#ffcccc;border: 1px solid #f07676;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
															        </c:if>
														        </c:when>
														        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
														        	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
														           		<input type="text" style="text-align:right;background-color:#a5e362;border: 1px solid #00bc0c;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
														         	</c:if>
														        	<c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
														           		<input type="text" style="text-align:right;background-color:#ffcccc;border: 1px solid #f07676;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
														           </c:if>
														        </c:when> 
														        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid')}">
														           		<input type="text" style="text-align:right;background-color:#9cd1fd;border:1px solid #3e88c6;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
														        </c:when>
														        <c:when test="${empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
														        	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
														        		<input type="text" style="text-align:right;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
														           </c:if>
														           <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
																		<input type="text" style="text-align:right;background-color:#ffcccc;border: 1px solid #f07676;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
														           </c:if>
														        </c:when>
														        <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid') && fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && (accountLineList.receivedAmount !='0' && accountLineList.receivedAmount !='0.00' && accountLineList.receivedAmount != null && accountLineList.receivedAmount != '')}">
																    	<input type="text" style="text-align:right;background-color:#ffcccc;border: 1px solid #f07676;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
														         </c:when>
														         <c:otherwise>
														          	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
														          		<input type="text" style="text-align:right" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
														            </c:if>
														            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
															          	<input type="text" style="text-align:right;background-color:#ffcccc;border: 1px solid #f07676;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
														            </c:if>
														         </c:otherwise>
													        </c:choose>
												        </c:if> 
												        <c:if test="${accountInterface!='Y'}">
													         <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
														           <c:choose>
															            <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid') && fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && (accountLineList.receivedAmount !='0' && accountLineList.receivedAmount !='0.00' && accountLineList.receivedAmount != null && accountLineList.receivedAmount != '')}">
																	    	<input type="text" style="text-align:right;background-color:#fed676;border:1px solid #ef8319;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
															          </c:when>
															          <c:otherwise>
															            	<input type="text" style="text-align:right;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>          
															          </c:otherwise>
														           </c:choose>
													           </c:if>
													            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
													         			<input type="text" style="text-align:right;background-color:#ffcccc;border: 1px solid #f07676;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
													            </c:if>
												        </c:if>
														<configByCorp:fieldVisibility componentId="component.field.Pricing.GeneratingInvoice">
															<c:if test="${accountLineList.recInvoiceNumber != '' && accountLineList.recInvoiceNumber != null}">
													        	<a><img align="top" title="Forms" onclick="findUserPermission1('${accountLineList.recInvoiceNumber}','${accountLineList.companyDivision}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/></a>
													        </c:if>
												        </configByCorp:fieldVisibility>
												        
												        <img id="rateImage"  class="openpopup" align="top" src="${pageContext.request.contextPath}/images/popup_icon14.png" onclick="getReceivableFieldDetails('${accountLineList.id}','Rec')"/>
														<%-- <img id="rateImage"  class="openpopup" align="top" src="${pageContext.request.contextPath}/images/popup_icon14.png"  onclick="getPayableSectionDetails('${accountLineList.id}','Rec');" /> --%>
									       			</td>
									       			 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
														<td>
										       				<c:if test="${!vanlineSettleColourStatus}">
										       					<c:choose> 
											       						<c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
																	        <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
																	           <input type="text" style="text-align:right;background-color: #fed676;border:1px solid #ef8319;" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11 " onkeydown="return onlyRateAllowed(event)" onchange="driverCommission('${accountLineList.id}');getId('${accountLineList.id}');"/>
																	        </c:if>
																	        <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
																	           <input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11 " onkeydown="return onlyRateAllowed(event)" onchange="driverCommission('${accountLineList.id}');getId('${accountLineList.id}');"/>
																	        </c:if>
																      	</c:when>
															      		<c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
																         	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
																           		<input type="text" style="text-align:right;background-color: #a5e362;border: 1px solid #00bc0c;" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
																           </c:if>
																            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
																           		<input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
																            </c:if>
																        </c:when>
																        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid')}">
																           		<input type="text" style="text-align:right;background-color: #9cd1fd;border:1px solid #3e88c6;" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
																        </c:when>
																       <c:when test="${empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
																         	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
																           		<input type="text" style="text-align:right;" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onchange="driverCommission('${accountLineList.id}');getId('${accountLineList.id}');"/>
																           </c:if>
																            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
																           		<input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11 " onkeydown="return onlyRateAllowed(event)" onchange="driverCommission('${accountLineList.id}');getId('${accountLineList.id}');"/>
																            </c:if>
																      </c:when>
																	   <c:otherwise>
																	   			<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
																		          		<input type="text" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" style="text-align:right;" size="6" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onchange="driverCommission('${accountLineList.id}');getId('${accountLineList.id}');"/>
																				</c:if>
																				<c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
																					<input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onchange="driverCommission('${accountLineList.id}');getId('${accountLineList.id}');"/>
																				</c:if>
																	   </c:otherwise>
															   </c:choose>
															</c:if>
															<c:if test="${vanlineSettleColourStatus}">
													       		<c:choose> 
															       <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && empty accountLineList.vanlineSettleDate}">
																       	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
																           <input type="text" style="text-align:right;background-color: #fed676;border:1px solid #ef8319;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onchange="driverCommission('${accountLineList.id}');getId('${accountLineList.id}');"/>
																        </c:if>
																		<c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
																           <input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onchange="driverCommission('${accountLineList.id}');getId('${accountLineList.id}');"/>
																		</c:if>           
															        </c:when>
															        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && empty accountLineList.vanlineSettleDate}">
															       		<c:if test="${accountLineList.paymentStatus !='Partially Paid'}"> 
															           		<input type="text" style="text-align:right;background-color: #a5e362;border: 1px solid #00bc0c;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" readonly="readonly" size="6" class="input-textUpper pr-f11" />
															           </c:if>
															           <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
															           		<input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" readonly="readonly" size="6" class="input-textUpper pr-f11" />
															           </c:if>
															        </c:when> 
															        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && not empty accountLineList.vanlineSettleDate}">
															         	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}"> 
															           		<input type="text" style="text-align:right;background-color: #9cd1fd;border:1px solid #3e88c6;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" readonly="readonly" size="6" class="input-textUpper pr-f11" />
															           </c:if>
															            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}"> 
															            	<input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" readonly="readonly" size="6" class="input-textUpper pr-f11" />
															            </c:if>
															        </c:when>
															        <c:when test="${empty accountLineList.recPostDate && empty accountLineList.recAccDate && empty accountLineList.vanlineSettleDate}">
															         	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}"> 
															        		<input type="text" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" style="text-align:right;" size="6" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onchange="driverCommission('${accountLineList.id}');getId('${accountLineList.id}');"/>
															           </c:if>
															            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
															        		<input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onchange="driverCommission('${accountLineList.id}');getId('${accountLineList.id}');"/>
															            </c:if> 
															        </c:when>
															         <c:otherwise>
															         	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}"> 
															          		<input type="text" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" style="text-align:right;" size="6" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onchange="driverCommission('${accountLineList.id}');getId('${accountLineList.id}');"/>
															            </c:if>
															            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
															          		<input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onchange="driverCommission('${accountLineList.id}');getId('${accountLineList.id}');"/>
															            </c:if> 
															         </c:otherwise>
													        	</c:choose>
													       </c:if>
														</td>
									       			</c:if>
								       				<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								       					
														<td>
															<input type="text" name ="payVatAmt${accountLineList.id}"  value="${accountLineList.payVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
														</td>
														<td>
															<input type="text" name ="recVatAmt${accountLineList.id}"  value="${accountLineList.recVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
														</td> 
													</c:if>
													<td>
														<input type="text" style="text-align:left" name="recInvoiceNumber${accountLineList.id}" value="${accountLineList.recInvoiceNumber }" title="${accountLineList.recInvoiceNumber}" readonly="readonly" size="6" class="input-textUpper pr-f11 " disabled="disabled"/>
													</td>
													<td>
														<input type="text" style="text-align:left;width:180px" name="description${accountLineList.id}" value="${accountLineList.description}" title="${accountLineList.description}" size="15" class="input-text pr-f11" onchange="getId('${accountLineList.id}');"/>
													</td>	
									       		</tr>
									       </c:if>
								       </c:if>
			       <!-- Actual Active Status Ends -->
			       <!-- Actual InActive Status Starts -->
								       <c:if test="${accountLineList.status==false}">	
									       <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
									       		<tr style="background-color: #f0f0f0;">
									       			<td>
									       				<c:if test="${empty accountLineList.recAccDate}">	
															<input type="text" style="text-align:right" name="recQuantity${accountLineList.id}"  value="${accountLineList.recQuantity}" size="4" class="input-text pr-f11" disabled="disabled"/>
														</c:if>
														<c:if test="${not empty accountLineList.recAccDate}">	
															<input type="text" style="text-align:right" name="recQuantity${accountLineList.id}"  value="${accountLineList.recQuantity}" size="4" class="input-text pr-f11 " disabled="disabled"/>
														</c:if>
									       			</td>
									       			<td>
									       				<c:if test="${empty accountLineList.payAccDate}">
															<c:choose>
										                        <c:when test="${accountLineList.country!='' && accountLineList.country!= null }">
										                           <%-- <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
										                          <a onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                      <c:out value="${accountLineList.country}" />
                                                                  </a>
										                        </c:when> 
										                        <c:otherwise>
										                        <%--   <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
										                        <a onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                      <c:out value="${baseCurrency}" />
                                                                  </a>
										                        </c:otherwise>
										                        </c:choose> 
									                    </c:if>
														<c:if test="${not empty accountLineList.payAccDate}">
															<c:choose>
										                        <c:when test="${accountLineList.country!='' && accountLineList.country!= null }">
										                           <%-- <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
										                          <a onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                      <c:out value="${accountLineList.country}" />
                                                                  </a>
										                        </c:when> 
										                        <c:otherwise>
										                          <%-- <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
										                        <a onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                      <c:out value="${baseCurrency}" />
                                                                  </a>
										                        </c:otherwise>
									                        </c:choose> 								
														</c:if>
									       			</td>
									       			<td></td>
									       			<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
									       			<td></td>
									       			</configByCorp:fieldVisibility>
									       			<td class="exprevbg">
									       				<c:if test="${empty accountLineList.payAccDate}">
															<input type="text" style="text-align:right" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" size="6" class="input-text pr-f11 " disabled="disabled"/>
														</c:if>
														<c:if test="${not empty accountLineList.payAccDate}">
															<input type="text" style="text-align:right" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" size="6" class="input-text pr-f11 " disabled="disabled"/>
														</c:if>
														<img id="rateImage"  class="openpopup" align="top" src="${pageContext.request.contextPath}/images/popup_icon14.png" onclick="getPayableSectionDetails('${accountLineList.id}');"/>
														<c:if test="${empty accountLineList.payAccDate}">
															<%-- <img id="rateImage"  class="openpopup" width="16" height="17" align="top" src="${pageContext.request.contextPath}/images/calc_new1.png" onclick="findRevisedQuantityLocalAmounts('${accountLineList.id}');"/> --%>
														</c:if>
														<c:choose>								
													         <c:when test="${accountLineList.payingStatus=='A' || accountLineList.payingStatus=='C'}">
													         	<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
													        </c:when>
													        <c:when test="${accountLineList.payingStatus=='N'}">
													        	<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
													        </c:when>
													        <c:when test="${accountLineList.payingStatus=='P'}">
													        	<img id="target" src="${pageContext.request.contextPath}/images/q1.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
													        </c:when>
													        <c:when test="${accountLineList.payingStatus=='S'}">
													         	<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
													        </c:when>
													         <c:when test="${accountLineList.payingStatus=='I'}">
													         	<img id="active" src="${pageContext.request.contextPath}/images/internal-Cost.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
													         </c:when>
													        <c:otherwise>
													        </c:otherwise>						         
														</c:choose>
									       			</td>
									       			
									       			<td>
														<input type="text" style="text-align:left" name="invoiceNumber${accountLineList.id}" value="${accountLineList.invoiceNumber }" title="${accountLineList.invoiceNumber}" size="6" class="input-text" disabled="disabled"/>
													</td>
													<td>
														<input type="text" style="text-align:left" name="note${accountLineList.id}" value="${accountLineList.note}" title="${accountLineList.note}" size="15" class="input-text pr-f11" disabled="disabled" />
													</td>
									       			<td></td>
									       			
									       			<c:if test="${chargeDiscountFlag}">	
														<td> 
															<c:if test="${empty accountLineList.recAccDate}">	
								 								<c:forEach items="${discountMap}" var="current">   
								       									   <c:if test="${current.key==accountLineList.id}"> 
								       									   <c:set value="${current.value}" var="disMap"/>
								       									   </c:if>
								    						    </c:forEach>  
															<select name ="actualDiscount${accountLineList.id}" id="actualDiscount${accountLineList.id}" style="width:70px" disabled="disabled" class="list-menu pr-f11" > 
																<c:forEach var="chrms" items="${disMap}" varStatus="loopStatus">
								                                  <c:choose>
									                                  <c:when test="${chrms.key == accountLineList.actualDiscount}">
									                                  <c:set var="selectedInd" value=" selected"></c:set>
									                                  </c:when>
									                                 <c:otherwise>
									                                  <c:set var="selectedInd" value=""></c:set>
									                                 </c:otherwise>
								                                  </c:choose>
								                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
								                                <c:out value="${chrms.value}"></c:out>
								                                </option>
								                                </c:forEach> 
															</select>
															</c:if>
															<c:if test="${not empty accountLineList.recAccDate}">	
															<input type="text" name ="actualDiscount${accountLineList.id}"  value="${accountLineList.actualDiscount}" style="width:50px" class="input-text pr-f11 " disabled="disabled">
															</c:if>
														</td>
													</c:if>
													<c:if test="${contractType}">
									       				<td></td>
									       			</c:if>
									       			<td>
									       				<c:if test="${empty accountLineList.recAccDate}">	
															<input type="text" style="text-align:right" name="recRate${accountLineList.id}" value="${accountLineList.recRate}" size="5" class="input-text pr-f11" disabled="disabled"/>
														</c:if>
														<c:if test="${not empty accountLineList.recAccDate}">	
															<input type="text" style="text-align:right" name="recRate${accountLineList.id}" value="${accountLineList.recRate}" size="5" class="input-text pr-f11 " disabled="disabled"/>
														</c:if>	
									       			</td>
									       			<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
									       			<td></td>
									       			</configByCorp:fieldVisibility>
									       			<c:if test="${multiCurrency=='Y'}">
									       				<td>
															<c:if test="${empty accountLineList.recAccDate}">	
																<c:choose>
											                        <c:when test="${accountLineList.recRateCurrency!='' && accountLineList.recRateCurrency !=null }">
											                          <%--  <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
											                            <a onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                           <c:out value="${accountLineList.recRateCurrency}" />
                                                                       </a>
											                        </c:when> 
											                        <c:otherwise>
											                          <%-- <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
											                          <a onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                           <c:out value="${baseCurrency}" />
                                                                       </a>
											                        </c:otherwise>
										                        </c:choose> 
										                     </c:if>
										                     <c:if test="${not empty accountLineList.recAccDate}">	 
											                        <c:choose>
												                        <c:when test="${accountLineList.recRateCurrency!='' && accountLineList.recRateCurrency !=null }"> 
												                           <%-- <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" /> --%>
												                           <a onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                              <c:out value="${accountLineList.recRateCurrency}" />
                                                                           </a>
												                        </c:when> 
												                        <c:otherwise>
												                          <%-- <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" /> --%>
												                        <a onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" class="fxAcc" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');">
                                                                              <c:out value="${baseCurrency}" />
                                                                           </a>
												                        </c:otherwise>
											                        </c:choose>
										                     </c:if>
										                 </td>
													</c:if>
									       			
									       			<td class="exprevbg">
														<c:if test="${accountInterface=='Y'}">
													       <c:choose> 
														       <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
															       <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
															           <input type="text" style="text-align:right;background-color: #fed676;border:1px solid #ef8319;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
															        </c:if>
															       <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
															           <input type="text" style="text-align:right;background-color:#ffcccc;border: 1px solid #f07676;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
															        </c:if>
														        </c:when>
														        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
														        	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
														           		<input type="text" style="text-align:right;background-color:#a5e362;border: 1px solid #00bc0c;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
														         	</c:if>
														        	<c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
														           		<input type="text" style="text-align:right;background-color:#ffcccc;border: 1px solid #f07676;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
														           </c:if>
														        </c:when> 
														        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid')}">
														           		<input type="text" style="text-align:right;background-color:#9cd1fd;border:1px solid #3e88c6;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
														        </c:when>
														        <c:when test="${empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
														        	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
														        		<input type="text" style="text-align:right;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
														           </c:if>
														           <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
																		<input type="text" style="text-align:right;background-color:#ffcccc;border: 1px solid #f07676;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
														           </c:if>
														        </c:when>
														        <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid') && fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && (accountLineList.receivedAmount !='0' && accountLineList.receivedAmount !='0.00' && accountLineList.receivedAmount != null && accountLineList.receivedAmount != '')}">
																    	<input type="text" style="text-align:right;background-color:#ffcccc;border: 1px solid #f07676;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
														         </c:when>
														         <c:otherwise>
														          	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
														          		<input type="text" style="text-align:right" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
														            </c:if>
														            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
															          	<input type="text" style="text-align:right;background-color:#ffcccc;border: 1px solid #f07676;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
														            </c:if>
														         </c:otherwise>
													        </c:choose>
												        </c:if> 
												        <c:if test="${accountInterface!='Y'}">
													         <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
														           <c:choose>
															            <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid') && fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && (accountLineList.receivedAmount !='0' && accountLineList.receivedAmount !='0.00' && accountLineList.receivedAmount != null && accountLineList.receivedAmount != '')}">
																	    	<input type="text" style="text-align:right;background-color:#fed676;border:1px solid #ef8319;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
															          </c:when>
															          <c:otherwise>
															            	<input type="text" style="text-align:right;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>          
															          </c:otherwise>
														           </c:choose>
													           </c:if>
													            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
													         			<input type="text" style="text-align:right;background-color:#ffcccc;border: 1px solid #f07676;" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
													            </c:if>
												        </c:if>
														<configByCorp:fieldVisibility componentId="component.field.Pricing.GeneratingInvoice">
															<c:if test="${accountLineList.recInvoiceNumber != '' && accountLineList.recInvoiceNumber != null}">
													        	<a><img align="top" title="Forms" onclick="findUserPermission1('${accountLineList.recInvoiceNumber}','${accountLineList.companyDivision}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/></a>
													        </c:if>
												        </configByCorp:fieldVisibility>
														<img id="rateImage"  class="openpopup" align="top" src="${pageContext.request.contextPath}/images/popup_icon14.png"  onclick="getReceivableFieldDetails('${accountLineList.id}');" />
									       			</td>
									       			
									       			<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
									       				<td>
															<input type="text" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" style="text-align:right;" size="6" class="input-text pr-f11 " disabled="disabled" />
														</td>
									       			</c:if>
									       			
									       			<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
														<td>
										       				<c:if test="${!vanlineSettleColourStatus}">
										       					<c:choose> 
											       						<c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
																	        <c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
																	           <input type="text" style="text-align:right;background-color: #fed676;border:1px solid #ef8319;" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11 " disabled="disabled"/>
																	        </c:if>
																	        <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
																	           <input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11 " disabled="disabled"/>
																	        </c:if>
																      	</c:when>
															      		<c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
																         	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
																           		<input type="text" style="text-align:right;background-color: #a5e362;border: 1px solid #00bc0c;" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
																           </c:if>
																            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
																           		<input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
																            </c:if>
																        </c:when>
																        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && (accountLineList.paymentStatus =='Fully Paid')}">
																           		<input type="text" style="text-align:right;background-color: #9cd1fd;border:1px solid #3e88c6;" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" disabled="disabled" size="6" class="input-textUpper pr-f11 "/>
																        </c:when>
																       <c:when test="${empty accountLineList.recPostDate && empty accountLineList.recAccDate && (accountLineList.paymentStatus !='Fully Paid')}">
																         	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
																           		<input type="text" style="text-align:right;" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11" disabled="disabled"/>
																           </c:if>
																            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
																           		<input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11 " disabled="disabled"/>
																            </c:if>
																      </c:when>
																	   <c:otherwise>
																	   			<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
																		          		<input type="text" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" style="text-align:right;" size="6" class="input-text pr-f11" disabled="disabled"/>
																				</c:if>
																				<c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
																					<input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11" disabled="disabled"/>
																				</c:if>
																	   </c:otherwise>
															   </c:choose>
															</c:if>
															<c:if test="${vanlineSettleColourStatus}">
													       		<c:choose> 
															       <c:when test="${not empty accountLineList.recPostDate && empty accountLineList.recAccDate && empty accountLineList.vanlineSettleDate}">
																       	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}">
																           <input type="text" style="text-align:right;background-color: #fed676;border:1px solid #ef8319;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11" disabled="disabled"/>
																        </c:if>
																		<c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
																           <input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11" disabled="disabled"/>
																		</c:if>           
															        </c:when>
															        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && empty accountLineList.vanlineSettleDate}">
															       		<c:if test="${accountLineList.paymentStatus !='Partially Paid'}"> 
															           		<input type="text" style="text-align:right;background-color: #a5e362;border: 1px solid #00bc0c;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" size="6" class="input-textUpper pr-f11" disabled="disabled"/>
															           </c:if>
															           <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
															           		<input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" size="6" class="input-textUpper pr-f11" disabled="disabled"/>
															           </c:if>
															        </c:when> 
															        <c:when test="${not empty accountLineList.recPostDate && not empty accountLineList.recAccDate && not empty accountLineList.vanlineSettleDate}">
															         	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}"> 
															           		<input type="text" style="text-align:right;background-color: #9cd1fd;border:1px solid #3e88c6;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" size="6" class="input-textUpper pr-f11" disabled="disabled"/>
															           </c:if>
															            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}"> 
															            	<input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" size="6" class="input-textUpper pr-f11" disabled="disabled"/>
															            </c:if>
															        </c:when>
															        <c:when test="${empty accountLineList.recPostDate && empty accountLineList.recAccDate && empty accountLineList.vanlineSettleDate}">
															         	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}"> 
															        		<input type="text" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" style="text-align:right;" size="6" class="input-text pr-f11" disabled="disabled"/>
															           </c:if>
															            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
															        		<input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11" disabled="disabled"/>
															            </c:if> 
															        </c:when>
															         <c:otherwise>
															         	<c:if test="${accountLineList.paymentStatus !='Partially Paid'}"> 
															          		<input type="text" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" style="text-align:right;" size="6" class="input-text pr-f11" disabled="disabled"/>
															            </c:if>
															            <c:if test="${accountLineList.paymentStatus =='Partially Paid'}">
															          		<input type="text" style="text-align:right;background-color: #FFCCCC;border: 1px solid #f07676;" name ="distributionAmount${accountLineList.id}"  value="${accountLineList.distributionAmount}" size="6" class="input-text pr-f11" disabled="disabled"/>
															            </c:if> 
															         </c:otherwise>
													        	</c:choose>
													       </c:if>
														</td>
									       			</c:if>
									       			
								       				<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								       					
														<td>
															<input type="text" name ="payVatAmt${accountLineList.id}"  value="${accountLineList.payVatAmt}" style="width:50px;text-align:right;" class="input-text pr-f11 " disabled="disabled">
														</td> 
														<td>
															<input type="text" name ="recVatAmt${accountLineList.id}"  value="${accountLineList.recVatAmt}" style="width:50px;text-align:right;" class="input-text pr-f11 " disabled="disabled">
														</td>
													</c:if>
													<td>
														<input type="text" style="text-align:left" name="recInvoiceNumber${accountLineList.id}" value="${accountLineList.recInvoiceNumber }" title="${accountLineList.recInvoiceNumber}" size="6" class="input-text pr-f11 " disabled="disabled"/>
													</td>	
									       			<td>
														<input type="text" style="text-align:left;width:180px" name="description${accountLineList.id}" value="${accountLineList.description}" title="${accountLineList.description}" size="15" class="input-text pr-f11" disabled="disabled"/>
													</td>
									       		</tr>
									       </c:if>
								       </c:if>
		       <!-- Actual InActive Status Ends -->
								  
					 <!-- All Hidden Fields Starts-->
					 <input type="hidden" id="id" name="id" value="${accountLineList.id}" />
					 <input type="hidden" id="pricePointAgent${accountLineList.id}"  name="pricePointAgent${accountLineList.id}" />
				     <input type="hidden" id="pricePointMarket${accountLineList.id}"  name="pricePointMarket${accountLineList.id}" />
				     <input type="hidden" id="pricePointTariff${accountLineList.id}"  name="pricePointTariff${accountLineList.id}" />
				     <input type="hidden" name="actgCode${accountLineList.id}" value="${accountLineList.actgCode}"/>
				     <input type="hidden"  name="oldbasis${accountLineList.id}" value="${accountLineList.basis}"/>
				     <input type="hidden"  name="vendorCodeOwneroperator${accountLineList.id}" id="vendorCodeOwneroperator${accountLineList.id}"/>
					<input type="hidden"  name="accChargeCodeTemp${accountLineList.id}" value="${accountLineList.chargeCode}"/>
					<input type="hidden"  name="recGl${accountLineList.id}" value="${accountLineList.recGl}"/>
					<input type="hidden"  name="payGl${accountLineList.id}" value="${accountLineList.payGl}"/>
					<input type="hidden"  name="contractExchangeRate${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>
					<input type="hidden"  name="contractValueDate${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
					<input type="hidden"  name="payableContractCurrency${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
					<input type="hidden"  name="payableContractExchangeRate${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
					<input type="hidden"  name="payableContractValueDate${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
					<input type="hidden"  name="division${accountLineList.id}" value="${accountLineList.division}"/>
					<input type="hidden"  name="payPostDate${accountLineList.id}" value="${accountLineList.payPostDate}"/>
					<input type="hidden"  name="VATExclude${accountLineList.id}" value="${accountLineList.VATExclude}"/>
					<input type="hidden"  name="oldVATExclude${accountLineList.id}" value="${accountLineList.VATExclude}"/>
					<input type="hidden"  name="vendorCodeOld${accountLineList.id}" value="${accountLineList.vendorCode}"/>
					<input type="hidden"  name="estimateVendorNameOld${accountLineList.id}" value="${accountLineList.estimateVendorName}"/>
					<input type="hidden" name="actgCodeOld${accountLineList.id}" value="${accountLineList.actgCode}"/>
					<input type="hidden"  name="estSellCurrencyNew${accountLineList.id}" value="${accountLineList.estSellCurrency}"/>
					<input type="hidden"  name="estSellCurrency${accountLineList.id}" value="${accountLineList.estSellCurrency}"/>
						<input type="hidden"  name="estSellValueDateNew${accountLineList.id}" value="${accountLineList.estSellValueDate}"/>
						<input type="hidden"  name="estSellExchangeRateNew${accountLineList.id}" value="${accountLineList.estSellExchangeRate}"/>
						<input type="hidden"  name="estSellExchangeRate${accountLineList.id}" value="${accountLineList.estSellExchangeRate}"/>
						<input type="hidden"  name="estSellLocalRateNew${accountLineList.id}" value="${accountLineList.estSellLocalRate}"/>
						<input type="hidden"  name="estSellLocalAmountNew${accountLineList.id}" value="${accountLineList.estSellLocalAmount}"/>
						<input type="hidden"  name="estSellLocalAmount${accountLineList.id}" value="${accountLineList.estSellLocalAmount}"/>
					<c:if test="${systemDefaultVatCalculationNew!='Y'}">
						<input type="hidden"  name="vatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}"/>
						<input type="hidden"  name="estVatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}"/>	
						<input type="hidden"  name="estExpVatAmt${accountLineList.id}" value="${accountLineList.estExpVatAmt}"/>								
					</c:if>	
					<c:if test="${systemDefaultVatCalculationNew!='Y' || discountUserFlag.pricingRevision!=true}">
						<input type="hidden"  name="revisionVatAmt${accountLineList.id}" value="${accountLineList.revisionVatAmt}"/>
						<input type="hidden"  name="revisionExpVatAmt${accountLineList.id}" value="${accountLineList.revisionExpVatAmt}"/>								
					</c:if>
					<c:if test="${systemDefaultVatCalculationNew!='Y' || discountUserFlag.pricingActual!=true}">
						<input type="hidden"  name="recVatAmt${accountLineList.id}" value="${accountLineList.recVatAmt}"/>								
						<input type="hidden"  name="payVatAmt${accountLineList.id}" value="${accountLineList.payVatAmt}"/>																														
					</c:if>	
					<c:if test="${systemDefaultVatCalculationNew!='Y'}">
						<input type="hidden"  name="recVatDescr${accountLineList.id}" value="${accountLineList.recVatDescr}"/>
						<input type="hidden"  name="recVatPercent${accountLineList.id}" value="${accountLineList.recVatPercent}"/>
						<input type="hidden"  name="payVatDescr${accountLineList.id}" value="${accountLineList.payVatDescr}"/>
						<input type="hidden"  name="payVatPercent${accountLineList.id}" value="${accountLineList.payVatPercent}"/>
					</c:if>
					<input type="hidden"  name="accRecVatDescr${accountLineList.id}" value="${accountLineList.recVatDescr}"/>
					<input type="hidden"  name="accPayVatDescr${accountLineList.id}"  value="${accountLineList.payVatDescr}"/>
					<input type="hidden"  name="oldRecVatDescr${accountLineList.id}" value="${accountLineList.recVatDescr}"/>
					<input type="hidden"  name="oldPayVatDescr${accountLineList.id}" value="${accountLineList.payVatDescr}"/>	
					<c:if test="${contractType}">
						<input type="hidden"  name="contractCurrency${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
						<input type="hidden" id="estimatePayableContractCurrencyNew${accountLineList.id}" name="estimatePayableContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimatePayableContractCurrency}"/>
						<input type="hidden" id="estimatePayableContractValueDateNew${accountLineList.id}" name="estimatePayableContractValueDateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractValueDate}"/>
						<input type="hidden" id="estimatePayableContractExchangeRateNew${accountLineList.id}" name="estimatePayableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractExchangeRate}"/>
						<input type="hidden" id="estimatePayableContractRateNew${accountLineList.id}" name="estimatePayableContractRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRate}"/>
						<input type="hidden"  name="estimatePayableContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRateAmmount}"/>
						<input type="hidden"  name="estimateContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimateContractCurrency}"/>
						<input type="hidden"  name="estimateContractValueDateNew${accountLineList.id}" value="${accountLineList.estimateContractValueDate}"/>
						<input type="hidden"  name="estimateContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimateContractExchangeRate}"/>
						<input type="hidden"  name="estimateContractExchangeRate${accountLineList.id}" value="${accountLineList.estimateContractExchangeRate}"/>
						<input type="hidden"  name="estimateContractRateNew${accountLineList.id}" value="${accountLineList.estimateContractRate}"/>
						<input type="hidden"  name="estimateContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimateContractRateAmmount}"/>					
						<input type="hidden"  name="revisionContractCurrency${accountLineList.id}" value="${accountLineList.revisionContractCurrency}"/>
						<input type="hidden"  name="revisionContractValueDate${accountLineList.id}" value="${accountLineList.revisionContractValueDate}"/>
						<input type="hidden"  name="revisionContractExchangeRate${accountLineList.id}" value="${accountLineList.revisionContractExchangeRate}"/>
						<input type="hidden"  name="revisionPayableContractCurrency${accountLineList.id}" value="${accountLineList.revisionPayableContractCurrency}"/>
						<input type="hidden"  name="revisionPayableContractValueDate${accountLineList.id}" value="${accountLineList.revisionPayableContractValueDate}"/>
						<input type="hidden"  name="revisionPayableContractExchangeRate${accountLineList.id}" value="${accountLineList.revisionPayableContractExchangeRate}"/>
						<input type="hidden"  name="contractCurrencyNew${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
						<input type="hidden"  name="contractValueDateNew${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
						<input type="hidden"  name="contractExchangeRateNew${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>
						<input type="hidden"  name="payableContractCurrencyNew${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
						<input type="hidden"  name="payableContractValueDateNew${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
						<input type="hidden"  name="payableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
					</c:if>						
					<input type="hidden"  id="estCurrencyNew${accountLineList.id}" name="estCurrencyNew${accountLineList.id}" value="${accountLineList.estCurrency}"/>
					<input type="hidden"  id="estValueDateNew${accountLineList.id}" name="estValueDateNew${accountLineList.id}" value="${accountLineList.estValueDate}"/>
					<input type="hidden"  id="estExchangeRateNew${accountLineList.id}" name="estExchangeRateNew${accountLineList.id}" value="${accountLineList.estExchangeRate}"/>
					<input type="hidden"  id="estLocalRateNew${accountLineList.id}" name="estLocalRateNew${accountLineList.id}" value="${accountLineList.estLocalRate}"/>
					<input type="hidden"  name="estLocalAmountNew${accountLineList.id}" value="${accountLineList.estLocalAmount}"/>
					
					<input type="hidden"  name="buyDependSellNew${accountLineList.id}" value="${accountLineList.buyDependSell}"/>
					<input type="hidden"  name="buyDependSell${accountLineList.id}" value="${accountLineList.buyDependSell}"/>
					<input type="hidden"  name="oldEstimateSellDeviationNew${accountLineList.id}" />								
					<input type="hidden"  name="oldEstimateDeviationNew${accountLineList.id}" />
					<input type="hidden"  name="revisionCurrency${accountLineList.id}" value="${accountLineList.revisionCurrency}"/>
					<input type="hidden"  name="revisionValueDate${accountLineList.id}" value="${accountLineList.revisionValueDate}"/>
					<input type="hidden"  name="revisionExchangeRate${accountLineList.id}" value="${accountLineList.revisionExchangeRate}"/>
					<input type="hidden"  name="estLocalAmount${accountLineList.id}" value="${accountLineList.estLocalAmount}"/>
					<input type="hidden"  name="countryNew${accountLineList.id}" value="${accountLineList.country}"/>
					<input type="hidden"  name="valueDateNew${accountLineList.id}" value="${accountLineList.valueDate}"/>
					<input type="hidden"  name="exchangeRateNew${accountLineList.id}" value="${accountLineList.exchangeRate}"/>
					<input type="hidden"  name="accountLineCostElement${accountLineList.id}" value="${accountLineList.accountLineCostElement}"/>
					<input type="hidden"  name="accountLineScostElementDescription${accountLineList.id}" value="${accountLineList.accountLineScostElementDescription}"/>
					<input type="hidden"  name="itemNew${accountLineList.id}" value="${accountLineList.itemNew}"/>
					<input type="hidden"  name="checkNew${accountLineList.id}" value="${accountLineList.checkNew}"/>
					<c:if test="${companyDivisionGlobal<=1}"> 
	                   	<input type="hidden" name="tempCompanyDivision${accountLineList.id}" id="tempCompanyDivision${accountLineList.id}"  value="${accountLineList.companyDivision}" />
					</c:if>
					<c:if  test="${compDivFlag!='Yes'}">
						<input type="hidden" name="companyDivision${accountLineList.id}"  value="${accountLineList.companyDivision}"/>
					</c:if>
					<input type="hidden" name ="deviation${accountLineList.id}"  value="${accountLineList.deviation}" />
					<input type="hidden" name ="estimateDeviation${accountLineList.id}"  value="${accountLineList.estimateDeviation}" />
					<input type="hidden" name ="estimateSellDeviation${accountLineList.id}"  value="${accountLineList.estimateSellDeviation}" />
					<input type="hidden"  name="oldestimateQuantity${accountLineList.id}" value="${accountLineList.estimateQuantity }"/>
					<input type="hidden"  name="oldEstimateDiscount${accountLineList.id}" value="${accountLineList.estimateDiscount}"/>
					<input type="hidden"  name="oldEstimateSellQuantity${accountLineList.id}" value="${accountLineList.estimateSellQuantity}"/>
					<input type="hidden"  name="estVatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}"/>
					<input type="hidden"  name="recRateCurrency${accountLineList.id}" value="${accountLineList.recRateCurrency}"/>
						<input type="hidden"  name="racValueDate${accountLineList.id}" value="${accountLineList.racValueDate}"/>
						<input type="hidden"  name="recRateExchange${accountLineList.id}" value="${accountLineList.recRateExchange}"/>
						<input type="hidden"  name="recCurrencyRate${accountLineList.id}" value="${accountLineList.recCurrencyRate}"/>
						<input type="hidden"  name="actualRevenueForeign${accountLineList.id}" value="${accountLineList.actualRevenueForeign}"/>
					
					<!-- Hidden Fields For Revision Starts-->
					<input type="hidden" name ="revisionDeviation${accountLineList.id}"  value="${accountLineList.revisionDeviation}" />
					<input type="hidden" name ="revisionSellDeviation${accountLineList.id}"  value="${accountLineList.revisionSellDeviation}" />
					<input type="hidden" name ="oldRevisionSellDeviation${accountLineList.id}" />
					<input type="hidden" name ="oldRevisionDeviation${accountLineList.id}" />
					<input type="hidden"  name="oldRevisionQuantity${accountLineList.id}" value="${accountLineList.revisionQuantity }"/>
					<c:if test="${contractType}">
						<input type="hidden"  name="revisionPayableContractRate${accountLineList.id}" value="${accountLineList.revisionPayableContractRate}"/>
						<input type="hidden"  name="revisionPayableContractRateAmmount${accountLineList.id}" value="${accountLineList.revisionPayableContractRateAmmount}"/>
						<input type="hidden"  name="revisionContractRate${accountLineList.id}" value="${accountLineList.revisionContractRate}"/>
						<input type="hidden"  name="revisionContractRateAmmount${accountLineList.id}" value="${accountLineList.revisionContractRateAmmount}"/>
					</c:if>
					<input type="hidden"  name="revisionLocalRate${accountLineList.id}" value="${accountLineList.revisionLocalRate}"/>
					<input type="hidden"  name="revisionLocalAmount${accountLineList.id}" value="${accountLineList.revisionLocalAmount}"/>
					<c:if test="${multiCurrency=='Y'}">
						<input type="hidden"  name="revisionSellCurrency${accountLineList.id}" value="${accountLineList.revisionSellCurrency}"/>
						<input type="hidden"  name="revisionSellValueDate${accountLineList.id}" value="${accountLineList.revisionSellValueDate}"/>
						<input type="hidden"  name="revisionSellExchangeRate${accountLineList.id}" value="${accountLineList.revisionSellExchangeRate}"/>
						<input type="hidden"  name="revisionSellLocalRate${accountLineList.id}" value="${accountLineList.revisionSellLocalRate}"/>
						<input type="hidden"  name="revisionSellLocalAmount${accountLineList.id}" value="${accountLineList.revisionSellLocalAmount}"/>
					</c:if>
					<input type="hidden"  name="oldRevisionDiscount${accountLineList.id}" value="${accountLineList.revisionDiscount}"/>
					<input type="hidden"  name="oldRevisionSellQuantity${accountLineList.id}" value="${accountLineList.revisionSellQuantity }"/>
					<input type="hidden"  name="oldRevisionSellRate${accountLineList.id}" value="${accountLineList.revisionSellRate}"/>
					<!-- Hidden Fields For Revision Ends-->
					<input type="hidden"  name="tempRecQuantity${accountLineList.id}" value="${accountLineList.recQuantity}"/>
					<input type="hidden" name="oldRecRate${accountLineList.id}" value="${accountLineList.checkNew}"/>
					<input type="hidden" name="checkNew${accountLineList.id}" value="${accountLineList.checkNew}"/>
					<input type="hidden" name="basisNew${accountLineList.id}" value="${accountLineList.basisNew}"/>
					<input type="hidden" name="basisNewType${accountLineList.id}" value="${accountLineList.basisNewType}"/>
					<input type="hidden" name="receivableSellDeviation${accountLineList.id}" value="${accountLineList.receivableSellDeviation}"/>
					<input type="hidden" name="oldReceivableDeviation${accountLineList.id}" value="${accountLineList.receivableSellDeviation}"/>
					<c:if test="${contractType}">
						<input type="hidden"  name="payableContractRateAmmount${accountLineList.id}" value="${accountLineList.payableContractRateAmmount}"/>
						<input type="hidden"  name="contractRate${accountLineList.id}" value="${accountLineList.contractRate}"/>
						<input type="hidden"  name="contractRateAmmount${accountLineList.id}" value="${accountLineList.contractRateAmmount}"/>
					</c:if>
					<input type="hidden"  name="country${accountLineList.id}" value="${accountLineList.country}"/>
					<input type="hidden"  name="valueDate${accountLineList.id}" value="${accountLineList.valueDate}"/>
					<input type="hidden"  name="exchangeRate${accountLineList.id}" value="${accountLineList.exchangeRate}"/>
					<input type="hidden"  name="localAmount${accountLineList.id}" value="${accountLineList.localAmount}"/>
					
					<input type="hidden"  name="recInvoiceNumber${accountLineList.id}" value="${accountLineList.recInvoiceNumber}"/>						
					<input type="hidden"  name="invoiceNumber${accountLineList.id}" value="${accountLineList.invoiceNumber}"/>
					<input type="hidden"  name="invoiceDate${accountLineList.id}" value="${accountLineList.invoiceDate}"/>
					<input type="hidden"  name="receivedDate${accountLineList.id}" value="${accountLineList.receivedDate}"/>
					<input type="hidden"  name="payingStatus${accountLineList.id}" value="${accountLineList.payingStatus}"/>
					
					<input type="hidden"  name="recVatGl${accountLineList.id}" value="${accountLineList.recVatGl}"/>
					<input type="hidden"  name="payVatGl${accountLineList.id}" value="${accountLineList.payVatGl}"/>
					<input type="hidden"  name="qstRecVatGl${accountLineList.id}" value="${accountLineList.qstRecVatGl}"/>
					<input type="hidden"  name="qstPayVatGl${accountLineList.id}" value="${accountLineList.qstPayVatGl}"/>
					<input type="hidden"  name="qstRecVatAmt${accountLineList.id}" value="${accountLineList.qstRecVatAmt}"/>
					<input type="hidden"  name="qstPayVatAmt${accountLineList.id}" value="${accountLineList.qstPayVatAmt}"/>
					<input type="hidden"  name="payDeviation${accountLineList.id}" value="${accountLineList.payDeviation}"/>
					<input type="hidden"  name="oldPayDeviation${accountLineList.id}" value="${accountLineList.payDeviation}"/>
					<input type="hidden"  name="createdBy${accountLineList.id}" id="createdBy${accountLineList.id}" value="${accountLineList.createdBy}"/>
					<input type="hidden"  name="networkSynchedId${accountLineList.id}" value="${accountLineList.networkSynchedId}"/>
					<input type="hidden"  name="oldActualDiscount${accountLineList.id}" value="${accountLineList.actualDiscount}"/>
					<c:set var="recTemp1" value="${accountLineList.recVatAmt}" />
					<c:set var="recTemp2" value="${accountLineList.qstRecVatAmt}" />
					<c:set var="recTemp" value="${recTemp1 + recTemp2}"/>
					<input type="hidden" name ="recVatAmtTemp${accountLineList.id}"  value="${recTemp}" />
					<c:set var="payTemp1" value="${accountLineList.payVatAmt}" />
					<c:set var="payTemp2" value="${accountLineList.qstPayVatAmt}" />
					<c:set var="payTemp" value="${payTemp1 + payTemp2}"/>
					<input type="hidden" name ="payVatAmtTemp${accountLineList.id}"  value="${payTemp}" />
					<input type="hidden" name="recAccDate${accountLineList.id}" id="recAccDate${accountLineList.id}"  value="${accountLineList.recAccDate}" />
					<input type="hidden" name="payAccDate${accountLineList.id}" id="payAccDate${accountLineList.id}"  value="${accountLineList.payAccDate}" />
					<input type="hidden" name="distributionAmount${accountLineList.id}" value="${accountLineList.distributionAmount}" />
					<input type="hidden" name="receivedAmount${accountLineList.id}" value="${accountLineList.receivedAmount}" />
					<input type="hidden" name="paymentStatus${accountLineList.id}" value="${accountLineList.paymentStatus}" />
					<input type="hidden" name="includeLHF${accountLineList.id}" value="${accountLineList.includeLHF}" />
					<input type="hidden" name="ignoreForBilling${accountLineList.id}" value="${accountLineList.ignoreForBilling}" />
					 <!-- All Hidden Fields Ends-->
					 </tbody>
					</table>
					</td>
					 </tr>
			</c:forEach>
		</tbody>
	</table>
	</c:if>
	
			<c:if test="${accountLineList=='[]'}">
				<table  class="table-price" id="dataTableD" style="width:98%;margin-top:2px;margin-bottom:0px;" border="0" cellpadding="0" cellspacing="0"> 
						<thead>
							<tr>
								<th>Audit</th>
								<th>#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Active</th>
								<th>Category</th>
								<c:if  test="${compDivFlag == 'Yes'}">
									<th>Division</th>
								</c:if>
								<th>Charge  Code</th>
								<th>Bill To Code</th>
								<th>Bill To Name</th>
								<th>Vendor Code</th>
								<th>Vendor  Name</th>
								<th>Basis</th>
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
									<th>Rec VAT Desc</th>
									<th>VAT&nbsp;%</th>
									<th>Pay VAT Desc</th>
									<th>VAT&nbsp;%</th>
								</c:if>
								<th style="width:2%;"></th> 
							</tr>
						</thead>
						<tbody>
					<tr>
					 	<c:if test="${systemDefaultVatCalculationNew!='Y'}">
					 		<td colspan="11" class="listwhitetextWhite">Nothing Found To Display.</td>
					 	</c:if>
					 	<c:if test="${systemDefaultVatCalculationNew=='Y'}">
					 		<td colspan="15" class="listwhitetextWhite">Nothing Found To Display.</td>
					 	</c:if> 
					</tr>
					
					 <tr>
					 	<td></td>
					 	
					 	<td align="right">
						 		<table cellspacing="0" cellpadding="0" style="margin:0px;margin-top:13px;">
						 			<tr>
						 			<td style="border:none !important;padding:0px !important;">
						 			<img id="rateImage"  style="cursor:default;" align="top" src="${pageContext.request.contextPath}/images/est-btn.png" />
						 			</td>
						 			</tr>
						 		<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">						 		
						 			<tr>
						 			<td style="border:none !important;padding:0px !important;padding-top:3px;">
						 			<img id="rateImage"  style="cursor: default; position: relative; top: 3px;" align="top" src="${pageContext.request.contextPath}/images/rev-btn.png" />
						 			</td>
						 			</tr>
						 		</c:if>
						 		<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">						 		
						 			<tr>
						 			<td style="border:none !important;padding:0px !important;">
						 			<img id="rateImage"  style="cursor: default; position: relative; top: 4px;" align="top" src="${pageContext.request.contextPath}/images/act-btn.png" />
						 			</td>
						 			</tr>
						 		</c:if>
						 		</table>
					 	</td>
					 	
						<td colspan="7" style="margin-top: 0px;">
							  
								  <table class="table-price" style="width:100%;margin-bottom: 0px;height:70px;" border="0" cellpadding="0" cellspacing="0">
								     <thead>
									       <tr>  
										        <th >Quantity</th>
										        <th >Buy Rate</th>
										        <configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
										        <th>Variance</th>
										        </configByCorp:fieldVisibility>
										        <th >FX</th>
										        <c:if test="${chargeDiscountFlag}">	
													<th>Disc %</th>
												</c:if>
												<c:if test="${contractType}">
													<th >Rec Quantity</th>
												</c:if>
										        <th>Sell Rate</th>
										        <configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
										        <th>Adjustment</th>
										        </configByCorp:fieldVisibility>
										        <c:if test="${multiCurrency=='Y'}">
													<th >FX</th>
												</c:if>
										        <th >Expense</th>
										        <th>Invoice#</th>
										        <th >Description</th>
										        <th >Mk%</th>
										        <th >Revenue</th>
										        <c:if test="${systemDefaultVatCalculationNew=='Y'}">
													<th >Pay VAT Amt</th>
													<th >Rec VAT Amt</th>
												</c:if>
												<th>Invoice#</th>
												<th >Description</th>
									       </tr>
									    </thead>
									    	
									    		<tr>  
										        	<td colspan="13" class="listwhitetextWhite">Nothing Found To Display.</td>
									       		</tr>
									    		
										 </table>
								</td>
							</tr>
						</tbody>
					</table>
				</c:if>
	
	
	<table  style="margin-top:-3px;margin-bottom:0px;width:98%" border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td>
	<table class="table-priceFoot pricemainbg" cellpadding="0" cellspacing="0" border="0" style="/*margin-left:105px;*/ margin-bottom:0px;width:50%; height:138px;float:left;" >
       <tr class="pricefooter pft-top" style="color:#15428b;font-size: 12px;">
            <td width="">&nbsp;</td>
	        <td style="padding-left:5px;">Expense</td>
	        <td>&nbsp;Revenue</td>
	        <td>&nbsp;Gross&nbsp;Margin</td>
	        <td style="padding-right:15px">&nbsp;Gross&nbsp;Margin%</td>		        
       </tr>
       <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			<tr class="pricefooter" >
				<td align="right" style="color: #15428b;font-family: arial,verdana;font-size: 12px;width:24%">&nbsp;&nbsp;Projected&nbsp;Actual&nbsp;Totals&nbsp;</td>				
				<td width="100" class="pr-f12Acc">
				<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${serviceOrder.projectedActualExpense}" />
				<%-- <input type="text" style="text-align:right;margin-left:5px" name="projectedActualExpense${serviceOrder.id}" value="${serviceOrder.projectedActualExpense}" readonly="readonly" size="6" class="input-textfooter pr-f11 "/> --%>
				</td>
				<td width="100" class="pr-f12Acc">
				<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${serviceOrder.projectedActualRevenue}" />
				<%-- <input type="text" style="text-align:right;margin-left:5px" name="projectedActualRevenue${serviceOrder.id}" value="${serviceOrder.projectedActualRevenue}" readonly="readonly" size="6" class="input-textfooter pr-f11 "/> --%>
				</td>
				<td></td><td></td>				
			</tr>
		</c:if>
			<tr class="pricefooter" >
				<td align="right" style="color: #15428b;font-family: arial,verdana;font-size: 12px;width:17%">&nbsp;&nbsp;Total&nbsp;Estimate&nbsp;</td>
				<td width="90" class="pr-f12Acc">
				<fmt:formatNumber type="number" maxFractionDigits="0" groupingUsed="true" value="${serviceOrder.estimatedTotalExpense}" />
				<%-- <input type="text" style="text-align:right;margin-left:5px" name="estimatedTotalExpense${serviceOrder.id}" value="${serviceOrder.estimatedTotalExpense}" readonly="readonly" size="6" class="input-textfooter pr-f11 "/> --%>
				</td>
				<td width="90" class="pr-f12Acc">
				<fmt:formatNumber type="number" maxFractionDigits="0" groupingUsed="true" value="${serviceOrder.estimatedTotalRevenue}" />
				<%-- <input type="text" style="text-align:right;margin-left:5px" name="estimatedTotalRevenue${serviceOrder.id}" value="${serviceOrder.estimatedTotalRevenue}" readonly="readonly" size="6" class="input-textfooter pr-f11 "/> --%>
				</td>
				<td width="100" class="pr-f12Acc">
				<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${serviceOrder.estimatedGrossMargin}" />
				<%-- <input type="text" style="text-align:right;margin-left:5px" name="estimatedGrossMargin${serviceOrder.id}" value="${serviceOrder.estimatedGrossMargin}" readonly="readonly" size="6" class="input-textfooter pr-f11 "/> --%>
				</td>
				<td width="90" class="pr-f12Acc">
				<fmt:formatNumber type="number" maxFractionDigits="1" groupingUsed="true" value="${serviceOrder.estimatedGrossMarginPercentage}" />
				<%-- <input type="text" style="text-align:right;margin-left:5px" name="estimatedGrossMarginPercentage${serviceOrder.id}" value="${serviceOrder.estimatedGrossMarginPercentage}" readonly="readonly" size="6" class="input-textfooter pr-f11 "/> --%>
				</td>
				
			</tr>
		<%-- <c:if test="${(discountUserFlag.pricingActual==false) && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			<tr class="pricefooter" >
				<td colspan="12"></td>
			</tr>
		</c:if> --%>
		<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			<tr class="pricefooter" >
				<td align="right" style="color: #15428b;font-family: arial,verdana;font-size: 12px;">&nbsp;&nbsp;Total&nbsp;Revision&nbsp;</td>
				<td class="pr-f12Acc">
				<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${serviceOrder.revisedTotalExpense}" />
				<%-- <input type="text" style="text-align:right;margin-left:5px" name="revisedTotalExpense${serviceOrder.id}" value="${serviceOrder.revisedTotalExpense}" readonly="readonly" size="6" class="input-textfooter pr-f11 "/> --%>
				</td>
				<td class="pr-f12Acc">
				<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${serviceOrder.revisedTotalRevenue}" />
				<%-- <input type="text" style="text-align:right;margin-left:5px" name="revisedTotalRevenue${serviceOrder.id}" value="${serviceOrder.revisedTotalRevenue}" readonly="readonly" size="6" class="input-textfooter pr-f11 "/> --%>
				</td>
				<td class="pr-f12Acc">
				<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${serviceOrder.revisedGrossMargin}" />
				<%-- <input type="text" style="text-align:right;margin-left:5px" name="revisedGrossMargin${serviceOrder.id}" value="${serviceOrder.revisedGrossMargin}" readonly="readonly" size="6" class="input-textfooter pr-f11 "/> --%>
				</td>
				<td class="pr-f12Acc">
				<fmt:formatNumber type="number" maxFractionDigits="1" groupingUsed="true" value="${serviceOrder.revisedGrossMarginPercentage}" />
				<%-- <input type="text" style="text-align:right;margin-left:5px" name="revisedGrossMarginPercentage${serviceOrder.id}" value="${serviceOrder.revisedGrossMarginPercentage}" readonly="readonly" size="6" class="input-textfooter pr-f11 "/> --%>
				</td>
				
			</tr>
		</c:if>
		<%-- <c:if test="${(discountUserFlag.pricingRevision==false) && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			<tr class="pricefooter" >
				<td colspan="12"></td>
			</tr>
		</c:if> --%>
		<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			<tr class="pricefooter" >
				<td align="right" style="color: #15428b;font-family: arial,verdana;font-size: 12px;">&nbsp;&nbsp;Total&nbsp;Actual&nbsp;</td>
				<td class="pr-f12Acc">
				<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${serviceOrder.actualExpense}" />
				<%-- <input type="text" style="text-align:right;margin-left:5px" name="actualTotalExpense${serviceOrder.id}" value="${serviceOrder.actualExpense}" readonly="readonly" size="6" class="input-textfooter pr-f11 "/> --%>
				<img class="openpopup" style="vertical-align:bottom;" src="${pageContext.request.contextPath}/images/invoice.png" title="Approved Vendor Invoice List" onclick="findVendorInvoiceTotal(this);"/>
				</td>
				<td class="pr-f12Acc">
				<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${serviceOrder.actualRevenue}" />
				<%-- <input type="text" style="text-align:right;margin-left:5px" name="actualTotalRevenue${serviceOrder.id}" value="${serviceOrder.actualRevenue}" readonly="readonly" size="6" class="input-textfooter pr-f11 "/> --%>
				<img class="openpopup" style="vertical-align:bottom;"  src="${pageContext.request.contextPath}/images/invoice.png" title="Invoice List With Sub-Total" onclick="findInvoiceTotal(this);"/>
				</td>
				<td class="pr-f12Acc">
				<fmt:formatNumber type="number" maxFractionDigits="2" groupingUsed="true" value="${serviceOrder.actualGrossMargin}" />
				<%-- <input type="text" style="text-align:right;margin-left:5px" name="actualGrossMargin${serviceOrder.id}" value="${serviceOrder.actualGrossMargin}" readonly="readonly" size="6" class="input-textfooter pr-f11 "/></td> --%>
				<td class="pr-f12Acc">
				<fmt:formatNumber type="number" maxFractionDigits="1" groupingUsed="true" value="${serviceOrder.actualGrossMarginPercentage}" />
				<%-- <input type="text" style="text-align:right;margin-left:5px" name="actualGrossMarginPercentage${serviceOrder.id}" value="${serviceOrder.actualGrossMarginPercentage}" readonly="readonly" size="6" class="input-textfooter pr-f11 "/> --%>
				</td>
				
			</tr>
		</c:if>
		<%-- <c:if test="${(discountUserFlag.pricingActual==false) && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			<tr class="pricefooter" >
				<td colspan="12"></td>
			</tr>
		</c:if> --%>
		</table>
<!-- Billing Section Starts -->	
		<table class="" cellpadding="0" cellspacing="0" border="0" style="margin-bottom:0px;height:138px;float:left;border:2px solid #74b3dc;border-left:none; width:50%" >
       		<tr class="pricefooter pft-top" style="color:#15428b;font-size: 12px;">
		        <td style="padding-left:25px;line-height:25px;border-bottom:1px solid #e0e0e0;color:#024abf;" colspan="12" align="center">Billing Details</td>
       		</tr>
       		<tr>
       			<td  colspan="12">
       				<table style="margin:0px;margin-left: 10px">
       					<tr>
       					<td class="listwhitetext" align="right">Rev.Recog</td>
       						<td>
       							<fmt:parseDate pattern="yyyy-MM-dd" value="${billing.revenueRecognition}" var="parsedRevenueRecognition" />
								<fmt:formatDate pattern="dd-MMM-yy" value="${parsedRevenueRecognition}" var="formattedRevenueRecognition" />
								<input type="text" name="billing.revenueRecognition" size="8" value="${formattedRevenueRecognition}" readonly="readonly" class="input-textUpper pr-f11"/>
       						</td>
       						<td class="listwhitetext" align="right">Bill&nbsp;To <font color="red" size="2">*</font></td>
       						<td>
       							<input type="text" style="text-align:left;" id="billToCode" name="billing.billToCode" value="${billing.billToCode}" size="6" readonly="readonly" class="input-textUpper pr-f11"/>
       						</td>
       						
       						<td class="listwhitetext" align="right" width="67">Bill&nbsp;To&nbsp;Name</td>
       						<td>
       							<input type="text" style="text-align:left;" name="billing.billToName" value="${billing.billToName}" size="30" readonly="readonly" class="input-textUpper pr-f11"/>
       						</td>
       					</tr>
       					
       					<tr>
       						<td class="listwhitetext" align="right" >Pricing&nbsp;Contract<font color="red" size="2">*</font></td>
       						<td colspan="3">
       							<input type="text" style="text-align:left;margin-top:2px" name="billing.contract" value="${billing.contract}" size="25" readonly="readonly" class="input-textUpper pr-f11"/>
       							<%-- <select name ="billing.contract" style="width:140px" class="list-menu pr-f11"> 
	       							<c:forEach var="chrms" items="${billingContractMap}" varStatus="loopStatus">
				                     <c:choose>
					                      <c:when test="${chrms.value == billing.contract}">
					                      		<c:set var="selectedInd" value=" selected"></c:set>
					                      </c:when>
					                      <c:otherwise>
					                       		<c:set var="selectedInd" value=""></c:set>
					                      </c:otherwise>
				                      </c:choose>
					                     <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
					                    		<c:out value="${chrms.value}"></c:out>
					                    </option>
							     	</c:forEach>
						     	</select> --%>
       						</td>
       						
       						<td class="listwhitetext" align="right">Email</td>
       						<td>
       							<input type="text" style="text-align:left;" id="billingEmail" name="billing.billingEmail" value="${billing.billingEmail}" size="30" readonly="readonly" class="input-textUpper pr-f11"/>
       						</td>
       						<td align="left"  width="17" height="20" class="listwhitetext"> <div id="openpopupnetworkimg" style=""><img class="openpopup" onclick="getContact();"  src="<c:url value='/images/plus-small.png'/>" /></td>
							<td align="center" class="listwhitetext" style="width:25px"><div id="billingEmailImage" style="vertical-align:middle; "><img class="openpopup" onclick="sendEmail();" src="<c:url value='/images/email_small.gif'/>" title="Send Email to:${billing.billingEmail} "/></td>	
       						
       					</tr>
       					
       					<tr>
       					<td colspan="8" align="left;">
       					<table style="margin:0px;">
       					
						<tr>
							<td class="listwhitetext" width="78" align="right">Billing&nbsp;Complete</td>
       						<td>
       							<fmt:parseDate pattern="yyyy-MM-dd" value="${billing.billComplete}" var="parsedRevenueRecognition" />
								<fmt:formatDate pattern="dd-MMM-yy" value="${parsedRevenueRecognition}" var="formattedRevenueRecognition" />
								<input type="text" name="billing.billComplete" id="billComplete" size="8" value="${formattedRevenueRecognition}" readonly="readonly" class="input-textUpper pr-f11"/>
       						</td>
							<td class="listwhitetext" align="right" style="padding-left:33px;">No&nbsp;More&nbsp;Work&nbsp;Tickets&nbsp;As&nbsp;Of</td>
       						<td>
       							<input type="text" style="text-align:left;" name="billing.noMoreWork" value="${billing.noMoreWork}" size="8" readonly="readonly" class="input-textUpper pr-f11"/>
       						</td>
       						<td>
       							<input type="button" class="pricelistbtn" style="font-size: 11px; height: 19px; padding: 1px 3px; width: 75px; line-height: 10px;" name="TicketList" id="TicketList"  value="Ticket List" onClick="findWorkTicketList()"/>
       						</td>
						</tr>
						<tr>
							<td class="listwhitetext" align="right" >Audit&nbsp;Complete</td>
       						<td>
       							<fmt:parseDate pattern="yyyy-MM-dd" value="${billing.auditComplete}" var="parsedRevenueRecognition" />
								<fmt:formatDate pattern="dd-MMM-yy" value="${parsedRevenueRecognition}" var="formattedRevenueRecognition" />
								<input type="text" style="margin-top:2px" name="billing.auditComplete" size="8" value="${formattedRevenueRecognition}" readonly="readonly" class="input-textUpper pr-f11"/>
       						</td>
							<td class="listwhitetext" align="right">Ready&nbsp;For&nbsp;Invoicing</td>
       						<td>
       							<input type="checkbox" style="margin:0px;" name="billing.readyForInvoicing" value="${billing.readyForInvoicing}" size="8"/>
       						</td>
						</tr>
						</table>
       					</td>
       					</tr>
       				</table>
       			</td>
       		</tr>
		</table>
<!-- Billing Section Ends -->		
			</td>
		</tr>
	</table>
	
	
	
<div style="width:92%;" id="allButton">
	<c:choose>
		<c:when test="${!trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup }">
			<input type="button" class="pricelistbtn-dis" style="width:65px;" name="pricingAddLine" value="Add Line"  disabled="true" /> 
		  	<input type="button" class="pricelistbtn-dis" style="width:55px;" name="pricingSave" value="Save" disabled="true" onclick="findAllPricingLineId('');"/>
		  	<input type="button" class="pricelistbtn-dis" style="width:140px;" disabled="true"	value="Add Default Template" />
		</c:when>
		<c:otherwise>  
			<input type="button" class="pricelistbtn" style="width:65px;" name="pricingAddLine" value="Add Line" onclick="checkBillingComplete();"/>					
            <input type="button" class="pricelistbtn" style="width:140px;" onclick="addDefaultAccountingTemplateAjax();" value="Add Default Template" />
			<c:if test="${not empty accountLineList}">
				<input type="button" class="pricelistbtn" style="width:55px;margin-top:12px;" name="pricingSave" value="Save" onclick="findAllPricingLineId('');" />
			</c:if>
			<c:if test="${empty accountLineList}">
				<input type="button" class="pricelistbtn-dis" style="width:55px;" name="pricingSave" value="Save" onclick="" disabled="disabled" />
			</c:if>
		</c:otherwise>
	</c:choose>

<sec-auth:authComponent componentId="module.accountLine.edit" replacementHtml="">
     <c:if test="${customerFile.moveType == null  || customerFile.moveType!='Quote'}">
  		<c:choose>
  		     <c:when test="${empty accountLineList}">
				<input type="button" class="pricelistbtn-dis" style="width:108px;margin-top:12px;" value="Generate Invoice" disabled="disabled" />
			</c:when>  
			<c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
				<c:choose> 
					<c:when test="${billToLength>0}">
						<input type="button" class="pricelistbtn" style="width:108px;margin-top:12px; "
						value="Generate Invoice" onclick="findBookingAgent('invoiceGen');"/>
					</c:when>
					<c:otherwise>
						<input type="button" class="pricelistbtn-dis" style="width:108px;"
						value="Generate Invoice" onclick="findBookingAgent('invoiceGen');" disabled="disabled"/>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<input type="button" class="pricelistbtn" style="width:108px;margin-top:12px;" value="Generate Invoice" onclick="findBookingAgent('invoiceGen');"/>
			</c:otherwise>
		</c:choose>
		
		<configByCorp:fieldVisibility componentId="component.accountLine.PreviewInvoice.editAll"> 
		    <c:if test="${emptyList!=true}" >                                                                                                   
		   		<input type="button" class="pricelistbtn" style="width:104px;  margin-top: 12px;"  name="PreviewInvoice"  value="Preview Invoice" onClick="return invoicePreview('${serviceOrder.shipNumber}',this);"/>
		    </c:if>
		   <c:if test="${emptyList==true}" >
		    	<input type="button" class="pricelistbtn-dis" style="width:104px;"  name="PreviewInvoice"  value="Preview Invoice" onClick="return invoicePreview('${serviceOrder.shipNumber}',this);" disabled="disabled"/>
		   </c:if>
    	</configByCorp:fieldVisibility>
     <configByCorp:fieldVisibility componentId="component.accountLine.PreviewInvoice.edit"> 
	     <c:if test="${emptyList!=true}" >                                                                                                   
	      	<input type="button" class="pricelistbtn" style="width:104px;  margin-top: 12px;"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=2827&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=Preview Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)"/>
	   </c:if>
	   <c:if test="${emptyList==true}" >
	   	<input type="button" class="pricelistbtn-dis" style="width:104px;"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=2827&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=Preview Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)" disabled="disabled"/>
	   </c:if>
    </configByCorp:fieldVisibility>
    <configByCorp:fieldVisibility componentId="component.accountLine.PreviewInvoice.editUGSG"> 
	     <c:if test="${emptyList!=true}" >                                                                                                   
	      	<input type="button" class="pricelistbtn" style="width:104px;  margin-top: 12px;"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=1807&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=Preview Tax Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)"/>
	    </c:if>
	     <c:if test="${emptyList==true}" >
	     	<input type="button" class="pricelistbtn-dis" style="width:104px;"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=1807&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=Preview Tax Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)" disabled="disabled"/>
	     </c:if>
     </configByCorp:fieldVisibility>
     <configByCorp:fieldVisibility componentId="component.accountLine.PreviewInvoice.editUGHK"> 
	     <script type="text/JavaScript">     
		    function validatePreview(){
		    var url='viewReportWithParam.html?id=954&list=main&decorator=popup&popup=true&reportParameter_Service Order Number=${serviceOrder.shipNumber}&reportParameter_Corporate ID=UGHK&fileType=PDF';
			document.forms['serviceForm1'].action =url;
			document.forms['serviceForm1'].submit();
			return true;
			} 
		</script>
	    <c:if test="${emptyList!=true && previewLine!=true}" >                                                                                                   
	      	<input type="button" class="pricelistbtn" style="width:104px;margin-top: 12px;"  name="PreviewInvoice"  value="Preview Invoice" onClick="return validatePreview();"/>
	    </c:if>
	     	<c:if test="${emptyList==true || previewLine==true}" >
	     <input type="button" class="pricelistbtn-dis" style="width:104px;"  name="PreviewInvoice"  value="Preview Invoice" onClick="return validatePreview();" disabled="disabled"/>
	     </c:if>
    </configByCorp:fieldVisibility>
    <!-- Added By Kunal For Ticket Number: 6858 -->
    <configByCorp:fieldVisibility componentId="component.accountLine.PreviewInvoice.editCWMS"> 
	     <c:if test="${emptyList!=true}" >                                                                                                   
	      	<input type="button" class="pricelistbtn" style="width:104px;margin-top: 12px;"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=1877&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=SO Line Not Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)"/>
	    </c:if>
	     <c:if test="${emptyList==true}" >
	     	<input type="button" class="pricelistbtn-dis" style="width:104px;"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=1877&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=SO Line Not Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)" disabled="disabled"/>
	     </c:if>
    </configByCorp:fieldVisibility>
    <configByCorp:fieldVisibility componentId="component.accountLine.PreviewInvoice.editUGCA"> 
	     <c:if test="${emptyList!=true}" >                                                                                                   
	      	<input type="button" class="pricelistbtn" style="width:104px;margin-top:12px;"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=3030&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=SO Line Not Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)"/>
	    </c:if>
	     <c:if test="${emptyList==true}" >
	     	<input type="button" class="pricelistbtn-dis" style="width:104px;"  name="PreviewInvoice"  value="Preview Invoice" onclick="javascript:openWindow('viewFormParam.html?id=3030&claimNumber=&cid=&jobNumber=${serviceOrder.shipNumber}&bookNumber=&noteID=&custID=&reportName=SO Line Not Invoice&docsxfer=Yes&reportModule=serviceOrder&reportSubModule=Accounting&formReportFlag=F&decorator=popup&popup=true',650,760)" disabled="disabled"/>
	     </c:if>
    </configByCorp:fieldVisibility>
    <!-- Modification Closed -->
    <!-- Added By Sangeeta For Ticket Number: 8020 -->
     <c:if test="${serviceOrder.companyDivision =='FFG' || serviceOrder.companyDivision =='FMG' || serviceOrder.companyDivision =='SSC'}"> 
     <configByCorp:fieldVisibility componentId="component.accountLine.PreviewInvoice.editSSCW"> 
	     <script type="text/JavaScript">     
		    function validatePreview(){
		    var url='viewReportWithParam.html?id=1918&reportName=SO Line Not Invoice&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Service Order Number=${serviceOrder.shipNumber}&reportParameter_Corporate ID=SSCW&fileType=PDF';
			document.forms['serviceForm1'].action =url;
			document.forms['serviceForm1'].submit();
			return true;
			}
		</script>
	    <c:if test="${emptyList!=true && previewLine!=true}" >                                                                                                   
	      	<input type="button" class="pricelistbtn" style="width:104px;margin-top:12px;"  name="PreviewInvoice"  value="Preview Invoice" onClick="return validatePreview();"/>
	    </c:if>
	    <c:if test="${emptyList==true || previewLine==true}" >
	     	<input type="button" class="pricelistbtn-dis" style="width:104px;"  name="PreviewInvoice"  value="Preview Invoice" onClick="return validatePreview();" disabled="disabled"/>
	    </c:if>
    </configByCorp:fieldVisibility>
    </c:if>
 	</c:if>
</sec-auth:authComponent>

<sec-auth:authComponent componentId="module.accountLineReverseInvoices.edit" replacementHtml="">
		 <c:choose>
		    <c:when test="${!trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup }">
		     	<input type="button" class="pricelistbtn" style="width:110px;"   value="Reverse Invoices" onclick="findReverseInvoice();" disabled="disabled"/>
		    </c:when>
	    	<c:otherwise>
			<c:if test="${emptyList!=true}" >
				<input type="button" class="pricelistbtn" style="width:110px;margin-top:12px;"   value="Reverse Invoices" onclick="findReverseInvoice();"/>
		   </c:if> 
		   <c:if test="${emptyList==true}" >
		   		<input type="button" class="pricelistbtn-dis" style="width:110px;"   value="Reverse Invoices" onclick="findReverseInvoice();" disabled="disabled"/>
		   </c:if>
		   </c:otherwise>
   		</c:choose>  
    </sec-auth:authComponent>	

<sec-auth:authComponent componentId="module.accountLine.edit" replacementHtml="">
   <c:if test="${accountInterface=='Y'}">
	   <c:if test="${emptyList!=true}" >
	    	<input type="button" class="pricelistbtn" style="width:155px;margin-top:12px; " value="Apply Payb. Posting Date" onclick="findBookingAgent('postingDate');"/>
		</c:if>
		<c:if test="${emptyList==true}" >
			<input type="button" class="pricelistbtn-dis" style="width:155px;" value="Apply Payb. Posting Date" onclick="findBookingAgent('postingDate');" disabled="disabled"//> 
		</c:if>
	</c:if>
</sec-auth:authComponent>
<sec-auth:authComponent componentId="module.accountLine.unPostPayable.Button" replacementHtml="">
	<input type="button" class="pricelistbtn" style="width:100px; margin-top:12px;"   value="Unpost Payable" onclick="findVendorInvoicesForUnposting()"/>
</sec-auth:authComponent>
	
    <td><c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 && serviceOrder.job !='' && serviceOrder.job!=null}">
   			<input type="button" class="pricelistbtn-dis" style="width:100px"  name="SynchBilling"  value="Synch Billing" onclick="countInvoice();"/>
		</c:if>
	</td>
    <sec-auth:authComponent componentId="module.accountLineReset.edit" replacementHtml="">
	<c:if test="${accountInterface=='Y'}">
	<c:if test="${emptyList!=true}" >
	<input type="button" class="pricelistbtn" style="width:127px;margin-top:12px;"   value="Reset Send to Dates" onclick="openSendtoDates();"/>
    </c:if>
    <c:if test="${emptyList==true}" >
    <input type="button" class="pricelistbtn-dis" style="width:127px;"   value="Reset Send to Dates" onclick="openSendtoDates();" disabled="disabled"/>
    </c:if>
    </c:if>
    </sec-auth:authComponent> 
   
    <configByCorp:fieldVisibility componentId="component.button.SSCW.showAdvances">
		<input type="button" class="pricelistbtn" style="width:115px;margin-top:12px;"  name="Show Advances"  value="Show Advances" onclick="getAdvDtls(this)" />
	</configByCorp:fieldVisibility>
    
    <sec-auth:authComponent componentId="module.accountLineDeleteSelectedInvoices.edit" replacementHtml="">                                                                                                   
     <c:if test="${emptyList!=true}" >
      <input type="button" class="pricelistbtn" style="width:125px;margin-top:12px;"  name="DeleteSelectedInvoices"  value="Delete Selected Invoices" onclick="findInvoiceTODelete();"/>
    </c:if>
     <c:if test="${emptyList==true}" >
     <input type="button" class="pricelistbtn-dis" style="width:125px;"  name="DeleteSelectedInvoices"  value="Delete Selected Invoices" onclick="findInvoiceTODelete();" disabled="disabled"/>
     </c:if>
    </sec-auth:authComponent>
  
    <c:if test="${fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}"> 
     <c:if test="${accountLineList!='[]'}"> 
     <c:if test="${emptyList!=true}" >
     <c:if test="${checkRecInvoiceForHVY>0 }" >
     <input type="button" class="pricelistbtn-dis" style="width:125px;"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();" disabled="disabled" />
    </c:if>
     <c:if test="${checkRecInvoiceForHVY==0 && serviceOrder.job !='' && serviceOrder.job!=null}" >
     <input type="button" class="pricelistbtn" style="width:125px;margin-top:12px;"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();"/>
    </c:if>
    </c:if>
    
    <c:if test="${emptyList==true}" >
     <input type="button" class="pricelistbtn-dis" style="width:125px;"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();" disabled="disabled" />
    </c:if>
    </c:if>
    <c:if test="${accountLineList=='[]'}"> 
    <c:if test="${emptyList!=true}" >
     <c:if test="${checkRecInvoiceForHVY>0}" >
     <input type="button" class="pricelistbtn-dis" style="width:125px;"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();" disabled="disabled" />
    </c:if>
     <c:if test="${checkRecInvoiceForHVY==0}" >
     <input type="button" class="pricelistbtn" style="width:125px;margin-top:12px;"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();"/>
    </c:if>
    </c:if>
     <c:if test="${emptyList==true}" >
     <input type="button" class="pricelistbtn-dis" style="width:125px;"  name="HVYCalculate"  value="HVY Calculate" onclick="calculateHVY();" disabled="disabled" />
    </c:if>
     </c:if>
    </c:if>
     <c:if test="${emptyList!=true}" > 
	     <c:choose>
		    <c:when test="${!trackingStatus.accNetworkGroup && trackingStatus.soNetworkGroup && billingCMMContractType}">
		    	<input type="button" class="pricelistbtn-dis" style="width:103px;"  name="Copy Estimates"  value="Copy Estimates" disabled="disabled"/>
		    </c:when>
	    	<c:otherwise>
	    		<input type="button" class="pricelistbtn" style="width:103px;margin-top:12px;"  name="Copy Estimates"  value="Copy Estimates" onclick="javascript:openWindow('copyEstimates.html?sid=${serviceOrder.id}&decorator=popup&popup=true')" />
	    	</c:otherwise>
	    </c:choose> 
     </c:if>
    <c:if test="${emptyList==true}" >
     	<input type="button" class="pricelistbtn-dis" style="width:103px;"  name="Copy Estimates"  value="Copy Estimates" disabled="disabled"/>
    </c:if>
    
    <sec-auth:authComponent componentId="module.accountLineReverseInvoices.edit" replacementHtml="">
		 <c:choose>
		    <c:when test="${trackingStatus.soNetworkGroup }">
		     	<input type="button" class="pricelistbtn-dis" style="width:100px;"   value="Copy Invoices" onclick="findCopyInvoice();" disabled="disabled"/>
		    </c:when>
	    <c:otherwise>
			<c:if test="${emptyList!=true}" >
				<input type="button" class="pricelistbtn" style="width:100px;margin-top:12px;"   value="Copy Invoices" onclick="findCopyInvoice();"/>
		   </c:if> 
		   <c:if test="${emptyList==true}" >
		   		<input type="button" class="pricelistbtn-dis" style="width:100px;"   value="Copy Invoices" onclick="findCopyInvoice();" disabled="disabled"/>
		   </c:if>
	   </c:otherwise>
	   </c:choose>  
    </sec-auth:authComponent>
        
    <configByCorp:fieldVisibility componentId="component.button.CWMS.commission">
    		<c:if test="${commissionJobName == 'Yes'}">
    		 <c:if test="${emptyList!=true}" >
    			<input type="button" class="pricelistbtn" style="width:100px;margin-top:12px;"  name="Commission"  value="Commission" onclick="calCommission(false)" />
    		</c:if>
    		 <c:if test="${emptyList==true}" >
    			<input type="button" class="pricelistbtn-dis" style="width:100px;"  name="Commission"  value="Commission" onclick="calCommission(false)" disabled="disabled"  />
    		</c:if>
    	</c:if>
    </configByCorp:fieldVisibility>
    <configByCorp:fieldVisibility componentId="component.button.commission.automation">
    		<c:if test="${commissionJobName == 'Yes'}">
    		 <c:if test="${emptyList!=true}" >
    			<input type="button" class="pricelistbtn" style="width:100px;margin-top:12px;"  name="autoCommission"  value="Commission" id="autoCommission" onclick="autoCommissionCal(this)" />
    		</c:if>
    		 <c:if test="${emptyList==true}" >
    			<input type="button" class="pricelistbtn-dis" style="width:100px;"  name="autoCommission"  value="Commission"  disabled="disabled"  />
    		</c:if>
    	</c:if>
    </configByCorp:fieldVisibility>
    
    <c:if test="${((!(trackingStatus.accNetworkGroup)) && billingCMMContractType && trackingStatus.soNetworkGroup) || (networkAgent && billingCMMContractType )}">
    	<input type="button" class="pricelistbtn" style="width:100px;margin-top:12px;"  name="MGMTFees"  value="MGMT Fees" onclick=" calculateMGMTFees()"   />
    </c:if> 
    <c:if test="${((!(trackingStatus.accNetworkGroup)) && (billingDMMContractType) && (trackingStatus.soNetworkGroup))  || (networkAgent && billingDMMContractType ) }">
     	<input type="button" class="pricelistbtn" style="width:100px;margin-top:12px;"  name="DiscountFees"  value="Discount Fees" onclick=" calculateDiscountFees()"   />
    </c:if>
 
 	<input type="button" class="pricelistbtn" style="width:56px;margin-top:12px;"  name="Reset"  value="Reset" onclick="resetFormData();" />
</div>
	
	
	
	
</div>