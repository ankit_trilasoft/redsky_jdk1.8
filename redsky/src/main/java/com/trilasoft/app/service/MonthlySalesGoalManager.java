package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.MonthlySalesGoal;


public interface MonthlySalesGoalManager extends GenericManager<MonthlySalesGoal, Long>{
	
	public MonthlySalesGoal getMonthlySalesGoalDetails(String partnerCode,String sessionCorpID,String yearVal);

}
