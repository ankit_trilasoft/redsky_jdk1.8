/**
. * This class interacts with Spring's HibernateTemplate to save/delete and
 * retrieve the basic "CustomerFile" objects in Redsky that allows for CustomerFile management.
 * @Class Name	CustomerFileDaoHibernate
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 * Extended by GenericDaoHibernate to  implement CustomerFileDao
 */

package com.trilasoft.app.dao.hibernate; 


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer; 

import javax.servlet.http.HttpServletRequest; 

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;
import org.hibernate.Hibernate;
import org.springframework.dao.DataAccessResourceFailureException; 

import com.trilasoft.app.dao.CustomerFileDao;
import com.trilasoft.app.dao.hibernate.dto.ExtractHibernateDTO;
import com.trilasoft.app.dao.hibernate.dto.ScheduledSurveyDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.CompanyDivision;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.ErrorLog;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.webapp.action.CommonUtil;
import com.trilasoft.app.dao.hibernate.dto.PrepaidServicesDTO;
import com.trilasoft.app.dao.hibernate.dto.AdditionalChargesDTO;




public class CustomerFileDaoHibernate extends GenericDaoHibernate<CustomerFile, Long> implements CustomerFileDao {   
	Logger logger = Logger.getLogger(CustomerFileDaoHibernate.class);
	private List customerFiles;
	private HibernateUtil hibernateUtil; 
	private User user;
    public CustomerFileDaoHibernate() {   
        super(CustomerFile.class);  
    }   
    private StringBuilder newSurveyFromBuilder;
    private StringBuilder newSurveyToBuilder;
    private String newSurveyFrom;
    private String newSurveyTo;
    private StringBuilder newEventFromBuilder;
    private StringBuilder newEventToBuilder;
    private String newEventFrom;
    private String newEventTo;    
    private Date workdtTmp;
    private ErrorLogManager errorLogManager;
    private String sessionCorpID; 
    public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
    }
    /**
     * Convenience method to get the request
     * @return current request
     */
    protected HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();  
    }
    public void getSessionCorpID()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();	
	}
    public List findMaximum(String corpID) {
    	try {
			return this
					.getSession()
					.createSQLQuery(
							"select max(sequenceNumber) from customerfile where sequenceNumber like '"
									+ corpID + "%' and corpID in ('" + corpID
									+ "')").list();
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
		return null;
    }
    
    public List findMaximumId() {
    	try {
			return getHibernateTemplate().find(
					"select max(id) from CustomerFile");
		} catch (Exception e) {
			 e.printStackTrace();
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    }
    
    public List findPortalId(String portalId) {
    	try {
			return this
					.getSession()
					.createSQLQuery(
							"select count(*) from app_user where username like '"
									+ portalId + "%'").list();
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    }
    
    public List findMaximumShip(String seqNum, String job,String CorpID) {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        Map filterMap=user.getFilterMap(); 
    	List userList=new ArrayList();
    	try {
			String companydivisionSetCode="";
			String partnerCodePopup ="";
			String bookingAgentSet="";
			if (filterMap!=null && (!(filterMap.isEmpty()))) {
				List list1 = new ArrayList();
				List list2 = new ArrayList();
				List list3 = new ArrayList();
				if(filterMap.containsKey("serviceOrderCompanyDivisionFilter")){
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderCompanyDivisionFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		list2.add("'" + listIterator.next().toString() + "'");	
		    	}
		    	companydivisionSetCode= list2.toString().replace("[","").replace("]", "");
		    	}
		    	if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
		    		list1.add("'" + code + "'");	
		    		}
		    	}
		    	partnerCodePopup = list1.toString().replace("[","").replace("]", "");
		    	}
		    	
		    	if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
		    		list3.add("'" + code + "'");	
		    		}
		    	}
		    	bookingAgentSet = list3.toString().replace("[","").replace("]", "");
		    	}
		    	String bookingAgentCondision="  ";
		    	if (bookingAgentSet != null && !bookingAgentSet.equals("") ) {
		    		bookingAgentCondision="  and bookingAgentCode in("+ bookingAgentSet + ")   ";
		    	}
				if (partnerCodePopup != null && !partnerCodePopup.equals("") &&  companydivisionSetCode != null && !companydivisionSetCode.equals("") ) {
					userList = this
							.getSession()
							.createSQLQuery(
									"select max(ship) from serviceorder where job like '"
											+ job.replaceAll("'", "''")
											+ "%' and sequenceNumber='"
											+ seqNum + "' and billToCode in("
											+ partnerCodePopup
											+ ") and companyDivision in("
							+ companydivisionSetCode
							+ ") and corpID='" + CorpID + "'"+bookingAgentCondision)
							.list();
				}else if (partnerCodePopup != null && !partnerCodePopup.equals("") ) {
					userList = this
					.getSession()
					.createSQLQuery(
							"select max(ship) from serviceorder where job like '"
									+ job.replaceAll("'", "''")
									+ "%' and sequenceNumber='"
									+ seqNum + "' and billToCode in("
									+ partnerCodePopup
									+ ") and corpID='" + CorpID + "'"+bookingAgentCondision)
					.list();
		   }else if (companydivisionSetCode != null && !companydivisionSetCode.equals("") ) {
			  userList = this
			 .getSession()
			 .createSQLQuery(
					"select max(ship) from serviceorder where job like '"
							+ job.replaceAll("'", "''")
							+ "%' and sequenceNumber='"
							+ seqNum + "' and companyDivision in("
							+ companydivisionSetCode
							+ ") and corpID='" + CorpID + "'"+bookingAgentCondision)
			.list();
         } else {
					userList = this
							.getSession()
							.createSQLQuery(
									"select max(ship) from serviceorder where job like '"
											+ job.replaceAll("'", "''")
											+ "%' and sequenceNumber='"
											+ seqNum + "' and corpID='"
											+ CorpID + "'"+bookingAgentCondision).list();
				}
			} else {
				userList = this
						.getSession()
						.createSQLQuery(
								"select max(ship) from serviceorder where job like '"
										+ job.replaceAll("'", "''")
										+ "%' and sequenceNumber='" + seqNum
										+ "' and corpID='" + CorpID + "'")
						.list();
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(CorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
    	return userList;
    }
   
    public List findMinimumShip(String seqNum, String job,String CorpID) {
    	List userList=new ArrayList();
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        Map filterMap=user.getFilterMap(); 
        String companydivisionSetCode="";
		String partnerCodePopup ="";
		String bookingAgentSet="";
    	try {
			List list1 = new ArrayList();
			List list2 = new ArrayList();
			List list3 = new ArrayList();
			if (filterMap!=null && (!(filterMap.isEmpty()))) {
				if(filterMap.containsKey("serviceOrderCompanyDivisionFilter")){
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderCompanyDivisionFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		list2.add("'" + listIterator.next().toString() + "'");	
		    	}
		    	companydivisionSetCode= list2.toString().replace("[","").replace("]", "");
		    	}
		    	if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
		    		list1.add("'" + code + "'");	
		    		}
		    	}
		    	partnerCodePopup = list1.toString().replace("[","").replace("]", "");
		    	}

		    	if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
		    		list3.add("'" + code + "'");	
		    		}
		    	}
		    	bookingAgentSet = list3.toString().replace("[","").replace("]", "");
		    	}
		    	String bookingAgentCondision="  ";
		    	if (bookingAgentSet != null && !bookingAgentSet.equals("") ) {
		    		bookingAgentCondision="  and bookingAgentCode in("+ bookingAgentSet + ")   ";
		    	}
				if (companydivisionSetCode != null  && !companydivisionSetCode.equals("") &&   partnerCodePopup != null && !partnerCodePopup.equals("") ) {
					userList = this
							.getSession()
							.createSQLQuery(
									"select min(ship) from serviceorder where job like '"
											+ job.replaceAll("'", "''")
											+ "%' and  sequenceNumber='"
											+ seqNum + "' and billToCode in("
											+ partnerCodePopup
											+ ") and companyDivision in("
							+ companydivisionSetCode
							+ ") and corpID='" + CorpID + "'"+bookingAgentCondision)
							.list();
				}else if (partnerCodePopup != null && !partnerCodePopup.equals("")   ) {
					userList = this
					.getSession()
					.createSQLQuery(
							"select min(ship) from serviceorder where job like '"
									+ job.replaceAll("'", "''")
									+ "%' and  sequenceNumber='"
									+ seqNum + "' and billToCode in("
									+ partnerCodePopup
									+ ") and corpID='" + CorpID + "'"+bookingAgentCondision)
					.list();
		}else if (companydivisionSetCode != null && !companydivisionSetCode.equals("")  ) {
			userList = this
			.getSession()
			.createSQLQuery(
					"select min(ship) from serviceorder where job like '"
							+ job.replaceAll("'", "''")
							+ "%' and  sequenceNumber='"
							+ seqNum + "' and companyDivision in("
							+ companydivisionSetCode
							+ ") and corpID='" + CorpID + "'"+bookingAgentCondision)
			.list();
} else {
					userList = this
							.getSession()
							.createSQLQuery(
									"select min(ship) from serviceorder where job like '"
											+ job.replaceAll("'", "''")
											+ "%' and  sequenceNumber='"
											+ seqNum + "'and corpID='" + CorpID
											+ "'"+bookingAgentCondision).list();
				}
			} else {
				userList = this
						.getSession()
						.createSQLQuery(
								"select min(ship) from serviceorder where job like '"
										+ job.replaceAll("'", "''")
										+ "%' and  sequenceNumber='" + seqNum
										+ "'and corpID='" + CorpID + "'")
						.list();
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(CorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
    	return userList;
    }
    
    public List findCountShip(String seqNum, String job,String CorpID) {
    	try {
			List userList = this
					.getSession()
					.createSQLQuery(
							"select count(*) from serviceorder where job like '"
									+ job.replaceAll("'", "''")
									+ "%' and sequenceNumber='" + seqNum
									+ "' and corpID='" + CorpID + "'").list();
			return userList;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(CorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    }
    
    public List findMaximumShip12(String seqNum) {
    	try {
			return getHibernateTemplate()
					.find("select max(ship) from PartnerQuote where sequenceNumber=?",
							seqNum);
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    }
    
    public List findAllRecords(){
    	try {
			List customerFiles = null;
			String columns = "id,sequenceNumber, lastName,firstName,billToName,job,coordinator,statusDate,status,createdOn";
			customerFiles = getHibernateTemplate()
					.find("select "
							+ columns
							+ " from CustomerFile where controlFlag ='C' OR quotationStatus ='Accepted'");
			try {
				customerFiles = hibernateUtil.convertScalarToComponent(columns,
						customerFiles, CustomerFile.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return customerFiles;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
      }
    
	public List<CustomerFile> findByCSODetail(String last, String first, String seqNum, String billToC, String job, String status,String coord) {
		try {
			List customerFiles = null;
			String columns = "id,sequenceNumber, lastName,firstName,billToName,job,coordinator,statusDate,status,createdOn";
			customerFiles = getHibernateTemplate()
					.find("select "
							+ columns
							+ " from CustomerFile where lastName like '"
							+ last.replaceAll("'", "''")
							+ "%' AND firstName like '"
							+ first.replaceAll("'", "''")
							+ "%' AND sequenceNumber like '"
							+ seqNum.replaceAll("'", "''")
							+ "%' AND billToName like '"
							+ billToC.replaceAll("'", "''")
							+ "%' AND job like '"
							+ job.replaceAll("'", "''")
							+ "%' AND status like '"
							+ status.replaceAll("'", "''")
							+ "%' AND coordinator like '"
							+ coord.replaceAll("'", "''")
							+ "%' AND (controlFlag ='C' OR quotationStatus ='Accepted')");
			try {
				customerFiles = hibernateUtil.convertScalarToComponent(columns,
						customerFiles, CustomerFile.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return customerFiles;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List<CustomerFile> findBySurveyDetail(String estimator, String originCity, Date survey) {
		try {
			if (estimator != "" && originCity != "" && survey != null) {
				customerFiles = getHibernateTemplate().find(
						"from CustomerFile where estimator like '"
								+ estimator.replaceAll("'", "''")
								+ "%' AND originCity like '"
								+ originCity.replaceAll("'", "''")
								+ "%' AND survey=?", survey);
			}
			if (estimator != "" && originCity != "" && survey == null) {
				customerFiles = getHibernateTemplate().find(
						"from CustomerFile where estimator like '"
								+ estimator.replaceAll("'", "''")
								+ "' AND originCity like '"
								+ originCity.replaceAll("'", "''") + "%'");
			}
			if (estimator != "" && originCity.equals("") && survey != null) {
				customerFiles = getHibernateTemplate().find(
						"from CustomerFile where estimator like '"
								+ estimator.replaceAll("'", "''")
								+ "%' AND survey=?", survey);
			}
			if (estimator.equals("") && originCity != "" && survey != null) {
				customerFiles = getHibernateTemplate().find(
						"from CustomerFile where originCity like '"
								+ originCity.replaceAll("'", "''")
								+ "%' AND survey=?", survey);
			}
			if (estimator.equals("") && originCity != "" && survey == null) {
				customerFiles = getHibernateTemplate().find(
						"from CustomerFile where originCity like '"
								+ originCity.replaceAll("'", "''") + "%'");
			}
			if (estimator.equals("") && originCity.equals("") && survey != null) {
				customerFiles = getHibernateTemplate().find(
						"from CustomerFile where survey=?", survey);
			}
			if (estimator != "" && originCity.equals("") && survey == null) {
				customerFiles = getHibernateTemplate().find(
						"from CustomerFile where estimator like '"
								+ estimator.replaceAll("'", "''") + "%'");
			}
			return customerFiles;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List findSequenceNumber(String seqNum){
    	try {
			return getHibernateTemplate().find(
					"from CustomerFile where sequenceNumber=?", seqNum);
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    }
	
	public List findByUserName(String userName){
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select u.username from app_user u,customerfile c where u.userType='CUSTOMER' and u.username=c.customerPortalId and u.username='"
									+ userName + "' ").list();
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List findCustomerDetails(String userName){
		try {
			return getHibernateTemplate().find(
					"from CustomerFile where customerPortalId=?", userName);
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	
	public List findUserFullName(String userName){
		List list = new ArrayList();
		try {
			list = this
					.getSession()
					.createSQLQuery(
							"select concat(first_name,' ',last_name) from app_user where username='"
									+ userName + "'").list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return list;
	}
	
	public List<User> findUserForRoleTransfer(String userName){
    	try {
			return getHibernateTemplate().find("from User where username=?",
					userName);
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
    }
	
	static String replaceWord(String original, String find, String replacement)
	{
			int i = original.indexOf(find);
			if (i < 0) {
			    return original;
			}
			String partBefore = original.substring(0, i);
			String partAfter  = original.substring(i + find.length());
			
			return partBefore + replacement + partAfter;
    }
	
	public boolean findUser(String portalId){
		try {
			List userList = this
					.getSession()
					.createSQLQuery(
							"select * from app_user where username='"
									+ portalId + "' ").list();
			boolean usr;
			if ((userList != null) || !(userList.isEmpty())) {
				usr = true;
			} else {
				usr = false;
			}
			return usr;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return false;
    }
	
	public boolean findUserEmail(String email){
		try {
			List emailList = getHibernateTemplate().find(
					"from User where email=?", email);
			boolean usr;
			if (!emailList.isEmpty()) {
				usr = true;
			} else {
				usr = false;
			}
			return usr;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return false;
    }
	
	public List<CustomerFile> getSurveyDetails(){
		List customerFiles = null;
        try {
			String columns = "id,sequenceNumber, lastName,firstName,billToName,job,coordinator,statusDate,status,createdOn";
			customerFiles = getHibernateTemplate()
					.find("select "
							+ columns
							+ " from CustomerFile where (controlFlag ='C' OR quotationStatus ='Accepted') AND survey >= now()");
			try {
				customerFiles = hibernateUtil.convertScalarToComponent(columns,
						customerFiles, CustomerFile.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return customerFiles;
     }
	
	public List<CustomerFile> findByCoordinator( String userName ){
		List customerFiles = null;
        try {
			String columns = "id,sequenceNumber, lastName,firstName,billToName,job,coordinator,statusDate,status,createdOn";
			customerFiles = getHibernateTemplate()
					.find("select "
							+ columns
							+ " from CustomerFile where (controlFlag ='C' OR quotationStatus ='Accepted') AND coordinator=?",
							userName);
			try {
				customerFiles = hibernateUtil.convertScalarToComponent(columns,
						customerFiles, CustomerFile.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return customerFiles;
     }

	public List<DataCatalog> findToolTip(String fieldName,String tableName,String corpId) {
		try {
			return getHibernateTemplate()
					.find("from DataCatalog where fieldName= '"
							+ fieldName
							+ "' AND tableName = '"
							+ tableName
							+ "' AND corpID in ('"
							+ corpId
							+ "','TSFT') AND description!='' group by tableName");
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List<SystemDefault> findDefaultBookingAgentDetail(String corpID){
    	try {
			return getHibernateTemplate().find(
					"from SystemDefault where corpID = ?", corpID);
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
    }
	
	public List findStateList(String bucket2, String corpId){
		try {
			if (bucket2 == null || bucket2.equals("")) {
				bucket2 = "NoCountry";
			}
			List<RefMaster> list = getHibernateTemplate()
					.find("select CONCAT(code,'#',description) from RefMaster where  bucket2=? order by description",
							bucket2);
			return list;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public List findActiveStateList(String bucket2, String corpId){
		try {
			if (bucket2 == null || bucket2.equals("")) {
				bucket2 = "NoCountry";
			}
			List<RefMaster> list = getHibernateTemplate()
					.find("select CONCAT(code,'#',description,'#',status) from RefMaster where  bucket2=? order by description",
							bucket2);
			return list;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}	
	public Map<String, String> findDefaultStateList(String bucket2, String corpId){
		try {
			if (bucket2 == null || bucket2.equals("")) {
				bucket2 = "NoCountry";
			}
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			String country = "";
			if (bucket2.equalsIgnoreCase("United States")) {
				country = "USA";
			} else if (bucket2.equalsIgnoreCase("India")) {
				country = "IND";
			} else if (bucket2.equalsIgnoreCase("Canada")) {
				country = "CAN";
			} else {
				country = bucket2;
			}
			List list = getSession()
					.createSQLQuery(
							"select code, description "
									+ "from refmaster "
									+ "where bucket2=:country and parameter ='STATE' order by description")
					.addScalar("code", Hibernate.STRING)
					.addScalar("description", Hibernate.STRING)
					.setParameter("country", country, Hibernate.STRING)
					.list();
			Iterator it = list.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				parameterMap.put(((String) row[0]).toUpperCase(),
						((String) row[1]).toUpperCase());
			}
			return parameterMap;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List<CustomerFile> searchQuotationFile(String last, String first, String seqNum, String billToC, String job, String quotationStatus,String coord) {
		List quotationFiles = null;
        try {
			String columns = "id,sequenceNumber, lastName,firstName,billToName,job,coordinator,statusDate,quotationStatus,createdOn";
			quotationFiles = getHibernateTemplate()
					.find("select "
							+ columns
							+ " from CustomerFile where lastName like '"
							+ last.replaceAll("'", "''").replaceAll(":", "''")
							+ "%' AND firstName like '"
							+ first.replaceAll("'", "''").replaceAll(":", "''")
							+ "%' AND sequenceNumber like '"
							+ seqNum.replaceAll("'", "''")
									.replaceAll(":", "''")
							+ "%' AND billToName like '"
							+ billToC.replaceAll("'", "''").replaceAll(":",
									"''")
							+ "%' AND job like '"
							+ job.replaceAll("'", "''").replaceAll(":", "''")
							+ "%' AND quotationStatus like '"
							+ quotationStatus.replaceAll("'", "''").replaceAll(
									":", "''")
							+ "%' AND coordinator like '"
							+ coord.replaceAll("'", "''").replaceAll(":", "''")
							+ "%' AND (controlFlag ='Q' OR quotationStatus ='Accepted')");
			try {
				quotationFiles = hibernateUtil.convertScalarToComponent(
						columns, quotationFiles, CustomerFile.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return quotationFiles;
	}
	
	public List<CustomerFile> customerList(){
		List customerFiles = null;
        try {
			String columns = "id,sequenceNumber, lastName,firstName,billToName,job,coordinator,statusDate,status,createdOn";
			customerFiles = getHibernateTemplate()
					.find("select "
							+ columns
							+ " from CustomerFile where controlFlag ='C' OR quotationStatus ='Accepted'");
			try {
				customerFiles = hibernateUtil.convertScalarToComponent(columns,
						customerFiles, CustomerFile.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return customerFiles;
		
	}
	
	public List<CustomerFile> findByCoordinatorQuotation( String userName ){
		List quotationFiles = null;
        try {
			String columns = "id,sequenceNumber, lastName,firstName,billToName,job,coordinator,statusDate,quotationStatus,createdOn";
			quotationFiles = getHibernateTemplate()
					.find("select "
							+ columns
							+ " from CustomerFile where (controlFlag ='Q' OR quotationStatus ='Accepted') AND coordinator=?",
							userName);
			try {
				quotationFiles = hibernateUtil.convertScalarToComponent(
						columns, quotationFiles, CustomerFile.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return quotationFiles;
    }
	
	public List<CustomerFile> quotationList(){
		List quotationFiles = null;
        try {
			String columns = "id,sequenceNumber, lastName,firstName,billToName,job,coordinator,statusDate,quotationStatus,createdOn";
			quotationFiles = getHibernateTemplate()
					.find("select "
							+ columns
							+ " from CustomerFile where (controlFlag ='Q' OR quotationStatus ='Accepted')");
			try {
				quotationFiles = hibernateUtil.convertScalarToComponent(
						columns, quotationFiles, CustomerFile.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return quotationFiles;
	}
	
	public List<ScheduledSurveyDTO> findAllScheduledSurveys(String corpID, String consult){
		List list1 = new ArrayList();
		try {
			String scheduledSurveysQuery = "(select id, userName as consultants, surveyDate as surDate, fromTime as frmTime, toTime, state, city, activityName, billto, '' as a, '' as b, '' as c, eventType, '' as controlFlag From calendarfile Where (date_format(surveyDate,'%Y-%m-%d') between date_format(now(),'%Y-%m-%d') and date_format(DATE_ADD(now(), INTERVAL 7 DAY),'%Y-%m-%d')) and (userName like '"
					+ consult.replaceAll("'", "''")
					+ "%' or userName like '"
					+ corpID
					+ "') AND corpID like '"
					+ corpID
					+ "') UNION (SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, if(firstName is null OR firstName ='',CONCAT_WS('',lastName,null),CONCAT_WS(', ',lastName,null,firstName)) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag  FROM customerfile where (date_format(survey,'%Y-%m-%d') between date_format(now(),'%Y-%m-%d') and date_format(DATE_ADD(now(), INTERVAL 7 DAY),'%Y-%m-%d')) and estimator like '"
					+ consult
					+ "%' AND corpID like '"
					+ corpID
					+ "') order by surDate, consultants, frmTime";
			List listScheduledSurveys = this.getSession()
					.createSQLQuery(scheduledSurveysQuery).list();
			Iterator it = listScheduledSurveys.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				ScheduledSurveyDTO scheduledSurveyDTO = new ScheduledSurveyDTO();
				scheduledSurveyDTO.setId(Long.parseLong(row[0].toString()));

				if (row[1] == null) {
					scheduledSurveyDTO.setUserName("");
				} else {
					scheduledSurveyDTO.setUserName(row[1].toString());
				}
				if (row[2] == null) {
					scheduledSurveyDTO.setSurveyDate(null);
				} else {
					String newWorkDate = row[2].toString();
					newWorkDate = newWorkDate.replace(".0", "");

					try {
						workdtTmp = new SimpleDateFormat("yyyy-MM-dd")
								.parse(newWorkDate);
					} catch (ParseException e) {

						e.printStackTrace();
					}
					scheduledSurveyDTO.setSurveyDate(workdtTmp);
				}
				if (row[3] == null) {
					scheduledSurveyDTO.setFromHrs("");
				} else {
					scheduledSurveyDTO.setFromHrs(row[3].toString());
				}
				if (row[4] == null) {
					scheduledSurveyDTO.setToHrs("");
				} else {
					scheduledSurveyDTO.setToHrs(row[4].toString());
				}
				if (row[5] == null) {
					scheduledSurveyDTO.setState("");
				} else {
					scheduledSurveyDTO.setState(row[5].toString());
				}
				if (row[6] == null) {
					scheduledSurveyDTO.setCity("");
				} else {
					scheduledSurveyDTO.setCity(row[6].toString());
				}
				if (row[7] == null) {
					scheduledSurveyDTO.setActivity("");
				} else {
					scheduledSurveyDTO.setActivity(row[7].toString());
				}
				if (row[8] == null) {
					scheduledSurveyDTO.setBillTo("");
				} else {
					scheduledSurveyDTO.setBillTo(row[8].toString());
				}
				if (row[9] == null) {
					scheduledSurveyDTO.setJobType("");
				} else {
					scheduledSurveyDTO.setJobType(row[9].toString());
				}
				if (row[10] == null) {
					scheduledSurveyDTO.setBillToCode("");
				} else {
					scheduledSurveyDTO.setBillToCode(row[10].toString());
				}
				if (row[11] == null) {
					scheduledSurveyDTO.setSequenceNumber("");
				} else {
					scheduledSurveyDTO.setSequenceNumber(row[11].toString());
				}
				if (row[12] == null) {
					scheduledSurveyDTO.setEventType("");
				} else {
					scheduledSurveyDTO.setEventType(row[12].toString());
				}
				if (row[13] == null) {
					scheduledSurveyDTO.setContlFlag("");
				} else {
					scheduledSurveyDTO.setContlFlag(row[13].toString());
				}
				if (row[14] == null) {
					scheduledSurveyDTO.setSurveyType("");
				} else {
					scheduledSurveyDTO.setSurveyType(row[14].toString());
				}
				scheduledSurveyDTO.setCorpId(row[15].toString());
				
				list1.add(scheduledSurveyDTO);
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
			return list1;
	}
	public List driverEventList(String sessionCorpID,String eventFrom,String eventTo)
	{
		try {
			if (!eventFrom.equalsIgnoreCase("")) {
				try {
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat(
							"yyyy-MM-dd");
					newEventFromBuilder = new StringBuilder(
							dateformatYYYYMMDD.format(new SimpleDateFormat(
									"EEE MMM dd HH:mm:ss Z yyyy")
									.parse(eventFrom)));
					newEventFrom = newEventFromBuilder.toString();
				} catch (ParseException e) {

					e.printStackTrace();
				}
			} else {
				newEventFrom = "";
			}
			if (!eventTo.equalsIgnoreCase("")) {
				try {
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat(
							"yyyy-MM-dd");
					newEventToBuilder = new StringBuilder(
							dateformatYYYYMMDD.format(new SimpleDateFormat(
									"EEE MMM dd HH:mm:ss Z yyyy")
									.parse(eventTo)));
					newEventTo = newEventToBuilder.toString();
				} catch (ParseException e) {

					e.printStackTrace();
				}
			} else {
				newEventTo = "";
			}
			List scheduledEventQuery = null;
			if (!newEventFrom.equalsIgnoreCase("")
					&& !newEventTo.equalsIgnoreCase("")) {
				scheduledEventQuery = getHibernateTemplate()
						.find("from CalendarFile where corpID like '"
								+ sessionCorpID
								+ "' AND (date_format(surveyDate,'%Y-%m-%d') BETWEEN '"
								+ newEventFrom + "' and '" + newEventTo
								+ "') and category = 'Driver' ");
			}
			return scheduledEventQuery;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	 public User loadUserByUsername(String username) throws UsernameNotFoundException {
	        try {
				List users = getHibernateTemplate().find("from User where username=?", username);
				if (users == null || users.isEmpty()) {
				    throw new UsernameNotFoundException("user '" + username + "' not found...");
				} else {
				    return (User) users.get(0);
				}
			} catch (Exception e) {
				 e.printStackTrace();
				getSessionCorpID();
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			}
	 return null;  
	 }
	public List<ScheduledSurveyDTO> scheduledSurveys(String sessionCorpID,String consult,String surveyFrom,String surveyTo,String surveyCity,String surveyJob){
		
		String companydivisionSetCode1="";
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        Map filterMap=user.getFilterMap(); 
        List list2 = new ArrayList();
		try{
			if (filterMap!=null && (!(filterMap.isEmpty()))) {
				if(filterMap.containsKey("serviceOrderCompanyDivisionFilter")){
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderCompanyDivisionFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		list2.add("'" + listIterator.next().toString() + "'");	
		    	}
		    	companydivisionSetCode1= list2.toString().replace("[","").replace("]", "");
		    	}	
			}
			
		}catch(Exception e){
			
		}
		
		try {
			if (!surveyFrom.equalsIgnoreCase("")) {
				try {
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat(
							"yyyy-MM-dd");
					newSurveyFromBuilder = new StringBuilder(
							dateformatYYYYMMDD.format(new SimpleDateFormat(
									"EEE MMM dd HH:mm:ss Z yyyy")
									.parse(surveyFrom)));
					newSurveyFrom = newSurveyFromBuilder.toString();
				} catch (ParseException e) {

					e.printStackTrace();
				}
			} else {
				newSurveyFrom = "";
			}
			if (!surveyTo.equalsIgnoreCase("")) {
				try {
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat(
							"yyyy-MM-dd");
					newSurveyToBuilder = new StringBuilder(
							dateformatYYYYMMDD.format(new SimpleDateFormat(
									"EEE MMM dd HH:mm:ss Z yyyy")
									.parse(surveyTo)));
					newSurveyTo = newSurveyToBuilder.toString();
				} catch (ParseException e) {

					e.printStackTrace();
				}
			} else {
				newSurveyTo = "";
			}
			List list1 = new ArrayList();
			String scheduledSurveysQuery = "";
			String query="";
			if(sessionCorpID.equals("STVF"))
			{
				if(companydivisionSetCode1!=null && (!(companydivisionSetCode1.equalsIgnoreCase("")))){
				query="SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, if(firstName is null OR firstName ='',CONCAT_WS('',lastName,null),CONCAT_WS(', ',lastName,null,firstName)) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
						+ surveyJob.replaceAll("'", "''")
						+ "%' AND originCity like '"
						+ surveyCity.replaceAll("'", "''")
						+ "%' AND (date_format(survey,'%Y-%m-%d') BETWEEN '"
						+ newSurveyFrom
						+ "' and '"
						+ newSurveyTo
						+ "') and estimator like '"
						+ consult.replaceAll("'", "''")
						+ "%' AND corpID like '"
						+ sessionCorpID
						+ "' AND companyDivision in("+companydivisionSetCode1+") UNION SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, if(firstName is null OR firstName ='',CONCAT_WS('',lastName,null),CONCAT_WS(', ',lastName,null,firstName)) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,'',corpID  FROM serviceorder where job like '"
						+ surveyJob.replaceAll("'", "''")
						+ "%' AND originCity like '"
						+ surveyCity.replaceAll("'", "''")
						+ "%' AND (date_format(survey,'%Y-%m-%d') BETWEEN '"
						+ newSurveyFrom
						+ "' and '"
						+ newSurveyTo
						+ "') and estimator like '"
						+ consult.replaceAll("'", "''")
						+ "%' AND corpID like '"
						+ sessionCorpID
						+ "' AND companyDivision in("+companydivisionSetCode1+")";
			}
			else
			{query="SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, if(firstName is null OR firstName ='',CONCAT_WS('',lastName,null),CONCAT_WS(', ',lastName,null,firstName)) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
					+ surveyJob.replaceAll("'", "''")
					+ "%' AND originCity like '"
					+ surveyCity.replaceAll("'", "''")
					+ "%' AND (date_format(survey,'%Y-%m-%d') BETWEEN '"
					+ newSurveyFrom
					+ "' and '"
					+ newSurveyTo
					+ "') and estimator like '"
					+ consult.replaceAll("'", "''")
					+ "%' AND corpID like '"
					+ sessionCorpID
					+ "'  UNION SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, if(firstName is null OR firstName ='',CONCAT_WS('',lastName,null),CONCAT_WS(', ',lastName,null,firstName)) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,'',corpID  FROM serviceorder where job like '"
					+ surveyJob.replaceAll("'", "''")
					+ "%' AND originCity like '"
					+ surveyCity.replaceAll("'", "''")
					+ "%' AND (date_format(survey,'%Y-%m-%d') BETWEEN '"
					+ newSurveyFrom
					+ "' and '"
					+ newSurveyTo
					+ "') and estimator like '"
					+ consult.replaceAll("'", "''")
					+ "%' AND corpID like '"
					+ sessionCorpID
					+ "'";
		}}
			else
			{
				if(companydivisionSetCode1!=null && (!(companydivisionSetCode1.equalsIgnoreCase("")))){
		query=	"SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, if(firstName is null OR firstName ='',CONCAT_WS('',lastName,null),CONCAT_WS(', ',lastName,null,firstName)) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
						+ surveyJob.replaceAll("'", "''")
						+ "%' AND originCity like '"
						+ surveyCity.replaceAll("'", "''")
						+ "%' AND (date_format(survey,'%Y-%m-%d') BETWEEN '"
						+ newSurveyFrom
						+ "' and '"
						+ newSurveyTo
						+ "') and estimator like '"
						+ consult.replaceAll("'", "''")
						+ "%' AND corpID like '"
						+ sessionCorpID
						+ "' AND companyDivision in("+companydivisionSetCode1+")";
				
			}
			else
			{
				query=	"SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, if(firstName is null OR firstName ='',CONCAT_WS('',lastName,null),CONCAT_WS(', ',lastName,null,firstName)) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
						+ surveyJob.replaceAll("'", "''")
						+ "%' AND originCity like '"
						+ surveyCity.replaceAll("'", "''")
						+ "%' AND (date_format(survey,'%Y-%m-%d') BETWEEN '"
						+ newSurveyFrom
						+ "' and '"
						+ newSurveyTo
						+ "') and estimator like '"
						+ consult.replaceAll("'", "''")
						+ "%' AND corpID like '"
						+ sessionCorpID
						+ "'";
			}}
			
			if(companydivisionSetCode1!=null && (!(companydivisionSetCode1.equalsIgnoreCase("")))){
			if (!newSurveyFrom.equalsIgnoreCase("")
					&& !newSurveyTo.equalsIgnoreCase("")) {
				scheduledSurveysQuery = "select id, userName as consultants, surveyDate as surDate, fromTime as frmTime, toTime, state, city, activityName, billto, '' as a, '' as b, '' as c, eventType, '' as controlFlag,'',corpID From calendarfile Where (userName like '"
						+ consult.replaceAll("'", "''")
						+ "%' or userName like '"
						+ sessionCorpID
						+ "') AND corpID like '"
						+ sessionCorpID
						+ "' AND (date_format(surveyDate,'%Y-%m-%d') BETWEEN '"
						+ newSurveyFrom
						+ "' and '"
						+ newSurveyTo
						+ "') and (category = 'Survey' or category = '') UNION "+query+" order by surDate, consultants, frmTime";
			}
			}else{
				if (!newSurveyFrom.equalsIgnoreCase("")
						&& !newSurveyTo.equalsIgnoreCase("")) {
					scheduledSurveysQuery = "select id, userName as consultants, surveyDate as surDate, fromTime as frmTime, toTime, state, city, activityName, billto, '' as a, '' as b, '' as c, eventType, '' as controlFlag,'',corpID From calendarfile Where (userName like '"
							+ consult.replaceAll("'", "''")
							+ "%' or userName like '"
							+ sessionCorpID
							+ "') AND corpID like '"
							+ sessionCorpID
							+ "' AND (date_format(surveyDate,'%Y-%m-%d') BETWEEN '"
							+ newSurveyFrom
							+ "' and '"
							+ newSurveyTo
							+ "') and (category = 'Survey' or category = '') UNION "+query+" order by surDate, consultants, frmTime";
				}
				
			}
			List listScheduledSurveys = this.getSession()
					.createSQLQuery(scheduledSurveysQuery).list();
			Iterator it = listScheduledSurveys.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				ScheduledSurveyDTO scheduledSurveyDTO = new ScheduledSurveyDTO();
				scheduledSurveyDTO.setId(Long.parseLong(row[0].toString()));

				if (row[1] == null) {
					scheduledSurveyDTO.setUserName("");
				} else {
					scheduledSurveyDTO.setUserName(row[1].toString());
				}
				if (row[2] == null) {
					scheduledSurveyDTO.setSurveyDate(null);
				} else {
					String newWorkDate = row[2].toString();
					newWorkDate = newWorkDate.replace(".0", "");

					try {
						workdtTmp = new SimpleDateFormat("yyyy-MM-dd")
								.parse(newWorkDate);
					} catch (ParseException e) {

						e.printStackTrace();
					}
					scheduledSurveyDTO.setSurveyDate(workdtTmp);
				}

				if (row[3] == null) {
					scheduledSurveyDTO.setFromHrs("");
				} else {
					scheduledSurveyDTO.setFromHrs(row[3].toString());
				}
				if (row[4] == null) {
					scheduledSurveyDTO.setToHrs("");
				} else {
					scheduledSurveyDTO.setToHrs(row[4].toString());
				}
				if (row[5] == null) {
					scheduledSurveyDTO.setState("");
				} else {
					scheduledSurveyDTO.setState(row[5].toString());
				}
				if (row[6] == null) {
					scheduledSurveyDTO.setCity("");
				} else {
					scheduledSurveyDTO.setCity(row[6].toString());
				}
				if (row[7] == null) {
					scheduledSurveyDTO.setActivity("");
				} else {
					scheduledSurveyDTO.setActivity(row[7].toString());
				}
				if (row[8] == null) {
					scheduledSurveyDTO.setBillTo("");
				} else {
					scheduledSurveyDTO.setBillTo(row[8].toString());
				}
				if (row[9] == null) {
					scheduledSurveyDTO.setJobType("");
				} else {
					scheduledSurveyDTO.setJobType(row[9].toString());
				}
				if (row[10] == null) {
					scheduledSurveyDTO.setBillToCode("");
				} else {
					scheduledSurveyDTO.setBillToCode(row[10].toString());
				}
				if (row[11] == null) {
					scheduledSurveyDTO.setSequenceNumber("");
				} else {
					scheduledSurveyDTO.setSequenceNumber(row[11].toString());
				}
				if (row[12] == null) {
					scheduledSurveyDTO.setEventType("");
				} else {
					scheduledSurveyDTO.setEventType(row[12].toString());
				}
				if (row[13] == null) {
					scheduledSurveyDTO.setContlFlag("");
				} else {
					scheduledSurveyDTO.setContlFlag(row[13].toString());
				}
				list1.add(scheduledSurveyDTO);
			}
			return list1;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
		public List searchAllScheduledEvent(String sessionCorpID,String eventFrom,String eventTo,String driverCode,String driverFirstName,String driverlastName){		
		try {
			if (!eventFrom.equalsIgnoreCase("")) {
				try {
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat(
							"yyyy-MM-dd");
					newEventFromBuilder = new StringBuilder(
							dateformatYYYYMMDD.format(new SimpleDateFormat(
									"dd-MMM-yy").parse(eventFrom)));
					newEventFrom = newEventFromBuilder.toString();
				} catch (ParseException e) {

					e.printStackTrace();
				}
			} else {
				newEventFrom = "";
			}
			if (!eventTo.equalsIgnoreCase("")) {
				try {
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat(
							"yyyy-MM-dd");
					newEventToBuilder = new StringBuilder(
							dateformatYYYYMMDD.format(new SimpleDateFormat(
									"dd-MMM-yy").parse(eventTo)));
					newEventTo = newEventToBuilder.toString();
				} catch (ParseException e) {

					e.printStackTrace();
				}
			} else {
				newEventTo = "";
			}
			List scheduledEventQuery = null;
			if (!newEventFrom.equalsIgnoreCase("")
					&& !newEventTo.equalsIgnoreCase("")) {
				scheduledEventQuery = getHibernateTemplate()
						.find("from CalendarFile where (firstName like '%"
								+ driverFirstName.trim().replaceAll(":", "''")
										.replaceAll("'", "''")
								+ "%' or firstName is null) AND (lastName like '%"
								+ driverlastName.trim().replaceAll(":", "''")
										.replaceAll("'", "''")
								+ "%' or lastName is null) AND (personId like '%"
								+ driverCode.trim().replaceAll(":", "''")
										.replaceAll("'", "''")
								+ "%') AND corpID like '"
								+ sessionCorpID
								+ "%' AND (date_format(surveyDate,'%Y-%m-%d') BETWEEN '"
								+ newEventFrom + "' and '" + newEventTo
								+ "') and category = 'Driver' ");
			} else {
				scheduledEventQuery = getHibernateTemplate()
						.find("from CalendarFile where (firstName like '%"
								+ driverFirstName.trim().replaceAll(":", "''")
										.replaceAll("'", "''")
								+ "%' or firstName is null) AND (lastName like '%"
								+ driverlastName.trim().replaceAll(":", "''")
										.replaceAll("'", "''")
								+ "%' or lastName is null) AND (personId like '%"
								+ driverCode.trim().replaceAll(":", "''")
										.replaceAll("'", "''")
								+ "%') AND corpID like '" + sessionCorpID
								+ "%' and category = 'Driver' ");
			}
			return scheduledEventQuery;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public List<ScheduledSurveyDTO> searchAllScheduledSurveys(String sessionCorpID,String consult,String surveyFrom,String surveyTo,String surveyCity,String surveyJob){

		String companydivisionSetCode="";
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        Map filterMap=user.getFilterMap(); 
        List list2 = new ArrayList();
        try{
			if (filterMap!=null && (!(filterMap.isEmpty()))) {
				if(filterMap.containsKey("serviceOrderCompanyDivisionFilter")){
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderCompanyDivisionFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		list2.add("'" + listIterator.next().toString() + "'");	
		    	}
		    	companydivisionSetCode= list2.toString().replace("[","").replace("]", "");
		    	}	
			}
			
		}catch(Exception e){
			
		}
		try {
			if (!surveyFrom.equalsIgnoreCase("")) {
				try {
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat(
							"yyyy-MM-dd");
					newSurveyFromBuilder = new StringBuilder(
							dateformatYYYYMMDD.format(new SimpleDateFormat(
									"dd-MMM-yy").parse(surveyFrom)));
					newSurveyFrom = newSurveyFromBuilder.toString();
				} catch (ParseException e) {

					e.printStackTrace();
				}
			} else {
				newSurveyFrom = "";
			}
			if (!surveyTo.equalsIgnoreCase("")) {
				try {
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat(
							"yyyy-MM-dd");
					newSurveyToBuilder = new StringBuilder(
							dateformatYYYYMMDD.format(new SimpleDateFormat(
									"dd-MMM-yy").parse(surveyTo)));
					newSurveyTo = newSurveyToBuilder.toString();
				} catch (ParseException e) {

					e.printStackTrace();
				}
			} else {
				newSurveyTo = "";
			}
			List list1 = new ArrayList();
			String scheduledSurveysQuery = "";
			String query="";
			if(sessionCorpID.equals("STVF"))
			{
				if (!newSurveyFrom.equalsIgnoreCase("")
						&& !newSurveyTo.equalsIgnoreCase("")) {
					if((companydivisionSetCode != null) && (!(companydivisionSetCode.equalsIgnoreCase("")))){
						query=	"SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, if(firstName is null OR firstName ='',CONCAT_WS('',lastName,null),CONCAT_WS(', ',lastName,null,firstName)) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
						+ surveyJob.replaceAll("'", "''")
						+ "%' AND originCity like '%"
						+ surveyCity.replaceAll("'", "''")
						+ "%' AND (date_format(survey,'%Y-%m-%d') BETWEEN '"
						+ newSurveyFrom
						+ "' and '"
						+ newSurveyTo
						+ "') and estimator like '"
						+ consult.replaceAll("'", "''")
						+ "%' AND corpID like '"
						+ sessionCorpID
						+ "' AND companyDivision in("+companydivisionSetCode+") UNION SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, if(firstName is null OR firstName ='',CONCAT_WS('',lastName,null),CONCAT_WS(', ',lastName,null,firstName)) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,'',corpID  FROM serviceorder where job like '"
								+ surveyJob.replaceAll("'", "''")
								+ "%' AND originCity like '%"
								+ surveyCity.replaceAll("'", "''")
								+ "%' AND (date_format(survey,'%Y-%m-%d') BETWEEN '"
								+ newSurveyFrom
								+ "' and '"
								+ newSurveyTo
								+ "') and estimator like '"
								+ consult.replaceAll("'", "''")
								+ "%' AND corpID like '"
								+ sessionCorpID
								+ "' AND companyDivision in("+companydivisionSetCode+")";
					
			}
					else{
						query="SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, if(firstName is null OR firstName ='',CONCAT_WS('',lastName,null),CONCAT_WS(', ',lastName,null,firstName)) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
						+ surveyJob.replaceAll("'", "''")
						+ "%' AND originCity like '%"
						+ surveyCity.replaceAll("'", "''")
						+ "%' AND (date_format(survey,'%Y-%m-%d') BETWEEN '"
						+ newSurveyFrom
						+ "' and '"
						+ newSurveyTo
						+ "') and estimator like '"
						+ consult.replaceAll("'", "''")
						+ "%' AND corpID like '"
						+ sessionCorpID
						+ "'UNION SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, if(firstName is null OR firstName ='',CONCAT_WS('',lastName,null),CONCAT_WS(', ',lastName,null,firstName)) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,'',corpID  FROM serviceorder where job like '"
						+ surveyJob.replaceAll("'", "''")
						+ "%' AND originCity like '%"
						+ surveyCity.replaceAll("'", "''")
						+ "%' AND (date_format(survey,'%Y-%m-%d') BETWEEN '"
						+ newSurveyFrom
						+ "' and '"
						+ newSurveyTo
						+ "') and estimator like '"
						+ consult.replaceAll("'", "''")
						+ "%' AND corpID like '"
						+ sessionCorpID
						+ "'";
								}
					}
				else {
					if (newSurveyFrom.equalsIgnoreCase("")
							&& !newSurveyTo.equalsIgnoreCase("")) {
						if((companydivisionSetCode != null) && (!(companydivisionSetCode.equalsIgnoreCase("")))){
						query="SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
							+ surveyJob.replaceAll("'", "''")
							+ "%' AND originCity like '%"
							+ surveyCity.replaceAll("'", "''")
							+ "%' AND (date_format(survey,'%Y-%m-%d') <= '"
							+ newSurveyTo
							+ "') and estimator like '"
							+ consult.replaceAll("'", "''")
							+ "%' AND corpID like '"
							+ sessionCorpID
							+ "' AND companyDivision in("+companydivisionSetCode+") UNION SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,'',corpID  FROM serviceorder where job like '"
							+ surveyJob.replaceAll("'", "''")
							+ "%' AND originCity like '%"
							+ surveyCity.replaceAll("'", "''")
							+ "%' AND (date_format(survey,'%Y-%m-%d') <= '"
							+ newSurveyTo
							+ "') and estimator like '"
							+ consult.replaceAll("'", "''")
							+ "%' AND corpID like '"
							+ sessionCorpID
							+ "' AND companyDivision in("+companydivisionSetCode+")";
						
						}
						
						else
						{
							query="SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
									+ surveyJob.replaceAll("'", "''")
									+ "%' AND originCity like '%"
									+ surveyCity.replaceAll("'", "''")
									+ "%' AND (date_format(survey,'%Y-%m-%d') <= '"
									+ newSurveyTo
									+ "') and estimator like '"
									+ consult.replaceAll("'", "''")
									+ "%' AND corpID like '"
									+ sessionCorpID
									+ "' UNION 	SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,'',corpID  FROM serviceorder where job like '"
									+ surveyJob.replaceAll("'", "''")
									+ "%' AND originCity like '%"
									+ surveyCity.replaceAll("'", "''")
									+ "%' AND (date_format(survey,'%Y-%m-%d') <= '"
									+ newSurveyTo
									+ "') and estimator like '"
									+ consult.replaceAll("'", "''")
									+ "%' AND corpID like '"
									+ sessionCorpID
									+ "'";
						}
						}
					if (!newSurveyFrom.equalsIgnoreCase("")
							&& newSurveyTo.equalsIgnoreCase("")) {
						if((companydivisionSetCode != null) && (!(companydivisionSetCode.equalsIgnoreCase("")))){
							query ="SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
							+ surveyJob.replaceAll("'", "''")
							+ "%' AND originCity like '%"
							+ surveyCity.replaceAll("'", "''")
							+ "%' AND (date_format(survey,'%Y-%m-%d') >= '"
							+ newSurveyFrom
							+ "') and estimator like '"
							+ consult.replaceAll("'", "''")
							+ "%' AND corpID like '"
							+ sessionCorpID
							+ "' AND companyDivision in("+companydivisionSetCode+") UNION SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,'',corpID  FROM serviceorder where job like '"
							+ surveyJob.replaceAll("'", "''")
							+ "%' AND originCity like '%"
							+ surveyCity.replaceAll("'", "''")
							+ "%' AND (date_format(survey,'%Y-%m-%d') >= '"
							+ newSurveyFrom
							+ "') and estimator like '"
							+ consult.replaceAll("'", "''")
							+ "%' AND corpID like '"
							+ sessionCorpID
							+ "' AND companyDivision in("+companydivisionSetCode+")";
					
					}
					else{
						query ="SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
							+ surveyJob.replaceAll("'", "''")
							+ "%' AND originCity like '%"
							+ surveyCity.replaceAll("'", "''")
							+ "%' AND (date_format(survey,'%Y-%m-%d') >= '"
							+ newSurveyFrom
							+ "') and estimator like '"
							+ consult.replaceAll("'", "''")
							+ "%' AND corpID like '"
							+ sessionCorpID
							+ "' UNION SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,'',corpID  FROM serviceorder where job like '"
							+ surveyJob.replaceAll("'", "''")
							+ "%' AND originCity like '%"
							+ surveyCity.replaceAll("'", "''")
							+ "%' AND (date_format(survey,'%Y-%m-%d') >= '"
							+ newSurveyFrom
							+ "') and estimator like '"
							+ consult.replaceAll("'", "''")
							+ "%' AND corpID like '"
							+ sessionCorpID
							+ "'";}}
					if (newSurveyFrom.equalsIgnoreCase("")
							&& newSurveyTo.equalsIgnoreCase("")) {
						if((companydivisionSetCode != null) && (!(companydivisionSetCode.equalsIgnoreCase("")))){
							query = "SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
							+ surveyJob.replaceAll("'", "''")
							+ "%' AND originCity like '%"
							+ surveyCity.replaceAll("'", "''")
							+ "%' and estimator like '"
							+ consult.replaceAll("'", "''")
							+ "%' AND corpID like '"
							+ sessionCorpID
							+ "' AND companyDivision in("+companydivisionSetCode+") UNION SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,'',corpID  FROM serviceorder where job like '"
							+ surveyJob.replaceAll("'", "''")
							+ "%' AND originCity like '%"
							+ surveyCity.replaceAll("'", "''")
							+ "%' and estimator like '"
							+ consult.replaceAll("'", "''")
							+ "%' AND corpID like '"
							+ sessionCorpID
							+ "' AND companyDivision in("+companydivisionSetCode+")";}
						else
						{
					
							query="SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
									+ surveyJob.replaceAll("'", "''")
									+ "%' AND originCity like '%"
									+ surveyCity.replaceAll("'", "''")
									+ "%' and estimator like '"
									+ consult.replaceAll("'", "''")
									+ "%' AND corpID like '"
									+ sessionCorpID
									+ "'UNION SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,'',corpID  FROM serviceorder where job like '"
							+ surveyJob.replaceAll("'", "''")
							+ "%' AND originCity like '%"
							+ surveyCity.replaceAll("'", "''")
							+ "%' and estimator like '"
							+ consult.replaceAll("'", "''")
							+ "%' AND corpID like '"
							+ sessionCorpID
							+ "'";
					}}}}
			else
			{

				if (!newSurveyFrom.equalsIgnoreCase("")
						&& !newSurveyTo.equalsIgnoreCase("")) {
					if((companydivisionSetCode != null) && (!(companydivisionSetCode.equalsIgnoreCase("")))){
						query=	"SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, if(firstName is null OR firstName ='',CONCAT_WS('',lastName,null),CONCAT_WS(', ',lastName,null,firstName)) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
						+ surveyJob.replaceAll("'", "''")
						+ "%' AND originCity like '%"
						+ surveyCity.replaceAll("'", "''")
						+ "%' AND (date_format(survey,'%Y-%m-%d') BETWEEN '"
						+ newSurveyFrom
						+ "' and '"
						+ newSurveyTo
						+ "') and estimator like '"
						+ consult.replaceAll("'", "''")
						+ "%' AND corpID like '"
						+ sessionCorpID
						+ "' AND companyDivision in("+companydivisionSetCode+") ";
					
			}
					else{
						query="SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, if(firstName is null OR firstName ='',CONCAT_WS('',lastName,null),CONCAT_WS(', ',lastName,null,firstName)) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
						+ surveyJob.replaceAll("'", "''")
						+ "%' AND originCity like '%"
						+ surveyCity.replaceAll("'", "''")
						+ "%' AND (date_format(survey,'%Y-%m-%d') BETWEEN '"
						+ newSurveyFrom
						+ "' and '"
						+ newSurveyTo
						+ "') and estimator like '"
						+ consult.replaceAll("'", "''")
						+ "%' AND corpID like '"
						+ sessionCorpID
						+ "'";
								}
					}
				else {
					if (newSurveyFrom.equalsIgnoreCase("")
							&& !newSurveyTo.equalsIgnoreCase("")) {
						if((companydivisionSetCode != null) && (!(companydivisionSetCode.equalsIgnoreCase("")))){
						query="SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
							+ surveyJob.replaceAll("'", "''")
							+ "%' AND originCity like '%"
							+ surveyCity.replaceAll("'", "''")
							+ "%' AND (date_format(survey,'%Y-%m-%d') <= '"
							+ newSurveyTo
							+ "') and estimator like '"
							+ consult.replaceAll("'", "''")
							+ "%' AND corpID like '"
							+ sessionCorpID
							+ "' AND companyDivision in("+companydivisionSetCode+") ";
						
						}
						
						else
						{
							query="SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
									+ surveyJob.replaceAll("'", "''")
									+ "%' AND originCity like '%"
									+ surveyCity.replaceAll("'", "''")
									+ "%' AND (date_format(survey,'%Y-%m-%d') <= '"
									+ newSurveyTo
									+ "') and estimator like '"
									+ consult.replaceAll("'", "''")
									+ "%' AND corpID like '"
									+ sessionCorpID
									+ "'";
						}
						}
					if (!newSurveyFrom.equalsIgnoreCase("")
							&& newSurveyTo.equalsIgnoreCase("")) {
						if((companydivisionSetCode != null) && (!(companydivisionSetCode.equalsIgnoreCase("")))){
							query ="SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
							+ surveyJob.replaceAll("'", "''")
							+ "%' AND originCity like '%"
							+ surveyCity.replaceAll("'", "''")
							+ "%' AND (date_format(survey,'%Y-%m-%d') >= '"
							+ newSurveyFrom
							+ "') and estimator like '"
							+ consult.replaceAll("'", "''")
							+ "%' AND corpID like '"
							+ sessionCorpID
							+ "' AND companyDivision in("+companydivisionSetCode+") ";
					
					}
					else{
						query ="SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
							+ surveyJob.replaceAll("'", "''")
							+ "%' AND originCity like '%"
							+ surveyCity.replaceAll("'", "''")
							+ "%' AND (date_format(survey,'%Y-%m-%d') >= '"
							+ newSurveyFrom
							+ "') and estimator like '"
							+ consult.replaceAll("'", "''")
							+ "%' AND corpID like '"
							+ sessionCorpID
							+ "'";}
					if (newSurveyFrom.equalsIgnoreCase("")
							&& newSurveyTo.equalsIgnoreCase("")) {
						if((companydivisionSetCode != null) && (!(companydivisionSetCode.equalsIgnoreCase("")))){
							query = "SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
							+ surveyJob.replaceAll("'", "''")
							+ "%' AND originCity like '%"
							+ surveyCity.replaceAll("'", "''")
							+ "%' and estimator like '"
							+ consult.replaceAll("'", "''")
							+ "%' AND corpID like '"
							+ sessionCorpID
							+ "' AND companyDivision in("+companydivisionSetCode+")";}
						else
						{
					
							query="SELECT id, estimator, survey, surveyTime, surveyTime2, originState, originCity, CONCAT(lastName,', ',firstName) as activityName, billtoname, job, billtoCode, sequenceNumber,'' as k, controlFlag as controlFlag,surveyType,corpID  FROM customerfile where job like '"
									+ surveyJob.replaceAll("'", "''")
									+ "%' AND originCity like '%"
									+ surveyCity.replaceAll("'", "''")
									+ "%' and estimator like '"
									+ consult.replaceAll("'", "''")
									+ "%' AND corpID like '"
									+ sessionCorpID
									+ "'";
					}}}
			}}
			if (!newSurveyFrom.equalsIgnoreCase("")
					&& !newSurveyTo.equalsIgnoreCase("")) {
				if((companydivisionSetCode != null) && (!(companydivisionSetCode.equalsIgnoreCase("")))){
					
					scheduledSurveysQuery = "select id, userName as consultants, surveyDate as surDate, fromTime as frmTime, toTime, state, city, activityName, billto, '' as a, '' as b, '' as c, eventType, '' as controlFlag,'',corpID From calendarfile Where (userName like '"
						+ consult.replaceAll("'", "''")
						+ "%' or userName like '"
						+ sessionCorpID
						+ "') AND corpID like '"
						+ sessionCorpID
						+ "' AND (date_format(surveyDate,'%Y-%m-%d') BETWEEN '"
						+ newSurveyFrom
						+ "' and '"
						+ newSurveyTo
						+ "') and (category = 'Survey' or category = '')   UNION "+query+" order by surDate, consultants, frmTime";
				}else{
				scheduledSurveysQuery = "select id, userName as consultants, surveyDate as surDate, fromTime as frmTime, toTime, state, city, activityName, billto, '' as a, '' as b, '' as c, eventType, '' as controlFlag,'',corpID From calendarfile Where (userName like '"
						+ consult.replaceAll("'", "''")
						+ "%' or userName like '"
						+ sessionCorpID
						+ "') AND corpID like '"
						+ sessionCorpID
						+ "' AND (date_format(surveyDate,'%Y-%m-%d') BETWEEN '"
						+ newSurveyFrom
						+ "' and '"
						+ newSurveyTo
						+ "') and (category = 'Survey' or category = '')   UNION  "+query+"  order by surDate, consultants, frmTime";
				}
			} else {
				if (newSurveyFrom.equalsIgnoreCase("")
						&& !newSurveyTo.equalsIgnoreCase("")) {
					if((companydivisionSetCode != null) && (!(companydivisionSetCode.equalsIgnoreCase("")))){
						
						scheduledSurveysQuery = "select id, userName as consultants, surveyDate as surDate, fromTime as frmTime, toTime, state, city, activityName, billto, '' as a, '' as b, '' as c, eventType, '' as controlFlag,'',corpID From calendarfile Where (userName like '"
							+ consult.replaceAll("'", "''")
							+ "%' or userName like '"
							+ sessionCorpID
							+ "') AND corpID like '"
							+ sessionCorpID
							+ "' AND (date_format(surveyDate,'%Y-%m-%d') <= '"
							+ newSurveyTo
							+ "') and (category = 'Survey' or category = '')  UNION  "+query+"  order by surDate, consultants, frmTime";
					}else{
					scheduledSurveysQuery = "select id, userName as consultants, surveyDate as surDate, fromTime as frmTime, toTime, state, city, activityName, billto, '' as a, '' as b, '' as c, eventType, '' as controlFlag,'',corpID From calendarfile Where (userName like '"
							+ consult.replaceAll("'", "''")
							+ "%' or userName like '"
							+ sessionCorpID
							+ "') AND corpID like '"
							+ sessionCorpID
							+ "' AND (date_format(surveyDate,'%Y-%m-%d') <= '"
							+ newSurveyTo
							+ "') and (category = 'Survey' or category = '')   UNION  "+query+"  order by surDate, consultants, frmTime";
					}
				}
				if (!newSurveyFrom.equalsIgnoreCase("")
						&& newSurveyTo.equalsIgnoreCase("")) {
					if((companydivisionSetCode != null) && (!(companydivisionSetCode.equalsIgnoreCase("")))){
						scheduledSurveysQuery = "select id, userName as consultants, surveyDate as surDate, fromTime as frmTime, toTime, state, city, activityName, billto, '' as a, '' as b, '' as c,	eventType, '' as controlFlag,'',corpID From calendarfile Where (userName like '"
							+ consult.replaceAll("'", "''")
							+ "%' or userName like '"
							+ sessionCorpID
							+ "') AND corpID like '"
							+ sessionCorpID
							+ "' AND (date_format(surveyDate,'%Y-%m-%d') >= '"
							+ newSurveyFrom
							+ "') and (category = 'Survey' or category = '') UNION  "+query+"  order by surDate, consultants, frmTime";	
					}else{
					scheduledSurveysQuery = "(select id, userName as consultants, surveyDate as surDate, fromTime as frmTime, toTime, state, city, activityName, billto, '' as a, '' as b, '' as c,	eventType, '' as controlFlag,'',corpID From calendarfile Where (userName like '"
							+ consult.replaceAll("'", "''")
							+ "%' or userName like '"
							+ sessionCorpID
							+ "') AND corpID like '"
							+ sessionCorpID
							+ "' AND (date_format(surveyDate,'%Y-%m-%d') >= '"
							+ newSurveyFrom
							+ "') and (category = 'Survey' or category = '')  UNION   "+query+"  order by surDate, consultants, frmTime";
					}
				}
				if (newSurveyFrom.equalsIgnoreCase("")
						&& newSurveyTo.equalsIgnoreCase("")) {
					if((companydivisionSetCode != null) && (!(companydivisionSetCode.equalsIgnoreCase("")))){
						scheduledSurveysQuery = "select id, userName as consultants, surveyDate as surDate, fromTime as frmTime, toTime, state, city, activityName, billto, '' as a, '' as b, '' as c, eventType, '' as controlFlag,'',corpID From calendarfile Where (userName like '"
							+ consult.replaceAll("'", "''")
							+ "%' or userName like '"
							+ sessionCorpID
							+ "') AND corpID like '"
							+ sessionCorpID
							+ "' and (category = 'Survey' or category = '')  UNION  "+query+"  order by surDate, consultants, frmTime";	
					}else{
					scheduledSurveysQuery = "select id, userName as consultants, surveyDate as surDate, fromTime as frmTime, toTime, state, city, activityName, billto, '' as a, '' as b, '' as c, eventType, '' as controlFlag,'',corpID From calendarfile Where (userName like '"
							+ consult.replaceAll("'", "''")
							+ "%' or userName like '"
							+ sessionCorpID
							+ "') AND corpID like '"
							+ sessionCorpID
							+ "' and (category = 'Survey' or category = '')  UNION  "+query+"  order by surDate, consultants, frmTime";
					}
				}
			}
			List listScheduledSurveys = this.getSession()
					.createSQLQuery(scheduledSurveysQuery).list();
			Iterator it = listScheduledSurveys.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				ScheduledSurveyDTO scheduledSurveyDTO = new ScheduledSurveyDTO();
				scheduledSurveyDTO.setId(Long.parseLong(row[0].toString()));

				if (row[1] == null) {
					scheduledSurveyDTO.setUserName("");
				} else {
					scheduledSurveyDTO.setUserName(row[1].toString());
				}
				if (row[2] == null) {
					scheduledSurveyDTO.setSurveyDate(null);
				} else {
					String newWorkDate = row[2].toString();
					newWorkDate = newWorkDate.replace(".0", "");

					try {
						workdtTmp = new SimpleDateFormat("yyyy-MM-dd")
								.parse(newWorkDate);
					} catch (ParseException e) {

						e.printStackTrace();
					}
					scheduledSurveyDTO.setSurveyDate(workdtTmp);
				}

				if (row[3] == null) {
					scheduledSurveyDTO.setFromHrs("");
				} else {
					scheduledSurveyDTO.setFromHrs(row[3].toString());
				}
				if (row[4] == null) {
					scheduledSurveyDTO.setToHrs("");
				} else {
					scheduledSurveyDTO.setToHrs(row[4].toString());
				}
				if (row[5] == null) {
					scheduledSurveyDTO.setState("");
				} else {
					scheduledSurveyDTO.setState(row[5].toString());
				}
				if (row[6] == null) {
					scheduledSurveyDTO.setCity("");
				} else {
					scheduledSurveyDTO.setCity(row[6].toString());
				}
				if (row[7] == null) {
					scheduledSurveyDTO.setActivity("");
				} else {
					scheduledSurveyDTO.setActivity(row[7].toString());
				}
				if (row[8] == null) {
					scheduledSurveyDTO.setBillTo("");
				} else {
					scheduledSurveyDTO.setBillTo(row[8].toString());
				}
				if (row[9] == null) {
					scheduledSurveyDTO.setJobType("");
				} else {
					scheduledSurveyDTO.setJobType(row[9].toString());
				}
				if (row[10] == null) {
					scheduledSurveyDTO.setBillToCode("");
				} else {
					scheduledSurveyDTO.setBillToCode(row[10].toString());
				}
				if (row[11] == null) {
					scheduledSurveyDTO.setSequenceNumber("");
				} else {
					scheduledSurveyDTO.setSequenceNumber(row[11].toString());
				}
				if (row[12] == null) {
					scheduledSurveyDTO.setEventType("");
				} else {
					scheduledSurveyDTO.setEventType(row[12].toString());
				}
				if (row[13] == null) {
					scheduledSurveyDTO.setContlFlag("");
				} else {
					scheduledSurveyDTO.setContlFlag(row[13].toString());
				}
				if (row[14] == null) {
					scheduledSurveyDTO.setSurveyType("");
				} else {
					scheduledSurveyDTO.setSurveyType(row[14].toString());
				}
				scheduledSurveyDTO.setCorpId(row[15].toString());
				
				list1.add(scheduledSurveyDTO);
			}
			return list1;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List findServiceOrderList(String sequenceNumber, String corpId) {
		try {
			return getHibernateTemplate().find(
					"from ServiceOrder where sequenceNumber='" + sequenceNumber
							+ "' and corpID='" + corpId
							+ "' AND status not in ('CNCL')");
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}

	public List findPartnerQuotesList(String sequenceNumber, String corpId) {
		try {
			return getHibernateTemplate().find(
					"from PartnerQuote where sequenceNumber='" + sequenceNumber
							+ "' and corpID='" + corpId + "'");
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	/* we have removed the calling of this method via bugzilla ticket # 13962 need to remove this method after 2 release cycle */
	public List findBookingCode(){
		try {
			return getHibernateTemplate()
					.find("select distinct(bookingAgentCode) from CustomerFile where bookingAgentCode is not null order by bookingAgentCode asc");
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List findDefaultBookingAgentCode(String companyDivision){
		try {
			List list = getHibernateTemplate().find(
					"from CompanyDivision where companyCode='"
							+ companyDivision + "'");
			return list;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	/* we have removed the calling of this method via bugzilla ticket # 13962 need to remove this method after 2 release cycle */
	public List findBillToCode(){
		try {
			return getHibernateTemplate()
					.find("select distinct(billToCode) from CustomerFile where billToCode is not null order by billToCode asc");
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	
	public List findCustJobContract(String billToCode, String job, Date createdon,String corpID ,String companyDivision, String bookingAgentCode){
		String query="";
		try {
			 List list3 = new ArrayList();
			 List agentparentList= new ArrayList();
			 List companydivisionBookingAgentList= new ArrayList();
				String bookingAgentSet="";
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		        User user = (User)auth.getPrincipal();
		        Map filterMap=user.getFilterMap();
		        String usertype=user.getUserType();
		    	List userList=new ArrayList();
		    	try {
					if(filterMap.containsKey("serviceOrderBillToCodeFilter") || filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
						if(filterMap.containsKey("serviceOrderBillToCodeFilter") && bookingAgentCode!=null && (!(bookingAgentCode.trim().equalsIgnoreCase("null"))) && (!(bookingAgentCode.trim().equalsIgnoreCase("")))){
						list3.add("'" + bookingAgentCode + "'");	
						}
						if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
						
			    		List partnerCorpIDList = new ArrayList(); 
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey(); 
								    partnerCorpIDList= (List)entry.getValue(); 
								}
							}
							
			    	Iterator listIterator= partnerCorpIDList.iterator();
			    	while (listIterator.hasNext()) {
			    		String code=listIterator.next().toString();
			    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
			    		list3.add("'" + code + "'");
			    		}
			    	} 
					}
						bookingAgentSet = list3.toString().replace("[","").replace("]", "");
				    	agentparentList=this.getSession().createSQLQuery("select distinct agentparent from partnerpublic where partnercode in ("+ bookingAgentSet + ")").list();
				    	Iterator agentparentListIterator= agentparentList.iterator();
				    	while (agentparentListIterator.hasNext()) {
				    		String code=agentparentListIterator.next().toString();
				    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
				    		list3.add("'" + code + "'");
				    		} 
				    	}
				    	bookingAgentSet = list3.toString().replace("[","").replace("]", "");
				    	companydivisionBookingAgentList=this.getSession().createSQLQuery("select distinct bookingAgentCode from companydivision where corpid in (select corpid from companydivision where bookingAgentCode in ("+ bookingAgentSet + "))").list(); 
				    	Iterator companydivisionBookingAgentListIterator= companydivisionBookingAgentList.iterator();
				    	while (companydivisionBookingAgentListIterator.hasNext()) {
				    		String code1=companydivisionBookingAgentListIterator.next().toString();
				    		if(code1!=null && (!(code1.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code1.trim().equalsIgnoreCase("")))){ 
				    		list3.add("'" + code1 + "'");
				    		} 
				    	}
				    	bookingAgentSet = list3.toString().replace("[","").replace("]", "");
					}
			    	}catch (Exception e) {
					  String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  logger.error("Error executing query "+ e.getStackTrace()[0]);
			    	 e.printStackTrace();
				}
		    	
		    if ((filterMap.containsKey("serviceOrderBookingAgentCodeFilter") || filterMap.containsKey("serviceOrderBillToCodeFilter") )  && bookingAgentSet != null && (!(bookingAgentSet.trim().equals(""))) && usertype!=null && usertype.trim().equalsIgnoreCase("USER") ) {
				 if(createdon !=null){
				    Date date1Str = createdon;
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
					   return getSession().createSQLQuery(("select '' union select distinct c.contract from contract c, contractaccount ca, contractaccount ca1 where ca.accountcode = '"+billToCode+"' and ca1.accountcode in ("+ bookingAgentSet + ") and ca.contract = ca1.contract and ca.corpID = ca1.corpID and ca.contract = c.contract and ca.corpID = c.corpID and ca.corpID = '"+corpID+"' and (c.jobtype like '%"+job+"%' or c.jobtype='') and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision='')) and ((date_format(c.ending,'%Y-%m-%d') >= if('"+formatedDate1+"'='null','2001-01-01','"+formatedDate1+"')) or (c.ending is null )) and ((date_format(c.begin,'%Y-%m-%d') <= if('"+formatedDate1+"'='null','2001-01-01','"+formatedDate1+"')) or (c.begin is null )) order by 1")).list();
					}else{
					   return getSession().createSQLQuery(("select '' union select distinct c.contract from contract c, contractaccount ca, contractaccount ca1 where ca.accountcode = '"+billToCode+"' and ca1.accountcode in ("+ bookingAgentSet + ") and ca.contract = ca1.contract and ca.corpID = ca1.corpID and ca.contract = c.contract and ca.corpID = c.corpID and ca.corpID = '"+corpID+"' and (c.jobtype like '%"+job+"%' or c.jobtype='')  and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision='')) and ((c.ending is null or date_format(c.ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d'))) and ( (c.begin is null or date_format(c.begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d'))) order by 1")).list();
					 }
			    
		    }else{
			if (createdon != null) {
				Date date1Str = createdon;
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat(
						"yyyy-MM-dd");
				StringBuilder formatedDate1 = new StringBuilder(
						dateformatYYYYMMDD1.format(date1Str));
				query="select '' union select  distinct c.contract from contract c where (c.jobtype like '%"+job+"%' or c.jobtype='') and c.corpID = '"+corpID+"' and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision='')) and ((date_format(c.ending,'%Y-%m-%d') >= if('"+formatedDate1+"'='null','2001-01-01','"+formatedDate1+"')) or (c.ending is null )) and ((date_format(c.begin,'%Y-%m-%d') <= if('"+formatedDate1+"'='null','2001-01-01','"+formatedDate1+"')) or (c.begin is null )) and c.contract not in (select contract from contractaccount where corpID='"+corpID+"' and accounttype='Account') union select distinct c.contract from contract c, contractaccount ca where ca.accountcode = '"+billToCode+"' and ca.contract = c.contract and ca.corpID = c.corpID and ca.corpID = '"+corpID+"' and (c.jobtype like '%"+job+"%' or c.jobtype='') and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision='')) and ((date_format(c.ending,'%Y-%m-%d') >= if('"+formatedDate1+"'='null','2001-01-01','"+formatedDate1+"')) or (c.ending is null )) and ((date_format(c.begin,'%Y-%m-%d') <= if('"+formatedDate1+"'='null','2001-01-01','"+formatedDate1+"')) or (c.begin is null )) order by 1";
				return getSession()
						.createSQLQuery(query).list();
			} else {
				query="select '' union select  distinct c.contract from contract c where (c.jobtype like '%"+job+"%' or c.jobtype='') and c.corpID = '"+corpID+"' and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision='')) and ((c.ending is null or date_format(c.ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d'))) and ((c.begin is null or date_format(c.begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d'))) and c.contract not in (select contract from contractaccount where corpID='"+corpID+"' and accounttype='Account') union select distinct c.contract from contract c, contractaccount ca where ca.accountcode = '"+billToCode+"' and ca.contract = c.contract and ca.corpID = c.corpID and ca.corpID = '"+corpID+"' and (c.jobtype like '%"+job+"%' or c.jobtype='')  and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision='')) and ((c.ending is null or date_format(c.ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d'))) and ( (c.begin is null or date_format(c.begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d'))) order by 1";
				return getSession()
						.createSQLQuery(query).list();
			}
		    }
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;

	}

	public List findCompanyDivision(String corpId){
		try {
			return getSession()
					.createQuery(
							"select distinct(companyCode) from CompanyDivision where corpID='"
									+ corpId + "' order by companyCode").list();
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List findCompanyDivisionByJob(String corpId, String job){
		try {
			return getSession()
					.createQuery(
							"select distinct c.companyCode  from CompanyDivision c, RefMaster r, PartnerPublic p where c.accountingCode = r.bucket2 and p.partnerCode = c.bookingAgentCode and r.parameter = 'JOB' and r.code = '"
									+ job
									+ "' and c.corpID='"
									+ corpId
									+ "' and c.companyCode != '' order by c.companyCode ")
					.list();
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    }
	
	public List findCompanyDivisionByBookAg(String corpId, String bookAg){
		List compCode = new ArrayList();
		try {
			List compCodes = getHibernateTemplate()
					.find("select distinct companyCode  from CompanyDivision  where bookingAgentCode ='"
							+ bookAg
							+ "' and corpID='"
							+ corpId
							+ "' and companyCode != '' and (inactiveEffectiveDate is null OR inactiveEffectiveDate > now()) and closeddivision is false order by companyCode ");
			if (!compCodes.isEmpty()) {
				compCode = compCodes;
			} else {
				compCode = getHibernateTemplate()
						.find("select distinct companyCode  from CompanyDivision  where corpID='"
								+ corpId
								+ "' and companyCode != '' and (inactiveEffectiveDate is null OR inactiveEffectiveDate > now()) and closeddivision is false  order by companyCode");
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return compCode;
	}
	
	public List findCompanyDivisionByBookAgAndCompanyDivision(String corpId, String bookAg,String companyDivision){
		List compCode = new ArrayList();
		List compCodes = new ArrayList();
		String query="";
		try {
			if(bookAg==null || bookAg.trim().equals("")){

				query="select distinct companyCode  from CompanyDivision  where  corpID='" + corpId + "' and companyCode != '' and (inactiveEffectiveDate is null OR inactiveEffectiveDate > now()) and closeddivision is false order by companyCode ";
				compCodes = getHibernateTemplate().find(query);
				if (!compCodes.isEmpty() && compCodes.contains(companyDivision)) {
					compCode = compCodes;
				}else{
					query="select distinct companyCode  from CompanyDivision  where corpID='" + corpId + "' and companyCode != '' and inactiveEffectiveDate < now() and closeddivision is false order by companyCode ";
					compCodes = getHibernateTemplate().find(query);
					if (!compCodes.isEmpty()) {
						compCode = compCodes;
					}
				}
					
			}else{
			query="select distinct companyCode  from CompanyDivision  where bookingAgentCode ='"
					+ bookAg
					+ "' and corpID='"
					+ corpId
					+ "' and companyCode != '' and (inactiveEffectiveDate is null OR inactiveEffectiveDate > now()) and closeddivision is false order by companyCode ";
			compCodes = getHibernateTemplate().find(query);
			if (!compCodes.isEmpty() && compCodes.contains(companyDivision)) {
				compCode = compCodes;
			}else{				
				query="select distinct companyCode  from CompanyDivision  where corpID='"
						+ corpId
						+ "' and companyCode != '' and (inactiveEffectiveDate is null OR inactiveEffectiveDate > now()) and closeddivision is false order by companyCode ";
				compCodes = getHibernateTemplate().find(query);
				if (!compCodes.isEmpty() && compCodes.contains(companyDivision)) {
					compCode = compCodes;
				}else{				
				query="select distinct companyCode  from CompanyDivision  where corpID='"
						+ corpId
						+ "' and companyCode != '' and inactiveEffectiveDate < now() and closeddivision is false order by companyCode ";
				compCodes = getHibernateTemplate().find(query);
				if (!compCodes.isEmpty()) {
					compCode = compCodes;
					}
				}
			}
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return compCode;
	}
	
	public List findCompanyDivisionWithoutBookAgAndCompanyDivision(String corpId){
		List compCode = new ArrayList();
		List compCodes = new ArrayList();
		String query="";
		try {
			query="select distinct companyCode  from CompanyDivision  where  corpID='" + corpId + "' and companyCode != '' and (inactiveEffectiveDate is null OR inactiveEffectiveDate > now()) order by companyCode ";
				compCodes = getHibernateTemplate().find(query);
			if (!compCodes.isEmpty()) {
				compCode = compCodes;
			}			
		}catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return compCode;
	}
	
	public int setStatus(String seqNum, String status,String statusNumber){
		try {
			
			getHibernateTemplate().flush();
			getHibernateTemplate().clear();
			
			int ret = getHibernateTemplate().bulkUpdate(
					"update ServiceOrder set status='" + status
							+ "', statusNumber='" + statusNumber
							+ "' where sequenceNumber= '" + seqNum
							+ "' and status ='DWNLD'");
		
			getHibernateTemplate().flush();
			getHibernateTemplate().clear();
			
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
		
	}
	
	public int setFnameLname(String shipNum, String firstName,String lastName){
		try {
			
			getHibernateTemplate().flush();
			getHibernateTemplate().clear();
			
			int ret = getHibernateTemplate().bulkUpdate(
					"update ServiceOrder set  lastName=? where sequenceNumber= '"
							+ shipNum + "' ", lastName);
			
			getHibernateTemplate().flush();
			getHibernateTemplate().clear();
			
			return ret;
			
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}
	
	public int setFirstName(String shipNum, String firstName){
		try {
			
			getHibernateTemplate().flush();
			getHibernateTemplate().clear();
			
			
			int ret = getHibernateTemplate().bulkUpdate(
					"update ServiceOrder set  firstName=? where sequenceNumber= '"
							+ shipNum + "' ", firstName);
			
			
			getHibernateTemplate().flush();
			getHibernateTemplate().clear();
			
			return ret;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}
	
	public int setPrefix(String shipNum, String prefix){
		try {
			
			getHibernateTemplate().flush();
			getHibernateTemplate().clear();
			
			
			int ret = getHibernateTemplate().bulkUpdate(
					"update ServiceOrder set  prefix=? where sequenceNumber= '"
							+ shipNum + "' ", prefix);
			
			getHibernateTemplate().flush();
			getHibernateTemplate().clear();
			
			return ret;
			
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}
	
	public int setSSNo(String shipNum, String socialSecurityNumber){
	    try {
	    	
	    	getHibernateTemplate().flush();
			getHibernateTemplate().clear();
			
			int ret = getHibernateTemplate().bulkUpdate(
					"update ServiceOrder set  socialSecurityNumber=? where sequenceNumber= '"
							+ shipNum + "' ", socialSecurityNumber) ;
	    	
			getHibernateTemplate().flush();
			getHibernateTemplate().clear();
			
			return ret;
			
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}
	
	public List<CompanyDivision> defBookingAgent(String companyCode,String corpID)
	{
    	try {
			return getHibernateTemplate().find(
					"select distinct bookingAgentCode from CompanyDivision where corpID='"
							+ corpID + "' AND companyCode=?", companyCode);
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    }
	
	public List findByCorpId(String corpID){
	  	try {
			return getHibernateTemplate().find(
					"select companyDivisionFlag from Company where corpID=?",
					corpID);
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}

	public ArrayList getServiceOrderClosedStatus(String sequenceNumberForStatus, String corpID) {
		try {
			return (ArrayList) getHibernateTemplate().find(
					"select status from ServiceOrder where sequenceNumber='"
							+ sequenceNumberForStatus + "' and corpID='"
							+ corpID + "' and status not in ('CLSD','CNCL')");
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public int workTicketUpdate(String shipNum, String firstName,String lastName){
		try {
			
			getHibernateTemplate().flush();
			getHibernateTemplate().clear();
			
			int ret = getHibernateTemplate().bulkUpdate(
					"update WorkTicket set  lastName=? where sequenceNumber= '"
							+ shipNum + "'", lastName);
			
			getHibernateTemplate().flush();
			getHibernateTemplate().clear();
			
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}
	
	public int workTicketFirstName(String shipNum, String firstName){
		try {
			return getHibernateTemplate().bulkUpdate(
					"update WorkTicket set  firstName=? where sequenceNumber= '"
							+ shipNum + "'", firstName);
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}
	
	public List findCoordEmailAddress(String userName){
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select email from app_user where username='"
									+ userName + "' ").list();
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			getSessionCorpID();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List findCoordSignature(String userName){
		try {
			return getHibernateTemplate().find(
					"select signature from User where username=?", userName);
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List addAndCopyCustomerFile(Long cid,String corpID){
	    try {
			return getHibernateTemplate().find(
					"from CustomerFile where corpID='" + corpID + "' and id=?",
					cid);
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public void updateEmailInUser(String cPortalId, String corpID,String emailId){
		  try {
			getHibernateTemplate().bulkUpdate(
					"update User set email='" + emailId + "' where username='"
							+ cPortalId + "' and corpID='" + corpID + "'");
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
	}
	
	public void resetPassword(String passwordNew,String confirmPasswordNew,String email,Date compareWithDate){
		try {
			Date date1Str = compareWithDate;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat(
					"yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder(
					dateformatYYYYMMDD1.format(date1Str));
			getHibernateTemplate().bulkUpdate(
					"Update User set updatedBy='"+getRequest().getRemoteUser()+"', updatedOn = now() , password = '" + passwordNew
							+ "', confirmPassword = '" + confirmPasswordNew
							+ "' ,passwordReset = true,pwdexpiryDate= '"
							+ formatedDate1 + "' where username=?", email);
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
	}
	
	public int setSalesStatus(String seqNum) {
		try {
			return getHibernateTemplate().bulkUpdate(
					"update CustomerFile set salesStatus='Pending' where sequenceNumber = '"
							+ seqNum + "' ");
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}
	
	public List findByBillToCode(String billToCode, String sessionCorpID) {
		try {
			return this.getSession()
					.createSQLQuery(
					"select if(firstName is not null and firstName!='',concat(firstName,' ',lastName),lastName) from partnerprivate  where partnerCode = '" + billToCode + "' and corpid in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') ").list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List findByBillToCodeSQLQuery(String billToCode, String sessionCorpID) {
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select lastName from partnerprivate  where partnerCode = '"
									+ billToCode + "'").list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}

	public List getSurveyConsultants(String job, Date survey, String surveyTime, String surveyTime2, String sessionCorpID) {
		List list1 = new ArrayList();
		try {
			List consultantsList = this
					.getSession()
					.createSQLQuery(
							"select username from app_user  ap , role r,  user_role ur where ap.id=ur.user_id and r.id= ur.role_id and ap.corpID='"
									+ sessionCorpID
									+ "' and r.name='ROLE_SALE' and ap.jobtype like '"
									+ job
									+ "' and username not in (select estimator from customerfile where date_format(survey,'%Y-%m-%d') = '"
									+ survey
									+ "' and corpid = '"
									+ sessionCorpID
									+ "' and length(estimator) > 1 and (surveyTime  between '"
									+ surveyTime
									+ "' and '"
									+ surveyTime2
									+ "') or  (surveytime2 between '"
									+ surveyTime
									+ "' and '"
									+ surveyTime2
									+ "') or (surveyTime <= '"
									+ surveyTime
									+ "' and surveytime2 > '"
									+ surveyTime2
									+ "'))").list();
			Iterator it = consultantsList.iterator();
			while (it.hasNext()) {
				Object row = it.next();
				ScheduledSurveyDTO scheduledSurveyDTO = new ScheduledSurveyDTO();
				if (row == null) {
					scheduledSurveyDTO.setUserName("");
				} else {
					scheduledSurveyDTO.setUserName(row.toString());
				}
				list1.add(scheduledSurveyDTO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return list1;
	}

	public List getSurveyConsultantsDetails(String job, Date survey, String surveyTime, String surveyTime2, String sessionCorpID) {
		List list1 = new ArrayList();
		try {
			String query = "select '           ' as sequencenumber ,username as estimator , '        ' as survey,'        ' as from,'       ' as to from app_user  ap , role r,  user_role ur where ap.id=ur.user_id and r.id= ur.role_id and ap.corpID='"
					+ sessionCorpID
					+ "' and r.name='ROLE_SALE' and ap.jobtype like '"
					+ job
					+ "' UNION select sequenceNumber, estimator, date_format(survey,'%Y-%m-%d') 'Survey', surveyTime, surveyTime2 from customerfile c where survey like '"
					+ survey
					+ "%' and corpid = '"
					+ sessionCorpID
					+ "' and c.job ='"
					+ job
					+ "' and length(estimator) > 1 order by 2";
			List consultantsList = this
					.getSession()
					.createSQLQuery(
							"select '           ' as sequencenumber ,username as estimator , '        ' as survey,'      ' as fromTime ,'			'as toTime from app_user  ap , role r,  user_role ur where ap.id=ur.user_id and r.id= ur.role_id and ap.corpID='"
									+ sessionCorpID
									+ "' and r.name='ROLE_SALE' and ap.jobtype like '"
									+ job
									+ "' UNION select sequenceNumber, estimator, date_format(survey,'%Y-%m-%d') 'Survey', surveyTime as fromTime, surveyTime2 as toTime from customerfile c where date_format(survey,'%Y-%m-%d %H:%i:%s') like  '"
									+ survey
									+ "%' and corpid = '"
									+ sessionCorpID
									+ "' and c.job ='"
									+ job
									+ "' and length(estimator) > 1 order by 2")
					.list();
			Iterator it = consultantsList.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				ScheduledSurveyDTO scheduledSurveyDTO = new ScheduledSurveyDTO();
				if (row[0] == null) {
					scheduledSurveyDTO.setSequenceNumber("");
				} else {
					scheduledSurveyDTO.setSequenceNumber(row[0].toString());
				}
				if (row[1] == null) {
					scheduledSurveyDTO.setUserName("");
				} else {
					scheduledSurveyDTO.setUserName(row[1].toString());
				}
				if (row[3] == null) {
					scheduledSurveyDTO.setFromHrs("");
				} else {
					scheduledSurveyDTO.setFromHrs(row[3].toString());
				}
				if (row[4] == null) {
					scheduledSurveyDTO.setToHrs("");
				} else {
					scheduledSurveyDTO.setToHrs(row[4].toString());
				}
				list1.add(scheduledSurveyDTO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return list1;
	}

	public ArrayList getServiceOrderCancelledStatus(String sequenceNumberForStatus, String corpID) {
		try {
			return (ArrayList) getHibernateTemplate().find(
					"select count(*) from ServiceOrder where sequenceNumber='"
							+ sequenceNumberForStatus + "' and corpID='"
							+ corpID + "' and status <> 'CNCL' ");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;	
	}
	
	public ArrayList getWorkTicketStatusClosedStatus(String sequenceNumberForStatus, String sessionCorpID) {
		try {
			return (ArrayList) getHibernateTemplate().find(
					"select count(*) from WorkTicket where sequenceNumber='"
							+ sequenceNumberForStatus + "' and corpID='"
							+ sessionCorpID + "' and targetActual <> 'C' ");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public void resetAccount(String name,String corpID){
		try {
			getHibernateTemplate()
					.bulkUpdate(
							"Update User set updatedBy='"+getRequest().getRemoteUser()+"', updatedOn = now() , enabled = false where username='"
									+ name + "' and corpID='" + corpID + "' and userType ='CUSTOMER' ");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
	}
	
	public void resetUpdateAccount(String name,String corpID){
		try {
			getHibernateTemplate()
					.bulkUpdate(
							"Update User set updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() ,  enabled = true,accountExpired = false, accountLocked = false, credentialsExpired = false where username='"
									+ name + "' and corpID='" + corpID + "' ");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
	}
	
	public List findCustomerPortalListDetails(String userName){
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select username from app_user where username like '"
									+ userName + "%' ").list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List findCompanyName(String corpID){
		try {
			return getHibernateTemplate().find(
					"select companyName,landingPageWelcomeMsg from Company where corpID='" + corpID
							+ "'");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public int updateServiceOrderOriginAddress(String originAddress1, String originAddress2,String originAddress3,String originCompany,String originCountry,String originState,String originCity,String originZip,String originPreferredContact,String originDayPhone,String originDayPhoneExt,String originHomePhone,String originMobile,String originFax,String email,String email2,String originCityCode,String originCountryCode,String sequenceNumber, String userName,String linkedUpdate){
		try {
			return getSession().createSQLQuery(
					"update serviceorder set originAddressLine1='"
							+ originAddress1.replaceAll("'", "\\\\'")
							+ "' ,originAddressLine2='"
							+ originAddress2.replaceAll("'", "\\\\'")
							+ "' ,originAddressLine3='"
							+ originAddress3.replaceAll("'", "\\\\'")
							+ "' ,originCompany='"
							+ originCompany.replaceAll("'", "\\\\'")
							+ "' ,originCountry='" + originCountry
							+ "' , originState='" + originState
							+ "' ,originCity='"
							+ originCity.replaceAll("'", "\\\\'")
							+ "' , originZip='"
							+ originZip.replaceAll("'", "\\\\'")
							+ "' , originPreferredContactTime='"
							+ originPreferredContact.replaceAll("'", "\\\\'")
							+ "' , originDayPhone='"
							+ originDayPhone.replaceAll("'", "\\\\'")
							+ "' , originDayExtn='" + originDayPhoneExt
							+ "' ,originHomePhone='"
							+ originHomePhone.replaceAll("'", "\\\\'")
							+ "' ,originMobile='"
							+ originMobile.replaceAll("'", "\\\\'")
							+ "' ,originFax='"
							+ originFax.replaceAll("'", "\\\\'") + "' ,email='"
							+ email.replaceAll("'", "\\\\'") + "' ,email2='"
							+ email2.replaceAll("'", "\\\\'")
							+ "',originCityCode='"
							+ originCityCode.replaceAll("'", "\\\\'")
							+ "',originCountryCode='"
							+ originCountryCode.replaceAll("'", "\\\\'")
							+ "',updatedOn=now(),updatedBy='"
							+ userName 
							+ "' where "+linkedUpdate+" sequenceNumber= '" + sequenceNumber
							+ "' ").executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}
	
	public int updateWorkTicketOriginAddress(String originAddress1, String originAddress2,String originAddress3,String originCompany,String originCountry,String originState,String originCity,String originZip,String originPreferredContact,String originDayPhone,String originDayPhoneExt,String originHomePhone,String originMobile,String originFax,String sequenceNumber, String userName){
		try {
			return getSession()
					.createSQLQuery(
							"update workticket set address1='"
									+ originAddress1.replaceAll("'", "\\\\'")
									+ "' ,address2='"
									+ originAddress2.replaceAll("'", "\\\\'")
									+ "' ,address3='"
									+ originAddress3.replaceAll("'", "\\\\'")
									+ "' ,originCompany='"
									+ originCompany.replaceAll("'", "\\\\'")
									+ "' ,originCountry='"
									+ originCountry
									+ "' , state='"
									+ originState
									+ "', city='"
									+ originCity.replaceAll("'", "\\\\'")
									+ "' , zip='"
									+ originZip.replaceAll("'", "\\\\'")
									+ "' , originPreferredContactTime='"
									+ originPreferredContact.replaceAll("'",
											"\\\\'")
									+ "' , phone='"
									+ originDayPhone.replaceAll("'", "\\\\'")
									+ "' , phoneExt='"
									+ originDayPhoneExt
											.replaceAll("'", "\\\\'")
									+ "' ,homePhone='"
									+ originHomePhone.replaceAll("'", "\\\\'")
									+ "' ,originMobile='"
									+ originMobile.replaceAll("'", "\\\\'")
									+ "' ,originFax='"
									+ originFax.replaceAll("'", "\\\\'")
									+ "',updatedOn=now(),updatedBy='"
							        + userName 
									+ "' where sequenceNumber= '"
									+ sequenceNumber
									+ "' and targetActual not in ('A','C') and service not in ('PUC','DLC') ")
					.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}
	
	public int updateServiceOrderDestinationAddress(String destinationAddress1,String destinationAddress2,String destinationAddress3,String destinationCompany,String destinationCountry,String destinationState,String destinationCity,String destinationZip,String destPreferredContact,String destinationDayPhone,String destinationDayPhoneExt,String destinationHomePhone,String destinationMobile,String destinationFax,String destinationEmail,String destinationEmail2,String destinationCityCode,String destinationCountryCode,String sequenceNumber, String userName,String linkedUpdate){
		try {
			return getSession().createSQLQuery(
					"update serviceorder set destinationAddressLine1='"
							+ destinationAddress1.replaceAll("'", "\\\\'")
							+ "' , destinationAddressLine2='"
							+ destinationAddress2.replaceAll("'", "\\\\'")
							+ "' , destinationAddressLine3='"
							+ destinationAddress3.replaceAll("'", "\\\\'")
							+ "' , destinationCompany='"
							+ destinationCompany.replaceAll("'", "\\\\'")
							+ "' , destinationCountry='" + destinationCountry
							+ "' , destinationState='" + destinationState
							+ "' , destinationCity='"
							+ destinationCity.replaceAll("'", "\\\\'")
							+ "' , destinationZip='"
							+ destinationZip.replaceAll("'", "\\\\'")
							+ "' , destPreferredContactTime='"
							+ destPreferredContact.replaceAll("'", "\\\\'")
							+ "' , destinationDayPhone='"
							+ destinationDayPhone.replaceAll("'", "\\\\'")
							+ "' , destinationDayExtn='"
							+ destinationDayPhoneExt.replaceAll("'", "\\\\'")
							+ "' , destinationHomePhone='"
							+ destinationHomePhone.replaceAll("'", "\\\\'")
							+ "' , destinationMobile='"
							+ destinationMobile.replaceAll("'", "\\\\'")
							+ "' , destinationFax='"
							+ destinationFax.replaceAll("'", "\\\\'")
							+ "' , destinationEmail='"
							+ destinationEmail.replaceAll("'", "\\\\'")
							+ "' , destinationEmail2='"
							+ destinationEmail2.replaceAll("'", "\\\\'")
							+ "' , destinationCityCode='"
							+ destinationCityCode.replaceAll("'", "\\\\'")
							+ "' , destinationCountryCode='"
							+ destinationCountryCode.replaceAll("'", "\\\\'")
							+ "',updatedOn=now(),updatedBy='"
							+ userName 
							+ "' where "+linkedUpdate+" sequenceNumber= '" + sequenceNumber
							+ "' ").executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}
	
	public int updateWorkTicketDestinationAddress(String destinationAddress1,String destinationAddress2,String destinationAddress3,String destinationCompany,String destinationCountry,String destinationState,String destinationCity,String destinationZip,String destPreferredContact,String destinationDayPhone,String destinationDayPhoneExt,String destinationHomePhone,String destinationMobile,String destinationFax,String sequenceNumber, String userName,String destinationCountryCode){
		try {
			String query="update workticket set destinationAddress1='"+ destinationAddress1.replaceAll("'","\\\\'")
					+ "',destinationAddress2='"+ destinationAddress2.replaceAll("'","\\\\'")
					+ "',destinationAddress3='"+ destinationAddress3.replaceAll("'","\\\\'")
					+ "',destinationCompany='"+ destinationCompany.replaceAll("'","\\\\'")
					+ "',destinationCountry='"+ destinationCountry+ "',destinationState='"+ destinationState+ "',destinationCountryCode='"+ destinationCountryCode
					+ "',destinationCity='"+ destinationCity.replaceAll("'", "\\\\'")
					+ "',destinationZip='"+ destinationZip.replaceAll("'", "\\\\'")
					+ "',destPreferredContactTime='"+ destPreferredContact.replaceAll("'","\\\\'")
					+ "',destinationPhone='"+ destinationDayPhone.replaceAll("'","\\\\'")
					+ "',destinationPhoneExt='"+ destinationDayPhoneExt.replaceAll("'","\\\\'")
					+ "',destinationHomePhone='"+ destinationHomePhone.replaceAll("'","\\\\'")
					+ "',destinationMobile='"+ destinationMobile.replaceAll("'", "\\\\'")
					+ "',destinationFax='"+ destinationFax.replaceAll("'", "\\\\'")
					+ "',updatedOn=now(),updatedBy='"+ userName+ "' where sequenceNumber= '"+ sequenceNumber+ "' and targetActual not in ('A','C') and service not in ('PUC','DLC') ";
			return getSession()
					.createSQLQuery(query)
					.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}
	
	public List findCityState(String zipCode, String corpID){
		try {
			String query = "select CONCAT(ifNULL(city,''),'#',IFNULL(state,''),'#',IFNULL(stateabbreviation,''),'#',IFNULL(primaryRecord,''),'#',IFNULL(phoneAreaCode,'')) from refzipgeocodemap where zipcode= '"
					+ zipCode
					+ "' and corpID in ('"
					+ corpID
					+ "','TSFT') order by primaryRecord desc";
			return this.getSession().createSQLQuery(query).list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public List findCityStateByCountry(String zipCode, String corpID ,String country){
		try {
			String query = "select CONCAT(ifNULL(city,''),'#',IFNULL(state,''),'#',IFNULL(stateabbreviation,''),'#',IFNULL(primaryRecord,''),'#',IFNULL(phoneAreaCode,'')) from refzipgeocodemap where zipcode= '"
					+ zipCode
					+ "' and corpID in ('"
					+ corpID
					+ "','TSFT') and country='"
					+ country
					+ "' order by primaryRecord desc";
			return this.getSession().createSQLQuery(query).list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public String  findCityStateFlex(String countryCodeFlex, String corpID){
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select flex3 from refmaster where parameter='COUNTRY' and code='"
									+ countryCodeFlex + "'and corpID in ('"
									+ corpID + "','TSFT') ").list().get(0)
					.toString();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return "";
	}
	
	public List findCityStateNotPrimary(String zipCode, String corpID,String country){
		try {
			zipCode = zipCode.replace("%20", " ");
			String query = "from RefZipGeoCodeMap where zipcode='" + zipCode
					+ "' and corpID in ('" + corpID + "','TSFT')";
			return getHibernateTemplate().find(
					"from RefZipGeoCodeMap where zipcode='" + zipCode
							+ "'and country='" + country + "' and corpID in ('"
							+ corpID + "','TSFT')");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List surveySchedule(String job, String corpID,String DateInterval,String fromDate,String toDate,String surveyTime,String surveyTime2,String consultant)
	{
		try {
			try {
				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
				SimpleDateFormat df1 = new SimpleDateFormat("dd-MMM-yy");
				Date du = new Date();
				du = df.parse(fromDate);
				df = new SimpleDateFormat("yyyy-MM-dd");
				fromDate = df.format(du);
				Date du1 = new Date();
				du1 = df1.parse(toDate);
				df1 = new SimpleDateFormat("yyyy-MM-dd");
				toDate = df1.format(du1);
			} catch (java.text.ParseException e) {
				e.printStackTrace();
			}
			List list1 = new ArrayList();
			if (DateInterval != null
					&& DateInterval.equalsIgnoreCase("lessThanOneDay")) {

				list1 = this
						.getSession()
						.createSQLQuery(
								"select concat(originAddress1,',',originCity,',',originState,',',originCountry,'-',originZip) as address, "
										+ "estimator, DATE_FORMAT(survey,'%b %d') 'Survey', surveytime, surveytime2,  "
										+ "concat(firstName,'&nbsp;',lastname),originCity, originState,"
										+ "date_format(DATE_SUB('"
										+ fromDate
										+ "', INTERVAL 0 DAY),'%d-%b-%y') startDay, "
										+ "date_format(DATE_SUB('"
										+ toDate
										+ "', INTERVAL 0 DAY),'%d-%b-%y') add7Day, 'CF' as activityName "
										+ ", '"
										+ surveyTime
										+ "' as fromTime,'"
										+ surveyTime2
										+ "' as toTime,DATE_FORMAT(survey,'%d-%b-%y') 'Survey1' ,sequenceNumber as cfnumber, billToCode as billtocode "
										+ "from customerfile c inner join app_user  ap on ap.username=c.estimator "
										+ "inner join user_role ur on ur.user_id=ap.id "
										+ "inner join  role r on r.id= ur.role_id where "
										+ "ap.corpID = '"
										+ corpID
										+ "' and r.name='ROLE_SALE' and ap.account_enabled is true and ap.account_expired is false and ap.account_locked is false and ap.credentials_expired is false and (ap.jobtype like '%"
										+ job
										+ "%' or ap.jobtype='' or ap.jobtype is null) "
										+ "and date_format(survey,'%y-%m-%d') between date_format(DATE_SUB('"
										+ fromDate
										+ "', INTERVAL 0 DAY),'%y-%m-%d') and date_format(DATE_SUB('"
										+ toDate
										+ "', INTERVAL 0 DAY),'%y-%m-%d') "
										+ "and c.corpid = '"
										+ corpID
										+ "'  and length(estimator) > 1 and estimator like '%"
										+ consultant
										+ "%' "
										+ "and ((surveyTime between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "') or (surveyTime2 between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "')) "
										+ "union SELECT '', cl.username, DATE_FORMAT(surveyDate,'%b %d') 'surveyDate',fromtime,totime, "
										+ "'','', '',date_format(DATE_SUB('"
										+ fromDate
										+ "', INTERVAL 0 DAY),'%d-%b-%y') startDay, "
										+ "date_format(DATE_SUB('"
										+ toDate
										+ "', INTERVAL 0 DAY),'%d-%b-%y') add7Day,activityName "
										+ ", '"
										+ surveyTime
										+ "' as fromTime,'"
										+ surveyTime2
										+ "' as toTime,'' as 'Survey1','','' "
										+ "FROM calendarfile cl, app_user ap, role r,user_role ur "
										+ "where date_format(cl.surveyDate,'%y-%m-%d') between date_format(DATE_SUB('"
										+ fromDate
										+ "', INTERVAL 0 DAY),'%y-%m-%d') "
										+ "and date_format(DATE_SUB('"
										+ toDate
										+ "', INTERVAL 0 DAY),'%y-%m-%d') "
										+ "and cl.corpid= '"
										+ corpID
										+ "' and ap.id=ur.user_id and r.id= ur.role_id and cl.username like '%"
										+ consultant
										+ "%'"
										+ "and ap.corpID = '"
										+ corpID
										+ "' "
										+ "and r.name='ROLE_SALE' and ap.account_enabled is true and ap.account_expired is false and ap.account_locked is false and ap.credentials_expired is false and (ap.jobtype like '%"
										+ job
										+ "%' or ap.jobtype='' or ap.jobtype is null) and ap.username=cl.username "
										+ "and ((fromtime between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "') or (totime between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "')) " + "order by 2,3,4").list();
			} else if (DateInterval != null
					&& DateInterval.equalsIgnoreCase("greaterThanOneDay")) {
				list1 = this
						.getSession()
						.createSQLQuery(
								"select concat(originAddress1,',',originCity,',',originState,',',originCountry,'-',originZip) as address, "
										+ "estimator, DATE_FORMAT(survey,'%b %d') 'Survey', surveytime, surveytime2,  "
										+ "concat(firstName,'&nbsp;',lastname),originCity, originState,"
										+ "date_format(DATE_ADD('"
										+ fromDate
										+ "', INTERVAL 0 DAY),'%d-%b-%y') startDay, "
										+ "date_format(DATE_ADD('"
										+ toDate
										+ "', INTERVAL 0 DAY),'%d-%b-%y') add7Day,'CF'  as activityName "
										+ ", '"
										+ surveyTime
										+ "' as fromTime,'"
										+ surveyTime2
										+ "' as toTime,DATE_FORMAT(survey,'%d-%b-%y') 'Survey1',sequenceNumber as cfnumber, billToCode as billtocode  "
										+ "from customerfile c inner join app_user  ap on ap.username=c.estimator "
										+ "inner join user_role ur on ur.user_id=ap.id "
										+ "inner join  role r on r.id= ur.role_id where "
										+ "ap.corpID = '"
										+ corpID
										+ "' and r.name='ROLE_SALE' and ap.account_enabled is true and ap.account_expired is false and ap.account_locked is false and ap.credentials_expired is false and (ap.jobtype like '%"
										+ job
										+ "%' or ap.jobtype='' or ap.jobtype is null) "
										+ "and date_format(survey,'%y-%m-%d') between date_format(DATE_ADD('"
										+ fromDate
										+ "', INTERVAL 0 DAY),'%y-%m-%d') and date_format(DATE_ADD('"
										+ toDate
										+ "', INTERVAL 0 DAY),'%y-%m-%d') "
										+ "and c.corpid = '"
										+ corpID
										+ "'  and length(estimator) > 1 and estimator like '%"
										+ consultant
										+ "%'"
										+ "and ((surveyTime between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "') or (surveyTime2 between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "')) "
										+ "union SELECT '', cl.username, DATE_FORMAT(surveyDate,'%b %d') 'surveyDate',fromtime,totime, "
										+ "'','', '',date_format(DATE_ADD('"
										+ fromDate
										+ "', INTERVAL 0 DAY),'%d-%b-%y') startDay, "
										+ "date_format(DATE_ADD('"
										+ toDate
										+ "', INTERVAL 0 DAY),'%d-%b-%y') add7Day,activityName "
										+ ", '"
										+ surveyTime
										+ "' as fromTime,'"
										+ surveyTime2
										+ "' as toTime,'' as 'Survey1' ,'','' "
										+ "FROM calendarfile cl, app_user ap, role r,user_role ur "
										+ "where date_format(cl.surveyDate,'%y-%m-%d') between date_format(DATE_ADD('"
										+ fromDate
										+ "', INTERVAL 0 DAY),'%y-%m-%d') "
										+ "and date_format(DATE_ADD('"
										+ toDate
										+ "', INTERVAL 0 DAY),'%y-%m-%d') "
										+ "and cl.corpid= '"
										+ corpID
										+ "' and ap.id=ur.user_id and r.id= ur.role_id and cl.username like '%"
										+ consultant
										+ "%' and ap.corpID = '"
										+ corpID
										+ "' "
										+ "and r.name='ROLE_SALE' and ap.account_enabled is true and ap.account_expired is false and ap.account_locked is false and ap.credentials_expired is false and (ap.jobtype like '%"
										+ job
										+ "%' or ap.jobtype='' or ap.jobtype is null) and ap.username=cl.username "
										+ "and ((fromtime between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "') or (totime between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "')) " + "order by 2,3,4").list();
			} else if (DateInterval != null
					&& DateInterval.equalsIgnoreCase("refreshAppointments")) {
				list1 = this
						.getSession()
						.createSQLQuery(
								"select concat(originAddress1,',',originCity,',',originState,',',originCountry,'-',originZip) as address, "
										+ "estimator, DATE_FORMAT(survey,'%b %d') 'Survey', surveytime, surveytime2,  "
										+ "concat(firstName,'&nbsp;',lastname),originCity, originState,"
										+ "date_format('"
										+ fromDate
										+ "','%d-%b-%y') startDay, "
										+ "date_format('"
										+ toDate
										+ "','%d-%b-%y') add7Day,'CF'  as activityName "
										+ ", '"
										+ surveyTime
										+ "' as fromTime,'"
										+ surveyTime2
										+ "' as toTime,DATE_FORMAT(survey,'%d-%b-%y') 'Survey1',sequenceNumber as cfnumber , billToCode as billtocode "
										+ "from customerfile c inner join app_user  ap on ap.username=c.estimator "
										+ "inner join user_role ur on ur.user_id=ap.id "
										+ "inner join  role r on r.id= ur.role_id where "
										+ "ap.corpID = '"
										+ corpID
										+ "' and r.name='ROLE_SALE' and ap.account_enabled is true and ap.account_expired is false and ap.account_locked is false and ap.credentials_expired is false and (ap.jobtype like '%"
										+ job
										+ "%' or ap.jobtype='' or ap.jobtype is null) "
										+ "and date_format(survey,'%y-%m-%d') between date_format('"
										+ fromDate
										+ "','%y-%m-%d') and date_format('"
										+ toDate
										+ "','%y-%m-%d') "
										+ "and c.corpid = '"
										+ corpID
										+ "'  and length(estimator) > 1 and estimator like '%"
										+ consultant
										+ "%'"
										+ "and ((surveyTime between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "') or (surveyTime2 between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "')) "
										+ "union SELECT '', cl.username, DATE_FORMAT(surveyDate,'%b %d') 'surveyDate',fromtime,totime, "
										+ "'','', '',date_format('"
										+ fromDate
										+ "','%d-%b-%y') startDay, "
										+ "date_format('"
										+ toDate
										+ "','%d-%b-%y') add7Day,activityName "
										+ ", '"
										+ surveyTime
										+ "' as fromTime,'"
										+ surveyTime2
										+ "' as toTime,'' as 'Survey1' ,'','' "
										+ "FROM calendarfile cl, app_user ap, role r,user_role ur "
										+ "where date_format(cl.surveyDate,'%y-%m-%d') between date_format('"
										+ fromDate
										+ "','%y-%m-%d') "
										+ "and date_format('"
										+ toDate
										+ "','%y-%m-%d') "
										+ "and cl.corpid= '"
										+ corpID
										+ "' and ap.id=ur.user_id and r.id= ur.role_id and cl.username like '%"
										+ consultant
										+ "%' and ap.corpID = '"
										+ corpID
										+ "' "
										+ "and r.name='ROLE_SALE' and ap.account_enabled is true and ap.account_expired is false and ap.account_locked is false and ap.credentials_expired is false and (ap.jobtype like '%"
										+ job
										+ "%' or ap.jobtype='' or ap.jobtype is null) and ap.username=cl.username "
										+ "and ((fromtime between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "') or (totime between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "')) " + "order by 2,3,4").list();
			} else if (DateInterval != null
					&& DateInterval.equalsIgnoreCase("homeDetails")) {
				list1 = this
						.getSession()
						.createSQLQuery(
								"SELECT distinct concat(addressHome,', ',cityHome,', ',provinceHome,', ',countryHome,'-',postalCodeHome) as consultantHomeAdd,UCASE(username) as estimator, date_format('"
										+ fromDate
										+ "','%d-%b-%y') as startDay, "
										+ "date_format('"
										+ toDate
										+ "','%d-%b-%y') as add7Day FROM app_user, user_role, role where "
										+ "user_role.user_id = app_user.id and role.id = user_role.role_id and "
										+ "app_user.account_expired=FALSE  and app_user.account_enabled=true and "
										+ "role.name='ROLE_SALE' and app_user.corpid = '"
										+ corpID
										+ "' "
										+ "and (jobtype is null or jobtype ='' or jobtype like '%"
										+ job
										+ "%' ) and replace(concat(addressHome,',',cityHome,',',provinceHome,',',countryHome,',',postalCodeHome),',','')<>'' order by username")
						.list();
			} else {
				list1 = this
						.getSession()
						.createSQLQuery(
								"select concat(originAddress1,',',originCity,',',originState,',',originCountry,'-',originZip) as address, "
										+ "estimator, DATE_FORMAT(survey,'%b %d') 'Survey', surveytime, surveytime2,  "
										+ "concat(firstName,'&nbsp;',lastname),originCity, "
										+ "originState,date_format(now(),'%d-%b-%y') startDay,"
										+ "date_format(DATE_ADD(now(), INTERVAL 7 DAY),'%d-%b-%y') add7Day, 'CF'  as activityName "
										+ ",('07:00') as fromTime,('18:00') as toTime,DATE_FORMAT(survey,'%d-%b-%y') 'Survey1',sequenceNumber as cfnumber, billToCode as billtocode "
										+ "from customerfile c,app_user  ap , role r,  "
										+ "user_role ur where ap.id=ur.user_id and r.id= ur.role_id and "
										+ "ap.corpID = '"
										+ corpID
										+ "' and r.name='ROLE_SALE' and ap.account_enabled is true and ap.account_expired is false and ap.account_locked is false and ap.credentials_expired is false and (ap.jobtype like '%"
										+ job
										+ "%' or ap.jobtype='' or ap.jobtype is null) "
										+ "and date_format(survey,'%y-%m-%d') between date_format(now(),'%y-%m-%d') "
										+ "and date_format(DATE_ADD(now(), INTERVAL 7 DAY),'%y-%m-%d') and c.corpid = '"
										+ corpID
										+ "' "
										+ " and length(estimator) > 1 and estimator like '%"
										+ consultant
										+ "%' and ap.username=c.estimator "
										+ "and ((surveyTime between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "') or (surveyTime2 between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "')) "
										+ "union SELECT '', cl.username, DATE_FORMAT(surveyDate,'%b %d') 'surveyDate',cl.fromtime,cl.totime, "
										+ "'','', '',date_format(now(),'%d-%b-%y') startDay, "
										+ "date_format(DATE_ADD(now(), INTERVAL 7 DAY),'%d-%b-%y') add7Day, "
										+ "activityName,('07:00') as fromTime,('18:00') as toTime,'' as 'Survey1','','' FROM calendarfile cl, app_user ap, role r,user_role ur "
										+ "where date_format(cl.surveyDate,'%Y-%m-%d') between date_format(now(),'%Y-%m-%d') and DATE_ADD(date_format(now(),'%Y-%m-%d'), INTERVAL 7 DAY) "
										+ "and cl.corpid= '"
										+ corpID
										+ "' and ap.id=ur.user_id and r.id= ur.role_id and ap.corpID = '"
										+ corpID
										+ "' "
										+ "and r.name='ROLE_SALE' and ap.account_enabled is true and ap.account_expired is false and ap.account_locked is false and ap.credentials_expired is false and (ap.jobtype like '%"
										+ job
										+ "%' or ap.jobtype='' or ap.jobtype is null) and ap.username=cl.username and cl.username like '%"
										+ consultant
										+ "%' "
										+ "and ((fromtime between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "') or (totime between '"
										+ surveyTime
										+ "' and '"
										+ surveyTime2
										+ "')) " + "order by 2,3,4").list();
			}
			return list1;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
}
	
	public List findsurveyScheduleList(String estimator,String surveyDate,String consultantTime,String consultantTime2,String corpID){
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select id from customerfile " + "where corpID = '"
									+ corpID
									+ "' and (date_format(survey,'%d-%b-%y') = '"
									+ surveyDate
									+ "' or date_format(survey,'%d-%b-%Y') = '"
									+ surveyDate
									+ "') "
									+ " and ((surveyTime between '"
									+ consultantTime
									+ "' and '"
									+ consultantTime2
									+ "') or (surveyTime2 between '"
									+ consultantTime
									+ "' and '"
									+ consultantTime2
									+ "') or (surveyTime <= '"
									+ consultantTime
									+ "' and surveyTime2 >='"
									+ consultantTime2
									+ "') or (surveyTime2 <= '"
									+ consultantTime
									+ "' and surveyTime>= '"
									+ consultantTime2
									+ "')) "
									+ "and estimator='"
									+ estimator
									+ "' "
									+ "union SELECT id FROM calendarfile where (date_format(surveyDate,'%d-%b-%y') = '"
									+ surveyDate
									+ "' or date_format(surveyDate,'%d-%b-%Y') = '"
									+ surveyDate + "') " + "and corpID = '"
									+ corpID + "' and ((fromtime between '"
									+ consultantTime + "' and '"
									+ consultantTime2
									+ "') or (totime between '"
									+ consultantTime + "' and '"
									+ consultantTime2 + "') or (fromtime <= '"
									+ consultantTime + "' and totime >='"
									+ consultantTime2 + "') or (totime <= '"
									+ consultantTime + "' and fromtime>= '"
									+ consultantTime2 + "')) "
									+ "and username='" + estimator + "' ")
					.list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}

	public List findCordinaorListRef(String custJobType, String corpID) {
		try {
			List cordinatorList = new ArrayList();
			String username = "";
			List cordList = getHibernateTemplate().find(
					"select coordinator from RefJobType where job='"
							+ custJobType + "' AND corpID='" + corpID + "'");
			if (!cordList.isEmpty()) {
				username = cordList.get(0).toString();
				return cordinatorList = this
						.getSession()
						.createSQLQuery(
								"SELECT distinct CONCAT(UPPER(username),'#',UPPER(alias)) FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE  and app_user.account_enabled=true "
								+ "and username='" + username + "'  order by alias")
						.list();
			} else {
				return cordinatorList;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
		
	}

	public List findEstimatorListRef(String custJobType, String corpID) {
		try {
			List estimatorList = new ArrayList();
			String username = "";
			List saleList = getHibernateTemplate().find(
					"select estimator from RefJobType where job='"
							+ custJobType + "' AND corpID='" + corpID + "'");
			if (!saleList.isEmpty()) {
				username = saleList.get(0).toString();
				return estimatorList = this
						.getSession()
						.createSQLQuery(
								"SELECT distinct CONCAT(UPPER(username),'#',UPPER(alias)) FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE  and app_user.account_enabled=true "
								+ "and username='" + username + "'   order by alias")
						.list();
			} else {
				return estimatorList;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	} 
	
	public List findEstimatorName(String estimatorEmail, String sessionCorpID){
		try {
			return getSession().createSQLQuery(
					"select concat(a.first_name,' ',a.last_name) FROM app_user a where a.username='"
							+ estimatorEmail + "' and a.corpID='"
							+ sessionCorpID + "' ").list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List findEstimatorFirstName(String estimatorEmail, String sessionCorpID){
		try {
			return getSession().createSQLQuery(
					"select a.first_name FROM app_user a where a.username='"
							+ estimatorEmail + "' and a.corpID='"
							+ sessionCorpID + "' ").list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public List findUserPhone(String remoteUser, String sessionCorpID){
		try {
			return getSession().createSQLQuery(
					"select a.phone_number FROM app_user a where a.username='"
							+ remoteUser + "' and a.corpID='" + sessionCorpID
							+ "' ").list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public List getEstimatorNumber(String estimator,String corpid){
		try {
			return getSession().createSQLQuery(
					"select a.mobileNumber FROM app_user a where a.username='"
							+ estimator + "' and a.corpID='"+ corpid+ "' ").list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpid,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public List findWebsite(String remoteUser, String sessionCorpID){
		try {
			return getSession().createSQLQuery(
					"select a.website FROM app_user a where a.username='"
							+ remoteUser + "' and a.corpID='" + sessionCorpID
							+ "' ").list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List surveyScheduleFromSurveyList(String job, String corpID,String DateInterval,String fromDate,String toDate,String surveyTime,String surveyTime2,String consultant,String surveyCity){
		List surlist1 = new ArrayList();
		try {
			surlist1 = this
					.getSession()
					.createSQLQuery(
							"select concat(originAddress1,',',originCity,',',originState,',',originCountry,'-',originZip) as address, "
									+ "estimator, DATE_FORMAT(survey,'%b %d') 'Survey', surveytime, surveytime2,  "
									+ "concat(firstName,'&nbsp;',lastname),originCity, "
									+ "originState,date_format('"
									+ fromDate
									+ "','%d-%b-%y') startDay,"
									+ "date_format('"
									+ toDate
									+ "','%d-%b-%y') add7Day, 'CF'  as activityName "
									+ ",('07:00') as fromTime,('18:00') as toTime,DATE_FORMAT(survey,'%d-%b-%y') 'Survey1',sequenceNumber as cfnumber, billToCode as billtocode "
									+ "from customerfile c,app_user  ap , role r,  "
									+ "user_role ur where ap.id=ur.user_id and r.id= ur.role_id and "
									+ "ap.corpID = '"
									+ corpID
									+ "' and r.name='ROLE_SALE' AND c.originCity like '"
									+ surveyCity
									+ "%' and ap.account_enabled is true and ap.account_expired is false and ap.account_locked is false and ap.credentials_expired is false and (ap.jobtype like '%"
									+ job
									+ "%' or ap.jobtype='' or ap.jobtype is null) "
									+ "and date_format(survey,'%y-%m-%d') between date_format('"
									+ fromDate
									+ "','%y-%m-%d') and date_format('"
									+ toDate
									+ "','%y-%m-%d') "
									+ "and c.corpid = '"
									+ corpID
									+ "' "
									+ " and length(estimator) > 1 and estimator like '%"
									+ consultant
									+ "%' and ap.username=c.estimator "
									+ "and ((surveyTime between '"
									+ surveyTime
									+ "' and '"
									+ surveyTime2
									+ "') or (surveyTime2 between '"
									+ surveyTime
									+ "' and '"
									+ surveyTime2
									+ "')) "
									+ "union SELECT '', cl.username, DATE_FORMAT(surveyDate,'%b %d') 'surveyDate',cl.fromtime,cl.totime, "
									+ "'','', '',date_format('"
									+ fromDate
									+ "','%d-%b-%y') startDay, "
									+ "date_format('"
									+ toDate
									+ "','%d-%b-%y') add7Day, "
									+ "activityName,('07:00') as fromTime,('18:00') as toTime,'' as 'Survey1','','' FROM calendarfile cl, app_user ap, role r,user_role ur "
									+ "where date_format(cl.surveyDate,'%y-%m-%d') between date_format('"
									+ fromDate
									+ "','%y-%m-%d') and date_format('"
									+ toDate
									+ "','%y-%m-%d') "
									+ "and cl.corpid= '"
									+ corpID
									+ "' and ap.id=ur.user_id and r.id= ur.role_id and ap.corpID = '"
									+ corpID
									+ "' "
									+ "and r.name='ROLE_SALE' and ap.account_enabled is true and ap.account_expired is false and ap.account_locked is false and ap.credentials_expired is false and (ap.jobtype like '%"
									+ job
									+ "%' or ap.jobtype='' or ap.jobtype is null) and ap.username=cl.username and cl.username like '%"
									+ consultant
									+ "%' "
									+ "and ((fromtime between '"
									+ surveyTime
									+ "' and '"
									+ surveyTime2
									+ "') or (totime between '"
									+ surveyTime
									+ "' and '"
									+ surveyTime2
									+ "')) "
									+ "order by 2,3,4").list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
	return surlist1;
	}
	
	public int updateSOStatus(String sequenceNumberForStatus, String statusReas, String sessionCorpID,String userName)	{
		try {
			return getHibernateTemplate().bulkUpdate(
					"update ServiceOrder set status='CNCL',statusReason='"
							+ statusReas
							+ "', statusNumber='400',updatedOn=now(),updatedBy='"+userName+"' where sequenceNumber='"
							+ sequenceNumberForStatus + "' and corpID='"
							+ sessionCorpID + "' ");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	} 

	public Map<String, String> findExistingStateList(String bucket2, String corpId, String stateCode) {
		try {
			if (bucket2 == null || bucket2.equals("")) {
				bucket2 = "NoCountry";
			}
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			List<RefMaster> list = getHibernateTemplate().find(
					"from RefMaster where bucket2 like '" + bucket2
							+ "%' and parameter ='STATE'  and code='"
							+ stateCode + "' order by description");
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode(),
						refMaster.getDescription());
			}
			return parameterMap;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	} 

	public List getPartnerDitailList(String sessionCorpID, String parentAgent) {
		try {
			return getHibernateTemplate().find(
					"select lastName from PartnerPrivate where partnerCode='"
							+ parentAgent + "'");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
		
	}

	public List getPartnerAccountmanager(String sessionCorpID, String parentAgent) {
		try {
			return getHibernateTemplate().find(
					"select accountManager from PartnerPrivate where partnerCode='"
							+ parentAgent + "' ");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}

	public  class DTOBookingAgent
	 {		
		 	private Object bookingagentcode;
		  	private Object companycode;
			private Object lastname;
			public Object getBookingagentcode() {
				return bookingagentcode;
			}
			public void setBookingagentcode(Object bookingagentcode) {
				this.bookingagentcode = bookingagentcode;
			}
			public Object getCompanycode() {
				return companycode;
			}
			public void setCompanycode(Object companycode) {
				this.companycode = companycode;
			}
			public Object getLastname() {
				return lastname;
			}
			public void setLastname(Object lastname) {
				this.lastname = lastname;
			}  
	 }
	
	public List getBookingAgentDetailList(String sessionCorpID, String parentAgent) {
		List list1 = new ArrayList();
		try {
			String query = "select distinct c.bookingagentcode , c.companycode, px.lastname  from partnerprivate p, companydivision c, partnerpublic px where p.companydivision = c.companycode and p.partnercode = '"
					+ parentAgent
					+ "' and p.corpid  in ('"
					+ sessionCorpID
					+ "','"
					+ hibernateUtil.getParentCorpID(sessionCorpID)
					+ "') and c.corpid = '"
					+ sessionCorpID
					+ "' and px.partnercode = c.bookingagentcode and p.partnerPublicId=px.id and c.scac = ''";
			List bookingAgentDetailListList = this.getSession()
					.createSQLQuery(query).list();
			Iterator it = bookingAgentDetailListList.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				DTOBookingAgent dtoBookingAgent = new DTOBookingAgent();
				if (row[0] == null) {
					dtoBookingAgent.setBookingagentcode("");
				} else {
					dtoBookingAgent.setBookingagentcode(row[0].toString());
				}
				if (row[1] == null) {
					dtoBookingAgent.setCompanycode("");
				} else {
					dtoBookingAgent.setCompanycode(row[1].toString());
				}
				if (row[2] == null) {
					dtoBookingAgent.setLastname("");
				} else {
					dtoBookingAgent.setLastname(row[2].toString());
				}
				list1.add(dtoBookingAgent);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return list1;
	}

	public int updateOrderInitiationStatus(Long id, String sessionCorpID, String quotationStatus) {
		int i=0;
		try {
			if (quotationStatus.equals("Accepted")) {
				i = getHibernateTemplate().bulkUpdate(
						"update CustomerFile set quotationStatus='"
								+ quotationStatus
								+ "', controlFlag='C',moveType='BookedMove' where id = '" + id
								+ "' and corpID='" + sessionCorpID + "'");
			}
			if (quotationStatus.equals("Rejected")) {
				i = getHibernateTemplate().bulkUpdate(
						"update CustomerFile set quotationStatus='"
								+ quotationStatus + "' where id = '" + id
								+ "' and corpID='" + sessionCorpID + "'");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return i;
	}

	public int updateOrderManagementStatus(Long id, String sessionCorpID, String quotationStatus,String orderAction,boolean accessQuotationFromCustomerFile) {
		try {
			if(orderAction!=null && orderAction.toString().equalsIgnoreCase("QR")){
				if(accessQuotationFromCustomerFile){
					return getHibernateTemplate()
							.bulkUpdate(
									"update CustomerFile set quotationStatus='New',moveType='Quote',controlFlag='C',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where id = '" + id + "' and corpID='" + sessionCorpID
											+ "'");
				}else{

					return getHibernateTemplate()
							.bulkUpdate(
									"update CustomerFile set quotationStatus='New',moveType='Quote',controlFlag='Q',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where id = '" + id + "' and corpID='" + sessionCorpID
											+ "'");
				
				}
			}else{
			return getHibernateTemplate()
					.bulkUpdate(
							"update CustomerFile set quotationStatus='"
									+ quotationStatus
									+ "', controlFlag='C', comptetive='N', salesStatus='Pending' ,moveType='BookedMove',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where id = '"
									+ id + "' and corpID='" + sessionCorpID
									+ "'");
			}
			} catch (Exception e) {
				e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}

	public List getRefJobTypeCountry(String job,String sessionCorpID,String compDivision){
		List al2 = new ArrayList();
		try {
			String str = "";
			List al = this
					.getSession()
					.createSQLQuery(
							"select concat(countryCode,'~',country) from refjobtype where job='"
									+ job + "' and corpID='" + sessionCorpID
									+ "' and companyDivision='" + compDivision
									+ "' ").list();
			if ((al != null) && (!al.isEmpty()) && (al.get(0) != null)) {
				str = al.get(0).toString();
			}
			if (!str.equalsIgnoreCase("") && !str.trim().equalsIgnoreCase("~")) {
				String arr[] = str.split("~");
				if (arr[0]!=null && !arr[0].equalsIgnoreCase("")) {
					String al1 = this
							.getSession()
							.createSQLQuery(
									"select flex3 from refmaster where parameter='COUNTRY' and code='"
											+ arr[0] + "'and corpID in ('"
											+ sessionCorpID + "','TSFT') ")
							.list().get(0).toString();
					str = al1;
				}
			}
			
			if ((str != null) && (!str.equalsIgnoreCase("") && !str.trim().equalsIgnoreCase("~"))
					&& (str.equalsIgnoreCase("Y"))) {
				al2 = this
						.getSession()
						.createSQLQuery(
								"select concat(countryCode,'~',country,'~Y') from refjobtype where job='"
										+ job + "' and corpID='"
										+ sessionCorpID
										+ "' and companyDivision='"
										+ compDivision + "' ").list();
			} else {
				al2 = this
						.getSession()
						.createSQLQuery(
								"select concat(countryCode,'~',country,'~N') from refjobtype where job='"
										+ job + "' and corpID='"
										+ sessionCorpID
										+ "' and companyDivision='"
										+ compDivision + "' ").list();
			}
			return al2;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return al2;
	}

	public List findMaximumSequenceNumber(String externalCorpID) {
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select max(sequenceNumber) from customerfile where sequenceNumber like '"
									+ externalCorpID + "%' and corpID ='"
									+ externalCorpID + "'").list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}

	public List findMaximumShipExternal(String sequenceNumber, String externalCorpID) {
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select max(ship) from serviceorder where  sequenceNumber='"
									+ sequenceNumber + "' and corpID ='"
									+ externalCorpID + "' ").list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}

	public Boolean checkNetworkRecord(String sessionCorpID,	String bookingAgentCode) {
		Boolean isNetwork=false;
		try {
			List isNetworkList = this
					.getSession()
					.createSQLQuery(
							"select count(*) from networkpartners where corpID='"
									+ sessionCorpID
									+ "' and agentpartnerCode='"
									+ bookingAgentCode
									+ "' and agentpartnerCode <> '' and agentpartnerCode is not null ")
					.list();
			if (Integer.parseInt(isNetworkList.get(0).toString()) > 0) {
				isNetwork = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return isNetwork;
	}

	public void updateIsNetworkFlag(Long customerFileId, String corpID,Long serviceOrderId, String contractType, Boolean isNetworkGroup) {
		try {
			if ((contractType == null || contractType.equals(""))
					&& !(isNetworkGroup)) {
				getHibernateTemplate().bulkUpdate(
						"update CustomerFile set isNetworkRecord=true where corpID='"
								+ corpID + "' and id=" + customerFileId + "");
			} else {
				getHibernateTemplate().bulkUpdate(
						"update CustomerFile set isNetworkRecord=true,contractType='"
								+ contractType + "',isNetworkGroup="
								+ isNetworkGroup + " where corpID='" + corpID
								+ "' and id=" + customerFileId + "");
			}
			getHibernateTemplate().bulkUpdate(
					"update ServiceOrder set isNetworkRecord=true where corpID='"
							+ corpID + "' and id=" + serviceOrderId + "");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
	}

	public List getLinkedSequenceNumber(String sequenceNumber, String recordType) {
		List linkedSequnceNumberList= new ArrayList();
		List linkedSequnceNumberReturnList= new ArrayList();
		List linkedSequnceNumberListOutput= new ArrayList();
		try {
			if (recordType.equalsIgnoreCase("Primary")) {
				linkedSequnceNumberList = this
						.getSession()
						.createSQLQuery(
								"select concat(sequenceNumber,if(bookingAgentSequenceNumber is not null and bookingAgentSequenceNumber !='',',',''),if(bookingAgentSequenceNumber is not null and bookingAgentSequenceNumber !='',bookingAgentSequenceNumber,'')) from customerfile where bookingAgentSequenceNumber='"
										+ sequenceNumber + "'").list();
				String linkSeqNumLst = "";
				if (linkedSequnceNumberList != null
						&& (!linkedSequnceNumberList.isEmpty())
						&& linkedSequnceNumberList.get(0) != null
						&& !(linkedSequnceNumberList.get(0).toString()
								.equalsIgnoreCase(""))) {
					linkSeqNumLst = "'"
							+ linkedSequnceNumberList.toString()
									.replace("[", "").replace("]", "")
									.replaceAll(" ", "").replaceAll(",", "','")
							+ "'";
					linkedSequnceNumberListOutput = this
							.getSession()
							.createSQLQuery(
									"select concat(sequenceNumber,if(bookingAgentSequenceNumber is not null and bookingAgentSequenceNumber !='',',',''),if(bookingAgentSequenceNumber is not null and bookingAgentSequenceNumber !='',bookingAgentSequenceNumber,'')) from customerfile where bookingAgentSequenceNumber in ("
											+ linkSeqNumLst + ")").list();
				}
				if (linkedSequnceNumberListOutput != null) {
					for (int i = 0; i < linkedSequnceNumberListOutput.size(); i++) {
						linkedSequnceNumberList
								.add(linkedSequnceNumberListOutput.get(i)
										.toString());
					}
				}
			}
			if (recordType.equalsIgnoreCase("Secondary")) {
				linkedSequnceNumberList = this
						.getSession()
						.createSQLQuery(
								"select concat(sequenceNumber,if(bookingAgentSequenceNumber is not null and bookingAgentSequenceNumber !='',',',''),if(bookingAgentSequenceNumber is not null and bookingAgentSequenceNumber !='',bookingAgentSequenceNumber,'')) from customerfile where (bookingAgentSequenceNumber='"
										+ sequenceNumber
										+ "' or sequenceNumber='"
										+ sequenceNumber + "')").list();
				String linkSeqNumLst = "";
				if (linkedSequnceNumberList != null
						&& (!linkedSequnceNumberList.isEmpty())
						&& linkedSequnceNumberList.get(0) != null
						&& !(linkedSequnceNumberList.get(0).toString()
								.equalsIgnoreCase(""))) {
					linkSeqNumLst = "'"
							+ linkedSequnceNumberList.toString()
									.replace("[", "").replace("]", "")
									.replaceAll(" ", "").replaceAll(",", "','")
							+ "'";
					linkedSequnceNumberListOutput = this
							.getSession()
							.createSQLQuery(
									"select concat(sequenceNumber,if(bookingAgentSequenceNumber is not null and bookingAgentSequenceNumber !='',',',''),if(bookingAgentSequenceNumber is not null and bookingAgentSequenceNumber !='',bookingAgentSequenceNumber,'')) from customerfile where (bookingAgentSequenceNumber in ("
											+ linkSeqNumLst
											+ ") or sequenceNumber in ("
											+ linkSeqNumLst + "))").list();
				}
				if (linkedSequnceNumberListOutput != null) {
					for (int i = 0; i < linkedSequnceNumberListOutput.size(); i++) {
						linkedSequnceNumberList
								.add(linkedSequnceNumberListOutput.get(i)
										.toString());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		} 
		if (linkedSequnceNumberList != null && (!linkedSequnceNumberList.isEmpty()) && linkedSequnceNumberList.get(0) != null && !(linkedSequnceNumberList.get(0).toString() .equalsIgnoreCase(""))) {
			String s= linkedSequnceNumberList.toString().replace("[", "").replace("]", "");	
			String[]  partnerPub = s.split(",");
			int arrayLength = partnerPub.length;
			for(int i=0;i<arrayLength;i++)
			 {
				if(partnerPub[i]!=null){
				linkedSequnceNumberReturnList.add(partnerPub[i].trim());
				}
			 }
		} 
		
		return linkedSequnceNumberReturnList;
	}

	public Long findRemoteCustomerFile(String sequenceNumber) {
	Long cid=0L;
		try {
			List custList = this
					.getSession()
					.createSQLQuery(
							"select id from customerfile where sequenceNumber='"
									+ sequenceNumber + "'").list();
			if (!custList.isEmpty()) {
				cid = Long.parseLong(custList.get(0).toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return cid;
	}

	public List findFieldToSync(String modalClass , String fieldType, String uniqueFieldType) {
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select concat(fieldName,'~',if(toFieldName is null or toFieldName='',fieldName,toFieldName)) from networkdatafields where modelName='"
									+ modalClass
									+ "' and transactionType='Update' and (type='' or type is null or type='"
									+ fieldType
									+ "' or type='"
									+ uniqueFieldType + "') ").list();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}

	public List findFieldToSyncCreate(String modalClass, String fieldType, String uniqueFieldType) {
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select concat(fieldName,'~',if(toFieldName is null or toFieldName='',fieldName,toFieldName)) from networkdatafields where modelName='"
									+ modalClass
									+ "' and transactionType in ('Create','Update') and (type='' or type is null or type='"
									+ fieldType
									+ "' or type='"
									+ uniqueFieldType + "') ").list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	public List findAddressList(String sequenceNumber, String sessionCorpID){
		List findAddressList=new ArrayList();
		 try {
			 List list = this.getSession().createSQLQuery("select distinct r.address1,r.description,s.sequenceNumber from customerfile c left outer join serviceorder s on c.sequenceNumber=s.sequenceNumber " +
					 	 "and s.status not in ('CNCL') left outer join refmaster r  on c.destinationCountryCode =r.code or s.destinationCountryCode =r.code where c.sequenceNumber = '"+ sequenceNumber +"' " +
					 	 "and  r.corpid in ('" + sessionCorpID + "','TSFT') " +
					 	 "and (r.language='en' "+" || r.language ='' || r.language is null ) " +  
					 	 "and r.parameter ='COUNTRY' ").list();
		
		 Iterator it = list.iterator();
		 DTO dto = null;
		 while(it.hasNext()){
			 Object[] row = (Object[]) it.next();
		     dto = new DTO();
             dto.setAddress1((row[0]==null?"":row[0]+""));
			 dto.setCountryName((row[1]==null?"":row[1]+""));
			 dto.setShipNumber((row[2]==null?"":row[2]+""));
			 findAddressList.add(dto);
		 }
			
		 } catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return findAddressList;
	 }
	
	public List<ServiceOrder> findAddress(String countryName, String sessionCorpID){
		try {
			return getHibernateTemplate().find(
					"select address1 from RefMaster where parameter='COUNTRY' and description='"
							+ countryName + "'");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	
	public List<ServiceOrder> findCportalResourceMgmtDoc(String seq,String sessionCorpID,String usertype){
		List findCportalResourceMgmtDoc=new ArrayList();
		 try {
			List list = this
					.getSession()
					.createSQLQuery(
							"select distinct crm.id,crm.documentType,crm.documentName,crm.fileFileName from cportalresourcemgmt crm,serviceorder s where (s.originCountry=crm.originCountry or s.destinationCountry=crm.destinationCountry) and s.job=crm.jobType and crm.billToCode='' and crm.documentType  not in ('Valuation', 'General','Internal') and crm.billToExcludes not like concat('%',s.billToCode,'%') and crm.corpId='"
									+ sessionCorpID
									+ "' and s.sequenceNumber= '" + seq + "'")
					.addScalar("crm.id", Hibernate.LONG)
					.addScalar("crm.documentType", Hibernate.STRING)
					.addScalar("crm.documentName", Hibernate.STRING)
					.addScalar("crm.fileFileName", Hibernate.STRING).list();
			Iterator it = list.iterator();
			DTO dto = null;
			DTO dto1 = null;
			DTO dto2 = null;
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				dto = new DTO();
				dto.setId(row[0]);
				dto.setDocumentType(row[1]);
				dto.setDocumentName(row[2]);
				dto.setFileFileName(row[3]);
				findCportalResourceMgmtDoc.add(dto);
			}
			List stageMajor = this
					.getSession()
					.createSQLQuery(
							"SELECT distinct crm.id,crm.documentType,crm.documentName,crm.fileFileName FROM cportalresourcemgmt crm,serviceorder s where s.billToCode=crm.billToCode and crm.documentType='Valuation'  and crm.billtocode=s.billtocode and crm.billToExcludes not like concat('%',s.billToCode,'%') and crm.corpId='"
									+ sessionCorpID
									+ "' and  s.sequenceNumber= '" + seq + "'")
					.addScalar("crm.id", Hibernate.LONG)
					.addScalar("crm.documentType", Hibernate.STRING)
					.addScalar("crm.documentName", Hibernate.STRING)
					.addScalar("crm.fileFileName", Hibernate.STRING).list();
			if (!stageMajor.isEmpty()) {
				Iterator it1 = stageMajor.iterator();
				while (it1.hasNext()) {
					Object[] row = (Object[]) it1.next();
					dto1 = new DTO();
					dto1.setId(row[0]);
					dto1.setDocumentType(row[1]);
					dto1.setDocumentName(row[2]);
					dto1.setFileFileName(row[3]);
					findCportalResourceMgmtDoc.add(dto1);
				}
			}
			List general = this
					.getSession()
					.createSQLQuery(
							"SELECT distinct crm.id,crm.documentType,crm.documentName,crm.fileFileName FROM cportalresourcemgmt crm,serviceorder s where crm.documentType='General' and (s.billToCode=crm.billToCode or crm.billToCode = '') and (s.originCountry=crm.originCountry or s.destinationCountry=crm.destinationCountry) and s.job=crm.jobType and crm.billToExcludes not like concat('%',s.billToCode,'%') and crm.corpId='"
									+ sessionCorpID
									+ "' and  s.sequenceNumber= '" + seq + "'")
					.addScalar("crm.id", Hibernate.LONG)
					.addScalar("crm.documentType", Hibernate.STRING)
					.addScalar("crm.documentName", Hibernate.STRING)
					.addScalar("crm.fileFileName", Hibernate.STRING).list();
			if (!general.isEmpty()) {
				Iterator it2 = general.iterator();
				while (it2.hasNext()) {
					Object[] row = (Object[]) it2.next();
					dto2 = new DTO();
					dto2.setId(row[0]);
					dto2.setDocumentType(row[1]);
					dto2.setDocumentName(row[2]);
					dto2.setFileFileName(row[3]);
					findCportalResourceMgmtDoc.add(dto2);
				}
			}
			if (usertype.equalsIgnoreCase("USER")) {
				List internal = this
						.getSession()
						.createSQLQuery(
								"SELECT distinct crm.id,crm.documentType,crm.documentName,crm.fileFileName FROM cportalresourcemgmt crm where crm.documentType='Internal' and crm.corpId='"
										+ sessionCorpID + "'")
						.addScalar("crm.id", Hibernate.LONG)
						.addScalar("crm.documentType", Hibernate.STRING)
						.addScalar("crm.documentName", Hibernate.STRING)
						.addScalar("crm.fileFileName", Hibernate.STRING).list();
				if (!internal.isEmpty()) {
					Iterator it2 = internal.iterator();
					while (it2.hasNext()) {
						Object[] row = (Object[]) it2.next();
						dto2 = new DTO();
						dto2.setId(row[0]);
						dto2.setDocumentType(row[1]);
						dto2.setDocumentName(row[2]);
						dto2.setFileFileName(row[3]);
						findCportalResourceMgmtDoc.add(dto2);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return findCportalResourceMgmtDoc;
		}
	
	public List<ServiceOrder> findCustomerResourceMgmtDoc(String seq,String sessionCorpID,String usertype){
		List findCportalResourceMgmtDoc=new ArrayList();
		 try {
			List list = this
					.getSession()
					.createSQLQuery(
							"select distinct crm.id,crm.documentType,crm.documentName,crm.fileFileName from cportalresourcemgmt crm,customerfile c where (c.originCountry=crm.originCountry or c.destinationCountry=crm.destinationCountry ) and c.job=crm.jobType and (c.billToCode=crm.billToCode or crm.billToCode='') and crm.documentType  not in ('Valuation', 'General','Internal') and crm.billToExcludes not like concat('%',c.billToCode,'%') and crm.corpId='"
									+ sessionCorpID
									+ "' and c.sequenceNumber= '" + seq + "'")
					.addScalar("crm.id", Hibernate.LONG)
					.addScalar("crm.documentType", Hibernate.STRING)
					.addScalar("crm.documentName", Hibernate.STRING)
					.addScalar("crm.fileFileName", Hibernate.STRING).list();
			Iterator it = list.iterator();
			DTO dto = null;
			DTO dto1 = null;
			DTO dto2 = null;
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				dto = new DTO();
				dto.setId(row[0]);
				dto.setDocumentType(row[1]);
				dto.setDocumentName(row[2]);
				dto.setFileFileName(row[3]);
				findCportalResourceMgmtDoc.add(dto);
			}
			List stageMajor = this
					.getSession()
					.createSQLQuery(
							"SELECT distinct crm.id,crm.documentType,crm.documentName,crm.fileFileName FROM cportalresourcemgmt crm,customerfile c where c.billToCode=crm.billToCode and crm.documentType='Valuation'  and ( crm.billtocode=c.billtocode or crm.billToCode='' )and crm.billToExcludes not like concat('%',c.billToCode,'%') and crm.corpId='"
									+ sessionCorpID
									+ "' and  c.sequenceNumber= '" + seq + "'")
					.addScalar("crm.id", Hibernate.LONG)
					.addScalar("crm.documentType", Hibernate.STRING)
					.addScalar("crm.documentName", Hibernate.STRING)
					.addScalar("crm.fileFileName", Hibernate.STRING).list();
			if (!stageMajor.isEmpty()) {
				Iterator it1 = stageMajor.iterator();
				while (it1.hasNext()) {
					Object[] row = (Object[]) it1.next();
					dto1 = new DTO();
					dto1.setId(row[0]);
					dto1.setDocumentType(row[1]);
					dto1.setDocumentName(row[2]);
					dto1.setFileFileName(row[3]);
					findCportalResourceMgmtDoc.add(dto1);
				}
			}
			List general = this
					.getSession()
					.createSQLQuery(
							"SELECT distinct crm.id,crm.documentType,crm.documentName,crm.fileFileName FROM cportalresourcemgmt crm,customerfile c where crm.documentType='General' and (c.billToCode=crm.billToCode or crm.billToCode = '') and (c.originCountry=crm.originCountry or c.destinationCountry=crm.destinationCountry ) and c.job=crm.jobType and crm.billToExcludes not like concat('%',c.billToCode,'%') and crm.corpId='"
									+ sessionCorpID
									+ "' and c.sequenceNumber= '" + seq + "'")
					.addScalar("crm.id", Hibernate.LONG)
					.addScalar("crm.documentType", Hibernate.STRING)
					.addScalar("crm.documentName", Hibernate.STRING)
					.addScalar("crm.fileFileName", Hibernate.STRING).list();
			if (!general.isEmpty()) {
				Iterator it2 = general.iterator();
				while (it2.hasNext()) {
					Object[] row = (Object[]) it2.next();
					dto2 = new DTO();
					dto2.setId(row[0]);
					dto2.setDocumentType(row[1]);
					dto2.setDocumentName(row[2]);
					dto2.setFileFileName(row[3]);
					findCportalResourceMgmtDoc.add(dto2);
				}
			}
			if (usertype.equalsIgnoreCase("USER")) {
				List internal = this
						.getSession()
						.createSQLQuery(
								"SELECT distinct crm.id,crm.documentType,crm.documentName,crm.fileFileName FROM cportalresourcemgmt crm where crm.documentType='Internal' and crm.corpId='"
										+ sessionCorpID + "'")
						.addScalar("crm.id", Hibernate.LONG)
						.addScalar("crm.documentType", Hibernate.STRING)
						.addScalar("crm.documentName", Hibernate.STRING)
						.addScalar("crm.fileFileName", Hibernate.STRING).list();
				if (!internal.isEmpty()) {
					Iterator it2 = internal.iterator();
					while (it2.hasNext()) {
						Object[] row = (Object[]) it2.next();
						dto2 = new DTO();
						dto2.setId(row[0]);
						dto2.setDocumentType(row[1]);
						dto2.setDocumentName(row[2]);
						dto2.setFileFileName(row[3]);
						findCportalResourceMgmtDoc.add(dto2);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return findCportalResourceMgmtDoc;
	}
	
	public  class DTO
	 {		
		    private Object shipNumber;
		    private Object mode;
		  	private Object originCityCode;
		  	
			private Object originCity; 
			private Object originCountryCode;
			private Object destinationCityCode; 
			private Object destinationCountryCode;
			private Object statusDate;
			private Object status;
			private Object deliveryShipper;
			private Object deliveryA;
			
			private Object firstname;
			private Object lastName;
			private Object job;
			
			private Object estimateGrossWeight;
			private Object estimatedNetWeight;
			private Object estimateGrossWeightKilo;
			private Object actualNetWeightKilo;
			private Object actualGrossWeight;
			private Object actualNetWeight;
			
			private Object carrierNumber;
			private Object carrierName;
			
			private Object carrierDeparture;
			private Object poeCode;
			
			private Object etDepart;
			private Object atDepart;
			private Object etArrival;
			private Object atArrival;
			
			
			private Object clearCustom;
			private Object clearCustomTA;
			private Object id;
			private Object sid;
			private Object serviceOrderId;
			private Object containerRecivedWH;
			private Object sitOriginYN;
			
			private Object sitOriginOn;
			private Object sitOriginA;
			private Object sitDestinationYN;
			private Object sitDestinationIn;
			private Object sitDestinationA;
			private Object destinationCity;
			private Object idNumber;
			private Object containerNumber;
			private Object carrierArrival;
			private Object originDest;
			private Object lebel;
			private Object target;
			private Object lebel1;
			private Object deliveryShipperDate;
			private Object deliveryADate;
			private Object customsClearance;
			private Object arrivedAtDest;
			private Object arrivedAtDestAct;
			private Object deliveryActual;
			private Object commodity;
			private Object sitOriginYNAct;
			private Object sitOriginYNActDate;
			private Object sitDestinYNAct;
			private Object sitDestinYNActDate;
			private Object beginPacking;
			private Object packA;
			private Object survey;
			private Object surveyAct;
			private Object beginLoad;
			private Object loadA;
			private Object carrierNumberss;
			private Object carrierNamess;
			private Object carrierDeparturess;
			private Object etDepartss;
			private Object atDepartss;
			private Object carrierArrivalss;
			private Object etArrivalss;
			private Object atArrivalss;
			private Object sitOriginMsg;
			private Object sitDestinMsg;
			private Object notesId;
			private Object noteType;
			private Object subject;
			private Object note;
			private Object noteSubType;
			private Object file;
			private Object fileId;
			private Object fileType;
			private Object fileContentType;
			private Object fileFileName;
			private Object location;
			private Object customerNumber;
			private Object description;
			private Object commodityName;
			private Object address1;
			private Object countryName;
			private Object ETA;
			private Object driverName;
			private Object driverCell;
			private Object ETADATE;
			private Object documentType;
			private Object documentName;
			private Object billToCode;
			private Object stageMajor;
			private Object genral;			
			private Object address2;
			private Object address3;
			private Object unit1;
			private Object overAllResponse;
			private Object clientOverallResponse;
			private Object customerFeedback;
			private Object feedBack;
			private Object OAEvaluation;
			private Object DAEvaluation;
			
			
			private Object inventory;
			private Object itemDescription;
			private Object lossComment;
			private Object lossType;
			private Object age;
			private Object originalCost;
			private Object requestedAmount;
			private Object cartonDamaged;
			private Object datePurchased;
			private Object createdBy;
			private Object createdOn;
			public Object getCreatedBy() {
				return createdBy;
			}
			public void setCreatedBy(Object createdBy) {
				this.createdBy = createdBy;
			}
			public Object getCreatedOn() {
				return createdOn;
			}
			public void setCreatedOn(Object createdOn) {
				this.createdOn = createdOn;
			}
			
	
			
			public Object getAge() {
				return age;
			}
			public void setAge(Object age) {
				this.age = age;
			}
			public Object getCartonDamaged() {
				return cartonDamaged;
			}
			public void setCartonDamaged(Object cartonDamaged) {
				this.cartonDamaged = cartonDamaged;
			}
			public Object getInventory() {
				return inventory;
			}
			public void setInventory(Object inventory) {
				this.inventory = inventory;
			}
			public Object getItemDescription() {
				return itemDescription;
			}
			public void setItemDescription(Object itemDescription) {
				this.itemDescription = itemDescription;
			}
			public Object getLossComment() {
				return lossComment;
			}
			public void setLossComment(Object lossComment) {
				this.lossComment = lossComment;
			}
			public Object getLossType() {
				return lossType;
			}
			public void setLossType(Object lossType) {
				this.lossType = lossType;
			}
			public Object getOriginalCost() {
				return originalCost;
			}
			public void setOriginalCost(Object originalCost) {
				this.originalCost = originalCost;
			}
			public Object getRequestedAmount() {
				return requestedAmount;
			}
			public void setRequestedAmount(Object requestedAmount) {
				this.requestedAmount = requestedAmount;
			}
			public Object getDatePurchased() {
				return datePurchased;
			}
			public void setDatePurchased(Object datePurchased) {
				this.datePurchased = datePurchased;
			}
			/**
			 * @return the note
			 */
			public Object getNote() {
				return note;
			}
			/**
			 * @param note the note to set
			 */
			public void setNote(Object note) {
				this.note = note;
			}
			/**
			 * @return the notesId
			 */
			public Object getNotesId() {
				return notesId;
			}
			/**
			 * @param notesId the notesId to set
			 */
			public void setNotesId(Object notesId) {
				this.notesId = notesId;
			}
			/**
			 * @return the noteType
			 */
			public Object getNoteType() {
				return noteType;
			}
			/**
			 * @param noteType the noteType to set
			 */
			public void setNoteType(Object noteType) {
				this.noteType = noteType;
			}
			/**
			 * @return the subject
			 */
			public Object getSubject() {
				return subject;
			}
			/**
			 * @param subject the subject to set
			 */
			public void setSubject(Object subject) {
				this.subject = subject;
			}
			/**
			 * @return the actualGrossWeight
			 */
			public Object getActualGrossWeight() {
				return actualGrossWeight;
			}
			/**
			 * @param actualGrossWeight the actualGrossWeight to set
			 */
			public void setActualGrossWeight(Object actualGrossWeight) {
				this.actualGrossWeight = actualGrossWeight;
			}
			/**
			 * @return the actualNetWeight
			 */
			public Object getActualNetWeight() {
				return actualNetWeight;
			}
			/**
			 * @param actualNetWeight the actualNetWeight to set
			 */
			public void setActualNetWeight(Object actualNetWeight) {
				this.actualNetWeight = actualNetWeight;
			}
			/**
			 * @return the atArrival
			 */
			public Object getAtArrival() {
				return atArrival;
			}
			/**
			 * @param atArrival the atArrival to set
			 */
			public void setAtArrival(Object atArrival) {
				this.atArrival = atArrival;
			}
			/**
			 * @return the atDepart
			 */
			public Object getAtDepart() {
				return atDepart;
			}
			/**
			 * @param atDepart the atDepart to set
			 */
			public void setAtDepart(Object atDepart) {
				this.atDepart = atDepart;
			}
			/**
			 * @return the carrierDeparture
			 */
			public Object getCarrierDeparture() {
				return carrierDeparture;
			}
			/**
			 * @param carrierDeparture the carrierDeparture to set
			 */
			public void setCarrierDeparture(Object carrierDeparture) {
				this.carrierDeparture = carrierDeparture;
			}
			/**
			 * @return the carrierName
			 */
			public Object getCarrierName() {
				return carrierName;
			}
			/**
			 * @param carrierName the carrierName to set
			 */
			public void setCarrierName(Object carrierName) {
				this.carrierName = carrierName;
			}
			/**
			 * @return the carrierNumber
			 */
			public Object getCarrierNumber() {
				return carrierNumber;
			}
			/**
			 * @param carrierNumber the carrierNumber to set
			 */
			public void setCarrierNumber(Object carrierNumber) {
				this.carrierNumber = carrierNumber;
			}
			/**
			 * @return the clearCustom
			 */
			public Object getClearCustom() {
				return clearCustom;
			}
			/**
			 * @param clearCustom the clearCustom to set
			 */
			public void setClearCustom(Object clearCustom) {
				this.clearCustom = clearCustom;
			}
			/**
			 * @return the containerRecivedWH
			 */
			public Object getContainerRecivedWH() {
				return containerRecivedWH;
			}
			/**
			 * @param containerRecivedWH the containerRecivedWH to set
			 */
			public void setContainerRecivedWH(Object containerRecivedWH) {
				this.containerRecivedWH = containerRecivedWH;
			}
			/**
			 * @return the deliveryA
			 */
			public Object getDeliveryA() {
				return deliveryA;
			}
			/**
			 * @param deliveryA the deliveryA to set
			 */
			public void setDeliveryA(Object deliveryA) {
				this.deliveryA = deliveryA;
			}
			/**
			 * @return the deliveryShipper
			 */
			public Object getDeliveryShipper() {
				return deliveryShipper;
			}
			/**
			 * @param deliveryShipper the deliveryShipper to set
			 */
			public void setDeliveryShipper(Object deliveryShipper) {
				this.deliveryShipper = deliveryShipper;
			}
			/**
			 * @return the destinationCityCode
			 */
			public Object getDestinationCityCode() {
				return destinationCityCode;
			}
			/**
			 * @param destinationCityCode the destinationCityCode to set
			 */
			public void setDestinationCityCode(Object destinationCityCode) {
				this.destinationCityCode = destinationCityCode;
			}
			/**
			 * @return the destinationCountryCode
			 */
			public Object getDestinationCountryCode() {
				return destinationCountryCode;
			}
			/**
			 * @param destinationCountryCode the destinationCountryCode to set
			 */
			public void setDestinationCountryCode(Object destinationCountryCode) {
				this.destinationCountryCode = destinationCountryCode;
			}
			/**
			 * @return the estimatedNetWeight
			 */
			public Object getEstimatedNetWeight() {
				return estimatedNetWeight;
			}
			/**
			 * @param estimatedNetWeight the estimatedNetWeight to set
			 */
			public void setEstimatedNetWeight(Object estimatedNetWeight) {
				this.estimatedNetWeight = estimatedNetWeight;
			}
			/**
			 * @return the estimateGrossWeight
			 */
			public Object getEstimateGrossWeight() {
				return estimateGrossWeight;
			}
			/**
			 * @param estimateGrossWeight the estimateGrossWeight to set
			 */
			public void setEstimateGrossWeight(Object estimateGrossWeight) {
				this.estimateGrossWeight = estimateGrossWeight;
			}
			/**
			 * @return the etArrival
			 */
			public Object getEtArrival() {
				return etArrival;
			}
			/**
			 * @param etArrival the etArrival to set
			 */
			public void setEtArrival(Object etArrival) {
				this.etArrival = etArrival;
			}
			/**
			 * @return the etDepart
			 */
			public Object getEtDepart() {
				return etDepart;
			}
			/**
			 * @param etDepart the etDepart to set
			 */
			public void setEtDepart(Object etDepart) {
				this.etDepart = etDepart;
			}
			/**
			 * @return the firstname
			 */
			public Object getFirstname() {
				return firstname;
			}
			/**
			 * @param firstname the firstname to set
			 */
			public void setFirstname(Object firstname) {
				this.firstname = firstname;
			}
			/**
			 * @return the id
			 */
			public Object getId() {
				return id;
			}
			/**
			 * @param id the id to set
			 */
			public void setId(Object id) {
				this.id = id;
			}
			/**
			 * @return the job
			 */
			public Object getJob() {
				return job;
			}
			/**
			 * @param job the job to set
			 */
			public void setJob(Object job) {
				this.job = job;
			}
			/**
			 * @return the lastName
			 */
			public Object getLastName() {
				return lastName;
			}
			/**
			 * @param lastName the lastName to set
			 */
			public void setLastName(Object lastName) {
				this.lastName = lastName;
			}
			/**
			 * @return the mode
			 */
			public Object getMode() {
				return mode;
			}
			/**
			 * @param mode the mode to set
			 */
			public void setMode(Object mode) {
				this.mode = mode;
			}
			/**
			 * @return the originCity
			 */
			public Object getOriginCity() {
				return originCity;
			}
			/**
			 * @param originCity the originCity to set
			 */
			public void setOriginCity(Object originCity) {
				this.originCity = originCity;
			}
			/**
			 * @return the originCityCode
			 */
			public Object getOriginCityCode() {
				return originCityCode;
			}
			/**
			 * @param originCityCode the originCityCode to set
			 */
			public void setOriginCityCode(Object originCityCode) {
				this.originCityCode = originCityCode;
			}
			/**
			 * @return the originCountryCode
			 */
			public Object getOriginCountryCode() {
				return originCountryCode;
			}
			/**
			 * @param originCountryCode the originCountryCode to set
			 */
			public void setOriginCountryCode(Object originCountryCode) {
				this.originCountryCode = originCountryCode;
			}
			/**
			 * @return the poeCode
			 */
			public Object getPoeCode() {
				return poeCode;
			}
			/**
			 * @param poeCode the poeCode to set
			 */
			public void setPoeCode(Object poeCode) {
				this.poeCode = poeCode;
			}
			/**
			 * @return the shipNumber
			 */
			public Object getShipNumber() {
				return shipNumber;
			}
			/**
			 * @param shipNumber the shipNumber to set
			 */
			public void setShipNumber(Object shipNumber) {
				this.shipNumber = shipNumber;
			}
			/**
			 * @return the sid
			 */
			public Object getSid() {
				return sid;
			}
			/**
			 * @param sid the sid to set
			 */
			public void setSid(Object sid) {
				this.sid = sid;
			}
			/**
			 * @return the sitDestinationA
			 */
			public Object getSitDestinationA() {
				return sitDestinationA;
			}
			/**
			 * @param sitDestinationA the sitDestinationA to set
			 */
			public void setSitDestinationA(Object sitDestinationA) {
				this.sitDestinationA = sitDestinationA;
			}
			/**
			 * @return the sitDestinationIn
			 */
			public Object getSitDestinationIn() {
				return sitDestinationIn;
			}
			/**
			 * @param sitDestinationIn the sitDestinationIn to set
			 */
			public void setSitDestinationIn(Object sitDestinationIn) {
				this.sitDestinationIn = sitDestinationIn;
			}
			/**
			 * @return the sitDestinationYN
			 */
			public Object getSitDestinationYN() {
				return sitDestinationYN;
			}
			/**
			 * @param sitDestinationYN the sitDestinationYN to set
			 */
			public void setSitDestinationYN(Object sitDestinationYN) {
				this.sitDestinationYN = sitDestinationYN;
			}
			/**
			 * @return the sitOriginA
			 */
			public Object getSitOriginA() {
				return sitOriginA;
			}
			/**
			 * @param sitOriginA the sitOriginA to set
			 */
			public void setSitOriginA(Object sitOriginA) {
				this.sitOriginA = sitOriginA;
			}
			/**
			 * @return the sitOriginOn
			 */
			public Object getSitOriginOn() {
				return sitOriginOn;
			}
			/**
			 * @param sitOriginOn the sitOriginOn to set
			 */
			public void setSitOriginOn(Object sitOriginOn) {
				this.sitOriginOn = sitOriginOn;
			}
			/**
			 * @return the sitOriginYN
			 */
			public Object getSitOriginYN() {
				return sitOriginYN;
			}
			/**
			 * @param sitOriginYN the sitOriginYN to set
			 */
			public void setSitOriginYN(Object sitOriginYN) {
				this.sitOriginYN = sitOriginYN;
			}
			/**
			 * @return the status
			 */
			public Object getStatus() {
				return status;
			}
			/**
			 * @param status the status to set
			 */
			public void setStatus(Object status) {
				this.status = status;
			}
			/**
			 * @return the statusDate
			 */
			public Object getStatusDate() {
				return statusDate;
			}
			/**
			 * @param statusDate the statusDate to set
			 */
			public void setStatusDate(Object statusDate) {
				this.statusDate = statusDate;
			}
			/**
			 * @return the destinationCounty
			 */
			public Object getDestinationCity() {
				return destinationCity;
			}
			/**
			 * @param destinationCounty the destinationCounty to set
			 */
			public void setDestinationCity(Object destinationCity) {
				this.destinationCity = destinationCity;
			}
			/**
			 * @return the containerNumber
			 */
			public Object getContainerNumber() {
				return containerNumber;
			}
			/**
			 * @param containerNumber the containerNumber to set
			 */
			public void setContainerNumber(Object containerNumber) {
				this.containerNumber = containerNumber;
			}
			/**
			 * @return the idNumber
			 */
			public Object getIdNumber() {
				return idNumber;
			}
			/**
			 * @param idNumber the idNumber to set
			 */
			public void setIdNumber(Object idNumber) {
				this.idNumber = idNumber;
			}
			/**
			 * @return the carrierArrival
			 */
			public Object getCarrierArrival() {
				return carrierArrival;
			}
			/**
			 * @param carrierArrival the carrierArrival to set
			 */
			public void setCarrierArrival(Object carrierArrival) {
				this.carrierArrival = carrierArrival;
			}
			/**
			 * @return the clearCustomTA
			 */
			public Object getClearCustomTA() {
				return clearCustomTA;
			}
			/**
			 * @param clearCustomTA the clearCustomTA to set
			 */
			public void setClearCustomTA(Object clearCustomTA) {
				this.clearCustomTA = clearCustomTA;
			}
			public Object getServiceOrderId() {
				return serviceOrderId;
			}
			public void setServiceOrderId(Object serviceOrderId) {
				this.serviceOrderId = serviceOrderId;
			}
			/**
			 * @return the originDest
			 */
			public Object getOriginDest() {
				return originDest;
			}
			/**
			 * @param originDest the originDest to set
			 */
			public void setOriginDest(Object originDest) {
				this.originDest = originDest;
			}
			/**
			 * @return the lebel
			 */
			public Object getLebel() {
				return lebel;
			}
			/**
			 * @param lebel the lebel to set
			 */
			public void setLebel(Object lebel) {
				this.lebel = lebel;
			}
			/**
			 * @return the target
			 */
			public Object getTarget() {
				return target;
			}
			/**
			 * @param target the target to set
			 */
			public void setTarget(Object target) {
				this.target = target;
			}
			/**
			 * @return the deliveryADate
			 */
			public Object getDeliveryADate() {
				return deliveryADate;
			}
			/**
			 * @param deliveryADate the deliveryADate to set
			 */
			public void setDeliveryADate(Object deliveryADate) {
				this.deliveryADate = deliveryADate;
			}
			/**
			 * @return the deliveryShipperDate
			 */
			public Object getDeliveryShipperDate() {
				return deliveryShipperDate;
			}
			/**
			 * @param deliveryShipperDate the deliveryShipperDate to set
			 */
			public void setDeliveryShipperDate(Object deliveryShipperDate) {
				this.deliveryShipperDate = deliveryShipperDate;
			}
			/**
			 * @return the lebel1
			 */
			public Object getLebel1() {
				return lebel1;
			}
			/**
			 * @param lebel1 the lebel1 to set
			 */
			public void setLebel1(Object lebel1) {
				this.lebel1 = lebel1;
			}
			/**
			 * @return the customsClearance
			 */
			public Object getCustomsClearance() {
				return customsClearance;
			}
			/**
			 * @param customsClearance the customsClearance to set
			 */
			public void setCustomsClearance(Object customsClearance) {
				this.customsClearance = customsClearance;
			}
			/**
			 * @return the arrivedAtDest
			 */
			public Object getArrivedAtDest() {
				return arrivedAtDest;
			}
			/**
			 * @param arrivedAtDest the arrivedAtDest to set
			 */
			public void setArrivedAtDest(Object arrivedAtDest) {
				this.arrivedAtDest = arrivedAtDest;
			}
			/**
			 * @return the arrivedAtDestAct
			 */
			public Object getArrivedAtDestAct() {
				return arrivedAtDestAct;
			}
			/**
			 * @param arrivedAtDestAct the arrivedAtDestAct to set
			 */
			public void setArrivedAtDestAct(Object arrivedAtDestAct) {
				this.arrivedAtDestAct = arrivedAtDestAct;
			}
			/**
			 * @return the deliveryActual
			 */
			public Object getDeliveryActual() {
				return deliveryActual;
			}
			/**
			 * @param deliveryActual the deliveryActual to set
			 */
			public void setDeliveryActual(Object deliveryActual) {
				this.deliveryActual = deliveryActual;
			}
			public Object getCommodity() {
				return commodity;
			}
			public void setCommodity(Object commodity) {
				this.commodity = commodity;
			}
			/**
			 * @return the sitDestinYNAct
			 */
			public Object getSitDestinYNAct() {
				return sitDestinYNAct;
			}
			/**
			 * @param sitDestinYNAct the sitDestinYNAct to set
			 */
			public void setSitDestinYNAct(Object sitDestinYNAct) {
				this.sitDestinYNAct = sitDestinYNAct;
			}
			/**
			 * @return the sitDestinYNActDate
			 */
			public Object getSitDestinYNActDate() {
				return sitDestinYNActDate;
			}
			/**
			 * @param sitDestinYNActDate the sitDestinYNActDate to set
			 */
			public void setSitDestinYNActDate(Object sitDestinYNActDate) {
				this.sitDestinYNActDate = sitDestinYNActDate;
			}
			/**
			 * @return the sitOriginYNAct
			 */
			public Object getSitOriginYNAct() {
				return sitOriginYNAct;
			}
			/**
			 * @param sitOriginYNAct the sitOriginYNAct to set
			 */
			public void setSitOriginYNAct(Object sitOriginYNAct) {
				this.sitOriginYNAct = sitOriginYNAct;
			}
			/**
			 * @return the sitOriginYNActDate
			 */
			public Object getSitOriginYNActDate() {
				return sitOriginYNActDate;
			}
			/**
			 * @param sitOriginYNActDate the sitOriginYNActDate to set
			 */
			public void setSitOriginYNActDate(Object sitOriginYNActDate) {
				this.sitOriginYNActDate = sitOriginYNActDate;
			}
			/**
			 * @return the beginPacking
			 */
			public Object getBeginPacking() {
				return beginPacking;
			}
			/**
			 * @param beginPacking the beginPacking to set
			 */
			public void setBeginPacking(Object beginPacking) {
				this.beginPacking = beginPacking;
			}
			/**
			 * @return the packA
			 */
			public Object getPackA() {
				return packA;
			}
			/**
			 * @param packA the packA to set
			 */
			public void setPackA(Object packA) {
				this.packA = packA;
			}
			/**
			 * @return the surveyAct
			 */
			public Object getSurveyAct() {
				return surveyAct;
			}
			/**
			 * @param surveyAct the surveyAct to set
			 */
			public void setSurveyAct(Object surveyAct) {
				this.surveyAct = surveyAct;
			}
			/**
			 * @return the surveyDate
			 */
			public Object getSurvey() {
				return survey;
			}
			/**
			 * @param surveyDate the surveyDate to set
			 */
			public void setSurvey(Object survey) {
				this.survey = survey;
			}
			/**
			 * @return the beginLoad
			 */
			public Object getBeginLoad() {
				return beginLoad;
			}
			/**
			 * @param beginLoad the beginLoad to set
			 */
			public void setBeginLoad(Object beginLoad) {
				this.beginLoad = beginLoad;
			}
			/**
			 * @return the loadA
			 */
			public Object getLoadA() {
				return loadA;
			}
			/**
			 * @param loadA the loadA to set
			 */
			public void setLoadA(Object loadA) {
				this.loadA = loadA;
			}
			/**
			 * @return the atArrivalss
			 */
			public Object getAtArrivalss() {
				return atArrivalss;
			}
			/**
			 * @param atArrivalss the atArrivalss to set
			 */
			public void setAtArrivalss(Object atArrivalss) {
				this.atArrivalss = atArrivalss;
			}
			/**
			 * @return the atDepartss
			 */
			public Object getAtDepartss() {
				return atDepartss;
			}
			/**
			 * @param atDepartss the atDepartss to set
			 */
			public void setAtDepartss(Object atDepartss) {
				this.atDepartss = atDepartss;
			}
			/**
			 * @return the carrierArrivalss
			 */
			public Object getCarrierArrivalss() {
				return carrierArrivalss;
			}
			/**
			 * @param carrierArrivalss the carrierArrivalss to set
			 */
			public void setCarrierArrivalss(Object carrierArrivalss) {
				this.carrierArrivalss = carrierArrivalss;
			}
			/**
			 * @return the carrierDeparturess
			 */
			public Object getCarrierDeparturess() {
				return carrierDeparturess;
			}
			/**
			 * @param carrierDeparturess the carrierDeparturess to set
			 */
			public void setCarrierDeparturess(Object carrierDeparturess) {
				this.carrierDeparturess = carrierDeparturess;
			}
			/**
			 * @return the carrierNamess
			 */
			public Object getCarrierNamess() {
				return carrierNamess;
			}
			/**
			 * @param carrierNamess the carrierNamess to set
			 */
			public void setCarrierNamess(Object carrierNamess) {
				this.carrierNamess = carrierNamess;
			}
			/**
			 * @return the carrierNumberss
			 */
			public Object getCarrierNumberss() {
				return carrierNumberss;
			}
			/**
			 * @param carrierNumberss the carrierNumberss to set
			 */
			public void setCarrierNumberss(Object carrierNumberss) {
				this.carrierNumberss = carrierNumberss;
			}
			/**
			 * @return the etArrivalss
			 */
			public Object getEtArrivalss() {
				return etArrivalss;
			}
			/**
			 * @param etArrivalss the etArrivalss to set
			 */
			public void setEtArrivalss(Object etArrivalss) {
				this.etArrivalss = etArrivalss;
			}
			/**
			 * @return the etDepartss
			 */
			public Object getEtDepartss() {
				return etDepartss;
			}
			/**
			 * @param etDepartss the etDepartss to set
			 */
			public void setEtDepartss(Object etDepartss) {
				this.etDepartss = etDepartss;
			}
			public Object getSitDestinMsg() {
				return sitDestinMsg;
			}
			public void setSitDestinMsg(Object sitDestinMsg) {
				this.sitDestinMsg = sitDestinMsg;
			}
			public Object getSitOriginMsg() {
				return sitOriginMsg;
			}
			public void setSitOriginMsg(Object sitOriginMsg) {
				this.sitOriginMsg = sitOriginMsg;
			}
			/**
			 * @return the noteSubType
			 */
			public Object getNoteSubType() {
				return noteSubType;
			}
			/**
			 * @param noteSubType the noteSubType to set
			 */
			public void setNoteSubType(Object noteSubType) {
				this.noteSubType = noteSubType;
			}
			/**
			 * @return the customerNumber
			 */
			public Object getCustomerNumber() {
				return customerNumber;
			}
			/**
			 * @param customerNumber the customerNumber to set
			 */
			public void setCustomerNumber(Object customerNumber) {
				this.customerNumber = customerNumber;
			}
			/**
			 * @return the description
			 */
			public Object getDescription() {
				return description;
			}
			/**
			 * @param description the description to set
			 */
			public void setDescription(Object description) {
				this.description = description;
			}
			/**
			 * @return the file
			 */
			public Object getFile() {
				return file;
			}
			/**
			 * @param file the file to set
			 */
			public void setFile(Object file) {
				this.file = file;
			}
			/**
			 * @return the fileContentType
			 */
			public Object getFileContentType() {
				return fileContentType;
			}
			/**
			 * @param fileContentType the fileContentType to set
			 */
			public void setFileContentType(Object fileContentType) {
				this.fileContentType = fileContentType;
			}
			/**
			 * @return the fileFileName
			 */
			public Object getFileFileName() {
				return fileFileName;
			}
			/**
			 * @param fileFileName the fileFileName to set
			 */
			public void setFileFileName(Object fileFileName) {
				this.fileFileName = fileFileName;
			}
			/**
			 * @return the fileId
			 */
			public Object getFileId() {
				return fileId;
			}
			/**
			 * @param fileId the fileId to set
			 */
			public void setFileId(Object fileId) {
				this.fileId = fileId;
			}
			/**
			 * @return the fileType
			 */
			public Object getFileType() {
				return fileType;
			}
			/**
			 * @param fileType the fileType to set
			 */
			public void setFileType(Object fileType) {
				this.fileType = fileType;
			}
			/**
			 * @return the location
			 */
			public Object getLocation() {
				return location;
			}
			/**
			 * @param location the location to set
			 */
			public void setLocation(Object location) {
				this.location = location;
			}
			/**
			 * @return the commodityName
			 */
			public Object getCommodityName() {
				return commodityName;
			}
			/**
			 * @param commodityName the commodityName to set
			 */
			public void setCommodityName(Object commodityName) {
				this.commodityName = commodityName;
			}
			/**
			 * @return the address1
			 */
			public Object getAddress1() {
				return address1;
			}
			/**
			 * @param address1 the address1 to set
			 */
			public void setAddress1(Object address1) {
				this.address1 = address1;
			}
			/**
			 * @return the countryName
			 */
			public Object getCountryName() {
				return countryName;
			}
			/**
			 * @param countryName the countryName to set
			 */
			public void setCountryName(Object countryName) {
				this.countryName = countryName;
			}
			/**
			 * @return the eTA
			 */
			public Object getETA() {
				return ETA;
			}
			/**
			 * @param eta the eTA to set
			 */
			public void setETA(Object eta) {
				ETA = eta;
			}
			/**
			 * @return the driverCell
			 */
			public Object getDriverCell() {
				return driverCell;
			}
			/**
			 * @param driverCell the driverCell to set
			 */
			public void setDriverCell(Object driverCell) {
				this.driverCell = driverCell;
			}
			/**
			 * @return the driverName
			 */
			public Object getDriverName() {
				return driverName;
			}
			/**
			 * @param driverName the driverName to set
			 */
			public void setDriverName(Object driverName) {
				this.driverName = driverName;
			}
			/**
			 * @return the eTADATE
			 */
			public Object getETADATE() {
				return ETADATE;
			}
			/**
			 * @param etadate the eTADATE to set
			 */
			public void setETADATE(Object etadate) {
				ETADATE = etadate;
			}
			/**
			 * @return the documentName
			 */
			public Object getDocumentName() {
				return documentName;
			}
			/**
			 * @param documentName the documentName to set
			 */
			public void setDocumentName(Object documentName) {
				this.documentName = documentName;
			}
			/**
			 * @return the documentType
			 */
			public Object getDocumentType() {
				return documentType;
			}
			/**
			 * @param documentType the documentType to set
			 */
			public void setDocumentType(Object documentType) {
				this.documentType = documentType;
			}
			/**
			 * @return the billToCode
			 */
			public Object getBillToCode() {
				return billToCode;
			}
			/**
			 * @param billToCode the billToCode to set
			 */
			public void setBillToCode(Object billToCode) {
				this.billToCode = billToCode;
			}
			/**
			 * @return the stageMajor
			 */
			public Object getStageMajor() {
				return stageMajor;
			}
			/**
			 * @param stageMajor the stageMajor to set
			 */
			public void setStageMajor(Object stageMajor) {
				this.stageMajor = stageMajor;
			}
			/**
			 * @return the genral
			 */
			public Object getGenral() {
				return genral;
			}
			/**
			 * @param genral the genral to set
			 */
			public void setGenral(Object genral) {
				this.genral = genral;
			}
			/**
			 * @return the address2
			 */
			public Object getAddress2() {
				return address2;
			}
			/**
			 * @param address2 the address2 to set
			 */
			public void setAddress2(Object address2) {
				this.address2 = address2;
			}
			/**
			 * @return the address3
			 */
			public Object getAddress3() {
				return address3;
			}
			/**
			 * @param address3 the address3 to set
			 */
			public void setAddress3(Object address3) {
				this.address3 = address3;
			}
			/**
			 * @return the unit1
			 */
			public Object getUnit1() {
				return unit1;
			}
			/**
			 * @param unit1 the unit1 to set
			 */
			public void setUnit1(Object unit1) {
				this.unit1 = unit1;
			}
			/**
			 * @return the actualNetWeightKilo
			 */
			public Object getActualNetWeightKilo() {
				return actualNetWeightKilo;
			}
			/**
			 * @param actualNetWeightKilo the actualNetWeightKilo to set
			 */
			public void setActualNetWeightKilo(Object actualNetWeightKilo) {
				this.actualNetWeightKilo = actualNetWeightKilo;
			}
			/**
			 * @return the estimateGrossWeightKilo
			 */
			public Object getEstimateGrossWeightKilo() {
				return estimateGrossWeightKilo;
			}
			/**
			 * @param estimateGrossWeightKilo the estimateGrossWeightKilo to set
			 */
			public void setEstimateGrossWeightKilo(Object estimateGrossWeightKilo) {
				this.estimateGrossWeightKilo = estimateGrossWeightKilo;
			}
			/**
			 * @return the overAllResponse
			 */
			public Object getOverAllResponse() {
				return overAllResponse;
			}
			/**
			 * @param overAllResponse the overAllResponse to set
			 */
			public void setOverAllResponse(Object overAllResponse) {
				this.overAllResponse = overAllResponse;
			}
			/**
			 * @return the clientOverallResponse
			 */
			public Object getClientOverallResponse() {
				return clientOverallResponse;
			}
			/**
			 * @param clientOverallResponse the clientOverallResponse to set
			 */
			public void setClientOverallResponse(Object clientOverallResponse) {
				this.clientOverallResponse = clientOverallResponse;
			}
			/**
			 * @return the customerFeedback
			 */
			public Object getCustomerFeedback() {
				return customerFeedback;
			}
			/**
			 * @param customerFeedback the customerFeedback to set
			 */
			public void setCustomerFeedback(Object customerFeedback) {
				this.customerFeedback = customerFeedback;
			}
			/**
			 * @return the feedBack
			 */
			public Object getFeedBack() {
				return feedBack;
			}
			/**
			 * @param feedBack the feedBack to set
			 */
			public void setFeedBack(Object feedBack) {
				this.feedBack = feedBack;
			}
			public Object getDAEvaluation() {
				return DAEvaluation;
			}
			public void setDAEvaluation(Object evaluation) {
				DAEvaluation = evaluation;
			}
			public Object getOAEvaluation() {
				return OAEvaluation;
			}
			public void setOAEvaluation(Object evaluation) {
				OAEvaluation = evaluation;
			}
			
			
	 }

	public Boolean isNetworkedRecord(Long customerFileId) {
		Boolean netRecord=false;
		try {
			List test = getHibernateTemplate().find(
					"from CustomerFile where isNetworkRecord is true and id="
							+ customerFileId + "");
			if (!test.isEmpty()) {
				netRecord = true;
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return netRecord;
	}

	public List findResponsiblePerson(String custJobType, String companyDivision, String sessionCorpID) {
		try {
			String query = "select CONCAT(if(personBilling is null or personBilling='',' ',personBilling)" +
					",'#',if(personAuditor is null or personAuditor='',' ',personAuditor)" +
					",'#',if(personPricing is null or personPricing='',' ',personPricing)" +
					",'#',if(personPayable is null or personPayable='',' ',personPayable)" +
					",'#',if(coordinator is null or coordinator='',' ',coordinator)" +
					",'#',if(estimator is null or estimator='',' ',estimator)" +
					",'#',if(internalBillingPerson is null or internalBillingPerson='',' ',internalBillingPerson))" +
					" from refjobtype where job = '"
					+ custJobType
					+ "' and companyDivision='"
					+ companyDivision + "' and corpId='"+sessionCorpID+"'";
			return this.getSession().createSQLQuery(query).list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

public class UniversalSearchDTO
{
	private Object type;
    private Object sequencenumber;
  	private Object status;  	
  	private Object firstname;
  	private Object lastname;
  	private Object job;
  	private Object billtoname;
  	private Object coordinator;
  	private Object origincountry;
  	private Object origincity;
  	private Object destinationcountry;
  	private Object destinationcity;
  	private Object createdon;
  	private Object createdby;
  	private Object node;
  	private Object companydivision;
  	private Object registrationnumber;
  	private Object id;
  	private Object personPricing;
  	private Object quotationStatus;
  	private Object email;
  	private Object accountName;
  	private Object moveType;
  	
	public Object getType() {
		return type;
	}
	public void setType(Object type) {
		this.type = type;
	}
	public Object getSequencenumber() {
		return sequencenumber;
	}
	public void setSequencenumber(Object sequencenumber) {
		this.sequencenumber = sequencenumber;
	}
	public Object getStatus() {
		return status;
	}
	public void setStatus(Object status) {
		this.status = status;
	}
	public Object getFirstname() {
		return firstname;
	}
	public void setFirstname(Object firstname) {
		this.firstname = firstname;
	}
	public Object getLastname() {
		return lastname;
	}
	public void setLastname(Object lastname) {
		this.lastname = lastname;
	}
	public Object getJob() {
		return job;
	}
	public void setJob(Object job) {
		this.job = job;
	}
	public Object getBilltoname() {
		return billtoname;
	}
	public void setBilltoname(Object billtoname) {
		this.billtoname = billtoname;
	}
	public Object getCoordinator() {
		return coordinator;
	}
	public void setCoordinator(Object coordinator) {
		this.coordinator = coordinator;
	}
	public Object getOrigincountry() {
		return origincountry;
	}
	public void setOrigincountry(Object origincountry) {
		this.origincountry = origincountry;
	}
	public Object getOrigincity() {
		return origincity;
	}
	public void setOrigincity(Object origincity) {
		this.origincity = origincity;
	}
	public Object getDestinationcountry() {
		return destinationcountry;
	}
	public void setDestinationcountry(Object destinationcountry) {
		this.destinationcountry = destinationcountry;
	}
	public Object getDestinationcity() {
		return destinationcity;
	}
	public void setDestinationcity(Object destinationcity) {
		this.destinationcity = destinationcity;
	}
	public Object getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Object createdon) {
		this.createdon = createdon;
	}
	public Object getCreatedby() {
		return createdby;
	}
	public void setCreatedby(Object createdby) {
		this.createdby = createdby;
	}
	public Object getNode() {
		return node;
	}
	public void setNode(Object node) {
		this.node = node;
	}
	public Object getCompanydivision() {
		return companydivision;
	}
	public void setCompanydivision(Object companydivision) {
		this.companydivision = companydivision;
	}
	public Object getRegistrationnumber() {
		return registrationnumber;
	}
	public void setRegistrationnumber(Object registrationnumber) {
		this.registrationnumber = registrationnumber;
	}
	public Object getId() {
		return id;
	}
	public void setId(Object id) {
		this.id = id;
	}
	public Object getPersonPricing() {
		return personPricing;
	}
	public void setPersonPricing(Object personPricing) {
		this.personPricing = personPricing;
	}
	public Object getQuotationStatus() {
		return quotationStatus;
	}
	public void setQuotationStatus(Object quotationStatus) {
		this.quotationStatus = quotationStatus;
	}
	public Object getEmail() {
		return email;
	}
	public void setEmail(Object email) {
		this.email = email;
	}
	public Object getAccountName() {
		return accountName;
	}
	public void setAccountName(Object accountName) {
		this.accountName = accountName;
	}
	public Object getMoveType() {
		return moveType;
	}
	public void setMoveType(Object moveType) {
		this.moveType = moveType;
	}
	
}

public List universalSearch(String firstName, String lastName, String sessionCorpID, String sequenceNumber, String billToName, String job, String coordinator, String companyDivision, String registrationNumber, Boolean activeStatus, String status, String personPricing, String serviceOrderSearchVal, String usertype, String email,String accountName,String cityCountryZipOption, String originCityOrZip,  String destinationCityOrZip, String originCountry, String destinationCountry) {
	List <Object> universalSearchList = new ArrayList<Object>();
	try {
		String registrationNumExact = registrationNumber;
		String partnerSetCode = "";
		String companydivisionSetCode="";
		String bookingAgentSet="";
		String flag = "0";
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        Map filterMap=user.getFilterMap(); 
		if (filterMap!=null && (!(filterMap.isEmpty()))) {
			List list1 = new ArrayList();
			List list2 = new ArrayList();
			List list3 = new ArrayList();

			if(filterMap.containsKey("serviceOrderCompanyDivisionFilter")){
	    		List partnerCorpIDList = new ArrayList(); 
					HashMap valueMap=	(HashMap)filterMap.get("serviceOrderCompanyDivisionFilter");
					if(valueMap!=null){
						Iterator mapIterator = valueMap.entrySet().iterator();
						while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							String key = (String) entry.getKey(); 
						    partnerCorpIDList= (List)entry.getValue(); 
						}
					}
					
	    	Iterator listIterator= partnerCorpIDList.iterator();
	    	while (listIterator.hasNext()) {
	    		list2.add("'" + listIterator.next().toString() + "'");	
	    	}
	    	companydivisionSetCode= list2.toString().replace("[","").replace("]", "");
	    	}
	    	if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
	    		List partnerCorpIDList = new ArrayList(); 
					HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
					if(valueMap!=null){
						Iterator mapIterator = valueMap.entrySet().iterator();
						while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							String key = (String) entry.getKey(); 
						    partnerCorpIDList= (List)entry.getValue(); 
						}
					}
					
	    	Iterator listIterator= partnerCorpIDList.iterator();
	    	while (listIterator.hasNext()) {
	    		String code=listIterator.next().toString();
	    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
	    		list1.add("'" + code + "'");	
	    		}
	    	}
	    	partnerSetCode = list1.toString().replace("[","").replace("]", "");
	    	}

	    	if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
	    		List partnerCorpIDList = new ArrayList(); 
					HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
					if(valueMap!=null){
						Iterator mapIterator = valueMap.entrySet().iterator();
						while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							String key = (String) entry.getKey(); 
						    partnerCorpIDList= (List)entry.getValue(); 
						}
					}
					
	    	Iterator listIterator= partnerCorpIDList.iterator();
	    	while (listIterator.hasNext()) {
	    		String code=listIterator.next().toString();
	    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
	    		list3.add("'" + code + "'");	
	    		}
	    	}
	    	bookingAgentSet = list3.toString().replace("[","").replace("]", "");
	    	}
		}
		List list = new ArrayList();
		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
		if (sequenceNumber == null) {
			sequenceNumber = "";
		}
		if (registrationNumber == null) {
			registrationNumber = "";
		} else {
			registrationNumber = CommonUtil.getRegistrationNum(
					registrationNumber, serviceOrderSearchVal);
		}
		if (billToName == null) {
			billToName = "";
		}
		if (job == null) {
			job = "";
		}
		if (coordinator == null) {
			coordinator = "";
		}
		if (companyDivision == null) {
			companyDivision = "";
		}
		if (personPricing == null) {
			personPricing = "";
		}
		if (email == null) {
			email = "";
		}
		if(accountName == null){
			accountName = "";
		}
		if(originCountry == null){
			originCountry = "";
		}
		if(destinationCountry == null){
			destinationCountry = "";
		}
		String originZip = "";
		String destinationZip = "";
		String originCity = "";
		String destinationCity = "";
		if((cityCountryZipOption!=null && !cityCountryZipOption.equalsIgnoreCase("")) && (cityCountryZipOption.equalsIgnoreCase("Zip"))){
			originZip = CommonUtil.getOriginZip(originCityOrZip,serviceOrderSearchVal);
			destinationZip = CommonUtil.getDestinationZip(destinationCityOrZip,serviceOrderSearchVal);
		}else if((cityCountryZipOption!=null && !cityCountryZipOption.equalsIgnoreCase("")) && (cityCountryZipOption.equalsIgnoreCase("City"))){
			originCity = originCityOrZip;
			destinationCity = destinationCityOrZip;
		}
		String query = null;
		if (activeStatus == true) {
			if (registrationNumber.equalsIgnoreCase("")) {
				if (serviceOrderSearchVal.equalsIgnoreCase("Start With")) {

					query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					if ((status == null || status.equals(""))) {
						query = query
								+ " and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status ='" + status + "') and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}
					if ((job != null && !job.equals(""))) {
						query = query + "and ( job ='" + job + "') ";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
					}
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator = '" + coordinator
								+ "') ";
						flag = "1";
					}
					if ((sequenceNumber != null && !sequenceNumber.equals(""))) {

						query = query
								+ "and sequencenumber like '"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%'";
						flag = "1";
					}

					if ((lastName != null && !lastName.equals(""))) {
						query = query
								+ "and ( lastName like '"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (personpricing like '"
								+ personPricing
								+ "%' or personpricing is null)";
						flag = "1";
					}

					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName like '"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and sequencenumber like '"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%' ";
						flag = "1";
					}

					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname like '"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					if ((job != null && !job.equals(""))) {
						query = query + " and ( job like '" + job + "%') ";
						flag = "1";
					}
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") ";
						flag = "1";
					}
					
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision like '"
								+ companyDivision + "%') ";
						flag = "1";
					}
					
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator like '"
								+ coordinator + "%') ";
						flag = "1";
					}
					
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status like '" + status
								+ "%') ";
						flag = "1";
					}
					
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity like '"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity like '"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') ";
						flag = "1";
					}
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip like '"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip like '"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query
								+ " and ( originCountry like '"
								+ originCountry.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query
								+ " and ( destinationCountry like '"
								+ destinationCountry.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') ";
						flag = "1";
					}
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName like '"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
						query = query + " and ( email like '" + email
								+ "%' or destinationEmail like '" + email + "%') order by controlflag, firstname";
						flag = "1";
					}
					if (flag != null && flag.equals("0")) {

						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ "where corpid = '"
								+ sessionCorpID
								+ "' and (status is null or status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') and (lastName is null or lastName like '"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (personpricing like '"
								+ personPricing
								+ "%' or personpricing is null)"
								+ "and (firstName is null or firstName like '"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and sequencenumber like '"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%' and (billtoname is null or billtoname like '"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and accountName is null or accountName like '"
								+ accountName.trim().replaceAll("'", "''").replaceAll(":", "''")
								+"%')  and (job is null or job like '%"
								+ job
								+ "%')";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") ";
							 
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						} 
						query = query
								+ " and (companydivision is null or companydivision like '%"
								+ companyDivision
								+ "%') and (coordinator is null or coordinator like '%"
								+ coordinator
								+ "%') and (status is null or status like '%"
								+ status
								+ "%') and (email is null or email like '%"
								+ email
								+ "%' or destinationEmail is null or destinationEmail like '%" + email + "%') ";
						if ((originCity != null && !originCity.equals(""))) {
							query = query+ " and (originCity is null or originCity like '%"+ originCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query+ " and (destinationCity is null or destinationCity like '%"+ destinationCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query+ " and (originZip is null or originZip like '%"+ originZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query+ " and (destinationZip is null or destinationZip like '%"+ destinationZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and (originCountry is null or originCountry like '%"+ originCountry.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						query = query+ " order by controlflag, firstname ";

					}

				} else if (serviceOrderSearchVal.equalsIgnoreCase("End With")) {

					query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					if ((status.equalsIgnoreCase("") || status==null)) {
						query = query
								+ " and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status ='" + status + "') and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}
					if ((job != null && !job.equals(""))) {
						query = query + "and ( job ='" + job + "') ";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
					}
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator = '" + coordinator
								+ "') ";
						flag = "1";
					}
					if ((sequenceNumber != null && !sequenceNumber.equals(""))) {
						query = query
								+ "and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
						flag = "1";
					}
					
					if ((lastName != null && !lastName.equals(""))) {
						query = query
								+ "and ( lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (personpricing like '%"
								+ personPricing + "' or personpricing is null)";
						flag = "1";
					}
					
					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "' ";
						flag = "1";
					}
					
					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((job != null && !job.equals(""))) {
						query = query + " and ( job like '%" + job + "') ";
						flag = "1";
					}
					
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") ";
						flag = "1";
						 
					}
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision like '%"
								+ companyDivision + "') ";
						flag = "1";
					}
					
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator like '%"
								+ coordinator + "') ";
						flag = "1";
					}
					
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status like '%" + status
								+ "') ";
						flag = "1";
					}
					
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity like '%"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity like '%"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "') ";
						flag = "1";
					}
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip like '%"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip like '%"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query
								+ " and ( originCountry like '%"
								+ originCountry.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query
								+ " and ( destinationCountry like '%"
								+ destinationCountry.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "') ";
						flag = "1";
					}
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
						query = query + " and ( email like '%" + email
								+ "' or destinationEmail like '%" + email + "') order by controlflag, firstname ";
						flag = "1";
					}
					if (flag != null && flag.equals("0")) {

						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ "where corpid = '"
								+ sessionCorpID
								+ "' and (status is null or status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') and (lastName is null or lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (personpricing like '%"
								+ personPricing
								+ "' or personpricing is null)"
								+ "and (firstName is null or firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "' and (billtoname is null or billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (accountName is null or accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (job is null or job like '%"
								+ job
								+ "%')";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") ";
							
							 
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						} 
						query = query
								+ " and (companydivision is null or companydivision like '%"
								+ companyDivision
								+ "%') and (coordinator is null or coordinator like '%"
								+ coordinator
								+ "%') and (status is null or status like '%"
								+ status
								+ "%') and (email is null or email like '%"
								+ email
								+ "%' or destinationEmail is null or destinationEmail like '%" + email+ "%') ";
						if ((originCity != null && !originCity.equals(""))) {
							query = query+ " and (originCity is null or originCity like '%"+ originCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query+ " and (destinationCity is null or destinationCity like '%"+ destinationCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query+ " and (originZip is null or originZip like '%"+ originZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query+ " and (destinationZip is null or destinationZip like '%"+ destinationZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and (originCountry is null or originCountry like '%"+ originCountry.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						query = query +" order by controlflag, firstname ";
						
					}

				} else if (serviceOrderSearchVal
						.equalsIgnoreCase("Exact Match")) {

					query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					if ((status.equalsIgnoreCase("") || status==null)) {
						query = query
								+ " and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}  
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status ='" + status + "') and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}
					if ((lastName != null && !lastName.equals(""))) {
						
						query = query
						+ "and ( lastName ='"
						+ lastName.trim().replaceAll("'", "''")
								.replaceAll(":", "''")
						+ "') ";
						if(personPricing != null && !personPricing.equals("")){
							query = query
							+ "and personpricing ='"
							+ personPricing.trim().replaceAll("'", "''")
									.replaceAll(":", "''") + "'";
						}
						flag = "1";
					}
					
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status ='" + status + "')";
						flag = "1";
					}
					if ((job != null && !job.equals(""))) {
						query = query + "and ( job ='" + job + "') ";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
					}
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator = '" + coordinator
								+ "') ";
						flag = "1";
					}
					
					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName = '"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "')  ";
						flag = "1";
					}
					if ((sequenceNumber != null && !sequenceNumber.equals(""))) {
						query = query
								+ "and sequencenumber = '"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
						flag = "1";
					}
					
					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname = '"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") ";
						flag = "1";
						 
					}
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision = '"
								+ companyDivision + "') ";
						flag = "1";
					}
					
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator = '" + coordinator
								+ "') ";
						flag = "1";
					}
				
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity = '"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity = '"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') ";
						flag = "1";
					}
					
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip = '"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip = '"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query
								+ " and ( originCountry = '"
								+ originCountry.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query
								+ " and ( destinationCountry = '"
								+ destinationCountry.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') ";
						flag = "1";
					}
					
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName = '"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
						query = query + " and ( email like '" + email
								+ "' or destinationEmail like '" + email + "') order by controlflag, firstname ";
						flag = "1";
					}
					if (flag != null && flag.equals("0")) {
						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ " where corpid = '"
								+ sessionCorpID
								+ "' and (status is null or status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";

						query = query
								+ " and (lastName is null or lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and personpricing = '"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "' ";
						query = query
								+ " and (firstName is null or firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%' ";
						query = query
								+ " and (billtoname is null or billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and (accountName is null or accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and (email is null or email like '%"
								+ email.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%' or destinationEmail like '%" + email.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%')";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") ";
							 
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						}
						if ((originCity != null && !originCity.equals(""))) {
							query = query+ " and (originCity is null or originCity like '%"+ originCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query+ " and (destinationCity is null or destinationCity like '%"+ destinationCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query+ " and (originZip is null or originZip like '%"+ originZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query+ " and (destinationZip is null or destinationZip like '%"+ destinationZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and (originCountry is null or originCountry like '%"+ originCountry+ "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry+ "%') ";
						}
						query = query + " order by controlflag, firstname";

					}

				} else {
						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					if ((status.equalsIgnoreCase("") || status==null)) {
						query = query
								+ " and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled')";
						flag = "1";
					}
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status ='" + status + "') and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}
					if ((job != null && !job.equals(""))) {
						query = query + "and ( job ='" + job + "') ";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
					}
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator = '" + coordinator
								+ "') ";
						flag = "1";
					}
					if ((sequenceNumber != null && !sequenceNumber.equals(""))) {
						query = query
								+ "and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%'";
						flag = "1";
					}
					
					if ((lastName != null && !lastName.equals(""))) {
						query = query
								+ "and ( lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (personpricing like '%"
								+ personPricing
								+ "%' or personpricing is null)";
						flag = "1";
					}
					
					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%' ";
						flag = "1";
					}
					
					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((job != null && !job.equals(""))) {
						query = query + " and ( job like '%" + job + "%') ";
						flag = "1";
					}
				
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") ";
						flag = "1";
						 
					}
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision like '%"
								+ companyDivision + "%') ";
						flag = "1";
					}
					
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator like '%"
								+ coordinator + "%') ";
						flag = "1";
					}
					
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status like '%" + status
								+ "%') ";
						flag = "1";
					}
					
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity like '%"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity like '%"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') ";
						flag = "1";
					}
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip like '%"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip like '%"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query
								+ " and ( originCountry like '%"+ originCountry + "%') ";
						flag = "1";
					}
					
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query
								+ " and ( destinationCountry like '%"+ destinationCountry+ "%') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
						query = query + " and ( email like '%" + email
								+ "%' or destinationEmail like '%" + email+ "%') order by controlflag, firstname ";
						flag = "1";
					}
					if (flag != null && flag.equals("0")) {

						
						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ "where corpid = '"
								+ sessionCorpID
								+ "' and (status is null or status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') and (lastName is null or lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (personpricing like '%"
								+ personPricing
								+ "%' or personpricing is null)"
								+ "and (firstName is null or firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%' and (billtoname is null or billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
								.replaceAll(":", "''")
								+ "%') and (accountName is null or accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
								.replaceAll(":", "''")
								+ "%') and (job is null or job like '%"
								+ job
								+ "%')";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") ";
							
							 
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						} 
						query = query
								+ " and ( companydivision is null or companydivision like '%"
								+ companyDivision
								+ "%') and (coordinator is null or coordinator like '%"
								+ coordinator
								+ "%') and (status is null or status like '%"
								+ status
								+ "%') and (email is null or email like '%"
								+ email
								+ "%' or destinationEmail is null or destinationEmail like '%" + email+ "%') ";
						if ((originCity != null && !originCity.equals(""))) {
							query = query+ " and (originCity is null or originCity like '%"+ originCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query+ " and (destinationCity is null or destinationCity like '%"+ destinationCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query+ " and (originZip is null or originZip like '%"+ originZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query+ " and (destinationZip is null or destinationZip like '%"+ destinationZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and (originCountry is null or originCountry like '%"+ originCountry+ "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry+ "%') ";
						}
						query = query + " order by controlflag, firstname";
					}
					

				}
			} else {
				if (serviceOrderSearchVal.equalsIgnoreCase("Start With")) {
					query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					if ((status == null && status.equals(""))) {
						query = query
								+ " and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}
					
					if ((lastName != null && !lastName.equals(""))) {
						query = query
								+ "and ( lastName like '"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (personpricing like '"
								+ personPricing
								+ "%' or personpricing is null)";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
						flag = "1";
					}
					
					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName like '"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and sequencenumber like '"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%' ";
						flag = "1";
					}
					
					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname like '"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName like '"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((job != null && !job.equals(""))) {
						query = query + " and ( job like '" + job + "%') ";
						flag = "1";
					}
					
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") ";
						
						flag = "1";
					}
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision like '"
								+ companyDivision + "%') ";
						flag = "1";
					}
					
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator like '"
								+ coordinator + "%') ";
						flag = "1";
					}
					
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status like '" + status
								+ "%') ";
						flag = "1";
					}
					
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity like '"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((registrationNumber != null && !registrationNumber
							.equals(""))) {
						query = query
								+ " and ( registrationNumber like '"
								+ registrationNumber.trim()
										.replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity like '"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') ";
						flag = "1";
					}
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip like '"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip like '"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "%') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query
								+ " and ( originCountry like '"+ originCountry + "%') ";
						flag = "1";
					}
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query
								+ " and ( destinationCountry like '"+ destinationCountry+ "%') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
						query = query + " and ( email like '" + email
								+ "%' or destinationEmail like '" + email+ "%') order by controlflag, firstname ";
						flag = "1";
					}

					if (flag != null && flag.equals("0")) {

						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ "where corpid = '"
								+ sessionCorpID
								+ "' and (status is null or status not in ('CANCEL','CLOSED','CNCL')) and (lastName is null or lastName like '"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (personpricing like '"
								+ personPricing
								+ "%' or personpricing is null)"
								+ "and (firstName is null or firstName like '"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and sequencenumber like '"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%' and (billtoname is null or billtoname like '"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (accountName is null or accountName like '"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (job is null or job like '%"
								+ job
								+ "%')";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") ";
							
							
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						} 
						query = query
								+ "and (registrationnumber is null or registrationnumber like '"
								+ registrationNumber.replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						query = query
								+ " and (companydivision is null or companydivision like '%"
								+ companyDivision
								+ "%') and (coordinator is null or coordinator like '%"
								+ coordinator
								+ "%') and (status is null or status like '%"
								+ status
								+ "%') and (email is null or email like '%"
								+ email
								+ "%' or destinationEmail is null or destinationEmail like '%" + email+ "%') ";
						if ((originCity != null && !originCity.equals(""))) {
							query = query+ " and (originCity is null or originCity like '%"+ originCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query+ " and (destinationCity is null or destinationCity like '%"+ destinationCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query+ " and (originZip is null or originZip like '%"+ originZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query+ " and (destinationZip is null or destinationZip like '%"+ destinationZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and (originCountry is null or originCountry like '%"+ originCountry+ "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry+ "%') ";
						}
						query = query + " order by controlflag, firstname";

					}

					
				} else if (serviceOrderSearchVal.equalsIgnoreCase("End With")) {

					query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					if ((status == null && status.equals(""))) {
						query = query
								+ " and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}
					
					if ((lastName != null && !lastName.equals(""))) {
						query = query
								+ "and ( lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (personpricing like '%"
								+ personPricing + "' or personpricing is null)";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
						flag = "1";
					}
					
					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "' ";
						flag = "1";
					}
					
					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((job != null && !job.equals(""))) {
						query = query + " and ( job like '%" + job + "') ";
						flag = "1";
					}
					
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") ";
						
						flag = "1";
					}
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision like '%"
								+ companyDivision + "') ";
						flag = "1";
					}
				
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator like '%"
								+ coordinator + "') ";
						flag = "1";
					}
					
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status like '%" + status
								+ "') ";
						flag = "1";
					}
					
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity like '%"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((registrationNumber != null && !registrationNumber
							.equals(""))) {
						query = query
								+ " and ( registrationNumber like '%"
								+ registrationNumber.trim()
										.replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity like '%"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') ";
						flag = "1";
					}
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip like '%"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip like '%"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query
								+ " and ( originCountry like '%"+ originCountry + "') ";
						flag = "1";
					}
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query
								+ " and ( destinationCountry like '%"+ destinationCountry+ "') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
						query = query + " and ( email like '%" + email
								+ "' or destinationEmail like '%" + email+ "') order by controlflag, firstname ";
						flag = "1";
					}
					if (flag != null && flag.equals("0")) {

						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ "where corpid = '"
								+ sessionCorpID
								+ "' and (status is null or status not in ('CANCEL','CLOSED','CNCL')) and (lastName is null or lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (personpricing like '%"
								+ personPricing
								+ "' or personpricing is null)"
								+ "and (firstName is null or firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "' and (billtoname is null or billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (billtoname is null or billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (job is null or job like '%"
								+ job
								+ "%')";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") "; 
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						} 
						query = query
								+ "and (registrationnumber is null or registrationnumber like '"
								+ registrationNumber.replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						query = query
								+ " and (companydivision is null or companydivision like '%"
								+ companyDivision
								+ "%') and (coordinator is null or coordinator like '%"
								+ coordinator
								+ "%') and (status is null or status like '%"
								+ status
								+ "%') (email is null or email like '%"
								+ email
								+ "%' or destinationEmail is null or destinationEmail like '%" + email+ "%') ";
						if ((originCity != null && !originCity.equals(""))) {
							query = query+ " and (originCity is null or originCity like '%"+ originCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query+ " and (destinationCity is null or destinationCity like '%"+ destinationCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query+ " and (originZip is null or originZip like '%"+ originZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query+ " and (destinationZip is null or destinationZip like '%"+ destinationZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and (originCountry is null or originCountry like '%"+ originCountry+ "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry+ "%') ";
						}
						query = query + " order by controlflag, firstname";
					}

					
				} else if (serviceOrderSearchVal
						.equalsIgnoreCase("Exact Match")) {
					query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					if ((status == null && status.equals(""))) {
						query = query
								+ " and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status ='" + status + "') and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}
					
					if ((job != null && !job.equals(""))) {
						query = query + "and ( job ='" + job + "') ";
						flag = "1";
					}
					
					if ((lastName != null && !lastName.equals(""))) {
						query = query
								+ "and ( lastName ='"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (personpricing='" + personPricing
								+ "')";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
						flag = "1";
					}
					
					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName = '"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "')  ";
						flag = "1";
					}
					if ((sequenceNumber != null && !sequenceNumber.equals(""))) {
						query = query
								+ "and sequencenumber = '"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
						flag = "1";
					}
					
					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname = '"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName = '"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") "; 
						flag = "1";
					}
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision = '"
								+ companyDivision + "') ";
						flag = "1";
					}
					
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator = '" + coordinator
								+ "') ";
						flag = "1";
					}
					
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity = '"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((registrationNumExact != null)
							&& (!registrationNumExact.trim().equalsIgnoreCase(
									""))) {
						query = query
								+ " and ( registrationnumber = '"
								+ registrationNumExact.replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity = '"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') ";
						flag = "1";
					}
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip like '"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip like '"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query
								+ " and ( originCountry like '"+ originCountry + "') ";
						flag = "1";
					}
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query
								+ " and ( destinationCountry like '"+ destinationCountry+ "') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
						query = query + " and ( email like '" + email
								+ "' or destinationEmail like '" + email+ "') order by controlflag, firstname ";
						flag = "1";
					}
					if (flag != null && flag.equals("0")) {
						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ " where corpid = '"
								+ sessionCorpID
								+ "' and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";

						query = query
								+ " and (lastName is null or lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and personpricing = '"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "' ";
						query = query
								+ " and (firstName is null or firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%' ";
						query = query
								+ " and (billtoname is null or billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and (accountName is null or accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and (registrationnumber is null or registrationnumber like '%"
								+ registrationNumExact.replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and (email is null or email like '%"
								+ email.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%' or destinationEmail is null or destinationEmail like '%" 
										+ email.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") "; 
							
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						}
						if ((originCity != null && !originCity.equals(""))) {
							query = query
									+ " and (originCity is null or originCity like '%"
									+ originCity.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query
									+ " and (destinationCity is null or destinationCity like '%"
									+ destinationCity.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%' )";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query
									+ " and ( originZip like '%"
									+ originZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query
									+ " and ( destinationZip like '%"
									+ destinationZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and ( originCountry like '%"+ originCountry + "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and ( destinationCountry like '%"+ destinationCountry+ "%') ";
						}
						query = query + " order by controlflag, firstname";

					}

				
				} else {
					query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					if ((status == null && status.equals(""))) {
						query = query
								+ " and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled')";
						flag = "1";
					}
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status ='" + status + "') ";
						flag = "1";
					}
					
					if ((lastName != null && !lastName.equals(""))) {
						query = query
								+ "and ( lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
						flag = "1";
					}
					if ((job != null && !job.equals(""))) {
						query = query + "and ( job ='" + job + "') ";
						flag = "1";
					}
					
					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%')  ";
						flag = "1";
					}
					if ((sequenceNumber != null && !sequenceNumber.equals(""))) {
						query = query
								+ "and sequencenumber = '"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
						flag = "1";
					}
					
					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") "; 
						flag = "1";
					}
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision = '"
								+ companyDivision + "') ";
						flag = "1";
					}
					
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator = '" + coordinator
								+ "') ";
						flag = "1";
					}
					
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity like '%"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((registrationNumber != null && !registrationNumber
							.equals(""))) {
						query = query
								+ " and ( registrationNumber like '"
								+ registrationNumber.trim()
										.replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity like '%"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') ";
						flag = "1";
					}
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip like '%"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip like '%"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "%') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query+ " and ( originCountry like '%"+ originCountry + "%') ";
						flag = "1";
					}
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query+ " and ( destinationCountry like '%"+ destinationCountry+ "%') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
						query = query + " and ( email like '%" + email
								+ "%' or destinationEmail like '%" + email+ "%') order by controlflag, firstname ";
						flag = "1";
					}
					if (flag != null && flag.equals("0")) {
						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ " where corpid = '"
								+ sessionCorpID
								+ "'and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') and job is null or job like '%"
								+ job + "%' ";

						query = query
								+ " and (lastName is null or lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and personpricing = '"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "' ";
						query = query
								+ " and (firstName is null or firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%' ";
						query = query
								+ " and (billtoname is null or billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and (accountName is null or accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and (registrationnumber is null or registrationnumber like '"
								+ registrationNumExact.replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						query = query
								+ " and (email is null or email like '%"
								+ email.replaceAll("'", "''")
										.replaceAll(":", "''") + "%' or destinationEmail is null or destinationEmail like '%" 
										+ email.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") "; 
							
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						}
						if ((originCity != null && !originCity.equals(""))) {
							query = query
									+ " and (originCity is null or originCity like '%"
									+ originCity.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query
									+ " and (destinationCity is null or destinationCity like '%"
									+ destinationCity.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%' )";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query
									+ " and ( originZip like '%"
									+ originZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query
									+ " and ( destinationZip like '%"
									+ destinationZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and ( originCountry like '%"+ originCountry + "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and ( destinationCountry like '%"+ destinationCountry+ "%') ";
						}
						query = query + " order by controlflag, firstname";

					}

				}
			}
		} else {
			if (registrationNumber.equalsIgnoreCase("")) {
				if (serviceOrderSearchVal.equalsIgnoreCase("Start With")) {

					query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status ='" + status + "')";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
					}
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator = '" + coordinator
								+ "') ";
						flag = "1";
					}
					if ((sequenceNumber != null && !sequenceNumber.equals(""))) {
						query = query
								+ "and sequencenumber like '"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%'";
						flag = "1";
					}
					if ((job != null && !job.equals(""))) {
						query = query + "and ( job ='" + job + "') ";
						flag = "1";
					}
					
					if ((lastName != null && !lastName.equals(""))) {
						query = query
								+ "and ( lastName like '"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (personpricing like '"
								+ personPricing
								+ "%' or personpricing is null)";
						flag = "1";
					}
					
					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName like '"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and sequencenumber like '"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%' ";
						flag = "1";
					}
					
					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname like '"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName like '"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((job != null && !job.equals(""))) {
						query = query + " and ( job like '" + job + "%') ";
						flag = "1";
					}
					
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") "; 
						flag = "1";
						
					}
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision like '"
								+ companyDivision + "%') ";
						flag = "1";
					}
					
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator like '"
								+ coordinator + "%') ";
						flag = "1";
					}
					
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status like '" + status
								+ "%') ";
						flag = "1";
					}
					
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity like '"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity like '"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "%') ";
						flag = "1";
					}
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip like '"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip like '"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "%') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query
								+ " and ( originCountry like '"+ originCountry + "%') ";
						flag = "1";
					}
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query
								+ " and ( destinationCountry like '"+ destinationCountry+ "%') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
						query = query + " and ( email like '" + email
								+ "%' or destinationEmail like '" + email+ "%') order by controlflag, firstname ";
						flag = "1";
					}
					if (flag != null && flag.equals("0")) {

						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ "where corpid = '"
								+ sessionCorpID
								+ "' and (status is null) and (lastName is null or lastName like '"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (personpricing like '"
								+ personPricing
								+ "%' or personpricing is null)"
								+ "and (firstName is null or firstName like '"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and sequencenumber like '"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%' and (billtoname is null or billtoname like '"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (accountName is null or accountName like '"
								+ accountName.trim().replaceAll("'", "''")
								.replaceAll(":", "''")
								+ "%') and (job is null or job like '%"
								+ job
								+ "%')";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") "; 
							
							
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						} 
						query = query
								+ " and (companydivision is null or companydivision like '%"
								+ companyDivision
								+ "%') and (coordinator is null or coordinator like '%"
								+ coordinator
								+ "%') and (status is null or status like '%"
								+ status
								+ "%') and (email is null or email like '%"
								+ email
								+ "%' or destinationEmail is null or destinationEmail like '%" + email+ "%') ";
						if ((originCity != null && !originCity.equals(""))) {
							query = query+ " and (originCity is null or originCity like '%"+ originCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query+ " and (destinationCity is null or destinationCity like '%"+ destinationCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query+ " and (originZip is null or originZip like '%"+ originZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query+ " and (destinationZip is null or destinationZip like '%"+ destinationZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and (originCountry is null or originCountry like '%"+ originCountry+ "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry+ "%') ";
						}
						query = query + " order by controlflag, firstname";

					}

				} else if (serviceOrderSearchVal.equalsIgnoreCase("End With")) {

					query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status ='" + status + "')";
						flag = "1";
					}
					if ((job != null && !job.equals(""))) {
						query = query + "and ( job ='" + job + "') ";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
					}
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator = '" + coordinator
								+ "') ";
						flag = "1";
					}
					if ((sequenceNumber != null && !sequenceNumber.equals(""))) {
						query = query
								+ "and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
						flag = "1";
					}
					
					if ((lastName != null && !lastName.equals(""))) {
						query = query
								+ "and ( lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (personpricing like '%"
								+ personPricing + "' or personpricing is null)";
						flag = "1";
					}
					
					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "' ";
						flag = "1";
					}
					
					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((job != null && !job.equals(""))) {
						query = query + " and ( job like '%" + job + "') ";
						flag = "1";
					}
					
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") "; 
						flag = "1";
						
					}
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision like '%"
								+ companyDivision + "') ";
						flag = "1";
					}
					
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator like '%"
								+ coordinator + "') ";
						flag = "1";
					}
					
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status like '%" + status
								+ "') ";
						flag = "1";
					}
					
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity like '%"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity like '%"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') ";
						flag = "1";
					}
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip like '%"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip like '%"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query+ " and ( originCountry like '%"+ originCountry + "') ";
						flag = "1";
					}
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query+ " and ( destinationCountry like '%"+ destinationCountry+ "') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
						query = query + " and ( email like '%" + email
								+ "' or destinationEmail like '%" + email+ "') order by controlflag, firstname ";
						flag = "1";
					}

					if (flag != null && flag.equals("0")) {

						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ "where corpid = '"
								+ sessionCorpID
								+ "' and (status is null) and (lastName is null or lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (personpricing like '%"
								+ personPricing
								+ "' or personpricing is null)"
								+ "and (firstName is null or firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "' and (billtoname is null or billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (accountName is null or accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
								.replaceAll(":", "''")
								+ "') and (job is null or job like '%"
								+ job
								+ "%')";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") "; 
							
							
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						} 
						query = query
								+ " and (companydivision is null or companydivision like '%"
								+ companyDivision
								+ "%') and (coordinator is null or coordinator like '%"
								+ coordinator
								+ "%') and (status is null or status like '%"
								+ status
								+ "%') and (email is null or email like '%"
								+ email
								+ "%' or destinationEmail is null or destinationEmail like '%" + email+ "%') ";
						if ((originCity != null && !originCity.equals(""))) {
							query = query+ " and (originCity is null or originCity like '%"+ originCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query+ " and (destinationCity is null or destinationCity like '%"+ destinationCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query+ " and (originZip is null or originZip like '%"+ originZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query+ " and (destinationZip is null or destinationZip like '%"+ destinationZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and (originCountry is null or originCountry like '%"+ originCountry+ "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry+ "%') ";
						}
						query = query + " order by controlflag, firstname";
					}

				} else if (serviceOrderSearchVal
						.equalsIgnoreCase("Exact Match")) {

					query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					if ((lastName != null && !lastName.equals(""))) {
						query = query
								+ "and ( lastName ='"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (personpricing='" + personPricing
								+ "')";
						flag = "1";
					}
					
					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName = '"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "')  ";
						flag = "1";
					}
					if ((sequenceNumber != null && !sequenceNumber.equals(""))) {
						query = query
								+ "and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%'";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
					}
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator = '" + coordinator
								+ "') ";
						flag = "1";
					}
					
					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname = '"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName = '"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((status == null && status.equals(""))) {
						query = query
								+ " and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status ='" + status + "') and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}
					if ((job != null && !job.equals(""))) {
						query = query + "and ( job ='" + job + "') ";
						flag = "1";
					}
					
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") "; 
						flag = "1";
						
					}
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision = '"
								+ companyDivision + "') ";
						flag = "1";
					}
					
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator = '" + coordinator
								+ "') ";
						flag = "1";
					}
					
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity = '"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity = '"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') ";
						flag = "1";
					}
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip = '"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip = '"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query
								+ " and ( originCountry = '"+ originCountry + "') ";
						flag = "1";
					}
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query
								+ " and ( destinationCountry = '"+ destinationCountry+ "') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
						query = query + " and ( email like '" + email
								+ "' or destinationEmail like '" + email+ "') order by controlflag, firstname ";
						flag = "1";
					}
					if (flag != null && flag.equals("0")) {
						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ " where corpid = '"
								+ sessionCorpID
								+ "' and (status is null) ";

						query = query
								+ " and (lastName is null or lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and personpricing = '"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "' ";
						query = query
								+ " and (firstName is null or firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%' ";
						query = query
								+ " and (billtoname is null or billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and (accountName is null or accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and (email is null or email like '%"
								+ email.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%' or destinationEmail is null or destinationEmail like '%" 
										+ email.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") "; 
							
							
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						}
						if ((originCity != null && !originCity.equals(""))) {
							query = query
									+ " and (originCity is null or originCity like '%"
									+ originCity.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query
									+ " and (destinationCity is null or destinationCity like '%"
									+ destinationCity.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%' )";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query
									+ " and (originZip is null or originZip like '%"
									+ originZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query
									+ " and (destinationZip is null or destinationZip like '%"
									+ destinationZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and (originCountry is null or originCountry like '%"+ originCountry + "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry+ "%') ";
						}
						query = query + " order by controlflag, firstname";

					}

					

				} else {
							query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					
					if ((sequenceNumber != null && !sequenceNumber.equals(""))) {
						query = query
								+ "and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%'";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
					}
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator = '" + coordinator
								+ "') ";
						flag = "1";
					}
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status ='" + status + "')";
						flag = "1";
					}
					if ((job != null && !job.equals(""))) {
						query = query + "and ( job ='" + job + "') ";
						flag = "1";
					}
					
					if ((lastName != null && !lastName.equals(""))) {
						query = query
								+ "and ( lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (personpricing like '%"
								+ personPricing
								+ "%' or personpricing is null)";
						flag = "1";
					}
					
					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%' ";
						flag = "1";
					}
					
					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((job != null && !job.equals(""))) {
						query = query + " and ( job like '%" + job + "%') ";
						flag = "1";
					}
					
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") "; 
						flag = "1";
						
					}
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision like '%"
								+ companyDivision + "%') ";
						flag = "1";
					}
					
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator like '%"
								+ coordinator + "%') ";
						flag = "1";
					}
					
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status like '%" + status
								+ "%') ";
						flag = "1";
					}
					
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity like '%"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity like '%"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') ";
						flag = "1";
					}
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip like '%"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip like '%"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "%') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query+ " and ( originCountry like '%"+ originCountry + "%') ";
						flag = "1";
					}
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query+ " and ( destinationCountry like '%"+ destinationCountry+ "%') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
						query = query + " and ( email like '%" + email
								+ "%' or destinationEmail like '%" + email+ "%') order by controlflag, firstname ";
						flag = "1";
					}
					if (flag != null && flag.equals("0")) {

						
						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ "where corpid = '"
								+ sessionCorpID
								+ "' and (status is null) and (lastName is null or lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (personpricing like '%"
								+ personPricing
								+ "%' or personpricing is null)"
								+ "and (firstName is null or firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%' and (billtoname is null or billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (accountName is null or accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (job is null or job like '%"
								+ job
								+ "%')";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") "; 
							
							
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						} 
						query = query
								+ " and ( companydivision is null or companydivision like '%"
								+ companyDivision
								+ "%') and (coordinator is null or coordinator like '%"
								+ coordinator
								+ "%') and (status is null or status like '%"
								+ status
								+ "%') and (email is null or email like '%"
								+ email
								+ "%' or destinationEmail is null or destinationEmail like '%" + email+ "%') ";
						if ((originCity != null && !originCity.equals(""))) {
							query = query+ " and (originCity is null or originCity like '%"+ originCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query+ " and (destinationCity is null or destinationCity like '%"+ destinationCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query+ " and (originZip is null or originZip like '%"+ originZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query+ " and (destinationZip is null or destinationZip like '%"+ destinationZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and (originCountry is null or originCountry like '%"+ originCountry+ "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry+ "%') ";
						}
						query = query + " order by controlflag, firstname";

					}
					

				}
			
			} else {

				
				if (serviceOrderSearchVal.equalsIgnoreCase("Start With")) {
					query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					
					if ((lastName != null && !lastName.equals(""))) {
						query = query
								+ "and ( lastName like '"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (personpricing like '"
								+ personPricing
								+ "%' or personpricing is null)";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
						flag = "1";
					}
					
					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName like '"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and sequencenumber like '"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%' ";
						flag = "1";
					}
					
					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname like '"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName like '"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((job != null && !job.equals(""))) {
						query = query + " and ( job like '" + job + "%') ";
						flag = "1";
					}
					
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") "; 
						flag = "1";
						
					}
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision like '"
								+ companyDivision + "%') ";
						flag = "1";
					}
					
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator like '"
								+ coordinator + "%') ";
						flag = "1";
					}
					
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status like '" + status
								+ "%') ";
						flag = "1";
					}
					
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity like '"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					
					if ((registrationNumber != null && !registrationNumber
							.equals(""))) {
						query = query
								+ " and ( registrationNumber like '"
								+ registrationNumber.trim()
										.replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity like '"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') ";
						flag = "1";
					}
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip like '"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						flag = "1";
					}
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip like '"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "%') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query+ " and ( originCountry like '"+ originCountry + "%') ";
						flag = "1";
					}
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query+ " and ( destinationCountry like '"+ destinationCountry+ "%') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
											query = query + " and ( email like '" + email
													+ "%' or destinationEmail like '" + email+ "%') order by controlflag, firstname ";
											flag = "1";
										}
					
					if (flag != null && flag.equals("0")) {

						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ "where corpid = '"
								+ sessionCorpID
								+ "' and (status is null) and (lastName is null or lastName like '"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (personpricing like '"
								+ personPricing
								+ "%' or personpricing is null)"
								+ "and (firstName is null or firstName like '"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and sequencenumber like '"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%' and (billtoname is null or billtoname like '"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (accountName is null or accountName like '"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%') and (job is null or job like '%"
								+ job
								+ "%')";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") "; 
							
							
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						} 
						query = query
								+ "and (registrationnumber is null or registrationnumber like '"
								+ registrationNumber.replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						query = query
								+ " and (companydivision is null or companydivision like '%"
								+ companyDivision
								+ "%') and (coordinator is null or coordinator like '%"
								+ coordinator
								+ "%') and (status is null or status like '%"
								+ status
								+ "%') and (email is null or email like '%"
								+ email
								+ "%' or destinationEmail is null or destinationEmail like '%" + email+ "%') ";
						if ((originCity != null && !originCity.equals(""))) {
							query = query+ " and (originCity is null or originCity like '%"+ originCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query+ " and (destinationCity is null or destinationCity like '%"+ destinationCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query+ " and (originZip is null or originZip like '%"+ originZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query+ " and (destinationZip is null or destinationZip like '%"+ destinationZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and (originCountry is null or originCountry like '%"+ originCountry+ "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry+ "%') ";
						}
						query = query + " order by controlflag, firstname";

					}


				} else if (serviceOrderSearchVal.equalsIgnoreCase("End With")) {

					query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					if ((lastName != null && !lastName.equals(""))) {
						query = query
								+ "and ( lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (personpricing like '%"
								+ personPricing + "' or personpricing is null)";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
						flag = "1";
					}
				
					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "' ";
						flag = "1";
					}
					
					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((job != null && !job.equals(""))) {
						query = query + " and ( job like '%" + job + "') ";
						flag = "1";
					}
					
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") "; 
						flag = "1";
						
					}
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision like '%"
								+ companyDivision + "') ";
						flag = "1";
					}
					
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator like '%"
								+ coordinator + "') ";
						flag = "1";
					}
					
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status like '%" + status
								+ "') ";
						flag = "1";
					}
					
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity like '%"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((registrationNumber != null && !registrationNumber
							.equals(""))) {
						query = query
								+ " and ( registrationNumber like '%"
								+ registrationNumber.trim()
										.replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity like '%"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') ";
						flag = "1";
					}
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip like '%"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip like '%"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query+ " and ( originCountry like '%"+ originCountry + "') ";
						flag = "1";
					}
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query+ " and ( destinationCountry like '%"+ destinationCountry+ "') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
						query = query + " and ( email like '%" + email
								+ "' or destinationEmail like '%" + email+ "') order by controlflag, firstname ";
						flag = "1";
					}
					if (flag != null && flag.equals("0")) {

						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ "where corpid = '"
								+ sessionCorpID
								+ "' and (status is null) and (lastName is null or lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (personpricing like '%"
								+ personPricing
								+ "' or personpricing is null)"
								+ "and (firstName is null or firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "' and (billtoname is null or billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (billtoname is null or billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (job is null or job like '%"
								+ job
								+ "%')";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") "; 
							
							
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						} 
						query = query
								+ "and (registrationnumber is null or registrationnumber like '"
								+ registrationNumber.replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						query = query
								+ " and (companydivision is null or companydivision like '%"
								+ companyDivision
								+ "%') and (coordinator is null or coordinator like '%"
								+ coordinator
								+ "%') and (status is null or status like '%"
								+ status
								+ "%') and (email is null or email like '%"
								+ email
								+ "%' or destinationEmail is null or destinationEmail like '%" + email+ "%') ";
						if ((originCity != null && !originCity.equals(""))) {
							query = query+ " and (originCity is null or originCity like '%"+ originCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query+ " and (destinationCity is null or destinationCity like '%"+ destinationCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query+ " and (originZip is null or originZip like '%"+ originZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query+ " and (destinationZip is null or destinationZip like '%"+ destinationZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and (originCountry is null or originCountry like '%"+ originCountry+ "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry+ "%') ";
						}
						query = query + " order by controlflag, firstname";
					}

					
				} else if (serviceOrderSearchVal
						.equalsIgnoreCase("Exact Match")) {
					query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
							+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
							+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
							+ "where corpid = '" + sessionCorpID + "'";
					
					if ((status.equalsIgnoreCase("") || status==null)) {
						query = query
								+ " and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}
					if ((status != null && !status.equals(""))) {
						query = query + " and ( status ='" + status + "') and ( status not in ('CANCEL','CLOSED','CNCL')) and quotationStatus not in ('Rejected','Cancelled') ";
						flag = "1";
					}
					if ((job != null && !job.equals(""))) {
						query = query + "and ( job ='" + job + "') ";
						flag = "1";
					}
					
					if ((lastName != null && !lastName.equals(""))) {
						query = query
								+ "and ( lastName ='"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') and (personpricing='" + personPricing
								+ "')";
						flag = "1";
					}
					
					if ((firstName != null && !firstName.equals(""))) {
						query = query
								+ " and ( firstName = '"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "')  ";
						flag = "1";
					}
					if ((sequenceNumber != null && !sequenceNumber.equals(""))) {
						query = query
								+ "and sequencenumber = '"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
						flag = "1";
					}
					if ((personPricing.trim().replaceAll("'", "''")
							.replaceAll(":", "''") != null && !personPricing
							.trim().replaceAll("'", "''").replaceAll(":", "''")
							.equals(""))) {
						query = query
								+ "and personpricing ='"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "'";
						flag = "1";
					}
					
					if ((billToName != null && !billToName.equals(""))) {
						query = query
								+ " and ( billtoname = '"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((accountName != null && !accountName.equals(""))) {
						query = query
								+ " and ( accountName = '"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((partnerSetCode != null)
							&& (!partnerSetCode.equals(""))) {
						query = query + " and billToCode in (" + partnerSetCode
								+ ") ";
						flag = "1";
					}
					if ((bookingAgentSet != null)
							&& (!bookingAgentSet.equals(""))) {
						query = query + " and bookingAgentCode in (" + bookingAgentSet
								+ ") "; 
						flag = "1";
						
					}
					if ((companydivisionSetCode != null)
							&& (!companydivisionSetCode.equals(""))) {
						query = query + " and companyDivision in (" + companydivisionSetCode
								+ ") ";
						flag = "1";
					} 
					if ((companyDivision != null && !companyDivision.equals(""))) {
						query = query + " and ( companydivision = '"
								+ companyDivision + "') ";
						flag = "1";
					}
					
					if ((coordinator != null && !coordinator.equals(""))) {
						query = query + " and ( coordinator = '" + coordinator
								+ "') ";
						flag = "1";
					}
					
					if ((originCity != null && !originCity.equals(""))) {
						query = query
								+ " and ( originCity = '"
								+ originCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					
					if ((registrationNumExact != null)
							&& (!registrationNumExact.trim().equalsIgnoreCase(
									""))) {
						query = query
								+ " and ( registrationnumber = '"
								+ registrationNumExact.replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((destinationCity != null && !destinationCity.equals(""))) {
						query = query
								+ " and ( destinationCity = '"
								+ destinationCity.trim().replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "') ";
						flag = "1";
					}
					if ((originZip != null && !originZip.equals(""))) {
						query = query
								+ " and ( originZip = '"
								+ originZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "') ";
						flag = "1";
					}
					if ((destinationZip != null && !destinationZip.equals(""))) {
						query = query
								+ " and ( destinationZip = '"
								+ destinationZip.trim().replaceAll("'", "''")
										.replaceAll(":", "''")+ "') ";
						flag = "1";
					}
					if ((originCountry != null && !originCountry.equals(""))) {
						query = query+ " and ( originCountry = '"+ originCountry + "') ";
						flag = "1";
					}
					if ((destinationCountry != null && !destinationCountry.equals(""))) {
						query = query+ " and ( destinationCountry = '"+ destinationCountry+ "') ";
						flag = "1";
					}
					if ((email != null && !email.equals(""))) {
						query = query + " and ( email like '" + email
								+ "' or destinationEmail like '" + email+ "') order by controlflag, firstname ";
						flag = "1";
					}
					if (flag != null && flag.equals("0")) {
						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ " where corpid = '"
								+ sessionCorpID
								+ "' and (status is null) ";

						query = query
								+ " and (lastName is null or lastName like '%"
								+ lastName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and personpricing = '"
								+ personPricing.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "' ";
						query = query
								+ " and (firstName is null or firstName like '%"
								+ firstName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and sequencenumber like '%"
								+ sequenceNumber.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%' ";
						query = query
								+ " and (billtoname is null or billtoname like '%"
								+ billToName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and (accountName is null or accountName like '%"
								+ accountName.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and (registrationnumber is null or registrationnumber like '%"
								+ registrationNumExact.replaceAll("'", "''")
										.replaceAll(":", "''") + "%') ";
						query = query
								+ " and (email is null or email like '%"
								+ email.trim().replaceAll("'", "''")
										.replaceAll(":", "''") + "%') or destinationEmail is null or destinationEmail like '%" 
										+ email.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") "; 
							
							
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							 
						}
						if ((originCity != null && !originCity.equals(""))) {
							query = query
									+ " and (originCity is null or originCity like '%"
									+ originCity.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
						}
						if ((destinationCity != null && !destinationCity.equals(""))) {
							query = query
									+ " and (destinationCity is null or destinationCity like '%"
									+ destinationCity.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%' )";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query
									+ " and (originZip is null or originZip like '%"
									+ originZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query
									+ " and (destinationZip is null or destinationZip like '%"
									+ destinationZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''")+ "%') ";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and (originCountry is null or originCountry like '%"+ originCountry + "%') ";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry+ "%') ";
						}
						query = query + " order by controlflag, firstname";

					}

					
				} else {
					if (serviceOrderSearchVal.equalsIgnoreCase("Start With")) {
						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ "where corpid = '" + sessionCorpID + "'";
						
						if ((lastName != null && !lastName.equals(""))) {
							query = query
									+ "and ( lastName like '"
									+ lastName.trim().replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "%') and (personpricing like '"
									+ personPricing
									+ "%' or personpricing is null)";
							flag = "1";
						}
						
						if ((firstName != null && !firstName.equals(""))) {
							query = query
									+ " and ( firstName like '"
									+ firstName.trim().replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "%') and sequencenumber like '"
									+ sequenceNumber.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''") + "%' ";
							flag = "1";
						}
						
						if ((billToName != null && !billToName.equals(""))) {
							query = query
									+ " and ( billtoname like '"
									+ billToName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							flag = "1";
						}
						if ((accountName != null && !accountName.equals(""))) {
							query = query
									+ " and ( accountName like '"
									+ accountName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							flag = "1";
						}
						
						if ((job != null && !job.equals(""))) {
							query = query + " and ( job like '" + job + "%') ";
							flag = "1";
						}
						
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
							flag = "1";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") "; 
							flag = "1";
							
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							flag = "1";
						} 
						if ((companyDivision != null && !companyDivision
								.equals(""))) {
							query = query + " and ( companydivision like '"
									+ companyDivision + "%') ";
							flag = "1";
						}
						
						if ((coordinator != null && !coordinator.equals(""))) {
							query = query + " and ( coordinator like '"
									+ coordinator + "%') ";
							flag = "1";
						}
						
						if ((status != null && !status.equals(""))) {
							query = query + " and ( status like '" + status
									+ "%') ";
							flag = "1";
						}
						
						if ((originCity != null && !originCity.equals(""))) {
							query = query
									+ " and ( originCity like '"
									+ originCity.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							flag = "1";
						}
						
						if ((registrationNumber != null && !registrationNumber
								.equals(""))) {
							query = query
									+ " and ( registrationNumber like '"
									+ registrationNumber.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							flag = "1";
						}
						if ((destinationCity != null && !destinationCity
								.equals(""))) {
							query = query
									+ " and ( destinationCity like '"
									+ destinationCity.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "%') ";
							flag = "1";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query
									+ " and ( originZip like '"
									+ originZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							flag = "1";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query
									+ " and ( destinationZip like '"
									+ destinationZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''")+ "%') ";
							flag = "1";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and ( originCountry like '"+ originCountry + "%') ";
							flag = "1";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and ( destinationCountry like '"+ destinationCountry+ "%') ";
							flag = "1";
						}
						if ((email != null && !email.equals(""))) {
							query = query + " and ( email like '" + email
									+ "%' or destinationEmail like '" + email+ "%') order by controlflag, firstname";
							flag = "1";
						}
						if (flag != null && flag.equals("0")) {

							query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
									+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
									+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
									+ "where corpid = '"
									+ sessionCorpID
									+ "' and (status is null) and (lastName is null or lastName like '"
									+ lastName.trim().replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "%') and (personpricing like '"
									+ personPricing
									+ "%' or personpricing is null)"
									+ "and (firstName is null or firstName like '"
									+ firstName.trim().replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "%') and sequencenumber like '"
									+ sequenceNumber.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "%' and (billtoname is null or billtoname like '"
									+ billToName.trim().replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "%') and (billtoname is null or billtoname like '"
									+ billToName.trim().replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "%') and (job is null or job like '%"
									+ job + "%')";
							if ((partnerSetCode != null)
									&& (!partnerSetCode.equals(""))) {
								query = query + " and billToCode in ("
										+ partnerSetCode + ") ";
							}
							if ((bookingAgentSet != null)
									&& (!bookingAgentSet.equals(""))) {
								query = query + " and bookingAgentCode in (" + bookingAgentSet
										+ ") "; 
								
								
							}
							if ((companydivisionSetCode != null)
									&& (!companydivisionSetCode.equals(""))) {
								query = query + " and companyDivision in (" + companydivisionSetCode
										+ ") ";
								 
							} 
							query = query
									+ "and (registrationnumber is null or registrationnumber like '"
									+ registrationNumber.replaceAll("'", "''")
											.replaceAll(":", "''") + "') ";
							query = query
									+ " and (companydivision is null or companydivision like '%"
									+ companyDivision
									+ "%') and (coordinator is null or coordinator like '%"
									+ coordinator
									+ "%') and (status is null or status like '%"
									+ status
									+ "%') and (email is null or email like '%"
									+ email
									+ "%' or destinationEmail is null or destinationEmail like '%" + email+ "%') ";
							if ((originCity != null && !originCity.equals(""))) {
								query = query+ " and (originCity is null or originCity like '"+ originCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
							}
							if ((destinationCity != null && !destinationCity.equals(""))) {
								query = query+ " and (destinationCity is null or destinationCity like '"+ destinationCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
							}
							if ((originZip != null && !originZip.equals(""))) {
								query = query+ " and (originZip is null or originZip like '"+ originZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
							}
							if ((destinationZip != null && !destinationZip.equals(""))) {
								query = query+ " and (destinationZip is null or destinationZip like '"+ destinationZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
							}
							if ((originCountry != null && !originCountry.equals(""))) {
								query = query+ " and (originCountry is null or originCountry like '"+ originCountry+ "%') ";
							}
							if ((destinationCountry != null && !destinationCountry.equals(""))) {
								query = query+ " and (destinationCountry is null or destinationCountry like '"+ destinationCountry+ "%') ";
							}
							query = query + " order by controlflag, firstname";

						}

						

					} else if (serviceOrderSearchVal
							.equalsIgnoreCase("End With")) {

						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ "where corpid = '" + sessionCorpID + "'";
						
						if ((lastName != null && !lastName.equals(""))) {
							query = query
									+ "and ( lastName like '%"
									+ lastName.trim().replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "') and (personpricing like '%"
									+ personPricing
									+ "' or personpricing is null)";
							flag = "1";
						}
						
						if ((firstName != null && !firstName.equals(""))) {
							query = query
									+ " and ( firstName like '%"
									+ firstName.trim().replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "') and sequencenumber like '%"
									+ sequenceNumber.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''") + "' ";
							flag = "1";
						}
						
						if ((billToName != null && !billToName.equals(""))) {
							query = query
									+ " and ( billtoname like '%"
									+ billToName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "') ";
							flag = "1";
						}
						if ((accountName != null && !accountName.equals(""))) {
							query = query
									+ " and ( accountName like '%"
									+ accountName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "') ";
							flag = "1";
						}
						
						if ((job != null && !job.equals(""))) {
							query = query + " and ( job like '%" + job + "') ";
							flag = "1";
						}
						
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
							flag = "1";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") "; 
							flag = "1";
							
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							flag = "1";
						} 
						if ((companyDivision != null && !companyDivision
								.equals(""))) {
							query = query + " and ( companydivision like '%"
									+ companyDivision + "') ";
							flag = "1";
						}
						
						if ((coordinator != null && !coordinator.equals(""))) {
							query = query + " and ( coordinator like '%"
									+ coordinator + "') ";
							flag = "1";
						}
						
						if ((status != null && !status.equals(""))) {
							query = query + " and ( status like '%" + status
									+ "') ";
							flag = "1";
						}
						
						if ((originCity != null && !originCity.equals(""))) {
							query = query
									+ " and ( originCity like '%"
									+ originCity.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "') ";
							flag = "1";
						}
						
						if ((registrationNumber != null && !registrationNumber
								.equals(""))) {
							query = query
									+ " and ( registrationNumber like '%"
									+ registrationNumber.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''") + "') ";
							flag = "1";
						}
						if ((destinationCity != null && !destinationCity
								.equals(""))) {
							query = query
									+ " and ( destinationCity like '%"
									+ destinationCity.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "')";
							flag = "1";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query
									+ " and ( originZip like '%"
									+ originZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "') ";
							flag = "1";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query
									+ " and ( destinationZip like '%"
									+ destinationZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''")+ "') ";
							flag = "1";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and ( originCountry like '%"+ originCountry + "') ";
							flag = "1";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and ( destinationCountry like '%"+ destinationCountry+ "') ";
							flag = "1";
						}
						if ((email != null && !email.equals(""))) {
							query = query + " and ( email like '%" + email
									+ "' or destinationEmail like '%" + email+ "') order by controlflag, firstname ";
							flag = "1";
						}
						if (flag != null && flag.equals("0")) {

							query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
									+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
									+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
									+ "where corpid = '"
									+ sessionCorpID
									+ "' and (status is null) and (lastName is null or lastName like '%"
									+ lastName.trim().replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "') and (personpricing like '%"
									+ personPricing
									+ "' or personpricing is null)"
									+ "and (firstName is null or firstName like '%"
									+ firstName.trim().replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "') and sequencenumber like '%"
									+ sequenceNumber.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "' and (billtoname is null or billtoname like '%"
									+ billToName.trim().replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "') and (accountName is null or accountName like '%"
									+ accountName.trim().replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "') and (job is null or job like '%"
									+ job + "%')";
							if ((partnerSetCode != null)
									&& (!partnerSetCode.equals(""))) {
								query = query + " and billToCode in ("
										+ partnerSetCode + ") ";
							}
							if ((bookingAgentSet != null)
									&& (!bookingAgentSet.equals(""))) {
								query = query + " and bookingAgentCode in (" + bookingAgentSet
										+ ") "; 
								
								
							}
							if ((companydivisionSetCode != null)
									&& (!companydivisionSetCode.equals(""))) {
								query = query + " and companyDivision in (" + companydivisionSetCode
										+ ") ";
								 
							} 
							query = query
									+ "and (registrationnumber is null or registrationnumber like '"
									+ registrationNumber.replaceAll("'", "''")
											.replaceAll(":", "''") + "') ";
							query = query
									+ " and (companydivision is null or companydivision like '%"
									+ companyDivision
									+ "%') and (coordinator is null or coordinator like '%"
									+ coordinator
									+ "%') and (status is null or status like '%"
									+ status
									+ "%') and (email is null or email like '%"
									+ email
									+ "%' or destinationEmail is null or destinationEmail like '%" + email+ "%') ";
							if ((originCity != null && !originCity.equals(""))) {
								query = query+ " and (originCity is null or originCity like '%"+ originCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "') ";
							}
							if ((destinationCity != null && !destinationCity.equals(""))) {
								query = query+ " and (destinationCity is null or destinationCity like '%"+ destinationCity.trim().replaceAll("'", "''").replaceAll(":", "''")+ "') ";
							}
							if ((originZip != null && !originZip.equals(""))) {
								query = query+ " and (originZip is null or originZip like '%"+ originZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "') ";
							}
							if ((destinationZip != null && !destinationZip.equals(""))) {
								query = query+ " and (destinationZip is null or destinationZip like '%"+ destinationZip.trim().replaceAll("'", "''").replaceAll(":", "''")+ "') ";
							}
							if ((originCountry != null && !originCountry.equals(""))) {
								query = query+ " and (originCountry is null or originCountry like '%"+ originCountry+ "') ";
							}
							if ((destinationCountry != null && !destinationCountry.equals(""))) {
								query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry+ "') ";
							}
							query = query + " order by controlflag, firstname";
						}

						

					} else if (serviceOrderSearchVal
							.equalsIgnoreCase("Exact Match")) {
						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ "where corpid = '" + sessionCorpID + "'";
						
						if ((lastName != null && !lastName.equals(""))) {
							query = query
									+ "and ( lastName ='"
									+ lastName.trim().replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "') and (personpricing='" + personPricing
									+ "')";
							flag = "1";
						}
						
						if ((firstName != null && !firstName.equals(""))) {
							query = query
									+ " and ( firstName = '"
									+ firstName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "')  ";
							flag = "1";
						}
						if ((sequenceNumber != null && !sequenceNumber
								.equals(""))) {
							query = query
									+ "and sequencenumber = '"
									+ sequenceNumber.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''") + "'";
							flag = "1";
						}
						
						if ((billToName != null && !billToName.equals(""))) {
							query = query
									+ " and ( billtoname = '"
									+ billToName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "') ";
							flag = "1";
						}
						if ((accountName != null && !accountName.equals(""))) {
							query = query
									+ " and ( accountName = '"
									+ accountName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "') ";
							flag = "1";
						}
						
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
							flag = "1";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") "; 
							flag = "1";
							
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							flag = "1";
						} 
						if ((companyDivision != null && !companyDivision
								.equals(""))) {
							query = query + " and ( companydivision = '"
									+ companyDivision + "') ";
							flag = "1";
						}
						
						if ((coordinator != null && !coordinator.equals(""))) {
							query = query + " and ( coordinator = '"
									+ coordinator + "') ";
							flag = "1";
						}
						
						if ((originCity != null && !originCity.equals(""))) {
							query = query
									+ " and ( originCity = '"
									+ originCity.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "') ";
							flag = "1";
						}
						
						if ((registrationNumExact != null)
								&& (!registrationNumExact.trim()
										.equalsIgnoreCase(""))) {
							query = query
									+ " and ( registrationnumber = '"
									+ registrationNumExact
											.replaceAll("'", "''").replaceAll(
													":", "''") + "') ";
							flag = "1";
						}
						if ((destinationCity != null && !destinationCity
								.equals(""))) {
							query = query
									+ " and ( destinationCity = '"
									+ destinationCity.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "') ";
							flag = "1";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query
									+ " and ( originZip = '"
									+ originZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "') ";
							flag = "1";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query
									+ " and ( destinationZip = '"
									+ destinationZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''")+ "') ";
							flag = "1";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and ( originCountry = '"+ originCountry + "') ";
							flag = "1";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and ( destinationCountry = '"+ destinationCountry+ "') ";
							flag = "1";
						}
						if ((email != null && !email.equals(""))) {
							query = query + " and ( email like '" + email
									+ "' or destinationEmail like '" + email+ "') order by controlflag, firstname ";
							flag = "1";
						}
						if (flag != null && flag.equals("0")) {
							query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
									+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
									+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
									+ " where corpid = '"
									+ sessionCorpID
									+ "' and (status is null) ";

							query = query
									+ " and (lastName is null or lastName like '%"
									+ lastName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							query = query
									+ " and personpricing = '"
									+ personPricing.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''") + "' ";
							query = query
									+ " and (firstName is null or firstName like '%"
									+ firstName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							query = query
									+ " and sequencenumber like '%"
									+ sequenceNumber.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''") + "%' ";
							query = query
									+ " and (billtoname is null or billtoname like '%"
									+ billToName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							query = query
									+ " and (accountName is null or accountName like '%"
									+ accountName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							
							query = query
									+ " and (registrationnumber is null or registrationnumber like '%"
									+ registrationNumExact
											.replaceAll("'", "''").replaceAll(
													":", "''") + "%') ";
							query = query
									+ " and (email is null or email like '%"
									+ email.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') or destinationEmail is null or destinationEmail like '%" 
											+ email.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
							if ((partnerSetCode != null)
									&& (!partnerSetCode.equals(""))) {
								query = query + " and billToCode in ("
										+ partnerSetCode + ") ";
							}
							if ((bookingAgentSet != null)
									&& (!bookingAgentSet.equals(""))) {
								query = query + " and bookingAgentCode in (" + bookingAgentSet
										+ ") "; 
								
								
							}
							if ((companydivisionSetCode != null)
									&& (!companydivisionSetCode.equals(""))) {
								query = query + " and companyDivision in (" + companydivisionSetCode
										+ ") ";
								 
							}
							if ((originCity != null && !originCity.equals(""))) {
								query = query
										+ " and (originCity is null or originCity like '%"
										+ originCity.trim().replaceAll("'", "''")
												.replaceAll(":", "''") + "%') ";
							}
							if ((destinationCity != null && !destinationCity.equals(""))) {
								query = query
										+ " and (destinationCity is null or destinationCity like '%"
										+ destinationCity.trim()
												.replaceAll("'", "''")
												.replaceAll(":", "''") + "%' )";
							}
							if ((originZip != null && !originZip.equals(""))) {
								query = query
										+ " and (originZip is null or originZip like '%"
										+ originZip.trim().replaceAll("'", "''")
												.replaceAll(":", "''") + "%') ";
							}
							if ((destinationZip != null && !destinationZip.equals(""))) {
								query = query
										+ " and (destinationZip is null or destinationZip like '%"
										+ destinationZip.trim().replaceAll("'", "''")
												.replaceAll(":", "''")+ "%') ";
							}
							if ((originCountry != null && !originCountry.equals(""))) {
								query = query+ " and (originCountry is null or originCountry like '%"+ originCountry + "%') ";
							}
							if ((destinationCountry != null && !destinationCountry.equals(""))) {
								query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry+ "%') ";
							}
							query = query + " order by controlflag, firstname";

						}

						
					} else {
						query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
								+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
								+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
								+ "where corpid = '" + sessionCorpID + "'";
						
						if ((status != null && !status.equals(""))) {
							query = query + " and ( status ='" + status + "') ";
							flag = "1";
						}
						
						if ((lastName != null && !lastName.equals(""))) {
							query = query
									+ "and ( lastName like '%"
									+ lastName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							flag = "1";
						}
						if ((personPricing.trim().replaceAll("'", "''")
								.replaceAll(":", "''") != null && !personPricing
								.trim().replaceAll("'", "''")
								.replaceAll(":", "''").equals(""))) {
							query = query
									+ "and personpricing ='"
									+ personPricing.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''") + "'";
						}
						if ((job != null && !job.equals(""))) {
							query = query + "and ( job ='" + job + "') ";
							flag = "1";
						}
						
						if ((firstName != null && !firstName.equals(""))) {
							query = query
									+ " and ( firstName like '%"
									+ firstName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%')  ";
							flag = "1";
						}
						if ((sequenceNumber != null && !sequenceNumber
								.equals(""))) {
							query = query
									+ "and sequencenumber = '"
									+ sequenceNumber.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''") + "'";
							flag = "1";
						}
						if ((billToName != null && !billToName.equals(""))) {
							query = query
									+ " and ( billtoname like '%"
									+ billToName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							flag = "1";
						}
						if ((accountName != null && !accountName.equals(""))) {
							query = query
									+ " and ( accountName like '%"
									+ accountName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							flag = "1";
						}
						
						if ((partnerSetCode != null)
								&& (!partnerSetCode.equals(""))) {
							query = query + " and billToCode in ("
									+ partnerSetCode + ") ";
							flag = "1";
						}
						if ((bookingAgentSet != null)
								&& (!bookingAgentSet.equals(""))) {
							query = query + " and bookingAgentCode in (" + bookingAgentSet
									+ ") "; 
							flag = "1";
							
						}
						if ((companydivisionSetCode != null)
								&& (!companydivisionSetCode.equals(""))) {
							query = query + " and companyDivision in (" + companydivisionSetCode
									+ ") ";
							flag = "1";
						} 
						if ((companyDivision != null && !companyDivision
								.equals(""))) {
							query = query + " and ( companydivision = '"
									+ companyDivision + "') ";
							flag = "1";
						}
						
						if ((coordinator != null && !coordinator.equals(""))) {
							query = query + " and ( coordinator = '"
									+ coordinator + "') ";
							flag = "1";
						}
						
						if ((originCity != null && !originCity.equals(""))) {
							query = query
									+ " and ( originCity like '%"
									+ originCity.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							flag = "1";
						}
						
						if ((registrationNumber != null && !registrationNumber
								.equals(""))) {
							query = query
									+ " and ( registrationNumber like '"
									+ registrationNumber.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''") + "') ";
							flag = "1";
						}
						
						if ((destinationCity != null && !destinationCity
								.equals(""))) {
							query = query
									+ " and ( destinationCity like '%"
									+ destinationCity.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''")
									+ "%') ";
							flag = "1";
						}
						if ((originZip != null && !originZip.equals(""))) {
							query = query
									+ " and ( originZip like '%"
									+ originZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							flag = "1";
						}
						if ((destinationZip != null && !destinationZip.equals(""))) {
							query = query
									+ " and ( destinationZip like '%"
									+ destinationZip.trim().replaceAll("'", "''")
											.replaceAll(":", "''")+ "%') ";
							flag = "1";
						}
						if ((originCountry != null && !originCountry.equals(""))) {
							query = query+ " and ( originCountry like '%"+ originCountry + "%') ";
							flag = "1";
						}
						if ((destinationCountry != null && !destinationCountry.equals(""))) {
							query = query+ " and ( destinationCountry like '%"+ destinationCountry+ "%') ";
							flag = "1";
						}
						if ((email != null && !email.equals(""))) {
							query = query + " and ( email like '%" + email
									+ "%' or destinationEmail like '%" + email+ "%') order by controlflag, firstname ";
							flag = "1";
						}
						if (flag != null && flag.equals("0")) {
							query = "select if(controlflag = 'C','CF',(if(controlflag='Q','QF',(if(controlflag='A','OR',(if(controlflag='G','GR',(if(controlflag='L','LC',''))))))))) as 'type', "
									+ "sequencenumber, status, firstname, lastname, job, billtoname ,"
									+ "coordinator, origincountry, origincity, destinationcountry, destinationcity, createdon, createdby, 'node',companydivision, registrationnumber,id,personPricing,quotationStatus,if(email is null or email='', destinationEmail,email) as email,accountName,moveType,orderIntiationStatus from customerfile "
									+ " where corpid = '"
									+ sessionCorpID
									+ "' and (status is null) ";

							query = query
									+ " and (lastName is null or lastName like '%"
									+ lastName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							query = query
									+ " and personpricing = '"
									+ personPricing.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''") + "' ";
							query = query
									+ " and (firstName is null or firstName like '%"
									+ firstName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							query = query
									+ " and sequencenumber like '%"
									+ sequenceNumber.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''") + "%' ";
							query = query
									+ " and (billtoname is null or billtoname like '%"
									+ billToName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							query = query
									+ " and (accountName is null or accountName like '%"
									+ accountName.trim().replaceAll("'", "''")
											.replaceAll(":", "''") + "%') ";
							query = query
									+ " and (registrationnumber is null or registrationnumber like '"
									+ registrationNumExact
											.replaceAll("'", "''").replaceAll(
													":", "''") + "') ";
							query = query
									+ " and (email is null or email like '%"
									+ email.trim()
											.replaceAll("'", "''")
											.replaceAll(":", "''") + "%' ) or destinationEmail is null or destinationEmail like '%" 
											+ email.trim().replaceAll("'", "''").replaceAll(":", "''")+ "%') ";
							if ((partnerSetCode != null)
									&& (!partnerSetCode.equals(""))) {
								query = query + " and billToCode in ("
										+ partnerSetCode + ") ";
							}
							if ((bookingAgentSet != null)
									&& (!bookingAgentSet.equals(""))) {
								query = query + " and bookingAgentCode in (" + bookingAgentSet
										+ ") "; 
								
								
							}
							if ((companydivisionSetCode != null)
									&& (!companydivisionSetCode.equals(""))) {
								query = query + " and companyDivision in (" + companydivisionSetCode
										+ ") ";
								 
							}
							if ((originCity != null && !originCity.equals(""))) {
								query = query
										+ " and (originCity is null or originCity like '%"
										+ originCity.trim().replaceAll("'", "''")
												.replaceAll(":", "''") + "%') ";
							}
							if ((destinationCity != null && !destinationCity.equals(""))) {
								query = query
										+ " and (destinationCity is null or destinationCity like '%"
										+ destinationCity.trim()
												.replaceAll("'", "''")
												.replaceAll(":", "''") + "%' )";
							}
							if ((originZip != null && !originZip.equals(""))) {
								query = query
										+ " and (originZip is null or originZip like '"
										+ originZip.trim().replaceAll("'", "''")
												.replaceAll(":", "''") + "') ";
							}
							if ((destinationZip != null && !destinationZip.equals(""))) {
								query = query
										+ " and (destinationZip is null or destinationZip like '"
										+ destinationZip.trim().replaceAll("'", "''")
												.replaceAll(":", "''")+ "') ";
							}
							if ((originCountry != null && !originCountry.equals(""))) {
								query = query+ " and (originCountry is null or originCountry like '%"+ originCountry + "%') ";
							}
							if ((destinationCountry != null && !destinationCountry.equals(""))) {
								query = query+ " and (destinationCountry is null or destinationCountry like '%"+ destinationCountry+ "%') ";
							}
							query = query + " order by controlflag, firstname";

						}

						
					}
				}

			}
		}
		list = this.getSession().createSQLQuery(query)
				.addScalar("type", Hibernate.STRING)
				.addScalar("sequencenumber", Hibernate.STRING)
				.addScalar("status", Hibernate.STRING)
				.addScalar("firstname", Hibernate.STRING)
				.addScalar("lastname", Hibernate.STRING)
				.addScalar("job", Hibernate.STRING)
				.addScalar("billtoname", Hibernate.STRING)
				.addScalar("coordinator", Hibernate.STRING)
				.addScalar("origincountry", Hibernate.STRING)
				.addScalar("origincity", Hibernate.STRING)
				.addScalar("destinationcountry", Hibernate.STRING)
				.addScalar("destinationcity", Hibernate.STRING)
				.addScalar("createdon", Hibernate.DATE)
				.addScalar("createdby", Hibernate.STRING)
				.addScalar("node", Hibernate.STRING)
				.addScalar("companydivision", Hibernate.STRING)
				.addScalar("registrationnumber", Hibernate.STRING)
				.addScalar("id", Hibernate.LONG)
				.addScalar("personPricing", Hibernate.STRING)
				.addScalar("quotationStatus", Hibernate.STRING)
				.addScalar("email", Hibernate.STRING)
				.addScalar("accountName", Hibernate.STRING)
				.addScalar("moveType", Hibernate.STRING)
				.addScalar("orderIntiationStatus",Hibernate.STRING)
				.list();
		
				
		Iterator it = list.iterator();
		UniversalSearchDTO dto = null;
		if(usertype.equalsIgnoreCase("ACCOUNT")){

			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				if(row[0]!=null && ((row[0].toString().trim().equalsIgnoreCase("CF")) || (row[0].toString().trim().equalsIgnoreCase("OR")))){
				dto = new UniversalSearchDTO();
				dto.setType(row[0].toString().trim());
				dto.setSequencenumber(row[1]);
				dto.setStatus(row[2]);
				dto.setFirstname(row[3]);
				dto.setLastname(row[4]);
				dto.setJob(row[5]);
				if (row[6] == null) {
					dto.setBilltoname("");
				} else {
					dto.setBilltoname(row[6]);
				}
				dto.setCoordinator(row[7]);
				dto.setOrigincountry(row[8]);
				dto.setOrigincity(row[9]);
				dto.setDestinationcountry(row[10]);
				dto.setDestinationcity(row[11]);
				dto.setCreatedon(row[12]);
				dto.setCreatedby(row[13]);
				dto.setNode(row[14]);
				dto.setCompanydivision(row[15]);
				if (row[16] == null) {
					dto.setRegistrationnumber("");
				} else {
					dto.setRegistrationnumber(row[16]);
				}
				dto.setId(row[17]);
				if (row[18] == null) {
					dto.setPersonPricing("");
				} else {
					dto.setPersonPricing(row[18]);
				}
				dto.setQuotationStatus(row[19]);
				dto.setEmail(row[20]);
				dto.setAccountName(row[21]);
				dto.setMoveType(row[22]);
				universalSearchList.add(dto);
				}

			}
				
		}else{
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			if(row[0]!=null && ((row[0].toString().trim().equalsIgnoreCase("OR"))) && (row[23]!=null && ((row[23].toString().trim().equalsIgnoreCase("Submitted"))))){
			dto = new UniversalSearchDTO();
			dto.setType(row[0].toString().trim());
			dto.setSequencenumber(row[1]);
			dto.setStatus(row[2]);
			dto.setFirstname(row[3]);
			dto.setLastname(row[4]);
			dto.setJob(row[5]);
			if (row[6] == null) {
				dto.setBilltoname("");
			} else {
				dto.setBilltoname(row[6]);
			}
			dto.setCoordinator(row[7]);
			dto.setOrigincountry(row[8]);
			dto.setOrigincity(row[9]);
			dto.setDestinationcountry(row[10]);
			dto.setDestinationcity(row[11]);
			dto.setCreatedon(row[12]);
			dto.setCreatedby(row[13]);
			dto.setNode(row[14]);
			dto.setCompanydivision(row[15]);
			if (row[16] == null) {
				dto.setRegistrationnumber("");
			} else {
				dto.setRegistrationnumber(row[16]);
			}
			dto.setId(row[17]);
			if (row[18] == null) {
				dto.setPersonPricing("");
			} else {
				dto.setPersonPricing(row[18]);
			}
			dto.setQuotationStatus(row[19]);
			dto.setEmail(row[20]);
			dto.setAccountName(row[21]);
			dto.setMoveType(row[22]);
			universalSearchList.add(dto);
			}else if(row[0]!=null && (!(row[0].toString().trim().equalsIgnoreCase("OR")))){
				dto = new UniversalSearchDTO();
				dto.setType(row[0].toString().trim());
				dto.setSequencenumber(row[1]);
				dto.setStatus(row[2]);
				dto.setFirstname(row[3]);
				dto.setLastname(row[4]);
				dto.setJob(row[5]);
				if (row[6] == null) {
					dto.setBilltoname("");
				} else {
					dto.setBilltoname(row[6]);
				}
				dto.setCoordinator(row[7]);
				dto.setOrigincountry(row[8]);
				dto.setOrigincity(row[9]);
				dto.setDestinationcountry(row[10]);
				dto.setDestinationcity(row[11]);
				dto.setCreatedon(row[12]);
				dto.setCreatedby(row[13]);
				dto.setNode(row[14]);
				dto.setCompanydivision(row[15]);
				if (row[16] == null) {
					dto.setRegistrationnumber("");
				} else {
					dto.setRegistrationnumber(row[16]);
				}
				dto.setId(row[17]);
				if (row[18] == null) {
					dto.setPersonPricing("");
				} else {
					dto.setPersonPricing(row[18]);
				}
				dto.setQuotationStatus(row[19]);
				dto.setEmail(row[20]);
				dto.setAccountName(row[21]);
				dto.setMoveType(row[22]);
				universalSearchList.add(dto);
			}
		}
		}
	} catch (Exception e) {
		logger.error("Error executing query"+ e,e);
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
  	 e.printStackTrace();
	}
		return universalSearchList;
	}
	
	public String getNetworkCoordinator(String externalCorpID) {
		String networkCoordinator="";
		try {
			List networkCoordinatorList = new ArrayList();
			networkCoordinatorList = this
					.getSession()
					.createSQLQuery(
							"select networkCoordinator from systemdefault where networkCoordinator is not null and corpID = '"
									+ externalCorpID + "'").list();
			if (networkCoordinatorList != null
					&& (!(networkCoordinatorList.isEmpty()))
					&& networkCoordinatorList.get(0) != null) {
				networkCoordinator = networkCoordinatorList.get(0).toString();
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return networkCoordinator; 
	}

	public String getSystemVolumeUnit(String sessionCorpID) {
		String VolumeUnit="Cft";
		try {
			List VolumeUnitList = new ArrayList();
			VolumeUnitList = getHibernateTemplate().find(
					"select volumeUnit from SystemDefault where corpID = ?",
					sessionCorpID);
			if (VolumeUnitList != null && (!(VolumeUnitList.isEmpty()))
					&& VolumeUnitList.get(0) != null) {
				VolumeUnit = VolumeUnitList.get(0).toString();
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return VolumeUnit;
	}

	public String getSystemWeightUnit(String sessionCorpID) {
		String WeightUnit="Lbs";
		try {
			List WeightUnitList = new ArrayList();
			WeightUnitList = getHibernateTemplate().find(
					"select weightUnit from SystemDefault where corpID = ?",
					sessionCorpID);
			if (WeightUnitList != null && (!(WeightUnitList.isEmpty()))
					&& WeightUnitList.get(0) != null) {
				WeightUnit = WeightUnitList.get(0).toString();
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return WeightUnit;
	}
	public class QuotationServiceOrder{
		private Object id;
		private Object shipNumber;
		private Object routing;
		private Object commodity;
		private Object job;
		private Object mode;
		private Object registrationNumber;
		private Object estimateGrossWeight;
		private Object estimateGrossWeightKilo;
		private Object estimateCubicFeet;
		private Object estimateCubicMtr;
		private Object estimatedTotalExpense;
		private Object estimatedTotalRevenue;
		private Object estimatedGrossMargin;
		private Object quoteAccept;
		private Object sequenceNumber;
		private Object packingMode;
		private Object quoteStatus;
		private Object statusReason;
		private Object bookingAgentCode;
		private Object billToCode; 
		private Object estimatedNetWeight;
		private Object unit1;
		private Object actualCubicFeet;
		private Object quoteAcceptReason;
		private Object companyDivision;
		
		public Object getEstimatedNetWeight() {
			return estimatedNetWeight;
		}
		public void setEstimatedNetWeight(Object estimatedNetWeight) {
			this.estimatedNetWeight = estimatedNetWeight;
		}
		public Object getUnit1() {
			return unit1;
		}
		public void setUnit1(Object unit1) {
			this.unit1 = unit1;
		}
		public Object getActualCubicFeet() {
			return actualCubicFeet;
		}
		public void setActualCubicFeet(Object actualCubicFeet) {
			this.actualCubicFeet = actualCubicFeet;
		}
		
		public Object getCommodity() {
			return commodity;
		}
		public void setCommodity(Object commodity) {
			this.commodity = commodity;
		}
		public Object getEstimateCubicFeet() {
			return estimateCubicFeet;
		}
		public void setEstimateCubicFeet(Object estimateCubicFeet) {
			this.estimateCubicFeet = estimateCubicFeet;
		}
		public Object getEstimateCubicMtr() {
			return estimateCubicMtr;
		}
		public void setEstimateCubicMtr(Object estimateCubicMtr) {
			this.estimateCubicMtr = estimateCubicMtr;
		}
		public Object getEstimatedGrossMargin() {
			return estimatedGrossMargin;
		}
		public void setEstimatedGrossMargin(Object estimatedGrossMargin) {
			this.estimatedGrossMargin = estimatedGrossMargin;
		}
		public Object getEstimatedTotalExpense() {
			return estimatedTotalExpense;
		}
		public void setEstimatedTotalExpense(Object estimatedTotalExpense) {
			this.estimatedTotalExpense = estimatedTotalExpense;
		}
		public Object getEstimatedTotalRevenue() {
			return estimatedTotalRevenue;
		}
		public void setEstimatedTotalRevenue(Object estimatedTotalRevenue) {
			this.estimatedTotalRevenue = estimatedTotalRevenue;
		}
		public Object getEstimateGrossWeight() {
			return estimateGrossWeight;
		}
		public void setEstimateGrossWeight(Object estimateGrossWeight) {
			this.estimateGrossWeight = estimateGrossWeight;
		}
		public Object getEstimateGrossWeightKilo() {
			return estimateGrossWeightKilo;
		}
		public void setEstimateGrossWeightKilo(Object estimateGrossWeightKilo) {
			this.estimateGrossWeightKilo = estimateGrossWeightKilo;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getJob() {
			return job;
		}
		public void setJob(Object job) {
			this.job = job;
		}
		public Object getMode() {
			return mode;
		}
		public void setMode(Object mode) {
			this.mode = mode;
		}
		public Object getQuoteAccept() {
			return quoteAccept;
		}
		public void setQuoteAccept(Object quoteAccept) {
			this.quoteAccept = quoteAccept;
		}
		public Object getRouting() {
			return routing;
		}
		public void setRouting(Object routing) {
			this.routing = routing;
		}
		public Object getSequenceNumber() {
			return sequenceNumber;
		}
		public void setSequenceNumber(Object sequenceNumber) {
			this.sequenceNumber = sequenceNumber;
		}
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		public Object getPackingMode() {
			return packingMode;
		}
		public void setPackingMode(Object packingMode) {
			this.packingMode = packingMode;
		}
		public Object getQuoteStatus() {
			return quoteStatus;
		}
		public void setQuoteStatus(Object quoteStatus) {
			this.quoteStatus = quoteStatus;
		}
		public Object getStatusReason() {
			return statusReason;
		}
		public void setStatusReason(Object statusReason) {
			this.statusReason = statusReason;
		}
		public Object getBookingAgentCode() {
			return bookingAgentCode;
		}
		public void setBookingAgentCode(Object bookingAgentCode) {
			this.bookingAgentCode = bookingAgentCode;
		}
		public Object getBillToCode() {
			return billToCode;
		}
		public void setBillToCode(Object billToCode) {
			this.billToCode = billToCode;
		}
		public Object getQuoteAcceptReason() {
			return quoteAcceptReason;
		}
		public void setQuoteAcceptReason(Object quoteAcceptReason) {
			this.quoteAcceptReason = quoteAcceptReason;
		}
		public Object getCompanyDivision() {
			return companyDivision;
		}
		public void setCompanyDivision(Object companyDivision) {
			this.companyDivision = companyDivision;
		}
		public Object getRegistrationNumber() {
			return registrationNumber;
		}
		public void setRegistrationNumber(Object registrationNumber) {
			this.registrationNumber = registrationNumber;
		}
	}
	public List getQuotationServiceOrderlist(Long id, String sessionCorpID) {
		List <Object> quotationServiceOrderlist = new ArrayList();
		try {
			List list = new ArrayList();
			String query = "select s.id,s.shipNumber,s.routing,s.commodity,s.job,s.mode,m.estimateGrossWeight,m.estimateGrossWeightKilo,m.estimateCubicFeet,m.estimateCubicMtr,s.estimatedTotalExpense,s.estimatedTotalRevenue,s.estimatedGrossMargin,s.quoteAccept,s.sequenceNumber,s.packingMode,s.quoteStatus,s.statusReason,s.bookingAgentCode,s.billToCode,m.estimatedNetWeight,m.unit1,m.actualCubicFeet,s.quoteAcceptReason,s.companyDivision,s.registrationNumber from serviceorder s, miscellaneous m where s.id=m.id and s.customerFileId='"
					+ id + "' and s.corpid='" + sessionCorpID + "'";
			list = this
					.getSession()
					.createSQLQuery(query)
					.addScalar("s.id", Hibernate.LONG)
					.addScalar("s.shipNumber", Hibernate.STRING)
					.addScalar("s.routing", Hibernate.STRING)
					.addScalar("s.commodity", Hibernate.STRING)
					.addScalar("s.job", Hibernate.STRING)
					.addScalar("s.mode", Hibernate.STRING)
					.addScalar("m.estimateGrossWeight", Hibernate.BIG_DECIMAL)
					.addScalar("m.estimateGrossWeightKilo",
							Hibernate.BIG_DECIMAL)
					.addScalar("m.estimateCubicFeet", Hibernate.BIG_DECIMAL)
					.addScalar("m.estimateCubicMtr", Hibernate.BIG_DECIMAL)
					.addScalar("s.estimatedTotalExpense", Hibernate.BIG_DECIMAL)
					.addScalar("s.estimatedTotalRevenue", Hibernate.BIG_DECIMAL)
					.addScalar("s.estimatedGrossMargin", Hibernate.BIG_DECIMAL)
					.addScalar("s.quoteAccept", Hibernate.STRING)
					.addScalar("s.sequenceNumber", Hibernate.STRING)
					.addScalar("s.packingMode", Hibernate.STRING)
					.addScalar("s.quoteStatus", Hibernate.STRING)
					.addScalar("s.statusReason", Hibernate.STRING)
					.addScalar("s.bookingAgentCode", Hibernate.STRING)
					.addScalar("s.billToCode", Hibernate.STRING)
					.addScalar("m.estimatedNetWeight", Hibernate.BIG_DECIMAL)
					.addScalar("m.unit1", Hibernate.STRING)
					.addScalar("m.actualCubicFeet", Hibernate.BIG_DECIMAL)
					.addScalar("s.quoteAcceptReason", Hibernate.STRING)
					.addScalar("s.companyDivision", Hibernate.STRING)
						.addScalar("s.registrationNumber", Hibernate.STRING)
					.list();
			Iterator it = list.iterator();
			QuotationServiceOrder quotationServiceOrder = null;
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				quotationServiceOrder = new QuotationServiceOrder();
				quotationServiceOrder.setId(row[0]);
				quotationServiceOrder.setShipNumber(row[1]);
				quotationServiceOrder.setRouting(row[2]);
				quotationServiceOrder.setCommodity(row[3]);
				quotationServiceOrder.setJob(row[4]);
				quotationServiceOrder.setMode(row[5]);
				quotationServiceOrder.setEstimateGrossWeight(row[6]);
				quotationServiceOrder.setEstimateGrossWeightKilo(row[7]);
				quotationServiceOrder.setEstimateCubicFeet(row[8]);
				quotationServiceOrder.setEstimateCubicMtr(row[9]);
				quotationServiceOrder.setEstimatedTotalExpense(row[10]);
				quotationServiceOrder.setEstimatedTotalRevenue(row[11]);
				quotationServiceOrder.setEstimatedGrossMargin(row[12]);
				quotationServiceOrder.setQuoteAccept(row[13]);
				quotationServiceOrder.setSequenceNumber(row[14]);
				quotationServiceOrder.setPackingMode(row[15]);
				quotationServiceOrder.setQuoteStatus(row[16]);
				quotationServiceOrder.setStatusReason(row[17]);
				quotationServiceOrder.setBookingAgentCode(row[18]);
				quotationServiceOrder.setBillToCode(row[19]);
				quotationServiceOrder.setEstimatedNetWeight(row[20]);
				quotationServiceOrder.setUnit1(row[21]);
				quotationServiceOrder.setActualCubicFeet(row[22]);
				quotationServiceOrder.setQuoteAcceptReason(row[23]);
				quotationServiceOrder.setCompanyDivision(row[24]);
				quotationServiceOrder.setRegistrationNumber(row[25]);
				quotationServiceOrderlist.add(quotationServiceOrder);
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return quotationServiceOrderlist;
	}

	public int updateSOStatusREOPEN(String sequenceNumber,  String sessionCorpID) {
		try {
			return getHibernateTemplate()
					.bulkUpdate(
							"update ServiceOrder set status='REOP',  statusNumber='1' where status='CNCL' and sequenceNumber='"
									+ sequenceNumber
									+ "' and corpID='"
									+ sessionCorpID + "' ");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return 0;
	}
	public String findRemovalReloCheck(String customerFileId){
	try {
		List list = this
				.getSession()
				.createSQLQuery(
						"select id from removalrelocationservice where id='"
								+ customerFileId + "';").list();
		if (list.isEmpty() || list.get(0) == null) {
			return "false";
		} else {
			if (list.get(0).toString().equals(customerFileId)) {
				return "true";
			} else {
				return "false";
			}
		}
	} catch (Exception e) {
		logger.error("Error executing query"+ e,e);
		getSessionCorpID();
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	    String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	    saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
  	    e.printStackTrace();
	}
	return "";
	}

	public String getNewEventFrom() {
		return newEventFrom;
	}

	public void setNewEventFrom(String newEventFrom) {
		this.newEventFrom = newEventFrom;
	}

	public StringBuilder getNewEventFromBuilder() {
		return newEventFromBuilder;
	}

	public void setNewEventFromBuilder(StringBuilder newEventFromBuilder) {
		this.newEventFromBuilder = newEventFromBuilder;
	}

	public String getNewEventTo() {
		return newEventTo;
	}

	public void setNewEventTo(String newEventTo) {
		this.newEventTo = newEventTo;
	}

	public StringBuilder getNewEventToBuilder() {
		return newEventToBuilder;
	}

	public void setNewEventToBuilder(StringBuilder newEventToBuilder) {
		this.newEventToBuilder = newEventToBuilder;
	}
	public class QuotesToGoDTO{
		private Object vanlinecode;
		private Object sequenceBookingAgentCode;
		private Object source;
		private Object sequenceNumber;
		private Object firstName;
		private Object lastName;
		private Object middleInitial;
		private Object prefix;
		private Object originHomePhone;
		private Object originMobile;
		private Object originFax;
		private Object email;
		private Object originAddress1;
		private Object originAddress2;
		private Object originCity;
		private Object originState;
		private Object originZip;
		private Object destinationAddress1;
		private Object destinationAddress2;
		private Object destinationCity;
		private Object destinationState;
		private Object destinationZip;
		private Object moveDate;
		private Object billToCode;
		private Object customerPhoneExt;
		private Object customerAlternatePhoneExt;

		public Object getBillToCode() {
			return billToCode;
		}
		public void setBillToCode(Object billToCode) {
			this.billToCode = billToCode;
		}
		public Object getDestinationAddress1() {
			return destinationAddress1;
		}
		public void setDestinationAddress1(Object destinationAddress1) {
			this.destinationAddress1 = destinationAddress1;
		}
		public Object getDestinationAddress2() {
			return destinationAddress2;
		}
		public void setDestinationAddress2(Object destinationAddress2) {
			this.destinationAddress2 = destinationAddress2;
		}
		public Object getDestinationCity() {
			return destinationCity;
		}
		public void setDestinationCity(Object destinationCity) {
			this.destinationCity = destinationCity;
		}
		public Object getDestinationState() {
			return destinationState;
		}
		public void setDestinationState(Object destinationState) {
			this.destinationState = destinationState;
		}
		public Object getDestinationZip() {
			return destinationZip;
		}
		public void setDestinationZip(Object destinationZip) {
			this.destinationZip = destinationZip;
		}
		public Object getEmail() {
			return email;
		}
		public void setEmail(Object email) {
			this.email = email;
		}
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getMiddleInitial() {
			return middleInitial;
		}
		public void setMiddleInitial(Object middleInitial) {
			this.middleInitial = middleInitial;
		}
		public Object getMoveDate() {
			return moveDate;
		}
		public void setMoveDate(Object moveDate) {
			this.moveDate = moveDate;
		}
		public Object getOriginAddress1() {
			return originAddress1;
		}
		public void setOriginAddress1(Object originAddress1) {
			this.originAddress1 = originAddress1;
		}
		public Object getOriginAddress2() {
			return originAddress2;
		}
		public void setOriginAddress2(Object originAddress2) {
			this.originAddress2 = originAddress2;
		}
		public Object getOriginCity() {
			return originCity;
		}
		public void setOriginCity(Object originCity) {
			this.originCity = originCity;
		}
		public Object getOriginFax() {
			return originFax;
		}
		public void setOriginFax(Object originFax) {
			this.originFax = originFax;
		}
		public Object getOriginHomePhone() {
			return originHomePhone;
		}
		public void setOriginHomePhone(Object originHomePhone) {
			this.originHomePhone = originHomePhone;
		}
		public Object getOriginMobile() {
			return originMobile;
		}
		public void setOriginMobile(Object originMobile) {
			this.originMobile = originMobile;
		}
		public Object getOriginState() {
			return originState;
		}
		public void setOriginState(Object originState) {
			this.originState = originState;
		}
		public Object getOriginZip() {
			return originZip;
		}
		public void setOriginZip(Object originZip) {
			this.originZip = originZip;
		}
		public Object getPrefix() {
			return prefix;
		}
		public void setPrefix(Object prefix) {
			this.prefix = prefix;
		}
		public Object getSequenceBookingAgentCode() {
			return sequenceBookingAgentCode;
		}
		public void setSequenceBookingAgentCode(Object sequenceBookingAgentCode) {
			this.sequenceBookingAgentCode = sequenceBookingAgentCode;
		}
		public Object getSequenceNumber() {
			return sequenceNumber;
		}
		public void setSequenceNumber(Object sequenceNumber) {
			this.sequenceNumber = sequenceNumber;
		}
		public Object getSource() {
			return source;
		}
		public void setSource(Object source) {
			this.source = source;
		}
		public Object getVanlinecode() {
			return vanlinecode;
		}
		public void setVanlinecode(Object vanlinecode) {
			this.vanlinecode = vanlinecode;
		}
		public Object getCustomerPhoneExt() {
			return customerPhoneExt;
		}
		public void setCustomerPhoneExt(Object customerPhoneExt) {
			this.customerPhoneExt = customerPhoneExt;
		}
		public Object getCustomerAlternatePhoneExt() {
			return customerAlternatePhoneExt;
		}
		public void setCustomerAlternatePhoneExt(Object customerAlternatePhoneExt) {
			this.customerAlternatePhoneExt = customerAlternatePhoneExt;
		}
		}
	public void updateVoxmeFlag(Long id){
		try {
			getHibernateTemplate().bulkUpdate(
					"update CustomerFile set voxmeFlag='Y' where id='" + id
							+ "'");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
	}
	
	
	public List getQuotesToGoList(Long id, String sessionCorpID,String companyDivision,String bookingAgentCode){
		List sendToQuotesToGoList=new ArrayList();
		try {
			List list = this
					.getSession()
					.createSQLQuery(
							"select cm.vanlinecode,concat(c.sequenceNumber ,'AAAAA',c.bookingAgentCode) as sequenceBookingAgentCode, "
									+ "c.source,c.sequenceNumber,c.firstName,c.lastName,c.middleInitial,c.prefix,if(c.originHomePhone is not null and c.originHomePhone!='',c.originHomePhone,if(c.originMobile is not null and c.originMobile!='',c.originMobile,c.originDayPhone)) as originHomePhone, "
									+ "if(c.originHomePhone is not null and c.originHomePhone!='',c.originMobile,if(c.originMobile is not null and c.originMobile!='',c.originDayPhone,'')) as originMobile , "
									+ "c.originFax,c.email,c.originAddress1,c.originAddress2,c.originCity,c.originState,c.originZip, "
									+ "c.destinationAddress1,c.destinationAddress2,c.destinationCity,c.destinationState, "
									+ "c.destinationZip,c.moveDate,c.billToCode , "
									+ "if((c.originHomePhone is null or c.originHomePhone='') and (c.originMobile is null or c.originMobile='') and (c.originDayPhone is not null and c.originDayPhone!=''),c.originDayPhoneExt,'') as customerPhoneExt , "
									+ "if((c.originHomePhone is null or c.originHomePhone='') and (c.originMobile is not null and  c.originMobile!='') and (c.originDayPhone is not null and c.originDayPhone!=''),c.originDayPhoneExt,'') as customerAlternatePhoneExt "
									+ "from customerfile c, companydivision cm "
									+ "where c.id='"
									+ id
									+ "' and cm.corpid='"
									+ sessionCorpID
									+ "' and cm.bookingAgentCode='"
									+ bookingAgentCode + "' limit 1")
					.addScalar("cm.vanlinecode", Hibernate.STRING)
					.addScalar("sequenceBookingAgentCode", Hibernate.STRING)
					.addScalar("c.source", Hibernate.STRING)
					.addScalar("c.sequenceNumber", Hibernate.STRING)
					.addScalar("c.firstName", Hibernate.STRING)
					.addScalar("c.lastName", Hibernate.STRING)
					.addScalar("c.middleInitial", Hibernate.STRING)
					.addScalar("c.prefix", Hibernate.STRING)
					.addScalar("originHomePhone", Hibernate.STRING)
					.addScalar("originMobile", Hibernate.STRING)
					.addScalar("c.originFax", Hibernate.STRING)
					.addScalar("c.email", Hibernate.STRING)
					.addScalar("c.originAddress1", Hibernate.STRING)
					.addScalar("c.originAddress2", Hibernate.STRING)
					.addScalar("c.originCity", Hibernate.STRING)
					.addScalar("c.originState", Hibernate.STRING)
					.addScalar("c.originZip", Hibernate.STRING)
					.addScalar("c.destinationAddress1", Hibernate.STRING)
					.addScalar("c.destinationAddress2", Hibernate.STRING)
					.addScalar("c.destinationCity", Hibernate.STRING)
					.addScalar("c.destinationState", Hibernate.STRING)
					.addScalar("c.destinationZip", Hibernate.STRING)
					.addScalar("c.moveDate", Hibernate.DATE)
					.addScalar("c.billToCode", Hibernate.STRING)
					.addScalar("customerPhoneExt", Hibernate.STRING)
					.addScalar("customerAlternatePhoneExt", Hibernate.STRING)
					.list();
			Iterator it = list.iterator();
			QuotesToGoDTO qotesDto = null;
			while (it.hasNext()) {
				Object[] obj = (Object[]) it.next();
				qotesDto = new QuotesToGoDTO();
				qotesDto.setVanlinecode(obj[0]);
				qotesDto.setSequenceBookingAgentCode(obj[1]);
				qotesDto.setSource(obj[2]);
				List souceFlex = this
						.getSession()
						.createSQLQuery(
								"select if(flex1 is null or flex1='','15',flex1) from refmaster where parameter='LEAD' and code='"
										+ obj[2]
										+ "'  and corpID='"
										+ sessionCorpID + "' ").list();
				if (souceFlex != null && !souceFlex.isEmpty()) {
					qotesDto.setSource(souceFlex.get(0).toString());
				} else {
					qotesDto.setSource("15");
				}
				qotesDto.setSequenceNumber(obj[3]);
				qotesDto.setFirstName(obj[4]);
				qotesDto.setLastName(obj[5]);
				qotesDto.setMiddleInitial(obj[6]);
				qotesDto.setPrefix(obj[7]);
				qotesDto.setOriginHomePhone(obj[8]);
				qotesDto.setOriginMobile(obj[9]);
				qotesDto.setOriginFax(obj[10]);
				qotesDto.setEmail(obj[11]);
				qotesDto.setOriginAddress1(obj[12]);
				qotesDto.setOriginAddress2(obj[13]);
				qotesDto.setOriginCity(obj[14]);
				qotesDto.setOriginState(obj[15]);
				qotesDto.setOriginZip(obj[16]);
				qotesDto.setDestinationAddress1(obj[17]);
				qotesDto.setDestinationAddress2(obj[18]);
				qotesDto.setDestinationCity(obj[19]);
				qotesDto.setDestinationState(obj[20]);
				qotesDto.setDestinationZip(obj[21]);
				qotesDto.setMoveDate(obj[22]);
				qotesDto.setBillToCode(obj[23]);
				List ppBillToCode = this
						.getSession()
						.createSQLQuery(
								"select * from partnerpublic where partnerCode='"
										+ obj[23]
										+ "' and isPrivateParty is true")
						.list();
				if (ppBillToCode != null && !ppBillToCode.isEmpty()) {
					qotesDto.setBillToCode("1");
				} else {
					qotesDto.setBillToCode("2");
				}
				qotesDto.setCustomerPhoneExt(obj[24]);
				qotesDto.setCustomerAlternatePhoneExt(obj[25]);
				sendToQuotesToGoList.add(qotesDto);
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return sendToQuotesToGoList;
	}
	public List getQuotesToValidate(String sessionCorpID,String jobCode, String companyDivision){
		try {
			return getHibernateTemplate().find(
					"select surveyTool from RefJobType where corpID='"
							+ sessionCorpID + "' and job='" + jobCode
							+ "' and companyDivision='" + companyDivision
							+ "' ");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	public void updateCustomerFileRegNumber(String trackingCodeValue,Long customerFileId, String corpID){
		try {
			getHibernateTemplate().bulkUpdate(
					"update CustomerFile set estimateNumber='"
							+ trackingCodeValue
							+ "',quotesToGoFlag=true where id="
							+ customerFileId + " and corpID='" + corpID + "' ");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
	}
	public String findSurveyTool(String corpID, String job, String companyDivision){
		try {
			return getHibernateTemplate().find(
					"select surveyTool from RefJobType where corpID='" + corpID
							+ "' and job='" + job + "' and companyDivision='"
							+ companyDivision + "'").toString();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return "";
	}
	public List findResourceIdPassword(String bookingAgentCode,String corpId){
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select concat(if(resourceID is null or resourceID='','1',resourceID),'~',if(resourcePassword is null or resourcePassword='','1',resourcePassword)) from companydivision where bookingAgentCode='"
									+ bookingAgentCode
									+ "' and corpID='"
									+ corpId + "' ").list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	  e.printStackTrace();
		}
		return null;
	}
	public String getMessageText(String userPhone, String companyDivision, String sessionCorpID){
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select if(textMessage is null or textMessage='','1',textMessage) from companydivision where companyCode='"
									+ companyDivision
									+ "' and corpID='"
									+ sessionCorpID + "' ").list().get(0)
					.toString();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	  e.printStackTrace();
		}
		return "";
	}
	
	public void updateServiceOrderBillToCode(String billTocode,String billToName,String sequenceNumber, String corpID){
		try {
			String query = "update ServiceOrder set billToCode='"
					+ billTocode.replaceAll("'", "") + "' , billToName='"
					+ billToName.replaceAll("'", "")
					+ "' where sequenceNumber='" + sequenceNumber
					+ "' and corpID='" + corpID + "'";
			getHibernateTemplate().bulkUpdate(query);
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	  e.printStackTrace();
		}
	}
	public void updateBillingBillToCode(String billTocode,String billToName,String sequenceNumber, String corpID){
		try {
			String query = "update Billing set billToCode='"
					+ billTocode.replaceAll("'", "") + "' , billToName='"
					+ billToName.replaceAll("'", "")
					+ "' where sequenceNumber='" + sequenceNumber
					+ "' and corpID='" + corpID + "'";
			getHibernateTemplate().bulkUpdate(query);
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
	}
	public int quoteReasonStatus(String corpID,Long id){
		int i=0;
		try {
			List l = getHibernateTemplate().find(
					"select count(*) from ServiceOrder where (quoteAccept='R'or quoteAccept='A') and customerFileId='"
							+ id + "' and corpID='" + corpID + "' ");
			if (l != null && !l.isEmpty()) {
				i = Integer.parseInt(l.get(0).toString());
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return i;
	}
	
	  public Long findNewRemoteCustomerFile(String sequenceNumber, String externalCorpID) {
		Long cid=0L;
			try {
				List custList = this
						.getSession()
						.createSQLQuery(
								"select id from customerfile where sequenceNumber='"
										+ sequenceNumber + "' and  corpID='"
										+ externalCorpID
										+ "' and  status!='CNCL' ").list();
				if (!custList.isEmpty()) {
					cid = Long.parseLong(custList.get(0).toString());
				}
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				getSessionCorpID();
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	e.printStackTrace();
			}
			return cid;
		}
		public List  BookingAgentName(String partnerCode,String corpid,Boolean cmmDmmFlag,String pType){
			try {
				List list3 = new ArrayList();
				String bookingAgentSet="";
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		        User user = (User)auth.getPrincipal();
		        Map filterMap=user.getFilterMap(); 
		    	List userList=new ArrayList();
		    	try {
				if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
		    		list3.add("'" + code + "'");	
		    		}
		    	}
		    	bookingAgentSet = list3.toString().replace("[","").replace("]", "");
		    	}
		    	}catch (Exception e) {
					logger.error("Error executing query"+ e,e);
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  saveLogError(corpid,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			    	 e.printStackTrace();
				}
		    	String bookingAgentCondision="  ";
		    	if (bookingAgentSet != null && !bookingAgentSet.equals("") ) {
		    		bookingAgentCondision="  and partnerCode in("+ bookingAgentSet + ")   ";
		    	}
		    	String bookingAgentValidSearchCondision="  ";
		    	String agentSearchCheck = this.getSession().createSQLQuery("select if(agentSearchValidation,'T','F') from systemdefault where corpid='"+user.getCorpID()+"' ").list().get(0).toString();
		    	if(agentSearchCheck.equals("T") && pType !=null && pType.trim().equalsIgnoreCase("AG")){
		    		bookingAgentValidSearchCondision=" and isAgent is true";
		    	}
				return this
						.getSession()
						.createSQLQuery(
								"select lastName, status, companyDivision, if(terminalEmail is null , '' ,terminalEmail), if(trackingUrl is null , '' ,trackingUrl), paymentMethod  from partner where  partnerCode='"
										+ partnerCode
										+ "' and corpID in ('TSFT','"
										+ hibernateUtil.getParentCorpID(corpid)
										+ "','"
										+ corpid
										+ "') and status='Approved'"+bookingAgentCondision+bookingAgentValidSearchCondision).list();
				} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(corpid,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
	public List findAccFieldToSyncCreate(String modalClass , String type) {
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select concat(fieldName,'~',if(toFieldName is null or toFieldName='',fieldName,toFieldName)) from networkdatafields where modelName='"
									+ modalClass
									+ "' and transactionType = 'Create' and (type='' or type is null or type='"
									+ type + "' or type='CMM/DMM' ) ").list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}

	public List findAccFieldToSync(String modalClass, String type) {
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select concat(fieldName,'~',if(toFieldName is null or toFieldName='',fieldName,toFieldName)) from networkdatafields where modelName='"
									+ modalClass
									+ "' and transactionType='Update'  and (type='' or type is null or type='"
									+ type + "' or type='CMM/DMM')  ").list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	public List findBillingFieldToSyncCreate(String modalClass, String fieldType, String uniqueFieldType) { 
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select concat(fieldName,'~',if(toFieldName is null or toFieldName='',fieldName,toFieldName)) from networkdatafields where modelName='"
									+ modalClass
									+ "' and transactionType in ('Create','Update') and (type='' or type is null or type='"
									+ uniqueFieldType
									+ "' or type='"
									+ fieldType + "') ").list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	
	}

	public List findBillingFieldToSync(String modalClass, String fieldType, String uniqueFieldType) { 
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select concat(fieldName,'~',if(toFieldName is null or toFieldName='',fieldName,toFieldName)) from networkdatafields where modelName='"
									+ modalClass
									+ "' and transactionType='Update' and (type='' or type is null or type='"
									+ uniqueFieldType
									+ "' or type='"
									+ fieldType + "') ").list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	public List findAccDMMNetWorkFieldToSync(String modalClass, String type) {
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select concat(fieldName,'~',if(toFieldName is null or toFieldName='',fieldName,toFieldName)) from networkdatafields where modelName='"
									+ modalClass
									+ "' and transactionType='Update'  and (type='' or type is null or type='"
									+ type
									+ "' or type='CMM/DMM') and (toFieldName is null or toFieldName='')  ")
					.list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	public List getFlatFileExtraction(String fileType, String parentAgent1, String corpId,String fromDt, String toDt,String addChargesType,boolean checkFieldVisibility){
		try {
			String partnerCode = "";
			StringTokenizer st = new StringTokenizer(parentAgent1, ",");
			while (st.hasMoreTokens()) {
				partnerCode = "'" + st.nextToken() + "'," + partnerCode;
			}
			StringBuffer sb = new StringBuffer(partnerCode);
			partnerCode = sb.replace(partnerCode.length() - 1,
					partnerCode.length(), "").toString();
			List shipments = new ArrayList();
			String query = null;
			List<Object> active = new ArrayList<Object>();
			if ("flatFile".equalsIgnoreCase(fileType)) {
				query = "select so.shipnumber,if(so.quoteStatus is null ,'',so.quoteStatus),(select concat(pp.firstname,' ',pp.lastname) from partnerpublic pp where pp.partnercode=p.agentparent) ,"
					+ "b.billToName,if(c.createdby='Networking','Networking',(select concat(au.first_name,' ',au.last_name) from app_user au where au.username=c.createdby)), "
					+ "concat(so.firstname,' ',so.lastname), so.mode , "
					+ "so.originCountryCode,so.destinationCountryCode,so.originCity,so.destinationCity , "
					+ "b.authUpdated,c.actualSurveyDate,ts.packA ,ts.loadA,ts.deliveryA , "
					+ "(select receivedinvoicedate from accountline where shipnumber=so.shipnumber and receivedinvoicedate is not null order by receivedinvoicedate asc limit 1)  , "
					+ "m.netEntitleCubicMtr,m.netEstimateCubicMtr,m.netActualCubicMtr , "
					+ "so.distance,(select ROUND(sum(a.actualRevenue), 2) from accountline a where a.serviceorderid=so.id and recInvoiceNumber is not null and recInvoiceNumber != '' and a.corpid='"
					+ corpId
					+ "' and activateAccPortal is true), "
					+ "(select ROUND(sum(actualRevenue), 2) from accountline where serviceorderid=so.id and corpid='"
					+ corpId
					+ "' and category in('Origin','Destin','Freight', 'Inland Frt', 'Revenue', 'Transport', 'Insurance') and recInvoiceNumber is not null and recInvoiceNumber != '' and activateAccPortal is true), "
					+ "(select ROUND(sum(actualRevenue), 2) from accountline where serviceorderid=so.id and corpid ='"
					+ corpId
					+ "' and category not in('Origin','Destin','Freight', 'Inland Frt', 'Revenue', 'Transport', 'Insurance') and recInvoiceNumber is not null and recInvoiceNumber != '' and activateAccPortal is true), "
					+ "c.assignmenttype, "
					+ "so.recommendation,so.preMoveSurvey,so.originServices,so.destinationservices,so.overall, "
					+ "(select if((n.linkedto=so.shipnumber and n.noteSubType='Issue'),n.note,'')) , df.nationality "
					+ "from billing b,partnerpublic p,customerfile c left outer join dsfamilydetails df on df.customerFileId=c.id and df.relationship='self',trackingstatus ts,miscellaneous m,serviceorder so "
					+ "left outer join accountline al on al.serviceorderid = so.id "
					+ "left outer join  notes n on n.linkedto=so.shipnumber and n.noteSubType='Issue' "
					+ "where b.billtocode in("
					+ partnerCode
					+ ") and p.partnercode in("
					+ partnerCode
					+ ") and p.corpid in ('TSFT','"
					+ hibernateUtil.getParentCorpID(corpId)
					+ "','"
					+ corpId
					+ "')"
					+ "and b.corpid='"
					+ corpId
					+ "' and c.sequencenumber=b.sequencenumber and so.id=b.id "
					+ "and ts.id=so.id and m.id = so.id "
					+ "and c.createdon between  '"
					+ fromDt
					+ "' and '"
					+ toDt
					+ "'"
					+ "and (so.status not in ('CNCL') "
					+ "or so.statusreason not in ('Duplicate')) "
					+ " and (select concat(pp.firstname,' ',pp.lastname) from partnerpublic pp where pp.partnercode=p.agentparent) is not null group by so.shipnumber order by al.receivedInvoiceDate";
			shipments = this.getSession().createSQLQuery(query).list();
			active = getFlatFileRecode(shipments, corpId,checkFieldVisibility);
			} else if ("claimFile".equalsIgnoreCase(fileType)) {
				query = "select distinct concat(so.firstname,' ',so.lastname) as 'Name Client' , "
						+ "(select concat(pp.firstname,' ',pp.lastname) from partnerpublic pp where pp.partnercode=p.agentparent ) as 'Account' , "
						+ "so.shipnumber as 'S/O',cl.claimnumber as 'Claim #', l.idNumber1 as 'Loss ID#',concat(c.originCity,'(',c.originCountryCode,') -> ',c.destinationCity,'(',c.destinationCountryCode,')') as 'From To', "
						+ "if(cl.insuranceUser,'Yes','No') as 'Handled By Insurer', "
						+ "l.losstype as 'Type',l.losscomment as 'Cause', l.damagebyaction as 'Caused By',cl.requestForm as 'Date Received', "
						+ "l.requestedamount as 'Claimed Amount', "
						+ "l.requestedamountcurrency as 'Claimed Currency', "
						+ "l.paidCompensationcustomer as 'Paid Amount', "
						+ "l.paidcompensationcustomercurrency as 'Paid Currency',cl.status as 'Claim Status', if(cl.cancelled is not null,cl.cancelled, "
						+ "if(cl.closedate is not null,cl.closedate, if(cl.submitdate is not null,cl.submitdate,if(cl.requestform is not null,cl.requestform,cl.requestform)))) as 'Status Date', "
						+ "cl.remarks as 'Remark' "
						+ "from billing b, "
						+ "partnerpublic p,customerfile c, serviceorder so left outer join claim cl on cl.shipnumber = so.shipnumber and cl.corpid='"
						+ corpId
						+ "' "
						+ "left outer join loss l on l.claimid=cl.id and l.corpid='"
						+ corpId
						+ "' "
						+ "where b.billtocode in("
						+ partnerCode
						+ ") and p.partnercode in("
						+ partnerCode
						+ ") and b.billtocode=p.partnercode "
						+ "and p.corpid in ('TSFT','"
						+ hibernateUtil.getParentCorpID(corpId)
						+ "','"
						+ corpId
						+ "') and so.id=cl.serviceorderid and b.corpid='"
						+ corpId
						+ "'  and so.id = b.id "
						+ "and c.id=so.customerfileid and cl.settlementForm between '"
						+ fromDt
						+ "' and '"
						+ toDt
						+ "' "
						+ "and (so.status not in ('CNCL') "
						+ "or so.statusreason not in ('Duplicate')) "
						+ "order by so.shipnumber asc";
				shipments = this.getSession().createSQLQuery(query).list();
				active = getClaimRecode(shipments);
			} else if ("invoiceFile".equalsIgnoreCase(fileType)) {
				query = "select if(s.firstName='',s.lastName,if(s.lastName='',s.firstName,concat(s.lastName,',',s.firstName))) as name,s.originCountry,s.destinationCountry,a.chargeCode,a.recInvoiceNumber,a.receivedInvoiceDate,ROUND(a.actualRevenue, 2),ROUND(a.recVatAmt, 2),a.recVatPercent,ROUND((a.actualRevenue+a.recVatAmt), 2) as vatAmt,a.recRateCurrency "
						+ "from accountline a left outer join serviceorder s on a.serviceorderid=s.id and s.corpid='"
						+ corpId
						+ "' and s.billToCode in ("
						+ partnerCode
						+ ")"
						+ " left outer join billing b on a.serviceorderid=b.id and s.corpid='"
						+ corpId
						+ "' and b.billToCode in ("
						+ partnerCode
						+ ") "
						+ "where a.receivedInvoiceDate between '"
						+ fromDt
						+ "' and '"
						+ toDt
						+ "'  and a.corpid='"
						+ corpId						
						+ "' and (s.status not in ('CNCL') "
						+ "or s.statusreason not in ('Duplicate')) and a.activateAccPortal is true and a.billToCode in ("
						+ partnerCode + ")";
				shipments = this.getSession().createSQLQuery(query).list();
				active = getInvoiceFileRecode(shipments);

			} else if ("additionalCharges".equalsIgnoreCase(fileType)) {
				if ("accountlineInvoice".equalsIgnoreCase(addChargesType)) {
					String code = partnerCode.replaceAll("'", "");
					query = "select if(c.firstname is null || c.firstname='',c.lastname,concat(c.firstname,' ',c.lastname)) as 'Transferee', "
							+ "c.sequencenumber as 'Customer File', "
							+ "s.shipnumber as 'Service Order', "
							+ "ch.description as 'Cost', "
							+ "a.description as 'Description', a.recratecurrency as 'Currency', "
							+ "ROUND(a.actualrevenue, 2) as 'Amount', "
							+ " ROUND(FindsumofActualRevenueofallserviceorder(c.sequencenumber,s.shipnumber,'"
							+ code
							+ "','"
							+ fromDt
							+ "','"
							+ toDt
							+ "') , 2) as 'Total Amount' "
							+ " from customerfile c Inner Join serviceorder s on c.id=s.customerfileid and s.corpid='"
							+ corpId
							+ "' "
							+ "Inner Join billing b on s.id=b.id and b.corpid='"
							+ corpId
							+ "' "
							+ "Left Outer join accountline a on s.id=a.serviceorderid and a.corpid='"
							+ corpId
							+ "' "
							+ "and a.status is true and a.activateaccportal is true and a.actualrevenue!='0.00'"
							+ "Inner Join contract co on a.contract=co.contract and co.corpid='"
							+ corpId
							+ "' "
							+ "Inner Join charges ch on ch.charge=a.chargecode and ch.contract=co.contract and ch.corpid='"
							+ corpId
							+ "' "
							+ "Inner Join costelement ce on ch.costelement=ce.costelement and ce.corpid='"
							+ corpId
							+ "' "
							+ "where b.billtocode in("
							+ partnerCode
							+ ") and c.billtocode in("
							+ partnerCode
							+ ") and a.billtocode in("
							+ partnerCode
							+ ") and s.billtocode in("
							+ partnerCode
							+ ")"
							+ "and ce.reportingalias in ('Accessorials','broker') and a.chargecode not in('Prepaid services')"
							+ "and (a.recinvoicenumber is not null and a.recinvoicenumber!='') and date_format(a.receivedinvoicedate,'%Y-%m-%d') between '"
							+ fromDt
							+ "' and '"
							+ toDt
							+ "' "
							+ "and (s.status not in ('CNCL') "
							+ "or s.statusreason not in ('Duplicate')) "
							+ "and a.receivedinvoicedate is not null and c.corpid='"
							+ corpId + "' order by c.sequencenumber";

					
					shipments = this.getSession().createSQLQuery(query).list();
					Iterator it = shipments.iterator();
					AdditionalChargesDTO additionalDTO = null;
					while (it.hasNext()) {
						Object[] object = (Object[]) it.next();
						additionalDTO = new AdditionalChargesDTO();
						additionalDTO.setTransferee(object[0]);
						additionalDTO.setCustomerFile(object[1]);
						additionalDTO.setServiceOrder(object[2]);
						additionalDTO.setCost(object[3]);
						additionalDTO.setDescription(object[4]);
						additionalDTO.setCurrency(object[5]);
						additionalDTO.setAmount(object[6]);
						additionalDTO.setTotalAmount(object[7]);
						active.add(additionalDTO);
					}
				} else if ("customerFileInitiation"
						.equalsIgnoreCase(addChargesType)) {
					String code = partnerCode.replaceAll("'", "");
					query = "select if(c.firstname is null || c.firstname='',c.lastname,concat(c.firstname,' ',c.lastname)) as 'Transferee', "
							+ "c.sequencenumber as 'Customer File', "
							+ "s.shipnumber as 'Service Order', "
							+ "ch.description as 'Cost', "
							+ "a.description as 'Description', a.recratecurrency as 'Currency', "
							+ "ROUND(a.actualrevenue, 2) as 'Amount', "
							+ " ROUND( FindsumofActualRevenue_Customerfile_Initiation(c.sequencenumber,s.shipnumber,'"
							+ code
							+ "','"
							+ fromDt
							+ "','"
							+ toDt
							+ "') , 2) as 'Total Amount' "
							+ " from customerfile c Inner Join serviceorder s on c.id=s.customerfileid and s.corpid='"
							+ corpId
							+ "' "
							+ "Inner Join billing b on s.id=b.id and b.corpid='"
							+ corpId
							+ "' "
							+ "Left Outer join accountline a on s.id=a.serviceorderid and a.corpid='"
							+ corpId
							+ "' "
							+ "and a.status is true and a.activateaccportal is true and a.actualrevenue!='0.00'"
							+ "Inner Join contract co on a.contract=co.contract and co.corpid='"
							+ corpId
							+ "' "
							+ "Inner Join charges ch on ch.charge=a.chargecode and ch.contract=co.contract and ch.corpid='"
							+ corpId
							+ "' "
							+ "Inner Join costelement ce on ch.costelement=ce.costelement and ce.corpid='"
							+ corpId
							+ "' "
							+ "where b.billtocode in("
							+ partnerCode
							+ ") and c.billtocode in("
							+ partnerCode
							+ ") and a.billtocode in("
							+ partnerCode
							+ ") and s.billtocode in("
							+ partnerCode
							+ ")"
							+ "and ce.reportingalias in ('Accessorials','broker') and a.chargecode not in('Prepaid services')"
							+ "and (a.recinvoicenumber is not null and a.recinvoicenumber!='') and date_format(c.initialContactDate,'%Y-%m-%d') between '"
							+ fromDt
							+ "' and '"
							+ toDt
							+ "' "
							+ "and (s.status not in ('CNCL') "
							+ "or s.statusreason not in ('Duplicate')) "
							+ "and a.receivedinvoicedate is not null and c.corpid='"
							+ corpId + "' order by c.sequencenumber";

					shipments = this.getSession().createSQLQuery(query).list();
					Iterator it = shipments.iterator();
					AdditionalChargesDTO additionalDTO = null;
					while (it.hasNext()) {
						Object[] object = (Object[]) it.next();
						additionalDTO = new AdditionalChargesDTO();
						additionalDTO.setTransferee(object[0]);
						additionalDTO.setCustomerFile(object[1]);
						additionalDTO.setServiceOrder(object[2]);
						additionalDTO.setCost(object[3]);
						additionalDTO.setDescription(object[4]);
						additionalDTO.setCurrency(object[5]);
						additionalDTO.setAmount(object[6]);
						additionalDTO.setTotalAmount(object[7]);
						active.add(additionalDTO);
					}

				}
			}
			return active;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
		
	}
	private Boolean qualitySurvey = new Boolean(false);
	public boolean getQualitySurvey(String corpId){
		try {
			qualitySurvey = Boolean.parseBoolean(this
					.getSession()
					.createSQLQuery(
							"select qualitysurvey from company where corpid='"
									+ corpId + "'").list().get(0).toString());
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return qualitySurvey;
	}
	public List getInvoiceFileRecode(List shipments){
		try {
			invoiceFileExtractHibernateDTO hibernateDTO;
			Iterator it = shipments.iterator();
			List<Object> active = new ArrayList<Object>();
			while (it.hasNext()) {
				int count = 0;
				Object[] obj = (Object[]) it.next();
				hibernateDTO = new invoiceFileExtractHibernateDTO();
				hibernateDTO.setName(obj[0]);
				hibernateDTO.setOriginCountry(obj[1]);
				hibernateDTO.setDestinationCountry(obj[2]);
				hibernateDTO.setChargeCode(obj[3]);
				hibernateDTO.setRecInvoiceNumber(obj[4]);
				hibernateDTO.setReceivedInvoiceDate(obj[5]);
				hibernateDTO.setActualRevenue(obj[6]);
				hibernateDTO.setRecVatAmt(obj[7]);
				hibernateDTO.setRecVatPercent(obj[8]);
				hibernateDTO.setVatAmt(obj[9]);
				hibernateDTO.setRecRateCurrency(obj[10]);
				active.add(hibernateDTO);
			}
			return active;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	e.printStackTrace();
		}
		return null;
	}
	public class invoiceFileExtractHibernateDTO{
		private Object name;
		private Object originCountry;
		private Object destinationCountry;
		private Object chargeCode;
		private Object recInvoiceNumber;
		private Object receivedInvoiceDate;
		private Object actualRevenue;
		private Object recVatAmt;
		private Object recVatPercent;
		private Object vatAmt;
		private Object recRateCurrency;
		public Object getName() {
			return name;
		}
		public void setName(Object name) {
			this.name = name;
		}
		public Object getOriginCountry() {
			return originCountry;
		}
		public void setOriginCountry(Object originCountry) {
			this.originCountry = originCountry;
		}
		public Object getDestinationCountry() {
			return destinationCountry;
		}
		public void setDestinationCountry(Object destinationCountry) {
			this.destinationCountry = destinationCountry;
		}
		public Object getChargeCode() {
			return chargeCode;
		}
		public void setChargeCode(Object chargeCode) {
			this.chargeCode = chargeCode;
		}
		public Object getRecInvoiceNumber() {
			return recInvoiceNumber;
		}
		public void setRecInvoiceNumber(Object recInvoiceNumber) {
			this.recInvoiceNumber = recInvoiceNumber;
		}
		public Object getReceivedInvoiceDate() {
			return receivedInvoiceDate;
		}
		public void setReceivedInvoiceDate(Object receivedInvoiceDate) {
			this.receivedInvoiceDate = receivedInvoiceDate;
		}
		public Object getActualRevenue() {
			return actualRevenue;
		}
		public void setActualRevenue(Object actualRevenue) {
			this.actualRevenue = actualRevenue;
		}
		public Object getRecVatAmt() {
			return recVatAmt;
		}
		public void setRecVatAmt(Object recVatAmt) {
			this.recVatAmt = recVatAmt;
		}
		public Object getRecVatPercent() {
			return recVatPercent;
		}
		public void setRecVatPercent(Object recVatPercent) {
			this.recVatPercent = recVatPercent;
		}
		public Object getVatAmt() {
			return vatAmt;
		}
		public void setVatAmt(Object vatAmt) {
			this.vatAmt = vatAmt;
		}
		public Object getRecRateCurrency() {
			return recRateCurrency;
		}
		public void setRecRateCurrency(Object recRateCurrency) {
			this.recRateCurrency = recRateCurrency;
		}
		@Override
		public String toString() {
			return  name
					+ "seprator" + originCountry
					+ "seprator" + destinationCountry
					+ "seprator" + chargeCode + "seprator"
					+ recInvoiceNumber + "seprator"
					+  ((receivedInvoiceDate!=null)?new SimpleDateFormat("yyyy/MM/dd").format(receivedInvoiceDate):"") + "seprator" + actualRevenue
					+ "seprator" + recVatAmt + "seprator"
					+ recVatPercent + "seprator" + vatAmt
					+ "seprator" + recRateCurrency;
		}
	}
	
	public List getAdditionalChargesExtraction(String fileType, String parentAgent1, String chargeCode, String corpId) {

		
		try {
			String partnerCode = "";
			StringTokenizer st = new StringTokenizer(parentAgent1, ",");
			while (st.hasMoreTokens()) {
				partnerCode = "'" + st.nextToken() + "'," + partnerCode;
			}
			StringBuffer sb = new StringBuffer(partnerCode);
			partnerCode = sb.replace(partnerCode.length() - 1,
					partnerCode.length(), "").toString();
			List shipments = new ArrayList();
			String query = null;
			List<Object> active = new ArrayList<Object>();
			if ("prepaid".equalsIgnoreCase(fileType)) {
				query = "select if(c.firstname is null || c.firstname='',c.lastname,concat(c.firstname,' ',c.lastname)) as 'Transferee', c.initialcontactdate as 'Initiated',c.sequencenumber as 'Move',s.shipnumber as 'Job',"
						+ "s.status as 'Status',count(a.recinvoicenumber) as 'Invoice Count' "
						+ "from customerfile c Inner Join serviceorder s on c.id=s.customerfileid and s.corpid='"
						+ corpId
						+ "' "
						+ "Left Outer join accountline a on s.id=a.serviceorderid and a.corpid='"
						+ corpId
						+ "' "
						+ "Inner Join billing b on s.id=b.id and b.corpid='"
						+ corpId
						+ "' "
						+ "where b.billtocode in("
						+ partnerCode
						+ ") and c.billtocode in("
						+ partnerCode
						+ ") and a.billtocode in("
						+ partnerCode
						+ ") and s.billtocode in("
						+ partnerCode
						+ ") and "
						+ "c.corpid='"
						+ corpId
						+ "' and s.corpid='"
						+ corpId
						+ "'  and a.corpid='"
						+ corpId
						+ "' and  c.id=s.customerfileid and c.billtocode=a.billtocode and s.id=a.serviceorderid and "
						+ "a.status is true and a.activateaccportal is true  and c.initialcontactdate is not null and a.receivedinvoicedate is not null "
						+ "and (s.status not in ('CNCL') "
						+ "or s.statusreason not in ('Duplicate')) and "
						+ "a.chargecode='"
						+ chargeCode
						+ "' group by s.shipnumber order by s.shipnumber";
				shipments = this.getSession().createSQLQuery(query).list();
				active = getPrepaidRecode(shipments);
			}
			return active;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	
	public List getFlatFileRecode(List shipments,String corpId,boolean checkFieldVisibility){
		try {
			ExtractHibernateDTO hibernateDTO;
			Iterator it = shipments.iterator();
			List<Object> active = new ArrayList<Object>();
			while (it.hasNext()) {
				int count = 0;
				Object[] obj = (Object[]) it.next();
				hibernateDTO = new ExtractHibernateDTO();

				hibernateDTO.setSequenceNumber(obj[count++]);
				String qtStatusCode=obj[count++].toString();
				if(!qtStatusCode.equals("")){
					String description="";
					String query="select description from refmaster where code='"+qtStatusCode+"' and  parameter= 'QUOTESTATUS' and corpID in ('TSFT','"+sessionCorpID+"') and  (language='en' or language='' or language is null)";
					List list= this.getSession().createSQLQuery(query).list();
					if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
						description=list.get(0).toString();
					}
					hibernateDTO.setQuoteStatus(description);
				}else{
					hibernateDTO.setQuoteStatus("");
				}
				hibernateDTO.setPartnerCode(obj[count++]);
				hibernateDTO.setBilltoCode(obj[count++]);
				hibernateDTO.setAccountContact(obj[count++]);
				hibernateDTO.setTransferee(obj[count++]);
				hibernateDTO.setMode(obj[count++]);
				hibernateDTO.setOriginCountrycode(obj[count++]);
				hibernateDTO.setDestinationCountrycode(obj[count++]);
				hibernateDTO.setOriginCity(obj[count++]);
				hibernateDTO.setDestinationCity(obj[count++]);
				hibernateDTO.setAuthorizationReceivedDt(obj[count++]);
				hibernateDTO.setSurvey(obj[count++]);
				hibernateDTO.setBeginPacking(obj[count++]);
				hibernateDTO.setBeginload(obj[count++]);
				hibernateDTO.setRequiredDeliveryDate(obj[count++]);
				hibernateDTO.setInvoiceDate(obj[count++]);

				hibernateDTO.setAuthorizedVolumeCBM(obj[count++]);
				hibernateDTO.setEstimatedVolumeCBM(obj[count++]);
				hibernateDTO.setShippedVolumeCBM(obj[count++]);
				hibernateDTO.setDistanceKM(obj[count++]);
				hibernateDTO.setTotalAmtInvoicedEUR(obj[count++]);
				hibernateDTO.setDoorToDoorEUR(obj[count++]);
				hibernateDTO.setAllAditionalCharges(obj[count++]);
				hibernateDTO.setAssignmentType(obj[count++]);
				hibernateDTO.setRecommendation(obj[count++]);
				hibernateDTO.setPreMoveServicsRating(obj[count++]);
				hibernateDTO.setOriginServicesRating(obj[count++]);
				hibernateDTO.setDestServicesRating(obj[count++]);
				hibernateDTO.setOverAllRating(obj[count++]);
				hibernateDTO.setComplaints(obj[count++]);
				if(checkFieldVisibility)
						{
				hibernateDTO.setNationality(obj[count++]);
						}
				hibernateDTO.setCorpId(corpId);
				hibernateDTO.setQualitySurvey(getQualitySurvey());
				active.add(hibernateDTO);
			}
			return active;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	
	public List getClaimRecode(List shipments){
		try {
			ExtractHibernateDTO hibernateDTO;
			Iterator it = shipments.iterator();
			List<Object> active = new ArrayList<Object>();
			while (it.hasNext()) {
				int count = 0;
				Object[] obj = (Object[]) it.next();
				hibernateDTO = new ExtractHibernateDTO();

				hibernateDTO.setClientName_Claim(obj[count++]);
				hibernateDTO.setAccount_Claim(obj[count++]);
				hibernateDTO.setServiceOrder_Claim(obj[count++]);
				hibernateDTO.setClaimNumber_Claim(obj[count++]);
				hibernateDTO.setLossNumber1_Claim(obj[count++]);
				hibernateDTO.setFromTo_Claim(obj[count++]);
				hibernateDTO.setHandleByInsurer_Claim(obj[count++]);
				hibernateDTO.setType_Claim(obj[count++]);
				hibernateDTO.setCause_Claim(obj[count++]);
				hibernateDTO.setCauseBy_Claim(obj[count++]);
				hibernateDTO.setDateReceived_Claim(obj[count++]);
				hibernateDTO.setClaimAmt_Claim(obj[count++]);
				hibernateDTO.setClaimCurrency_Claim(obj[count++]);
				hibernateDTO.setPaidAmt_Claim(obj[count++]);
				hibernateDTO.setPaidCurrency_Claim(obj[count++]);
				hibernateDTO.setClaimStatus_Claim(obj[count++]);
				hibernateDTO.setStatusDt_Claim(obj[count++]);
				hibernateDTO.setRemark_Claim(obj[count++]);

				active.add(hibernateDTO);
			}
			return active;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	public List getAdditionalRecode(List shipments){
		try {
			AdditionalChargesDTO hibernateDTO;
			Iterator it = shipments.iterator();
			List<Object> active = new ArrayList<Object>();
			while (it.hasNext()) {
				int count = 0;
				Object[] obj = (Object[]) it.next();
				hibernateDTO = new AdditionalChargesDTO();
				hibernateDTO.setTransferee(obj[0]);
				hibernateDTO.setCustomerFile(obj[1]);
				hibernateDTO.setServiceOrder(obj[2]);
				hibernateDTO.setCost(obj[3]);
				hibernateDTO.setDescription(obj[4]);
				hibernateDTO.setCurrency(obj[5]);
				hibernateDTO.setAmount(obj[6]);
				hibernateDTO.setTotalAmount(obj[7]);
				active.add(hibernateDTO);
			}
			return active;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	public List getPrepaidRecode(List shipments){
		try {
			PrepaidServicesDTO hibernateDTO;
			Iterator it = shipments.iterator();
			List<Object> active = new ArrayList<Object>();
			while (it.hasNext()) {
				int count = 0;
				Object[] obj = (Object[]) it.next();
				hibernateDTO = new PrepaidServicesDTO();

				hibernateDTO.setTransferee(obj[0]);
				hibernateDTO.setInitated(obj[1]);
				hibernateDTO.setMove(obj[2]);
				hibernateDTO.setJob(obj[3]);
				hibernateDTO.setStatus(obj[4]);
				hibernateDTO.setInvoices(obj[5]);
				active.add(hibernateDTO);
			}
			return active;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	
	
	
	public List<ServiceOrder> getServiceOrderList(Long id) {
		try {
			return getHibernateTemplate().find(
					"from ServiceOrder where customerFileId='" + id + "'  ");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	public String enableStateList(String corpId){
		String countryList="";
		try {
			List al = this
					.getSession()
					.createSQLQuery(
							"select GROUP_CONCAT(description) from refmaster where  parameter='COUNTRY' and state='Y' and corpID in ('TSFT','"
									+ corpId
									+ "') and (language='en' or language='' or language is null ) ")
					.list();
			if ((al != null) && (!al.isEmpty()) && (al.get(0) != null)) {
				countryList = al.get(0).toString();
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return countryList;
	}
	public String enableStateList1(String corpId){
		String countryList="";
		try {
			List al = this
					.getSession()
					.createSQLQuery(
							"select GROUP_CONCAT(code) from refmaster where  parameter='COUNTRY' and state='Y' and corpID in ('TSFT','"
									+ corpId
									+ "') and (language='en' or language='' or language is null ) ")
					.list();
			if ((al != null) && (!al.isEmpty()) && (al.get(0) != null)) {
				countryList = al.get(0).toString();
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return countryList;
	}
	
	/* Method Added By kunal for ticket number: 6183*/
	public List getFilterData(Long userId, String sessionCorpID) {
			List list = new ArrayList();
			try {
				list = this
						.getSession()
						.createSQLQuery(
								"select tableName,filterValues,fieldName, dss.name from userdatasecurity uds , "
										+ "datasecuritypermission dsp , datasecurityfilter dsf , datasecurityset dss where uds.userdatasecurity_id = dsp.datasecurityset_id "
										+ "and dsp.datasecurityset_id = dss.id and uds.user_id="
										+ userId
										+ " and dsf.corpID in('"
										+ sessionCorpID
										+ "','TSFT') and dsf.id=dsp.datasecurityfilter_id")
						.list();
				
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return list;
		}
		public List getOrderInitiationBillToCode(String sessionCorpID, String parentCode)
		{List <Object> partnerList = new ArrayList<Object>();
			try {
				String query="";
				Partner partner = null;
				query = "select distinct p.partnerCode,p.lastName,p.billingCountryCode,pp.extReference,p.billingCity from partnerpublic p , partnerprivate pp  where pp.partnerpublicid = p.id and pp.corpid in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') AND  p.partnerCode in ("+ parentCode + ")  and  p.status = 'Approved'"
											;
			   List list = this.getSession().createSQLQuery(query).list();
				
				Iterator it = list.iterator();
				 
								while (it.hasNext()) {
									Object[] row = (Object[]) it.next();
									partner = new Partner();
									if(row[0]!=null){
									partner.setPartnerCode(row[0].toString());
									}else{
										partner.setPartnerCode("");	
									}
									if(row[1]!=null){
									partner.setLastName(row[1].toString());
									}else{
										partner.setLastName("");	
									}
									if(row[2]!=null){
									partner.setBillingCountryCode(row[2].toString());
									}else{
									partner.setBillingCountryCode("");	
									}
									if(row[3]!=null){
									partner.setExtReference(row[3].toString());
									}else{
										partner.setExtReference("");	
									}
									if(row[4]!=null){
										partner.setBillingCity(row[4].toString());
										}else{
											partner.setBillingCity("");	
										}
									partnerList.add(partner);
								}
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return partnerList;
		}
		
		public List searchOrderInitiationBillToCode(String sessionCorpID, String parentCode, String partnerCode, String description,String billingCountryCode,String billingCountry,String extReference, String billingCity)
		{List <Object> partnerList = new ArrayList<Object>();
			try {
			    Partner partner = null;
				String query="";
				query = "select distinct p.partnerCode,p.lastName,p.billingCountryCode,pp.extReference,p.billingCity from partnerpublic p, partnerprivate pp where pp.partnerpublicid = p.id  and  pp.corpid in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')  AND p.partnerCode in ("+ parentCode + ")"
								+ " and p.partnerCode like '%"+ partnerCode + "%' and p.lastName like '%"+description.replaceAll("'", "''")+ "%'"
										+ " and p.billingCountryCode like '%"+ billingCountryCode.replaceAll("'", "''") + "%' "
                                                 + "and p.billingCountry like '%"+ billingCountry.replaceAll("'", "''") + "%'"
                                                 + "and p.billingCity like '%"+ billingCity.replaceAll("'", "''") + "%'"
												 +" and p.status = 'Approved'   and  pp.extReference like '%"+ extReference.replaceAll("'", "''") + "%'";
				List list = this.getSession().createSQLQuery(query).list();
				
				Iterator it = list.iterator(); 
								while (it.hasNext()) {
									Object[] row = (Object[]) it.next();
									partner = new Partner();
									if(row[0]!=null){
									partner.setPartnerCode(row[0].toString());
									}else{
										partner.setPartnerCode("");	
									}
									if(row[1]!=null){
									partner.setLastName(row[1].toString());
									}else{
										partner.setLastName("");	
									}
									if(row[2]!=null){
									partner.setBillingCountryCode(row[2].toString());
									}else{
									partner.setBillingCountryCode("");	
									}
									if(row[3]!=null){
									partner.setExtReference(row[3].toString());
									}else{
										partner.setExtReference("");	
									}
									if(row[4]!=null){
										partner.setBillingCity(row[4].toString());
										}else{
											partner.setBillingCity("");	
										}
									partnerList.add(partner);
								}
				
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			
			return partnerList;
		}
		public List findMergeSOList(String sessionCorpID, String bookingAgentCode, Boolean checkAccessQuotation){
			try {
				String query="";
				if(checkAccessQuotation){
					query = " select distinct(c.sequenceNumber) from serviceorder b, customerfile c, customerfile q where " +
							" b.corpid='"+sessionCorpID+"' and b.status not in ('CNCL','CLSD','DWNLD') and b.bookingAgentShipNumber!=''  " +
							" and b.bookingAgentShipNumber is not null and b.bookingAgentCode = '"+bookingAgentCode+"' " +
							" and b.customerfileid=c.id and b.bookingAgentCode = c.bookingAgentCode " +
							" and b.bookingAgentCode = q.bookingAgentCode and c.corpid='"+sessionCorpID+"' " +
							"and q.corpid='"+sessionCorpID+"' and c.controlflag='C' and q.movetype='Quote' order by 1";
				}else{
				query = " select distinct(c.sequenceNumber) from serviceorder b, customerfile c, customerfile q where " +
						" b.corpid='"+sessionCorpID+"' and b.status not in ('CNCL','CLSD','DWNLD') and b.bookingAgentShipNumber!=''  " +
						" and b.bookingAgentShipNumber is not null and b.bookingAgentCode = '"+bookingAgentCode+"' " +
						" and b.customerfileid=c.id and b.bookingAgentCode = c.bookingAgentCode " +
						" and b.bookingAgentCode = q.bookingAgentCode and c.corpid='"+sessionCorpID+"' " +
						"and q.corpid='"+sessionCorpID+"' and c.controlflag='C' and q.controlflag='Q' order by 1";
				}
				List list = this.getSession().createSQLQuery(query).list();
				return list;
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		public List findLinkSOtoCF(String sessionCorpID, String seqNumber){
			List <Object> mergeList = new ArrayList<Object>();
			try {
				String query = "select s.shipnumber,s.commodity,s.mode,if(s.firstname is not null and s.firstname!='',concat(s.firstname,' ',s.lastname),s.lastname) as shipper,s.id  from serviceorder s where s.sequencenumber='"
						+ seqNumber
						+ "' and s.status not in ('CNCL','CLSD','DWNLD') ";
				List list = this.getSession().createSQLQuery(query)
						.addScalar("s.shipnumber", Hibernate.STRING)
						.addScalar("s.commodity", Hibernate.STRING)
						.addScalar("s.mode", Hibernate.STRING)
						.addScalar("shipper", Hibernate.STRING)
						.addScalar("s.id", Hibernate.STRING).list();
				Iterator it = list.iterator();
				DTOMerge dto = null;
				while (it.hasNext()) {
					Object[] row = (Object[]) it.next();
					dto = new DTOMerge();
					dto.setShipNumber(row[0]);
					dto.setCommodity(row[1]);
					dto.setMode(row[2]);
					dto.setShipper(row[3]);
					dto.setId(row[4]);
					mergeList.add(dto);
				}
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return  mergeList;
		}
		public  class DTOMerge{	
			private Object shipNumber;
			private Object commodity;
			private Object mode;
			private Object shipper;
			private Object id;
			
			public Object getShipNumber() {
				return shipNumber;
			}
			public void setShipNumber(Object shipNumber) {
				this.shipNumber = shipNumber;
			}
			public Object getCommodity() {
				return commodity;
			}
			public void setCommodity(Object commodity) {
				this.commodity = commodity;
			}
			public Object getMode() {
				return mode;
			}
			public void setMode(Object mode) {
				this.mode = mode;
			}
			public Object getShipper() {
				return shipper;
			}
			public void setShipper(Object shipper) {
				this.shipper = shipper;
			}
			public Object getId() {
				return id;
			}
			public void setId(Object id) {
				this.id = id;
			}
			
		 }
		public List getQFLinkAccLine(String sessionCorpID, Long sid){
			try {
				return getHibernateTemplate()
						.find("select id from AccountLine where serviceOrderId='"
								+ sid + "' and corpID='" + sessionCorpID + "' ");
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		public List getQFFilesId(String sessionCorpID, String fileId){
			try {
				return getHibernateTemplate().find(
						"select id from MyFile where fileId='" + fileId
								+ "' and corpID='" + sessionCorpID + "' ");
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		public List getQFNotesId(String sessionCorpID, String notesId,Long notesKeyId){
			try {
				return getHibernateTemplate().find(
						"select id from Notes where notesId='" + notesId
								+ "' and corpID='" + sessionCorpID
								+ "' and notesKeyId ='" + notesKeyId + "' ");
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		public List getQFInventoryData(String sessionCorpID, Long customerFileID){
			try {
				return getHibernateTemplate().find(
						"select id from InventoryData where customerFileID='"
								+ customerFileID + "' and corpID='"
								+ sessionCorpID + "' ");
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		public List getQuotesInventoryData(String sessionCorpID,Long customerFileId, Long sid, String mode){
			try {
				return getHibernateTemplate().find(
						"select id from InventoryData where customerFileID='"
								+ customerFileId + "' and corpID='"
								+ sessionCorpID + "' and serviceOrderID = '"
								+ sid + "' ");
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		public List getQFInventoryPathList(String sessionCorpID, Long invId, Long accInfoId){
			try {
				return getHibernateTemplate().find(
						"select id from InventoryPath where corpID='"
								+ sessionCorpID + "' and inventoryDataId = '"
								+ invId + "' "
								+ "' ");
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		public List getQFAccessInfo(String sessionCorpID, Long id){
			try {
				return getHibernateTemplate().find(
						"select id from AccessInfo where corpID='"
								+ sessionCorpID + "' and customerFileId='" + id + "' ");
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		public Boolean getQualitySurvey() {
			return qualitySurvey;
		}
		public void setQualitySurvey(Boolean qualitySurvey) {
			this.qualitySurvey = qualitySurvey;
		}
		public List getServiceOrderListRemote(Long id) {
			try {
				return this
						.getSession()
						.createSQLQuery(
								"select id from serviceorder where customerFileId='"
										+ id + "' ").list();
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				getSessionCorpID();
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		public String findPrivatepartyBilltoCode(String billToCode, String corpID) {
			String billtocode="";
			try {
				List BillToCodeList = this
						.getSession()
						.createSQLQuery(
								"select partnerCode from partnerpublic where partnerCode='"
										+ billToCode
										+ "' and isPrivateParty is true  AND isAgent = false AND isVendor = false AND isCarrier= false AND isAccount = false AND isOwnerOp = false   and corpid in ('TSFT','"
										+ hibernateUtil.getParentCorpID(corpID)
										+ "','" + corpID + "') ").list();
				if (BillToCodeList != null
						&& (!(BillToCodeList.isEmpty()))
						&& BillToCodeList.get(0) != null
						&& (!(BillToCodeList.get(0).toString().trim()
								.equals("")))) {
					billtocode = BillToCodeList.get(0).toString().trim();
				}
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return billtocode;
		}
		public List findMaximumShipCF(String seqNum, String job, String CorpID) {
			try {
				return this.getSession().createSQLQuery(
						"select max(ship) from serviceorder where job like '"
								+ job.replaceAll("'", "''")
								+ "%' and sequenceNumber='"+seqNum+"' and corpid='"+CorpID+"' ").list();
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(CorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		public String findAgentparentByBillToCode(String billToCode, String corpID){
			String accType="";
				try {
					List al = this
							.getSession()
							.createSQLQuery(
									"select concat(if(agentParent is null or agentParent='',' ',agentParent),'~',if(agentParentName is null or agentParentName='',' ',agentParentName)) from partnerpublic where isAccount=true and status='Approved' and agentParent is not null and agentParent!='' and partnerCode='"
											+ billToCode
											+ "' and corpid in ('TSFT','"
											+ hibernateUtil
													.getParentCorpID(corpID)
											+ "','" + corpID + "') ").list();
					if ((al != null) && (!al.isEmpty()) && (al.get(0) != null)) {
						accType = al.get(0).toString();
					} else {
						List al1 = this
								.getSession()
								.createSQLQuery(
										"select agentParent from partnerpublic where isAccount=true and status='Approved' and partnerCode='"
												+ billToCode
												+ "' and corpid in ('TSFT','"
												+ hibernateUtil
														.getParentCorpID(corpID)
												+ "','" + corpID + "') ")
								.list();
						if ((al != null) && (!al.isEmpty())
								&& (al.get(0) != null)) {
							accType = "AC";
						} else {
							accType = "NOTAC";
						}

					}
				} catch (Exception e) {
					logger.error("Error executing query"+ e,e);
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			    	 e.printStackTrace();
				}
			return accType;
		}
		public Boolean checkInsurance(String billToCode, String corpID){
		 Boolean Insurance=false;
		 try {
			List insuranceList = this
					.getSession()
					.createSQLQuery(
							"select pp.noInsurance,pp.lumpSum,pp.detailedList from partnerprivate pp left outer join partnerpublic p on pp.partnerCode=p.partnerCode  where pp.partnerCode='"
									+ billToCode
									+ "' and pp.corpID ='"
									+ corpID + "' and p.isAccount is true and  pp.status = 'Approved'  and  p.status = 'Approved' ")
					.list();
			if (insuranceList != null && !(insuranceList.isEmpty())) {
				Iterator insurance11 = insuranceList.iterator();
				while (insurance11.hasNext()) {
					Object[] row = (Object[]) insurance11.next();
					if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(true) && row[1].equals(true)
							&& row[2].equals(true)) {
						Insurance = true;
					} else if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(false) && row[1].equals(true)
							&& row[2].equals(true)) {
						Insurance = true;
					} else if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(true) && row[1].equals(false)
							&& row[2].equals(true)) {
						Insurance = true;
					} else if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(true) && row[1].equals(true)
							&& row[2].equals(false)) {
						Insurance = true;
					} else if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(false) && row[1].equals(false)
							&& row[2].equals(true)) {
						Insurance = true;
					} else if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(false) && row[1].equals(true)
							&& row[2].equals(false)) {
						Insurance = true;
					} else if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(false) && row[1].equals(false)
							&& row[2].equals(false)) {
						List insSystemDefault = this
								.getSession()
								.createSQLQuery(
										"select noInsurance from systemdefault where corpid='"
												+ corpID + "'").list();
						if (insSystemDefault != null
								&& !(insSystemDefault.isEmpty())
								&& (insSystemDefault.get(0) != null)
								&& (insSystemDefault.get(0).equals(false))) {
							Insurance = true;
						}
					} else if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(true) && row[1].equals(false)
							&& row[2].equals(false)) {
						List insSystemDefault = this
								.getSession()
								.createSQLQuery(
										"select noInsurance from systemdefault where corpid='"
												+ corpID + "'").list();
						if (insSystemDefault != null
								&& !(insSystemDefault.isEmpty())
								&& (insSystemDefault.get(0) != null)
								&& (insSystemDefault.get(0).equals(false))) {
							Insurance = true;
						}
					}
				}

			} else {
				List insSystemDefault = this
						.getSession()
						.createSQLQuery(
								"select noInsurance from systemdefault where corpid='"
										+ corpID + "'").list();
				if (insSystemDefault != null && !(insSystemDefault.isEmpty())
						&& (insSystemDefault.get(0) != null)
						&& (insSystemDefault.get(0).equals(false))) {
					Insurance = true;
				}
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return Insurance;
		}
		public List findAgentAccFieldToSyncCreate(String modalClass, String type) {
			try {
				return this
						.getSession()
						.createSQLQuery(
								"select concat(fieldName,'~',fieldName) from networkdatafields where modelName='"
										+ modalClass
										+ "' and transactionType='Create'  and (type='' or type is null or type='"
										+ type
										+ "' or type='CMM/DMM') and (toFieldName is null or toFieldName='')  ")
						.list();
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				getSessionCorpID();
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		public Boolean getInsurancebooking(String partnerCode,String billToCode, String corpID){
			List companyBookingAgent=new ArrayList();
			Boolean Insurance=false;
			 try {
				companyBookingAgent = getHibernateTemplate().find(
						"from CompanyDivision where bookingAgentCode='"
								+ partnerCode + "' and  corpID='" + corpID
								+ "' ");
				if (companyBookingAgent != null
						&& !(companyBookingAgent.isEmpty())
						&& (companyBookingAgent.get(0) != null)) {
					Insurance = true;
				} else {
					companyBookingAgent = getHibernateTemplate().find(
							"from PartnerPublic where partnerCode='"
									+ billToCode + "' and  corpID in('"
									+ corpID
									+ "','TSFT') and partnerType='DMM' ");
					if (companyBookingAgent != null
							&& !(companyBookingAgent.isEmpty())) {
						Insurance = true;
					}
				}
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return Insurance;
		}
		public List getInsuranceValue(String billToCode, String corpID){
			 
			List insurance=new ArrayList();
			try {
				if(billToCode !=null && (!(billToCode.trim().equalsIgnoreCase("null"))) && (!(billToCode.trim().equalsIgnoreCase("")))){
				List insuranceList = this
						.getSession()
						.createSQLQuery(
								"select pp.noInsurance,pp.lumpSum,pp.detailedList,concat(s.baseCurrency,' ',FLOOR(pp.lumpSumAmount),' ','Per',' ',pp.lumpPerUnit) as Amount from partnerprivate pp left outer join partnerpublic p on pp.partnerCode=p.partnerCode,systemdefault s  where pp.partnerCode='"
										+ billToCode
										+ "' and s.corpID='"
										+ corpID
										+ "' and pp.corpID ='"
										+ corpID + "' and p.isAccount is true")
						.list();
				if (insuranceList != null && !(insuranceList.isEmpty())) {
					Iterator insurance11 = insuranceList.iterator();
					while (insurance11.hasNext()) {
						Object[] row = (Object[]) insurance11.next();
						if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(true) && row[1].equals(true)
								&& row[2].equals(true)) {
							insurance.add(" ");
							insurance.add(row[3]);
							insurance.add("No Insurance");
							insurance.add("Detailed List");
						} else if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(false) && row[1].equals(true)
								&& row[2].equals(true)) {
							insurance.add(" ");
							insurance.add(row[3]);
							insurance.add("Detailed List");
						} else if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(true) && row[1].equals(false)
								&& row[2].equals(true)) {
							insurance.add(" ");
							insurance.add("No Insurance");
							insurance.add("Detailed List");
						} else if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(true) && row[1].equals(true)
								&& row[2].equals(false)) {
							insurance.add(" ");
							insurance.add(row[3]);
							insurance.add("No Insurance");
						} else if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(false) && row[1].equals(false)
								&& row[2].equals(true)) {
							insurance.add(" ");
							insurance.add("Detailed List");
						} else if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(false) && row[1].equals(true)
								&& row[2].equals(false)) {
							insurance.add(" ");
							insurance.add(row[3]);
						} else if (row[0] !=null && row[1] !=null && row[2] !=null && row[0].equals(false) && row[1].equals(false)
								&& row[2].equals(false)) {
							List insSystemDefault = this
									.getSession()
									.createSQLQuery(
											"select noInsurance,concat(baseCurrency,' ',insurancePerUnit,' ','Per',' ',unitForInsurance) as Amount,detailedList,noInsuranceCportal,lumpSum from systemdefault where corpid='"
													+ corpID + "'").list();
							if (insSystemDefault != null
									&& !(insSystemDefault.isEmpty())) {
								Iterator insurance111 = insSystemDefault
										.iterator();
								while (insurance111.hasNext()) {
									Object[] row1 = (Object[]) insurance111
											.next();
									if (row1[0].equals(false)) {
										insurance.add(" ");
										if(row1[4].equals(true)){
											insurance.add(row1[1]);
										}
										if (row1[2].equals(true)) {
											insurance.add("Detailed List");
										}
										if (row1[3].equals(true)) {
											insurance.add("No Insurance");
										}
									}
								}
							}
						} else if (row[0] !=null && row[1] !=null && row[2] !=null &&  row[0].equals(true) && row[1].equals(false)
								&& row[2].equals(false)) {
							List insSystemDefault = this
									.getSession()
									.createSQLQuery(
											"select noInsurance,concat(baseCurrency,' ',insurancePerUnit,' ','Per',' ',unitForInsurance) as Amount,detailedList,noInsuranceCportal,lumpSum from systemdefault where corpid='"
													+ corpID + "'").list();
							if (insSystemDefault != null
									&& !(insSystemDefault.isEmpty())) {
								Iterator insurance111 = insSystemDefault
										.iterator();
								while (insurance111.hasNext()) {
									Object[] row1 = (Object[]) insurance111
											.next();
									if (row1[0].equals(false)) {
										insurance.add(" ");
										if(row1[4].equals(true)){
											insurance.add(row1[1]);	
										}
										if (row1[2].equals(true)) {
											insurance.add("Detailed List");
										}
										insurance.add("No Insurance");
									}
								}
							}
						}
					}
				} else {
					List insSystemDefault = this
							.getSession()
							.createSQLQuery(
									"select noInsurance,concat(baseCurrency,' ',insurancePerUnit,' ','Per',' ',unitForInsurance)as Amount,detailedList,noInsuranceCportal,lumpSum from systemdefault where corpid='"
											+ corpID + "'").list();
					if (insSystemDefault != null
							&& !(insSystemDefault.isEmpty())) {
						Iterator insurance112 = insSystemDefault.iterator();
						while (insurance112.hasNext()) {
							Object[] row11 = (Object[]) insurance112.next();
							if (row11[0].equals(false)) {
								  insurance.add(" ");
								if(row11[4].equals(true)){
									insurance.add(row11[1]);
								}
								if (row11[2].equals(true)) {
									insurance.add("Detailed List");
								}
								if (row11[3].equals(true)) {
									insurance.add("No Insurance");
								}
							}
						}
					}
				}
				}
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return insurance;
		}
		public List findOrderInitiationBillToCode(String sessionCorpID, String parentCode, String bCode) {

			try {
				List partnerList = new ArrayList();
				Partner partnerPublic = null;
				return getHibernateTemplate().find(
						" select lastName from PartnerPublic   where partnerCode in ("
								+ parentCode + ") and partnerCode='" + bCode
								+ "'  and  status = 'Approved'");
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		public void getUpdateBilling(String noInsurance,String seqNumber,String sessionCorpID){
			try {
				getSession().createSQLQuery(
						"update billing set noInsurance='" + noInsurance
								+ "' where  sequenceNumber='" + seqNumber
								+ "' and  corpID='" + sessionCorpID
								+ "' and insSubmitDate is null ")
						.executeUpdate();
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}	
		}
		public List getUserURL(String userRole, String sessionCorpID) {
		
			try {
				return	this .getSession() .createSQLQuery("select replace(m2.URL,'/','') from menu_item_permission m1, menu_item m2 where m1.menuItemId = m2.id and m1.corpid='"+sessionCorpID+"' and recipient in(" + userRole +") and permission='2' ").list();
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
			
		}
		public void updateInAllSoCordName(Long custid,String cordName,String sessionCorpID){
			try {
				String query="update serviceorder set coordinator='" + cordName
								+ "',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where  customerFileId='" + custid
								+ "' and  corpID='" + sessionCorpID	+ "'";
				getSession().createSQLQuery(query).executeUpdate();
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
		}
		public List getChildAgentCode(String filterValues, String sessionCorpID) {
			
			List temp;
			try {
				String query = "select partnercode from partnerpublic where agentparent = '"+filterValues+"' and corpid in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') and status ='Approved' and partnercode <> '"+filterValues+"' ";
				temp = this.getSession().createSQLQuery(query).list();
				return temp;
			} catch (Exception  e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
			
		}
		public List getPartnerDetails(String parentAgent ,String sessionCorpID){
			try
			{
			String query = "select vanLineAffiliation,utsNumber from partnerpublic where partnercode ='"+parentAgent+"' and corpid in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') and status ='Approved' and isAgent is true ";
			List temp =  this.getSession().createSQLQuery(query).list();
			return temp;
		  } catch (Exception  e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
		}
		public List findLeadAutocompleteList(String stateNameOri,String countryNameOri,String cityNameOri, String corpId){
			
			try {
				String query="select distinct (city) from refzipgeocodemap where stateabbreviation='"+stateNameOri+"' and country='"+countryNameOri+"' and city like '"+cityNameOri+"%' and corpID in ('"+corpId+"','TSFT','"+hibernateUtil.getParentCorpID(corpId)+"') ";
				List temp =  this.getSession().createSQLQuery(query).list();
				return temp;
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		public List findLeadAutocompleteListDest(String stateNameDes,String countryNameDes,String cityNameDes, String corpId){
			try
			{
			String query="select distinct (city) from refzipgeocodemap where stateabbreviation='"+stateNameDes+"' and country='"+countryNameDes+"' and city like '"+cityNameDes+"%' and corpID in ('"+corpId+"','TSFT','"+hibernateUtil.getParentCorpID(corpId)+"') ";
			List temp =  this.getSession().createSQLQuery(query).list();
			return temp;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
		}
		public String findParentByAccCode(String accCode, String sessionCorpID){
			String accValue="";
			String query="";
			try {
			
					query="select agentParent from partnerpublic where partnerCode='"+accCode+"' and corpid in ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"')";
			
				
				List temp =  this.getSession().createSQLQuery(query).list();
				if ((temp != null) && (!temp.isEmpty()) && (temp.get(0) != null)) {
					accValue = temp.get(0).toString();
				}
				accValue=accValue+","+accCode;
				if(accValue.contains("T56292")){
					accValue="true";
				}else{
					accValue="false";
				}			
				return accValue;
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return accValue;
		}
		public List findLeadCaptureList(String sequenceNumber,String lastName,String firstName,Date createdOn, String job,String salesPerson,String status,Date moveDate,String serviceOrderSearchType, String sessionCorpID){
			
			try {
				String createdOnDate = "";
				if (!(createdOn == null)) {
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(createdOn));
					createdOnDate = nowYYYYMMDD1.toString();
				}
				String moveDate1 = "";
				if (!(moveDate == null)) {
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(moveDate));
					moveDate1 = nowYYYYMMDD1.toString();
				}
				List leadCaptureList=new ArrayList();
				String query = null;
				if (serviceOrderSearchType.equalsIgnoreCase("Start With")) {
							query = "select sequenceNumber,lastName,firstName,job, salesMan,statusDate,leadStatus,createdOn,id from customerfile where controlflag ='L' and corpid = '"+sessionCorpID+"'";
							
							if(sequenceNumber!=null && !sequenceNumber.equals("")){
								query = query + " and sequenceNumber like '"+ sequenceNumber.trim().replaceAll("'", "''").replaceAll(":", "''") + "%'";
							}
							if(lastName!=null && !lastName.equals("")){
								query = query + " and lastName like '"+ lastName.trim().replaceAll("'", "''").replaceAll(":", "''") + "%'";
							}
							if(firstName!=null && !firstName.equals("")){
								query = query + " and firstName like '"+ firstName.trim().replaceAll("'", "''").replaceAll(":", "''") + "%'";
							}
							if(createdOn!=null && !createdOn.equals("")){
								query = query + " and createdOn like '"+createdOnDate+"%'";
							}
							if(job!=null && !job.equals("")){
								query = query + " and job = '"+job+"'";
							}
							if(salesPerson!=null && !salesPerson.equals("")){
								query = query + " and salesMan = '"+salesPerson+"'";
							}
							if(status!=null && !status.equals("")){
								query = query + " and leadStatus = '"+status+"'";
							}
							if(moveDate!=null && !moveDate.equals("")){
								query = query + " and moveDate like '"+moveDate1+"%'";
							}
					}else if (serviceOrderSearchType.equalsIgnoreCase("End With")) {
						query = "select sequenceNumber,lastName,firstName,job, salesMan,statusDate,leadStatus,createdOn,id from customerfile where controlflag ='L' and corpid = '"+sessionCorpID+"'";
						
						if(sequenceNumber!=null && !sequenceNumber.equals("")){
							query = query + " and sequenceNumber like '%"+ sequenceNumber.trim().replaceAll("'", "''").replaceAll(":", "''") + "'";
						}
						if(lastName!=null && !lastName.equals("")){
							query = query + " and lastName like '%"+ lastName.trim().replaceAll("'", "''").replaceAll(":", "''") + "'";
						}
						if(firstName!=null && !firstName.equals("")){
							query = query + " and firstName like '%"+ firstName.trim().replaceAll("'", "''").replaceAll(":", "''") + "'";
						}
						if(createdOn!=null && !createdOn.equals("")){
							query = query + " and createdOn like '"+createdOnDate+"%'";
						}
						if(job!=null && !job.equals("")){
							query = query + " and job = '"+job+"'";
						}
						if(salesPerson!=null && !salesPerson.equals("")){
							query = query + " and salesMan = '"+salesPerson+"'";
						}
						if(status!=null && !status.equals("")){
							query = query + " and leadStatus = '"+status+"'";
						}
						if(moveDate!=null && !moveDate.equals("")){
							query = query + " and moveDate like '"+moveDate1+"%'";
						}
					}else if (serviceOrderSearchType.equalsIgnoreCase("Exact Match")) {
						query = "select sequenceNumber,lastName,firstName,job, salesMan,statusDate,leadStatus,createdOn,id from customerfile where controlflag ='L' and corpid = '"+sessionCorpID+"'";
						
						if(sequenceNumber!=null && !sequenceNumber.equals("")){
							query = query + " and sequenceNumber = '"+ sequenceNumber.trim().replaceAll("'", "''").replaceAll(":", "''") + "'";
						}
						if(lastName!=null && !lastName.equals("")){
							query = query + " and lastName = '"+ lastName.trim().replaceAll("'", "''").replaceAll(":", "''") + "'";
						}
						if(firstName!=null && !firstName.equals("")){
							query = query + " and firstName = '"+ firstName.trim().replaceAll("'", "''").replaceAll(":", "''") + "'";
						}
						if(createdOn!=null && !createdOn.equals("")){
							query = query + " and createdOn like '"+createdOnDate+"%'";
						}
						if(job!=null && !job.equals("")){
							query = query + " and job = '"+job+"'";
						}
						if(salesPerson!=null && !salesPerson.equals("")){
							query = query + " and salesMan = '"+salesPerson+"'";
						}
						if(status!=null && !status.equals("")){
							query = query + " and leadStatus = '"+status+"'";
						}
						if(moveDate!=null && !moveDate.equals("")){
							query = query + " and moveDate like '"+moveDate1+"%'";
						}
					}else {
						query = "select sequenceNumber,lastName,firstName,job, salesMan,statusDate,leadStatus,createdOn,id from customerfile where controlflag ='L' and corpid = '"+sessionCorpID+"'";
						
						if(sequenceNumber!=null && !sequenceNumber.equals("")){
							query = query + " and sequenceNumber like '%"+ sequenceNumber.trim().replaceAll("'", "''").replaceAll(":", "''") + "%'";
						}
						if(lastName!=null && !lastName.equals("")){
							query = query + " and lastName like '%"+ lastName.trim().replaceAll("'", "''").replaceAll(":", "''") + "%'";
						}
						if(firstName!=null && !firstName.equals("")){
							query = query + " and firstName like '%"+ firstName.trim().replaceAll("'", "''").replaceAll(":", "''") + "%'";
						}
						if(createdOn!=null && !createdOn.equals("")){
							query = query + " and createdOn like '"+createdOnDate+"%'";
						}
						if(job!=null && !job.equals("")){
							query = query + " and job = '"+job+"'";
						}
						if(salesPerson!=null && !salesPerson.equals("")){
							query = query + " and salesMan = '"+salesPerson+"'";
						}
						if(status!=null && !status.equals("")){
							query = query + " and leadStatus = '"+status+"'";
						}
						if(moveDate!=null && !moveDate.equals("")){
							query = query + " and moveDate like '"+moveDate1+"%'";
						}
					}
				List leadCapList = this.getSession().createSQLQuery(query).list();
				leadCapList dto = null;
				Iterator it = leadCapList.iterator();
				while (it.hasNext()){
					Object []row= (Object [])it.next();
					dto = new leadCapList();
					dto.setSequenceNumber(row[0]);
					dto.setLastName(row[1]);
					dto.setFirstName(row[2]);
					dto.setJob(row[3]);
					dto.setSalesMan(row[4]);
					dto.setStatusDate(row[5]);
					dto.setStatus(row[6]);
					dto.setCreatedOn(row[7]);
					dto.setId(row[8]);
					leadCaptureList.add(dto);
				}
				return leadCaptureList;
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		public class leadCapList{
			private Object sequenceNumber;
			private Object lastName;
			private Object firstName;
			private Object job;
			private Object salesMan;
			private Object statusDate;
			private Object status;
			private Object createdOn;
			private Object id;
			public Object getSequenceNumber() {
				return sequenceNumber;
			}
			public void setSequenceNumber(Object sequenceNumber) {
				this.sequenceNumber = sequenceNumber;
			}
			public Object getLastName() {
				return lastName;
			}
			public void setLastName(Object lastName) {
				this.lastName = lastName;
			}
			public Object getFirstName() {
				return firstName;
			}
			public void setFirstName(Object firstName) {
				this.firstName = firstName;
			}
			public Object getJob() {
				return job;
			}
			public void setJob(Object job) {
				this.job = job;
			}
			public Object getSalesMan() {
				return salesMan;
			}
			public void setSalesMan(Object salesMan) {
				this.salesMan = salesMan;
			}
			public Object getStatusDate() {
				return statusDate;
			}
			public void setStatusDate(Object statusDate) {
				this.statusDate = statusDate;
			}
			public Object getStatus() {
				return status;
			}
			public void setStatus(Object status) {
				this.status = status;
			}
			public Object getCreatedOn() {
				return createdOn;
			}
			public void setCreatedOn(Object createdOn) {
				this.createdOn = createdOn;
			}
			public Object getId() {
				return id;
			}
			public void setId(Object id) {
				this.id = id;
			}
			
		}
		public String findCountryNameByCode(String countyName,String sessionCorpID,String parameter){
			String countryDesc="";
			try {
				String query="select description from refmaster where code='"+countyName+"' and  parameter= '"+ parameter+ "' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') and  (language='en' or language='' or language is null)";
				List list= this.getSession().createSQLQuery(query).list();
				if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
					countryDesc=list.get(0).toString();
				}
				
			} catch (DataAccessResourceFailureException e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			} 
			return countryDesc;
		}			
		
		public String getCompanyDivisionNetworkCoordinator(String companyDivision, String externalCorpID) {
			String networkCoordinator="";
			try {
				List networkCoordinatorList = new ArrayList();
				networkCoordinatorList = this .getSession() .createSQLQuery("select vanlineCoordinator from companydivision where vanlineCoordinator is not null and corpID = '"+ externalCorpID + "' and companyCode='"+companyDivision+"'").list();
				if (networkCoordinatorList != null && (!(networkCoordinatorList.isEmpty())) && (networkCoordinatorList.get(0) != null) && (!(networkCoordinatorList.get(0).toString().trim().equals("")))) {
					networkCoordinator = networkCoordinatorList.get(0).toString();
				}
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				getSessionCorpID();
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return networkCoordinator; 
		}
		
		public void saveLogError(String corpID, Date createdOn ,String message , String methods , String createdBy , String module ){
			ErrorLog errorLog =new ErrorLog();
		          errorLog.setCreatedOn(createdOn);
		          errorLog.setMessage(message);
		          errorLog.setCorpId(corpID);
		          errorLog.setMethods(methods);
		          errorLog.setCreatedBy(createdBy);
		          errorLog.setModule(module);
		          errorLogManager.save(errorLog);
		      }
		
		public void setErrorLogManager(ErrorLogManager errorLogManager) {
			this.errorLogManager = errorLogManager;
		}
		
		public List findPartnerDetailsForAutoComplete(String partnerName,String partnerNameType,String corpid,Boolean cmmDmmFlag){
			try {
				String regex = ".*\\d+.*";
				List partnerDetailList1=new ArrayList();
				String query="";
				if(partnerName.matches(regex)){
						 query="select distinct(p.partnercode),p.lastname,p.billingcountry,p.billingstate,p.billingcity,concat(if(isAgent is true,'Agent',if(isAccount is true,'Account',if(isPrivateParty is true,'Private',if(isVendor is true,'Vendor',if(isOwnerOp is true,'Owner Op',if(isCarrier is true,'Carrier','RSKY'))))))) as partnerType,c.bookingAgentCode,p.agentClassification, p.aliasName,p.terminalCountry,p.terminalState,p.terminalCity from partner p left outer join companydivision c on c.bookingAgentCode=p.partnerCode where  (p.partnercode like'"
								+ partnerName.replaceAll("'", "''")
								+ "%')and p.corpID in ('"
								+ hibernateUtil.getParentCorpID(corpid)
								+ "','"
								+ corpid
								+ "') "+partnerNameType+" and p.status='Approved' ORDER BY FIELD(p.agentClassification, 'Primary', 'Secondary', 'COD', 'OTHER',''),p.partnercode limit 100";
				}else{
					 query="select distinct(p.partnercode),p.lastname,p.billingcountry,p.billingstate,p.billingcity,concat(if(isAgent is true,'Agent',if(isAccount is true,'Account',if(isPrivateParty is true,'Private',if(isVendor is true,'Vendor',if(isOwnerOp is true,'Owner Op',if(isCarrier is true,'Carrier','RSKY'))))))) as partnerType,c.bookingAgentCode,p.agentClassification, p.aliasName,p.terminalCountry,p.terminalState,p.terminalCity from partner p left outer join companydivision c on c.bookingAgentCode=p.partnerCode where  (p.lastname like'"
							+ partnerName.replaceAll("'", "''")
							+ "%' or p.aliasName like'"
							+ partnerName.replaceAll("'", "''")
							+ "%')and p.corpID in ('"
							+ hibernateUtil.getParentCorpID(corpid)
							+ "','"
							+ corpid
							+ "') "+partnerNameType+" and p.status='Approved' ORDER BY FIELD(p.agentClassification, 'Primary', 'Secondary', 'COD', 'OTHER',''),p.partnercode limit 100";
				}
				List partnerDetailList=this.getSession().createSQLQuery(query).list();
				partnerDetailsList dto = null;
				Iterator it = partnerDetailList.iterator();
				while (it.hasNext()){
					Object []row= (Object [])it.next();
					dto = new partnerDetailsList();
					dto.setPartnercode(row[0]);
					dto.setLastname(row[1]);
					dto.setBillingcountry(row[2]);
					dto.setBillingstate(row[3]);
					dto.setBillingcity(row[4]);
					dto.setPartnerType(row[5]);
					dto.setBookingAgentCode(row[6]);
					dto.setAgentClassification(row[7]);
					dto.setAliasName(row[8]);
					dto.setTerminalCountry(row[9]);
					dto.setTerminalState(row[10]);
					dto.setTerminalCity(row[11]);
					partnerDetailList1.add(dto);
				}
				return partnerDetailList1;
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(corpid,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		
		public List findBillDetailsForAutoComplete(String partnerName,String partnerNameType,String corpid){
			try {
				String regex = ".*\\d+.*";
				List partnerDetailList1=new ArrayList();
				String query="";
				if(partnerName.matches(regex)){
						 query="select distinct(p.partnercode),p.lastname,p.billingcountry,p.billingstate,p.billingcity,concat(if(isAgent is true,'Agent',if(isAccount is true,'Account',if(isPrivateParty is true,'Private',if(isVendor is true,'Vendor',if(isOwnerOp is true,'Owner Op',if(isCarrier is true,'Carrier','RSKY'))))))) as partnerType,c.bookingAgentCode,p.agentClassification, p.aliasName,p.terminalCountry,p.terminalState,p.terminalCity  from partner p left outer join companydivision c on c.bookingAgentCode=p.partnerCode where  (p.partnercode like'"
								+ partnerName.replaceAll("'", "''")
								+ "%')and p.corpID in ('"
								+ hibernateUtil.getParentCorpID(corpid)
								+ "','"
								+ corpid
								+ "') "+partnerNameType+" and p.status='Approved' ORDER BY FIELD(p.agentClassification, 'Primary', 'Secondary', 'COD', 'OTHER',''),p.partnercode limit 100";
				}else{
					 query="select distinct(p.partnercode),p.lastname,p.billingcountry,p.billingstate,p.billingcity,concat(if(isAgent is true,'Agent',if(isAccount is true,'Account',if(isPrivateParty is true,'Private',if(isVendor is true,'Vendor',if(isOwnerOp is true,'Owner Op',if(isCarrier is true,'Carrier','RSKY'))))))) as partnerType,c.bookingAgentCode,p.agentClassification, p.aliasName,p.terminalCountry,p.terminalState,p.terminalCity from partner p left outer join companydivision c on c.bookingAgentCode=p.partnerCode where  (p.lastname like'"
							+ partnerName.replaceAll("'", "''")
							+ "%' or p.aliasName like'"
							+ partnerName.replaceAll("'", "''")
							+ "%')and p.corpID in ('"
							+ hibernateUtil.getParentCorpID(corpid)
							+ "','"
							+ corpid
							+ "')"+partnerNameType+" and p.status='Approved' ORDER BY FIELD(p.agentClassification, 'Primary', 'Secondary', 'COD', 'OTHER',''),p.partnercode limit 100";
				}
				List partnerDetailList=this.getSession().createSQLQuery(query).list();
				partnerDetailsList dto = null;
				Iterator it = partnerDetailList.iterator();
				while (it.hasNext()){
					Object []row= (Object [])it.next();
					dto = new partnerDetailsList();
					dto.setPartnercode(row[0]);
					dto.setLastname(row[1]);
					dto.setBillingcountry(row[2]);
					dto.setBillingstate(row[3]);
					dto.setBillingcity(row[4]);
					dto.setPartnerType(row[5]);
					dto.setBookingAgentCode(row[6]);
					dto.setAgentClassification(row[7]);
					dto.setAliasName(row[8]);
					dto.setTerminalCountry(row[9]);
					dto.setTerminalState(row[10]);
					dto.setTerminalCity(row[11]);
					partnerDetailList1.add(dto);
				}
				return partnerDetailList1;
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(corpid,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return null;
		}
		
		public class partnerDetailsList{
			private Object partnercode;
			private Object lastname;
			private Object billingcountry;
			private Object billingstate;
			private Object billingcity;
			private Object partnerType;
			private Object bookingAgentCode;
			private Object agentClassification;
			private Object aliasName;
			private Object extReference;
			private Object terminalCountry;
			private Object terminalState;
			private Object terminalCity;
			public Object getPartnercode() {
				return partnercode;
			}
			public void setPartnercode(Object partnercode) {
				this.partnercode = partnercode;
			}
			public Object getLastname() {
				return lastname;
			}
			public void setLastname(Object lastname) {
				this.lastname = lastname;
			}
			public Object getBillingcountry() {
				return billingcountry;
			}
			public void setBillingcountry(Object billingcountry) {
				this.billingcountry = billingcountry;
			}
			public Object getBillingstate() {
				return billingstate;
			}
			public void setBillingstate(Object billingstate) {
				this.billingstate = billingstate;
			}
			public Object getBillingcity() {
				return billingcity;
			}
			public void setBillingcity(Object billingcity) {
				this.billingcity = billingcity;
			}
			public Object getPartnerType() {
				return partnerType;
			}
			public void setPartnerType(Object partnerType) {
				this.partnerType = partnerType;
			}
			public Object getBookingAgentCode() {
				return bookingAgentCode;
			}
			public void setBookingAgentCode(Object bookingAgentCode) {
				this.bookingAgentCode = bookingAgentCode;
			}
			public Object getAgentClassification() {
				return agentClassification;
			}
			public void setAgentClassification(Object agentClassification) {
				this.agentClassification = agentClassification;
			}
			public Object getAliasName() {
				return aliasName;
			}
			public void setAliasName(Object aliasName) {
				this.aliasName = aliasName;
			}
			public Object getExtReference() {
				return extReference;
			}
			public void setExtReference(Object extReference) {
				this.extReference = extReference;
			}
			public Object getTerminalCountry() {
				return terminalCountry;
			}
			public void setTerminalCountry(Object terminalCountry) {
				this.terminalCountry = terminalCountry;
			}
			public Object getTerminalState() {
				return terminalState;
			}
			public void setTerminalState(Object terminalState) {
				this.terminalState = terminalState;
			}
			public Object getTerminalCity() {
				return terminalCity;
			}
			public void setTerminalCity(Object terminalCity) {
				this.terminalCity = terminalCity;
			}			
			
		}

	/*	public void  updateSOExtractFieldOnCFSave(Long custid) {
			try {
				List list=this.getSession().createSQLQuery("select id,isSoExtract from serviceorder where customerFileId='" + custid+"'").list(); 
				Iterator it=list.iterator();
				while(it.hasNext()){
					Object rows[]=(Object[])it.next();
					if(rows[1]==null){
						String query="update serviceorder set isSoExtract=null"
							+ " where  id='" + new Long(rows[0].toString())+"'";
							
			getSession().createSQLQuery(query).executeUpdate();
					}else{
						String query="update serviceorder set isSoExtract=false"
							+ " where  id='" + new Long(rows[0].toString())+"'";
							
			getSession().createSQLQuery(query).executeUpdate();
					}
				}
				
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			} 
		}*/
		public void updateUpdaterOnCFSave(Long id){
			try {
				List list=this.getSession().createSQLQuery("select id,isUpdater from serviceorder where customerFileId='" + id+"'").list(); 
				Iterator it=list.iterator();
				while(it.hasNext()){
					Object rows[]=(Object[])it.next();
					if(rows[1]==null){
						String query="update serviceorder set isUpdater=null"
							+ " where  id='" + new Long(rows[0].toString())+"'";
							
			getSession().createSQLQuery(query).executeUpdate();
					}else{
						String query="update serviceorder set isUpdater=false"
							+ " where  id='" + new Long(rows[0].toString())+"'";
							
			getSession().createSQLQuery(query).executeUpdate();
					}
				}
				
			} catch (Exception e) {
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			} 
		}
		   public String getUserCompanyDivisionFilter() {
		    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		        User user = (User)auth.getPrincipal();
		        Map filterMap=user.getFilterMap(); 
		    	List companyDivisionList=new ArrayList();
		    	String companyDivisionSet="";
		    	try {
					
		    		if(filterMap.containsKey("serviceOrderCompanyDivisionFilter")){
			    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderCompanyDivisionFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						Iterator listIterator= partnerCorpIDList.iterator();
						while (listIterator.hasNext()) {
							companyDivisionList.add("'" + listIterator.next().toString() + "'");	
						}
						companyDivisionSet= companyDivisionList.toString().replace("[","").replace("]", "");
			    	}
						
		    	}catch(Exception e){}
		    	return companyDivisionSet;
		    }	
		public List surveyCalendarList(String corpID,String consultant){
			String userFilter=getUserCompanyDivisionFilter();
			Date startDate=null;
			Date endDate=null;
			Calendar calc = Calendar.getInstance();
			calc.setTime(new Date());
			calc.add(Calendar.DATE, -60);
			startDate=calc.getTime();
			calc.setTime(new Date());
			calc.add(Calendar.DATE, 360);
			endDate=calc.getTime();
			
			String startDate1="";
			String endDate1="";
			
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			
			StringBuilder sb = new StringBuilder(df.format(startDate));
			startDate1 = sb.toString();
			
			sb = new StringBuilder(df.format(endDate));
			endDate1 = sb.toString();
			
			
   String query="SELECT "+ 
				"if(actualSurveyDate is not null,date_format(actualSurveyDate,'%Y-%m-%d'),date_format(survey,'%Y-%m-%d')) 'start', "+ 
				"if(actualSurveyDate is not null,date_format(actualSurveyDate,'%Y-%m-%d'),date_format(survey,'%Y-%m-%d')) 'end', "+
				"if(surveyTime is null OR surveyTime='' OR surveyTime='00:00','',surveyTime) 'surveyTimeFrom', "+
				"if(surveyTime2 is null OR surveyTime2='' OR surveyTime2='00:00','',surveyTime2) 'surveyTimeTo', "+
				"if(firstName is null or firstName='','',firstName) 'firstName', "+ 
				"if(lastName is null or lastName='','',lastName) 'lastName', "+
				"if(originAddress1 is null or originAddress1='','',originAddress1) 'originAddress1', "+ 
				"if(originCity is null or originCity='','',originCity) 'originCity', "+
				"if(originState is null or originState='','',originState) 'originState', "+ 
				"if(originCountryCode is null or originCountryCode='','',originCountryCode) 'originCountry', "+ 
				"if(originZip is null or originZip='','',originZip) 'originZip', "+
				"if(destinationAddress1 is null or destinationAddress1='','',destinationAddress1) 'destinationAddress1', "+ 
				"if(destinationCity is null or destinationCity='','',destinationCity) 'destinationCity', "+
				"if(destinationState is null or destinationState='','',destinationState) 'destinationState', "+ 
				"if(destinationCountryCode is null or destinationCountryCode='','',destinationCountryCode) 'destinationCountry', "+ 
				"if(destinationZip is null or destinationZip='','',destinationZip) 'destinationZip', "+
				"if(estimator is null or estimator='','',estimator) 'estimator', "+ 
				"if(actualSurveyDate is not null,'<font color=red>A</font>','<font color=red>S</font>') surveyDateStatus, "+   
				"id as customerId "+
				"FROM customerfile where corpID = '"+corpID+"' "+
				"AND ((actualSurveyDate is not null) OR (survey is not null)) "+ 
				"and status not in ('CANCEL','CLOSED','CNCL') and quotationStatus not in ('Rejected','Cancelled') "+
				"AND ((date_format(actualSurveyDate,'%Y-%m-%d') BETWEEN '"+startDate1+"' and  '"+endDate1+"') OR (date_format(survey,'%Y-%m-%d') BETWEEN '"+startDate1+"' and  '"+endDate1+"')) "+
				((userFilter!=null && !userFilter.equalsIgnoreCase(""))?" and companyDivision in ("+userFilter+")":"")+
				((consultant!=null && !consultant.equalsIgnoreCase(""))?" and estimator = '"+consultant+"'":"")+
				"order by 1,2";
			
						List DailyList=this.getSession().createSQLQuery(query).list();
						SurveyCalendarDTO dto = null;
						List dailySheetList=new ArrayList();
			Iterator it = DailyList.iterator();
			String str="";
			while(it.hasNext()){
				Object []row= (Object [])it.next();
				dto = new SurveyCalendarDTO();
				dto.setStart(row[0]);
				dto.setEnd(row[1]);
				dto.setSurveyTimeFrom(row[2]);
				dto.setSurveyTimeTo(row[3]);
				dto.setFirstName(row[4]);
				dto.setLastName(row[5]);
				
				dto.setOriginAddress1(row[6]);
				dto.setOriginCity(row[7]);
				dto.setOriginState(row[8]);
				dto.setOriginCountry(row[9]);
				dto.setOriginZip(row[10]);

				dto.setDestinationAddress1(row[11]);
				dto.setDestinationCity(row[12]);
				dto.setDestinationState(row[13]);
				dto.setDestinationCountry(row[14]);
				dto.setDestinationZip(row[15]);

				
				dto.setEstimator(row[16]);
				dto.setSurveyDateStatus(row[17]);
				dto.setId(row[18]);
				if(dto.getFirstName()!=null && !dto.getFirstName().toString().trim().equalsIgnoreCase("")){
					dto.setFirstName(dto.getFirstName().toString().trim().replaceAll("\"", ""));
				}
				if(dto.getLastName()!=null && !dto.getLastName().toString().trim().equalsIgnoreCase("")){
					dto.setLastName(dto.getLastName().toString().trim().replaceAll("\"", ""));
				}	
				if(dto.getOriginAddress1()!=null && !dto.getOriginAddress1().toString().trim().equalsIgnoreCase("")){
					dto.setOriginAddress1(dto.getOriginAddress1().toString().trim().replaceAll("\"", ""));
				}
				
				str=	(!dto.getSurveyTimeFrom().toString().equalsIgnoreCase("") && !dto.getSurveyTimeTo().toString().equalsIgnoreCase("")?"<font color=#2779aa><strong>"+dto.getSurveyTimeFrom()+" - "+dto.getSurveyTimeTo()+"</strong></font> ":"")+""
						+ (((dto.getFirstName()!=null && !dto.getFirstName().toString().equalsIgnoreCase(""))||(dto.getLastName()!=null && !dto.getLastName().toString().equalsIgnoreCase("")))?" for <font color=#2779aa><a target=_blank href=editCustomerFile.html?id="+dto.getId()+">"+dto.getFirstName()+" "+dto.getLastName()+"</a></font> ":"")+""
						+ " at: "+(dto.getOriginAddress1()!=null && !dto.getOriginAddress1().toString().equalsIgnoreCase("")?dto.getOriginAddress1()+", ":"")+""
						+ ""+(dto.getOriginCity()!=null && !dto.getOriginCity().toString().equalsIgnoreCase("")?dto.getOriginCity()+", ":"")+""
						+ ""+(dto.getOriginState()!=null && !dto.getOriginState().toString().equalsIgnoreCase("")?dto.getOriginState()+" ":"")+""
						+ ""+(dto.getOriginZip()!=null && !dto.getOriginZip().toString().equalsIgnoreCase("")?dto.getOriginZip()+(dto.getOriginCountry()!=null && !dto.getOriginCountry().toString().equalsIgnoreCase("")?", ":""):"")+""
						+ ""+(dto.getOriginCountry()!=null && !dto.getOriginCountry().toString().equalsIgnoreCase("")?dto.getOriginCountry()+"":"")+""
						+ ""+(dto.getEstimator()!=null && !dto.getEstimator().toString().equalsIgnoreCase("")?" by "+dto.getEstimator():"")+""
						+ ""+(dto.getSurveyDateStatus()!=null && !dto.getSurveyDateStatus().toString().equalsIgnoreCase("")?" ("+dto.getSurveyDateStatus()+")":"");
					    
				dto.setTitle(str);
				dailySheetList.add(dto);
			}
			return dailySheetList;
		}
		
		 public class SurveyCalendarDTO {			
			private Object start;
			private Object end;
			private Object title;
			
			private Object surveyTimeFrom;
			private Object surveyTimeTo;
			private Object firstName;
			private Object lastName;
			
			private Object originAddress1;
			private Object originCity;
			private Object originState;
			private Object originCountry;
			private Object originZip;
			
			private Object destinationAddress1;
			private Object destinationCity;
			private Object destinationState;
			private Object destinationCountry;
			private Object destinationZip;
			
			private Object estimator;
			private Object surveyDateStatus;
			private Object id;
			public Object getStart() {
				return start;
			}
			public void setStart(Object start) {
				this.start = start;
			}
			public Object getEnd() {
				return end;
			}
			public void setEnd(Object end) {
				this.end = end;
			}
			public Object getTitle() {
				return title;
			}
			public void setTitle(Object title) {
				this.title = title;
			}
			public Object getSurveyTimeFrom() {
				return surveyTimeFrom;
			}
			public void setSurveyTimeFrom(Object surveyTimeFrom) {
				this.surveyTimeFrom = surveyTimeFrom;
			}
			public Object getSurveyTimeTo() {
				return surveyTimeTo;
			}
			public void setSurveyTimeTo(Object surveyTimeTo) {
				this.surveyTimeTo = surveyTimeTo;
			}
			public Object getFirstName() {
				return firstName;
			}
			public void setFirstName(Object firstName) {
				this.firstName = firstName;
			}
			public Object getLastName() {
				return lastName;
			}
			public void setLastName(Object lastName) {
				this.lastName = lastName;
			}
			public Object getOriginAddress1() {
				return originAddress1;
			}
			public void setOriginAddress1(Object originAddress1) {
				this.originAddress1 = originAddress1;
			}
			public Object getOriginCity() {
				return originCity;
			}
			public void setOriginCity(Object originCity) {
				this.originCity = originCity;
			}
			public Object getOriginState() {
				return originState;
			}
			public void setOriginState(Object originState) {
				this.originState = originState;
			}
			public Object getOriginCountry() {
				return originCountry;
			}
			public void setOriginCountry(Object originCountry) {
				this.originCountry = originCountry;
			}
			public Object getOriginZip() {
				return originZip;
			}
			public void setOriginZip(Object originZip) {
				this.originZip = originZip;
			}
			public Object getDestinationAddress1() {
				return destinationAddress1;
			}
			public void setDestinationAddress1(Object destinationAddress1) {
				this.destinationAddress1 = destinationAddress1;
			}
			public Object getDestinationCity() {
				return destinationCity;
			}
			public void setDestinationCity(Object destinationCity) {
				this.destinationCity = destinationCity;
			}
			public Object getDestinationState() {
				return destinationState;
			}
			public void setDestinationState(Object destinationState) {
				this.destinationState = destinationState;
			}
			public Object getDestinationCountry() {
				return destinationCountry;
			}
			public void setDestinationCountry(Object destinationCountry) {
				this.destinationCountry = destinationCountry;
			}
			public Object getDestinationZip() {
				return destinationZip;
			}
			public void setDestinationZip(Object destinationZip) {
				this.destinationZip = destinationZip;
			}
			public Object getEstimator() {
				return estimator;
			}
			public void setEstimator(Object estimator) {
				this.estimator = estimator;
			}
			public Object getSurveyDateStatus() {
				return surveyDateStatus;
			}
			public void setSurveyDateStatus(Object surveyDateStatus) {
				this.surveyDateStatus = surveyDateStatus;
			}
			public Object getId() {
				return id;
			}
			public void setId(Object id) {
				this.id = id;
			}
		}
		 
		
		public String findDefaultValFromPartner(String bCode, String sessionCorpID){
			String accValue="";
			try {
				String query="select concat(if(contract is null or contract='','NA',contract),'~',if(networkPartnerCode is null or networkPartnerCode='','NA',networkPartnerCode),'~',if(networkPartnerName is null or networkPartnerName='','NA',networkPartnerName),"
						+ "'~',if(source is null or source='','NA',source),'~',if(vendorCode is null or vendorCode='','NA',vendorCode),'~',if(vendorName is null or vendorName='','NA',vendorName),'~',if(insuranceHas is null or insuranceHas='','NA',insuranceHas),"
						+ "'~',if(insuranceOption is null or insuranceOption='','NA',insuranceOption),'~',if(defaultVat is null or defaultVat='','NA',defaultVat),'~',if(billToAuthorization is null or billToAuthorization='','NA',billToAuthorization),"
						+ " '~',if(storageEmail is null or storageEmail='','NA',storageEmail),'~',if(storageEmailType is null or storageEmailType='','NA',storageEmailType),'~',if(vatBillingGroup is null or vatBillingGroup='','NODATA',vatBillingGroup)) "
						+" from partnerprivate where partnerCode='"+ bCode+ "' and corpid in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')" ;
				List temp =  this.getSession().createSQLQuery(query).list();
				if ((temp != null) && (!temp.isEmpty()) && (temp.get(0) != null)) {
					accValue = temp.get(0).toString();
				}		
							
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
			return accValue;
		}
		
	public void  updateSODashboardSurveyDt(CustomerFile customerFile) {
		     Long t1=System.currentTimeMillis();
			try {
				List list=this.getSession().createSQLQuery("select shipNumber from serviceorder where customerFileId='" + customerFile.getId()+"'").list(); 
				Iterator it=list.iterator();
				while(it.hasNext()){
					Object rows=it.next();
					if(rows!=null && !(rows.toString().equals(""))){
						String updateQuery="";
						if(customerFile!=null && customerFile.getActualSurveyDate()!=null){					
							Date dt=new Date();	
							SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
							try{
								 format=new SimpleDateFormat("yyyy-MM-dd");
								String dt1=format.format(customerFile.getActualSurveyDate());
							     updateQuery="update sodashboard set survey='"+dt1+"' where shipNumber='"+rows.toString()+"'";
								}catch(Exception e){					
								}							
							
						}else{
							
							updateQuery="update sodashboard set survey=null where shipNumber='"+rows.toString()+"'";	
						}
						if(updateQuery!=null && !(updateQuery.equals(""))){
							 getSession().createSQLQuery(updateQuery).executeUpdate();
							}
						updateQuery="";
						if(customerFile!=null && customerFile.getSurvey()!=null){
							Date dt=new Date();	
							String dt1="";
							SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
							try{
								 format=new SimpleDateFormat("yyyy-MM-dd");
								 dt1=format.format(customerFile.getSurvey());
								}catch(Exception e){					
								}
							String load=format.format(customerFile.getSurvey());
							String curr=format.format(dt);
							updateQuery="update sodashboard set surveyDt='"+dt1+"' where shipNumber='"+rows.toString()+"'";							
							
						}else{
							updateQuery="update sodashboard set surveyDt=null where shipNumber='"+rows.toString()+"'";	
						}
						if(updateQuery!=null && !(updateQuery.equals(""))){
						 getSession().createSQLQuery(updateQuery).executeUpdate();
						}
						
					}
				}
			}catch (Exception e) {
				logger.error("Error executing query"+ e,e);
			}
			Long t2=System.currentTimeMillis();
			
			}
	public List findNotesValueForOutlook(String seqNumber,Long id, String sessionCorpID){
		List noteDetailList=new ArrayList();
		try {
			String query="select subject,note from notes where corpid='"+sessionCorpID+"' "
						+" and notesid='"+seqNumber+"' and noteskeyid='"+id+"' and (displaysurvey is true or noteSubType='Survey') ";
			List list =  this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			while(it.hasNext()){
				 Object []row= (Object [])it.next();
				 notesDetailDTO dto = new notesDetailDTO();
				 dto.setSubject(row[0].toString());
				 dto.setDetails(row[1].toString());
				 noteDetailList.add(dto);
			}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return noteDetailList;
	}
	public class notesDetailDTO{
		private Object subject;
		private Object details;
		public Object getSubject() {
			return subject;
		}
		public void setSubject(Object subject) {
			this.subject = subject;
		}
		public Object getDetails() {
			return details;
		}
		public void setDetails(Object details) {
			this.details = details;
		}
	}
	public List getAllUserContactListByPartnerCode(String billToCode , String sessionCorpID ,String name, String emailId )
	{
		String query="";
		query="Select distinct concat(first_name,' ',last_name)," +
		"ap.email,ap.phone_number from app_user ap, userdatasecurity uds, datasecurityset  dss, " +
		"datasecurityfilter dsf, datasecuritypermission dsp " +
		"where uds.userdatasecurity_id=dss.id and ap.id = uds.user_id "+
		"and userType='ACCOUNT' "+
		"and ap.account_enabled is true "+
		"and ap.account_expired is not true "+
		"and ap.account_locked is not true "+
		"and ap.credentials_expired is not true "+
		"and dsf.id=dsp.datasecurityfilter_id " +
		"and dss.id=dsp.datasecurityset_id " +
		"and dsf.filtervalues in('"+billToCode+"')" +
		"and ap.corpID in ('TSFT','"+sessionCorpID+"')";
		if(!name.equalsIgnoreCase("")){
			query = query +" and concat(ap.first_name,' ',ap.last_name) like '%" + name + "%'";
		}
		if(!emailId.equalsIgnoreCase("")){
			query = query +" and email like '%" + emailId + "%'";	
		}
		query = query +" order by 1";	
		List agentUsers= this.getSession().createSQLQuery(query).list();
		
		
		List result= new ArrayList(); 
		
		if(agentUsers.size()>0){
			Iterator it=agentUsers.iterator();
			while(it.hasNext()) 
			{
				Object []obj=(Object[]) it.next();
				UserDTO userDTO=new UserDTO();
				userDTO.setFull_name(obj[0]);
				userDTO.setEmail(obj[1]);
				userDTO.setContact(obj[2]);
				result.add(userDTO);
			}
		}
		return  result;
	}
	

	public  class UserDTO
	 {
		private Object full_name;
		private Object email;
		private Object contact;
		public Object getFull_name() {
			return full_name ;
		}
		public void setFull_name(Object full_name) {
			this.full_name = full_name;
		}
		
		public Object getEmail() {
			return email;
		}
		public void setEmail(Object email) {
			this.email = email;
		}
		public Object getContact() {
			return contact;
		}
		public void setContact(Object contact) {
			this.contact = contact;
		}
		
	 }

	public void updateServiceOrderLinkedField(Long id, String lastName, String prefix, String rank, String firstName, String socialSecurityNumber, Boolean vip,String userName) {
				try {
			  getSession() .createSQLQuery("update serviceorder set lastName='"+lastName+"', prefix='"+prefix+"', rank='"+rank+"', firstName='"+firstName+"', socialSecurityNumber='"+socialSecurityNumber+"', vip="+vip+" , updatedBy='"+userName+"', updatedOn=now()  where customerFileId= '"+id+"' ") .executeUpdate();
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		
	}
	
	
	public List findBilltoNameAutocompleteList(String billtoName, String corpId){
		try
		{
		String query="select distinct (billtoname) from serviceorder where  billtoname like '"+billtoName+"%' and corpID in ('"+corpId+"','TSFT','"+hibernateUtil.getParentCorpID(corpId)+"') ";
		List temp =  this.getSession().createSQLQuery(query).list();
		return temp;
	} catch (Exception e) {
		 e.printStackTrace();
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
    	  saveLogError(corpId,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	}
	return null;
	}
	public void updateLinkedCustomerFileStatus(String status, int statusNumber, Date statusDate, Long customerFileId,String userName) {
		getSession().createSQLQuery("update customerfile set status='"+status+"',statusNumber="+statusNumber+",statusDate=now(), updatedOn=now(),updatedBy='"+userName+"' where id='"+customerFileId+"' and status !='"+status+"' ").executeUpdate();
		
	}
	public String getNetworkUnit(String corpID) { 
		String networkCoordinator="";
		try {
			List networkCoordinatorList = new ArrayList();
			networkCoordinatorList = this
					.getSession()
					.createSQLQuery(
							"select concat(if(WeightUnit is null or WeightUnit='','No',WeightUnit),'~',if(VolumeUnit is null or VolumeUnit='','No',VolumeUnit)) from systemdefault where networkCoordinator is not null and corpID = '"
									+ corpID + "'").list();
			if (networkCoordinatorList != null
					&& (!(networkCoordinatorList.isEmpty()))
					&& networkCoordinatorList.get(0) != null) {
				networkCoordinator = networkCoordinatorList.get(0).toString();
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return networkCoordinator; 
	}
	public void updateBillingNetworkBillToCode(String sequenceNumber, String accountCode, String accountName, String oldCustomerFileAccountCode) {
		getHibernateTemplate().bulkUpdate("update AccountLine set networkBillToCode='"+accountCode+"',networkBillToName='"+accountName+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where networkBillToCode='"+oldCustomerFileAccountCode+"' and   sequenceNumber=?",sequenceNumber);
		getHibernateTemplate().bulkUpdate("update Billing set networkBillToCode='"+accountCode+"',networkBillToName='"+accountName+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where networkBillToCode='"+oldCustomerFileAccountCode+"' and   sequenceNumber=?",sequenceNumber);
		
	}
	public String checkNetworkCoordinatorInUser(String cfContractType, String sessionCorpID){
		String type = "";
		List list = this.getSession().createSQLQuery("select count(*) from app_user where username='"+cfContractType+"' and networkCoordinatorStatus=TRUE " ).list();	
		if(list!=null && (!(list.isEmpty())) && list.get(0)!=null)
		{
			if(Integer.parseInt(list.get(0).toString()) > 0 ){
				type="true";  
			}
		}
		return type;
	}
	
	public String checkCorpIdInCompDiv(String bookAg){
		String type = "";
		List list = this.getSession().createSQLQuery("SELECT corpid FROM companydivision c where bookingAgentCode='"+bookAg+"'" ).list();	
		if(list!=null && (!(list.isEmpty())) && list.get(0)!=null)
		{
			type=list.get(0).toString();
		}
		return type;
	}
	public List findOpenCompanyDivision(String sessionCorpID) {
		try {
			return getSession()
					.createQuery(
							"select distinct(companyCode) from CompanyDivision where corpID='"
									+ sessionCorpID + "' and closeddivision is false  order by companyCode").list();
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public void updateBillToName(String billToCode, String billToName, String corpid, Long id, String shipNumber){
		try {
			getHibernateTemplate().flush();
			String query1 = "update PartnerPrivate set lastName='"+billToName+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where partnerCode='"+billToCode+"' and corpid='"+corpid+"'";
			getHibernateTemplate().bulkUpdate(query1);
			
			getHibernateTemplate().flush();
			String query2 = "update PartnerPublic set lastName='"+billToName+"',aliasName='"+billToName+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where partnerCode='"+billToCode+"' and corpid='"+corpid+"'";
			getHibernateTemplate().bulkUpdate(query2);
			getHibernateTemplate().flush();
			String query4 = "update Billing set billtoname='"+billToName+"', billToCode='"+billToCode+"', privatePartyBillingCode='"+billToCode+"' , privatePartyBillingName='"+billToName+"', updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where shipNumber in ("+shipNumber+") and corpID='"+corpid+"'";
			getHibernateTemplate().bulkUpdate(query4);
			
			getHibernateTemplate().flush();
			String query5 = "update WorkTicket set account='"+billToCode+"',accountName='"+billToName+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where shipNumber in ("+shipNumber+") and corpid='"+corpid+"'";
			getHibernateTemplate().bulkUpdate(query5);
			
			getHibernateTemplate().flush();
			String query6 = "update AccountLine set billToName='"+billToName+"', billToCode='"+billToCode+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where shipNumber in ("+shipNumber+") and (recPostDate is null) and corpID='"+corpid+"'";
			getHibernateTemplate().bulkUpdate(query6);
			
			getHibernateTemplate().flush();
			getHibernateTemplate().clear();
			
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
	}
	
	public Boolean findCustPortalId(String linkedSeqNumber,	String sequenceNumber){
		Boolean cPortalList=false;
		try {
			String query = "select portalIdActive from customerfile where sequencenumber in("+linkedSeqNumber+") and sequencenumber!='"+sequenceNumber+"' and portalIdActive is true ";
			List custPortalList = this.getSession().createSQLQuery(query).list();
			if (custPortalList!=null && (!(custPortalList.isEmpty())) && custPortalList.get(0).toString().equals("true")) {
				cPortalList = true;
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return cPortalList;
	}
	
	public String getCompanyCodeAndJobFromCompanyDivision(String sessionCorpID, String bookAg){
		String returnVal="";
		try {
		List list = this.getSession().createSQLQuery("select concat(if(companyCode is null or companyCode='','NA',companyCode),'~',if(vanlinedefaultJobtype is null or vanlinedefaultJobtype='','NA',vanlinedefaultJobtype)) from companydivision where bookingAgentCode='"+bookAg+"' and corpid='"+sessionCorpID+"'" ).list();	
		if(list!=null && (!(list.isEmpty())) && list.get(0)!=null){
			returnVal=list.get(0).toString();
		}
		}catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return returnVal;
	}
	
	public List checkPrivatePartyFromPartner(String billToCode,String sessionCorpID,String bookingAgentCode){
		List temp=new ArrayList();
		try
		{
		String query="select if(isPrivateParty,1,0) from partner where partnercode='"+billToCode+"'  and corpID ='"+sessionCorpID+"' ";
		temp =  this.getSession().createSQLQuery(query).list();
		if(temp!=null && (!(temp.isEmpty())) && temp.get(0)!=null){
		String query1="select concat('~',bookingAgentCode) from companydivision where bookingAgentCode in('"+bookingAgentCode+"') and corpID ='"+sessionCorpID+"' ";
		List temp1 =  this.getSession().createSQLQuery(query1).list();
		if(temp1!=null && (!(temp1.isEmpty())) && temp1.get(0)!=null){
			temp.add(temp1.get(0).toString());
		}else{
			temp.add("~"+"1");
		}
		}
		return temp;
	} catch (Exception e) {
		 e.printStackTrace();
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	}
	return null;
	}
	public String getSurveyMessageText(String parameter, String language, String sessionCorpID){
		List temp=new ArrayList();
		String textVal="";
		String	query="select if(description is null or description='','1',description) from refmaster where parameter='"+ parameter+ "' and language = '"+language+"' and corpID='"+ sessionCorpID + "'";
		String	query1="select if(description is null or description='','1',description) from refmaster where parameter='"+ parameter+ "' and language = 'en' and corpID='"+ sessionCorpID + "'";
		
		try {
			temp = this.getSession().createSQLQuery(query).list();
			if(temp!=null && (!(temp.isEmpty())) && temp.get(0)!=null){
				textVal = temp.get(0).toString(); 
			}else{
				temp = this.getSession().createSQLQuery(query1).list();
				if(temp!=null && (!(temp.isEmpty())) && temp.get(0)!=null){
					textVal = temp.get(0).toString(); 
				}
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return textVal;
	}
	
	public String getPreffixFromRefmaster(String parameter, String language, String code){
		List temp=new ArrayList();
		String preffixVal="";
		String query="";
		if((language!=null && !language.equalsIgnoreCase("")) && language.equalsIgnoreCase("fr")){
			query = "select concat(flex2,' ',flex1) from refmaster where parameter ='"+parameter+"' and language='"+language+"' and code='"+code+"'";
		}else if((language!=null && !language.equalsIgnoreCase("")) && language.equalsIgnoreCase("nl")){
			query = "select flex2 from refmaster where parameter ='"+parameter+"' and language='"+language+"' and code='"+code+"'";
		}else{
			query = "select code from refmaster where parameter ='"+parameter+"' and language='en' and code='"+code+"'";
		}
		try {
			temp = this.getSession().createSQLQuery(query).list();
			if(temp!=null && (!(temp.isEmpty())) && temp.get(0)!=null){
				preffixVal = temp.get(0).toString(); 
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return preffixVal;
	}
	
	public String getDataUpdateDiffLanguage(String services, String corpId,String changedLanguage,Long serviceId){
		List temp=new ArrayList();
		String updatedServiceValue="";
		String query="";
		if(services!=null && !services.equals("")){
			String datavalue[] = services.split("#");
			for(String ss:datavalue){
				String idvalue[] = ss.split(":");
				if(changedLanguage.equals("en")){
					query="select englishId from refquoteservices where id='"+idvalue[0]+"' and corpID = '"+corpId+"' ";
				}else if(changedLanguage.equals("nl")){
					query="select dutchId from refquoteservices where id='"+idvalue[0]+"' and corpID = '"+corpId+"' ";
				}else if(changedLanguage.equals("fr")){
                	query="select frenchId from refquoteservices where id='"+idvalue[0]+"' and corpID = '"+corpId+"' ";
				}else if(changedLanguage.equals("de")){
                	query="select germanId from refquoteservices where id='"+idvalue[0]+"' and corpID = '"+corpId+"' ";
				}else{
					query="select englishId from refquoteservices where id='"+idvalue[0]+"' and corpID = '"+corpId+"' ";
				}
				
				temp = this.getSession().createSQLQuery(query).list();
				if(temp!=null && (!(temp.isEmpty())) && temp.get(0)!=null){
					if(updatedServiceValue.equals("")){
						updatedServiceValue = temp.get(0).toString()+":"+idvalue[1];	
					}else{
						updatedServiceValue = updatedServiceValue+"#"+temp.get(0).toString()+":"+idvalue[1];
					}
				}else{
					if(changedLanguage.equals("fr")){
						changedLanguage="French";
					}else if(changedLanguage.equals("nl")){
						changedLanguage="Dutch";
					}else if(changedLanguage.equals("de")){
						changedLanguage="German";
					}else{
						changedLanguage="English";
					}
					return "Not all Inclusions and Exclusions are available in "+changedLanguage+". Not all Service Orders were translated.";
				}
			}
			getHibernateTemplate().flush();
			String query1="update ServiceOrder set incExcServiceType='"+updatedServiceValue+"',incExcServiceTypeLanguage='"+changedLanguage+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where id="+serviceId+"";
			getHibernateTemplate().bulkUpdate(query1);
			getHibernateTemplate().flush();
			getHibernateTemplate().clear();
		}
		return null;
	}
	public Map<String, String> getContractAccountMap(String sessionCorpID) {
		
		List contractAccountList= getHibernateTemplate().find("select contract,accountCode,accountName from Contract where corpID='"+sessionCorpID+"' and accountCode !='' and accountCode is not null and accountName!='' and accountName is not null ");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();  
		Iterator its=contractAccountList.iterator();
		while (its.hasNext()) {
		Object[] row = (Object[]) its.next();
				parameterMap.put(row[0].toString().trim(), row[1].toString().trim()+"~"+row[2].toString().trim());
			} 
		return parameterMap;
	}
	public String getContractAccountDetail(String externalCorpID, String contract) {
		String contractAccount="";
		try{
		List contractAccountList= this.getSession().createSQLQuery("select CONCAT(accountCode,'~',accountName) from contract where corpID='"+externalCorpID+"' and contract='"+contract+"' and accountCode !='' and accountCode is not null and accountName!='' and accountName is not null ").list();
		if(contractAccountList !=null && (!(contractAccountList.isEmpty())) && contractAccountList.get(0)!=null && (!(contractAccountList.get(0).toString().trim().equals("")))){
			contractAccount	=contractAccountList.get(0).toString().trim();
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return contractAccount;
	}
	public List findAccEstimateRevisionFieldToSync(String modalClass, String type) {
		try {
			return this
					.getSession()
					.createSQLQuery(
							"select concat(fieldName,'~',if(toFieldName is null or toFieldName='',fieldName,toFieldName)) from networkdatafields where modelName='"
									+ modalClass
									+ "' and transactionType='Update'  and (type='' or type is null or type='"
									+ type + "' or type='CMM/DMM') and (toFieldName like 'estimate%' or fieldName like 'estimate%' or toFieldName like 'revision%' or fieldName like 'revision%')  ").list();
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List findIkeaOrderPartnerDetailsForAutoComplete(String partnerName,String partnerCodes,String corpid){
		try {
			String regex = ".*\\d+.*";
			List partnerDetailList1=new ArrayList();
			String query="";
			if(partnerName.matches(regex)){
					 query="select distinct(p.partnercode),p.lastname,p.billingcountry,p.billingstate,p.billingcity,concat(if(isAgent is true,'Agent',if(isAccount is true,'Account',if(isPrivateParty is true,'Private',if(isVendor is true,'Vendor',if(isOwnerOp is true,'Owner Op',if(isCarrier is true,'Carrier','RSKY'))))))) as partnerType, p.aliasName,p.extReference from partner p "
					 		+ " where  (p.partnercode like'"+ partnerName.replaceAll("'", "''")	+ "%')and p.corpID in ('"+corpid+"','TSFT','"+hibernateUtil.getParentCorpID(corpid)+"') "
							+ " and partnercode in ("+partnerCodes+") and p.status='Approved' ORDER BY p.partnercode limit 100";
			}else{
				 query="select distinct(p.partnercode),p.lastname,p.billingcountry,p.billingstate,p.billingcity,concat(if(isAgent is true,'Agent',if(isAccount is true,'Account',if(isPrivateParty is true,'Private',if(isVendor is true,'Vendor',if(isOwnerOp is true,'Owner Op',if(isCarrier is true,'Carrier','RSKY'))))))) as partnerType, p.aliasName,p.extReference from partner p "
				 		+ " where  (p.lastname like'"+ partnerName.replaceAll("'", "''")+ "%' or p.aliasName like'"+partnerName.replaceAll("'", "''")+ "%')"
						+ " and p.corpID in ('"+corpid+"','TSFT','"+hibernateUtil.getParentCorpID(corpid)+"') and partnercode in ("+partnerCodes+") and p.status='Approved' ORDER BY p.partnercode limit 100";
			}
			List partnerDetailList=this.getSession().createSQLQuery(query).list();
			partnerDetailsList dto = null;
			Iterator it = partnerDetailList.iterator();
			while (it.hasNext()){
				Object []row= (Object [])it.next();
				dto = new partnerDetailsList();
				dto.setPartnercode(row[0]);
				dto.setLastname(row[1]);
				dto.setBillingcountry(row[2]);
				dto.setBillingstate(row[3]);
				dto.setBillingcity(row[4]);
				dto.setPartnerType(row[5]);
				dto.setAliasName(row[6]);
				dto.setExtReference(row[7]);
				partnerDetailList1.add(dto);
			}
			return partnerDetailList1;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(corpid,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List getIkeaInfo(String sessionCorpID,String parentCode,String partnerCode){
		try {
			List partnerList = new ArrayList();
			Partner partnerPublic = null;
			return getHibernateTemplate().find("select concat(lastName,'#',billingEmail) from PartnerPublic   where partnerCode in ("
					+ parentCode + ") and partnerCode='" + partnerCode
					+ "'  and  status = 'Approved'");
			
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
		
	}
	
	public List findUserSignature(String fromEmail){
		try {
			return getHibernateTemplate().find(
					"select signature from User where email=?", fromEmail);
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			getSessionCorpID();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public String findRegNumber(String corpId,Long id){
		String regNum="";
		List reg = this.getSession().createSQLQuery("select registrationNumber from customerfile where id="+id+" ").list();
		if(reg!=null && !reg.isEmpty() && reg.get(0)!=null){
			regNum = reg.get(0).toString();
		}
		return regNum;
	}
	public Map<String, String> parentAccountCodeMap(String sessionCorpID) { 
		Map<String, String> childAgentCodeMap=new LinkedHashMap<String, String> ();
		String query = "select partnercode,group_concat(agentparent) from partnerpublic where corpID in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') and agentparent != '' and  agentparent is not null and partnercode !='' and partnercode is not null  and status ='Approved' and agentparent !=partnercode  group by partnercode order by partnercode";
		List childAgentCodeList =  this.getSession().createSQLQuery(query).list();
		Iterator its=childAgentCodeList.iterator();
		while (its.hasNext()) {
		List codeList =new ArrayList(); 
		Object[] row = (Object[]) its.next();
		if(row[1] !=null){ 
		childAgentCodeMap.put(row[0].toString().trim(), row[1].toString());
			}  
	     }
		return childAgentCodeMap;
	}
	public Map<String, List> childAccountCodeMap(String sessionCorpID) { 
		Map<String, List> childAgentCodeMap=new LinkedHashMap<String, List> ();
		String query = "select agentparent,group_concat(partnercode) from partnerpublic where corpID in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') and agentparent != '' and  agentparent is not null  and agentparent !=partnercode and status ='Approved' group by agentparent order by agentparent";
		List childAgentCodeList =  this.getSession().createSQLQuery(query).list();
		Iterator its=childAgentCodeList.iterator();
		while (its.hasNext()) {
		List codeList =new ArrayList(); 
		Object[] row = (Object[]) its.next();
		if(row[1] !=null){
		String partnercode = row[1].toString();
		String[] str1 = partnercode.split(","); 
		for (int i=0; i < str1.length; i++) {
			codeList.add(str1[i]);
		}
		}else{
		
		}
		childAgentCodeMap.put(row[0].toString().trim(), codeList);
			} 
		return childAgentCodeMap;
	}

	public List findAgentVendors( String parentCode){
	
		List <Object> partnerList = new ArrayList<Object>();
		try {
			String query="";
			Partner partner = null;
		
			query = "select distinct p.partnerCode,p.lastName,p.billingCountryCode from partnerpublic p , partnerprivate pp  where pp.partnerpublicid = p.id and pp.corpid in (select corpID  from company where allowAgentInvoiceUpload=true) AND  p.partnerCode in ("+ parentCode + ")  and  p.status = 'Approved'"
										;
		   List list = this.getSession().createSQLQuery(query).list();
			
			Iterator it = list.iterator();
			 
							while (it.hasNext()) {
								Object[] row = (Object[]) it.next();
								partner = new Partner();
								if(row[0]!=null){
								partner.setPartnerCode(row[0].toString());
								}else{
									partner.setPartnerCode("");	
								}
								if(row[1]!=null){
								partner.setLastName(row[1].toString());
								}else{
									partner.setLastName("");	
								}
								if(row[2]!=null){
								partner.setBillingCountryCode(row[2].toString());
								}else{
								partner.setBillingCountryCode("");	
								} 
								partnerList.add(partner);
							}
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return partnerList;
		
	}
	public List findAgentBillToCode(String parentCode, String bCode) {

		try {
			List partnerList = new ArrayList();
			Partner partnerPublic = null;
			return getHibernateTemplate().find(
					" select lastName from PartnerPublic   where partnerCode in ("
							+ parentCode + ") and partnerCode='" + bCode
							+ "'  and  status = 'Approved'");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	public List searchAgentVendors(String sessionCorpID, String parentCode, String partnerCode, String description, String billingCountryCode, String billingCountry, String extReference, String billingCity) {List <Object> partnerList = new ArrayList<Object>();
	try {
	    Partner partner = null;
		String query="";
		query = "select distinct p.partnerCode,p.lastName,p.billingCountryCode,p.billingCity from partnerpublic p, partnerprivate pp where pp.partnerpublicid = p.id  and pp.corpid in (select corpID  from company where allowAgentInvoiceUpload=true)  AND p.partnerCode in ("+ parentCode + ")"
						+ " and p.partnerCode like '%"+ partnerCode + "%' and p.lastName like '%"+description.replaceAll("'", "''")+ "%'"
								+ " and p.billingCountryCode like '%"+ billingCountryCode.replaceAll("'", "''") + "%' "
                                         + "and p.billingCountry like '%"+ billingCountry.replaceAll("'", "''") + "%'"
                                         + "and p.billingCity like '%"+ billingCity.replaceAll("'", "''") + "%'"
										 +" and p.status = 'Approved'    ";
		List list = this.getSession().createSQLQuery(query).list();
		
		Iterator it = list.iterator(); 
						while (it.hasNext()) {
							Object[] row = (Object[]) it.next();
							partner = new Partner();
							if(row[0]!=null){
							partner.setPartnerCode(row[0].toString());
							}else{
								partner.setPartnerCode("");	
							}
							if(row[1]!=null){
							partner.setLastName(row[1].toString());
							}else{
								partner.setLastName("");	
							}
							if(row[2]!=null){
							partner.setBillingCountryCode(row[2].toString());
							}else{
							partner.setBillingCountryCode("");	
							} 
							if(row[3]!=null){
								partner.setBillingCity(row[3].toString());
								}else{
									partner.setBillingCity("");	
								}
							partnerList.add(partner);
						}
		
	} catch (Exception e) {
		logger.error("Error executing query"+ e,e);
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
    	  saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
    	 e.printStackTrace();
	}
	
	return partnerList;
}
	
	}


