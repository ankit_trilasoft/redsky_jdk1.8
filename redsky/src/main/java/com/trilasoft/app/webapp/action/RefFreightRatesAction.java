package com.trilasoft.app.webapp.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import org.displaytag.properties.SortOrderEnum;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Port;
import com.trilasoft.app.model.RefFreightRates;
import com.trilasoft.app.model.Userlogfile;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.service.RefFreightRatesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;
import com.trilasoft.app.webapp.helper.SearchCriterion;

public class RefFreightRatesAction extends BaseAction implements Preparable{
	
	private String file;
	private String sessionCorpID;
	private RefFreightRatesManager refFreightRatesManager;
	private RefFreightRates refFreightRates;
	private List existingContractsList;
	private Long id;
	private RefMasterManager refMasterManager;
	private List mode;
	private Map<String,String> country;
	private Map<String,String> currency;
	private Map<String,String> equipmentTypeList;
	private List missingPortList;
	
	public void prepare() throws Exception {
		
	}
	public RefFreightRatesAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}
	
	public String getComboList(String corpID){ 
	 	country = refMasterManager.findByParameter(corpID, "COUNTRY");
	 	mode = refMasterManager.findByParameters(corpID, "MODE");
	 	currency=refMasterManager.findCodeOnleByParameter(corpID, "CURRENCY");
	 	equipmentTypeList = refMasterManager.findByParameter(corpID, "EQUIP");
	 	 return SUCCESS;
	}
	
	public String seaFreightUpdate(){
		//System.out.println("\n\n Inside solomonsPmtUpdate");
		return SUCCESS;
	}
	
	public String processSeaFreightUpdate(){
		try
		{   
			String userName = getRequest().getRemoteUser();
			String Key= refFreightRatesManager.saveData("TSFT",file,userName); 
			if(Key ==""){
				saveMessage("Invalid File"); 
			}
			else{saveMessage(getText(Key)); }
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
		return SUCCESS; 
	}
	
    public String getExistingContracts(){ 
		  existingContractsList=refFreightRatesManager.getExistingContracts(sessionCorpID); 
		  return SUCCESS;
	}
    public String getMissingPort(){ 
    	missingPortList=refFreightRatesManager.getMissingPort(sessionCorpID); 
		  return SUCCESS;
	}
    
    public String edit() {
		getComboList(sessionCorpID);
		if (id != null) {
			refFreightRates = refFreightRatesManager.get(id);
		} else {
			refFreightRates = new RefFreightRates(); 
			refFreightRates.setCreatedOn(new Date());
			refFreightRates.setUpdatedOn(new Date());
		} 
		return SUCCESS;
	}
    
	public String save() throws Exception {  
		getComboList(sessionCorpID);
		boolean isNew = (refFreightRates.getId() == null);  
				if (isNew) { 
					refFreightRates.setCreatedOn(new Date());
		        } 
				refFreightRates.setCorpID(sessionCorpID);
				refFreightRates.setUpdatedOn(new Date());
				refFreightRates.setUpdatedBy(getRequest().getRemoteUser());
				refFreightRatesManager.save(refFreightRates);  
			    String key = (isNew) ? "refFreightRates.added" : "refFreightRates.updated";
			    saveMessage(getText(key)); 
				return SUCCESS;
			
	}
	
	public String list()
	{  
		getComboList(sessionCorpID);
		userlogFileExt= paginateListFactory.getPaginatedListFromRequest(getRequest());
		String key = "Please enter your search criteria below";
		saveMessage(getText(key));
		return SUCCESS;	
	}
	private ExtendedPaginatedList userlogFileExt;
	private PaginateListFactory paginateListFactory;
	private PagingLookupManager pagingLookupManager;
	
	public String search()
	{ 
		getComboList(sessionCorpID);
		boolean serviceContractNo=(refFreightRates.getServiceContractNo() == null);
		boolean carrier = (refFreightRates.getCarrier() == null);
		boolean originCountry = (refFreightRates.getOriginCountry() == null);
		boolean destinationCountry = (refFreightRates.getDestinationCountry() == null);
		List<SearchCriterion> searchCriteria = new ArrayList(); 
		if(!serviceContractNo|| !carrier || !originCountry || !destinationCountry)
		{ 	
			    userlogFileExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
			    String sort = (String) getRequest().getParameter("sort");  
				searchCriteria.add(new SearchCriterion("serviceContractNo", refFreightRates.getServiceContractNo(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				searchCriteria.add(new SearchCriterion("carrier", refFreightRates.getCarrier(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				searchCriteria.add(new SearchCriterion("originCountry", refFreightRates.getOriginCountry(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				searchCriteria.add(new SearchCriterion("destinationCountry", refFreightRates.getDestinationCountry(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				
			}
			pagingLookupManager.getAllRecordsPage(RefFreightRates.class, userlogFileExt, searchCriteria); 
		
		return SUCCESS;	
	}
	public String delete() {
		refFreightRatesManager.remove(id);
		saveMessage(getText("Sea Freight Record  is deleted sucessfully"));
		
		return SUCCESS;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public void setRefFreightRatesManager(
			RefFreightRatesManager refFreightRatesManager) {
		this.refFreightRatesManager = refFreightRatesManager;
	}
	public RefFreightRates getRefFreightRates() {
		return refFreightRates;
	}
	public void setRefFreightRates(RefFreightRates refFreightRates) {
		this.refFreightRates = refFreightRates;
	}
	public List getExistingContractsList() {
		return existingContractsList;
	}
	public void setExistingContractsList(List existingContractsList) {
		this.existingContractsList = existingContractsList;
	}
	public Map<String, String> getCountry() {
		return country;
	}
	public void setCountry(Map<String, String> country) {
		this.country = country;
	}
	public Map<String, String> getCurrency() {
		return currency;
	}
	public void setCurrency(Map<String, String> currency) {
		this.currency = currency;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List getMode() {
		return mode;
	}
	public void setMode(List mode) {
		this.mode = mode;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public PaginateListFactory getPaginateListFactory() {
		return paginateListFactory;
	}
	public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
		this.paginateListFactory = paginateListFactory;
	}
	public PagingLookupManager getPagingLookupManager() {
		return pagingLookupManager;
	}
	public void setPagingLookupManager(PagingLookupManager pagingLookupManager) {
		this.pagingLookupManager = pagingLookupManager;
	}
	public ExtendedPaginatedList getUserlogFileExt() {
		return userlogFileExt;
	}
	public void setUserlogFileExt(ExtendedPaginatedList userlogFileExt) {
		this.userlogFileExt = userlogFileExt;
	}
	public Map<String, String> getEquipmentTypeList() {
		return equipmentTypeList;
	}
	public void setEquipmentTypeList(Map<String, String> equipmentTypeList) {
		this.equipmentTypeList = equipmentTypeList;
	}
	public List getMissingPortList() {
		return missingPortList;
	}
	public void setMissingPortList(List missingPortList) {
		this.missingPortList = missingPortList;
	}

}
