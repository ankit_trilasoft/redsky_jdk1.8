/**
 * This class represents the basic "WorkTicket" object in Redsky that allows for WorkTicket of Shipment.
 * @Class Name	WorkTicket
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */


package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="workticket")
@FilterDefs({
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")}),
@FilterDef(name = "serviceOrderCompanyDivisionFilter", parameters = { @ParamDef(name = "companydivision", type = "string")})
})
@Filters( {
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)"),
@Filter(name = "serviceOrderCompanyDivisionFilter", condition = " companyDivision in (:companydivision)  ")
} )
public class WorkTicket extends BaseObject implements ListLinkData, Comparable<WorkTicket> {
	private Long id;
	private Long ticket;
	private String sequenceNumber;
	private String shipNumber;
	private String registrationNumber;
	private String targetActual;
	private String corpID;
	private String originMobile;
	private String destinationMobile;
	private String storageMeasurement;
	private String jobMode;
	private String jobType;
	private String service;
	private String warehouse;
	private Double estimatedWeight;
	private String confirm;
	private Date confirmDate;
	private String firstName;
	private String lastName;
	private String phone;
	private String address1;
	private String address2;
	private String address3;
	private String address4;
	private String city;
	private String state;
	private String zip;
	private String instructions;
	private String instructionCode;
	private String account;
	private String accountName;
	private int crews=0;
	private Double estimatedHours;
	private Date openDate;
	private String whoCancelled;
	private int estimatedCartoons=0;
	private Double estimatedCubicFeet;
	private String scheduledArrive;
	private String leaveWareHouse;
	private String destinationAddress1;
	private String destinationAddress2;
	private String destinationAddress3;
	private String destinationAddress4;
	private String destinationCity;
	private String destinationState;
	private String destinationPhone;
	private String destinationZip;
	private String mi;
	private String destinationCountryCode;
	private String destinationCountry;
	private String prefix;
	private String suffix;
	private String homePhone;
	private String destinationHomePhone;
	private Double actualWeight;
	private Double crewRevenue;
	private String beginningTime;
	private String contactFax;
	private String contactName;
	private String contactPhone;
	private Boolean destinationDock;
	private String destinationElevator;
	private String endTime;
	private String originContactName;
	private String originContactPhone;
	private String originContactWeekend;
	private Boolean originDock;
	private String originElevator;
	private Boolean weekendFlag;	
	private String orderBy;
	private Double stgAmount;
	private String stgHow;
	private int trucks=0;
	private String revenueCalculation;
	private Double revenue;
	private String timeFrom;
	private String timeTo;
	private int endMiles=0;
	private int beginningMiles=0;
	private String contract;
	private Boolean qc;
	private Boolean noCharge;
	private Date approvedDate;
	private String approvedBy;
	private String qcId;
	private Date date1;
	private Date date2;
	private Date scheduled1;
	private Date scheduled2;
	private Date scanned;
	private String originCompany;
	private String destinationCompany;
	private String originFax;
	private String destinationFax;
	private String salesMan;
	private String estimator;
	private String coordinator;
	private String originCountryCode;
	private String originCountry;
	private String carrier;

	private String destMeters;
	private String origMeters;
	private String siteS;
	private String siteD;
	private String parkingS;	
	private String parkingD;
	
	private String phoneExt;
	private String destinationPhoneExt;
	private String containerNumber;
	private String unit1;
	private String unit2;
	private String reviewStatus;
	private Date reviewStatusDate;
	private Date statusDate;
	private Double extraPayroll;
	private String payMethod;
	private Long serviceOrderId;
	private String companyDivision;
	private Boolean collectCOD;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String vendorCode;
	private String vendorName;
	private String storageOut;
	//Distance Calculation...
	private BigDecimal distance;
	private String distanceInKmMiles;
	private Boolean originShuttle;
	private Boolean destinationShuttle;
	private Boolean originParkingPermit;
	private Boolean destinationParkingPermit;
	private String bondedGoods;
	private String whichFloor;
	private String farInMeters;
	private String parking;
	private String dest_park;
	private String dest_farInMeters;
	private String dest_whichFloor;
	private Integer estimatedPieces=0;
	private Double actualVolume;
//	 Mapping Variables	
	//private Set<Storage> storages; //One To Many RelationShip With Storage
	//private Set<BookStorage> bookStorages; //One To Many RelationShip With BookStorage
	private Set<Location> locations; //One To Many RelationShip With Location
	private ServiceOrder serviceOrder; //One To Many RelationShip With ServiceOrder
	//private Set<ItemsJbkEquip> itemsJbkEquips; //One To Many RelationShip With ItemsJbkEquip
	private Integer crates=0;
	private String changes;
	private Boolean resourceReqdAm;
	private Boolean resourceReqdPm;
	private String originPreferredContactTime;
	private String destPreferredContactTime;
	private String description;
	private String orderId;
	private String invoiceNumber;
	private String originLongCarry;
	private String destinationLongCarry;
	private String attachedFile;
	private Boolean hse;
	private BigDecimal requiredCrew;
	private String ticketAssignedStatus;
	private String originAddressBook;
	private String destinationAddressBook;
	private Boolean thirdPartyRequired;
	private Boolean acceptByDriver;
	private String crewName;
	private String projectLeadName;
	private String projectLeadNumber;
	private Boolean outOfScope;
	private String crewVehicleSummary;
	private Integer fvpValue;
	private Integer acvValue;
	private Double hours;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("ticket", ticket)
				.append("description",description)
				.append("sequenceNumber", sequenceNumber)
				.append("shipNumber", shipNumber)
				.append("registrationNumber", registrationNumber)
				.append("targetActual", targetActual).append("corpID", corpID)
				.append("originMobile", originMobile)
				.append("destinationMobile", destinationMobile)
				.append("storageMeasurement", storageMeasurement)
				.append("jobMode", jobMode).append("jobType", jobType)
				.append("service", service).append("warehouse", warehouse)
				.append("estimatedWeight", estimatedWeight)
				.append("confirm", confirm).append("confirmDate", confirmDate)
				.append("firstName", firstName).append("lastName", lastName)
				.append("phone", phone).append("address1", address1)
				.append("address2", address2).append("address3", address3)
				.append("address4", address4).append("city", city)
				.append("state", state).append("zip", zip)
				.append("instructions", instructions)
				.append("instructionCode", instructionCode)
				.append("account", account).append("accountName", accountName)
				.append("crews", crews)
				.append("estimatedHours", estimatedHours)
				.append("openDate", openDate)
				.append("whoCancelled", whoCancelled)
				.append("estimatedCartoons", estimatedCartoons)
				.append("estimatedCubicFeet", estimatedCubicFeet)
				.append("scheduledArrive", scheduledArrive)
				.append("leaveWareHouse", leaveWareHouse)
				.append("destinationAddress1", destinationAddress1)
				.append("destinationAddress2", destinationAddress2)
				.append("destinationAddress3", destinationAddress3)
				.append("destinationAddress4", destinationAddress4)
				.append("destinationCity", destinationCity)
				.append("destinationState", destinationState)
				.append("destinationPhone", destinationPhone)
				.append("destinationZip", destinationZip).append("mi", mi)
				.append("destinationCountryCode", destinationCountryCode)
				.append("destinationCountry", destinationCountry)
				.append("prefix", prefix).append("suffix", suffix)
				.append("homePhone", homePhone)
				.append("destinationHomePhone", destinationHomePhone)
				.append("actualWeight", actualWeight)
				.append("crewRevenue", crewRevenue)
				.append("beginningTime", beginningTime)
				.append("contactFax", contactFax)
				.append("contactName", contactName)
				.append("contactPhone", contactPhone)
				.append("destinationDock", destinationDock)
				.append("destinationElevator", destinationElevator)
				.append("endTime", endTime)
				.append("originContactName", originContactName)
				.append("originContactPhone", originContactPhone)
				.append("originContactWeekend", originContactWeekend)
				.append("originDock", originDock)
				.append("originElevator", originElevator)
				.append("weekendFlag", weekendFlag).append("orderBy", orderBy)
				.append("stgAmount", stgAmount).append("stgHow", stgHow)
				.append("trucks", trucks)
				.append("revenueCalculation", revenueCalculation)
				.append("revenue", revenue).append("timeFrom", timeFrom)
				.append("timeTo", timeTo).append("endMiles", endMiles)
				.append("beginningMiles", beginningMiles)
				.append("contract", contract).append("qc", qc)
				.append("noCharge", noCharge)
				.append("approvedDate", approvedDate)
				.append("approvedBy", approvedBy).append("qcId", qcId)
				.append("date1", date1).append("date2", date2)
				.append("scheduled1", scheduled1)
				.append("scheduled2", scheduled2).append("scanned", scanned)
				.append("originCompany", originCompany)
				.append("destinationCompany", destinationCompany)
				.append("originFax", originFax)
				.append("destinationFax", destinationFax)
				.append("salesMan", salesMan).append("estimator", estimator)
				.append("coordinator", coordinator)
				.append("originCountryCode", originCountryCode)
				.append("originCountry", originCountry)
				.append("carrier", carrier).append("destMeters", destMeters)
				.append("origMeters", origMeters).append("siteS", siteS)
				.append("siteD", siteD).append("parkingS", parkingS)
				.append("parkingD", parkingD).append("phoneExt", phoneExt)
				.append("destinationPhoneExt", destinationPhoneExt)
				.append("containerNumber", containerNumber)
				.append("unit1", unit1).append("unit2", unit2)
				.append("reviewStatus", reviewStatus)
				.append("reviewStatusDate", reviewStatusDate)
				.append("statusDate", statusDate)
				.append("extraPayroll", extraPayroll)
				.append("payMethod", payMethod)
				.append("serviceOrderId", serviceOrderId)
				.append("companyDivision", companyDivision)
				.append("collectCOD", collectCOD)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("vendorCode", vendorCode)
				.append("vendorName", vendorName)
				.append("storageOut", storageOut).append("distance", distance)
				.append("distanceInKmMiles", distanceInKmMiles)
				.append("originShuttle", originShuttle)
				.append("destinationShuttle", destinationShuttle)
				.append("originParkingPermit", originParkingPermit)
				.append("destinationParkingPermit", destinationParkingPermit)
				.append("bondedGoods", bondedGoods)
				.append("whichFloor", whichFloor)
				.append("farInMeters", farInMeters).append("parking", parking)
				.append("dest_park", dest_park)
				.append("dest_farInMeters", dest_farInMeters)
				.append("dest_whichFloor", dest_whichFloor)
				.append("estimatedPieces", estimatedPieces)
				.append("actualVolume", actualVolume).append("crates", crates)
				.append("changes", changes)
				.append("resourceReqdAm", resourceReqdAm)
				.append("resourceReqdPm", resourceReqdPm)
				.append("originPreferredContactTime",originPreferredContactTime)
				.append("destPreferredContactTime",destPreferredContactTime)
				.append("orderId",orderId)
				.append("invoiceNumber",invoiceNumber)
				.append("originLongCarry",originLongCarry)
				.append("destinationLongCarry",destinationLongCarry)
				.append("attachedFile",attachedFile)
				.append("hse",hse).append("requiredCrew",requiredCrew).append("ticketAssignedStatus",ticketAssignedStatus)
				.append("originAddressBook",originAddressBook).append("destinationAddressBook",destinationAddressBook)
				.append("thirdPartyRequired",thirdPartyRequired)
				.append("acceptByDriver",acceptByDriver)
				.append("crewName",crewName)
				.append("projectLeadName",projectLeadName)
				.append("projectLeadNumber",projectLeadNumber)
				.append("outOfScope",outOfScope)
				.append("crewVehicleSummary",crewVehicleSummary)
				.append("fvpValue",fvpValue)
				.append("acvValue",acvValue)
				.append("hours",hours) 
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof WorkTicket))
			return false;
		WorkTicket castOther = (WorkTicket) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(ticket, castOther.ticket)
				.append(description,castOther.description)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(shipNumber, castOther.shipNumber)
				.append(registrationNumber, castOther.registrationNumber)
				.append(targetActual, castOther.targetActual)
				.append(corpID, castOther.corpID)
				.append(originMobile, castOther.originMobile)
				.append(destinationMobile, castOther.destinationMobile)
				.append(storageMeasurement, castOther.storageMeasurement)
				.append(jobMode, castOther.jobMode)
				.append(jobType, castOther.jobType)
				.append(service, castOther.service)
				.append(warehouse, castOther.warehouse)
				.append(estimatedWeight, castOther.estimatedWeight)
				.append(confirm, castOther.confirm)
				.append(confirmDate, castOther.confirmDate)
				.append(firstName, castOther.firstName)
				.append(lastName, castOther.lastName)
				.append(phone, castOther.phone)
				.append(address1, castOther.address1)
				.append(address2, castOther.address2)
				.append(address3, castOther.address3)
				.append(address4, castOther.address4)
				.append(city, castOther.city)
				.append(state, castOther.state)
				.append(zip, castOther.zip)
				.append(instructions, castOther.instructions)
				.append(instructionCode, castOther.instructionCode)
				.append(account, castOther.account)
				.append(accountName, castOther.accountName)
				.append(crews, castOther.crews)
				.append(estimatedHours, castOther.estimatedHours)
				.append(openDate, castOther.openDate)
				.append(whoCancelled, castOther.whoCancelled)
				.append(estimatedCartoons, castOther.estimatedCartoons)
				.append(estimatedCubicFeet, castOther.estimatedCubicFeet)
				.append(scheduledArrive, castOther.scheduledArrive)
				.append(leaveWareHouse, castOther.leaveWareHouse)
				.append(destinationAddress1, castOther.destinationAddress1)
				.append(destinationAddress2, castOther.destinationAddress2)
				.append(destinationAddress3, castOther.destinationAddress3)
				.append(destinationAddress4, castOther.destinationAddress4)
				.append(destinationCity, castOther.destinationCity)
				.append(destinationState, castOther.destinationState)
				.append(destinationPhone, castOther.destinationPhone)
				.append(destinationZip, castOther.destinationZip)
				.append(mi, castOther.mi)
				.append(destinationCountryCode,
						castOther.destinationCountryCode)
				.append(destinationCountry, castOther.destinationCountry)
				.append(prefix, castOther.prefix)
				.append(suffix, castOther.suffix)
				.append(homePhone, castOther.homePhone)
				.append(destinationHomePhone, castOther.destinationHomePhone)
				.append(actualWeight, castOther.actualWeight)
				.append(crewRevenue, castOther.crewRevenue)
				.append(beginningTime, castOther.beginningTime)
				.append(contactFax, castOther.contactFax)
				.append(contactName, castOther.contactName)
				.append(contactPhone, castOther.contactPhone)
				.append(destinationDock, castOther.destinationDock)
				.append(destinationElevator, castOther.destinationElevator)
				.append(endTime, castOther.endTime)
				.append(originContactName, castOther.originContactName)
				.append(originContactPhone, castOther.originContactPhone)
				.append(originContactWeekend, castOther.originContactWeekend)
				.append(originDock, castOther.originDock)
				.append(originElevator, castOther.originElevator)
				.append(weekendFlag, castOther.weekendFlag)
				.append(orderBy, castOther.orderBy)
				.append(stgAmount, castOther.stgAmount)
				.append(stgHow, castOther.stgHow)
				.append(trucks, castOther.trucks)
				.append(revenueCalculation, castOther.revenueCalculation)
				.append(revenue, castOther.revenue)
				.append(timeFrom, castOther.timeFrom)
				.append(timeTo, castOther.timeTo)
				.append(endMiles, castOther.endMiles)
				.append(beginningMiles, castOther.beginningMiles)
				.append(contract, castOther.contract)
				.append(qc, castOther.qc)
				.append(noCharge, castOther.noCharge)
				.append(approvedDate, castOther.approvedDate)
				.append(approvedBy, castOther.approvedBy)
				.append(qcId, castOther.qcId)
				.append(date1, castOther.date1)
				.append(date2, castOther.date2)
				.append(scheduled1, castOther.scheduled1)
				.append(scheduled2, castOther.scheduled2)
				.append(scanned, castOther.scanned)
				.append(originCompany, castOther.originCompany)
				.append(destinationCompany, castOther.destinationCompany)
				.append(originFax, castOther.originFax)
				.append(destinationFax, castOther.destinationFax)
				.append(salesMan, castOther.salesMan)
				.append(estimator, castOther.estimator)
				.append(coordinator, castOther.coordinator)
				.append(originCountryCode, castOther.originCountryCode)
				.append(originCountry, castOther.originCountry)
				.append(carrier, castOther.carrier)
				.append(destMeters, castOther.destMeters)
				.append(origMeters, castOther.origMeters)
				.append(siteS, castOther.siteS)
				.append(siteD, castOther.siteD)
				.append(parkingS, castOther.parkingS)
				.append(parkingD, castOther.parkingD)
				.append(phoneExt, castOther.phoneExt)
				.append(destinationPhoneExt, castOther.destinationPhoneExt)
				.append(containerNumber, castOther.containerNumber)
				.append(unit1, castOther.unit1)
				.append(unit2, castOther.unit2)
				.append(reviewStatus, castOther.reviewStatus)
				.append(reviewStatusDate, castOther.reviewStatusDate)
				.append(statusDate, castOther.statusDate)
				.append(extraPayroll, castOther.extraPayroll)
				.append(payMethod, castOther.payMethod)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(companyDivision, castOther.companyDivision)
				.append(collectCOD, castOther.collectCOD)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(vendorCode, castOther.vendorCode)
				.append(vendorName, castOther.vendorName)
				.append(storageOut, castOther.storageOut)
				.append(distance, castOther.distance)
				.append(distanceInKmMiles, castOther.distanceInKmMiles)
				.append(originShuttle, castOther.originShuttle)
				.append(destinationShuttle, castOther.destinationShuttle)
				.append(originParkingPermit, castOther.originParkingPermit)
				.append(destinationParkingPermit,
						castOther.destinationParkingPermit)
				.append(bondedGoods, castOther.bondedGoods)
				.append(whichFloor, castOther.whichFloor)
				.append(farInMeters, castOther.farInMeters)
				.append(parking, castOther.parking)
				.append(dest_park, castOther.dest_park)
				.append(dest_farInMeters, castOther.dest_farInMeters)
				.append(dest_whichFloor, castOther.dest_whichFloor)
				.append(estimatedPieces, castOther.estimatedPieces)
				.append(actualVolume, castOther.actualVolume)
				.append(crates, castOther.crates)
				.append(changes, castOther.changes)
				.append(resourceReqdAm, castOther.resourceReqdAm)
				.append(resourceReqdPm, castOther.resourceReqdPm)
				.append(originPreferredContactTime,castOther.originPreferredContactTime)
				.append(destPreferredContactTime,castOther.destPreferredContactTime)
				.append(orderId,castOther.orderId)
				.append(invoiceNumber,castOther.invoiceNumber)
				.append(originLongCarry,castOther.originLongCarry)
				.append(destinationLongCarry,castOther.destinationLongCarry)
				.append(attachedFile,castOther.attachedFile)
				.append(hse, castOther.hse).append(requiredCrew, castOther.requiredCrew).append(ticketAssignedStatus, castOther.ticketAssignedStatus)
				.append(originAddressBook,castOther.originAddressBook).append(destinationAddressBook,castOther.destinationAddressBook)
				.append(thirdPartyRequired,castOther.thirdPartyRequired)
				.append("acceptByDriver",castOther.acceptByDriver)
				.append("Crew_name",castOther.crewName)
				.append("projectLeadName",castOther.projectLeadName)
				.append("projectLeadNumber",castOther.projectLeadNumber)
				.append("outOfScope",castOther.outOfScope)
				.append("crewVehicleSummary",castOther.crewVehicleSummary)
				.append("fvpValue",castOther.fvpValue)
				.append("acvValue",castOther.acvValue)
				.append("hours",castOther.hours)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(ticket).append(description)
				.append(sequenceNumber).append(shipNumber)
				.append(registrationNumber).append(targetActual).append(corpID)
				.append(originMobile).append(destinationMobile)
				.append(storageMeasurement).append(jobMode).append(jobType)
				.append(service).append(warehouse).append(estimatedWeight)
				.append(confirm).append(confirmDate).append(firstName)
				.append(lastName).append(phone).append(address1)
				.append(address2).append(address3).append(address4)
				.append(city).append(state).append(zip).append(instructions)
				.append(instructionCode).append(account).append(accountName)
				.append(crews).append(estimatedHours).append(openDate)
				.append(whoCancelled).append(estimatedCartoons)
				.append(estimatedCubicFeet).append(scheduledArrive)
				.append(leaveWareHouse).append(destinationAddress1)
				.append(destinationAddress2).append(destinationAddress3)
				.append(destinationAddress4).append(destinationCity)
				.append(destinationState).append(destinationPhone)
				.append(destinationZip).append(mi)
				.append(destinationCountryCode).append(destinationCountry)
				.append(prefix).append(suffix).append(homePhone)
				.append(destinationHomePhone).append(actualWeight)
				.append(crewRevenue).append(beginningTime).append(contactFax)
				.append(contactName).append(contactPhone)
				.append(destinationDock).append(destinationElevator)
				.append(endTime).append(originContactName)
				.append(originContactPhone).append(originContactWeekend)
				.append(originDock).append(originElevator).append(weekendFlag)
				.append(orderBy).append(stgAmount).append(stgHow)
				.append(trucks).append(revenueCalculation).append(revenue)
				.append(timeFrom).append(timeTo).append(endMiles)
				.append(beginningMiles).append(contract).append(qc)
				.append(noCharge).append(approvedDate).append(approvedBy)
				.append(qcId).append(date1).append(date2).append(scheduled1)
				.append(scheduled2).append(scanned).append(originCompany)
				.append(destinationCompany).append(originFax)
				.append(destinationFax).append(salesMan).append(estimator)
				.append(coordinator).append(originCountryCode)
				.append(originCountry).append(carrier).append(destMeters)
				.append(origMeters).append(siteS).append(siteD)
				.append(parkingS).append(parkingD).append(phoneExt)
				.append(destinationPhoneExt).append(containerNumber)
				.append(unit1).append(unit2).append(reviewStatus)
				.append(reviewStatusDate).append(statusDate)
				.append(extraPayroll).append(payMethod).append(serviceOrderId)
				.append(companyDivision).append(collectCOD).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(vendorCode).append(vendorName).append(storageOut)
				.append(distance).append(distanceInKmMiles)
				.append(originShuttle).append(destinationShuttle)
				.append(originParkingPermit).append(destinationParkingPermit)
				.append(bondedGoods).append(whichFloor).append(farInMeters)
				.append(parking).append(dest_park).append(dest_farInMeters)
				.append(dest_whichFloor).append(estimatedPieces)
				.append(actualVolume).append(crates).append(changes)
				.append(resourceReqdAm).append(resourceReqdPm)
				.append(originPreferredContactTime)
				.append(destPreferredContactTime)
				.append(orderId)
				.append(invoiceNumber)
				.append(originLongCarry)
				.append(destinationLongCarry)
				.append(attachedFile)
				.append(hse).append(requiredCrew).append(ticketAssignedStatus)
				.append(originAddressBook).append(destinationAddressBook)
				.append(thirdPartyRequired)
				.append(acceptByDriver)
				.append(crewName)
				.append(projectLeadName)
				.append(projectLeadNumber)
				.append(outOfScope)
				.append(crewVehicleSummary)
				.append(fvpValue)
				.append(acvValue)
				.append(hours)
				.toHashCode();
	}
	public int compareTo(final WorkTicket other) {
		return new CompareToBuilder().append(id, other.id).append(ticket,
				other.ticket).append(sequenceNumber, other.sequenceNumber).append(description, other.description)
				.append(shipNumber, other.shipNumber).append(
						registrationNumber, other.registrationNumber).append(
						targetActual, other.targetActual).append(corpID,
						other.corpID).append(originMobile, other.originMobile)
				.append(destinationMobile, other.destinationMobile).append(
						storageMeasurement, other.storageMeasurement).append(
						jobMode, other.jobMode).append(jobType, other.jobType)
				.append(service, other.service).append(warehouse,
						other.warehouse).append(estimatedWeight,
						other.estimatedWeight).append(confirm, other.confirm)
				.append(confirmDate, other.confirmDate).append(firstName,
						other.firstName).append(lastName, other.lastName)
				.append(phone, other.phone).append(address1, other.address1)
				.append(address2, other.address2).append(address3,
						other.address3).append(address4, other.address4)
				.append(city, other.city).append(state, other.state).append(
						zip, other.zip)
				.append(instructions, other.instructions).append(
						instructionCode, other.instructionCode).append(account,
						other.account).append(accountName, other.accountName)
				.append(crews, other.crews).append(estimatedHours,
						other.estimatedHours).append(openDate, other.openDate)
				.append(whoCancelled, other.whoCancelled).append(
						estimatedCartoons, other.estimatedCartoons).append(
						estimatedCubicFeet, other.estimatedCubicFeet).append(
						scheduledArrive, other.scheduledArrive).append(
						leaveWareHouse, other.leaveWareHouse).append(
						destinationAddress1, other.destinationAddress1).append(
						destinationAddress2, other.destinationAddress2).append(
						destinationAddress3, other.destinationAddress3).append(
						destinationAddress4, other.destinationAddress4).append(
						destinationCity, other.destinationCity).append(
						destinationState, other.destinationState).append(
						destinationPhone, other.destinationPhone).append(
						destinationZip, other.destinationZip).append(mi,
						other.mi).append(destinationCountryCode,
						other.destinationCountryCode).append(
						destinationCountry, other.destinationCountry).append(
						prefix, other.prefix).append(suffix, other.suffix)
				.append(homePhone, other.homePhone).append(
						destinationHomePhone, other.destinationHomePhone)
				.append(actualWeight, other.actualWeight).append(crewRevenue,
						other.crewRevenue).append(beginningTime,
						other.beginningTime).append(contactFax,
						other.contactFax)
				.append(contactName, other.contactName).append(contactPhone,
						other.contactPhone).append(destinationDock,
						other.destinationDock).append(destinationElevator,
						other.destinationElevator).append(endTime,
						other.endTime).append(originContactName,
						other.originContactName).append(originContactPhone,
						other.originContactPhone).append(originContactWeekend,
						other.originContactWeekend).append(originDock,
						other.originDock).append(originElevator,
						other.originElevator).append(orderBy, other.orderBy)
				.append(stgAmount, other.stgAmount)
				.append(stgHow, other.stgHow).append(trucks, other.trucks)
				.append(revenueCalculation, other.revenueCalculation).append(
						revenue, other.revenue)
				.append(timeFrom, other.timeFrom).append(timeTo, other.timeTo)
				.append(endMiles, other.endMiles).append(beginningMiles,
						other.beginningMiles).append(contract, other.contract)
				.append(qc, other.qc).append(noCharge, other.noCharge).append(
						approvedDate, other.approvedDate).append(approvedBy,
						other.approvedBy).append(qcId, other.qcId).append(
						date1, other.date1).append(date2, other.date2).append(
						scheduled1, other.scheduled1).append(scheduled2,
						other.scheduled2).append(scanned, other.scanned)
				.append(originCompany, other.originCompany).append(
						destinationCompany, other.destinationCompany).append(
						originFax, other.originFax).append(destinationFax,
						other.destinationFax).append(salesMan, other.salesMan)
				.append(estimator, other.estimator).append(coordinator,
						other.coordinator).append(originCountryCode,
						other.originCountryCode).append(originCountry,
						other.originCountry).append(carrier, other.carrier)
				.append(phoneExt, other.phoneExt).append(destinationPhoneExt,
						other.destinationPhoneExt).append(containerNumber,
						other.containerNumber).append(unit1, other.unit1)
				.append(unit2, other.unit2).append(reviewStatus,
						other.reviewStatus).append(reviewStatusDate,
						other.reviewStatusDate).append(statusDate,
						other.statusDate).append(extraPayroll,
						other.extraPayroll).append(payMethod, other.payMethod)
				.append(serviceOrderId, other.serviceOrderId).append(
						companyDivision, other.companyDivision).append(
						collectCOD, other.collectCOD).append(createdBy,
						other.createdBy).append(updatedBy, other.updatedBy)
				.append(createdOn, other.createdOn).append(updatedOn,
						other.updatedOn).append(vendorCode, other.vendorCode)
				.append(vendorName, other.vendorName).append(storageOut,
						other.storageOut)
						.append(orderId, other.orderId)
						.append(invoiceNumber,other.invoiceNumber)
						.append(originLongCarry,other.originLongCarry)
						.append(destinationLongCarry,other.destinationLongCarry).append(ticketAssignedStatus,other.ticketAssignedStatus)
						.append(originAddressBook, other.originAddressBook).append(destinationAddressBook, other.destinationAddressBook)
						.append(thirdPartyRequired, other.thirdPartyRequired)
                        .append("acceptByDriver",other.acceptByDriver)
				        .append("crewName",other.crewName)
				        .append("projectLeadName",other.projectLeadName)
				        .append("projectLeadNumber",other.projectLeadNumber)
				        .append("outOfScope",other.outOfScope)
				        .append("crewVehicleSummary",other.crewVehicleSummary)
				        .append("fvpValue",other.fvpValue)
				        .append("acvValue",other.acvValue)
						.toComparison();
	}
	/*
	@OneToMany(mappedBy="workTicket", cascade = CascadeType.ALL, fetch = FetchType.LAZY )

	public Set<Storage> getStorages() {
	return storages;
	}
	public void setStorages(Set<Storage> storages){
	this.storages = storages;
	}

	@OneToMany(mappedBy="workTicket", cascade = CascadeType.ALL, fetch = FetchType.LAZY)

	public Set<BookStorage> getBookStorages() {
	return bookStorages;
	}
	public void setBookStorages(Set<BookStorage> bookStorages){
	this.bookStorages = bookStorages;
	}
	*/
	@ManyToOne
	@JoinColumn(name="serviceOrderId", nullable=false, updatable=false,
	referencedColumnName="id")
	
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	/*@OneToMany(mappedBy="workTicket", cascade = CascadeType.ALL, fetch = FetchType.LAZY)

	public Set<ItemsJbkEquip> getItemsJbkEquips() {
		return itemsJbkEquips;
	}
	public void setItemsJbkEquips(Set<ItemsJbkEquip> itemsJbkEquips){
		this.itemsJbkEquips = itemsJbkEquips;
	}*/
	@Column(length=8)
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	@Column
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
	@Column
	public Double getActualWeight() {
		return actualWeight;
	}
	public void setActualWeight(Double actualWeight) {
		this.actualWeight = actualWeight;
	}
	@Column(length=50)
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	@Column(length=100)
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	@Column(length=60)
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	@Column(length=50)
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	@Column(length=50)
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	@Column(length=30)
	public String getAddress4() {
		return address4;
	}
	public void setAddress4(String address4) {
		this.address4 = address4;
	}
		
	@Column(nullable=false)
	public int getBeginningMiles() {
		return beginningMiles;
	}
	public void setBeginningMiles(int beginningMiles) {
		this.beginningMiles = beginningMiles;
	}
	@Column(length=6)
	public String getBeginningTime() {
		return beginningTime;
	}
	public void setBeginningTime(String beginningTime) {
		this.beginningTime = beginningTime;
	}
		
	@Column(length=40)
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Column(length=20)
	public String getConfirm() {
		return confirm;
	}
	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}
	@Column
	public Date getConfirmDate() {
		return confirmDate;
	}
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}
	@Column(length=20)
	public String getContactFax() {
		return contactFax;
	}
	public void setContactFax(String contactFax) {
		this.contactFax = contactFax;
	}
	@Column(length=35)
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	@Column(length=20)
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	@Column
	public Double getCrewRevenue() {
		return crewRevenue;
	}
	public void setCrewRevenue(Double crewRevenue) {
		this.crewRevenue = crewRevenue;
	}
	@Column(nullable=false)
	public int getCrews() {
		return crews;
	}
	public void setCrews(int crews) {
		this.crews = crews;
	}
	
	@Column
	public Date getDate1() {
		return date1;
	}
	public void setDate1(Date date1) {
		this.date1 = date1;
	}
	@Column
	public Date getDate2() {
		return date2;
	}
	public void setDate2(Date date2) {
		this.date2 = date2;
	}
	
	@Column(length=50)
	public String getDestinationAddress1() {
		return destinationAddress1;
	}
	public void setDestinationAddress1(String destinationAddress1) {
		this.destinationAddress1 = destinationAddress1;
	}
	@Column(length=50) 
	public String getDestinationAddress2() {
		return destinationAddress2;
	}
	public void setDestinationAddress2(String destinationAddress2) {
		this.destinationAddress2 = destinationAddress2;
	}
	@Column(length=50)
	public String getDestinationAddress3() {
		return destinationAddress3;
	}
	public void setDestinationAddress3(String destinationAddress3) {
		this.destinationAddress3 = destinationAddress3;
	}
	@Column(length=30)
	public String getDestinationAddress4() {
		return destinationAddress4;
	}
	public void setDestinationAddress4(String destinationAddress4) {
		this.destinationAddress4 = destinationAddress4;
	}
	@Column(length=40)
	public String getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	@Column(length=280)
	public String getDestinationCompany() {
		return destinationCompany;
	}
	public void setDestinationCompany(String destinationCompany) {
		this.destinationCompany = destinationCompany;
	}
	@Column(length=25)
	public String getDestinationCountry() {
		return destinationCountry;
	}
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	@Column(length=3)
	public String getDestinationCountryCode() {
		return destinationCountryCode;
	}
	public void setDestinationCountryCode(String destinationCountryCode) {
		this.destinationCountryCode = destinationCountryCode;
	}
	@Column(length=25)
	public String getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	@Column(length=3)
	public String getOriginCountryCode() {
		return originCountryCode;
	}
	public void setOriginCountryCode(String originCountryCode) {
		this.originCountryCode = originCountryCode;
	}
	@Column
	public String getDestinationElevator() {
		return destinationElevator;
	}
	public void setDestinationElevator(String destinationElevator) {
		this.destinationElevator = destinationElevator;
	}
	@Column
	public Boolean getDestinationDock() {
		return destinationDock;
	}
	public void setDestinationDock(Boolean destinationDock) {
		this.destinationDock = destinationDock;
	}
	@Column(length=20)
	public String getDestinationFax() {
		return destinationFax;
	}
	public void setDestinationFax(String destinationFax) {
		this.destinationFax = destinationFax;
	}
	@Column(length=20)
	public String getDestinationHomePhone() {
		return destinationHomePhone;
	}
	public void setDestinationHomePhone(String destinationHomePhone) {
		this.destinationHomePhone = destinationHomePhone;
	}
	@Column(length=20)
	public String getDestinationPhone() {
		return destinationPhone;
	}
	public void setDestinationPhone(String destinationPhone) {
		this.destinationPhone = destinationPhone;
	}
	@Column(length=2)
	public String getDestinationState() {
		return destinationState;
	}
	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}
	@Column(length=10)
	public String getDestinationZip() {
		return destinationZip;
	}
	public void setDestinationZip(String destinationZip) {
		this.destinationZip = destinationZip;
	}
	
	@Column(nullable=false)
	public int getEndMiles() {
		return endMiles;
	}
	public void setEndMiles(int endMiles) {
		this.endMiles = endMiles;
	}
	@Column(length=6)
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	@Column(nullable=false)
	public int getEstimatedCartoons() {
		return estimatedCartoons;
	}
	public void setEstimatedCartoons(int estimatedCartoons) {
		this.estimatedCartoons = estimatedCartoons;
	}
	@Column
	public Double getEstimatedCubicFeet() {
		return estimatedCubicFeet;
	}
	public void setEstimatedCubicFeet(Double estimatedCubicFeet) {
		this.estimatedCubicFeet = estimatedCubicFeet;
	}
	
	@Column	
		public Double getEstimatedHours() {
		return estimatedHours;
	}
	public void setEstimatedHours(Double estimatedHours) {
		this.estimatedHours = estimatedHours;
	}
	
	@Column
	public Double getEstimatedWeight() {
		return estimatedWeight;
	}
	public void setEstimatedWeight(Double estimatedWeight) {
		this.estimatedWeight = estimatedWeight;
	}
	
	
	@Column(length=80)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(length=20)
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=8)
	public String getInstructionCode() {
		return instructionCode;
	}
	public void setInstructionCode(String instructionCode) {
		this.instructionCode = instructionCode;
	}
	@Column
	public String getInstructions() {
		return instructions;
	}
	@Column
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	@Column(length=20)  
	public String getJobMode() {
		return jobMode;
	}
	public void setJobMode(String jobMode) {
		this.jobMode = jobMode;
	}
	@Column(length=3)  
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	@Column(length=80)
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Column(length=6)
	public String getLeaveWareHouse() {
		return leaveWareHouse;
	}
	public void setLeaveWareHouse(String leaveWareHouse) {
		this.leaveWareHouse = leaveWareHouse;
	}
	@Column(length=1)
	public String getMi() {
		return mi;
	}
	public void setMi(String mi) {
		this.mi = mi;
	}
			
	@Column
	public Date getOpenDate() {
		return openDate;
	}
	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}
	@Column(length=30)
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	@Column(length=280)
	public String getOriginCompany() {
		return originCompany;
	}
	public void setOriginCompany(String originCompany) {
		this.originCompany = originCompany;
	}
	@Column(length=35)
	public String getOriginContactName() {
		return originContactName;
	}
	public void setOriginContactName(String originContactName) {
		this.originContactName = originContactName;
	}
	@Column(length=20)
	public String getOriginContactPhone() {
		return originContactPhone;
	}
	public void setOriginContactPhone(String originContactPhone) {
		this.originContactPhone = originContactPhone;
	}
	@Column(length=20)
	public String getOriginContactWeekend() {
		return originContactWeekend;
	}
	public void setOriginContactWeekend(String originContactWeekend) {
		this.originContactWeekend = originContactWeekend;
	}
	@Column(length=20)
	public String getOriginElevator() {
		return originElevator;
	}
	public void setOriginElevator(String originElevator) {
		this.originElevator = originElevator;
	}
	@Column
	public Boolean getOriginDock() {
		return originDock;
	}
	public void setOriginDock(Boolean originDock) {
		this.originDock = originDock;
	}
	@Column(length=20)
	public String getOriginFax() {
		return originFax;
	}
	public void setOriginFax(String originFax) {
		this.originFax = originFax;
	}
	
	@Column(length=20)
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Column(length=15)
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
		
	@Column
	public Boolean getQc() {
		return qc;
	}
	public void setQc(Boolean qc) {
		this.qc = qc;
	}
	@Column(length=82)
	public String getQcId() {
		return qcId;
	}
	public void setQcId(String qcId) {
		this.qcId = qcId;
	}
	
	
	@Column(length=20)  
	public String getRegistrationNumber() {
		return registrationNumber;
	}
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	@Column
	public Double getRevenue() {
		return revenue;
	}
	public void setRevenue(Double revenue) {
		this.revenue = revenue;
	}
	@Column(length=3) 
	public String getRevenueCalculation() {
		return revenueCalculation;
	}
	public void setRevenueCalculation(String revenueCalculation) {
		this.revenueCalculation = revenueCalculation;
	}
	@Column
	public Date getScanned() {
		return scanned;
	}
	public void setScanned(Date scanned) {
		this.scanned = scanned;
	}
	@Column
	public Date getScheduled1() {
		return scheduled1;
	}
	public void setScheduled1(Date scheduled1) {
		this.scheduled1 = scheduled1;
	}
	@Column
	public Date getScheduled2() {
		return scheduled2;
	}
	public void setScheduled2(Date scheduled2) {
		this.scheduled2 = scheduled2;
	}
	@Column(length=6) 
	public String getScheduledArrive() {
		return scheduledArrive;
	}
	public void setScheduledArrive(String scheduledArrive) {
		this.scheduledArrive = scheduledArrive;
	}
	@Column(length=15)  
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column(length=30)  
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column(length=2)
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column
	public Double getStgAmount() {
		return stgAmount;
	}
	public void setStgAmount(Double stgAmount) {
		this.stgAmount = stgAmount;
	}
	@Column(length=1)
	public String getStgHow() {
		return stgHow;
	}
	public void setStgHow(String stgHow) {
		this.stgHow = stgHow;
	}
	@Column(length=10)
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	
	@Column(length=20)  
	public String getTargetActual() {
		return targetActual;
	}
	public void setTargetActual(String targetActual) {
		this.targetActual = targetActual;
	}
	@Column
	public Long getTicket() {
		return ticket;
	}
	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}
	@Column(length=8)
	public String getTimeFrom() {
		return timeFrom;
	}
	public void setTimeFrom(String timeFrom) {
		this.timeFrom = timeFrom;
	}
	@Column(length=8)
	public String getTimeTo() {
		return timeTo;
	}
	public void setTimeTo(String timeTo) {
		this.timeTo = timeTo;
	}
	
	@Column(nullable=false)
	public int getTrucks() {
		return trucks;
	}
	public void setTrucks(int trucks) {
		this.trucks = trucks;
	}
	@Column(length=20)  
	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	
	@Column(length=20)
	public String getWhoCancelled() {
		return whoCancelled;
	}
	public void setWhoCancelled(String whoCancelled) {
		this.whoCancelled = whoCancelled;
	}
	@Column(length=10)
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length=82)  
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(length=82)
	public String getEstimator() {
		return estimator;
	}
	public void setEstimator(String estimator) {
		this.estimator = estimator;
	}
	
	@Column(length=25)
	public String getSalesMan() {
		return salesMan;
	}
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}
	@Column(length=82)
	public String getCoordinator() {
		return coordinator;
	}
	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}
	
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}
	@Column
	public Date getApprovedDate() {
		return approvedDate;
	}
	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}
	@Column
	public Boolean getNoCharge() {
		return noCharge;
	}
	public void setNoCharge(Boolean noCharge) {
		this.noCharge = noCharge;
	}
	
	@Column( length=25 )
	public String getDestinationMobile() {
		return destinationMobile;
	}
	public void setDestinationMobile(String destinationMobile) {
		this.destinationMobile = destinationMobile;
	}
	@Column( length=25 )
	public String getOriginMobile() {
		return originMobile;
	}
	public void setOriginMobile(String originMobile) {
		this.originMobile = originMobile;
	}
	
	@Column( length=4 )
	public String getDestinationPhoneExt() {
		return destinationPhoneExt;
	}
	public void setDestinationPhoneExt(String destinationPhoneExt) {
		this.destinationPhoneExt = destinationPhoneExt;
	}
	@Column( length=4 )
	public String getPhoneExt() {
		return phoneExt;
	}
	public void setPhoneExt(String phoneExt) {
		this.phoneExt = phoneExt;
	}
	@Column(length=50)
	public String getContainerNumber() {
		return containerNumber;
	}
	public void setContainerNumber(String containerNumber) {
		this.containerNumber = containerNumber;
	}
	@Transient
	public String getListCode() {
		// TODO Auto-generated method stub
		return this.ticket.toString();
	}
	@Transient
	public String getListDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListFifthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListFourthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListSecondDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListSixthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListThirdDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Column( length=10)
	public String getReviewStatus() {
		return reviewStatus;
	}
	public void setReviewStatus(String reviewStatus) {
		this.reviewStatus = reviewStatus;
	}
	public Date getReviewStatusDate() {
		return reviewStatusDate;
	}
	public void setReviewStatusDate(Date reviewStatusDate) {
		this.reviewStatusDate = reviewStatusDate;
	}
	@Column(length=70)
	public String getStorageMeasurement() {
		return storageMeasurement;
	}
	public void setStorageMeasurement(String storageMeasurement) {
		this.storageMeasurement = storageMeasurement;
	}
	@Column(length=10)
	public String getUnit1() {
		return unit1;
	}
	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}
	@Column(length=10)
	public String getUnit2() {
		return unit2;
	}
	public void setUnit2(String unit2) {
		this.unit2 = unit2;
	}
	@Transient
	public String getListSeventhDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Column
	public Date getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}
	@Column(length=10)
	public Double getExtraPayroll() {
		return extraPayroll;
	}
	public void setExtraPayroll(Double extraPayroll) {
		this.extraPayroll = extraPayroll;
	}
	@Column(length=50)
	public String getPayMethod() {
		return payMethod;
	}
	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}
	@Column(length=20,insertable = false, updatable = false)  
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	@Column( length=10)
	public String getCompanyDivision() {
		return companyDivision;
	}
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	@Column
	public Boolean getCollectCOD() {
		return collectCOD;
	}
	public void setCollectCOD(Boolean collectCOD) {
		this.collectCOD = collectCOD;
	}
	@Column
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	@Column
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	@Transient
	public String getListEigthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListNinthDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Transient
	public String getListTenthDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the storageOut
	 */
	@Column( length=1)
	public String getStorageOut() {
		return storageOut;
	}
	/**
	 * @param storageOut the storageOut to set
	 */
	public void setStorageOut(String storageOut) {
		this.storageOut = storageOut;
	}
	/**
	 * @return the distance
	 */
	@Column(length=20)
	public BigDecimal getDistance() {
		return distance;
	}
	/**
	 * @param distance the distance to set
	 */
	public void setDistance(BigDecimal distance) {
		this.distance = distance;
	}
	/**
	 * @return the distanceInKmMiles
	 */
	@Column(length=5)
	public String getDistanceInKmMiles() {
		return distanceInKmMiles;
	}
	/**
	 * @param distanceInKmMiles the distanceInKmMiles to set
	 */
	public void setDistanceInKmMiles(String distanceInKmMiles) {
		this.distanceInKmMiles = distanceInKmMiles;
	}
	@Column
	public Boolean getDestinationParkingPermit() {
		return destinationParkingPermit;
	}
	public void setDestinationParkingPermit(Boolean destinationParkingPermit) {
		this.destinationParkingPermit = destinationParkingPermit;
	}
	@Column
	public Boolean getDestinationShuttle() {
		return destinationShuttle;
	}
	public void setDestinationShuttle(Boolean destinationShuttle) {
		this.destinationShuttle = destinationShuttle;
	}
	@Column
	public Boolean getOriginParkingPermit() {
		return originParkingPermit;
	}
	public void setOriginParkingPermit(Boolean originParkingPermit) {
		this.originParkingPermit = originParkingPermit;
	}
	@Column
	public Boolean getOriginShuttle() {
		return originShuttle;
	}
	public void setOriginShuttle(Boolean originShuttle) {
		this.originShuttle = originShuttle;
	}
	@Column(length=1)
	public String getBondedGoods() {
		return bondedGoods;
	}
	public void setBondedGoods(String bondedGoods) {
		this.bondedGoods = bondedGoods;
	}
	/**
	 * @return the farInMeters
	 */
	public String getFarInMeters() {
		return farInMeters;
	}
	/**
	 * @param farInMeters the farInMeters to set
	 */
	public void setFarInMeters(String farInMeters) {
		this.farInMeters = farInMeters;
	}
	/**
	 * @return the whichFloor
	 */
	public String getWhichFloor() {
		return whichFloor;
	}
	/**
	 * @param whichFloor the whichFloor to set
	 */
	public void setWhichFloor(String whichFloor) {
		this.whichFloor = whichFloor;
	}
	/**
	 * @return the dest_farInMeters
	 */
	public String getDest_farInMeters() {
		return dest_farInMeters;
	}

	public void setDest_farInMeters(String dest_farInMeters) {
		this.dest_farInMeters = dest_farInMeters;
	}
	
	public String getDest_park() {
		return dest_park;
	}

	public void setDest_park(String dest_park) {
		this.dest_park = dest_park;
	}

	public String getDest_whichFloor() {
		return dest_whichFloor;
	}

	public void setDest_whichFloor(String dest_whichFloor) {
		this.dest_whichFloor = dest_whichFloor;
	}
	
	public String getParking() {
		return parking;
	}
	
	public void setParking(String parking) {
		this.parking = parking;
	}
	@Column(length=20)
	public String getDestMeters() {
		return destMeters;
	}
	
	public void setDestMeters(String destMeters) {
		this.destMeters = destMeters;
	}
	@Column(length=20)
	public String getOrigMeters() {
		return origMeters;
	}
	
	public void setOrigMeters(String origMeters) {
		this.origMeters = origMeters;
	}
	@Column(length=20)
	public String getParkingD() {
		return parkingD;
	}
	
	public void setParkingD(String parkingD) {
		this.parkingD = parkingD;
	}
	@Column(length=20)	
	public String getParkingS() {
		return parkingS;
	}
	
	public void setParkingS(String parkingS) {
		this.parkingS = parkingS;
	}
	@Column(length=20)
	public String getSiteD() {
		return siteD;
	}
	public void setSiteD(String siteD) {
		this.siteD = siteD;
	}
	@Column(length=20)
	public String getSiteS() {
		return siteS;
	}
	
	public void setSiteS(String siteS) {
		this.siteS = siteS;
	}

	@Column(length=1)
	public Boolean getWeekendFlag() {
		return weekendFlag;
	}
	public void setWeekendFlag(Boolean weekendFlag) {
		this.weekendFlag = weekendFlag;
	}
	@Column
	public Integer getEstimatedPieces() {
		return estimatedPieces;
	}
	public void setEstimatedPieces(Integer estimatedPieces) {
		this.estimatedPieces = estimatedPieces;
	}
	@Column
	public Integer getCrates() {
		return crates;
	}
	public void setCrates(Integer crates) {
		this.crates = crates;
	}	
	public Double getActualVolume() {
		return actualVolume;
	}
	public void setActualVolume(Double actualVolume) {
		this.actualVolume = actualVolume;
	}
	
	public String getChanges() {
		return changes;
	}
	public void setChanges(String changes) {
		this.changes = changes;
	}
	public Boolean getResourceReqdAm() {
		return resourceReqdAm;
	}
	public void setResourceReqdAm(Boolean resourceReqdAm) {
		this.resourceReqdAm = resourceReqdAm;
	}
	public Boolean getResourceReqdPm() {
		return resourceReqdPm;
	}
	public void setResourceReqdPm(Boolean resourceReqdPm) {
		this.resourceReqdPm = resourceReqdPm;
	}
	@Column
	public String getOriginPreferredContactTime() {
		return originPreferredContactTime;
	}

	public void setOriginPreferredContactTime(String originPreferredContactTime) {
		this.originPreferredContactTime = originPreferredContactTime;
	}
	@Column
	public String getDestPreferredContactTime() {
		return destPreferredContactTime;
	}

	public void setDestPreferredContactTime(String destPreferredContactTime) {
		this.destPreferredContactTime = destPreferredContactTime;
	}
	
	@Column
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	@Column
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	@Column
	public String getOriginLongCarry() {
		return originLongCarry;
	}
	public void setOriginLongCarry(String originLongCarry) {
		this.originLongCarry = originLongCarry;
	}
	@Column
	public String getDestinationLongCarry() {
		return destinationLongCarry;
	}
	public void setDestinationLongCarry(String destinationLongCarry) {
		this.destinationLongCarry = destinationLongCarry;
	}
	@Column
	public String getAttachedFile() {
		return attachedFile;
	}
	public void setAttachedFile(String attachedFile) {
		this.attachedFile = attachedFile;
	}
	@Column
	public Boolean getHse() {
		return hse;
	}
	public void setHse(Boolean hse) {
		this.hse = hse;
	}
	public BigDecimal getRequiredCrew() {
		return requiredCrew;
	}
	public void setRequiredCrew(BigDecimal requiredCrew) {
		this.requiredCrew = requiredCrew;
	}
	@Column
	public String getTicketAssignedStatus() {
		return ticketAssignedStatus;
	}
	public void setTicketAssignedStatus(String ticketAssignedStatus) {
		this.ticketAssignedStatus = ticketAssignedStatus;
	}
	public String getOriginAddressBook() {
		return originAddressBook;
	}
	public void setOriginAddressBook(String originAddressBook) {
		this.originAddressBook = originAddressBook;
	}
	public String getDestinationAddressBook() {
		return destinationAddressBook;
	}
	public void setDestinationAddressBook(String destinationAddressBook) {
		this.destinationAddressBook = destinationAddressBook;
	}
	@Column(length=1)
	public Boolean getThirdPartyRequired() {
		return thirdPartyRequired;
	}
	public void setThirdPartyRequired(Boolean thirdPartyRequired) {
		this.thirdPartyRequired = thirdPartyRequired;
	}
	@Column(length=1)
	public Boolean getacceptByDriver() {
		return acceptByDriver;
	}
	public void setacceptByDriver(Boolean acceptByDriver) {
		this.acceptByDriver = acceptByDriver;
	}
	@Column
	public String getCrewName() {
		return crewName;
	}
	public void setCrewName(String crewName) {
		this.crewName = crewName;
	}
	@Column
	public String getProjectLeadName() {
		return projectLeadName;
	}
	public void setProjectLeadName(String projectLeadName) {
		this.projectLeadName = projectLeadName;
	}
	@Column
	public String getProjectLeadNumber() {
		return projectLeadNumber;
	}
	public void setProjectLeadNumber(String projectLeadNumber) {
		this.projectLeadNumber = projectLeadNumber;
	}
	/**
	 * @return the outOfScope
	 */
	@Column
	public Boolean getOutOfScope() {
		return outOfScope;
	}
	/**
	 * @param outOfScope the outOfScope to set
	 */
	public void setOutOfScope(Boolean outOfScope) {
		this.outOfScope = outOfScope;
	}
	/**
	 * @return the crewVehicleSummary
	 */
	@Column
	public String getCrewVehicleSummary() {
		return crewVehicleSummary;
	}
	/**
	 * @param crewVehicleSummary the crewVehicleSummary to set
	 */
	public void setCrewVehicleSummary(String crewVehicleSummary) {
		this.crewVehicleSummary = crewVehicleSummary;
	}
	@Column
	public Integer getFvpValue() {
		return fvpValue;
	}
	public void setFvpValue(Integer fvpValue) {
		this.fvpValue = fvpValue;
	}
	@Column
	public Integer getAcvValue() {
		return acvValue;
	}
	public void setAcvValue(Integer acvValue) {
		this.acvValue = acvValue;
	}
	
	@Column
	public Double getHours() {
		return hours;
	}
	public void setHours(Double hours) {
		this.hours = hours;
	}

}
	