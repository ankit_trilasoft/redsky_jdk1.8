package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.RefJobType;

public interface RefJobTypeManager  extends GenericManager<RefJobType, Long> {

	public List isExisted(String job,String companydivision,String sessionCorpID);
	public List searchResult(String job,String companydivision,String surveytool, String sessionCorpID);
	
}
