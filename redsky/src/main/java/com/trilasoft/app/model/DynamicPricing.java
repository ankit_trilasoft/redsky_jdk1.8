package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="dynamicpricing")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID") 

public class DynamicPricing extends BaseObject{
	private Long id;
	private String corpID;
	private String updatedBy;
	private String createdBy;
	private Date updatedOn;
	private Date createdOn;

	private String dynamicPricingType;
	private Double regularHourlyRatePer2Men=new Double("0.00");
	private Double regularHourlyRatePer3Men=new Double("0.00");
	private Double regularHourlyRatePer4Men=new Double("0.00");
	private Double monthEndHourlyRatePer2Men=new Double("0.00");
	private Double monthEndHourlyRatePer3Men=new Double("0.00");
	private Double monthEndHourlyRatePer4Men=new Double("0.00");
	private Double extraMenRate=new Double("0.00");
	private Integer noOfEmployees=new Integer("0");
	private Double saturdayRatePersent=new Double("0.00");
	private Double sundayRatePersent=new Double("0.00");
	private Integer minRange1Men=new Integer("0");
	private Integer maxRange1Men=new Integer("0");
	private Integer minRange2Men=new Integer("0");
	private Integer maxRange2Men=new Integer("0");
	private Integer minRange3Men=new Integer("0");
	private Integer maxRange3Men=new Integer("0");
	private Integer minRange4Men=new Integer("0");
	private Integer maxRange4Men=new Integer("0");
	private Integer minRange5Men=new Integer("0");
	private Integer maxRange5Men=new Integer("0");
	private Integer minRange6Men=new Integer("0");
	private Integer maxRange6Men=new Integer("0");
	private Double deviationRange1forLessThan7Days=new Double("0.00");
	private Double deviationRange2forLessThan7Days=new Double("0.00");
	private Double deviationRange3forLessThan7Days=new Double("0.00");
	private Double deviationRange4forLessThan7Days=new Double("0.00");
	private Double deviationRange5forLessThan7Days=new Double("0.00");
	private Double deviationRange6forLessThan7Days=new Double("0.00");
	private Double deviationRange1forBetween7to13Days=new Double("0.00");
	private Double deviationRange2forBetween7to13Days=new Double("0.00");
	private Double deviationRange3forBetween7to13Days=new Double("0.00");
	private Double deviationRange4forBetween7to13Days=new Double("0.00");
	private Double deviationRange5forBetween7to13Days=new Double("0.00");
	private Double deviationRange6forBetween7to13Days=new Double("0.00");
	private Double deviationRange1forBetween14to21Days=new Double("0.00");
	private Double deviationRange2forBetween14to21Days=new Double("0.00");
	private Double deviationRange3forBetween14to21Days=new Double("0.00");
	private Double deviationRange4forBetween14to21Days=new Double("0.00");
	private Double deviationRange5forBetween14to21Days=new Double("0.00");
	private Double deviationRange6forBetween14to21Days=new Double("0.00");
	private Double deviationRange1forMoreThan21Days=new Double("0.00");
	private Double deviationRange2forMoreThan21Days=new Double("0.00");
	private Double deviationRange3forMoreThan21Days=new Double("0.00");
	private Double deviationRange4forMoreThan21Days=new Double("0.00");
	private Double deviationRange5forMoreThan21Days=new Double("0.00");
	private Double deviationRange6forMoreThan21Days=new Double("0.00");
	private Double regularRatePer2MenRange1forMoreThan21Days=new Double("0.00");
	private Double regularRatePer3MenRange1forMoreThan21Days=new Double("0.00");
	private Double regularRatePer4MenRange1forMoreThan21Days=new Double("0.00");
	private Double regularRatePer2MenRange2forMoreThan21Days=new Double("0.00");
	private Double regularRatePer3MenRange2forMoreThan21Days=new Double("0.00");
	private Double regularRatePer4MenRange2forMoreThan21Days=new Double("0.00");
	private Double regularRatePer2MenRange3forMoreThan21Days=new Double("0.00");
	private Double regularRatePer3MenRange3forMoreThan21Days=new Double("0.00");
	private Double regularRatePer4MenRange3forMoreThan21Days=new Double("0.00");
	private Double regularRatePer2MenRange4forMoreThan21Days=new Double("0.00");
	private Double regularRatePer3MenRange4forMoreThan21Days=new Double("0.00");
	private Double regularRatePer4MenRange4forMoreThan21Days=new Double("0.00");
	private Double regularRatePer2MenRange5forMoreThan21Days=new Double("0.00");
	private Double regularRatePer3MenRange5forMoreThan21Days=new Double("0.00");
	private Double regularRatePer4MenRange5forMoreThan21Days=new Double("0.00");
	private Double regularRatePer2MenRange6forMoreThan21Days=new Double("0.00");
	private Double regularRatePer3MenRange6forMoreThan21Days=new Double("0.00");
	private Double regularRatePer4MenRange6forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer2MenRange1forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer3MenRange1forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer4MenRange1forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer2MenRange2forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer3MenRange2forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer4MenRange2forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer2MenRange3forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer3MenRange3forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer4MenRange3forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer2MenRange4forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer3MenRange4forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer4MenRange4forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer2MenRange5forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer3MenRange5forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer4MenRange5forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer2MenRange6forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer3MenRange6forMoreThan21Days=new Double("0.00");
	private Double monthEndRatePer4MenRange6forMoreThan21Days=new Double("0.00");
	private Double regularRatePer2MenRange1forBetween14to21Days=new Double("0.00");
	private Double regularRatePer3MenRange1forBetween14to21Days=new Double("0.00");
	private Double regularRatePer4MenRange1forBetween14to21Days=new Double("0.00");
	private Double regularRatePer2MenRange2forBetween14to21Days=new Double("0.00");
	private Double regularRatePer3MenRange2forBetween14to21Days=new Double("0.00");
	private Double regularRatePer4MenRange2forBetween14to21Days=new Double("0.00");
	private Double regularRatePer2MenRange3forBetween14to21Days=new Double("0.00");
	private Double regularRatePer3MenRange3forBetween14to21Days=new Double("0.00");
	private Double regularRatePer4MenRange3forBetween14to21Days=new Double("0.00");
	private Double regularRatePer2MenRange4forBetween14to21Days=new Double("0.00");
	private Double regularRatePer3MenRange4forBetween14to21Days=new Double("0.00");
	private Double regularRatePer4MenRange4forBetween14to21Days=new Double("0.00");
	private Double regularRatePer2MenRange5forBetween14to21Days=new Double("0.00");
	private Double regularRatePer3MenRange5forBetween14to21Days=new Double("0.00");
	private Double regularRatePer4MenRange5forBetween14to21Days=new Double("0.00");
	private Double regularRatePer2MenRange6forBetween14to21Days=new Double("0.00");
	private Double regularRatePer3MenRange6forBetween14to21Days=new Double("0.00");
	private Double regularRatePer4MenRange6forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer2MenRange1forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer3MenRange1forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer4MenRange1forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer2MenRange2forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer3MenRange2forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer4MenRange2forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer2MenRange3forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer3MenRange3forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer4MenRange3forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer2MenRange4forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer3MenRange4forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer4MenRange4forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer2MenRange5forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer3MenRange5forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer4MenRange5forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer2MenRange6forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer3MenRange6forBetween14to21Days=new Double("0.00");
	private Double monthEndRatePer4MenRange6forBetween14to21Days=new Double("0.00");
	private Double regularRatePer2MenRange1forBetween7to13Days=new Double("0.00");
	private Double regularRatePer3MenRange1forBetween7to13Days=new Double("0.00");
	private Double regularRatePer4MenRange1forBetween7to13Days=new Double("0.00");
	private Double regularRatePer2MenRange2forBetween7to13Days=new Double("0.00");
	private Double regularRatePer3MenRange2forBetween7to13Days=new Double("0.00");
	private Double regularRatePer4MenRange2forBetween7to13Days=new Double("0.00");
	private Double regularRatePer2MenRange3forBetween7to13Days=new Double("0.00");
	private Double regularRatePer3MenRange3forBetween7to13Days=new Double("0.00");
	private Double regularRatePer4MenRange3forBetween7to13Days=new Double("0.00");
	private Double regularRatePer2MenRange4forBetween7to13Days=new Double("0.00");
	private Double regularRatePer3MenRange4forBetween7to13Days=new Double("0.00");
	private Double regularRatePer4MenRange4forBetween7to13Days=new Double("0.00");
	private Double regularRatePer2MenRange5forBetween7to13Days=new Double("0.00");
	private Double regularRatePer3MenRange5forBetween7to13Days=new Double("0.00");
	private Double regularRatePer4MenRange5forBetween7to13Days=new Double("0.00");
	private Double regularRatePer2MenRange6forBetween7to13Days=new Double("0.00");
	private Double regularRatePer3MenRange6forBetween7to13Days=new Double("0.00");
	private Double regularRatePer4MenRange6forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer2MenRange1forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer3MenRange1forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer4MenRange1forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer2MenRange2forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer3MenRange2forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer4MenRange2forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer2MenRange3forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer3MenRange3forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer4MenRange3forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer2MenRange4forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer3MenRange4forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer4MenRange4forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer2MenRange5forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer3MenRange5forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer4MenRange5forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer2MenRange6forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer3MenRange6forBetween7to13Days=new Double("0.00");
	private Double monthEndRatePer4MenRange6forBetween7to13Days=new Double("0.00");
	private Double regularRatePer2MenRange1forLessThan7Days=new Double("0.00");
	private Double regularRatePer3MenRange1forLessThan7Days=new Double("0.00");
	private Double regularRatePer4MenRange1forLessThan7Days=new Double("0.00");
	private Double regularRatePer2MenRange2forLessThan7Days=new Double("0.00");
	private Double regularRatePer3MenRange2forLessThan7Days=new Double("0.00");
	private Double regularRatePer4MenRange2forLessThan7Days=new Double("0.00");
	private Double regularRatePer2MenRange3forLessThan7Days=new Double("0.00");
	private Double regularRatePer3MenRange3forLessThan7Days=new Double("0.00");
	private Double regularRatePer4MenRange3forLessThan7Days=new Double("0.00");
	private Double regularRatePer2MenRange4forLessThan7Days=new Double("0.00");
	private Double regularRatePer3MenRange4forLessThan7Days=new Double("0.00");
	private Double regularRatePer4MenRange4forLessThan7Days=new Double("0.00");
	private Double regularRatePer2MenRange5forLessThan7Days=new Double("0.00");
	private Double regularRatePer3MenRange5forLessThan7Days=new Double("0.00");
	private Double regularRatePer4MenRange5forLessThan7Days=new Double("0.00");
	private Double regularRatePer2MenRange6forLessThan7Days=new Double("0.00");
	private Double regularRatePer3MenRange6forLessThan7Days=new Double("0.00");
	private Double regularRatePer4MenRange6forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer2MenRange1forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer3MenRange1forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer4MenRange1forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer2MenRange2forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer3MenRange2forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer4MenRange2forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer2MenRange3forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer3MenRange3forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer4MenRange3forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer2MenRange4forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer3MenRange4forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer4MenRange4forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer2MenRange5forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer3MenRange5forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer4MenRange5forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer2MenRange6forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer3MenRange6forLessThan7Days=new Double("0.00");
	private Double monthEndRatePer4MenRange6forLessThan7Days=new Double("0.00");
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("corpID", corpID)
				.append("updatedBy", updatedBy)
				.append("createdBy", createdBy)
				.append("updatedOn", updatedOn)
				.append("createdOn", createdOn)
				.append("dynamicPricingType", dynamicPricingType)
				.append("regularHourlyRatePer2Men", regularHourlyRatePer2Men)
				.append("regularHourlyRatePer3Men", regularHourlyRatePer3Men)
				.append("regularHourlyRatePer4Men", regularHourlyRatePer4Men)
				.append("monthEndHourlyRatePer2Men", monthEndHourlyRatePer2Men)
				.append("monthEndHourlyRatePer3Men", monthEndHourlyRatePer3Men)
				.append("monthEndHourlyRatePer4Men", monthEndHourlyRatePer4Men)
				.append("extraMenRate", extraMenRate)
				.append("noOfEmployees", noOfEmployees)
				.append("saturdayRatePersent", saturdayRatePersent)
				.append("sundayRatePersent", sundayRatePersent)
				.append("minRange1Men", minRange1Men)
				.append("maxRange1Men", maxRange1Men)
				.append("minRange2Men", minRange2Men)
				.append("maxRange2Men", maxRange2Men)
				.append("minRange3Men", minRange3Men)
				.append("maxRange3Men", maxRange3Men)
				.append("minRange4Men", minRange4Men)
				.append("maxRange4Men", maxRange4Men)
				.append("minRange5Men", minRange5Men)
				.append("maxRange5Men", maxRange5Men)
				.append("minRange6Men", minRange6Men)
				.append("maxRange6Men", maxRange6Men)
				.append("deviationRange1forLessThan7Days",
						deviationRange1forLessThan7Days)
				.append("deviationRange2forLessThan7Days",
						deviationRange2forLessThan7Days)
				.append("deviationRange3forLessThan7Days",
						deviationRange3forLessThan7Days)
				.append("deviationRange4forLessThan7Days",
						deviationRange4forLessThan7Days)
				.append("deviationRange5forLessThan7Days",
						deviationRange5forLessThan7Days)
				.append("deviationRange6forLessThan7Days",
						deviationRange6forLessThan7Days)
				.append("deviationRange1forBetween7to13Days",
						deviationRange1forBetween7to13Days)
				.append("deviationRange2forBetween7to13Days",
						deviationRange2forBetween7to13Days)
				.append("deviationRange3forBetween7to13Days",
						deviationRange3forBetween7to13Days)
				.append("deviationRange4forBetween7to13Days",
						deviationRange4forBetween7to13Days)
				.append("deviationRange5forBetween7to13Days",
						deviationRange5forBetween7to13Days)
				.append("deviationRange6forBetween7to13Days",
						deviationRange6forBetween7to13Days)
				.append("deviationRange1forBetween14to21Days",
						deviationRange1forBetween14to21Days)
				.append("deviationRange2forBetween14to21Days",
						deviationRange2forBetween14to21Days)
				.append("deviationRange3forBetween14to21Days",
						deviationRange3forBetween14to21Days)
				.append("deviationRange4forBetween14to21Days",
						deviationRange4forBetween14to21Days)
				.append("deviationRange5forBetween14to21Days",
						deviationRange5forBetween14to21Days)
				.append("deviationRange6forBetween14to21Days",
						deviationRange6forBetween14to21Days)
				.append("deviationRange1forMoreThan21Days",
						deviationRange1forMoreThan21Days)
				.append("deviationRange2forMoreThan21Days",
						deviationRange2forMoreThan21Days)
				.append("deviationRange3forMoreThan21Days",
						deviationRange3forMoreThan21Days)
				.append("deviationRange4forMoreThan21Days",
						deviationRange4forMoreThan21Days)
				.append("deviationRange5forMoreThan21Days",
						deviationRange5forMoreThan21Days)
				.append("deviationRange6forMoreThan21Days",
						deviationRange6forMoreThan21Days)
				.append("regularRatePer2MenRange1forMoreThan21Days",
						regularRatePer2MenRange1forMoreThan21Days)
				.append("regularRatePer3MenRange1forMoreThan21Days",
						regularRatePer3MenRange1forMoreThan21Days)
				.append("regularRatePer4MenRange1forMoreThan21Days",
						regularRatePer4MenRange1forMoreThan21Days)
				.append("regularRatePer2MenRange2forMoreThan21Days",
						regularRatePer2MenRange2forMoreThan21Days)
				.append("regularRatePer3MenRange2forMoreThan21Days",
						regularRatePer3MenRange2forMoreThan21Days)
				.append("regularRatePer4MenRange2forMoreThan21Days",
						regularRatePer4MenRange2forMoreThan21Days)
				.append("regularRatePer2MenRange3forMoreThan21Days",
						regularRatePer2MenRange3forMoreThan21Days)
				.append("regularRatePer3MenRange3forMoreThan21Days",
						regularRatePer3MenRange3forMoreThan21Days)
				.append("regularRatePer4MenRange3forMoreThan21Days",
						regularRatePer4MenRange3forMoreThan21Days)
				.append("regularRatePer2MenRange4forMoreThan21Days",
						regularRatePer2MenRange4forMoreThan21Days)
				.append("regularRatePer3MenRange4forMoreThan21Days",
						regularRatePer3MenRange4forMoreThan21Days)
				.append("regularRatePer4MenRange4forMoreThan21Days",
						regularRatePer4MenRange4forMoreThan21Days)
				.append("regularRatePer2MenRange5forMoreThan21Days",
						regularRatePer2MenRange5forMoreThan21Days)
				.append("regularRatePer3MenRange5forMoreThan21Days",
						regularRatePer3MenRange5forMoreThan21Days)
				.append("regularRatePer4MenRange5forMoreThan21Days",
						regularRatePer4MenRange5forMoreThan21Days)
				.append("regularRatePer2MenRange6forMoreThan21Days",
						regularRatePer2MenRange6forMoreThan21Days)
				.append("regularRatePer3MenRange6forMoreThan21Days",
						regularRatePer3MenRange6forMoreThan21Days)
				.append("regularRatePer4MenRange6forMoreThan21Days",
						regularRatePer4MenRange6forMoreThan21Days)
				.append("monthEndRatePer2MenRange1forMoreThan21Days",
						monthEndRatePer2MenRange1forMoreThan21Days)
				.append("monthEndRatePer3MenRange1forMoreThan21Days",
						monthEndRatePer3MenRange1forMoreThan21Days)
				.append("monthEndRatePer4MenRange1forMoreThan21Days",
						monthEndRatePer4MenRange1forMoreThan21Days)
				.append("monthEndRatePer2MenRange2forMoreThan21Days",
						monthEndRatePer2MenRange2forMoreThan21Days)
				.append("monthEndRatePer3MenRange2forMoreThan21Days",
						monthEndRatePer3MenRange2forMoreThan21Days)
				.append("monthEndRatePer4MenRange2forMoreThan21Days",
						monthEndRatePer4MenRange2forMoreThan21Days)
				.append("monthEndRatePer2MenRange3forMoreThan21Days",
						monthEndRatePer2MenRange3forMoreThan21Days)
				.append("monthEndRatePer3MenRange3forMoreThan21Days",
						monthEndRatePer3MenRange3forMoreThan21Days)
				.append("monthEndRatePer4MenRange3forMoreThan21Days",
						monthEndRatePer4MenRange3forMoreThan21Days)
				.append("monthEndRatePer2MenRange4forMoreThan21Days",
						monthEndRatePer2MenRange4forMoreThan21Days)
				.append("monthEndRatePer3MenRange4forMoreThan21Days",
						monthEndRatePer3MenRange4forMoreThan21Days)
				.append("monthEndRatePer4MenRange4forMoreThan21Days",
						monthEndRatePer4MenRange4forMoreThan21Days)
				.append("monthEndRatePer2MenRange5forMoreThan21Days",
						monthEndRatePer2MenRange5forMoreThan21Days)
				.append("monthEndRatePer3MenRange5forMoreThan21Days",
						monthEndRatePer3MenRange5forMoreThan21Days)
				.append("monthEndRatePer4MenRange5forMoreThan21Days",
						monthEndRatePer4MenRange5forMoreThan21Days)
				.append("monthEndRatePer2MenRange6forMoreThan21Days",
						monthEndRatePer2MenRange6forMoreThan21Days)
				.append("monthEndRatePer3MenRange6forMoreThan21Days",
						monthEndRatePer3MenRange6forMoreThan21Days)
				.append("monthEndRatePer4MenRange6forMoreThan21Days",
						monthEndRatePer4MenRange6forMoreThan21Days)
				.append("regularRatePer2MenRange1forBetween14to21Days",
						regularRatePer2MenRange1forBetween14to21Days)
				.append("regularRatePer3MenRange1forBetween14to21Days",
						regularRatePer3MenRange1forBetween14to21Days)
				.append("regularRatePer4MenRange1forBetween14to21Days",
						regularRatePer4MenRange1forBetween14to21Days)
				.append("regularRatePer2MenRange2forBetween14to21Days",
						regularRatePer2MenRange2forBetween14to21Days)
				.append("regularRatePer3MenRange2forBetween14to21Days",
						regularRatePer3MenRange2forBetween14to21Days)
				.append("regularRatePer4MenRange2forBetween14to21Days",
						regularRatePer4MenRange2forBetween14to21Days)
				.append("regularRatePer2MenRange3forBetween14to21Days",
						regularRatePer2MenRange3forBetween14to21Days)
				.append("regularRatePer3MenRange3forBetween14to21Days",
						regularRatePer3MenRange3forBetween14to21Days)
				.append("regularRatePer4MenRange3forBetween14to21Days",
						regularRatePer4MenRange3forBetween14to21Days)
				.append("regularRatePer2MenRange4forBetween14to21Days",
						regularRatePer2MenRange4forBetween14to21Days)
				.append("regularRatePer3MenRange4forBetween14to21Days",
						regularRatePer3MenRange4forBetween14to21Days)
				.append("regularRatePer4MenRange4forBetween14to21Days",
						regularRatePer4MenRange4forBetween14to21Days)
				.append("regularRatePer2MenRange5forBetween14to21Days",
						regularRatePer2MenRange5forBetween14to21Days)
				.append("regularRatePer3MenRange5forBetween14to21Days",
						regularRatePer3MenRange5forBetween14to21Days)
				.append("regularRatePer4MenRange5forBetween14to21Days",
						regularRatePer4MenRange5forBetween14to21Days)
				.append("regularRatePer2MenRange6forBetween14to21Days",
						regularRatePer2MenRange6forBetween14to21Days)
				.append("regularRatePer3MenRange6forBetween14to21Days",
						regularRatePer3MenRange6forBetween14to21Days)
				.append("regularRatePer4MenRange6forBetween14to21Days",
						regularRatePer4MenRange6forBetween14to21Days)
				.append("monthEndRatePer2MenRange1forBetween14to21Days",
						monthEndRatePer2MenRange1forBetween14to21Days)
				.append("monthEndRatePer3MenRange1forBetween14to21Days",
						monthEndRatePer3MenRange1forBetween14to21Days)
				.append("monthEndRatePer4MenRange1forBetween14to21Days",
						monthEndRatePer4MenRange1forBetween14to21Days)
				.append("monthEndRatePer2MenRange2forBetween14to21Days",
						monthEndRatePer2MenRange2forBetween14to21Days)
				.append("monthEndRatePer3MenRange2forBetween14to21Days",
						monthEndRatePer3MenRange2forBetween14to21Days)
				.append("monthEndRatePer4MenRange2forBetween14to21Days",
						monthEndRatePer4MenRange2forBetween14to21Days)
				.append("monthEndRatePer2MenRange3forBetween14to21Days",
						monthEndRatePer2MenRange3forBetween14to21Days)
				.append("monthEndRatePer3MenRange3forBetween14to21Days",
						monthEndRatePer3MenRange3forBetween14to21Days)
				.append("monthEndRatePer4MenRange3forBetween14to21Days",
						monthEndRatePer4MenRange3forBetween14to21Days)
				.append("monthEndRatePer2MenRange4forBetween14to21Days",
						monthEndRatePer2MenRange4forBetween14to21Days)
				.append("monthEndRatePer3MenRange4forBetween14to21Days",
						monthEndRatePer3MenRange4forBetween14to21Days)
				.append("monthEndRatePer4MenRange4forBetween14to21Days",
						monthEndRatePer4MenRange4forBetween14to21Days)
				.append("monthEndRatePer2MenRange5forBetween14to21Days",
						monthEndRatePer2MenRange5forBetween14to21Days)
				.append("monthEndRatePer3MenRange5forBetween14to21Days",
						monthEndRatePer3MenRange5forBetween14to21Days)
				.append("monthEndRatePer4MenRange5forBetween14to21Days",
						monthEndRatePer4MenRange5forBetween14to21Days)
				.append("monthEndRatePer2MenRange6forBetween14to21Days",
						monthEndRatePer2MenRange6forBetween14to21Days)
				.append("monthEndRatePer3MenRange6forBetween14to21Days",
						monthEndRatePer3MenRange6forBetween14to21Days)
				.append("monthEndRatePer4MenRange6forBetween14to21Days",
						monthEndRatePer4MenRange6forBetween14to21Days)
				.append("regularRatePer2MenRange1forBetween7to13Days",
						regularRatePer2MenRange1forBetween7to13Days)
				.append("regularRatePer3MenRange1forBetween7to13Days",
						regularRatePer3MenRange1forBetween7to13Days)
				.append("regularRatePer4MenRange1forBetween7to13Days",
						regularRatePer4MenRange1forBetween7to13Days)
				.append("regularRatePer2MenRange2forBetween7to13Days",
						regularRatePer2MenRange2forBetween7to13Days)
				.append("regularRatePer3MenRange2forBetween7to13Days",
						regularRatePer3MenRange2forBetween7to13Days)
				.append("regularRatePer4MenRange2forBetween7to13Days",
						regularRatePer4MenRange2forBetween7to13Days)
				.append("regularRatePer2MenRange3forBetween7to13Days",
						regularRatePer2MenRange3forBetween7to13Days)
				.append("regularRatePer3MenRange3forBetween7to13Days",
						regularRatePer3MenRange3forBetween7to13Days)
				.append("regularRatePer4MenRange3forBetween7to13Days",
						regularRatePer4MenRange3forBetween7to13Days)
				.append("regularRatePer2MenRange4forBetween7to13Days",
						regularRatePer2MenRange4forBetween7to13Days)
				.append("regularRatePer3MenRange4forBetween7to13Days",
						regularRatePer3MenRange4forBetween7to13Days)
				.append("regularRatePer4MenRange4forBetween7to13Days",
						regularRatePer4MenRange4forBetween7to13Days)
				.append("regularRatePer2MenRange5forBetween7to13Days",
						regularRatePer2MenRange5forBetween7to13Days)
				.append("regularRatePer3MenRange5forBetween7to13Days",
						regularRatePer3MenRange5forBetween7to13Days)
				.append("regularRatePer4MenRange5forBetween7to13Days",
						regularRatePer4MenRange5forBetween7to13Days)
				.append("regularRatePer2MenRange6forBetween7to13Days",
						regularRatePer2MenRange6forBetween7to13Days)
				.append("regularRatePer3MenRange6forBetween7to13Days",
						regularRatePer3MenRange6forBetween7to13Days)
				.append("regularRatePer4MenRange6forBetween7to13Days",
						regularRatePer4MenRange6forBetween7to13Days)
				.append("monthEndRatePer2MenRange1forBetween7to13Days",
						monthEndRatePer2MenRange1forBetween7to13Days)
				.append("monthEndRatePer3MenRange1forBetween7to13Days",
						monthEndRatePer3MenRange1forBetween7to13Days)
				.append("monthEndRatePer4MenRange1forBetween7to13Days",
						monthEndRatePer4MenRange1forBetween7to13Days)
				.append("monthEndRatePer2MenRange2forBetween7to13Days",
						monthEndRatePer2MenRange2forBetween7to13Days)
				.append("monthEndRatePer3MenRange2forBetween7to13Days",
						monthEndRatePer3MenRange2forBetween7to13Days)
				.append("monthEndRatePer4MenRange2forBetween7to13Days",
						monthEndRatePer4MenRange2forBetween7to13Days)
				.append("monthEndRatePer2MenRange3forBetween7to13Days",
						monthEndRatePer2MenRange3forBetween7to13Days)
				.append("monthEndRatePer3MenRange3forBetween7to13Days",
						monthEndRatePer3MenRange3forBetween7to13Days)
				.append("monthEndRatePer4MenRange3forBetween7to13Days",
						monthEndRatePer4MenRange3forBetween7to13Days)
				.append("monthEndRatePer2MenRange4forBetween7to13Days",
						monthEndRatePer2MenRange4forBetween7to13Days)
				.append("monthEndRatePer3MenRange4forBetween7to13Days",
						monthEndRatePer3MenRange4forBetween7to13Days)
				.append("monthEndRatePer4MenRange4forBetween7to13Days",
						monthEndRatePer4MenRange4forBetween7to13Days)
				.append("monthEndRatePer2MenRange5forBetween7to13Days",
						monthEndRatePer2MenRange5forBetween7to13Days)
				.append("monthEndRatePer3MenRange5forBetween7to13Days",
						monthEndRatePer3MenRange5forBetween7to13Days)
				.append("monthEndRatePer4MenRange5forBetween7to13Days",
						monthEndRatePer4MenRange5forBetween7to13Days)
				.append("monthEndRatePer2MenRange6forBetween7to13Days",
						monthEndRatePer2MenRange6forBetween7to13Days)
				.append("monthEndRatePer3MenRange6forBetween7to13Days",
						monthEndRatePer3MenRange6forBetween7to13Days)
				.append("monthEndRatePer4MenRange6forBetween7to13Days",
						monthEndRatePer4MenRange6forBetween7to13Days)
				.append("regularRatePer2MenRange1forLessThan7Days",
						regularRatePer2MenRange1forLessThan7Days)
				.append("regularRatePer3MenRange1forLessThan7Days",
						regularRatePer3MenRange1forLessThan7Days)
				.append("regularRatePer4MenRange1forLessThan7Days",
						regularRatePer4MenRange1forLessThan7Days)
				.append("regularRatePer2MenRange2forLessThan7Days",
						regularRatePer2MenRange2forLessThan7Days)
				.append("regularRatePer3MenRange2forLessThan7Days",
						regularRatePer3MenRange2forLessThan7Days)
				.append("regularRatePer4MenRange2forLessThan7Days",
						regularRatePer4MenRange2forLessThan7Days)
				.append("regularRatePer2MenRange3forLessThan7Days",
						regularRatePer2MenRange3forLessThan7Days)
				.append("regularRatePer3MenRange3forLessThan7Days",
						regularRatePer3MenRange3forLessThan7Days)
				.append("regularRatePer4MenRange3forLessThan7Days",
						regularRatePer4MenRange3forLessThan7Days)
				.append("regularRatePer2MenRange4forLessThan7Days",
						regularRatePer2MenRange4forLessThan7Days)
				.append("regularRatePer3MenRange4forLessThan7Days",
						regularRatePer3MenRange4forLessThan7Days)
				.append("regularRatePer4MenRange4forLessThan7Days",
						regularRatePer4MenRange4forLessThan7Days)
				.append("regularRatePer2MenRange5forLessThan7Days",
						regularRatePer2MenRange5forLessThan7Days)
				.append("regularRatePer3MenRange5forLessThan7Days",
						regularRatePer3MenRange5forLessThan7Days)
				.append("regularRatePer4MenRange5forLessThan7Days",
						regularRatePer4MenRange5forLessThan7Days)
				.append("regularRatePer2MenRange6forLessThan7Days",
						regularRatePer2MenRange6forLessThan7Days)
				.append("regularRatePer3MenRange6forLessThan7Days",
						regularRatePer3MenRange6forLessThan7Days)
				.append("regularRatePer4MenRange6forLessThan7Days",
						regularRatePer4MenRange6forLessThan7Days)
				.append("monthEndRatePer2MenRange1forLessThan7Days",
						monthEndRatePer2MenRange1forLessThan7Days)
				.append("monthEndRatePer3MenRange1forLessThan7Days",
						monthEndRatePer3MenRange1forLessThan7Days)
				.append("monthEndRatePer4MenRange1forLessThan7Days",
						monthEndRatePer4MenRange1forLessThan7Days)
				.append("monthEndRatePer2MenRange2forLessThan7Days",
						monthEndRatePer2MenRange2forLessThan7Days)
				.append("monthEndRatePer3MenRange2forLessThan7Days",
						monthEndRatePer3MenRange2forLessThan7Days)
				.append("monthEndRatePer4MenRange2forLessThan7Days",
						monthEndRatePer4MenRange2forLessThan7Days)
				.append("monthEndRatePer2MenRange3forLessThan7Days",
						monthEndRatePer2MenRange3forLessThan7Days)
				.append("monthEndRatePer3MenRange3forLessThan7Days",
						monthEndRatePer3MenRange3forLessThan7Days)
				.append("monthEndRatePer4MenRange3forLessThan7Days",
						monthEndRatePer4MenRange3forLessThan7Days)
				.append("monthEndRatePer2MenRange4forLessThan7Days",
						monthEndRatePer2MenRange4forLessThan7Days)
				.append("monthEndRatePer3MenRange4forLessThan7Days",
						monthEndRatePer3MenRange4forLessThan7Days)
				.append("monthEndRatePer4MenRange4forLessThan7Days",
						monthEndRatePer4MenRange4forLessThan7Days)
				.append("monthEndRatePer2MenRange5forLessThan7Days",
						monthEndRatePer2MenRange5forLessThan7Days)
				.append("monthEndRatePer3MenRange5forLessThan7Days",
						monthEndRatePer3MenRange5forLessThan7Days)
				.append("monthEndRatePer4MenRange5forLessThan7Days",
						monthEndRatePer4MenRange5forLessThan7Days)
				.append("monthEndRatePer2MenRange6forLessThan7Days",
						monthEndRatePer2MenRange6forLessThan7Days)
				.append("monthEndRatePer3MenRange6forLessThan7Days",
						monthEndRatePer3MenRange6forLessThan7Days)
				.append("monthEndRatePer4MenRange6forLessThan7Days",
						monthEndRatePer4MenRange6forLessThan7Days).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DynamicPricing))
			return false;
		DynamicPricing castOther = (DynamicPricing) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(createdOn, castOther.createdOn)
				.append(dynamicPricingType, castOther.dynamicPricingType)
				.append(regularHourlyRatePer2Men,
						castOther.regularHourlyRatePer2Men)
				.append(regularHourlyRatePer3Men,
						castOther.regularHourlyRatePer3Men)
				.append(regularHourlyRatePer4Men,
						castOther.regularHourlyRatePer4Men)
				.append(monthEndHourlyRatePer2Men,
						castOther.monthEndHourlyRatePer2Men)
				.append(monthEndHourlyRatePer3Men,
						castOther.monthEndHourlyRatePer3Men)
				.append(monthEndHourlyRatePer4Men,
						castOther.monthEndHourlyRatePer4Men)
				.append(extraMenRate, castOther.extraMenRate)
				.append(noOfEmployees, castOther.noOfEmployees)
				.append(saturdayRatePersent, castOther.saturdayRatePersent)
				.append(sundayRatePersent, castOther.sundayRatePersent)
				.append(minRange1Men, castOther.minRange1Men)
				.append(maxRange1Men, castOther.maxRange1Men)
				.append(minRange2Men, castOther.minRange2Men)
				.append(maxRange2Men, castOther.maxRange2Men)
				.append(minRange3Men, castOther.minRange3Men)
				.append(maxRange3Men, castOther.maxRange3Men)
				.append(minRange4Men, castOther.minRange4Men)
				.append(maxRange4Men, castOther.maxRange4Men)
				.append(minRange5Men, castOther.minRange5Men)
				.append(maxRange5Men, castOther.maxRange5Men)
				.append(minRange6Men, castOther.minRange6Men)
				.append(maxRange6Men, castOther.maxRange6Men)
				.append(deviationRange1forLessThan7Days,
						castOther.deviationRange1forLessThan7Days)
				.append(deviationRange2forLessThan7Days,
						castOther.deviationRange2forLessThan7Days)
				.append(deviationRange3forLessThan7Days,
						castOther.deviationRange3forLessThan7Days)
				.append(deviationRange4forLessThan7Days,
						castOther.deviationRange4forLessThan7Days)
				.append(deviationRange5forLessThan7Days,
						castOther.deviationRange5forLessThan7Days)
				.append(deviationRange6forLessThan7Days,
						castOther.deviationRange6forLessThan7Days)
				.append(deviationRange1forBetween7to13Days,
						castOther.deviationRange1forBetween7to13Days)
				.append(deviationRange2forBetween7to13Days,
						castOther.deviationRange2forBetween7to13Days)
				.append(deviationRange3forBetween7to13Days,
						castOther.deviationRange3forBetween7to13Days)
				.append(deviationRange4forBetween7to13Days,
						castOther.deviationRange4forBetween7to13Days)
				.append(deviationRange5forBetween7to13Days,
						castOther.deviationRange5forBetween7to13Days)
				.append(deviationRange6forBetween7to13Days,
						castOther.deviationRange6forBetween7to13Days)
				.append(deviationRange1forBetween14to21Days,
						castOther.deviationRange1forBetween14to21Days)
				.append(deviationRange2forBetween14to21Days,
						castOther.deviationRange2forBetween14to21Days)
				.append(deviationRange3forBetween14to21Days,
						castOther.deviationRange3forBetween14to21Days)
				.append(deviationRange4forBetween14to21Days,
						castOther.deviationRange4forBetween14to21Days)
				.append(deviationRange5forBetween14to21Days,
						castOther.deviationRange5forBetween14to21Days)
				.append(deviationRange6forBetween14to21Days,
						castOther.deviationRange6forBetween14to21Days)
				.append(deviationRange1forMoreThan21Days,
						castOther.deviationRange1forMoreThan21Days)
				.append(deviationRange2forMoreThan21Days,
						castOther.deviationRange2forMoreThan21Days)
				.append(deviationRange3forMoreThan21Days,
						castOther.deviationRange3forMoreThan21Days)
				.append(deviationRange4forMoreThan21Days,
						castOther.deviationRange4forMoreThan21Days)
				.append(deviationRange5forMoreThan21Days,
						castOther.deviationRange5forMoreThan21Days)
				.append(deviationRange6forMoreThan21Days,
						castOther.deviationRange6forMoreThan21Days)
				.append(regularRatePer2MenRange1forMoreThan21Days,
						castOther.regularRatePer2MenRange1forMoreThan21Days)
				.append(regularRatePer3MenRange1forMoreThan21Days,
						castOther.regularRatePer3MenRange1forMoreThan21Days)
				.append(regularRatePer4MenRange1forMoreThan21Days,
						castOther.regularRatePer4MenRange1forMoreThan21Days)
				.append(regularRatePer2MenRange2forMoreThan21Days,
						castOther.regularRatePer2MenRange2forMoreThan21Days)
				.append(regularRatePer3MenRange2forMoreThan21Days,
						castOther.regularRatePer3MenRange2forMoreThan21Days)
				.append(regularRatePer4MenRange2forMoreThan21Days,
						castOther.regularRatePer4MenRange2forMoreThan21Days)
				.append(regularRatePer2MenRange3forMoreThan21Days,
						castOther.regularRatePer2MenRange3forMoreThan21Days)
				.append(regularRatePer3MenRange3forMoreThan21Days,
						castOther.regularRatePer3MenRange3forMoreThan21Days)
				.append(regularRatePer4MenRange3forMoreThan21Days,
						castOther.regularRatePer4MenRange3forMoreThan21Days)
				.append(regularRatePer2MenRange4forMoreThan21Days,
						castOther.regularRatePer2MenRange4forMoreThan21Days)
				.append(regularRatePer3MenRange4forMoreThan21Days,
						castOther.regularRatePer3MenRange4forMoreThan21Days)
				.append(regularRatePer4MenRange4forMoreThan21Days,
						castOther.regularRatePer4MenRange4forMoreThan21Days)
				.append(regularRatePer2MenRange5forMoreThan21Days,
						castOther.regularRatePer2MenRange5forMoreThan21Days)
				.append(regularRatePer3MenRange5forMoreThan21Days,
						castOther.regularRatePer3MenRange5forMoreThan21Days)
				.append(regularRatePer4MenRange5forMoreThan21Days,
						castOther.regularRatePer4MenRange5forMoreThan21Days)
				.append(regularRatePer2MenRange6forMoreThan21Days,
						castOther.regularRatePer2MenRange6forMoreThan21Days)
				.append(regularRatePer3MenRange6forMoreThan21Days,
						castOther.regularRatePer3MenRange6forMoreThan21Days)
				.append(regularRatePer4MenRange6forMoreThan21Days,
						castOther.regularRatePer4MenRange6forMoreThan21Days)
				.append(monthEndRatePer2MenRange1forMoreThan21Days,
						castOther.monthEndRatePer2MenRange1forMoreThan21Days)
				.append(monthEndRatePer3MenRange1forMoreThan21Days,
						castOther.monthEndRatePer3MenRange1forMoreThan21Days)
				.append(monthEndRatePer4MenRange1forMoreThan21Days,
						castOther.monthEndRatePer4MenRange1forMoreThan21Days)
				.append(monthEndRatePer2MenRange2forMoreThan21Days,
						castOther.monthEndRatePer2MenRange2forMoreThan21Days)
				.append(monthEndRatePer3MenRange2forMoreThan21Days,
						castOther.monthEndRatePer3MenRange2forMoreThan21Days)
				.append(monthEndRatePer4MenRange2forMoreThan21Days,
						castOther.monthEndRatePer4MenRange2forMoreThan21Days)
				.append(monthEndRatePer2MenRange3forMoreThan21Days,
						castOther.monthEndRatePer2MenRange3forMoreThan21Days)
				.append(monthEndRatePer3MenRange3forMoreThan21Days,
						castOther.monthEndRatePer3MenRange3forMoreThan21Days)
				.append(monthEndRatePer4MenRange3forMoreThan21Days,
						castOther.monthEndRatePer4MenRange3forMoreThan21Days)
				.append(monthEndRatePer2MenRange4forMoreThan21Days,
						castOther.monthEndRatePer2MenRange4forMoreThan21Days)
				.append(monthEndRatePer3MenRange4forMoreThan21Days,
						castOther.monthEndRatePer3MenRange4forMoreThan21Days)
				.append(monthEndRatePer4MenRange4forMoreThan21Days,
						castOther.monthEndRatePer4MenRange4forMoreThan21Days)
				.append(monthEndRatePer2MenRange5forMoreThan21Days,
						castOther.monthEndRatePer2MenRange5forMoreThan21Days)
				.append(monthEndRatePer3MenRange5forMoreThan21Days,
						castOther.monthEndRatePer3MenRange5forMoreThan21Days)
				.append(monthEndRatePer4MenRange5forMoreThan21Days,
						castOther.monthEndRatePer4MenRange5forMoreThan21Days)
				.append(monthEndRatePer2MenRange6forMoreThan21Days,
						castOther.monthEndRatePer2MenRange6forMoreThan21Days)
				.append(monthEndRatePer3MenRange6forMoreThan21Days,
						castOther.monthEndRatePer3MenRange6forMoreThan21Days)
				.append(monthEndRatePer4MenRange6forMoreThan21Days,
						castOther.monthEndRatePer4MenRange6forMoreThan21Days)
				.append(regularRatePer2MenRange1forBetween14to21Days,
						castOther.regularRatePer2MenRange1forBetween14to21Days)
				.append(regularRatePer3MenRange1forBetween14to21Days,
						castOther.regularRatePer3MenRange1forBetween14to21Days)
				.append(regularRatePer4MenRange1forBetween14to21Days,
						castOther.regularRatePer4MenRange1forBetween14to21Days)
				.append(regularRatePer2MenRange2forBetween14to21Days,
						castOther.regularRatePer2MenRange2forBetween14to21Days)
				.append(regularRatePer3MenRange2forBetween14to21Days,
						castOther.regularRatePer3MenRange2forBetween14to21Days)
				.append(regularRatePer4MenRange2forBetween14to21Days,
						castOther.regularRatePer4MenRange2forBetween14to21Days)
				.append(regularRatePer2MenRange3forBetween14to21Days,
						castOther.regularRatePer2MenRange3forBetween14to21Days)
				.append(regularRatePer3MenRange3forBetween14to21Days,
						castOther.regularRatePer3MenRange3forBetween14to21Days)
				.append(regularRatePer4MenRange3forBetween14to21Days,
						castOther.regularRatePer4MenRange3forBetween14to21Days)
				.append(regularRatePer2MenRange4forBetween14to21Days,
						castOther.regularRatePer2MenRange4forBetween14to21Days)
				.append(regularRatePer3MenRange4forBetween14to21Days,
						castOther.regularRatePer3MenRange4forBetween14to21Days)
				.append(regularRatePer4MenRange4forBetween14to21Days,
						castOther.regularRatePer4MenRange4forBetween14to21Days)
				.append(regularRatePer2MenRange5forBetween14to21Days,
						castOther.regularRatePer2MenRange5forBetween14to21Days)
				.append(regularRatePer3MenRange5forBetween14to21Days,
						castOther.regularRatePer3MenRange5forBetween14to21Days)
				.append(regularRatePer4MenRange5forBetween14to21Days,
						castOther.regularRatePer4MenRange5forBetween14to21Days)
				.append(regularRatePer2MenRange6forBetween14to21Days,
						castOther.regularRatePer2MenRange6forBetween14to21Days)
				.append(regularRatePer3MenRange6forBetween14to21Days,
						castOther.regularRatePer3MenRange6forBetween14to21Days)
				.append(regularRatePer4MenRange6forBetween14to21Days,
						castOther.regularRatePer4MenRange6forBetween14to21Days)
				.append(monthEndRatePer2MenRange1forBetween14to21Days,
						castOther.monthEndRatePer2MenRange1forBetween14to21Days)
				.append(monthEndRatePer3MenRange1forBetween14to21Days,
						castOther.monthEndRatePer3MenRange1forBetween14to21Days)
				.append(monthEndRatePer4MenRange1forBetween14to21Days,
						castOther.monthEndRatePer4MenRange1forBetween14to21Days)
				.append(monthEndRatePer2MenRange2forBetween14to21Days,
						castOther.monthEndRatePer2MenRange2forBetween14to21Days)
				.append(monthEndRatePer3MenRange2forBetween14to21Days,
						castOther.monthEndRatePer3MenRange2forBetween14to21Days)
				.append(monthEndRatePer4MenRange2forBetween14to21Days,
						castOther.monthEndRatePer4MenRange2forBetween14to21Days)
				.append(monthEndRatePer2MenRange3forBetween14to21Days,
						castOther.monthEndRatePer2MenRange3forBetween14to21Days)
				.append(monthEndRatePer3MenRange3forBetween14to21Days,
						castOther.monthEndRatePer3MenRange3forBetween14to21Days)
				.append(monthEndRatePer4MenRange3forBetween14to21Days,
						castOther.monthEndRatePer4MenRange3forBetween14to21Days)
				.append(monthEndRatePer2MenRange4forBetween14to21Days,
						castOther.monthEndRatePer2MenRange4forBetween14to21Days)
				.append(monthEndRatePer3MenRange4forBetween14to21Days,
						castOther.monthEndRatePer3MenRange4forBetween14to21Days)
				.append(monthEndRatePer4MenRange4forBetween14to21Days,
						castOther.monthEndRatePer4MenRange4forBetween14to21Days)
				.append(monthEndRatePer2MenRange5forBetween14to21Days,
						castOther.monthEndRatePer2MenRange5forBetween14to21Days)
				.append(monthEndRatePer3MenRange5forBetween14to21Days,
						castOther.monthEndRatePer3MenRange5forBetween14to21Days)
				.append(monthEndRatePer4MenRange5forBetween14to21Days,
						castOther.monthEndRatePer4MenRange5forBetween14to21Days)
				.append(monthEndRatePer2MenRange6forBetween14to21Days,
						castOther.monthEndRatePer2MenRange6forBetween14to21Days)
				.append(monthEndRatePer3MenRange6forBetween14to21Days,
						castOther.monthEndRatePer3MenRange6forBetween14to21Days)
				.append(monthEndRatePer4MenRange6forBetween14to21Days,
						castOther.monthEndRatePer4MenRange6forBetween14to21Days)
				.append(regularRatePer2MenRange1forBetween7to13Days,
						castOther.regularRatePer2MenRange1forBetween7to13Days)
				.append(regularRatePer3MenRange1forBetween7to13Days,
						castOther.regularRatePer3MenRange1forBetween7to13Days)
				.append(regularRatePer4MenRange1forBetween7to13Days,
						castOther.regularRatePer4MenRange1forBetween7to13Days)
				.append(regularRatePer2MenRange2forBetween7to13Days,
						castOther.regularRatePer2MenRange2forBetween7to13Days)
				.append(regularRatePer3MenRange2forBetween7to13Days,
						castOther.regularRatePer3MenRange2forBetween7to13Days)
				.append(regularRatePer4MenRange2forBetween7to13Days,
						castOther.regularRatePer4MenRange2forBetween7to13Days)
				.append(regularRatePer2MenRange3forBetween7to13Days,
						castOther.regularRatePer2MenRange3forBetween7to13Days)
				.append(regularRatePer3MenRange3forBetween7to13Days,
						castOther.regularRatePer3MenRange3forBetween7to13Days)
				.append(regularRatePer4MenRange3forBetween7to13Days,
						castOther.regularRatePer4MenRange3forBetween7to13Days)
				.append(regularRatePer2MenRange4forBetween7to13Days,
						castOther.regularRatePer2MenRange4forBetween7to13Days)
				.append(regularRatePer3MenRange4forBetween7to13Days,
						castOther.regularRatePer3MenRange4forBetween7to13Days)
				.append(regularRatePer4MenRange4forBetween7to13Days,
						castOther.regularRatePer4MenRange4forBetween7to13Days)
				.append(regularRatePer2MenRange5forBetween7to13Days,
						castOther.regularRatePer2MenRange5forBetween7to13Days)
				.append(regularRatePer3MenRange5forBetween7to13Days,
						castOther.regularRatePer3MenRange5forBetween7to13Days)
				.append(regularRatePer4MenRange5forBetween7to13Days,
						castOther.regularRatePer4MenRange5forBetween7to13Days)
				.append(regularRatePer2MenRange6forBetween7to13Days,
						castOther.regularRatePer2MenRange6forBetween7to13Days)
				.append(regularRatePer3MenRange6forBetween7to13Days,
						castOther.regularRatePer3MenRange6forBetween7to13Days)
				.append(regularRatePer4MenRange6forBetween7to13Days,
						castOther.regularRatePer4MenRange6forBetween7to13Days)
				.append(monthEndRatePer2MenRange1forBetween7to13Days,
						castOther.monthEndRatePer2MenRange1forBetween7to13Days)
				.append(monthEndRatePer3MenRange1forBetween7to13Days,
						castOther.monthEndRatePer3MenRange1forBetween7to13Days)
				.append(monthEndRatePer4MenRange1forBetween7to13Days,
						castOther.monthEndRatePer4MenRange1forBetween7to13Days)
				.append(monthEndRatePer2MenRange2forBetween7to13Days,
						castOther.monthEndRatePer2MenRange2forBetween7to13Days)
				.append(monthEndRatePer3MenRange2forBetween7to13Days,
						castOther.monthEndRatePer3MenRange2forBetween7to13Days)
				.append(monthEndRatePer4MenRange2forBetween7to13Days,
						castOther.monthEndRatePer4MenRange2forBetween7to13Days)
				.append(monthEndRatePer2MenRange3forBetween7to13Days,
						castOther.monthEndRatePer2MenRange3forBetween7to13Days)
				.append(monthEndRatePer3MenRange3forBetween7to13Days,
						castOther.monthEndRatePer3MenRange3forBetween7to13Days)
				.append(monthEndRatePer4MenRange3forBetween7to13Days,
						castOther.monthEndRatePer4MenRange3forBetween7to13Days)
				.append(monthEndRatePer2MenRange4forBetween7to13Days,
						castOther.monthEndRatePer2MenRange4forBetween7to13Days)
				.append(monthEndRatePer3MenRange4forBetween7to13Days,
						castOther.monthEndRatePer3MenRange4forBetween7to13Days)
				.append(monthEndRatePer4MenRange4forBetween7to13Days,
						castOther.monthEndRatePer4MenRange4forBetween7to13Days)
				.append(monthEndRatePer2MenRange5forBetween7to13Days,
						castOther.monthEndRatePer2MenRange5forBetween7to13Days)
				.append(monthEndRatePer3MenRange5forBetween7to13Days,
						castOther.monthEndRatePer3MenRange5forBetween7to13Days)
				.append(monthEndRatePer4MenRange5forBetween7to13Days,
						castOther.monthEndRatePer4MenRange5forBetween7to13Days)
				.append(monthEndRatePer2MenRange6forBetween7to13Days,
						castOther.monthEndRatePer2MenRange6forBetween7to13Days)
				.append(monthEndRatePer3MenRange6forBetween7to13Days,
						castOther.monthEndRatePer3MenRange6forBetween7to13Days)
				.append(monthEndRatePer4MenRange6forBetween7to13Days,
						castOther.monthEndRatePer4MenRange6forBetween7to13Days)
				.append(regularRatePer2MenRange1forLessThan7Days,
						castOther.regularRatePer2MenRange1forLessThan7Days)
				.append(regularRatePer3MenRange1forLessThan7Days,
						castOther.regularRatePer3MenRange1forLessThan7Days)
				.append(regularRatePer4MenRange1forLessThan7Days,
						castOther.regularRatePer4MenRange1forLessThan7Days)
				.append(regularRatePer2MenRange2forLessThan7Days,
						castOther.regularRatePer2MenRange2forLessThan7Days)
				.append(regularRatePer3MenRange2forLessThan7Days,
						castOther.regularRatePer3MenRange2forLessThan7Days)
				.append(regularRatePer4MenRange2forLessThan7Days,
						castOther.regularRatePer4MenRange2forLessThan7Days)
				.append(regularRatePer2MenRange3forLessThan7Days,
						castOther.regularRatePer2MenRange3forLessThan7Days)
				.append(regularRatePer3MenRange3forLessThan7Days,
						castOther.regularRatePer3MenRange3forLessThan7Days)
				.append(regularRatePer4MenRange3forLessThan7Days,
						castOther.regularRatePer4MenRange3forLessThan7Days)
				.append(regularRatePer2MenRange4forLessThan7Days,
						castOther.regularRatePer2MenRange4forLessThan7Days)
				.append(regularRatePer3MenRange4forLessThan7Days,
						castOther.regularRatePer3MenRange4forLessThan7Days)
				.append(regularRatePer4MenRange4forLessThan7Days,
						castOther.regularRatePer4MenRange4forLessThan7Days)
				.append(regularRatePer2MenRange5forLessThan7Days,
						castOther.regularRatePer2MenRange5forLessThan7Days)
				.append(regularRatePer3MenRange5forLessThan7Days,
						castOther.regularRatePer3MenRange5forLessThan7Days)
				.append(regularRatePer4MenRange5forLessThan7Days,
						castOther.regularRatePer4MenRange5forLessThan7Days)
				.append(regularRatePer2MenRange6forLessThan7Days,
						castOther.regularRatePer2MenRange6forLessThan7Days)
				.append(regularRatePer3MenRange6forLessThan7Days,
						castOther.regularRatePer3MenRange6forLessThan7Days)
				.append(regularRatePer4MenRange6forLessThan7Days,
						castOther.regularRatePer4MenRange6forLessThan7Days)
				.append(monthEndRatePer2MenRange1forLessThan7Days,
						castOther.monthEndRatePer2MenRange1forLessThan7Days)
				.append(monthEndRatePer3MenRange1forLessThan7Days,
						castOther.monthEndRatePer3MenRange1forLessThan7Days)
				.append(monthEndRatePer4MenRange1forLessThan7Days,
						castOther.monthEndRatePer4MenRange1forLessThan7Days)
				.append(monthEndRatePer2MenRange2forLessThan7Days,
						castOther.monthEndRatePer2MenRange2forLessThan7Days)
				.append(monthEndRatePer3MenRange2forLessThan7Days,
						castOther.monthEndRatePer3MenRange2forLessThan7Days)
				.append(monthEndRatePer4MenRange2forLessThan7Days,
						castOther.monthEndRatePer4MenRange2forLessThan7Days)
				.append(monthEndRatePer2MenRange3forLessThan7Days,
						castOther.monthEndRatePer2MenRange3forLessThan7Days)
				.append(monthEndRatePer3MenRange3forLessThan7Days,
						castOther.monthEndRatePer3MenRange3forLessThan7Days)
				.append(monthEndRatePer4MenRange3forLessThan7Days,
						castOther.monthEndRatePer4MenRange3forLessThan7Days)
				.append(monthEndRatePer2MenRange4forLessThan7Days,
						castOther.monthEndRatePer2MenRange4forLessThan7Days)
				.append(monthEndRatePer3MenRange4forLessThan7Days,
						castOther.monthEndRatePer3MenRange4forLessThan7Days)
				.append(monthEndRatePer4MenRange4forLessThan7Days,
						castOther.monthEndRatePer4MenRange4forLessThan7Days)
				.append(monthEndRatePer2MenRange5forLessThan7Days,
						castOther.monthEndRatePer2MenRange5forLessThan7Days)
				.append(monthEndRatePer3MenRange5forLessThan7Days,
						castOther.monthEndRatePer3MenRange5forLessThan7Days)
				.append(monthEndRatePer4MenRange5forLessThan7Days,
						castOther.monthEndRatePer4MenRange5forLessThan7Days)
				.append(monthEndRatePer2MenRange6forLessThan7Days,
						castOther.monthEndRatePer2MenRange6forLessThan7Days)
				.append(monthEndRatePer3MenRange6forLessThan7Days,
						castOther.monthEndRatePer3MenRange6forLessThan7Days)
				.append(monthEndRatePer4MenRange6forLessThan7Days,
						castOther.monthEndRatePer4MenRange6forLessThan7Days)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(updatedBy).append(createdBy).append(updatedOn)
				.append(createdOn).append(dynamicPricingType)
				.append(regularHourlyRatePer2Men)
				.append(regularHourlyRatePer3Men)
				.append(regularHourlyRatePer4Men)
				.append(monthEndHourlyRatePer2Men)
				.append(monthEndHourlyRatePer3Men)
				.append(monthEndHourlyRatePer4Men).append(extraMenRate)
				.append(noOfEmployees).append(saturdayRatePersent)
				.append(sundayRatePersent).append(minRange1Men)
				.append(maxRange1Men).append(minRange2Men).append(maxRange2Men)
				.append(minRange3Men).append(maxRange3Men).append(minRange4Men)
				.append(maxRange4Men).append(minRange5Men).append(maxRange5Men)
				.append(minRange6Men).append(maxRange6Men)
				.append(deviationRange1forLessThan7Days)
				.append(deviationRange2forLessThan7Days)
				.append(deviationRange3forLessThan7Days)
				.append(deviationRange4forLessThan7Days)
				.append(deviationRange5forLessThan7Days)
				.append(deviationRange6forLessThan7Days)
				.append(deviationRange1forBetween7to13Days)
				.append(deviationRange2forBetween7to13Days)
				.append(deviationRange3forBetween7to13Days)
				.append(deviationRange4forBetween7to13Days)
				.append(deviationRange5forBetween7to13Days)
				.append(deviationRange6forBetween7to13Days)
				.append(deviationRange1forBetween14to21Days)
				.append(deviationRange2forBetween14to21Days)
				.append(deviationRange3forBetween14to21Days)
				.append(deviationRange4forBetween14to21Days)
				.append(deviationRange5forBetween14to21Days)
				.append(deviationRange6forBetween14to21Days)
				.append(deviationRange1forMoreThan21Days)
				.append(deviationRange2forMoreThan21Days)
				.append(deviationRange3forMoreThan21Days)
				.append(deviationRange4forMoreThan21Days)
				.append(deviationRange5forMoreThan21Days)
				.append(deviationRange6forMoreThan21Days)
				.append(regularRatePer2MenRange1forMoreThan21Days)
				.append(regularRatePer3MenRange1forMoreThan21Days)
				.append(regularRatePer4MenRange1forMoreThan21Days)
				.append(regularRatePer2MenRange2forMoreThan21Days)
				.append(regularRatePer3MenRange2forMoreThan21Days)
				.append(regularRatePer4MenRange2forMoreThan21Days)
				.append(regularRatePer2MenRange3forMoreThan21Days)
				.append(regularRatePer3MenRange3forMoreThan21Days)
				.append(regularRatePer4MenRange3forMoreThan21Days)
				.append(regularRatePer2MenRange4forMoreThan21Days)
				.append(regularRatePer3MenRange4forMoreThan21Days)
				.append(regularRatePer4MenRange4forMoreThan21Days)
				.append(regularRatePer2MenRange5forMoreThan21Days)
				.append(regularRatePer3MenRange5forMoreThan21Days)
				.append(regularRatePer4MenRange5forMoreThan21Days)
				.append(regularRatePer2MenRange6forMoreThan21Days)
				.append(regularRatePer3MenRange6forMoreThan21Days)
				.append(regularRatePer4MenRange6forMoreThan21Days)
				.append(monthEndRatePer2MenRange1forMoreThan21Days)
				.append(monthEndRatePer3MenRange1forMoreThan21Days)
				.append(monthEndRatePer4MenRange1forMoreThan21Days)
				.append(monthEndRatePer2MenRange2forMoreThan21Days)
				.append(monthEndRatePer3MenRange2forMoreThan21Days)
				.append(monthEndRatePer4MenRange2forMoreThan21Days)
				.append(monthEndRatePer2MenRange3forMoreThan21Days)
				.append(monthEndRatePer3MenRange3forMoreThan21Days)
				.append(monthEndRatePer4MenRange3forMoreThan21Days)
				.append(monthEndRatePer2MenRange4forMoreThan21Days)
				.append(monthEndRatePer3MenRange4forMoreThan21Days)
				.append(monthEndRatePer4MenRange4forMoreThan21Days)
				.append(monthEndRatePer2MenRange5forMoreThan21Days)
				.append(monthEndRatePer3MenRange5forMoreThan21Days)
				.append(monthEndRatePer4MenRange5forMoreThan21Days)
				.append(monthEndRatePer2MenRange6forMoreThan21Days)
				.append(monthEndRatePer3MenRange6forMoreThan21Days)
				.append(monthEndRatePer4MenRange6forMoreThan21Days)
				.append(regularRatePer2MenRange1forBetween14to21Days)
				.append(regularRatePer3MenRange1forBetween14to21Days)
				.append(regularRatePer4MenRange1forBetween14to21Days)
				.append(regularRatePer2MenRange2forBetween14to21Days)
				.append(regularRatePer3MenRange2forBetween14to21Days)
				.append(regularRatePer4MenRange2forBetween14to21Days)
				.append(regularRatePer2MenRange3forBetween14to21Days)
				.append(regularRatePer3MenRange3forBetween14to21Days)
				.append(regularRatePer4MenRange3forBetween14to21Days)
				.append(regularRatePer2MenRange4forBetween14to21Days)
				.append(regularRatePer3MenRange4forBetween14to21Days)
				.append(regularRatePer4MenRange4forBetween14to21Days)
				.append(regularRatePer2MenRange5forBetween14to21Days)
				.append(regularRatePer3MenRange5forBetween14to21Days)
				.append(regularRatePer4MenRange5forBetween14to21Days)
				.append(regularRatePer2MenRange6forBetween14to21Days)
				.append(regularRatePer3MenRange6forBetween14to21Days)
				.append(regularRatePer4MenRange6forBetween14to21Days)
				.append(monthEndRatePer2MenRange1forBetween14to21Days)
				.append(monthEndRatePer3MenRange1forBetween14to21Days)
				.append(monthEndRatePer4MenRange1forBetween14to21Days)
				.append(monthEndRatePer2MenRange2forBetween14to21Days)
				.append(monthEndRatePer3MenRange2forBetween14to21Days)
				.append(monthEndRatePer4MenRange2forBetween14to21Days)
				.append(monthEndRatePer2MenRange3forBetween14to21Days)
				.append(monthEndRatePer3MenRange3forBetween14to21Days)
				.append(monthEndRatePer4MenRange3forBetween14to21Days)
				.append(monthEndRatePer2MenRange4forBetween14to21Days)
				.append(monthEndRatePer3MenRange4forBetween14to21Days)
				.append(monthEndRatePer4MenRange4forBetween14to21Days)
				.append(monthEndRatePer2MenRange5forBetween14to21Days)
				.append(monthEndRatePer3MenRange5forBetween14to21Days)
				.append(monthEndRatePer4MenRange5forBetween14to21Days)
				.append(monthEndRatePer2MenRange6forBetween14to21Days)
				.append(monthEndRatePer3MenRange6forBetween14to21Days)
				.append(monthEndRatePer4MenRange6forBetween14to21Days)
				.append(regularRatePer2MenRange1forBetween7to13Days)
				.append(regularRatePer3MenRange1forBetween7to13Days)
				.append(regularRatePer4MenRange1forBetween7to13Days)
				.append(regularRatePer2MenRange2forBetween7to13Days)
				.append(regularRatePer3MenRange2forBetween7to13Days)
				.append(regularRatePer4MenRange2forBetween7to13Days)
				.append(regularRatePer2MenRange3forBetween7to13Days)
				.append(regularRatePer3MenRange3forBetween7to13Days)
				.append(regularRatePer4MenRange3forBetween7to13Days)
				.append(regularRatePer2MenRange4forBetween7to13Days)
				.append(regularRatePer3MenRange4forBetween7to13Days)
				.append(regularRatePer4MenRange4forBetween7to13Days)
				.append(regularRatePer2MenRange5forBetween7to13Days)
				.append(regularRatePer3MenRange5forBetween7to13Days)
				.append(regularRatePer4MenRange5forBetween7to13Days)
				.append(regularRatePer2MenRange6forBetween7to13Days)
				.append(regularRatePer3MenRange6forBetween7to13Days)
				.append(regularRatePer4MenRange6forBetween7to13Days)
				.append(monthEndRatePer2MenRange1forBetween7to13Days)
				.append(monthEndRatePer3MenRange1forBetween7to13Days)
				.append(monthEndRatePer4MenRange1forBetween7to13Days)
				.append(monthEndRatePer2MenRange2forBetween7to13Days)
				.append(monthEndRatePer3MenRange2forBetween7to13Days)
				.append(monthEndRatePer4MenRange2forBetween7to13Days)
				.append(monthEndRatePer2MenRange3forBetween7to13Days)
				.append(monthEndRatePer3MenRange3forBetween7to13Days)
				.append(monthEndRatePer4MenRange3forBetween7to13Days)
				.append(monthEndRatePer2MenRange4forBetween7to13Days)
				.append(monthEndRatePer3MenRange4forBetween7to13Days)
				.append(monthEndRatePer4MenRange4forBetween7to13Days)
				.append(monthEndRatePer2MenRange5forBetween7to13Days)
				.append(monthEndRatePer3MenRange5forBetween7to13Days)
				.append(monthEndRatePer4MenRange5forBetween7to13Days)
				.append(monthEndRatePer2MenRange6forBetween7to13Days)
				.append(monthEndRatePer3MenRange6forBetween7to13Days)
				.append(monthEndRatePer4MenRange6forBetween7to13Days)
				.append(regularRatePer2MenRange1forLessThan7Days)
				.append(regularRatePer3MenRange1forLessThan7Days)
				.append(regularRatePer4MenRange1forLessThan7Days)
				.append(regularRatePer2MenRange2forLessThan7Days)
				.append(regularRatePer3MenRange2forLessThan7Days)
				.append(regularRatePer4MenRange2forLessThan7Days)
				.append(regularRatePer2MenRange3forLessThan7Days)
				.append(regularRatePer3MenRange3forLessThan7Days)
				.append(regularRatePer4MenRange3forLessThan7Days)
				.append(regularRatePer2MenRange4forLessThan7Days)
				.append(regularRatePer3MenRange4forLessThan7Days)
				.append(regularRatePer4MenRange4forLessThan7Days)
				.append(regularRatePer2MenRange5forLessThan7Days)
				.append(regularRatePer3MenRange5forLessThan7Days)
				.append(regularRatePer4MenRange5forLessThan7Days)
				.append(regularRatePer2MenRange6forLessThan7Days)
				.append(regularRatePer3MenRange6forLessThan7Days)
				.append(regularRatePer4MenRange6forLessThan7Days)
				.append(monthEndRatePer2MenRange1forLessThan7Days)
				.append(monthEndRatePer3MenRange1forLessThan7Days)
				.append(monthEndRatePer4MenRange1forLessThan7Days)
				.append(monthEndRatePer2MenRange2forLessThan7Days)
				.append(monthEndRatePer3MenRange2forLessThan7Days)
				.append(monthEndRatePer4MenRange2forLessThan7Days)
				.append(monthEndRatePer2MenRange3forLessThan7Days)
				.append(monthEndRatePer3MenRange3forLessThan7Days)
				.append(monthEndRatePer4MenRange3forLessThan7Days)
				.append(monthEndRatePer2MenRange4forLessThan7Days)
				.append(monthEndRatePer3MenRange4forLessThan7Days)
				.append(monthEndRatePer4MenRange4forLessThan7Days)
				.append(monthEndRatePer2MenRange5forLessThan7Days)
				.append(monthEndRatePer3MenRange5forLessThan7Days)
				.append(monthEndRatePer4MenRange5forLessThan7Days)
				.append(monthEndRatePer2MenRange6forLessThan7Days)
				.append(monthEndRatePer3MenRange6forLessThan7Days)
				.append(monthEndRatePer4MenRange6forLessThan7Days).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getDynamicPricingType() {
		return dynamicPricingType;
	}
	public void setDynamicPricingType(String dynamicPricingType) {
		this.dynamicPricingType = dynamicPricingType;
	}
	@Column
public Double getRegularHourlyRatePer2Men() {
		return regularHourlyRatePer2Men;
	}
	public void setRegularHourlyRatePer2Men(Double regularHourlyRatePer2Men) {
		this.regularHourlyRatePer2Men = regularHourlyRatePer2Men;
	}
	@Column
public Double getRegularHourlyRatePer3Men() {
		return regularHourlyRatePer3Men;
	}
	public void setRegularHourlyRatePer3Men(Double regularHourlyRatePer3Men) {
		this.regularHourlyRatePer3Men = regularHourlyRatePer3Men;
	}
	@Column
public Double getRegularHourlyRatePer4Men() {
		return regularHourlyRatePer4Men;
	}
	public void setRegularHourlyRatePer4Men(Double regularHourlyRatePer4Men) {
		this.regularHourlyRatePer4Men = regularHourlyRatePer4Men;
	}
	@Column
public Double getMonthEndHourlyRatePer2Men() {
		return monthEndHourlyRatePer2Men;
	}
	public void setMonthEndHourlyRatePer2Men(Double monthEndHourlyRatePer2Men) {
		this.monthEndHourlyRatePer2Men = monthEndHourlyRatePer2Men;
	}
	@Column
public Double getMonthEndHourlyRatePer3Men() {
		return monthEndHourlyRatePer3Men;
	}
	public void setMonthEndHourlyRatePer3Men(Double monthEndHourlyRatePer3Men) {
		this.monthEndHourlyRatePer3Men = monthEndHourlyRatePer3Men;
	}
	@Column
	public Double getMonthEndHourlyRatePer4Men() {
		return monthEndHourlyRatePer4Men;
	}
	public void setMonthEndHourlyRatePer4Men(Double monthEndHourlyRatePer4Men) {
		this.monthEndHourlyRatePer4Men = monthEndHourlyRatePer4Men;
	}
	@Column
public Double getExtraMenRate() {
		return extraMenRate;
	}
	public void setExtraMenRate(Double extraMenRate) {
		this.extraMenRate = extraMenRate;
	}
	@Column
	public Integer getNoOfEmployees() {
		return noOfEmployees;
	}
	public void setNoOfEmployees(Integer noOfEmployees) {
		this.noOfEmployees = noOfEmployees;
	}
	@Column
public Double getSaturdayRatePersent() {
		return saturdayRatePersent;
	}
	public void setSaturdayRatePersent(Double saturdayRatePersent) {
		this.saturdayRatePersent = saturdayRatePersent;
	}
	@Column
public Double getSundayRatePersent() {
		return sundayRatePersent;
	}
	public void setSundayRatePersent(Double sundayRatePersent) {
		this.sundayRatePersent = sundayRatePersent;
	}
	@Column
	public Integer getMinRange1Men() {
		return minRange1Men;
	}
	public void setMinRange1Men(Integer minRange1Men) {
		this.minRange1Men = minRange1Men;
	}
	@Column
	public Integer getMaxRange1Men() {
		return maxRange1Men;
	}
	public void setMaxRange1Men(Integer maxRange1Men) {
		this.maxRange1Men = maxRange1Men;
	}
	@Column
	public Integer getMinRange2Men() {
		return minRange2Men;
	}
	public void setMinRange2Men(Integer minRange2Men) {
		this.minRange2Men = minRange2Men;
	}
	@Column
	public Integer getMaxRange2Men() {
		return maxRange2Men;
	}
	public void setMaxRange2Men(Integer maxRange2Men) {
		this.maxRange2Men = maxRange2Men;
	}
	@Column
	public Integer getMinRange3Men() {
		return minRange3Men;
	}
	public void setMinRange3Men(Integer minRange3Men) {
		this.minRange3Men = minRange3Men;
	}
	@Column
	public Integer getMaxRange3Men() {
		return maxRange3Men;
	}
	public void setMaxRange3Men(Integer maxRange3Men) {
		this.maxRange3Men = maxRange3Men;
	}
	@Column
	public Integer getMinRange4Men() {
		return minRange4Men;
	}
	public void setMinRange4Men(Integer minRange4Men) {
		this.minRange4Men = minRange4Men;
	}
	@Column
	public Integer getMaxRange4Men() {
		return maxRange4Men;
	}
	public void setMaxRange4Men(Integer maxRange4Men) {
		this.maxRange4Men = maxRange4Men;
	}
	@Column
	public Integer getMinRange5Men() {
		return minRange5Men;
	}
	public void setMinRange5Men(Integer minRange5Men) {
		this.minRange5Men = minRange5Men;
	}
	@Column
	public Integer getMaxRange5Men() {
		return maxRange5Men;
	}
	public void setMaxRange5Men(Integer maxRange5Men) {
		this.maxRange5Men = maxRange5Men;
	}
	@Column
	public Integer getMinRange6Men() {
		return minRange6Men;
	}
	public void setMinRange6Men(Integer minRange6Men) {
		this.minRange6Men = minRange6Men;
	}
	@Column
	public Integer getMaxRange6Men() {
		return maxRange6Men;
	}
	public void setMaxRange6Men(Integer maxRange6Men) {
		this.maxRange6Men = maxRange6Men;
	}
	@Column
	public Double getDeviationRange1forLessThan7Days() {
		return deviationRange1forLessThan7Days;
	}
	public void setDeviationRange1forLessThan7Days(
			Double deviationRange1forLessThan7Days) {
		this.deviationRange1forLessThan7Days = deviationRange1forLessThan7Days;
	}
	@Column
public Double getDeviationRange2forLessThan7Days() {
		return deviationRange2forLessThan7Days;
	}
	public void setDeviationRange2forLessThan7Days(
			Double deviationRange2forLessThan7Days) {
		this.deviationRange2forLessThan7Days = deviationRange2forLessThan7Days;
	}
	@Column
public Double getDeviationRange3forLessThan7Days() {
		return deviationRange3forLessThan7Days;
	}
	public void setDeviationRange3forLessThan7Days(
			Double deviationRange3forLessThan7Days) {
		this.deviationRange3forLessThan7Days = deviationRange3forLessThan7Days;
	}
	@Column
public Double getDeviationRange4forLessThan7Days() {
		return deviationRange4forLessThan7Days;
	}
	public void setDeviationRange4forLessThan7Days(
			Double deviationRange4forLessThan7Days) {
		this.deviationRange4forLessThan7Days = deviationRange4forLessThan7Days;
	}
	@Column
public Double getDeviationRange5forLessThan7Days() {
		return deviationRange5forLessThan7Days;
	}
	public void setDeviationRange5forLessThan7Days(
			Double deviationRange5forLessThan7Days) {
		this.deviationRange5forLessThan7Days = deviationRange5forLessThan7Days;
	}
	@Column
	public Double getDeviationRange6forLessThan7Days() {
		return deviationRange6forLessThan7Days;
	}
	public void setDeviationRange6forLessThan7Days(
			Double deviationRange6forLessThan7Days) {
		this.deviationRange6forLessThan7Days = deviationRange6forLessThan7Days;
	}
	@Column
public Double getDeviationRange1forBetween7to13Days() {
		return deviationRange1forBetween7to13Days;
	}
	public void setDeviationRange1forBetween7to13Days(
			Double deviationRange1forBetween7to13Days) {
		this.deviationRange1forBetween7to13Days = deviationRange1forBetween7to13Days;
	}
	@Column
public Double getDeviationRange2forBetween7to13Days() {
		return deviationRange2forBetween7to13Days;
	}
	public void setDeviationRange2forBetween7to13Days(
			Double deviationRange2forBetween7to13Days) {
		this.deviationRange2forBetween7to13Days = deviationRange2forBetween7to13Days;
	}
	@Column
public Double getDeviationRange3forBetween7to13Days() {
		return deviationRange3forBetween7to13Days;
	}
	public void setDeviationRange3forBetween7to13Days(
			Double deviationRange3forBetween7to13Days) {
		this.deviationRange3forBetween7to13Days = deviationRange3forBetween7to13Days;
	}
	@Column
public Double getDeviationRange4forBetween7to13Days() {
		return deviationRange4forBetween7to13Days;
	}
	public void setDeviationRange4forBetween7to13Days(
			Double deviationRange4forBetween7to13Days) {
		this.deviationRange4forBetween7to13Days = deviationRange4forBetween7to13Days;
	}
	@Column
public Double getDeviationRange5forBetween7to13Days() {
		return deviationRange5forBetween7to13Days;
	}
	public void setDeviationRange5forBetween7to13Days(
			Double deviationRange5forBetween7to13Days) {
		this.deviationRange5forBetween7to13Days = deviationRange5forBetween7to13Days;
	}
	@Column
public Double getDeviationRange6forBetween7to13Days() {
		return deviationRange6forBetween7to13Days;
	}
	public void setDeviationRange6forBetween7to13Days(
			Double deviationRange6forBetween7to13Days) {
		this.deviationRange6forBetween7to13Days = deviationRange6forBetween7to13Days;
	}
	@Column
public Double getDeviationRange1forBetween14to21Days() {
		return deviationRange1forBetween14to21Days;
	}
	public void setDeviationRange1forBetween14to21Days(
			Double deviationRange1forBetween14to21Days) {
		this.deviationRange1forBetween14to21Days = deviationRange1forBetween14to21Days;
	}
	@Column
	public Double getDeviationRange2forBetween14to21Days() {
		return deviationRange2forBetween14to21Days;
	}
	public void setDeviationRange2forBetween14to21Days(
			Double deviationRange2forBetween14to21Days) {
		this.deviationRange2forBetween14to21Days = deviationRange2forBetween14to21Days;
	}
	@Column
public Double getDeviationRange3forBetween14to21Days() {
		return deviationRange3forBetween14to21Days;
	}
	public void setDeviationRange3forBetween14to21Days(
			Double deviationRange3forBetween14to21Days) {
		this.deviationRange3forBetween14to21Days = deviationRange3forBetween14to21Days;
	}
	@Column
public Double getDeviationRange4forBetween14to21Days() {
		return deviationRange4forBetween14to21Days;
	}
	public void setDeviationRange4forBetween14to21Days(
			Double deviationRange4forBetween14to21Days) {
		this.deviationRange4forBetween14to21Days = deviationRange4forBetween14to21Days;
	}
	@Column
public Double getDeviationRange5forBetween14to21Days() {
		return deviationRange5forBetween14to21Days;
	}
	public void setDeviationRange5forBetween14to21Days(
			Double deviationRange5forBetween14to21Days) {
		this.deviationRange5forBetween14to21Days = deviationRange5forBetween14to21Days;
	}
	@Column
public Double getDeviationRange6forBetween14to21Days() {
		return deviationRange6forBetween14to21Days;
	}
	public void setDeviationRange6forBetween14to21Days(
			Double deviationRange6forBetween14to21Days) {
		this.deviationRange6forBetween14to21Days = deviationRange6forBetween14to21Days;
	}
	@Column
public Double getDeviationRange1forMoreThan21Days() {
		return deviationRange1forMoreThan21Days;
	}
	public void setDeviationRange1forMoreThan21Days(
			Double deviationRange1forMoreThan21Days) {
		this.deviationRange1forMoreThan21Days = deviationRange1forMoreThan21Days;
	}
	@Column
public Double getDeviationRange2forMoreThan21Days() {
		return deviationRange2forMoreThan21Days;
	}
	public void setDeviationRange2forMoreThan21Days(
			Double deviationRange2forMoreThan21Days) {
		this.deviationRange2forMoreThan21Days = deviationRange2forMoreThan21Days;
	}
	@Column
public Double getDeviationRange3forMoreThan21Days() {
		return deviationRange3forMoreThan21Days;
	}
	public void setDeviationRange3forMoreThan21Days(
			Double deviationRange3forMoreThan21Days) {
		this.deviationRange3forMoreThan21Days = deviationRange3forMoreThan21Days;
	}
	@Column
public Double getDeviationRange4forMoreThan21Days() {
		return deviationRange4forMoreThan21Days;
	}
	public void setDeviationRange4forMoreThan21Days(
			Double deviationRange4forMoreThan21Days) {
		this.deviationRange4forMoreThan21Days = deviationRange4forMoreThan21Days;
	}
	@Column
public Double getDeviationRange5forMoreThan21Days() {
		return deviationRange5forMoreThan21Days;
	}
	public void setDeviationRange5forMoreThan21Days(
			Double deviationRange5forMoreThan21Days) {
		this.deviationRange5forMoreThan21Days = deviationRange5forMoreThan21Days;
	}
	@Column
public Double getDeviationRange6forMoreThan21Days() {
		return deviationRange6forMoreThan21Days;
	}
	public void setDeviationRange6forMoreThan21Days(
			Double deviationRange6forMoreThan21Days) {
		this.deviationRange6forMoreThan21Days = deviationRange6forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer2MenRange1forMoreThan21Days() {
		return regularRatePer2MenRange1forMoreThan21Days;
	}
	public void setRegularRatePer2MenRange1forMoreThan21Days(
			Double regularRatePer2MenRange1forMoreThan21Days) {
		this.regularRatePer2MenRange1forMoreThan21Days = regularRatePer2MenRange1forMoreThan21Days;
	}
	@Column
	public Double getRegularRatePer3MenRange1forMoreThan21Days() {
		return regularRatePer3MenRange1forMoreThan21Days;
	}
	public void setRegularRatePer3MenRange1forMoreThan21Days(
			Double regularRatePer3MenRange1forMoreThan21Days) {
		this.regularRatePer3MenRange1forMoreThan21Days = regularRatePer3MenRange1forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer4MenRange1forMoreThan21Days() {
		return regularRatePer4MenRange1forMoreThan21Days;
	}
	public void setRegularRatePer4MenRange1forMoreThan21Days(
			Double regularRatePer4MenRange1forMoreThan21Days) {
		this.regularRatePer4MenRange1forMoreThan21Days = regularRatePer4MenRange1forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer2MenRange2forMoreThan21Days() {
		return regularRatePer2MenRange2forMoreThan21Days;
	}
	public void setRegularRatePer2MenRange2forMoreThan21Days(
			Double regularRatePer2MenRange2forMoreThan21Days) {
		this.regularRatePer2MenRange2forMoreThan21Days = regularRatePer2MenRange2forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer3MenRange2forMoreThan21Days() {
		return regularRatePer3MenRange2forMoreThan21Days;
	}
	public void setRegularRatePer3MenRange2forMoreThan21Days(
			Double regularRatePer3MenRange2forMoreThan21Days) {
		this.regularRatePer3MenRange2forMoreThan21Days = regularRatePer3MenRange2forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer4MenRange2forMoreThan21Days() {
		return regularRatePer4MenRange2forMoreThan21Days;
	}
	public void setRegularRatePer4MenRange2forMoreThan21Days(
			Double regularRatePer4MenRange2forMoreThan21Days) {
		this.regularRatePer4MenRange2forMoreThan21Days = regularRatePer4MenRange2forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer2MenRange3forMoreThan21Days() {
		return regularRatePer2MenRange3forMoreThan21Days;
	}
	public void setRegularRatePer2MenRange3forMoreThan21Days(
			Double regularRatePer2MenRange3forMoreThan21Days) {
		this.regularRatePer2MenRange3forMoreThan21Days = regularRatePer2MenRange3forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer3MenRange3forMoreThan21Days() {
		return regularRatePer3MenRange3forMoreThan21Days;
	}
	public void setRegularRatePer3MenRange3forMoreThan21Days(
			Double regularRatePer3MenRange3forMoreThan21Days) {
		this.regularRatePer3MenRange3forMoreThan21Days = regularRatePer3MenRange3forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer4MenRange3forMoreThan21Days() {
		return regularRatePer4MenRange3forMoreThan21Days;
	}
	public void setRegularRatePer4MenRange3forMoreThan21Days(
			Double regularRatePer4MenRange3forMoreThan21Days) {
		this.regularRatePer4MenRange3forMoreThan21Days = regularRatePer4MenRange3forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer2MenRange4forMoreThan21Days() {
		return regularRatePer2MenRange4forMoreThan21Days;
	}
	public void setRegularRatePer2MenRange4forMoreThan21Days(
			Double regularRatePer2MenRange4forMoreThan21Days) {
		this.regularRatePer2MenRange4forMoreThan21Days = regularRatePer2MenRange4forMoreThan21Days;
	}
	@Column
	public Double getRegularRatePer3MenRange4forMoreThan21Days() {
		return regularRatePer3MenRange4forMoreThan21Days;
	}
	public void setRegularRatePer3MenRange4forMoreThan21Days(
			Double regularRatePer3MenRange4forMoreThan21Days) {
		this.regularRatePer3MenRange4forMoreThan21Days = regularRatePer3MenRange4forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer4MenRange4forMoreThan21Days() {
		return regularRatePer4MenRange4forMoreThan21Days;
	}
	public void setRegularRatePer4MenRange4forMoreThan21Days(
			Double regularRatePer4MenRange4forMoreThan21Days) {
		this.regularRatePer4MenRange4forMoreThan21Days = regularRatePer4MenRange4forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer2MenRange5forMoreThan21Days() {
		return regularRatePer2MenRange5forMoreThan21Days;
	}
	public void setRegularRatePer2MenRange5forMoreThan21Days(
			Double regularRatePer2MenRange5forMoreThan21Days) {
		this.regularRatePer2MenRange5forMoreThan21Days = regularRatePer2MenRange5forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer3MenRange5forMoreThan21Days() {
		return regularRatePer3MenRange5forMoreThan21Days;
	}
	public void setRegularRatePer3MenRange5forMoreThan21Days(
			Double regularRatePer3MenRange5forMoreThan21Days) {
		this.regularRatePer3MenRange5forMoreThan21Days = regularRatePer3MenRange5forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer4MenRange5forMoreThan21Days() {
		return regularRatePer4MenRange5forMoreThan21Days;
	}
	public void setRegularRatePer4MenRange5forMoreThan21Days(
			Double regularRatePer4MenRange5forMoreThan21Days) {
		this.regularRatePer4MenRange5forMoreThan21Days = regularRatePer4MenRange5forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer2MenRange6forMoreThan21Days() {
		return regularRatePer2MenRange6forMoreThan21Days;
	}
	public void setRegularRatePer2MenRange6forMoreThan21Days(
			Double regularRatePer2MenRange6forMoreThan21Days) {
		this.regularRatePer2MenRange6forMoreThan21Days = regularRatePer2MenRange6forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer3MenRange6forMoreThan21Days() {
		return regularRatePer3MenRange6forMoreThan21Days;
	}
	public void setRegularRatePer3MenRange6forMoreThan21Days(
			Double regularRatePer3MenRange6forMoreThan21Days) {
		this.regularRatePer3MenRange6forMoreThan21Days = regularRatePer3MenRange6forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer4MenRange6forMoreThan21Days() {
		return regularRatePer4MenRange6forMoreThan21Days;
	}
	public void setRegularRatePer4MenRange6forMoreThan21Days(
			Double regularRatePer4MenRange6forMoreThan21Days) {
		this.regularRatePer4MenRange6forMoreThan21Days = regularRatePer4MenRange6forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange1forMoreThan21Days() {
		return monthEndRatePer2MenRange1forMoreThan21Days;
	}
	public void setMonthEndRatePer2MenRange1forMoreThan21Days(
			Double monthEndRatePer2MenRange1forMoreThan21Days) {
		this.monthEndRatePer2MenRange1forMoreThan21Days = monthEndRatePer2MenRange1forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange1forMoreThan21Days() {
		return monthEndRatePer3MenRange1forMoreThan21Days;
	}
	public void setMonthEndRatePer3MenRange1forMoreThan21Days(
			Double monthEndRatePer3MenRange1forMoreThan21Days) {
		this.monthEndRatePer3MenRange1forMoreThan21Days = monthEndRatePer3MenRange1forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange1forMoreThan21Days() {
		return monthEndRatePer4MenRange1forMoreThan21Days;
	}
	public void setMonthEndRatePer4MenRange1forMoreThan21Days(
			Double monthEndRatePer4MenRange1forMoreThan21Days) {
		this.monthEndRatePer4MenRange1forMoreThan21Days = monthEndRatePer4MenRange1forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange2forMoreThan21Days() {
		return monthEndRatePer2MenRange2forMoreThan21Days;
	}
	public void setMonthEndRatePer2MenRange2forMoreThan21Days(
			Double monthEndRatePer2MenRange2forMoreThan21Days) {
		this.monthEndRatePer2MenRange2forMoreThan21Days = monthEndRatePer2MenRange2forMoreThan21Days;
	}
	@Column
	public Double getMonthEndRatePer3MenRange2forMoreThan21Days() {
		return monthEndRatePer3MenRange2forMoreThan21Days;
	}
	public void setMonthEndRatePer3MenRange2forMoreThan21Days(
			Double monthEndRatePer3MenRange2forMoreThan21Days) {
		this.monthEndRatePer3MenRange2forMoreThan21Days = monthEndRatePer3MenRange2forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange2forMoreThan21Days() {
		return monthEndRatePer4MenRange2forMoreThan21Days;
	}
	public void setMonthEndRatePer4MenRange2forMoreThan21Days(
			Double monthEndRatePer4MenRange2forMoreThan21Days) {
		this.monthEndRatePer4MenRange2forMoreThan21Days = monthEndRatePer4MenRange2forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange3forMoreThan21Days() {
		return monthEndRatePer2MenRange3forMoreThan21Days;
	}
	public void setMonthEndRatePer2MenRange3forMoreThan21Days(
			Double monthEndRatePer2MenRange3forMoreThan21Days) {
		this.monthEndRatePer2MenRange3forMoreThan21Days = monthEndRatePer2MenRange3forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange3forMoreThan21Days() {
		return monthEndRatePer3MenRange3forMoreThan21Days;
	}
	public void setMonthEndRatePer3MenRange3forMoreThan21Days(
			Double monthEndRatePer3MenRange3forMoreThan21Days) {
		this.monthEndRatePer3MenRange3forMoreThan21Days = monthEndRatePer3MenRange3forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange3forMoreThan21Days() {
		return monthEndRatePer4MenRange3forMoreThan21Days;
	}
	public void setMonthEndRatePer4MenRange3forMoreThan21Days(
			Double monthEndRatePer4MenRange3forMoreThan21Days) {
		this.monthEndRatePer4MenRange3forMoreThan21Days = monthEndRatePer4MenRange3forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange4forMoreThan21Days() {
		return monthEndRatePer2MenRange4forMoreThan21Days;
	}
	public void setMonthEndRatePer2MenRange4forMoreThan21Days(
			Double monthEndRatePer2MenRange4forMoreThan21Days) {
		this.monthEndRatePer2MenRange4forMoreThan21Days = monthEndRatePer2MenRange4forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange4forMoreThan21Days() {
		return monthEndRatePer3MenRange4forMoreThan21Days;
	}
	public void setMonthEndRatePer3MenRange4forMoreThan21Days(
			Double monthEndRatePer3MenRange4forMoreThan21Days) {
		this.monthEndRatePer3MenRange4forMoreThan21Days = monthEndRatePer3MenRange4forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange4forMoreThan21Days() {
		return monthEndRatePer4MenRange4forMoreThan21Days;
	}
	public void setMonthEndRatePer4MenRange4forMoreThan21Days(
			Double monthEndRatePer4MenRange4forMoreThan21Days) {
		this.monthEndRatePer4MenRange4forMoreThan21Days = monthEndRatePer4MenRange4forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange5forMoreThan21Days() {
		return monthEndRatePer2MenRange5forMoreThan21Days;
	}
	public void setMonthEndRatePer2MenRange5forMoreThan21Days(
			Double monthEndRatePer2MenRange5forMoreThan21Days) {
		this.monthEndRatePer2MenRange5forMoreThan21Days = monthEndRatePer2MenRange5forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange5forMoreThan21Days() {
		return monthEndRatePer3MenRange5forMoreThan21Days;
	}
	public void setMonthEndRatePer3MenRange5forMoreThan21Days(
			Double monthEndRatePer3MenRange5forMoreThan21Days) {
		this.monthEndRatePer3MenRange5forMoreThan21Days = monthEndRatePer3MenRange5forMoreThan21Days;
	}
	@Column
	public Double getMonthEndRatePer4MenRange5forMoreThan21Days() {
		return monthEndRatePer4MenRange5forMoreThan21Days;
	}
	public void setMonthEndRatePer4MenRange5forMoreThan21Days(
			Double monthEndRatePer4MenRange5forMoreThan21Days) {
		this.monthEndRatePer4MenRange5forMoreThan21Days = monthEndRatePer4MenRange5forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange6forMoreThan21Days() {
		return monthEndRatePer2MenRange6forMoreThan21Days;
	}
	public void setMonthEndRatePer2MenRange6forMoreThan21Days(
			Double monthEndRatePer2MenRange6forMoreThan21Days) {
		this.monthEndRatePer2MenRange6forMoreThan21Days = monthEndRatePer2MenRange6forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange6forMoreThan21Days() {
		return monthEndRatePer3MenRange6forMoreThan21Days;
	}
	public void setMonthEndRatePer3MenRange6forMoreThan21Days(
			Double monthEndRatePer3MenRange6forMoreThan21Days) {
		this.monthEndRatePer3MenRange6forMoreThan21Days = monthEndRatePer3MenRange6forMoreThan21Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange6forMoreThan21Days() {
		return monthEndRatePer4MenRange6forMoreThan21Days;
	}
	public void setMonthEndRatePer4MenRange6forMoreThan21Days(
			Double monthEndRatePer4MenRange6forMoreThan21Days) {
		this.monthEndRatePer4MenRange6forMoreThan21Days = monthEndRatePer4MenRange6forMoreThan21Days;
	}
	@Column
public Double getRegularRatePer2MenRange1forBetween14to21Days() {
		return regularRatePer2MenRange1forBetween14to21Days;
	}
	public void setRegularRatePer2MenRange1forBetween14to21Days(
			Double regularRatePer2MenRange1forBetween14to21Days) {
		this.regularRatePer2MenRange1forBetween14to21Days = regularRatePer2MenRange1forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer3MenRange1forBetween14to21Days() {
		return regularRatePer3MenRange1forBetween14to21Days;
	}
	public void setRegularRatePer3MenRange1forBetween14to21Days(
			Double regularRatePer3MenRange1forBetween14to21Days) {
		this.regularRatePer3MenRange1forBetween14to21Days = regularRatePer3MenRange1forBetween14to21Days;
	}
	@Column
	public Double getRegularRatePer4MenRange1forBetween14to21Days() {
		return regularRatePer4MenRange1forBetween14to21Days;
	}
	public void setRegularRatePer4MenRange1forBetween14to21Days(
			Double regularRatePer4MenRange1forBetween14to21Days) {
		this.regularRatePer4MenRange1forBetween14to21Days = regularRatePer4MenRange1forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer2MenRange2forBetween14to21Days() {
		return regularRatePer2MenRange2forBetween14to21Days;
	}
	public void setRegularRatePer2MenRange2forBetween14to21Days(
			Double regularRatePer2MenRange2forBetween14to21Days) {
		this.regularRatePer2MenRange2forBetween14to21Days = regularRatePer2MenRange2forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer3MenRange2forBetween14to21Days() {
		return regularRatePer3MenRange2forBetween14to21Days;
	}
	public void setRegularRatePer3MenRange2forBetween14to21Days(
			Double regularRatePer3MenRange2forBetween14to21Days) {
		this.regularRatePer3MenRange2forBetween14to21Days = regularRatePer3MenRange2forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer4MenRange2forBetween14to21Days() {
		return regularRatePer4MenRange2forBetween14to21Days;
	}
	public void setRegularRatePer4MenRange2forBetween14to21Days(
			Double regularRatePer4MenRange2forBetween14to21Days) {
		this.regularRatePer4MenRange2forBetween14to21Days = regularRatePer4MenRange2forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer2MenRange3forBetween14to21Days() {
		return regularRatePer2MenRange3forBetween14to21Days;
	}
	public void setRegularRatePer2MenRange3forBetween14to21Days(
			Double regularRatePer2MenRange3forBetween14to21Days) {
		this.regularRatePer2MenRange3forBetween14to21Days = regularRatePer2MenRange3forBetween14to21Days;
	}
	@Column
	public Double getRegularRatePer3MenRange3forBetween14to21Days() {
		return regularRatePer3MenRange3forBetween14to21Days;
	}
	public void setRegularRatePer3MenRange3forBetween14to21Days(
			Double regularRatePer3MenRange3forBetween14to21Days) {
		this.regularRatePer3MenRange3forBetween14to21Days = regularRatePer3MenRange3forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer4MenRange3forBetween14to21Days() {
		return regularRatePer4MenRange3forBetween14to21Days;
	}
	public void setRegularRatePer4MenRange3forBetween14to21Days(
			Double regularRatePer4MenRange3forBetween14to21Days) {
		this.regularRatePer4MenRange3forBetween14to21Days = regularRatePer4MenRange3forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer2MenRange4forBetween14to21Days() {
		return regularRatePer2MenRange4forBetween14to21Days;
	}
	public void setRegularRatePer2MenRange4forBetween14to21Days(
			Double regularRatePer2MenRange4forBetween14to21Days) {
		this.regularRatePer2MenRange4forBetween14to21Days = regularRatePer2MenRange4forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer3MenRange4forBetween14to21Days() {
		return regularRatePer3MenRange4forBetween14to21Days;
	}
	public void setRegularRatePer3MenRange4forBetween14to21Days(
			Double regularRatePer3MenRange4forBetween14to21Days) {
		this.regularRatePer3MenRange4forBetween14to21Days = regularRatePer3MenRange4forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer4MenRange4forBetween14to21Days() {
		return regularRatePer4MenRange4forBetween14to21Days;
	}
	public void setRegularRatePer4MenRange4forBetween14to21Days(
			Double regularRatePer4MenRange4forBetween14to21Days) {
		this.regularRatePer4MenRange4forBetween14to21Days = regularRatePer4MenRange4forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer2MenRange5forBetween14to21Days() {
		return regularRatePer2MenRange5forBetween14to21Days;
	}
	public void setRegularRatePer2MenRange5forBetween14to21Days(
			Double regularRatePer2MenRange5forBetween14to21Days) {
		this.regularRatePer2MenRange5forBetween14to21Days = regularRatePer2MenRange5forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer3MenRange5forBetween14to21Days() {
		return regularRatePer3MenRange5forBetween14to21Days;
	}
	public void setRegularRatePer3MenRange5forBetween14to21Days(
			Double regularRatePer3MenRange5forBetween14to21Days) {
		this.regularRatePer3MenRange5forBetween14to21Days = regularRatePer3MenRange5forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer4MenRange5forBetween14to21Days() {
		return regularRatePer4MenRange5forBetween14to21Days;
	}
	public void setRegularRatePer4MenRange5forBetween14to21Days(
			Double regularRatePer4MenRange5forBetween14to21Days) {
		this.regularRatePer4MenRange5forBetween14to21Days = regularRatePer4MenRange5forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer2MenRange6forBetween14to21Days() {
		return regularRatePer2MenRange6forBetween14to21Days;
	}
	public void setRegularRatePer2MenRange6forBetween14to21Days(
			Double regularRatePer2MenRange6forBetween14to21Days) {
		this.regularRatePer2MenRange6forBetween14to21Days = regularRatePer2MenRange6forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer3MenRange6forBetween14to21Days() {
		return regularRatePer3MenRange6forBetween14to21Days;
	}
	public void setRegularRatePer3MenRange6forBetween14to21Days(
			Double regularRatePer3MenRange6forBetween14to21Days) {
		this.regularRatePer3MenRange6forBetween14to21Days = regularRatePer3MenRange6forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer4MenRange6forBetween14to21Days() {
		return regularRatePer4MenRange6forBetween14to21Days;
	}
	public void setRegularRatePer4MenRange6forBetween14to21Days(
			Double regularRatePer4MenRange6forBetween14to21Days) {
		this.regularRatePer4MenRange6forBetween14to21Days = regularRatePer4MenRange6forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange1forBetween14to21Days() {
		return monthEndRatePer2MenRange1forBetween14to21Days;
	}
	public void setMonthEndRatePer2MenRange1forBetween14to21Days(
			Double monthEndRatePer2MenRange1forBetween14to21Days) {
		this.monthEndRatePer2MenRange1forBetween14to21Days = monthEndRatePer2MenRange1forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange1forBetween14to21Days() {
		return monthEndRatePer3MenRange1forBetween14to21Days;
	}
	public void setMonthEndRatePer3MenRange1forBetween14to21Days(
			Double monthEndRatePer3MenRange1forBetween14to21Days) {
		this.monthEndRatePer3MenRange1forBetween14to21Days = monthEndRatePer3MenRange1forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange1forBetween14to21Days() {
		return monthEndRatePer4MenRange1forBetween14to21Days;
	}
	public void setMonthEndRatePer4MenRange1forBetween14to21Days(
			Double monthEndRatePer4MenRange1forBetween14to21Days) {
		this.monthEndRatePer4MenRange1forBetween14to21Days = monthEndRatePer4MenRange1forBetween14to21Days;
	}
	@Column
	public Double getMonthEndRatePer2MenRange2forBetween14to21Days() {
		return monthEndRatePer2MenRange2forBetween14to21Days;
	}
	public void setMonthEndRatePer2MenRange2forBetween14to21Days(
			Double monthEndRatePer2MenRange2forBetween14to21Days) {
		this.monthEndRatePer2MenRange2forBetween14to21Days = monthEndRatePer2MenRange2forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange2forBetween14to21Days() {
		return monthEndRatePer3MenRange2forBetween14to21Days;
	}
	public void setMonthEndRatePer3MenRange2forBetween14to21Days(
			Double monthEndRatePer3MenRange2forBetween14to21Days) {
		this.monthEndRatePer3MenRange2forBetween14to21Days = monthEndRatePer3MenRange2forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange2forBetween14to21Days() {
		return monthEndRatePer4MenRange2forBetween14to21Days;
	}
	public void setMonthEndRatePer4MenRange2forBetween14to21Days(
			Double monthEndRatePer4MenRange2forBetween14to21Days) {
		this.monthEndRatePer4MenRange2forBetween14to21Days = monthEndRatePer4MenRange2forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange3forBetween14to21Days() {
		return monthEndRatePer2MenRange3forBetween14to21Days;
	}
	public void setMonthEndRatePer2MenRange3forBetween14to21Days(
			Double monthEndRatePer2MenRange3forBetween14to21Days) {
		this.monthEndRatePer2MenRange3forBetween14to21Days = monthEndRatePer2MenRange3forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange3forBetween14to21Days() {
		return monthEndRatePer3MenRange3forBetween14to21Days;
	}
	public void setMonthEndRatePer3MenRange3forBetween14to21Days(
			Double monthEndRatePer3MenRange3forBetween14to21Days) {
		this.monthEndRatePer3MenRange3forBetween14to21Days = monthEndRatePer3MenRange3forBetween14to21Days;
	}
	@Column
	public Double getMonthEndRatePer4MenRange3forBetween14to21Days() {
		return monthEndRatePer4MenRange3forBetween14to21Days;
	}
	public void setMonthEndRatePer4MenRange3forBetween14to21Days(
			Double monthEndRatePer4MenRange3forBetween14to21Days) {
		this.monthEndRatePer4MenRange3forBetween14to21Days = monthEndRatePer4MenRange3forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange4forBetween14to21Days() {
		return monthEndRatePer2MenRange4forBetween14to21Days;
	}
	public void setMonthEndRatePer2MenRange4forBetween14to21Days(
			Double monthEndRatePer2MenRange4forBetween14to21Days) {
		this.monthEndRatePer2MenRange4forBetween14to21Days = monthEndRatePer2MenRange4forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange4forBetween14to21Days() {
		return monthEndRatePer3MenRange4forBetween14to21Days;
	}
	public void setMonthEndRatePer3MenRange4forBetween14to21Days(
			Double monthEndRatePer3MenRange4forBetween14to21Days) {
		this.monthEndRatePer3MenRange4forBetween14to21Days = monthEndRatePer3MenRange4forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange4forBetween14to21Days() {
		return monthEndRatePer4MenRange4forBetween14to21Days;
	}
	public void setMonthEndRatePer4MenRange4forBetween14to21Days(
			Double monthEndRatePer4MenRange4forBetween14to21Days) {
		this.monthEndRatePer4MenRange4forBetween14to21Days = monthEndRatePer4MenRange4forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange5forBetween14to21Days() {
		return monthEndRatePer2MenRange5forBetween14to21Days;
	}
	public void setMonthEndRatePer2MenRange5forBetween14to21Days(
			Double monthEndRatePer2MenRange5forBetween14to21Days) {
		this.monthEndRatePer2MenRange5forBetween14to21Days = monthEndRatePer2MenRange5forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange5forBetween14to21Days() {
		return monthEndRatePer3MenRange5forBetween14to21Days;
	}
	public void setMonthEndRatePer3MenRange5forBetween14to21Days(
			Double monthEndRatePer3MenRange5forBetween14to21Days) {
		this.monthEndRatePer3MenRange5forBetween14to21Days = monthEndRatePer3MenRange5forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange5forBetween14to21Days() {
		return monthEndRatePer4MenRange5forBetween14to21Days;
	}
	public void setMonthEndRatePer4MenRange5forBetween14to21Days(
			Double monthEndRatePer4MenRange5forBetween14to21Days) {
		this.monthEndRatePer4MenRange5forBetween14to21Days = monthEndRatePer4MenRange5forBetween14to21Days;
	}
	@Column
	public Double getMonthEndRatePer2MenRange6forBetween14to21Days() {
		return monthEndRatePer2MenRange6forBetween14to21Days;
	}
	public void setMonthEndRatePer2MenRange6forBetween14to21Days(
			Double monthEndRatePer2MenRange6forBetween14to21Days) {
		this.monthEndRatePer2MenRange6forBetween14to21Days = monthEndRatePer2MenRange6forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange6forBetween14to21Days() {
		return monthEndRatePer3MenRange6forBetween14to21Days;
	}
	public void setMonthEndRatePer3MenRange6forBetween14to21Days(
			Double monthEndRatePer3MenRange6forBetween14to21Days) {
		this.monthEndRatePer3MenRange6forBetween14to21Days = monthEndRatePer3MenRange6forBetween14to21Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange6forBetween14to21Days() {
		return monthEndRatePer4MenRange6forBetween14to21Days;
	}
	public void setMonthEndRatePer4MenRange6forBetween14to21Days(
			Double monthEndRatePer4MenRange6forBetween14to21Days) {
		this.monthEndRatePer4MenRange6forBetween14to21Days = monthEndRatePer4MenRange6forBetween14to21Days;
	}
	@Column
public Double getRegularRatePer2MenRange1forBetween7to13Days() {
		return regularRatePer2MenRange1forBetween7to13Days;
	}
	public void setRegularRatePer2MenRange1forBetween7to13Days(
			Double regularRatePer2MenRange1forBetween7to13Days) {
		this.regularRatePer2MenRange1forBetween7to13Days = regularRatePer2MenRange1forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer3MenRange1forBetween7to13Days() {
		return regularRatePer3MenRange1forBetween7to13Days;
	}
	public void setRegularRatePer3MenRange1forBetween7to13Days(
			Double regularRatePer3MenRange1forBetween7to13Days) {
		this.regularRatePer3MenRange1forBetween7to13Days = regularRatePer3MenRange1forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer4MenRange1forBetween7to13Days() {
		return regularRatePer4MenRange1forBetween7to13Days;
	}
	public void setRegularRatePer4MenRange1forBetween7to13Days(
			Double regularRatePer4MenRange1forBetween7to13Days) {
		this.regularRatePer4MenRange1forBetween7to13Days = regularRatePer4MenRange1forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer2MenRange2forBetween7to13Days() {
		return regularRatePer2MenRange2forBetween7to13Days;
	}
	public void setRegularRatePer2MenRange2forBetween7to13Days(
			Double regularRatePer2MenRange2forBetween7to13Days) {
		this.regularRatePer2MenRange2forBetween7to13Days = regularRatePer2MenRange2forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer3MenRange2forBetween7to13Days() {
		return regularRatePer3MenRange2forBetween7to13Days;
	}
	public void setRegularRatePer3MenRange2forBetween7to13Days(
			Double regularRatePer3MenRange2forBetween7to13Days) {
		this.regularRatePer3MenRange2forBetween7to13Days = regularRatePer3MenRange2forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer4MenRange2forBetween7to13Days() {
		return regularRatePer4MenRange2forBetween7to13Days;
	}
	public void setRegularRatePer4MenRange2forBetween7to13Days(
			Double regularRatePer4MenRange2forBetween7to13Days) {
		this.regularRatePer4MenRange2forBetween7to13Days = regularRatePer4MenRange2forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer2MenRange3forBetween7to13Days() {
		return regularRatePer2MenRange3forBetween7to13Days;
	}
	public void setRegularRatePer2MenRange3forBetween7to13Days(
			Double regularRatePer2MenRange3forBetween7to13Days) {
		this.regularRatePer2MenRange3forBetween7to13Days = regularRatePer2MenRange3forBetween7to13Days;
	}
	@Column
	public Double getRegularRatePer3MenRange3forBetween7to13Days() {
		return regularRatePer3MenRange3forBetween7to13Days;
	}
	public void setRegularRatePer3MenRange3forBetween7to13Days(
			Double regularRatePer3MenRange3forBetween7to13Days) {
		this.regularRatePer3MenRange3forBetween7to13Days = regularRatePer3MenRange3forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer4MenRange3forBetween7to13Days() {
		return regularRatePer4MenRange3forBetween7to13Days;
	}
	public void setRegularRatePer4MenRange3forBetween7to13Days(
			Double regularRatePer4MenRange3forBetween7to13Days) {
		this.regularRatePer4MenRange3forBetween7to13Days = regularRatePer4MenRange3forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer2MenRange4forBetween7to13Days() {
		return regularRatePer2MenRange4forBetween7to13Days;
	}
	public void setRegularRatePer2MenRange4forBetween7to13Days(
			Double regularRatePer2MenRange4forBetween7to13Days) {
		this.regularRatePer2MenRange4forBetween7to13Days = regularRatePer2MenRange4forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer3MenRange4forBetween7to13Days() {
		return regularRatePer3MenRange4forBetween7to13Days;
	}
	public void setRegularRatePer3MenRange4forBetween7to13Days(
			Double regularRatePer3MenRange4forBetween7to13Days) {
		this.regularRatePer3MenRange4forBetween7to13Days = regularRatePer3MenRange4forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer4MenRange4forBetween7to13Days() {
		return regularRatePer4MenRange4forBetween7to13Days;
	}
	public void setRegularRatePer4MenRange4forBetween7to13Days(
			Double regularRatePer4MenRange4forBetween7to13Days) {
		this.regularRatePer4MenRange4forBetween7to13Days = regularRatePer4MenRange4forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer2MenRange5forBetween7to13Days() {
		return regularRatePer2MenRange5forBetween7to13Days;
	}
	public void setRegularRatePer2MenRange5forBetween7to13Days(
			Double regularRatePer2MenRange5forBetween7to13Days) {
		this.regularRatePer2MenRange5forBetween7to13Days = regularRatePer2MenRange5forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer3MenRange5forBetween7to13Days() {
		return regularRatePer3MenRange5forBetween7to13Days;
	}
	public void setRegularRatePer3MenRange5forBetween7to13Days(
			Double regularRatePer3MenRange5forBetween7to13Days) {
		this.regularRatePer3MenRange5forBetween7to13Days = regularRatePer3MenRange5forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer4MenRange5forBetween7to13Days() {
		return regularRatePer4MenRange5forBetween7to13Days;
	}
	public void setRegularRatePer4MenRange5forBetween7to13Days(
			Double regularRatePer4MenRange5forBetween7to13Days) {
		this.regularRatePer4MenRange5forBetween7to13Days = regularRatePer4MenRange5forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer2MenRange6forBetween7to13Days() {
		return regularRatePer2MenRange6forBetween7to13Days;
	}
	public void setRegularRatePer2MenRange6forBetween7to13Days(
			Double regularRatePer2MenRange6forBetween7to13Days) {
		this.regularRatePer2MenRange6forBetween7to13Days = regularRatePer2MenRange6forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer3MenRange6forBetween7to13Days() {
		return regularRatePer3MenRange6forBetween7to13Days;
	}
	public void setRegularRatePer3MenRange6forBetween7to13Days(
			Double regularRatePer3MenRange6forBetween7to13Days) {
		this.regularRatePer3MenRange6forBetween7to13Days = regularRatePer3MenRange6forBetween7to13Days;
	}
	@Column
	public Double getRegularRatePer4MenRange6forBetween7to13Days() {
		return regularRatePer4MenRange6forBetween7to13Days;
	}
	public void setRegularRatePer4MenRange6forBetween7to13Days(
			Double regularRatePer4MenRange6forBetween7to13Days) {
		this.regularRatePer4MenRange6forBetween7to13Days = regularRatePer4MenRange6forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange1forBetween7to13Days() {
		return monthEndRatePer2MenRange1forBetween7to13Days;
	}
	public void setMonthEndRatePer2MenRange1forBetween7to13Days(
			Double monthEndRatePer2MenRange1forBetween7to13Days) {
		this.monthEndRatePer2MenRange1forBetween7to13Days = monthEndRatePer2MenRange1forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange1forBetween7to13Days() {
		return monthEndRatePer3MenRange1forBetween7to13Days;
	}
	public void setMonthEndRatePer3MenRange1forBetween7to13Days(
			Double monthEndRatePer3MenRange1forBetween7to13Days) {
		this.monthEndRatePer3MenRange1forBetween7to13Days = monthEndRatePer3MenRange1forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange1forBetween7to13Days() {
		return monthEndRatePer4MenRange1forBetween7to13Days;
	}
	public void setMonthEndRatePer4MenRange1forBetween7to13Days(
			Double monthEndRatePer4MenRange1forBetween7to13Days) {
		this.monthEndRatePer4MenRange1forBetween7to13Days = monthEndRatePer4MenRange1forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange2forBetween7to13Days() {
		return monthEndRatePer2MenRange2forBetween7to13Days;
	}
	public void setMonthEndRatePer2MenRange2forBetween7to13Days(
			Double monthEndRatePer2MenRange2forBetween7to13Days) {
		this.monthEndRatePer2MenRange2forBetween7to13Days = monthEndRatePer2MenRange2forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange2forBetween7to13Days() {
		return monthEndRatePer3MenRange2forBetween7to13Days;
	}
	public void setMonthEndRatePer3MenRange2forBetween7to13Days(
			Double monthEndRatePer3MenRange2forBetween7to13Days) {
		this.monthEndRatePer3MenRange2forBetween7to13Days = monthEndRatePer3MenRange2forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange2forBetween7to13Days() {
		return monthEndRatePer4MenRange2forBetween7to13Days;
	}
	public void setMonthEndRatePer4MenRange2forBetween7to13Days(
			Double monthEndRatePer4MenRange2forBetween7to13Days) {
		this.monthEndRatePer4MenRange2forBetween7to13Days = monthEndRatePer4MenRange2forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange3forBetween7to13Days() {
		return monthEndRatePer2MenRange3forBetween7to13Days;
	}
	public void setMonthEndRatePer2MenRange3forBetween7to13Days(
			Double monthEndRatePer2MenRange3forBetween7to13Days) {
		this.monthEndRatePer2MenRange3forBetween7to13Days = monthEndRatePer2MenRange3forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange3forBetween7to13Days() {
		return monthEndRatePer3MenRange3forBetween7to13Days;
	}
	public void setMonthEndRatePer3MenRange3forBetween7to13Days(
			Double monthEndRatePer3MenRange3forBetween7to13Days) {
		this.monthEndRatePer3MenRange3forBetween7to13Days = monthEndRatePer3MenRange3forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange3forBetween7to13Days() {
		return monthEndRatePer4MenRange3forBetween7to13Days;
	}
	public void setMonthEndRatePer4MenRange3forBetween7to13Days(
			Double monthEndRatePer4MenRange3forBetween7to13Days) {
		this.monthEndRatePer4MenRange3forBetween7to13Days = monthEndRatePer4MenRange3forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange4forBetween7to13Days() {
		return monthEndRatePer2MenRange4forBetween7to13Days;
	}
	public void setMonthEndRatePer2MenRange4forBetween7to13Days(
			Double monthEndRatePer2MenRange4forBetween7to13Days) {
		this.monthEndRatePer2MenRange4forBetween7to13Days = monthEndRatePer2MenRange4forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange4forBetween7to13Days() {
		return monthEndRatePer3MenRange4forBetween7to13Days;
	}
	public void setMonthEndRatePer3MenRange4forBetween7to13Days(
			Double monthEndRatePer3MenRange4forBetween7to13Days) {
		this.monthEndRatePer3MenRange4forBetween7to13Days = monthEndRatePer3MenRange4forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange4forBetween7to13Days() {
		return monthEndRatePer4MenRange4forBetween7to13Days;
	}
	public void setMonthEndRatePer4MenRange4forBetween7to13Days(
			Double monthEndRatePer4MenRange4forBetween7to13Days) {
		this.monthEndRatePer4MenRange4forBetween7to13Days = monthEndRatePer4MenRange4forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange5forBetween7to13Days() {
		return monthEndRatePer2MenRange5forBetween7to13Days;
	}
	public void setMonthEndRatePer2MenRange5forBetween7to13Days(
			Double monthEndRatePer2MenRange5forBetween7to13Days) {
		this.monthEndRatePer2MenRange5forBetween7to13Days = monthEndRatePer2MenRange5forBetween7to13Days;
	}
	@Column
	public Double getMonthEndRatePer3MenRange5forBetween7to13Days() {
		return monthEndRatePer3MenRange5forBetween7to13Days;
	}
	public void setMonthEndRatePer3MenRange5forBetween7to13Days(
			Double monthEndRatePer3MenRange5forBetween7to13Days) {
		this.monthEndRatePer3MenRange5forBetween7to13Days = monthEndRatePer3MenRange5forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange5forBetween7to13Days() {
		return monthEndRatePer4MenRange5forBetween7to13Days;
	}
	public void setMonthEndRatePer4MenRange5forBetween7to13Days(
			Double monthEndRatePer4MenRange5forBetween7to13Days) {
		this.monthEndRatePer4MenRange5forBetween7to13Days = monthEndRatePer4MenRange5forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange6forBetween7to13Days() {
		return monthEndRatePer2MenRange6forBetween7to13Days;
	}
	public void setMonthEndRatePer2MenRange6forBetween7to13Days(
			Double monthEndRatePer2MenRange6forBetween7to13Days) {
		this.monthEndRatePer2MenRange6forBetween7to13Days = monthEndRatePer2MenRange6forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange6forBetween7to13Days() {
		return monthEndRatePer3MenRange6forBetween7to13Days;
	}
	public void setMonthEndRatePer3MenRange6forBetween7to13Days(
			Double monthEndRatePer3MenRange6forBetween7to13Days) {
		this.monthEndRatePer3MenRange6forBetween7to13Days = monthEndRatePer3MenRange6forBetween7to13Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange6forBetween7to13Days() {
		return monthEndRatePer4MenRange6forBetween7to13Days;
	}
	public void setMonthEndRatePer4MenRange6forBetween7to13Days(
			Double monthEndRatePer4MenRange6forBetween7to13Days) {
		this.monthEndRatePer4MenRange6forBetween7to13Days = monthEndRatePer4MenRange6forBetween7to13Days;
	}
	@Column
public Double getRegularRatePer2MenRange1forLessThan7Days() {
		return regularRatePer2MenRange1forLessThan7Days;
	}
	public void setRegularRatePer2MenRange1forLessThan7Days(
			Double regularRatePer2MenRange1forLessThan7Days) {
		this.regularRatePer2MenRange1forLessThan7Days = regularRatePer2MenRange1forLessThan7Days;
	}
	@Column
public Double getRegularRatePer3MenRange1forLessThan7Days() {
		return regularRatePer3MenRange1forLessThan7Days;
	}
	public void setRegularRatePer3MenRange1forLessThan7Days(
			Double regularRatePer3MenRange1forLessThan7Days) {
		this.regularRatePer3MenRange1forLessThan7Days = regularRatePer3MenRange1forLessThan7Days;
	}
	@Column
	public Double getRegularRatePer4MenRange1forLessThan7Days() {
		return regularRatePer4MenRange1forLessThan7Days;
	}
	public void setRegularRatePer4MenRange1forLessThan7Days(
			Double regularRatePer4MenRange1forLessThan7Days) {
		this.regularRatePer4MenRange1forLessThan7Days = regularRatePer4MenRange1forLessThan7Days;
	}
	@Column
public Double getRegularRatePer2MenRange2forLessThan7Days() {
		return regularRatePer2MenRange2forLessThan7Days;
	}
	public void setRegularRatePer2MenRange2forLessThan7Days(
			Double regularRatePer2MenRange2forLessThan7Days) {
		this.regularRatePer2MenRange2forLessThan7Days = regularRatePer2MenRange2forLessThan7Days;
	}
	@Column
public Double getRegularRatePer3MenRange2forLessThan7Days() {
		return regularRatePer3MenRange2forLessThan7Days;
	}
	public void setRegularRatePer3MenRange2forLessThan7Days(
			Double regularRatePer3MenRange2forLessThan7Days) {
		this.regularRatePer3MenRange2forLessThan7Days = regularRatePer3MenRange2forLessThan7Days;
	}
	@Column
public Double getRegularRatePer4MenRange2forLessThan7Days() {
		return regularRatePer4MenRange2forLessThan7Days;
	}
	public void setRegularRatePer4MenRange2forLessThan7Days(
			Double regularRatePer4MenRange2forLessThan7Days) {
		this.regularRatePer4MenRange2forLessThan7Days = regularRatePer4MenRange2forLessThan7Days;
	}
	@Column
public Double getRegularRatePer2MenRange3forLessThan7Days() {
		return regularRatePer2MenRange3forLessThan7Days;
	}
	public void setRegularRatePer2MenRange3forLessThan7Days(
			Double regularRatePer2MenRange3forLessThan7Days) {
		this.regularRatePer2MenRange3forLessThan7Days = regularRatePer2MenRange3forLessThan7Days;
	}
	@Column
public Double getRegularRatePer3MenRange3forLessThan7Days() {
		return regularRatePer3MenRange3forLessThan7Days;
	}
	public void setRegularRatePer3MenRange3forLessThan7Days(
			Double regularRatePer3MenRange3forLessThan7Days) {
		this.regularRatePer3MenRange3forLessThan7Days = regularRatePer3MenRange3forLessThan7Days;
	}
	@Column
	public Double getRegularRatePer4MenRange3forLessThan7Days() {
		return regularRatePer4MenRange3forLessThan7Days;
	}
	public void setRegularRatePer4MenRange3forLessThan7Days(
			Double regularRatePer4MenRange3forLessThan7Days) {
		this.regularRatePer4MenRange3forLessThan7Days = regularRatePer4MenRange3forLessThan7Days;
	}
	@Column
public Double getRegularRatePer2MenRange4forLessThan7Days() {
		return regularRatePer2MenRange4forLessThan7Days;
	}
	public void setRegularRatePer2MenRange4forLessThan7Days(
			Double regularRatePer2MenRange4forLessThan7Days) {
		this.regularRatePer2MenRange4forLessThan7Days = regularRatePer2MenRange4forLessThan7Days;
	}
	@Column
public Double getRegularRatePer3MenRange4forLessThan7Days() {
		return regularRatePer3MenRange4forLessThan7Days;
	}
	public void setRegularRatePer3MenRange4forLessThan7Days(
			Double regularRatePer3MenRange4forLessThan7Days) {
		this.regularRatePer3MenRange4forLessThan7Days = regularRatePer3MenRange4forLessThan7Days;
	}
	@Column
public Double getRegularRatePer4MenRange4forLessThan7Days() {
		return regularRatePer4MenRange4forLessThan7Days;
	}
	public void setRegularRatePer4MenRange4forLessThan7Days(
			Double regularRatePer4MenRange4forLessThan7Days) {
		this.regularRatePer4MenRange4forLessThan7Days = regularRatePer4MenRange4forLessThan7Days;
	}
	@Column
public Double getRegularRatePer2MenRange5forLessThan7Days() {
		return regularRatePer2MenRange5forLessThan7Days;
	}
	public void setRegularRatePer2MenRange5forLessThan7Days(
			Double regularRatePer2MenRange5forLessThan7Days) {
		this.regularRatePer2MenRange5forLessThan7Days = regularRatePer2MenRange5forLessThan7Days;
	}
	@Column
public Double getRegularRatePer3MenRange5forLessThan7Days() {
		return regularRatePer3MenRange5forLessThan7Days;
	}
	public void setRegularRatePer3MenRange5forLessThan7Days(
			Double regularRatePer3MenRange5forLessThan7Days) {
		this.regularRatePer3MenRange5forLessThan7Days = regularRatePer3MenRange5forLessThan7Days;
	}
	@Column
	public Double getRegularRatePer4MenRange5forLessThan7Days() {
		return regularRatePer4MenRange5forLessThan7Days;
	}
	public void setRegularRatePer4MenRange5forLessThan7Days(
			Double regularRatePer4MenRange5forLessThan7Days) {
		this.regularRatePer4MenRange5forLessThan7Days = regularRatePer4MenRange5forLessThan7Days;
	}
	@Column
public Double getRegularRatePer2MenRange6forLessThan7Days() {
		return regularRatePer2MenRange6forLessThan7Days;
	}
	public void setRegularRatePer2MenRange6forLessThan7Days(
			Double regularRatePer2MenRange6forLessThan7Days) {
		this.regularRatePer2MenRange6forLessThan7Days = regularRatePer2MenRange6forLessThan7Days;
	}
	@Column
public Double getRegularRatePer3MenRange6forLessThan7Days() {
		return regularRatePer3MenRange6forLessThan7Days;
	}
	public void setRegularRatePer3MenRange6forLessThan7Days(
			Double regularRatePer3MenRange6forLessThan7Days) {
		this.regularRatePer3MenRange6forLessThan7Days = regularRatePer3MenRange6forLessThan7Days;
	}
	@Column
public Double getRegularRatePer4MenRange6forLessThan7Days() {
		return regularRatePer4MenRange6forLessThan7Days;
	}
	public void setRegularRatePer4MenRange6forLessThan7Days(
			Double regularRatePer4MenRange6forLessThan7Days) {
		this.regularRatePer4MenRange6forLessThan7Days = regularRatePer4MenRange6forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange1forLessThan7Days() {
		return monthEndRatePer2MenRange1forLessThan7Days;
	}
	public void setMonthEndRatePer2MenRange1forLessThan7Days(
			Double monthEndRatePer2MenRange1forLessThan7Days) {
		this.monthEndRatePer2MenRange1forLessThan7Days = monthEndRatePer2MenRange1forLessThan7Days;
	}
	@Column
	public Double getMonthEndRatePer3MenRange1forLessThan7Days() {
		return monthEndRatePer3MenRange1forLessThan7Days;
	}
	public void setMonthEndRatePer3MenRange1forLessThan7Days(
			Double monthEndRatePer3MenRange1forLessThan7Days) {
		this.monthEndRatePer3MenRange1forLessThan7Days = monthEndRatePer3MenRange1forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange1forLessThan7Days() {
		return monthEndRatePer4MenRange1forLessThan7Days;
	}
	public void setMonthEndRatePer4MenRange1forLessThan7Days(
			Double monthEndRatePer4MenRange1forLessThan7Days) {
		this.monthEndRatePer4MenRange1forLessThan7Days = monthEndRatePer4MenRange1forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange2forLessThan7Days() {
		return monthEndRatePer2MenRange2forLessThan7Days;
	}
	public void setMonthEndRatePer2MenRange2forLessThan7Days(
			Double monthEndRatePer2MenRange2forLessThan7Days) {
		this.monthEndRatePer2MenRange2forLessThan7Days = monthEndRatePer2MenRange2forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange2forLessThan7Days() {
		return monthEndRatePer3MenRange2forLessThan7Days;
	}
	public void setMonthEndRatePer3MenRange2forLessThan7Days(
			Double monthEndRatePer3MenRange2forLessThan7Days) {
		this.monthEndRatePer3MenRange2forLessThan7Days = monthEndRatePer3MenRange2forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange2forLessThan7Days() {
		return monthEndRatePer4MenRange2forLessThan7Days;
	}
	public void setMonthEndRatePer4MenRange2forLessThan7Days(
			Double monthEndRatePer4MenRange2forLessThan7Days) {
		this.monthEndRatePer4MenRange2forLessThan7Days = monthEndRatePer4MenRange2forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange3forLessThan7Days() {
		return monthEndRatePer2MenRange3forLessThan7Days;
	}
	public void setMonthEndRatePer2MenRange3forLessThan7Days(
			Double monthEndRatePer2MenRange3forLessThan7Days) {
		this.monthEndRatePer2MenRange3forLessThan7Days = monthEndRatePer2MenRange3forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange3forLessThan7Days() {
		return monthEndRatePer3MenRange3forLessThan7Days;
	}
	public void setMonthEndRatePer3MenRange3forLessThan7Days(
			Double monthEndRatePer3MenRange3forLessThan7Days) {
		this.monthEndRatePer3MenRange3forLessThan7Days = monthEndRatePer3MenRange3forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange3forLessThan7Days() {
		return monthEndRatePer4MenRange3forLessThan7Days;
	}
	public void setMonthEndRatePer4MenRange3forLessThan7Days(
			Double monthEndRatePer4MenRange3forLessThan7Days) {
		this.monthEndRatePer4MenRange3forLessThan7Days = monthEndRatePer4MenRange3forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange4forLessThan7Days() {
		return monthEndRatePer2MenRange4forLessThan7Days;
	}
	public void setMonthEndRatePer2MenRange4forLessThan7Days(
			Double monthEndRatePer2MenRange4forLessThan7Days) {
		this.monthEndRatePer2MenRange4forLessThan7Days = monthEndRatePer2MenRange4forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange4forLessThan7Days() {
		return monthEndRatePer3MenRange4forLessThan7Days;
	}
	public void setMonthEndRatePer3MenRange4forLessThan7Days(
			Double monthEndRatePer3MenRange4forLessThan7Days) {
		this.monthEndRatePer3MenRange4forLessThan7Days = monthEndRatePer3MenRange4forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange4forLessThan7Days() {
		return monthEndRatePer4MenRange4forLessThan7Days;
	}
	public void setMonthEndRatePer4MenRange4forLessThan7Days(
			Double monthEndRatePer4MenRange4forLessThan7Days) {
		this.monthEndRatePer4MenRange4forLessThan7Days = monthEndRatePer4MenRange4forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange5forLessThan7Days() {
		return monthEndRatePer2MenRange5forLessThan7Days;
	}
	public void setMonthEndRatePer2MenRange5forLessThan7Days(
			Double monthEndRatePer2MenRange5forLessThan7Days) {
		this.monthEndRatePer2MenRange5forLessThan7Days = monthEndRatePer2MenRange5forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange5forLessThan7Days() {
		return monthEndRatePer3MenRange5forLessThan7Days;
	}
	public void setMonthEndRatePer3MenRange5forLessThan7Days(
			Double monthEndRatePer3MenRange5forLessThan7Days) {
		this.monthEndRatePer3MenRange5forLessThan7Days = monthEndRatePer3MenRange5forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange5forLessThan7Days() {
		return monthEndRatePer4MenRange5forLessThan7Days;
	}
	public void setMonthEndRatePer4MenRange5forLessThan7Days(
			Double monthEndRatePer4MenRange5forLessThan7Days) {
		this.monthEndRatePer4MenRange5forLessThan7Days = monthEndRatePer4MenRange5forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer2MenRange6forLessThan7Days() {
		return monthEndRatePer2MenRange6forLessThan7Days;
	}
	public void setMonthEndRatePer2MenRange6forLessThan7Days(
			Double monthEndRatePer2MenRange6forLessThan7Days) {
		this.monthEndRatePer2MenRange6forLessThan7Days = monthEndRatePer2MenRange6forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer3MenRange6forLessThan7Days() {
		return monthEndRatePer3MenRange6forLessThan7Days;
	}
	public void setMonthEndRatePer3MenRange6forLessThan7Days(
			Double monthEndRatePer3MenRange6forLessThan7Days) {
		this.monthEndRatePer3MenRange6forLessThan7Days = monthEndRatePer3MenRange6forLessThan7Days;
	}
	@Column
public Double getMonthEndRatePer4MenRange6forLessThan7Days() {
		return monthEndRatePer4MenRange6forLessThan7Days;
	}
	public void setMonthEndRatePer4MenRange6forLessThan7Days(
			Double monthEndRatePer4MenRange6forLessThan7Days) {
		this.monthEndRatePer4MenRange6forLessThan7Days = monthEndRatePer4MenRange6forLessThan7Days;
	}

}
