update app_user set newsUpdateFlag=true ;

ALTER TABLE `redsky`.`userguides` ADD COLUMN `videoFlag` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`app_user` ADD COLUMN `newsUpdateFlag` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`company` ADD COLUMN `defaultBillingRate` DECIMAL(4,2) DEFAULT 20 AFTER `cmmdmmAgent`;

ALTER TABLE `redsky`.`company` ADD COLUMN `rskyBillGroup` VARCHAR(80) DEFAULT NULL AFTER `defaultBillingRate`;

ALTER TABLE `redsky`.`redskybilling` ADD COLUMN `billingRate` DECIMAL(4,2) DEFAULT 20;

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)
values('TSFT', 'test', 'Test', 20, 'RSKYBillGroup','en',now(),'dmaurya',now(),'dmaurya');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)
values('TSFT', 'A', 'TestA', 20, 'RSKYBillGroup','en',now(),'dmaurya',now(),'dmaurya');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)
values('TSFT', 'B', 'TestB', 20, 'RSKYBillGroup','en',now(),'dmaurya',now(),'dmaurya');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)
values('TSFT', 'C', 'TestC', 20, 'RSKYBillGroup','en',now(),'dmaurya',now(),'dmaurya');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','RSKYBillGroup',50,true,now(),'dmaurya',now(),'dmaurya',1,1,'');

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `storageEmail` VARCHAR(45) DEFAULT NULL AFTER `minInsurancePerUnit`;

ALTER TABLE `redsky`.`userguides` MODIFY COLUMN `documentName` TEXT  DEFAULT NULL;

update refmaster set parameter='STATUSREASON' where parameter='statusReason';

insert into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby,language)
values('SSCW', 'DRIVER', 'DRIVER', 20,'USER_TYPE',now(),'subrat',now(),'subrat','en');

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `detailedList` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `noInsuranceCportal` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`partnerpublic` ADD COLUMN `storageEmail` varchar(30) ;

ALTER TABLE `redsky`.`billing` ADD COLUMN `storageEmail` varchar(30);

ALTER TABLE `redsky`.`billing` ADD COLUMN `wareHousereceiptDate` DATETIME DEFAULT NULL AFTER `insSubmitDate`;

update systemdefault set noinsurance=true;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('AccountLine','estimateSellDeviation' ,'Create','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','estimateSellDeviation' ,'Update','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','estimateDeviation' ,'Create','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','estimateDeviation' ,'Update','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','revisionSellDeviation' ,'Create','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','revisionSellDeviation' ,'Update','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','revisionDeviation' ,'Create','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','revisionDeviation' ,'Update','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','deviation' ,'Create','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','deviation' ,'Update','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','receivableSellDeviation' ,'Create','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','receivableSellDeviation' ,'Update','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','payDeviation' ,'Create','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','payDeviation' ,'Update','amishra',now(),'amishra',now(),false,'','','');

Do Not Run Both Function 
/*DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`role` $$
CREATE  FUNCTION `role`(user1 varchar(50)) RETURNS varchar(500) CHARSET latin1
BEGIN
DECLARE Name1 varchar(500) ;

select group_concat(name,'/') into Name1 from app_user a,user_role u,role r
where a.id=u.user_id and u.role_id=r.id and username=user1;

RETURN Name1;

END $$

DELIMITER ;

DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`permission` $$
CREATE  FUNCTION `permission`(permission1 varchar(50)) RETURNS varchar(500) CHARSET latin1
BEGIN
DECLARE Name1 varchar(500) ;

SELECT group_concat(d.name,'/') into Name1 FROM userdatasecurity u,app_user a,datasecurityset d where u.user_id=a.id and u.userdatasecurity_id=d.id
and username=permission1;

RETURN Name1;

END $$

DELIMITER ;*/


