<%@ include file="/common/taglibs.jsp"%> 


<head> 
<title><fmt:message key="auditSetupList.title"/></title> 
<meta name="heading" content="<fmt:message key='auditSetupList.heading'/>"/> 


<style>
 span.pagelinks {
display:block;
font-size:0.90em;
margin-bottom:2px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>
</head> 
<c:set var="buttons">  
     <input type="button" class="cssbutton1" style="width:55px; height:25px" onclick="location.href='<c:url value="/auditSetupForm.html"/>'" value="<fmt:message key="button.add"/>"/>   	
</c:set> 
<s:form id="auditSetupList" name="auditSetupList" action="searchAuditSetup" method="post" validate="true">   

<div id="Layer1" style="width:100%;">
<div id="otabs"><ul><li><a class="current"><span>Search</span></a></li></ul> </div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:12px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="table">
	<thead>
	  <tr>
		<th><fmt:message key="auditSetup.tableName"/></th>
		<th><fmt:message key="auditSetup.fieldName"/></th>
		<th><fmt:message key="auditSetup.auditable"/></th>
		<th>Alert</th>
		
	  </tr>
	</thead> 
	
	<tbody>
	  	<tr>
		   <td><s:textfield name="auditFieldName"  required="true" cssClass="input-text" size="25" cssStyle="width:150px;"/></td>
		   <td><s:textfield name="auditTableName"  required="true" cssClass="input-text" size="25" cssStyle="width:150px;"/></td>
		   <td><s:select cssClass="list-menu"   name="auditAuditable" list="{'true','false'}" headerKey="" headerValue="" cssStyle="width:150px"/></td>
		   <td><s:textfield name="isAlertValue"  required="true" cssClass="input-text" size="20" cssStyle="width:150px;"/></td>
		   </tr>
		   <tr>
		   <td colspan="3"></td>
		
		   <td style="border-left: hidden;">
		       <s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;" method="search" key="button.search"/>  
		       <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/>     
		   </td>
	  	</tr>
	</tbody>
 </table>
 	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<c:out value="${searchresults}" escapeXml="false" /> 

<div id="otabs">
	<ul><li><a class="current"><span>Audit Setup</span></a></li></ul>
</div>
<div class="spnblk">&nbsp;</div>
	<display:table name="AuditSetupSet" class="table" requestURI="" id="AuditSetupssList"  defaultsort="1" export="false" pagesize="10" style="margin-top:1px;"> 
			<display:column property="tableName" href="auditSetupForm.html" paramId="id" paramProperty="id" sortable="true" titleKey="auditSetup.tableName"/> 
			<display:column property="fieldName" sortable="true" titleKey="auditSetup.fieldName"/> 
			<display:column property="description" sortable="true" titleKey="auditSetup.description"/> 
			<c:if test="${AuditSetupssList.auditable=='true' }">
            <display:column titleKey="auditSetup.auditable"><img src="${pageContext.request.contextPath}/images/tick01.gif" /></display:column> 
            </c:if>
            <c:if test="${AuditSetupssList.auditable!='true' }">
            <display:column titleKey="auditSetup.auditable"><img src="${pageContext.request.contextPath}/images/cancel001.gif" /></display:column> 
            </c:if>  
          <display:column property="alertValue" sortable="true" title="Alert"/>
	</display:table> 
 </div>
<c:out value="${buttons}" escapeXml="false" /> 
</s:form>


<script language="javascript" type="text/javascript">
function clear_fields(){
	var i;
		for(i=0;i<=2;i++) {
			document.forms['auditSetupList'].elements[i].value = "";
			document.forms['auditSetupList'].elements['auditAuditable'].value = "";
	}
	document.forms['auditSetupList'].elements['isAlertValue'].value = "";
}
</script>		