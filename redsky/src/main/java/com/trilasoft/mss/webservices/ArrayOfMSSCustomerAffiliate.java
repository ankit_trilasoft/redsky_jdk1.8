
package com.trilasoft.mss.webservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfMSSCustomerAffiliate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfMSSCustomerAffiliate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MSSCustomerAffiliate" type="{http://www.mss1.com/webservices/}MSSCustomerAffiliate" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfMSSCustomerAffiliate", propOrder = {
    "mssCustomerAffiliate"
})
public class ArrayOfMSSCustomerAffiliate {

    @XmlElement(name = "MSSCustomerAffiliate")
    protected List<MSSCustomerAffiliate> mssCustomerAffiliate;

    /**
     * Gets the value of the mssCustomerAffiliate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mssCustomerAffiliate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMSSCustomerAffiliate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MSSCustomerAffiliate }
     * 
     * 
     */
    public List<MSSCustomerAffiliate> getMSSCustomerAffiliate() {
        if (mssCustomerAffiliate == null) {
            mssCustomerAffiliate = new ArrayList<MSSCustomerAffiliate>();
        }
        return this.mssCustomerAffiliate;
    }

}
