<%@ include file="/common/taglibs.jsp"%>   
 <%@ taglib prefix="s" uri="/struts-tags" %> 
<head>   
<script language="JavaScript">
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    
    var http555 = getHTTPObject();
  
 
    
// function for validations.
var form_submitted = false;
function submit_form()
{
  if (form_submitted)
  {
    alert ("Your form has already been submitted. Please wait...");
    return false;
  }
  else
  {
    form_submitted = true;
    return true;
  }
}
</script>
<style type="text/css">
/* collapse */
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
</head>   
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<fmt:formatDate var="UpdatedOnFormattedValue" value="${brokerDetails.createdOn}"pattern="${displayDateTimeEditFormat}"/>
<s:hidden name="brokerDetails.createdOn" value="${UpdatedOnFormattedValue}" />
<s:hidden name="brokerDetails.id" value="${brokerDetails.id}"/>
<s:hidden name="brokerDetails.createdBy" />
<s:hidden name="sid" value="<%=request.getParameter("id")%>"/>
<c:set var="shipNum" value="${serviceOrder.shipNumber}"/>
<s:hidden name="shipNum" id="shipNum" value="${serviceOrder.shipNumber}"/>
<s:hidden name="brokerDetails.serviceOrderId" id="brokerDetails.serviceOrderId" value="%{serviceOrder.id}"/>
<s:hidden name="brokerDetails.corpID" />
<s:hidden name="forwardingAjaxVal" value="Yes"/>
	<s:hidden name="firstDescription" />
	<s:hidden name="secondDescription" />    
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription" />
	<s:hidden name="sixthDescription" />
	<c:set var="field"  value="<%=request.getParameter("field") %>"/>
<s:hidden name="field" id="field" value="<%=request.getParameter("field") %>" />
<s:hidden name="field1" id="field1" value="<%=request.getParameter("field1") %>" />
<c:set var="field1" value="<%=request.getParameter("field1") %>"/>
<div id="Layer1" style="width:100%">
<div id="content" align="center" style="margin-bottom:2px;">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td colspan="2" style="vertical-align: top; padding-top: 6px; padding-right: 5px;">
			<fieldset>
			<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
				<tbody>
					
                     <tr>
                     <td align="right" class="listwhitetext" width="">Vendor&nbsp;Code</td>
					<td style="width:60px"><s:textfield cssStyle="width:65px"  cssClass="input-text"  id="brokerDetailsVendorCode" key="brokerDetails.vendorCode"   required="true" onchange="valid(this,'special');checkBroker();" tabindex=""/>
					<td><img align="left" class="openpopup" width="17" height="20" onclick="openPopWindow();" src="<c:url value='/images/open-popup.gif'/>"  onclick="document.forms['brokerDetailsForm'].elements['brokerDetails.vendorCode'].focus();" id="openpopup9.img"  /></td>
					<td align="right" class="listwhitetext" >Name</td>		
					<td colspan="2">
					<s:textfield cssStyle="width:145px"  cssClass="input-text" id="brokerDetailsVendorName"  key="brokerDetails.vendorName" onchange="findPartnerDetailsByName('brokerDetailsVendorCode','brokerDetailsVendorName');" onkeyup="findPartnerDetails('brokerDetailsVendorName','brokerDetailsVendorCode','brokerDetailsvendorNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','',event);"  required="true"  tabindex=""/>
					<div id="brokerDetailsvendorNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
					</td>					
                     </tr>
                     <tr>
                     <td align="right" class="listwhitetext">Entry&nbsp;Date</td>
					
					<c:if test="${not empty brokerDetails.entryDate}">
						<s:text id="brokerDetailsentryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="brokerDetails.entryDate"/></s:text>
						<td><s:textfield cssClass="input-text" id="entryDate" name="brokerDetails.entryDate" value="%{brokerDetailsentryDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td><img id="entryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty brokerDetails.entryDate}">
						<td><s:textfield cssClass="input-text" id="entryDate" name="brokerDetails.entryDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td><img id="entryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>

					<td align="right" class="listwhitetext">Last changed Date</td>
					<c:if test="${not empty brokerDetails.lastChangeDate}">
						<s:text id="brokerDetailslastChangeDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="brokerDetails.lastChangeDate"/></s:text>
						<td style="width:60px;"><s:textfield cssClass="input-text" id="lastChangeDate" name="brokerDetails.lastChangeDate" value="%{brokerDetailslastChangeDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td><img id="lastChangeDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty brokerDetails.lastChangeDate}">
						<td style="width:60px;"><s:textfield cssClass="input-text" id="lastChangeDate" name="brokerDetails.lastChangeDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td ><img id="lastChangeDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>					
                     </tr>
                     
                     <tr>
                     <td align="right" class="listwhitetext">Quoted</td>
					<td colspan="2"><s:textfield cssStyle="width:65px;text-align: right;"  cssClass="input-text"   key="brokerDetails.quoted"   required="true"  tabindex="" onchange="onlyFloatNewForm(this);"/>
					<td align="right" class="listwhitetext">Quote Ref</td>
					<td colspan="2"><s:textfield cssStyle="width:90px"  cssClass="input-text" key="brokerDetails.quotedRef"   required="true"  tabindex=""/>
					</td>					
                     </tr>
                     
                    <tr>
                     <td align="right" class="listwhitetext">Docs&nbsp;Out&nbsp;Date</td>					
					<c:if test="${not empty brokerDetails.docsOutDate}">
						<s:text id="brokerDetailsdocsOutDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="brokerDetails.docsOutDate"/></s:text>
						<td style="width:60px;"><s:textfield cssClass="input-text" id="docsOutDate" name="brokerDetails.docsOutDate" value="%{brokerDetailsdocsOutDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td><img id="docsOutDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty brokerDetails.docsOutDate}">
						<td style="width:60px;"><s:textfield cssClass="input-text" id="docsOutDate" name="brokerDetails.docsOutDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td ><img id="docsOutDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>
                     </tr>
                     
                     <tr>
                     <td align="right" class="listwhitetext">Duty</td>
					<td colspan="2"><s:textfield cssStyle="width:65px;text-align: right;"  cssClass="input-text"   key="brokerDetails.dutie"   required="true"  tabindex="" onchange="onlyFloatNewForm(this);"/>
					<td></td>	
                     </tr>
                     
                    <tr>
                     <td align="right" class="listwhitetext">Customs&nbsp;Release</td>					
					<c:if test="${not empty brokerDetails.customER}">
						<s:text id="brokerDetailscustomERFormattedValue" name="${FormDateValue}"><s:param name="value" value="brokerDetails.customER"/></s:text>
						<td style="width:60px;"><s:textfield cssClass="input-text" id="customER" name="brokerDetails.customER" value="%{brokerDetailscustomERFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td><img id="customER-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty brokerDetails.customER}">
						<td style="width:60px;"><s:textfield cssClass="input-text" id="customER" name="brokerDetails.customER" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td ><img id="customER-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>
                     </tr>
                     
                     <tr><td style="height:0px;"></td></tr>
                     
                     </tbody>
					</table>
					</fieldset>					
					 <sec-auth:authComponent componentId="module.button.consignee.saveButton">														              
					<s:submit cssClass="cssbuttonA" key="button.save" method="save" cssStyle="width:55px; margin-bottom:5px; margin-top:10px;" />   
					<s:reset cssClass="cssbutton1" key="Reset" cssStyle="width:55px;" tabindex="10"/>
					</sec-auth:authComponent>				
				</td>				
				<td style="padding-right:5px;vertical-align:top;">
			<fieldset>
			<legend>ISF</legend>
			<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
				<tbody>
					
                    <tr>
                     <td align="right" class="listwhitetext">ISF Broker</td>
					<td colspan="2"><s:textfield cssStyle="width:145px"  cssClass="input-text"   key="brokerDetails.isfBroker"   required="true"  tabindex=""/>
					</td>
                     </tr>
                     
                     <tr>
                     <td align="right" class="listwhitetext">ISF Reference #</td>
					<td colspan="2"><s:textfield cssStyle="width:90px"  cssClass="input-text"   key="brokerDetails.isfReference"   required="true"  tabindex=""/>		
					</td>					
                     </tr>
                     <tr>
                     <td align="right" class="listwhitetext">ISF&nbsp;Docs Received</td>
					
					<c:if test="${not empty brokerDetails.isfDocsReceived}">
						<s:text id="brokerDetailsisfDocsReceivedFormattedValue" name="${FormDateValue}"><s:param name="value" value="brokerDetails.isfDocsReceived"/></s:text>
						<td style="width:60px;"><s:textfield cssClass="input-text" id="isfDocsReceived" name="brokerDetails.isfDocsReceived" value="%{brokerDetailsisfDocsReceivedFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td><img id="isfDocsReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty brokerDetails.isfDocsReceived}">
						<td style="width:60px;"><s:textfield cssClass="input-text" id="isfDocsReceived" name="brokerDetails.isfDocsReceived" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td ><img id="isfDocsReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>
                     </tr>
                     <tr>
                     <td align="right" class="listwhitetext">ISF&nbsp;Sent to Broker</td>
					
					<c:if test="${not empty brokerDetails.isfSentBroker}">
						<s:text id="brokerDetailsisfSentBrokerFormattedValue" name="${FormDateValue}"><s:param name="value" value="brokerDetails.isfSentBroker"/></s:text>
						<td style="width:60px;"><s:textfield cssClass="input-text" id="isfSentBroker" name="brokerDetails.isfSentBroker" value="%{brokerDetailsisfSentBrokerFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td><img id="isfSentBroker-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty brokerDetails.isfSentBroker}">
						<td style="width:60px;"><s:textfield cssClass="input-text" id="isfSentBroker" name="brokerDetails.isfSentBroker" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td ><img id="isfSentBroker-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>
                     </tr>
                     <tr>
                     <td align="right" class="listwhitetext">ISF&nbsp;Received From Broker</td>
					
					<c:if test="${not empty brokerDetails.receivedBroker}">
						<s:text id="brokerDetailsreceivedBrokerFormattedValue" name="${FormDateValue}"><s:param name="value" value="brokerDetails.receivedBroker"/></s:text>
						<td style="width:60px;"><s:textfield cssClass="input-text" id="receivedBroker" name="brokerDetails.receivedBroker" value="%{brokerDetailsreceivedBrokerFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td><img id="receivedBroker-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty brokerDetails.receivedBroker}">
						<td style="width:60px;"><s:textfield cssClass="input-text" id="receivedBroker" name="brokerDetails.receivedBroker" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td ><img id="receivedBroker-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>
                     </tr>
                     <tr>
                     <td align="right" class="listwhitetext">ISF&nbsp;Match Record</td>
					<c:if test="${not empty brokerDetails.matchRecord}">
						<s:text id="brokerDetailsmatchRecordFormattedValue" name="${FormDateValue}"><s:param name="value" value="brokerDetails.matchRecord"/></s:text>
						<td style="width:60px;"><s:textfield cssClass="input-text" id="matchRecord" name="brokerDetails.matchRecord" value="%{brokerDetailsmatchRecordFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td><img id="matchRecord-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty brokerDetails.matchRecord}">
						<td style="width:60px;"><s:textfield cssClass="input-text" id="matchRecord" name="brokerDetails.matchRecord" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this);" tabindex="72"/></td>
						<td ><img id="matchRecord-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="surveyDate" HEIGHT=20 WIDTH=20/></td>
					</c:if>
                     </tr>
                     </tbody>
					</table>
				 </fieldset>
				 </td>
				 <td style="vertical-align:top">
			<fieldset>
			<legend>Documents</legend>
			<table style="margin:0 0 5px;" cellspacing="0" cellpadding="3" border="0">
				<tbody>
					
                    <tr>
                    <td align="right"  width="20px"><s:checkbox name="brokerDetails.cf3299" value="${brokerDetails.cf3299}" fieldValue="true" tabindex="2"/></td>
                     <td  class="listwhitetext" align="left" >CF3299</td>	  	   		
                     </tr>
                     
                      <tr>
                    <td align="right"  width="20px"><s:checkbox name="brokerDetails.supplement" value="${brokerDetails.supplement}" fieldValue="true" tabindex="2"/></td>
                     <td  class="listwhitetext" align="left" >Supplemental</td>	  	   		
                     </tr>
                      <tr>
                    <td align="right"  width="20px"><s:checkbox name="brokerDetails.powerOfAttorney" value="${brokerDetails.powerOfAttorney}" fieldValue="true" tabindex="2"/></td>
                     <td  class="listwhitetext" align="left" >Power&nbsp;of&nbsp;Attorney</td>	  	   		
                     </tr>
                      <tr>
                    <td align="right"  width="20px"><s:checkbox name="brokerDetails.passPort" value="${brokerDetails.passPort}" fieldValue="true" tabindex="2"/></td>
                     <td  class="listwhitetext" align="left" >Passport</td>	  	   		
                     </tr>
                      <tr>
                    <td align="right"  width="20px"><s:checkbox name="brokerDetails.visa" value="${brokerDetails.visa}" fieldValue="true" tabindex="2"/></td>
                     <td  class="listwhitetext" align="left" >Visa</td>	  	   		
                     </tr>
                      <tr>
                    <td align="right"  width="20px"><s:checkbox name="brokerDetails.doc194" value="${brokerDetails.doc194}" fieldValue="true" tabindex="2"/></td>
                     <td  class="listwhitetext" align="left" >194</td>	  	   		
                     </tr>                        
                     </tbody>
					</table>
			</fieldset>
			</td>						
		</tr>		
</tbody>
</table>	
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>	
			
</div>
  <script type="text/javascript">
   setCalendarFunctionality();
   
   function openPopWindow(){
		 document.getElementById('typeFormPlace').value="1";
		javascript:openWindow('venderPartners.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=brokerDetails.vendorName&fld_code=brokerDetails.vendorCode');
	} 
   
   var http2 = getHTTPObject();
   function checkBroker(){
	    var vendorId = document.forms['brokerDetailsForm'].elements['brokerDetails.vendorCode'].value;
	    if(vendorId ==''){
	    	document.forms['brokerDetailsForm'].elements['brokerDetails.vendorName'].value='';
	    }
	    if(vendorId !=''){
	    	<configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
		    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=VN&partnerCode=" + encodeURI(vendorId);
		    </configByCorp:fieldVisibility>
		    <configByCorp:fieldVisibility componentId="component.field.vanline">
		    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
		    </configByCorp:fieldVisibility>
		    http2.open("GET", url, true);
		    http2.onreadystatechange = handleHttpResponse90;
		    http2.send(null);
		}	 }
   
	function handleHttpResponse90(){
		 
		if (http2.readyState == 4){
	                var results = http2.responseText
	                results = results.trim();
	                var res = results.split("#"); 
	                if(res.length>2){
	                	if(res[2] == 'Approved'){
	                		document.forms['brokerDetailsForm'].elements['brokerDetails.vendorName'].value = res[1];
	                   		document.forms['brokerDetailsForm'].elements['brokerDetails.vendorCode'].select();
	                	}else{
		           			alert("Vendor code is not approved" ); 
						    document.forms['brokerDetailsForm'].elements['brokerDetails.vendorName'].value="";
						    document.forms['brokerDetailsForm'].elements['brokerDetails.vendorCode'].value="";
						    document.forms['brokerDetailsForm'].elements['brokerDetails.vendorCode'].select();
		           		}  
	               	}else{
	                     alert("Vendor code not valid" );
	                 	 document.forms['brokerDetailsForm'].elements['brokerDetails.vendorName'].value="";
						 document.forms['brokerDetailsForm'].elements['brokerDetails.vendorCode'].value="";
						 document.forms['brokerDetailsForm'].elements['brokerDetails.vendorCode'].select();
	                } } }
	
	function copyPartnerDetails(vendorCode,lastName,vendorNameId,vendorCodeId,autocompleteDivId){
		lastName=lastName.replace("~","'");
		document.getElementById(vendorNameId).value=lastName;
		document.getElementById(vendorCodeId).value=vendorCode;
		document.getElementById(autocompleteDivId).style.display = "none";	
		checkBroker();
			
	}
	function closeMyDiv(autocompleteDivId,vendorNameId,vendorCodeId){	
		document.getElementById(autocompleteDivId).style.display = "none";
		if(document.getElementById(vendorCodeId).value==''){
			document.getElementById(vendorNameId).value="";	
		}
		}
	
	function onlyFloatNewForm(val){
		var value=val.value;
		var pattern = /^\d+.?\d*$/;
		 if(value!=''){
			 var n = value.indexOf(".");
		if(n==0){
				 value=0+value;
				 document.getElementById(val.id).value=value;
			 }
		if (checkFloatForm('brokerDetailsForm','brokerDetails.quoted','Enter valid Number') == false)
        {
           document.forms['brokerDetailsForm'].elements['brokerDetails.quoted'].value='0'; 
           document.forms['brokerDetailsForm'].elements['brokerDetails.quoted'].focus();
           return false;
        }
		if (checkFloatForm('brokerDetailsForm','brokerDetails.dutie','Enter valid Number') == false)
        {
           document.forms['brokerDetailsForm'].elements['brokerDetails.dutie'].value='0'; 
           document.forms['brokerDetailsForm'].elements['brokerDetails.dutie'].focus();
           return false;
        }
		if ( value.match(pattern)==null ){
			document.getElementById(val.id).value="";
			alert("Enter valid Number");
			return false;
		}
		} 
	}
   
	function checkFloatForm(e,t,n){
		  var r;
		  var i=document.forms[e].elements[t];
		  if(i!="undefined"&&i!=null){
			  var s=document.forms[e].elements[t].value;
			  var o=0;
			  for(r=0;r<s.length;r++)
			  {
				  var u=s.charAt(r);if(u==".")
				  {o=o+1}
				  if((u<"0"||u>"9")&&u!="."||o>"1")
				  {
					  alert(n);
					  document.forms[e].elements[t].value="";
					  document.forms[e].elements[t].focus();
					  return false}}}
		  return true;
	  }
	
	
</script>
<script type="text/javascript">
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}" ></c:redirect>
		</c:if>
		}
		catch(e){}
</script>
<script type="text/javascript">
    var fieldName = document.getElementById('field').value;
    var fieldName1 = document.getElementById('field1').value;
    document.forms['brokerDetailsForm'].elements[fieldName].className = 'rules-textUpper'; 
    document.forms['brokerDetailsForm'].elements[fieldName1].className = 'rules-textUpper'; 
 </script> 