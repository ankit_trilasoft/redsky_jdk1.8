ALTER TABLE `redsky`.`scheduleresourceoperation` ADD COLUMN `unit1` VARCHAR(10) AFTER `isDriverUsed`,ADD COLUMN `unit2` VARCHAR(10) AFTER `unit1`;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `basedAt` VARCHAR(8) AFTER `gender`;

ALTER TABLE `redsky`.`billing` ADD COLUMN `billingEmail` VARCHAR(65) AFTER `codAmount`;

ALTER TABLE `redsky`.`billing` ADD COLUMN `insuranceValueEntitleCurrency` VARCHAR(3) AFTER `billingEmail`;

ALTER TABLE `redsky`.`company` ADD COLUMN `customerFeedback` VARCHAR(25) AFTER `weeklyBilling`;

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `sentDate` DATETIME AFTER `est`,ADD COLUMN `returnDate` DATETIME AFTER `sentDate`,ADD COLUMN `qualitySurveyExecutedBy` VARCHAR(25) AFTER `returnDate`,ADD COLUMN `preMoveSurvey` VARCHAR(2) AFTER `qualitySurveyExecutedBy`,ADD COLUMN `originServices` VARCHAR(2) AFTER `preMoveSurvey`,ADD COLUMN `destinationServices` VARCHAR(2) AFTER `originServices`,ADD COLUMN `overAll` VARCHAR(2) AFTER `destinationServices`,ADD COLUMN `recommendation` VARCHAR(2) AFTER `overAll`,ADD COLUMN `general` MEDIUMTEXT AFTER `recommendation`,ADD COLUMN `transferee` MEDIUMTEXT AFTER `general`,ADD COLUMN `internal` MEDIUMTEXT AFTER `transferee`;

INSERT INTO refmaster(corpID, code, description, fieldLength,parameter,createdOn,updatedOn,createdBy,updatedBy) VALUES('TSFT','Key Survey', 'Key Survey', 25, 'QUALITYSURVEYBY',now(),now(),'dkumar','dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','QUALITYSURVEYBY',25,true,now(),'dkumar',now(),'dkumar',1,1,'');

ALTER TABLE `redsky`.`vatIdentification` MODIFY COLUMN `eachBlock` INTEGER(2) UNSIGNED DEFAULT 0;
 
ALTER TABLE `redsky`.`vatIdentification` MODIFY COLUMN `numberOfBlock` VARCHAR(15) DEFAULT '0';

ALTER TABLE `redsky`.`vatIdentification` ADD COLUMN `dataType` VARCHAR(300) AFTER `blockValue`;

ALTER TABLE `redsky`.`vatIdentification` ADD COLUMN `message` VARCHAR(500) AFTER `dataType`;

ALTER TABLE `redsky`.`partnerpublic` ADD COLUMN `transPort` VARCHAR(20) AFTER `UTSmovingCompanyType`;

ALTER TABLE `redsky`.`company` ADD COLUMN `autoGenerateAccRef` VARCHAR(1) AFTER `customerFeedback`;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `minRec` VARCHAR(8) AFTER `transDoc`, ADD COLUMN `maxRec` VARCHAR(8) AFTER `minRec`, ADD COLUMN `minPay` VARCHAR(8) AFTER `maxRec`, ADD COLUMN `maxPay` VARCHAR(8) AFTER `minPay`, ADD COLUMN `suspenseDefault` VARCHAR(8) AFTER `maxPay`, ADD COLUMN `currentRec` VARCHAR(8) AFTER `suspenseDefault`, ADD COLUMN `currentPay` VARCHAR(8) AFTER `currentRec`;

ALTER TABLE `redsky`.`billing` ADD COLUMN `baseInsuranceValue` DECIMAL(19,3) DEFAULT 0.000 AFTER `insuranceValueEntitleCurrency`,ADD COLUMN `insuranceBuyRate` DECIMAL(19,3) DEFAULT 0.000 AFTER `baseInsuranceValue`,ADD COLUMN `baseInsuranceTotal` VARCHAR(15) AFTER `insuranceBuyRate`,ADD COLUMN `exchangeRate` DECIMAL(19,4) DEFAULT 1.000 AFTER `baseInsuranceTotal`,ADD COLUMN `valueDate` DATETIME DEFAULT null AFTER `exchangeRate`;

INSERT INTO refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby) values ('TSFT', 'Dutch', 'Dutch',20, 'LANGUAGE',now(),'dkumar',now(),'dkumar'),('TSFT', 'English', 'English', 20, 'LANGUAGE',now(),'dkumar',now(),'dkumar'),('TSFT', 'French', 'French', 20, 'LANGUAGE',now(),'dkumar',now(),'dkumar'),('TSFT', 'German', 'German', 20, 'LANGUAGE',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','LANGUAGE',20,true,now(),'dkumar',now(),'dkumar',1,1,'');

ALTER TABLE `redsky`.`frequentlyaskedquestions` ADD COLUMN `language` VARCHAR(20) AFTER `partnerId`;

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `originAgentName` VARCHAR(255) AFTER `doNotSendQRequest`, ADD COLUMN `originAgentCode` VARCHAR(25) AFTER `originAgentName`;

ALTER TABLE `redsky`.`billing` MODIFY COLUMN `currency` VARCHAR(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `VIS_assignees` VARCHAR(100) AFTER `SCH_noOfChildren`,ADD COLUMN `VIS_employers` VARCHAR(100) AFTER `VIS_assignees`,ADD COLUMN `VIS_convenant` VARCHAR(100) AFTER `VIS_employers`,ADD COLUMN `VIS_residencePermitHolderNameRP` VARCHAR(100) AFTER `VIS_convenant`,ADD COLUMN `VIS_providerNotificationDateRP` DATETIME AFTER `VIS_residencePermitHolderNameRP`,ADD COLUMN `VIS_visaExpiryDateRP` DATETIME AFTER `VIS_providerNotificationDateRP`,ADD COLUMN `VIS_expiryReminder3MosPriorToExpiryRP` DATETIME AFTER `VIS_visaExpiryDateRP`,ADD COLUMN `VIS_workPermitExpiryRP` DATETIME AFTER `VIS_expiryReminder3MosPriorToExpiryRP`,ADD COLUMN `VIS_questionnaireSentDate` DATETIME AFTER `VIS_workPermitExpiryRP`,ADD COLUMN `VIS_primaryEmail` VARCHAR(65) AFTER `VIS_questionnaireSentDate`,ADD COLUMN `VIS_authorizationLetter` BIT(1) DEFAULT b'0' AFTER `VIS_primaryEmail`,ADD COLUMN `VIS_copyResidence` BIT(1) DEFAULT b'0' AFTER `VIS_authorizationLetter`;
 
ALTER TABLE `redsky`.`dspdetails` CHANGE COLUMN `VIS_workPermitExpiryRP` `VIS_residenceExpiryRP` DATETIME,ADD COLUMN `RNT_paidCurrency` VARCHAR(3) AFTER `VIS_copyResidence`,ADD COLUMN `TRG_employeeTime` VARCHAR(10) AFTER `RNT_paidCurrency`,ADD COLUMN `TRG_spouseTime` VARCHAR(10) AFTER `TRG_employeeTime`,ADD COLUMN `LAN_employeeTime` VARCHAR(10) AFTER `TRG_spouseTime`,ADD COLUMN `LAN_spouseTime` VARCHAR(10) AFTER `LAN_employeeTime`,ADD COLUMN `LAN_childrenTime` VARCHAR(10) AFTER `LAN_spouseTime`;

INSERT INTO refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby) values ('VOER', 'Hours', 'Hours', 20, 'TIMEDURATION',now(),'dkumar',now(),'dkumar'),('VOER', 'Days', 'Days', 20, 'TIMEDURATION',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('VOER','TIMEDURATION',20,true,now(),'dkumar',now(),'dkumar',1,1,'');

ALTER TABLE `redsky`.`workticket` ADD COLUMN `changes` TEXT AFTER `crates`;

ALTER TABLE `redsky`.`contractaccount` ADD COLUMN `parentAccountCode` VARCHAR(8) AFTER `agentCorpID`;

ALTER TABLE `redsky`.`cportalresourcemgmt` ADD COLUMN `language` VARCHAR(20) AFTER `billToExcludes`,ADD COLUMN `infoPackage` VARCHAR(45) AFTER `language`;

INSERT INTO refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby) values('TSFT', 'Introduction', 'Introduction', 45, 'INFOPACKAGE',now(),'dkumar',now(),'dkumar'),('TSFT', 'About Us', 'About Us', 45, 'INFOPACKAGE',now(),'dkumar',now(),'dkumar'),('TSFT', 'Employee Guide', 'Employee Guide', 45, 'INFOPACKAGE',now(),'dkumar',now(),'dkumar'),('TSFT', 'Policy', 'Policy', 45, 'INFOPACKAGE',now(),'dkumar',now(),'dkumar'),('TSFT', 'Insurance', 'Insurance', 45, 'INFOPACKAGE',now(),'dkumar',now(),'dkumar'),('TSFT', 'Quick Process Guide', 'Quick Process Guide', 45, 'INFOPACKAGE',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','INFOPACKAGE',45,true,now(),'dkumar',now(),'dkumar',1,1,'');

ALTER TABLE `redsky`.`reports` ADD COLUMN `mode` VARCHAR(15) AFTER `billToCode`;
 
ALTER TABLE `redsky`.`contractpolicy` ADD COLUMN `sectionName` VARCHAR(20) AFTER `policy`,ADD COLUMN `language` VARCHAR(20) AFTER `sectionName`, ADD COLUMN `fileName` VARCHAR(225) AFTER `language`, ADD COLUMN `fileSize` VARCHAR(25) AFTER `fileName`;

ALTER TABLE `redsky`.`contractpolicy` ADD COLUMN `documentType` VARCHAR(45) AFTER `fileSize`,ADD COLUMN `documentName` VARCHAR(225) AFTER `documentType`;

INSERT INTO refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby) values('TSFT', 'Introduction', 'Introduction', 20, 'SECTIONNAME',now(),'subrat',now(),'subrat'),('TSFT', 'About Us', 'About Us', 20, 'SECTIONNAME',now(),'subrat',now(),'subrat'),('TSFT', 'Employee Guide', 'Employee Guide', 20, 'SECTIONNAME',now(),'subrat',now(),'subrat'),('TSFT', 'Policy', 'Policy', 20, 'SECTIONNAME',
now(),'subrat',now(),'subrat'),('TSFT', 'Insurance', 'Insurance', 20, 'SECTIONNAME',now(),'subrat',now(),'subrat'),('TSFT', 'Quick Process Guide', 'Quick Process Guide', 20, 'SECTIONNAME',now(),'subrat',now(),'subrat');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,
customType) VALUES('TSFT','SECTIONNAME',20,true,now(),'subrat',now(),'subrat',1,1,'');

ALTER TABLE `redsky`.`contractpolicy` MODIFY COLUMN `id` BIGINT(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `redsky`.`company` ADD COLUMN `TransfereeInfopackage` bit(1) DEFAULT false ;

CREATE TABLE `redsky`.`policydocument` (
  `id` BIGINT(20) UNSIGNED AUTO_INCREMENT,
  `documentName` VARCHAR(225),
  `fileSize` VARCHAR(25),
  `corpID` VARCHAR(15),
  `createdBy` VARCHAR(82),
  `createdOn` DATETIME,
  `updatedBy` VARCHAR(82),
  `updatedOn` DATETIME,
  `partnerCode`VARCHAR(8),
  `description` VARCHAR(375),
  `file` TINYBLOB,
  `sectionName` VARCHAR(20),
  `language` VARCHAR(20),
  PRIMARY KEY(`id`)
)
ENGINE = InnoDB;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `basedAtName` VARCHAR(225) AFTER `basedAt`;

ALTER TABLE `redsky`.`accountcontact` ADD COLUMN `userName` VARCHAR(25) AFTER `leadSourceOther`;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `ratingScale` VARCHAR(45) AFTER `oneRequestPerCF`;

ALTER TABLE `redsky`.`contract` ADD COLUMN `published` VARCHAR(2) AFTER `origContract`;



Please take giten sir help to execute below work

DROP VIEW IF EXISTS `redsky`.`partner`;
CREATE VIEW  `redsky`.`partner` AS select `a`.`id` AS `id`,`a`.`parent` AS `parent`,`a`.`middleInitial` AS `middleInitial`,`a`.`billingAddress1` AS `billingAddress1`,`a`.`billingAddress2` AS `billingAddress2`,`a`.`billingAddress3` AS `billingAddress3`,`a`.`billingAddress4` AS `billingAddress4`,`a`.`billingCity` AS `billingCity`,`a`.`billingCountry` AS `billingCountry`,`a`.`billingCountryCode` AS `billingCountryCode`,`a`.`billingEmail` AS `billingEmail`,`a`.`billingFax` AS `billingFax`,`a`.`billingPhone` AS `billingPhone`,`a`.`billingState` AS `billingState`,`a`.`billingTelex` AS `billingTelex`,`a`.`billingZip` AS `billingZip`,`a`.`effectiveDate` AS `effectiveDate`,`a`.`isAccount` AS `isAccount`,`a`.`isAgent` AS `isAgent`,`a`.`isCarrier` AS `isCarrier`,`a`.`isVendor` AS `isVendor`,`a`.`mailingAddress1` AS `mailingAddress1`,`a`.`mailingAddress2` AS `mailingAddress2`,`a`.`mailingAddress3` AS `mailingAddress3`,`a`.`mailingAddress4` AS `mailingAddress4`,`a`.`mailingCity` AS `mailingCity`,`a`.`mailingCountry` AS `mailingCountry`,`a`.`mailingCountryCode` AS `mailingCountryCode`,`a`.`mailingEmail` AS `mailingEmail`,`a`.`mailingFax` AS `mailingFax`,`a`.`mailingPhone` AS `mailingPhone`,`a`.`mailingState` AS `mailingState`,`a`.`mailingTelex` AS `mailingTelex`,`a`.`mailingZip` AS `mailingZip`,`a`.`partnerPrefix` AS `partnerPrefix`,`a`.`partnerSuffix` AS `partnerSuffix`,`a`.`terminalAddress1` AS `terminalAddress1`,`a`.`terminalAddress2` AS `terminalAddress2`,`a`.`terminalAddress3` AS `terminalAddress3`,`a`.`terminalAddress4` AS `terminalAddress4`,`a`.`terminalCity` AS `terminalCity`,`a`.`terminalCountry` AS `terminalCountry`,`a`.`terminalCountryCode` AS `terminalCountryCode`,`a`.`terminalEmail` AS `terminalEmail`,`a`.`terminalFax` AS `terminalFax`,`a`.`terminalPhone` AS `terminalPhone`,`a`.`terminalState` AS `terminalState`,`a`.`terminalTelex` AS `terminalTelex`,`a`.`terminalZip` AS `terminalZip`,`a`.`isPrivateParty` AS `isPrivateParty`,`a`.`air` AS `air`,`a`.`sea` AS `sea`,`a`.`surface` AS `surface`,`a`.`isOwnerOp` AS `isOwnerOp`,`a`.`typeOfVendor` AS `typeOfVendor`,`a`.`agentParent` AS `agentParent`,`a`.`location1` AS `location1`,`a`.`location2` AS `location2`,`a`.`location3` AS `location3`,`a`.`location4` AS `location4`,`a`.`companyProfile` AS `companyProfile`,`a`.`url` AS `url`,`a`.`latitude` AS `latitude`,`a`.`longitude` AS `longitude`,`a`.`trackingUrl` AS `trackingUrl`,`a`.`yearEstablished` AS `yearEstablished`,`a`.`companyFacilities` AS `companyFacilities`,`a`.`companyCapabilities` AS `companyCapabilities`,`a`.`companyDestiantionProfile` AS `companyDestiantionProfile`,`a`.`serviceRangeKms` AS `serviceRangeKms`,`a`.`serviceRangeMiles` AS `serviceRangeMiles`,`a`.`fidiNumber` AS `fidiNumber`,`a`.`OMNINumber` AS `OMNINumber`,`a`.`IAMNumber` AS `IAMNumber`,`a`.`AMSANumber` AS `AMSANumber`,`a`.`WERCNumber` AS `WERCNumber`,`a`.`facilitySizeSQFT` AS `facilitySizeSQFT`,`a`.`qualityCertifications` AS `qualityCertifications`,`a`.`vanLineAffiliation` AS `vanLineAffiliation`,`a`.`serviceLines` AS `serviceLines`,`a`.`facilitySizeSQMT` AS `facilitySizeSQMT`,`b`.`corpid` AS `corpid`,`b`.`warehouse` AS `warehouse`,`a`.`createdBy` AS `createdBy`,`a`.`createdOn` AS `createdOn`,`a`.`updatedBy` AS `updatedBy`,`a`.`updatedOn` AS `updatedOn`,`a`.`firstName` AS `firstName`,`a`.`lastName` AS `lastName`,if((`a`.`isPrivateParty` is true),`a`.`status`,`b`.`status`) AS `status`,`a`.`nextVanLocation` AS `nextVanLocation`,`a`.`nextReportOn` AS `nextReportOn`,`b`.`billingInstruction` AS `billingInstruction`,`b`.`billingInstructionCode` AS `billingInstructionCode`,`b`.`coordinator` AS `coordinator`,`b`.`salesMan` AS `salesMan`,`b`.`qc` AS `qc`,`b`.`abbreviation` AS `abbreviation`,`b`.`billPayOption` AS `billPayOption`,`b`.`billPayType` AS `billPayType`,`b`.`billToGroup` AS `billToGroup`,`b`.`longPercentage` AS `longPercentage`,`b`.`needAuth` AS `needAuth`,`a`.`partnerCode` AS `partnerCode`,`b`.`storageBillingGroup` AS `storageBillingGroup`,`b`.`accountHolder` AS `accountHolder`,`b`.`paymentMethod` AS `paymentMethod`,`b`.`payOption` AS `payOption`,`b`.`multiAuthorization` AS `multiAuthorization`,`b`.`payableUploadCheck` AS `payableUploadCheck`,`b`.`companyDivision` AS `companyDivision`,`b`.`invoiceUploadCheck` AS `invoiceUploadCheck`,`b`.`billingUser` AS `billingUser`,`b`.`payableUser` AS `payableUser`,`b`.`pricingUser` AS `pricingUser`,`b`.`partnerPortalActive` AS `partnerPortalActive`,`b`.`partnerPortalId` AS `partnerPortalId`,`b`.`associatedAgents` AS `associatedAgents`,`b`.`viewChild` AS `viewChild`,`b`.`creditTerms` AS `creditTerms`,`b`.`accountingDefault` AS `accountingDefault`,`b`.`acctDefaultJobType` AS `acctDefaultJobType`,`b`.`stopNotAuthorizedInvoices` AS `stopNotAuthorizedInvoices`,`b`.`doNotCopyAuthorizationSO` AS `doNotCopyAuthorizationSO`,`b`.`partnerPublicId` AS `partnerPublicId`,`b`.`driverAgency` AS `driverAgency`,`b`.`validNationalCode` AS `validNationalCode`,`b`.`cardNumber` AS `cardNumber`,`b`.`cardStatus` AS `cardStatus`,`b`.`accountId` AS `accountId`,`b`.`customerId` AS `customerId`,`b`.`licenseNumber` AS `licenseNumber`,`b`.`licenseState` AS `licenseState`,`b`.`cardSettelment` AS `cardSettelment`,`b`.`directDeposit` AS `directDeposit`,`b`.`fuelPurchase` AS `fuelPurchase`,`b`.`expressCash` AS `expressCash`,`b`.`creditCheck` AS `creditCheck`,`b`.`creditAmount` AS `creditAmount`,`b`.`creditCurrency` AS `creditCurrency`,`b`.`yruAccess` AS `yruAccess`,`b`.`taxId` AS `taxId`,`b`.`taxIdType` AS `taxIdType`,`b`.`extReference` AS `extReference`,`a`.`startDate` AS `startDate`,`b`.`accountManager` AS `accountManager`,`b`.`classcode` AS `classcode`,`b`.`vatNumber` AS `vatNumber`,`a`.`utsNumber` AS `utsNumber`,`a`.`isNetworkPartner` AS `isNetworkPartner`,`a`.`vanLastLocation` AS `vanLastLocation`,`a`.`vanLastReportOn` AS `vanLastReportOn`,`a`.`vanAvailCube` AS `vanAvailCube`,`a`.`vanLastReportTime` AS `vanLastReportTime`,`a`.`currentVanAgency` AS `currentVanAgency`,`a`.`currentVanID` AS `currentVanID`,`a`.`currentTractorAgency` AS `currentTractorAgency`,`a`.`currentTractorID` AS `currentTractorID`,`b`.`driverType` AS `driverType`,`b`.`noDispatch` AS `noDispatch`,`a`.`aliasName` AS `aliasName`,`a`.`networkGroup` AS `networkGroup`,`b`.`insuranceAuthorized` AS `insuranceAuthorized`,`b`.`mc` AS `mc` from (`partnerpublic` `a` join `partnerprivate` `b`) where (`a`.`id` = `b`.`partnerPublicId`);

Please add menu item �Account Details�
under Menu �Move Management�
 *only �ACCOUNT� users the ability to view info related to their account.
Menu Name:- Account Details
Url:-  /partnerDetailsPage.html

please add menu item �Role transfer� under �Security� Drop-down
URL:-       /roleTransferList.html





