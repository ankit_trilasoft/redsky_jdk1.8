delimiter $$

CREATE trigger redsky.trigger_add_history_userdatasecurity

after update on redsky.userdatasecurity

for each row

BEGIN

  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;
  DECLARE oldCorpid LONGTEXT;
  DECLARE newUser LONGTEXT;
  DECLARE newRoleName LONGTEXT;

  SET fieldNameList = " "; 
  SET oldValueList = " ";
  SET newValueList = " ";
  SET oldCorpid = " ";
  SET newUser = " ";
  SET newRoleName = " ";

select corpid into oldCorpid from datasecurityset where id=NEW.userdatasecurity_id limit 1;
if(old.updatedby is null and new.updatedby is not null) then
IF (NEW.userdatasecurity_id <> '' or NEW.userdatasecurity_id is not null) THEN
    select name into newRoleName from datasecurityset where id=NEW.userdatasecurity_id;
       select CONCAT(fieldNameList,'userdatasecurity.userdatasecurity_id~') into fieldNameList;
       select CONCAT(newValueList,newRoleName,'~') into newValueList;
   END IF;
   END IF;
CALL add_tblHistory (NEW.user_id,"userdatasecurity", fieldNameList, oldValueList, newValueList, NEW.updatedby, oldCorpid, now());


END $$
delimiter;
