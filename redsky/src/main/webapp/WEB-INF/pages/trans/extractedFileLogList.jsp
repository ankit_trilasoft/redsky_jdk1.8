<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<head> 
<title>Extracted File Log</title> 
<meta name="heading" content="Extracted File Log"/> 
<style type="text/css">
div.exportlinks {margin:-5px 0px 10px 10px;	padding:2px 4px 2px 0px;!padding:2px 4px 18px 0px;	width:100%;	}
form {margin-top:0px;!margin-top:-5px;}
div#main {margin:-5px 0 0;}
.table td, .table th, .tableHeaderTable td {padding: 0.4em;}
.table th.alignCenter a {text-align: center;}
#overlay11 {filter:alpha(opacity=70);-moz-opacity:0.7;-khtml-opacity: 0.7;opacity: 0.7;position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png); }
span.pagelinks {display:block;font-size:0.95em;margin-bottom:3px;margin-top:-18px;padding:2px 0px;text-align:right;width:100%;}
div.error, span.error, li.error, div.message {width:450px;}
</style>
 <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
</head>  
<s:form id="extractedFileLogList" name="extractedFileLogList" action="searchExtractedFileLog" method="post" validate="true">   
<s:hidden id="checkExtractedFileLogClick" name="checkExtractedFileLogClick"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<div id="Layer1" style="width:100%;">
<div id="otabs"><ul><li><a class="current"><span>Search</span></a></li></ul> </div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:12px;"><span></span></div>
   <div class="center-content">
<table class="table">
	<thead>
	  <tr>
		<th>Module</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>From Extract Date</th>
		<th>To Extract Date</th>  
	  </tr>
	</thead> 
	
	<tbody>
	  	<tr>
		   <td><s:textfield name="moduleName" id="extractedFileLogCount"  required="true" cssClass="input-text" size="25" onblur="searchExtractedFileLogDays();"/></td>
		   <td><s:textfield name="extractCreatedbyFirstName"  required="true" cssClass="input-text" size="25"/></td> 
		   <td><s:textfield name="extractCreatedbyLastName"  required="true" cssClass="input-text" size="25"/></td> 
		   <c:if test="${not empty fromExtractDate}">
						<s:text id="fromExtractDate" name="${FormDateValue}"> <s:param name="value" value="fromExtractDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="fromExtractDate" name="fromExtractDate" value="%{fromExtractDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="fromExtractDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="searchExtractedFileLogforDays();"/></td>
		   </c:if>
		   <c:if test="${empty fromExtractDate}" >
						<td ><s:textfield cssClass="input-text" id="fromExtractDate" name="fromExtractDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="fromExtractDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="searchExtractedFileLogforDays();"/></td>
		   </c:if> 
		   <c:if test="${not empty toExtractDate}">
						<s:text id="toExtractDate" name="${FormDateValue}"> <s:param name="value" value="toExtractDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="toExtractDate" name="toExtractDate" value="%{toExtractDate}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="toExtractDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="searchExtractedFileLogforDays();"/></td>
		   </c:if>
		   <c:if test="${empty toExtractDate}" >
						<td><s:textfield cssClass="input-text" id="toExtractDate" name="toExtractDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="toExtractDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="searchExtractedFileLogforDays();"/></td>
		   </c:if> 
		   </tr>
		   <tr>
		   <td colspan="4"></td>
		
		   <td style="border-left: hidden;">
		       <s:submit cssClass="cssbutton1" cssStyle="width:55px;" key="button.search" onclick="return checkDate();"/>  
		       <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/>     
		   </td>
	  	</tr>
	</tbody>
 </table>
 	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>


<div id="otabs">
	<ul><li><a class="current"><span>Extracted File Log</span></a></li></ul>
</div>
<div class="spnblk">&nbsp;</div>
	<display:table name="extractedFileLogSet" class="table" requestURI="" id="extractedFileLogSet"  defaultsort="3" export="false" pagesize="10" style="margin-top:1px;"> 
			<display:column property="module"  sortable="true"  title="Module" style="width:130px"/> 
			<display:column property="createdBy" sortable="true" title="User" style="width:130px"/> 
            <display:column title="Extract date" style="width:130px" sortable="true" sortProperty="createdOn" >
            <fmt:formatDate value="${extractedFileLogSet.createdOn}" pattern="${displayDateTimeFormat}"/>
            </display:column>
	        <display:column title="File Name"   sortProperty="fileName" style="width:130px">
				<a onclick="downloadSelectedFile('${extractedFileLogSet.id}');">
				<c:out value="${fn:substring(extractedFileLogSet.fileName, 0, fn:indexOf(extractedFileLogSet.fileName,'_'))}" escapeXml="false"/></a>
		    </display:column>	
	</display:table> 
 </div>
<c:out value="${buttons}" escapeXml="false" /> 
</s:form>

<script type="text/javascript">
	setOnSelectBasedMethods(["searchExtractedFileLogDays()"]);
	setCalendarFunctionality();
	showOrHide(0);
</script>
<script language="javascript" type="text/javascript">
function searchExtractedFileLogforDays(){
	 document.forms['extractedFileLogList'].elements['checkExtractedFileLogClick'].value = '1';
	}
function clear_fields(){ 
    document.forms['extractedFileLogList'].elements['moduleName'].value = "";
	document.forms['extractedFileLogList'].elements['extractCreatedbyFirstName'].value = "";
	document.forms['extractedFileLogList'].elements['extractCreatedbyLastName'].value = ""; 
	document.forms['extractedFileLogList'].elements['fromExtractDate'].value = "";
	document.forms['extractedFileLogList'].elements['toExtractDate'].value = "";
}
function checkDate() { 
 var fromExtractDate = document.forms['extractedFileLogList'].elements['fromExtractDate'].value;
 var toExtractDate = document.forms['extractedFileLogList'].elements['toExtractDate'].value;
 var module = document.forms['extractedFileLogList'].elements['moduleName'].value;
 module=module.trim();
 var createdBy = document.forms['extractedFileLogList'].elements['extractCreatedbyFirstName'].value;
 createdBy=createdBy.trim();
 var createdByLastName = document.forms['extractedFileLogList'].elements['extractCreatedbyLastName'].value;
 createdByLastName=createdByLastName.trim();
 if(module=='' && createdBy=='' && createdByLastName==''){ 
	    if(fromExtractDate==''&& toExtractDate!=''){
	    	alert('Please select From Extract Date ');
	    	return false;
	    } 
	    if(fromExtractDate!='' && toExtractDate=='')
	    {
	    	alert('Please select the To Extract Date');
	    	return false;
	    } 
	   return true; 
	} 
    return true; 
}

function searchExtractedFileLogDays()
{ 
 var date1 = document.forms['extractedFileLogList'].elements['fromExtractDate'].value;	 
 var date2 = document.forms['extractedFileLogList'].elements['toExtractDate'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  if(daysApart<0)
  {
    alert("To Extract Date should be greater than From Extract Date.");
    document.forms['extractedFileLogList'].elements['toExtractDate'].value='';
  } 
  else{
  	if(document.forms['extractedFileLogList'].elements['toExtractDate'].value == ''){
  		document.forms['extractedFileLogList'].elements['toExtractDate'].value = document.forms['extractedFileLogList'].elements['fromExtractDate'].value;
  	}
  }
 	document.forms['searchForm'].elements['checkExtractedFileLogClick'].value = '';
}


function downloadSelectedFile(id){ 
	var url=""; 
	url="ExtractedFileServletAction.html?id="+id; 
	location.href=url;
}
</script>		