<%@ include file="/common/taglibs.jsp"%>  
<head>   
<title><fmt:message key="vanLineList.title"/></title>   
<meta name="heading" content="<fmt:message key='vanLineList.heading'/>"/>   
<script language="javascript" type="text/javascript">
function clear_fields(){
	var i;
		for(i=0;i<=1;i++){
			document.forms['searchForm'].elements['vanLine.agent'].value = "";
			document.forms['searchForm'].elements['vanLine.weekEnding'].value = "";			
			document.forms['searchForm'].elements['vanLine.glType'].value = "";
		}
}

</script>
<style>
	<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script language="JavaScript" type="text/javascript">
	var cal = new CalendarPopup(); 
	cal.showYearNavigation(); 
	cal.showYearNavigationInput();  
</script>

<style>
 span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:95%;
!width:95%;
}
</style>
</head>   
  
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editVanLine.html"/>'"  value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px" align="top" method="searchVanLine" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>   
<%-- <c:choose>
<c:when test="${chkWith=='yes'}">
<c:set var="url" value="<c:url value='/vanLineListWithReconcile.html?agent=${agent}&weekEnding=${weekEnding}'/>"/> 
</c:when>
<c:otherwise>

<c:set var="url" value="<c:url value='/vanLineListWithoutReconcile.html?agent=${agent}&weekEnding=${weekEnding}'/>"/>
</c:otherwise>
</c:choose> --%>
     
<s:form id="searchForm" action="searchVanLine" method="post" validate="true" >
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<c:set var="totalamt" value="0.00"/>
<c:set var="totalamt1" value="0.00"/>
<c:set var="totalamt111" value="0.00"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="agent" value="<%=request.getParameter("agent") %>"/>
<c:set var="agent" value="<%=request.getParameter("agent") %>"/>
<s:hidden name="weekEnding" value="<%=request.getParameter("weekEnding") %>"/>
<c:set var="weekEnding" value="<%=request.getParameter("weekEnding") %>"/> 
		<div id="newmnav">
		  <ul>
		  <li>
		  <c:choose>
             <c:when test="${chkWith=='yes'}">
		  <a href="<c:url value='/vanLineListWithReconcile.html?chkWith=${chkWith}&agent=${agent}&weekEnding=${weekEnding}'/>"><span>Order Settlement</span></a>
		    </c:when>
		    <c:otherwise>
             <a href="<c:url value='/vanLineListWithoutReconcile.html?chkWith=${chkWith}&agent=${agent}&weekEnding=${weekEnding}'/>"><span>Order Settlement</span></a>
            </c:otherwise>
          </c:choose>  
		  </li>
		      <li>
		        <c:choose>
             <c:when test="${chkWith=='yes'}">
		  <a href="<c:url value='/miscellaneousWithSettlementList.html?chkWith=${chkWith}&agent=${agent}&weekEnding=${weekEnding}'/>"><span>Miscellaneous Settlement</span></a>
		    </c:when>
		    <c:otherwise>
             <a href="<c:url value='/miscellaneousWithoutSettlementList.html?chkWith=${chkWith}&agent=${agent}&weekEnding=${weekEnding}'/>"><span>Miscellaneous Settlement</span></a>
            </c:otherwise>
          </c:choose> 		      
		      </li>
		      <li id="newmnav1" style="background:#FFF "><a class="current"><span>Category Settlement<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  	
		  </ul>
</div><div class="spn" style="!margin-bottom:0px;">&nbsp;</div>
      <!--<input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/orderSettlementList.html"/>'"  value="Order Settlement"/> 
        <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/miscellaneousSettlementList.html"/>'"  value="Miscellaneous Settlement"/> 
   <s:set name="vanLineList" value="vanLineList" scope="request"/>   
     -->
<display:table name="vanLineList" class="table" requestURI="" id="vanLineList" export="false"  pagesize="50" style="width:100%;margin-bottom:0px;" >   
<%--     <display:column property="statementCategory" sortable="true" titleKey="vanLine.statementCategory" style="width:80px" /> --%>	      
	<display:column  style="width:10px">	
	<img id="cat-${vanLineList_rowNum}" onclick ="displayResult('${vanLineList.statementCategory}','${vanLineList.agent}','${vanLineList.weekEnding }',this,this.id);" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
	</display:column>
	<display:column property="statementCategory" sortable="true" titleKey="vanLine.statementCategory" style="text-align:center;width:60px;">
	<%-- <a href="editMiscellaneousVanLine.html?id=${vanLineList.id}" target="_blank" ><c:out value="${vanLineList.statementCategory}" /></a>  --%>
	</display:column>	
	<display:column property="statementDescription" sortable="true" titleKey="vanLine.description" style="width:300px"/>
	<display:column property="agent" sortable="true" titleKey="vanLine.agent" style="width:80px" />  
	
	<display:column property="weekEnding" sortable="true" titleKey="vanLine.weekEnding" style="width:30px" format="{0,date,dd-MMM-yyyy}" />	
	
	<display:column   sortable="true"  titleKey="vanLine.amtDueAgent" style="text-align: right; width:80px;"  > 
	<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${vanLineList.amtDueAgent}"  /> </display:column>
    
     <c:if test="${flagvanLineData ne null && flagvanLineData=='1'}">
	 <c:set var="totalamt" value="${(vanLineList.totalAllStatusAmount)}"/>
	 </c:if> 
	 <c:if test="${flagvanLineData ne null && flagvanLineData=='1'}">
	<c:set var="totalamt1" value="${(vanLineList.reconcileStatusAmount)}"/> 
	</c:if>
	<c:if test="${flagvanLineData ne null && flagvanLineData=='1'}">
	<c:set var="totalamt111" value="${(vanLineList.suspenseStatusAmount)}"/> 
	</c:if> 
	<display:column  sortable="true" titleKey="vanLine.reconcileAmount" style="text-align: right; width:80px;" ><fmt:formatNumber type="number" 
           maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${vanLineList.reconcileAmount}" />	</display:column>	
	
    <display:setProperty name="paging.banner.item_name" value="vanLine"/>   
    <display:setProperty name="paging.banner.items_name" value="vanLines"/>    
    <display:setProperty name="export.excel.filename" value="Van Line List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Van Line List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Van Line List.pdf"/>     
</display:table>
 <div style="background:url(images/bg_listheader.png)repeat-x scroll 0 0 #BCD2EF;height:53px;font-size:13px;font-weight:bold;margin-bottom:5px;border:2px solid #74B3DC;border-top:none;">
 	<table width="100%">
    <tr>
     <td colspan="4" align="right" class="tdheight-footer" style="border-right:hidden;">&nbsp;</td>     
     <td align="right" style="padding:0px;width:77%;" class="tdheight-footer">Amount(All Statuses)&nbsp;</td>
     <td class="tdheight-footer" width="12%" align="right" style="padding-right:14px;">
     <%-- <fmt:formatNumber type="number" 
            maxFractionDigits="3" value="${totalamt}" /> --%>
             <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalamt}"  />
            </td>  	     
     <td align="right" width="12%" class="tdheight-footer"></td>  
   </tr>
    <tr>
     <td colspan="4" align="right" class="tdheight-footer" style="border-right:hidden;">&nbsp;</td>     
     <td align="right" style="padding:0px;width:77%;" class="tdheight-footer">Amount(Reconciled/Approved)&nbsp;</td>
     <td class="tdheight-footer" width="12%" align="right" style="padding-right:14px;">
    <%--  <fmt:formatNumber type="number" 
            maxFractionDigits="3" value="${totalamt1}" /> --%>
            <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalamt1}"  />
            </td>  	     
     <td align="right" width="12%" class="tdheight-footer"></td>  
   </tr>
    <tr>
     <td colspan="4" align="right" class="tdheight-footer" style="border-right:hidden;">&nbsp;</td>     
     <td align="right" style="padding:0px;width:77%;" class="tdheight-footer">Amount(Suspense)&nbsp;</td>
     <td class="tdheight-footer" width="12%" align="right" style="padding-right:14px;">
    <%--  <fmt:formatNumber type="number" 
            maxFractionDigits="3" value="${totalamt1}" /> --%>
            <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalamt111}"  />
            </td>  	     
     <td align="right" width="12%" class="tdheight-footer"></td>  
   </tr>
   </table> 
    </div>
<c:out value="${buttons}" escapeXml="false" />   
<c:set var="isTrue" value="false" scope="session"/>
</s:form>  
 <script type="text/javascript">
var flagValue = 0;
var val = 0;
var lastId ='';
var flagValue1 = 0;
var val1 = 0;
var lastId1 ='';
function displayResult(regNum,agent,week,position,id)
{
	//alert(regNum);
	//alert(agent);
	//alert(week);
	//alert(position);
	//alert(id);
	  regNum = regNum.replace( /&/g, '%26' );
	 if(flagValue==0){
		var table=document.getElementById("vanLineList");
		var rownum = document.getElementById(id);
		lastId = rownum.id;
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val = newrow+1;
		var row=table.insertRow(val);
		row.setAttribute("id",val);
		//alert(flagValue);
	   // alert(val);
		var url='vanLineCategoryRegNum.html?vanRegNum='+regNum+'&agent='+agent+'&weekEnd='+week+'&decorator=simple&popup=true';
		//alert(url);
		
		new Ajax.Request(url,
				  {
			
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				     // alert(response);
				      var container = $(regNum);
				      container.update(response);
				      container.show();
				    },
				    onFailure: function(){
					    //alert('Something went wrong...') 
					    }
				  });
		
		     row.innerHTML = "<td colspan=\"7\"><div id="+regNum+"></div></td>";
		     flagValue = 1;
		     flagValue1 = 0;
			 val1 = 0;
			 lastId1 ='';
		     return 0;
	}
	if(flagValue==1){
	//alert(flagValue);
	//alert(val);
		document.getElementById(val).parentNode.removeChild(document.getElementById(val));
		flagValue = 0;
		var table=document.getElementById("vanLineList");
		var rownum = document.getElementById(id);
		if(lastId!=rownum.id){
		lastId = rownum.id;
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val = newrow+1;
		var row=table.insertRow(val);
		row.setAttribute("id",val);
		
		
		var url='vanLineCategoryRegNum.html?vanRegNum='+regNum+'&agent='+agent+'&weekEnd='+week+'&decorator=simple&popup=true';
		//alert(url);
		
		new Ajax.Request(url,
				  {
			
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				    // alert(response);
				      var container = $(regNum);
				      container.update(response);
				      container.show();
				    },
				    onFailure: function(){ 
				    	//alert('Something went wrong...')
				    	}
				  });
		
		     row.innerHTML = "<td colspan=\"7\"><div id="+regNum+"></div></td>";
		     flagValue = 1;
		}
		flagValue1 = 0;
		 val1 = 0;
		 lastId1 ='';
	     return 1;
	} 
 flagValue1 = 0;
	 val1 = 0;
	 lastId1 ='';
			
}



function displayResultList(regNum,statementCategory,agent,week,position,id)
{
	//alert(statementCategory);
	//alert(regNum);
	//alert(agent);
	//alert(week);
	//alert(position);
	//alert(id);
	 if(flagValue1==0){
		var table=document.getElementById("vanLineRegNumList");
		//var id1=id.replace("reg-",""); 
		var id1=id; 
		var rownum = document.getElementById(id1);
		lastId1 = rownum.id;
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val1 = newrow+1;
		var row=table.insertRow(val1);
		var newId = "reg-"+val1+"-T";
		//alert(flagValue1);
		//alert(newId);
		row.setAttribute("id",newId);
		var url1='vanLineCategoryRegNumberList.html?statementCategory='+statementCategory+'&vanRegNum='+regNum+'&agent='+agent+'&weekEnd='+week+'&decorator=simple&popup=true';
		//alert(url1);
		
		new Ajax.Request(url1,
				  {
			
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				     // alert(response);
				      var container = $(regNum);
				      container.update(response);
				      container.show();
				    },
				    onFailure: function(){
					   // alert('Something went wrong...') 
					    }
				  });
		
		     row.innerHTML = "<td colspan=\"10\"><div id="+regNum+"></div></td>";
		     flagValue1 = 1;
		     return 0;
	}
	if(flagValue1==1){
		var newId = "reg-"+val1+"-T";
		//alert(flagValue1);
		//alert(newId);
		//alert(document.getElementById(newId));
		document.getElementById(newId).parentNode.removeChild(document.getElementById(newId));	
		flagValue1 = 0;
		var table=document.getElementById("vanLineRegNumList");
		//var id1=id.replace("reg-","");
		var id1=id; 
		var rownum = document.getElementById(id1);
		if(lastId1!=rownum.id){
		lastId1 = rownum.id;
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val1 = newrow+1;
		var row=table.insertRow(val1);
		newId = "reg-"+val1+"-T";
		
		row.setAttribute("id",newId);
		var url1='vanLineCategoryRegNumberList.html?statementCategory='+statementCategory+'&vanRegNum='+regNum+'&agent='+agent+'&weekEnd='+week+'&decorator=simple&popup=true';
		//alert(url);
		
		new Ajax.Request(url1,
				  {
			
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				     //alert(response);
				      var container = $(regNum);
				      container.update(response);
				      container.show();
				    },
				    onFailure: function(){ 
				    	//alert('Something went wrong...')
				    	}
				  });
		
		     row.innerHTML = "<td colspan=\"10\"><div id="+regNum+"></div></td>";
		     flagValue1 = 1;
		}
	     return 1;
	} 
			
}


</script>
<script type="text/javascript">      
      function setUrl(aref,id11){
    	 var  chkComp="";
    	  <configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
    	  chkComp="YES";
    	  </configByCorp:fieldVisibility>
    	 // var id11='${subcontractorChargesList1.id}';
    	 // alert(chkComp);
    	 // alert(id11);
    	 window.open('editVanLine.html?id='+id11+'&glCodeFlag='+chkComp+'&chkcategorypage=yes&decorator=popup&popup=true',800,600);
    	  
      }
     
      
      </script>
      <script type="text/javascript">      
      function setUrl1(aref,id11){
    	 var  chkComp="";
    	 //window.open('editVanLine.html?id=${vanLineRegNumList.id}&chkcategorypage=yes&decorator=popup&popup=true',800,600)
    	  <configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
    	  chkComp="YES";
    	  </configByCorp:fieldVisibility>
    	 // var id11='${subcontractorChargesList1.id}';
    	 // alert(chkComp);
    	 // alert(id11);
    	 window.open('editVanLine.html?id='+id11+'&glCodeFlag='+chkComp+'&chkcategorypage=yes&decorator=popup&popup=true',800,600);
    	  
      }
     
      
      </script>
