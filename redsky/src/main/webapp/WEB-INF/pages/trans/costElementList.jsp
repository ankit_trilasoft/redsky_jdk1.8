<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="/common/tooltip.jsp"%>
<head>   
    <title>Cost Element List</title>   
    <meta name="heading" content="Cost Element List"/>   

<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<style>
span.pagelinks {display:block;font-size:0.9em;margin-bottom:3px;margin-top:-9px;!margin-top:-20px;padding:2px 0px;text-align:right;width:100%;
}
</style>

</head>
<s:form id="costElementListPage" action="searchCostElementList" method="post" validate="true">
<div id="layer1" style="width:100%;">
<div id="newmnav" style="!padding-bottom:5px;">
	  <ul>
	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>	  	
	  </ul>
</div>
<div class="spn">&nbsp;</div> 	
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px;" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px;" onclick="clear_fields();"/>     
</c:set>
<div id="content" align="center">
<div id="liquid-round" style="margin:0px;">
    <div class="top"><span></span></div>
    <div class="center-content">
		<table class="table" border="0">
		<thead>
		<tr>
		<th>Cost Element</th>
		<th>Description</th>		
		<th></th>	
		</tr>
		</thead>	
		<tbody>
		<tr>
			<td align="left">
			 <s:textfield name="tableN" required="true" cssClass="input-text" size="45"/>
			</td>
			<td align="left">
			 <s:textfield name="tableD" required="true" cssClass="input-text" size="45"/>
			</td>
			<td><c:out value="${searchbuttons}" escapeXml="false"/></td>					
		</tr>							
		</tbody>
		</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	<div id="Layer1" style="width:100%;">
<s:set name="costElements" value="costElements" scope="request"/>  
			<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Cost Element List</span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
	<display:table name="costElements" class="table" requestURI="" id="costElements" export="true" style="width:100%;!margin-top:12px;" decorator='${!accountpopup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}'>   
	     <display:column sortProperty="costElement" sortable="true" title="Cost Element" >
	    <a onclick="editCostElementMethod(${costElements.id});" ><c:out value="${costElements.costElement}" /></a>
		</display:column>
	    <display:column property="description" sortable="true" title="Cost Element Description" style="width:260px"/>
	    <display:column property="recGl" sortable="true" title="Rec Gl" />
	    <display:column property="payGl" sortable="true" title="Pay Gl" />
	    <display:column title="Remove" style="width: 15px;">
	    <a><img align="middle" title="Remove" onclick="confirmSubmit(${costElements.id});"  style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>	</a> 
  				</display:column>				    
	</display:table> 
	<input type="button" class="cssbuttonA" style="width:60px;" onclick="editCostElementMethod('none')" value="Add"/>   
</div>
</div>
</s:form>

<script language="javascript">
function editCostElementMethod(targetElement2){
	if(targetElement2=='none')
	{
		location.href='editCostElementForm.html';
	}
	else
	{
		location.href='editCostElementForm.html?id='+targetElement2;
	}
} 
function confirmSubmit(targetElement)      
{
	  var soIdNum=targetElement;
	  var url="checkForCostEleUseInCharge.html?ajax=1&decorator=simple&popup=true&costId="+encodeURI(soIdNum);
	  http1.open("GET", url, true); 
      http1.onreadystatechange =function(){ handleHttpResponse(targetElement);}; 
      http1.send(null); 
}
function handleHttpResponse(targetElement){
         if (http1.readyState == 4)
         {
         			  var results = http1.responseText
		               results = results.trim();					               
         			 if(results=="notUsed")	{
						var agree=confirm("Are you sure you want to Delete this Cost Element?");
						var did = targetElement;
						if (agree){
							location.href='costElementDelete.html?id='+did;
						}else{
							return false;
						}
					}else{
						alert('Cost Element In Use.');
					}
         } 
   }	


function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}	    
    var http1 = getHTTPObject();
	function clear_fields(){
		document.forms['costElementListPage'].elements['tableN'].value = '';
		document.forms['costElementListPage'].elements['tableD'].value = '';		
     }
</script>