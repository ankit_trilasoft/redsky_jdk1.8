package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.InventoryArticle;

public interface InventoryArticleManager extends GenericManager<InventoryArticle,Long>{
	   List	getArticleAutoComplete(String sessionCorpID,String article);
}
