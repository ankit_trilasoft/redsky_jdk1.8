<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>  
<head>   
    <title>Relocation Details</title>   
    <meta name="heading" content="Relocation Details"/>    
    <style><%@ include file="/common/calenderStyle.css"%></style>
    <style>
    .headtab_bg_center {
     width: 70%;
	}
	.dspdoclist {color:#444;margin:0px;font-family:arial,verdana;font-size:1.05em;line-height:1.6em;}
	div#main p {margin-top:0px;}
	div#mydiv{margin-top: -45px;}
	input[type="checkbox"]{margin:0px;}
	.cfDiv{z-index:9999;position:absolute;margin-top:30px;left:700px;width:350px;height:auto;background:#FFF;border:2px solid #6AC6F0;border-radius:4px;display:none;}
	.open-div {vertical-align:middle;float:right;display:none; width:110px;}
</style>
<style>
#overlayDSP {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:90; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
.class1 {
   display: block; /* ensures it’s invisible until it’s called */
    /* position: absolute; makes the div go into a position that’s absolute to the browser viewing area */
     /*left: 20%; positions the div half way horizontally */
   /*  top: 2%; positions the div half way vertically */
    padding: 15px; 
    border: 5px solid #74B3DC;
    background-color: #ffffff;
    width: 60%;
    height: 500px;
    overflow:auto; 
    z-index: 100; /* makes the div the top layer, so it’ll lay on top of the other content */
}
</style>
<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>    
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
	 <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">

function getCountryCode(){
	var countryName=document.forms['dspDetailsForm'].elements['dspDetails.TEN_country'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseCountryName;
    http4.send(null);
}
  
function handleHttpResponseCountryName(){
   if (http4.readyState == 4){
      var results = http4.responseText
      results = results.trim();
      if(results.length>=1){
		document.forms['dspDetailsForm'].elements['dspDetails.TEN_country'].value = results; 					
	  }
   }
}

/* var TEN_country=document.getElementById('TEN_country').value;
if(TEN_country == null ||  TEN_country=='' ){
	document.getElementById('TEN_state').disable = true;
} */

function enableStateListOrigin(){ 
	var oriCon=document.forms['dspDetailsForm'].elements['dspDetails.TEN_country'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(oriCon)> -1);
	  if(index != ''){
	  		document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'].disabled = false;
	  }else{
		  document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'].disabled = true;
	  }	        
}

function getState(targetElement,change) {
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
		if(country=="${entry.value}"){
			countryCode="${entry.key}";
		}
	</c:forEach>
	 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = function(){handleHttpResponse8(change);}
     http3.send(null);
}

function handleHttpResponse8(change){
     if (http3.readyState == 4){
        var results = http3.responseText
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'];
		targetElement.length = res.length;
			for(i=0;i<res.length;i++){
				if(res[i] == ''){
					document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'].options[i].text = '';
					document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'].options[i].value = '';
				}else{
					stateVal = res[i].split("#");
					document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'].options[i].text = stateVal[1];
					document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'].options[i].value = stateVal[0];
				}
			}
			if(change=='userChange'){
				document.getElementById("TEN_state").value = '';
			}else{
				document.getElementById("TEN_state").value = '${dspDetails.TEN_state}';
			}
     }
}  


function openOriginLocation() {
    var city = document.forms['dspDetailsForm'].elements['dspDetails.TEN_city'].value;
    var country = document.forms['dspDetailsForm'].elements['dspDetails.TEN_country'].value;
    var zip = document.forms['dspDetailsForm'].elements['dspDetails.TEN_zipCode'].value;
    var address = "";
   /* if(country == 'United States'){ Commented out for ticket number: 7022*/
    	address = document.forms['dspDetailsForm'].elements['dspDetails.TEN_addressLine1'].value+","+document.forms['dspDetailsForm'].elements['dspDetails.TEN_addressLine2'].value;
    /*}  else  {
    	address = document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
    } Commented out for ticket number: 7022*/
    address = address.replace("#","-");
    var state = document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'].value;
    if(country!='Netherlands'){
		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
    }else{
    	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+country);
    }
}


window.onload=function(){
<c:if test="${serviceOrder.job == 'RLO'}">
	<sec-auth:authComponent componentId="module.accPortal.status.hideVisibility">
	var textareaArray = ['dspDetails.CAR_comment','dspDetails.COL_comment','dspDetails.TRG_comment','dspDetails.HOM_comment','dspDetails.RNT_comment','dspDetails.SET_comment','dspDetails.LAN_comment','dspDetails.MMG_comment','dspDetails.ONG_comment','dspDetails.PRV_comment','dspDetails.AIO_comment','dspDetails.EXP_comment','dspDetails.RPT_comment','dspDetails.SCH_comment','dspDetails.TAX_comment','dspDetails.TAC_comment','dspDetails.TEN_comment','dspDetails.VIS_comment','dspDetails.WOP_comment','dspDetails.REP_comment','dspDetails.DPS_comment','dspDetails.HSM_comment','dspDetails.FRL_comment','dspDetails.SPA_comment','dspDetails.INS_comment','dspDetails.INP_comment','dspDetails.EDA_comment','dspDetails.TAS_comment'];
	if(document.images) {
	    var totalImages = document.images.length;
	    	for (var i=0;i<totalImages;i++) {
				if(!(document.images[i].src.indexOf('nav') > -1)){
					document.images[i].onclick = function(){ return false; }
				} }  }
		var divList = document.getElementsByTagName('div');
		for(var j = 0; j < divList.length; j++){
			if(divList[j].className == 'link-up'){
				divList[j].onclick = function(){ return false; }
			} }
	var elementsLen=document.getElementsByTagName('*').length;
	for(i=0;i<=elementsLen-1;i++) {
			var element = document.forms['dspDetailsForm'].elements[i];
				if(element.type == 'text') {
							element.readOnly =true;
							element.className = 'input-textUpper';		
					} else {
							document.forms['dspDetailsForm'].elements[i].disabled=true;
				}
				if(element.type == 'textarea'){
					for(var k = 0 ; k < textareaArray.length ; k++){
						if(textareaArray[k] == element.name){
							element.disabled = false; 
						} } } 
			if(document.forms['dspDetailsForm'].elements[i].type=='submit' || document.forms['dspDetailsForm'].elements[i].type=='button') {
				document.forms['dspDetailsForm'].elements[i].disabled = false;
			} }   
	  document.forms['dspDetailsForm'].setAttribute('bypassDirtyCheck',false);
	</sec-auth:authComponent>
</c:if>
}
var elementNameT;
function setFieldName(elementName){
	elementNameT = elementName;
}
function callCalendarBassedMethods(){
	if(elementNameT == 'serviceCompleteDate'){
		validateServiceCompleteDate();
	}else if(elementNameT!=undefined && elementNameT!='' && elementNameT != 'serviceCompleteDate' && check_serviceEndDate()){
		validateServiceFinishedDate(elementNameT);
	}
	elementNameT="";
}
function check_serviceEndDate(){
	var serviceType='${serviceOrder.serviceType}';
	var flag=true;
	var serviceArray=serviceType.split(",");
	var temp='';
	if(serviceArray!=undefined && serviceArray!='' && serviceArray.length>0){
		for(var i=0;i<serviceArray.length && flag;i++){
			temp=serviceArray[i];
			if(document.forms['dspDetailsForm'].elements['dspDetails.'+temp+'_serviceEndDate'].value==""){
				flag=false;
			}
		}
	}
	return flag;
}
function validateServiceCompleteDate(){
	<c:if test="${serviceCompleteDateVal!=null && serviceCompleteDateVal!='' && serviceCompleteDateVal!='N'}">
	    var serviceCompleteDateVal='${serviceCompleteDateVal}';
	    serviceCompleteDateVal=serviceCompleteDateVal.replace('N,','');
		alert("Please upload ("+serviceCompleteDateVal+") documents and proceed to fill the Service Complete Date.");
		document.forms['dspDetailsForm'].elements['dspDetails.serviceCompleteDate'].value="";
	</c:if>
}
function validateServiceFinishedDate(finishDate){
	<c:if test="${serviceCompleteDateVal!=null && serviceCompleteDateVal!='' && serviceCompleteDateVal!='N'}">
	    var serviceCompleteDateVal='${serviceCompleteDateVal}';
	    serviceCompleteDateVal=serviceCompleteDateVal.replace('N,','');
		alert("Please upload ("+serviceCompleteDateVal+") documents and proceed to fill the Service Complete Date.");
		document.forms['dspDetailsForm'].elements[finishDate].value="";
	</c:if>
}
function findCorpIdOfCode(key1){
	var key='';
	var value='';
	var findKey='';
	<c:forEach var="entry" items="${comDivCodePerCorpIdMap}">
	if(findKey==''){
		key="${entry.key}";	
		value="${entry.value}";
		if(value.indexOf(key1)>-1){
			findKey=key+"^"+value;
		}
	}	
	</c:forEach>	
	return findKey;
}
function enableElement(){
	progressBarAutoSave('1');
	var elementsList = document.getElementsByTagName('*');
	for(i=0;i<elementsList.length;i++) {
				if(elementsList[i].type == 'text') {
						elementsList[i].readOnly =false;
						elementsList[i].className = 'input';		
					} else {
						elementsList[i].disabled=false;
				}
				if(elementsList[i].type == 'textarea'){
						elementsList[i].disabled = false; 
				} }
	progressBarAutoSave('1');
} 
var reloMap = {};
var serviceForReloMap = '${jsonServiceForRelo}';
serviceForReloMap=serviceForReloMap.replace("{","");
serviceForReloMap=serviceForReloMap.replace("}","");
var arr=serviceForReloMap.split(",");
for(var i=0;i<arr.length;i++){
	   var key=arr[i].split(":")[0];
	   key=key.replace("\"","");
	   key=key.replace("\"","");
	   var value=arr[i].split(":")[1];
	   value=value.replace("\"","");
	   value=value.replace("\"","");
	   reloMap[key] = value;
 }
var vendorChangeStatus='';
function checkAlreadylinkedUp(savedVendorCode,vendorCode,vendorCodeEXSo,savedVendorEXSO,presentVendorCodeExSO){ 
	if(vendorChangeStatus == 'YES' && presentVendorCodeExSO == savedVendorEXSO && vendorCode != savedVendorCode){
		alert('Please Save the Service Order before Link Up');
		return false;
	} 
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'CAR')>-1}">
	var presentCAR_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.CAR_vendorCode'].value;
	var CAR_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.CAR_vendorCodeEXSO'].value;
	if(CAR_vendorCodeEXSO!='' && presentCAR_vendorCode==vendorCode){
	  alert("Link file ("+CAR_vendorCodeEXSO+") already exists for agent "+presentCAR_vendorCode+".")
	  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=CAR_vendorCodeEXSO;
	  return false;
	}
	</c:if>
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RLS')>-1}">
	var presentRLS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.RLS_vendorCode'].value;
	var RLS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.RLS_vendorCodeEXSO'].value;
	if(RLS_vendorCodeEXSO!='' && presentRLS_vendorCode==vendorCode){
	  alert("Link file ("+RLS_vendorCodeEXSO+") already exists for agent "+presentRLS_vendorCode+".")
	  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=RLS_vendorCodeEXSO;
	  return false;
	}
	</c:if>
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'CAT')>-1}">
	var presentCAT_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.CAT_vendorCode'].value;
	var CAT_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.CAT_vendorCodeEXSO'].value;
	if(CAT_vendorCodeEXSO!='' && presentCAT_vendorCode==vendorCode){
	  alert("Link file ("+CAT_vendorCodeEXSO+") already exists for agent "+presentCAT_vendorCode+".")
	  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=CAT_vendorCodeEXSO;
	  return false;
	}
	</c:if>
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'CLS')>-1}">
	var presentCLS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.CLS_vendorCode'].value;
	var CLS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.CLS_vendorCodeEXSO'].value;
	if(CLS_vendorCodeEXSO!='' && presentCLS_vendorCode==vendorCode){
	  alert("Link file ("+CLS_vendorCodeEXSO+") already exists for agent "+presentCLS_vendorCode+".")
	  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=CLS_vendorCodeEXSO;
	  return false;
	}
	</c:if>
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'CHS')>-1}">
	var presentCHS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.CHS_vendorCode'].value;
	var CHS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.CHS_vendorCodeEXSO'].value;
	if(CHS_vendorCodeEXSO!='' && presentCHS_vendorCode==vendorCode){
	  alert("Link file ("+CHS_vendorCodeEXSO+") already exists for agent "+presentCHS_vendorCode+".")
	  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=CHS_vendorCodeEXSO;
	  return false;
	}
	</c:if>
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'DPS')>-1}">
	var presentDPS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.DPS_vendorCode'].value;
	var DPS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.DPS_vendorCodeEXSO'].value;
	if(DPS_vendorCodeEXSO!='' && presentDPS_vendorCode==vendorCode){
	  alert("Link file ("+DPS_vendorCodeEXSO+") already exists for agent "+presentDPS_vendorCode+".")
	  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=DPS_vendorCodeEXSO;
	  return false;
	}
	</c:if>
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'HSM')>-1}">
	var presentHSM_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.HSM_vendorCode'].value;
	var HSM_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.HSM_vendorCodeEXSO'].value;
	if(HSM_vendorCodeEXSO!='' && presentHSM_vendorCode==vendorCode){
	  alert("Link file ("+HSM_vendorCodeEXSO+") already exists for agent "+presentHSM_vendorCode+".")
	  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=HSM_vendorCodeEXSO;
	  return false;
	}
	</c:if>
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'PDT')>-1}">
	var presentPDT_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.PDT_vendorCode'].value;
	var PDT_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.PDT_vendorCodeEXSO'].value;
	if(PDT_vendorCodeEXSO!='' && presentPDT_vendorCode==vendorCode){
	  alert("Link file ("+PDT_vendorCodeEXSO+") already exists for agent "+presentPDT_vendorCode+".")
	  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=PDT_vendorCodeEXSO;
	  return false;
	}
	</c:if>
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RCP')>-1}">
	var presentRCP_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.RCP_vendorCode'].value;
	var RCP_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.RCP_vendorCodeEXSO'].value;
	if(RCP_vendorCodeEXSO!='' && presentRCP_vendorCode==vendorCode){
	  alert("Link file ("+RCP_vendorCodeEXSO+") already exists for agent "+presentRCP_vendorCode+".")
	  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=RCP_vendorCodeEXSO;
	  return false;
	}
	</c:if>
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'SPA')>-1}">
	var presentSPA_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.SPA_vendorCode'].value;
	var SPA_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.SPA_vendorCodeEXSO'].value;
	if(SPA_vendorCodeEXSO!='' && presentSPA_vendorCode==vendorCode){
	  alert("Link file ("+SPA_vendorCodeEXSO+") already exists for agent "+presentSPA_vendorCode+".")
	  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=SPA_vendorCodeEXSO;
	  return false;
	}
	</c:if>
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TCS')>-1}">
	var presentTCS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.TCS_vendorCode'].value;
	var TCS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TCS_vendorCodeEXSO'].value;
	if(TCS_vendorCodeEXSO!='' && presentTCS_vendorCode==vendorCode){
	  alert("Link file ("+TCS_vendorCodeEXSO+") already exists for agent "+presentTCS_vendorCode+".")
	  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=TCS_vendorCodeEXSO;
	  return false;
	}
	</c:if>	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'MTS')>-1}">
	var presentMTS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.MTS_vendorCode'].value;
	var MTS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.MTS_vendorCodeEXSO'].value;
	if(MTS_vendorCodeEXSO!='' && presentMTS_vendorCode==vendorCode){
	  alert("Link file ("+MTS_vendorCodeEXSO+") already exists for agent "+presentMTS_vendorCode+".")
	  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=MTS_vendorCodeEXSO;
	  return false;
	}
	</c:if>
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'DSS')>-1}">
	var presentDSS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.DSS_vendorCode'].value;
	var DSS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.DSS_vendorCodeEXSO'].value;
	if(DSS_vendorCodeEXSO!='' && presentDSS_vendorCode==vendorCode){
	  alert("Link file ("+DSS_vendorCodeEXSO+") already exists for agent "+presentDSS_vendorCode+".")
	  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=DSS_vendorCodeEXSO;
	  return false;
	}
	</c:if>
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'COL')>-1}">
	var presentCOL_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.COL_vendorCode'].value;
	var COL_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.COL_vendorCodeEXSO'].value;
	  if(COL_vendorCodeEXSO!='' && presentCOL_vendorCode==vendorCode){
		  alert("Link file ("+COL_vendorCodeEXSO+") already exists for agent "+presentCOL_vendorCode+".")
		  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=COL_vendorCodeEXSO;
		  return false;
		}
	</c:if>	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TRG')>-1}"> 
	var presentTRG_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.TRG_vendorCode'].value;
	var TRG_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TRG_vendorCodeEXSO'].value;
	  if(TRG_vendorCodeEXSO!='' && presentTRG_vendorCode==vendorCode){
		  alert("Link file ("+TRG_vendorCodeEXSO+") already exists for agent "+presentTRG_vendorCode+".")
		  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=TRG_vendorCodeEXSO;
		  return false;
		}
	</c:if>	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'HOM')>-1}"> 	
    var presentHOM_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.HOM_vendorCode'].value;
    var HOM_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.HOM_vendorCodeEXSO'].value;
	 if(HOM_vendorCodeEXSO!='' && presentHOM_vendorCode==vendorCode){
		  alert("Link file ("+HOM_vendorCodeEXSO+") already exists for agent "+presentHOM_vendorCode+".")
		  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=HOM_vendorCodeEXSO;
		  return false;
	   }
	 </c:if> 
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RNT')>-1}"> 	   
    var presentRNT_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.RNT_vendorCode'].value;
	var RNT_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.RNT_vendorCodeEXSO'].value;
	 if(RNT_vendorCodeEXSO!='' && presentRNT_vendorCode==vendorCode){
		  alert("Link file ("+RNT_vendorCodeEXSO+") already exists for agent "+presentRNT_vendorCode+".")
		  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=RNT_vendorCodeEXSO;
		  return false;
	  }
	 </c:if> 
	 <c:if test="${fn1:indexOf(serviceOrder.serviceType,'LAN')>-1}">   
    var presentLAN_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.LAN_vendorCode'].value;
    var LAN_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.LAN_vendorCodeEXSO'].value;
	  if(LAN_vendorCodeEXSO!='' && presentLAN_vendorCode==vendorCode){
		   alert("Link file ("+LAN_vendorCodeEXSO+") already exists for agent "+presentLAN_vendorCode+".")
		   document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=LAN_vendorCodeEXSO;
		   return false;
	   }
	  </c:if> 
	 <c:if test="${fn1:indexOf(serviceOrder.serviceType,'MMG')>-1}">   
	 var presentMMG_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.MMG_vendorCode'].value;
	 var MMG_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.MMG_vendorCodeEXSO'].value;
	   if(MMG_vendorCodeEXSO!='' && presentMMG_vendorCode==vendorCode){
		   alert("Link file ("+MMG_vendorCodeEXSO+") already exists for agent "+presentMMG_vendorCode+".")
		   document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=MMG_vendorCodeEXSO;
		   return false;
	   } 
	  </c:if>
	 <c:if test="${fn1:indexOf(serviceOrder.serviceType,'ONG')>-1}">       
	 var presentONG_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.ONG_vendorCode'].value;
	 var ONG_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.ONG_vendorCodeEXSO'].value;
	   if(ONG_vendorCodeEXSO!='' && presentONG_vendorCode==vendorCode){
			alert("Link file ("+ONG_vendorCodeEXSO+") already exists for agent "+presentONG_vendorCode+".")
			document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=ONG_vendorCodeEXSO;
			return false;
	   } 
	  </c:if> 
	  <c:if test="${fn1:indexOf(serviceOrder.serviceType,'PRV')>-1}">   
	var presentPRV_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.PRV_vendorCode'].value;
	var PRV_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.PRV_vendorCodeEXSO'].value;
	  if(PRV_vendorCodeEXSO!='' && presentPRV_vendorCode==vendorCode){
		   alert("Link file ("+PRV_vendorCodeEXSO+") already exists for agent "+presentPRV_vendorCode+".")
		   document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=PRV_vendorCodeEXSO;
		   return false;
	  } 
	  </c:if>  
	  <c:if test="${fn1:indexOf(serviceOrder.serviceType,'AIO')>-1}">              			 	 
	var presentAIO_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.AIO_vendorCode'].value;
	var AIO_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.AIO_vendorCodeEXSO'].value;
	  if(AIO_vendorCodeEXSO!='' && presentAIO_vendorCode==vendorCode){
		  alert("Link file ("+AIO_vendorCodeEXSO+") already exists for agent "+presentAIO_vendorCode+".")
		   document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=AIO_vendorCodeEXSO;
		  return false;
	   }
	  </c:if>  
	  <c:if test="${fn1:indexOf(serviceOrder.serviceType,'EXP')>-1}">     
	var presentEXP_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.EXP_vendorCode'].value;
	var EXP_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.EXP_vendorCodeEXSO'].value;
	  if(EXP_vendorCodeEXSO!='' && presentEXP_vendorCode==vendorCode){
		  alert("Link file ("+EXP_vendorCodeEXSO+") already exists for agent "+presentEXP_vendorCode+".")
		  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=EXP_vendorCodeEXSO;
	      return false;
	   }
	  </c:if>  
	  <c:if test="${fn1:indexOf(serviceOrder.serviceType,'RPT')>-1}">      
    var presentRPT_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.RPT_vendorCode'].value;
	var RPT_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.RPT_vendorCodeEXSO'].value;
	  if(RPT_vendorCodeEXSO!='' && presentRPT_vendorCode==vendorCode){
		   alert("Link file ("+RPT_vendorCodeEXSO+") already exists for agent "+presentRPT_vendorCode+".")
		   document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=RPT_vendorCodeEXSO;
		   return false;
	  }
	  </c:if>  
	  <c:if test="${fn1:indexOf(serviceOrder.serviceType,'SCH')>-1}">    
	var presentSCH_schoolSelected=document.forms['dspDetailsForm'].elements['dspDetails.SCH_schoolSelected'].value;
    var SCH_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.SCH_vendorCodeEXSO'].value;
	 if(SCH_vendorCodeEXSO!='' && presentSCH_schoolSelected==vendorCode){
		 alert("Link file ("+SCH_vendorCodeEXSO+") already exists for agent "+presentSCH_schoolSelected+".")
		 document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=SCH_vendorCodeEXSO;
		 return false;
	 }
	 </c:if>  
	  <c:if test="${fn1:indexOf(serviceOrder.serviceType,'TAX')>-1}">    
    var presentTAX_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.TAX_vendorCode'].value;
	var TAX_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TAX_vendorCodeEXSO'].value;
	  if(TAX_vendorCodeEXSO!='' && presentTAX_vendorCode==vendorCode){
		   alert("Link file ("+TAX_vendorCodeEXSO+") already exists for agent "+presentTAX_vendorCode+".")
		   document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=TAX_vendorCodeEXSO;
		   return false;
	  } 
	  </c:if>  
	  <c:if test="${fn1:indexOf(serviceOrder.serviceType,'TAC')>-1}">    
    var presentTAC_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.TAC_vendorCode'].value;
    var TAC_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TAC_vendorCodeEXSO'].value;
	  if(TAC_vendorCodeEXSO!='' && presentTAC_vendorCode==vendorCode){
		   alert("Link file ("+TAC_vendorCodeEXSO+") already exists for agent "+presentTAC_vendorCode+".")
		    document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=TAC_vendorCodeEXSO;
		   return false;
	  } 
	  </c:if>  
	  <c:if test="${fn1:indexOf(serviceOrder.serviceType,'TEN')>-1}">    
    var presentTEN_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.TEN_vendorCode'].value;
	var TEN_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TEN_vendorCodeEXSO'].value;
	  if(TEN_vendorCodeEXSO!='' && presentTEN_vendorCode==vendorCode){
		  alert("Link file ("+TEN_vendorCodeEXSO+") already exists for agent "+presentTEN_vendorCode+".")
		  document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=TEN_vendorCodeEXSO;
		  return false;
	  }
	  </c:if>  
	  <c:if test="${fn1:indexOf(serviceOrder.serviceType,'VIS')>-1}">    
	var presentVIS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.VIS_vendorCode'].value;
	var VIS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.VIS_vendorCodeEXSO'].value;
	  if(VIS_vendorCodeEXSO!='' && presentVIS_vendorCode==vendorCode){
		   alert("Link file ("+VIS_vendorCodeEXSO+") already exists for agent "+presentVIS_vendorCode+".")
		   document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=VIS_vendorCodeEXSO;
		   return false;
	   } 
	  </c:if>  
	  <c:if test="${fn1:indexOf(serviceOrder.serviceType,'WOP')>-1}">    
		var presentWOP_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.WOP_vendorCode'].value;
		var WOP_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.WOP_vendorCodeEXSO'].value;
		  if(WOP_vendorCodeEXSO!='' && presentWOP_vendorCode==vendorCode){
			   alert("Link file ("+WOP_vendorCodeEXSO+") already exists for agent "+presentWOP_vendorCode+".")
			   document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=WOP_vendorCodeEXSO;
			   return false;
		   } 
		  </c:if>
		  <c:if test="${fn1:indexOf(serviceOrder.serviceType,'FRL')>-1}">    
			var presentFRL_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.FRL_vendorCode'].value;
			var FRL_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.FRL_vendorCodeEXSO'].value;
			  if(FRL_vendorCodeEXSO!='' && presentFRL_vendorCode==vendorCode){
				   alert("Link file ("+FRL_vendorCodeEXSO+") already exists for agent "+presentFRL_vendorCode+".")
				   document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=FRL_vendorCodeEXSO;
				   return false;
			   } 
			  </c:if>
			  <c:if test="${fn1:indexOf(serviceOrder.serviceType,'APU')>-1}">    
				var presentAPU_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.APU_vendorCode'].value;
				var APU_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.APU_vendorCodeEXSO'].value;
				  if(APU_vendorCodeEXSO!='' && presentAPU_vendorCode==vendorCode){
					   alert("Link file ("+APU_vendorCodeEXSO+") already exists for agent "+presentAPU_vendorCode+".")
					   document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=APU_vendorCodeEXSO;
					   return false;
				   } 
				  </c:if>
		  
	 <c:if test="${fn1:indexOf(serviceOrder.serviceType,'HOB')>-1}">    
			var presentHOB_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.HOB_vendorCode'].value;
			var HOB_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.HOB_vendorCodeEXSO'].value;
			  if(HOB_vendorCodeEXSO!='' && presentHOB_vendorCode==vendorCode){
				   alert("Link file ("+HOB_vendorCodeEXSO+") already exists for agent "+presentHOB_vendorCode+".")
				   document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=HOB_vendorCodeEXSO;
				   return false;
			   } 
		</c:if> 
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'FLB')>-1}">    
		var presentFLB_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.FLB_vendorCode'].value;
		var FLB_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.FLB_vendorCodeEXSO'].value;
		  if(FLB_vendorCodeEXSO!='' && presentFLB_vendorCode==vendorCode){
			   alert("Link file ("+FLB_vendorCodeEXSO+") already exists for agent "+presentFLB_vendorCode+".")
			   document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=FLB_vendorCodeEXSO;
			   return false;
		   } 
	</c:if> 
	   <c:if test="${fn1:indexOf(serviceOrder.serviceType,'REP')>-1}">    
			var presentREP_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.REP_vendorCode'].value;
			var REP_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.REP_vendorCodeEXSO'].value;
			  if(REP_vendorCodeEXSO!='' && presentREP_vendorCode==vendorCode){
				   alert("Link file ("+REP_vendorCodeEXSO+") already exists for agent "+presentREP_vendorCode+".")
				   document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=REP_vendorCodeEXSO;
				   return false;
			   } 
		</c:if>  
	  <c:if test="${fn1:indexOf(serviceOrder.serviceType,'SET')>-1}">     
	 var presentSET_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.SET_vendorCode'].value;
     var SET_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.SET_vendorCodeEXSO'].value;
	   if(SET_vendorCodeEXSO!='' && presentSET_vendorCode==vendorCode){
			alert("Link file ("+SET_vendorCodeEXSO+") already exists for agent "+presentSET_vendorCode+".")
			document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=SET_vendorCodeEXSO;
			return false;
	    }
	   </c:if>  
	   <c:if test="${networkAgent }">
	  var presentBookingAgentCode=document.forms['dspDetailsForm'].elements['serviceOrder.bookingAgentCode'].value; 
	  var bookingAgentExSO = document.forms['dspDetailsForm'].elements['trackingStatusBookingAgentExSO'].value;
		if(bookingAgentExSO!='' && presentBookingAgentCode==vendorCode){
			alert("Link file ("+bookingAgentExSO+") already exists for agent "+presentBookingAgentCode+".")
			document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=bookingAgentExSO;
			return false;
		}
		</c:if> 
	 var presentNetworkPartnerCode=document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value;
	 var networkAgentExSO=document.forms['dspDetailsForm'].elements['trackingStatusNetworkAgentExSO'].value;
	   if(networkAgentExSO!='' && presentNetworkPartnerCode==vendorCode){
			alert("Link file ("+networkAgentExSO+") already exists for agent "+presentNetworkPartnerCode+".")
			document.forms['dspDetailsForm'].elements[vendorCodeEXSo].value=networkAgentExSO;
			return false;
	   }else{
		   return true;
		   } }

function checkMoveTypeAgent(type){
	var vendorCode="";
	if(type=='BOK'){
		vendorCode=document.forms['dspDetailsForm'].elements['serviceOrder.bookingAgentCode'].value;
	}else{
		vendorCode=document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value;
	}
	if((vendorCode!=null)&&(vendorCode!=undefined)&&(vendorCode.trim()!='')){
		var moveType=document.forms['dspDetailsForm'].elements['moveType'].value;
		var checkAccessQuotation=document.forms['dspDetailsForm'].elements['checkAccessQuotation'].value;
		if(moveType=='Quote' && checkAccessQuotation=='Y'){
			var url="checkMoveTypeAgent.html?ajax=1&decorator=simple&popup=true&networkVendorCode=" + encodeURI(vendorCode);
			http21999.open("GET", url, true);
			http21999.onreadystatechange = function() {handleHttpResponsecheckNetworkPartnerBookingAgent(type,vendorCode);};
			http21999.send(null); 
		}else{
			if(type=='BOK'){
				createExternalEntriesTrackingBookingAgent();
			}else{
				createExternalEntriesNetworkGroup();
			}	
		}
	}else{
		if(type=='BOK'){
			createExternalEntriesTrackingBookingAgent();
		}else{
			createExternalEntriesNetworkGroup();
		}		
	}
}
function handleHttpResponsecheckNetworkPartnerBookingAgent(type,vendorCode){
	if (http21999.readyState == 4){
		var results = http21999.responseText
        results = results.trim();
		if((results.length > 0)&&(results=='Y')){			
			if(type=='BOK'){
				createExternalEntriesTrackingBookingAgent();
			}else{
				createExternalEntriesNetworkGroup();
			}					
		}else{
			alert("Can't link this file, As Quote level linking is not enabled for - "+vendorCode+".");
		}
	}
}
function createExternalEntriesBookingAgent(preFix,savedVendor,savedVendorEXSO){
	var soStatus = '${serviceOrder.status}';
	if(soStatus!='CNCL'){
		var vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].value;
		if((vendorCode!=null)&&(vendorCode!=undefined)&&(vendorCode.trim()!='')){
			var moveType=document.forms['dspDetailsForm'].elements['moveType'].value;
			var checkAccessQuotation=document.forms['dspDetailsForm'].elements['checkAccessQuotation'].value;
			var pre=preFix.replace("_","");
			var url="checkNetworkPartnerServiceType.html?ajax=1&decorator=simple&popup=true&networkVendorCode=" + encodeURI(vendorCode) +"&preFixCode=" + encodeURI(pre);
			if(moveType=='Quote' && checkAccessQuotation=='Y'){
				url="checkNetworkPartnerServiceType.html?ajax=1&decorator=simple&popup=true&networkVendorCode=" + encodeURI(vendorCode) +"&preFixCode=" + encodeURI(pre) +"&moveTypeCheck=Y";
			}
			http21989.open("GET", url, true);
			http21989.onreadystatechange = function() {handleHttpResponsecheckNetworkPartnerServiceType(preFix,savedVendor,savedVendorEXSO,vendorCode);};
			http21989.send(null);
		}else{
			createExternalEntriesBookingAgentNew(preFix,savedVendor,savedVendorEXSO);
		}
	}else{
		alert('Can not process for Link-Up, Because SO status is Cancelled.')
	}
}
function handleHttpResponsecheckNetworkPartnerServiceType(preFix,savedVendor,savedVendorEXSO,vendorCode){
	if (http21989.readyState == 4){
		var results = http21989.responseText
        results = results.trim();
		if(results.length > 1){
	        var desCrip="";
	        var pre=preFix.replace("_","");
			<c:forEach var="entry" items="${serviceRelos}"> 
				var key="${entry.key}";
				var value="${entry.value}";
				key=key.trim();
				value=value.trim();
				if(key==pre){
					desCrip=value;
				}
			</c:forEach>			
			var res=results.split("~");
			if((res[0]=='Y')&&(res[1]=='Y')){
				createExternalEntriesBookingAgentNew(preFix,savedVendor,savedVendorEXSO);
			}else if((res[0]=='Y')&&(res[1]=='N')){
				alert("Can't link this file, As Quote level linking is not enabled for - "+vendorCode+".");
			}else if((res[0]=='N')&&(res[1]=='Y')){
				alert("The RUC, you are trying to link to is not using '"+desCrip+"' relocation service(s)");
			}else if((res[0]=='N')&&(res[1]=='N')){
				createExternalEntriesBookingAgentNew(preFix,savedVendor,savedVendorEXSO);
			}else{
				alert("Can't link this file, As Quote level linking is not enabled for - "+vendorCode+".");
			}
		}
	}
}
function createExternalEntriesBookingAgentNew(preFix,savedVendor,savedVendorEXSO){
	var soStatus = '${serviceOrder.status}';
	if(soStatus!='CNCL'){
		document.getElementById("linkup_"+preFix+"vendorCodeEXSO").style.display="none";
		var vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].value;
		if(vendorCode.trim()==''){
			alert('Please enter a valid Agent Code');
			document.getElementById("linkup_"+preFix+"vendorCodeEXSO").style.display="block";
			return false;
		}	
		var vendorCodeExSOTemp  ='dspDetails.'+preFix+'vendorCodeEXSO';
		var presentVendorCodeExSO=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCodeEXSO'].value;
		var checkAlreadylinked = checkAlreadylinkedUp(savedVendor,vendorCode,vendorCodeExSOTemp,savedVendorEXSO,presentVendorCodeExSO); 
		if(checkAlreadylinked != false){
		 agree= confirm("Do you want to link this record with Agent "+vendorCode);
		if(agree){
			progressBarAutoSave('1');
			document.forms['dspDetailsForm'].elements['buttonType'].value='dspDetails'+preFix+'vendorCode';
			var vendorCodeExSO=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCodeEXSO'].value;
				var url="networkPartner.html?ajax=1&decorator=simple&popup=true&respectiveNetworkAgent=" + encodeURI(vendorCode)+"&parentShipNumber=${serviceOrder.bookingAgentShipNumber}";
				http234.open("GET", url, true);
				http234.onreadystatechange = function() {handleHttpResponseBookingAgentCodeNetworkGroup(preFix);};
				http234.send(null); 
		}else{document.getElementById("linkup_"+preFix+"vendorCodeEXSO").style.display="block";}
		}else{document.getElementById("linkup_"+preFix+"vendorCodeEXSO").style.display="block";}
	}else{
		alert('Can not process for Link-Up, Because SO status is Cancelled.')
	}
}		
	function handleHttpResponseBookingAgentCodeNetworkGroup(preFix){
		if (http234.readyState == 4){
			var results = http234.responseText
            results = results.trim();
			if(results=='true'){
				var vendorCodeExSO=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCodeEXSO'].value;
				if(vendorCodeExSO==''){
					document.forms['dspDetailsForm'].elements['buttonType'].value='dspDetails'+preFix+'vendorCode';
					GetSelectedItem();
					var url="saveDspDetails.html";
	        		document.forms['dspDetailsForm'].action = url;
	        		document.forms['dspDetailsForm'].submit();
				}else{
					var savedAgent= document.forms['dspDetailsForm'].elements[''+preFix+'vendorCodeValue'].value;
					var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].value;
					 var findKey=findCorpIdOfCode(savedAgent);
						var sameCompanyCodeflag=false;
						if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
							sameCompanyCodeflag=true;
						}	 
					if(savedAgent!=presentAgent){
						if(!sameCompanyCodeflag){
							progressBarAutoSave('0');
							agree= confirm("Changing the agent will remove the current RedSky Network partner link");
							if(agree){
								progressBarAutoSave('1');
								document.forms['dspDetailsForm'].elements['buttonType'].value='dspDetails'+preFix+'vendorCode';
								document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCodeEXSO'].value='remove'+vendorCodeExSO;
								GetSelectedItem();
								var url="saveDspDetails.html";
				        		document.forms['dspDetailsForm'].action = url;
				        		document.forms['dspDetailsForm'].submit();
							}
						}
					}else{
						progressBarAutoSave('0');
						alert('Already linked Up'); 
						document.forms['dspDetailsForm'].elements['buttonType'].value="";
						document.getElementById("linkup_"+preFix+"vendorCodeEXSO").style.display="block";
					} } 
            }else if(results=='PARENT'){
            	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].value;
            	progressBarAutoSave('0');
            	alert('Vendor '+presentAgent+' is try to Link Up with Parent.');
				document.forms['dspDetailsForm'].elements['buttonType'].value="";
				document.getElementById("linkup_"+preFix+"vendorCodeEXSO").style.display="block";
            }else{
            	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].value;
            	progressBarAutoSave('0');
            	alert('Vendor '+presentAgent+' is not a network partner');
				document.forms['dspDetailsForm'].elements['buttonType'].value="";
				document.getElementById("linkup_"+preFix+"vendorCodeEXSO").style.display="block";
            }	 } }
	function createExternalEntriesTrackingBookingAgent(){
		var soStatus = '${serviceOrder.status}';
		if(soStatus!='CNCL'){
			document.getElementById("linkup_bookingAgent").style.display="none";
			var bookingAgentCode=document.forms['dspDetailsForm'].elements['serviceOrder.bookingAgentCode'].value;
			var networkPartnerCode=document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value;
			var billingContractType=document.forms['dspDetailsForm'].elements['billingContractType'].value;
			if(networkPartnerCode.trim()==''){
				alert('Please enter a valid Network Code');
				document.getElementById("linkup_bookingAgent").style.display="block";
				return false; 
			}
			if(bookingAgentCode.trim()==''){
				alert('Please enter a valid Booking Agent Code in service Order details');
				document.getElementById("linkup_bookingAgent").style.display="block";
				return false; 
			}
			if(billingContractType!='true'){
			    alert('Pricing Contract in the Billing section is not of CMM/ DMM type.');
			    document.getElementById("linkup_bookingAgent").style.display="block";
				return false; 
			}
			var savedVendorCode = '${serviceOrder.bookingAgentCode}';
			var presentEXSO = document.forms['dspDetailsForm'].elements['trackingStatusBookingAgentExSO'].value;  
			var checkAlreadylinked = checkAlreadylinkedUp(savedVendorCode,bookingAgentCode,'trackingStatusBookingAgentExSO',presentEXSO,'${trackingStatus.networkAgentExSO}'); 
			if(checkAlreadylinked != false){
			agree= confirm("Do you want to link this record with Booking Agent "+bookingAgentCode);
			if(agree){
				progressBarAutoSave('1');
				document.forms['dspDetailsForm'].elements['buttonType'].value='LinkUpBookingAgent';
				var bookingAgentExSO=document.forms['dspDetailsForm'].elements['trackingStatusBookingAgentExSO'].value;
					var url="networkPartner.html?ajax=1&decorator=simple&popup=true&respectiveNetworkAgent=" + encodeURI(bookingAgentCode)+"&parentShipNumber=${serviceOrder.bookingAgentShipNumber}";
					http234.open("GET", url, true);
					http234.onreadystatechange = handleHttpResponseTrackingBookingAgentCodeNetworkGroup;
					http234.send(null);
				
			}else{document.getElementById("linkup_bookingAgent").style.display="block";}
			}else{document.getElementById("linkup_bookingAgent").style.display="block";}
		}else{
			alert('Can not process for Link-Up, Because SO status is Cancelled.')
		}
	}			
		function handleHttpResponseTrackingBookingAgentCodeNetworkGroup(){
			if (http234.readyState == 4){
				var results = http234.responseText
	            results = results.trim();
				if(results=='true'){
					var bookingAgentExSO=document.forms['dspDetailsForm'].elements['trackingStatusBookingAgentExSO'].value;
					if(bookingAgentExSO==''){
						document.forms['dspDetailsForm'].elements['buttonType'].value='LinkUpBookingAgent';
						GetSelectedItem();
						var url="saveDspDetails.html";
		        		document.forms['dspDetailsForm'].action = url;
		        		document.forms['dspDetailsForm'].submit();
					}else{
						var savedAgent='${serviceOrder.bookingAgentCode}';
						var presentAgent=document.forms['dspDetailsForm'].elements['serviceOrder.bookingAgentCode'].value;
						 var findKey=findCorpIdOfCode(savedAgent);
							var sameCompanyCodeflag=false;
							if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
								sameCompanyCodeflag=true;
							}	 
						if(savedAgent!=presentAgent){
							if(!sameCompanyCodeflag){
								progressBarAutoSave('0');
								agree= confirm("Changing the agent will remove the current RedSky Network partner link");
								if(agree){
									progressBarAutoSave('1');
									document.forms['dspDetailsForm'].elements['buttonType'].value='LinkUpBookingAgent';
									document.forms['dspDetailsForm'].elements['trackingStatusBookingAgentExSO'].value='remove'+bookingAgentExSO;
									GetSelectedItem();
									var url="saveDspDetails.html";
					        		document.forms['dspDetailsForm'].action = url;
					        		document.forms['dspDetailsForm'].submit();
								}
							}
						}else{
							progressBarAutoSave('0');
							alert('Already linked Up');
							document.forms['dspDetailsForm'].elements['buttonType'].value="";
							document.getElementById("linkup_bookingAgent").style.display="block";
						} } 
	            }else if(results=='PARENT'){
	            	var presentAgent=document.forms['dspDetailsForm'].elements['serviceOrder.bookingAgentCode'].value;
	            	progressBarAutoSave('0');
	            	alert('Agent '+presentAgent+' is try to Link Up with Parent.');
					document.forms['dspDetailsForm'].elements['buttonType'].value="";
					document.getElementById("linkup_bookingAgent").style.display="block";
	            }else{
	            	var presentAgent=document.forms['dspDetailsForm'].elements['serviceOrder.bookingAgentCode'].value;
	            	progressBarAutoSave('0');
					alert('Agent '+presentAgent+' is not a network partner');
					document.forms['dspDetailsForm'].elements['buttonType'].value="";
					document.getElementById("linkup_bookingAgent").style.display="block";
	            } }  } 
		function createExternalEntriesNetworkGroup(){
			var soStatus = '${serviceOrder.status}';
			if(soStatus!='CNCL'){
				document.getElementById("linkup_Network").style.display="none";
				var networkPartnerCode=document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value;
				var billingContractType=document.forms['dspDetailsForm'].elements['billingContractType'].value;
				if(networkPartnerCode.trim()==''){
					alert('Please enter a valid Agent Code');
					document.getElementById("linkup_Network").style.display="block";
					return false; 
				}
				if(billingContractType!='true'){
				    alert('Pricing Contract in the Billing section is not of CMM/ DMM type.');
				    document.getElementById("linkup_Network").style.display="block";
					return false; 
				}		 
				var savedVendorCode = '${trackingStatus.networkPartnerCode}';
				var presentEXSO = document.forms['dspDetailsForm'].elements['trackingStatusNetworkAgentExSO'].value; 
				var checkAlreadylinked = checkAlreadylinkedUp(savedVendorCode,networkPartnerCode,'trackingStatusNetworkAgentExSO',presentEXSO,'${trackingStatus.networkAgentExSO}');
				if(checkAlreadylinked != false){
				agree= confirm("Do you want to link this record with Network Agent "+networkPartnerCode);
				if(agree){
					progressBarAutoSave('1');
					document.forms['dspDetailsForm'].elements['buttonType'].value='LinkUpNetworkGroup';
					var networkAgentExSO=document.forms['dspDetailsForm'].elements['trackingStatusNetworkAgentExSO'].value;
						var url="networkPartner.html?ajax=1&decorator=simple&popup=true&respectiveNetworkAgent=" + encodeURI(networkPartnerCode)+"&parentShipNumber=${serviceOrder.bookingAgentShipNumber}";
						http234.open("GET", url, true);
						http234.onreadystatechange = handleHttpResponseNetworkPartnerNetworkGroup;
						http234.send(null); 
				}else{document.getElementById("linkup_Network").style.display="block";}
				}else{document.getElementById("linkup_Network").style.display="block";}
			}else{
				alert('Can not process for Link-Up, Because SO status is Cancelled.')
			}
		}
			 function handleHttpResponseNetworkPartnerNetworkGroup(){
				if (http234.readyState == 4){
					var results = http234.responseText
		            results = results.trim();
					if(results=='true'){
						var networkAgentExSO=document.forms['dspDetailsForm'].elements['trackingStatusNetworkAgentExSO'].value;
						if(networkAgentExSO==''){
							document.forms['dspDetailsForm'].elements['buttonType'].value='LinkUpNetworkGroup';
							GetSelectedItem();
							var url="saveDspDetails.html";
			        		document.forms['dspDetailsForm'].action = url;
			        		document.forms['dspDetailsForm'].submit();
						}else{
							var savedAgent='${trackingStatus.networkPartnerCode}';
							var presentAgent=document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value;
							 var findKey=findCorpIdOfCode(savedAgent);
								var sameCompanyCodeflag=false;
								if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
									sameCompanyCodeflag=true;
								}	 
							if(savedAgent!=presentAgent){
								if(!sameCompanyCodeflag){
									progressBarAutoSave('0');
									agree= confirm("Changing the agent will remove the current RedSky Network partner link");
									if(agree){
										progressBarAutoSave('1');
										document.forms['dspDetailsForm'].elements['buttonType'].value='LinkUpNetworkGroup';
										document.forms['dspDetailsForm'].elements['trackingStatusNetworkAgentExSO'].value='remove'+networkAgentExSO;
										GetSelectedItem();
										var url="saveDspDetails.html";
						        		document.forms['dspDetailsForm'].action = url;
						        		document.forms['dspDetailsForm'].submit();
									}
								}
							}else{
								progressBarAutoSave('0');
								alert('Already linked Up'); 
								document.forms['dspDetailsForm'].elements['buttonType'].value="";
								document.getElementById("linkup_Network").style.display="block";
							} } 
		            }else if(results=='PARENT'){
		            	var presentAgent=document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value;
		            	progressBarAutoSave('0');
		            	alert('Agent '+presentAgent+' is try to Link Up with Parent.');
						document.forms['dspDetailsForm'].elements['buttonType'].value="";
						document.getElementById("linkup_Network").style.display="block";
			            }else{
		            	var presentAgent=document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value;
		            	progressBarAutoSave('0');
		            	alert('Agent '+presentAgent+' is not a network partner');
						document.forms['dspDetailsForm'].elements['buttonType'].value="";
						document.getElementById("linkup_Network").style.display="block";
		            } }  } 
	function createExternalEntriesBookingAgent1(preFix,savedVendor,savedVendorEXSO){
		var soStatus = '${serviceOrder.status}';
		if(soStatus!='CNCL'){
			var vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].value; 
			if((vendorCode!=null)&&(vendorCode!=undefined)&&(vendorCode.trim()!='')){
			var moveType=document.forms['dspDetailsForm'].elements['moveType'].value;
			var checkAccessQuotation=document.forms['dspDetailsForm'].elements['checkAccessQuotation'].value;
			var pre=preFix.replace("_","");
			var url="checkNetworkPartnerServiceType.html?ajax=1&decorator=simple&popup=true&networkVendorCode=" + encodeURI(vendorCode) +"&preFixCode=" + encodeURI(pre);		
			if(moveType=='Quote' && checkAccessQuotation=='Y'){
				url="checkNetworkPartnerServiceType.html?ajax=1&decorator=simple&popup=true&networkVendorCode=" + encodeURI(vendorCode) +"&preFixCode=" + encodeURI(pre) +"&moveTypeCheck=Y";
			}
			http21979.open("GET", url, true);
			http21979.onreadystatechange = function() {handleHttpResponsecheckNetworkPartnerServiceType1(preFix,savedVendor,savedVendorEXSO,vendorCode);};
			http21979.send(null); 
			}else{
				createExternalEntriesBookingAgent1New(preFix,savedVendor,savedVendorEXSO);
			}
		}else{
			alert('Can not process for Link-Up, Because SO status is Cancelled.')
		}
	}
	function handleHttpResponsecheckNetworkPartnerServiceType1(preFix,savedVendor,savedVendorEXSO,vendorCode){
		if (http21979.readyState == 4){
			var results = http21979.responseText
	        results = results.trim();
			if(results.length > 1){
		        var desCrip="";
		        var pre=preFix.replace("_","");
				<c:forEach var="entry" items="${serviceRelos}"> 
					var key="${entry.key}";
					var value="${entry.value}";
					key=key.trim();
					value=value.trim();
					if(key==pre){
						desCrip=value;
					}
				</c:forEach>
				var res=results.split("~");
				if((res[0]=='Y')&&(res[1]=='Y')){
					createExternalEntriesBookingAgent1New(preFix,savedVendor,savedVendorEXSO);
				}else if((res[0]=='Y')&&(res[1]=='N')){
					alert("Can't link this file, As Quote level linking is not enabled for - "+vendorCode+".");
				}else if((res[0]=='N')&&(res[1]=='Y')){
					alert("The RUC, you are trying to link to is not using '"+desCrip+"' relocation service(s)");
				}else if((res[0]=='N')&&(res[1]=='N')){
					createExternalEntriesBookingAgent1New(preFix,savedVendor,savedVendorEXSO);
				}else{
					alert("Can't link this file, As Quote level linking is not enabled for - "+vendorCode+".");
				}
			}
		}		
	}
	function createExternalEntriesBookingAgent1New(preFix,savedVendor,savedVendorEXSO){
		var soStatus = '${serviceOrder.status}';
		if(soStatus!='CNCL'){
			document.getElementById("linkup_"+preFix+"vendorCodeEXSO").style.display="none";
			var vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].value; 
			if(vendorCode.trim()==''){
				alert('Please enter a valid Agent Code');
				document.getElementById("linkup_"+preFix+"vendorCodeEXSO").style.display="block";
				return false;
			} 
			var presentVendorCodeExSO=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCodeEXSO'].value; 
			var vendorCodeExSOTemp=	'dspDetails.'+preFix+'vendorCodeEXSO';
			var checkAlreadylinked = checkAlreadylinkedUp(savedVendor,vendorCode,vendorCodeExSOTemp,savedVendorEXSO,presentVendorCodeExSO); 
			if(checkAlreadylinked!=false){
			agree= confirm("Do you want to link this record with Agent "+vendorCode);
			if(agree){ 
				progressBarAutoSave('1');
				document.forms['dspDetailsForm'].elements['buttonType'].value='dspDetails'+preFix+'schoolSelected';
				var vendorCodeExSO=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCodeEXSO'].value;
					var url="networkPartner.html?ajax=1&decorator=simple&popup=true&respectiveNetworkAgent=" + encodeURI(vendorCode)+"&parentShipNumber=${serviceOrder.bookingAgentShipNumber}";
					http234.open("GET", url, true);
					http234.onreadystatechange = function() {handleHttpResponseBookingAgentCodeNetworkGroup1(preFix);};
					http234.send(null); 
			}else{document.getElementById("linkup_"+preFix+"vendorCodeEXSO").style.display="block";}
			}else{document.getElementById("linkup_"+preFix+"vendorCodeEXSO").style.display="block";}
		}else{
			alert('Can not process for Link-Up, Because SO status is Cancelled.')
		}
	}	
		 function handleHttpResponseBookingAgentCodeNetworkGroup1(preFix){
			if (http234.readyState == 4){
				var results = http234.responseText
	            results = results.trim();
				if(results=='true'){
					var vendorCodeExSO=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCodeEXSO'].value;
					if(vendorCodeExSO==''){
						document.forms['dspDetailsForm'].elements['buttonType'].value='dspDetails'+preFix+'schoolSelected';
						GetSelectedItem();
						var url="saveDspDetails.html";
		        		document.forms['dspDetailsForm'].action = url;
		        		document.forms['dspDetailsForm'].submit();
					}else{
						var savedAgent= document.forms['dspDetailsForm'].elements[''+preFix+'schoolSelectedValue'].value;
						var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].value;
  					    var findKey=findCorpIdOfCode(savedAgent);
						var sameCompanyCodeflag=false;
							if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
								sameCompanyCodeflag=true;
							}	 
						if(savedAgent!=presentAgent){
							progressBarAutoSave('0');
							if(!sameCompanyCodeflag){
								agree= confirm("Changing the agent will remove the current RedSky Network partner link");
								if(agree){
									progressBarAutoSave('1');
									document.forms['dspDetailsForm'].elements['buttonType'].value='dspDetails'+preFix+'schoolSelected';
									document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCodeEXSO'].value='remove'+vendorCodeExSO;
									GetSelectedItem();
									var url="saveDspDetails.html";
					        		document.forms['dspDetailsForm'].action = url;
					        		document.forms['dspDetailsForm'].submit();
								}
							}
						}else{
							progressBarAutoSave('0');
							alert('Already linked Up');
							document.forms['dspDetailsForm'].elements['buttonType'].value="";
							document.getElementById("linkup_"+preFix+"vendorCodeEXSO").style.display="block";
						} } 
	            }else if(results=='PARENT'){
	            	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].value;
	            	progressBarAutoSave('0');
	            	alert('Vendor '+presentAgent+' is try to Link Up with Parent.');
					document.forms['dspDetailsForm'].elements['buttonType'].value="";
					document.getElementById("linkup_"+preFix+"vendorCodeEXSO").style.display="block";
	            }else{
	            	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].value;
	            	progressBarAutoSave('0');
	            	alert('Vendor '+presentAgent+' is not a network partner');
					document.forms['dspDetailsForm'].elements['buttonType'].value="";
					document.getElementById("linkup_"+preFix+"vendorCodeEXSO").style.display="block";
	            } }  } 
	var http234 = getHTTPObject234();
	var http21989 = getHTTPObject234();
	var http21979 = getHTTPObject234();
	var http21999 = getHTTPObject234();
	var httpsendMailVendor = getHTTPObject234();
	var http21984 = getHTTPObject234();
	var http23411 = getHTTPObject234();
	function getHTTPObject234() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }  else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
	} 
/* Method to Calculate 3 Month Back Date Formed By Kunal*/
function calcDays(sourceElement, targetElement) {
 var date1 = sourceElement.value;	 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')  {
       month = "01";
   } else if(month == 'Feb') {
       month = "02";
   } else if(month == 'Mar') {
       month = "03"
   } else if(month == 'Apr')  {
       month = "04"
   } else if(month == 'May') {
       month = "05"
   } else if(month == 'Jun') {
       month = "06"
   }  else if(month == 'Jul') {
       month = "07"
   } else if(month == 'Aug') {
       month = "08"
   } else if(month == 'Sep')  {
       month = "09"
   } else if(month == 'Oct')  {
       month = "10"
   }  else if(month == 'Nov')  {
       month = "11"
   } else if(month == 'Dec')  {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var eDate = Calendar.parseDate(finalDate,true);
   if(eDate != null){	  
	    if(sourceElement.id == 'TAC_leaseExpireDate'){
		    	var monIndx=parseInt(eDate.getMonth())+1;
		    	var dateIndx=parseInt(eDate.getDate());
		    	if(((monIndx==3)||(monIndx==5)||(monIndx==7)||(monIndx==10)||(monIndx==12))&&((dateIndx==31)||(dateIndx==30)||(dateIndx==29))){
		    		if(((monIndx==5)||(monIndx==7)||(monIndx==10)||(monIndx==12))&&((dateIndx==31)||(dateIndx==30)||(dateIndx==29))){
		    			eDate.setMonth(eDate.getMonth() - 1);
		    			if(dateIndx==31){
		    				eDate.setMonth(eDate.getMonth() - 1);
		    				eDate.setDate(30);
		    			}else{
		    				eDate.setDate(dateIndx);
		    			}
		    		}else{		    
		    			eDate.setMonth(eDate.getMonth() - 1);			
		    			var year=eDate.getFullYear();
		    			var day=0;
		    	           if((dateIndx==30)||(dateIndx==31)){
		    	        	   eDate.setMonth(eDate.getMonth() - 1);
		    	           }		    			
		    	           if( ( (year % 4 == 0) && ( year % 100 != 0) )|| (year % 400 == 0) )
		                		day = 29;
		            		else{
		                		day = 28;
		 	    	           if((dateIndx!=30)&&(dateIndx!=31)){
				 	    	        	  eDate.setMonth(eDate.getMonth() - 1);
			    	           } }	    			
		    			eDate.setDate(day);
		    		}
		    	}else{
		    		eDate.setMonth(eDate.getMonth() - 1);
		    	}
	    }else{
	    	var monIndx=parseInt(eDate.getMonth())+1;
	    	var dateIndx=parseInt(eDate.getDate());
	    	if(((monIndx==5)||(monIndx==7)||(monIndx==12))&&((dateIndx==31)||(dateIndx==30)||(dateIndx==29))){
	    		if(((monIndx==7)||(monIndx==12))&&((dateIndx==31)||(dateIndx==30)||(dateIndx==29))){
	    			eDate.setMonth(eDate.getMonth() - 3);
	    			if(dateIndx==31){
	    				eDate.setMonth(eDate.getMonth() - 1);
	    				eDate.setDate(30);
	    			}else{
	    				eDate.setDate(dateIndx);
	    			}
	    		}else{
	    			eDate.setMonth(eDate.getMonth() - 3);
	    			var year=eDate.getFullYear();
	    			var day=0;
	    	           if((dateIndx==30)||(dateIndx==31)){
	    	        	   eDate.setMonth(eDate.getMonth() - 1);
	    	           }	            			    			
	    				if( ( (year % 4 == 0) && ( year % 100 != 0) )|| (year % 400 == 0) )
	                		day = 29;
	            		else{
	                		day = 28;	    
	 	    	           if((dateIndx!=30)&&(dateIndx!=31)){
	 	    	        	  eDate.setMonth(eDate.getMonth() - 1);
		    	           } }
	    			eDate.setDate(day); 
	    		}
	    	}else{
	    		eDate.setMonth(eDate.getMonth() - 3);
	    	} }
  		document.getElementById(targetElement).value  = Calendar.printDate(eDate,"%d-%b-%y");
  } }
var searchFlag='';
function validateForEXSO(fieldId){
	if(searchFlag=='TRUE'){ 
	 	var presentVendorExSo=document.forms['dspDetailsForm'].elements[fieldId].value;
		if(presentVendorExSo!=''){
			document.forms['dspDetailsForm'].elements[fieldId].value='';
			searchFlag='';
		} } }
function winOpenDest(prefix,savedVendorEXSO){
	document.forms['dspDetailsForm'].elements['tempVendorCode'].value=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'vendorCode'].value;
	document.forms['dspDetailsForm'].elements['tempPrefix'].value=prefix;
	
	var savedAgent= document.forms['dspDetailsForm'].elements[''+prefix+'vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	
	<c:if test="${isNetworkBookingAgent==true || networkAgent}">
	var VendorCodeExSo=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'vendorCodeEXSO'].value;
	if(VendorCodeExSo!='' && savedVendorEXSO == VendorCodeExSo){
		agree= confirm("Changing the agent will remove the current RedSky Network partner link");
		if(agree){
			if(!sameCompanyCodeflag){
			vendorChangeStatus='YES';
			}
			openWindow('destinationPartnersRelo.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&jobRelo=&jobReloName='+prefix+'&decorator=popup&popup=true&fld_sixthDescription=dspDetails.'+prefix+'vendorContact&fld_fifthDescription=dspDetails.'+prefix+'vendorEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=dspDetails.'+prefix+'vendorName&fld_code=dspDetails.'+prefix+'vendorCode');
		}
	}else{
		searchFlag='TRUE';
		openWindow('destinationPartnersRelo.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&jobRelo=&jobReloName='+prefix+'&decorator=popup&popup=true&fld_sixthDescription=dspDetails.'+prefix+'vendorContact&fld_fifthDescription=dspDetails.'+prefix+'vendorEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=dspDetails.'+prefix+'vendorName&fld_code=dspDetails.'+prefix+'vendorCode');
		}
	</c:if>
	<c:if test="${isNetworkBookingAgent!=true && !networkAgent}">
	openWindow('destinationPartnersRelo.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&jobRelo=&jobReloName='+prefix+'&decorator=popup&popup=true&fld_sixthDescription=dspDetails.'+prefix+'vendorContact&fld_fifthDescription=dspDetails.'+prefix+'vendorEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=dspDetails.'+prefix+'vendorName&fld_code=dspDetails.'+prefix+'vendorCode');
   </c:if>
   //showContactImage(prefix);
}
function winOpenDest1(prefix,savedVendorEXSO){
	document.forms['dspDetailsForm'].elements['tempVendorCode'].value=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'schoolSelected'].value;
	document.forms['dspDetailsForm'].elements['tempPrefix'].value=prefix;	
	
	var savedAgent= document.forms['dspDetailsForm'].elements[''+prefix+'schoolSelectedValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'schoolSelected'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	
		
	<c:if test="${isNetworkBookingAgent==true || networkAgent}">
	var VendorCodeExSo=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'vendorCodeEXSO'].value;
	if(VendorCodeExSo!='' && savedVendorEXSO == VendorCodeExSo){
		agree= confirm("Changing the agent will remove the current RedSky Network partner link");
		if(agree){
			if(!sameCompanyCodeflag){
				vendorChangeStatus='YES';
				}			
			openWindow('destinationPartnersRelo.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&jobRelo=&jobReloName='+prefix+'&decorator=popup&popup=true&fld_sixthDescription=dspDetails.'+prefix+'contactPerson&fld_fifthDescription=dspDetails.'+prefix+'website&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=dspDetails.'+prefix+'schoolName&fld_code=dspDetails.'+prefix+'schoolSelected');	
		}else{
		}
	}
	else{
		openWindow('destinationPartnersRelo.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&jobRelo=&jobReloName='+prefix+'&decorator=popup&popup=true&fld_sixthDescription=dspDetails.'+prefix+'contactPerson&fld_fifthDescription=dspDetails.'+prefix+'website&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=dspDetails.'+prefix+'schoolName&fld_code=dspDetails.'+prefix+'schoolSelected');	
	}
	</c:if>		
	<c:if test="${isNetworkBookingAgent!=true && networkAgent}">
	openWindow('destinationPartnersRelo.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&jobRelo=&jobReloName='+prefix+'&decorator=popup&popup=true&fld_sixthDescription=dspDetails.'+prefix+'contactPerson&fld_fifthDescription=dspDetails.'+prefix+'website&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=dspDetails.'+prefix+'schoolName&fld_code=dspDetails.'+prefix+'schoolSelected');
	</c:if>
}
function currentDateRateOnActualization(){
	 var mydate=new Date();
  var daym;
  var year=mydate.getFullYear()
  var y=""+year;
  if (year < 1000)
  year+=1900
  var day=mydate.getDay()
  var month=mydate.getMonth()+1
  if(month == 1)month="Jan";
  if(month == 2)month="Feb";
  if(month == 3)month="Mar";
	  if(month == 4)month="Apr";
	  if(month == 5)month="May";
	  if(month == 6)month="Jun";
	  if(month == 7)month="Jul";
	  if(month == 8)month="Aug";
	  if(month == 9)month="Sep";
	  if(month == 10)month="Oct";
	  if(month == 11)month="Nov";
	  if(month == 12)month="Dec";
	  var daym=mydate.getDate()
	  if (daym<10)
	  daym="0"+daym
	  var datam = daym+"-"+month+"-"+y.substring(2,4); 
	  return datam;
}

function resendEmail(prefix){
	var resendEmailServiceName=document.forms['dspDetailsForm'].elements['dspDetails.resendEmailServiceName'].value;
	if(resendEmailServiceName==null || resendEmailServiceName==undefined){
		resendEmailServiceName="";
	}
	var resendUpdate="";
	var updateService="";
	var origVendor='';
	var origVendorContact='';
		var sid="${serviceOrder.id}";
	var surveyEmailList="${surveyEmailList}";
	if(surveyEmailList==null || surveyEmailList==undefined || surveyEmailList=='null'){
		surveyEmailList="";
	}
	var customerSurveyServiceDetails="${customerSurveyServiceDetails}";
	if(customerSurveyServiceDetails==null || customerSurveyServiceDetails==undefined || surveyEmailList=='null'){
		customerSurveyServiceDetails="";
	}
	var serviceType="${serviceOrder.serviceType}";
	if(serviceType==null || serviceType==undefined || serviceType=='null'){
		serviceType="";
	}
	var destination='NO';
	<c:if test="${fn1:indexOf(customerSurveyServiceDetails,'MMG')>-1}">
	destination='YES'
	</c:if>

	var specialServices="HSM,MMG,HOM";	
	if(surveyEmailList!=''){
		var arr=surveyEmailList.split(",");
		for(var k=0;k<arr.length;k++){
			var type=arr[k];
			if((customerSurveyServiceDetails=='' || customerSurveyServiceDetails.indexOf(type)<0) && specialServices.indexOf(type)<0 && serviceType.indexOf(type)>-1){
				if(updateService==""){
						updateService=type;
				}else{
					updateService=updateService+","+type;
				}
			}
		}
		if(prefix=='NET'){}else if(prefix=='SCH'){
			origVendor=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'_schoolSelected'].value;
			origVendorContact=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'_contactPerson'].value;
		}else{
			origVendor=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'_vendorCode'].value;
			origVendorContact=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'_vendorContact'].value;			
		}		
	}
	if(prefix=='NET'){
		document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'_emailSent'].value=currentDateRateOnActualization();
		resendUpdate='NET';
		document.getElementById('resend'+prefix).disabled = true;
	}else if(prefix=='MMG'){
		document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'_orginEmailSent'].value=currentDateRateOnActualization();
		try{
			<c:if test="${fn1:indexOf(customerSurveyServiceDetails,'MMG')>-1}">
	    		document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'_destinationEmailSent'].value=currentDateRateOnActualization();
	    	</c:if>
		}catch(e){}
	    resendUpdate='MMG';
		document.getElementById('resend'+prefix).disabled = true;
	}else if(prefix=='HSM'){
		document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'_emailSent'].value=currentDateRateOnActualization();
		resendUpdate='HSM';
		document.getElementById('resend'+prefix).disabled = true;
	}else if(prefix=='HOM'){
		document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'_emailSent'].value=currentDateRateOnActualization();
		resendUpdate='HOM';
		document.getElementById('resend'+prefix).disabled = true;
	}else{
		try{
			var arr1=updateService.split(",");
			var origVendor1='';
			var origVendorContact1='';
			for(var k=0;k<arr1.length;k++){
				var type=arr1[k];
				if(type!='SCH'){
					origVendor1=document.forms['dspDetailsForm'].elements['dspDetails.'+type+'_vendorCode'].value;
					origVendorContact1=document.forms['dspDetailsForm'].elements['dspDetails.'+type+'_vendorContact'].value;
				}else{
					origVendor1=document.forms['dspDetailsForm'].elements['dspDetails.'+type+'_schoolSelected'].value;
					origVendorContact1=document.forms['dspDetailsForm'].elements['dspDetails.'+type+'_contactPerson'].value;
				}
				if(origVendor==origVendor1 && origVendorContact==origVendorContact1){
				document.forms['dspDetailsForm'].elements['dspDetails.'+type+'_emailSent'].value=currentDateRateOnActualization();
				document.getElementById('resend'+type).disabled = true;
					if(resendUpdate==""){
						resendUpdate=type;
					}else{
						resendUpdate=resendUpdate+","+type;
					}				
				}
			}
		}catch(e){}
	}
	progressBarAutoSave('1');
	var url="resendEmailUpdateAjax.html?ajax=1&decorator=simple&popup=true&resendUpdate=" + encodeURI(resendUpdate)+"&soId=" + encodeURI(sid)+"&resendEmailServiceName=" + encodeURI(resendEmailServiceName)+"&destination=" + encodeURI(destination);
	httpResendEmailUpdate.open("GET", url, true);
	httpResendEmailUpdate.onreadystatechange = handleHttpResponseResendEmailUpdate;
	httpResendEmailUpdate.send(null);
}
function handleHttpResponseResendEmailUpdate(){
	if (httpResendEmailUpdate.readyState == 4){
        var results = httpResendEmailUpdate.responseText
        results = results.trim();   
        document.forms['dspDetailsForm'].elements['dspDetails.resendEmailServiceName'].value=results;
        alert("Email Has Been Scheduled.");
        progressBarAutoSave('0');
	}
}
function sendMailVendorCheck(){
	var prefix=document.forms['dspDetailsForm'].elements['tempPrefix'].value;
	var serviceType=prefix.replace('_','');
	var origVendor='';
	if(serviceType!='SCH'){
		origVendor=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'vendorCode'].value;
	}else{
		origVendor=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'schoolSelected'].value;
	}
	var idd=serviceType.toLowerCase();
	var tempVendorCode=document.forms['dspDetailsForm'].elements['tempVendorCode'].value;
	
	if(tempVendorCode!=origVendor){
			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
   			updateService(prefix);
   		   </configByCorp:fieldVisibility>    
	}
	document.forms['dspDetailsForm'].elements['tempVendorCode'].value='';
	document.forms['dspDetailsForm'].elements['tempPrefix'].value='';
}
function findAgent(position,agentType,prefix) {
	var partnerCode = "";
	var shipNumberA="";
	var originalCorpID = "${sessionCorpID}";
	if(agentType=='OA') {
		partnerCode = document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'vendorCode'].value; 
		var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&originalCorpID=" + encodeURI(originalCorpID);
		ajax_showTooltip(url,position);	
	} }
function findAgent1(position,agentType,prefix) {
	var partnerCode = "";
	var shipNumberA="";
	var originalCorpID = "${sessionCorpID}";
	if(agentType=='OA') {
		partnerCode = document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'schoolSelected'].value; 
		var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&originalCorpID=" + encodeURI(originalCorpID);
		ajax_showTooltip(url,position);	
	} } 
function findNetworkAgent(position,agentType) {
	var partnerCode = "";
	var shipNumberA="";
	var originalCorpID = "${sessionCorpID}";
	if(agentType=='BA') {	
		shipNumberA = document.forms['dspDetailsForm'].elements['shipNumberCode'].value; 
		var url="bookerAddress.html?ajax=1&decorator=simple&popup=true&shipNumberA=" + encodeURI(shipNumberA);		
		ajax_showTooltip(url,position);	
	} 
	if(agentType=='NT') {	
		partnerCode = document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value; 
		var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&originalCorpID=" + encodeURI(originalCorpID);
		ajax_showTooltip(url,position);	
	}
	return false;
} 
function checkVendorNameRelo1(preFix,savedVendorCodeEXSO){
	<c:if test="${isNetworkBookingAgent==true || networkAgent}">
	var VendorCodeExSo=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCodeEXSO'].value;
	
	var savedAgent= document.forms['dspDetailsForm'].elements[''+preFix+'schoolSelectedValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].value;
	    var findKey=findCorpIdOfCode(savedAgent);
	var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	if(VendorCodeExSo!='' && VendorCodeExSo == savedVendorCodeEXSO && !sameCompanyCodeflag){
		agree= confirm("Changing the agent will remove the current RedSky Network partner link");
		if(agree){
			var vendorId=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].value;	 
			  var job='';
			  if(vendorId == '')
			  {
			   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolName'].value="";
			   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'contactPerson'].value="";
			   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'website'].value="";
      			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
       			updateService(preFix);
       		   </configByCorp:fieldVisibility>
       			showHideAddress();    
			   }
			  if(vendorId != '') {
			   var url="vendorNameRelo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId)+"&jobRelo="+ encodeURI(job);
			   http3.open("GET",url,true);	  
			   http3.onreadystatechange = function() {handleHttpResponse31(preFix);};
			   http3.send(null);
			  }	
		}else{
			document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].value=document.forms['dspDetailsForm'].elements[''+preFix+'schoolSelectedValue'].value;
		}
	}else{
		if(!sameCompanyCodeflag){
			document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCodeEXSO'].value='';
		}
		var vendorId=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].value;	 
		  var job='';
		  if(vendorId == '') {
		   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolName'].value="";
		   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'contactPerson'].value="";
		   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'website'].value="";
  			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
   			updateService(preFix);
   		   </configByCorp:fieldVisibility>
   		showHideAddress();
		   }
		  if(vendorId != '') {
		   var url="vendorNameRelo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId)+"&jobRelo="+ encodeURI(job);
		   http3.open("GET",url,true);	  
		   http3.onreadystatechange = function() {handleHttpResponse31(preFix);};
		   http3.send(null);
		  } }
	</c:if>
	<c:if test="${isNetworkBookingAgent!=true && !networkAgent}">
	  var vendorId=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].value;	 
	  var job='';
	  if(vendorId == '') {
	   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolName'].value="";
	   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'contactPerson'].value="";
	   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'website'].value="";
			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
   			updateService(preFix);
   		   </configByCorp:fieldVisibility>
   		showHideAddress(); 
	   }
	  if(vendorId != '') {
	   var url="vendorNameRelo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId)+"&jobRelo="+ encodeURI(job);
	   http3.open("GET",url,true);	  
	   http3.onreadystatechange = function() {handleHttpResponse31(preFix);};
	   http3.send(null);
	  }
	  </c:if>
	  }
function updateService(preFix){
	var mailServiceType=document.forms['dspDetailsForm'].elements['dspDetails.mailServiceType'].value;
	var serviceType=preFix.replace("_","");
	if((mailServiceType!=null)&&(mailServiceType!=undefined)&&(mailServiceType.trim()!='')){
		if(mailServiceType.indexOf(serviceType)>-1){
			mailServiceType=mailServiceType.replace(serviceType,'');
			mailServiceType=mailServiceType.replace(",,",",");
	        var len1=mailServiceType.length-1;
	        if(len1==mailServiceType.lastIndexOf(",")){
	        	mailServiceType=mailServiceType.substring(0,len1);
	        }
	        if(mailServiceType.indexOf(",")==0){
	        	mailServiceType=mailServiceType.substring(1,mailServiceType.length);
	        }
			document.forms['dspDetailsForm'].elements['dspDetails.mailServiceType'].value=mailServiceType;
		}
		var idd=serviceType.toLowerCase();
		document.forms['dspDetailsForm'].elements[idd].checked=false;
		document.forms['dspDetailsForm'].elements[idd].disabled=false;
 	} }
	function handleHttpResponse31(preFix){
			if (http3.readyState == 4){
	                var results = http3.responseText
	                results = results.trim();                
	                var res = results.split("#");                        
			           if(res.size() >= 2){ 
			           		if(res[2] == 'Approved'){
			           				<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
					           	 	updateService(preFix);
					           	    </configByCorp:fieldVisibility>
			           		        document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolName'].value = res[1];
			           				document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'contactPerson'].value = res[3];
			           				document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'website'].value = res[5];			           			
			           			    document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].select();
			           			 showHideAddress();
			           		}else{
			           		    alert("Vendor Code is not approved" ); 
							    document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolName'].value="";
							    document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].value="";							    
							    document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'contactPerson'].value ="";
			           			document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'website'].value ="";
			           			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
			           			updateService(preFix);
			           		   </configByCorp:fieldVisibility>
			           			showHideAddress();
							    document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].select();
			           		}
	                }else{
	                     alert("Vendor Code not valid" );
						    document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolName'].value="";
						    document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].value="";							    
						    document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'contactPerson'].value ="";
		           			document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'website'].value ="";
		           			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
		           			updateService(preFix);
		           		   </configByCorp:fieldVisibility>
		           		showHideAddress();   
						    document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'schoolSelected'].select();
				   }  } }

function checkVendorNameRelo(preFix,savedVendorCodeEXSO){
	<c:if test="${isNetworkBookingAgent==true || networkAgent}">
	var VendorCodeExSo=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCodeEXSO'].value;

	var savedAgent= document.forms['dspDetailsForm'].elements[''+preFix+'vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(VendorCodeExSo!='' && VendorCodeExSo == savedVendorCodeEXSO && !sameCompanyCodeflag){
		agree= confirm("Changing the agent will remove the current RedSky Network partner link");
		if(agree){
			vendorChangeStatus='YES';
			var vendorId=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].value;
			  var job='';
			  if(vendorId == ''){
				   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorName'].value="";
				   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorContact'].value="";
				   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorEmail'].value="";
          			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
           			updateService(preFix);
           		   </configByCorp:fieldVisibility>
           		showHideAddress();
			   }else {
				   var url="vendorNameRelo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId)+"&jobRelo="+ encodeURI(job);
				   http3.open("GET",url,true);	  
				   http3.onreadystatechange = function() {handleHttpResponse3(preFix);};
				   http3.send(null);
			  }	
		}else{
			document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].value= document.forms['dspDetailsForm'].elements[''+preFix+'vendorCodeValue'].value;
		}
	}else{
		if(!sameCompanyCodeflag){
		document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCodeEXSO'].value = '';
		}
		 var vendorId=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].value;
		  var job='';
		  if(vendorId == '') {
			   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorName'].value="";
			   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorContact'].value="";
			   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorEmail'].value="";
      			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
       			updateService(preFix);
       		   </configByCorp:fieldVisibility>
       		showHideAddress(); 
		   }else {
			   var url="vendorNameRelo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId)+"&jobRelo="+ encodeURI(job);
			   http3.open("GET",url,true);	  
			   http3.onreadystatechange = function() {handleHttpResponse3(preFix);};
			   http3.send(null);
		  }	 }
	</c:if>
	<c:if test="${isNetworkBookingAgent!=true && !networkAgent}">
	  var vendorId=document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].value;
	  var job='';
	  if(vendorId == ''){
		   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorName'].value="";
		   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorContact'].value="";
		   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorEmail'].value="";
  			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
   			updateService(preFix);
   		   </configByCorp:fieldVisibility>
   		showHideAddress(); 
	   }else {
		   var url="vendorNameRelo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId)+"&jobRelo="+ encodeURI(job);
		   http3.open("GET",url,true);	  
		   http3.onreadystatechange = function() {handleHttpResponse3(preFix);};
		   http3.send(null);
	  }
	  </c:if>
	  
	  }
	function handleHttpResponse3(preFix){
			if (http3.readyState == 4){
	                var results = http3.responseText
	                results = results.trim();                
	                var res = results.split("#");                        
			           if(res.size() >= 2){ 
			           		if(res[2] == 'Approved'){
			           			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
			           			if(preFix!='INS_' && preFix!='INP_' && preFix!='EDA_' && preFix!='TAS_'){
			           				updateService(preFix);
			           			}
			        	       </configByCorp:fieldVisibility>
			           		   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorName'].value = res[1];
			           		   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorContact'].value = res[3];
			           		   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorEmail'].value = res[5];			           			
			           		   document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].select();
			           		showHideAddress();
			           		}else{
			           		    alert("Vendor Code is not approved" ); 
							    document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorName'].value="";
							    document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].value="";
							    document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorContact'].value ="";
			           			document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorEmail'].value ="";
			           			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
			           			updateService(preFix);
			           		   </configByCorp:fieldVisibility>
							    document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].select();
							    showHideAddress();
			           		}
	                }else{
	                     alert("Vendor Code not valid" );
	                     document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorName'].value=""; 
						 document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].value="";
						 document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorContact'].value ="";
			           	 document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorEmail'].value ="";
	           			<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	           			updateService(preFix);
	           		   </configByCorp:fieldVisibility>
						 document.forms['dspDetailsForm'].elements['dspDetails.'+preFix+'vendorCode'].select();
						 showHideAddress();
				   }  } }
	  function goPrev() {
			progressBarAutoSave('1');
			var soIdNum =document.forms['dspDetailsForm'].elements['serviceOrder.id'].value;
			var seqNm =document.forms['dspDetailsForm'].elements['serviceOrder.sequenceNumber'].value;
			var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
			 http5.open("GET", url, true); 
		     http5.onreadystatechange = handleHttpResponseOtherShip; 
		     http5.send(null); 
		   } 
		 function goNext() {
			progressBarAutoSave('1');
			var soIdNum =document.forms['dspDetailsForm'].elements['serviceOrder.id'].value;
			var seqNm =document.forms['dspDetailsForm'].elements['serviceOrder.sequenceNumber'].value;
			var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
			 http5.open("GET", url, true); 
		     http5.onreadystatechange = handleHttpResponseOtherShip; 
		     http5.send(null); 
		   } 
		  function handleHttpResponseOtherShip(){
		             if (http5.readyState == 4) {
		          		 var results = http5.responseText
		                 results = results.trim();
						 var id1=results;	 
			             findOtherServiceType(id1);
		             } }     
			function findOtherServiceType(id1)  {
				  var soIdNum=id1;
				  var url="findOtherServiceType.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum);
				  http10.open("GET", url, true); 
		          http10.onreadystatechange =function(){ handleHttpResponseOtherShipType(id1);}; 
		          http10.send(null); 
			}
		  function handleHttpResponseOtherShipType(id1){
		             if (http10.readyState == 4)
		             {
		             			  var results = http10.responseText
					               results = results.trim();					               
								if(results=="")	{
								location.href = 'editTrackingStatus.html?id='+id1;
								}
								else{
								location.href = 'editDspDetails.html?id='+id1;
								}  }  }	
		    var http10 = getHTTPObject();   
		    var http5 = getHTTPObject();   
		    var http2 = getHTTPObject();
		    var http = getHTTPObject();    
		  function goToUrl(id) {
			progressBarAutoSave('1');
			findOtherServiceType(id);
			}
		function findCustomerOtherSO(position) {
		 var sid=document.forms['dspDetailsForm'].elements['customerFile.id'].value;
		 var soIdNum=document.forms['dspDetailsForm'].elements['serviceOrder.id'].value;
		 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
		  ajax_showTooltip(url,position);	
		  }	
	function getHTTPObject() {
	    var xmlhttp;
	    if(window.XMLHttpRequest) {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject) {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp) {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }  }
	    return xmlhttp;
	}	    
	    var http3 = getHTTPObject();
	    var httpResendEmailUpdate = getHTTPObject();
animatedcollapse.addDiv('car', 'fade=1,hide=0')
animatedcollapse.addDiv('rls', 'fade=1,hide=0')
animatedcollapse.addDiv('col', 'fade=1,hide=0')
animatedcollapse.addDiv('trg', 'fade=1,hide=0')
animatedcollapse.addDiv('hom', 'fade=1,hide=0')
animatedcollapse.addDiv('rnt', 'fade=1,hide=0')
animatedcollapse.addDiv('set', 'fade=1,hide=0')
animatedcollapse.addDiv('lan', 'fade=1,hide=0')
animatedcollapse.addDiv('mmg', 'fade=1,hide=0')
animatedcollapse.addDiv('ong', 'fade=1,hide=0') 
animatedcollapse.addDiv('prv', 'fade=1,hide=0')
animatedcollapse.addDiv('aio', 'fade=1,hide=0')
animatedcollapse.addDiv('exp', 'fade=1,hide=0')
animatedcollapse.addDiv('rpt', 'fade=1,hide=0')
animatedcollapse.addDiv('rpt1', 'fade=1,hide=1')
animatedcollapse.addDiv('rpt2', 'fade=1,hide=1')
animatedcollapse.addDiv('sch', 'fade=1,hide=0')
animatedcollapse.addDiv('tax', 'fade=1,hide=0')
animatedcollapse.addDiv('tac', 'fade=1,hide=0')
animatedcollapse.addDiv('ten', 'fade=1,hide=0')
animatedcollapse.addDiv('vis', 'fade=1,hide=0')
animatedcollapse.addDiv('doc', 'fade=1,hide=1')
animatedcollapse.addDiv('wop', 'fade=1,hide=0')
animatedcollapse.addDiv('rep', 'fade=1,hide=0')
animatedcollapse.addDiv('net', 'fade=1,hide=0') 

animatedcollapse.addDiv('cat', 'fade=1,hide=0')
animatedcollapse.addDiv('cls', 'fade=1,hide=0')
animatedcollapse.addDiv('chs', 'fade=1,hide=0')
animatedcollapse.addDiv('dps', 'fade=1,hide=0')
animatedcollapse.addDiv('hsm', 'fade=1,hide=0')
animatedcollapse.addDiv('pdt', 'fade=1,hide=0')
animatedcollapse.addDiv('rcp', 'fade=1,hide=0')
animatedcollapse.addDiv('spa', 'fade=1,hide=0')
animatedcollapse.addDiv('tcs', 'fade=1,hide=0')
animatedcollapse.addDiv('mts', 'fade=1,hide=0')
animatedcollapse.addDiv('dss', 'fade=1,hide=0')
animatedcollapse.addDiv('hob', 'fade=1,hide=0')
animatedcollapse.addDiv('flb', 'fade=1,hide=0')
animatedcollapse.addDiv('frl', 'fade=1,hide=0')
animatedcollapse.addDiv('apu', 'fade=1,hide=0')
animatedcollapse.addDiv('ins', 'fade=1,hide=0')
animatedcollapse.addDiv('inp', 'fade=1,hide=0')
animatedcollapse.addDiv('eda', 'fade=1,hide=0')
animatedcollapse.addDiv('tas', 'fade=1,hide=0')
animatedcollapse.init()

function  checkNetworkPartnerName(){
	<c:if test="${isNetworkBookingAgent==true || networkAgent}">
		 var networkAgentExSO=document.forms['dspDetailsForm'].elements['trackingStatusNetworkAgentExSO'].value;
			if(networkAgentExSO!=''){
				agree= confirm("Changing the Network will remove the current RedSky Network partner link");
				if(agree){
					var vendorId = document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value;
				    if(vendorId == ''){
				    	document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerName'].value='';
				    	document.forms['dspDetailsForm'].elements['trackingStatusNetworkEmail'].value='';
				    	showHideAddress();    
				    } if(vendorId!=''){ 
					    var url="networkPartnerName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode=" + encodeURI(vendorId); 
					    http2.open("GET", url, true);
					    http2.onreadystatechange = handleHttpResponseNetworkPartnerName;
					    http2.send(null);
				    }
	       }else{
	          document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value='${trackingStatus.networkPartnerCode}';
	       }
	   } else{ 		
	           var vendorId = document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value;
				    if(vendorId == ''){
				    	document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerName'].value='';
				    	document.forms['dspDetailsForm'].elements['trackingStatusNetworkEmail'].value='';
				    	showHideAddress();    
				    } 
				    if(vendorId!=''){ 
					    var url="networkPartnerName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode=" + encodeURI(vendorId); 
					    http2.open("GET", url, true);
					    http2.onreadystatechange = handleHttpResponseNetworkPartnerName;
					    http2.send(null);
				    } }
	    </c:if>
		<c:if test="${isNetworkBookingAgent!=true && !networkAgent}">
		var vendorId = document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value;
	    if(vendorId == ''){
	    	document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerName'].value='';
	    	document.forms['dspDetailsForm'].elements['trackingStatusNetworkEmail'].value='';
	    	showHideAddress();    
	    } 
	    if(vendorId!=''){ 
		    var url="networkPartnerName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode=" + encodeURI(vendorId); 
		    http2.open("GET", url, true);
		    http2.onreadystatechange = handleHttpResponseNetworkPartnerName;
		    http2.send(null);
	    }
		</c:if>
		} 
function handleHttpResponseNetworkPartnerName(){
	if (http2.readyState == 4){
            var results = http2.responseText
            results = results.trim();
            var res = results.split("#"); 
            if(res.length>2){
            	if(res[2] == 'Approved'){
       				document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerName'].value = res[1];
       				document.forms['dspDetailsForm'].elements['trackingStatusNetworkEmail'].value = res[4];
       			 	document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].select();
       			 showHideAddress();     
           		}else{
           			alert("Network code is not approved" ); 
				    document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerName'].value="";
				    document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value="";
				    document.forms['dspDetailsForm'].elements['trackingStatusNetworkEmail'].value ="";
			 	    document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].select();
			 	   showAddressImage(); 
           		}
           	}else {
           	     alert("Network code is not valid" ); 
             	 document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerName'].value="";
             	 document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value = "";
             	 document.forms['dspDetailsForm'].elements['trackingStatusNetworkEmail'].value = "";
			 	 document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].select();  
			 	showHideAddress();
		   }  } } 
function winOpenPartnerNetwork (){
	<c:if test="${isNetworkBookingAgent==true || networkAgent}">
		 var networkAgentExSO1=document.forms['dspDetailsForm'].elements['trackingStatusNetworkAgentExSO'].value;
			if(networkAgentExSO1!=''){
				agree= confirm("Changing the Network will remove the current RedSky Network partner link");
				if(agree){
				 openWindow('networkPartnersPopUp.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatusNetworkEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatusNetworkPartnerName&fld_code=trackingStatusNetworkPartnerCode');
				 }
	        } else{
	            openWindow('networkPartnersPopUp.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatusNetworkEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatusNetworkPartnerName&fld_code=trackingStatusNetworkPartnerCode');
	        }
	 </c:if>
	 <c:if test="${isNetworkBookingAgent!=true && !networkAgent}">
	      openWindow('networkPartnersPopUp.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatusNetworkEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatusNetworkPartnerName&fld_code=trackingStatusNetworkPartnerCode');
	  </c:if>
	} 
function checkEmail(fieldId) {
	var email = document.forms['dspDetailsForm'].elements[fieldId];
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!filter.test(email.value)) {
			alert('Please provide a valid email address');
			document.forms['dspDetailsForm'].elements[fieldId].value=''; 
			document.forms['dspDetailsForm'].elements[fieldId].focus();
			return false;
		}showHideAddress();
		}
function showPartnerAlert(display, code, position){ 
    if(code == ''){ 
    	document.getElementById(position).style.display="none";
    	ajax_hideTooltip();
    }else if(code != ''){
    	getBAPartner(display,code,position);
    }  }
function getBAPartner(display,code,position){
	var url = "findPartnerAlertList.html?ajax=1&decorator=simple&popup=true&notesId=" + encodeURI(code);
     http.open("GET", url, true);
     http.onreadystatechange = function() {handleHttpResponsePartner(display,code,position)};
     http.send(null);
}
function handleHttpResponsePartner(display,code,position){
 	if (http.readyState == 4){
		var results = http.responseText
        results = results.trim();
        if(results.length > 555){
          	document.getElementById(position).style.display="block"; 
          	if(display != 'onload'){
          		document.getElementById(position).style.display="none";
          		getPartnerAlert(code,position);
          	}
      	}else{
      		document.getElementById(position).style.display="none";
      		ajax_hideTooltip();
      	} } }
function getPartnerAlert(code,position){
	 if(code == ''){ 
		 document.getElementById(position).style.display="none";
	    	ajax_hideTooltip();}
	 else{
		 document.getElementById(position).style.display="block"; }
}
function showEmailImage() {
    var agentEmailCode=document.forms['dspDetailsForm'].elements['trackingStatusBookingAgentEmail'].value; 
    if(agentEmailCode==''){ 
    document.getElementById("hidEmailImage").style.display="none";
    }
    else if(agentEmailCode!=''){
     document.getElementById("hidEmailImage").style.display="block";
    } } 
function sendEmail(target) {
		var originEmail = target;
		var subject = 'S/O# ${serviceOrder.shipNumber}';
		subject = subject+" ${serviceOrder.firstName}";
		subject = subject+" ${serviceOrder.lastName}"; 
		var mailto_link = "mailto:"+encodeURI(originEmail)+"?subject="+encodeURI(subject);		
		win = window.open(mailto_link,'emailWindow'); 
		if (win && win.open &&!win.closed) win.close(); 
		} 
      function GetSelectedItem() {
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'CAR')>-1}">   
	var savedCAR_vendorCode='${dspDetails.CAR_vendorCode}';
	savedCAR_vendorCode=savedCAR_vendorCode.trim();
	var presentCAR_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.CAR_vendorCode'].value;
	var CAR_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.CAR_vendorCodeEXSO'].value;
	var savedCAR_vendorCodeEXSO='${dspDetails.CAR_vendorCodeEXSO}';
	
	var savedAgent= document.forms['dspDetailsForm'].elements['CAR_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.CAR_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	
	if(CAR_vendorCodeEXSO!='' && savedCAR_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedCAR_vendorCode!=presentCAR_vendorCode && savedCAR_vendorCode!='' && savedCAR_vendorCode!='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.CAR_vendorCodeEXSO'].value='remove'+CAR_vendorCodeEXSO;
		} } 
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RLS')>-1}">   
	var savedRLS_vendorCode='${dspDetails.RLS_vendorCode}';
	savedRLS_vendorCode=savedRLS_vendorCode.trim();
	var presentRLS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.RLS_vendorCode'].value;
	var RLS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.RLS_vendorCodeEXSO'].value;
	var savedRLS_vendorCodeEXSO='${dspDetails.RLS_vendorCodeEXSO}';
	
	var savedAgent= document.forms['dspDetailsForm'].elements['RLS_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.RLS_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(RLS_vendorCodeEXSO!='' && savedRLS_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedRLS_vendorCode!=presentRLS_vendorCode && savedRLS_vendorCode!='' && savedRLS_vendorCode!='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.RLS_vendorCodeEXSO'].value='remove'+RLS_vendorCodeEXSO;
		} } 
	</c:if> 

	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'CAT')>-1}">   
	var savedCAT_vendorCode='${dspDetails.CAT_vendorCode}';
	savedCAT_vendorCode=savedCAT_vendorCode.trim();
	var presentCAT_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.CAT_vendorCode'].value;
	var CAT_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.CAT_vendorCodeEXSO'].value;
	var savedCAT_vendorCodeEXSO='${dspDetails.CAT_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['CAT_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.CAT_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(CAT_vendorCodeEXSO!='' && savedCAT_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedCAT_vendorCode!=presentCAT_vendorCode && savedCAT_vendorCode!='' && savedCAT_vendorCode!='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.CAT_vendorCodeEXSO'].value='remove'+CAT_vendorCodeEXSO;
		} } 
	</c:if> 
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'CLS')>-1}">   
	var savedCLS_vendorCode='${dspDetails.CLS_vendorCode}';
	savedCLS_vendorCode=savedCLS_vendorCode.trim();
	var presentCLS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.CLS_vendorCode'].value;
	var CLS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.CLS_vendorCodeEXSO'].value;
	var savedCLS_vendorCodeEXSO='${dspDetails.CLS_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['CLS_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.CLS_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(CLS_vendorCodeEXSO!='' && savedCLS_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedCLS_vendorCode!=presentCLS_vendorCode && savedCLS_vendorCode!='' && savedCLS_vendorCode!='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.CLS_vendorCodeEXSO'].value='remove'+CLS_vendorCodeEXSO;
		} } 
	</c:if> 
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'CHS')>-1}">   
	var savedCHS_vendorCode='${dspDetails.CHS_vendorCode}';
	savedCHS_vendorCode=savedCHS_vendorCode.trim();
	var presentCHS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.CHS_vendorCode'].value;
	var CHS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.CHS_vendorCodeEXSO'].value;
	var savedCHS_vendorCodeEXSO='${dspDetails.CHS_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['CHS_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.CHS_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(CHS_vendorCodeEXSO!='' && savedCHS_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedCHS_vendorCode!=presentCHS_vendorCode && savedCHS_vendorCode!='' && savedCHS_vendorCode!='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.CHS_vendorCodeEXSO'].value='remove'+CHS_vendorCodeEXSO;
		} } 
	</c:if> 
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'DPS')>-1}">   
	var savedDPS_vendorCode='${dspDetails.DPS_vendorCode}';
	savedDPS_vendorCode=savedDPS_vendorCode.trim();
	var presentDPS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.DPS_vendorCode'].value;
	var DPS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.DPS_vendorCodeEXSO'].value;
	var savedDPS_vendorCodeEXSO='${dspDetails.DPS_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['DPS_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.DPS_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 

	if(DPS_vendorCodeEXSO!='' && savedDPS_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedDPS_vendorCode!=presentDPS_vendorCode && savedDPS_vendorCode!='' && savedDPS_vendorCode!='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.DPS_vendorCodeEXSO'].value='remove'+DPS_vendorCodeEXSO;
		} } 
	</c:if> 
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'HSM')>-1}">   
	var savedHSM_vendorCode='${dspDetails.HSM_vendorCode}';
	savedHSM_vendorCode=savedHSM_vendorCode.trim();
	var presentHSM_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.HSM_vendorCode'].value;
	var HSM_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.HSM_vendorCodeEXSO'].value;
	var savedHSM_vendorCodeEXSO='${dspDetails.HSM_vendorCodeEXSO}';
	
	var savedAgent= document.forms['dspDetailsForm'].elements['HSM_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.HSM_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(HSM_vendorCodeEXSO!='' && savedHSM_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedHSM_vendorCode!=presentHSM_vendorCode && savedHSM_vendorCode!='' && savedHSM_vendorCode!='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.HSM_vendorCodeEXSO'].value='remove'+HSM_vendorCodeEXSO;
		} } 
	</c:if> 
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'PDT')>-1}">   
	var savedPDT_vendorCode='${dspDetails.PDT_vendorCode}';
	savedPDT_vendorCode=savedPDT_vendorCode.trim();
	var presentPDT_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.PDT_vendorCode'].value;
	var PDT_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.PDT_vendorCodeEXSO'].value;
	var savedPDT_vendorCodeEXSO='${dspDetails.PDT_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['PDT_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.PDT_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(PDT_vendorCodeEXSO!='' && savedPDT_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedPDT_vendorCode!=presentPDT_vendorCode && savedPDT_vendorCode!='' && savedPDT_vendorCode!='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.PDT_vendorCodeEXSO'].value='remove'+PDT_vendorCodeEXSO;
		} } 
	</c:if> 
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RCP')>-1}">   
	var savedRCP_vendorCode='${dspDetails.RCP_vendorCode}';
	savedRCP_vendorCode=savedRCP_vendorCode.trim();
	var presentRCP_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.RCP_vendorCode'].value;
	var RCP_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.RCP_vendorCodeEXSO'].value;
	var savedRCP_vendorCodeEXSO='${dspDetails.RCP_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['RCP_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.RCP_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(RCP_vendorCodeEXSO!='' && savedRCP_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedRCP_vendorCode!=presentRCP_vendorCode && savedRCP_vendorCode!='' && savedRCP_vendorCode!='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.RCP_vendorCodeEXSO'].value='remove'+RCP_vendorCodeEXSO;
		} } 
	</c:if> 
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'SPA')>-1}">   
	var savedSPA_vendorCode='${dspDetails.SPA_vendorCode}';
	savedSPA_vendorCode=savedSPA_vendorCode.trim();
	var presentSPA_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.SPA_vendorCode'].value;
	var SPA_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.SPA_vendorCodeEXSO'].value;
	var savedSPA_vendorCodeEXSO='${dspDetails.SPA_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['SPA_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.SPA_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(SPA_vendorCodeEXSO!='' && savedSPA_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedSPA_vendorCode!=presentSPA_vendorCode && savedSPA_vendorCode!='' && savedSPA_vendorCode!='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.SPA_vendorCodeEXSO'].value='remove'+SPA_vendorCodeEXSO;
		} } 
	</c:if> 
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TCS')>-1}">   
	var savedTCS_vendorCode='${dspDetails.TCS_vendorCode}';
	savedTCS_vendorCode=savedTCS_vendorCode.trim();
	var presentTCS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.TCS_vendorCode'].value;
	var TCS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TCS_vendorCodeEXSO'].value;
	var savedTCS_vendorCodeEXSO='${dspDetails.TCS_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['TCS_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.TCS_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 

	if(TCS_vendorCodeEXSO!='' && savedTCS_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedTCS_vendorCode!=presentTCS_vendorCode && savedTCS_vendorCode!='' && savedTCS_vendorCode!='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.TCS_vendorCodeEXSO'].value='remove'+TCS_vendorCodeEXSO;
		} } 
	</c:if> 
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'MTS')>-1}">   
	var savedMTS_vendorCode='${dspDetails.MTS_vendorCode}';
	savedMTS_vendorCode=savedMTS_vendorCode.trim();
	var presentMTS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.MTS_vendorCode'].value;
	var MTS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.MTS_vendorCodeEXSO'].value;
	var savedMTS_vendorCodeEXSO='${dspDetails.MTS_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['MTS_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.MTS_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(MTS_vendorCodeEXSO!='' && savedMTS_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedMTS_vendorCode!=presentMTS_vendorCode && savedMTS_vendorCode!='' && savedMTS_vendorCode!='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.MTS_vendorCodeEXSO'].value='remove'+MTS_vendorCodeEXSO;
		} } 
	</c:if>	

	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'DSS')>-1}">   
	var savedDSS_vendorCode='${dspDetails.DSS_vendorCode}';
	savedDSS_vendorCode=savedDSS_vendorCode.trim();
	var presentDSS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.DSS_vendorCode'].value;
	var DSS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.DSS_vendorCodeEXSO'].value;
	var savedDSS_vendorCodeEXSO='${dspDetails.DSS_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['DSS_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.DSS_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(DSS_vendorCodeEXSO!='' && savedDSS_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedDSS_vendorCode!=presentDSS_vendorCode && savedDSS_vendorCode!='' && savedDSS_vendorCode!='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.DSS_vendorCodeEXSO'].value='remove'+DSS_vendorCodeEXSO;
		} } 
	</c:if>	
		
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'COL')>-1}">   
	var savedCOL_vendorCode='${dspDetails.COL_vendorCode}';
	savedCOL_vendorCode=savedCOL_vendorCode.trim();
	var presentCOL_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.COL_vendorCode'].value;
	var COL_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.COL_vendorCodeEXSO'].value;
	var savedCOL_vendorCodeEXSO='${dspDetails.COL_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['COL_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.COL_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 

	if(COL_vendorCodeEXSO!='' && savedCOL_vendorCodeEXSO!=''){
	 if(!sameCompanyCodeflag && savedCOL_vendorCode!=presentCOL_vendorCode && savedCOL_vendorCode!='' && savedCOL_vendorCode !='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.COL_vendorCodeEXSO'].value='remove'+COL_vendorCodeEXSO;
		} } 
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TRG')>-1}">
	var savedTRG_vendorCode='${dspDetails.TRG_vendorCode}';
	savedTRG_vendorCode=savedTRG_vendorCode.trim();
	var presentTRG_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.TRG_vendorCode'].value;
	var TRG_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TRG_vendorCodeEXSO'].value;
	var savedTRG_vendorCodeEXSO='${dspDetails.TRG_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['TRG_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.TRG_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(TRG_vendorCodeEXSO!='' && savedTRG_vendorCodeEXSO!=''){
	 if(!sameCompanyCodeflag && savedTRG_vendorCode!=presentTRG_vendorCode && savedTRG_vendorCode !='' && savedTRG_vendorCode !='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.TRG_vendorCodeEXSO'].value='remove'+TRG_vendorCodeEXSO;
		} } 
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'HOM')>-1}">
	var savedHOM_vendorCode='${dspDetails.HOM_vendorCode}';
	savedHOM_vendorCode=savedHOM_vendorCode.trim();
	var presentHOM_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.HOM_vendorCode'].value;
	var HOM_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.HOM_vendorCodeEXSO'].value;
	var savedHOM_vendorCodeEXSO='${dspDetails.HOM_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['HOM_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.HOM_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(HOM_vendorCodeEXSO!='' && savedHOM_vendorCodeEXSO!=''){
	 if(!sameCompanyCodeflag && savedHOM_vendorCode!=presentHOM_vendorCode && savedHOM_vendorCode !='' && savedHOM_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.HOM_vendorCodeEXSO'].value='remove'+HOM_vendorCodeEXSO;
		} }
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RNT')>-1}"> 
	var savedRNT_vendorCode='${dspDetails.RNT_vendorCode}';
	savedRNT_vendorCode=savedRNT_vendorCode.trim();
	var presentRNT_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.RNT_vendorCode'].value;
	var RNT_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.RNT_vendorCodeEXSO'].value;
	var savedRNT_vendorCodeEXSO='${dspDetails.RNT_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['RNT_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.RNT_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 

	if(RNT_vendorCodeEXSO!='' && savedRNT_vendorCodeEXSO!=''){
	 if(!sameCompanyCodeflag && savedRNT_vendorCode!=presentRNT_vendorCode && savedRNT_vendorCode != '' && savedRNT_vendorCode !='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.RNT_vendorCodeEXSO'].value='remove'+RNT_vendorCodeEXSO;
		}  } 
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'LAN')>-1}"> 
	var savedLAN_vendorCode='${dspDetails.LAN_vendorCode}';
	savedLAN_vendorCode=savedLAN_vendorCode.trim();
	var presentLAN_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.LAN_vendorCode'].value;
	var LAN_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.LAN_vendorCodeEXSO'].value;
	var savedLAN_vendorCodeEXSO='${dspDetails.LAN_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['LAN_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.LAN_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 

	if(LAN_vendorCodeEXSO!='' && savedLAN_vendorCodeEXSO!='' ){
	 if(!sameCompanyCodeflag && savedLAN_vendorCode!=presentLAN_vendorCode && savedLAN_vendorCode !='' && savedLAN_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.LAN_vendorCodeEXSO'].value='remove'+LAN_vendorCodeEXSO;
		} } 
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'MMG')>-1}"> 
	var savedMMG_vendorCode='${dspDetails.MMG_vendorCode}';
	savedMMG_vendorCode=savedMMG_vendorCode.trim();
	var presentMMG_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.MMG_vendorCode'].value;
	var MMG_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.MMG_vendorCodeEXSO'].value;
	var savedMMG_vendorCodeEXSO='${dspDetails.MMG_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['MMG_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.MMG_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(MMG_vendorCodeEXSO!='' && savedMMG_vendorCodeEXSO!=''){
	 if(!sameCompanyCodeflag && savedMMG_vendorCode!=presentMMG_vendorCode && savedMMG_vendorCode !='' && savedMMG_vendorCode !='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.MMG_vendorCodeEXSO'].value='remove'+MMG_vendorCodeEXSO;
		} } 
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'ONG')>-1}">
	var savedONG_vendorCode='${dspDetails.ONG_vendorCode}';
	savedONG_vendorCode=savedONG_vendorCode.trim();
	var presentONG_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.ONG_vendorCode'].value;
	var ONG_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.ONG_vendorCodeEXSO'].value;
	var savedONG_vendorCodeEXSO='${dspDetails.ONG_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['ONG_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.ONG_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(ONG_vendorCodeEXSO!='' && savedONG_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedONG_vendorCode!=presentONG_vendorCode && savedONG_vendorCode !='' && savedONG_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.ONG_vendorCodeEXSO'].value='remove'+ONG_vendorCodeEXSO;
		} } 
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'PRV')>-1}">
	var savedPRV_vendorCode='${dspDetails.PRV_vendorCode}';
	savedPRV_vendorCode=savedPRV_vendorCode.trim();
	var presentPRV_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.PRV_vendorCode'].value;
	var PRV_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.PRV_vendorCodeEXSO'].value;
	var savedPRV_vendorCodeEXSO='${dspDetails.PRV_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['PRV_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.PRV_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(PRV_vendorCodeEXSO!='' && savedPRV_vendorCodeEXSO!=''){
	 if(!sameCompanyCodeflag && savedPRV_vendorCode!=presentPRV_vendorCode && savedPRV_vendorCode != '' && savedPRV_vendorCode !='null'){
		document.forms['dspDetailsForm'].elements['dspDetails.PRV_vendorCodeEXSO'].value='remove'+PRV_vendorCodeEXSO;
		} } 
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'AIO')>-1}">
	var savedAIO_vendorCode='${dspDetails.AIO_vendorCode}';
	savedAIO_vendorCode= savedAIO_vendorCode.trim();
	var presentAIO_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.AIO_vendorCode'].value;
	var AIO_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.AIO_vendorCodeEXSO'].value;
	var savedAIO_vendorCodeEXSO='${dspDetails.AIO_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['AIO_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.AIO_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 

	if(AIO_vendorCodeEXSO!='' && savedAIO_vendorCodeEXSO!=''){
	 if(!sameCompanyCodeflag && savedAIO_vendorCode!=presentAIO_vendorCode && savedAIO_vendorCode !='' && savedAIO_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.AIO_vendorCodeEXSO'].value='remove'+AIO_vendorCodeEXSO;
		} } 
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'EXP')>-1}">
	var savedEXP_vendorCode='${dspDetails.EXP_vendorCode}';
	savedEXP_vendorCode = savedEXP_vendorCode.trim();
	var presentEXP_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.EXP_vendorCode'].value;
	var EXP_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.EXP_vendorCodeEXSO'].value;
	var savedEXP_vendorCodeEXSO='${dspDetails.EXP_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['EXP_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.EXP_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(EXP_vendorCodeEXSO!='' && savedEXP_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedEXP_vendorCode!=presentEXP_vendorCode && savedEXP_vendorCode != '' && savedEXP_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.EXP_vendorCodeEXSO'].value='remove'+EXP_vendorCodeEXSO;
		} } 
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RPT')>-1}">
	var savedRPT_vendorCode='${dspDetails.RPT_vendorCode}';
	savedRPT_vendorCode =savedRPT_vendorCode.trim();
	var presentRPT_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.RPT_vendorCode'].value;
	var RPT_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.RPT_vendorCodeEXSO'].value;
	var savedRPT_vendorCodeEXSO='${dspDetails.RPT_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['RPT_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.RPT_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(RPT_vendorCodeEXSO!='' && savedRPT_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedRPT_vendorCode!=presentRPT_vendorCode &&  savedRPT_vendorCode != '' && savedRPT_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.RPT_vendorCodeEXSO'].value='remove'+RPT_vendorCodeEXSO;
		} } 
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'SCH')>-1}">
	try{
		var savedSCH_schoolSelected='${dspDetails.SCH_schoolSelected}';
		savedSCH_schoolSelected = savedSCH_schoolSelected.trim();
		var presentSCH_schoolSelected=document.forms['dspDetailsForm'].elements['dspDetails.SCH_schoolSelected'].value;
		var SCH_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.SCH_vendorCodeEXSO'].value;
		var savedSCH_vendorCodeEXSO='${dspDetails.SCH_vendorCodeEXSO}';
		var savedAgent= document.forms['dspDetailsForm'].elements['SCH_vendorCodeValue'].value;
		var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.SCH_vendorCode'].value;
		 var findKey=findCorpIdOfCode(savedAgent);
			var sameCompanyCodeflag=false;
			if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
				sameCompanyCodeflag=true;
			}	 
		
		if(SCH_vendorCodeEXSO!='' && savedSCH_vendorCodeEXSO !=''){
		 if(!sameCompanyCodeflag && savedSCH_schoolSelected!=presentSCH_schoolSelected && savedSCH_schoolSelected != '' && savedSCH_schoolSelected != 'null'){
			document.forms['dspDetailsForm'].elements['dspDetails.SCH_vendorCodeEXSO'].value='remove'+SCH_vendorCodeEXSO;
			} }
		}catch(e){}
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TAX')>-1}">  
	var savedTAX_vendorCode='${dspDetails.TAX_vendorCode}';
	savedTAX_vendorCode = savedTAX_vendorCode.trim();
	var presentTAX_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.TAX_vendorCode'].value;
	var TAX_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TAX_vendorCodeEXSO'].value;
	var savedTAX_vendorCodeEXSO='${dspDetails.TAX_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['TAX_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.TAX_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(TAX_vendorCodeEXSO!='' && savedTAX_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedTAX_vendorCode!=presentTAX_vendorCode && savedTAX_vendorCode !='' && savedTAX_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.TAX_vendorCodeEXSO'].value='remove'+TAX_vendorCodeEXSO;
		} }
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TAC')>-1}">  
	var savedTAC_vendorCode='${dspDetails.TAC_vendorCode}';
	savedTAC_vendorCode = savedTAC_vendorCode.trim();
	var presentTAC_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.TAC_vendorCode'].value;
	var TAC_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TAC_vendorCodeEXSO'].value;
	var savedTAC_vendorCodeEXSO='${dspDetails.TAC_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['TAC_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.TAC_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(TAC_vendorCodeEXSO!='' && savedTAC_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedTAC_vendorCode!=presentTAC_vendorCode && savedTAC_vendorCode != '' && savedTAC_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.TAC_vendorCodeEXSO'].value='remove'+TAC_vendorCodeEXSO;
		} } 
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TEN')>-1}"> 
	var savedTEN_vendorCode='${dspDetails.TEN_vendorCode}';
	savedTEN_vendorCode = savedTEN_vendorCode.trim();
	var presentTEN_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.TEN_vendorCode'].value;
	var TEN_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TEN_vendorCodeEXSO'].value;
	var savedTEN_vendorCodeEXSO='${dspDetails.TEN_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['TEN_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.TEN_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(TEN_vendorCodeEXSO!='' && savedTEN_vendorCodeEXSO !='' ){
	 if(!sameCompanyCodeflag && savedTEN_vendorCode!=presentTEN_vendorCode && savedTEN_vendorCode != '' && savedTEN_vendorCode !='null' ){
		document.forms['dspDetailsForm'].elements['dspDetails.TEN_vendorCodeEXSO'].value='remove'+TEN_vendorCodeEXSO;
		} }
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'VIS')>-1}">  
	var savedVIS_vendorCode='${dspDetails.VIS_vendorCode}';
	savedVIS_vendorCode = savedVIS_vendorCode.trim();
	var presentVIS_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.VIS_vendorCode'].value;
	var VIS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.VIS_vendorCodeEXSO'].value;
	var savedVIS_vendorCodeEXSO='${dspDetails.VIS_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['VIS_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.VIS_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 

	if(VIS_vendorCodeEXSO!='' && savedVIS_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedVIS_vendorCode!=presentVIS_vendorCode && savedVIS_vendorCode != '' && savedVIS_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.VIS_vendorCodeEXSO'].value='remove'+VIS_vendorCodeEXSO;
		} }
	</c:if> 
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'WOP')>-1}">  
	var savedWOP_vendorCode='${dspDetails.WOP_vendorCode}';
	savedWOP_vendorCode = savedWOP_vendorCode.trim();
	var presentWOP_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.WOP_vendorCode'].value;
	var WOP_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.WOP_vendorCodeEXSO'].value;
	var savedWOP_vendorCodeEXSO='${dspDetails.WOP_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['WOP_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.WOP_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(WOP_vendorCodeEXSO!='' && savedWOP_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedWOP_vendorCode!=presentWOP_vendorCode && savedWOP_vendorCode != '' && savedWOP_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.WOP_vendorCodeEXSO'].value='remove'+WOP_vendorCodeEXSO;
		} }
	</c:if>
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'FRL')>-1}">  
	var savedFRL_vendorCode='${dspDetails.FRL_vendorCode}';
	savedFRL_vendorCode = savedFRL_vendorCode.trim();
	var presentFRL_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.FRL_vendorCode'].value;
	var FRL_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.FRL_vendorCodeEXSO'].value;
	var savedFRL_vendorCodeEXSO='${dspDetails.FRL_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['FRL_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.FRL_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(FRL_vendorCodeEXSO!='' && savedFRL_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedFRL_vendorCode!=presentFRL_vendorCode && savedFRL_vendorCode != '' && savedFRL_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.FRL_vendorCodeEXSO'].value='remove'+FRL_vendorCodeEXSO;
		} }
	</c:if>
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'APU')>-1}">  
	var savedAPU_vendorCode='${dspDetails.APU_vendorCode}';
	savedAPU_vendorCode = savedAPU_vendorCode.trim();
	var presentAPU_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.APU_vendorCode'].value;
	var APU_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.APU_vendorCodeEXSO'].value;
	var savedAPU_vendorCodeEXSO='${dspDetails.APU_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['APU_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.APU_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(APU_vendorCodeEXSO!='' && savedAPU_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedAPU_vendorCode!=presentAPU_vendorCode && savedAPU_vendorCode != '' && savedAPU_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.APU_vendorCodeEXSO'].value='remove'+APU_vendorCodeEXSO;
		} }
	</c:if>
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'HOB')>-1}">  
	var savedHOB_vendorCode='${dspDetails.HOB_vendorCode}';
	savedHOB_vendorCode = savedHOB_vendorCode.trim();
	var presentHOB_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.HOB_vendorCode'].value;
	var HOB_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.HOB_vendorCodeEXSO'].value;
	var savedHOB_vendorCodeEXSO='${dspDetails.HOB_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['HOB_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.HOB_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(HOB_vendorCodeEXSO!='' && savedHOB_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedHOB_vendorCode!=presentHOB_vendorCode && savedHOB_vendorCode != '' && savedHOB_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.HOB_vendorCodeEXSO'].value='remove'+HOB_vendorCodeEXSO;
		} }
	</c:if> 
	
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'FLB')>-1}">  
	var savedFLB_vendorCode='${dspDetails.FLB_vendorCode}';
	savedFLB_vendorCode = savedFLB_vendorCode.trim();
	var presentFLB_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.FLB_vendorCode'].value;
	var FLB_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.FLB_vendorCodeEXSO'].value;
	var savedFLB_vendorCodeEXSO='${dspDetails.FLB_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['FLB_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.FLB_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 
	
	if(FLB_vendorCodeEXSO!='' && savedFLB_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedFLB_vendorCode!=presentFLB_vendorCode && savedFLB_vendorCode != '' && savedFLB_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.FLB_vendorCodeEXSO'].value='remove'+FLB_vendorCodeEXSO;
		} }
	</c:if>
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'REP')>-1}">  
	var savedREP_vendorCode='${dspDetails.REP_vendorCode}';
	savedREP_vendorCode = savedREP_vendorCode.trim();
	var presentREP_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.REP_vendorCode'].value;
	var REP_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.REP_vendorCodeEXSO'].value;
	var savedREP_vendorCodeEXSO='${dspDetails.REP_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['REP_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.REP_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 

	if(REP_vendorCodeEXSO!='' && savedREP_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedREP_vendorCode!=presentREP_vendorCode && savedREP_vendorCode != '' && savedREP_vendorCode != 'null'){
		document.forms['dspDetailsForm'].elements['dspDetails.REP_vendorCodeEXSO'].value='remove'+REP_vendorCodeEXSO;
		} }
	</c:if>  
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'SET')>-1}">    
	var savedSET_vendorCode='${dspDetails.SET_vendorCode}';
	savedSET_vendorCode=savedSET_vendorCode.trim();
	var presentSET_vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.SET_vendorCode'].value;
	var SET_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.SET_vendorCodeEXSO'].value;
	var savedSET_vendorCodeEXSO='${dspDetails.SET_vendorCodeEXSO}';
	var savedAgent= document.forms['dspDetailsForm'].elements['SET_vendorCodeValue'].value;
	var presentAgent=document.forms['dspDetailsForm'].elements['dspDetails.SET_vendorCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 

	if(SET_vendorCodeEXSO!='' && savedSET_vendorCodeEXSO !=''){
	 if(!sameCompanyCodeflag && savedSET_vendorCode!=presentSET_vendorCode && savedSET_vendorCode !='' && savedSET_vendorCode != 'null' ){
		document.forms['dspDetailsForm'].elements['dspDetails.SET_vendorCodeEXSO'].value='remove'+SET_vendorCodeEXSO;
		} }
	</c:if>  
	var savedNetworkPartnerCode='${trackingStatus.networkPartnerCode}';
	savedNetworkPartnerCode= savedNetworkPartnerCode.trim();
	var presentNetworkPartnerCode=document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value;
	var networkAgentExSO=document.forms['dspDetailsForm'].elements['trackingStatusNetworkAgentExSO'].value;
	var savednetworkAgentExSO='${trackingStatus.networkAgentExSO}';
	var savedAgent= '${trackingStatus.networkPartnerCode}';
	var presentAgent=document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value;
	 var findKey=findCorpIdOfCode(savedAgent);
		var sameCompanyCodeflag=false;
		if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
			sameCompanyCodeflag=true;
		}	 

	if(networkAgentExSO!='' && savednetworkAgentExSO!=''){
	 if(!sameCompanyCodeflag && savedNetworkPartnerCode!=presentNetworkPartnerCode && savedNetworkPartnerCode !='' && savedNetworkPartnerCode != 'null'){
		document.forms['dspDetailsForm'].elements['trackingStatusNetworkAgentExSO'].value='remove'+networkAgentExSO; 
		} }  
 
}
function textLimit(field,maxlen) {
	if(field.value.length > maxlen){
	while(field.value.length > maxlen){
	field.value=field.value.replace(/.$/,'');
	} } }
function focusDate(target) {
	document.forms['dspDetailsForm'].elements[target].focus();
}
function checkOnCheckBox(){
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'SET')>-1}">
	var reminderServices="";
	try{
	 if(document.getElementById('SET_pPermit').checked){
	  reminderServices=reminderServices+",SET_pPermit";
	 }
	}catch(e){}
	try{
	 if(document.getElementById('SET_gID').checked){
		  reminderServices=reminderServices+",SET_gID";
		 }
	}catch(e){}
	try{
	 if(document.getElementById('SET_initialRequested').checked){
		  reminderServices=reminderServices+",SET_initialRequested";
		 }
	}catch(e){}try{
	if(document.getElementById('SET_bAccount').checked){
			  reminderServices=reminderServices+",SET_bAccount";
		}
	}catch(e){}try{
	if(document.getElementById('SET_hCare').checked){
		  reminderServices=reminderServices+",SET_hCare";
	}
	}catch(e){}try{
	if(document.getElementById('SET_cHall').checked){
			  reminderServices=reminderServices+",SET_cHall";
		}
	}catch(e){}try{
	if(document.getElementById('SET_utilitie').checked){
		  reminderServices=reminderServices+",SET_utilitie";
	}
	}catch(e){}try{
	if(document.getElementById('SET_mConnection').checked){
		  reminderServices=reminderServices+",SET_mConnection";
	}
	}catch(e){}try{
	if(document.getElementById('SET_autoRegistration').checked){
		  reminderServices=reminderServices+",SET_autoRegistration";
	}
	}catch(e){}try{	
	if(document.getElementById('SET_dLicense').checked){
		  reminderServices=reminderServices+",SET_dLicense";
	}
	}catch(e){}
	 document.forms['dspDetailsForm'].elements['dspDetails.reminderServices'].value=reminderServices;
</c:if>
}



function copyDate() { 
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'SET')>-1}">
 var date = document.forms['dspDetailsForm'].elements['dspDetails.SET_parkingPermit'].value;
 if(document.forms['dspDetailsForm'].elements['dspDetails.SET_serviceStartDate'].value=="")
 {
 document.forms['dspDetailsForm'].elements['dspDetails.SET_serviceStartDate'].value=date;
 }
 var date = document.forms['dspDetailsForm'].elements['dspDetails.SET_driverLicense'].value;
 if(document.forms['dspDetailsForm'].elements['dspDetails.SET_serviceEndDate'].value=="")
 {
 document.forms['dspDetailsForm'].elements['dspDetails.SET_serviceEndDate'].value=date;
 }
	</c:if>
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'ONG')>-1}">
 var date = document.forms['dspDetailsForm'].elements['dspDetails.ONG_date1'].value;
 if(document.forms['dspDetailsForm'].elements['dspDetails.ONG_serviceStartDate'].value=="")
 {
 document.forms['dspDetailsForm'].elements['dspDetails.ONG_serviceStartDate'].value=date;
 }
 var date = document.forms['dspDetailsForm'].elements['dspDetails.ONG_date3'].value;
 if(document.forms['dspDetailsForm'].elements['dspDetails.ONG_serviceEndDate'].value=="")
 {
 document.forms['dspDetailsForm'].elements['dspDetails.ONG_serviceEndDate'].value=date;
 }
	</c:if>
}
function selectColumn1(targetElement) {  
  var rowId=targetElement.value;  
  if(targetElement.checked) {
	  var userCheckStatus = document.forms['dspDetailsForm'].elements['assignees'].value;
     if(userCheckStatus == '')
     {
	  	document.forms['dspDetailsForm'].elements['assignees'].value = rowId;
     }  else {
      var userCheckStatus=	document.forms['dspDetailsForm'].elements['assignees'].value = userCheckStatus + '#' + rowId;
     document.forms['dspDetailsForm'].elements['assignees'].value = userCheckStatus.replace( '##' , '#' );
     } }
  if(targetElement.checked==false) {
    var userCheckStatus = document.forms['dspDetailsForm'].elements['assignees'].value;
    var userCheckStatus=document.forms['dspDetailsForm'].elements['assignees'].value = userCheckStatus.replace( rowId , '' );
    document.forms['dspDetailsForm'].elements['assignees'].value = userCheckStatus.replace( '##' , '#' );
    }  }
function checkColumn1(){
	vendorChangeStatus='';
	var userCheckStatus ="";
	userCheckStatus = '${dspDetails.VIS_assignees}';		  
	if(userCheckStatus!="") {
		document.forms['dspDetailsForm'].elements['assignees'].value="";
		var column = userCheckStatus.split(",");  
		for(i = 0; i<column.length ; i++)
		{    
		     var userCheckStatus = document.forms['dspDetailsForm'].elements['assignees'].value;  
	    	 var userCheckStatus=	document.forms['dspDetailsForm'].elements['assignees'].value = userCheckStatus + '#' +column[i];
	        	if(column[i]!="")
	        	{
	     				document.getElementById(column[i]).checked=true;
	        	} }  }
	if(userCheckStatus==""){	}
}
function selectColumn2(targetElement) {  
  var rowId=targetElement.value;  
  if(targetElement.checked) {
	  var userCheckStatus = document.forms['dspDetailsForm'].elements['employers'].value;
     if(userCheckStatus == '') {
	  	document.forms['dspDetailsForm'].elements['employers'].value = rowId;
     } else {
      var userCheckStatus=	document.forms['dspDetailsForm'].elements['employers'].value = userCheckStatus + '#' + rowId;
     document.forms['dspDetailsForm'].elements['employers'].value = userCheckStatus.replace( '##' , '#' );
     } }
  if(targetElement.checked==false) {
    var userCheckStatus = document.forms['dspDetailsForm'].elements['employers'].value;
    var userCheckStatus=document.forms['dspDetailsForm'].elements['employers'].value = userCheckStatus.replace( rowId , '' );
    document.forms['dspDetailsForm'].elements['employers'].value = userCheckStatus.replace( '##' , '#' );
    } }
function checkColumn2(){
	var userCheckStatus ="";
	userCheckStatus = '${dspDetails.VIS_employers}';		  
	if(userCheckStatus!="") {
		document.forms['dspDetailsForm'].elements['employers'].value="";
		var column = userCheckStatus.split(",");  
		for(i = 0; i<column.length ; i++)
		{    
		     var userCheckStatus = document.forms['dspDetailsForm'].elements['employers'].value;  
	    	 var userCheckStatus=	document.forms['dspDetailsForm'].elements['employers'].value = userCheckStatus + '#' +column[i];
	        	if(column[i]!="")
	        	{
	     				document.getElementById(column[i]).checked=true;
	        	} }  }
	if(userCheckStatus==""){	}
}
function selectColumn3(targetElement)  {  
  var rowId=targetElement.value;  
  if(targetElement.checked) {
	  var userCheckStatus = document.forms['dspDetailsForm'].elements['convenant'].value;
     if(userCheckStatus == '') {
	  	document.forms['dspDetailsForm'].elements['convenant'].value = rowId;
     } else {
      var userCheckStatus=	document.forms['dspDetailsForm'].elements['convenant'].value = userCheckStatus + '#' + rowId;
     document.forms['dspDetailsForm'].elements['convenant'].value = userCheckStatus.replace( '##' , '#' );
     } }
  if(targetElement.checked==false) {
    var userCheckStatus = document.forms['dspDetailsForm'].elements['convenant'].value;
    var userCheckStatus=document.forms['dspDetailsForm'].elements['convenant'].value = userCheckStatus.replace( rowId , '' );
    document.forms['dspDetailsForm'].elements['convenant'].value = userCheckStatus.replace( '##' , '#' );
    }  }
function checkColumn3(){
	var userCheckStatus ="";
	userCheckStatus = '${dspDetails.VIS_convenant}';		  
	if(userCheckStatus!="") {
		document.forms['dspDetailsForm'].elements['convenant'].value="";
		var column = userCheckStatus.split(",");  
		for(i = 0; i<column.length ; i++) {    
		     var userCheckStatus = document.forms['dspDetailsForm'].elements['convenant'].value;  
	    	 var userCheckStatus=	document.forms['dspDetailsForm'].elements['convenant'].value = userCheckStatus + '#' +column[i];
	        	if(column[i]!="")
	        	{
	     				document.getElementById(column[i]).checked=true;
	        	} }  }
	if(userCheckStatus==""){	}
}
function changeStatus(){
	   document.forms['dspDetailsForm'].elements['formStatus'].value='1';
	}
function sendMailVendor(serviceType){
	<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	if(serviceType!=''){
		var sid="${serviceOrder.id}";
		var vendorEmail='';
		var vendorName='';
		var vendorCode='';
		if(serviceType!='SCH'){
			vendorEmail=document.forms['dspDetailsForm'].elements['dspDetails.'+serviceType+'_vendorEmail'].value;	
			vendorName=document.forms['dspDetailsForm'].elements['dspDetails.'+serviceType+'_vendorName'].value;
			vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.'+serviceType+'_vendorCode'].value;
		}else{
			vendorEmail=document.forms['dspDetailsForm'].elements['dspDetails.'+serviceType+'_website'].value;	
			vendorName=document.forms['dspDetailsForm'].elements['dspDetails.'+serviceType+'_schoolName'].value;
			vendorCode=document.forms['dspDetailsForm'].elements['dspDetails.'+serviceType+'_schoolSelected'].value;			
		} 
		if((vendorCode!=null)&&(vendorCode!=undefined)&&(vendorCode.trim()!='')&&(vendorEmail!=null)&&(vendorEmail!=undefined)&&(vendorEmail.trim()!='')){
			progressBarAutoSave('1');
			var entrytemp='';
			var serviceName='';
			<c:forEach var="entry" items="${serviceRelos}"> 
				entrytemp="${entry.key}";
				if(entrytemp==serviceType){
					serviceName="${entry.value}";
				}
			</c:forEach>
	   	    var url="sendMailVendorAjax.html?ajax=1&decorator=simple&popup=true&sid="+sid+"&vendorCode=" + encodeURIComponent(vendorCode)+"&vendorName=" + encodeURIComponent(vendorName)+"&serviceName=" + encodeURIComponent(serviceName)+"&vendorEmail=" + encodeURIComponent(vendorEmail)+"&serviceType=" + encodeURIComponent(serviceType); 
	    	httpsendMailVendor.open("GET", url, true);
	   		httpsendMailVendor.onreadystatechange = function() {handleHttphttpsendMailVendor(serviceType);};
	    	httpsendMailVendor.send(null);
		}else{
			var idd=serviceType.toLowerCase();
			document.forms['dspDetailsForm'].elements[idd].checked=false;
		}
	}else{
		var idd=serviceType.toLowerCase();
		document.forms['dspDetailsForm'].elements[idd].checked=false;
	}
	</configByCorp:fieldVisibility>
}
function handleHttphttpsendMailVendor(serviceType){
 	if (httpsendMailVendor.readyState == 4){
		var results = httpsendMailVendor.responseText
        results = results.trim();
        if(results.length > 2){
        	 progressBarAutoSave('0');
        	 var idd=serviceType.toLowerCase();
        	 if(results.trim()!='FALSE'){
	    		document.forms['dspDetailsForm'].elements[idd].disabled=true;
	    		var mailServiceType=document.forms['dspDetailsForm'].elements['dspDetails.mailServiceType'].value;
	    		if((mailServiceType!=null)&&(mailServiceType!=undefined)&&(mailServiceType.trim()!='')){
		    		if(mailServiceType.indexOf(serviceType)<0){
	    				document.forms['dspDetailsForm'].elements['dspDetails.mailServiceType'].value=mailServiceType+","+serviceType;
		    		}
			 	}else{
			 		document.forms['dspDetailsForm'].elements['dspDetails.mailServiceType'].value=serviceType;
			 	}
        	 }else{
        		 document.forms['dspDetailsForm'].elements[idd].checked=false;
        	 } } } } 
function onlyFloatNumsAllowed(evt)
{
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==110) || (keyCode==67) || (keyCode==86); 
}
function checkFloat(temp)
{ 
var check='';  
var i; 
var s = temp.value;
var fieldName = temp.name;  
var count = 0;
var countArth = 0;
for (i = 0; i < s.length; i++)
{   
    var c = s.charAt(i); 
    if(c == '.')  {
    	count = count+1
    }
    if(c == '-')    {
    	countArth = countArth+1
    }
    if((i!=0)&&(c=='-')) 	{
   	  alert("Only Float Value Allowed." ); 
      document.forms['dspDetailsForm'].elements[fieldName].select();
      document.forms['dspDetailsForm'].elements[fieldName].value=''; 
   	  return false;
   	} 
    if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
    	check='Invalid'; 
    }  } 
if(check=='Invalid'){ 
alert("Only Float Value Allowed." ); 
document.forms['dspDetailsForm'].elements[fieldName].select();
document.forms['dspDetailsForm'].elements[fieldName].value=''; 
return false;
}  else{
s=Math.round(s*100)/100;
var value=""+s;
if(value.indexOf(".") == -1){
value=value+".00";
} 
if((value.indexOf(".")+3 != value.length)){
value=value+"0";
}
document.forms['dspDetailsForm'].elements[fieldName].value=value; 
return true;
}  }
function findBillingIdName(field1,field2){
    var billingCode = document.forms['dspDetailsForm'].elements[field1].value;
    billingCode = billingCode.trim();
    if(billingCode!=''){
    	var url="findBillingIdNameAjax.html?ajax=1&decorator=simple&popup=true&billToCode=" + encodeURI(billingCode);
     	http21984.open("GET", url, true);
     	http21984.onreadystatechange = function(){ handleHttpResponse41984(field1,field2);};
     	http21984.send(null);
    }else{
		 document.forms['dspDetailsForm'].elements[field1].value="";
		 document.forms['dspDetailsForm'].elements[field2].value="";
		 document.forms['dspDetailsForm'].elements[field1].focus();    
    }
}
function handleHttpResponse41984(field1,field2)
{
     if (http21984.readyState == 4) {
	        var results = http21984.responseText
	        results = results.trim();
	        if(results.length>=1){
	        	document.forms['dspDetailsForm'].elements[field2].value=results;
	        }else{
	            alert("Agent Code not Approved" ); 
	   		 document.forms['dspDetailsForm'].elements[field1].value="";
			 document.forms['dspDetailsForm'].elements[field2].value="";
			 document.forms['dspDetailsForm'].elements[field1].focus();    
	        }
     }
}
function setBillToCode1(field1,field2,field3){
	javascript:openWindow("partnersPopup.html?partnerType=AG&flag=0&firstName=${customerFile.firstName}&lastName=${customerFile.lastName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description="+field2+"&fld_code="+field1+"&fld_seventhDescription="+field3+"");
}
function creatingYearDropDown(){
    var targetElement = document.forms['dspDetailsForm'].elements['dspDetails.MTS_mortgageTerm'];
    var temp="${dspDetails.MTS_mortgageTerm}";
    if((temp==null)||(temp==undefined)){
    	temp="";
    }
	targetElement.length = 9;
    var j=2012;	
	for(i=1;i<9;i++)
		{
		  		document.forms['dspDetailsForm'].elements['dspDetails.MTS_mortgageTerm'].options[i].value=''+j;
		  		document.forms['dspDetailsForm'].elements['dspDetails.MTS_mortgageTerm'].options[i].text=''+j;
		  		j++;
 		}
	document.forms['dspDetailsForm'].elements['dspDetails.MTS_mortgageTerm'].value=temp;
}
function customerFeebackDetails(DivId,serviceName){	
	 var shipNumber='${serviceOrder.shipNumber}';	
	 var soId='${serviceOrder.id}';
		$.get("customerFeebackAjax.html?ajax=1&decorator=simple&popup=true", 
				{shipnumber:shipNumber,feebackDivId:DivId,DSserviceOrderId:soId,serviceName:serviceName},
				function(data){
					//alert(data)
					 document.getElementById(DivId).style.display = "block";
					$("#"+DivId).html(data);				
			});	
	  
	}
function closeMyDivrat(autocompleteDivId,partnerNameId,paertnerCodeId){	
	document.getElementById(autocompleteDivId).style.display = "none";
}
function showContactImage(prefix){
	<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
	var vendorCode="";
	if(prefix=='SCH_'){
	vendorCode=prefix+'schoolSelected';
	}else{
	vendorCode=prefix+'vendorCode';
	}
	//alert(vendorCode)
	var hidImagePlus=prefix+'hidImagePlus';
    var agentCode=document.getElementById(vendorCode).value;
    if(agentCode!='') {      
        document.getElementById(hidImagePlus).style.display="block";
       } 
    </configByCorp:fieldVisibility>
    }
function getContactDetails(prifix,e){
	var venderCode="";	
	var divpro=prifix+'_hidImagePlus';
	var offset = $('#'+divpro).offset();
var height = $('#'+divpro).height();
var width = $('#'+divpro).width();
var top = offset.top-390 + height + "px";
var right = offset.left-320 + width + "px";	
	if(prifix=='SCH'){
		venderCode=document.getElementById(prifix+'_schoolSelected').value;
	}else{
		venderCode=document.getElementById(prifix+'_vendorCode').value;
	}
	$.get("findContactDetailsAjax.html?ajax=1&decorator=simple&popup=true", 
			{prifix:prifix,partnerCode:venderCode},
			function(data){
				document.getElementById("ContactDetails").style.display = "block";		
			    document.getElementById("ContactDetails").className  = "class1";
				$('#ContactDetails').css( {
				    'position': 'absolute',
				    'right': right,
				    'top': top
				});			    
			    //document.getElementById("overlayDSP").style.display = "block";				   		 
				$("#ContactDetails").html(data);
							
		});
}
function closeMyDiv(){

	document.getElementById("ContactDetails").style.display = "none";
	document.getElementById("overlayDSP").style.display = "none";

}
$(document).ready(function() {
	var serviceType='${serviceOrder.serviceType}';
	var surveyEmailList='${surveyEmailList}';
	<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
	serviceType=serviceType.split(",");
	for(i=0;i<serviceType.length;i++)
	{
		var venderCode="";
		if(serviceType[i]=='SCH'){
			venderCode=document.getElementById(serviceType[i]+'_schoolSelected').value;
		}else{
			venderCode=document.getElementById(serviceType[i]+'_vendorCode').value;
		}
		if(surveyEmailList.indexOf(serviceType[i])>-1) {      
			if(serviceType[i]=='SCH'){
				document.getElementById(serviceType[i]+'_contactPerson').readOnly = true;
				document.getElementById(serviceType[i]+'_contactPerson').className = 'input-textUpper';

			}else{
				document.getElementById(serviceType[i]+'_vendorContact').readOnly = true;
				document.getElementById(serviceType[i]+'_vendorContact').className = 'input-textUpper';
			}
		}
	 if(venderCode.trim()!='') {
	        document.getElementById(serviceType[i]+'_hidImagePlus').style.display="block";
		  }
	   }
	</configByCorp:fieldVisibility>
});
$(document).ready(function() {
	   <c:forEach var="entry" items="${serviceRelos}"> 
		code="${entry.key}";
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,entry.key)>-1}">
		if(code=='SCH'){
			venderCode=document.forms['dspDetailsForm'].elements['dspDetails.'+code+'_schoolSelected'].value;
		}else{
			venderCode=document.forms['dspDetailsForm'].elements['dspDetails.'+code+'_vendorCode'].value; 
		}
		var redskyuser=document.getElementById('hid'+code+'_RUC');
			var agentPortal=document.getElementById('hid'+code+'_AP');
			var dlinkButton=document.getElementById('linkup_'+code+'_vendorCodeEXSO');
			venderCode=venderCode.trim();
			if(venderCode==''){
				if(redskyuser!=undefined && redskyuser!=null){
	  			 redskyuser.style.display="none";
				}
				if(agentPortal!=undefined && agentPortal!=null){
	   			 agentPortal.style.display="none";
				}
	   			if(dlinkButton!=undefined && dlinkButton!=null){
	   			 dlinkButton.style.display="none";
	   			}
			}else{
				var findCode=findVendor(venderCode);
				if(findCode != undefined && findCode!=null && findCode!=''){
					if(findCode=='RUC'){
						if(redskyuser!=undefined && redskyuser!=null){
		   				redskyuser.style.display="block";
						}
						if(agentPortal!=undefined && agentPortal!=null){
		   	     		agentPortal.style.display="none";
						}
		   	     	if(dlinkButton!=undefined && dlinkButton!=null){
		   	     			dlinkButton.style.display="block";
		   	     		}
		   			}else if(findCode=='AP'){
		   				if(redskyuser!=undefined && redskyuser!=null){
		   			 		redskyuser.style.display="none";
		   				}
		   				if(agentPortal!=undefined && agentPortal!=null){
		   			 agentPortal.style.display="block";
		   				}
		   			if(dlinkButton!=undefined && dlinkButton!=null){
		   			 dlinkButton.style.display="none";
		   			}
		   		 }
				}else{
					if(redskyuser!=undefined && redskyuser!=null){
		  			 redskyuser.style.display="none";
					}
					if(agentPortal!=undefined && agentPortal!=null){
		   			 agentPortal.style.display="none";
					}
		   			if(dlinkButton!=undefined && dlinkButton!=null){
		   			 dlinkButton.style.display="none";
		   			}				
				}
			}		 
			</c:if>
		</c:forEach>
			
});
function openDiv(){
	<c:forEach var="entry" items="${serviceRelos}"> 
	code="${entry.key}";
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,entry.key)>-1}">
	if(code=='SCH'){
		venderCode=document.forms['dspDetailsForm'].elements['dspDetails.'+code+'_schoolSelected'].value;
	}else{
		venderCode=document.forms['dspDetailsForm'].elements['dspDetails.'+code+'_vendorCode'].value; 
	}
	var redskyuser=document.getElementById('hid'+code+'_RUC');
		var agentPortal=document.getElementById('hid'+code+'_AP');
		var dlinkButton=document.getElementById('linkup_'+code+'_vendorCodeEXSO');
		venderCode=venderCode.trim();
		if(venderCode==''){
			if(redskyuser!=undefined && redskyuser!=null){
  			 redskyuser.style.display="none";
			}
			if(agentPortal!=undefined && agentPortal!=null){
   			 agentPortal.style.display="none";
			}
   			if(dlinkButton!=undefined && dlinkButton!=null){
   			 dlinkButton.style.display="none";
   			}
		}else{
			var findCode=findVendor(venderCode);
			if(findCode != undefined && findCode!=null && findCode!=''){
				if(findCode=='RUC'){
					if(redskyuser!=undefined && redskyuser!=null){
	   				redskyuser.style.display="block";
					}
					if(agentPortal!=undefined && agentPortal!=null){
	   	     		agentPortal.style.display="none";
					}
	   	     	if(dlinkButton!=undefined && dlinkButton!=null){
	   	     			dlinkButton.style.display="block";
	   	     		}
	   			}else if(findCode=='AP'){
	   				if(redskyuser!=undefined && redskyuser!=null){
	   			 redskyuser.style.display="none";
	   				}
	   				if(agentPortal!=undefined && agentPortal!=null){
	   			 agentPortal.style.display="block";
	   				}
	   			if(dlinkButton!=undefined && dlinkButton!=null){
	   			 dlinkButton.style.display="none";
	   			}
	   		 }
			}else{
				if(redskyuser!=undefined && redskyuser!=null){
	  			 redskyuser.style.display="none";
				}
				if(agentPortal!=undefined && agentPortal!=null){
	   			 agentPortal.style.display="none";
				}
	   			if(dlinkButton!=undefined && dlinkButton!=null){
	   			 dlinkButton.style.display="none";
	   			}				
			}
		}
		</c:if>
	</c:forEach>
	   showHideAddress();
}
function findVendor(vendorCode){
	var portalType='';
	var vendorForReloMap = '${isRedSky}';
	var arr1=vendorForReloMap.split(",");
	for(var i=0;i<arr1.length;i++){
		   var key=arr1[i].split("~")[0];
		   key=key.trim();
		   var value=arr1[i].split("~")[1];
		   value=value.trim();
		   if(vendorCode==key){
			   portalType=value;
		   }
	 }
	 return portalType;
}
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
	</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<c:set var="fileID" value="%{serviceOrder.id}"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session" /> 
			    <c:set var="noteID"	value="${serviceOrder.shipNumber}" scope="session" /> 
			    <c:set var="noteFor" value="ServiceOrder" scope="session" /> 
			    <c:if test="${empty serviceOrder.id}">
				<c:set var="isTrue" value="false" scope="request" />
			    </c:if> 
			    <c:if test="${not empty serviceOrder.id}">
				<c:set var="isTrue" value="true" scope="request" />
			    </c:if>
<c:set var="ppType" value=""/>
<s:form id="dspDetailsForm" action="saveDspDetails" method="post" validate="true" onsubmit="return validateAgentCode();">
<c:set var="contactValid" value="false"/>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<c:set var="contactValid" value="true"/>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>

<s:hidden name="dspDetails.resendEmailServiceName" />
<s:hidden name="serviceCompleteDateVal" />
<s:hidden name="dspDetails.reminderServices" />
<s:hidden name="shipNumberCode" value="${serviceOrder.sequenceNumber}${serviceOrder.ship}"/>
<s:hidden name="serviceOrder.bookingAgentCode" value="${serviceOrder.bookingAgentCode}" />
<s:hidden name="billingContractType"  value="${billingContractType}"/>
<s:hidden name="formStatus" value=""/>
<s:hidden name="tempVendorCode" value=""/>
<s:hidden name="tempPrefix" value=""/>
<s:hidden name="dspDetails.mailServiceType"/>
<s:hidden name="serviceOrder.sequenceNumber" value="%{serviceOrder.sequenceNumber}"/> 
<s:hidden name="id" value="${dspDetails.id}" />
 <s:hidden name="serviceTypeSO1" value="${serviceOrder.serviceType}"/>
<s:hidden name="dspDetails.id"   />
<s:hidden name="serviceOrder.serviceType"/>
<s:hidden name="dspDetails.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="dspDetails.serviceOrderId" />
<s:hidden name="minShip" />	
<s:hidden name="shipSize" />
<s:hidden name="countShip" />
<s:hidden name="formStatus" value="0"/>
<s:hidden name="moveType"/>
<s:hidden name="checkAccessQuotation"/>
<s:hidden name="CAR_vendorCodeValue" value="${dspDetails.CAR_vendorCode}" />
<s:hidden name="RLS_vendorCodeValue" value="${dspDetails.RLS_vendorCode}" />
<s:hidden name="COL_vendorCodeValue" value="${dspDetails.COL_vendorCode}" />
<s:hidden name="TRG_vendorCodeValue" value="${dspDetails.TRG_vendorCode}" />
<s:hidden name="HOM_vendorCodeValue" value="${dspDetails.HOM_vendorCode}" />
<s:hidden name="RNT_vendorCodeValue" value="${dspDetails.RNT_vendorCode}" />
<s:hidden name="LAN_vendorCodeValue" value="${dspDetails.LAN_vendorCode}" />
<s:hidden name="MMG_vendorCodeValue" value="${dspDetails.MMG_vendorCode}" />
<s:hidden name="ONG_vendorCodeValue" value="${dspDetails.ONG_vendorCode}" />
<s:hidden name="PRV_vendorCodeValue" value="${dspDetails.PRV_vendorCode}" />
<s:hidden name="AIO_vendorCodeValue" value="${dspDetails.AIO_vendorCode}" />
<s:hidden name="EXP_vendorCodeValue" value="${dspDetails.EXP_vendorCode}" />
<s:hidden name="RPT_vendorCodeValue" value="${dspDetails.RPT_vendorCode}" />
<s:hidden name="SCH_schoolSelectedValue" value="${dspDetails.SCH_schoolSelected}" />
<s:hidden name="TAX_vendorCodeValue" value="${dspDetails.TAX_vendorCode}" />
<s:hidden name="TAC_vendorCodeValue" value="${dspDetails.TAC_vendorCode}" />
<s:hidden name="TEN_vendorCodeValue" value="${dspDetails.TEN_vendorCode}" />
<s:hidden name="VIS_vendorCodeValue" value="${dspDetails.VIS_vendorCode}" />
<s:hidden name="WOP_vendorCodeValue" value="${dspDetails.WOP_vendorCode}" />
<s:hidden name="REP_vendorCodeValue" value="${dspDetails.REP_vendorCode}" />
<s:hidden name="SET_vendorCodeValue" value="${dspDetails.SET_vendorCode}" />

<s:hidden name="CAT_vendorCodeValue" value="${dspDetails.CAT_vendorCode}" />
<s:hidden name="CLS_vendorCodeValue" value="${dspDetails.CLS_vendorCode}" />
<s:hidden name="CHS_vendorCodeValue" value="${dspDetails.CHS_vendorCode}" />
<s:hidden name="DPS_vendorCodeValue" value="${dspDetails.DPS_vendorCode}" />
<s:hidden name="HSM_vendorCodeValue" value="${dspDetails.HSM_vendorCode}" />
<s:hidden name="PDT_vendorCodeValue" value="${dspDetails.PDT_vendorCode}" />
<s:hidden name="RCP_vendorCodeValue" value="${dspDetails.RCP_vendorCode}" />
<s:hidden name="SPA_vendorCodeValue" value="${dspDetails.SPA_vendorCode}" />
<s:hidden name="TCS_vendorCodeValue" value="${dspDetails.TCS_vendorCode}" />
<s:hidden name="MTS_vendorCodeValue" value="${dspDetails.MTS_vendorCode}" />
<s:hidden name="DSS_vendorCodeValue" value="${dspDetails.DSS_vendorCode}" />
<s:hidden name="HOB_vendorCodeValue" value="${dspDetails.HOB_vendorCode}" />
<s:hidden name="FLB_vendorCodeValue" value="${dspDetails.FLB_vendorCode}" />
<s:hidden name="FRL_vendorCodeValue" value="${dspDetails.FRL_vendorCode}" />
<s:hidden name="APU_vendorCodeValue" value="${dspDetails.APU_vendorCode}" />
<s:hidden name="INS_vendorCodeValue" value="${dspDetails.INS_vendorCode}" />
<s:hidden name="INP_vendorCodeValue" value="${dspDetails.INP_vendorCode}" />
<s:hidden name="EDA_vendorCodeValue" value="${dspDetails.EDA_vendorCode}" />
<s:hidden name="TAS_vendorCodeValue" value="${dspDetails.TAS_vendorCode}" />
<s:hidden name="buttonType" />
<c:if test="${(isNetworkBookingAgent!=true) && !networkAgent}">
<s:hidden name="dspDetails.CAR_vendorCodeEXSO" />
<s:hidden name="dspDetails.RLS_vendorCodeEXSO" />
<s:hidden name="dspDetails.COL_vendorCodeEXSO" />
<s:hidden name="dspDetails.TRG_vendorCodeEXSO" />
<s:hidden name="dspDetails.HOM_vendorCodeEXSO" />
<s:hidden name="dspDetails.RNT_vendorCodeEXSO" />
<s:hidden name="dspDetails.LAN_vendorCodeEXSO" />
<s:hidden name="dspDetails.MMG_vendorCodeEXSO" />
<s:hidden name="dspDetails.ONG_vendorCodeEXSO" />
<s:hidden name="dspDetails.PRV_vendorCodeEXSO" />
<s:hidden name="dspDetails.AIO_vendorCodeEXSO" />
<s:hidden name="dspDetails.EXP_vendorCodeEXSO" />
<s:hidden name="dspDetails.RPT_vendorCodeEXSO" />
<s:hidden name="dspDetails.SCH_vendorCodeEXSO" />
<s:hidden name="dspDetails.TAX_vendorCodeEXSO" />
<s:hidden name="dspDetails.TAC_vendorCodeEXSO" />
<s:hidden name="dspDetails.TEN_vendorCodeEXSO" />
<s:hidden name="dspDetails.VIS_vendorCodeEXSO" />
<s:hidden name="dspDetails.WOP_vendorCodeEXSO" />
<s:hidden name="dspDetails.REP_vendorCodeEXSO" />
<s:hidden name="dspDetails.SET_vendorCodeEXSO" /> 
<s:hidden name="dspDetails.CAT_vendorCodeEXSO" />
<s:hidden name="dspDetails.CLS_vendorCodeEXSO" />
<s:hidden name="dspDetails.CHS_vendorCodeEXSO" />
<s:hidden name="dspDetails.DPS_vendorCodeEXSO" />
<s:hidden name="dspDetails.HSM_vendorCodeEXSO" />
<s:hidden name="dspDetails.PDT_vendorCodeEXSO" />
<s:hidden name="dspDetails.RCP_vendorCodeEXSO" />
<s:hidden name="dspDetails.SPA_vendorCodeEXSO" />
<s:hidden name="dspDetails.TCS_vendorCodeEXSO" />
<s:hidden name="dspDetails.MTS_vendorCodeEXSO" />
<s:hidden name="dspDetails.DSS_vendorCodeEXSO" />
<s:hidden name="dspDetails.HOB_vendorCodeEXSO" />
<s:hidden name="dspDetails.FLB_vendorCodeEXSO" />
<s:hidden name="dspDetails.FRL_vendorCodeEXSO" />
<s:hidden name="dspDetails.APU_vendorCodeEXSO" />
<s:hidden name="dspDetails.INS_vendorCodeEXSO" />
<s:hidden name="dspDetails.INP_vendorCodeEXSO" />
<s:hidden name="dspDetails.EDA_vendorCodeEXSO" />
<s:hidden name="dspDetails.TAS_vendorCodeEXSO" />
</c:if>
<s:hidden name="serviceOrder.id"/>
	<s:hidden name="secondDescription" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" /> 
	<s:hidden name="fifthDescription" /> <s:hidden name="sixthDescription" /> 
	<s:hidden name="assignees" value="${dspDetails.VIS_assignees}" />
	<s:hidden name="employers" value="${dspDetails.VIS_employers}" />
	<s:hidden name="convenant" value="${dspDetails.VIS_convenant}" /> 
	<c:set var="currencySign" value="${currencySign}"/> 
<s:hidden id="countDSAutomobileAssistanceNotes" name="countDSAutomobileAssistanceNotes" value="<%=request.getParameter("countDSAutomobileAssistanceNotes") %>"/> 
<c:set var="countDSAutomobileAssistanceNotes" value="<%=request.getParameter("countDSAutomobileAssistanceNotes") %>" />
<s:hidden id="countDSColaServiceNotes" name="countDSColaServiceNotes" value="<%=request.getParameter("countDSColaServiceNotes") %>"/> 
<c:set var="countDSColaServiceNotes" value="<%=request.getParameter("countDSColaServiceNotes") %>" />
<s:hidden id="countDSCrossCulturalTrainingNotes" name="countDSCrossCulturalTrainingNotes" value="<%=request.getParameter("countDSCrossCulturalTrainingNotes") %>"/> 
<c:set var="countDSCrossCulturalTrainingNotes" value="<%=request.getParameter("countDSCrossCulturalTrainingNotes") %>" />
<s:hidden id="countDSHomeFindingAssistancePurchaseNotes" name="countDSHomeFindingAssistancePurchaseNotes" value="<%=request.getParameter("countDSHomeFindingAssistancePurchaseNotes") %>"/> 
<c:set var="countDSHomeFindingAssistancePurchaseNotes" value="<%=request.getParameter("countDSHomeFindingAssistancePurchaseNotes") %>" />
<s:hidden id="countDsHomeRentalNotes" name="countDsHomeRentalNotes" value="<%=request.getParameter("countDsHomeRentalNotes") %>"/> 
<c:set var="countDsHomeRentalNotes" value="<%=request.getParameter("countDsHomeRentalNotes") %>" />
<s:hidden id="countDSLanguageNotes" name="countDSLanguageNotes" value="<%=request.getParameter("countDSLanguageNotes") %>"/> 
<c:set var="countDSLanguageNotes" value="<%=request.getParameter("countDSLanguageNotes") %>" />
<s:hidden id="countDSMoveMgmtNotes" name="countDSMoveMgmtNotes" value="<%=request.getParameter("countDSMoveMgmtNotes") %>"/> 
<c:set var="countDSMoveMgmtNotes" value="<%=request.getParameter("countDSMoveMgmtNotes") %>" />
<s:hidden id="countDSOngoingSupportNotes" name="countDSOngoingSupportNotes" value="<%=request.getParameter("countDSOngoingSupportNotes") %>"/> 
<c:set var="countDSOngoingSupportNotes" value="<%=request.getParameter("countDSOngoingSupportNotes") %>" />
<s:hidden id="countDSPreviewTripNotes" name="countDSPreviewTripNotes" value="<%=request.getParameter("countDSPreviewTripNotes") %>"/> 
<c:set var="countDSPreviewTripNotes" value="<%=request.getParameter("countDSPreviewTripNotes") %>" />
<s:hidden id="countDsRelocationAreaInfoOrientationNotes" name="countDsRelocationAreaInfoOrientationNotes" value="<%=request.getParameter("countDsRelocationAreaInfoOrientationNotes") %>"/> 
<c:set var="countDsRelocationAreaInfoOrientationNotes" value="<%=request.getParameter("countDsRelocationAreaInfoOrientationNotes") %>" />
<s:hidden id="countDSRelocationExpenseManagementNotes" name="countDSRelocationExpenseManagementNotes" value="<%=request.getParameter("countDSRelocationExpenseManagementNotes") %>"/>
<c:set var="countDSRelocationExpenseManagementNotes" value="<%=request.getParameter("countDSRelocationExpenseManagementNotes") %>" />
<s:hidden id="countDSRepatriationNotes" name="countDSRepatriationNotes" value="<%=request.getParameter("countDSRepatriationNotes") %>"/> 
<c:set var="countDSRepatriationNotes" value="<%=request.getParameter("countDSRepatriationNotes") %>" />
<s:hidden id="countDSSchoolEducationalCounselingNotes" name="countDSSchoolEducationalCounselingNotes" value="<%=request.getParameter("countDSSchoolEducationalCounselingNotes") %>"/> 
<c:set var="countDSSchoolEducationalCounselingNotes" value="<%=request.getParameter("countDSSchoolEducationalCounselingNotes") %>" />
<s:hidden id="countdsTaxServicesNotes" name="countdsTaxServicesNotes" value="<%=request.getParameter("countdsTaxServicesNotes") %>"/> 
<c:set var="countdsTaxServicesNotes" value="<%=request.getParameter("countdsTaxServicesNotes") %>" />
<s:hidden id="countDSTemporaryAccommodationNotes" name="countDSTemporaryAccommodationNotes" value="<%=request.getParameter("countDSTemporaryAccommodationNotes") %>"/> 
<c:set var="countDSTemporaryAccommodationNotes" value="<%=request.getParameter("countDSTemporaryAccommodationNotes") %>" />
<s:hidden id="countDSTenancyManagementNotes" name="countDSTenancyManagementNotes" value="<%=request.getParameter("countDSTenancyManagementNotes") %>"/> 
<c:set var="countDSTenancyManagementNotes" value="<%=request.getParameter("countDSTenancyManagementNotes") %>" />
<s:hidden id="countDSVisaImmigrationNotes" name="countDSVisaImmigrationNotes" value="<%=request.getParameter("countDSVisaImmigrationNotes") %>"/> 
<c:set var="countDSVisaImmigrationNotes" value="<%=request.getParameter("countDSVisaImmigrationNotes") %>" />
<s:hidden id="countDSWorkPermitNotes" name="countDSWorkPermitNotes" value="<%=request.getParameter("countDSWorkPermitNotes") %>"/> 
<c:set var="countDSWorkPermitNotes" value="<%=request.getParameter("countDSWorkPermitNotes") %>" />
<s:hidden id="countDsHotelBookingsNotes" name="countDsHotelBookingsNotes" value="<%=request.getParameter("countDsHotelBookingsNotes") %>"/> 
<c:set var="countDsHotelBookingsNotes" value="<%=request.getParameter("countDsHotelBookingsNotes") %>" />
<s:hidden id="countDsFlightBookingsNotes" name="countDsFlightBookingsNotes" value="<%=request.getParameter("countDsFlightBookingsNotes") %>"/> 
<c:set var="countDsFlightBookingsNotes" value="<%=request.getParameter("countDsFlightBookingsNotes") %>" />
<s:hidden id="countDSResidencePermitNotes" name="countDSResidencePermitNotes" value="<%=request.getParameter("countDSResidencePermitNotes") %>"/> 
<c:set var="countDSResidencePermitNotes" value="<%=request.getParameter("countDSResidencePermitNotes") %>" />
<s:hidden id="countDSRentalSearchNotes" name="countDSResidencePermitNotes" value="<%=request.getParameter("countDSRentalSearchNotes") %>"/> 
<c:set var="countDSRentalSearchNotes" value="<%=request.getParameter("countDSRentalSearchNotes") %>" />
<s:hidden id="countDSCandidateAssessmentNotes" name="countDSCandidateAssessmentNotes" value="<%=request.getParameter("countDSCandidateAssessmentNotes") %>"/> 
<c:set var="countDSCandidateAssessmentNotes" value="<%=request.getParameter("countDSCandidateAssessmentNotes") %>" />
<s:hidden id="countDSClosingServicesNotes" name="countDSClosingServicesNotes" value="<%=request.getParameter("countDSClosingServicesNotes") %>"/> 
<c:set var="countDSClosingServicesNotes" value="<%=request.getParameter("countDSClosingServicesNotes") %>" />
<s:hidden id="countDSComparableHousingStudyNotes" name="countDSComparableHousingStudyNotes" value="<%=request.getParameter("countDSComparableHousingStudyNotes") %>"/> 
<c:set var="countDSComparableHousingStudyNotes" value="<%=request.getParameter("countDSComparableHousingStudyNotes") %>" />
<s:hidden id="countDSDepartureServicesNotes" name="countDSDepartureServicesNotes" value="<%=request.getParameter("countDSDepartureServicesNotes") %>"/> 
<c:set var="countDSDepartureServicesNotes" value="<%=request.getParameter("countDSDepartureServicesNotes") %>" />
<s:hidden id="countDSHomeSaleMarketingAssistanceNotes" name="countDSHomeSaleMarketingAssistanceNotes" value="<%=request.getParameter("countDSHomeSaleMarketingAssistanceNotes") %>"/> 
<c:set var="countDSHomeSaleMarketingAssistanceNotes" value="<%=request.getParameter("countDSHomeSaleMarketingAssistanceNotes") %>" />
<s:hidden id="countDSPolicyDevelopmentNotes" name="countDSPolicyDevelopmentNotes" value="<%=request.getParameter("countDSPolicyDevelopmentNotes") %>"/> 
<c:set var="countDSPolicyDevelopmentNotes" value="<%=request.getParameter("countDSPolicyDevelopmentNotes") %>" />
<s:hidden id="countDSRelocationCostProjectionsNotes" name="countDSRelocationCostProjectionsNotes" value="<%=request.getParameter("countDSRelocationCostProjectionsNotes") %>"/> 
<c:set var="countDSRelocationCostProjectionsNotes" value="<%=request.getParameter("countDSRelocationCostProjectionsNotes") %>" />
<s:hidden id="countDSSpousalAssistanceNotes" name="countDSSpousalAssistanceNotes" value="<%=request.getParameter("countDSSpousalAssistanceNotes") %>"/> 
<c:set var="countDSSpousalAssistanceNotes" value="<%=request.getParameter("countDSSpousalAssistanceNotes") %>" />
<s:hidden id="countDSTechnologySolutionsNotes" name="countDSTechnologySolutionsNotes" value="<%=request.getParameter("countDSTechnologySolutionsNotes") %>"/> 
<c:set var="countDSTechnologySolutionsNotes" value="<%=request.getParameter("countDSTechnologySolutionsNotes") %>" />
<s:hidden id="countDSMortgageServicesNotes" name="countDSMortgageServicesNotes" value="<%=request.getParameter("countDSMortgageServicesNotes") %>"/> 
<c:set var="countDSMortgageServicesNotes" value="<%=request.getParameter("countDSMortgageServicesNotes") %>" />
<s:hidden id="countDSDestinationsSchoolServicesNotes" name="countDSDestinationsSchoolServicesNotes" value="<%=request.getParameter("countDSDestinationsSchoolServicesNotes") %>"/> 
<c:set var="countDSDestinationsSchoolServicesNotes" value="<%=request.getParameter("countDSDestinationsSchoolServicesNotes") %>" />
<s:hidden id="countDSAirportPickUpNotes" name="countDSAirportPickUpNotes" value="<%=request.getParameter("countDSAirportPickUpNotes") %>"/> 
<c:set var="countDSAirportPickUpNotes" value="<%=request.getParameter("countDSAirportPickUpNotes") %>" />
<s:hidden id="countDSFurnitureRentalNotes" name="countDSFurnitureRentalNotes" value="<%=request.getParameter("countDSFurnitureRentalNotes") %>"/> 
<c:set var="countDSFurnitureRentalNotes" value="<%=request.getParameter("countDSFurnitureRentalNotes") %>" />
<s:hidden id="countDSInternationalInsuranceNotes" name="countDSInternationalInsuranceNotes" value="<%=request.getParameter("countDSInternationalInsuranceNotes") %>"/> 
<c:set var="countDSInternationalInsuranceNotes" value="<%=request.getParameter("countDSInternationalInsuranceNotes") %>" />
<s:hidden id="countDSInternationalPensionNotes" name="countDSInternationalPensionNotes" value="<%=request.getParameter("countDSInternationalPensionNotes") %>"/> 
<c:set var="countDSInternationalPensionNotes" value="<%=request.getParameter("countDSInternationalPensionNotes") %>" />
<s:hidden id="countDSEducationAdviceNotes" name="countDSEducationAdviceNotes" value="<%=request.getParameter("countDSEducationAdviceNotes") %>"/> 
<c:set var="countDSEducationAdviceNotes" value="<%=request.getParameter("countDSEducationAdviceNotes") %>" />
<s:hidden id="countDSTaxSupportNotes" name="countDSTaxSupportNotes" value="<%=request.getParameter("countDSTaxSupportNotes") %>"/> 
<c:set var="countDSTaxSupportNotes" value="<%=request.getParameter("countDSTaxSupportNotes") %>" />
<div id="overlayDSP" style="display:none" ></div>
<div id="ContactDetails" style="display:none"></div>

<s:hidden name="customerFile.id" value="%{customerFile.id}" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat1" value="dd-NNN-yy"/>
<configByCorp:fieldVisibility componentId="component.accPortal.status.hiddenVisibility">
	<sec-auth:authComponent componentId="module.accPortal.status.hideVisibility">
		<s:hidden name="dspDetails.PRV_flightNumber" value="${dspDetails.PRV_flightNumber }"/>
		<s:hidden name="dspDetails.VIS_vendorCode" value="${dspDetails.VIS_vendorCode }"/>
		<s:hidden name="dspDetails.VIS_vendorName" value="${dspDetails.VIS_vendorName }"/>
		<s:hidden name="dspDetails.VIS_vendorCodeEXSO" value="${dspDetails.VIS_vendorCodeEXSO }"/>
		<s:hidden name="dspDetails.VIS_vendorEmail" value="${dspDetails.VIS_vendorEmail }"/>
		<s:hidden name="dspDetails.VIS_vendorContact" value="${dspDetails.VIS_vendorContact }"/>
		<s:hidden name="dspDetails.VIS_workPermitVisaHolderName" value="${dspDetails.VIS_workPermitVisaHolderName}"/>
		<s:hidden name="dspDetails.VIS_primaryEmail" value="${dspDetails.VIS_primaryEmail }"/> 
		<c:if test="${not empty dspDetails.VIS_providerNotificationDate}">
			<s:text id="VIS_providerNotificationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_providerNotificationDate"/></s:text>
			<s:hidden name="dspDetails.VIS_providerNotificationDate" value="%{VIS_providerNotificationDateFormattedValue}"/>
		</c:if>
		<c:if test="${empty dspDetails.VIS_providerNotificationDate}">
			<s:hidden name="dspDetails.VIS_providerNotificationDate"/>
		</c:if> 
		<c:if test="${not empty dspDetails.VIS_visaExpiryDate}">
			<s:text id="VIS_visaExpiryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_visaExpiryDate"/></s:text>
			<s:hidden name="dspDetails.VIS_visaExpiryDate" value="%{VIS_visaExpiryDateFormattedValue}"/>
		</c:if>
		<c:if test="${empty dspDetails.VIS_visaExpiryDate}">
			<s:hidden name="dspDetails.VIS_visaExpiryDate"/>
		</c:if> 
		<c:if test="${not empty dspDetails.VIS_questionnaireSentDate}">
			<s:text id="VIS_questionnaireSentDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.VIS_questionnaireSentDate"/></s:text>
			<s:hidden name="dspDetails.VIS_questionnaireSentDate" value="%{VIS_questionnaireSentDateFormattedValue}"/>
		</c:if>
		<c:if test="${empty dspDetails.VIS_questionnaireSentDate}">
			<s:hidden name="dspDetails.VIS_questionnaireSentDate"/>
		</c:if> 
	</sec-auth:authComponent>
</configByCorp:fieldVisibility>	
<div id="Layer1"  style="width:100%;">
		<div id="newmnav" style="float: left;"> 
		    <ul>
		    
		    <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
		    <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		      <li><a onclick="enableElement();saveOnTabChange('SERVICEORDER');"><span>S/O Details</span></a></li>
		    </c:if>
		     <c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
		     <li><a onclick="enableElement();saveOnTabChange('SERVICEORDER');"><span>Quotes</span></a></li>
		     </c:if>
		    </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.trackingStatus.billingTab">
             <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >	
			  <li><a onclick="enableElement();saveOnTabChange('BILLING');" ><span>Billing</span></a></li>
			  </sec-auth:authComponent>
            </sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">
			<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">  
			  <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			   </c:when> --%>
			   <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			</c:when>
			   <c:otherwise> 
		         <li><a onclick="enableElement();saveOnTabChange('ACCOUNTLINE');"><span>Accounting</span></a></li>
		      </c:otherwise>
		     </c:choose>
		     </c:if> 
		    </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		    <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">  
			  <c:choose> 
			   <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			</c:when>
			   <c:otherwise> 
		         <li><a onclick="enableElement();saveOnTabChange('NEWACCOUNTLINE');"><span>Accounting</span></a></li>
		      </c:otherwise>
		     </c:choose>
		     </c:if> 
		    </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
           	 	<li><a onclick="enableElement();saveOnTabChange('COSTING');"><span>Costing</span></a></li>
           	  </sec-auth:authComponent> 
			  <li  id="newmnav1" style="background:#FFF"><a class="current"><span>Status</span></a></li>
	   <sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.ticketTab">
	    <c:if test="${serviceOrder.job =='RLO'}"> 
		 <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li> 
	   </c:if>
	    </sec-auth:authComponent>	
	    <configByCorp:fieldVisibility componentId="component.standard.claimTab"> 	
	  <sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.claimsTab">
	  <c:if test="${serviceOrder.job =='RLO'}"> 	
		<li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
	  </c:if> 
	  </sec-auth:authComponent>
	  </configByCorp:fieldVisibility>
			  <sec-auth:authComponent componentId="module.tab.summary.summaryTab">
				<li><a onclick="enableElement();saveOnTabChange('SUMMARY');"><span>Summary</span></a></li>
			</sec-auth:authComponent>	
	 		<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.trackingStatus.customerfileTab">  
			  <li><a onclick="enableElement();saveOnTabChange('CUSTOMERFILE');"><span>Customer File</span></a></li>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">  
			   <c:if test="${serviceOrder.job =='RLO'}"> 
		 		<li><a href="soAdditionalDateDetails.html?sid=${serviceOrder.id}" ><span>Critical Dates</span></a></li> 
	  			 </c:if>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.trackingStatus.reportTab">  
			  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Billing&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			 </sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.trackingStatus.auditTab">
			  <li><a onclick="window.open('auditList.html?id=${serviceOrder.id}&tableName=dspDetails&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
			 </sec-auth:authComponent>
			 
           	  <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
           	 	<li><a onclick="enableElement();saveOnTabChange('DOCUMENT');"><span>Document</span></a></li>
           	  </sec-auth:authComponent>
			 </ul> 
		</div> 
      	<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;height:22px;float: none; "><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}"  >
  		<a><img align="middle" id="navigation1" onclick="enableElement();goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" id="navigation2" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" id="navigation3" onclick="enableElement();goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" id="navigation4" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td> 
		<c:if test="${countShip != 1}" >
		<td width="20px" align="left" style="!padding-top:1px;">
		<a><img class="openpopup" id="navigation5" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</td>
		</c:if>
		<c:if test="${countShip == 1}" >
		<td width="20px" align="left" >
  		<a><img align="middle" id="navigation6" src="images/navdisable_05.png"/></a>
  		</td>
  		</c:if>
  		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 1px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400)" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if> 
		</c:if>
		</tr>
		</table> 
		<div class="spn">&nbsp;</div>
     <div style="!margin-top:8px; "> 
      <%@ include file="/WEB-INF/pages/trans/serviceOrderJobHeader.jsp"%>
     </div>
      </div>
      <div id="layer1" style="width:100%; margin-top: 10px;">
<div id="newmnav">
			<ul>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Relocation Details<img id="navigation1" src="images/navarrow.gif" align="absmiddle" /></span></a></li>
           </ul>
       </div><div class="spn" >&nbsp;</div> 
<div id="content" align="center" >
<div id="liquid-round-top">
   <div class="top" style="margin-top: 0px;!margin-top:2px;"><span></span></div>
   <div class="center-content">   
	<table class="" cellspacing="0" cellpadding="0" border="0"  width="100%">
      <c:if test="${serviceOrder.isNetworkRecord  &&  trackingStatus.createdBy=='Networking' }">
      <tr><td>
      <table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
 		  <tr>
 		  <td align="right" class="listwhitetext" width="125px" > Parent Network S/O# &nbsp;</td> 
		<td>
			<s:textfield cssClass="input-textUpper"  name="serviceOrder.bookingAgentShipNumber" size="39" required="true" readonly="true" tabindex="6"/>
		 </td>
 		  </tr></table>
 		  <tr height="10px"></tr>
 		  </td></tr>
 		  </c:if> 
   <tr>
   <td>
   <table cellpadding="0" cellspacing="0" width="100%" style="margin:0px;" >
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Status Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center" style="cursor:default; " >
</td>
<td class="headtab_right">
</td>
</tr>
</table>
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" > 
<td align="right" class="listwhitetext" style="width:77px; !width:81px; padding-right:2px; !padding-right:1px;"><fmt:message key='serviceOrder.coordinator'/>&nbsp;</td>
<td align="left" style="padding-left:2px;"><s:textfield cssClass="input-textUpper"  name="serviceOrder.coordinator" size="25"required="true" readonly="true" tabindex="3"/></td>
<td align="right" class="listwhitetext" width="60px"><fmt:message key='customerFile.status'/></td>
<td align="left" class="listwhitetext"> <s:textfield  cssClass="input-textUpper" name="soStatus" value="%{serviceOrder.status}" size="10" readonly="true" tabindex="4"/> </td>
<td align="right" class="listwhitetext" width="70px"><fmt:message key='customerFile.statusDate'/></td>
<c:if test="${not empty serviceOrder.statusDate}">
<s:text id="statusDate" name="${FormDateValue}"><s:param name="value" value="serviceOrder.statusDate" /></s:text>
<td><s:textfield cssClass="input-textUpper" name="serviceOrder.statusDate" value="%{statusDate}" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="5"/></td>
</c:if>
<c:if test="${empty serviceOrder.statusDate}">
<td align="left" style="width:5px"></td>
<td><s:textfield cssClass="input-textUpper" name="serviceOrder.statusDate" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="5" /></td>
</c:if> 
<td align="right" class="listwhitetext" width="120px">Service Complete Date</td>
<c:if test="${not empty dspDetails.serviceCompleteDate}">
			 <s:text id="dspDetailsSserviceCompleteDateValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.serviceCompleteDate"/></s:text>
			 <td><s:textfield id="serviceCompleteDate" cssClass="input-text" name="dspDetails.serviceCompleteDate" value="%{dspDetailsSserviceCompleteDateValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="serviceCompleteDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.serviceCompleteDate}">
		<td><s:textfield id="serviceCompleteDate" cssClass="input-text" name="dspDetails.serviceCompleteDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="serviceCompleteDate-trigger" name="serviceCompleteDate" onclick="setFieldName(this.name);" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	<c:if test="${empty serviceOrder.id}">
<td  align="left" style="width:195px;!width:190px;"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
 
<%-- Added By Kunal for ticket number: 6092 --%>
<td align="right" width="66" valign="middle" class="listwhitetext" rowspan="2">
	<c:if test="${serviceOrder.vip}">
			<img id="vipImage" src="${pageContext.request.contextPath}/images/vip_icon.png" />
	</c:if>
</td>
   <configByCorp:fieldVisibility componentId="component.tab.dspDetails.HomeFinding">
	    <c:if test="${serviceOrder.job == 'RLO'}">
<td align="right" class="listwhitetext" width="120px">Total DSP Days utilized</td>
<td><s:textfield cssClass="input-text"  name="dspDetails.totalDspDays" size="7"required="true"  maxlength="10"tabindex="3" onkeypress="return isNumberKey(event);"/></td>
</c:if>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table>
   </td>
   </tr>
   <c:if test="${usertype!='PARTNER'}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
	 <tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('net')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Network
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="net">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
 <tr>
		<td align="right" class="listwhitetext" width="125"><fmt:message key='partner.lastName4'/></td>
        <td colspan="4" >
			<s:textfield cssClass="input-textUpper"  name="serviceOrder.bookingAgentName" size="39" required="true" readonly="true" tabindex="6"/>
			<img align="top" class="openpopup" width="17" height="20" onclick="findNetworkAgent(this,'BA');" src="<c:url value='/images/address2.png'/>" />&nbsp;	
		</td> 
		<td align="right" class="listwhitetext" >Network&nbsp;</td>
		<td colspan="3">
<table cellpadding="1" cellspacing="0" style="margin:0px; padding:0px;" border="0">
<tr>
<td><s:textfield cssClass="input-text" name="trackingStatusNetworkPartnerCode" value="${trackingStatus.networkPartnerCode}"  size="5" maxlength="10" onchange="valid(this,'special');checkNetworkPartnerName();changeStatus();" tabindex="6" onselect="" onkeypress=" " onfocus=" "/></td>
<td width="17"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpenPartnerNetwork();changeStatus();" id="openpopupnetwork.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
<td><s:textfield  cssClass="input-textUpper" name="trackingStatusNetworkPartnerName" value="${trackingStatus.networkPartnerName }" size="25" tabindex="6" /></td>
<td align="left" valign="top">
	<div id="hidImageNET">
			<img align="top" class="openpopup" width="17" height="20" onclick="findNetworkAgent(this,'NT');" src="<c:url value='/images/address2.png'/>" />
	</div>
	</td>
<td align="left">
<div id="hidNTImageNET" style="vertical-align:middle; display: none;"><a><img id="navigation7" class="openpopup" onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value,'hidNTImageNET');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
</td>
<script type="text/javascript">
setTimeout("showPartnerAlert('onload',document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value,'hidNTImageNET')");
			</script>				
			</tr>
			</table>
		</td>
			<c:if test="${dspDetails.serviceCompleteDate!=null && not empty dspDetails.serviceCompleteDate && (not empty serviceOrder.customerRating || not empty serviceOrder.feedBack)}">
			<td  align="right" colspan="3" style="width:115px;!width:100px;"><img id="countDSNetworkFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSNetworkFeebackId','NET');"/></td>
			<div id="DSNetworkFeebackId" class="cfDiv"></div>
			</c:if>
		
	</tr>
	<tr>
			<td align="right" class="listwhitetext" >Booking Agent Contact&nbsp;</td>
			<td colspan="4">
			<table cellpadding="1" cellspacing="0" style="margin:0px; padding:0px;">
			<tr>
			<td>
			<s:textfield  cssClass="input-text" name="trackingStatusBookingAgentContact" value="${trackingStatus.bookingAgentContact }" onchange="" size="39" tabindex="7" /></td>
			<td width="56"><img align="left" class="openpopup" width="17" height="20" onclick="getContact('BA1',document.forms['dspDetailsForm'].elements['serviceOrder.bookingAgentCode'].value);" id="openpopupnetwork.img" src="<c:url value='/images/plus-small.png'/>" /></td>
			</tr>
			</table>
			</td>
			<td width="115" align="right" class="listwhitetext" >Network Contact&nbsp;</td>			
			<td colspan="4">
			<table cellpadding="1" cellspacing="0" style="margin:0px; padding:0px;">
			<tr>
			<td><s:textfield  cssClass="input-text" name="trackingStatusNetworkContact" value="${trackingStatus.networkContact }" size="39" tabindex="7" /></td>			
			</tr>
			</table>
			</td>
			
	</tr>
	<tr>
	<td align="right" class="listwhitetext" >Booking Agent Email&nbsp;</td>
	<td colspan="4">
	<table cellpadding="1" cellspacing="0" style="margin:0px; padding:0px;">
	<tr>	
	<td>			
	<s:textfield  cssClass="input-text" name="trackingStatusBookingAgentEmail" value="${trackingStatus.bookingAgentEmail }" size="39" onchange="checkEmail('trackingStatusBookingAgentEmail');" onblur="showEmailImage();" tabindex="8" /></td>
	<td><div id="hidEmailImage" style="vertical-align:middle; "><img class="openpopup" id="email1" onclick="sendEmail(document.forms['dspDetailsForm'].elements['trackingStatusBookingAgentEmail'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${trackingStatus.bookingAgentEmail}"/></td>
	</tr>
	</table>
	</td>
	 <td width="115" align="right" class="listwhitetext" >Network Email&nbsp;</td>			
			<td colspan="4">
			<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;">
			<tr>
			<td><s:textfield  cssClass="input-text" name="trackingStatusNetworkEmail" value="${trackingStatus.networkEmail }" onchange="checkEmail('trackingStatusNetworkEmail');showHideAddress();changeStatus();" onblur="showNetworkEmailImage();" size="39" tabindex="8" /></td>			
			<td align="left">
			<div id="hidEmailImage1NET" style="display:none; vertical-align:middle;padding-left:3px; ">
			<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['trackingStatusNetworkEmail'].value)" id="email2" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${trackingStatus.networkEmail}"/>
			</div>
			</td>
			</tr>
			</table>
			</td>
		    
		    <c:if test="${not empty dspDetails.NET_emailSent}"><td align="right" class="listwhitetext">Email Sent</td>
				 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.NET_emailSent"/></s:text>
				 <td><s:textfield id="NET_emailSent" cssClass="input-textUpper" name="dspDetails.NET_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
				<td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendNET" value="Resend Email" disabled=true onclick="resendEmail('NET');"/></td>
		    </c:if>
	</tr>
	<tr>
	<td colspan="4" align="left">  
	<c:if test="${networkAgent}"> 
	<table cellpadding="1" cellspacing="0" style="margin:0px; padding:0px;width:389px;text-align:right;">
			<tr>
			<td width="10" style="min-width:17px;">&nbsp;</td>
			 <td><div class="link-up" id="linkup_bookingAgent"  onclick="return  checkMoveTypeAgent('BOK')" >&nbsp;</div></td>		
			<td align="right" class="listwhitetext" width="45">Ext&nbsp;SO&nbsp;#&nbsp;</td>
			<td align="left"><s:textfield name="trackingStatusBookingAgentExSO"  value="${trackingStatus.bookingAgentExSO }" cssClass="input-textUpper shiftExtRight" size="39" readonly="true" tabindex="13"/></td>
			</tr>
			</table> 
	</c:if>
	</td> 
 	<td width="56"></td>
			<c:choose>
			<c:when test="${(isNetworkBookingAgent==true && !(networkAgent) )}">
			<td align="right">
			<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;">			
			<tr>			
			<td><div class="link-up" id="linkup_Network" onclick="return  checkMoveTypeAgent('NET')" >&nbsp;</div></td> 
			<td style="!width:10px">&nbsp;</td>
			<td align="right" class="listwhitetext" >Ext&nbsp;SO&nbsp;#&nbsp;</td> 
			</tr>
			</table>
			</td>
			<td align="left" colspan="3"><s:textfield name="trackingStatusNetworkAgentExSO"  value="${trackingStatus.networkAgentExSO}" cssClass="input-textUpper shiftExtRight" size="39" readonly="true" tabindex="13"/></td>
			</c:when>
			<c:otherwise>
			<td align="left" colspan="4"></td>
			<s:hidden name="trackingStatusNetworkAgentExSO"  />
			</c:otherwise>
			</c:choose>
	</tr>
	</tbody></table></div></td>
			</tr>
	</table> 
	</td> 
	</tr> 
	</c:if> 
<c:if test="${usertype!='PARTNER'}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'CAR')>-1}">
	<!-- begin car -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('car')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='CAR'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="car">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" colspan="2" width="365">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"��style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.CAR_vendorCode" id="CAR_vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorNameRelo('CAR_','${dspDetails.CAR_vendorCodeEXSO}'),changeStatus(),chkIsVendorRedSky('CAR_');" onblur="showContactImage('CAR_')" /></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('CAR_','${dspDetails.CAR_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.CAR_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
   <td align="left" valign="top" width="20">
	<div id="hidImageCAR" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','CAR_');" src="<c:url value='/images/address2.png'/>" />
	</div>
	</td>
	<td align="left">
<div id="hidNTImageCAR" style="vertical-align: middle;display: block;">
<a>
<img class="openpopup"  align="top" src="${pageContext.request.contextPath}/images/navarrows_05.png" 
onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.CAR_vendorCode'].value,
'hidNTImageCAR');">
</a>
</div>
</td>
	</td>
<td align="left">
<div style="float:left;width:52px;">
<div id="hidCAR_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidCAR_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.CAR_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_serviceStartDate"/></s:text>
			 <td><s:textfield id="CAR_serviceStartDate" cssClass="input-text" name="dspDetails.CAR_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CAR_serviceStartDate}">
		<td><s:textfield id="CAR_serviceStartDate" cssClass="input-text" name="dspDetails.CAR_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="CAR_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSAutomobileAssistanceNotes == '0' || countDSAutomobileAssistanceNotes == '' || countDSAutomobileAssistanceNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSAutomobileAssistanceNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsAutomobileAssistan&imageId=countDSAutomobileAssistanceNotesImage&fieldId=countDSAutomobileAssistanceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsAutomobileAssistan&imageId=countDSAutomobileAssistanceNotesImage&fieldId=countDSAutomobileAssistanceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSAutomobileAssistanceNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsAutomobileAssistan&imageId=countDSAutomobileAssistanceNotesImage&fieldId=countDSAutomobileAssistanceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsAutomobileAssistan&imageId=countDSAutomobileAssistanceNotesImage&fieldId=countDSAutomobileAssistanceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
<c:if test="${fn1:indexOf(customerSurveyServiceDetails,'CAR')>-1}">
<td  align="right" style="width:115px;!width:100px;"><img id="countDSAutomobileAssistanceFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSAutomobileAssistanceFeebackId','CAR');"/></td>
<div id="DSAutomobileAssistanceFeebackId" class="cfDiv"></div>
</c:if>
</tr>
<tr>
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">
<td align="right"><div class="link-up" id="linkup_CAR_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('CAR_','${dspDetails.CAR_vendorCode}','${dspDetails.CAR_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"�width="350px"�style="margin:0px;padding:0px">
<tr>		
<td align="right" class="listwhitetext" width="69px" >Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.CAR_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" readonly="true"/></td>
 </tr></table></td>
 </c:if>
<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isCARmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'CAR')>-1}">
	 	<c:set var="isCARmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="car" id="car" onclick="changeStatus();sendMailVendor('CAR');" value="${isCARmailServiceType}" fieldValue="true" disabled="${isCARmailServiceType}"/></td>
 </configByCorp:fieldVisibility>
</tr> 
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'CAR')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2">
<s:textfield cssClass="input-text" id="CAR_vendorContact" key="dspDetails.CAR_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />

 <div id="CAR_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('CAR',this);" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.CAR_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_serviceEndDate"/></s:text>
			 <td><s:textfield id="CAR_serviceEndDate" cssClass="input-text" name="dspDetails.CAR_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CAR_serviceEndDate}">
		<td><s:textfield id="CAR_serviceEndDate" cssClass="input-text" name="dspDetails.CAR_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.CAR_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    
	    <c:if test="${not empty dspDetails.CAR_emailSent && fn1:indexOf(surveyEmailList,'CAR')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_emailSent"/></s:text>
			 <td><s:textfield id="CAR_emailSent" cssClass="input-textUpper" name="dspDetails.CAR_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendCAR" value="Resend Email" disabled=true onclick="resendEmail('CAR');"/></td>
	    </c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="CAR_vendorEmail" key="dspDetails.CAR_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
<td align="left" valign="top"><div id="hidEmailImageCAR" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.CAR_vendorEmail'].value)" id="emailCAR" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.CAR_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.CAR_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedCAR_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.CAR_displyOtherVendorCode}">
	 <c:set var="ischeckedCAR_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.CAR_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedCAR_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" style="margin:0px;">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>	
</table>
<table>
	<tr>
	<td align="right"   class="listwhitetext">Notification Date</td>
   	<c:if test="${not empty dspDetails.CAR_notificationDate}">
	 <s:text id="notificationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_notificationDate"/></s:text>
	 <td width="65px" ><s:textfield id="CAR_notificationDate" cssClass="input-text" name="dspDetails.CAR_notificationDate" value="%{notificationDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_notificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
   <c:if test="${empty dspDetails.CAR_notificationDate}">
	<td width="65px" ><s:textfield id="CAR_notificationDate" cssClass="input-text" name="dspDetails.CAR_notificationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_notificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
 </c:if> 
 	<td align="right"   class="listwhitetext">Pick-up Date</td>
   	<c:if test="${not empty dspDetails.CAR_pickUpDate}">
	 <s:text id="pickUpDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_pickUpDate"/></s:text>
	 <td width="65px" ><s:textfield id="CAR_pickUpDate" cssClass="input-text" name="dspDetails.CAR_pickUpDate" value="%{pickUpDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_pickUpDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
   <c:if test="${empty dspDetails.CAR_pickUpDate}">
	<td width="65px" ><s:textfield id="CAR_pickUpDate" cssClass="input-text" name="dspDetails.CAR_pickUpDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_pickUpDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
 </c:if>
	</tr>
		<tr>
	<td align="right"   class="listwhitetext">Registration Date</td>
   	<c:if test="${not empty dspDetails.CAR_registrationDate}">
	 <s:text id="registrationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_registrationDate"/></s:text>
	 <td width="65px" ><s:textfield id="CAR_registrationDate" cssClass="input-text" name="dspDetails.CAR_registrationDate" value="%{registrationDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_registrationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
   <c:if test="${empty dspDetails.CAR_registrationDate}">
	<td width="65px" ><s:textfield id="CAR_registrationDate" cssClass="input-text" name="dspDetails.CAR_registrationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_registrationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
 </c:if> 
 	<td align="right"   class="listwhitetext">Delivery Date</td>
   	<c:if test="${not empty dspDetails.CAR_deliveryDate}">
	 <s:text id="deliveryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_deliveryDate"/></s:text>
	 <td width="65px" ><s:textfield id="CAR_deliveryDate" cssClass="input-text" name="dspDetails.CAR_deliveryDate" value="%{deliveryDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_deliveryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
   <c:if test="${empty dspDetails.CAR_deliveryDate}">
	<td width="65px" ><s:textfield id="CAR_deliveryDate" cssClass="input-text" name="dspDetails.CAR_deliveryDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_deliveryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
 </c:if>
	</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.CAR_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>	
</table>	
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr> 
	</c:if> 
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'COL')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('col')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center">
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='COL'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="col">
  <table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365" colspan="2">
<table class="detailTabLabel" border="0" �cellspacing="0"�cellpadding="0"��style="margin:0px;" >
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.COL_vendorCode" id="COL_vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorNameRelo('COL_','${dspDetails.COL_vendorCodeEXSO}'),changeStatus(),chkIsVendorRedSky('COL_');" onblur="showContactImage('COL_')" /></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('COL_','${dspDetails.COL_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.COL_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	<td align="left" valign="top">
		<div id="hidImageCOL" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','COL_');" src="<c:url value='/images/address2.png'/>" />
	</div></td>
	<td align="left" valign="top"><div id="hidNTImageCOL" style="display: block;">
			<img align="top" class="openpopup"  onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.COL_vendorCode'].value,
			'hidNTImageCOL');" src="${pageContext.request.contextPath}/images/navarrows_05.png" />
			</div></td>
	</td>
	<td align="left">
<div style="float:left;width:52px;">
<div id="hidCOL_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidCOL_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.COL_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.COL_serviceStartDate"/></s:text>
			 <td><s:textfield id="COL_serviceStartDate" cssClass="input-text" name="dspDetails.COL_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="COL_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.COL_serviceStartDate}">
		<td><s:textfield id="COL_serviceStartDate" cssClass="input-text" name="dspDetails.COL_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="COL_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSColaServiceNotes == '0' || countDSColaServiceNotes == '' || countDSColaServiceNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSColaServiceNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsCola&imageId=countDSColaServiceNotesImage&fieldId=countDSColaServiceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsCola&imageId=countDSColaServiceNotesImage&fieldId=countDSColaServiceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countdspDetailsNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsCola&imageId=countDSColaServiceNotesImage&fieldId=countDSColaServiceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsCola&imageId=countDSColaServiceNotesImage&fieldId=countDSColaServiceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
<c:if test="${fn1:indexOf(customerSurveyServiceDetails,'COL')>-1}">
<td  align="right" style="width:115px;!width:100px;"><img id="countDSColaServiceFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSColaServiceFeebackId','COL');"/></td>
<div id="DSColaServiceFeebackId" class="cfDiv"></div>
</c:if>
</tr> 
<tr>
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">
<td align="right"><div class="link-up" id="linkup_COL_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('COL_','${dspDetails.COL_vendorCode}','${dspDetails.COL_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"� width="350px"�style="margin:0px;padding:0px">
<tr>			
<td align="right" class="listwhitetext" width="69px" >Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.COL_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" readonly="true" /></td>
</tr></table></td>
</c:if><configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isCOLmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'COL')>-1}">
	 	<c:set var="isCOLmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="col" id="col" onclick="changeStatus();sendMailVendor('COL');" value="${isCOLmailServiceType}" fieldValue="true" disabled="${isCOLmailServiceType}"/></td>
</configByCorp:fieldVisibility>
</tr> 
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'COL')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" id="COL_vendorContact" key="dspDetails.COL_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />

 <div id="COL_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('COL');" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.COL_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.COL_serviceEndDate"/></s:text>
			 <td><s:textfield id="COL_serviceEndDate" cssClass="input-text" name="dspDetails.COL_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="COL_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.COL_serviceEndDate}">
		<td><s:textfield id="COL_serviceEndDate" cssClass="input-text" name="dspDetails.COL_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="COL_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.COL_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    
	    <c:if test="${not empty dspDetails.COL_emailSent && fn1:indexOf(surveyEmailList,'COL')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.COL_emailSent"/></s:text>
			 <td><s:textfield id="COL_emailSent" cssClass="input-textUpper" name="dspDetails.COL_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendCOL" value="Resend Email" disabled=true onclick="resendEmail('COL');"/></td>
	    </c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="COL_vendorEmail" key="dspDetails.COL_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
<td align="left" valign="top"><div id="hidEmailImageCOL" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.COL_vendorEmail'].value)" id="emailCOL" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.COL_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.COL_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedCOL_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.COL_displyOtherVendorCode}">
	 <c:set var="ischeckedCOL_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.COL_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedCOL_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table align="left" class="detailTabLabel">

<tr> 
<td  align="right" class="listwhitetext" >Estimated&nbsp;Tax&nbsp;Allowance</td>
<td ><s:textfield  cssClass="input-text" name="dspDetails.COL_estimatedTaxAllowance" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
<td></td>
<td align="right" width="225px" class="listwhitetext">Notification Date</td>
	    <c:if test="${not empty dspDetails.COL_notificationDate}">
			 <s:text id="customerFileNotificationToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.COL_notificationDate"/></s:text>
			 <td width="50"><s:textfield id="COL_notificationDate" cssClass="input-text" name="dspDetails.COL_notificationDate" value="%{customerFileNotificationToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="COL_notificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.COL_notificationDate}">
		<td width="50"><s:textfield id="COL_notificationDate" cssClass="input-text" name="dspDetails.COL_notificationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="COL_notificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr>
<tr>
<td  align="right" class="listwhitetext" >Estimated&nbsp;Housing&nbsp;Allowance</td>
<td ><s:textfield  cssClass="input-text" name="dspDetails.COL_estimatedHousingAllowance" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
</tr>
<tr>
<td  align="right" class="listwhitetext" >Estimated&nbsp;Transportation&nbsp;Allowance</td>
<td ><s:textfield  cssClass="input-text" name="dspDetails.COL_estimatedTransportationAllowance" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
</tr>
<tr>
<td  align="right" class="listwhitetext" >Estimated Allowance&nbsp;${systemDefaultCurrency}</td>
    <td ><s:textfield  cssClass="input-text" name="dspDetails.COL_estimatedAllowanceEuro" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
<td width="85px" align="right" class="listwhitetext" colspan="0"></td>
<td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.COL_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td>
</tr>
<tr><td></td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if> 
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TRG')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('trg')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='TRG'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="trg">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365" colspan="2">
<table class="detailTabLabel" border="0" cellspacing="0"�cellpadding="0"��style="margin:0px;" >
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.TRG_vendorCode" id="TRG_vendorCode" onchange="checkVendorNameRelo('TRG_','${dspDetails.TRG_vendorCodeEXSO}');changeStatus();chkIsVendorRedSky('TRG_');" readonly="false" size="5" maxlength="10" onblur="showContactImage('TRG_')" /></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('TRG_','${dspDetails.TRG_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.TRG_vendorName" readonly="true" cssStyle="width:19.5em" maxlength="200"  />
			<td align="left" valign="top"><div id="hidImageTRG" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','TRG_');" src="<c:url value='/images/address2.png'/>" />
	</div></td>
	<td align="left" valign="top"><div id="hidNTImageTRG" style="display: block;">
			<img align="top" class="openpopup"  onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.TRG_vendorCode'].value,
			'hidNTImageTRG');" src="${pageContext.request.contextPath}/images/navarrows_05.png" />
			</div></td>
	</td>
	<td align="left">
<div style="float:left;width:52px;">
<div id="hidTRG_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidTRG_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.TRG_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TRG_serviceStartDate"/></s:text>
			 <td><s:textfield id="TRG_serviceStartDate" cssClass="input-text" name="dspDetails.TRG_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TRG_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TRG_serviceStartDate}">
		<td><s:textfield id="TRG_serviceStartDate" cssClass="input-text" name="dspDetails.TRG_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TRG_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</td>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
  <c:if test="${not empty dspDetails.id}">
 <c:choose>
 <c:when test="${countDSCrossCulturalTrainingNotes == '0' || countDSCrossCulturalTrainingNotes == '' || countDSCrossCulturalTrainingNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSCrossCulturalTrainingNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsCrossCulturalTrain&imageId=countDSCrossCulturalTrainingNotesImage&fieldId=countDSCrossCulturalTrainingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsCrossCulturalTrain&imageId=countDSCrossCulturalTrainingNotesImage&fieldId=countDSCrossCulturalTrainingNotes&decorator=popup&popup=true',800,600);" ></a></td> 
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSCrossCulturalTrainingNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsCrossCulturalTrain&imageId=countDSCrossCulturalTrainingNotesImage&fieldId=countDSCrossCulturalTrainingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsCrossCulturalTrain&imageId=countDSCrossCulturalTrainingNotesImage&fieldId=countDSCrossCulturalTrainingNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
<c:if test="${fn1:indexOf(customerSurveyServiceDetails,'TRG')>-1}">
<td  align="right" style="width:115px;!width:100px;"><img id="countDSCrossCulturalTrainingFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSCrossCulturalTrainingFeebackId','TRG');"/></td>
<div id="DSCrossCulturalTrainingFeebackId" class="cfDiv"></div>
</c:if> 
</tr>
<tr>
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">
<td align="right"><div class="link-up" id="linkup_TRG_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('TRG_','${dspDetails.TRG_vendorCode}','${dspDetails.TRG_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"� width="350px"�style="margin:0px;padding:0px">
<tr>			
<td align="right" class="listwhitetext" width="69px"  >Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.TRG_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" readonly="true" /></td>
</tr></table></td>
</c:if><configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isTRGmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'TRG')>-1}">
	 	<c:set var="isTRGmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="trg" id="trg" onclick="changeStatus();sendMailVendor('TRG');" value="${isTRGmailServiceType}" fieldValue="true" disabled="${isTRGmailServiceType}"/></td>
</configByCorp:fieldVisibility>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" id="TRG_vendorContact" key="dspDetails.TRG_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();"/>

 <div id="TRG_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('TRG');" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.TRG_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TRG_serviceEndDate"/></s:text>
			 <td><s:textfield id="TRG_serviceEndDate" cssClass="input-text" name="dspDetails.TRG_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TRG_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TRG_serviceEndDate}">
		<td><s:textfield id="TRG_serviceEndDate" cssClass="input-text" name="dspDetails.TRG_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TRG_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.TRG_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    
	    <c:if test="${not empty dspDetails.TRG_emailSent && fn1:indexOf(surveyEmailList,'TRG')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TRG_emailSent"/></s:text>
			 <td><s:textfield id="TRG_emailSent" cssClass="input-textUpper" name="dspDetails.TRG_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendTRG" value="Resend Email" disabled=true onclick="resendEmail('TRG');"/></td>
	    </c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="TRG_vendorEmail" key="dspDetails.TRG_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();" />
<td align="left" valign="top"><div id="hidEmailImageTRG" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.TRG_vendorEmail'].value)" id="emailTRG" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.TRG_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.TRG_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedTRG_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.TRG_displyOtherVendorCode}">
	 <c:set var="ischeckedTRG_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.TRG_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedTRG_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility> 
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table cellpadding="2" style="margin-left:100px" class="detailTabLabel">
<tr>
<td align="right" class="listwhitetext" >Employee Hours/Days Authorized</td>
<td align="left" class="listwhitetext" width="" ><s:textfield cssClass="input-text" key="dspDetails.TRG_employeeDaysAuthorized" readonly="false" size="7" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" key="dspDetails.TRG_employeeTime"	list="%{timeduration}" cssStyle="width:65px" onchange="changeStatus();"/></td>
<td width="" ></td>
</tr>
<tr>
<td align="right" class="listwhitetext" >Spouse Hours/Days Authorized </td>
<td align="left" class="listwhitetext" width="" ><s:textfield cssClass="input-text" key="dspDetails.TRG_spouseDaysAuthorized" readonly="false" size="7" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" key="dspDetails.TRG_spouseTime" list="%{timeduration}" cssStyle="width:65px" onchange="changeStatus();"/></td>
 <td width="" ></td>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.TRG_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr><td></td></tr>
</table> 
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if> 
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'HOM')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('hom')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='HOM'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="hom">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3"
		border="0">
		<tbody>
			<tr>
				<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
				<td align="left" width="360" colspan="2">
				<table class="detailTabLabel" border="0" cellspacing="0"�cellpadding="0"��style="margin:0px;" >
					<tr>
						<td align="left" class="listwhitetext"><s:textfield
							cssClass="input-text" key="dspDetails.HOM_vendorCode" readonly="false" id="HOM_vendorCode"	size="5" maxlength="10" onchange="checkVendorNameRelo('HOM_','${dspDetails.HOM_vendorCodeEXSO}'),chkIsVendorRedSky('HOM_'),changeStatus();" onblur="showContactImage('HOM_')" /></td>
						<td align="left" width="10"><img id="imgId2" align="left"
							class="openpopup" width="17" height="20" onclick="winOpenDest('HOM_','${dspDetails.HOM_vendorCodeEXSO}');"
							src="<c:url value='/images/open-popup.gif'/>" /></td>
						<td align="left" class="listwhitetext" style="padding-left:4px">
						<s:textfield cssClass="input-text" key="dspDetails.HOM_vendorName" readonly="true" cssStyle="width:19.5em" maxlength="200" onchange="changeStatus();"/>
						 <td align="left" valign="top"><div id="hidImageHOM" style="display: block;">
						<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','HOM_');" src="<c:url value='/images/address2.png'/>" />
						</div></td>
						<td align="left" valign="top">
						<div id="hidNTImageHOM" style="display: block;">
							<img align="top" class="openpopup"  onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.HOM_vendorCode'].value,'hidNTImageHOM');" src="${pageContext.request.contextPath}/images/navarrows_05.png" />
			            </div>
			            </td>
						</td>
						<td align="left">
<div style="float:left;width:52px;">
<div id="hidHOM_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidHOM_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
					</tr>
				</table>
				</td>
				<td align="right" width="100px" class="listwhitetext">Service Start</td>
				<c:if
					test="${not empty dspDetails.HOM_serviceStartDate}">
					<s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}">
						<s:param name="value" value="dspDetails.HOM_serviceStartDate" />
					</s:text>
					<td><s:textfield id="HOM_serviceStartDate" cssClass="input-text" name="dspDetails.HOM_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"
						onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
					<td><img id="HOM_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if
					test="${empty dspDetails.HOM_serviceStartDate}">
					<td><s:textfield id="HOM_serviceStartDate" cssClass="input-text"
						name="dspDetails.HOM_serviceStartDate"
						cssStyle="width:65px" maxlength="11" readonly="true"
						onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
					<td><img id="HOM_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if> 
				<c:if test="${empty dspDetails.id}">
					<td align="right" style="width:115px;!width:190px;"><img id="imgId3"
						src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
						HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
				</c:if>
				<c:if test="${not empty dspDetails.id}">
					<c:choose>
						<c:when
							test="${countDSHomeFindingAssistancePurchaseNotes == '0' || countDSHomeFindingAssistancePurchaseNotes == '' || countDSHomeFindingAssistancePurchaseNotes == null}">
							<td align="right" style="width:115px;!width:190px;"><img
								id="countDSHomeFindingAssistancePurchaseNotesImage"
								src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
								HEIGHT=17 WIDTH=50
								onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsHomePurchase&imageId=countDSHomeFindingAssistancePurchaseNotesImage&fieldId=countDSHomeFindingAssistancePurchaseNotes&decorator=popup&popup=true',800,600);" /><a
								onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsHomePurchase&imageId=countDSHomeFindingAssistancePurchaseNotesImage&fieldId=countDSHomeFindingAssistancePurchaseNotes&decorator=popup&popup=true',800,600);"></a></td>
						</c:when>
						<c:otherwise>
							<td align="right" style="width:115px;!width:100px;"><img
								id="countDSHomeFindingAssistancePurchaseNotesImage"
								src="${pageContext.request.contextPath}/images/notes_open1.jpg"
								HEIGHT=17 WIDTH=50
								onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsHomePurchase&imageId=countDSHomeFindingAssistancePurchaseNotesImage&fieldId=countDSHomeFindingAssistancePurchaseNotes&decorator=popup&popup=true',800,600);" /><a
								onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsHomePurchase&imageId=countDSHomeFindingAssistancePurchaseNotesImage&fieldId=countDSHomeFindingAssistancePurchaseNotes&decorator=popup&popup=true',800,600);"></a></td>
						</c:otherwise>
					</c:choose>
				</c:if> 
				<c:if test="${fn1:indexOf(customerSurveyServiceDetails,'HOM')>-1}">
               <td  align="right" style="width:115px;!width:100px;"><img id="countDSHomeFindingAssistanceFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSHomeFindingAssistanceFeebackId','HOM');"/></td>
              <div id="DSHomeFindingAssistanceFeebackId" class="cfDiv"></div>
             </c:if>
			</tr>
<tr>
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">
				<td align="right"><div class="link-up" id="linkup_HOM_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('HOM_','${dspDetails.HOM_vendorCode}','${dspDetails.HOM_vendorCodeEXSO}')" >&nbsp;</div></td>
				<td align="left" width="300" colspan="2">
                <table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0" width="350px"��style="margin:0px; padding:0px">
                <tr>			
				<td align="right" class="listwhitetext" width="69px" >Ext SO #&nbsp;</td>
				 <td><s:textfield name="dspDetails.HOM_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" readonly="true" /></td>
				</tr></table></td>
</c:if><configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isHOMmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'HOM')>-1}">
	 	<c:set var="isHOMmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="hom" id="hom" onclick="changeStatus();sendMailVendor('HOM');" value="${isHOMmailServiceType}" fieldValue="true" disabled="${isHOMmailServiceType}"/></td>
</configByCorp:fieldVisibility>
</tr>
			<tr>
				<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'HOM')>-1}"><font color="red" size="2">*</font></c:if></td>
				<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" id="HOM_vendorContact" key="dspDetails.HOM_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />				
			
					 <div id="HOM_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('HOM');" src="<c:url value='/images/plus-small.png'/>" /></div>
					</td>
				<td align="right" class="listwhitetext">Service Finish</td>
				<c:if
					test="${not empty dspDetails.HOM_serviceEndDate}">
					<s:text id="customerFileSubmissionToTranfFormattedValue"
						name="${FormDateValue}">
						<s:param name="value"
							value="dspDetails.HOM_serviceEndDate" />
					</s:text>
					<td><s:textfield id="HOM_serviceEndDate" cssClass="input-text"
						name="dspDetails.HOM_serviceEndDate"
						value="%{customerFileSubmissionToTranfFormattedValue}"
						cssStyle="width:65px" maxlength="11" readonly="true"
						onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
					<td><img id="HOM_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty dspDetails.HOM_serviceEndDate}">
					<td><s:textfield id="HOM_serviceEndDate" cssClass="input-text"
						name="dspDetails.HOM_serviceEndDate"
						cssStyle="width:65px" maxlength="11" readonly="true"
						onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
					<td><img id="HOM_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.HOM_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
				</c:if>
	    
	    <c:if test="${not empty dspDetails.HOM_emailSent && fn1:indexOf(surveyEmailList,'HOM')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_emailSent"/></s:text>
			 <td><s:textfield id="HOM_emailSent" cssClass="input-textUpper" name="dspDetails.HOM_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendHOM" value="Resend Email" disabled=true onclick="resendEmail('HOM');"/></td>
	    </c:if>
</tr>
			<tr>
				<td align="right" class="listwhitetext" width="83">Vendor Email</td>
				<td align="left" class="listwhitetext" width="300">
				<s:textfield cssClass="input-text" key="dspDetails.HOM_vendorEmail" readonly="false" id="HOM_vendorEmail" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
				<td align="left" valign="top"><div id="hidEmailImageHOM"  style="display: block;">
				<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.HOM_vendorEmail'].value)" id="emailHOM" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.HOM_vendorEmail}"/>
				</div>					</td>
					</td>
					<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
					<td align="right" class="listwhitetext" >Payment&nbsp;of&nbsp;Referral<c:if test="${contactValid=='true'}"><font color="red" size="2">*</font></c:if></td>
						<td colspan="3" ><s:select cssClass="list-menu" id="HOM_paymentResponsibility" name="dspDetails.HOM_paymentResponsibility" list="%{paymentReferral}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
					</configByCorp:fieldVisibility>
					<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
				<c:set var="ischeckedHOM_displyOtherVendorCode" value="false" />
				    <c:if test="${dspDetails.HOM_displyOtherVendorCode}">
					 <c:set var="ischeckedHOM_displyOtherVendorCode" value="true" />
					</c:if>
				   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
				  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.HOM_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedHOM_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
			</configByCorp:fieldVisibility>
			</tr>
		</tbody>
	</table>
	<table width="100%" cellpadding="2" class="detailTabLabel">
		<tr>
			<td align="left" class="vertlinedata"></td>
		</tr>
	</table>
	<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
	<table width="" border="0" style="margin-left:19px" class="detailTabLabel">
	
	
	
	<tr>
		<!--<td align="right" class="listwhitetext" width="83">Agent&nbsp;Code<font color="red" size="2">*</font></td>
		<td align="left" colspan="1" width="365">
		<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"��style="margin:0px;">
		<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HOM_agentCode" readonly="false" size="5" maxlength="10" onchange="findBillingIdName('dspDetails.HOM_agentCode','dspDetails.HOM_agentName');changeStatus();"  /></td>
			<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="setBillToCode1('dspDetails.HOM_agentCode','dspDetails.HOM_agentName','dspDetails.HOM_agentEmail');document.forms['dspDetailsForm'].elements['dspDetails.HOM_agentCode'].focus();changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
			<td align="left" class="listwhitetext" style="padding-left:4px">
			<s:textfield	cssClass="input-text" key="dspDetails.HOM_agentName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
			</td>
		</tr>
		</table>
		</td>
		--><td align="right" width="100px" class="listwhitetext">Initial&nbsp;Contact&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.HOM_initialContactDate}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_initialContactDate"/></s:text>
					 <td><s:textfield id="HOM_initialContactDate" cssClass="input-text" name="dspDetails.HOM_initialContactDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_initialContactDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_initialContactDate}">
				<td><s:textfield id="HOM_initialContactDate" cssClass="input-text" name="dspDetails.HOM_initialContactDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_initialContactDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
	</tr>
	
	<tr>
		<!--<td align="right" class="listwhitetext" width="83">Agent&nbsp;Phone</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HOM_agentPhone" readonly="false" size="58" maxlength="58" onchange=""  /></td>

		--><td align="right" width="100px" class="listwhitetext">House&nbsp;Hunting&nbsp;Trip&nbsp;Arrival</td>
			    <c:if test="${not empty dspDetails.HOM_houseHuntingTripArrival}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_houseHuntingTripArrival"/></s:text>
					 <td><s:textfield id="HOM_houseHuntingTripArrival" cssClass="input-text" name="dspDetails.HOM_houseHuntingTripArrival" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_houseHuntingTripArrival-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_houseHuntingTripArrival}">
				<td><s:textfield id="HOM_houseHuntingTripArrival" cssClass="input-text" name="dspDetails.HOM_houseHuntingTripArrival" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_houseHuntingTripArrival-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
	</tr>	
	
		<tr>
		<!--<td align="right" class="listwhitetext" width="83">Agent&nbsp;Email</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HOM_agentEmail" readonly="false" size="58" maxlength="58" onchange=""  /></td>

		--><td align="right" width="100px" class="listwhitetext">House&nbsp;Hunting&nbsp;Trip&nbsp;Departure</td>
			    <c:if test="${not empty dspDetails.HOM_houseHuntingTripDeparture}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_houseHuntingTripDeparture"/></s:text>
					 <td><s:textfield id="HOM_houseHuntingTripDeparture" cssClass="input-text" name="dspDetails.HOM_houseHuntingTripDeparture" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_houseHuntingTripDeparture-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_houseHuntingTripDeparture}">
				<td><s:textfield id="HOM_houseHuntingTripDeparture" cssClass="input-text" name="dspDetails.HOM_houseHuntingTripDeparture" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_houseHuntingTripDeparture-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
	</tr>
	</table>
	</configByCorp:fieldVisibility>
			<table width="708" border="0" class="detailTabLabel">
		<tr>
			<td align="right" width="162px" class="listwhitetext">View Status
			</td>
			<td width="65px"><s:select cssClass="list-menu"
				key="dspDetails.HOM_viewStatus"
				list="%{viewStatus}" headerKey="" headerValue=""
				cssStyle="width:105px" onchange="changeStatus();"/></td>
						<td align="right" width="59px" class="listwhitetext">Move&nbsp;Date</td>
			<c:if test="${not empty dspDetails.HOM_moveInDate}">
				<s:text id="customerFileSubmissionToTranfFormattedValue1"
					name="${FormDateValue}">
					<s:param name="value"
						value="dspDetails.HOM_moveInDate" />
				</s:text>
				<td width="67"><s:textfield id="HOM_moveInDate" cssClass="input-text"
					name="dspDetails.HOM_moveInDate"
					value="%{customerFileSubmissionToTranfFormattedValue1}"
					cssStyle="width:65px" maxlength="11" readonly="true"
					onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
				<td><img id="HOM_moveInDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty dspDetails.HOM_moveInDate}">
				<td width="67"><s:textfield id="HOM_moveInDate" cssClass="input-text"
					name="dspDetails.HOM_moveInDate"
					cssStyle="width:65px" maxlength="11" readonly="true"
					onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
				<td><img id="HOM_moveInDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">	
		<td align="right" class="listwhitetext" width="124">Estimated&nbsp;HSR&nbsp;Referral&nbsp;$</td>
		<td><s:textfield cssClass="input-text" key="dspDetails.HOM_estimatedHSRReferral" readonly="false" cssStyle="width:65px;" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  /></td>
</configByCorp:fieldVisibility>
	</tr>	
				
		</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.HOM_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td>
<td colspan="3" rowspan="2">
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<table>
<tr>
<td align="right" class="listwhitetext" width="166">Actual &nbsp;HSR&nbsp;Referral&nbsp;$</td>
		<td align="left"  colspan="2" class="listwhitetext" width=""><s:textfield cssClass="input-text" key="dspDetails.HOM_actualHSRReferral" readonly="false" cssStyle="width:65px;" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
		</td>
</tr>
<tr>
	<td align="right" width="100px" class="listwhitetext">Offer&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.HOM_offerDate}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_offerDate"/></s:text>
					 <td><s:textfield id="HOM_offerDate" cssClass="input-text" name="dspDetails.HOM_offerDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_offerDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_offerDate}">
				<td><s:textfield id="HOM_offerDate" cssClass="input-text" name="dspDetails.HOM_offerDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_offerDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
</tr>
<tr>
		<td align="right" width="131px" class="listwhitetext">Closing&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.HOM_closingDate}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_closingDate"/></s:text>
					 <td width="50"><s:textfield id="HOM_closingDate" cssClass="input-text" name="dspDetails.HOM_closingDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_closingDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_closingDate}">
				<td  width="50"><s:textfield id="HOM_closingDate" cssClass="input-text" name="dspDetails.HOM_closingDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_closingDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>	
				
</tr>
</table>
</configByCorp:fieldVisibility>	
</td>
</tr>		
		<tr><td></td></tr>
	</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if> 
		<!-- start aio -->
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'AIO')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('aio')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='AIO'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="aio">
  	   <table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"��style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.AIO_vendorCode" id="AIO_vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorNameRelo('AIO_','${dspDetails.AIO_vendorCodeEXSO}'),chkIsVendorRedSky('AIO_'),changeStatus();" onblur="showContactImage('AIO_')" /></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('AIO_','${dspDetails.AIO_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.AIO_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
		<td align="left" valign="top"><div id="hidImageAIO" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','AIO_');" src="<c:url value='/images/address2.png'/>" />
	</div></td>
	<td align="left" valign="top"><div id="hidNTImageAIO" style="display: block;">
			<img align="top" class="openpopup" onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.AIO_vendorCode'].value,
			'hidNTImageAIO');" src="${pageContext.request.contextPath}/images/navarrows_05.png" />
			</div></td>
	</td>
	<td align="left">
<div style="float:left;width:52px;">
<div id="hidAIO_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidAIO_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.AIO_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_serviceStartDate"/></s:text>
			 <td><s:textfield id="AIO_serviceStartDate" cssClass="input-text" name="dspDetails.AIO_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="AIO_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_serviceStartDate}">
		<td><s:textfield id="AIO_serviceStartDate" cssClass="input-text" name="dspDetails.AIO_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="AIO_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>

</td> 
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
	<c:choose>
		<c:when test="${countDsRelocationAreaInfoOrientationNotes == '0' || countDsRelocationAreaInfoOrientationNotes == '' || countDsRelocationAreaInfoOrientationNotes == null}">
		     <td  align="right" style="width:115px;!width:190px;"><img id="countDsRelocationAreaInfoOrientationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsOrientation&imageId=countDsRelocationAreaInfoOrientationNotesImage&fieldId=countDsRelocationAreaInfoOrientationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsOrientation&imageId=countDsRelocationAreaInfoOrientationNotesImage&fieldId=countDsRelocationAreaInfoOrientationNotes&decorator=popup&popup=true',800,600);" ></a></td>
		</c:when>
		<c:otherwise> 
			<td  align="right" style="width:115px;!width:100px;"><img id="countDsRelocationAreaInfoOrientationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsOrientation&imageId=countDsRelocationAreaInfoOrientationNotesImage&fieldId=countDsRelocationAreaInfoOrientationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsOrientation&imageId=countDsRelocationAreaInfoOrientationNotesImage&fieldId=countDsRelocationAreaInfoOrientationNotes&decorator=popup&popup=true',800,600);" ></a></td>
		</c:otherwise>
	</c:choose> 
</c:if>
           <c:if test="${fn1:indexOf(customerSurveyServiceDetails,'AIO')>-1}">
               <td  align="right" style="width:115px;!width:100px;"><img id="countDsRelocationAreaInfoOrientationFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DsRelocationAreaInfoOrientationFeebackId','AIO');"/></td>
              <div id="DsRelocationAreaInfoOrientationFeebackId" class="cfDiv"></div>
             </c:if> 
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">

<td align="right"><div class="link-up" id="linkup_AIO_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('AIO_','${dspDetails.AIO_vendorCode}','${dspDetails.AIO_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"� width="100%px"�style="margin:0px;padding:0px">
<tr>			
<td align="right" class="listwhitetext" width="69px" >Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.AIO_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" cssStyle="width:227px;margin-left: 3px" readonly="true" /></td>
</tr></table></td>

</c:if>
</c:if><configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isAIOmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'AIO')>-1}">
	 	<c:set var="isAIOmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="aio" id="aio" onclick="changeStatus();sendMailVendor('AIO');" value="${isAIOmailServiceType}" fieldValue="true"  disabled="${isAIOmailServiceType}"/></td>
</configByCorp:fieldVisibility>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'AIO')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" id="AIO_vendorContact" key="dspDetails.AIO_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />

 <div id="AIO_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('AIO');" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.AIO_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_serviceEndDate"/></s:text>
			 <td><s:textfield id="AIO_serviceEndDate" cssClass="input-text" name="dspDetails.AIO_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="AIO_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_serviceEndDate}">
		<td><s:textfield id="AIO_serviceEndDate" cssClass="input-text" name="dspDetails.AIO_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="AIO_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.AIO_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
	    
	    <c:if test="${not empty dspDetails.AIO_emailSent && fn1:indexOf(surveyEmailList,'AIO')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_emailSent"/></s:text>
			 <td><s:textfield id="AIO_emailSent" cssClass="input-textUpper" name="dspDetails.AIO_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendAIO" value="Resend Email" disabled=true onclick="resendEmail('AIO');"/></td>
	    </c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="AIO_vendorEmail" key="dspDetails.AIO_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
<td align="left" valign="top"><div id="hidEmailImageAIO" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.AIO_vendorEmail'].value)" id="emailAIO" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.AIO_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.AIO_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedAIO_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.AIO_displyOtherVendorCode}">
	 <c:set var="ischeckedAIO_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.AIO_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedAIO_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</c:if>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
   
<table cellspacing="0" cellpadding="3" border="0" class="detailTabLabel" style="margin-left:-20px">
<tbody>
<tr>
<td align="right"  width="200px"  class="listwhitetext">Flight Departure</td>
	    <c:if test="${not empty dspDetails.AIO_flightDeparture}">
			 <s:text id="customerFileFlightDeparture" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_flightDeparture"/></s:text>
			 <td width="" ><s:textfield id="AIO_flightDeparture" cssClass="input-text" name="dspDetails.AIO_flightDeparture" value="%{customerFileFlightDeparture}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
			 <img id="AIO_flightDeparture-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_flightDeparture}">
		<td width="" ><s:textfield id="AIO_flightDeparture" cssClass="input-text" name="dspDetails.AIO_flightDeparture" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
		<img id="AIO_flightDeparture-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr>
<tr>
<td align="right" class="listwhitetext">Flight Arrival</td>
	    <c:if test="${not empty dspDetails.AIO_flightArrival}">
			 <s:text id="customerFileFlightArrival" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_flightArrival"/></s:text>
			 <td width="" ><s:textfield id="AIO_flightArrival" cssClass="input-text" name="dspDetails.AIO_flightArrival" value="%{customerFileFlightArrival}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
			 <img id="AIO_flightArrival-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_flightArrival}">
		<td width="" ><s:textfield id="AIO_flightArrival" cssClass="input-text" name="dspDetails.AIO_flightArrival" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
		<img id="AIO_flightArrival-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr>
<tr>
<td align="right" class="listwhitetext" >Airline Flight</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.AIO_airlineFlight" readonly="false" size="31" maxlength="25" cssStyle="width:208px;" onchange="changeStatus();"/></td>
</tr>
<tr>
<td align="right" valign="top" class="listwhitetext" >Temporary accommodation<br>contact information</td>
<td align="left" class="listwhitetext" >
<s:textarea  rows="2" cssStyle="width:208px;" id="dspDetails.AIO_contactInformation" name="dspDetails.AIO_contactInformation"  cssClass="textarea" onchange="changeStatus();"/>	
</td>
</tr>
<tr>
<td align="right" class="listwhitetext">Days of service authorized # </td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.AIO_serviceAuthorized" readonly="false" size="10" cssStyle="width:65px;" maxlength="10" onchange="changeStatus();"/></td>
</tr>
<tr>

<td align="right"  class="listwhitetext">Initial date of service requested</td>
	    <c:if test="${not empty dspDetails.AIO_initialDateOfServiceRequested}">
			 <s:text id="customerFileInitialDateOfServiceRequested" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_initialDateOfServiceRequested"/></s:text>
			 <td width="" ><s:textfield id="AIO_initialDateOfServiceRequested" cssClass="input-text" name="dspDetails.AIO_initialDateOfServiceRequested" value="%{customerFileInitialDateOfServiceRequested}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
			 <img id="AIO_initialDateOfServiceRequested-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_initialDateOfServiceRequested}">
		<td width="" ><s:textfield id="AIO_initialDateOfServiceRequested" cssClass="input-text" name="dspDetails.AIO_initialDateOfServiceRequested" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
		<img id="AIO_initialDateOfServiceRequested-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr>
<tr>
<td align="right"  class="listwhitetext">Provider notification sent</td>
	    <c:if test="${not empty dspDetails.AIO_providerNotificationSent}">
			 <s:text id="customerFileProviderNotificationSent" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_providerNotificationSent"/></s:text>
			 <td width="" ><s:textfield id="AIO_providerNotificationSent" cssClass="input-text" name="dspDetails.AIO_providerNotificationSent" value="%{customerFileProviderNotificationSent}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
			 <img id="AIO_providerNotificationSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_providerNotificationSent}">
		<td width="" ><s:textfield id="AIO_providerNotificationSent" cssClass="input-text" name="dspDetails.AIO_providerNotificationSent" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
		<img id="AIO_providerNotificationSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
<tr>
<td align="right"  class="listwhitetext">Provider confirmation received </td>
	    <c:if test="${not empty dspDetails.AIO_providerConfirmationReceived}">
			 <s:text id="customerFileProviderConfirmationReceived" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_providerConfirmationReceived"/></s:text>
			 <td width="" ><s:textfield id="AIO_providerConfirmationReceived" cssClass="input-text" name="dspDetails.AIO_providerConfirmationReceived" value="%{customerFileProviderConfirmationReceived}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
			 <img id="AIO_providerConfirmationReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_providerConfirmationReceived}">
		<td width="" ><s:textfield id="AIO_providerConfirmationReceived" cssClass="input-text" name="dspDetails.AIO_providerConfirmationReceived" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
		<img id="AIO_providerConfirmationReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr>

<tr>
<td align="right" class="listwhitetext">Prearrival discussion between provider and family</td>
	    <c:if test="${not empty dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily}">
			 <s:text id="customerFilePrearrivalDiscussionOfProviderAndFamily" name="${FormDateValue}"><s:param name="value" value="dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily"/></s:text>
			 <td width="" ><s:textfield id="AIO_prearrivalDiscussionOfProviderAndFamily" cssClass="input-text" name="dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily" value="%{customerFilePrearrivalDiscussionOfProviderAndFamily}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
			 <img id="AIO_prearrivalDiscussionOfProviderAndFamily-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily}">
		<td width="" ><s:textfield id="AIO_prearrivalDiscussionOfProviderAndFamily" cssClass="input-text" name="dspDetails.AIO_prearrivalDiscussionOfProviderAndFamily" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
		<img id="AIO_prearrivalDiscussionOfProviderAndFamily-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr>
</configByCorp:fieldVisibility>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="34" name="dspDetails.AIO_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>

<tr>
<td align="right" class="listwhitetext" height="10"></td>

</tr>
</tbody>
</table>

	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
		<!-- end aio -->
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RNT')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('rnt')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='RNT'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="rnt">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365" colspan="2">
<table class="detailTabLabel" border="0" �cellspacing="0"�cellpadding="0"��style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.RNT_vendorCode" id="RNT_vendorCode" readonly="false" size="5" maxlength="10" 	onchange="checkVendorNameRelo('RNT_','${dspDetails.RNT_vendorCodeEXSO}'),chkIsVendorRedSky('RNT_'),changeStatus();chkIsVendorRedSky('RNT_');" onblur="showContactImage('RNT_')" /></td>
	<td align="left"width="10"><img align="left" id="imgId1" class="openpopup" width="17" height="20" onclick="winOpenDest('RNT_','${dspDetails.RNT_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.RNT_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" /></td>
			<td valign="top">
<div id="hidImageRNT" style="display: block;">
<img class="openpopup" width="17" align="left" height="20" src="<c:url value='/images/address2.png'/>" onclick="findAgent(this,'OA','RNT_');">
</div></td>
	<td align="left" valign="top"><div id="hidNTImageRNT" style="display: block;">
			<img align="top" class="openpopup" onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.RNT_vendorCode'].value,
			'hidNTImageRNT');" src="${pageContext.request.contextPath}/images/navarrows_05.png" />
			</div></td>
			<td align="left">
<div style="float:left;width:52px;">
<div id="hidRNT_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidRNT_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.RNT_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_serviceStartDate"/></s:text>
			 <td><s:textfield id="RNT_serviceStartDate" cssClass="input-text" name="dspDetails.RNT_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RNT_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_serviceStartDate}">
		<td><s:textfield id="RNT_serviceStartDate" cssClass="input-text" name="dspDetails.RNT_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RNT_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDsHomeRentalNotes == '0' || countDsHomeRentalNotes == '' || countDsHomeRentalNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSHomeRentalNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsHomeRental&imageId=countDSHomeRentalNotesImage&fieldId=countDsHomeRentalNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsHomeRental&imageId=countDSHomeRentalNotesImage&fieldId=countDsHomeRentalNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSHomeRentalNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsHomeRental&imageId=countDSHomeRentalNotesImage&fieldId=countDsHomeRentalNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsHomeRental&imageId=countDSHomeRentalNotesImage&fieldId=countDsHomeRentalNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
	          <c:if test="${fn1:indexOf(customerSurveyServiceDetails,'RNT')>-1}">
               <td  align="right" style="width:115px;!width:100px;"><img id="countDSHomeRentalFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSHomeRentalFeebackId','RNT');"/></td>
              <div id="DSHomeRentalFeebackId" class="cfDiv"></div>
             </c:if>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">
<td align="right"><div class="link-up" id="linkup_RNT_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('RNT_','${dspDetails.RNT_vendorCode}','${dspDetails.RNT_vendorCodeEXSO}')" >&nbsp;</div></td>	
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"� width="350px"�style="margin:0px; padding:0px">
<tr>		
<td align="right" class="listwhitetext"  width="69px">Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.RNT_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" readonly="true" /></td>
</tr></table></td>

</c:if>
</c:if><configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isRNTmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'RNT')>-1}">
	 	<c:set var="isRNTmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="rnt" id="rnt" onclick="changeStatus();sendMailVendor('RNT');" value="${isRNTmailServiceType}" fieldValue="true" disabled="${isRNTmailServiceType}"/></td>
</configByCorp:fieldVisibility>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'RNT')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2" ><s:textfield cssClass="input-text" id="RNT_vendorContact" key="dspDetails.RNT_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />

 <div id="RNT_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('RNT');" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.RNT_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_serviceEndDate"/></s:text>
			 <td><s:textfield id="RNT_serviceEndDate" cssClass="input-text" name="dspDetails.RNT_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RNT_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_serviceEndDate}">
		<td><s:textfield id="RNT_serviceEndDate" cssClass="input-text" name="dspDetails.RNT_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RNT_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.RNT_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    
	    <c:if test="${not empty dspDetails.RNT_emailSent && fn1:indexOf(surveyEmailList,'RNT')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_emailSent"/></s:text>
			 <td><s:textfield id="RNT_emailSent" cssClass="input-textUpper" name="dspDetails.RNT_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendRNT" value="Resend Email" disabled=true onclick="resendEmail('RNT');"/></td>
	    </c:if>

</tr>
	 			 
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="RNT_vendorEmail" key="dspDetails.RNT_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
<td align="left" valign="top"><div id="hidEmailImageRNT" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.RNT_vendorEmail'].value)" id="emailRNT" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.RNT_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.RNT_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedRNT_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.RNT_displyOtherVendorCode}">
	 <c:set var="ischeckedRNT_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.RNT_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedRNT_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</c:if>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tr><td align="right" class="listwhitetext">Monthly Rental Allowance</td>
    <td ><s:textfield  cssClass="input-text" name="dspDetails.RNT_monthlyRentalAllowance" cssStyle="width:65px" maxlength="10"  onchange="changeStatus();"/>   </td>
    <td align="right" class="listwhitetext">Currency</td>
		    	<td align="left" colspan="0"><s:select name="dspDetails.RNT_securityDepositCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:60px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="62"/></td>
    <td align="right" class="listwhitetext" style="width:215px;">Lease Start Date</td>
     <c:if test="${not empty dspDetails.RNT_leaseStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_leaseStartDate"/></s:text>
			 <td width="60"><s:textfield id="RNT_leaseStartDate" cssClass="input-text" name="dspDetails.RNT_leaseStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RNT_leaseStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_leaseStartDate}">
		<td width="60"><s:textfield id="RNT_leaseStartDate" cssClass="input-text" name="dspDetails.RNT_leaseStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RNT_leaseStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr><td  align="right" class="listwhitetext" >Security Deposit</td>
    <td ><s:textfield  cssClass="input-text" name="dspDetails.RNT_securityDeposit" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
    <td align="right" class="listwhitetext">Currency</td>
		    	<td align="left" colspan="0"><s:select name="dspDetails.RNT_monthlyRentalAllowanceCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:60px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="62"/></td>
    <td align="right" class="listwhitetext" width="142">Lease Expire Date</td>
   <c:if test="${not empty dspDetails.RNT_leaseExpireDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_leaseExpireDate"/></s:text>
			 <td><s:textfield id="RNT_leaseExpireDate" cssClass="input-text" name="dspDetails.RNT_leaseExpireDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_leaseExpireDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_leaseExpireDate}">
		<td><s:textfield id="RNT_leaseExpireDate" cssClass="input-text" name="dspDetails.RNT_leaseExpireDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_leaseExpireDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr><td colspan="4"></td>
<configByCorp:fieldVisibility componentId="component.tab.dspDetails.checkInDateShowVOER">
<td align="right" colspan=""  width="216" class="listwhitetext" >Check&nbsp;In&nbsp;Date</td>
<c:if test="${not empty dspDetails.RNT_checkInDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_checkInDate"/></s:text>
			 <td><s:textfield id="RNT_checkInDate" cssClass="input-text" name="dspDetails.RNT_checkInDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_checkInDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_checkInDate}">
		<td><s:textfield id="RNT_checkInDate" cssClass="input-text" name="dspDetails.RNT_checkInDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_checkInDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.tab.dspDetails.reminderDate">
<td align="right" colspan=""  width="216" class="listwhitetext" >Reminder&nbsp;Date&nbsp;3&nbsp;Mos&nbsp;Prior&nbsp;to&nbsp;Expiry&nbsp;of&nbsp;Lease</td>
		<c:if test="${not empty dspDetails.RNT_reminderDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_reminderDate"/></s:text>
			 <td><s:textfield id="RNT_reminderDate" cssClass="input-text" name="dspDetails.RNT_reminderDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_reminderDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_reminderDate}">
		<td><s:textfield id="RNT_reminderDate" cssClass="input-text" name="dspDetails.RNT_reminderDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_reminderDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
		 </tr>
<tr>
<td align="right" class="listwhitetext" >Negotiated Rent</td>
<td colspan="3" ><s:textarea cssClass="textarea"  rows="2" cols="40" name="dspDetails.RNT_negotiatedRent"  onchange="changeStatus();" /> </td>
<td colspan="4" style="vertical-align:top;">
<table class="detailTabLabel">
<tr>
<td width="84"></td>
<td align="right" width="123" class="listwhitetext">Lease&nbsp;Signee</td>
<td align="left" colspan="0"><s:select name="dspDetails.RNT_leaseSignee" list="%{leaseSignee}" cssClass="list-menu" cssStyle="width:93px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
</tr>
<tr>
<td width="84"></td>
<configByCorp:fieldVisibility componentId="component.Portal.visaExtensionNeeded.showVOER">
<td align="right" class="listwhitetext" >Lease&nbsp;Extension&nbsp;Needed</td>
<td><s:select cssClass="list-menu" name="dspDetails.RNT_leaseExtensionNeeded" list="%{yesno}" cssStyle="width:93px;" /></td>
 </configByCorp:fieldVisibility>
 </tr>
 <tr>
<td width="87"></td>
<configByCorp:fieldVisibility componentId="component.Portal.homeSearchType.showIMSB">
<td align="right" width="126" class="listwhitetext">Home&nbsp;Search&nbsp;Type</td>
<td align="left" colspan="0"><s:select name="dspDetails.RNT_homeSearchType" list="{'Accompanied','Un-Accompanied'}" cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
 </configByCorp:fieldVisibility>
 </tr>
 
 </table>
 </td>
</tr>
<tr>
<td align="right" class="listwhitetext" >Utilities Included</td>
<td colspan="5">
<table class="detailTabLabel">
<tr>
<td><s:select cssClass="list-menu" id="leaseReviewUtilities" name="dspDetails.RNT_utilitiesIncluded" list="%{utilities}" value="%{multiplutilities}" multiple="true" cssStyle="width:260px; height:100px" headerKey="" headerValue="" /></td>
   <td class="listwhitetext" align="left" colspan="3" style="font-size:10px; font-style: italic;">
   <b>* Use Control + mouse to select multiple Utilities Type</b>
 </td>
 	    <configByCorp:fieldVisibility componentId="component.tab.dspDetails.HomeFinding">
	    <c:if test="${serviceOrder.job == 'RLO'}">
	              <td align="right" class="listwhitetext" style="width:167px;">RSF Received</td> 
		 			<c:if test="${not empty dspDetails.RNT_rsfReceived}">
		
						<s:text id="RNT_rsfReceivedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_rsfReceived"/></s:text>
						<td style="width:94px;"><s:textfield cssClass="input-text" id="RNT_rsfReceived" name="dspDetails.RNT_rsfReceived" required="true" size="7" value="%{RNT_rsfReceivedValue}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
						<img id="RNT_rsfReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty dspDetails.RNT_rsfReceived}">
						<td style="width:94px;"><s:textfield cssClass="input-text" id="RNT_rsfReceived" name="dspDetails.RNT_rsfReceived" required="true" size="7" cssStyle="width:65px;"  onkeydown="return onlyDel(event,this)"/>
						<img id="RNT_rsfReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
		                  <td align="right" class="listwhitetext" style="width:100px;">Move in Inspection</td> 
						 	<c:if test="${not empty dspDetails.RNT_moveInspection}">
						<s:text id="RNT_moveInspectionValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_moveInspection"/></s:text>
						<td style="width:96px;"><s:textfield cssClass="input-text" id="RNT_moveInspection" name="dspDetails.RNT_moveInspection" required="true" size="7" value="%{RNT_moveInspectionValue}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
						<img id="RNT_moveInspection-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty dspDetails.RNT_moveInspection}">
						<td style="width:96px;"><s:textfield cssClass="input-text" id="RNT_moveInspection" name="dspDetails.RNT_moveInspection" required="true" size="7" cssStyle="width:65px;"  onkeydown="return onlyDel(event,this)"/>
						<img id="RNT_moveInspection-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						</c:if>
				</configByCorp:fieldVisibility> 
 </tr>
 </table>
 </td>
</tr>
<tr>
<td align="right" class="listwhitetext" >Deposit Paid By</td>
<td colspan="4" ><s:select cssClass="list-menu" name="dspDetails.RNT_depositPaidBy" list="%{depositby}" cssStyle="width:263px;" headerKey="" headerValue="" /></td>
</tr>

<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.RNT_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
		<!-- end rnt -->
	
	<!-- start set --> 
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'SET')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('set')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='SET'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="set">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"��style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="SET_vendorCode" key="dspDetails.SET_vendorCode" size="5" maxlength="10" onchange="checkVendorNameRelo('SET_','${dspDetails.SET_vendorCodeEXSO}'),chkIsVendorRedSky('SET_'),changeStatus();" onblur="showContactImage('SET_')" /></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('SET_','${dspDetails.SET_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.SET_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
			<td align="left" valign="top"><div id="hidImageSET" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','SET_');" src="<c:url value='/images/address2.png'/>" />
	</div></td>
	<td align="left" valign="top"><div id="hidNTImageSET" style="display: block;">
			<img align="top" class="openpopup"  onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.SET_vendorCode'].value,
			'hidNTImageSET');" src="${pageContext.request.contextPath}/images/navarrows_05.png"/>
			</div></td>
	</td>
	<td align="left">
<div style="float:left;width:52px;">
<div id="hidSET_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidSET_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
</c:if>
<c:if test="${usertype == 'ACCOUNT' and serviceOrder.job == 'RLO'}">
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"��style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.SET_vendorCode" size="5" maxlength="10" 
onchange="checkVendorNameRelo('SET_','${dspDetails.SET_vendorCodeEXSO}'),chkIsVendorRedSky('SET_'),changeStatus();"  /></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('SET_','${dspDetails.SET_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.SET_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
			<td align="left" valign="top"><div id="hidImageSET" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','SET_');" src="<c:url value='/images/address2.png'/>" />
	</div></td>
	<td align="left" valign="top"><div id="hidNTImageSET" style="display: block;">
			<img align="top" class="openpopup"  onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.SET_vendorCode'].value,
			'hidNTImageSET');" src="${pageContext.request.contextPath}/images/navarrows_05.png"/>
			</div></td>
	</td>
	<td align="left">
<div style="float:left;width:52px;">
<div id="hidSET_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidSET_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
</configByCorp:fieldVisibility>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.SET_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_serviceStartDate"/></s:text>
			 <td><s:textfield id="SET_serviceStartDate" cssClass="input-text" name="dspDetails.SET_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onselect="changeStatus();"/></td><td><img id="SET_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_serviceStartDate}">
		<td><s:textfield id="SET_serviceStartDate" cssClass="input-text" name="dspDetails.SET_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onselect="changeStatus();"/></td><td><img id="SET_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>		
		</c:if>

<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSSettlingInNotes == '0' || countDSSettlingInNotes == '' || countDSSettlingInNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSSettlingInNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsSettlingIn&imageId=countDSSettlingInNotesImage&fieldId=countDSSettlingInNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsSettlingIn&imageId=countDSSettlingInNotesImage&fieldId=countDSSettlingInNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSSettlingInNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsSettlingIn&imageId=countDSSettlingInNotesImage&fieldId=countDSSettlingInNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsSettlingIn&imageId=countDSSettlingInNotesImage&fieldId=countDSSettlingInNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
             <c:if test="${fn1:indexOf(customerSurveyServiceDetails,'SET')>-1}">
               <td  align="right" style="width:115px;!width:100px;"><img id="countDSSettlingInFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSSettlingInFeebackId','SET');"/></td>
              <div id="DSSettlingInFeebackId" class="cfDiv"></div>
             </c:if>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">
<td align="right"><div class="link-up" id="linkup_SET_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('SET_','${dspDetails.SET_vendorCode}','${dspDetails.SET_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"� width="350px"�style="margin:0px;padding:0px">
<tr>			
<td align="right" class="listwhitetext"  width="69px">Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.SET_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" readonly="true" /></td>
</tr></table></td>
</c:if>
</c:if>

<c:if test="${usertype == 'ACCOUNT' and serviceOrder.job == 'RLO'}">
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">
<td align="right"><div class="link-up" id="linkup_SET_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('SET_','${dspDetails.SET_vendorCode}','${dspDetails.SET_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"� width="350px"�style="margin:0px;padding:0px">
<tr>			
<td align="right" class="listwhitetext"  width="69px">Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.SET_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" readonly="true" /></td>
</tr></table></td>
</c:if>
</configByCorp:fieldVisibility>
</c:if>
<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isSETmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'SET')>-1}">
	 	<c:set var="isSETmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="set" id="set" onclick="changeStatus();sendMailVendor('SET');" value="${isSETmailServiceType}" fieldValue="true" disabled="${isSETmailServiceType}"/></td>
</configByCorp:fieldVisibility>

</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'SET')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" id="SET_vendorContact" key="dspDetails.SET_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />
 <div id="SET_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('SET');" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
</c:if>
<c:if test="${usertype == 'ACCOUNT' and serviceOrder.job == 'RLO'}">
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'SET')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" id="SET_vendorContact" key="dspDetails.SET_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" onblur="showContactImage('SET_')" /></td>
</configByCorp:fieldVisibility>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.SET_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_serviceEndDate"/></s:text>
			 <td><s:textfield id="SET_serviceEndDate" cssClass="input-text" name="dspDetails.SET_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_serviceEndDate}">
		<td><s:textfield id="SET_serviceEndDate" cssClass="input-text" name="dspDetails.SET_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.SET_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    
	    <c:if test="${not empty dspDetails.SET_emailSent && fn1:indexOf(surveyEmailList,'SET')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_emailSent"/></s:text>
			 <td><s:textfield id="SET_emailSent" cssClass="input-textUpper" name="dspDetails.SET_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendSET" value="Resend Email" disabled=true onclick="resendEmail('SET');"/></td>
	    </c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="SET_vendorEmail" key="dspDetails.SET_vendorEmail" readonly="false" size="57" maxlength="65"  onchange="changeStatus();showHideAddress();"/>
<td align="left" valign="top"><div id="hidEmailImageSET" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.SET_vendorEmail'].value)" id="emailSET" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.SET_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.SET_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedSET_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.SET_displyOtherVendorCode}">
	 <c:set var="ischeckedSET_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.SET_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedSET_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</c:if>
<c:if test="${usertype == 'ACCOUNT' and serviceOrder.job == 'RLO'}">
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" key="dspDetails.SET_vendorEmail" readonly="false" size="57" maxlength="65"  onchange="changeStatus();showHideAddress();"/>
<td align="left" valign="top"><div id="hidEmailImageSET" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.SET_vendorEmail'].value)" id="emailSET" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.SET_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedSET_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.SET_displyOtherVendorCode}">
	 <c:set var="ischeckedSET_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.SET_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedSET_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</configByCorp:fieldVisibility>
</c:if>
<tr> <td></td></tr>
</tbody>
</table> 
<table width="100%" cellpadding="0" cellspacing="0" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
<tr><td align="left" class="subcontenttabChild" style="font-weight:bold;padding-bottom:0px;">Services Assisted</td></tr>
</table >
<table class="detailTabLabel" border="0">
<tr><td></td></tr>
<tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right"><c:set var="isSET_pPermit" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_pPermit')>-1}">
<c:set var="isSET_pPermit" value="true" />
</c:if>
<s:checkbox name="SET_pPermit" id="SET_pPermit" onclick="changeStatus();" value="${isSET_pPermit}" fieldValue="true" />
</td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >Parking Permit</td>

	    <c:if test="${not empty dspDetails.SET_parkingPermit}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_parkingPermit"/></s:text>
			 <td width="70"><s:textfield id="SET_parkingPermit" cssClass="input-text" name="dspDetails.SET_parkingPermit" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_parkingPermit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_parkingPermit}">
		<td width="70"><s:textfield id="SET_parkingPermit" cssClass="input-text" name="dspDetails.SET_parkingPermit" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_parkingPermit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right"><c:set var="isSET_gID" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_gID')>-1}">
<c:set var="isSET_gID" value="true" />
</c:if>
<s:checkbox name="SET_gID" id="SET_gID" onclick="changeStatus();" value="${isSET_gID}" fieldValue="true" /></td>
		<td align="right" class="listwhitetext" >Government&nbsp;ID</td>
	    <c:if test="${not empty dspDetails.SET_governmentID}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_governmentID"/></s:text>
			 <td width="70"><s:textfield id="SET_governmentID" cssClass="input-text" name="dspDetails.SET_governmentID" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_parkingPermit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_governmentID}">
		<td width="70"><s:textfield id="SET_governmentID" cssClass="input-text" name="dspDetails.SET_governmentID" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_governmentID-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<td  width="40" align="right"><c:set var="isSET_initialRequested" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_initialRequested')>-1}">
<c:set var="isSET_initialRequested" value="true" />
</c:if>
<s:checkbox name="SET_initialRequested" id="SET_initialRequested" onclick="changeStatus();" value="${isSET_initialRequested}" fieldValue="true" />
</td>
		<td align="right" class="listwhitetext" >Initial&nbsp;date&nbsp;of&nbsp;service&nbsp;requested</td>
	    <c:if test="${not empty dspDetails.SET_initialDateofServiceRequested}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_initialDateofServiceRequested"/></s:text>
			 <td width="70"><s:textfield id="SET_initialDateofServiceRequested" cssClass="input-text" name="dspDetails.SET_initialDateofServiceRequested" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_initialDateofServiceRequested-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_initialDateofServiceRequested}">
		<td width="70"><s:textfield id="SET_initialDateofServiceRequested" cssClass="input-text" name="dspDetails.SET_initialDateofServiceRequested" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_initialDateofServiceRequested-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</configByCorp:fieldVisibility>
		
</tr>
<tr><configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right">
<c:set var="isSET_bAccount" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_bAccount')>-1}">
<c:set var="isSET_bAccount" value="true" />
</c:if>
<s:checkbox name="SET_bAccount" id="SET_bAccount" onclick="changeStatus();" value="${isSET_bAccount}" fieldValue="true" />
</td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >Bank Account Opening</td>
	    <c:if test="${not empty dspDetails.SET_bankAccount}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_bankAccount"/></s:text>
			 <td><s:textfield id="SET_bankAccount" cssClass="input-text" name="dspDetails.SET_bankAccount" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_bankAccount-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_bankAccount}">
		<td><s:textfield id="SET_bankAccount" cssClass="input-text" name="dspDetails.SET_bankAccount" cssStyle="width:65px" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="SET_bankAccount-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right">
<c:set var="isSET_hCare" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_hCare')>-1}">
<c:set var="isSET_hCare" value="true" />
</c:if>
<s:checkbox name="SET_hCare" id="SET_hCare" onclick="changeStatus();" value="${isSET_hCare}" fieldValue="true" />
</td>
		<td align="right" class="listwhitetext" >Heath&nbsp;Care</td>
	    <c:if test="${not empty dspDetails.SET_heathCare}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_heathCare"/></s:text>
			 <td width="70"><s:textfield id="SET_heathCare" cssClass="input-text" name="dspDetails.SET_heathCare" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_heathCare-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_heathCare}">
		<td width="70"><s:textfield id="SET_heathCare" cssClass="input-text" name="dspDetails.SET_heathCare" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_heathCare-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
			</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
		<td align="right" class="listwhitetext" >Provider&nbsp;notification&nbsp;sent</td>
	    <c:if test="${not empty dspDetails.SET_providerNotificationSent}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_providerNotificationSent"/></s:text>
			 <td width="70"><s:textfield id="SET_providerNotificationSent" cssClass="input-text" name="dspDetails.SET_providerNotificationSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_providerNotificationSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_providerNotificationSent}">
		<td width="70"><s:textfield id="SET_providerNotificationSent" cssClass="input-text" name="dspDetails.SET_providerNotificationSent" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_providerNotificationSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
</tr>
<tr><configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right"><c:set var="isSET_cHall" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_cHall')>-1}">
<c:set var="isSET_cHall" value="true" />
</c:if>
<s:checkbox name="SET_cHall" id="SET_cHall" onclick="changeStatus();" value="${isSET_cHall}" fieldValue="true" />
</td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >City Hall Registration</td>

	    <c:if test="${not empty dspDetails.SET_cityHall}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_cityHall"/></s:text>
			 <td><s:textfield id="SET_cityHall" cssClass="input-text" name="dspDetails.SET_cityHall" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_cityHall-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_cityHall}">
		<td><s:textfield id="SET_cityHall" cssClass="input-text" name="dspDetails.SET_cityHall" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="SET_cityHall-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right">	
<c:set var="isSET_utilitie" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_utilitie')>-1}">
<c:set var="isSET_utilitie" value="true" />
</c:if>
<s:checkbox name="SET_utilitie" id="SET_utilitie" onclick="changeStatus();" value="${isSET_utilitie}" fieldValue="true" />
</td>
		<td align="right" class="listwhitetext" >Utilities</td>
	    <c:if test="${not empty dspDetails.SET_utilities}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_utilities"/></s:text>
			 <td width="70"><s:textfield id="SET_utilities" cssClass="input-text" name="dspDetails.SET_utilities" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_utilities-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_utilities}">
		<td width="70"><s:textfield id="SET_utilities" cssClass="input-text" name="dspDetails.SET_utilities" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_utilities-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
		<td align="right" class="listwhitetext" >Provider&nbsp;confirmation&nbsp;received</td>
	    <c:if test="${not empty dspDetails.SET_providerConfirmationReceived}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_providerConfirmationReceived"/></s:text>
			 <td width="70"><s:textfield id="SET_providerConfirmationReceived" cssClass="input-text" name="dspDetails.SET_providerConfirmationReceived" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_providerConfirmationReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_providerConfirmationReceived}">
		<td width="70"><s:textfield id="SET_providerConfirmationReceived" cssClass="input-text" name="dspDetails.SET_providerConfirmationReceived" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_providerConfirmationReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
		</configByCorp:fieldVisibility>
</tr>
<tr><configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right"><c:set var="isSET_mConnection" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_mConnection')>-1}">
<c:set var="isSET_mConnection" value="true" />
</c:if>
<s:checkbox name="SET_mConnection" id="SET_mConnection" onclick="changeStatus();" value="${isSET_mConnection}" fieldValue="true" />
</td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >Media Connection</td>

	    <c:if test="${not empty dspDetails.SET_internetConn}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_internetConn"/></s:text>
			 <td><s:textfield id="SET_internetConn" cssClass="input-text" name="dspDetails.SET_internetConn" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_internetConn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_internetConn}">
		<td><s:textfield id="SET_internetConn" cssClass="input-text" name="dspDetails.SET_internetConn" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="SET_internetConn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right">
<c:set var="isSET_autoRegistration" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_autoRegistration')>-1}">
<c:set var="isSET_autoRegistration" value="true" />
</c:if>
<s:checkbox name="SET_autoRegistration" id="SET_autoRegistration" onclick="changeStatus();" value="${isSET_autoRegistration}" fieldValue="true" />
</td>
		<td align="right" class="listwhitetext" >Automobile&nbsp;Registration</td>
	    <c:if test="${not empty dspDetails.SET_automobileRegistration}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_automobileRegistration"/></s:text>
			 <td width="70"><s:textfield id="SET_automobileRegistration" cssClass="input-text" name="dspDetails.SET_automobileRegistration" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_automobileRegistration-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_automobileRegistration}">
		<td width="70"><s:textfield id="SET_automobileRegistration" cssClass="input-text" name="dspDetails.SET_automobileRegistration" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_automobileRegistration-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</configByCorp:fieldVisibility>
</tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">		
<td width="40" align="right">		
<c:set var="isSET_dLicense" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_dLicense')>-1}">
<c:set var="isSET_dLicense" value="true" />
</c:if>
<s:checkbox name="SET_dLicense" id="SET_dLicense" onclick="changeStatus();" value="${isSET_dLicense}" fieldValue="true" />
</td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >Driver's License</td>
	    <c:if test="${not empty dspDetails.SET_driverLicense}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_driverLicense"/></s:text>
			 <td><s:textfield id="SET_driverLicense" cssClass="input-text" name="dspDetails.SET_driverLicense" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_driverLicense-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_driverLicense}">
		<td><s:textfield id="SET_driverLicense" cssClass="input-text" name="dspDetails.SET_driverLicense" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="SET_driverLicense-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
		<td align="right" class="listwhitetext" >Day&nbsp;of&nbsp;service&nbsp;authorized#</td>
	    <c:if test="${not empty dspDetails.SET_dayofServiceAuthorized}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_dayofServiceAuthorized"/></s:text>
			 <td width="70"><s:textfield id="SET_dayofServiceAuthorized" cssClass="input-text" name="dspDetails.SET_dayofServiceAuthorized" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_dayofServiceAuthorized-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_dayofServiceAuthorized}">
		<td width="70"><s:textfield id="SET_dayofServiceAuthorized" cssClass="input-text" name="dspDetails.SET_dayofServiceAuthorized" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_dayofServiceAuthorized-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
</tr>
<tr>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">		
<td width="40" align="right"></td>
</configByCorp:fieldVisibility>
<td align="right" colspan="1" class="listwhitetext" >Comment</td><td colspan="5"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.SET_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
			<!-- end set -->	
	<!-- start lan -->
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'LAN')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('lan')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='LAN'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="lan">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"��style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.LAN_vendorCode" id="LAN_vendorCode" readonly="false" size="5" maxlength="10" 	onchange="checkVendorNameRelo('LAN_','${dspDetails.LAN_vendorCodeEXSO}');chkIsVendorRedSky('LAN_');changeStatus();" onblur="showContactImage('LAN_')"/></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('LAN_','${dspDetails.LAN_vendorCodeEXSO}');changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.LAN_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"   />
			<td align="left" valign="top"><div id="hidImageLAN" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','LAN_');" src="<c:url value='/images/address2.png'/>" />
	</div></td>
	<td align="left" valign="top"><div id="hidNTImageLAN" style="display: block;">
			<img align="top" class="openpopup"  onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.LAN_vendorCode'].value,
			'hidNTImageLAN');" src="${pageContext.request.contextPath}/images/navarrows_05.png" />
			</div></td>
	</td>
	<td align="left">
<div style="float:left;width:52px;">
<div id="hidLAN_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidLAN_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.LAN_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.LAN_serviceStartDate"/></s:text>
			 <td><s:textfield id="LAN_serviceStartDate" cssClass="input-text" name="dspDetails.LAN_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="LAN_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.LAN_serviceStartDate}">
		<td><s:textfield id="LAN_serviceStartDate" cssClass="input-text" name="dspDetails.LAN_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="LAN_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</td>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
 <c:if test="${not empty dspDetails.id}">
 <c:choose>
 <c:when test="${countDSLanguageNotes == '0' || countDSLanguageNotes == '' || countDSLanguageNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSLanguageNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsLanguage&imageId=countDSLanguageNotesImage&fieldId=countDSLanguageNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsLanguage&imageId=countDSLanguageNotesImage&fieldId=countDSLanguageNotes&decorator=popup&popup=true',800,600);" ></a></td> 
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSLanguageNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsLanguage&imageId=countDSLanguageNotesImage&fieldId=countDSLanguageNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsLanguage&imageId=countDSLanguageNotesImage&fieldId=countDSLanguageNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if> 
             <c:if test="${fn1:indexOf(customerSurveyServiceDetails,'LAN')>-1}">
               <td  align="right" style="width:115px;!width:100px;"><img id="countDSLanguageFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSLanguageFeebackId','LAN');"/></td>
              <div id="DSLanguageFeebackId" class="cfDiv"></div>
             </c:if>
</tr>
<tr>
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">

<td align="right"><div class="link-up" id="linkup_LAN_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('LAN_','${dspDetails.LAN_vendorCode}','${dspDetails.LAN_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"� width="350px"�style="margin:0px;padding:0px">
<tr>			
<td align="right" class="listwhitetext" width="69px" >Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.LAN_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" readonly="true" /></td>
</tr></table></td>

</c:if><configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isLANmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'LAN')>-1}">
	 	<c:set var="isLANmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="lan" id="lan" onclick="changeStatus();sendMailVendor('LAN');" value="${isLANmailServiceType}" fieldValue="true" disabled="${isLANmailServiceType}"/></td>
</configByCorp:fieldVisibility>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'LAN')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" id="LAN_vendorContact" key="dspDetails.LAN_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();"/>

 <div id="LAN_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('LAN');" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
<td align="right" class="listwhitetext">Service Finish</td>
			 <c:if test="${not empty dspDetails.LAN_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.LAN_serviceEndDate"/></s:text>
			 <td><s:textfield id="LAN_serviceEndDate" cssClass="input-text" name="dspDetails.LAN_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="LAN_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		     </c:if>
	         <c:if test="${empty dspDetails.LAN_serviceEndDate}">
		     <td><s:textfield id="LAN_serviceEndDate" cssClass="input-text" name="dspDetails.LAN_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="LAN_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.LAN_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		     </c:if>
	    
	    <c:if test="${not empty dspDetails.LAN_emailSent && fn1:indexOf(surveyEmailList,'LAN')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.LAN_emailSent"/></s:text>
			 <td><s:textfield id="LAN_emailSent" cssClass="input-textUpper" name="dspDetails.LAN_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendLAN" value="Resend Email" disabled=true onclick="resendEmail('LAN');"/></td>
	    </c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="LAN_vendorEmail" key="dspDetails.LAN_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();" />
<td align="left" valign="top"><div id="hidEmailImageLAN" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.LAN_vendorEmail'].value)" id="emailLAN" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.LAN_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.LAN_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedLAN_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.LAN_displyOtherVendorCode}">
	 <c:set var="ischeckedLAN_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.LAN_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedLAN_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata" ></td>
</tr>
</table>
<table width="" cellpadding="2" class="detailTabLabel">
<tr>
<td align="right" class="listwhitetext" width="" >Employee Hours/Days Authorized</td>
<td align="left" class="listwhitetext" width="65px" ><s:textfield cssClass="input-text" key="dspDetails.LAN_employeeDaysAuthorized" readonly="false" size="7" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" key="dspDetails.LAN_employeeTime" list="%{timeduration}" cssStyle="width:65px" onchange="changeStatus();"/></td>
<td width="100px" align="right" class="listwhitetext"> Notification Date</td>
	    <c:if test="${not empty dspDetails.LAN_notificationDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.LAN_notificationDate"/></s:text>
			 <td width="" ><s:textfield id="LAN_notificationDate" cssClass="input-text" name="dspDetails.LAN_notificationDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:67px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="LAN_notificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.LAN_notificationDate}">
		<td width="" ><s:textfield id="LAN_notificationDate" cssClass="input-text" name="dspDetails.LAN_notificationDate" cssStyle="width:67px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="LAN_notificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" >Spouse Hours/Days Authorized </td>
<td align="left" class="listwhitetext" width="70px" ><s:textfield cssClass="input-text" key="dspDetails.LAN_spouseDaysAuthorized" readonly="false" size="7" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" key="dspDetails.LAN_spouseTime" list="%{timeduration}" cssStyle="width:65px" onchange="changeStatus();"/></td>
<td align="right" class="listwhitetext" width="228px" >Children Hours/Days Authorized</td>
<td align="left" class="listwhitetext" width="" ><s:textfield cssClass="input-text" key="dspDetails.LAN_childrenDaysAuthorized" readonly="false" size="9" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" key="dspDetails.LAN_childrenTime" list="%{timeduration}" cssStyle="width:65px" onchange="changeStatus();"/></td>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  cssStyle="width: 271px;" rows="4" cols="40" name="dspDetails.LAN_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr><td></td></tr>
</table>

	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
		<!-- end lan -->
	<!-- start mmg -->
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'MMG')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('mmg')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='MMG'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="mmg">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"��style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_vendorCode" id="MMG_vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorNameRelo('MMG_','${dspDetails.MMG_vendorCodeEXSO}'),chkIsVendorRedSky('MMG_');changeStatus();" onblur="showContactImage('MMG_')"  /></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('MMG_','${dspDetails.MMG_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield cssClass="input-text" key="dspDetails.MMG_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    		<td align="left" valign="top"><div id="hidImageMMG" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','MMG_');" src="<c:url value='/images/address2.png'/>" />
	</div></td>
	<td align="left" valign="top"><div id="hidNTImageMMG" style="display: block;">
			<img align="top" class="openpopup"  onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.MMG_vendorCode'].value,
			'hidNTImageMMG');" src="${pageContext.request.contextPath}/images/navarrows_05.png" />
			</div></td>
	</td>
	<td align="left">
<div style="float:left;width:52px;">
<div id="hidMMG_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidMMG_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
 <c:if test="${cmmDmmAgent || networkAgent}">
 <td align="right" width="" class="listwhitetext">Vendor&nbsp;Initiation</td>
	    <c:if test="${not empty dspDetails.MMG_vendorInitiation}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MMG_vendorInitiation"/></s:text>
			 <td><s:textfield id="MMG_vendorInitiation" cssClass="input-text" name="dspDetails.MMG_vendorInitiation" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="MMG_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.MMG_vendorInitiation}">
		<td><s:textfield id="MMG_vendorInitiation" cssClass="input-text" name="dspDetails.MMG_vendorInitiation" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="MMG_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</c:if>

<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:80px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSMoveMgmtNotes == '0' || countDSMoveMgmtNotes == '' || countDSMoveMgmtNotes == null}">
<td  align="right" style="width:80px;"><img id="countDSMoveMgmtNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsMoveMgmt&imageId=countDSMoveMgmtNotesImage&fieldId=countDSMoveMgmtNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsMoveMgmt&imageId=countDSMoveMgmtNotesImage&fieldId=countDSMoveMgmtNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:80px;"><img id="countDSMoveMgmtNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsMoveMgmt&imageId=countDSMoveMgmtNotesImage&fieldId=countDSMoveMgmtNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsMoveMgmt&imageId=countDSMoveMgmtNotesImage&fieldId=countDSMoveMgmtNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
              <c:if test="${fn1:indexOf(customerSurveyServiceDetails,'MMG')>-1}">
               <td  align="right" style="width:115px;!width:100px;"><img id="countDSMoveMgmtFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSMoveMgmtFeebackId','MMG');"/></td>
              <div id="DSMoveMgmtFeebackId" class="cfDiv"></div>
             </c:if>
</tr>
<tr>
<c:if test="${(isNetworkBookingAgent==true)|| networkAgent}">
<td align="right"><div class="link-up" id="linkup_MMG_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('MMG_','${dspDetails.MMG_vendorCode}','${dspDetails.MMG_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"� width="100%;"�style="margin:0px;padding:0px">
<tr>			
<td align="right" class="listwhitetext"  width="69px" >Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.MMG_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" cssStyle="width:292px;" size="42" readonly="true" /></td>
</tr></table></td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.MMG_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MMG_serviceStartDate"/></s:text>
			 <td><s:textfield id="MMG_serviceStartDate" cssClass="input-text" name="dspDetails.MMG_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="MMG_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.MMG_serviceStartDate}">
		<td><s:textfield id="MMG_serviceStartDate" cssClass="input-text" name="dspDetails.MMG_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="MMG_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>

<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'MMG')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" id="MMG_vendorContact" key="dspDetails.MMG_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />

 <div id="MMG_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('MMG');" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
	<td align="right" class="listwhitetext">Service Finish</td>
    <c:if test="${not empty dspDetails.MMG_serviceEndDate}">
		 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MMG_serviceEndDate"/></s:text>
		 <td><s:textfield id="MMG_serviceEndDate" cssClass="input-text" name="dspDetails.MMG_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="MMG_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
    <c:if test="${empty dspDetails.MMG_serviceEndDate}">
	<td><s:textfield id="MMG_serviceEndDate" cssClass="input-text" name="dspDetails.MMG_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="MMG_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.MMG_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
	</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300" ><s:textfield cssClass="input-text" id="MMG_vendorEmail" key="dspDetails.MMG_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
<td align="left" valign="top"><div id="hidEmailImageMMG" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.MMG_vendorEmail'].value)" id="emailMMG" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.MMG_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isMMGmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'MMG')>-1}">
	 	<c:set var="isMMGmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="mmg" id="mmg" onclick="changeStatus();sendMailVendor('MMG');" value="${isMMGmailServiceType}" fieldValue="true" disabled="${isMMGmailServiceType}"/></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.MMG_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedMMG_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.MMG_displyOtherVendorCode}">
	 <c:set var="ischeckedMMG_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.MMG_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedMMG_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td>
<td colspan="2"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.MMG_comment" onkeyup="textLimit(this,4999)" cssStyle="width:364px;" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td>

<td colspan="4">
<table style="margin:0 0 0 4px;">
<tr>
	    <c:if test="${not empty dspDetails.MMG_orginEmailSent && fn1:indexOf(surveyEmailList,'MMG')>-1}"><td align="right" class="listwhitetext">Orgin Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MMG_orginEmailSent"/></s:text>
			 <td><s:textfield id="MMG_orginEmailSent" cssClass="input-textUpper" name="dspDetails.MMG_orginEmailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
		</c:if>
		<c:if test="${(not empty dspDetails.MMG_orginEmailSent || not empty dspDetails.MMG_destinationEmailSent) && fn1:indexOf(surveyEmailList,'MMG')>-1 }">
		<td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendMMG" value="Resend Email" disabled=true onclick="resendEmail('MMG');"/></td>
		</c:if>
	</tr>	
		<tr>
	    <c:if test="${not empty dspDetails.MMG_destinationEmailSent && fn1:indexOf(surveyEmailList,'MMG')>-1}"><td align="right" class="listwhitetext">Destination Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MMG_destinationEmailSent"/></s:text>
			 <td><s:textfield id="MMG_destinationEmailSent" cssClass="input-textUpper" name="dspDetails.MMG_destinationEmailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
		</c:if>
		</tr>
</table>
</td>
</tr>
<tr><td></td></tr>
 

	<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
	<table width="100%" cellpadding="2" class="detailTabLabel">
		<tr>
			<td align="left" class="vertlinedata"></td>
		</tr>
	</table>
	<table width="640" border="0" class="detailTabLabel">
	<tr>
	 <td align="right" class="listwhitetext" width="83">Origin&nbsp;Agent&nbsp;Code<font color="red" size="2">*</font></td>
	 <td align="left" colspan="1" width="365">
		<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"��style="margin:0px;">
		<tr>	<td align="left" ><s:textfield cssClass="input-text" key="dspDetails.MMG_originAgentCode" readonly="false" size="5" maxlength="10" onchange="findBillingIdName('dspDetails.MMG_originAgentCode','dspDetails.MMG_originAgentName');changeStatus();"  /></td>
			<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="setBillToCode1('dspDetails.MMG_originAgentCode','dspDetails.MMG_originAgentName','dspDetails.MMG_originAgentEmail');document.forms['dspDetailsForm'].elements['dspDetails.MMG_originAgentCode'].focus();changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
			<td align="left" style="padding-left:4px">
			<s:textfield	cssClass="input-text" key="dspDetails.MMG_originAgentName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
			</td>
		</tr>
		</table>
		</td>
		<td align="right" class="listwhitetext" width="83">Destination&nbsp;Agent&nbsp;Code<font color="red" size="2">*</font></td>
		<td align="left" colspan="1" width="365">
		<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"��style="margin:0px;">
		<tr>	<td align="left" ><s:textfield cssClass="input-text" key="dspDetails.MMG_destinationAgentCode" readonly="false" size="5" maxlength="10" onchange="findBillingIdName('dspDetails.MMG_destinationAgentCode','dspDetails.MMG_destinationAgentName');changeStatus();"  /></td>
			<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="setBillToCode1('dspDetails.MMG_destinationAgentCode','dspDetails.MMG_destinationAgentName','dspDetails.MMG_destinationAgentEmail');document.forms['dspDetailsForm'].elements['dspDetails.MMG_destinationAgentCode'].focus();changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
			<td align="left" style="padding-left:4px">
			<s:textfield	cssClass="input-text" key="dspDetails.MMG_destinationAgentName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
			</td>
		</tr>
		</table>
		</td>
	</tr>
	
	<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Phone</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_originAgentPhone" readonly="false" size="58" maxlength="58" onchange=""  /></td>

		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Phone</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_destinationAgentPhone" readonly="false" size="58" maxlength="58" onchange=""  /></td>
	</tr>	
	
		<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Email</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_originAgentEmail" readonly="false" size="58" maxlength="58" onchange=""  /></td>

		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Email</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_destinationAgentEmail" readonly="false" size="58" maxlength="58" onchange=""  /></td>

	</tr>
	</table>
	</configByCorp:fieldVisibility>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
		<!-- end mmg -->
	<!-- start ong -->
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'ONG')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('ong')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='ONG'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="ong">
  <table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"��style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.ONG_vendorCode" id="ONG_vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorNameRelo('ONG_','${dspDetails.ONG_vendorCodeEXSO}'),chkIsVendorRedSky('ONG_'),changeStatus();"  onblur="showContactImage('ONG_')" /></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('ONG_','${dspDetails.ONG_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.ONG_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    		<td align="left" valign="top"><div id="hidImageONG" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','ONG_');" src="<c:url value='/images/address2.png'/>" />
	</div></td>
	<td align="left" valign="top"><div id="hidNTImageONG" style="display: block;">
			<img align="top" class="openpopup"  onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.ONG_vendorCode'].value,
			'hidNTImageONG');" src="${pageContext.request.contextPath}/images/navarrows_05.png" />
			</div></td>
	</td>
	<td align="left">
<div style="float:left;width:52px;">
<div id="hidONG_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidONG_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.ONG_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_serviceStartDate"/></s:text>
			 <td><s:textfield id="ONG_serviceStartDate" cssClass="input-text" name="dspDetails.ONG_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_serviceStartDate}">
		<td><s:textfield id="ONG_serviceStartDate" cssClass="input-text" name="dspDetails.ONG_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="ONG_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSOngoingSupportNotes == '0' || countDSOngoingSupportNotes == '' || countDSOngoingSupportNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSOngoingSupportNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsOngoingSupport&imageId=countDSOngoingSupportNotesImage&fieldId=countDSOngoingSupportNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsOngoingSupport&imageId=countDSOngoingSupportNotesImage&fieldId=countDSOngoingSupportNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSOngoingSupportNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsOngoingSupport&imageId=countDSOngoingSupportNotesImage&fieldId=countDSOngoingSupportNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsOngoingSupport&imageId=countDSOngoingSupportNotesImage&fieldId=countDSOngoingSupportNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
           <c:if test="${fn1:indexOf(customerSurveyServiceDetails,'ONG')>-1}">
               <td  align="right" style="width:115px;!width:100px;"><img id="countDSOngoingSupportFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSOngoingSupportFeebackId','ONG');"/></td>
              <div id="DSOngoingSupportFeebackId" class="cfDiv"></div>
             </c:if>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">

<td align="right"><div class="link-up" id="linkup_ONG_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('ONG_','${dspDetails.ONG_vendorCode}','${dspDetails.ONG_vendorCodeEXSO}')" >&nbsp;</div></td>	
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0" width="100%"�cellpadding="0"��style="margin:0px;padding:0px">
<tr>		
<td align="right" class="listwhitetext" width="69px" >Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.ONG_vendorCodeEXSO"  cssClass = "input-textUpper shiftExtRight" size="42" cssStyle="width:292px;" readonly="true" /></td>
</tr></table></td>

</c:if>
</c:if><configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isONGmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'ONG')>-1}">
	 	<c:set var="isONGmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="ong" id="ong" onclick="changeStatus();sendMailVendor('ONG');" value="${isONGmailServiceType}" fieldValue="true" disabled="${isONGmailServiceType}"/></td>
</configByCorp:fieldVisibility>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'ONG')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" id="ONG_vendorContact" key="dspDetails.ONG_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />

 <div id="ONG_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('ONG');" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.ONG_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_serviceEndDate"/></s:text>
			 <td><s:textfield id="ONG_serviceEndDate" cssClass="input-text" name="dspDetails.ONG_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_serviceEndDate}">
		<td><s:textfield id="ONG_serviceEndDate" cssClass="input-text" name="dspDetails.ONG_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.ONG_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
	    
	    <c:if test="${not empty dspDetails.ONG_emailSent && fn1:indexOf(surveyEmailList,'ONG')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_emailSent"/></s:text>
			 <td><s:textfield id="ONG_emailSent" cssClass="input-textUpper" name="dspDetails.ONG_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendONG" value="Resend Email" disabled=true onclick="resendEmail('ONG');"/></td>
	    </c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300" ><s:textfield cssClass="input-text" id="ONG_vendorEmail" key="dspDetails.ONG_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
<td align="left" valign="top"><div id="hidEmailImageONG" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.ONG_vendorEmail'].value)" id="emailONG" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.ONG_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.ONG_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedONG_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.ONG_displyOtherVendorCode}">
	 <c:set var="ischeckedONG_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.ONG_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedONG_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>

</tr>
</c:if>
<tr><td></td></tr>
</tbody>
</table>
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table>
<tr><td  align="right" class="listwhitetext">Services Assisted (1)</td><td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.ONG_dateType1" readonly="false" size="53" maxlength="150" onchange="changeStatus()"/></td>
<td align="right" class="listwhitetext" width="140">Date</td>
     <c:if test="${not empty dspDetails.ONG_date1}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_date1"/></s:text>
			 <td><s:textfield id="ONG_date1" cssClass="input-text" name="dspDetails.ONG_date1" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_date1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_date1}">
		<td><s:textfield id="ONG_date1" cssClass="input-text" name="dspDetails.ONG_date1" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_date1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr><td align="right" class="listwhitetext">(2)</td><td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.ONG_dateType2" readonly="false" size="53" maxlength="150" onchange="changeStatus()"/></td>
<td align="right" class="listwhitetext">Date</td>
     <c:if test="${not empty dspDetails.ONG_date2}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_date2"/></s:text>
			 <td><s:textfield id="ONG_date2" cssClass="input-text" name="dspDetails.ONG_date2" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_date2-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_date2}">
		<td><s:textfield id="ONG_date2" cssClass="input-text" name="dspDetails.ONG_date2" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_date2-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
</tr>
<tr><td align="right" class="listwhitetext">(3)</td><td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.ONG_dateType3" readonly="false" size="53" maxlength="150" onchange="changeStatus()"/></td>
<td align="right" class="listwhitetext">Date</td>
     <c:if test="${not empty dspDetails.ONG_date3}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_date3"/></s:text>
			 <td><s:textfield id="ONG_date3" cssClass="input-text" name="dspDetails.ONG_date3" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_date3-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_date3}">
		<td><s:textfield id="ONG_date3" cssClass="input-text" name="dspDetails.ONG_date3" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_date3-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr><td align="right" class="listwhitetext">Comment</td><td colspan="3">
<s:textarea cssClass="textarea"  rows="3" cols="49" cssStyle="width:340px;" name="dspDetails.ONG_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" />
</td></tr>
</table>
 	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
		<!-- end ong -->						
	<!-- start prv -->
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'PRV')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('prv')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='PRV'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="prv">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"��style="margin:0px; ">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="PRV_vendorCode" key="dspDetails.PRV_vendorCode" readonly="false" size="5" maxlength="10" 	onchange="checkVendorNameRelo('PRV_','${dspDetails.PRV_vendorCodeEXSO}');chkIsVendorRedSky('PRV_');changeStatus();" onblur="showContactImage('PRV_')" /></td>
	<td align="left"width="10"><img align="left" id="imgid1" class="openpopup" width="17" height="20" onclick="winOpenDest('PRV_','${dspDetails.PRV_vendorCodeEXSO}');changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.PRV_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
			<td align="left" valign="top"><div id="hidImagePRV" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','PRV_');" src="<c:url value='/images/address2.png'/>" />
	</div></td>
	<td align="left" valign="top"><div id="hidNTImagePRV" style="display: block;">
			<img align="top" class="openpopup"  onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.PRV_vendorCode'].value,
			'hidNTImagePRV');" src="${pageContext.request.contextPath}/images/navarrows_05.png" />
			</div></td>
	</td>
	<td align="left">
<div style="float:left;width:52px;">
<div id="hidPRV_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidPRV_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.PRV_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_serviceStartDate"/></s:text>
			 <td><s:textfield id="PRV_serviceStartDate" cssClass="input-text" name="dspDetails.PRV_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_serviceStartDate}">
		<td><s:textfield id="PRV_serviceStartDate" cssClass="input-text" name="dspDetails.PRV_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</td>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgid2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSPreviewTripNotes == '0' || countDSPreviewTripNotes == '' || countDSPreviewTripNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSPreviewTripNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Preview&imageId=countDSPreviewTripNotesImage&fieldId=countDSPreviewTripNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=Preview&imageId=countDSPreviewTripNotesImage&fieldId=countDSPreviewTripNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSPreviewTripNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Preview&imageId=countDSPreviewTripNotesImage&fieldId=countDSPreviewTripNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=Preview&imageId=countDSPreviewTripNotesImage&fieldId=countDSPreviewTripNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
             <c:if test="${fn1:indexOf(customerSurveyServiceDetails,'PRV')>-1}">
               <td  align="right" style="width:115px;!width:100px;"><img id="countDSPreviewTripFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSPreviewTripFeebackId','PRV');"/></td>
              <div id="DSPreviewTripFeebackId" class="cfDiv"></div>
             </c:if>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">

<td align="right"><div class="link-up" id="linkup_PRV_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('PRV_','${dspDetails.PRV_vendorCode}','${dspDetails.PRV_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"� width="100%;"�style="margin:0px;padding:0px">
<tr>			
<td align="right" class="listwhitetext" width="69px">Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.PRV_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" cssStyle="width:292px;" readonly="true" /></td>
</tr></table></td>

</c:if>
</c:if><configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isPRVmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'PRV')>-1}">
	 	<c:set var="isPRVmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="prv" id="prv" onclick="changeStatus();sendMailVendor('PRV');" value="${isPRVmailServiceType}" fieldValue="true" disabled="${isPRVmailServiceType}"/></td>
</configByCorp:fieldVisibility>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'PRV')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2" ><s:textfield cssClass="input-text" id="PRV_vendorContact" key="dspDetails.PRV_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />

 <div id="PRV_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('PRV');" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.PRV_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_serviceEndDate"/></s:text>
			 <td><s:textfield id="PRV_serviceEndDate" cssClass="input-text" name="dspDetails.PRV_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_serviceEndDate}">
		<td><s:textfield id="PRV_serviceEndDate" cssClass="input-text" name="dspDetails.PRV_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.PRV_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
	    
	    <c:if test="${not empty dspDetails.PRV_emailSent && fn1:indexOf(surveyEmailList,'PRV')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_emailSent"/></s:text>
			 <td><s:textfield id="PRV_emailSent" cssClass="input-textUpper" name="dspDetails.PRV_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendPRV" value="Resend Email" disabled=true onclick="resendEmail('PRV');"/></td>
	    </c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="PRV_vendorEmail" key="dspDetails.PRV_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();"/>
<td align="left" valign="top"><div id="hidEmailImagePRV" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.PRV_vendorEmail'].value)" id="emailPRV" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.PRV_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.PRV_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedPRV_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.PRV_displyOtherVendorCode}">
	 <c:set var="ischeckedPRV_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.PRV_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedPRV_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</c:if>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel" >
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table border="0" class="detailTabLabel" >
<tr>
<td align="right" class="listwhitetext">Travel Planner Notification Date </td>
	    <c:if test="${not empty dspDetails.PRV_travelPlannerNotification}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_travelPlannerNotification"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_travelPlannerNotification" cssClass="input-text" name="dspDetails.PRV_travelPlannerNotification" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_travelPlannerNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_travelPlannerNotification}">
		<td width="65px" ><s:textfield id="PRV_travelPlannerNotification" cssClass="input-text" name="dspDetails.PRV_travelPlannerNotification" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_travelPlannerNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<td align="right" class="listwhitetext" >Orientation Days Authorized</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.PRV_areaOrientationDays" readonly="false" size="9" maxlength="2"  onchange="changeStatus()"/></td>
</tr>
<tr>
<td align="right"  class="listwhitetext">Travel departure </td>
	    <c:if test="${not empty dspDetails.PRV_travelDeparture}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_travelDeparture"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_travelDeparture" cssClass="input-text" name="dspDetails.PRV_travelDeparture" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_travelDeparture-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_travelDeparture}">
		<td width="65px" ><s:textfield id="PRV_travelDeparture" cssClass="input-text" name="dspDetails.PRV_travelDeparture" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_travelDeparture-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>		
<td align="right"  class="listwhitetext">Arrival </td>
	    <c:if test="${not empty dspDetails.PRV_travelArrival}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_travelArrival"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_travelArrival" cssClass="input-text" name="dspDetails.PRV_travelArrival" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_travelArrival-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_travelArrival}">
		<td width="65px" ><s:textfield id="PRV_travelArrival" cssClass="input-text" name="dspDetails.PRV_travelArrival" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_travelArrival-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>		
</tr>
<tr>
<c:choose>
	<c:when test="${usertype == 'ACCOUNT' and serviceOrder.job == 'RLO'}">
		<configByCorp:fieldVisibility componentId="component.accPortal.status.hideVisibility">
			<td align="right"  class="listwhitetext">Flight # </td>
	        <td width="65px" ><s:textfield id="PRV_flightNumber" cssClass="input-text" name="dspDetails.PRV_flightNumber"  cssStyle="width:65px" maxlength="20"  onfocus="changeStatus();"/></td>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.accPortal.status.hiddenVisibility">
			<td colspan="2"></td>
		</configByCorp:fieldVisibility>
	</c:when>
	<c:otherwise>
		    <td align="right"  class="listwhitetext">Flight # </td>
	        <td width="65px" ><s:textfield id="PRV_flightNumber" cssClass="input-text" name="dspDetails.PRV_flightNumber"  cssStyle="width:65px" maxlength="20"  onfocus="changeStatus();"/></td>
	</c:otherwise>
</c:choose>
<td align="right" colspan="2"  width="170px"  class="listwhitetext">Flight Arrival Time</td>
<td width="65px" ><s:textfield  cssClass="input-text" name="dspDetails.PRV_flightArrivalTime"  cssStyle="width:65px" maxlength="11"  onfocus="changeStatus();" onchange="completeTimeString();changeStatus();"/></td>
			
	
</tr>
<tr>
<td align="right"   class="listwhitetext">Hotel Details </td>
<td colspan="5">
<s:textarea name="dspDetails.PRV_hotelDetails" cols="40" cssClass="textarea" rows="4" onchange="changeStatus()"/>
</td>
</tr>
<tr>
<td align="right"  class="listwhitetext">Provider Notification <br>And Services Authorized </td>
	    <c:if test="${not empty dspDetails.PRV_providerNotificationAndServicesAuthorized}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_providerNotificationAndServicesAuthorized"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_providerNotificationAndServicesAuthorized" cssClass="input-text" name="dspDetails.PRV_providerNotificationAndServicesAuthorized" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_providerNotificationAndServicesAuthorized-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_providerNotificationAndServicesAuthorized}">
		<td width="65px" ><s:textfield id="PRV_providerNotificationAndServicesAuthorized" cssClass="input-text" name="dspDetails.PRV_providerNotificationAndServicesAuthorized" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_providerNotificationAndServicesAuthorized-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
<td align="right"  class="listwhitetext"> Meet & Greet At Airport </td>
	    <c:if test="${not empty dspDetails.PRV_meetGgreetAtAirport}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_meetGgreetAtAirport"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_meetGgreetAtAirport" cssClass="input-text" name="dspDetails.PRV_meetGgreetAtAirport" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_meetGgreetAtAirport-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_meetGgreetAtAirport}">
		<td width="65px" ><s:textfield id="PRV_meetGgreetAtAirport" cssClass="input-text" name="dspDetails.PRV_meetGgreetAtAirport" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_meetGgreetAtAirport-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>		
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.PRV_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr> <td></td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
		<!-- end prv -->

		<!-- end aio -->
	<!-- start exp -->
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'EXP')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('exp')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='EXP'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="exp">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350" colspan="2">
<table class="detailTabLabel"  cellpadding="0"  cellspacing="0" border="0" style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.EXP_vendorCode" id="EXP_vendorCode" readonly="false" size="6" maxlength="10" 	onchange="checkVendorNameRelo('EXP_','${dspDetails.EXP_vendorCodeEXSO}'),chkIsVendorRedSky('EXP_'),changeStatus();" onblur="showContactImage('EXP_')" /></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('EXP_','${dspDetails.EXP_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.EXP_vendorName" readonly="true" cssStyle="width:19.3em" maxlength="200" onchange="changeStatus();"  />
			<td align="center" width="22" valign="top"><div id="hidImageEXP" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','EXP_');" src="<c:url value='/images/address2.png'/>" />
	</div></td>
	<td align="left" valign="top"><div id="hidNTImageEXP" style="display: block;">
			<img align="top" class="openpopup" onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.EXP_vendorCode'].value,
			'hidNTImageEXP');" src="${pageContext.request.contextPath}/images/navarrows_05.png" />
			</div></td>
	</td>
	<td align="left">
<div style="float:left;width:52px;">
<div id="hidEXP_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidEXP_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.EXP_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.EXP_serviceStartDate"/></s:text>
			 <td><s:textfield id="EXP_serviceStartDate" cssClass="input-text" name="dspDetails.EXP_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EXP_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.EXP_serviceStartDate}">
		<td><s:textfield id="EXP_serviceStartDate" cssClass="input-text" name="dspDetails.EXP_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EXP_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>


<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSRelocationExpenseManagementNotes == '0' || countDSRelocationExpenseManagementNotes == '' || countDSRelocationExpenseManagementNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSRelocationExpenseManagementNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsExpenseManagement&imageId=countDSRelocationExpenseManagementNotesImage&fieldId=countDSRelocationExpenseManagementNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsExpenseManagement&imageId=countDSRelocationExpenseManagementNotesImage&fieldId=countDSRelocationExpenseManagementNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSRelocationExpenseManagementNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsExpenseManagement&imageId=countDSRelocationExpenseManagementNotesImage&fieldId=countDSRelocationExpenseManagementNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsExpenseManagement&imageId=countDSRelocationExpenseManagementNotesImage&fieldId=countDSRelocationExpenseManagementNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
           <c:if test="${fn1:indexOf(customerSurveyServiceDetails,'EXP')>-1}">
               <td  align="right" style="width:115px;!width:100px;"><img id="countDSRelocationExpenseFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSRelocationExpenseFeebackId','EXP');"/></td>
              <div id="DSRelocationExpenseFeebackId" class="cfDiv"></div>
             </c:if> 
</tr>
<tr>
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">

<td align="right"><div class="link-up" id="linkup_EXP_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('EXP_','${dspDetails.EXP_vendorCode}','${dspDetails.EXP_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="300" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"�width="350px"� style="margin:0px;padding:0px">
<tr>			
<td align="right" class="listwhitetext" width="69px" >Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.EXP_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" readonly="true" /></td>
</tr></table></td>

</c:if><configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isEXPmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'EXP')>-1}">
	 	<c:set var="isEXPmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="exp" id="exp" onclick="changeStatus();sendMailVendor('EXP');" value="${isEXPmailServiceType}" fieldValue="true" disabled="${isEXPmailServiceType}"/></td>
</configByCorp:fieldVisibility>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'EXP')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2" ><s:textfield cssClass="input-text" id="EXP_vendorContact" key="dspDetails.EXP_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />

 <div id="EXP_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('EXP');" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.EXP_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.EXP_serviceEndDate"/></s:text>
			 <td><s:textfield id="EXP_serviceEndDate" cssClass="input-text" name="dspDetails.EXP_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EXP_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.EXP_serviceEndDate}">
		<td><s:textfield id="EXP_serviceEndDate" cssClass="input-text" name="dspDetails.EXP_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EXP_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.EXP_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    
	    <c:if test="${not empty dspDetails.EXP_emailSent && fn1:indexOf(surveyEmailList,'EXP')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.EXP_emailSent"/></s:text>
			 <td><s:textfield id="EXP_emailSent" cssClass="input-textUpper" name="dspDetails.EXP_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendEXP" value="Resend Email" disabled=true onclick="resendEmail('EXP');"/></td>
	    </c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="EXP_vendorEmail" key="dspDetails.EXP_vendorEmail" readonly="false" size="57" maxlength="65"  onchange="changeStatus();showHideAddress();"/>
<td align="left" valign="top"><div id="hidEmailImageEXP" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.EXP_vendorEmail'].value)" id="emailEXP" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.EXP_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.EXP_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedEXP_displyOtherVendorCode" value="false" />
<c:if test="${dspDetails.EXP_displyOtherVendorCode}">
	 <c:set var="ischeckedEXP_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.EXP_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedEXP_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>

<table border="0" style="margin-left:50px" class="detailTabLabel">
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
<tr>
<td align="right"  width="100px"  class="listwhitetext">Paid Status  </td>
	     <td width="275px" ><s:select cssClass="list-menu" key="dspDetails.EXP_paidStatus" list="%{paidStatus}" headerKey="" headerValue="" cssStyle="width:105px" onchange="changeStatus();"/></td>
       <td></td>
		
<td align="right" class="listwhitetext">Provider Notification Date</td>
	    <c:if test="${not empty dspDetails.EXP_providerNotification}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue1" name="${FormDateValue}"><s:param name="value" value="dspDetails.EXP_providerNotification"/></s:text>
			 <td><s:textfield id="EXP_providerNotification" cssClass="input-text" name="dspDetails.EXP_providerNotification" value="%{customerFileSubmissionToTranfFormattedValue1}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EXP_providerNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.EXP_providerNotification}">
		<td><s:textfield id="EXP_providerNotification" cssClass="input-text" name="dspDetails.EXP_providerNotification" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="EXP_providerNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
</configByCorp:fieldVisibility>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.EXP_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr><td></td></tr>
</table>

	</div>
	</td>
	</tr>
	<tr><td></td></tr>
	</table>
	</td>
	</tr></c:if>
		<!-- end exp -->	
		<!-- start rpt -->
		<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RPT')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('rpt')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='RPT'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="rpt">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="365" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"��style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="RPT_vendorCode" key="dspDetails.RPT_vendorCode" readonly="false" size="5" maxlength="10" 	onchange="checkVendorNameRelo('RPT_','${dspDetails.RPT_vendorCodeEXSO}'),chkIsVendorRedSky('RPT_'),changeStatus();" onblur="showContactImage('RPT_')"/></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('RPT_','${dspDetails.RPT_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.RPT_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
			<td align="left" valign="top"><div id="hidImageRPT" style="display: block;">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','RPT_');" src="<c:url value='/images/address2.png'/>" />
	</div></td>
	<td align="left" valign="top"><div id="hidNTImageRPT" style="display: block;">
			<img align="top" class="openpopup" onclick="getPartnerAlert(document.forms['dspDetailsForm'].elements['dspDetails.RPT_vendorCode'].value,
			'hidNTImageRPT');" src="${pageContext.request.contextPath}/images/navarrows_05.png" />
			</div></td>
	</td>
	<td align="left">
<div style="float:left;width:52px;">
<div id="hidRPT_RUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
<div id="hidRPT_AP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
</div>
</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.RPT_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_serviceStartDate"/></s:text>
			 <td><s:textfield id="RPT_serviceStartDate" cssClass="input-text" name="dspDetails.RPT_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_serviceStartDate}">
		<td><s:textfield id="RPT_serviceStartDate" cssClass="input-text" name="dspDetails.RPT_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</td>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
 <c:if test="${not empty dspDetails.id}">
 <c:choose>
 <c:when test="${countDSRepatriationNotes == '0' || countDSRepatriationNotes == '' || countDSRepatriationNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSRepatriationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsRepatriation&imageId=countDSRepatriationNotesImage&fieldId=countDSRepatriationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsRepatriation&imageId=countDSRepatriationNotesImage&fieldId=countDSRepatriationNotes&decorator=popup&popup=true',800,600);" ></a></td> 
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSRepatriationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsRepatriation&imageId=countDSRepatriationNotesImage&fieldId=countDSRepatriationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsRepatriation&imageId=countDSRepatriationNotesImage&fieldId=countDSRepatriationNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
 <c:if test="${fn1:indexOf(customerSurveyServiceDetails,'RPT')>-1}">
               <td  align="right" style="width:115px;!width:100px;"><img id="countDSRepatriationFeebackImage" src="${pageContext.request.contextPath}/images/customerFeeback.png"  onclick="customerFeebackDetails('DSRepatriationFeebackId','RPT');"/></td>
              <div id="DSRepatriationFeebackId" class="cfDiv"></div>
             </c:if>  
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<c:if test="${(isNetworkBookingAgent==true) || networkAgent}">

<td align="right"><div class="link-up" id="linkup_RPT_vendorCodeEXSO"  onclick="return  createExternalEntriesBookingAgent('RPT_','${dspDetails.RPT_vendorCode}','${dspDetails.RPT_vendorCodeEXSO}')" >&nbsp;</div></td>
<td align="left" width="365" colspan="2">
<table class="detailTabLabel" border="0"�cellspacing="0"�cellpadding="0"�width="350px"�style="margin:0px;padding:0px">
<tr>			
<td align="right" class="listwhitetext"  width="69px">Ext SO #&nbsp;</td>
 <td><s:textfield name="dspDetails.RPT_vendorCodeEXSO"  cssClass="input-textUpper shiftExtRight" size="42" readonly="true" /></td>
</tr></table></td>

</c:if>
</c:if><configByCorp:fieldVisibility componentId="component.tab.dspDetails.mailServiceType">
	 	<c:set var="isRPTmailServiceType" value="false" />
     <c:if test="${fn1:indexOf(dspDetails.mailServiceType,'RPT')>-1}">
	 	<c:set var="isRPTmailServiceType" value="true" />
	 </c:if>
    <td align="right" class="listwhitetext" width="">Sent&nbsp;to&nbsp;Vendor</td>
    <td class="listwhitetext" width=""><s:checkbox name="rpt" id="rpt" onclick="changeStatus();sendMailVendor('RPT');" value="${isRPTmailServiceType}" fieldValue="true"  disabled="${isRPTmailServiceType}"/></td>
</configByCorp:fieldVisibility>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact<c:if test="${fn1:indexOf(surveyEmailList,'RPT')>-1}"><font color="red" size="2">*</font></c:if></td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" id="RPT_vendorContact" key="dspDetails.RPT_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" />

 <div id="RPT_hidImagePlus" class="open-div"><img class="openpopup" width="17" height="20" onclick="getContactDetails('RPT');" src="<c:url value='/images/plus-small.png'/>" /></div>
</td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
			 <c:if test="${not empty dspDetails.RPT_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_serviceEndDate"/></s:text>
			 <td><s:textfield id="RPT_serviceEndDate" cssClass="input-text" name="dspDetails.RPT_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		     </c:if>
	         <c:if test="${empty dspDetails.RPT_serviceEndDate}">
		     <td><s:textfield id="RPT_serviceEndDate" cssClass="input-text" name="dspDetails.RPT_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.RPT_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		     </c:if>
		     
	    
	    <c:if test="${not empty dspDetails.RPT_emailSent && fn1:indexOf(surveyEmailList,'RPT')>-1}"><td align="right" class="listwhitetext">Email Sent</td>
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_emailSent"/></s:text>
			 <td><s:textfield id="RPT_emailSent" cssClass="input-textUpper" name="dspDetails.RPT_emailSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
			 <td><input class="cssbutton" style="width:90px; height:25px;" type="button" id="resendRPT" value="Resend Email" disabled=true onclick="resendEmail('RPT');"/></td>
	    </c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" width="300"><s:textfield cssClass="input-text" id="RPT_vendorEmail" key="dspDetails.RPT_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();showHideAddress();" />
<td align="left" valign="top"><div id="hidEmailImageRPT" style="display: block;">
<img align="top" class="openpopup" onclick="sendEmail(document.forms['dspDetailsForm'].elements['dspDetails.RPT_vendorEmail'].value)" id="emailRPT" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${dspDetails.RPT_vendorEmail}"/>
</div></td>
</td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.RPT_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedRPT_displyOtherVendorCode" value="false" />
<c:if test="${dspDetails.RPT_displyOtherVendorCode}">
	 <c:set var="ischeckedRPT_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.RPT_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedRPT_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
  </configByCorp:fieldVisibility>
</tr>
</c:if>
</tbody>
</table>
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>

<table border="0" style="margin-left:42px" class="detailTabLabel">
<tr><td align="right" class="listwhitetext">Damage Cost</td>
<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="dspDetails.RPT_damageCost" readonly="false" size="9" maxlength="10" onchange="changeStatus();" /></td>

<td align="right" class="listwhitetext">Security Deposit</td>
<td align="left" class="listwhitetext" colspan="2" cssStyle="width:65px;"><s:textfield cssClass="input-text" key="dspDetails.RPT_securityDeposit" readonly="false" size="9" maxlength="10" onchange="changeStatus();" /></td></tr>
<tr>
<td align="right"    class="listwhitetext">Landlord Paid Notification Date </td>
	    <c:if test="${not empty dspDetails.RPT_landlordPaidNotificationDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_landlordPaidNotificationDate"/></s:text>
			 <td width="65px" ><s:textfield id="RPT_landlordPaidNotificationDate" cssClass="input-text" name="dspDetails.RPT_landlordPaidNotificationDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_landlordPaidNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_landlordPaidNotificationDate}">
		<td width="65px" ><s:textfield id="RPT_landlordPaidNotificationDate" cssClass="input-text" name="dspDetails.RPT_landlordPaidNotificationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_landlordPaidNotificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
<td align="right" class="listwhitetext"  width="243px">Professional Cleaning Date</td>
			 <c:if test="${not empty dspDetails.RPT_professionalCleaningDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_professionalCleaningDate"/></s:text>
			 <td><s:textfield id="RPT_professionalCleaningDate" cssClass="input-text" name="dspDetails.RPT_professionalCleaningDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_professionalCleaningDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		     </c:if>
	         <c:if test="${empty dspDetails.RPT_professionalCleaningDate}">
		     <td><s:textfield id="RPT_professionalCleaningDate" cssClass="input-text" name="dspDetails.RPT_professionalCleaningDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_professionalCleaningDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		     </c:if>		
</tr>
<tr>
<td align="right"   class="listwhitetext">Negotiated Deposit Returned Date </td>
	    <c:if test="${not empty dspDetails.RPT_negotiatedDepositReturnedDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_negotiatedDepositReturnedDate"/></s:text>
			 <td width="65px" ><s:textfield id="RPT_negotiatedDepositReturnedDate" cssClass="input-text" name="dspDetails.RPT_negotiatedDepositReturnedDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_negotiatedDepositReturnedDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_negotiatedDepositReturnedDate}">
		<td width="65px" ><s:textfield id="RPT_negotiatedDepositReturnedDate" cssClass="input-text" name="dspDetails.RPT_negotiatedDepositReturnedDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_negotiatedDepositReturnedDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
	<td align="right"   class="listwhitetext">Utilities Shut Off  Date</td>
	    <c:if test="${not empty dspDetails.RPT_utilitiesShutOff}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_utilitiesShutOff"/></s:text>
			 <td width="65px" ><s:textfield id="RPT_utilitiesShutOff" cssClass="input-text" name="dspDetails.RPT_utilitiesShutOff" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_utilitiesShutOff-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_utilitiesShutOff}">
		<td width="65px" ><s:textfield id="RPT_utilitiesShutOff" cssClass="input-text" name="dspDetails.RPT_utilitiesShutOff" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_utilitiesShutOff-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>	
</tr>
<tr>
<td colspan="3"></td>
<td align="right"  class="listwhitetext">Move Out Date</td>
	    <c:if test="${not empty dspDetails.RPT_moveOutDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RPT_moveOutDate"/></s:text>
			 <td width="65px" ><s:textfield id="RPT_moveOutDate" cssClass="input-text" name="dspDetails.RPT_moveOutDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_moveOutDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RPT_moveOutDate}">
		<td width="65px" ><s:textfield id="RPT_moveOutDate" cssClass="input-text" name="dspDetails.RPT_moveOutDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RPT_moveOutDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>	
</tr>
</table>	

<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>

<table class="detailTabLabel" border="0" style="margin-left:55px;">
<tr>
<td align="right" class="listwhitetext" >Deposit Returned To</td>
<td rowspan="" ><s:select cssClass="list-menu" name="dspDetails.RPT_depositReturnedTo" list="%{depositby}" cssStyle="width:242px;" headerKey="" headerValue="" /></td>
</tr>
<tr>
<td align="right"  class="listwhitetext">Comment</td>
<td colspan="5">
<s:textarea name="dspDetails.RPT_comment" cols="39" cssClass="textarea" onkeyup="textLimit(this,199)" onblur="textLimit(this,199)"  rows="3" onchange="changeStatus()"/>
</td>
</tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>	
			<!-- end rpt -->
					<jsp:include flush="true" page="dspDetailsForm2.jsp"></jsp:include>								
		</c:if>
		</table>
		</td>
		</tr>
		</table>

<c:if test="${usertype=='PARTNER'}">
<jsp:include flush="true" page="dspDetailsFormSec.jsp"></jsp:include>
</c:if>
</div><div class="bottom-header"><span style="height:50px"></span></div></div></div></div>
	<table width="60%">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${dspDetails.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="dspDetails.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${dspDetails.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						<c:if test="${not empty dspDetails.id}">
								<s:hidden name="dspDetails.createdBy"/>
								<td ><s:label name="createdBy" value="%{dspDetails.createdBy}"/></td>
							</c:if>
							<c:if test="${empty dspDetails.id}">
								<s:hidden name="dspDetails.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${dspDetails.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="dspDetails.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${dspDetails.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty dspDetails.id}">
							<s:hidden name="dspDetails.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{dspDetails.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty dspDetails.id}">
							<s:hidden name="dspDetails.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
		<s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" method="save" key="button.save" theme="simple" onclick=""/>
		<input class="cssbutton" style="width:55px; height:25px;" type="button" value="Reset" onclick="this.form.reset();checkColumn1();checkColumn2();checkColumn3();openDiv();setTenState();getUtilityCheckBoxValue();showAssigneeContribution();checkFLBDetails(document.forms['dspDetailsForm'].elements['dspDetails.FLB_addOn']);"/>
	
	<c:set var="field" value="<%=request.getParameter("field") %>"/>
<s:hidden name="field" value="<%=request.getParameter("field") %>" />
<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
<c:set var="field1" value="<%=request.getParameter("field1") %>"/>		
</s:form>
	<script type="text/javascript">
var fieldName = document.forms['dspDetailsForm'].elements['field'].value; 
var fieldName1 = document.forms['dspDetailsForm'].elements['field1'].value;
if(fieldName!=''){
document.forms['dspDetailsForm'].elements[fieldName].className = 'rules-textUpper';
}
if(fieldName1!=''){
document.forms['dspDetailsForm'].elements[fieldName1].className = 'rules-textUpper';
}
</script>
<script type="text/javascript">
checkColumn1();
checkColumn2();
checkColumn3();
</script>
<script type="text/javascript">
function enableResendEmail(){
	var surveyEmailList='${resendEmailList}';
	
	if(surveyEmailList==null || surveyEmailList==undefined || surveyEmailList=='null'){
		surveyEmailList="";
	}
	var customerSurveyServiceDetails='${customerSurveyServiceDetails}';
	if(customerSurveyServiceDetails==null || customerSurveyServiceDetails==undefined || customerSurveyServiceDetails=='null'){
		customerSurveyServiceDetails="";
	}
	var availableList='${serviceOrder.serviceType}';
	if(availableList==null || availableList==undefined || availableList=='null'){
		availableList="";
	}
	if(availableList!='' && surveyEmailList!=''){
		var serviceType=surveyEmailList.split(",");
		for(i=0;i<serviceType.length;i++){
			if((availableList.indexOf(serviceType[i])>-1 || serviceType[i]=='NET') && (customerSurveyServiceDetails=='' || customerSurveyServiceDetails.indexOf(serviceType[i])<0)){
				if(serviceType[i]=='NET'){
					<c:choose>
					<c:when test="${not empty serviceOrder.customerRating || not empty serviceOrder.feedBack}">
					</c:when>
					<c:otherwise>
					try{
						document.getElementById('resend'+serviceType[i]).disabled = false;
					}catch(e){}
					</c:otherwise>
					</c:choose>
				}else{
					document.getElementById('resend'+serviceType[i]).disabled = false;
				}
			}
		}
	}
}
enableResendEmail();
</script>
<script type="text/javascript">
try{
	<c:if test="${isNetworkBookingAgent==true  || networkAgent}">
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'CAR')>-1}">
	    var CAR_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.CAR_vendorCodeEXSO'].value;
	    var isRemoveCAR_vendorCodeEXSO = CAR_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveCAR_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.CAR_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'RLS')>-1}">
	    var RLS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.RLS_vendorCodeEXSO'].value;
	    var isRemoveRLS_vendorCodeEXSO = RLS_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveRLS_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.RLS_vendorCodeEXSO'].value="";
    	}
	    </c:if>	    
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'COL')>-1}">
	    var COL_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.COL_vendorCodeEXSO'].value;
	    var isRemoveCOL_vendorCodeEXSO = COL_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveCOL_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.COL_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'TRG')>-1}">
	    var TRG_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TRG_vendorCodeEXSO'].value;
	    var isRemoveTRG_vendorCodeEXSO = TRG_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveTRG_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.TRG_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'HOM')>-1}">
	    var HOM_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.HOM_vendorCodeEXSO'].value;
	    var isRemoveHOM_vendorCodeEXSO = HOM_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveHOM_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.HOM_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'RNT')>-1}">
	    var RNT_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.RNT_vendorCodeEXSO'].value;
	    var isRemoveRNT_vendorCodeEXSO = RNT_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveRNT_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.RNT_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'LAN')>-1}">
	    var LAN_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.LAN_vendorCodeEXSO'].value;
	    var isRemoveLAN_vendorCodeEXSO = LAN_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveLAN_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.LAN_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'MMG')>-1}">
	    var MMG_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.MMG_vendorCodeEXSO'].value;
	    var isRemoveMMG_vendorCodeEXSO = MMG_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveMMG_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.MMG_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'ONG')>-1}">
	    var ONG_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.ONG_vendorCodeEXSO'].value;
	    var isRemoveONG_vendorCodeEXSO = ONG_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveONG_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.ONG_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'PRV')>-1}">
	    var PRV_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.PRV_vendorCodeEXSO'].value;
	    var isRemovePRV_vendorCodeEXSO = PRV_vendorCodeEXSO.indexOf('emove');
    	if(isRemovePRV_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.PRV_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'AIO')>-1}">
	    var AIO_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.AIO_vendorCodeEXSO'].value;
	    var isRemoveAIO_vendorCodeEXSO = AIO_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveAIO_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.AIO_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'EXP')>-1}">
	    var EXP_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.EXP_vendorCodeEXSO'].value;
	    var isRemoveEXP_vendorCodeEXSO = EXP_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveEXP_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.EXP_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'RPT')>-1}">
	    var RPT_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.RPT_vendorCodeEXSO'].value;
	    var isRemoveRPT_vendorCodeEXSO = RPT_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveRPT_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.RPT_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'SCH')>-1}">
	    var SCH_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.SCH_vendorCodeEXSO'].value;
	    var isRemoveSCH_vendorCodeEXSO = SCH_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveSCH_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.SCH_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'TAX')>-1}">
	    var TAX_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TAX_vendorCodeEXSO'].value;
	    var isRemoveTAX_vendorCodeEXSO = TAX_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveTAX_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.TAX_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'TAC')>-1}">
	    var TAC_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TAC_vendorCodeEXSO'].value;
	    var isRemoveTAC_vendorCodeEXSO = TAC_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveTAC_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.TAC_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'TEN')>-1}">
	    var TEN_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TEN_vendorCodeEXSO'].value;
	    var isRemoveTEN_vendorCodeEXSO = TEN_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveTEN_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.TEN_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'VIS')>-1}">
	    var VIS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.VIS_vendorCodeEXSO'].value;
	    var isRemoveVIS_vendorCodeEXSO = VIS_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveVIS_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.VIS_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'WOP')>-1}">
	    var WOP_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.WOP_vendorCodeEXSO'].value;
	    var isRemoveWOP_vendorCodeEXSO = WOP_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveWOP_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.WOP_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'REP')>-1}">
	    var REP_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.REP_vendorCodeEXSO'].value;
	    var isRemoveREP_vendorCodeEXSO = REP_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveREP_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.REP_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'SET')>-1}">
	    var SET_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.SET_vendorCodeEXSO'].value;
	    var isRemoveSET_vendorCodeEXSO = SET_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveSET_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.SET_vendorCodeEXSO'].value="";
    	}
	    </c:if> 
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'CAT')>-1}">
	    var CAT_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.CAT_vendorCodeEXSO'].value;
	    var isRemoveCAT_vendorCodeEXSO = CAT_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveCAT_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.CAT_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'CLS')>-1}">
	    var CLS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.CLS_vendorCodeEXSO'].value;
	    var isRemoveCLS_vendorCodeEXSO = CLS_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveCLS_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.CLS_vendorCodeEXSO'].value="";
    	}
	    </c:if>

	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'CHS')>-1}">
	    var CHS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.CHS_vendorCodeEXSO'].value;
	    var isRemoveCHS_vendorCodeEXSO = CHS_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveCHS_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.CHS_vendorCodeEXSO'].value="";
    	}
	    </c:if>

	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'DPS')>-1}">
	    var DPS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.DPS_vendorCodeEXSO'].value;
	    var isRemoveDPS_vendorCodeEXSO = DPS_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveDPS_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.DPS_vendorCodeEXSO'].value="";
    	}
	    </c:if>

	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'HSM')>-1}">
	    var HSM_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.HSM_vendorCodeEXSO'].value;
	    var isRemoveHSM_vendorCodeEXSO = HSM_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveHSM_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.HSM_vendorCodeEXSO'].value="";
    	}
	    </c:if>

	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'PDT')>-1}">
	    var PDT_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.PDT_vendorCodeEXSO'].value;
	    var isRemovePDT_vendorCodeEXSO = PDT_vendorCodeEXSO.indexOf('emove');
    	if(isRemovePDT_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.PDT_vendorCodeEXSO'].value="";
    	}
	    </c:if>

	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'RCP')>-1}">
	    var RCP_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.RCP_vendorCodeEXSO'].value;
	    var isRemoveRCP_vendorCodeEXSO = RCP_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveRCP_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.RCP_vendorCodeEXSO'].value="";
    	}
	    </c:if>

	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'SPA')>-1}">
	    var SPA_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.SPA_vendorCodeEXSO'].value;
	    var isRemoveSPA_vendorCodeEXSO = SPA_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveSPA_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.SPA_vendorCodeEXSO'].value="";
    	}
	    </c:if>

	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'TCS')>-1}">
	    var TCS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TCS_vendorCodeEXSO'].value;
	    var isRemoveTCS_vendorCodeEXSO = TCS_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveTCS_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.TCS_vendorCodeEXSO'].value="";
    	}
	    </c:if>

	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'MTS')>-1}">
	    var MTS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.MTS_vendorCodeEXSO'].value;
	    var isRemoveMTS_vendorCodeEXSO = MTS_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveMTS_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.MTS_vendorCodeEXSO'].value="";
    	}
	    </c:if>

	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'DSS')>-1}">
	    var DSS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.DSS_vendorCodeEXSO'].value;
	    var isRemoveDSS_vendorCodeEXSO = DSS_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveDSS_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.DSS_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'HOB')>-1}">
	    var HOB_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.HOB_vendorCodeEXSO'].value;
	    var isRemoveHOB_vendorCodeEXSO = HOB_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveHOB_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.HOB_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'FLB')>-1}">
	    var FLB_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.FLB_vendorCodeEXSO'].value;
	    var isRemoveFLB_vendorCodeEXSO = FLB_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveFLB_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.FLB_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    	    
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'FRL')>-1}">
	    var FRL_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.FRL_vendorCodeEXSO'].value;
	    var isRemoveFRL_vendorCodeEXSO = FRL_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveFRL_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.FRL_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'APU')>-1}">
	    var APU_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.APU_vendorCodeEXSO'].value;
	    var isRemoveAPU_vendorCodeEXSO = APU_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveAPU_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.APU_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'INS')>-1}">
	    var INS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.INS_vendorCodeEXSO'].value;
	    var isRemoveINS_vendorCodeEXSO = INS_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveINS_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.INS_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'INP')>-1}">
	    var INP_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.INP_vendorCodeEXSO'].value;
	    var isRemoveINP_vendorCodeEXSO = INP_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveINP_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.INP_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'EDA')>-1}">
	    var EDA_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.EDA_vendorCodeEXSO'].value;
	    var isRemoveEDA_vendorCodeEXSO = EDA_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveEDA_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.EDA_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    <c:if test="${fn1:indexOf(serviceOrder.serviceType,'TAS')>-1}">
	    var TAS_vendorCodeEXSO=document.forms['dspDetailsForm'].elements['dspDetails.TAS_vendorCodeEXSO'].value;
	    var isRemoveTAS_vendorCodeEXSO = TAS_vendorCodeEXSO.indexOf('emove');
    	if(isRemoveTAS_vendorCodeEXSO>0) {
    		document.forms['dspDetailsForm'].elements['dspDetails.TAS_vendorCodeEXSO'].value="";
    	}
	    </c:if>
	    	    
	    <c:if test="${networkAgent }"> 
		  var bookingAgentExSO = document.forms['dspDetailsForm'].elements['trackingStatusBookingAgentExSO'].value;
		  var isRemoveBookingAgentExSO = bookingAgentExSO.indexOf('emove');
		  if(isRemoveBookingAgentExSO>0){
	    		document.forms['dspDetailsForm'].elements['trackingStatusBookingAgentExSO'].value="";
	    	}
		</c:if> 
		 var networkAgentExSO=document.forms['dspDetailsForm'].elements['trackingStatusNetworkAgentExSO'].value;
		 var isRemoveNetworkAgentExSO = networkAgentExSO.indexOf('emove');
		  if(isRemoveNetworkAgentExSO>0){
	    		document.forms['dspDetailsForm'].elements['trackingStatusNetworkAgentExSO'].value="";
	    	} 
	  </c:if>
}catch(e){}	  
	  </script>
<script type="text/javascript">
<c:choose>
<c:when test="${usertype == 'ACCOUNT' and serviceOrder.job == 'RLO'}">
<configByCorp:fieldVisibility componentId="component.accPortal.status.hideVisibility">
	setOnSelectBasedMethods(["callCalendarBassedMethods(),calcDays(document.getElementById('RNT_leaseExpireDate'),'RNT_reminderDate'),calcDays(document.getElementById('TAC_leaseExpireDate'),'TAC_expiryReminderPriorToExpiry')"]);
	setCalendarFunctionality();
</configByCorp:fieldVisibility>
</c:when>
<c:otherwise>
	setOnSelectBasedMethods(["updateBillThroughDate(),callCalendarBassedMethods(),calcDays(document.getElementById('RNT_leaseExpireDate'),'RNT_reminderDate'),calcDays(document.getElementById('TAC_leaseExpireDate'),'TAC_expiryReminderPriorToExpiry')"]);
	setCalendarFunctionality();
</c:otherwise>
</c:choose>
function delayTime(){ 
}
function showHideAddress() {
	var partnerCode='';
	var prefix='';
	<c:forEach var="entry" items="${serviceRelos}"> 
	prefix="${entry.key}";
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,entry.key)>-1}">
	if(prefix=='SCH'){
		partnerCode=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'_schoolSelected'].value;
	}else{
		partnerCode=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'_vendorCode'].value; 
	}
	partnerCode=partnerCode.trim();
    if(partnerCode=='')  {
    document.getElementById("hidImage"+prefix).style.display="none";
    document.getElementById("hidNTImage"+prefix).style.display="none";
    }
    else if(partnerCode!='') {
     document.getElementById("hidImage"+prefix).style.display="block";
     document.getElementById("hidNTImage"+prefix).style.display="block";
    }
    </c:if>
	</c:forEach>
		prefix="NET";
		partnerCode=document.forms['dspDetailsForm'].elements['trackingStatusNetworkPartnerCode'].value;
		partnerCode=partnerCode.trim();
	    if(partnerCode=='')  { 
	        document.getElementById("hidImage"+prefix).style.display="none";
	        document.getElementById("hidNTImage"+prefix).style.display="none"; 
	    }else if(partnerCode!='') {
	         document.getElementById("hidImage"+prefix).style.display="block";
	         document.getElementById("hidNTImage"+prefix).style.display="block";    
	    }	
	    showHideEmail();
}
function showHideEmail() {
	var partnerEmail='';
	prefix='';
	<c:forEach var="entry" items="${serviceRelos}"> 
	prefix="${entry.key}";
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,entry.key)>-1}"> 
	if(prefix=='SCH'){
		partnerEmail=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'_website'].value;
	}else{
		partnerEmail=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'_vendorEmail'].value;
	}
	partnerEmail=partnerEmail.trim();
    if(partnerEmail=='')  { 
    document.getElementById("hidEmailImage"+prefix).style.display="none";
    }
    else if(partnerEmail!='') {
     document.getElementById("hidEmailImage"+prefix).style.display="block";
    }
    </c:if>
	</c:forEach>
	prefix="NET";
	var agentEmailCode1=document.forms['dspDetailsForm'].elements['trackingStatusNetworkEmail'].value;
	agentEmailCode1=agentEmailCode1.trim();
	if(agentEmailCode1=='')  { 
        document.getElementById("hidEmailImage1"+prefix).style.display="none";
    }else if(agentEmailCode1!='') {
         document.getElementById("hidEmailImage1"+prefix).style.display="block";    
    } }
function getContact(agentType,partnerCode){
	if(agentType.length > 0 && partnerCode.length >0){
		openWindow('userContacts.html?agentType='+agentType+'&partnerCode='+partnerCode+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=trackingStatus.originAgentVanlineCode&fld_description=trackingStatus.originAgent&fld_code=trackingStatus.originAgentCode',1000,550);
	} } 	
	function saveOnTabChange(tabType){
		var url = "saveDspDetails.html?tabType="+tabType;
		document.forms['dspDetailsForm'].action= url;	 
		document.forms['dspDetailsForm'].submit();
	}	
    try{
	    showEmailImage(); 
	    showHideAddress();
    }
	    catch(e){}
	try{
	    	creatingYearDropDown()
    }catch(e){}
    
    function onlyNumsAllowed(evt)
    {
      var keyCode = evt.which ? evt.which : evt.keyCode;
      return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
    }
    function isNumeric(targetElement)
    {   var i;
        var s = targetElement.value;
        for (i = 0; i < s.length; i++){   
            var c = s.charAt(i);
            if (((c < "0") || (c > "9"))) {
            alert("Enter valid Number");
            targetElement.value="";
            return false;
            }
        }
        return true;
    }
   function validateVendorContact(){
	   	var flag1="F";
	   	var contact="";
	   	var returnType=true;
		 <configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
		 flag1="T";
		 </configByCorp:fieldVisibility>
	   var surveyEmailList='${surveyEmailList}';
	   var serviceType='${serviceOrder.serviceType}';
	   var emailArr=surveyEmailList.split(",");
	   for(var k=0;k<emailArr.length;k++){
		   var code=emailArr[k];
		   if(reloMap[code] != undefined && reloMap[code]!=null && reloMap.hasOwnProperty(code) && code !='' && serviceType.indexOf(code)>-1){
			   	if(code=='SCH'){
			   		contact=document.getElementById(code+'_contactPerson').value;
			   	}else{
			   		contact=document.getElementById(code+'_vendorContact').value;
			   	}
			   if(contact.trim()=='' && flag1=='T'){
		    		alert('Vendor Contact is Required Field for '+reloMap[code]+'.');
		    		returnType=false;
		    		break;
			   }
		   }		   
	   }
	return returnType; 
}
    function validateAgentCode(){
    	var serviceType='${serviceOrder.serviceType}';
    	var originAgentCode="";
    	var destinationAgentCode=""; 
    	var paymentResponsibilityHSM="";
    	var paymentResponsibilityHOM="";
    	var surveyEmailList='${surveyEmailList}';
    	var TEN_city="";
    	var TEN_zipCode="";
    	var TEN_addressLine1="";
    	var TEN_country="";
    	var TEN_rentAmount="";
    	var TEN_leasedBy="";
    	var TEN_rentCurrency="";
    	
    	var tenancyManagementVal="0";
    	<configByCorp:fieldVisibility componentId="component.dspdetails.tenancyManagement">
    		tenancyManagementVal="1";
	 	</configByCorp:fieldVisibility>
	 	if((serviceType.indexOf("TEN")>-1) && tenancyManagementVal=="1"){
	    		TEN_city=document.getElementById('TEN_city').value;
	    	 	TEN_zipCode=document.getElementById('TEN_zipCode').value;
	    		TEN_addressLine1=document.getElementById('TEN_addressLine1').value;
	    		TEN_country=document.getElementById('TEN_country').value;
	    		TEN_rentAmount=document.getElementById('TEN_rentAmount').value;
	    		TEN_leasedBy=document.getElementById('TEN_leasedBy').value;
	    		TEN_rentCurrency=document.getElementById('TEN_rentCurrency').value;
	 	}
	 	
    	if(serviceType.indexOf("MMG")>-1){
    		try{	
    			originAgentCode=document.forms['dspDetailsForm'].elements['dspDetails.MMG_originAgentCode'].value;
    	 		destinationAgentCode=document.forms['dspDetailsForm'].elements['dspDetails.MMG_destinationAgentCode'].value;
    	 	}catch(e){}
    	}   
    	if(serviceType.indexOf("HSM")>-1){
    		try{
    			vendorContactHSM=document.forms['dspDetailsForm'].elements['dspDetails.HSM_vendorContact'].value;    		
    			paymentResponsibilityHSM=document.forms['dspDetailsForm'].elements['dspDetails.HSM_paymentResponsibility'].value;
    		}catch(e){}
    	}
    	if(serviceType.indexOf("HOM")>-1){
    		try{
    			vendorContactHOM=document.forms['dspDetailsForm'].elements['dspDetails.HOM_vendorContact'].value;    		 
    			paymentResponsibilityHOM=document.forms['dspDetailsForm'].elements['dspDetails.HOM_paymentResponsibility'].value;
    		}catch(e){}
    	} 	
    	var flag="F";
    	 <configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
    	 flag="T";
    	 </configByCorp:fieldVisibility>
    	if(originAgentCode=='' && flag=='T' && serviceType.indexOf("MMG")>-1){
    		alert('Origin Agent Code is Required Field for Move Mgmt Services.');
    		return false;
    	}else if(destinationAgentCode=='' && flag=='T' && serviceType.indexOf("MMG")>-1){
    		alert('Destination Agent Code is Required Field for Move Mgmt Services.');
    		return false;
    	}else if(paymentResponsibilityHOM=='' && flag=='T' && serviceType.indexOf("HOM")>-1){
    		alert('Payment of Referral is Required Field for Home Purchase Assistance Services.');
        	document.forms['dspDetailsForm'].elements['dspDetails.HOM_paymentResponsibility'].focus();
    		return false;
    	}else if(paymentResponsibilityHSM=='' && flag=='T' && serviceType.indexOf("HSM")>-1){
    		alert('Payment of Referral is Required Field for Home Sale Marketing Assistance Services.');
    		document.forms['dspDetailsForm'].elements['dspDetails.HSM_paymentResponsibility'].focus();
    		return false;
    	}else if(!validateVendorContact()){
    		return false;
    	}else if((TEN_city==null || TEN_city == '') && (serviceType.indexOf("TEN")>-1) && tenancyManagementVal=="1"){
    		alert(' City is required field.');
    		document.getElementById('TEN_city').focus();
    		return false;
    	}else if((TEN_zipCode == null ||  TEN_zipCode=='') && (serviceType.indexOf("TEN")>-1) && tenancyManagementVal=="1" ){
    		alert(' Zip Code is required field.');
    		document.getElementById('TEN_zipCode').focus();
    		return false;
    	}else if((TEN_addressLine1 == null ||  TEN_addressLine1=='') && (serviceType.indexOf("TEN")>-1) && tenancyManagementVal=="1" ){
    		alert(' Address Line1 is required field.');
    		document.getElementById('TEN_addressLine1').focus();
    		return false;
    	}else if((TEN_leasedBy == null ||  TEN_leasedBy=='') && (serviceType.indexOf("TEN")>-1) && tenancyManagementVal=="1" ){
    		alert(' Leased By is required field.');
    		document.getElementById('TEN_leasedBy').focus();
    		return false;
    	}else if((TEN_rentAmount == null ||  TEN_rentAmount=='') && (serviceType.indexOf("TEN")>-1) && tenancyManagementVal=="1" ){
    		alert(' Rent Amount is required field.');
    		document.getElementById('TEN_rentAmount').focus();
    		return false;
    	}else if((TEN_rentCurrency == null ||  TEN_rentCurrency=='') && (serviceType.indexOf("TEN")>-1) && tenancyManagementVal=="1" ){
    		alert(' Rent Currency is required field.');
    		document.getElementById('TEN_rentCurrency').focus();
    		return false;
    	}else if((TEN_country == null ||  TEN_country=='') && (serviceType.indexOf("TEN")>-1) && tenancyManagementVal=="1" ){
    		alert(' Country is required field.');
    		document.getElementById('TEN_country').focus();
    		return false;
    	}else{
      		 copyDate();
    		 enableElement();GetSelectedItem();checkOnCheckBox();
    		 return true;
    		
    	}
    }
    
    function chkIsVendorRedSky(prefix){
    	var vendorId='';
    	var redskyuser=document.getElementById('hid'+prefix+'RUC');
		var agentPortal=document.getElementById('hid'+prefix+'AP');
        if(prefix=='SCH_'){
    	   var vendorId=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'schoolSelected'].value;
       }else{
    	var vendorId=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'vendorCode'].value;
       }
    	var dlinkButton=document.getElementById('linkup_'+prefix+'vendorCodeEXSO');
    	if(vendorId==''){
    		if(redskyuser!=undefined && redskyuser!=null){
        	redskyuser.style.display="none";
    		}
    		if(agentPortal!=undefined && agentPortal!=null){
        	agentPortal.style.display="none";
    		}
        	if(dlinkButton!=undefined && dlinkButton!=null){
    		dlinkButton.style.display="none";
        	}
    	}else{
    		var url = "chkVendorRUC.html?ajax=1&decorator=simple&popup=true&partCode="+vendorId;
    		http23411.open("GET", url, true);
    		http23411.onreadystatechange = function(){chkPartnerDestinRUC(prefix)};
    		http23411.send(null); 
    	}
    	}

    	function chkPartnerDestinRUC(prefix){
    		var dlinkButton=document.getElementById('linkup_'+prefix+'vendorCodeEXSO');
    		var redskyuser=document.getElementById('hid'+prefix+'RUC');
    		var agentPortal=document.getElementById('hid'+prefix+'AP');
    		if (http23411.readyState == 4){
    			var results = http23411.responseText
    	        results = results.trim();
    			if(results==''){			
    				if(dlinkButton!=undefined && dlinkButton!=null){
    					dlinkButton.style.display="none";	
    				}
    				if(redskyuser!=undefined && redskyuser!=null){
    				redskyuser.style.display="none";
    				}
    				if(agentPortal!=undefined && agentPortal!=null){
    				agentPortal.style.display="none";
    				}
    			}else{			
    				if(results=='RUC'){				
    					if(dlinkButton!=undefined && dlinkButton!=null){	
    						dlinkButton.style.display="block";
    					}
    					if(redskyuser!=undefined && redskyuser!=null){
    					redskyuser.style.display="block";
    					}
    					if(agentPortal!=undefined && agentPortal!=null){
    					agentPortal.style.display="none";	
    					}
    				}else if(results=="AP"){				
    					if(dlinkButton!=undefined && dlinkButton!=null){
    						dlinkButton.style.display="none";				
    					}
    					if(agentPortal!=undefined && agentPortal!=null){
    					agentPortal.style.display="block";
    					}
    					if(redskyuser!=undefined && redskyuser!=null){
    					redskyuser.style.display="none";
    					}
    					var vendorId=document.forms['dspDetailsForm'].elements['dspDetails.'+prefix+'vendorCode'].value;
    					var url = "chkVendorAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+vendorId;
    					http23411.open("GET", url, true);
    					http23411.onreadystatechange = function(){chkAgentDestinPartnerRUC()};
    					http23411.send(null);
    			}
    				} 
    			} 
    		}

    	function chkAgentDestinPartnerRUC(){
    		if (http23411.readyState == 4){
    			var results = http23411.responseText
    	        results = results.trim();
    			if(results=='AG'){			
    			}
    		}
    	}
</script>
<script type="text/javascript">
try{
	checkFLBDetails(document.forms['dspDetailsForm'].elements['dspDetails.FLB_addOn']);
	}
	catch(e){}
	function checkFLBDetails(result){
		if(result!='' && result!=null && result!='undefined'){
			if(result.checked==true){
			document.getElementById('checkFLBId').style.display ='block';
			}
			if(result.checked!=true){
				document.forms['dspDetailsForm'].elements['dspDetails.FLB_departureDate1'].value='';
				document.forms['dspDetailsForm'].elements['dspDetails.FLB_arrivalDate1'].value='';
				document.forms['dspDetailsForm'].elements['dspDetails.FLB_additionalBookingReminderDate1'].value='';
				document.getElementById('checkFLBId').style.display ='none';
			}
		}
	}
	
	 <configByCorp:fieldVisibility componentId="component.dspdetails.tenancyManagement">
	try{
		getState(document.forms['dspDetailsForm'].elements['dspDetails.TEN_country'],'');
		//showAssigneeContribution();
		showUtilitiesInc();
		//showAssigneeContributionAmount();
		var rentAmount = eval(document.getElementById("TEN_rentAmount").value);
		var rentAllowance = eval(document.getElementById("TEN_rentAllowance").value);
		if(rentAmount > rentAllowance){
			document.getElementById("rentAmt").style.display ='block';
			document.getElementById("TEN_contributionAmount").value = '${dspDetails.TEN_contributionAmount}';
		}else{
			document.getElementById("rentAmt").style.display ='none';
		}
		if(rentAllowance > rentAmount){
			document.getElementById("assigneeContributionAmount").style.display ='block';
			document.getElementById("TEN_assigneeContributionAmount").value = '${dspDetails.TEN_assigneeContributionAmount}';
		}else{
			document.getElementById("assigneeContributionAmount").style.display ='none';
		}
	}catch(e){}
	
		function setTenState(){
		 var countryVal ='${dspDetails.TEN_country}';
		 var enbState = '${enbState}';
		 var index = (enbState.indexOf(countryVal)> -1);
		  if(index != ''){
		  		document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'].disabled = false;
		  		getStateOnReset(countryVal);
		  }else{
			  document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'].disabled = true;
		  }
		}
		
		
		function saveTenancyManagementCheckBox(target,name,newFieldName){
			var serviceOrderId='${serviceOrder.id}';
			var fieldName = name;
			var fieldValue=false;
			 if(target.checked){
				 fieldValue=true;  
		  	  }
			 if(target.checked==false){
				 fieldValue=false; 
			 }
			 var updateValue = false;
			 new Ajax.Request('/redsky/saveTenancyManagementUtilities.html?ajax=1&serviceOrderId='+serviceOrderId+'&fieldName='+fieldName+'&fieldValue='+fieldValue+'&updateUtilityServiceTable='+updateValue+'&decorator=simple&popup=true',
					  {
					    method:'get',
					    onSuccess: function(transport){
					    var response = transport.responseText || "no response text";
					    if(newFieldName!=undefined){
						   updateUtilitiesFieldValues(target,newFieldName,'');
					    }
					    },
					    onFailure: function(){ 
						    }
					  });
		}
		</configByCorp:fieldVisibility>
		function showUtilitiesInc(){
			
			var tenGasWater = document.getElementById("TEN_Gas_Water").checked;
			 if(tenGasWater==true || tenGasWater=='true'){
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Gas_Water'].checked = true;
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Gas_Water'].disabled = true; 
			 }
			 var tenGas = document.getElementById("TEN_Gas").checked;
			 if(tenGas==true || tenGas=='true'){
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Gas'].checked = true;
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Gas'].disabled = true; 
			 }
			 var tenElectric = document.getElementById("TEN_Electricity").checked;
			 if(tenElectric==true || tenElectric=='true'){
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Electricity'].checked = true;
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Electricity'].disabled = true; 
			 }
			 var tenTvInternet = document.getElementById("TEN_TV_Internet_Phone").checked;
			 if(tenTvInternet==true || tenTvInternet=='true'){
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_TV_Internet_Phone'].checked = true;
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_TV_Internet_Phone'].disabled = true; 
			 }
			 var tenMobilePhone = document.getElementById("TEN_mobilePhone").checked;
			 if(tenMobilePhone==true || tenMobilePhone=='true'){
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_mobilePhone'].checked = true;
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_mobilePhone'].disabled = true; 
			 }
			 var tenFurnitureRent = document.getElementById("TEN_furnitureRental").checked;
			 if(tenFurnitureRent==true || tenFurnitureRent=='true'){
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_furnitureRental'].checked = true;
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_furnitureRental'].disabled = true; 
			 }
			 var tenCleanService = document.getElementById("TEN_cleaningServices").checked;
			 if(tenCleanService==true || tenCleanService=='true'){
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_cleaningServices'].checked = true;
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_cleaningServices'].disabled = true; 
			 }
			 var tenParkingPermit = document.getElementById("TEN_parkingPermit").checked;
			 if(tenParkingPermit==true || tenParkingPermit=='true'){
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_parkingPermit'].checked = true;
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_parkingPermit'].disabled = true; 
			 }
			 var tenComTax = document.getElementById("TEN_communityTax").checked;
			 if(tenComTax==true || tenComTax=='true'){
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_communityTax'].checked = true;
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_communityTax'].disabled = true; 
			 }
			 var tenInsurance = document.getElementById("TEN_insurance").checked;
			 if(tenInsurance==true || tenInsurance=='true'){
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_insurance'].checked = true;
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_insurance'].disabled = true; 
			 }
			 var tenGardenMainten = document.getElementById("TEN_gardenMaintenance").checked;
			 if(tenGardenMainten==true || tenGardenMainten=='true'){
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_gardenMaintenance'].checked = true;
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_gardenMaintenance'].disabled = true; 
			 }
			 
			 var tenMiscellaneous = document.getElementById("TEN_Miscellaneous").checked;
			 if(tenMiscellaneous==true || tenMiscellaneous=='true'){
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Miscellaneous'].checked = true;
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Miscellaneous'].disabled = true; 
			 }
			 
			 var tenGasElectric = document.getElementById("TEN_Gas_Electric").checked;
			 if(tenGasElectric==true || tenGasElectric=='true'){
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Gas_Electric'].checked = true;
				 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Gas_Electric'].disabled = true; 
			 }
			 
			 var TEN_utilitiesIncluded = document.getElementById("TEN_utilitiesIncluded").value;
			 if(TEN_utilitiesIncluded == "Yes"){
				document.getElementById("utilitiesIncludedShow").style.display ='block';
					 var tenUtilityGasWater = document.getElementById("TEN_Utility_Gas_Water").checked;
					 if((tenGasWater==false || tenGasWater=='false') && (tenUtilityGasWater==true || tenUtilityGasWater=='true')){
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Gas_Water'].disabled = true; 
					 }else{
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Gas_Water'].disabled = false;
					 }
					 var tenUtilityGas = document.getElementById("TEN_Utility_Gas").checked;
					 if((tenUtilityGas==true || tenUtilityGas=='true') && (tenGas==false || tenGas=='false')){
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Gas'].disabled = true; 
					 }else{
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Gas'].disabled = false;
					 }
					 var tenUtilityElectric = document.getElementById("TEN_Utility_Electricity").checked;
					 if((tenUtilityElectric==true || tenUtilityElectric=='true') && (tenElectric==false || tenElectric=='false')){
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Electricity'].disabled = true; 
					 }else{
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Electricity'].disabled = false;
					 }
					 var tenUtilityTvInternet = document.getElementById("TEN_Utility_TV_Internet_Phone").checked;
					 if((tenUtilityTvInternet==true || tenUtilityTvInternet=='true') && (tenTvInternet==false || tenTvInternet=='false')){
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_TV_Internet_Phone'].disabled = true; 
					 }else{
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_TV_Internet_Phone'].disabled = false;
					 }
					 var tenUtilityMobilePhone = document.getElementById("TEN_Utility_mobilePhone").checked;
					 if((tenUtilityMobilePhone==true || tenUtilityMobilePhone=='true') && (tenMobilePhone==false || tenMobilePhone=='false')){
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_mobilePhone'].disabled = true; 
					 }else{
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_mobilePhone'].disabled = false;
					 }
					 var tenUtilityFurnitureRent = document.getElementById("TEN_Utility_furnitureRental").checked;
					 if((tenUtilityFurnitureRent==true || tenUtilityFurnitureRent=='true') && (tenFurnitureRent==false || tenFurnitureRent=='false')){
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_furnitureRental'].disabled = true; 
					 }else{
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_furnitureRental'].disabled = false; 
					 }
					 var tenUtilityCleanService = document.getElementById("TEN_Utility_cleaningServices").checked;
					 if((tenUtilityCleanService==true || tenUtilityCleanService=='true') && (tenCleanService==false || tenCleanService=='false')){
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_cleaningServices'].disabled = true; 
					 }else{
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_cleaningServices'].disabled = false;
					 }
					 var tenUtilityParkingPermit = document.getElementById("TEN_Utility_parkingPermit").checked;
					 if((tenUtilityParkingPermit==true || tenUtilityParkingPermit=='true') && (tenParkingPermit==false || tenParkingPermit=='false')){
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_parkingPermit'].disabled = true; 
					 }else{
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_parkingPermit'].disabled = false;
					 }
					 var tenUtilityComTax = document.getElementById("TEN_Utility_communityTax").checked;
					 if((tenUtilityComTax==true || tenUtilityComTax=='true') && (tenComTax==false || tenComTax=='false')){
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_communityTax'].disabled = true; 
					 }else{
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_communityTax'].disabled = false;
					 }
					 var tenUtilityInsurance = document.getElementById("TEN_Utility_insurance").checked;
					 if((tenUtilityInsurance==true || tenUtilityInsurance=='true') && (tenInsurance==false || tenInsurance=='false')){
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_insurance'].disabled = true; 
					 }else{
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_insurance'].disabled = false;
					 }
					 var tenUtilityGardenMainten = document.getElementById("TEN_Utility_gardenMaintenance").checked;
					 if((tenUtilityGardenMainten==true || tenUtilityGardenMainten=='true') && (tenGardenMainten==false || tenGardenMainten=='false')){
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_gardenMaintenance'].disabled = true; 
					 }else{
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_gardenMaintenance'].disabled = false;
					 }
					 
					 var tenUtilityMiscellaneous = document.getElementById("TEN_Utility_Miscellaneous").checked;
					 if((tenUtilityMiscellaneous==true || tenUtilityMiscellaneous=='true') && (tenMiscellaneous==false || tenMiscellaneous=='false')){
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Miscellaneous'].disabled = true; 
					 }else{
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Miscellaneous'].disabled = false;
					 }
					 
					 var tenUtilityGasElectric = document.getElementById("TEN_Utility_Gas_Electric").checked;
					 if((tenUtilityGasElectric==true || tenUtilityGasElectric=='true') && (tenGasElectric==false || tenGasElectric=='false')){
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Gas_Electric'].disabled = true; 
					 }else{
						 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Gas_Electric'].disabled = false;
					 }
			}else{
				 document.getElementById("utilitiesIncludedShow").style.display ='none';
			}
		}
		
		function updateUtilitiesFieldValues(target,newFieldName,type){
			var serviceOrderId='${serviceOrder.id}';
			var fieldName = newFieldName;
			var fieldValue=false;
			var currencyValue ="";
			 if(target.checked){
				 fieldValue=true;
				 currencyValue=document.getElementById('TEN_rentCurrency').value;
		  	  }
			 if(target.checked==false){
				 fieldValue=false; 
			 }
			 var updateValue=false;
			 if(type=='clicked'){
				 updateValue=true;
			 }
			 
			 new Ajax.Request('/redsky/saveTenancyManagementUtilities.html?ajax=1&serviceOrderId='+serviceOrderId+'&fieldName='+fieldName+'&fieldValue='+fieldValue+'&updateUtilityServiceTable='+updateValue+'&currencyValue='+currencyValue+'&decorator=simple&popup=true',
					  {
					    method:'get',
					    onSuccess: function(transport){
					    var response = transport.responseText || "no response text";
					    if(target.checked && type==''){
					    	document.getElementById(fieldName).checked = true;
					    	document.getElementById(fieldName).disabled = true;
					    }
					    if(target.checked==false){
					    	document.getElementById(fieldName).checked = false;
					    	document.getElementById(fieldName).disabled = false; 
						 }
					    if(type=='clicked'){
					    	var tenancyUtilityServiceDiv = document.getElementById("tenancyUtilityServiceAjaxDetail");
					    	tenancyUtilityServiceDiv.innerHTML = response;
					    	showUtilitiesInc();
					    	setOnSelectBasedMethods(["updateBillThroughDate()"]);
					    	setCalendarFunctionality();
					    }
					    },
					    onFailure: function(){ 
						    }
					  });
		}
		function findExchangeRateGlobal(currency){
			 var rec='1';
				<c:forEach var="entry" items="${currencyExchangeRate}">
					if('${entry.key}'==currency.trim()){
						rec='${entry.value}';
					}
				</c:forEach>
				return rec;
		}
		
		function showAssigneeContributionAmount(){
			var rentAmount = eval(document.getElementById("TEN_rentAmount").value);
			var rentAllowance = eval(document.getElementById("TEN_rentAllowance").value);
			var rentAllowanceCurr = document.getElementById("TEN_allowanceCurrency").value;
			var rentAmountCurr = document.getElementById("TEN_rentCurrency").value;
			
			if(rentAmount == null || rentAmount=='' || rentAmount == undefined){
				rentAmount=0;
			}
			if(rentAllowance == null || rentAllowance=='' || rentAllowance == undefined){
				rentAllowance=0;
			}
			var rentAllowanceExRate = findExchangeRateGlobal(rentAllowanceCurr);
			var rentAllowanceFinalAmount = rentAllowance/rentAllowanceExRate;
			rentAllowanceFinalAmount = Math.round(rentAllowanceFinalAmount*10000)/10000;
			
			var rentAmountExRate = findExchangeRateGlobal(rentAmountCurr);
			var rentAmountFinalAmount = rentAmount/rentAmountExRate;
			rentAmountFinalAmount = Math.round(rentAmountFinalAmount*10000)/10000;
			
			if(rentAllowance > rentAmount){
				document.getElementById("assigneeContributionAmount").style.display ='block';
				var assigneeContributionFinalAmount = rentAllowanceFinalAmount-rentAmountFinalAmount;
				assigneeContributionFinalAmount = Math.round(assigneeContributionFinalAmount*10000)/10000;
				assigneeContributionFinalAmount = (assigneeContributionFinalAmount < 0) ? assigneeContributionFinalAmount * -1 : assigneeContributionFinalAmount;
				document.getElementById("TEN_assigneeContributionAmount").value = assigneeContributionFinalAmount;
				document.getElementById("rentAmt").style.display ='none';
				document.getElementById("TEN_contributionAmount").value = 0;
			}else{
				document.getElementById("rentAmt").style.display ='block';
				var assigneeContributionFinalAmount = rentAmountFinalAmount-rentAllowanceFinalAmount;
				assigneeContributionFinalAmount = Math.round(assigneeContributionFinalAmount*10000)/10000;
				assigneeContributionFinalAmount = (assigneeContributionFinalAmount < 0) ? assigneeContributionFinalAmount * -1 : assigneeContributionFinalAmount;
				document.getElementById("TEN_contributionAmount").value = assigneeContributionFinalAmount;
				document.getElementById("TEN_assigneeContributionCurrency").value = rentAmountCurr;
				document.getElementById("assigneeContributionAmount").style.display ='none';
				document.getElementById("TEN_assigneeContributionAmount").value = 0;
			}
		}
		
		function getStateOnReset(targetElement) {
			var country = targetElement;
			var countryCode = "";
			<c:forEach var="entry" items="${countryCod}">
				if(country=="${entry.value}"){
					countryCode="${entry.key}";
				}
			</c:forEach>
			 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
			 httpState.open("GET", url, true);
			 httpState.onreadystatechange = handleHttpResponse88;
			 httpState.send(null);
		}
		
		function handleHttpResponse88(){
		     if (httpState.readyState == 4){
		        var results = httpState.responseText
		        results = results.trim();
		        res = results.split("@");
		        targetElement = document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'];
				targetElement.length = res.length;
					for(i=0;i<res.length;i++) {
					if(res[i] == ''){
						document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'].options[i].text = '';
						document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'].options[i].value = '';
					}else{
						stateVal = res[i].split("#");
						document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'].options[i].text = stateVal[1];
						document.forms['dspDetailsForm'].elements['dspDetails.TEN_state'].options[i].value = stateVal[0];
						
					if (document.getElementById("TEN_state").options[i].value == '${dspDetails.TEN_state}'){
					   document.getElementById("TEN_state").options[i].defaultSelected = true;
					} } }
					document.getElementById("TEN_state").value = '${dspDetails.TEN_state}';
		     }
		}
		var httpState = getHTTPObject();
		
	 	function validate_email(targetElement,fieldName){
	 	 	if(targetElement.value!=''){
	 	 	if(targetElement.value.match(/\ /)){
	 	 		alert("Please enter valid email address.");
	 	 		document.forms['dspDetailsForm'].elements[fieldName].value ='';
	 	 		return false
	 	 	}
	 	 	with (targetElement){
	 	 	apos=value.indexOf("@")
	 	 	dotpos=value.lastIndexOf(".")
	 	 	if (apos<1||dotpos-apos<2){
	 	 		alert("Please enter valid email address.");
	 	 		document.forms['dspDetailsForm'].elements[fieldName].value ='';
	 	 		return false
	 	 	}else{
	 	 		return true
	 	 	}
	 	 	}
	 	 	}
	 	 	}
	 	function onlyRateAllowed(evt){
			var keyCode = evt.which ? evt.which : evt.keyCode; 
			return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110|| (keyCode==173)); 
		}
	 	
	 	function getUtilityCheckBoxValue(){
	 		<configByCorp:fieldVisibility componentId="component.dspdetails.tenancyManagement">
	 		var soId='${serviceOrder.id}';
	 		new Ajax.Request('/redsky/findUtilitiesCheckBoxValuesAjax.html?ajax=1&serviceOrderId='+soId+'&decorator=simple&popup=true',
	 							  {
	 							    method:'get',
	 							    onSuccess: function(transport){
	 							    var response = transport.responseText || "no response text";
	 							    var res = response.split("~");
	 							   var	TEN_utilitiesIncluded = res[24].trim();
	 								if(TEN_utilitiesIncluded == "Yes"){
	 								document.getElementById("utilitiesIncludedShow").style.display ='block';
		 							   	 var tenGasWater = res[0];
		 								 if(tenGasWater==1 || tenGasWater=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Gas_Water'].checked = true;
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Gas_Water'].disabled = false;
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Gas_Water'].checked = false;
		 								 }
		 								 var tenGas = res[1];
		 								 if(tenGas==1 || tenGas=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Gas'].checked = true;
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Gas'].disabled = false;
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Gas'].checked = false;
		 								 }
		 								var tenElectric = res[2];
		 								 if(tenElectric==1 || tenElectric=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Electricity'].checked = true;
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Electricity'].disabled = false;
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Electricity'].checked = false;
		 								 }
		 								 var tenTvInternet = res[3];
		 								 if(tenTvInternet==1 || tenTvInternet=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_TV_Internet_Phone'].checked = true;
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_TV_Internet_Phone'].disabled = false;
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_TV_Internet_Phone'].checked = false;
		 								 }
		 								 var tenMobilePhone = res[4];
		 								 if(tenMobilePhone==1 || tenMobilePhone=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_mobilePhone'].checked = true;
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_mobilePhone'].disabled = false;
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_mobilePhone'].checked = false;
		 								 }
		 								 var tenFurnitureRent = res[5];
		 								 if(tenFurnitureRent==1 || tenFurnitureRent=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_furnitureRental'].checked = true;
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_furnitureRental'].disabled = false;
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_furnitureRental'].checked = false;
		 								 }
		 								 var tenCleanService = res[6];
		 								 if(tenCleanService==1 || tenCleanService=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_cleaningServices'].checked = true;
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_cleaningServices'].disabled = false;
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_cleaningServices'].checked = false;
		 								 }
		 								 var tenParkingPermit = res[7];
		 								 if(tenParkingPermit==1 || tenParkingPermit=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_parkingPermit'].checked = true;
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_parkingPermit'].disabled = false;
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_parkingPermit'].checked = false;
		 								 }
		 								 var tenComTax = res[8];
		 								 if(tenComTax==1 || tenComTax=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_communityTax'].checked = true;
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_communityTax'].disabled = false;
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_communityTax'].checked = false;
		 								 }
		 								 var tenInsurance = res[9];
		 								 if(tenInsurance==1 || tenInsurance=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_insurance'].checked = true;
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_insurance'].disabled = false;
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_insurance'].checked = false;
		 								 }
		 								 var tenGardenMainten = res[10];
		 								 if(tenGardenMainten==1 || tenGardenMainten=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_gardenMaintenance'].checked = true;
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_gardenMaintenance'].disabled = false;
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_gardenMaintenance'].checked = false;
		 								 }
		 								 var tenMiscellaneous = res[11];
		 								 if(tenMiscellaneous==1 || tenMiscellaneous=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Miscellaneous'].checked = true;
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Miscellaneous'].disabled = false;
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Miscellaneous'].checked = false;
		 								 }
		 								 var tenGasElectric = res[25];
		 								 if(tenGasElectric==1 || tenGasElectric=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Gas_Electric'].checked = true; 
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Gas_Electric'].checked = false;
		 								 }
	 								}else{
	 									 document.getElementById("utilitiesIncludedShow").style.display ='none';
	 								}
		 								 var tenUtilityGasWater = res[12];
		 								 if(tenUtilityGasWater==1 || tenUtilityGasWater=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Gas_Water'].checked = true; 
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Gas_Water'].checked = false;
		 								 }
		 								 var tenUtilityGas = res[13];
		 								 if(tenUtilityGas==1 || tenUtilityGas=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Gas'].checked = true; 
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Gas'].checked = false;
		 								 }
		 								 var tenUtilityElectric = res[14];
		 								 if(tenUtilityElectric==1 || tenUtilityElectric=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Electricity'].checked = true; 
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Electricity'].checked = false;
		 								 }
		 								 var tenUtilityTvInternet = res[15];
		 								 if(tenUtilityTvInternet==1 || tenUtilityTvInternet=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_TV_Internet_Phone'].checked = true; 
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_TV_Internet_Phone'].checked = false; 
		 								 }
		 								 var tenUtilityMobilePhone = res[16];
		 								 if(tenUtilityMobilePhone==1 || tenUtilityMobilePhone=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_mobilePhone'].checked = true; 
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_mobilePhone'].checked = false;
		 								 }
		 								 var tenUtilityFurnitureRent = res[17];
		 								 if(tenUtilityFurnitureRent==1 || tenUtilityFurnitureRent=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_furnitureRental'].checked = true; 
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_furnitureRental'].checked = false;
		 								 }
		 								 var tenUtilityCleanService = res[18];
		 								 if(tenUtilityCleanService==1 || tenUtilityCleanService=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_cleaningServices'].checked = true; 
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_cleaningServices'].checked = false;
		 								 }
		 								 var tenUtilityParkingPermit = res[19];
		 								 if(tenUtilityParkingPermit==1 || tenUtilityParkingPermit=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_parkingPermit'].checked = true; 
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_parkingPermit'].checked = false;
		 								 }
		 								 var tenUtilityComTax = res[20];
		 								 if(tenUtilityComTax==1 || tenUtilityComTax=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_communityTax'].checked = true; 
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_communityTax'].checked = false;
		 								 }
		 								 var tenUtilityInsurance = res[21];
		 								 if(tenUtilityInsurance==1 || tenUtilityInsurance=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_insurance'].checked = true; 
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_insurance'].checked = false;
		 								 }
		 								 var tenUtilityGardenMainten = res[22];
		 								 if(tenUtilityGardenMainten==1 || tenUtilityGardenMainten=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_gardenMaintenance'].checked = true; 
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_gardenMaintenance'].checked = false;
		 								 }
		 								 var tenUtilityMiscellaneous = res[23];
		 								 if(tenUtilityMiscellaneous==1 || tenUtilityMiscellaneous=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Miscellaneous'].checked = true; 
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Miscellaneous'].checked = false;
		 								 }
		 								 var tenUtilityGasElectric = res[26];
		 								 if(tenUtilityGasElectric==1 || tenUtilityGasElectric=='1'){
		 									 document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Gas_Electric'].checked = true; 
		 								 }else{
		 									document.forms['dspDetailsForm'].elements['dspDetails.TEN_Utility_Gas_Electric'].checked = false;
		 								 }
	 							    },
	 							    onFailure: function(){ 
	 								    }
	 							  });
	 		</configByCorp:fieldVisibility>
	 	}
	function getCurrency(targetElement){
		var country = targetElement.value;
		var countryCode = "";
		var currencyVal = "";
		<c:forEach var="entry" items="${countryCod}">
		if(country=="${entry.value}"){
			countryCode="${entry.key}";
		}
		</c:forEach>
		<c:forEach var="entry1" items="${countryCodeAndFlex5Map}">
		if(countryCode=="${entry1.key}"){
			currencyVal="${entry1.value}";
		}
		</c:forEach>
		document.getElementById('TEN_rentCurrency').value = currencyVal;
	}
	function getVatPercent(){ 
	     <c:forEach var="entry" items="${euVatPercentList}">
	        if(document.getElementById('TEN_housingRentVatDesc').value=='${entry.key}') {
	        	document.getElementById('TEN_housingRentVatPercent').value='${entry.value}'; 
	       	}
	     </c:forEach>
	 }
</script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.10.1.min.js"></script> 
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery.tablesorter.js" ></script> 
   