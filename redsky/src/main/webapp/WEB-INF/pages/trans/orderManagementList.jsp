<%@ include file="/common/taglibs.jsp"%>  

 
<head>   
    <title>Order Management</title>   
    <meta name="heading" content="Order Management"/>   
    
<script language="javascript" type="text/javascript">

function clear_fields(){
		//document.forms['searchForm'].elements['activeStatus'].checked = false;
		//document.forms['searchForm'].elements['customerFile.job'].value  = "";
		//document.forms['searchForm'].elements['customerFile.coordinator'].value  = "";
		//document.forms['searchForm'].elements['customerFile.status'].value  = "";
		document.forms['searchForm'].elements['customerFile.sequenceNumber'].value  = ""; 
		document.forms['searchForm'].elements['customerFile.lastName'].value  = "";
		document.forms['searchForm'].elements['customerFile.firstName'].value  = "";
		document.forms['searchForm'].elements['customerFile.billToAuthorization'].value  = "";
		document.forms['searchForm'].elements['customerFile.billToReference'].value  = "";
		document.forms['searchForm'].elements['customerFile.quotationStatus'].value  = "";
		document.forms['searchForm'].elements['customerFile.billToName'].value  = "";
		//document.forms['searchForm'].elements['customerFile.accountName'].value ="";
		
}

function goToCustomerDetail(targetValue){
		document.forms['searchForm'].elements['id'].value = targetValue;
		document.forms['searchForm'].action = 'editOrderManagement.html?from=list';
		document.forms['searchForm'].submit();
}
function goToSearchCustomerDetail(){
		return selectSearchField();
		document.forms['searchForm'].action = 'searchOrderManagements.html';
		document.forms['searchForm'].submit();
}

function selectSearchField(){
	
		
		//var activeStatus=document.forms['searchForm'].elements['activeStatus'].checked;
		//var job=document.forms['searchForm'].elements['customerFile.job'].value;
		//var coordinator=document.forms['searchForm'].elements['customerFile.coordinator'].value;
		//var status=document.forms['searchForm'].elements['customerFile.status'].value;
		var sequenceNumber=document.forms['searchForm'].elements['customerFile.sequenceNumber'].value; 
		var lastName=document.forms['searchForm'].elements['customerFile.lastName'].value;
		var firstName=document.forms['searchForm'].elements['customerFile.firstName'].value;
		var billToAuthorization=document.forms['searchForm'].elements['customerFile.billToAuthorization'].value;
		var billToReference=document.forms['searchForm'].elements['customerFile.billToReference'].value;
		var billToName=document.forms['searchForm'].elements['customerFile.billToName'].value;
		//if(activeStatus==false)
		if(sequenceNumber=='' && lastName=='' &&  firstName=='' && billToAuthorization=='' && billToReference=='' && billToName=='' )
			{
				//alert('Please select any one of the search criteria!');	
				//return false;	
			}
	}
</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}

div.error, span.error, li.error, div.message {

width:450px;
margin-top:0px; 
}
form {
margin-top:-40px;
!margin-top:-10px;
}

div#main {
margin:-5px 0 0;

}

 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
</head>   
  
<c:set var="buttons">    
    <input type="button" class="cssbuttonA" style="width:55px; height:25px;"  
        onclick="location.href='<c:url value="/editOrderManagement.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px;!margin-bottom: 10px;" align="top"  key="button.search" onclick="return goToSearchCustomerDetail();"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
</c:set>   

<s:hidden name="fileID"  id= "fileID" value=""/>    
<s:form id="searchForm" action="searchOrderManagements.html" method="post" validate="true">  
<s:hidden name="csoSortOrder"   value="${csoSortOrder}"/>
<c:set var="csoSortOrder"  value="${csoSortOrder}"/>
<s:hidden name="orderForCso"   value="${orderForCso}"/>
<c:set var="orderForCso"  value="${orderForCso}"/>
<configByCorp:fieldVisibility componentId="component.link.customerFileList.AccountNameSEC">
<s:hidden name="customerFile.accountName" />
</configByCorp:fieldVisibility> 
<s:hidden name="id" />
<s:hidden name="actStatus" />
<c:set var="coordinatr" value="<%=request.getParameter("coordinatr") %>" />
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" style="width:100%" border="1" >
<thead>
<tr>
<th>Order #</th>
<th><fmt:message key="customerFile.lastName"/></th>
<th><fmt:message key="customerFile.firstName"/></th>

<th>Account PO#</th>
<th>Account Reference #</th>
<th>Bill To Name</th>
<!-- <th>Status</th> -->


</tr></thead>	
		<tbody>
		<tr>
			<td width="25" align="left">
			    <s:textfield name="customerFile.sequenceNumber" required="true" cssClass="input-text"  size="18" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td width="25" align="left">
			    <s:textfield name="customerFile.lastName" required="true" cssClass="input-text" size="18" />
			</td>
			<td width="25" align="left">
			    <s:textfield name="customerFile.firstName" required="true" cssClass="input-text" size="18" />
			</td> 
			<td width="25" align="left">
			    <s:textfield name="customerFile.billToAuthorization" required="true" cssClass="input-text" size="18" />
			</td>
		    <td width="25" align="left">
			    <s:textfield name="customerFile.billToReference" required="true" cssClass="input-text" size="18" />
			</td>
			<td width="25" align="left">
					<s:textfield name="customerFile.billToName" id="billToName1" size="20"  cssStyle="width:120px;margin-top:2px;!width:100px;" cssClass="input-text" />
			</td>
			<%-- <td width="25" align="left">
			    <s:select cssClass="list-menu"  headerKey="" headerValue="" name="customerFile.quotationStatus" list="{'','New','Rejected'}"  />
			 </td> --%>
			 <s:hidden name="customerFile.orderIntiationStatus" value="Submitted" />
			</tr>
			
			<tr >
			
			<td colspan="5"></td> 
			<td width="140"  align="center" style="border-left: hidden;">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
<c:out value="${searchresults}" escapeXml="false" />  
		<div id="otabs" style="margin-top: -10px;">
		  <ul>
		    <li><a class="current"><span>Order List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<s:set name="customerFileExt" value="customerFiles" scope="request"/>   
<display:table name="customerFileExt" class="table" requestURI="" id="customerFileList" export="true"  pagesize="25" style="width:100%;" >   
  
  <%--   <display:column sortable="true" titleKey="customerFile.sequenceNumber"><a onclick="goToCustomerDetail(${customerFileList.id});" style="cursor:pointer"><c:out value="${customerFileList.sequenceNumber}" /></a></display:column> --%>
    <display:column property="sequenceNumber" sortable="true" title="Order #" url="/editOrderManagement.html?from=list" paramId="id" paramProperty="id"/>
    <display:column property="lastName" sortable="true" titleKey="customerFile.lastName" style="width:90px" maxLength="20"/>
    <display:column property="firstName" sortable="true" titleKey="customerFile.firstName" maxLength="20"/>
    <display:column property="coordinator" style="text-transform: uppercase;" sortable="true" titleKey="customerFile.coordinator" />
    <display:column property="quotationStatus" style="text-transform: uppercase;" sortable="true" title="Status" />
    <display:column property="originCountry" sortable="true" titleKey="serviceOrder.originCountry" style="width:20px"/>
	<display:column property="originCity" sortable="true" titleKey="serviceOrder.originCity1" style="width:60px"/>    
	<display:column property="destinationCountry" sortable="true" titleKey="serviceOrder.destinationCountry" style="width:20px"/> 
	<display:column property="destinationCity" sortable="true" titleKey="serviceOrder.destinationCity1" style="width:60px"/>   
    <display:column property="billToName" sortable="true" title="Bill To Name" style="width:95px" maxLength="19"/>
    
    <display:column property="createdOn" sortable="true" titleKey="customerFile.createdOn" style="width:100px" format="{0,date,dd-MMM-yyyy}"/>
       
   
    <display:setProperty name="paging.banner.item_name" value="customerfile"/>   
    <display:setProperty name="paging.banner.items_name" value="customers"/>   
  
    <display:setProperty name="export.excel.filename" value="CustomerFile List.xls"/>   
    <display:setProperty name="export.csv.filename" value="CustomerFile List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="CustomerFile List.pdf"/>   
</display:table>   
<s:hidden name="componentId" value="customerFile" />  
<c:set var="isTrue" value="false" scope="session"/>
</s:form>
 
<script type="text/javascript">   
   try{
    document.forms['searchForm'].elements['customerFile.sequenceNumber'].focus();  
  }
  catch(e){}
   try{
    document.forms['searchForm'].elements['customerFile.coordinator'].value='${coordinatr}';
    }
    catch(e){}
   try{
    <c:if test="${detailPage == true}" >
    	<c:redirect url="/editOrderManagement.html?from=list&controlFlagBnt=yes&id=${customerFileList.id}"  />
	</c:if>
	}
	catch(e){}
</script>	
<script type="text/javascript">   
    highlightTableRows("customerFileList");  
    Form.focusFirstElement($("searchForm"));
	try{
   	if('${activeStatus}'=='true')
   	{
   		document.forms['searchForm'].elements['activeStatus'].checked = true;
   	}
   	}
   	catch(e){}
   try{
   if('${activeStatus}'=='false')
   	{
   		document.forms['searchForm'].elements['activeStatus'].checked = false;
   	}
   	}
   	catch(e){}
</script> 