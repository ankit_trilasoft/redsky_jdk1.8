<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
  <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
  
<head>   
    <title><fmt:message key="claimDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='claimDetail.heading'/>"/>
    
    <style><%@ include file="/common/calenderStyle.css"%></style>

<style type="text/css">
	/* collapse */	
	.upper-case{text-transform:capitalize;}
 .ui-autocomplete { max-height: 250px; overflow-y: auto; /* prevent horizontal scrollbar */overflow-x: hidden;}
  /* IE 6 doesn't support max-height 
   */
  * html .ui-autocomplete {height: 100px;}	
</style>
</head>
 <c:set var="hideValuationDetails" value="false"></c:set>
 		<configByCorp:fieldVisibility componentId="component.tab.cportal.hideValuationDetails">
		<c:set var="hideValuationDetails" value="true">
		</c:set>
		</configByCorp:fieldVisibility>
<c:set var="currencySign" value="${currencySign}"/> 
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="claimForm" action="saveClaim" onsubmit="return submit_form(),activecheck();" method="post" validate="true">
   
<s:hidden name="claim.id" value="%{claim.id}"/>
<s:hidden name="claim.corpID" />

<s:hidden name="claim.submitFlag" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.ship" /> 
<s:hidden name="claim.idNumber" /> 
<s:hidden name="claim.claimUID" /> 
<s:hidden name="serviceOrder.sid" value="%{serviceOrder.id}"/>
 
<s:hidden name="soId" value="%{serviceOrder.id}"/>
<s:hidden name="customerFile.status"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
		<c:if test="${hideValuationDetails == 'false' }">
		<s:hidden name="claim.assistanceRequired" />
		</c:if>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="propertyAmount" value="N" />
<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
	<c:set var="propertyAmount" value="Y" />
</configByCorp:fieldVisibility>
<c:if test="${roleEmployee && responsibleAgent}">
	<c:set var="editable" value="false"/>
</c:if>
<c:if test="${roleEmployee && (!responsibleAgent)}">
	<c:set var="editable" value="true"/>
</c:if>
<c:if test="${(!roleEmployee) && (responsibleAgent)}">
	<c:set var="editable" value="true"/>
</c:if>
<c:if test="${(!roleEmployee) && (!responsibleAgent)}">
	<c:set var="editable" value="false"/>
</c:if>
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<c:set var="field" value="<%=request.getParameter("field") %>"/>
<s:hidden name="field" value="<%=request.getParameter("field") %>" />
<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
<c:set var="field1" value="<%=request.getParameter("field1") %>"/>
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.serviceorder' }">
   <c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.billing' }">
   <c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accounting' }">
   <c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.newAccounting' }">
   <c:redirect url="/pricingList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.forwarding' }">
	<c:if test="${forwardingTabVal!='Y'}">
   		<c:redirect url="/containers.html?id=${serviceOrder.id}"/>
   </c:if>
   <c:if test="${forwardingTabVal=='Y'}">
   		<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}"/>
	</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.OI' }">
	<c:redirect url="/operationResource.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.domestic' }">
   <c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.status' }">
<c:if test="${serviceOrder.job =='RLO'}"> 
		 <c:redirect url="/editDspDetails.html?id=${serviceOrder.id}" />
</c:if>
<c:if test="${serviceOrder.job !='RLO'}"> 
		<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}" />
</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.ticket' }">
   <c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.customerfile' }">
   <c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.claimso' }">
   <c:redirect url="/claims.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.claimitem' }">
   <c:redirect url="/losss.html?id=${claim.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.criticaldate' }">
	<sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
		<c:redirect url="/soAdditionalDateDetails.html?sid=${serviceOrder.id}" />
	</sec-auth:authComponent>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
<s:hidden id="countForClaimNotes" name="countForClaimNotes" value="<%=request.getParameter("countForClaimNotes") %>"/>
<s:hidden id="countClaimValuationNotes" name="countClaimValuationNotes" value="<%=request.getParameter("countClaimValuationNotes") %>"/>
<s:hidden name="claim.serviceOrderId" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.id"/>
<c:set var="countForClaimNotes" value="<%=request.getParameter("countForClaimNotes") %>" />
<c:set var="countClaimValuationNotes" value="<%=request.getParameter("countClaimValuationNotes") %>" />

	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:if test="${not empty claim.noProcess}">
		 	<s:text id="claimnoProcess" name="${FormDateValue}"> <s:param name="value" value="claim.noProcess" /></s:text>
			<s:hidden  name="claim.noProcess" value="%{claimnoProcess}" /> 
</c:if>
<c:if test="${empty claim.noProcess}">
	<s:hidden name="claim.noProcess"/> 
</c:if>
<c:if test="${not empty claim.claimEmailSent}">
		 	<s:text id="claimEmailSentID" name="${FormDateValue}"> <s:param name="value" value="claim.claimEmailSent" /></s:text>
			<s:hidden  name="claim.claimEmailSent" value="%{claimEmailSentID}" /> 
</c:if>
<c:if test="${empty claim.claimEmailSent}">
	<s:hidden name="claim.claimEmailSent"/> 
</c:if>
<div id="Layer1" style="width:100%" onkeydown="changeStatus();"> 

<div id="Layer3" style="width:100%;">
		<div id="newmnav">
            <ul>
             <sec-auth:authComponent componentId="module.tab.claims.serviceorderTab">
             <li><a onclick="setReturnString('gototab.serviceorder');return autoSaveFunc('none');"><span>S/O Details</span></a></li>
             </sec-auth:authComponent>
               <sec-auth:authComponent componentId="module.tab.claims.billingTab">
             <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
             	<li><a onclick="setReturnString('gototab.billing');return autoSaveFunc('none');"><span>Billing</span></a></li>
             </sec-auth:authComponent>
             </sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.claims.accountingTab">
             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
             <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
			      <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			   </c:when>
			    <c:otherwise> 
		          <li><a onclick="setReturnString('gototab.accounting');return autoSaveFunc('none');"><span>Accounting</span></a></li>
		       </c:otherwise>
		     </c:choose>
		     </c:if>
		     </sec-auth:authComponent>
		     <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
             <c:choose> 
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
			      <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			   </c:when>
			    <c:otherwise> 
		          <li><a onclick="setReturnString('gototab.newAccounting');return autoSaveFunc('none');"><span>Accounting</span></a></li>
		       </c:otherwise>
		     </c:choose>
		     </c:if>
		     </sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
   	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a onclick="setReturnString('gototab.OI');return autoSaveFunc('none');"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
	         <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
           	  </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.claims.forwardingTab">
		      <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		       <c:if test="${userType!='ACCOUNT'}">
             		<li><a onclick="setReturnString('gototab.forwarding');return autoSaveFunc('none');"><span>Forwarding</span></a></li>
             </c:if>
             <c:if test="${userType=='ACCOUNT' && serviceOrder.job !='RLO'}">
              <li><a href="servicePartnerss.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
             </c:if>
             </c:if>
             </sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.claims.domesticTab">
             <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
             <li><a onclick="setReturnString('gototab.domestic');return autoSaveFunc('none');"><span>Domestic</span></a></li>
             </c:if>
             </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
              <c:if test="${serviceOrder.job =='INT'}">
               <li><a onclick="setReturnString('gototab.domestic');return autoSaveFunc('none');"><span>Domestic</span></a></li>
              </c:if>
              </sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.claims.statusTab">
             <li><a onclick="setReturnString('gototab.status');return autoSaveFunc('none');"><span>Status</span></a></li>
             </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.claims.ticketTab">
             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
             <li><a onclick="setReturnString('gototab.ticket');return autoSaveFunc('none');"><span>Ticket</span></a></li>
             </c:if>
             </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.claims.summaryTab">
			  	<li><a href="findSummaryList.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
			  	</sec-auth:authComponent>
               <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
               <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Claims</span></a></li>
             </c:if>
             </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.claims.customerFileTab">
             <li><a onclick="setReturnString('gototab.customerfile');return autoSaveFunc('none');"><span>Customer File</span></a></li>
             </sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
				 <li><a onclick="setReturnString('gototab.criticaldate');return autoSaveFunc('none');"><span>Critical Dates</span></a></li>
			</sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
           	 	<li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
           	  </sec-auth:authComponent> 
           	  <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 			</configByCorp:fieldVisibility> 
	 			<c:if test="${userType=='USER'}">
		 		  <configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
	  				<li><a href="findEmailSetupTemplateByModuleNameSO.html?sid=${serviceOrder.id}"><span>View Emails</span></a></li>
	  			  </configByCorp:fieldVisibility>
                </c:if>         	  
   </ul>
 </div>
 		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 0px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
 <div class="spn spnSF" style="clear:both;">&nbsp;</div>
   <div style="padding-bottom:0px;!padding-bottom:3px;"></div>

 </div>
 <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
<c:if test="${not empty claim.id}">
<div id="Layer4" style="width:100%;">
<div id="newmnav">
			<ul>
             <li><a onclick="setReturnString('gototab.claimso');return autoSaveFunc('none');"><span>Claims</span></a></li>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Claim Detail</span></a></li>
             <li><a onclick="setReturnString('gototab.claimitem');return autoSaveFunc('none');"><span>Claim Items</span></a></li>
               <sec-auth:authComponent componentId="module.tab.claims.reportTab">
             <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&claimNumber=${claim.claimNumber}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Claims&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
             </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.trackingStatus.auditTab">
           	 <li><a onclick="window.open('auditList.html?id=${claim.id}&tableName=claim&decorator=popup&popup=true','audit','height=400,width=770,top=100, left=120, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
           	  </sec-auth:authComponent>
              </ul>
                 
       </div><div class="spn spnSF">&nbsp;</div>
    <div style="padding-bottom:0px;!padding-bottom:3px;"></div>
       </div>       
 </td></tr>
</c:if>
<c:if test="${empty claim.id}">

<div id="newmnav">
			<ul>
             <li><a onclick="setReturnString('gototab.claimso');return autoSaveFunc('none');"><span>Claims</span></a></li>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Claim Detail</span></a></li>
             <li><a><span>Claim Items</span></a></li>
              <li><a><span>Forms</span></a></li>
             
         </ul>
       </div><div class="spn spnSF">&nbsp;</div>
     <div style="padding-bottom:0px;!padding-bottom:5px;"></div>
 </td></tr>
</c:if>

<div id="content" align="center" >
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
<table style="margin-bottom:5px;" cellspacing="0" cellpadding="0" border="0"  width="100%">
<tbody>
<tr>
<td>
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" width="">
<tbody>
<tr><td height="5px"></td></tr>
<tr>
<td align="right" class="listwhitetext" ><fmt:message key="claim.claimNumber"/></td>
<td align="left"><s:textfield id="claimPerson1" cssClass="input-textUpper" name="claim.claimNumber" cssStyle="width:200px" maxlength="10" readonly="true" /></td>

<configByCorp:fieldVisibility componentId="component.claim.unigroupclaim.edit"> 
<td align="right" class="listwhitetext" >Unigroup&nbsp;Claim#</td>
<td align="left" style="padding-bottom:2px;"><s:textfield cssClass="input-textUpper" name="claim.clmsClaimNbr" cssStyle="text-align:right;width:237px;" maxlength="55" required="true" readonly="true" /></td>
</configByCorp:fieldVisibility>
<td align="right"  width="73"  class="listwhitetext">Claims</td>
<td><s:select name="claim.claimPerson" cssClass="list-menu" list="%{claimPersonList}"  cssStyle="width:242px" headerKey="" headerValue="" onchange="changeStatus();" /></td>

<td align="right" width="66" valign="middle" class="listwhitetext" rowspan="2">
	<c:if test="${serviceOrder.vip}">
		<div style="position:absolute;top:36px;right:235px;">
			<img id="vipImage" src="${pageContext.request.contextPath}/images/vip_icon.png" />
		</div>
	</c:if>
</td>
<sec-auth:authComponent componentId="module.tab.claims.customerFileTab">
<c:if test="${empty claim.id}">
<td style="position:absolute;right:45px;margin-top:42px;" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty claim.id}">
<c:choose>
<c:when test="${countForClaimNotes == '0' || countForClaimNotes == '' || countForClaimNotes == null}">
<td style="position:absolute;right:45px;margin-top:42px;" align="right"><img id="countForClaimNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${claim.claimNumber}&noteFor=Claim&subType=Claim&imageId=countForClaimNotesImage&fieldId=countForClaimNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${claim.claimNumber}&noteFor=Claim&subType=Claim&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td style="position:absolute;right:45px;margin-top:42px;" align="right"> <img id="countForClaimNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${claim.claimNumber}&noteFor=Claim&subType=Claim&imageId=countForClaimNotesImage&fieldId=countForClaimNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${claim.claimNumber}&noteFor=Claim&subType=Claim&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</sec-auth:authComponent>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claim.addressLine1"/></td>
<td align="left"><s:textfield cssClass="input-text upper-case" name="claim.addressLine1" cssStyle="width:200px" maxlength="100" required="true" onblur="titleCase(this)" /></td>
<td align="right" class="listwhitetext" ><fmt:message key="claim.paidByName"/></td>
<td align="left" style="padding-bottom:2px;"><s:textfield cssClass="input-textUpper" name="claim.paidByName" cssStyle="width:238px" maxlength="55" required="true" readonly="true" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claim.addressLine2"/></td>
<td align="left"><s:textfield cssClass="input-text upper-case" name="claim.addressLine2" cssStyle="width:200px" maxlength="100" required="true" onblur="titleCase(this)" /></td>
<configByCorp:fieldVisibility componentId="component.claim.ClaimType.edit"> 
<td align="right"  width="73"  class="listwhitetext">Claim Type</td>
<td><s:select name="claim.claimType" cssClass="list-menu" list="%{claimTypeList}"  cssStyle="width:240px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
</configByCorp:fieldVisibility>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claim.addressLine3"/></td>
<td align="left"><s:textfield cssClass="input-text upper-case" name="claim.addressLine3" cssStyle="width:200px" maxlength="100" required="true" onblur="titleCase(this)" /></td>
<td align="right" class="listwhitetext">Responsible&nbsp;Agent</td>
<td align="left" style="float:left;">
<s:textfield cssClass="input-text" readonly="${roleEmployee}" key="claim.responsibleAgentCode" id="responsibleAgentCodeId" cssStyle="width:45px;" maxlength="10" onchange="valid(this,'special');checkVendorNameNew();showPartnerAlert('onload',this.value,'responsibleAgentNameDivId');" tabindex=""    onkeypress="return disableEnterKey(event);" onfocus="showPartnerAlert('onload',this.value,'responsibleAgentNameDivId');"/>
<c:if test="${!roleEmployee}">
<img align="top" class="openpopup" width="17" height="20" onclick="javascript:openWindow('agentPartners.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=claim.responsibleAgentName&fld_code=claim.responsibleAgentCode');" src="<c:url value='/images/open-popup.gif'/>" />
</c:if>
<s:textfield cssStyle="width:164px" readonly="${roleEmployee}"  cssClass="input-text"  id="responsibleAgentNameId" key="claim.responsibleAgentName"  required="true" onchange="populateAgentCodeAlert('OA','responsibleAgentNameId');showPartnerAlert('onload',this.value,'responsibleAgentNameDivId');findPartnerDetailsByName('responsibleAgentCodeId','responsibleAgentNameId');" onkeyup="findPartnerDetailsNew('responsibleAgentNameId','responsibleAgentCodeId','responsibleAgentNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','',event);"   tabindex="" />
<div id="responsibleAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
</td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claim.country"/></td>
<td align="left"><s:select cssClass="list-menu"  name="claim.country" list="%{dcountry}" cssStyle="width:203px" onchange="getCountryCode(this);autoPopulate_customerFile_destinationCountry(this);getState(this);changeStatus();enableStateListOrigin();"  /></td>
<td align="right" class="listwhitetext"><fmt:message key="claim.state"/></td>
<td align="left"><s:select cssClass="list-menu"  name="claim.state" id="state" list="%{dstates}" cssStyle="width:243px" onchange="autoPopulate_customerFile_destinationCityCode(this);changeStatus();" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claim.city"/></td>
<td align="left"><s:textfield cssClass="input-text upper-case" name="claim.city" id="city" onblur="titleCase(this)" onkeypress="" cssStyle="width:200px" maxlength="30" required="true" onchange="autoPopulate_customerFile_destinationCityCode(this);" onkeyup="autoCompleterAjaxCity();" /></td>
<td align="right" class="listwhitetext"><fmt:message key="claim.zip"/></td>
<td align="left"><s:textfield cssClass="input-text" name="claim.zip" cssStyle="width:238px" maxlength="10" /></td>

</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claim.phone"/></td>
<td align="left"><s:textfield cssClass="input-text" name="claim.phone" cssStyle="width:200px" maxlength="20" required="true"  /></td>
<td align="right" class="listwhitetext"><fmt:message key="claim.shipperWorkPhone"/></td>
<td align="left"><s:textfield cssClass="input-text" name="claim.shipperWorkPhone" cssStyle="width:238px" maxlength="26" required="true"  /></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claim.fax"/></td>
<td align="left"><s:textfield cssClass="input-text" name="claim.fax" cssStyle="width:200px" maxlength="20" required="true"  /></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claim.email"/></td>
<td colspan=""><s:textfield cssClass="input-text" name="claim.email" cssStyle="width:200px" maxlength="65" required="true"  onchange="return validateEmail(this)" /></td>
<td align="right" class="listwhitetext" ><fmt:message key="claim.email2"/></td>
<td colspan=""><s:textfield cssClass="input-text" name="claim.email2" cssStyle="width:238px" maxlength="65" required="true"  onchange="return validateEmail(this)" /></td>
<configByCorp:fieldVisibility componentId="component.tab.claim.claimIntegrationApps">
<c:if test="${not empty claim.id}">
<c:if test="${moveCount > 0}">						
							<td align="left" class="listwhitetext" width="50"><img class="openpopup" id="openpopup.img1" style="vertical-align:text-bottom;margin-left:17px; margin-top:1px;" title="Move Cloud" src="<c:url value='/images/move4u.png'/>" />
							<c:choose> 
			                <c:when test="${emailSetupValue=='Ready For Sending'}">
					        <img style="float:right;" src="<c:url value='/images/ques-small.gif'/>"  title="Ready For Sending"/>
			                </c:when>
			                <c:when test="${fn:indexOf(emailSetupValue,'Ready For Sending')>-1}">
					        <img  style="float:right;" src="<c:url value='/images/cancel001.gif'/>" title="${fn:replace(emailSetupValue,'Ready For Sending','')}"/>
			                </c:when> 							
			                <c:otherwise>
				            <img  style="float:right;" src="<c:url value='/images/tick01.gif'/>"  title="${emailSetupValue}"/>
			                </c:otherwise>			
		                    </c:choose>	
							</td>
							</c:if>
<td colspan="2"><input type="button" class="cssbutton1" onclick="sendEmailToClaim()" value="Send To MoveCloud" style="width:124px; height:23px"/></td>
</c:if>
 </configByCorp:fieldVisibility>
</tr>
<tr>
<td align="right" class="listwhitetext" width="86"><fmt:message key="claim.formRecived"/></td>
<c:if test="${not empty claim.formRecived}">
<s:text id="claimFormRecivedFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.formRecived"/></s:text>
<td><s:textfield cssClass="input-text" id="formRecived" name="claim.formRecived" value="%{claimFormRecivedFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)"  onselect="calcDays()"/><img id="formRecived_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty claim.formRecived}">
<td><s:textfield cssClass="input-text" id="formRecived" name="claim.formRecived" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)"  onselect="calcDays()"/><img id="formRecived_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>

<td align="right" class="listwhitetext" width="130"><fmt:message key="claim.insuranceCompanyPaid"/></td>
<c:if test="${not empty claim.insuranceCompanyPaid}">
<s:text id="claimInsuranceCompanyPaidFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.insuranceCompanyPaid"/></s:text>
<td><s:textfield cssClass="input-text" id="insuranceCompanyPaid" name="claim.insuranceCompanyPaid" value="%{claimInsuranceCompanyPaidFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)" /><img id="insuranceCompanyPaid_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty claim.insuranceCompanyPaid}">
<td><s:textfield cssClass="input-text" id="insuranceCompanyPaid" name="claim.insuranceCompanyPaid" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)" /><img id="insuranceCompanyPaid_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:set var="isPortalFlag" value="false"/>
													<c:if test="${claim.cportalAccess}">
														<c:set var="isPortalFlag" value="true"/>
													</c:if>
													
 <sec-auth:authComponent componentId="module.claimForm.section.claimStatus.edit">
	<%
	String sectionClaimStatus="true";
	int permission  = (Integer)request.getAttribute("module.claimForm.section.claimStatus.edit" + "Permission");
 	if (permission > 2 ){
 		sectionClaimStatus = "false";
 		System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionClaimStatus);
 	}
 	System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionClaimStatus);
  %>

  <td align="right" class="listwhitetext" >Claims&nbsp;Required</td>
  <td width="" ><s:checkbox key="claim.cportalAccess" id ="wonder" cssStyle="margin:0px;" value="${isPortalFlag}"  fieldValue="true" onclick="cportalCheckDetailStauts();"  disabled="<%=sectionClaimStatus%>"/></td>
</sec-auth:authComponent>
 <c:if test="${hideValuationDetails == 'true' }">
 <c:set var="isAssistanceRequired" value="false"/>
													<c:if test="${claim.assistanceRequired}">
														<c:set var="isAssistanceRequired" value="true"/>
													</c:if>
<table style="margin:0px;padding:0px;">
<tr>
<td width="95"></td> <td align="left" colspan="4" class="listwhitetext"><fmt:message key='claimService.assistanceReq'/></td>
<td width="102" align="right" class="listwhitetext">Assistance&nbsp;Required</td>
<td align="left">
<s:checkbox key='claim.assistanceRequired' value ="${isAssistanceRequired}" cssStyle="vertical-align:middle;margin:0px;"/>
</td>
</tr>
</table>
</c:if>
</tr>
</tbody>
</table>
<table style="margin:0px;width:100%;">
<tr><td align="center" colspan="15" class="vertlinedata"></td></tr>
</table>
<table style="margin:0px;">
<tbody>
 <sec-auth:authComponent componentId="module.claimForm.section.claimStatus.edit">
<tr>
<td colspan="15">
<table class="detailTabLabel">
<tr>
<td align="right" class="listwhitetext" width="85"><fmt:message key="claim.bankAccountNo"/></td>
<td align="left"><s:textfield cssClass="input-text" name="claim.bankAccountNo" cssStyle="width:200px" maxlength="30" required="true" /></td>

<td align="right" class="listwhitetext"><fmt:message key="claim.bankName"/></td>
<td align="left" colspan="3"><s:textfield cssClass="input-text" name="claim.bankName" cssStyle="width:304px" maxlength="100" /></td>
<sec-auth:authComponent componentId="module.tab.claims.customerFileTab">
<c:if test="${empty claim.id}">
<td  style="position:absolute;right:45px;" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 align="top" onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty claim.id}">
<c:choose>
<c:when test="${countClaimValuationNotes == '0' || countClaimValuationNotes == '' || countClaimValuationNotes == null}">
<td style="position:absolute;right:45px;" align="right"> 
<img id="countClaimValuationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 align="top" onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber }&noteFor=Claim&subType=ClaimValuation&imageId=countClaimValuationNotesImage&fieldId=countClaimValuationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${claim.claimNumber1}&noteFor=Claim&subType=ClaimValuation&decorator=popup&popup=true',800,600);" ></a>

</td>
</c:when>
<c:otherwise>
<td style="position:absolute;right:45px;" align="right">
<img id="countClaimValuationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg"  HEIGHT=17 WIDTH=50 align="top" onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber }&noteFor=Claim&subType=ClaimValuation&imageId=countClaimValuationNotesImage&fieldId=countClaimValuationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${claim.claimNumber1}&noteFor=Claim&subType=ClaimValuation&decorator=popup&popup=true',800,600);" ></a>
</td>
</c:otherwise>
</c:choose> 
</c:if>	
</sec-auth:authComponent>


</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claim.numberIBAN"/></td>
<td align="left"><s:textfield cssClass="input-text" name="claim.numberIBAN" cssStyle="width:200px" maxlength="50" /></td>

<td align="right" width="134" class="listwhitetext"><fmt:message key="claim.bankAddress"/></td>
<td align="left" colspan="3"><s:textfield cssClass="input-text" name="claim.bankAddress" cssStyle="width:304px" maxlength="200" /></td>

</tr>

<tr>
<configByCorp:fieldVisibility componentId="component.claim.loss.lossImages"> 
<td align="right" class="listwhitetext">Bank's&nbsp;Country</td>
<td align="left"><s:select cssClass="list-menu"  name="claim.claimantCountry" list="%{country}" headerKey="" headerValue="" cssStyle="width:205px" onchange="getCountryCode(this);changeStatus();" /></td>
<td align="right" class="listwhitetext">Bank's&nbsp;City</td>
<td align="left"><s:textfield cssClass="input-text upper-case" name="claim.claimantCity" id="city" onblur="titleCase(this)" onkeypress="" cssStyle="width:200px" maxlength="15" required="true" onchange="" onkeyup="" /></td>
</configByCorp:fieldVisibility>
</tr>

<tr>
<td align="right" class="listwhitetext">BIC&nbsp;/&nbsp;SWIFT&nbsp;code</td>
<td align="left"><s:textfield cssClass="input-text" name="claim.swiftCode" cssStyle="width:200px" maxlength="50"  /></td>
</tr>
<tr>
<td align="right" class="listwhitetext">Branch&nbsp;/&nbsp;sort&nbsp;code</td>
<td align="left"><s:textfield cssClass="input-text" name="claim.branchandsortcode" cssStyle="width:200px" maxlength="180" /></td>

<td align="right" width="120" class="listwhitetext">Name&nbsp;account&nbsp;holder</td>
<td align="left" colspan="3"><s:textfield cssClass="input-text" name="claim.accountname" cssStyle="width:304px" maxlength="100" /></td>         
</tr>
<configByCorp:fieldVisibility componentId="component.tab.claim.claimIntegrationApps">
<tr>
<td align="right" class="listwhitetext">Add.&nbsp;Bank&nbsp;Info</td>
<td colspan="5"><s:textarea id="instructions" name="claim.additionalbankinformation" cssStyle="width:538px;" rows="4" cssClass="textarea" onkeydown="return checkLength();" /></td>								
</tr>
</configByCorp:fieldVisibility>
</table>
</td>
</tr>
</sec-auth:authComponent>
</tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tbody>

<tr><td height="0" width="100%" align="left" >
<%
	String sectionClaimStatus="false";
%>

	<div  onClick="javascript:animatedcollapse.toggle('claimstatus')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Claim Status
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>			
		
		
		<div id="claimstatus">
		<table class="detailTabLabel" border="0" width="100%">
 		<tbody>
 		<tr><td align="left" height="5px"></td></tr>
 		<tr><td colspan="15" >
<table class="detailTabLabel" border="0" cellpadding="2" cellspacing="0" >
<tbody>
<tr>
<td align="right" width="85px" class="listwhitetext"><fmt:message key="claim.requestForm"/></td>
<c:if test="${not empty claim.requestForm}">
<s:text id="claimRequestFormFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.requestForm"/></s:text>
<td width="40px"><s:textfield cssClass="input-text" id="requestForm"  name="claim.requestForm" value="%{claimRequestFormFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)"/></td><td>
<% if (sectionClaimStatus.equalsIgnoreCase("false")){%> 
	<img id="requestForm_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
<%} %>
</td>
</c:if>
<c:if test="${empty claim.requestForm}">
<td width="40px"><s:textfield cssClass="input-text" id="requestForm" name="claim.requestForm" required="true"  cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)"/></td><td>
<% if (sectionClaimStatus.equalsIgnoreCase("false")){%> 
	<img id="requestForm_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
<%} %>
</td>
</c:if>
<td width="10px"></td>
<td align="right" class="listwhitetext" width="100px">Acceptance Sent</td>
<c:if test="${not empty claim.acceptanceSent}">
<s:text id="acceptanceSentFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.acceptanceSent"/></s:text>
<td width="40px"><s:textfield cssClass="input-text" id="acceptanceSent"  name="claim.acceptanceSent" value="%{acceptanceSentFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)"/></td><td>
<% if (sectionClaimStatus.equalsIgnoreCase("false")){%>
 <img id="acceptanceSent_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
 <%} %>
</td>
</c:if>
<c:if test="${empty claim.acceptanceSent}">
<td width="40px"><s:textfield cssClass="input-text" id="acceptanceSent" name="claim.acceptanceSent"  required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)"/></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%>  <img id="acceptanceSent_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<td width="0px"></td>
<td align="right" class="listwhitetext">Cheque&nbsp;To&nbsp;Shipper</td>
<c:if test="${not empty claim.inspecteDate}">
<s:text id="claimInspecteDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.inspecteDate"/></s:text>
<td><s:textfield cssClass="input-text" id="inspecteDate" name="claim.inspecteDate" value="%{claimInspecteDateFormattedValue}" required="true" cssStyle="width:60px" maxlength="11"  onkeydown="onlyDel(event,this)"/></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%>  <img id="inspecteDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.inspecteDate}">
<td><s:textfield cssClass="input-text" id="inspecteDate" name="claim.inspecteDate" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)"  /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="inspecteDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<td width="0px"></td>
<td align="right" class="listwhitetext" ><fmt:message key="claim.dateOfInspection"/></td>
<c:if test="${not empty claim.dateOfInspection}">
<s:text id="claimInspectionDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.dateOfInspection"/></s:text>
<td width="40px"><s:textfield cssClass="input-text" id="dateOfInspection" name="claim.dateOfInspection" value="%{claimInspectionDateFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="dateOfInspection_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.dateOfInspection}">
<td width="40px"><s:textfield cssClass="input-text" id="dateOfInspection" name="claim.dateOfInspection" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="dateOfInspection_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<td align="right" class="listwhitetext" >Status</td>
<td align="left"><s:select cssClass="list-menu" name="claim.status"  cssStyle="width:65px" list="%{claimStatus}"  headerKey="" headerValue="" disabled="<%=sectionClaimStatus%>"/></td>
<td width="40px"></td>
<c:if test="${from=='rule'}">
<c:if test="${field1=='claim.insuranceUser'}">
<td align="right" class="listwhitetext" colspan="3" ><font color="red">Claim&nbsp;Handled&nbsp;By&nbsp;Insurer</font></td>
</c:if>
</c:if>
<c:if test="${from=='rule'}">
<c:if test="${field1 !='claim.insuranceUser'}">
<td align="right" class="listwhitetext" colspan="3" >Claim&nbsp;Handled&nbsp;By&nbsp;Insurer</font></td>
</c:if>
</c:if>
<c:if test="${from!='rule'}">
<td align="right" class="listwhitetext" colspan="3" ><font color="red">Claim&nbsp;Handled&nbsp;By&nbsp;Insurer</font></td>
</c:if>
<td colspan="2" class="listwhitetext" nowrap="nowrap" align="left" style="padding-left:0px;">
	<s:checkbox cssStyle="vertical-align:middle;" name="claim.insuranceUser" id="insuranceUser" value="${claim.insuranceUser}"  onclick="changeInsurerStatus(this);"/>
</td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claim.formSent"/></td>
<c:if test="${not empty claim.formSent}">
<s:text id="claimFormSentFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.formSent"/></s:text>
<td><s:textfield cssClass="input-text" id="formSent" name="claim.formSent" value="%{claimFormSentFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="formSent_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.formSent}">
<td><s:textfield cssClass="input-text" id="formSent" name="claim.formSent" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%><img id="formSent_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<td width="0px"></td>
<td align="right" class="listwhitetext">Acceptance Rcvd</td>
<c:if test="${not empty claim.acceptanceRcvd}">
<s:text id="claimacceptanceRcvdFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.acceptanceRcvd"/></s:text>
<td><s:textfield cssClass="input-text" id="acceptanceRcvd" name="claim.acceptanceRcvd" value="%{claimacceptanceRcvdFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%><img id="acceptanceRcvd_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.acceptanceRcvd}">
<td><s:textfield cssClass="input-text" id="acceptanceRcvd" name="claim.acceptanceRcvd" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="acceptanceRcvd_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if><td width="0px"></td>
<td align="right" class="listwhitetext" >Cheque&nbsp;To&nbsp;Vendor</td>
<c:if test="${not empty claim.toVendor}">
<s:text id="claimToVendorFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.toVendor"/></s:text>
<td><s:textfield cssClass="input-text" id="toVendor" name="claim.toVendor" value="%{claimToVendorFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="toVendor_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.toVendor}">
<td><s:textfield cssClass="input-text" id="toVendor" name="claim.toVendor" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="toVendor_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<td align="right" class="listwhitetext"></td>
<td align="right" class="listwhitetext">Submitted</td>
<c:if test="${not empty claim.submitDate}">
<s:text id="claimSubmitDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.submitDate"/></s:text>
<td width="40px"><s:textfield cssClass="input-text" id="submitDate" name="claim.submitDate" value="%{claimSubmitDateFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%><img id="submitDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.submitDate}">
<td width="40px"><s:textfield cssClass="input-text" id="submitDate" name="claim.submitDate" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="submitDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>

<td align="right" class="listwhitetext"><fmt:message key="claim.closeDate"/></td>
<c:if test="${not empty claim.closeDate}">
<s:text id="claimCloseDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.closeDate"/></s:text>
<td width="40px"><s:textfield cssClass="input-text" id="closeDate" name="claim.closeDate" value="%{claimCloseDateFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)"  onselect="calcDays()"/></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%><img id="closeDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.closeDate}">
<td><s:textfield cssClass="input-text" id="closeDate" name="claim.closeDate" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)"  onselect="calcDays()"/></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%><img id="closeDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>


</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claim.notifyShipper"/></td>
<c:if test="${not empty claim.notifyShipper}">
<s:text id="claimNotifyShipperFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.notifyShipper"/></s:text>
<td><s:textfield cssClass="input-text" id="notifyShipper" name="claim.notifyShipper" value="%{claimNotifyShipperFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="notifyShipper_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.notifyShipper}">                                                  
<td><s:textfield cssClass="input-text" id="notifyShipper" name="claim.notifyShipper" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%><img id="notifyShipper_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<td width=""></td>
<td align="right" class="listwhitetext" >Settlement Form</td>
<c:if test="${not empty claim.settlementForm}">
<s:text id="claimsettlementFormFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.settlementForm"/></s:text>
<td><s:textfield cssClass="input-text" id="settlementForm" name="claim.settlementForm" value="%{claimsettlementFormFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%><img id="settlementForm_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.settlementForm}">
<td><s:textfield cssClass="input-text" id="settlementForm" name="claim.settlementForm" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%><img id="settlementForm_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<td width=""></td>
<td align="right" class="listwhitetext" >Payment By Supplier 1</td>
<c:if test="${not empty claim.supplier1pay}">
<s:text id="claimSupplier1PayFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.supplier1pay"/></s:text>
<td><s:textfield cssClass="input-text" id="supplier1pay" name="claim.supplier1pay" value="%{claimSupplier1PayFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="supplier1pay_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.supplier1pay}">
<td><s:textfield cssClass="input-text" id="supplier1pay" name="claim.supplier1pay" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%><img id="supplier1pay_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<td width=""></td>
<td align="right" class="listwhitetext"><fmt:message key="claim.cancelled"/></td>
<c:if test="${not empty claim.cancelled}">
<s:text id="claimCancelledFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.cancelled"/></s:text>
<td><s:textfield cssClass="input-text" id="cancelled" name="claim.cancelled" value="%{claimCancelledFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%><img id="cancelled_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.cancelled}">
<td><s:textfield cssClass="input-text" id="cancelled" name="claim.cancelled" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="cancelled_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>

<td align="right" class="listwhitetext" width="120">Customer Delivery Date</td>
<c:if test="${not empty claim.confirmedDeliveryDateByCustomer}">
<s:text id="claimConfirmedDeliveryDateByCustomerFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.confirmedDeliveryDateByCustomer"/></s:text>
<td><s:textfield cssClass="input-text" id="confirmedDeliveryDateByCustomer" name="claim.confirmedDeliveryDateByCustomer" value="%{claimConfirmedDeliveryDateByCustomerFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="confirmedDeliveryDateByCustomer_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.confirmedDeliveryDateByCustomer}">
<td><s:textfield cssClass="input-text" id="confirmedDeliveryDateByCustomer" name="claim.confirmedDeliveryDateByCustomer" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="confirmedDeliveryDateByCustomer_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>

</tr>
<tr>
<td align="right" class="listwhitetext">Doc's&nbsp;Completed</td>
<c:if test="${not empty claim.docCompleted}">
<s:text id="claimDocCompletedByCustomerFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.docCompleted"/></s:text>
<td><s:textfield cssClass="input-text" id="docCompletedByCustomer" name="claim.docCompleted" value="%{claimDocCompletedByCustomerFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="docCompletedByCustomer_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.docCompleted}">
<td><s:textfield cssClass="input-text" id="docCompletedByCustomer" name="claim.docCompleted" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="docCompletedByCustomer_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<td align="right" class="listwhitetext"></td>
<td align="right" class="listwhitetext" width="100px"><fmt:message key="claim.insurerNotification"/></td>
<c:if test="${not empty claim.insurerNotification}">
<s:text id="claimInsurerNotificationFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.insurerNotification"/></s:text>
<td width="40px"><s:textfield cssClass="input-text" id="insurerNotification" name="claim.insurerNotification" value="%{claimInsurerNotificationFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td>
<% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="insurerNotification_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %>
</td>
</c:if>
<c:if test="${empty claim.insurerNotification}">
<td width="40px"><s:textfield cssClass="input-text" id="insurerNotification" name="claim.insurerNotification" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="insurerNotification_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<td align="right" class="listwhitetext"></td>
<td align="right" class="listwhitetext" width="115px">Payment By Supplier 2</td>
<c:if test="${not empty claim.supplier2pay}">
<s:text id="claimSupplier2PayFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.supplier2pay"/></s:text>
<td width="40px"><s:textfield cssClass="input-text" id="supplier2pay" name="claim.supplier2pay" value="%{claimSupplier2PayFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td>
<% if (sectionClaimStatus.equalsIgnoreCase("false")){%><img id="supplier2pay_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %>
</td>
</c:if>
<c:if test="${empty claim.supplier2pay}">
<td width="40px"><s:textfield cssClass="input-text" id="supplier2pay" name="claim.supplier2pay" required="true" cssStyle="width:60px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="supplier2pay_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<td></td>
<td align="right" class="listwhitetext" id="claimBy1">Claim By</td>
<td align="right"><s:textfield cssClass="input-text" id="claimBy" name="claim.claimBy" readonly="true" cssStyle="width:60px" /></td>
<td align="right" class="listwhitetext"></td>
<td align="right" class="listwhitetext" id="noOfDaysForSettelment1">No.of&nbsp;days&nbsp;for&nbsp;settelment</td>
<td align=""><s:textfield cssClass="input-text" id="noOfDaysForSettelment" name="claim.noOfDaysForSettelment" onselect="calcDays();" onchange="calcDays();" readonly="true" cssStyle="width:60px" maxlength="11" /></td>

</tr>

</tbody>
</table>
</td>
</tr>

<tr><td height="5"></td></tr>
<tr><td align="center" colspan="15" class="vertlinedata"></td></tr>
</tr>
<tr><td colspan="11"  width="">
<table class="detailTabLabel" border="0" cellpadding="3" cellspacing="0" >
<tbody>
<tr><td height="5"></td></tr>
<tr>
<td align="right" class="listwhitetext" width="96px"> <fmt:message key="claim.requestReimbursement"/></td>
<td align="left"><s:select cssClass="list-menu" cssStyle="width:65px" name="claim.reimbursementCurrency"  required="true"  list="%{currency}"    headerKey="" headerValue=""/></td>
<td align="right" class="listwhitetext"><fmt:message key="claim.requestReimbursementAmt"/></td>
<td align="left"><s:textfield cssClass="input-text" name="claim.reimbursement" cssStyle="width:65px" maxlength="7" required="true"  readonly="<%=sectionClaimStatus%>" onchange="onlyFloat(this);" /></td>

<td align="right" class="listwhitetext" nowrap="nowrap">Req Reimburse Date</td>
<c:if test="${not empty claim.requestReimbursement}">
<s:text id="claimRequestReimbursementFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.requestReimbursement"/></s:text>
<td width="100px"><s:textfield cssClass="input-text" id="requestReimbursement" name="claim.requestReimbursement" value="%{claimRequestReimbursementFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)" /><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="requestReimbursement_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.requestReimbursement}">
<td width="100px"><s:textfield cssClass="input-text" id="requestReimbursement" name="claim.requestReimbursement" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)" /><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="requestReimbursement_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>

<configByCorp:fieldVisibility componentId="component.claim.uvlSetelement.edit"> 
<td align="right" class="listwhitetext"><fmt:message key="claim.uvlAmount"/>&nbsp;${currencySign }</td>
<td align="left"><s:textfield cssClass="input-text" name="claim.uvlAmount" cssStyle="width:65px" maxlength="7" readonly="<%=sectionClaimStatus%>" onchange="onlyFloat(this);" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.claim.unigroupclaim.edit"> 
<td></td><td align="right" class="listwhitetext"><fmt:message key="claim.origClmsAmt"/>&nbsp;${currencySign }</td>
<td align="left"><s:textfield cssClass="input-text" name="claim.origClmsAmt" cssStyle="text-align:right" size ="7" maxlength="7" onchange="onlyFloat(this);" /></td>
</configByCorp:fieldVisibility>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claim.reciveReimbursement1"/></td>
<td align="left"><s:select cssClass="list-menu" cssStyle="width:65px" name="claim.reciveReimbursementCurrency"  required="true"  list="%{currency}"    headerKey="" headerValue=""/></td>

<td align="right" class="listwhitetext"><fmt:message key="claim.reciveReimbursement1Amt"/></td>
<td align="left"><s:textfield cssClass="input-text" name="claim.reciveReimbursement" cssStyle="width:65px" maxlength="7" readonly="<%=sectionClaimStatus%>" onchange="onlyFloat(this);" /></td>


<td align="right" class="listwhitetext" nowrap="nowrap">Rcvd Reimburse Date</td>
<c:if test="${not empty claim.insurerAcknowledgement}">
<s:text id="claimInsurerAcknowledgementFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.insurerAcknowledgement"/></s:text>
<td width="100px"><s:textfield cssClass="input-text" id="insurerAcknowledgement" name="claim.insurerAcknowledgement" value="%{claimInsurerAcknowledgementFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)"  /><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="insurerAcknowledgement_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.insurerAcknowledgement}">
<td width="100px"><s:textfield cssClass="input-text" id="insurerAcknowledgement" name="claim.insurerAcknowledgement" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)" /><% if (sectionClaimStatus.equalsIgnoreCase("false")){%> <img id="insurerAcknowledgement_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<configByCorp:fieldVisibility componentId="component.claim.uvlSetelement.edit"> 
<td align="right" class="listwhitetext">UVL/MVL Settlement Rcvd Date</td>
<c:if test="${not empty claim.uvlRecived}">
<s:text id="claimUvlRecivedFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.uvlRecived"/></s:text>
<td><s:textfield cssClass="input-text" id="uvlRecived" name="claim.uvlRecived" value="%{claimUvlRecivedFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%>  <img id="uvlRecived_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
<c:if test="${empty claim.uvlRecived}">
<td><s:textfield cssClass="input-text" id="uvlRecived" name="claim.uvlRecived" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)" /></td><td><% if (sectionClaimStatus.equalsIgnoreCase("false")){%><img id="uvlRecived_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/><%} %></td>
</c:if>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.claim.unigroupclaim.edit"> 
<td align="right" class="listwhitetext"><fmt:message key="claim.pmntAmt"/>&nbsp;${currencySign }</td>
<td align="left"><s:textfield cssClass="input-text" name="claim.pmntAmt" cssStyle="text-align:right" size ="7" maxlength="7" onchange="onlyFloat(this);" /></td>
</configByCorp:fieldVisibility>
<tr>
<td align="right" class="listwhitetext">Goodwill&nbsp;Currency</td>
<td align="left"><s:select cssClass="list-menu" cssStyle="width:65px" name="claim.goodWillCurrency" required="true"  list="%{currency}" headerKey="" headerValue=""/></td>

<td align="right" class="listwhitetext">Goodwill&nbsp;Payment</td>
<td align="left"><s:textfield cssClass="input-text" name="claim.goodWillPayment" cssStyle="text-align:right;width:61px" maxlength="7" onchange="onlyFloat(this);"/></td>

</tr>
</tr>
</tbody>
</table>
</td>
</tr>

<c:if test="${propertyAmount=='Y'}">
<tr><td height="5"></td></tr>
<tr><td align="center" colspan="15" class="vertlinedata"></td></tr>
</tr>
<tr><td colspan="11"  width="">
<table style="margin:0px;" border="0" cellpadding="3" cellspacing="0" >
<tbody>
<tr><td height="1"></td></tr>
<tr>
<td align="right" class="listwhitetext" width="123px">Property Damage Amt</td>
<td align="left"><s:textfield cssClass="input-text" name="claim.propertyDamageAmount" cssStyle="width:65px" maxlength="7" onchange="onlyFloat(this);calculateTotalPaidToCustomer('claim.propertyDamageAmount');" /></td>
<td align="right" class="listwhitetext">Customer SVC Amt</td>
<td align="left"><s:textfield cssClass="input-text" name="claim.customerSvcAmount" cssStyle="width:65px" maxlength="7" onchange="onlyFloat(this);calculateTotalPaidToCustomer('claim.customerSvcAmount');" /></td>

</tr>
<tr>
<td align="right" class="listwhitetext">Lost/Damaged Items Amt</td>
<td align="left"><s:textfield cssClass="input-text" name="claim.lostDamageAmount" cssStyle="width:65px" maxlength="7" onchange="onlyFloat(this);calculateTotalPaidToCustomer('claim.lostDamageAmount');"/></td>

<td align="left" class="listwhitetext">Total Paid to the Customer</td>
<td align="left"><s:textfield cssClass="input-textUpper" name="claim.totalPaidToCustomerAmount" cssStyle="width:65px" maxlength="7" readonly="true" /></td>

</tr>
</tbody>
</table>
</td>
</tr>
</c:if>
<c:if test="${propertyAmount=='N'}">
<s:hidden name="claim.propertyDamageAmount"/>
<s:hidden name="claim.customerSvcAmount"/>
<s:hidden name="claim.lostDamageAmount"/>
<s:hidden name="claim.totalPaidToCustomerAmount"/>
</c:if>
</tbody>
</table>
</div>

</td>
</tr>


</tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<sec-auth:authComponent componentId="module.claimForm.section.claimStatus.edit">
	<%
	String sectionClaimStatus1="true";
	int permission1  = (Integer)request.getAttribute("module.claimForm.section.claimStatus.edit" + "Permission");
 	if (permission1 > 2 ){
 		sectionClaimStatus1 = "false";
 		System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionClaimStatus1);
 	}
 	System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionClaimStatus1);
  %>

<tr>
	<td height="0" width="100%" align="left" >
		
		<div  onClick="javascript:animatedcollapse.toggle('valuation')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Valuation
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
		
										
  		<div id="valuation">
		<table class="detailTabLabel" border="0" width="100%">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
	
<tr>                
<td align="right" class="listwhitetext" width="125"><fmt:message key="claim.insurerCode"/></td>
<td align="left" width="165"><s:textfield cssClass="input-textUpper" id="claimInsurerCodeId" name="claim.insurerCode" cssStyle="width:135px" maxlength="8" required="true" readonly="<%=sectionClaimStatus1%>" onchange = "valid(this,'special');checkVendorName();" />
<% if (sectionClaimStatus1.equalsIgnoreCase("false")){%>
<img class="openpopup" width="17" height="20" align="top" onclick="javascript:openWindow('venderPartners.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=claim.insurer&fld_code=claim.insurerCode');" src="<c:url value='/images/open-popup.gif'/>" />
<%} %>
</td>

<td align="left" colspan="4"><s:textfield cssClass="input-text" id="claimInsurerId" name="claim.insurer"  onkeyup="findPartnerDetails('claimInsurerId','claimInsurerCodeId','claimInsurerDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true)','',event);" cssStyle="width:290px" maxlength="55" required="true"  />
<div id="claimInsurerDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
</td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claim.claimCertificateNumber"/></td>
<td align="left"><s:textfield cssClass="input-text" name="claim.claimCertificateNumber" cssStyle="width:134px" maxlength="15" readonly="<%=sectionClaimStatus1%>" required="true" /></td>
<td align="right" class="listwhitetext" width="152"><fmt:message key="claim.valuedFor1"/></td>
<td align="left" width="138"><s:textfield cssClass="input-textUpper" name="billing.insuranceValueActual" cssStyle="width:135px" maxlength="7" readonly="true" /></td>
<td align="right" width="160" class="listwhitetext"><fmt:message key="claim.valuedForCurrency"/></td>
<td align="left"><s:select cssClass="list-menu"  name="claimCurrency" list="%{currency}" cssStyle="width:100px" headerKey="" headerValue="" disabled="true" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claim.options"/></td>
<td align="left"><s:textfield cssClass="input-textUpper" name="billing.insuranceOption" cssStyle="width:135px" maxlength="65" required="true" readonly="true"  /></td>
<td align="right" class="listwhitetext">Deductible&nbsp;Amt.</td>
<td align="left" style=""><s:select cssClass="list-menu" name="claim.deductible" list="%{claimDeductible}" headerKey="" headerValue="" cssStyle="width:137px" /></td>
<td align="right" class="listwhitetext">Deductible&nbsp;Amt.&nbsp;Currency</td>
<td align="left" style=""><s:select cssClass="list-menu" name="claim.deductibleAmtCurrency" list="%{currency}" headerKey="" headerValue="" cssStyle="width:100px" /></td>

<td></td><td></td>
</tr>


<tr><td align="right" class="listwhitetext">Declared&nbsp;Value&nbsp;Protection</td>
<td align="left"><s:textfield cssClass="input-textUpper" name="claim.fullValueProtection" cssStyle="width:135px" maxlength="30" required="true"  readonly="true"  /></td>
<td align="right" class="listwhitetext">Deductible&nbsp;Insurer</td>
<td align="left" style=""><s:select cssClass="list-menu" name="claim.deductibleInsurer" list="%{DeductibleInsurer}" headerKey="" headerValue="" cssStyle="width:137px" /></td>
<td align="right" class="listwhitetext">Deductible&nbsp;Insurer&nbsp;Currency</td>
<td align="left" style=""><s:select cssClass="list-menu" name="claim.deductibleInsurerCurrency" list="%{currency}" headerKey="" headerValue="" cssStyle="width:100px" /></td>

</tr>
<tr>
<td align="right" class="listwhitetext" id="hidePremiumLabel"><fmt:message key="claim.premium"/></td>
<td align="left" id="hidePremiumField"><s:textfield cssClass="input-text" name="claim.premium" cssStyle="width:135px" maxlength="7" readonly="<%=sectionClaimStatus1%>" onchange="onlyFloat(this);" /></td>
<td align="right" class="listwhitetext"><fmt:message key="claim.crossReference"/></td>
<td align="left"><s:textfield cssClass="input-text" name="claim.crossReference" cssStyle="width:134px" maxlength="20" required="true"  readonly="<%=sectionClaimStatus1%>"/></td>
<td align="right" class="listwhitetext"><fmt:message key="claim.coordinator"/></td>
<td align="left"><s:textfield cssClass="input-textUpper" name="serviceOrder.coordinator" cssStyle="width:98px"  maxlength="15" required="true"  readonly="true" /></td>

</tr>
</tbody>
</table>
</div></td>
</tr>
</table>



</td>
</tr>


<tr>
						
						<td height="10" width="100%" align="left" style="margin: 0px">						
     				<div onClick="javascript:animatedcollapse.toggle('claimRamarks')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;Claim Remarks
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
					  </div>
							<div id="claimRamarks" class="switchgroup1">
							
	<table class="detailTabLabel" border="0">
	<tr><td align="left" height="1px"></td></tr>
<tr>
	<td width="124" align="right" valign="absmiddle" class="listwhitetext">Remarks</td>
		<s:hidden name="claim.remarks"/>
	<td align="left" colspan="3" width="180"><s:textarea  cssStyle="width:725px;height:75px;"  cssClass="input-textarea" name="claim.remarks" disabled="true" /></td>
</tr>
<tr>


<c:set var="isSignatureFlag" value="false"/>
													<c:if test="${claim.signature}">
														<c:set var="isSignatureFlag" value="true"/>
													</c:if>
													<td width="102" align="right" class="listwhitetext" >Signature</td>
    <td width="16"><s:checkbox key="claim.signature" value="${isSignatureFlag}" fieldValue="true" disabled="true"  /></td>
<s:hidden name="claim.signature"/>
<td align="right" class="listwhitetext">Signature Date</td>
<c:if test="${not empty claim.signatutreDate}">
<s:text id="claimSignatutreDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.signatutreDate"/></s:text>
<td><s:textfield  id="signatutreDate" name="claim.signatutreDate" value="%{claimSignatutreDateFormattedValue}" required="true" cssStyle="width:65px; border:1px solid #CCCCCC;" maxlength="11" readonly="true" /></td>
</c:if>
<c:if test="${empty claim.signatutreDate}">
<td><s:textfield  id="signatutreDate" name="claim.signatutreDate" required="true" cssStyle="width:65px; border:1px solid #CCCCCC;" maxlength="11" readonly="true" /></td>
</c:if>
								   
  </tr>
  
  </sec-auth:authComponent>
  </table>
</div>
<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >
		
		<div  onClick="javascript:animatedcollapse.toggle('agentRemediation')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Agent&nbsp;Remediation
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
		
										
<div id="agentRemediation">
<table class="detailTabLabel" border="0" width="100%">
<tbody>
<tr><td align="left" height="5px"></td></tr>
	
<tr>                
<td align="right" class="listwhitetext" width="125"  valign="top">Location&nbsp;/leg</td>
<td align="left" width="165"  valign="top">
<c:choose>
<c:when test="${(!roleEmployee) || responsibleAgent}">
<s:select cssClass="list-menu" name="claim.location" list="%{location}" headerKey="" headerValue="" cssStyle="width:100px" />
</c:when>
<c:otherwise>
<select id="locationVal"  style="width:100px" class="list-menu pr-f11" disabled="true"> 
               <option value=""></option>
                <c:forEach var="chrms" items="${location}" varStatus="loopStatus">
                <c:choose>
                	<c:when test="${chrms.key == claim.location}">
                		<c:set var="selectedInd" value=" selected"></c:set>
                	</c:when>
                	<c:otherwise>
                		<c:set var="selectedInd" value=""></c:set>
                	</c:otherwise>
                </c:choose>
                <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                	<c:out value="${chrms.value}"></c:out>
                </option>
                </c:forEach> 
</select>
<s:hidden cssClass="input-textUpper" name="claim.location" cssStyle="width:96px"  />
</c:otherwise>
</c:choose>
</td>
<td align="right" class="listwhitetext" width="125" rowspan="4" valign="center">Action&nbsp;to&nbsp;prevent&nbsp;recurrence</td>
<td align="left" rowspan="4" width="165">
	<s:textarea name="claim.actiontoPreventRecurrence" id="actiontoPreventRecurrence" required="true" cssClass="textarea" cssStyle="width:330px;height:80px;" readonly="${editable}"/>
</td>
</tr>
<tr>
<td align="right" class="listwhitetext">Factor</td>
<td align="left">
<c:choose>
<c:when test="${(!roleEmployee) || responsibleAgent}">
<s:select cssClass="list-menu" name="claim.factor" list="%{factor}" headerKey="" headerValue="" cssStyle="width:100px" />
</c:when>
<c:otherwise>
<select id="factorVal"  style="width:100px" class="list-menu pr-f11" disabled="true"> 
               <option value=""></option>
                <c:forEach var="chrms" items="${factor}" varStatus="loopStatus">
                <c:choose>
                	<c:when test="${chrms.key == claim.factor}">
                		<c:set var="selectedInd" value=" selected"></c:set>
                	</c:when>
                	<c:otherwise>
                		<c:set var="selectedInd" value=""></c:set>
                	</c:otherwise>
                </c:choose>
                <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                	<c:out value="${chrms.value}"></c:out>
                </option>
                </c:forEach> 
</select>
<s:hidden cssClass="input-textUpper" name="claim.factor" cssStyle="width:96px"  />
</c:otherwise>
</c:choose>
</td>
</tr>
<tr>
<td align="right" class="listwhitetext">Occurrence</td>
<td align="left">
<c:choose>
<c:when test="${(!roleEmployee) || responsibleAgent}">
<s:select cssClass="list-menu" name="claim.occurence" list="%{occurence}" headerKey="" headerValue="" cssStyle="width:100px" />
</c:when>
<c:otherwise>
<select id="occurenceVal"  style="width:100px" class="list-menu pr-f11" disabled="true"> 
               <option value=""></option>
                <c:forEach var="chrms" items="${occurence}" varStatus="loopStatus">
                <c:choose>
                	<c:when test="${chrms.key == claim.occurence}">
                		<c:set var="selectedInd" value=" selected"></c:set>
                	</c:when>
                	<c:otherwise>
                		<c:set var="selectedInd" value=""></c:set>
                	</c:otherwise>
                </c:choose>
                <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                	<c:out value="${chrms.value}"></c:out>
                </option>
                </c:forEach> 
</select>
<s:hidden cssClass="input-textUpper" name="claim.occurence" cssStyle="width:96px"  />
</c:otherwise>
</c:choose>
</td>
<td></td><td></td>
</tr>


<tr>
<td align="right" class="listwhitetext">Avoidable</td>
<td align="left">
<c:choose>
<c:when test="${(!roleEmployee) || responsibleAgent}">
<s:select cssClass="list-menu" name="claim.avoidable" list="%{claimVal}" headerKey="" headerValue="" cssStyle="width:100px" />
</c:when>
<c:otherwise>
<select id="avoidableVal"  style="width:100px" class="list-menu pr-f11" disabled="true"> 
               <option value=""></option>
                <c:forEach var="chrms" items="${claimVal}" varStatus="loopStatus">
                <c:choose>
                	<c:when test="${chrms.key == claim.avoidable}">
                		<c:set var="selectedInd" value=" selected"></c:set>
                	</c:when>
                	<c:otherwise>
                		<c:set var="selectedInd" value=""></c:set>
                	</c:otherwise>
                </c:choose>
                <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                	<c:out value="${chrms.value}"></c:out>
                </option>
                </c:forEach> 
</select>
<s:hidden cssClass="input-textUpper" name="claim.avoidable" cssStyle="width:96px"  />
</c:otherwise>
</c:choose>
</td>
</tr>
<tr>
<td align="right" class="listwhitetext">Culpable</td>
<td align="left">
<c:choose>
<c:when test="${(!roleEmployee) || responsibleAgent}">
<s:select cssClass="list-menu" name="claim.culpable" list="%{claimVal}" headerKey="" headerValue="" cssStyle="width:100px" />
</c:when>
<c:otherwise>
<select id="culpableVal"  style="width:100px" class="list-menu pr-f11" disabled="true"> 
               <option value=""></option>
                <c:forEach var="chrms" items="${claimVal}" varStatus="loopStatus">
                <c:choose>
                <c:when test="${chrms.key == claim.culpable}">
                <c:set var="selectedInd" value=" selected"></c:set>
                </c:when>
                <c:otherwise>
                <c:set var="selectedInd" value=""></c:set>
                </c:otherwise>
                </c:choose>
                <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                <c:out value="${chrms.value}"></c:out>
                </option>
                </c:forEach> 
                </select>
	<s:hidden cssClass="input-textUpper" name="claim.culpable" cssStyle="width:96px"  />
</c:otherwise>
</c:choose>
</td>
<td align="right" class="listwhitetext" width="125" rowspan="4" valign="center">Remediation&nbsp;Comment</td>
<td align="left" rowspan="4" width="165">
<s:textarea name="claim.remediationComment" id="remediationComment" required="true" cssClass="textarea" cssStyle="width:330px;height:80px;" readonly="${editable}"/>
</td>
</tr>
<tr>
<td align="right" style="vertical-align:top;padding-top:5px;" class="listwhitetext">Remediation&nbsp;Closed</td>
<td align="left" valign="top">
<c:if test="${not empty claim.remediationClosingDate}">
<c:if test="${!roleEmployee}">
<s:text id="remediationClosingDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.remediationClosingDate"/></s:text>
<s:textfield cssClass="input-text" id="remediationClosingDate" name="claim.remediationClosingDate" value="%{remediationClosingDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)"  onselect="calcDays()"/>
<img id="remediationClosingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
</c:if>
<c:if test="${roleEmployee}">
<s:text id="remediationClosingDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="claim.remediationClosingDate"/></s:text>
<s:textfield cssClass="input-text" id="remediationClosingDate"  value="%{remediationClosingDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)"  onselect="calcDays()"/>
<s:hidden  name="claim.remediationClosingDate" value="%{remediationClosingDateFormattedValue}" />
<img id="remediationClosingDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date.')"/> 
</c:if>
</c:if>
<c:if test="${empty claim.remediationClosingDate}">
<c:if test="${!roleEmployee}">
<s:textfield cssClass="input-text" id="remediationClosingDate" name="claim.remediationClosingDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)"  onselect="calcDays()"/>
<img id="remediationClosingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
</c:if>

<c:if test="${roleEmployee}">
<s:textfield cssClass="input-text" id="remediationClosingDate" name="claim.remediationClosingDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)"  onselect="calcDays()"/>
<img id="remediationClosingDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date.')"/>
</c:if>
</c:if>
</td>

</tr>

<tr><td align="left" height="5px"></td></tr>
</tbody>
</table>
</div>
</td>
</tr>


</table>						
		</td>
						</tr>
						

<sec-auth:authComponent componentId="module.tab.claims.customerFileTab">
<tr>
						
						<td height="10" width="100%" align="left" style="margin:0px;">
						<div id="hid">
     				<div onClick="javascript:animatedcollapse.toggle('cportal')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;Customer Portal Detail
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_special">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
					  </div>
							<div id="cportal" class="switchgroup1">
							
						<table cellpadding="1" cellspacing="0" class="detailTabLabel">
<tr>
													<td width="119" align="right" valign="absmiddle" class="listwhitetext"><fmt:message key='customerFile.customerPortalId'/></td>
													
												  <td align="left" width="180"><s:textfield cssClass="input-textUpper" name="customerFile.customerPortalId" required="true" size="15" maxlength="80" readonly="true" /></td>

													<td width="40" align="right"  class="listwhitetext" >Email:&nbsp;</td>
    <td width="50" ><s:textfield cssClass="input-textUpper" name="customerFile.email"  readonly = "true" size="30" maxlength="65" /></td>
	
								   
  </tr></table>

										</div>
						</div>
						</td>
						</tr>


 </sec-auth:authComponent>

</table>
	</div>
<div class="bottom-header" style="margin-top:40px;"><span></span></div>
</div>
</div> 

				<!--<table style="width:735px;" >
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='claim.createdOn'/></b></td>
							<s:hidden name="claim.createdOn" />
							<td style="width:120px"><fmt:formatDate value="${claim.createdOn}" pattern="${displayDateTimeFormat}"/>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='claim.createdBy' /></b></td>
							<c:if test="${not empty claim.id}">
								<s:hidden name="claim.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{claim.createdBy}"/></td>
							</c:if>
							<c:if test="${empty claim.id}">
								<s:hidden name="claim.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='claim.updatedOn'/></b></td>
							<s:hidden name="claim.updatedOn"/>
							<td style="width:120px"><fmt:formatDate value="${claim.updatedOn}" pattern="${displayDateTimeFormat}"/>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='claim.updatedBy' /></b></td>
						
						<c:if test="${not empty claim.id}">
								<s:hidden name="claim.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{claim.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty claim.id}">
								<s:hidden name="claim.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>

-->

	<table border="0">
					<tbody>
						
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='claim.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="claimCreatedOnFormattedValue" value="${claim.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="claim.createdOn" value="${claimCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${claim.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='claim.createdBy' /></b></td>
							<c:if test="${not empty claim.id}">
								<s:hidden name="claim.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{claim.createdBy}"/></td>
							</c:if>
							<c:if test="${empty claim.id}">
								<s:hidden name="claim.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='claim.updatedOn'/></b></td>
							<fmt:formatDate var="claimupdatedOnFormattedValue" value="${claim.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="claim.updatedOn" value="${claimupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${claim.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='claim.updatedBy' /></b></td>
							<c:if test="${not empty claim.id}">
								<s:hidden name="claim.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{claim.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty claim.id}">
								<s:hidden name="claim.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>

</tbody></table>

</div>
 <s:submit cssClass="cssbutton" method="save" id="saveButton" key="button.save" cssStyle="width:60px;" />
     <c:if test="${not empty claim.id}">
      <c:set var="ServiceOrderID" value="${serviceOrder.id}" scope="session"/>    
     <input type="button" class="cssbutton" id="addButton" onclick="location.href='<c:url value="/editClaim.html?sid=${ServiceOrderID}"/>'"  
        value="<fmt:message key="button.add"/>" style="width:60px;" />  
	</c:if>        
<input class="cssbutton1" id="resetButton" type="button" value="Reset" style="width:60px;"  onmousemove="checkDate();"  onclick="this.form.reset();autoPopulateClaimCurrency(); setInsuranceUser();newFunctionForCountryState();"/>

<s:hidden name="claim.sequenceNumber" value="%{serviceOrder.sequenceNumber}" />  
<s:hidden name="claim.registrationNumber" value="%{serviceOrder.registrationNumber}" />
<s:hidden name="claim.firstName" value="%{serviceOrder.firstName}" />
<s:hidden name="claim.lastName" value="%{serviceOrder.lastName}" />
<s:hidden name="claim.cityCode" value="%{claim.cityCode}" />
<s:hidden name="claim.countryCode" value="%{claim.countryCode}" />
<s:hidden name="claim.moveCloudStatus"/>
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<c:set var="idOfWhom" value="${claim.id}" scope="session"/>
<c:set var="noteID" value="${claim.claimNumber}" scope="session"/>
<c:set var="noteFor" value="Claim" scope="session"/>
<c:set var="idOfTasks" value="${claim.id}" scope="session"/>
<c:set var="tableName" value="claim" scope="session"/>
 <c:set var="tasksSoId" value="${serviceOrder.id}" scope="session"/>
<c:if test="${empty claim.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty claim.id}">
<c:if test="${userType!='AGENT'}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<c:if test="${userType=='AGENT'}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
</c:if>
</s:form>


<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
<script type="text/javascript">
	animatedcollapse.addDiv('valuation', 'fade=1,hide=1')
	animatedcollapse.addDiv('claimstatus', 'fade=1,hide=1')
	animatedcollapse.addDiv('cportal', 'fade=1,hide=1')
	animatedcollapse.addDiv('claimRamarks', 'fade=1,hide=1')
	animatedcollapse.addDiv('agentRemediation', 'fade=1,hide=1')
	animatedcollapse.init()
</script>
<%-- Script Added By Kunal For Ticket Number: 6053 --%>
<script type="text/javascript">
	function changeInsurerStatus(targetElement){
		if(targetElement.checked){
			targetElement.value = true;
		}
		else{
			targetElement.value = false;
		}
	}
	function  checkLength(){
		
		var txt = document.forms['claimForm'].elements['claim.additionalbankinformation'].value;
		if(txt.length >1000){
			alert('You can not enter more than 1000 characters.');
			document.forms['claimForm'].elements['claim.additionalbankinformation'].value = txt.substring(0,980);
			return false;
		}
	}
</script>
<script type="text/javascript"> 
function titleCase(element){
	var txt=element.value; var spl=txt.split(" "); 
	var upstring=""; for(var i=0;i<spl.length;i++){ 
	try{ 
	upstring+=spl[i].charAt(0).toUpperCase(); 
	}catch(err){} 
	upstring+=spl[i].substring(1, spl[i].length); 
	upstring+=" ";   
	} 
	element.value=upstring.substring(0,upstring.length-1); 
	 
}
</script>
<%-- Script Closed Here --%>
<script language="JavaScript">
<sec-auth:authComponent componentId="module.script.claim.form.corpAccountScript">
	window.onload = function() {
		trap();
		var elementsLen=document.forms['claimForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['claimForm'].elements[i].type=='text')
					{
						document.forms['claimForm'].elements[i].readOnly =true;
						document.forms['claimForm'].elements[i].className = 'input-textUpper';
					}
					else
					{
						document.forms['claimForm'].elements[i].disabled=true;
					}
			}
	}
</sec-auth:authComponent>
function trap(){
	if(document.images) {
	      var totalImages = document.images.length;
	      	for (var i=0;i<totalImages;i++) {
	    		$('#claimForm').find('img[src="${pageContext.request.contextPath}/images/calender.png"]').attr('src','images/navarrow.gif');
	    		$('#claimForm').find('img[src="${pageContext.request.contextPath}/images/notes_empty1.jpg"]').attr('src','images/navarrow.gif');
	    		$('#claimForm').find('img[src="${pageContext.request.contextPath}/images/notes_open1.jpg"]').attr('src','images/navarrow.gif');
	    		$('#claimForm').find('img[src="/redsky/images/open-popup.gif"]').attr('src','images/navarrow.gif');
	    		$('#claimForm').find('img[src="images/navarrows_03.png"]').attr('src','images/navarrow.gif');
	    		$('#claimForm').find('img[src="images/navarrows_04.png"]').attr('src','images/navarrow.gif');
	    		$('#claimForm').find('img[src="${pageContext.request.contextPath}/images/navarrows_05.png"]').attr('src','images/navarrow.gif');		    		
	    		$('#claimForm').find('img[src="images/navdisable_03.png"]').attr('src','images/navarrow.gif');
	    		$('#claimForm').find('img[src="images/navdisable_04.png"]').attr('src','images/navarrow.gif');
	    		$('#claimForm').find('img[src="images/navdisable_05.png"]').attr('src','images/navarrow.gif');
			} } 
}

function test(){
	return false;
}

function right(e) {
		//var msg = "Sorry, you don't have permission.";
	if (navigator.appName == 'Netscape' && e.which == 1) {
		//alert(msg);
		return false;
	}
	if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
		//alert(msg);
		return false;
	}
	else return true;
}

function onlyTimeFormatAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
}
function notExists(){
	alert("The Claim info has not been saved yet");
}
function autoPopulate_claim_originCountry(targetElement) {
	document.forms['claimForm'].elements['claim.originCountryCode'].value=targetElement.options[targetElement.selectedIndex].value;
	if(targetElement.value == 'USA' || targetElement.value == 'CAN' ){
		document.forms['claimForm'].elements['claim.state'].disabled = false;
	}else{
		document.forms['claimForm'].elements['claim.state'].disabled = true;
		document.forms['claimForm'].elements['claim.state'].value = '';
		autoPopulate_claim_originCityCode(targetElement);
	}
}

var digits = "0123456789";
var phoneNumberDelimiters = "()- ";
var validWorldPhoneChars = phoneNumberDelimiters + "+";
var minDigitsInIPhoneNumber = 10;

function isInteger(s){   
	var i;
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    return true;
}

function stripCharsInBag(s, bag){  
	var i;
    var returnString = "";
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function checkInternationalPhone(strPhone){
	var s=stripCharsInBag(strPhone,validWorldPhoneChars);
	return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}

function validatePhone(targetElelemnt){
	var Phone=targetElelemnt.value;
	if (checkInternationalPhone(Phone)==false){
		alert("Please enter a valid phone number")
		targetElelemnt.value="";
		return false;
	}
	return true;
 }

function checkDate() {
	var i;
	for(i=0;i<=14;i++){
		if(document.forms['claimForm'].elements[i].value == "null" ){
			document.forms['claimForm'].elements[i].value="";
		}
	}		
}

function validate_email(field){
	with (field){
		apos=value.indexOf("@")
		dotpos=value.lastIndexOf(".")
		if (apos<1||dotpos-apos<2) 
		  {alert("Not a valid e-mail address!");return false}
		else {return true}
	}
}
function autoPopulate_customerFile_destinationCountry(targetElement) {  
	var dCountry = targetElement.value;
	var dCountryCode = '';
	if(dCountry == 'United States'){
		dCountryCode = "USA";
	}
	if(dCountry == 'India'){
		dCountryCode = "IND";
	}
	if(dCountry == 'Canada'){
		dCountryCode = "CAN";
	}
	if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
		document.forms['claimForm'].elements['claim.state'].disabled = false;
		
	}else{
		document.forms['claimForm'].elements['claim.state'].disabled = true;
		document.forms['claimForm'].elements['claim.state'].value = '';
		autoPopulate_customerFile_destinationCityCode(dCountryCode,'special');
	}
}

function autoPopulate_customerFile_destinationCountry1(targetElement) {
	if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
		document.forms['claimForm'].elements['claim.state'].disabled = false;
	}else{
		document.forms['claimForm'].elements['claim.state'].disabled = true;
		document.forms['claimForm'].elements['claim.state'].value = '';
	}
	autoPopulate_customerFile_destinationCityCode(targetElement);
}

function checkVendorName(){
    var vendorId = document.forms['claimForm'].elements['claim.insurerCode'].value;
    if(vendorId == ''){
    	document.forms['claimForm'].elements['claim.insurer'].value=""
    }
   if(vendorId != ''){
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
   }  
}

function getState(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
		if(country=="${entry.value}"){
			countryCode="${entry.key}";
		}
	</c:forEach>
	 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse8;
     http3.send(null);
}	
	
function getCountryCode(){
	var countryName=document.forms['claimForm'].elements['claim.country'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseCountryName;
    http4.send(null);
}
  
function handleHttpResponseCountryName(){
   if (http4.readyState == 4){
      var results = http4.responseText
      results = results.trim();
      if(results.length>=1){
		document.forms['claimForm'].elements['claim.countryCode'].value = results; 					
	  }
   }
}
        
function getState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse8;
     http3.send(null);
}

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

function handleHttpResponse2(){
  if (http2.readyState == 4){
     var results = http2.responseText
     results = results.trim();
     var res = results.split("#"); 
     if(res.size() >= 2){ 
       	if(res[2] == 'Approved'){
       		document.forms['claimForm'].elements['claim.insurer'].value = res[1];
       	}else{
       		alert("Vendor code is not approved" ); 
    		document.forms['claimForm'].elements['claim.insurer'].value=""
  		    document.forms['claimForm'].elements['claim.insurerCode'].value="";
  			document.forms['claimForm'].elements['claim.insurerCode'].select();
       	}			
      }else{
         ///   alert("Vendor code not valid");    
   ///document.forms['claimForm'].elements['claim.insurer'].value=""
///document.forms['claimForm'].elements['claim.insurerCode'].value="";
 /// 	document.forms['claimForm'].elements['claim.insurerCode'].select();
 	  }
   }
}

/* Added By Kunal for ticket number: 6006 */
 var http55 = getHTTPObject55();
 var http66 = getHTTPObject66();
 var http8 = getHTTPObject8();
 var httpClaimApp = getHTTPObjectClaimApp();
 function getHTTPObjectClaimApp()
 {
     var xmlhttp;
     if(window.XMLHttpRequest)
     {
         xmlhttp = new XMLHttpRequest();
     }
     else if (window.ActiveXObject)
     {
         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
         if (!xmlhttp)
         {
             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
         }
     }
     return xmlhttp;
 } 
function getHTTPObject55()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
function getHTTPObject66()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  

function getHTTPObject8()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  

/* Method added for claim apps to mailing */
function sendEmailToClaim(){
	var id = document.forms['claimForm'].elements['claim.id'].value;
	var clmCert=document.forms['claimForm'].elements['claim.claimCertificateNumber'].value;
	if(clmCert ==''){
		alert("Please enter value in Certificate.");
	}else{
	var agree= confirm("Press OK to send to claim app, else press Cancel.");	  	   
    if(agree){
    	var url="sendClaimEmailApp.html?id="+id+"&decorator=simple&popup=true";
    	httpClaimApp.open("GET", url, true);
    	httpClaimApp.onreadystatechange=httpClaimMailSender;
    	httpClaimApp.send(null);
   	}else {             
	    }
}
}

/* End of Method */

function httpClaimMailSender(){
	if (httpClaimApp.readyState == 4){
		 var results = httpClaimApp.responseText
         results = results.trim();
       if(results=='Y'){
       alert("Request is scheduled for MoveCloud.");
      
       var id = document.forms['claimForm'].elements['claim.id'].value;
   	   document.forms['claimForm'].action="editClaim.html?id="+id;
   	   document.forms['claimForm'].submit();
       
       }
       showOrHide(0);
    }
	}
/* Code Modified By Kunal for ticket number: 6006 */

function findClaimAccountInfoName(){
	var partnerCode = '${customerFile.billToCode}';
    var url="findClaimAccountInfoName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode);    
    http8.open("GET", url, true);
    http8.onreadystatechange = handleHttpResponse2001;
    http8.send(null);
}
var testAuditorStatus='';
function handleHttpResponse2001(){
	 if (http8.readyState == 4){
         var results = http8.responseText
         results = results.trim();
         results=results.replace("[",''); 
         results = results.replace("]",''); 
			//alert("Response1: " + res[0].length + "Response2: " + res[1].length +"Response3: " + res[2].length +"Response4: " + res[3].length);
			if(results.trim() != null &&  results.trim() !='' && results.trim() != 'undefined'){
				 	var claimPersonList = document.getElementsByName('claim.claimPerson')[0];
				 	for(var i = 0 ; i < claimPersonList.length ; i++){
						if(results.trim() != '' && results.trim() != null && results == document.getElementsByName('claim.claimPerson')[0].options[i].value){
							document.getElementsByName('claim.claimPerson')[0].options[i].selected = "true";
						}
				 	}
			}
	 }
}

/* *************************************************** */
function getClaimPerson(temp){
	if('${claim.claimPerson}' == '' || '${claim.claimPerson}' == null){
			fillResponsiblePerson();
			findClaimAccountInfoName();
		}
		/* var jobType = "${serviceOrder.job}";
     	var url = "findClaimPerson.html?ajax=1&decorator=simple&popup=true&jobType=" + encodeURI(jobType);
     	http55.open("GET", url, true);
     	http55.onreadystatechange = function(){handleHttpResponse55(temp);};
     	http55.send(null); */
}
function handleHttpResponse55(temp){
		/* if (http55.readyState == 4){
				 var results = http55.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.getElementsByName('claim.claimPerson')[0];
               	targetElement.length = res.length;
              /*  	if("${claim.id}" != null ){
 					document.getElementsByName('claim.claimPerson')[0].options[0].text = "${claim.claimPerson}";
					document.getElementsByName('claim.claimPerson')[0].options[0].value = "${claim.claimPerson}";
 				}
 				for(i=1;i<=res.length;i++)
					{
						if(res[i] == ''){
							document.getElementsByName('claim.claimPerson')[0].options[i].text = '';
							document.getElementsByName('claim.claimPerson')[0].options[i].value = '';
						}else{
						stateVal = res[i].split("#");
							document.getElementsByName('claim.claimPerson')[0].options[i].text = stateVal[1];
							document.getElementsByName('claim.claimPerson')[0].options[i].value = stateVal[0];
						}
						if(('${claim.claimPerson}' != '' || '${claim.claimPerson}' != null) && '${claim.claimPerson}'.trim() == document.getElementsByName('claim.claimPerson')[0].options[i].value){
							document.getElementsByName('claim.claimPerson')[0].options[i].selected = "true";
		 				}
					} 
 				
 				if('${claim.claimPerson}' == '' || '${claim.claimPerson}' == null){
 					fillResponsiblePerson();
 					findClaimAccountInfoName();
 				}
          } */
  }

function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
	document.getElementById(autocompleteDivId).style.display = "none";
	if(document.getElementById(paertnerCodeId).value==''){
		document.getElementById(partnerNameId).value="";	
	}
	}

	function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
		lastName=lastName.replace("~","'");
		document.getElementById(partnerNameId).value=lastName;
		document.getElementById(paertnerCodeId).value=partnercode;
		document.getElementById(autocompleteDivId).style.display = "none";	
		if(paertnerCodeId=='claimInsurerCodeId'){
		}
		if(paertnerCodeId=='responsibleAgentCodeId'){
			checkVendorNameNew();
		}
	}
function fillResponsiblePerson(){
		var jobType = "${serviceOrder.job}";
     	var url = "fillClaimResponsiblePerson.html?ajax=1&decorator=simple&popup=true&jobType=" + encodeURI(jobType) + "&cDivision=" + encodeURI('${serviceOrder.companyDivision}');
     	http66.open("GET", url, true);
     	http66.onreadystatechange = handleHttpResponse66;
     	http66.send(null);
}
function handleHttpResponse66(){ 
	   if (http66.readyState == 4){
	                var results = http66.responseText
	                results = results.trim();
	                results=results.replace("[",''); 
	                results = results.replace("]",''); 
	                var res = results.split("#");
					//alert("Response1: " + res[0].length + "Response2: " + res[1].length +"Response3: " + res[2].length +"Response4: " + res[3].length);
					if(res[0]!= null && res[0]!='' && res[0]!= 'undefined'){
						 	var claimPersonList = document.getElementsByName('claim.claimPerson')[0];
						 	for(var i = 0 ; i < claimPersonList.length ; i++){
								if(res[0].trim() == document.getElementsByName('claim.claimPerson')[0].options[i].value){
									document.getElementsByName('claim.claimPerson')[0].options[i].selected = "true";
								}
						 	}
  					}
	        }
	   }
  
/* Changes Closed */       
function handleHttpResponse8()
        {
             if (http3.readyState == 4)
             {
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['claimForm'].elements['claim.state'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['claimForm'].elements['claim.state'].options[i].text = '';
					document.forms['claimForm'].elements['claim.state'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['claimForm'].elements['claim.state'].options[i].text = stateVal[1];
					document.forms['claimForm'].elements['claim.state'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("state").value = '${claim.state}';
             }
        }         
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var http3 = getHTTPObject();
    var http4 = getHTTPObject();
    
	function autoPopulate_customerFile_destinationCityCode(targetElement) {
		if(document.forms['claimForm'].elements['claim.city'].value != ''){
			document.forms['claimForm'].elements['claim.cityCode'].value=document.forms['claimForm'].elements['claim.city'].value+','+document.forms['claimForm'].elements['claim.state'].value;
		}
		if(document.forms['claimForm'].elements['claim.state'].value == ''){
			document.forms['claimForm'].elements['claim.cityCode'].value=document.forms['claimForm'].elements['claim.city'].value;
		}
			}
function autoSaveFunc(clickType){
     progressBarAutoSave('1');
	if ('${autoSavePrompt}' == 'No'){
		var noSaveAction = '<c:out value="${claim.id}"/>'; 
		var id1 =document.forms['claimForm'].elements['serviceOrder.id'].value;
      	var idc = document.forms['claimForm'].elements['claim.id'].value; 		
		if(document.forms['claimForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
 			noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
        }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.billing')
           {
             noSaveAction = 'editBilling.html?id='+id1;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.accounting')
           {
              noSaveAction = 'accountLineList.html?sid='+id1;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.newAccounting')
        {
           noSaveAction = 'pricingList.html?sid='+id1;
        }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.forwarding'){
             <c:if test="${forwardingTabVal!='Y'}">
 	  			noSaveAction = 'containers.html?id='+id1;
 	  		</c:if>
		  	<c:if test="${forwardingTabVal=='Y'}">
		  		noSaveAction = 'containersAjaxList.html?id='+id1;
		  </c:if>
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.OI')
        {
          noSaveAction = 'operationResource.html?id='+id1;
        }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.domestic')
           {
             noSaveAction = 'editMiscellaneous.html?id='+id1;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.status')
           {
            <c:if test="${serviceOrder.job =='RLO'}">
            noSaveAction = 'editDspDetails.html?id='+id1; 
			</c:if>
         <c:if test="${serviceOrder.job !='RLO'}">
         noSaveAction =  'editTrackingStatus.html?id='+id1; 
			</c:if>
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.ticket')
           {
             noSaveAction = 'customerWorkTickets.html?id='+id1;
           }
        ///if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.claims')
          // {
          //   noSaveAction = 'claims.html?id='+id1;
        //   }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.customerfile')
           {
        	var cidVal='${customerFile.id}';
             noSaveAction ='editCustomerFile.html?id='+cidVal;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value == 'gototab.claimso'){
               noSaveAction = 'claims.html?id='+id1;
               }
        if(document.forms['claimForm'].elements['gotoPageString'].value == 'gototab.claimitem'){
            noSaveAction = 'losss.html?id='+idc; 
               }
        processAutoSave(document.forms['claimForm'], noSaveAction, noSaveAction);
	}else{
	var id1 =document.forms['claimForm'].elements['serviceOrder.id'].value;
      	var idc = document.forms['claimForm'].elements['claim.id'].value;
      	var check= document.forms['claimForm'].elements['formStatus'].value;
      	//if (formIsDirty(document.forms['claimForm']))
		if(check=='1'){
				var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='claimDetail.heading'/>");
       			if(agree){
		           if(document.forms['claimForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
 			noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
        }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.billing')
           {
             noSaveAction = 'editBilling.html?id='+id1;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.accounting')
           {
              noSaveAction = 'accountLineList.html?sid='+id1;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.newAccounting')
        {
           noSaveAction = 'pricingList.html?sid='+id1;
        }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.forwarding')
           {
        	<c:if test="${forwardingTabVal!='Y'}">
	  			noSaveAction = 'containers.html?id='+id1;
	  		</c:if>
		  <c:if test="${forwardingTabVal=='Y'}">
		  		noSaveAction = 'containersAjaxList.html?id='+id1;
		  </c:if>
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.OI')
        {
          noSaveAction = 'operationResource.html?id='+id1;
        }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.domestic')
           {
             noSaveAction = 'editMiscellaneous.html?id='+id1;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.status')
           {
            <c:if test="${serviceOrder.job =='RLO'}">
            noSaveAction = 'editDspDetails.html?id='+id1; 
			</c:if>
         <c:if test="${serviceOrder.job !='RLO'}">
         noSaveAction =  'editTrackingStatus.html?id='+id1; 
			</c:if>
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.ticket')
           {
             noSaveAction = 'customerWorkTickets.html?id='+id1;
           }
        ///if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.claims')
          // {
          //   noSaveAction = 'claims.html?id='+id1;
        //   }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.customerfile')
           {
        	var cidVal='${customerFile.id}';
             noSaveAction ='editCustomerFile.html?id='+cidVal;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value == 'gototab.claimso'){
               noSaveAction = 'claims.html?id='+id1;
               }
        if(document.forms['claimForm'].elements['gotoPageString'].value == 'gototab.claimitem'){
             noSaveAction = 'losss.html?id='+idc; 
               }
       				 processAutoSave(document.forms['claimForm'], 'saveClaim.html', noSaveAction);
        		}else
        		{
        		if(document.forms['claimForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
 			noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
        }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.billing')
           {
             noSaveAction = 'editBilling.html?id='+id1;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.accounting')
           {
              noSaveAction = 'accountLineList.html?sid='+id1;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.newAccounting')
        {
           noSaveAction = 'pricingList.html?sid='+id1;
        }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.forwarding')
           {
        	<c:if test="${forwardingTabVal!='Y'}">
	  			noSaveAction = 'containers.html?id='+id1;
	  		</c:if>
		  <c:if test="${forwardingTabVal=='Y'}">
		  		noSaveAction = 'containersAjaxList.html?id='+id1;
		  </c:if>
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.OI')
        {
          noSaveAction = 'operationResource.html?id='+id1;
        }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.domestic')
           {
             noSaveAction = 'editMiscellaneous.html?id='+id1;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.status')
           {
            <c:if test="${serviceOrder.job =='RLO'}">
            noSaveAction = 'editDspDetails.html?id='+id1; 
			</c:if>
         <c:if test="${serviceOrder.job !='RLO'}">
         noSaveAction =  'editTrackingStatus.html?id='+id1; 
			</c:if>
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.ticket')
           {
             noSaveAction = 'customerWorkTickets.html?id='+id1;
           }
        ///if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.claims')
          // {
          //   noSaveAction = 'claims.html?id='+id1;
        //   }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.customerfile')
           {
        	var cidVal='${customerFile.id}';
             noSaveAction ='editCustomerFile.html?id='+cidVal;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value == 'gototab.claimso'){
               noSaveAction = 'claims.html?id='+id1;
               }
               
        if(document.forms['claimForm'].elements['gotoPageString'].value == 'gototab.claimitem'){
             noSaveAction = 'losss.html?id='+idc;  
               }	 processAutoSave(document.forms['claimForm'], noSaveAction, noSaveAction);
        		
        		}
  		}else{
  			if(document.forms['claimForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
 			noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
        }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.billing')
           {
             noSaveAction = 'editBilling.html?id='+id1;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.accounting')
           {
              noSaveAction = 'accountLineList.html?sid='+id1;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.newAccounting')
        {
           noSaveAction = 'pricingList.html?sid='+id1;
        }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.forwarding')
           {
        	<c:if test="${forwardingTabVal!='Y'}">
	  			noSaveAction = 'containers.html?id='+id1;
	  		</c:if>
		  <c:if test="${forwardingTabVal=='Y'}">
		  		noSaveAction = 'containersAjaxList.html?id='+id1;
		  </c:if>
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.OI')
        {
          noSaveAction = 'operationResource.html?id='+id1;
        }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.domestic')
           {
             noSaveAction = 'editMiscellaneous.html?id='+id1;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.status')
           {
            <c:if test="${serviceOrder.job =='RLO'}">
            noSaveAction = 'editDspDetails.html?id='+id1; 
			</c:if>
         <c:if test="${serviceOrder.job !='RLO'}">
         noSaveAction =  'editTrackingStatus.html?id='+id1; 
			</c:if>
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.ticket')
           {
             noSaveAction = 'customerWorkTickets.html?id='+id1;
           }
        ///if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.claims')
          // {
          //   noSaveAction = 'claims.html?id='+id1;
        //   }
        if(document.forms['claimForm'].elements['gotoPageString'].value =='gototab.customerfile')
           {
        	var cidVal='${customerFile.id}';
             noSaveAction ='editCustomerFile.html?id='+cidVal;
           }
        if(document.forms['claimForm'].elements['gotoPageString'].value == 'gototab.claimso'){
               noSaveAction = 'claims.html?id='+id1;
               }
               
        if(document.forms['claimForm'].elements['gotoPageString'].value == 'gototab.claimitem'){
              noSaveAction = 'losss.html?id='+idc;  
               }	processAutoSave(document.forms['claimForm'], noSaveAction, noSaveAction);
  		}
  	}
}			
//For Cporatl through claim access. 
function cportalCheckDetailStauts()
{
      var partnerType=document.getElementById('wonder').checked;      
     var checkCustomerPortalId=document.forms['claimForm'].elements['customerFile.customerPortalId'].value;     
		var el = document.getElementById('hid');
		if(partnerType == true){
		if(checkCustomerPortalId==''){
		alert("Please fill in the Customer Portal ID details in Customer File.");
		document.forms['claimForm'].elements['claim.cportalAccess'].checked=false;
		}else{
		el.style.display = 'block';
		el.style.visibility = 'visible';
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		if(document.forms['claimForm'].elements['claim.requestForm'].value==''){
		document.forms['claimForm'].elements['claim.requestForm'].value=datam;
		}if(document.forms['claimForm'].elements['claim.formSent'].value==''){		
		document.forms['claimForm'].elements['claim.formSent'].value=datam;
		}
		}
		}
		else{
		el.style.display = 'none';
		}
}
//End of method.

//Check the status of customer

function checkPortalBoxId() {
	    var checkPortalStatus=document.forms['claimForm'].elements['customerFile.status'].value;	   
		var checkPortalBox = document.forms['claimForm'].elements['customerFile.portalIdActive'].checked;
		if(checkPortalBox == true){		
		if(checkPortalStatus =='CNCL' || checkPortalStatus =='CLOSED')
		{
		alert("You can't Activate the ID for Customer Portal because job is Closed/Cancelled");
		document.forms['claimForm'].elements['customerFile.portalIdActive'].checked=false;
		}
		}
	}
	
//End of method.	

function changeStatus()
{
   document.forms['claimForm'].elements['formStatus'].value = '1';
}
function activecheck(){
document.forms['claimForm'].elements['claim.cportalAccess'].disabled = false;
document.forms['claimForm'].elements['claim.status'].disabled = false; 
}
function autoPopulateClaimCurrency()
{
	if(document.forms['claimForm'].elements['claimCurrency']!=undefined){
    document.forms['claimForm'].elements['claimCurrency'].value='${billing.currency}';
	}
}

function enableStateListOrigin(){ 
	var oriCon=document.forms['claimForm'].elements['claim.country'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(oriCon)> -1);
	  if(index != ''){
	  		document.forms['claimForm'].elements['claim.state'].disabled = false;
	  }else{
		  document.forms['claimForm'].elements['claim.state'].disabled = true;
	  }	        
}

function newFunctionForCountryState(){	
	 var originAbc ='${claim.country}';
	 var enbState = '${enbState}';
	 var index = (enbState.indexOf(originAbc)> -1);	
	 if(index != ''){
		 document.forms['claimForm'].elements['claim.state'].disabled =false;
	 getOriginStateReset(originAbc);
	 }
	 else{
		 document.forms['claimForm'].elements['claim.state'].disabled =true;
	 }	 
	 }

	function getOriginStateReset(target) {
		var country = target;
	 	var countryCode = "";
		<c:forEach var="entry" items="${countryCod}">
		if(country=="${entry.value}"){
			countryCode="${entry.key}";
		}
		</c:forEach>
		 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	     httpState1.open("GET", url, true);
	     httpState1.onreadystatechange = handleHttpResponse555555;
	     httpState1.send(null);
	}
	          
	 function handleHttpResponse555555(){
       if (httpState1.readyState == 4){
          var results = httpState1.responseText
          results = results.trim();
          res = results.split("@");
          targetElement = document.forms['claimForm'].elements['claim.state'];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++) {
				if(res[i] == ''){
				document.forms['claimForm'].elements['claim.state'].options[i].text = '';
				document.forms['claimForm'].elements['claim.state'].options[i].value = '';
				}else{
				stateVal = res[i].split("#");
				document.forms['claimForm'].elements['claim.state'].options[i].text = stateVal[1];
				document.forms['claimForm'].elements['claim.state'].options[i].value = stateVal[0];
				
				if (document.getElementById("state").options[i].value == '${claim.state}'){
				   document.getElementById("state").options[i].defaultSelected = true;
				} } }
				document.getElementById("state").value = '${claim.state}';
       }
  }  
	  var httpState1 = getHTTPObjectState1();	  
	function getHTTPObjectState1()	 {
	     var xmlhttp;
	     if(window.XMLHttpRequest)
	     {
	         xmlhttp = new XMLHttpRequest();
	     }
	     else if (window.ActiveXObject)
	     {
	         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	         if (!xmlhttp)
	         {
	             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	         }
	     }
	     return xmlhttp;
	 } 
</script>

<script type="text/javascript">
    var fieldName = document.forms['claimForm'].elements['field'].value;
    var fieldName1 = document.forms['claimForm'].elements['field1'].value;
	if(fieldName!=''){
		document.forms['claimForm'].elements[fieldName].className = 'rules-textUpper';
		animatedcollapse.addDiv('valuation', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('claimstatus', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('cportal', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('claimRamarks', 'fade=0,persist=0,show=1')
		animatedcollapse.init()
	}
	if(fieldName1!=''){
		document.forms['claimForm'].elements[fieldName1].className = 'rules-textUpper';
		animatedcollapse.addDiv('valuation', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('claimstatus', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('cportal', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('claimRamarks', 'fade=0,persist=0,show=1')
		animatedcollapse.init()
	}
 </script>
<script type="text/javascript">
	var fieldName = document.forms['claimForm'].elements['field'].value;
	var fieldName1 = document.forms['claimForm'].elements['field1'].value;
	if(fieldName!=''){
	document.forms['claimForm'].elements[fieldName].className = 'rules-textUpper';
	}
	if(fieldName1!=''){
	document.forms['claimForm'].elements[fieldName1].className = 'rules-textUpper';
	}
	
</script>
<script type="text/javascript">

function validateEmail(val){
	if(val.value.trim()!=""){
		var x=val.value;
		var atpos=x.indexOf("@");
		var dotpos=x.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
		  {
		  alert("Please enter valid e-mail address");
		  val.value='';
		  return false;
		  }
	}
}

	try{
	autoPopulateClaimCurrency();
	cportalCheckDetailStauts();
	autoPopulate_customerFile_destinationCountry(document.forms['claimForm'].elements['claim.country']);
	}
	catch(e){}
	try{
	getState(document.forms['claimForm'].elements['claim.country']);
	}
	catch(e){}
	try{
		 var enbState = '${enbState}';
		 var oriCon=document.forms['claimForm'].elements['claim.country'].value;
		 if(oriCon=="")	 {
				document.getElementById('state').disabled = true;
				document.getElementById('state').value ="";
		 }else{
		  		if(enbState.indexOf(oriCon)> -1){
					document.getElementById('state').disabled = false; 
					document.getElementById('state').value='${claim.state}';
				}else{
					document.getElementById('state').disabled = true;
					document.getElementById('state').value ="";
				} 
			}
		 }catch(e){}
</script>

<script type="text/javascript">   
    Form.focusFirstElement($("claimForm"));
</script>  		
<script type="text/javascript">
	setOnSelectBasedMethods(["calcDays()"]);
	setCalendarFunctionality();
	function setInsuranceUser(){
		if('${claim.insuranceUser}'.toString() == "true"){
			document.getElementById("insuranceUser").checked = true;
			document.getElementById("insuranceUser").value = '${claim.insuranceUser}';
		}
		else{
			var insurance = document.getElementById("insuranceUser");
			if(insurance != null){
				document.getElementById("insuranceUser").checked = false;
				document.getElementById("insuranceUser").value = '${claim.insuranceUser}';
			}
		}
	}
	window.onload = function(){
		
		<sec-auth:authComponent componentId="module.script.claim.form.corpAccountScript">
			var elementsLength= document.getElementsByTagName('input');
			for(i = 0; i < elementsLength.length; i++){
				if(elementsLength[i].type=='button' || elementsLength[i].type=='submit'){
					elementsLength[i].style.display = 'none';
				}
			}
		</sec-auth:authComponent>
		
			setInsuranceUser();
		  getClaimPerson('load');
		 
	}
	
	try{
		<c:if test="${not empty claim.id}">
		<c:if test="${redirect=='yes'}">
		 <c:redirect url="/editClaim.html?id=${claim.id}"/>
		</c:if>
		</c:if>
	}catch(e){
		   
	  }	
</script>
<script type="text/javascript">
function autoCompleterAjaxCity(){
	var countryName = document.forms['claimForm'].elements['claim.country'].value;
	var stateNameDes = document.forms['claimForm'].elements['claim.state'].value;
	var cityNameDes = document.forms['claimForm'].elements['claim.city'].value;		
	var countryNameDes = "";
	<c:forEach var="entry" items="${countryCod}">
	if(countryName=="${entry.value}"){
		countryNameDes="${entry.key}";
	}
	</c:forEach>
	if(cityNameDes!=''){
	var data = 'leadCaptureAutocompleteDestAjax.html?ajax=1&stateNameDes='+stateNameDes+'&countryNameDes='+countryNameDes+'&cityNameDes='+cityNameDes+'&decorator=simple&popup=true';
	$( "#city" ).autocomplete({				 
	      source: data		      
	    });
	}
}
</script>
<script type="text/javascript">
function calcDays()
{
<configByCorp:fieldVisibility componentId="component.tab.claim.claimByNoDaySettlement">
 var date2 = document.forms['claimForm'].elements['claim.formRecived'].value;
 var date1 = document.forms['claimForm'].elements['claim.closeDate'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')  {
       month = "01";
   }
   else if(month == 'Feb') {
       month = "02";
   }
   else if(month == 'Mar') {
       month = "03"
   }
   else if(month == 'Apr')  {
       month = "04"
   }
   else if(month == 'May') {
       month = "05"
   }
   else if(month == 'Jun') {
       month = "06"
   }
   else if(month == 'Jul') {
       month = "07"
   }
   else if(month == 'Aug') {
       month = "08"
   }
   else if(month == 'Sep')  {
       month = "09"
   }
   else if(month == 'Oct')  {
       month = "10"
   }
   else if(month == 'Nov')  {
       month = "11"
   }
   else if(month == 'Dec')  {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')  {
       month2 = "01";
   }
   else if(month2 == 'Feb') {
       month2 = "02";
   }
   else if(month2 == 'Mar') {
       month2 = "03"
   }
   else if(month2 == 'Apr') {
       month2 = "04"
   }
   else if(month2 == 'May')  {
       month2 = "05"
   }
   else if(month2 == 'Jun') {
       month2 = "06"
   }
   else if(month2 == 'Jul') {
       month2 = "07"
   }
   else if(month2 == 'Aug')  {
       month2 = "08"
   }
   else if(month2 == 'Sep')  {
       month2 = "09"
   }
   else if(month2 == 'Oct')  {
       month2 = "10"
   }
   else if(month2 == 'Nov') {
       month2 = "11"
   }
   else if(month2 == 'Dec')  {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = (Math.round((sDate-eDate)/86400000))+1;
  document.forms['claimForm'].elements['claim.noOfDaysForSettelment'].value = daysApart; 
  if(daysApart<1) {
    alert("Closed Date must be greater than to Received Form Date");
    document.forms['claimForm'].elements['claim.noOfDaysForSettelment'].value='';
    document.forms['claimForm'].elements['claim.closeDate'].value='';
  }
  if(document.forms['claimForm'].elements['claim.noOfDaysForSettelment'].value=='NaN')   {
     document.forms['claimForm'].elements['claim.noOfDaysForSettelment'].value = '';
   }
  </configByCorp:fieldVisibility>
}
try{
	var checkRole = 0;
	<configByCorp:fieldVisibility componentId="component.tab.claim.claimByNoDaySettlement">
	checkRole  = 14;
	</configByCorp:fieldVisibility>
	if(checkRole == 14){
		 document.getElementById('claimBy').style.display="block";
		 document.getElementById('noOfDaysForSettelment').style.display="block";
		 document.getElementById('claimBy1').style.display="block";
		 document.getElementById('noOfDaysForSettelment1').style.display="block";
	}else{
		document.getElementById('claimBy').style.display="none";
		document.getElementById('noOfDaysForSettelment').style.display="none";
		document.getElementById('claimBy1').style.display="none";
		document.getElementById('noOfDaysForSettelment1').style.display="none";
	}
}
catch(e){}

window.onload = function(){	
	<sec-auth:authComponent componentId="module.script.form.claimScript">
		var elementsLength= document.getElementsByTagName('input');
		for(i = 0; i < elementsLength.length; i++){
			if(elementsLength[i].type=='button'){
				elementsLength[i].style.display = 'none';
			}
		}
	</sec-auth:authComponent>	
}
window.onload = function() {
<sec-auth:authComponent componentId="module.script.form.agentScript">
	trap();
	 $('#claimForm').find('textarea, button, select,input[type=checkbox]').attr('disabled','disabled');
	 $('#claimForm').find('input[type=text]').attr('readonly','true').addClass('input-textUpper');
	 var saveButton = $('#saveButton');
	 $(saveButton).hide();
	 var addButton = $('#addButton');
	 $(addButton).hide();
	 var resetButton = $('#resetButton');
	 $(resetButton).hide();
</sec-auth:authComponent>
}
</script>
<script type="text/javascript">
$(document).ready( function(){
	$("select,input,textfield,textarea,button").each(function() {
    $(this).addClass('myclass');
    
});

$(".myclass").each(function(i=0,ele) {
    $(this).attr('tabindex', i+1) ;
    });
    
})   
</script>
<script type="text/javascript">
function findPartnerDetailsNew(e,t,n,r,i,s){
	<c:if test="${!roleEmployee}">
	var billtoCode="";
	var o=s.which?s.which:s.keyCode;
	if(o!=9){
		var u=document.getElementById(e).value;
		if(u.length<=1){
			document.getElementById(t).value=""
			}if(u.length>=1){
				$.get("partnerDetailsAutocompleteAjax.html?ajax=1&decorator=simple&popup=true",
						{
							partnerNameAutoCopmlete:u,
							partnerNameId:e,
							paertnerCodeId:t,
							autocompleteDivId:n,
							autocompleteCondition:r,
							billToCode:billtoCode
					},function(e){
						document.getElementById(n).style.display="block";
						$("#"+n).html(e)})
						}
			else{
				document.getElementById(n).style.display="none"
				}
			}else{
				document.getElementById(n).style.display="none"
				}
	</c:if>
	}

	
function checkVendorNameNew(){
    var vendorId = document.forms['claimForm'].elements['claim.responsibleAgentCode'].value;
    if(vendorId == ''){
    	document.forms['claimForm'].elements['claim.responsibleAgentName'].value='';
    }
    if(vendorId!=''){ 
	    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse8;
	    http2.send(null);
    }
    
}

function handleHttpResponse8(){
	if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>2){
                	if(res[2] == 'Approved'){
                			document.forms['claimForm'].elements['claim.responsibleAgentName'].value = res[1];
	           				document.forms['claimForm'].elements['claim.responsibleAgentCode'].select();
	           		}else{
	           			 alert("Responsible Agent code is not approved" ); 
	           			 document.forms['claimForm'].elements['claim.responsibleAgentName'].value="";
	                 	 document.forms['claimForm'].elements['claim.responsibleAgentCode'].value="";
					 	 document.forms['claimForm'].elements['claim.responsibleAgentCode'].select();
	           		}
               	}else{
                     alert("Responsible Agent code not valid" );
                 	 document.forms['claimForm'].elements['claim.responsibleAgentName'].value="";
                 	 document.forms['claimForm'].elements['claim.responsibleAgentCode'].value="";
				 	 document.forms['claimForm'].elements['claim.responsibleAgentCode'].select();
				 	 
			   }    } }
			   
window.onload =function ModifyPlaceHolder () {		
    var input = document.getElementById ("responsibleAgentNameId");
     input.placeholder = "Responsible Agent Search";
    
	}   
	
function showPartnerAlert(display, code, position){
    if(code == ''){ 
    	document.getElementById(position).style.display="none";
    	ajax_hideTooltip();
    }  }

function disableEnterKey(e){ 
	var key; 
	    if(window.event){ 
	    key = window.event.keyCode; 
	    } else { 
	    key = e.which;      
	    } 
	   // alert("ganesh--"+key)
	    if(key == 13){ 
	    return false; 
	    } else { 
	    return true; 
	    } 
	}
	
</script>
<script type="text/javascript">
	try{ 
		var  permissionTest  = "";
		<sec-auth:authComponent componentId="module.script.form.claimPremiumScript">
		  	permissionTest  = <%=(Integer)request.getAttribute("module.script.form.claimPremiumScript" + "Permission")%>;
		</sec-auth:authComponent>
	if(permissionTest >= 2){
		
	}else{
		<c:if test="${userType=='PARTNER'}">
			document.getElementById('hidePremiumLabel').style.display="none";
			document.getElementById('hidePremiumField').style.display="none";
		</c:if>
		}	
  	document.getElementById('claimPerson1').focus();
  	}catch(e){}
  	
  	document.getElementById('actiontoPreventRecurrence').setAttribute('maxlength', '599');
  	document.getElementById('remediationComment').setAttribute('maxlength', '599');
  	<c:if test="${roleEmployee=='true'}">
	document.getElementById('remediationClosingDate').readOnly=true;
	</c:if>
	<sec-auth:authComponent componentId="module.script.form.agentScript">
 	window.onload = function() { 
		trap();
		document.forms['claimForm'].elements['addButton'].style.display='none';
	}
</sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.script.form.corpAccountScript">
 	window.onload = function() { 
		//trap();
		document.forms['claimForm'].elements['addButton'].style.display='none';
	}
</sec-auth:authComponent>

function calculateTotalPaidToCustomer(fieldName){
	document.forms['claimForm'].elements[fieldName].value = Math.round(document.forms['claimForm'].elements[fieldName].value*100)/100;
	var propDamAmt = document.forms['claimForm'].elements['claim.propertyDamageAmount'].value;
	if(propDamAmt==''){
		propDamAmt=0.00;
	}
	var custSvcAmt = document.forms['claimForm'].elements['claim.customerSvcAmount'].value;
	if(custSvcAmt==''){
		custSvcAmt=0.00;
	}
	var lostDamAmt = document.forms['claimForm'].elements['claim.lostDamageAmount'].value;
	if(lostDamAmt==''){
		lostDamAmt=0.00;
	}
	var finalAmount = parseFloat(propDamAmt)+ parseFloat(custSvcAmt)+ parseFloat(lostDamAmt);
	document.forms['claimForm'].elements['claim.totalPaidToCustomerAmount'].value = Math.round(finalAmount*100)/100; ;
}
</script>
