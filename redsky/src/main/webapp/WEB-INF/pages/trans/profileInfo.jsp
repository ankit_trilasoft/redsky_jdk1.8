<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<title>Profile Information</title>
<meta name="heading" content="Profile Information" />
<style type="text/css">
input[type="checkbox"] {
    vertical-align: bottom;
    margin:0px;
}
.table td, .table th, .tableHeaderTable td {
    border: 1px solid #74b3dc;
    padding: 0.3em;
}
.table tbody tr:hover, .table tr.over, .contribTable tr:hover {
    background-color: none !important;
    border-bottom: none !important;
    border-top: none !important;
 
}

</style>

    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script>
function changeStatus(){
	document.forms['profileInfo'].elements['formStatus'].value = '1';
}
</script>

<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<style>

.listwhitetext2 {
background-color:#FFFFFF;
color:#444444;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
text-decoration:none;
padding-bottom:5px;
}
.bluefieldset {border:1px solid #219DD1;}
</style>

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('GeneralInfo', 'fade=0,persist=1,hide=1')
animatedcollapse.init()
</script>
</head>

<s:form id="profileInfo"  name="profileInfo" action="" method="post" validate="true" >
	<s:hidden name="popupval" value="${papam.popup}" />
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy" />
	<s:hidden name="profileInfo.partnerCode" />
	<s:hidden name="profileInfo.id" />
	<s:hidden name="profileInfo.corpId" />
	<s:hidden name="profileInfo.partnerPrivateId" />
	<s:hidden name="secondDescription" />
		<s:hidden name="thirdDescription" />
		<s:hidden name="firstDescription" />
		<s:hidden name="fourthDescription" />
		<s:hidden name="fifthDescription" />
		<s:hidden name="sixthDescription" />
		<s:hidden name="seventhDescription"/>
 <c:if test="${empty partnerPrivate.id}">
	<c:set var="isTrue" value="false" scope="request"/>
 </c:if>
<c:if test="${not empty partnerPrivate.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
	<s:hidden name="formStatus" value="" />

	<div id="newmnav" style="!margin-bottom:3px;">
	<ul>
		<c:if test="${partnerType == 'AC'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=AC"><span>Account Detail</span></a></li>
			<li><a href="editNewAccountProfile.html?id=${partnerId}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Account Profile</span></a></li>
			<li><a href="editPartnerPrivate.html?partnerId=${partnerPublic.id}&partnerCode=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Additional Info</span></a></li>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Profile Information</span></a></li>
			<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			<c:if test="${not empty partnerPrivate.id}">
			<configByCorp:fieldVisibility componentId="component.standard.accountContactTab">
				<li><a href="accountContactList.html?id=${partnerId}&partnerType=${partnerType}"><span>Account Contact</span></a></li>
				</configByCorp:fieldVisibility>
				<c:if test="${checkTransfereeInfopackage==true}">
				<li><a href="editContractPolicy.html?id=${partnerId}&partnerType=${partnerType}"><span>Policy</span></a></li>
				</c:if>
			</c:if>
			<c:if test="${partnerPublic.partnerPortalActive == true}">
			 <configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">
			<li><a href="partnerUsersList.html?id=${partnerId}&partnerType=${partnerType}"><span>Portal Users</span></a></li>
			</configByCorp:fieldVisibility>
			</c:if>
			<c:if test="${paramValue == 'View'}">
					<li><a href="searchPartnerView.html"><span>Public List</span></a></li>
				</c:if>
				<c:if test="${paramValue != 'View'}">
					<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
				</c:if>
				<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'AC'}">
					<li><a href="partnerReloSvcs.html?id=${partnerId}&partnerType=${partnerType}"><span>Services</span></a></li>
				</c:if>
					<c:if test="${partnerType == 'AC'}">
				 <c:if test="${not empty partnerPublic.id}">
				<c:url value="frequentlyAskedQuestionsList.html" var="url">
				<c:param name="partnerCode" value="${partnerPublic.partnerCode}"/>
				<c:param name="partnerType" value="AC"/>
				<c:param name="partnerId" value="${partnerPublic.id}"/>
				<c:param name="lastName" value="${partnerPublic.lastName}"/>
				<c:param name="status" value="${partnerPublic.status}"/>
				</c:url>				 
				<li><a href="${url}"><span>FAQ</span></a></li>
				<c:if test="${partnerPublic.partnerPortalActive == true}">
					<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
				</c:if>
				  </c:if>
				</c:if>	
				<c:if test="${not empty partnerPrivate.id}">
					<li><a onclick="openNotesPopupTab(this);"><span>Notes</span></a></li>
				</c:if>
				</c:if>

		<c:if test="${partnerType == 'AG'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=${partnerType}"><span>Agent Detail</span></a></li>
			<li><a href="findPartnerProfileList.html?code=${partnerPrivate.partnerCode}&partnerType=${partnerType}&id=${partnerId}"><span>Agent Profile</span></a></li>
			<li id="newmnav1" style="background:#FFF"><a class="current"><span>Additional Info<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			
			<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Vanline Ref</span></a></li>
			<li><a href="baseList.html?id=${partnerId}"><span>Base</span></a></li>
			<c:if test="${partnerType == 'AG'}">
				<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
			</c:if>
			<c:if test="${partnerType == 'AG'}">
				<li><a href="partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Services</span></a></li>
			</c:if>
			<c:if test="${partnerPublic.partnerPortalActive == true}">
			   <configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
	       			<li><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & contacts</span></a></li>
	       			<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
			        </configByCorp:fieldVisibility>
			</c:if>
			
			       <li id="AGR"><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
			 
			 
			  	
		</c:if>
		<c:if test="${partnerType == 'VN'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=${partnerType}"><span>Vendor Detail</span></a></li>
			<li id="newmnav1" style="background:#FFF"><a class="current"><span>Additional Info<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			<li><a href="partnerUsersList.html?id=${partnerId}&partnerType=${partnerType}"><span>Users & contacts</span></a></li>		
		</c:if>
		<c:if test="${partnerType == 'CR'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=${partnerType}"><span>Carrier Detail</span></a></li>
		</c:if>
		<c:if test="${partnerType == 'CR' || partnerType == 'VN'}">
			<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.tab.partner.preferredAgent.show">
				<c:if test="${partnerType == 'AC'}">
							<li><a href="searchPreferredAgentList.html?partnerId=${partnerPublic.id}&partnerCode=${partnerCode}&partnerType=${partnerType}"><span>Preferred Agent</span></a></li>
						</c:if>
		</configByCorp:fieldVisibility>
	</ul>
	</div>
	<div class="spn">&nbsp;</div>

	<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
	<div id="content" align="center">
	<div id="liquid-round-top">
	<div class="top"><span></span></div>
		
	<!-- Start AccoutType Section	-->
	
	<c:if test="${partnerType == 'AC'}">
	
		<div class="center-content">
			<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%;margin-bottom:5px;">
				<tbody>
					<tr>						
						<td colspan="20" width="100%" align="left" style="margin: 0px">
     						<div onClick="javascript:animatedcollapse.toggle('GeneralInfo')" style="margin: 0px">
			 					<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
									<tr>
					  					<td class="headtab_left">
					  					</td>
					  					<td nowrap class="headtab_center">&nbsp;&nbsp;General Information
					  					</td>
					  					<td width="28" valign="top" class="headtab_bg"></td>
					  					<td class="headtab_bg_special">&nbsp;
					  					</td>
					  					<td class="headtab_right">
					  					</td>
				 					</tr>
								</table>
					  		</div>
					  	</td>
					 </tr>
			
				<tr>
					<td>
					
							<table class="table" border="0" style="margin:0px;border:1px solid #74b3dc;width:95%">
							<tr>
						<td align="right" class="listwhitetext" style="width:133px;text-align:right;"><b>Services&nbsp;Provided</b></td>
						<td align="left" class="listwhitetext">
						<c:set var="isdomFlag" value="false" />
						<c:if test="${profileInfo.dom}">
										<c:set var="isdomFlag" value="true" />
						</c:if>
						<s:checkbox key="profileInfo.dom" fieldValue="true" value="${isdomFlag}"  tabindex="" /><b>&nbsp;Domestic</b>
						</td>
						<td align="left" class="listwhitetext">
						<c:set var="isintlFlag" value="false" />
						<c:if test="${profileInfo.intl}">
									<c:set var="isintlFlag" value="true" />
								</c:if>
								<s:checkbox key="profileInfo.intl" fieldValue="true" value="${isintlFlag}"  tabindex="" />
								<b>International</b>
								</td>
						<td align="left" class="listwhitetext">
						<c:set var="iscomFlag" value="false" />
						<c:if test="${profileInfo.intl}">
									<c:set var="iscomFlag" value="true" />
								</c:if>
								<s:checkbox key="profileInfo.intl" fieldValue="true" value="${iscomFlag}"  tabindex="" />
								<b>Commercial Svcs</b>
								</td>
						<td align="left" class="listwhitetext">
						<c:set var="isrloFlag" value="false" />
						<c:if test="${profileInfo.rlo}">
									<c:set var="isrloFlag" value="true" />
								</c:if>
								<s:checkbox key="profileInfo.rlo" fieldValue="true" value="${isrloFlag}"  tabindex="" />
								<b>Relo Svcs</b>
								</td>
								
								<td align="left" class="listwhitetext">
								<c:set var="isothFlag" value="false" />
								<c:if test="${profileInfo.oth}">
									<c:set var="isothFlag" value="true" />
								</c:if>
								<s:checkbox key="profileInfo.oth" fieldValue="true" value="${isothFlag}"  tabindex="" />
								<b>Others</b>
								</td>
								<td  align="left" style="text-align:right;" class="listwhitetext" ><b>If&nbsp;Other,&nbsp;Explain:</b></td>
							<td align="left" class="listwhitetext" >
								<s:textfield cssClass="input-text" id="profileInfo.othComnts" name="profileInfo.othComnts" maxLength="500" cssStyle="width:150px;" size="35"  tabindex=""/>
							</td>
						
						</tr>
							
							
							</table>
							<table width="95%" cellspacing="0" cellpadding="0" style="margin: 0px; padding: 0px;">
											<tbody><tr class="subcontenttabChild">
								<td width="215px" align="center"><b>&nbsp;Service&nbsp;Providers</b></td>
								</tr>
								</tbody></table>
						
							
						
						<table style="margin:0px;border:2px solid #74b3dc;width:95%" class="table">
						<tr>
						<td class="listwhitetext" style="text-align:right;"><b>Agent</b></td>
						<td class="listwhitetext" style="text-align:center;"><b>Company&nbsp;Name</b></td>
						<td class="listwhitetext" style="text-align:center;"><b>Contact</b></td>
						<td class="listwhitetext" style="text-align:center;"><b>Phone</b></td>
						<td class="listwhitetext" style="text-align:center;"><b>Email&nbsp;Address</b></td>
						<td class="listwhitetext" style="text-align:center;"><b>Various Contacts</b></td>						
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:right;"><b>RMC</b></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.agentCompanyName" name="profileInfo.agentCompanyName" maxLength="45"  size="30"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.agentContact" name="profileInfo.agentContact"    maxLength="45" size="30" tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.agentPhone" name="profileInfo.agentPhone"   maxLength="45" size="30" tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.agentEmail" name="profileInfo.agentEmail"  maxLength="45"  size="30" tabindex="" onchange="checkEmail(this,'profileInfo.agentEmail')"/></td>
						<td class="listwhitetext" rowspan="7" style="vertical-align:top">
						<s:textarea name="profileInfo.variousContacts" id="variousContacts" required="true" cssClass="textarea" tabindex="1" cssStyle="width:250px;height:200px;" onkeypress="return checkLength();" />
						</td>
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:right;"><b>Audit&nbsp;Firm</b></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.auditfirmCompanyName" name="profileInfo.auditfirmCompanyName"  size="30" maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.auditfirmContact" name="profileInfo.auditfirmContact"  size="30" maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.auditfirmPhone" name="profileInfo.auditfirmPhone"  size="30" maxLength="45" tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.auditfirmEmail" name="profileInfo.auditfirmEmail" size="30"  maxLength="45"  tabindex="" onchange="checkEmail(this,'profileInfo.auditfirmEmail')"/></td>
						
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:right;"><b>HHG&nbsp;Provider-Domestic</b></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderDomCompanyName" name="profileInfo.hhgProviderDomCompanyName" size="30"  maxLength="45"   tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderDomContact" name="profileInfo.hhgProviderDomContact" size="30" maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderDomPhone" name="profileInfo.hhgProviderDomPhone" size="30" maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProvideDomEmail" name="profileInfo.hhgProvideDomEmail"  size="30" maxLength="45"  tabindex="" onchange="checkEmail(this,'profileInfo.hhgProvideDomEmail')"/></td>
						
						</tr>
						
						<tr>
						<td class="listwhitetext" rowspan="4" style="text-align:right;"><b>HHG&nbsp;Provider-Intl</b></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderIntlCompanyName" name="profileInfo.hhgProviderIntlCompanyName" size="30" maxLength="45"   tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderIntlContact" name="profileInfo.hhgProviderIntlContact" size="30" maxLength="45"   tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderIntlPhone" name="profileInfo.hhgProviderIntlPhone"  size="30" maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProvideIntlEmail" name="profileInfo.hhgProvideIntlEmail" size="30"  maxLength="45"   tabindex="" onchange="checkEmail(this,'profileInfo.hhgProvideIntlEmail')"/></td>
						</tr>
						<tr>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderIntlCompanyName2" name="profileInfo.hhgProviderIntlCompanyName2" size="30" maxLength="45"   tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderIntlContact2" name="profileInfo.hhgProviderIntlContact2" size="30" maxLength="45"   tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderIntlPhone2" name="profileInfo.hhgProviderIntlPhone2"  size="30" maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProvideIntlEmail2" name="profileInfo.hhgProvideIntlEmail2"  size="30" maxLength="45"  tabindex="" onchange="checkEmail(this,'profileInfo.hhgProvideIntlEmail2')"/></td>
						</tr>
						<tr>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderIntlCompanyName3" name="profileInfo.hhgProviderIntlCompanyName3" size="30" maxLength="45"   tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderIntlContact3" name="profileInfo.hhgProviderIntlContact3"  size="30" maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderIntlPhone3" name="profileInfo.hhgProviderIntlPhone3"   size="30" maxLength="45" tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProvideIntlEmail3" name="profileInfo.hhgProvideIntlEmail3"   size="30" maxLength="45" tabindex="" onchange="checkEmail(this,'profileInfo.hhgProvideIntlEmail3')"/></td>
						</tr>
						<tr>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderIntlCompanyName4" name="profileInfo.hhgProviderIntlCompanyName4"  size="30"  maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderIntlContact4" name="profileInfo.hhgProviderIntlContact4"  size="30"  maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProviderIntlPhone4" name="profileInfo.hhgProviderIntlPhone4"   size="30" maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="profileInfo.hhgProvideIntlEmail4" name="profileInfo.hhgProvideIntlEmail4"   size="30" maxLength="45"  tabindex="" onchange="checkEmail(this,'profileInfo.hhgProvideIntlEmail4')"/></td>
						</tr>
						</table>
				
				</td></tr></tbody></table>
						

   <!--Information new page end-->
	</div>
	</c:if>		
		
	<div class="bottom-header" style="margin-top:40px;"><span></span></div>	
	</div>
	</div>
	</div>
	<table border="0" style="width:850px;">
		<tr>
			<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn' /></b></td>
			<fmt:formatDate var="containerCreatedOnFormattedValue" value="${profileInfo.createdOn}" pattern="${displayDateTimeEditFormat}" />
			<s:hidden name="profileInfo.createdOn" value="${containerCreatedOnFormattedValue}" />
			<td width="120px"><fmt:formatDate value="${profileInfo.createdOn}" pattern="${displayDateTimeFormat}" /></td>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='container.createdBy' /></td>
			<c:if test="${not empty profileInfo.id}">
				<s:hidden name="profileInfo.createdBy" />
				<td><s:label name="createdBy" value="%{profileInfo.createdBy}" /></td>
			</c:if>
			<c:if test="${empty profileInfo.id}">
				<s:hidden name="profileInfo.createdBy" value="${pageContext.request.remoteUser}" />
				<td><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
			</c:if>
			<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn' /></td>
			<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${profileInfo.updatedOn}" pattern="${displayDateTimeEditFormat}" />
			<s:hidden name="profileInfo.updatedOn" value="${containerUpdatedOnFormattedValue}" />
			<td width="120px"><fmt:formatDate value="${profileInfo.updatedOn}" pattern="${displayDateTimeFormat}" /></td>
			<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></td>
			<c:if test="${not empty profileInfo.id}">
				<s:hidden name="profileInfo.updatedBy" />
				<td style="width:85px"><s:label name="updatedBy" value="%{profileInfo.updatedBy}" /></td>
			</c:if>
			<c:if test="${empty profileInfo.id}">
				<s:hidden name="profileInfo.updatedBy" value="${pageContext.request.remoteUser}" />
				<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}" /></td>
			</c:if>
		</tr>
	</table>

	<c:if test="${empty param.popup}">
		<input id="button.save" type="button" class="cssbutton"  value="Save" name="button.save" style="width:60px;" onclick="return submitPartner();" tabindex="" />
		<s:reset cssClass="cssbutton" key="Reset"  cssStyle="width:60px; height:25px " tabindex="" />
	</c:if>
	

</s:form>

<script type="text/javascript">

function submitPartner(){
	document.forms['profileInfo'].action= "saveProfileInfo.html?partnerId=${partnerId}&partnerCode=${partnerCode}&partnerType=${partnerType}";
	 document.forms['profileInfo'].submit();
}

function checkEmail(targetElelemnt,id) {
	var elementValue=targetElelemnt.value;
	var status = false;     
	var emailRegEx = /^[A-Z0-9.'_%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
	     if (elementValue.search(emailRegEx) == -1) {
	    	 targetElelemnt.value="";
	    	 document.forms['profileInfo'].elements[id].focus();
	         alert("Please enter a valid email address.");
	     }
	     else {
	          status = true;
	     }
	     return status;
	}
	
function checkLength(){
	var txt1 =  document.getElementById('variousContacts').value;
	if(txt1.length >1000){
		alert("Cannot enter more than 1000 characters in Various Contacts.");
		 document.getElementById('variousContacts').value = txt1.substring(0,1000);
		return false;
	}else{
		return true;
	}
} 

</script>
