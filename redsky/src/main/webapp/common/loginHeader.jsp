<%@ include file="/common/taglibs.jsp"%>

<c:if test="${pageContext.request.locale.language != 'en'}">
    <div id="switchLocale"><a href="<c:url value='/?locale=en'/>"><fmt:message key="webapp.name"/> in English</a></div>
</c:if>

<div id="branding">
<!--  
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="257" height="48"><img src="<c:url value='/images/topimage.jpg'/>" width="275" height="49" /></td>
    <td background="<c:url value='/images/top-bg.jpg'/>"></td>
    <td colspan="2" align="left" valign="middle" background="<c:url value='/images/top-bg.jpg'/>"></a></td>
  </tr>
</table>
-->  
</div>
<hr />

<%-- Put constants into request scope --%>
<appfuse:constants scope="request"/>

