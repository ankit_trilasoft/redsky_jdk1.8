package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DefaultAccountLine;
import com.trilasoft.app.model.Discount;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.PricingDetailsValue;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DefaultAccountLineManager;
import com.trilasoft.app.service.DiscountManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.ExchangeRateManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.PartnerAccountRefManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.webapp.json.JSONArray;
import com.trilasoft.app.webapp.json.JSONException;
import com.trilasoft.app.webapp.json.JSONObject;
import com.trilasoft.pricepoint.wsclient.PricePointClientServiceCall;

public class PricingLineAction  extends BaseAction implements Preparable {
	static final Logger logger = Logger.getLogger(PricingLineAction.class);
	Date currentdate = new Date();
	private ErrorLogManager errorLogManager;
	private ServiceOrderManager serviceOrderManager;
	private TrackingStatusManager trackingStatusManager;
	private MiscellaneousManager miscellaneousManager ;
	private BillingManager billingManager; 
	private CompanyManager companyManager;
	private CompanyDivisionManager companyDivisionManager;
	private CustomerFileManager customerFileManager;
	private AccountLineManager accountLineManager;
	private RefMasterManager refMasterManager;
	private PartnerManager partnerManager;
	private DiscountManager discountManager;
	private Company company; 
	private Billing billing;
	private Billing billingRecods;
	private ServiceOrder serviceOrder;
	private ServiceOrder serviceOrderToRecods;
	private TrackingStatus trackingStatus;
	private TrackingStatus trackingStatusToRecods;
	private  Miscellaneous  miscellaneous;
	private CustomerFile customerFile;
	private CustomerFile customerFileToRecods;
	private SystemDefault systemDefault;
	private User discountUserFlag;
	private Long sid;
	private String sessionCorpID;
	private String estVatFlag;
	private String commissionJobName;
	private String jobName;
	private String systemDefaultVatCalculationNew;
	private String payVatDescrList;
	private String recVatDescrList;
	private String systemDefaultmiscVl;
	private String baseCurrency;
	private  String multiCurrency;
	private String baseCurrencyCompanyDivision="";
	private String accountLineStatus="";
	private String accUpdateAjax;
	private String usertype; 
	private String commodityForUser;
	private String bookingAppUserCode;
	private String defaultSoURL;
	private String serviceTypeForUser;
	private Integer companyDivisionGlobal;
	private Boolean acctDisplayEntitled = new Boolean(true);
    private Boolean acctDisplayEstimate = new Boolean(true);
    private Boolean acctDisplayRevision = new Boolean(true);
    private Boolean chargeDiscountFlag=false;
    private Boolean activeStatus;
    private boolean accountLineAccountPortalFlag=false;
    private boolean networkAgent=false;
    private boolean writeOffReasonFlag=false;
    private List compDivForBookingAgentList;
    private List companyDivis=new ArrayList();
    private List<SystemDefault> sysDefaultDetail;
    private List accountLineList;
    private Set<Role> roles;
    private  Map<String,String> euVatPercentList;
    private  Map<String,String> payVatPercentList;
    private  Map<String,String>estVatList;
    private  Map<String,String> payVatList;
    private Map<String, String> category;
    private Map<String, String> writeoffReasonList=new LinkedHashMap<String, String>();
    private List writeoffReasonWithFlex1List=new ArrayList();
    private Map<String,String> basis;
    private Map<String,Map<String,String>> discountMap; 
    private Date pricePointUserStartDate;
    private boolean pricePointFlag=false;
    private boolean pricePointexpired=false;
    private boolean contractType=false;
    private boolean billingCMMContractType=false;
	private boolean billingDMMContractType=false;
	private int billToLength = 0;
	/*		Start New Field ************************************************************************************************  */	
	private String checkContractChargesMandatory= "0";
	private boolean checkAccessQuotation=false;
	private Boolean costElementFlag;
	private String accountInterface;
	private String companyDivisionAcctgCodeUnique;
	private Map<String, String> currencyExchangeRate;
	private ExchangeRateManager exchangeRateManager;
	private Map<String, String> euvatRecGLMap = new LinkedHashMap<String, String>();
	private Map<String, String> payVatPayGLMap = new LinkedHashMap<String, String>();
	private Map<String, String> qstEuvatRecGLMap = new LinkedHashMap<String, String>();
	private Map<String, String> qstPayVatPayGLMap = new LinkedHashMap<String, String>();
	private Map<String, String> qstEuvatRecGLAmtMap = new LinkedHashMap<String, String>();
	private Map<String, String> qstPayVatPayGLAmtMap = new LinkedHashMap<String, String>();
	private Map<String,String> estVatPersentList;
	private AccountLine accountLinePopup;
	private Map<String,String> payingStatus;
	private Map<String,String> country;
	private String shipNumber;
	private String oldRecRateCurrency;
	private String addPriceLine;
	private String inActiveLine;
	//private String isSOExtract;
	private AccountLine accountLine;
	private Long id;
	private String divisionFlag;
	private String SoContract="";
	private String SoBillToCode="";
	private String reloServiceType;
	private String serviceTypeSO;
	public String rollUpInvoiceFlag;
	private List chargeCodeList;
	private Boolean ignoreForBilling=false;
	private int billingFlag=0;
	private String vanLineAccountView;
	public String vanLineAccCategoryURL;
	private ChargesManager chargesManager;
	private DefaultAccountLineManager defaultAccountLineManager;
	private String pricingData;	
	private List quotationDiscriptionList;
    private String setDescriptionChargeCode;
    private String setDefaultDescriptionChargeCode;
    private String actualDiscountFlag;
    private String revisionDiscountFlag;
    private AccountLine synchedAccountLine;
    private String weightType;
	private  Map<String,String>UTSIpayVatPercentList;
	private  Map<String,String>UTSIeuVatPercentList;
	DecimalFormat decimalFormat = new DecimalFormat("#.####");
	private  Map<String,String>UTSIpayVatList;
	private ServiceOrder serviceOrderRemote;
	 private String shipSize;
	 private String minShip;
	private String countShip;
	private boolean singleCompanyDivisionInvoicing=false;
   private Boolean oAdAWeightVolumeValidationForInv=false;
   private Boolean subOADAValidationForInv=false;
   private List ownBookingAgentCode;
   private Boolean ownBookingAgentFlag=false;
   private String buttonType;
	private String companyDivisionForTicket;
	private String billToCodeForTicket;
    private String emptyList;
    private String  accCorpID;
    private String salesCommisionRate;
    private String grossMarginThreshold;
    private String roleSupervisor;
    private  Map<String,String>paymentStatus;
    //private boolean utsiRecAccDateFlag=false;
	//private boolean utsiPayAccDateFlag=false;
	private boolean accNonEditFlag=false;
	private String userRoleExecutive;
	private String systemDefaultstorage;
	private Long aid;
	private Map<String, String> myfileDocumentList=new HashMap<String, String>();
	private static Map pageFieldVisibilityMap = AppInitServlet.pageFieldVisibilityComponentMap;
	private String setRegistrationNumber;
	private String dateUpdate;
	private Map<String, String> companyDivisionMap = new LinkedHashMap<String, String>();
	private String compDivFlag;
	private User user;
	private boolean payablesXferWithApprovalOnly = false;
	private Boolean autuPopPostDate=false;
	private	Date payDate=null;
	private List closeCompanyDivision;
	private boolean checkFieldVisibility=false;
	private  List contractBillList = new ArrayList();
	private Map<String, String> billingContractMap = new LinkedHashMap<String, String>();
	private String varianceExpenseAmount;
 	private String revisionExpense;
 	private String actualExpense;
 	private String localAmount;
 	private String vendorCode;
 	private String chargeCode;
 	private String estimateVendorName;
 	private String actgCode;
 	private ServicePartner servicePartner; 
 	private boolean accrualReadyContractFlag=false;
 	private PartnerPrivateManager partnerPrivateManager; 
 	private Double minMargin;
 	private Map<String, String> fieldNameList = new LinkedHashMap<String, String>();
 	private boolean vanlineSettleColourStatus= false;
 	private List getGlTypeList = new ArrayList();
 	private Boolean driverCommissionSetUp=false;
 	private AccountLine accountLineOld; 	
 	private String oiJobList;

    
	/*		End New Field ************************************************************************************************  */	
	public PricingLineAction(){
		   Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	         	user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
	        if(user.getAcctDisplayEntitled()!=null){
	        	acctDisplayEntitled=user.getAcctDisplayEntitled();
	        }
	        if(user.getAcctDisplayEstimate()!=null){
	        	acctDisplayEstimate=user.getAcctDisplayEstimate();
	        }
	        if(user.getAcctDisplayRevision()!=null){
	        	acctDisplayRevision=user.getAcctDisplayRevision();
	        } 
	        usertype=user.getUserType();
	    	roles = user.getRoles();
	        pricePointUserStartDate=user.getPricePointStartDate();
	        commodityForUser = user.getCommodity();
	        if(commodityForUser==null){
	        	commodityForUser="";
	        }
	        
	        if(user.isDefaultSearchStatus()){
	 		   activeStatus=true;
	 	   	}else{
	 		   activeStatus=false;
	 	   	}
	        bookingAppUserCode=user.getBookingAgent();
	        if(user.getAcctDisplayEntitled()!=null){
	        	acctDisplayEntitled=user.getAcctDisplayEntitled();
	        }
	        if(user.getAcctDisplayEstimate()!=null){
	        	acctDisplayEstimate=user.getAcctDisplayEstimate();
	        }
	        if(user.getAcctDisplayRevision()!=null){
	        	acctDisplayRevision=user.getAcctDisplayRevision();
	        }
	        if(user.getDefaultSoURL()!=null){
	        	defaultSoURL=user.getDefaultSoURL();
	        }
	        discountUserFlag=user; 
	        serviceTypeForUser=user.getServiceType();
	        if(serviceTypeForUser==null) {
	        	serviceTypeForUser="";
	        } 
	        pricePointUserStartDate=user.getPricePointStartDate();
	        Set<Role> roles = user.getRoles();
			Role role = new Role();
			Iterator it = roles.iterator();
			String userRole1;
			while (it.hasNext()) {
				role = (Role) it.next();
				userRole1 = role.getName();
				if(userRole1.equalsIgnoreCase("ROLE_EXECUTIVE")){
					userRoleExecutive = "yes";
				}else{
					userRoleExecutive = "no";
				}
			}
	        
		}
	
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try {
			accCorpID=sessionCorpID;
			userName=getRequest().getRemoteUser();
			closeCompanyDivision=serviceOrderManager.findClosedCompanyDivision(sessionCorpID);
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }
			 
			if(company.getPayablesXferWithApprovalOnly()!=null){
				 payablesXferWithApprovalOnly =company.getPayablesXferWithApprovalOnly();
			 }else{
				 payablesXferWithApprovalOnly =false;
			 }
			if(company.getAccountLineNonEditable()!=null && company.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
				 accNonEditFlag =true;
			 }
			 
			if(company!=null){
				if(pricePointUserStartDate!=null){
					pricePointFlag=company.getEnablePricePoint();
				 }
				if(company.getPriceEndDate()!=null && company.getPriceEndDate().compareTo(new Date())>0){					 
						 pricePointexpired=true;
				}
				if((company.getChargeDiscountSetting()!=null)&&(company.getChargeDiscountSetting())){
					 	chargeDiscountFlag = true;
				}
			
				if(company.getEstimateVATFlag()!=null){
						estVatFlag=company.getEstimateVATFlag();
				}
				if(company.getUTSI()!=null){
					 networkAgent=company.getUTSI();
				}else{
					networkAgent=false;
				}				 
				if(company.getMultiCurrency()!=null){
				 multiCurrency = company.getMultiCurrency();
				 }
			 
				if(company.getoAdAWeightVolumeMandatoryValidation()!=null){
				 oAdAWeightVolumeValidationForInv=company.getoAdAWeightVolumeMandatoryValidation();
				}
				if(company.getsubOADASoftValidation()!=null){
				 subOADAValidationForInv=company.getsubOADASoftValidation();
				}
				 
				checkAccessQuotation=company.getAccessQuotationFromCustomerFile();
				singleCompanyDivisionInvoicing=company.getSingleCompanyDivisionInvoicing();
				compDivFlag = company.getCompanyDivisionFlag(); 
			}
			currencyExchangeRate=exchangeRateManager.getExchangeRateWithCurrency(sessionCorpID);
			writeoffReasonList =refMasterManager.findByParameterOnlyDescription(sessionCorpID, "Writeoff_Reason");
			writeoffReasonWithFlex1List =refMasterManager. findByParameterFlex1(sessionCorpID, "Writeoff_Reason");
			category = new LinkedHashMap<String, String>();
			category.put("", "");
			category.putAll(refMasterManager.findByParameter(sessionCorpID, "ACC_CATEGORY"));
			
			basis = new LinkedHashMap<String, String>();
			basis.put("", "");
			basis.putAll(refMasterManager.findByParameter(sessionCorpID, "ACC_BASIS"));
			
			euvatRecGLMap = refMasterManager.findByParameterVatRecGL(sessionCorpID, "EUVAT");
		    payVatPayGLMap = refMasterManager.findByParameterVatPayGL(sessionCorpID, "PAYVATDESC");

		    Map<String, Map<String, String>> valueMap=refMasterManager.findByParameterQstVatGL(sessionCorpID);
		    qstEuvatRecGLMap = valueMap.get("EUVAT");
		    qstPayVatPayGLMap = valueMap.get("PAYVATDESC");
		    valueMap=refMasterManager.findByParameterQstVatAmtGL(sessionCorpID);
		    qstEuvatRecGLAmtMap = valueMap.get("EUVAT");
		    qstPayVatPayGLAmtMap = valueMap.get("PAYVATDESC");
		    paymentStatus=refMasterManager.findByParameter(sessionCorpID, "PAY_STATUS");
		    payingStatus=refMasterManager.findByParameter(sessionCorpID, "ACC_STATUS");
		    country=refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
		    companyDivisionGlobal=companyDivisionManager.findGlobalCompanyCode(sessionCorpID);
		    euVatPercentList = refMasterManager.findVatPercentList(sessionCorpID, "EUVAT");
			estVatPersentList=euVatPercentList;
			payVatPercentList = refMasterManager.findVatPercentList(sessionCorpID, "PAYVATDESC");
			estVatList=new LinkedHashMap<String, String>();
		    estVatList.put("", "");
		    estVatList.putAll(refMasterManager.findByParameterWithoutParent(sessionCorpID, "EUVAT"));
		    payVatList=new LinkedHashMap<String, String>();
		    payVatList.put("", "");
		    payVatList.putAll(refMasterManager.findByParameterWithoutParent(sessionCorpID, "PAYVATDESC"));
			
			sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault1 : sysDefaultDetail) {
				systemDefaultVatCalculationNew="N";
				systemDefault=systemDefault1;
					
					if(systemDefault.getVatCalculation()){
						systemDefaultVatCalculation="true";	
					}
					payVatDescrList=systemDefault1.getPayableVat();
					recVatDescrList=systemDefault1.getReceivableVat();
					if(systemDefault1.getCommissionable()!=null){
						jobName	=systemDefault1.getCommissionable();
					}
					if(systemDefault1.getMiscVl()!=null){
						systemDefaultmiscVl=systemDefault1.getMiscVl();
					}
					if(systemDefault1.getBaseCurrency()!=null){
						baseCurrency=systemDefault1.getBaseCurrency();	
					}
					if(systemDefault1.getStorage()!=null){
						systemDefaultstorage = systemDefault1.getStorage();
					}
					checkContractChargesMandatory=systemDefault.getContractChargesMandatory();
					costElementFlag=false;
					if(systemDefault.getCostElement()){
					costElementFlag=systemDefault.getCostElement();
					}

					if(systemDefault.getAccountLineAccountPortalFlag() !=null ){
						accountLineAccountPortalFlag = 	systemDefault.getAccountLineAccountPortalFlag();
					}
					if(systemDefault.getVatCalculation()!=null && systemDefault.getVatCalculation()){
						systemDefaultVatCalculationNew="Y";	
					}
					if(systemDefault.getAccountingInterface()!=null && !systemDefault.getAccountingInterface().equalsIgnoreCase("") && !systemDefault.getAccountingInterface().equalsIgnoreCase("N")){
						accountInterface="Y";	
					}else{
						accountInterface="N";	
					}
					
					if(systemDefault.getCompanyDivisionAcctgCodeUnique()!=null && !systemDefault.getCompanyDivisionAcctgCodeUnique().equalsIgnoreCase("") && !systemDefault.getCompanyDivisionAcctgCodeUnique().equalsIgnoreCase("N")){
						companyDivisionAcctgCodeUnique="Y";	
					}else{
						companyDivisionAcctgCodeUnique="N";	
					}
					if(systemDefault.getBaseCurrency() !=null ){
						baseCurrency=systemDefault.getBaseCurrency();
					}
					if(systemDefault.getSalesCommisionRate()!=null){
						salesCommisionRate = systemDefault.getSalesCommisionRate();
					}
					if(systemDefault.getGrossMarginThreshold()!=null){
						grossMarginThreshold= systemDefault.getGrossMarginThreshold();
					}
					if(systemDefault.getVanlineSettleColourStatus()!=null){
						vanlineSettleColourStatus=systemDefault.getVanlineSettleColourStatus();
					}
				 	driverCommissionSetUp=systemDefault.getDriverCommissionSetUp();
					autuPopPostDate=systemDefault.getAutoPayablePosting();
					payDate=systemDefault.getPostDate1();
					
				}
				
			}
			
			String permKey = sessionCorpID +"-"+"module.accountLine.section.receivableDetail.edit";
			  visibilityForReceivableDetail=AppInitServlet.roleBasedComponentPerms.containsKey(permKey);
			  if(visibilityForReceivableDetail){
				  receivableSectionDetail=true;
			  }
		fieldNameList = new LinkedHashMap<String, String>();
		fieldNameList.put("","");
		fieldNameList.put("basis","Basis");
		fieldNameList.put("billToCode","Bill To Code");
		fieldNameList.put("billToName","Bill To Name");
		fieldNameList.put("category","Category");
		fieldNameList.put("companyDivision","Company Division");
		fieldNameList.put("chargeCode","Charge Code");
		fieldNameList.put("vendorCode", "Vendor Code");
		fieldNameList.put("estimateVendorName", "Vendor Name");
		fieldNameList.put("invoiceNumber", "Payable Invoice");
		fieldNameList.put("payNotInvoiced", "Payable Not Invoiced");
		fieldNameList.put("recInvoiceNumber", "Receivable Invoice");
		fieldNameList.put("recNotInvoiced", "Receivable Not Invoiced");
		}catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
	    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	private Boolean visibilityForReceivableDetail=false;
	private Boolean receivableSectionDetail=false;
	@SkipValidation
 	public String findAllPricingLineAjax(){
 		Long timeTaken;
		try {
			Long startTime = System.currentTimeMillis();
			
			getPricingHelper();
	/*	
			SoContract=customerFile.getContract();
			if(SoContract!=null && (!(SoContract.toString().equals("")))){
				String	contractTypeValue = accountLineManager.checkContractType(SoContract,sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;
				}
			}*/
			//billing = billingManager.get(id);
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String	contractTypeValue="";
				contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}
			}
			 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
			    }
			    if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			      	billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
			      }
				 if(contractType && trackingStatus.getAccNetworkGroup() && billingCMMContractType ){
						Set<String> billToSet = new HashSet<String>();
						
						List billToCodeNtList = accountLineManager.getBillToCode(serviceOrder.getShipNumber(),serviceOrder.getId(),trackingStatus.getNetworkPartnerCode());
						if(billToCodeNtList != null && billToCodeNtList.size() > 0){
							for (int i = 0; i < billToCodeNtList.size(); i++) {
								String partnertype=  partnerManager.checkPartnerType(billToCodeNtList.get(i).toString());
								if("".equals(partnertype.trim())){
									billToSet.add(billToCodeNtList.get(i).toString());
								}
							}
							billToLength = billToSet.size();
						}
					}	
			
			companyDivis.add("");
			companyDivis.addAll(customerFileManager.findCompanyDivisionByBookAgAndCompanyDivision(serviceOrder.getCorpID(), "",serviceOrder.getCompanyDivision()));
			
			if(accountLineStatus == null || "".equals(accountLineStatus)){
				accountLineStatus="true";
			}
			accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
			if(accountLineList == null || accountLineList.isEmpty()){
				emptyList="true";
			}
			Iterator<AccountLine> idIterator =  accountLineList.iterator(); 
			while (idIterator.hasNext()){
				AccountLine accountLineOld = (AccountLine) idIterator.next(); 
				try{
					if(accountLineOld.getCompanyDivision()!=null && (!(accountLineOld.getCompanyDivision().equals("")))){	
						if(companyDivis.contains(accountLineOld.getCompanyDivision())){
						}else{
							companyDivis.add(accountLineOld.getCompanyDivision());	
						}
					}
				}catch(Exception e){}
				
				try{
					  if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup())	{
						   String networkSynchedId=	accountLineManager.getNetworkSynchedId(accountLineOld.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
					      	if(!(networkSynchedId.equals(""))){
					      		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
					      		Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
					      		boolean accNonEditable=false;
								 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
									 accNonEditable =true;
								 }
								 if(accNonEditable){
									 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
						      			 //utsiRecAccDateFlag=true;
										 accountLineOld.setUtsiRecAccDateFlag(true);
						      		}
									 if(synchedAccountLine.getInvoiceNumber()!=null && (!(synchedAccountLine.getInvoiceNumber().toString().toString().equals("")))){
						      			 //utsiPayAccDateFlag=true;
						      			accountLineOld.setUtsiPayAccDateFlag(true);
						      		}
								 }else{
					      		if(synchedAccountLine.getRecAccDate()!=null && accountLine.getRecAccDate()== null){
					      			 //utsiRecAccDateFlag=true;
					      			accountLineOld.setUtsiRecAccDateFlag(true);
					      		}
					      		if(synchedAccountLine.getPayAccDate()!=null && accountLine.getRecAccDate()== null){
					      			 //utsiPayAccDateFlag=true;
					      			accountLineOld.setUtsiPayAccDateFlag(true);
					      		}
							} 	
					      	} 
					      	accountLineList=new ArrayList();
					      	accountLineList.add(accountLineOld);
					  	}
					}catch(Exception e){}
				
			}
			for(int i = 0;i<companyDivis.size();i++){
				companyDivisionMap.put((String) companyDivis.get(i), (String) companyDivis.get(i));
			}
			if((accountLineList!=null)&&(!accountLineList.isEmpty())){
				discountMap=new HashMap<String, Map<String,String>>();
				List<AccountLine> cc =accountLineList;
				List<Discount> ds=null;
				Map<String,String> tempData=null;
				ds=discountManager.discountMapList(sessionCorpID);
				for(AccountLine acclineDis:cc){
					//ds=discountManager.discountList(billing.getContract(),acclineDis.getChargeCode(),sessionCorpID);
					tempData=new HashMap<String, String>();
					tempData.put("", "");
					for(Discount discountRec:ds){
						if((discountRec.getChargeCode()!=null)&&(discountRec.getChargeCode().equalsIgnoreCase(acclineDis.getChargeCode()))&&(discountRec.getContract()!=null)&&(discountRec.getContract().equalsIgnoreCase(billing.getContract()))){
							tempData.put(discountRec.getDiscount()+"", discountRec.getDescription());
						}
					}
					discountMap.put(acclineDis.getId()+"", tempData);
				}
			}
			
			timeTaken = System.currentTimeMillis()-startTime;
			logger.warn("\n\nTime taken to find All Pricing by serviceOrderId: "+timeTaken+"\n\n");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return CANCEL;
		}
 		return SUCCESS;
 	}
	
	@SkipValidation
	public void getPricingHelper(){
		try {
			serviceOrder = serviceOrderManager.get(sid);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			/*if(accUpdateAjax!=null && accUpdateAjax.equalsIgnoreCase("T")){
				//updateAccountLineAjax(serviceOrder);
			}*/
			try {
				billing =billingManager.get(sid);
				trackingStatus=trackingStatusManager.get(sid);
				miscellaneous=miscellaneousManager.get(sid);
				if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
					weightType="lbscft";
				}else{
					weightType="kgscbm";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			customerFile = serviceOrder.getCustomerFile();
			ownBookingAgentCode = companyDivisionManager.checkBookingAgentCode(serviceOrder.getBookingAgentCode(), sessionCorpID);
		    if(ownBookingAgentCode == null || ownBookingAgentCode.isEmpty()){
		    ownBookingAgentFlag=true;  	
		    }
			  
			shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
			minShip =  customerFileManager.findMinimumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
			countShip =  customerFileManager.findCountShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
			compDivForBookingAgentList = companyDivisionManager.checkBookingAgentCode(serviceOrder.getBookingAgentCode(), sessionCorpID);
			
			commissionJobName="No";
			if(jobName!=null && !jobName.equalsIgnoreCase("") && jobName.contains(serviceOrder.getJob())){
				commissionJobName = "Yes";
			}
			 
			baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID); 
			
		    contractType=false;
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String	contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
			    }
			    if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			      	billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
			      }
				 if(contractType && trackingStatus.getAccNetworkGroup() && billingCMMContractType ){
						Set<String> billToSet = new HashSet<String>();
						
						List billToCodeNtList = accountLineManager.getBillToCode(serviceOrder.getShipNumber(),serviceOrder.getId(),trackingStatus.getNetworkPartnerCode());
						if(billToCodeNtList != null && billToCodeNtList.size() > 0){
							for (int i = 0; i < billToCodeNtList.size(); i++) {
								String partnertype=  partnerManager.checkPartnerType(billToCodeNtList.get(i).toString());
								if("".equals(partnertype.trim())){
									billToSet.add(billToCodeNtList.get(i).toString());
								}
							}
							billToLength = billToSet.size();
						}
					}	
		    //getNotesForIconChange();
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		
   }
	private String systemDefaultVatCalculation;
	private String accountLineSortValue;
	@SkipValidation
	public String pricingList(){

		try {
			accountLineSortValue = "asc";
			serviceOrder = serviceOrderManager.get(sid);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			billing =billingManager.get(sid);
			trackingStatus=trackingStatusManager.get(sid);
			miscellaneous=miscellaneousManager.get(sid);
			String permKey = sessionCorpID +"-"+"component.accountline.accrualbutton.show";
			checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
			if(checkFieldVisibility){
			Set<ServicePartner> servicePartner = serviceOrder.getServicePartners();
			ServicePartner servicePartner1 = new ServicePartner();
			accrualReadyContractFlag=true;
			Iterator itr = servicePartner.iterator();
				while (itr.hasNext()){
					servicePartner1=null;
					servicePartner1 = (ServicePartner)itr.next();
					if(servicePartner1.isStatus() && servicePartner1.getOmni()!=null && (!(servicePartner1.getOmni().toString().trim().equals("")))){
						accrualReadyContractFlag=false;
					} 
			    }
				String billToCode = billing.getBillToCode(); 
				minMargin = partnerPrivateManager.getminMargin(billToCode); 
			}

			try {
				
				contractType=false;
				if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
					String	contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
					if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
						contractType=true;	
					}
				}
				if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				  	billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
				  }
				if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
					  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
				 }
				 if(contractType && trackingStatus.getAccNetworkGroup() && billingCMMContractType ){
						Set<String> billToSet = new HashSet<String>();
						
						List billToCodeNtList = accountLineManager.getBillToCode(serviceOrder.getShipNumber(),serviceOrder.getId(),trackingStatus.getNetworkPartnerCode());
						if(billToCodeNtList != null && billToCodeNtList.size() > 0){
							for (int i = 0; i < billToCodeNtList.size(); i++) {
								String partnertype=  partnerManager.checkPartnerType(billToCodeNtList.get(i).toString());
								if("".equals(partnertype.trim())){
									billToSet.add(billToCodeNtList.get(i).toString());
								}
							}
							billToLength = billToSet.size();
						}
					}
				contractBillList = billingManager.findBillContractAccountList("",billing.getBillToCode(), serviceOrder.getJob(), billing.getCreatedOn(), sessionCorpID,serviceOrder.getCompanyDivision(),serviceOrder.getBookingAgentCode());
				for(int i = 0;i<contractBillList.size();i++){
					billingContractMap.put((String) contractBillList.get(i), (String) contractBillList.get(i));
				}
				
				if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
					weightType="lbscft";
				}else{
					weightType="kgscbm";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			ownBookingAgentCode = companyDivisionManager.checkBookingAgentCode(serviceOrder.getBookingAgentCode(), sessionCorpID);
		    if(ownBookingAgentCode == null || ownBookingAgentCode.isEmpty()){
		    	ownBookingAgentFlag=true;  	
		    }
			customerFile = serviceOrder.getCustomerFile();
			shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
			minShip =  customerFileManager.findMinimumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
			countShip =  customerFileManager.findCountShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
			 
			//compDivForBookingAgentList = companyDivisionManager.checkBookingAgentCode(serviceOrder.getBookingAgentCode(), sessionCorpID);
			baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID);
			commissionJobName="No";
			if(jobName!=null && !jobName.equalsIgnoreCase("") && jobName.contains(serviceOrder.getJob())){
				commissionJobName = "Yes";
			}
		    if(accountLineStatus == null || "".equals(accountLineStatus)){
				accountLineStatus="true";
			}
		    
		    accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
		    if(accountLineList == null || accountLineList.isEmpty()){
				emptyList="true";
			}
			companyDivis.add("");
			companyDivis.addAll(customerFileManager.findCompanyDivisionByBookAgAndCompanyDivision(serviceOrder.getCorpID(), "",serviceOrder.getCompanyDivision()));
			Iterator<AccountLine> idIterator =  accountLineList.iterator(); 
			accountLineList=new ArrayList();
			while (idIterator.hasNext()){
				AccountLine accountLine = (AccountLine) idIterator.next(); 
				try{
					if(accountLine.getCompanyDivision()!=null && (!(accountLine.getCompanyDivision().equals("")))){	
						if(companyDivis.contains(accountLine.getCompanyDivision())){
						}else{
							companyDivis.add(accountLine.getCompanyDivision());	
						}
					}
				}catch(Exception e){}
				
				try{
					  if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup()){
						   String networkSynchedId=	accountLineManager.getNetworkSynchedId(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
					      	if(!(networkSynchedId.equals(""))){
						      		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
						      		Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
						      		boolean accNonEditable=false;
									 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
										 accNonEditable =true;
									 }
									 if(accNonEditable){
										 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
							      			 //utsiRecAccDateFlag=true;
											 accountLine.setUtsiRecAccDateFlag(true);
							      		}
										 if(synchedAccountLine.getInvoiceNumber()!=null && (!(synchedAccountLine.getInvoiceNumber().toString().toString().equals("")))){
							      			 //utsiPayAccDateFlag=true;
											 accountLine.setUtsiPayAccDateFlag(true);
							      		}
									 }else{
							      		if(synchedAccountLine.getRecAccDate()!=null && accountLine.getRecAccDate()== null){
							      			 //utsiRecAccDateFlag=true;
							      			accountLine.setUtsiRecAccDateFlag(true);
							      		}
							      		if(synchedAccountLine.getPayAccDate()!=null && accountLine.getRecAccDate()== null){
							      			 //utsiPayAccDateFlag=true;
							      			accountLine.setUtsiPayAccDateFlag(true);
							      		}
								} 	
					      	} 
					      	accountLineList.add(accountLine);
					  	}else{
					  		accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
					  	}
					}catch(Exception e){}
			}
			for(int i = 0;i<companyDivis.size();i++){
				companyDivisionMap.put((String) companyDivis.get(i), (String) companyDivis.get(i));
			}
			
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String	contractTypeValue="";
				contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}
			}
			//baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID); 
		    
			if((accountLineList!=null)&&(!accountLineList.isEmpty())){
				discountMap=new HashMap<String, Map<String,String>>();
				List<AccountLine> cc =accountLineList;
				List<Discount> ds=null;
				Map<String,String> tempData=null;
				ds=discountManager.discountMapList(sessionCorpID);
				for(AccountLine acclineDis:cc){
					//ds=discountManager.discountList(billing.getContract(),acclineDis.getChargeCode(),sessionCorpID);
					tempData=new HashMap<String, String>();
					tempData.put("", "");
					for(Discount discountRec:ds){
						if((discountRec.getChargeCode()!=null)&&(discountRec.getChargeCode().equalsIgnoreCase(acclineDis.getChargeCode()))&&(discountRec.getContract()!=null)&&(discountRec.getContract().equalsIgnoreCase(billing.getContract()))){
							tempData.put(discountRec.getDiscount()+"", discountRec.getDescription());
						}
					}
					discountMap.put(acclineDis.getId()+"", tempData);
				}
			}
		    updateAccountLineAjax(serviceOrder); 
		    
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}		
		return SUCCESS;
	}
	
	 @SkipValidation
	 	public String findAccountlineActiveListAjax(){ 
			try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				serviceOrder=serviceOrderManager.get(sid);
				getRequest().setAttribute("soLastName",serviceOrder.getLastName());
				billing = billingManager.get(sid);
				miscellaneous=miscellaneousManager.get(sid);
				trackingStatus=trackingStatusManager.get(sid);
				if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
					weightType="lbscft";
				}else{
					weightType="kgscbm";
				}
				if(accountLineStatus == null || "".equals(accountLineStatus)){
					accountLineStatus="true";
				}
				accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
				if(accountLineList == null || accountLineList.isEmpty()){
					emptyList="true";
				}
				
				companyDivis.add("");
				companyDivis.addAll(customerFileManager.findCompanyDivisionByBookAgAndCompanyDivision(serviceOrder.getCorpID(), "",serviceOrder.getCompanyDivision()));
 				Iterator<AccountLine> idIterator =  accountLineList.iterator();
 				accountLineList=new ArrayList();
				while (idIterator.hasNext()){
					AccountLine accountLine = (AccountLine) idIterator.next(); 
					try{
						if(accountLine.getCompanyDivision()!=null && (!(accountLine.getCompanyDivision().equals("")))){	
							if(companyDivis.contains(accountLine.getCompanyDivision())){
							}else{
								companyDivis.add(accountLine.getCompanyDivision());	
							}
						}
					}catch(Exception e){}
					
					try{
						  if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup())	{
							   String networkSynchedId=	accountLineManager.getNetworkSynchedId(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
						      	if(!(networkSynchedId.equals(""))){
						      		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
						      		Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
						      		boolean accNonEditable=false;
									 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
										 accNonEditable =true;
									 }
									 if(accNonEditable){
										 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
							      			 //utsiRecAccDateFlag=true;
											 accountLine.setUtsiRecAccDateFlag(true);
							      		}
										 if(synchedAccountLine.getInvoiceNumber()!=null && (!(synchedAccountLine.getInvoiceNumber().toString().toString().equals("")))){
							      			 //utsiPayAccDateFlag=true;
											 accountLine.setUtsiPayAccDateFlag(true);
							      		}
									 }else{
						      		if(synchedAccountLine.getRecAccDate()!=null && accountLine.getRecAccDate()== null){
						      			 //utsiRecAccDateFlag=true;
						      			accountLine.setUtsiRecAccDateFlag(true);
						      		}
						      		if(synchedAccountLine.getPayAccDate()!=null && accountLine.getRecAccDate()== null){
						      			 //utsiPayAccDateFlag=true;
						      			accountLine.setUtsiPayAccDateFlag(true);
						      		}
								} 	
						      	} 
						      	accountLineList.add(accountLine);
						  	}else{
						  		accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
						  	}
						}catch(Exception e){}
					
 				}
				for(int i = 0;i<companyDivis.size();i++){
					companyDivisionMap.put((String) companyDivis.get(i), (String) companyDivis.get(i));
				}
 				
				if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
					String	contractTypeValue="";
					contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
					if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
						contractType=true;	
					}
				}
				 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
					  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
				    }
				    if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				      	billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
				      }
					 if(contractType && trackingStatus.getAccNetworkGroup() && billingCMMContractType ){
							Set<String> billToSet = new HashSet<String>();
							
							List billToCodeNtList = accountLineManager.getBillToCode(serviceOrder.getShipNumber(),serviceOrder.getId(),trackingStatus.getNetworkPartnerCode());
							if(billToCodeNtList != null && billToCodeNtList.size() > 0){
								for (int i = 0; i < billToCodeNtList.size(); i++) {
									String partnertype=  partnerManager.checkPartnerType(billToCodeNtList.get(i).toString());
									if("".equals(partnertype.trim())){
										billToSet.add(billToCodeNtList.get(i).toString());
									}
								}
								billToLength = billToSet.size();
							}
						}	
				contractBillList = billingManager.findBillContractAccountList("",billing.getBillToCode(), serviceOrder.getJob(), billing.getCreatedOn(), sessionCorpID,serviceOrder.getCompanyDivision(),serviceOrder.getBookingAgentCode());
				for(int i = 0;i<contractBillList.size();i++){
					billingContractMap.put((String) contractBillList.get(i), (String) contractBillList.get(i));
				}
				//baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID); 
			    
				if((accountLineList!=null)&&(!accountLineList.isEmpty())){
					discountMap=new HashMap<String, Map<String,String>>();
					List<AccountLine> cc =accountLineList;
					List<Discount> ds=null;
					Map<String,String> tempData=null;
					ds=discountManager.discountMapList(sessionCorpID);
					for(AccountLine acclineDis:cc){
						//ds=discountManager.discountList(billing.getContract(),acclineDis.getChargeCode(),sessionCorpID);
						tempData=new HashMap<String, String>();
						tempData.put("", "");
						for(Discount discountRec:ds){
							if((discountRec.getChargeCode()!=null)&&(discountRec.getChargeCode().equalsIgnoreCase(acclineDis.getChargeCode()))&&(discountRec.getContract()!=null)&&(discountRec.getContract().equalsIgnoreCase(billing.getContract()))){
								tempData.put(discountRec.getDiscount()+"", discountRec.getDescription());
							}
						}
						discountMap.put(acclineDis.getId()+"", tempData);
					}
				}
			    
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	e.printStackTrace();
		    	return "errorlog";
			}
			return SUCCESS;
		}
	  @SkipValidation
	 	public String findAccountlineAllListAjax(){ 
			try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				accountLineStatus = getRequest().getParameter("accountLineStatus");
				serviceOrder=serviceOrderManager.get(sid);
				getRequest().setAttribute("soLastName",serviceOrder.getLastName());
				billing = billingManager.get(sid);
				trackingStatus=trackingStatusManager.get(sid);
				miscellaneous=miscellaneousManager.get(sid);
				if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
					weightType="lbscft";
				}else{
					weightType="kgscbm";
				}
				if(accountLineStatus == null || "".equals(accountLineStatus)){
					accountLineStatus="true";
				}
				accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
				if(accountLineList == null || accountLineList.isEmpty()){
					emptyList="true";
				}
				companyDivis.add("");
				companyDivis.addAll(customerFileManager.findCompanyDivisionByBookAgAndCompanyDivision(serviceOrder.getCorpID(), "",serviceOrder.getCompanyDivision()));
 				Iterator<AccountLine> idIterator =  accountLineList.iterator();
 				accountLineList=new ArrayList();
				while (idIterator.hasNext()){
					AccountLine accountLine = (AccountLine) idIterator.next(); 
					try{
						if(accountLine.getCompanyDivision()!=null && (!(accountLine.getCompanyDivision().equals("")))){	
							if(companyDivis.contains(accountLine.getCompanyDivision())){
							}else{
								companyDivis.add(accountLine.getCompanyDivision());	
							}
						}
					}catch(Exception e){}
					
					try{
						  if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup())	{
							   String networkSynchedId=	accountLineManager.getNetworkSynchedId(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
						      	if(!(networkSynchedId.equals(""))){
						      		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
						      		Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
						      		boolean accNonEditable=false;
									 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
										 accNonEditable =true;
									 }
									 if(accNonEditable){
										 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
							      			 //utsiRecAccDateFlag=true;
											 accountLine.setUtsiRecAccDateFlag(true);
							      		}
										 if(synchedAccountLine.getInvoiceNumber()!=null && (!(synchedAccountLine.getInvoiceNumber().toString().toString().equals("")))){
							      			 //utsiPayAccDateFlag=true;
											 accountLine.setUtsiPayAccDateFlag(true);
							      		}
									 }else{
						      		if(synchedAccountLine.getRecAccDate()!=null && accountLine.getRecAccDate()== null){
						      			 //utsiRecAccDateFlag=true;
						      			accountLine.setUtsiRecAccDateFlag(true);
						      		}
						      		if(synchedAccountLine.getPayAccDate()!=null && accountLine.getRecAccDate()== null){
						      			 //utsiPayAccDateFlag=true;
						      			accountLine.setUtsiPayAccDateFlag(true);
						      		}
								} 	
						      	} 
						      	accountLineList.add(accountLine);
						  	}else{
						  		accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
						  	}
						}catch(Exception e){}
 				}
				for(int i = 0;i<companyDivis.size();i++){
					companyDivisionMap.put((String) companyDivis.get(i), (String) companyDivis.get(i));
				}
 				
				if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
					String	contractTypeValue="";
					contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
					if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
						contractType=true;	
					}
				}
				 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
					  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
				    }
				    if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				      	billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
				      }
					 if(contractType && trackingStatus.getAccNetworkGroup() && billingCMMContractType ){
							Set<String> billToSet = new HashSet<String>();
							
							List billToCodeNtList = accountLineManager.getBillToCode(serviceOrder.getShipNumber(),serviceOrder.getId(),trackingStatus.getNetworkPartnerCode());
							if(billToCodeNtList != null && billToCodeNtList.size() > 0){
								for (int i = 0; i < billToCodeNtList.size(); i++) {
									String partnertype=  partnerManager.checkPartnerType(billToCodeNtList.get(i).toString());
									if("".equals(partnertype.trim())){
										billToSet.add(billToCodeNtList.get(i).toString());
									}
								}
								billToLength = billToSet.size();
							}
						}	
				contractBillList = billingManager.findBillContractAccountList("",billing.getBillToCode(), serviceOrder.getJob(), billing.getCreatedOn(), sessionCorpID,serviceOrder.getCompanyDivision(),serviceOrder.getBookingAgentCode());
				for(int i = 0;i<contractBillList.size();i++){
					billingContractMap.put((String) contractBillList.get(i), (String) contractBillList.get(i));
				}
				//baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID);
			    
				if((accountLineList!=null)&&(!accountLineList.isEmpty())){
					discountMap=new HashMap<String, Map<String,String>>();
					List<AccountLine> cc =accountLineList;
					List<Discount> ds=null;
					Map<String,String> tempData=null;
					ds=discountManager.discountMapList(sessionCorpID);
					for(AccountLine acclineDis:cc){
						//ds=discountManager.discountList(billing.getContract(),acclineDis.getChargeCode(),sessionCorpID);
						tempData=new HashMap<String, String>();
						tempData.put("", "");
						for(Discount discountRec:ds){
							if((discountRec.getChargeCode()!=null)&&(discountRec.getChargeCode().equalsIgnoreCase(acclineDis.getChargeCode()))&&(discountRec.getContract()!=null)&&(discountRec.getContract().equalsIgnoreCase(billing.getContract()))){
								tempData.put(discountRec.getDiscount()+"", discountRec.getDescription());
							}
						}
						discountMap.put(acclineDis.getId()+"", tempData);
					}
				}
				
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	e.printStackTrace();
		    	return "errorlog";
			}
			return SUCCESS;
		}
	  private String accountLineQuery;
	  private String accountLineOrderByVal;
	  @SkipValidation
	 	public String findAccountLineListByFilterAjax(){ 
			try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				serviceOrder=serviceOrderManager.get(sid);
				getRequest().setAttribute("soLastName",serviceOrder.getLastName());
				billing = billingManager.get(sid);
				miscellaneous=miscellaneousManager.get(sid);
				trackingStatus=trackingStatusManager.get(sid);
				if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
					weightType="lbscft";
				}else{
					weightType="kgscbm";
				}
				if(accountLineStatus == null || "".equals(accountLineStatus)){
					accountLineStatus="true";
				}
				accountLineList = accountLineManager.findAccountLineListByFilter(accountLineQuery,accountLineStatus,accountLineOrderByVal);
				if(accountLineList == null || accountLineList.isEmpty()){
					emptyList="true";
				}
				
				companyDivis.add("");
				companyDivis.addAll(customerFileManager.findCompanyDivisionByBookAgAndCompanyDivision(serviceOrder.getCorpID(), "",serviceOrder.getCompanyDivision()));
				Iterator<AccountLine> idIterator =  accountLineList.iterator();
				accountLineList=new ArrayList();
				while (idIterator.hasNext()){
					AccountLine accountLine = (AccountLine) idIterator.next(); 
					try{
						if(accountLine.getCompanyDivision()!=null && (!(accountLine.getCompanyDivision().equals("")))){	
							if(companyDivis.contains(accountLine.getCompanyDivision())){
							}else{
								companyDivis.add(accountLine.getCompanyDivision());	
							}
						}
					}catch(Exception e){}
					
					try{
						  if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup())	{
							   String networkSynchedId=	accountLineManager.getNetworkSynchedId(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
						      	if(!(networkSynchedId.equals(""))){
						      		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
						      		Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
						      		boolean accNonEditable=false;
									 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
										 accNonEditable =true;
									 }
									 if(accNonEditable){
										 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
							      			 //utsiRecAccDateFlag=true;
											 accountLine.setUtsiRecAccDateFlag(true);
							      		}
										 if(synchedAccountLine.getInvoiceNumber()!=null && (!(synchedAccountLine.getInvoiceNumber().toString().toString().equals("")))){
							      			 //utsiPayAccDateFlag=true;
											 accountLine.setUtsiPayAccDateFlag(true);
							      		}
									 }else{
						      		if(synchedAccountLine.getRecAccDate()!=null && accountLine.getRecAccDate()== null){
						      			 //utsiRecAccDateFlag=true;
						      			accountLine.setUtsiRecAccDateFlag(true);
						      		}
						      		if(synchedAccountLine.getPayAccDate()!=null && accountLine.getRecAccDate()== null){
						      			 //utsiPayAccDateFlag=true;
						      			accountLine.setUtsiPayAccDateFlag(true);
						      		}
								} 	
						      	} 
						      	accountLineList.add(accountLine);
						  	}else{
						  		accountLineList = accountLineManager.findAccountLineListByFilter(accountLineQuery,accountLineStatus,accountLineOrderByVal);
						  	}
						}catch(Exception e){}
					
				}
				for(int i = 0;i<companyDivis.size();i++){
					companyDivisionMap.put((String) companyDivis.get(i), (String) companyDivis.get(i));
				}
				
				if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
					String	contractTypeValue="";
					contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
					if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
						contractType=true;	
					}
				}
				 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
					  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
				    }
				    if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				      	billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
				      }
					 if(contractType && trackingStatus.getAccNetworkGroup() && billingCMMContractType ){
							Set<String> billToSet = new HashSet<String>();
							
							List billToCodeNtList = accountLineManager.getBillToCode(serviceOrder.getShipNumber(),serviceOrder.getId(),trackingStatus.getNetworkPartnerCode());
							if(billToCodeNtList != null && billToCodeNtList.size() > 0){
								for (int i = 0; i < billToCodeNtList.size(); i++) {
									String partnertype=  partnerManager.checkPartnerType(billToCodeNtList.get(i).toString());
									if("".equals(partnertype.trim())){
										billToSet.add(billToCodeNtList.get(i).toString());
									}
								}
								billToLength = billToSet.size();
							}
						}	
				contractBillList = billingManager.findBillContractAccountList("",billing.getBillToCode(), serviceOrder.getJob(), billing.getCreatedOn(), sessionCorpID,serviceOrder.getCompanyDivision(),serviceOrder.getBookingAgentCode());
				for(int i = 0;i<contractBillList.size();i++){
					billingContractMap.put((String) contractBillList.get(i), (String) contractBillList.get(i));
				}
				//baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID); 
			    
				if((accountLineList!=null)&&(!accountLineList.isEmpty())){
					discountMap=new HashMap<String, Map<String,String>>();
					List<AccountLine> cc =accountLineList;
					List<Discount> ds=null;
					Map<String,String> tempData=null;
					ds=discountManager.discountMapList(sessionCorpID);
					for(AccountLine acclineDis:cc){
						//ds=discountManager.discountList(billing.getContract(),acclineDis.getChargeCode(),sessionCorpID);
						tempData=new HashMap<String, String>();
						tempData.put("", "");
						for(Discount discountRec:ds){
							if((discountRec.getChargeCode()!=null)&&(discountRec.getChargeCode().equalsIgnoreCase(acclineDis.getChargeCode()))&&(discountRec.getContract()!=null)&&(discountRec.getContract().equalsIgnoreCase(billing.getContract()))){
								tempData.put(discountRec.getDiscount()+"", discountRec.getDescription());
							}
						}
						discountMap.put(acclineDis.getId()+"", tempData);
					}
				}
				if((fieldName!=null && !fieldName.equals("")) && (!fieldName.equalsIgnoreCase("payNotInvoiced") && !fieldName.equalsIgnoreCase("recNotInvoiced"))){
					fieldList =  accountLineManager.findDistinctFieldValue(serviceOrder.getShipNumber(),fieldName,accountLineStatus);
				}
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	e.printStackTrace();
		    	return "errorlog";
			}
			return SUCCESS;
		}
	  private List fieldList = new ArrayList();
	  @SkipValidation
	 	public String findAccountLineFieldValueListAjax(){ 
			try {
				fieldList =  accountLineManager.findDistinctFieldValue(shipNumber,fieldName,accountLineStatus);
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	e.printStackTrace();
		    	return "errorlog";
			}
			return SUCCESS;
		}
	private String fieldName;
	private String returnAjaxStringValue;
	private String userName;
	private Date accrualDate;
	private String accrualDateDel;
   public String autoFillAccrualFields(){
	   	 //company= companyManager.findByCorpID(sessionCorpID).get(0);
	     userName= getRequest().getRemoteUser();
	     if(accrualDateDel==null || accrualDateDel.trim().equals("")){
	    	 accrualDateDel="";	 
	     if(accrualDate==null){
	    	 accrualDate=new Date(); 
	     }
	     }
	     //System.out.println("\n\n\n\n accrualDateDel "+accrualDateDel+"accrualDate " +accrualDate);
	     serviceOrderManager.updateAccrualFields(sid,accrualDate,userName,sessionCorpID,roleSupervisor,accrualDateDel);
	     Date date = new Date();
	     SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
	     //sdf.setTimeZone(TimeZone.getTimeZone(company.getTimeZone()));
	     String date1="";
	     if(roleSupervisor.equals("Y")){
		     if(accrualDate!=null){
		    	 date1= sdf.format(accrualDate);
		     }else{
		    	 date1="";
		     }
	     }else{
	    	  date1 = sdf.format(date); 
	     }
	     returnAjaxStringValue = userName+"~"+date1;
	     return SUCCESS;   
   }
   
   public String checkJobForCoordinator(){
	   returnAjaxStringValue = serviceOrderManager.JobForCoordinator(shipNumber, sessionCorpID);
	   if(!returnAjaxStringValue.isEmpty()){
		   returnAjaxStringValue = "Show";
	   }
	   return SUCCESS;
   }
   
	@SkipValidation
	public String savePricingAjax(){

		try {
			Long startTime = System.currentTimeMillis();
			String user = getRequest().getRemoteUser();
			
			getPricingHelper();
			List modeValue=serviceOrderManager.findPriceCode(serviceOrder.getMode(), "MODE",sessionCorpID);
			String billingContract = billing.getContract();
			if(billingContract != null && !"".equals(billingContract)){
				String contractTypeValue = accountLineManager.checkContractType(billingContract,sessionCorpID);
				if(contractTypeValue != null && (!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}
			}
			if(accountLineStatus == null || accountLineStatus.equals("")){
				accountLineStatus="true";
			}
			 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			  	billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
			  }
			contractBillList = billingManager.findBillContractAccountList("",billing.getBillToCode(), serviceOrder.getJob(), billing.getCreatedOn(), sessionCorpID,serviceOrder.getCompanyDivision(),serviceOrder.getBookingAgentCode());
			for(int i = 0;i<contractBillList.size();i++){
				billingContractMap.put((String) contractBillList.get(i), (String) contractBillList.get(i));
			}
			
			BigDecimal totalExp = new BigDecimal(0);
			Map<String, String> vendorCodeVatMap=accountLineManager.getVendorVatCodeMap(sessionCorpID, serviceOrder.getShipNumber());
			if(pricingData != null && pricingData.trim().length() > 0){
				JSONObject objectInArray = null;
				JSONArray tempObj=new JSONObject(pricingData).getJSONArray("records");
			    for (int i = 0, size = tempObj.length(); i < size; i++){
			      	objectInArray = tempObj.getJSONObject(i);
			        Long id = Long.valueOf(objectInArray.optString("id").trim());
					String currencyValue=""  ;
					String contactCurrency = "";
					String chargeStr = "";
					String vatCodePersent="";
					Boolean bb=false;
					String tempActgCode="";
					
					if(billingContract != null && !"".equals(billingContract.trim())){
						  List<Charges> chargeList =chargesManager.findChargesList(objectInArray.optString("chargeCode"), billingContract, sessionCorpID);
						  for (Charges charge : chargeList) {
							  bb=charge.getVATExclude();
						  }
					  }
					try{
						  if(!objectInArray.optString("vendorCode").isEmpty()){
							    if(bb!=null && (!(bb))){ 
									String vatCode=vendorCodeVatMap.get(objectInArray.optString("vendorCode"));			
									if((vatCode!=null)&&(!vatCode.equalsIgnoreCase(""))){
										vatCodePersent=vatCode+"~"+payVatPercentList.get(vatCode);
									}
								}
						  }
					  }catch(Exception e){e.printStackTrace();}
						  if(!costElementFlag){
								quotationDiscriptionList=	accountLineManager.findQuotationDiscription(billingContract,objectInArray.optString("chargeCode"),sessionCorpID);
						  }else{
								quotationDiscriptionList = accountLineManager.findChargeDetailFromSO(billingContract,objectInArray.optString("chargeCode"),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
						  }
					  if(quotationDiscriptionList!=null && !quotationDiscriptionList.isEmpty() && quotationDiscriptionList.get(0)!=null && !quotationDiscriptionList.get(0).toString().equalsIgnoreCase("NoDiscription")){
						  chargeStr= quotationDiscriptionList.get(0).toString();
					  }
					  List l1ExRate=null;
					  List l2ExRate=null;
					  if(chargeStr.split("#").length>3){
						  l1ExRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,chargeStr.split("#")[3]);
					  }
					  if(chargeStr.split("#").length>4){
					  	  l2ExRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,chargeStr.split("#")[4]);
					  }
					  /* Start - 9569 - Issue in 'Ignore Invoicing' */
					 /* try{
					  if(!objectInArray.optString("category").isEmpty() && objectInArray.optString("category").equalsIgnoreCase("Internal Cost")){
							 if(!costElementFlag)	{    
								 chargeCodeList=chargesManager.findInternalCostChargeCodeChargeCode(objectInArray.optString("chargeCode"), serviceOrder.getCompanyDivision(),sessionCorpID);
							 }else{
								 chargeCodeList=chargesManager.findInternalCostChargeCodeCostElement(objectInArray.optString("chargeCode"), serviceOrder.getCompanyDivision(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision()); 	 
							 }
						  }else{
						     if(billing.getContract()!=null && !billing.getContract().equalsIgnoreCase("")){
							  if(!costElementFlag)	{
								  chargeCodeList=chargesManager.findChargeCode(objectInArray.optString("chargeCode"), billing.getContract());
							  }
							  else{
								  chargeCodeList=chargesManager.findChargeCodeCostElement(objectInArray.optString("chargeCode"), billing.getContract(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());  
							  }
						    }
						  }
					  }catch(Exception e){}*/
					  
    					 try{
	    					 if(!objectInArray.optString("vendorCode").isEmpty()){
								  String actCode="";							
									String companyDivisionAcctgCodeUnique="";
									 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
									 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
										 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
									 }
							         if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
							 			actCode=partnerManager.getAccountCrossReference(objectInArray.optString("vendorCode"),serviceOrder.getCompanyDivision(),sessionCorpID);
							 		}else{
							 			actCode=partnerManager.getAccountCrossReferenceUnique(objectInArray.optString("vendorCode"),sessionCorpID);
							 		}
							         tempActgCode=actCode;					    
							  }
    					 }catch(Exception e){e.printStackTrace();}								  
    					 AccountLine accountLine = accountLineManager.get(id);
    					 try {
    						 AccountLine accTemp = new AccountLine();
    						 String chargeCodeTemp1=objectInArray.optString("chargeCode");
							  String oldChargeCode=accountLine.getChargeCode();
							  if(chargeCodeTemp1==null){chargeCodeTemp1="";}
							  if(oldChargeCode==null){oldChargeCode="";}
							  String descriptionChangeFlag="N";
							  if(!objectInArray.optString("chargeCode").equalsIgnoreCase(oldChargeCode)){
								  descriptionChangeFlag="Y";
							  }    
							  
								accountLine.setCategory(objectInArray.optString("category"));
								accTemp.setChargeCode(objectInArray.optString("chargeCode"));
								accountLine.setVendorCode(objectInArray.optString("vendorCode"));
								accountLine.setEstimateVendorName(objectInArray.optString("estimateVendorName"));
								accountLine.setBasis((objectInArray.optString("basis").isEmpty())?"":objectInArray.optString("basis").replace("Percentage", "%"));
								accountLine.setEstimateQuantity((objectInArray.optString("estimateQuantity").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimateQuantity").trim()));
								accountLine.setEstimateRate((objectInArray.optString("estimateRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimateRate").trim()));
								accountLine.setEstimateSellRate((objectInArray.optString("estimateSellRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimateSellRate").trim()));
								accountLine.setEstimateExpense((objectInArray.optString("estimateExpense").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimateExpense").trim()));
								accountLine.setEstimatePassPercentage((objectInArray.optString("estimatePassPercentage").isEmpty()) ? new Integer(0) : new Integer(objectInArray.optString("estimatePassPercentage").trim()));
								accountLine.setEstimateRevenueAmount((objectInArray.optString("estimateRevenueAmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimateRevenueAmount").trim()));
								accountLine.setQuoteDescription(objectInArray.optString("quoteDescription"));
								accountLine.setIncludeLHF((objectInArray.optString("includeLHF").isEmpty()) ? false : Boolean.parseBoolean(objectInArray.optString("includeLHF")));
								accountLine.setIgnoreForBilling((objectInArray.optString("ignoreForBilling").isEmpty()) ? false : Boolean.parseBoolean(objectInArray.optString("ignoreForBilling")));
								try{
								//accountLine.setRollUpInInvoice((objectInArray.optString("rollUpInInvoice").isEmpty()) ? false : Boolean.parseBoolean(objectInArray.optString("rollUpInInvoice")));
								}catch(Exception e){}
								//accountLine.setStatus((objectInArray.optString("status").isEmpty()) ? false : Boolean.parseBoolean(objectInArray.optString("status")));
								//accountLine.setAuthorization("authorization");
								accountLine.setDivision(objectInArray.optString("division"));
								accountLine.setEstVatDescr(objectInArray.optString("vatDecr"));
								accountLine.setEstVatPercent(objectInArray.optString("vatPers"));
								accountLine.setBillToCode(objectInArray.optString("billToCode"));
								accountLine.setBillToName(objectInArray.optString("billToName"));
								try{
									accountLine.setEstExpVatAmt((objectInArray.optString("estExpVatAmt").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estExpVatAmt").trim()));
								}catch(Exception e){}
								accountLine.setEstVatAmt((objectInArray.optString("vatAmt").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("vatAmt").trim()));
								accountLine.setDeviation(objectInArray.optString("deviation")); 
								accountLine.setEstimateDeviation((objectInArray.optString("estimateDeviation").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimateDeviation").trim()));
								accountLine.setEstimateSellDeviation((objectInArray.optString("estimateSellDeviation").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimateSellDeviation").trim()));
								accountLine.setRevisionDeviation((objectInArray.optString("revisionDeviation").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionDeviation").trim()));
								accountLine.setRevisionSellDeviation((objectInArray.optString("revisionSellDeviation").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionSellDeviation").trim()));
								accountLine.setReceivableSellDeviation((objectInArray.optString("receivableSellDeviation").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("receivableSellDeviation").trim()));
								accountLine.setPayDeviation((objectInArray.optString("payDeviation").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("payDeviation").trim()));
								try{
									if(vatCodePersent!=null && !vatCodePersent.trim().equalsIgnoreCase("") && !vatCodePersent.trim().equalsIgnoreCase("~")){
										accountLine.setPayVatDescr(vatCodePersent.split("~")[0]);
										accountLine.setPayVatPercent(vatCodePersent.split("~")[1]);											
									}
								  }catch(Exception e){e.printStackTrace();}
								 
								accountLine.setRecGl(objectInArray.optString("recGl")); 
								accountLine.setPayGl(objectInArray.optString("payGl"));
								  /*if(quotationDiscriptionList!=null && !quotationDiscriptionList.isEmpty() && quotationDiscriptionList.get(0)!=null && !quotationDiscriptionList.get(0).toString().equalsIgnoreCase("NoDiscription")){
									  chargeStr= quotationDiscriptionList.get(0).toString();
								  }
							   if(!chargeStr.equalsIgnoreCase("")){
									  String [] chrageDetailArr = chargeStr.split("#");
									  if(accountLine.getRecPostDate()==null){
										  accountLine.setRecGl(chrageDetailArr[1]);
									  }
									  if(accountLine.getPayPostDate()==null){
										  accountLine.setPayGl(chrageDetailArr[2]);
									  }				      						  
									  contactCurrency = chrageDetailArr[3];
									  currencyValue = chrageDetailArr[4];
									  
								  }else{
									  accountLine.setRecGl("");
									  accountLine.setPayGl("");
								  }*/
							   String chargeCode = accTemp.getChargeCode();
							   if(contractType && !chargeCode.equalsIgnoreCase(accountLine.getChargeCode())){
								  String decimalValue="0.00";
								  String ExchangeRateDecimalValue="0.00";
								  
								   accountLine.setContractCurrency(contactCurrency);
								   accountLine.setPayableContractCurrency(currencyValue);
								   accountLine.setEstimatePayableContractCurrency(currencyValue); 
								   accountLine.setEstimateContractCurrency(contactCurrency); 
								   accountLine.setRevisionPayableContractCurrency(currencyValue); 
								   accountLine.setRevisionContractCurrency(contactCurrency); 
								   if(l1ExRate!=null && !l1ExRate.isEmpty() && l1ExRate.get(0)!=null && !l1ExRate.get(0).toString().equals("") ){
									   decimalValue = l1ExRate.get(0).toString();
								   }
								   if(l2ExRate!=null && !l2ExRate.isEmpty() && l2ExRate.get(0)!=null && !l2ExRate.get(0).toString().equals("") ){
									   ExchangeRateDecimalValue = l2ExRate.get(0).toString();
								   }
								   if(decimalValue!=null && (!(decimalValue.equals("")))){
										try{
											accountLine.setContractExchangeRate(new BigDecimal(decimalValue));
											accountLine.setPayableContractExchangeRate(new BigDecimal(ExchangeRateDecimalValue));
											accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(ExchangeRateDecimalValue)); 
											accountLine.setEstimateContractExchangeRate(new BigDecimal(decimalValue)); 
											accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(ExchangeRateDecimalValue)); 
											accountLine.setRevisionContractExchangeRate(new BigDecimal(decimalValue)); 
									    }catch(Exception e){
											e.printStackTrace();
										}
								   }else{
										accountLine.setContractExchangeRate(new BigDecimal(0.00)); 
										accountLine.setPayableContractExchangeRate(new BigDecimal(0.00));
										accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(0.00)); 
										accountLine.setEstimateContractExchangeRate(new BigDecimal(0.00)); 
										accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(0.00)); 
										accountLine.setRevisionContractExchangeRate(new BigDecimal(0.00)); 
								   }
								   accountLine.setContractValueDate(new Date());
								   accountLine.setPayableContractValueDate(new Date());
								   accountLine.setEstimatePayableContractValueDate(new Date());
								   accountLine.setEstimateContractValueDate(new Date());
								   accountLine.setRevisionPayableContractValueDate(new Date());
								   accountLine.setRevisionContractValueDate(new Date());
								  }								   
								
						   		/*if(chargeCodeList!=null && !chargeCodeList.isEmpty() && chargeCodeList.get(0)!=null && !chargeCodeList.get(0).toString().equalsIgnoreCase("")){
						   				try{
											  if(!chargeCode.equalsIgnoreCase(accountLine.getChargeCode())){
													String[] printOnInvoiceArr=chargeCodeList.get(0).toString().split("#"); 
													if(printOnInvoiceArr.length>6){
													String printOnInvoice=printOnInvoiceArr[6];
													if((printOnInvoice.equalsIgnoreCase("Y"))){
														printOnInvoice="true";
														ignoreForBilling=Boolean.parseBoolean(printOnInvoice);
														accountLine.setIgnoreForBilling(ignoreForBilling);
													}else{
														printOnInvoice="false";
														ignoreForBilling=Boolean.parseBoolean(printOnInvoice);
														accountLine.setIgnoreForBilling(ignoreForBilling);
													}
													}
											  }
										}catch(Exception e){
											e.printStackTrace();
										}
									  if((rollUpInvoiceFlag!=null && !rollUpInvoiceFlag.equalsIgnoreCase("")) && rollUpInvoiceFlag.equalsIgnoreCase("True")){ 
											try{
												if(!chargeCode.equalsIgnoreCase(accountLine.getChargeCode())){
												String[] rollUpInInvoiceArr = chargeCodeList.get(0).toString().split("#");
												if(rollUpInInvoiceArr.length>13){
													String rollUpInInvoice = rollUpInInvoiceArr[13];
													if(rollUpInInvoice.equalsIgnoreCase("Y")){
														rollUpInInvoice="true";
														ignoreForBilling=Boolean.parseBoolean(rollUpInInvoice);
														accountLine.setRollUpInInvoice(ignoreForBilling);									
													}else{
														rollUpInInvoice="false";
														ignoreForBilling=Boolean.parseBoolean(rollUpInInvoice);
														accountLine.setRollUpInInvoice(ignoreForBilling);
													}
												}	
												}
											}catch(Exception e){
												e.printStackTrace();
											}
										}
									  }*/								   
							  
									   		
								    accountLine.setEstimateSellQuantity((objectInArray.optString("estimateSellQuantity").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimateSellQuantity").trim()));
								    accountLine.setRevisionSellQuantity((objectInArray.optString("revisionSellQuantity").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionSellQuantity").trim()));
								    accountLine.setEstCurrency(objectInArray.optString("estCurrencyNew"));
									accountLine.setEstSellCurrency(objectInArray.optString("estSellCurrencyNew"));
									accountLine.setEstValueDate(getDateFormat(objectInArray.optString("estValueDateNew"))); //estValueDateNew
									accountLine.setEstSellValueDate(getDateFormat(objectInArray.optString("estSellValueDateNew"))); //estSellValueDateNew
									accountLine.setEstExchangeRate((objectInArray.optString("estExchangeRateNew").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estExchangeRateNew").trim())); //estExchangeRateNew
									accountLine.setEstSellExchangeRate((objectInArray.optString("estSellExchangeRateNew").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estSellExchangeRateNew").trim())); //estSellExchangeRateNew
									accountLine.setEstLocalRate((objectInArray.optString("estLocalRateNew").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estLocalRateNew").trim())); //estLocalRateNew
									accountLine.setEstSellLocalRate((objectInArray.optString("estSellLocalRateNew").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estSellLocalRateNew").trim())); //estSellLocalRateNew
									accountLine.setEstLocalAmount((objectInArray.optString("estLocalAmountNew").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estLocalAmountNew").trim())); //estLocalAmountNew
									accountLine.setEstSellLocalAmount((objectInArray.optString("estSellLocalAmountNew").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estSellLocalAmountNew").trim())); //estSellLocalAmountNew
									accountLine.setRevisionCurrency(objectInArray.optString("revisionCurrency")); //revisionCurrencyNew
									accountLine.setCountry(objectInArray.optString("country")); //countryNew
									accountLine.setRevisionValueDate(getDateFormat(objectInArray.optString("revisionValueDate")));
									accountLine.setValueDate(getDateFormat(objectInArray.optString("valueDate")));
									accountLine.setRevisionExchangeRate((objectInArray.optString("revisionExchangeRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionExchangeRate").trim())); //revisionExchangeRateNew
									accountLine.setExchangeRate((objectInArray.optString("exchangeRate").isEmpty()) ? new Double(0.00) : new Double(objectInArray.optString("exchangeRate").trim())); //exchangeRateNew
												
									 if(contractType){
										  try{
											  accountLine.setEstimatePayableContractCurrency(objectInArray.optString("estimatePayableContractCurrencyNew"));  //estimatePayableContractCurrencyNew
											  accountLine.setEstimateContractCurrency(objectInArray.optString("estimateContractCurrencyNew"));  //estimateContractCurrencyNew
											  accountLine.setEstimatePayableContractValueDate(getDateFormat(objectInArray.optString("estimatePayableContractValueDateNew")));  //estimatePayableContractValueDateNew
											  accountLine.setEstimateContractValueDate(getDateFormat(objectInArray.optString("estimateContractValueDateNew")));  //estimateContractValueDateNew
											  accountLine.setEstimatePayableContractExchangeRate((objectInArray.optString("estimatePayableContractExchangeRateNew").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimatePayableContractExchangeRateNew").trim())); //estimatePayableContractExchangeRateNew
											  accountLine.setEstimateContractExchangeRate((objectInArray.optString("estimateContractExchangeRateNew").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimateContractExchangeRateNew").trim())); //estimateContractExchangeRateNew
											  accountLine.setEstimatePayableContractRate((objectInArray.optString("estimatePayableContractRateNew").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimatePayableContractRateNew").trim())); //estimatePayableContractRateNew
											  accountLine.setEstimateContractRate((objectInArray.optString("estimateContractRateNew").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimateContractRateNew").trim())); // estimateContractRateNew
											  accountLine.setEstimatePayableContractRateAmmount((objectInArray.optString("estimatePayableContractRateAmmountNew").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimatePayableContractRateAmmountNew").trim())); // estimatePayableContractRateAmmountNew
											  accountLine.setEstimateContractRateAmmount((objectInArray.optString("estimateContractRateAmmountNew").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimateContractRateAmmountNew").trim())); // estimateContractRateAmmountNew
											  accountLine.setRevisionContractCurrency(objectInArray.optString("revisionContractCurrency")); //revisionContractCurrencyNew
											  accountLine.setRevisionPayableContractCurrency(objectInArray.optString("revisionPayableContractCurrency")); //revisionPayableContractCurrencyNew
											  accountLine.setContractCurrency(objectInArray.optString("contractCurrency"));//contractCurrencyNew
											  accountLine.setPayableContractCurrency(objectInArray.optString("payableContractCurrency"));//payableContractCurrencyNew
											  accountLine.setRevisionContractValueDate(getDateFormat(objectInArray.optString("revisionContractValueDate")));  //revisionContractValueDateNew
											  accountLine.setRevisionPayableContractValueDate(getDateFormat(objectInArray.optString("revisionPayableContractValueDate"))); ////revisionPayableContractValueDateNew
											  accountLine.setContractValueDate(getDateFormat(objectInArray.optString("contractValueDate"))); // contractValueDateNew
											  accountLine.setPayableContractValueDate(getDateFormat(objectInArray.optString("payableContractValueDate"))); // payableContractValueDateNew
											  accountLine.setRevisionContractExchangeRate((objectInArray.optString("revisionContractExchangeRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionContractExchangeRate").trim())); //revisionContractExchangeRateNew
											  accountLine.setRevisionPayableContractExchangeRate((objectInArray.optString("revisionPayableContractExchangeRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionPayableContractExchangeRate").trim())); //revisionPayableContractExchangeRateNew
											  accountLine.setContractExchangeRate((objectInArray.optString("contractExchangeRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("contractExchangeRate").trim())); //contractExchangeRateNew
											  accountLine.setPayableContractExchangeRate((objectInArray.optString("payableContractExchangeRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("payableContractExchangeRate").trim())); // payableContractExchangeRateNew
										  }catch(Exception e){
											  e.printStackTrace();
										  }
									  }														  
										  try{
											  if(chargeCode.equalsIgnoreCase(accountLine.getChargeCode())){							
											  }else{
												  accountLine.setVATExclude(bb);
											  }
										  }catch(Exception e){
											  accountLine.setVATExclude(bb);
											  e.printStackTrace();
										  }							  
										  accountLine.setChargeCode(chargeCode);
										  
										  if(accountLine.getVATExclude() !=null && accountLine.getVATExclude()){
											   accountLine.setRecVatDescr("");
											   accountLine.setRecVatPercent("");
											   accountLine.setRecVatAmt(new BigDecimal(0));
										  }
										  if(!objectInArray.optString("accountLineNumber").isEmpty()){
											  accountLine.setAccountLineNumber(objectInArray.optString("accountLineNumber"));
										  }

										  /*String value=objectInArray.optString("quoteDescription");
										  if(setDescriptionChargeCode!=null &&(!setDescriptionChargeCode.equalsIgnoreCase(""))&&(descriptionChangeFlag.equalsIgnoreCase("Y"))){
											  accountLine.setQuoteDescription(value); 
											  accountLine.setDescription(value);
											  accountLine.setNote(value);
										  }else{
											  accountLine.setQuoteDescription(value); 
										  } 
										  if(setDefaultDescriptionChargeCode!=null && (!setDefaultDescriptionChargeCode.equalsIgnoreCase(""))&&(descriptionChangeFlag.equalsIgnoreCase("Y"))){
										  accountLine.setQuoteDescription(value); 
										  if(accountLine.getDescription() ==null || accountLine.getDescription().trim().equals("")){
										  accountLine.setDescription(value);
										  }
										  if(accountLine.getNote()==null || accountLine.getNote().trim().equals("") ){
										  accountLine.setNote(value);
										  }
										  }else{
											  accountLine.setQuoteDescription(value); 
										  }*/    					  
										  accountLine.setCompanyDivision(objectInArray.optString("companyDivision"));
										  try {
												if(!objectInArray.optString("category").isEmpty() && (objectInArray.optString("category").toString().equalsIgnoreCase("Origin") || objectInArray.optString("category").toString().equalsIgnoreCase("Destin")) && !objectInArray.optString("pricePointAgent").isEmpty() && !objectInArray.optString("pricePointMarket").isEmpty() && !objectInArray.optString("pricePointTariff").isEmpty() ){
													if(accountLine.getExternalIntegrationReference()==null || accountLine.getExternalIntegrationReference().equals("")){
														String pricePointId= saveRequestedQuotePricePoint(objectInArray.optString("pricePointAgent"),objectInArray.optString("pricePointMarket"),objectInArray.optString("pricePointTariff"),objectInArray.optString("category"),serviceOrder.getId(),accountLine.getId(),serviceOrder,miscellaneous,modeValue,company);  
														  
														if(pricePointId!=null && !pricePointId.equals("")){
															accountLine.setReference(pricePointId);
															accountLine.setEstimateDate(new Date());
															accountLine.setExternalIntegrationReference("PP");
														}
													}else if(accountLine.getExternalIntegrationReference().equalsIgnoreCase("PP")){
														pricPointUpdateDetails(serviceOrder.getId(),accountLine.getReference(),serviceOrder,miscellaneous,sysDefaultDetail,modeValue);
													}
											
												  }
											} catch (Exception e1) {
												e1.printStackTrace();
											}														  
										  
										 try{
											  accountLine.setEstimateDiscount(((objectInArray.optString("estimateDiscount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("estimateDiscount").trim())));
											  if(costElementFlag){
											  accountLine.setAccountLineCostElement((!objectInArray.optString("accountLineCostElement").isEmpty()) ? objectInArray.optString("accountLineCostElement").trim() : "");
											  accountLine.setAccountLineScostElementDescription((!objectInArray.optString("accountLineScostElementDescription").isEmpty()) ? objectInArray.optString("accountLineScostElementDescription").trim() : "");
											  }
											 }catch(Exception e){e.printStackTrace();}
											 try{
												accountLine.setRecVatDescr(objectInArray.optString("recVatDescr"));
												accountLine.setRecVatPercent(objectInArray.optString("recVatPercent"));
												accountLine.setPayVatDescr(objectInArray.optString("payVatDescr"));
												accountLine.setPayVatPercent(objectInArray.optString("payVatPercent"));
											 }catch(Exception e){e.printStackTrace();}
														  
									 if(actualDiscountFlag!=null && actualDiscountFlag.equalsIgnoreCase("TRUE")){
										  accountLine.setActualRevenue(((objectInArray.optString("actualRevenue").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("actualRevenue").trim())));
										  accountLine.setRecRate(((objectInArray.optString("recRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("recRate").trim())));
										  accountLine.setRecQuantity(((objectInArray.optString("recQuantity").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("recQuantity").trim())));
										  accountLine.setActualExpense(((objectInArray.optString("actualExpense").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("actualExpense").trim())));
										  accountLine.setActualDiscount(((objectInArray.optString("actualDiscount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("actualDiscount").trim())));
										  accountLine.setPayableContractCurrency(((objectInArray.optString("payableContractCurrency").isEmpty()) ? "" : objectInArray.optString("payableContractCurrency").trim()));
										  accountLine.setPayableContractExchangeRate(((objectInArray.optString("payableContractExchangeRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("payableContractExchangeRate").trim())));
										  accountLine.setPayableContractRateAmmount(((objectInArray.optString("payableContractRateAmmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("payableContractRateAmmount").trim())));
										  accountLine.setPayableContractValueDate(((objectInArray.optString("payableContractValueDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("payableContractValueDate"))));
										  accountLine.setCountry(((objectInArray.optString("country").isEmpty()) ? "" : objectInArray.optString("country").trim()));
										  accountLine.setExchangeRate(((objectInArray.optString("exchangeRate").isEmpty()) ? new Double(0.00) : new Double(objectInArray.optString("exchangeRate"))));
										  accountLine.setLocalAmount(((objectInArray.optString("localAmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("localAmount").trim())));
										  accountLine.setValueDate(((objectInArray.optString("valueDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("valueDate"))));
										  accountLine.setContractCurrency(((objectInArray.optString("contractCurrency").isEmpty()) ? "" : objectInArray.optString("contractCurrency").trim()));
										  accountLine.setContractExchangeRate(((objectInArray.optString("contractExchangeRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("contractExchangeRate").trim())));
										  accountLine.setContractRate(((objectInArray.optString("contractRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("contractRate").trim())));
										  accountLine.setContractValueDate(((objectInArray.optString("contractValueDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("contractValueDate"))));
										  accountLine.setContractRateAmmount(((objectInArray.optString("contractRateAmmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("contractRateAmmount").trim())));
										  accountLine.setRecRateCurrency(((objectInArray.optString("recRateCurrency").isEmpty()) ? "" : objectInArray.optString("recRateCurrency").trim()));
										  accountLine.setRecRateExchange(((objectInArray.optString("recRateExchange").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("recRateExchange").trim())));
										  accountLine.setRecCurrencyRate(((objectInArray.optString("recCurrencyRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("recCurrencyRate").trim())));
										  accountLine.setActualRevenueForeign(((objectInArray.optString("actualRevenueForeign").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("actualRevenueForeign").trim())));
										  accountLine.setRacValueDate(((objectInArray.optString("racValueDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("racValueDate"))));
										  accountLine.setRecVatDescr(objectInArray.optString("recVatDescr"));
										  accountLine.setRecVatPercent(objectInArray.optString("recVatPercent"));
										  accountLine.setRecVatAmt(((objectInArray.optString("recVatAmt").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("recVatAmt").trim())));
										  accountLine.setPayVatDescr(objectInArray.optString("payVatDescr"));
										  accountLine.setPayVatPercent(objectInArray.optString("payVatPercent"));
										  accountLine.setPayVatAmt(((objectInArray.optString("payVatAmt").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("payVatAmt").trim())));
										  accountLine.setPayingStatus(objectInArray.optString("payingStatus"));
										  //accountLine.setReceivedDate(((objectInArray.optString("receivedDate").isEmpty()) ? null : CommonUtil.multiParse(objectInArray.optString("receivedDate"))));
										 // accountLine.setInvoiceNumber(objectInArray.optString("invoiceNumber"));
										  //accountLine.setInvoiceDate(((objectInArray.optString("invoiceDate").isEmpty()) ? null : CommonUtil.multiParse(objectInArray.optString("invoiceDate"))));
										  accountLine.setDescription(objectInArray.optString("description"));
										  accountLine.setNote(objectInArray.optString("note"));
										try{
										  accountLine.setQstRecVatAmt(((objectInArray.optString("qstRecVatAmt").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("qstRecVatAmt").trim())));
										  accountLine.setQstPayVatAmt(((objectInArray.optString("qstPayVatAmt").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("qstPayVatAmt").trim())));
										  accountLine.setQstRecVatGl(objectInArray.optString("qstRecVatGl"));
										  accountLine.setQstPayVatGl(objectInArray.optString("qstPayVatGl"));
										  accountLine.setRecVatGl(objectInArray.optString("recVatGl"));
										  accountLine.setPayVatGl(objectInArray.optString("payVatGl"));
										}catch(Exception e){}
										try{
											if(accountLine.getPayAccDate() ==null && (accountLine.getPayingStatus()!=null)&&(accountLine.getPayingStatus().equalsIgnoreCase("C"))){
												 accountLine.setPayPayableDate(new Date());
												 accountLine.setPayPayableStatus("Fully Paid");
												 accountLine.setPayPayableVia("Credit Card");
												 accountLine.setPayPayableAmount(accountLine.getActualExpense());
												 accountLine.setPayPostDate(systemDefault.getPostDate1());
												 accountLine.setPayAccDate(new Date());
												 accountLine.setPayXferUser(user);
												 accountLine.setPayXfer("Credit Card");
											}
										}catch(Exception e){}
									  }																 
														  
									  if(revisionDiscountFlag!=null && revisionDiscountFlag.equalsIgnoreCase("TRUE")){
										  accountLine.setRevisionPayableContractCurrency(((objectInArray.optString("revisionPayableContractCurrency").isEmpty()) ? "" : objectInArray.optString("revisionPayableContractCurrency").trim()));
										  accountLine.setRevisionPayableContractValueDate(((objectInArray.optString("revisionPayableContractValueDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("revisionPayableContractValueDate"))));
										  accountLine.setRevisionPayableContractExchangeRate(((objectInArray.optString("revisionPayableContractExchangeRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionPayableContractExchangeRate").trim())));
										  accountLine.setRevisionPayableContractRate(((objectInArray.optString("revisionPayableContractRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionPayableContractRate").trim())));
										  accountLine.setRevisionPayableContractRateAmmount(((objectInArray.optString("revisionPayableContractRateAmmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionPayableContractRateAmmount").trim())));
										  accountLine.setRevisionContractCurrency(((objectInArray.optString("revisionContractCurrency").isEmpty()) ? "" : objectInArray.optString("revisionContractCurrency").trim()));
										  accountLine.setRevisionContractValueDate(((objectInArray.optString("revisionContractValueDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("revisionContractValueDate"))));
										  accountLine.setRevisionContractExchangeRate(((objectInArray.optString("revisionContractExchangeRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionContractExchangeRate").trim())));
										  accountLine.setRevisionContractRate(((objectInArray.optString("revisionContractRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionContractRate").trim())));
										  accountLine.setRevisionContractRateAmmount(((objectInArray.optString("revisionContractRateAmmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionContractRateAmmount").trim())));
										  accountLine.setRevisionCurrency(((objectInArray.optString("revisionCurrency").isEmpty()) ? "" : objectInArray.optString("revisionCurrency").trim()));
										  accountLine.setRevisionValueDate(((objectInArray.optString("revisionValueDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("revisionValueDate"))));
										  accountLine.setRevisionExchangeRate(((objectInArray.optString("revisionExchangeRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionExchangeRate").trim())));
										  accountLine.setRevisionLocalRate(((objectInArray.optString("revisionLocalRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionLocalRate").trim())));
										  accountLine.setRevisionLocalAmount(((objectInArray.optString("revisionLocalAmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionLocalAmount").trim())));
										  accountLine.setRevisionSellCurrency(((objectInArray.optString("revisionSellCurrency").isEmpty()) ? "" : objectInArray.optString("revisionSellCurrency").trim()));
										  accountLine.setRevisionSellValueDate(((objectInArray.optString("revisionSellValueDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("revisionSellValueDate"))));
										  accountLine.setRevisionSellExchangeRate(((objectInArray.optString("revisionSellExchangeRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionSellExchangeRate").trim())));
										  accountLine.setRevisionSellLocalRate(((objectInArray.optString("revisionSellLocalRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionSellLocalRate").trim())));
										  accountLine.setRevisionSellLocalAmount(((objectInArray.optString("revisionSellLocalAmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionSellLocalAmount").trim())));
										  accountLine.setRevisionQuantity(((objectInArray.optString("revisionQuantity").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionQuantity").trim())));
										  accountLine.setRevisionDiscount(((objectInArray.optString("revisionDiscount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionDiscount").trim())));
										  accountLine.setRevisionRate(((objectInArray.optString("revisionRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionRate").trim())));
										  accountLine.setRevisionSellRate(((objectInArray.optString("revisionSellRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionSellRate").trim())));
										  accountLine.setRevisionExpense(((objectInArray.optString("revisionExpense").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionExpense").trim())));
										  accountLine.setRevisionRevenueAmount(((objectInArray.optString("revisionRevenueAmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionRevenueAmount").trim())));
										  accountLine.setRevisionPassPercentage(((objectInArray.optString("revisionPassPercentage").isEmpty()) ? new Integer(0) : new Integer(objectInArray.optString("revisionPassPercentage").trim())));
										  accountLine.setRevisionVatDescr(objectInArray.optString("revisionVatDescr"));
										  accountLine.setRevisionVatPercent(objectInArray.optString("revisionVatPercent"));
										  accountLine.setRevisionDescription(objectInArray.optString("revisionDescription"));
										  accountLine.setRevisionVatAmt(((objectInArray.optString("revisionVatAmt").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionVatAmt").trim())));
										try{
											accountLine.setRevisionExpVatAmt(((objectInArray.optString("revisionExpVatAmt").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionExpVatAmt").trim())));
										}catch(Exception e){}
										try{
											accountLine.setVarianceExpenseAmount(((objectInArray.optString("varianceExpenseAmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("varianceExpenseAmount").trim())));															
											accountLine.setVarianceRevenueAmount(((objectInArray.optString("varianceRevenueAmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("varianceRevenueAmount").trim())));
										}catch(Exception e){}
									  }	
														  
									  accountLine.setUpdatedOn(new Date());
								      accountLine.setUpdatedBy(getRequest().getRemoteUser());
								      accountLine.setActgCode(tempActgCode);
								      if(accountLine.getVATExclude() !=null && accountLine.getVATExclude()){
								           accountLine.setRecVatDescr("");
								           accountLine.setRecVatPercent("");
								           accountLine.setRecVatAmt(new BigDecimal(0));
								           accountLine.setPayVatDescr("");
								           accountLine.setPayVatPercent("");
								           accountLine.setPayVatAmt(new BigDecimal(0));
								           accountLine.setEstVatAmt(new BigDecimal(0));
								           accountLine.setEstExpVatAmt(new BigDecimal(0));
								           accountLine.setRevisionVatAmt(new BigDecimal(0));
								           accountLine.setRevisionExpVatAmt(new BigDecimal(0));
								         }															 
															 
							     /*if((accountLine.getPayingStatus()!=null)&&(accountLine.getPayingStatus().equalsIgnoreCase("A"))&&(accountLine.getLocalAmount()!=null)&&(accountLine.getLocalAmount().floatValue()!=0)&& (accountLine.getActualExpense()!=null && accountLine.getActualExpense().doubleValue()!=0)){
								    	
										if(autuPopPostDate){
											try{
									    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
										    		Date dt1=new Date();
										    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
										    		String dt11=sdfDestination.format(dt1);
										    		dt1=sdfDestination.parse(dt11);
										    		Date dt2 =null;
										    		try{
										    		dt2 = new Date();
										    		}catch(Exception e){
										    			System.out.println("Getting exception in date is : "+e+"\n\n\n\n Date value is :"+dt2);
										    		}				    		
										    		
										    		dt2=payDate;
										    		if(dt2!=null){
											    		if(dt1.compareTo(dt2)>0){
											    			payDate=dt2;
											    		}else{
											    			payDate=dt1;
											    		}
										    		}
									    		}
										}catch(Exception e){}			
											if(payDate!=null){
												if((accountLine.getPayPostDate()==null)&&(accountLine.getPayAccDate()==null)){
													accountLine.setPayPostDate(payDate);
												}
											}
											 
										}
									}			*/												 
							     accountLine= accountLineManager.save(accountLine);
															     
								/* CMM/DMM Link synch start */
								 if(( "MGMTFEE".equalsIgnoreCase(accountLine.getChargeCode())||  "DMMFEE".equalsIgnoreCase(accountLine.getChargeCode())  || "DMMFXFEE".equalsIgnoreCase(accountLine.getChargeCode()) )   ){
									 
								 }else{
								  if(billingCMMContractType){
						        	if(serviceOrder.getIsNetworkRecord()){
						        	if(trackingStatus.getAccNetworkGroup())	{	
						        	String networkSynchedId=	accountLineManager.getNetworkSynchedIdCreate(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
						        	if(!(networkSynchedId.equals(""))){
						        		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
						        		synchornizeAccountLine(synchedAccountLine,accountLine); 
						        	}else{
						        		 if(trackingStatus.getAccNetworkGroup())	{
						                     if(serviceOrder.getIsNetworkRecord()){
						                      String partnertype="";
						                   	  if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
						                   		  partnertype=  partnerManager.checkPartnerType(accountLine.getBillToCode());
						                   	  }
						                   	  if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (!(partnertype.equals("")))  && (accountLine.getRecInvoiceNumber() ==null || accountLine.getRecInvoiceNumber().toString().trim().equals(""))){
						                   	  //int j=accountLineManager.updateNetworkSynchedId(accountLine.getId());  
						           				List linkedShipNumber=findLinkerShipNumber(serviceOrder);
						           				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
						           				createAccountLine(serviceOrderRecords,serviceOrder,accountLine); 
						                     }}
						                   }
						        		
						        		}
						        		}
						        	}
								   }
						        if(billingDMMContractType){
						               if(serviceOrder.getIsNetworkRecord()){
						               if(trackingStatus.getAccNetworkGroup())	{	
						              	String networkSynchedId=	accountLineManager.getNetworkSynchedIdCreate(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
						              	if(!(networkSynchedId.equals(""))){
						              		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
						              		synchornizeDMMAccountLine(synchedAccountLine,accountLine,trackingStatus); 
						              	} else{
						              	   if(trackingStatus.getAccNetworkGroup())	{
						                       if(serviceOrder.getIsNetworkRecord()){
						                    	   boolean billtotype=false;
						                      	  if(billing.getNetworkBillToCode()!=null && (!(billing.getNetworkBillToCode().toString().equals(""))) && accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))  && accountLine.getNetworkBillToCode()!=null && (!(accountLine.getNetworkBillToCode().toString().equals(""))) ){
						                      		  billtotype =accountLine.getBillToCode().trim().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode().trim());
						                      	  }	   
						                       	if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (billtotype)  && (accountLine.getRecInvoiceNumber() ==null || accountLine.getRecInvoiceNumber().toString().trim().equals(""))){	
						                       	    int j=accountLineManager.updateNetworkSynchedId(accountLine.getId());  
						             				List linkedShipNumber=findLinkerShipNumber(serviceOrder);
						             				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
						             				createDMMAccountLine(serviceOrderRecords,serviceOrder,accountLine,billing); 
						                       }}
						                     }    					              		
						              	}
						              	}
						              }    					        	  
						        }
							}															     
					  }catch(Exception e){
						  e.printStackTrace();
					  }
				  }												
			    }				
			
			try{
			if(oldRecRateCurrency!=null && !oldRecRateCurrency.equalsIgnoreCase("")){
				String invArray[]=oldRecRateCurrency.split(",");
				for(int j=0;j<invArray.length;j++){
					oldRecRateCurrency=invArray[j].split("~")[1];
					AccountLine alt=accountLineManager.get(Long.parseLong(oldRecRateCurrency));
					if(multiCurrency.trim().equalsIgnoreCase("Y")&& (!(alt.getRecInvoiceNumber().toString().equals(""))) && (!(oldRecRateCurrency.equals("")))){
						SimpleDateFormat racValueDate = new SimpleDateFormat("yyyy-MM-dd");
					    StringBuilder racValueDates = new StringBuilder(racValueDate.format(alt.getRacValueDate()));  
						int i=accountLineManager.updateCurrencyToInvoice(alt.getShipNumber(),alt.getCorpID(),alt.getRecInvoiceNumber(),alt.getRecRateExchange(),alt.getRecRateCurrency(),alt.getId(),racValueDates.toString(),alt.getBillToCode(),alt.getBillToName(),getRequest().getRemoteUser(),contractType);
					}	
				}
				oldRecRateCurrency="";
			}
			}catch(Exception e){e.printStackTrace();}
			if(addPriceLine != null && "AddLine".equals(addPriceLine.trim())){
				addLineInAccountlineListViewAjax();
			}else if(addPriceLine != null && "AddTemplate".equals(addPriceLine.trim())){
				addDefaultTemplateAjax();
			}else{
				try{
					updateAccountLineAjax(serviceOrder);
				}catch(Exception e){
					
				}
			}
			
			accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
			if(accountLineList == null || accountLineList.isEmpty()){
				emptyList="true";
			}
			companyDivis.add("");
			companyDivis.addAll(customerFileManager.findCompanyDivisionByBookAgAndCompanyDivision(serviceOrder.getCorpID(), "",serviceOrder.getCompanyDivision()));
			Iterator<AccountLine> idIterator =  accountLineList.iterator();
			accountLineList=new ArrayList();
			while (idIterator.hasNext()){
				AccountLine accountLine = (AccountLine) idIterator.next(); 
				try{
					if(accountLine.getCompanyDivision()!=null && (!(accountLine.getCompanyDivision().equals("")))){	
						if(companyDivis.contains(accountLine.getCompanyDivision())){
						}else{
							companyDivis.add(accountLine.getCompanyDivision());	
						}
					}
				}catch(Exception e){}
				
				try{
					  if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup())	{
						   String networkSynchedId=	accountLineManager.getNetworkSynchedId(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
					      	if(!(networkSynchedId.equals(""))){
					      		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
					      		Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
					      		boolean accNonEditable=false;
								 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
									 accNonEditable =true;
								 }
								 if(accNonEditable){
									 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
						      			 //utsiRecAccDateFlag=true;
										 accountLine.setUtsiRecAccDateFlag(true);
						      		}
									 if(synchedAccountLine.getInvoiceNumber()!=null && (!(synchedAccountLine.getInvoiceNumber().toString().toString().equals("")))){
						      			 //utsiPayAccDateFlag=true;
										 accountLine.setUtsiPayAccDateFlag(true);
						      		}
								 }else{
					      		if(synchedAccountLine.getRecAccDate()!=null && accountLine.getRecAccDate()== null){
					      			 //utsiRecAccDateFlag=true;
					      			accountLine.setUtsiRecAccDateFlag(true);
					      		}
					      		if(synchedAccountLine.getPayAccDate()!=null && accountLine.getRecAccDate()== null){
					      			 //utsiPayAccDateFlag=true;
					      			accountLine.setUtsiPayAccDateFlag(true);
					      		}
							} 	
					      	} 
					      	accountLineList.add(accountLine);
					  	}else{
					  		accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
					  	}
					}catch(Exception e){}
				}
			for(int i = 0;i<companyDivis.size();i++){
				companyDivisionMap.put((String) companyDivis.get(i), (String) companyDivis.get(i));
			}
			
			if((accountLineList!=null)&&(!accountLineList.isEmpty())){
				discountMap=new HashMap<String, Map<String,String>>();
				List<AccountLine> cc =accountLineList;
				List<Discount> ds=null;
				Map<String,String> tempData=null;
				ds=discountManager.discountMapList(sessionCorpID);
				for(AccountLine acclineDis:cc){
					//ds=discountManager.discountList(billing.getContract(),acclineDis.getChargeCode(),sessionCorpID);
					tempData=new HashMap<String, String>();
					tempData.put("", "");
					for(Discount discountRec:ds){
						if((discountRec.getChargeCode()!=null)&&(discountRec.getChargeCode().equalsIgnoreCase(acclineDis.getChargeCode()))&&(discountRec.getContract()!=null)&&(discountRec.getContract().equalsIgnoreCase(billing.getContract()))){
							tempData.put(discountRec.getDiscount()+"", discountRec.getDescription());
						}
					}
					discountMap.put(acclineDis.getId()+"", tempData);
				}
			}
			
			Long timeTaken = System.currentTimeMillis()-startTime;
			logger.warn("\n\nTime taken to save Pricing Line Ajax  "+timeTaken+"\n\n");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return CANCEL;
		}
		/*if(isSOExtract!=null && isSOExtract.equals("yes")){
			//accountLineManager.updateSOExtFalg(serviceOrder.getId());
			isSOExtract="";
			}*/
		return SUCCESS;
		
	}
	@SkipValidation
	public String addLineInAccountlineListViewAjax(){
		try {
			serviceOrder = serviceOrderManager.get(id);
			billing= billingManager.get(id);
			trackingStatus=trackingStatusManager.get(id);
			getPricingHelper();
			String user = getRequest().getRemoteUser();
			accountLine = new AccountLine(); 
			boolean activateAccPortal =true;
			try{
			if(accountLineAccountPortalFlag){	
			activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
			accountLine.setActivateAccPortal(activateAccPortal);
			}
			}catch(Exception e){
				
			}
			accountLine.setBasis("");
			accountLine.setCategory(""); 
			accountLine.setCorpID(sessionCorpID);
			accountLine.setStatus(true);
			accountLine.setServiceOrder(serviceOrder);
			accountLine.setServiceOrderId(serviceOrder.getId());
			accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
			accountLine.setShipNumber(serviceOrder.getShipNumber());
			accountLine.setCreatedOn(new Date());
			accountLine.setCreatedBy(user);
			accountLine.setUpdatedOn(new Date());
			accountLine.setUpdatedBy(user); 
			accountLine.setDisplayOnQuote(false);
			accountLine.setAdditionalService(false);
			accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
			accountLine.setContract(billing.getContract());
			accountLine.setEstVatDescr("4"); 
			accountLine.setEstVatPercent(euVatPercentList.get("4"));
			accountLine.setBillToCode(billing.getBillToCode());
			accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
			accountLine.setNetworkBillToName(billing.getNetworkBillToName());
			 
			if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
				accountLine.setRecVatDescr(billing.getPrimaryVatCode());
				if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
					String recVatPercent="0";
					if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
			    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
			    		}
					accountLine.setRecVatPercent(recVatPercent);
					}
			}
			if(systemDefault!=null){
		        try{
		        if(systemDefault.getPayableVat()!=null && !systemDefault.getPayableVat().equalsIgnoreCase("")){
		        	accountLine.setPayVatDescr(systemDefault.getPayableVat());
			         if(payVatPercentList.containsKey(systemDefault.getPayableVat())){
			        	 accountLine.setPayVatPercent(payVatPercentList.get(systemDefault.getPayableVat()))	; 
			         }
		        }
		         if(systemDefault.getReceivableVat()!=null && (!(systemDefault.getReceivableVat().equals("")))){	 
			         if(accountLine.getRecVatDescr() ==null || accountLine.getRecVatDescr().toString().trim().equals("")){
			          accountLine.setRecVatDescr(systemDefault.getReceivableVat());
			         }
			         accountLine.setRevisionVatDescr(systemDefault.getReceivableVat());
			         accountLine.setEstVatDescr(systemDefault.getReceivableVat()); 
						
			         if(euVatPercentList.containsKey(systemDefault.getReceivableVat())){
			        	 accountLine.setRecVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()))	;
			         }
		         }
		         }catch(Exception e){
		        	 e.printStackTrace();	 
		         }
		     }
			accountLine.setBillToName(billing.getBillToName());
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
			}
			contractBillList = billingManager.findBillContractAccountList("",billing.getBillToCode(), serviceOrder.getJob(), billing.getCreatedOn(), sessionCorpID,serviceOrder.getCompanyDivision(),serviceOrder.getBookingAgentCode());
			for(int i = 0;i<contractBillList.size();i++){
				billingContractMap.put((String) contractBillList.get(i), (String) contractBillList.get(i));
			}
			if((!(trackingStatus.getSoNetworkGroup())) && billingCMMContractType && networkAgent && (!(trackingStatus.getAccNetworkGroup()))){
				String actCode="";
				accountLine.setVendorCode(serviceOrder.getBookingAgentCode());  
				accountLine.setEstimateVendorName(serviceOrder.getBookingAgentName());
				String companyDivisionAcctgCodeUnique="";
				 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
				 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
					 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
				 }
			     if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
					actCode=partnerManager.getAccountCrossReference(serviceOrder.getBookingAgentCode(),serviceOrder.getCompanyDivision(),sessionCorpID);
				}else{
					actCode=partnerManager.getAccountCrossReferenceUnique(serviceOrder.getBookingAgentCode(),sessionCorpID);
				}
			     accountLine.setActgCode(actCode);	
			}
			if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
				String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
				try{  
					String roles[] = agentRoleValue.split("~");
			          if(roles[3].toString().equals("hauler")){
				           if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
				        	   accountLine.setDivision("01");
				           }else if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())) {
				        	   accountLine.setDivision("01");
				           }else{
				        	   accountLine.setDivision("03");
				           }
			          }else{
			           accountLine.setDivision("03"); 
			          }
				}catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			contractType=false;
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String	contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}
			}
			if(multiCurrency.equalsIgnoreCase("Y")){
				String billingCurrency="";
				if(contractType){
					billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getBillToCode(),sessionCorpID);
				}			
				if(!billingCurrency.equalsIgnoreCase("")){
					AccountLineUtil.setRecRateCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, billingCurrency);
					/*if(contractType){
					AccountLineUtil.setContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, billingCurrency);
					}*/
				}else{
					if(baseCurrencyCompanyDivision==null ||baseCurrencyCompanyDivision.equals("")){
						AccountLineUtil.setRecRateCurrencyAndExchangeRageForBaseCurrency(accountLine, baseCurrency);
						/*if(contractType){
						AccountLineUtil.setContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrency);
						}*/
					}else{
						AccountLineUtil.setRecRateCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrencyCompanyDivision);
						/*if(contractType){
						AccountLineUtil.setContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrencyCompanyDivision);
						}*/
					}
				}
				if(contractType){
					if(baseCurrencyCompanyDivision==null ||baseCurrencyCompanyDivision.equals("")){
						AccountLineUtil.setContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrency);
					}else{
						AccountLineUtil.setContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrencyCompanyDivision);
					}
				}
			}
			if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals("")){
				AccountLineUtil.setCountryCurrencyAndExchangeRageForBaseCurrency(accountLine, baseCurrency);
				if(contractType){
				AccountLineUtil.setOnlyPayableContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrency);
				}
			}else{
				AccountLineUtil.setCountryCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrencyCompanyDivision);
				if(contractType){
				AccountLineUtil.setOnlyPayableContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrencyCompanyDivision);
				}
			}
			
			accountLine.setAccountLineNumber(AccountLineUtil.getAccountLineNumber(serviceOrderManager, serviceOrder));
			accountLine=accountLineManager.save(accountLine); 
			//accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
			//updateAccountLineAjax(serviceOrder);
			if(accountLineStatus == null || "".equals(accountLineStatus)){
				accountLineStatus="true";
			}
			accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
			if(accountLineList == null || accountLineList.isEmpty()){
				emptyList="true";
			}
			companyDivis.add("");
			companyDivis.addAll(customerFileManager.findCompanyDivisionByBookAgAndCompanyDivision(serviceOrder.getCorpID(), "",serviceOrder.getCompanyDivision()));
			Iterator<AccountLine> idIterator =  accountLineList.iterator();
			accountLineList=new ArrayList();
			while (idIterator.hasNext()){
				AccountLine accountLine = (AccountLine) idIterator.next(); 
				try{
					if(accountLine.getCompanyDivision()!=null && (!(accountLine.getCompanyDivision().equals("")))){	
						if(companyDivis.contains(accountLine.getCompanyDivision())){
						}else{
							companyDivis.add(accountLine.getCompanyDivision());	
						}
					}
				}catch(Exception e){}
				
				try{
					  if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup())	{
						   String networkSynchedId=	accountLineManager.getNetworkSynchedId(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
					      	if(!(networkSynchedId.equals(""))){
					      		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
					      		Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
					      		boolean accNonEditable=false;
								 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
									 accNonEditable =true;
								 }
								 if(accNonEditable){
									 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
						      			 //utsiRecAccDateFlag=true;
										 accountLine.setUtsiRecAccDateFlag(true);
						      		}
									 if(synchedAccountLine.getInvoiceNumber()!=null && (!(synchedAccountLine.getInvoiceNumber().toString().toString().equals("")))){
						      			 //utsiPayAccDateFlag=true;
										 accountLine.setUtsiPayAccDateFlag(true);
						      		}
								 }else{
					      		if(synchedAccountLine.getRecAccDate()!=null && accountLine.getRecAccDate()== null){
					      			 //utsiRecAccDateFlag=true;
					      			accountLine.setUtsiRecAccDateFlag(true);
					      		}
					      		if(synchedAccountLine.getPayAccDate()!=null && accountLine.getRecAccDate()== null){
					      			 //utsiPayAccDateFlag=true;
					      			accountLine.setUtsiPayAccDateFlag(true);
					      		}
							} 	
					      	} 
					      	accountLineList.add(accountLine);
					  	}else{
					  		accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
					  	}
					}catch(Exception e){}
				}
			for(int i = 0;i<companyDivis.size();i++){
				companyDivisionMap.put((String) companyDivis.get(i), (String) companyDivis.get(i));
			}
			baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID); 
			if((accountLineList!=null)&&(!accountLineList.isEmpty())){
				discountMap=new HashMap<String, Map<String,String>>();
				List<AccountLine> cc =accountLineList;
				List<Discount> ds=null;
				Map<String,String> tempData=null;
				ds=discountManager.discountMapList(sessionCorpID);
				for(AccountLine acclineDis:cc){
					//ds=discountManager.discountList(billing.getContract(),acclineDis.getChargeCode(),sessionCorpID);
					tempData=new HashMap<String, String>();
					tempData.put("", "");
					for(Discount discountRec:ds){
						if((discountRec.getChargeCode()!=null)&&(discountRec.getChargeCode().equalsIgnoreCase(acclineDis.getChargeCode()))&&(discountRec.getContract()!=null)&&(discountRec.getContract().equalsIgnoreCase(billing.getContract()))){
							tempData.put(discountRec.getDiscount()+"", discountRec.getDescription());
						}
					}
					discountMap.put(acclineDis.getId()+"", tempData);
				}
			}
		    
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return CANCEL;
		}
		return SUCCESS;
	}
	@SkipValidation
	public String addDefaultTemplateAjax(){

		try {
			serviceOrder = serviceOrderManager.get(id);
			billing= billingManager.get(id);
			SoContract = billing.getContract();
			trackingStatus=trackingStatusManager.get(id);
			String user = getRequest().getRemoteUser();
			getPricingHelper();
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			contractBillList = billingManager.findBillContractAccountList("",billing.getBillToCode(), serviceOrder.getJob(), billing.getCreatedOn(), sessionCorpID,serviceOrder.getCompanyDivision(),serviceOrder.getBookingAgentCode());
			for(int i = 0;i<contractBillList.size();i++){
				billingContractMap.put((String) contractBillList.get(i), (String) contractBillList.get(i));
			}
			
			if(serviceOrder.getJob().trim().equalsIgnoreCase("RLO")){
				reloServiceType = serviceOrder.getServiceType();
				while(reloServiceType.indexOf("#")>-1){
					reloServiceType=reloServiceType.replace('#',',');
				}
				serviceTypeSO = reloServiceType;
			}else{
				serviceTypeSO = serviceOrder.getServiceType();
			}
			List defaultList = defaultAccountLineManager.getDefaultAccountList(serviceOrder.getJob(), serviceOrder.getRouting(), serviceOrder.getMode(),billing.getContract(),serviceOrder.getBillToCode(),serviceOrder.getPackingMode(),serviceOrder.getCommodity(),serviceTypeSO,serviceOrder.getCompanyDivision(),serviceOrder.getOriginCountryCode(),serviceOrder.getDestinationCountryCode(),miscellaneous.getEquipment(),serviceOrder.getOriginCity(),serviceOrder.getDestinationCity());
																
			Iterator it = defaultList.iterator();
			while(it.hasNext()){
				DefaultAccountLine defaultAccountLine = (DefaultAccountLine)it.next();
				AccountLine accountLine = new AccountLine();
				boolean activateAccPortal =true;
				try{
				if(accountLineAccountPortalFlag){	
				activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
				accountLine.setActivateAccPortal(activateAccPortal);
				}
				}catch(Exception e){
					
				}
				/////////////10822 Start//////////////
				if(serviceOrder.getMode()== null || serviceOrder.getMode().equalsIgnoreCase("AIR"))
				{
					if(defaultAccountLine.getBasis().equalsIgnoreCase("cwt"))
					{
						if(miscellaneous.getEstimateGrossWeight()!=null)
						{
							accountLine.setEstimateQuantity(miscellaneous.getEstimateGrossWeight());
							accountLine.setEstimateSellQuantity(miscellaneous.getEstimateGrossWeight());
							if(defaultAccountLine.getRate()!=null)
							{
								accountLine.setEstimateExpense((miscellaneous.getEstimateGrossWeight().multiply(BigDecimal.valueOf(defaultAccountLine.getRate()))).divide(new BigDecimal(100.0)));
							}else
							{
								accountLine.setEstimateExpense(new BigDecimal(0.00)); 
							}
							if(defaultAccountLine.getSellRate()!=null)
							{
								accountLine.setEstimateRevenueAmount((miscellaneous.getEstimateGrossWeight().multiply(defaultAccountLine.getSellRate())).divide(new BigDecimal(100.0)));
							}else
							{
								accountLine.setEstimateRevenueAmount(new BigDecimal(0.00)); 
							}
						}else{
								if(defaultAccountLine.getQuantity()!=null && (!(defaultAccountLine.getQuantity().toString().equals(""))))
								{
									accountLine.setEstimateQuantity(new BigDecimal(defaultAccountLine.getQuantity())); 
									accountLine.setEstimateSellQuantity(new BigDecimal(defaultAccountLine.getQuantity()));
									if(defaultAccountLine.getAmount()!=null)
									{
										accountLine.setEstimateExpense((BigDecimal.valueOf(defaultAccountLine.getAmount())));
									}else
									{
										accountLine.setEstimateExpense(new BigDecimal(0.00)); 
									}
									if(defaultAccountLine.getEstimatedRevenue()!=null)
									{
										accountLine.setEstimateRevenueAmount(defaultAccountLine.getEstimatedRevenue());
									}else
									{
										accountLine.setEstimateRevenueAmount(new BigDecimal(0.00)); 
									}
								}else{
									accountLine.setEstimateQuantity(new BigDecimal(0.00));
									accountLine.setEstimateSellQuantity(new BigDecimal(0.00)); 
									accountLine.setEstimateExpense(new BigDecimal(0.00));
									accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
								} 
							}
					}
						else if(defaultAccountLine.getBasis().equalsIgnoreCase("kg"))
						{
							if(miscellaneous.getEstimateGrossWeightKilo()!=null)
							{
								 accountLine.setEstimateQuantity(miscellaneous.getEstimateGrossWeightKilo());
								 accountLine.setEstimateSellQuantity(miscellaneous.getEstimateGrossWeightKilo());
								 if(defaultAccountLine.getRate()!=null)
								 {
									 accountLine.setEstimateExpense((miscellaneous.getEstimateGrossWeightKilo().multiply(BigDecimal.valueOf(defaultAccountLine.getRate()))));
								 }
								 else
								 {
									 accountLine.setEstimateExpense(new BigDecimal(0.00));
								 }
								 if(defaultAccountLine.getSellRate()!=null)
								 {
									 accountLine.setEstimateRevenueAmount((miscellaneous.getEstimateGrossWeightKilo().multiply(defaultAccountLine.getSellRate())));
								 }
								 else
								 {
										accountLine.setEstimateRevenueAmount(new BigDecimal(0.00)); 
								 }
							 }
							else
							{
								 if(defaultAccountLine.getQuantity()!=null && (!(defaultAccountLine.getQuantity().toString().equals(""))))
								 {
										accountLine.setEstimateQuantity(new BigDecimal(defaultAccountLine.getQuantity())); 
										accountLine.setEstimateSellQuantity(new BigDecimal(defaultAccountLine.getQuantity()));
										if(defaultAccountLine.getAmount()!=null)
										{
											accountLine.setEstimateExpense((BigDecimal.valueOf(defaultAccountLine.getAmount())));
										}
										else
										{
											accountLine.setEstimateExpense(new BigDecimal(0.00));
										}
										if(defaultAccountLine.getEstimatedRevenue()!=null)
										{
										accountLine.setEstimateRevenueAmount(defaultAccountLine.getEstimatedRevenue());
										}
										else
										{
											accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
										}
										
								 }
								 else
								 {
										accountLine.setEstimateQuantity(new BigDecimal(0.00));
										accountLine.setEstimateSellQuantity(new BigDecimal(0.00)); 
										accountLine.setEstimateExpense(new BigDecimal(0.00));
										accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
								 } 
							 }
						 }
						 else if(defaultAccountLine.getBasis().equalsIgnoreCase("cft"))
						 {
							 if(miscellaneous.getEstimateCubicFeet()!=null)
							 {
								 accountLine.setEstimateQuantity(miscellaneous.getEstimateCubicFeet());
								 accountLine.setEstimateSellQuantity(miscellaneous.getEstimateCubicFeet());
								 if(defaultAccountLine.getRate()!=null)
								 {
									 accountLine.setEstimateExpense((miscellaneous.getEstimateCubicFeet().multiply(BigDecimal.valueOf(defaultAccountLine.getRate()))));
								 }
								 else
								 {
										 accountLine.setEstimateExpense(new BigDecimal(0.00));
								 }
								 if(defaultAccountLine.getSellRate()!=null)
								 {
									 accountLine.setEstimateRevenueAmount((miscellaneous.getEstimateCubicFeet().multiply(defaultAccountLine.getSellRate())));
								 }
								 else
								 {
										 accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
								 }
							 }
							 else
							 {
								 if(defaultAccountLine.getQuantity()!=null && (!(defaultAccountLine.getQuantity().toString().equals(""))))
								 {
										accountLine.setEstimateQuantity(new BigDecimal(defaultAccountLine.getQuantity())); 
										accountLine.setEstimateSellQuantity(new BigDecimal(defaultAccountLine.getQuantity()));
										if(defaultAccountLine.getAmount()!=null)
										{
											accountLine.setEstimateExpense((BigDecimal.valueOf(defaultAccountLine.getAmount())));
										}
										else
										{
											accountLine.setEstimateExpense(new BigDecimal(0.00));
										}
										if(defaultAccountLine.getEstimatedRevenue()!=null)
										{
											accountLine.setEstimateRevenueAmount(defaultAccountLine.getEstimatedRevenue());
										}
										else
										{
											accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
										}
										
								 }
								 else
								 {
										accountLine.setEstimateQuantity(new BigDecimal(0.00));
										accountLine.setEstimateSellQuantity(new BigDecimal(0.00)); 
										accountLine.setEstimateExpense(new BigDecimal(0.00));
										accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
								 } 
							 } 
						 }
						 else if(defaultAccountLine.getBasis().equalsIgnoreCase("cbm"))
						 {
							 if(miscellaneous.getEstimateCubicMtr()!=null)
							 {
								 accountLine.setEstimateQuantity(miscellaneous.getEstimateCubicMtr());
								 accountLine.setEstimateSellQuantity(miscellaneous.getEstimateCubicMtr());
								 if(defaultAccountLine.getRate()!=null)
								 {
									 accountLine.setEstimateExpense((miscellaneous.getEstimateCubicMtr().multiply(BigDecimal.valueOf(defaultAccountLine.getRate()))));
								 }
								 else
								 {
									 accountLine.setEstimateExpense(new BigDecimal(0.00));
								 }
								 if(defaultAccountLine.getSellRate()!=null)
								 {
									 accountLine.setEstimateRevenueAmount((miscellaneous.getEstimateCubicMtr().multiply(defaultAccountLine.getSellRate())));
								 }
								 else
								 {
									 accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
								 }
								 
							 }
							 else
							 {
								 if(defaultAccountLine.getQuantity()!=null && (!(defaultAccountLine.getQuantity().toString().equals(""))))
								 {
										accountLine.setEstimateQuantity(new BigDecimal(defaultAccountLine.getQuantity())); 
										accountLine.setEstimateSellQuantity(new BigDecimal(defaultAccountLine.getQuantity()));
										if(defaultAccountLine.getAmount()!=null)
										{
											accountLine.setEstimateExpense((BigDecimal.valueOf(defaultAccountLine.getAmount())));
										}
										else
										{
											accountLine.setEstimateExpense(new BigDecimal(0.00));
										}
										if(defaultAccountLine.getEstimatedRevenue()!=null)
										{
											accountLine.setEstimateRevenueAmount(defaultAccountLine.getEstimatedRevenue());
										}
										else
										{
											accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
										}
										
								 }
								 else{
										accountLine.setEstimateQuantity(new BigDecimal(0.00));
										accountLine.setEstimateSellQuantity(new BigDecimal(0.00));
										accountLine.setEstimateExpense(new BigDecimal(0.00));
										accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
									} 
							 } 
						 }
						 else
						 	{
							 if(defaultAccountLine.getQuantity()!=null && (!(defaultAccountLine.getQuantity().toString().equals(""))))
							 	{
									accountLine.setEstimateQuantity(new BigDecimal(defaultAccountLine.getQuantity())); 
									accountLine.setEstimateSellQuantity(new BigDecimal(defaultAccountLine.getQuantity()));
									if(defaultAccountLine.getAmount()!=null)
									{
										accountLine.setEstimateExpense((BigDecimal.valueOf(defaultAccountLine.getAmount())));
									}
									else
									{
										accountLine.setEstimateExpense(new BigDecimal(0.00));
									}
									if(defaultAccountLine.getEstimatedRevenue()!=null)
									{
										accountLine.setEstimateRevenueAmount(defaultAccountLine.getEstimatedRevenue());
									}
									else
									{
										accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
									}
									
								}
							 else{
									accountLine.setEstimateQuantity(new BigDecimal(0.00));
									accountLine.setEstimateSellQuantity(new BigDecimal(0.00));
									accountLine.setEstimateExpense(new BigDecimal(0.00));
									accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
								}
							 
						 }
					}
					 else{
						 if(defaultAccountLine.getBasis().equalsIgnoreCase("cwt"))
						 {
							 if(miscellaneous.getEstimatedNetWeight()!=null)
							 {
								 accountLine.setEstimateQuantity(miscellaneous.getEstimatedNetWeight());
								 accountLine.setEstimateSellQuantity(miscellaneous.getEstimatedNetWeight());
								 if(defaultAccountLine.getRate()!=null)
								 {
									 accountLine.setEstimateExpense((miscellaneous.getEstimatedNetWeight().multiply(BigDecimal.valueOf(defaultAccountLine.getRate()))).divide(new BigDecimal(100.0)));
								 }else{
										 accountLine.setEstimateExpense(new BigDecimal(0.00)); 
									 }
								 if(defaultAccountLine.getSellRate()!=null)
								 {
									 accountLine.setEstimateRevenueAmount((miscellaneous.getEstimatedNetWeight().multiply(defaultAccountLine.getSellRate())).divide(new BigDecimal(100.0)));
								 }else{
										 accountLine.setEstimateRevenueAmount(new BigDecimal(0.00)); 
									 }
							 }else{
								 	if(defaultAccountLine.getQuantity()!=null && (!(defaultAccountLine.getQuantity().toString().equals(""))))
								 	{
										accountLine.setEstimateQuantity(new BigDecimal(defaultAccountLine.getQuantity())); 
										accountLine.setEstimateSellQuantity(new BigDecimal(defaultAccountLine.getQuantity()));
										if(defaultAccountLine.getAmount()!=null)
										{
											accountLine.setEstimateExpense((BigDecimal.valueOf(defaultAccountLine.getAmount())));
										}else{
											accountLine.setEstimateExpense(new BigDecimal(0.00)); 
										}
										if(defaultAccountLine.getEstimatedRevenue()!=null)
										{
											accountLine.setEstimateRevenueAmount(defaultAccountLine.getEstimatedRevenue());
										}else{
											accountLine.setEstimateRevenueAmount(new BigDecimal(0.00)); 
										}
										
									}else{
											accountLine.setEstimateQuantity(new BigDecimal(0.00));
											accountLine.setEstimateSellQuantity(new BigDecimal(0.00)); 
											accountLine.setEstimateExpense(new BigDecimal(0.00));
											accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
										} 
							 	}
						 }
						 else if(defaultAccountLine.getBasis().equalsIgnoreCase("kg"))
						 {
							 if(miscellaneous.getEstimatedNetWeightKilo()!=null)
							 {
								 accountLine.setEstimateQuantity(miscellaneous.getEstimatedNetWeightKilo());
								 accountLine.setEstimateSellQuantity(miscellaneous.getEstimatedNetWeightKilo());
								 if(defaultAccountLine.getRate()!=null)
								 {
									 accountLine.setEstimateExpense((miscellaneous.getEstimatedNetWeightKilo().multiply(BigDecimal.valueOf(defaultAccountLine.getRate()))));
								 }else{
										 accountLine.setEstimateExpense(new BigDecimal(0.00));
									 }
								 if(defaultAccountLine.getSellRate()!=null)
								 {
									 accountLine.setEstimateRevenueAmount((miscellaneous.getEstimatedNetWeightKilo().multiply(defaultAccountLine.getSellRate())));
								 }else{
										 accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
									 }
							 }else{
								 	if(defaultAccountLine.getQuantity()!=null && (!(defaultAccountLine.getQuantity().toString().equals(""))))
								 	{
										accountLine.setEstimateQuantity(new BigDecimal(defaultAccountLine.getQuantity())); 
										accountLine.setEstimateSellQuantity(new BigDecimal(defaultAccountLine.getQuantity()));
										if(defaultAccountLine.getAmount()!=null)
										{
											accountLine.setEstimateExpense((BigDecimal.valueOf(defaultAccountLine.getAmount())));
										}else{
											accountLine.setEstimateExpense(new BigDecimal(0.00));
										}
										if(defaultAccountLine.getEstimatedRevenue()!=null)
										{
											accountLine.setEstimateRevenueAmount(defaultAccountLine.getEstimatedRevenue());
										}else{
											accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
										}
									}else{
											accountLine.setEstimateQuantity(new BigDecimal(0.00));
											accountLine.setEstimateSellQuantity(new BigDecimal(0.00));
											accountLine.setEstimateExpense(new BigDecimal(0.00));
											accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
										} 
							 }
						 }
						 else if(defaultAccountLine.getBasis().equalsIgnoreCase("cft"))
						 {
							 if(miscellaneous.getNetEstimateCubicFeet()!=null)
							 {
								 accountLine.setEstimateQuantity(miscellaneous.getNetEstimateCubicFeet());
								 accountLine.setEstimateSellQuantity(miscellaneous.getNetEstimateCubicFeet());
								 if(defaultAccountLine.getRate()!=null)
								 {
									 accountLine.setEstimateExpense((miscellaneous.getNetEstimateCubicFeet().multiply(BigDecimal.valueOf(defaultAccountLine.getRate()))));
								 }else{
									 	accountLine.setEstimateExpense(new BigDecimal(0.00));
									 }
								 if(defaultAccountLine.getSellRate()!=null)
								 {
									 accountLine.setEstimateRevenueAmount((miscellaneous.getNetEstimateCubicFeet().multiply(defaultAccountLine.getSellRate())));
								 }else{
										 accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
									 }
							 }else{
								 if(defaultAccountLine.getQuantity()!=null && (!(defaultAccountLine.getQuantity().toString().equals(""))))
								 {
										accountLine.setEstimateQuantity(new BigDecimal(defaultAccountLine.getQuantity())); 
										accountLine.setEstimateSellQuantity(new BigDecimal(defaultAccountLine.getQuantity()));
										if(defaultAccountLine.getAmount()!=null)
										{
											accountLine.setEstimateExpense((BigDecimal.valueOf(defaultAccountLine.getAmount())));
										}else{
											accountLine.setEstimateExpense(new BigDecimal(0.00));
										}
										if(defaultAccountLine.getEstimatedRevenue()!=null)
										{
											accountLine.setEstimateRevenueAmount(defaultAccountLine.getEstimatedRevenue());
										}else{
											accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
										}
								 }else{
										accountLine.setEstimateQuantity(new BigDecimal(0.00));
										accountLine.setEstimateSellQuantity(new BigDecimal(0.00));
										accountLine.setEstimateExpense(new BigDecimal(0.00));
										accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
										} 
							 } 
						 }
						 else if(defaultAccountLine.getBasis().equalsIgnoreCase("cbm"))
						 {
							 if(miscellaneous.getNetEstimateCubicMtr()!=null)
							 {
								 accountLine.setEstimateQuantity(miscellaneous.getNetEstimateCubicMtr());
								 accountLine.setEstimateSellQuantity(miscellaneous.getNetEstimateCubicMtr());
								 if(defaultAccountLine.getRate()!=null)
								 {
									 accountLine.setEstimateExpense((miscellaneous.getNetEstimateCubicMtr().multiply(BigDecimal.valueOf(defaultAccountLine.getRate()))));
								 }else{
										 accountLine.setEstimateExpense(new BigDecimal(0.00));
									 }
								 if(defaultAccountLine.getSellRate()!=null)
								 {
									 accountLine.setEstimateRevenueAmount((miscellaneous.getNetEstimateCubicMtr().multiply(defaultAccountLine.getSellRate())));
								 }else{
										 accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
									 }
							 }else{
								 if(defaultAccountLine.getQuantity()!=null && (!(defaultAccountLine.getQuantity().toString().equals(""))))
								 {
										accountLine.setEstimateQuantity(new BigDecimal(defaultAccountLine.getQuantity())); 
										accountLine.setEstimateSellQuantity(new BigDecimal(defaultAccountLine.getQuantity()));
										if(defaultAccountLine.getAmount()!=null)
										{
											accountLine.setEstimateExpense((BigDecimal.valueOf(defaultAccountLine.getAmount())));
										}else{
											accountLine.setEstimateExpense(new BigDecimal(0.00));
										}
										if(defaultAccountLine.getEstimatedRevenue()!=null)
										{
										accountLine.setEstimateRevenueAmount(defaultAccountLine.getEstimatedRevenue());
										}else{
											accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
										}
								 }else{
										accountLine.setEstimateQuantity(new BigDecimal(0.00));
										accountLine.setEstimateSellQuantity(new BigDecimal(0.00));
										accountLine.setEstimateExpense(new BigDecimal(0.00));
										accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
									} 
							 } 
						 }
						 else{
							 if(defaultAccountLine.getQuantity()!=null && (!(defaultAccountLine.getQuantity().toString().equals(""))))
							 {
									accountLine.setEstimateQuantity(new BigDecimal(defaultAccountLine.getQuantity())); 
									accountLine.setEstimateSellQuantity(new BigDecimal(defaultAccountLine.getQuantity()));
									if(defaultAccountLine.getAmount()!=null)
									{
										accountLine.setEstimateExpense((BigDecimal.valueOf(defaultAccountLine.getAmount())));
									}else{
										accountLine.setEstimateExpense(new BigDecimal(0.00));
									}
									if(defaultAccountLine.getEstimatedRevenue()!=null)
									{
										accountLine.setEstimateRevenueAmount(defaultAccountLine.getEstimatedRevenue());
									}else{
										accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
									}
							 }else{
									accountLine.setEstimateQuantity(new BigDecimal(0.00));
									accountLine.setEstimateSellQuantity(new BigDecimal(0.00));
									accountLine.setEstimateExpense(new BigDecimal(0.00));
									accountLine.setEstimateRevenueAmount(new BigDecimal(0.00));
								}
							 
						 }
					 }
				if(defaultAccountLine.getRate()!=null && (!(defaultAccountLine.getRate().toString().equals("")))){
					accountLine.setEstimateRate(new BigDecimal(defaultAccountLine.getRate()));
					accountLine.setEstLocalRate(new BigDecimal(defaultAccountLine.getRate()));
				}else{
					accountLine.setEstimateRate(new BigDecimal(0.00));
					accountLine.setEstLocalRate(new BigDecimal(0.0));
				}
				accountLine.setBasis(defaultAccountLine.getBasis());
				accountLine.setCategory(defaultAccountLine.getCategories());
				
				if(defaultAccountLine.getEstimatedRevenue()!=null && defaultAccountLine.getEstimatedRevenue().doubleValue()>0.00){
					accountLine.setDisplayOnQuote(true);
				}else{
					accountLine.setDisplayOnQuote(false);
				}
				if(defaultAccountLine.getMarkUp()!=null && (!( defaultAccountLine.getMarkUp().toString().equals("")))){
					accountLine.setEstimatePassPercentage(defaultAccountLine.getMarkUp());
				}else{
					accountLine.setEstimatePassPercentage(0);	
				}
				if(defaultAccountLine.getSellRate()!=null && (!(defaultAccountLine.getSellRate().toString().equals("")))){
					accountLine.setEstimateSellRate(defaultAccountLine.getSellRate());
					accountLine.setEstSellLocalRate(defaultAccountLine.getSellRate());
				}else {
					accountLine.setEstimateSellRate(new BigDecimal(0.00));
					accountLine.setEstSellLocalRate(new BigDecimal(0.00));
				}
				accountLine.setChargeCode(defaultAccountLine.getChargeCode());
				accountLine.setRecQuantity(new BigDecimal(0.00));
				accountLine.setRecRate(new BigDecimal(0.00));
				try {
					String ss=chargesManager.findPopulateCostElementData(defaultAccountLine.getChargeCode(), SoContract,sessionCorpID);
					if(!ss.equalsIgnoreCase("")){
						accountLine.setAccountLineCostElement(ss.split("~")[0]);
						accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				///Code for AccountLine division 
				if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
					String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
					try{  
						String roles[] = agentRoleValue.split("~");
				          if(roles[3].toString().equals("hauler")){
					           if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
					        	   accountLine.setDivision("01");
					           }else if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())) {
					        	   accountLine.setDivision("01");
					           }else{
					        	   accountLine.setDivision("03");
					           }
				          }else{
				           accountLine.setDivision("03"); 
				          }
					}catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				///Code for AccountLine division 			
				try{
					List<Charges> chargeList = chargesManager.findChargesList(defaultAccountLine.getChargeCode(),SoContract,sessionCorpID);
					for (Charges charge : chargeList) {
						if(defaultAccountLine.getDescription()!=null && (!(defaultAccountLine.getDescription().toString().trim().equals("")))){
							accountLine.setDescription(defaultAccountLine.getDescription());
							accountLine.setNote(defaultAccountLine.getDescription());
							accountLine.setRevisionDescription(defaultAccountLine.getDescription());
							accountLine.setQuoteDescription(defaultAccountLine.getDescription());	
						}else{
							accountLine.setDescription(charge.getDescription());
							accountLine.setNote(charge.getDescription());
							accountLine.setRevisionDescription(charge.getDescription());
							accountLine.setQuoteDescription(charge.getDescription());
						}
						accountLine.setVATExclude(charge.getVATExclude());
						if(charge.getPrintOnInvoice()!=null && charge.getPrintOnInvoice()){
						accountLine.setIgnoreForBilling(true);
						}else{
						accountLine.setIgnoreForBilling(false);	
						}
					    if(!costElementFlag){
					    	accountLine.setRecGl(charge.getGl());
					    	accountLine.setPayGl(charge.getExpGl());
					    }else{
					    	accountLine = AccountLineUtil.getChargeGLCode(accountLineManager, serviceOrder, defaultAccountLine.getChargeCode(), SoContract, sessionCorpID, accountLine);
					    }
						
						accountLine = AccountLineUtil.setContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, charge.getContractCurrency());
						accountLine.setEstimateContractRate(accountLine.getEstimateContractExchangeRate().multiply(accountLine.getEstimateSellRate()));
						accountLine.setEstimateContractRateAmmount(accountLine.getEstimateContractExchangeRate().multiply(accountLine.getEstimateRevenueAmount()));
						
						accountLine = AccountLineUtil.setOnlyPayableContractCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, charge.getPayableContractCurrency());
						accountLine.setEstimatePayableContractRate(accountLine.getEstimatePayableContractExchangeRate().multiply(accountLine.getEstimateRate()));
						accountLine.setEstimatePayableContractRateAmmount(accountLine.getEstimatePayableContractExchangeRate().multiply(accountLine.getEstimateExpense()));
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if((defaultAccountLine.getBillToCode()!=null)&&(!defaultAccountLine.getBillToCode().equalsIgnoreCase(""))){
					accountLine.setBillToCode(defaultAccountLine.getBillToCode());
					accountLine.setBillToName(defaultAccountLine.getBillToName());
				}else{
					accountLine.setBillToCode(billing.getBillToCode());
					accountLine.setBillToName(billing.getBillToName());
				}
				accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
				accountLine.setNetworkBillToName(billing.getNetworkBillToName());
			   
				if(defaultAccountLine.getVendorCode()!=null && (!(defaultAccountLine.getVendorCode().toString().equals("")))){
					accountLine.setVendorCode(defaultAccountLine.getVendorCode());
					accountLine.setEstimateVendorName(defaultAccountLine.getVendorName());
					String companyDivisionAcctgCodeUnique="";
					String actCode="";
					 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
					 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
						 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
					 }
			         if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
			 			actCode=partnerManager.getAccountCrossReference(accountLine.getVendorCode(),serviceOrder.getCompanyDivision(),sessionCorpID);
			 		}else{
			 			actCode=partnerManager.getAccountCrossReferenceUnique(accountLine.getVendorCode(),sessionCorpID);
			 		}
			         accountLine.setActgCode(actCode);
				}else{
					if(SoContract!=null && (!(SoContract.toString().equals("")))){
						  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,SoContract);
					}
					if((!(trackingStatus.getSoNetworkGroup())) && billingCMMContractType && networkAgent && (!(trackingStatus.getAccNetworkGroup()))){
						String actCode="";
						accountLine.setVendorCode(serviceOrder.getBookingAgentCode());  
						accountLine.setEstimateVendorName(serviceOrder.getBookingAgentName());
						String companyDivisionAcctgCodeUnique="";
						 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
						 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
							 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
						 }
				         if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
				 			actCode=partnerManager.getAccountCrossReference(serviceOrder.getBookingAgentCode(),serviceOrder.getCompanyDivision(),sessionCorpID);
				 		}else{
				 			actCode=partnerManager.getAccountCrossReferenceUnique(serviceOrder.getBookingAgentCode(),sessionCorpID);
				 		}
				         accountLine.setActgCode(actCode);	
					}else{
						accountLine.setVendorCode(defaultAccountLine.getVendorCode());
						accountLine.setEstimateVendorName(defaultAccountLine.getVendorName());
						String companyDivisionAcctgCodeUnique="";
						String actCode="";
						 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
						 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
							 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
						 }
				         if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
				 			actCode=partnerManager.getAccountCrossReference(accountLine.getVendorCode(),serviceOrder.getCompanyDivision(),sessionCorpID);
				 		}else{
				 			actCode=partnerManager.getAccountCrossReferenceUnique(accountLine.getVendorCode(),sessionCorpID);
				 		}
				         accountLine.setActgCode(actCode);
					}
				}

				if(multiCurrency.equalsIgnoreCase("Y")){
					contractType=false;
					if(SoContract!=null && (!(SoContract.toString().equals("")))){
						String	contractTypeValue=	accountLineManager.checkContractType(SoContract,sessionCorpID);
						if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
							contractType=true;	
						}
					}
					String billingCurrency="";
					this.contractType=contractType;
					if(contractType){
						billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getBillToCode(),sessionCorpID);
					}				
					if(!billingCurrency.equalsIgnoreCase("")){
						accountLine = AccountLineUtil.setRecRateCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, billingCurrency);
						accountLine.setEstSellLocalRate(accountLine.getEstSellExchangeRate().multiply(accountLine.getEstimateSellRate()));
						accountLine.setEstSellLocalAmount(accountLine.getEstSellExchangeRate().multiply(accountLine.getEstimateRevenueAmount()));
					}else{
						if(baseCurrencyCompanyDivision==null ||baseCurrencyCompanyDivision.equals("")){
							accountLine = AccountLineUtil.setRecRateCurrencyAndExchangeRageForBaseCurrency( accountLine, baseCurrency);
							accountLine.setEstSellLocalRate(accountLine.getEstSellExchangeRate().multiply(accountLine.getEstimateSellRate()));
							accountLine.setEstSellLocalAmount(accountLine.getEstSellExchangeRate().multiply(accountLine.getEstimateRevenueAmount()));
						}else{
							accountLine = AccountLineUtil.setRecRateCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrencyCompanyDivision);
							accountLine.setEstSellLocalRate(accountLine.getEstSellExchangeRate().multiply(accountLine.getEstimateSellRate()));
							accountLine.setEstSellLocalAmount(accountLine.getEstSellExchangeRate().multiply(accountLine.getEstimateRevenueAmount()));
						}
					}
				}
				contractType=false; 
				if(SoContract!=null && (!(SoContract.toString().equals("")))){
				String	contractTypeValue=	accountLineManager.checkContractType(SoContract,sessionCorpID);
					if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
						contractType=true;	
					} 
				}
				String billingCurrency="";
				if(contractType){
				billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getVendorCode(),sessionCorpID);
				}
				if(!billingCurrency.equalsIgnoreCase("") && contractType){
					accountLine = AccountLineUtil.setCountryCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, billingCurrency);
					accountLine.setEstLocalRate(BigDecimal.valueOf(accountLine.getExchangeRate()).multiply(accountLine.getEstimateRate()));
					accountLine.setEstLocalAmount(BigDecimal.valueOf(accountLine.getExchangeRate()).multiply(accountLine.getEstimateExpense()));
				}else{
					if(baseCurrencyCompanyDivision == null ||  baseCurrencyCompanyDivision.equals("")){
						accountLine = AccountLineUtil.setCountryCurrencyAndExchangeRageForBaseCurrency(accountLine, baseCurrency);
						accountLine.setEstLocalRate(BigDecimal.valueOf(accountLine.getExchangeRate()).multiply(accountLine.getEstimateRate()));
						accountLine.setEstLocalAmount(BigDecimal.valueOf(accountLine.getExchangeRate()).multiply(accountLine.getEstimateExpense()));
					}else{
						accountLine = AccountLineUtil.setCountryCurrencyAndExchangeRage(accountLine, exchangeRateManager, sessionCorpID, baseCurrencyCompanyDivision);
						accountLine.setEstLocalRate(BigDecimal.valueOf(accountLine.getExchangeRate()).multiply(accountLine.getEstimateRate()));
						accountLine.setEstLocalAmount(BigDecimal.valueOf(accountLine.getExchangeRate()).multiply(accountLine.getEstimateExpense()));
					}
				}
				//accountLine.setQuoteDescription(defaultAccountLine.getDescription());
				accountLine.setCorpID(sessionCorpID);
				accountLine.setServiceOrderId(serviceOrder.getId());
				accountLine.setStatus(true); 
				accountLine.setServiceOrder(serviceOrder);
				accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
				accountLine.setShipNumber(serviceOrder.getShipNumber());
				accountLine.setCreatedOn(new Date());
				accountLine.setCreatedBy(user);
				accountLine.setUpdatedOn(new Date());
				accountLine.setUpdatedBy(user); 
				accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
				accountLine.setContract(SoContract);
				
			    accountLine.setAccountLineNumber(AccountLineUtil.getAccountLineNumber(serviceOrderManager, serviceOrder));
			    if(systemDefault!=null && systemDefault.getVatCalculation() && !accountLine.getVATExclude()){
			    	   accountLine.setEstVatDescr(defaultAccountLine.getEstVatDescr());
			    	   accountLine.setEstVatPercent(defaultAccountLine.getEstVatPercent()); 
			    	   accountLine.setEstVatAmt(defaultAccountLine.getEstVatAmt()); 
			       }
			    	
			    if(!accountLine.getVATExclude()){
					try{
						if(accountLine.getRecVatDescr()==null || (accountLine.getRecVatDescr().toString().trim().equals(""))){
						if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals(""))) && (billing.getPrimaryVatCode()!=null) && (!(billing.getPrimaryVatCode().toString().equals("")))){
				    		accountLine.setRecVatDescr(billing.getPrimaryVatCode());
				    		if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
					    		String recVatPercent="0";
					    		if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
						    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
						    		}
								accountLine.setRecVatPercent(recVatPercent);
					    		}else{
						    		accountLine.setRecVatDescr(systemDefault.getReceivableVat());
							        if(euVatPercentList.containsKey(systemDefault.getReceivableVat())){
							        	 accountLine.setRecVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()));
									}
						    	}
				    	}
						else if((defaultAccountLine.getEstVatDescr()!=null) && (!(defaultAccountLine.getEstVatDescr().toString().trim().equals("")))){
							accountLine.setRecVatDescr(defaultAccountLine.getEstVatDescr());
				        	accountLine.setRecVatPercent(defaultAccountLine.getEstVatPercent()); 
					    	}
						else{
				    		accountLine.setRecVatDescr(systemDefault.getReceivableVat());
					        if(euVatPercentList.containsKey(systemDefault.getReceivableVat())){
					        	 accountLine.setRecVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()));
							}
				    	}
						
						}
			        
				}catch(Exception e){
		        	 e.printStackTrace();	 
		         }
				}
			    	
			    	
			    	
			    try{
		        	 if(!accountLine.getVATExclude()){
				         if(accountLine.getPayVatDescr()==null || (accountLine.getPayVatDescr().toString().trim().equals(""))){
			        	 if((accountLine.getVendorCode() !=null && trackingStatus.getOriginAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getOriginAgentCode().toString())) && ((billing.getOriginAgentVatCode()!=null) && (!billing.getOriginAgentVatCode().equals(""))) ){
			        		 accountLine.setPayVatDescr(billing.getOriginAgentVatCode());
			        	 } 
			        	 else if((accountLine.getVendorCode() !=null && trackingStatus.getDestinationAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getDestinationAgentCode().toString())) && ((billing.getDestinationAgentVatCode()!=null) && (!billing.getDestinationAgentVatCode().equals("")))){
			        		 accountLine.setPayVatDescr(billing.getDestinationAgentVatCode());
			        	 }
			        	 else if((accountLine.getVendorCode() !=null && trackingStatus.getOriginSubAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode().toString())) && ((billing.getOriginSubAgentVatCode()!=null) && (!billing.getOriginSubAgentVatCode().equals("")))){
			        		 accountLine.setPayVatDescr(billing.getOriginSubAgentVatCode());
			        	 }
			        	 else if((accountLine.getVendorCode() !=null && trackingStatus.getDestinationSubAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode().toString())) && ((billing.getDestinationSubAgentVatCode()!=null) && (!billing.getDestinationSubAgentVatCode().equals("")))){
			        		 accountLine.setPayVatDescr(billing.getDestinationSubAgentVatCode());
			        	 }		 
			        	 
			        	 else if((accountLine.getVendorCode() !=null && trackingStatus.getForwarderCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getForwarderCode().toString())) && ((billing.getForwarderVatCode()!=null) && (!billing.getForwarderVatCode().equals("")))){
			        		 accountLine.setPayVatDescr(billing.getForwarderVatCode());
			        	 }
			        	 else if((accountLine.getVendorCode() !=null && trackingStatus.getBrokerCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getBrokerCode().toString())) && ((billing.getBrokerVatCode()!=null) && (!billing.getBrokerVatCode().equals("")))){
			        		 accountLine.setPayVatDescr(billing.getBrokerVatCode());
			        	 }
			        	 else if((accountLine.getVendorCode() !=null && trackingStatus.getNetworkPartnerCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode().toString())) && ((billing.getNetworkPartnerVatCode()!=null) && (!billing.getNetworkPartnerVatCode().equals("")))){
			        		 accountLine.setPayVatDescr(billing.getNetworkPartnerVatCode());
			        	 }
			        	 
			        	 else if((accountLine.getVendorCode() !=null && serviceOrder.getBookingAgentCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(serviceOrder.getBookingAgentCode().toString())) && ((billing.getBookingAgentVatCode()!=null) && (!billing.getBookingAgentVatCode().equals("")))){
			        		 accountLine.setPayVatDescr(billing.getBookingAgentVatCode());
			        	 }
			        	 else if((accountLine.getVendorCode() !=null && billing.getVendorCode() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(billing.getVendorCode().toString())) && ((billing.getVendorCodeVatCode()!=null) && (!billing.getVendorCodeVatCode().equals("")))){
			        		 accountLine.setPayVatDescr(billing.getVendorCodeVatCode());
			        	 }
			        	 else if((accountLine.getVendorCode() !=null && billing.getVendorCode1() !=null && accountLine.getVendorCode().toString().equalsIgnoreCase(billing.getVendorCode1().toString())) && ((billing.getVendorCodeVatCode1()!=null) && (!billing.getVendorCodeVatCode1().equals("")))){
			        		 accountLine.setPayVatDescr(billing.getVendorCodeVatCode1());
			        	 }
			        	 else if(defaultAccountLine.getEstExpVatDescr()!=null && (!defaultAccountLine.getEstExpVatDescr().toString().equalsIgnoreCase(""))) {
			        		 accountLine.setPayVatDescr(defaultAccountLine.getEstExpVatDescr());
					         accountLine.setPayVatPercent(defaultAccountLine.getEstExpVatPercent()); 
			        	 }
			        	 else{
			        	 if(accountLine.getPayVatDescr() ==null || accountLine.getPayVatDescr().toString().equals("") ){ 
			        	 if(systemDefault.getPayableVat()!=null && !systemDefault.getPayableVat().equals("")){
			               accountLine.setPayVatDescr(systemDefault.getPayableVat());
			        	 }
			        	 
			        	 }
			        	}
			        	if(payVatPercentList.containsKey(accountLine.getPayVatDescr())){
			        		 accountLine.setPayVatPercent(payVatPercentList.get(accountLine.getPayVatDescr()))	; 
			        	 }
			        	 
				     }
			        	 }
		         }catch(Exception e){
		        	 e.printStackTrace();	 
		         }
			    
			    /*if((rollUpInvoiceFlag!=null && !rollUpInvoiceFlag.equalsIgnoreCase("")) && rollUpInvoiceFlag.equalsIgnoreCase("True")){
			    	if(SoContract!=null && !SoContract.equalsIgnoreCase("")){
						  if(!costElementFlag)	{
							  chargeCodeList=chargesManager.findChargeCode(defaultAccountLine.getChargeCode(), SoContract);
						  } else{
							  chargeCodeList=chargesManager.findChargeCodeCostElement(defaultAccountLine.getChargeCode(), SoContract,sessionCorpID,jobtypeSO, routingSO,companyDivisionSO);  
						  }
			    	}
			    	if(chargeCodeList!=null && !chargeCodeList.isEmpty() && chargeCodeList.get(0)!=null && !chargeCodeList.get(0).toString().equalsIgnoreCase("")){
					try{
						String[] rollUpInInvoiceArr = chargeCodeList.get(0).toString().split("#");
						if(rollUpInInvoiceArr.length>13){
							String rollUpInInvoice = rollUpInInvoiceArr[13];
							if(rollUpInInvoice.equalsIgnoreCase("Y")){
								rollUpInInvoice="true";
								ignoreForBilling=Boolean.parseBoolean(rollUpInInvoice);
								accountLine.setRollUpInInvoice(ignoreForBilling);									
							}else{
								rollUpInInvoice="false";
								ignoreForBilling=Boolean.parseBoolean(rollUpInInvoice);
								accountLine.setRollUpInInvoice(ignoreForBilling);
							}
						}							
					}catch(Exception e){
						e.printStackTrace();
					}
			   	}
			  }*/
		         try{
		        	 if(systemDefault!=null && systemDefault.getVatCalculation() && !accountLine.getVATExclude()){
		        		 
		        			Double estVatAmt=0.00;
		        			Double payVatPercent=0.00;
		        			payVatPercent=(accountLine.getRecVatPercent()==null?0.00:Double.parseDouble(accountLine.getRecVatPercent()));
		        			estVatAmt=(accountLine.getEstimateRevenueAmount()==null?0.00:Double.parseDouble(accountLine.getEstimateRevenueAmount()+""));
		        			if(contractType){
		        				estVatAmt=(accountLine.getEstSellLocalAmount()==null?0.00:Double.parseDouble(accountLine.getEstSellLocalAmount()+""));
		        			}
		        			estVatAmt=(estVatAmt*payVatPercent)/100;
		        			accountLine.setEstVatAmt(new BigDecimal(estVatAmt+""));
		        			estVatAmt=0.00;
		        			payVatPercent=0.00;
		        			payVatPercent=(accountLine.getPayVatPercent()==null?0.00:Double.parseDouble(accountLine.getPayVatPercent()+""));
		        			estVatAmt=(accountLine.getEstimateExpense()==null?0.00:Double.parseDouble(accountLine.getEstimateExpense()+""));
		        			if(contractType){
		        				estVatAmt=(accountLine.getEstLocalAmount()==null?0.00:Double.parseDouble(accountLine.getEstLocalAmount()+""));
		        			}
		        			estVatAmt=(estVatAmt*payVatPercent)/100;
		        			accountLine.setEstExpVatAmt(new BigDecimal(estVatAmt+""));
		        		 
		        	 }
		         }catch(Exception e){
		        	 e.printStackTrace();	 
		         }
			      if(accountLine.getVATExclude() !=null && accountLine.getVATExclude()){
			           accountLine.setRecVatDescr("");
			           accountLine.setRecVatPercent("");
			           accountLine.setRecVatAmt(new BigDecimal(0));
			           accountLine.setPayVatDescr("");
			           accountLine.setPayVatPercent("");
			           accountLine.setPayVatAmt(new BigDecimal(0));
			           accountLine.setEstVatAmt(new BigDecimal(0));
			           accountLine.setEstExpVatAmt(new BigDecimal(0));
			           accountLine.setRevisionVatAmt(new BigDecimal(0));
			           accountLine.setRevisionExpVatAmt(new BigDecimal(0));
			         }						      			         
			accountLine = accountLineManager.save(accountLine);
			}
			
			accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
			if(accountLineList == null || accountLineList.isEmpty()){
				emptyList="true";
			}
			companyDivis.add("");
			companyDivis.addAll(customerFileManager.findCompanyDivisionByBookAgAndCompanyDivision(serviceOrder.getCorpID(), "",serviceOrder.getCompanyDivision()));
			Iterator<AccountLine> idIterator =  accountLineList.iterator();
			accountLineList=new ArrayList();
			while (idIterator.hasNext()){
				AccountLine accountLine = (AccountLine) idIterator.next(); 
				try{
					if(accountLine.getCompanyDivision()!=null && (!(accountLine.getCompanyDivision().equals("")))){	
						if(companyDivis.contains(accountLine.getCompanyDivision())){
						}else{
							companyDivis.add(accountLine.getCompanyDivision());	
						}
					}
				}catch(Exception e){}
				
				try{
					  if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup())	{
						   String networkSynchedId=	accountLineManager.getNetworkSynchedId(accountLine.getId(),sessionCorpID, trackingStatus.getNetworkPartnerCode());
					      	if(!(networkSynchedId.equals(""))){
					      		synchedAccountLine= accountLineManager.getForOtherCorpid(Long.parseLong(networkSynchedId));
					      		Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
					      		boolean accNonEditable=false;
								 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
									 accNonEditable =true;
								 }
								 if(accNonEditable){
									 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
						      			 //utsiRecAccDateFlag=true;
										 accountLine.setUtsiRecAccDateFlag(true);
						      		}
									 if(synchedAccountLine.getInvoiceNumber()!=null && (!(synchedAccountLine.getInvoiceNumber().toString().toString().equals("")))){
						      			 //utsiPayAccDateFlag=true;
										 accountLine.setUtsiPayAccDateFlag(true);
						      		}
								 }else{
					      		if(synchedAccountLine.getRecAccDate()!=null && accountLine.getRecAccDate()== null){
					      			 //utsiRecAccDateFlag=true;
					      			accountLine.setUtsiRecAccDateFlag(true);
					      		}
					      		if(synchedAccountLine.getPayAccDate()!=null && accountLine.getRecAccDate()== null){
					      			 //utsiPayAccDateFlag=true;
					      			accountLine.setUtsiPayAccDateFlag(true);
					      		}
							} 	
					      	} 
					      	accountLineList.add(accountLine);
					  	}else{
					  		accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
					  	}
					}catch(Exception e){}
				}
			for(int i = 0;i<companyDivis.size();i++){
				companyDivisionMap.put((String) companyDivis.get(i), (String) companyDivis.get(i));
			}
			if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String	contractTypeValue="";
				contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
				if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
					contractType=true;	
				}
			}
			baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID); 
		    
			if((accountLineList!=null)&&(!accountLineList.isEmpty())){
				discountMap=new HashMap<String, Map<String,String>>();
				List<AccountLine> cc =accountLineList;
				List<Discount> ds=null;
				Map<String,String> tempData=null;
				ds=discountManager.discountMapList(sessionCorpID);
				for(AccountLine acclineDis:cc){
					//ds=discountManager.discountList(billing.getContract(),acclineDis.getChargeCode(),sessionCorpID);
					tempData=new HashMap<String, String>();
					tempData.put("", "");
					for(Discount discountRec:ds){
						if((discountRec.getChargeCode()!=null)&&(discountRec.getChargeCode().equalsIgnoreCase(acclineDis.getChargeCode()))&&(discountRec.getContract()!=null)&&(discountRec.getContract().equalsIgnoreCase(billing.getContract()))){
							tempData.put(discountRec.getDiscount()+"", discountRec.getDescription());
						}
					}
					discountMap.put(acclineDis.getId()+"", tempData);
				}
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return CANCEL;
		}
		//updateAccountLineAjax(serviceOrder);
		return SUCCESS;
	
	}	
	@SkipValidation
	public String inActivePricingLineAjax(){
		try {
			ServiceOrder serviceOrder = serviceOrderManager.get(id);
			trackingStatus =trackingStatusManager.get(sid);
			String loginUser = getRequest().getRemoteUser();	
			boolean soNetworkGroup=false;
			if(trackingStatus.getAccNetworkGroup()!=null){
				soNetworkGroup=trackingStatus.getAccNetworkGroup();
			}
			if(inActiveLine != null && inActiveLine.trim().length() > 0){
				String arr[] = inActiveLine.split(",");
				for (String id : arr) {
					Long id1 = Long.parseLong(id);
					AccountLine accLine = accountLineManager.get(id1);
					accLine.setStatus(false);
					accLine.setUpdatedOn(new Date());
					accLine.setUpdatedBy(getRequest().getRemoteUser());											
					accountLineManager.save(accLine);
					
					if(soNetworkGroup){
					int payPost=accountLineManager.updateAccInactive(id,sessionCorpID,soNetworkGroup,sid,loginUser);
					}
					
					
				}
				
				updateAccountLineAjax(serviceOrder);
				if(soNetworkGroup){
					List linkedShipNumber=findLinkerShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					Iterator  it=serviceOrderRecords.iterator();
			    	while(it.hasNext()){
			    		try{
			    		serviceOrderToRecods=(ServiceOrder)it.next(); 
			    		trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
			    		if(trackingStatusToRecods.getSoNetworkGroup() && (!trackingStatusToRecods.getAccNetworkGroup())  && (!(serviceOrder.getShipNumber().toString().equalsIgnoreCase(serviceOrderToRecods.getShipNumber().toString())))){
			    			updateExternalAccountLine(serviceOrderToRecods,serviceOrderToRecods.getCorpID());		
			        	}
			    		}catch(Exception e){
			    			
			    		}
			    	}
					
				}
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return CANCEL;
		}
		
		return SUCCESS;
	
	}
	 @SkipValidation
	    public String updateAccountLineAjax(ServiceOrder serviceOrder){ 
		 try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
					BigDecimal cuDivisormar=new BigDecimal(100);
					if((billingManager.checkById(sid)).isEmpty())
					{ 
						billingFlag=0; 
					}else{ 
						billingFlag=1; 
					}
					BigDecimal estimateRevSum=new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString());
					BigDecimal distributedAmountSum=new BigDecimal((accountLineManager.getdistributedAmountSum(serviceOrder.getShipNumber())).get(0).toString());
					BigDecimal projectedDistributedTotalAmountSum=new BigDecimal((accountLineManager.getProjectedDistributedTotalAmountSum(serviceOrder.getShipNumber())).get(0).toString());
					BigDecimal projectedActualExpenseSum=new BigDecimal((accountLineManager.getProjectedActualExpenseSum(serviceOrder.getShipNumber())).get(0).toString());
					BigDecimal projectedActualRevenueSum=new BigDecimal((accountLineManager.getProjectedActualRevenueSum(serviceOrder.getShipNumber())).get(0).toString());
					BigDecimal projectedActualRevenueSumHVY=new BigDecimal((accountLineManager.getProjectedActualRevenueSumHVY(serviceOrder.getShipNumber())).get(0).toString());
					BigDecimal estimateExpSum=new BigDecimal((accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString());
					BigDecimal revisionRevSum=new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString());
					BigDecimal revisionExpSum=new BigDecimal((accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString());
					BigDecimal entitleSum=new BigDecimal((accountLineManager.getEntitleSum(serviceOrder.getShipNumber())).get(0).toString());
					BigDecimal actualExpSum=new BigDecimal((accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString());
					BigDecimal actualRevSum=new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString());
					BigDecimal projectedRSum=accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV");
					BigDecimal projectedESum=accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"EXP");
					
					serviceOrder.setEstimatedTotalRevenue(estimateRevSum);
					serviceOrder.setDistributedTotalAmount(distributedAmountSum);
					serviceOrder.setProjectedDistributedTotalAmount(projectedDistributedTotalAmountSum);
					serviceOrder.setProjectedActualExpense(projectedActualExpenseSum);
					if(serviceOrder.getJob().equals("HVY")){
					BigDecimal sum = projectedActualRevenueSum;
					sum=sum.add(projectedActualRevenueSumHVY);
					serviceOrder.setProjectedActualRevenue(sum);
					
					}else{
						serviceOrder.setProjectedActualRevenue(projectedActualRevenueSum);
					}
					BigDecimal divisormarEst=estimateRevSum;
					
					if(serviceOrder.getEstimatedTotalRevenue()==null || serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0"))||serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.00")))
					{
				       serviceOrder.setEstimatedGrossMarginPercentage(new BigDecimal("0"));
					} 
					else 
					{
					serviceOrder.setEstimatedGrossMarginPercentage((((estimateRevSum.subtract (estimateExpSum))).multiply(cuDivisormar)).divide(divisormarEst,2));
					}
					serviceOrder.setEstimatedGrossMargin((estimateRevSum.subtract (estimateExpSum)));
					serviceOrder.setRevisedGrossMargin((revisionRevSum.subtract (revisionExpSum)));
					serviceOrder.setRevisedTotalRevenue(revisionRevSum);
					BigDecimal divisormarEstPer=revisionRevSum;
					if(serviceOrder.getRevisedTotalRevenue() == null || serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0"))||serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.00"))){
					   serviceOrder.setRevisedGrossMarginPercentage(new BigDecimal("0"));
					}
					else
					{
					serviceOrder.setRevisedGrossMarginPercentage((((revisionRevSum.subtract (revisionExpSum))).multiply(cuDivisormar)).divide(divisormarEstPer,2));
					}
					serviceOrder.setRevisedTotalExpense(revisionExpSum);
					serviceOrder.setEstimatedTotalExpense(estimateExpSum);
					serviceOrder.setEntitledTotalAmount(entitleSum);
					serviceOrder.setActualExpense(actualExpSum);
					if(serviceOrder.getJob().equals("HVY")){
						BigDecimal sum =actualRevSum;
						if(sum !=null && sum.doubleValue()>0){
						sum=sum.add(projectedActualRevenueSumHVY);
						serviceOrder.setActualRevenue(sum);
						}else{
							serviceOrder.setActualRevenue(new BigDecimal(0.00));	
						}
					}else{
						serviceOrder.setActualRevenue(actualRevSum);
					     }
					serviceOrder.setActualGrossMargin((actualRevSum.subtract (actualExpSum)));
					BigDecimal divisormar=actualRevSum;
					if(serviceOrder.getActualRevenue()== null || serviceOrder.getActualRevenue().equals(new BigDecimal("0"))||serviceOrder.getActualRevenue().equals(new BigDecimal("0.00")))
					{
						serviceOrder.setActualGrossMarginPercentage(new BigDecimal("0"));
					}
					else
					{
					   serviceOrder.setActualGrossMarginPercentage((((actualRevSum.subtract (actualExpSum))).multiply(cuDivisormar)).divide(divisormar,2));
					}
					/*# 7784 - Always show actual gross margin in accountline overview Start*/
					try{
					serviceOrder.setProjectedGrossMargin(projectedRSum.subtract (projectedESum));
					if(projectedRSum.doubleValue()!=0){
						serviceOrder.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((serviceOrder.getProjectedGrossMargin()).doubleValue()/(projectedRSum).doubleValue())*100));
					}else{
						serviceOrder.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
					}
					}catch(Exception e){}
					/*# 7784 - Always show actual gross margin in accountline overview End*/
					serviceOrder.setUpdatedOn(new Date());
					serviceOrder.setUpdatedBy(getRequest().getRemoteUser());
					try{
					serviceOrderManager.updateFromAccountLine(serviceOrder);
					}catch(Exception e){
						e.printStackTrace();
						
					}
					serviceOrder = serviceOrderManager.get(sid);
					if(accountLineStatus==null||accountLineStatus.equals(""))
					{
						accountLineStatus="true";
					}
					if(!"category".equals(vanLineAccountView)){
				    	accountLineList = accountLineManager.getAccountLineList(serviceOrder.getShipNumber(),accountLineStatus);
				    	getSession().setAttribute("accountLines", accountLineList);
					}
					
					if("category".equals(vanLineAccountView)){
				    	   vanLineAccCategoryURL = "?sid="+serviceOrder.getId()+"&vanLineAccountView="+vanLineAccountView;
				    	   return "category"; 
				    }
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
		    	 return CANCEL;
			}
		    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		    		return SUCCESS;
		    }
		@SkipValidation
	    private List findLinkerShipNumber(ServiceOrder serviceOrder) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	List linkedShipNumberList= new ArrayList();
	    	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
	    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
	    	}else{
	    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
	    	}
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return linkedShipNumberList;
	    }
		@SkipValidation
	    private List<Object> findServiceOrderRecords(List linkedShipNumber,	ServiceOrder serviceOrderLocal) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			List<Object> recordList= new ArrayList();
	    	Iterator it =linkedShipNumber.iterator();
	    	while(it.hasNext()){
	    		String shipNumber= it.next().toString();
	    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(shipNumber));
	    		recordList.add(serviceOrderRemote);
	    	}
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return recordList;
	    	}
		@SkipValidation
	    public void updateExternalAccountLine( ServiceOrder externalServiceOrder,String externalCorpID){ 
	    	try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				BigDecimal cuDivisormar=new BigDecimal(100);
				 
				externalServiceOrder.setEstimatedTotalRevenue(new BigDecimal((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setDistributedTotalAmount(new BigDecimal((accountLineManager.getExternaldistributedAmountSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setProjectedDistributedTotalAmount(new BigDecimal((accountLineManager.getExternalProjectedDistributedTotalAmountSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setProjectedActualExpense(new BigDecimal((accountLineManager.getExternalProjectedActualExpenseSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setProjectedActualRevenue(new BigDecimal((accountLineManager.getExternalProjectedActualRevenueSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				BigDecimal divisormarEst=new BigDecimal(((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())); 
				if(externalServiceOrder.getEstimatedTotalRevenue()==null || externalServiceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0"))||externalServiceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.00"))){
					externalServiceOrder.setEstimatedGrossMarginPercentage(new BigDecimal("0"));
				} 
				else 
				{
					externalServiceOrder.setEstimatedGrossMarginPercentage((((new BigDecimal((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalEstimateExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())))).multiply(cuDivisormar)).divide(divisormarEst,2));
				} 
				externalServiceOrder.setEstimatedGrossMargin((new BigDecimal((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalEstimateExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()))));
				externalServiceOrder.setRevisedGrossMargin((new BigDecimal((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalRevisionExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()))));
				externalServiceOrder.setRevisedTotalRevenue(new BigDecimal((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				BigDecimal divisormarEstPer=new BigDecimal(((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				if(externalServiceOrder.getRevisedTotalRevenue() == null || externalServiceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0"))||externalServiceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.00"))){
					externalServiceOrder.setRevisedGrossMarginPercentage(new BigDecimal("0"));
				}
				else{
					externalServiceOrder.setRevisedGrossMarginPercentage((((new BigDecimal((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalRevisionExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())))).multiply(cuDivisormar)).divide(divisormarEstPer,2));
				} 
				externalServiceOrder.setRevisedTotalExpense(new BigDecimal((accountLineManager.getExternalRevisionExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setEstimatedTotalExpense(new BigDecimal((accountLineManager.getExternalEstimateExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setEntitledTotalAmount(new BigDecimal((accountLineManager.getExternalEntitleSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setActualExpense(new BigDecimal((accountLineManager.getExternalActualExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setActualRevenue(new BigDecimal((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setActualGrossMargin((new BigDecimal((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalActualExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()))));
				BigDecimal divisormar=new BigDecimal(((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				if(externalServiceOrder.getActualRevenue()== null || externalServiceOrder.getActualRevenue().equals(new BigDecimal("0"))||externalServiceOrder.getActualRevenue().equals(new BigDecimal("0.00"))){
					externalServiceOrder.setActualGrossMarginPercentage(new BigDecimal("0"));
				}
				else{
					externalServiceOrder.setActualGrossMarginPercentage((((new BigDecimal((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalActualExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())))).multiply(cuDivisormar)).divide(divisormar,2));
				}
				/*# 7784 - Always show actual gross margin in accountline overview Start*/
				try{
					externalServiceOrder.setProjectedGrossMargin(accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"REV").subtract (accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"EXP")));
				if(accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"REV").doubleValue()!=0){
					externalServiceOrder.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((externalServiceOrder.getProjectedGrossMargin()).doubleValue()/(accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"REV")).doubleValue())*100));				
				}else{
					externalServiceOrder.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
				}
				}catch(Exception e){}
				/*# 7784 - Always show actual gross margin in accountline overview End*/
				
				serviceOrderManager.updateFromAccountLine(externalServiceOrder);
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			} 
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");  
	    }		
    	public Date getDateFormat(String dateTemp){
    		try {
				if(dateTemp == null || "".equals(dateTemp.trim())){
					return new Date();
				}else{
					try {
						return CommonUtil.multiParse(dateTemp);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}
    		Date date = new Date();
    
    		return date;
    	}
    	public String pricPointUpdateDetails(Long psSoId,String tariffId,ServiceOrder serviceOrder,Miscellaneous miscellaneous,List<SystemDefault> sysDefaultDetail,List modeValue){
			//serviceOrder=serviceOrderManager.get(psSoId);
		    //miscellaneous = miscellaneousManager.get(psSoId);
		    String corpIdBaseCurrency="";
		    //sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					corpIdBaseCurrency=systemDefault.getBaseCurrency();	            
				}
			}
		    if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
				weightType="lbscft";
			}
			else{
				weightType="kgscbm";
			}
			String pricingForMode="";
			if(serviceOrder.getJob()!=null && serviceOrder.getJob().equalsIgnoreCase("STO")){
				pricingForMode="PS";
			}else{
			if(serviceOrder.getMode()!=null && !serviceOrder.getMode().equals("") && !serviceOrder.getMode().equalsIgnoreCase("Sea")){
				//List modeValue = serviceOrderManager.findPriceCode(serviceOrder.getMode(), "MODE",sessionCorpID);
				 if (modeValue != null && !modeValue.isEmpty() && modeValue.get(0)!=null) {
					 pricingForMode=modeValue.get(0).toString();
				 }
			}		
	        if(serviceOrder.getMode()!=null && !serviceOrder.getMode().equals("") && serviceOrder.getMode().equalsIgnoreCase("Sea")){
				if(serviceOrder.getPackingMode()!=null && !serviceOrder.getPackingMode().equals("")){
					if(serviceOrder.getPackingMode().equals("RORO") || serviceOrder.getPackingMode().equals("GRP") || serviceOrder.getPackingMode().equals("BKBLK") || serviceOrder.getPackingMode().equals("LCL") || serviceOrder.getPackingMode().equals("LVAN")  || serviceOrder.getPackingMode().equals("T/W")){
					      pricingForMode="LCL";
					     }
					     if(serviceOrder.getPackingMode().equals("LOOSE") || serviceOrder.getPackingMode().equals("LVCO") || serviceOrder.getPackingMode().equals("LVCNT") || serviceOrder.getPackingMode().equals("LVCNTR") ){
					      pricingForMode="FCL_L";
					     } 
				}
			}
			}	
			String weight_unit="";
			String volume_unit="";
			if(weightType!=null && !weightType.equalsIgnoreCase("")){
				weight_unit=weightType.substring(0, 3);
				if(weight_unit.equalsIgnoreCase("lbs")){
					weight_unit="lb";
				}else if(weight_unit.equalsIgnoreCase("kgs")){
					weight_unit="kg";
				}
				volume_unit=weightType.substring(3, 6);
				if(volume_unit.equalsIgnoreCase("cbm")){
					volume_unit="mt";
				}
					
			}		
		
			BigDecimal soWeight=new BigDecimal("0.0");
			BigDecimal soVolume=new BigDecimal("0.0");
			
			if(weightType.equalsIgnoreCase("lbscft") && miscellaneous.getEstimatedNetWeight()!=null && !miscellaneous.getEstimatedNetWeight().toString().equals("") && miscellaneous.getNetEstimateCubicFeet()!=null && !miscellaneous.getNetEstimateCubicFeet().toString().equals("") ){
				 soWeight= miscellaneous.getEstimatedNetWeight();
				 soVolume= miscellaneous.getNetEstimateCubicFeet();
				}
		   if(weightType.equalsIgnoreCase("kgscbm") && miscellaneous.getEstimatedNetWeightKilo()!=null && !miscellaneous.getEstimatedNetWeightKilo().toString().equals("") && miscellaneous.getNetEstimateCubicMtr()!=null && !miscellaneous.getNetEstimateCubicMtr().toString().equals("")){
					 soWeight= miscellaneous.getEstimatedNetWeightKilo();
					 soVolume= miscellaneous.getNetEstimateCubicMtr();
			   }		
				Integer Tariff=0;
				if(tariffId !=null && ! tariffId.equals("")){
					Tariff=Integer.parseInt(tariffId.trim());
				}		
				
			String transferee="";
			if(serviceOrder.getFirstName()!=null && !serviceOrder.getFirstName().equals("") && serviceOrder.getLastName()!=null && !serviceOrder.getLastName().equals("") ){
				transferee=serviceOrder.getFirstName()+" "+serviceOrder.getLastName();
			}else{
				transferee=serviceOrder.getFirstName();
			}

			String readDetails="";	
			String readRefId="";
			String quoteStatus="";
			if(serviceOrder.getMoveType()!=null && serviceOrder.getMoveType().equalsIgnoreCase("Quote")){
				if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("PE")){
					quoteStatus="Pending";
				}else if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("AC")){
					quoteStatus="Booked";
				}else if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("RE")){
					quoteStatus="Cancelled";
				}else if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("SU")){
					quoteStatus="Complete";
				}else{
					quoteStatus="Pending";
				}
			//}else if(serviceOrder.getMoveType()!=null && serviceOrder.getMoveType().equalsIgnoreCase("BookedMove")){    					
			}else{
				quoteStatus="Booked";
			}
			try {
				JSONObject obj = new JSONObject();
				obj.put("api_key", "nT6EZ375o8ZoiFT7kD9i2M4074V7j7xJ");
				obj.put("id", tariffId);
				obj.put("currency", corpIdBaseCurrency);
				System.out.println(obj.toString());
				readDetails=PricePointClientServiceCall.readQuotesDetails(obj.toString());
				JSONObject jsr = new JSONObject(readDetails);		
				JSONArray content = jsr.getJSONArray("quotes");
				for(int n = 0; n < content.length(); n++){
				JSONObject record = content.getJSONObject(n);
				   //String pricingWeight=record.getString("declared_weight");
				   //readWeight= new BigDecimal(pricingWeight);
				   //String pricingVolume=record.getString("declared_volume");
				   //readVolume= new BigDecimal(pricingVolume);	
				   readRefId=record.getString("id");	
				  
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			Integer readId=0;
			if(readRefId !=null && ! readRefId.equals("")){
				readId=Integer.parseInt(readRefId.trim());
			}
			try {    				
				String updateSupplement="";
				JSONObject obup = new JSONObject();
				obup.put("api_key", "nT6EZ375o8ZoiFT7kD9i2M4074V7j7xJ");
				obup.put("id", Tariff);
				JSONObject obup1 = new JSONObject();
				obup1.put("id", readId);
				obup1.put("weight", soWeight);
				obup1.put("volume", soVolume);
				obup1.put("type", pricingForMode);
				JSONArray list = new JSONArray();
				list.put(obup1);
				obup.accumulate("shipments", list);
				obup.put("weight_unit", weight_unit);
				obup.put("volume_unit", volume_unit);
				obup.put("transferee", transferee);    			
				obup.put("status", quoteStatus);
				updateSupplement=PricePointClientServiceCall.udateQuotesDetails(obup.toString());			
			} catch (Exception e) {
				// TODO: handle exception
			}
		return SUCCESS;
	   } 
    	@SkipValidation
    	public String saveRequestedQuotePricePoint(String pricePointAgentId,String pricePointMarket,String pricePointTariff,String category,Long id,Long AccId,ServiceOrder serviceOrder,Miscellaneous miscellaneous,List modeValue,Company company){
    		    try {
					//serviceOrder=serviceOrderManager.get(id);
					//miscellaneous = miscellaneousManager.get(id);
					if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
						weightType="lbscft";
					}
					else{
						weightType="kgscbm";
					}
					String pricingForMode="";
					if(serviceOrder.getJob()!=null && serviceOrder.getJob().equalsIgnoreCase("STO")){
						pricingForMode="PS";
					}else{
					if(serviceOrder.getMode()!=null && !serviceOrder.getMode().equals("") && !serviceOrder.getMode().equalsIgnoreCase("Sea")){
						//List modeValue = serviceOrderManager.findPriceCode(serviceOrder.getMode(), "MODE",sessionCorpID);
						 if (modeValue != null && !modeValue.isEmpty() && modeValue.get(0)!=null) {
							 pricingForMode=modeValue.get(0).toString();
						 }
					}		
					if(serviceOrder.getMode()!=null && !serviceOrder.getMode().equals("") && serviceOrder.getMode().equalsIgnoreCase("Sea")){
						if(serviceOrder.getPackingMode()!=null && !serviceOrder.getPackingMode().equals("")){
							if(serviceOrder.getPackingMode().equals("RORO") || serviceOrder.getPackingMode().equals("GRP") || serviceOrder.getPackingMode().equals("BKBLK") || serviceOrder.getPackingMode().equals("LCL") || serviceOrder.getPackingMode().equals("LVAN")  || serviceOrder.getPackingMode().equals("T/W")){
							      pricingForMode="LCL_L";
							     }
							     if(serviceOrder.getPackingMode().equals("LOOSE") || serviceOrder.getPackingMode().equals("LVCO") || serviceOrder.getPackingMode().equals("LVCNT") || serviceOrder.getPackingMode().equals("LVCNTR") ){
							      pricingForMode="FCL_L";
							     } 
						}
					}
					}
					String clientID="";
					Integer client_id=0;
					//company= companyManager.findByCorpID(sessionCorpID).get(0);
					if(company!=null){
						clientID=company.getClientID();
					}
					if(clientID!=null && !clientID.equalsIgnoreCase("")){
						client_id=Integer.parseInt(clientID);
					}
					String weight_unit="";
					String volume_unit="";
					String packService="";
					if(weightType!=null && !weightType.equalsIgnoreCase("")){
						weight_unit=weightType.substring(0, 3);
						if(weight_unit.equalsIgnoreCase("lbs")){
							weight_unit="lb";
						}else if(weight_unit.equalsIgnoreCase("kgs")){
							weight_unit="kg";
						}
						volume_unit=weightType.substring(3, 6);
						if(volume_unit.equalsIgnoreCase("cbm")){
							volume_unit="mt";
						}
							
					}		
					if(category.equalsIgnoreCase("Origin")){
						packService="origin";
					}else if(category.equalsIgnoreCase("Destin")){
						packService="destination";
					}else{
						packService="";	
					}
					Long marketId=Long.parseLong(pricePointMarket);
					BigDecimal soWeight=new BigDecimal("0.0");
					BigDecimal soVolume=new BigDecimal("0.0");
					
					if(weightType.equalsIgnoreCase("lbscft") && miscellaneous.getEstimatedNetWeight()!=null && !miscellaneous.getEstimatedNetWeight().toString().equals("") && miscellaneous.getNetEstimateCubicFeet()!=null && !miscellaneous.getNetEstimateCubicFeet().toString().equals("") ){
						 soWeight= miscellaneous.getEstimatedNetWeight();
						 soVolume= miscellaneous.getNetEstimateCubicFeet();
						}
					if(weightType.equalsIgnoreCase("kgscbm") && miscellaneous.getEstimatedNetWeightKilo()!=null && !miscellaneous.getEstimatedNetWeightKilo().toString().equals("") && miscellaneous.getNetEstimateCubicMtr()!=null && !miscellaneous.getNetEstimateCubicMtr().toString().equals("")){
							 soWeight= miscellaneous.getEstimatedNetWeightKilo();
							 soVolume= miscellaneous.getNetEstimateCubicMtr();
					   }
						Integer marketagent=0;
						if(pricePointAgentId!=null && !pricePointAgentId.equals("")){
							marketagent=Integer.parseInt(pricePointAgentId);
						}
						Integer Tariff=0;
						if(pricePointTariff !=null && ! pricePointTariff.equals("")){
							Tariff=Integer.parseInt(pricePointTariff.trim());
						}
					String transferee="";
					if(serviceOrder.getFirstName()!=null && !serviceOrder.getFirstName().equals("") && serviceOrder.getLastName()!=null && !serviceOrder.getLastName().equals("") ){
						transferee=serviceOrder.getFirstName()+" "+serviceOrder.getLastName();
					}else{
						transferee=serviceOrder.getFirstName();
					}
					String quoteStatus="";
					if(serviceOrder.getMoveType()!=null && serviceOrder.getMoveType().equalsIgnoreCase("Quote")){
    					if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("PE")){
    						quoteStatus="Pending";
    					}else if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("AC")){
    						quoteStatus="Booked";
    					}else if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("RE")){
    						quoteStatus="Cancelled";
    					}else if(serviceOrder.getQuoteStatus()!=null && serviceOrder.getQuoteStatus().equalsIgnoreCase("SU")){
    						quoteStatus="Complete";
    					}else{
    						quoteStatus="Pending";
    					}			
    				}else{
    					quoteStatus="Booked";
    				}
					String priceId="";
					 try {
						String pricingSupplement="";
						JSONObject obj = new JSONObject();
						obj.put("api_key", "nT6EZ375o8ZoiFT7kD9i2M4074V7j7xJ");
						obj.put("client", client_id);
						obj.put("market", marketId);
						obj.put("agent", marketagent);
						JSONObject obj1 = new JSONObject();
						obj1.put("type", pricingForMode);
						obj1.put("weight", soWeight);
						obj1.put("volume", soVolume);
						obj1.put("tariff", Tariff);
						JSONArray list = new JSONArray();
						     list.put(obj1);		
						 obj.accumulate("shipments", list);				
						 obj.put("service", packService);
						 obj.put("weight_unit", weight_unit);
						 obj.put("volume_unit", volume_unit);
						 obj.put("transferee", transferee);
						 
						 obj.put("status", quoteStatus);
						System.out.println(obj.toString());
						pricingSupplement=PricePointClientServiceCall.saveQuotesDetails(obj.toString());
						PricingDetailsValue dto = null;
						JSONObject jsr = new JSONObject(pricingSupplement);				
						for(int n = 0; n < jsr.length(); n++){
						JSONObject record = jsr;
						String status=record.getString("status");
						priceId=record.getString("id");	
						System.out.println("priceId"+priceId);
						}
						//serviceOrderManager.tariffIdUpdate(AccId,priceId,sessionCorpID);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return priceId;
				} catch (NumberFormatException e) {
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			    	 e.printStackTrace();
			    	 return CANCEL;
				}
    	}   
    	
    	 @SkipValidation
 	    private void createAccountLine(List<Object> records, ServiceOrder  serviceOrder,AccountLine accountLine) {
 			try {
 				Iterator  it=records.iterator();
 				while(it.hasNext()){
 					  serviceOrderToRecods=(ServiceOrder)it.next();
 					try{
 					trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
 					 billingRecods=billingManager.getForOtherCorpid(serviceOrderToRecods.getId()); 
 					if(trackingStatusToRecods.getSoNetworkGroup() && (!trackingStatusToRecods.getAccNetworkGroup())  && (!(serviceOrder.getShipNumber().toString().equalsIgnoreCase(serviceOrderToRecods.getShipNumber().toString())))){
 						trackingStatus=trackingStatusManager.get(serviceOrder.getId()); 
 						String externalCorpID = trackingStatusToRecods.getCorpID();
 		        		String externalBaseCurrency=accountLineManager.searchBaseCurrency(externalCorpID).get(0).toString();
 		        		UTSIeuVatPercentList=refMasterManager.findVatPercentList(externalCorpID, "EUVAT");
 		        		UTSIpayVatPercentList = refMasterManager.findVatPercentList(externalCorpID, "PAYVATDESC");
 						List fieldToSync=customerFileManager.findAccFieldToSyncCreate("AccountLine","CMM");
 						AccountLine accountLineNew= new AccountLine();
 						try{
 							Iterator it1=fieldToSync.iterator();
 							while(it1.hasNext()){
 							String field=it1.next().toString();
 							String[] splitField=field.split("~");
 							String fieldFrom=splitField[0];
 							String fieldTo=splitField[1];
 							PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
 							try{
 								if(fieldFrom!=null && fieldTo!=null && fieldFrom.trim().equalsIgnoreCase("recRateExchange") && fieldTo.trim().equalsIgnoreCase("exchangeRate"))	{
 									BigDecimal payExchangeRateBig=accountLine.getRecRateExchange();
 									if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
 									payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
 									accountLineNew.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
 									} 
 								}else{
 							        beanUtilsBean.setProperty(accountLineNew,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
 								}
 							}catch(Exception ex){
 								logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
 							}
 							
 							}
 						}catch(Exception e){
 							
 						}
 						
 				        accountLineNew.setNetworkSynchedId(accountLine.getId());
 				        accountLineNew.setAccountLineNumber(accountLine.getAccountLineNumber()); 
 				        accountLineNew.setCompanyDivision(serviceOrderToRecods.getCompanyDivision());
 				        accountLineNew.setVendorCode(serviceOrderToRecods.getBookingAgentCode());
 				        accountLineNew.setEstimateVendorName(serviceOrderToRecods.getBookingAgentName());
 				         String actCode="";
 				         String  companyDivisionAcctgCodeUnique1="";
 						 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(externalCorpID);
 						 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
 							 companyDivisionAcctgCodeUnique1 =companyDivisionAcctgCodeUniqueList.get(0).toString();
 						 }
 				        if(companyDivisionAcctgCodeUnique1.equalsIgnoreCase("Y")){
 							actCode=partnerManager.getAccountCrossReference(serviceOrderToRecods.getBookingAgentCode(),serviceOrderToRecods.getCompanyDivision(),externalCorpID);
 						}else{
 							actCode=partnerManager.getAccountCrossReferenceUnique(serviceOrderToRecods.getBookingAgentCode(),externalCorpID);
 						}
 				        accountLineNew.setActgCode(actCode);
 				        if(accountLineNew.getEstimateRevenueAmount() !=null && accountLineNew.getEstimateRevenueAmount().doubleValue()>0){
 				        accountLineNew.setEstimatePassPercentage(100);
 				        }
 				        if(accountLineNew.getRevisionRevenueAmount() !=null && accountLineNew.getRevisionRevenueAmount().doubleValue()>0){
 				        accountLineNew.setRevisionPassPercentage(100);
 				        }
 				        
 						String chargeStr="";
 						List glList = accountLineManager.findChargeDetailFromSO(billingRecods.getContract(),accountLineNew.getChargeCode(),externalCorpID,serviceOrderToRecods.getJob(),serviceOrderToRecods.getRouting(),serviceOrderToRecods.getCompanyDivision());
 						if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
 							  chargeStr= glList.get(0).toString();
 						  }
 						   if(!chargeStr.equalsIgnoreCase("")){
 							  String [] chrageDetailArr = chargeStr.split("#");
 							  accountLineNew.setRecGl(chrageDetailArr[1]);
 							  accountLineNew.setPayGl(chrageDetailArr[2]);
 							}else{
 								accountLineNew.setRecGl("");
 								accountLineNew.setPayGl("");
 						  }
 					//}
 				        accountLineNew.setId(null);
 				        accountLineNew.setCorpID(externalCorpID);
 				        accountLineNew.setCreatedBy("Networking");
 				        accountLineNew.setUpdatedBy("Networking");
 				        accountLineNew.setCreatedOn(new Date());
 				        accountLineNew.setUpdatedOn(new Date()); 
 				       
 						accountLineNew.setRacValueDate(new Date());
 						
 						accountLineNew.setContractValueDate(new Date());
 					    
 						accountLineNew.setValueDate(new Date());
 					    
 						BigDecimal payExchangeRateBig=accountLine.getRecRateExchange();
 						if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
 							payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
 							accountLineNew.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
 						}
 						
 						if(externalBaseCurrency !=null && baseCurrency!=null && (!(baseCurrency.trim().equalsIgnoreCase(externalBaseCurrency.trim())))){
 							DecimalFormat decimalFormatExchangeRate = new DecimalFormat("#.####");
 							String estExchangeRate="1"; 
 				            BigDecimal estExchangeRateBig=new BigDecimal("1.0000");
 				            List estRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstSellCurrency());
 							 if((estRate!=null)&&(!estRate.isEmpty())&& estRate.get(0)!=null && (!(estRate.get(0).toString().equals(""))))
 							 {
 								 estExchangeRate=estRate.get(0).toString();
 							 
 							 } 
 							estExchangeRateBig=new BigDecimal(estExchangeRate);	
 							accountLineNew.setEstSellExchangeRate(estExchangeRateBig); 
 							accountLineNew.setEstExchangeRate(estExchangeRateBig);
 							if(accountLine.getEstSellLocalRate()!=null && accountLine.getEstSellLocalRate().doubleValue()!=0){
 							accountLineNew.setEstimateSellRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
 							accountLineNew.setEstimateRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
 							}
 							if(accountLine.getEstSellLocalAmount()!=null && accountLine.getEstSellLocalAmount().doubleValue()!=0){
 							accountLineNew.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
 							accountLineNew.setEstimateExpense(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
 							} 
 							BigDecimal estcontractExchangeRateBig=new BigDecimal("1");
 							try{ 	
 								estcontractExchangeRateBig=((new BigDecimal(decimalFormatExchangeRate.format((accountLineNew.getEstimateContractRate().doubleValue())/(accountLineNew.getEstimateSellRate().doubleValue()))))); 
 							}catch(Exception es){
 								
 							} 
 							accountLineNew.setEstimateContractExchangeRate(estcontractExchangeRateBig);
 							accountLineNew.setEstimatePayableContractExchangeRate(estcontractExchangeRateBig);
 							
 							
 							String revisionExchangeRate="1"; 
 				            BigDecimal revisionExchangeRateBig=new BigDecimal("1.0000");
 				            List revisionRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionSellCurrency());
 							 if((revisionRate!=null)&&(!revisionRate.isEmpty())&& revisionRate.get(0)!=null && (!(revisionRate.get(0).toString().equals(""))))
 							 {
 								 revisionExchangeRate=revisionRate.get(0).toString();
 							 
 							 } 
 							 revisionExchangeRateBig=new BigDecimal(revisionExchangeRate);	
 							accountLineNew.setRevisionSellExchangeRate(revisionExchangeRateBig);
 							accountLineNew.setRevisionExchangeRate(revisionExchangeRateBig);
 							if(accountLine.getRevisionSellLocalRate()!=null && accountLine.getRevisionSellLocalRate().doubleValue()!=0){
 							accountLineNew.setRevisionSellRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
 							accountLineNew.setRevisionRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
 							}
 							if(accountLine.getRevisionSellLocalAmount()!=null && accountLine.getRevisionSellLocalAmount().doubleValue()!=0){
 							accountLineNew.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
 							accountLineNew.setRevisionExpense(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
 							}
 							
 							
 							
 							BigDecimal revisioncontractExchangeRateBig=new BigDecimal("1");	
 							try{ 
 								revisioncontractExchangeRateBig=((new BigDecimal(decimalFormatExchangeRate.format((accountLineNew.getRevisionContractRate().doubleValue())/(accountLineNew.getRevisionSellRate().doubleValue())))));
 								
 							}catch(Exception es){
 								
 							}
 							accountLineNew.setRevisionContractExchangeRate(revisioncontractExchangeRateBig);
 							accountLineNew.setRevisionPayableContractExchangeRate(revisioncontractExchangeRateBig);
 							 
 							
 							
 							
 							String recExchangeRate="1"; 
 				            BigDecimal recExchangeRateBig=new BigDecimal("1.0000");
 				            List recRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRecRateCurrency());
 							 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
 							 {
 								 recExchangeRate=recRate.get(0).toString();
 							 
 							 } 
 							recExchangeRateBig=new BigDecimal(recExchangeRate);	
 							accountLineNew.setRecRateExchange(recExchangeRateBig);
 							payExchangeRateBig=accountLineNew.getRecRateExchange();
 							if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
 								payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
 								accountLineNew.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
 							}
 							if(accountLine.getRecCurrencyRate()!=null && accountLine.getRecCurrencyRate().doubleValue()!=0){
 							accountLineNew.setRecRate((new BigDecimal(decimalFormatExchangeRate.format((accountLine.getRecCurrencyRate().doubleValue())/(recExchangeRateBig.doubleValue())))));
 							}
 							if(accountLine.getActualRevenueForeign()!=null && accountLine.getActualRevenueForeign().doubleValue()!=0){
 							accountLineNew.setActualRevenue((new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue())/(recExchangeRateBig.doubleValue())))));
 							try{
 								accountLineNew.setActualExpense((new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue())/(recExchangeRateBig.doubleValue())))));
 								}catch(Exception e){
 					    			
 							}
 							
 							
 				    		}
 							BigDecimal contractExchangeRateBig=new BigDecimal("1");	
 							try{
 								
 								contractExchangeRateBig=(new BigDecimal(decimalFormatExchangeRate.format((accountLineNew.getContractRate().doubleValue())/(accountLineNew.getRecRate().doubleValue()))));
 								
 							}catch(Exception es){
 								
 							}
 							accountLineNew.setContractExchangeRate(contractExchangeRateBig);
 							accountLineNew.setPayableContractExchangeRate(contractExchangeRateBig); 
 						}

 						if(accountLine.getRecVatDescr()!=null && (!(accountLine.getRecVatDescr().toString().trim().equals("")))){
 						if(UTSIeuVatPercentList.containsKey(sessionCorpID+"_"+accountLine.getRecVatDescr())){		
 						accountLineNew.setRecVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
 						}else{
 							accountLineNew.setRecVatDescr("");
 							accountLineNew.setRecVatPercent("0");
 							accountLineNew.setRecVatAmt(new BigDecimal(0));
 							accountLineNew.setEstVatAmt(new BigDecimal(0));
 							accountLineNew.setRevisionVatAmt(new BigDecimal(0));
 							}
 						if(UTSIpayVatPercentList.containsKey(sessionCorpID+"_"+accountLine.getRecVatDescr())){ 
 						accountLineNew.setPayVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
 						}else{
 							accountLineNew.setPayVatDescr("");
 							accountLineNew.setPayVatPercent("0");
 							accountLineNew.setPayVatAmt(new BigDecimal(0));
 							accountLineNew.setEstExpVatAmt(new BigDecimal(0)); 
 							accountLineNew.setRevisionExpVatAmt(new BigDecimal(0));
 							}
 						}
 				        accountLineNew.setServiceOrderId(serviceOrderToRecods.getId());
 				        accountLineNew.setSequenceNumber(serviceOrderToRecods.getSequenceNumber());
 				        accountLineNew.setShipNumber(serviceOrderToRecods.getShipNumber());
 				        accountLineNew.setServiceOrder(serviceOrderToRecods);
 				        accountLineNew=accountLineManager.save(accountLineNew);
 				        updateExternalAccountLine(serviceOrderToRecods,externalCorpID);
 					}
 					}catch(Exception e){
 						
 					}
 				}
 			} catch (Exception e) {
 				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
 		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
 		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
 		    	 e.printStackTrace();
 			}
 		}   
    	 @SkipValidation
 	    private void synchornizeDMMAccountLine(AccountLine synchedAccountLine, AccountLine accountLine,TrackingStatus trackingStatus) {
 			try {
 				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
 				Date todayDate=new Date();
	    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd");
	    		String dt11=sdfDestination.format(todayDate);
	    	    todayDate=sdfDestination.parse(dt11);
 				Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
 		  		boolean accEditable=true;
 		  		boolean accPayEditable=true;
 				 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
 					 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
 						 accEditable=false;
 					 }
 					 if(synchedAccountLine.getInvoiceNumber()!=null && (!(synchedAccountLine.getInvoiceNumber().toString().toString().equals("")))){
 						 accPayEditable=false;
 					 }
 				 }else{ 
 		  		 if(synchedAccountLine.getRecAccDate()!=null ){
 		  			accEditable=false;
 		  		 }
 		  		if(synchedAccountLine.getPayAccDate()!=null ){
 		  			accPayEditable=false;
 		  		 }
 				 }
 				if((accEditable || accPayEditable)  && synchedAccountLine.getChargeCode()!=null && (!(synchedAccountLine.getChargeCode().trim().equalsIgnoreCase("DMMFXFEE"))) && (!(synchedAccountLine.getChargeCode().trim().equalsIgnoreCase("DMMFEE")))){
 					String billtocode=synchedAccountLine.getBillToCode();
 					Boolean fXRateOnActualizationDate=false;
 					try{
 					fXRateOnActualizationDate=billingManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId()).getfXRateOnActualizationDate();
 					}catch(Exception e){e.printStackTrace();}
 					
 				boolean estimateContractCurrencyFlag=false;
 				   boolean estimateBillingCurrencyFlag=false;
 				   boolean revisionContractCurrencyFlag=false;
 				   boolean revisionBillingCurrencyFlag=false;
 				   boolean contractCurrencyFlag=false;
 				   boolean  billingCurrencyFlag=false;
 				   boolean  chargeCodeFlag=false;
 				   if((!(accountLine.getEstimateContractCurrency().equals(synchedAccountLine.getEstimateContractCurrency()))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getEstimateContractRate() !=null && synchedAccountLine.getEstimateContractRate()!=null &&    (accountLine.getEstimateContractRate().doubleValue() != synchedAccountLine.getEstimateContractRate().doubleValue()))  || (accountLine.getEstimateContractValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getEstimateContractValueDate())))==0)){
 					   estimateContractCurrencyFlag=true;
 				   }
 				   if((!(accountLine.getEstSellCurrency().equals(synchedAccountLine.getEstimatePayableContractCurrency()))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getEstimateContractRate() !=null && synchedAccountLine.getEstimateContractRate()!=null &&  ( (accountLine.getEstimateContractRate().doubleValue() != synchedAccountLine.getEstimateContractRate().doubleValue()) || (!(accountLine.getEstimateContractCurrency().equals(synchedAccountLine.getEstimateContractCurrency()))) ) )  || (accountLine.getEstSellValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getEstSellValueDate())))==0)){
 					   estimateBillingCurrencyFlag=true;
 				   }
 				   if((!(accountLine.getRevisionContractCurrency().equals(synchedAccountLine.getRevisionContractCurrency())))  || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getRevisionContractRate()!=null && synchedAccountLine.getRevisionContractRate()!=null &&  (accountLine.getRevisionContractRate().doubleValue() != synchedAccountLine.getRevisionContractRate().doubleValue()))  || (accountLine.getRevisionContractValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getRevisionContractValueDate())))==0)){
 					   revisionContractCurrencyFlag=true;
 				   }
 				   if((!(accountLine.getRevisionSellCurrency().equals(synchedAccountLine.getRevisionPayableContractCurrency()))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getRevisionContractRate()!=null && synchedAccountLine.getRevisionContractRate()!=null &&  ( (accountLine.getRevisionContractRate().doubleValue() != synchedAccountLine.getRevisionContractRate().doubleValue()) || (!(accountLine.getRevisionContractCurrency().equals(synchedAccountLine.getRevisionContractCurrency()))) ))  || (accountLine.getRevisionSellValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getRevisionSellValueDate())))==0)){
 					   revisionBillingCurrencyFlag=true;
 				   }
 				   if((!(accountLine.getContractCurrency().equals(synchedAccountLine.getContractCurrency()))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getContractRate() !=null && synchedAccountLine.getContractRate() !=null &&   (accountLine.getContractRate().doubleValue() != synchedAccountLine.getContractRate().doubleValue()))   || (accountLine.getContractValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getContractValueDate())))==0)){
 					   contractCurrencyFlag=true;
 				   }
 				   if((!(accountLine.getRecRateCurrency().equals(synchedAccountLine.getPayableContractCurrency()))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getContractRate() !=null && synchedAccountLine.getContractRate() !=null && ( (accountLine.getContractRate().doubleValue() != synchedAccountLine.getContractRate().doubleValue()) || (!(accountLine.getContractCurrency().equals(synchedAccountLine.getContractCurrency())))  ) )  || (accountLine.getRacValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getRacValueDate())))==0)){
 					   billingCurrencyFlag=true;
 				   }
 				   if((!(accountLine.getChargeCode().equals(synchedAccountLine.getChargeCode())))){
 					   chargeCodeFlag=true;  
 				   }
 				List fieldToSync=customerFileManager.findAccFieldToSync("AccountLine","DMM"); 
 				String companyDivisionUTSI=accountLineManager.getUTSICompanyDivision(synchedAccountLine.getCorpID(),accountLine.getBillToCode()) ;
 				UTSIpayVatList=	refMasterManager.findByParameterUTSIPayVat (sessionCorpID, "PAYVATDESC",synchedAccountLine.getCorpID());
				UTSIpayVatPercentList = refMasterManager.findVatPercentList(synchedAccountLine.getCorpID(), "PAYVATDESC");
 					Iterator it1=fieldToSync.iterator();
 					while(it1.hasNext()){
 						String field=it1.next().toString();
 						String[] splitField=field.split("~");
 						String fieldFrom=splitField[0];
 						String fieldTo=splitField[1];
 						try{
 							//BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
 							//beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
 							PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
 							try{
 							beanUtilsBean.setProperty(synchedAccountLine,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
 							}catch(Exception ex){
 								System.out.println("\n\n\n\n\n error while setting property--->"+ex);
 							}
 							
 						}catch(Exception ex){
 							System.out.println("\n\n\n\n Exception while copying");
 							ex.printStackTrace();
 						}
 						
 					}
 					try{
 						
								
								if(accountLine.getRecVatDescr()!=null && (!(accountLine.getRecVatDescr().toString().trim().equals("")))){ 
									synchedAccountLine.setPayVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr()); 
									}
								String payVatDescr="";
						    	if(UTSIpayVatList.containsKey(synchedAccountLine.getPayVatDescr()+"~"+companyDivisionUTSI)){
						    		payVatDescr=UTSIpayVatList.get(synchedAccountLine.getPayVatDescr()+"~"+companyDivisionUTSI);
						    		synchedAccountLine.setPayVatDescr(payVatDescr);
						    		String payVatPercent="0";
						    		if(UTSIpayVatPercentList.containsKey(payVatDescr)){ 
				    				payVatPercent=UTSIpayVatPercentList.get(payVatDescr);
						    		}
						    		synchedAccountLine.setPayVatPercent(payVatPercent);
						    	} else{
						    		if(UTSIpayVatList.containsKey(synchedAccountLine.getPayVatDescr()+"~NO")){
			 				    		payVatDescr=UTSIpayVatList.get(synchedAccountLine.getPayVatDescr()+"~NO");
			 				    		synchedAccountLine.setPayVatDescr(payVatDescr);
			 				    		String payVatPercent="0";
			 				    		if(UTSIpayVatPercentList.containsKey(payVatDescr)){ 
			 		    				payVatPercent=UTSIpayVatPercentList.get(payVatDescr);
			 				    		}
			 				    		synchedAccountLine.setPayVatPercent(payVatPercent);
			 				    	}else{
			 				    		synchedAccountLine.setPayVatDescr("");
			 				    		synchedAccountLine.setPayVatPercent("0");
			 				    	}
						    	}
						    	if(!(accountLine.getShipNumber().equals(synchedAccountLine.getShipNumber()))){
			 				    	synchedAccountLine.setUpdatedBy(accountLine.getCorpID()+":"+getRequest().getRemoteUser());
			 				    	synchedAccountLine.setUpdatedOn(new Date());
			 				    }
 					}catch(Exception ex){
							System.out.println("\n\n\n\n Exception while copying");
							ex.printStackTrace();
						}
						
 					if(billtocode!=null &&   (!(billtocode.equalsIgnoreCase(synchedAccountLine.getBillToCode())))){		
 						String billingCurrency=serviceOrderManager.getBillingCurrencyValue(synchedAccountLine.getBillToCode(),synchedAccountLine.getCorpID());
 						if(!billingCurrency.equalsIgnoreCase(""))
 						{
 							synchedAccountLine.setRecRateCurrency(billingCurrency);
 							synchedAccountLine.setEstSellCurrency(billingCurrency);
 							synchedAccountLine.setRevisionSellCurrency(billingCurrency); 
 							String recExchangeRate="1"; 
 				            BigDecimal recExchangeRateBig=new BigDecimal("1.0000");
 				            List recRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getRecRateCurrency());
 							 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
 							 {
 								 recExchangeRate=recRate.get(0).toString();
 							 }
 							recExchangeRateBig=new BigDecimal(recExchangeRate);
 							synchedAccountLine.setRecRateExchange(recExchangeRateBig);
 							synchedAccountLine.setRacValueDate(new Date()); 
 							synchedAccountLine.setEstSellExchangeRate(recExchangeRateBig);
 							synchedAccountLine.setEstSellValueDate(new Date()); 
 							synchedAccountLine.setRevisionSellExchangeRate(recExchangeRateBig);
 							synchedAccountLine.setRevisionSellValueDate(new Date());  
 							if(synchedAccountLine.getRecRateCurrency().equalsIgnoreCase(synchedAccountLine.getContractCurrency())){
 								synchedAccountLine.setContractExchangeRate(recExchangeRateBig);
 								synchedAccountLine.setContractValueDate(new Date());
 							}
 							if(synchedAccountLine.getEstSellCurrency().equalsIgnoreCase(synchedAccountLine.getEstimateContractCurrency())){
 								synchedAccountLine.setEstimateContractExchangeRate(recExchangeRateBig);
 								synchedAccountLine.setEstimateContractValueDate(new Date());
 							}
 							if(synchedAccountLine.getRevisionSellCurrency().equalsIgnoreCase(synchedAccountLine.getRevisionContractCurrency())){
 								synchedAccountLine.setRevisionContractExchangeRate(recExchangeRateBig);
 								synchedAccountLine.setRevisionContractValueDate(new Date());
 							}
 						
 						}
 					} 
 					
 					
 					try{
 						if(chargeCodeFlag){
 							ServiceOrder soObj =  serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
 						billingRecods =billingManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
 						 Boolean extCostElement =  accountLineManager.getExternalCostElement(synchedAccountLine.getCorpID());
 						if(!extCostElement){
 					  List  agentGLList=accountLineManager.getGLList(synchedAccountLine.getChargeCode(),billingRecods.getContract(),synchedAccountLine.getCorpID());
 						if(agentGLList!=null && (!(agentGLList.isEmpty())) && agentGLList.get(0)!=null){
 							
 							String[] GLarrayData=agentGLList.get(0).toString().split("#"); 
 							String recGl=GLarrayData[0];
 							String payGl=GLarrayData[1];
 							if(!(recGl.equalsIgnoreCase("NO"))){
 								synchedAccountLine.setRecGl(recGl);
 							}
 							if(!(payGl.equalsIgnoreCase("NO"))){
 								synchedAccountLine.setPayGl(payGl);
 							}
 							String VATExclude=GLarrayData[2];
 							if(VATExclude.equalsIgnoreCase("Y")){
 								synchedAccountLine.setVATExclude(true);
 							}else{
 								synchedAccountLine.setVATExclude(false);
 							}
 						}
 						}else{
 							String chargeStr="";
 							List glList = accountLineManager.findChargeDetailFromSO(billingRecods.getContract(),synchedAccountLine.getChargeCode(),synchedAccountLine.getCorpID(),soObj.getJob(),soObj.getRouting(),soObj.getCompanyDivision());
 							if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
 								  chargeStr= glList.get(0).toString();
 							  }
 							   if(!chargeStr.equalsIgnoreCase("")){
 								  String [] chrageDetailArr = chargeStr.split("#");
 								  synchedAccountLine.setRecGl(chrageDetailArr[1]);
 								  synchedAccountLine.setPayGl(chrageDetailArr[2]);
 								}else{
 									synchedAccountLine.setRecGl("");
 									synchedAccountLine.setPayGl("");
 							 }
 						}
 						}
 						}catch(Exception e){
 							e.printStackTrace();
 							
 						}
 					
 						List bookingAgentCodeList=companyDivisionManager.getBookingAgentCode(accountLine.getCompanyDivision(),sessionCorpID);
 				        if(bookingAgentCodeList!=null && (!(bookingAgentCodeList.isEmpty())) && bookingAgentCodeList.get(0)!=null && (!(bookingAgentCodeList.get(0).toString().trim().equals(""))))
 				        {
 				        	synchedAccountLine.setVendorCode(bookingAgentCodeList.get(0).toString());
 				        	List bookingAgentNamelist=	serviceOrderManager.findByBooking(bookingAgentCodeList.get(0).toString(), sessionCorpID);
 				            
 				        	if(bookingAgentNamelist!=null && (!(bookingAgentNamelist.isEmpty())) && bookingAgentNamelist.get(0)!=null && (!(bookingAgentNamelist.get(0).toString().trim().equals("")))){
 				        		synchedAccountLine.setEstimateVendorName(bookingAgentNamelist.get(0).toString());
 				            }
 				        	
 				        }
 				      
 				         String actCode="";
 				         String  companyDivisionAcctgCodeUnique1="";
 						 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(synchedAccountLine.getCorpID());
 						 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
 							 companyDivisionAcctgCodeUnique1 =companyDivisionAcctgCodeUniqueList.get(0).toString();
 						 }
 				        if(companyDivisionAcctgCodeUnique1.equalsIgnoreCase("Y")){
 							actCode=partnerManager.getAccountCrossReference(synchedAccountLine.getVendorCode(),synchedAccountLine.getCompanyDivision(),synchedAccountLine.getCorpID());
 						}else{
 							actCode=partnerManager.getAccountCrossReferenceUnique(synchedAccountLine.getVendorCode(),synchedAccountLine.getCorpID());
 						}
 				        synchedAccountLine.setActgCode(actCode);
 				        String estimateContractExchangeRate="1"; 
 				        BigDecimal estimateContractExchangeRateBig=synchedAccountLine.getEstimateContractExchangeRate();
 				        if(estimateContractCurrencyFlag){
 				        List estimateContractRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getEstimateContractCurrency ());
 						 if((estimateContractRate!=null)&&(!estimateContractRate.isEmpty())&& estimateContractRate.get(0)!=null && (!(estimateContractRate.get(0).toString().equals(""))))
 						 {
 							 estimateContractExchangeRate=estimateContractRate.get(0).toString();
 						 
 						 } 
 						 estimateContractExchangeRateBig=new BigDecimal(estimateContractExchangeRate);	
 						 synchedAccountLine.setEstimateContractExchangeRate(estimateContractExchangeRateBig); 

 	                       //if(fXRateOnActualizationDate!=null && fXRateOnActualizationDate){
 	                    	   String estSellExchangeRate="1"; 
 	                    	  List estSellRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getEstSellCurrency());
 							 if((estSellRate!=null)&&(!estSellRate.isEmpty())&& estSellRate.get(0)!=null && (!(estSellRate.get(0).toString().equals(""))))
 							 {
 								 estSellExchangeRate=estSellRate.get(0).toString();
 							 }
 							 BigDecimal estSellExchangeRateBig=new BigDecimal(estSellExchangeRate); 
 							 synchedAccountLine.setEstSellExchangeRate(estSellExchangeRateBig);
 							 synchedAccountLine.setEstSellValueDate(new Date());
 	                       //}
 				        }
 						 synchedAccountLine.setEstimateSellRate(new BigDecimal(decimalFormat.format(synchedAccountLine.getEstimateContractRate().doubleValue()/synchedAccountLine.getEstimateContractExchangeRate().doubleValue())));
 						 if(accountLine.getEstimateSellDeviation()!=null && synchedAccountLine.getEstimateContractRateAmmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && synchedAccountLine.getEstimateContractRateAmmount().doubleValue()>0){
 							 synchedAccountLine.setEstimateContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getEstimateContractRateAmmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
 							 }else{
 								 
 							 }
 						 synchedAccountLine.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format(synchedAccountLine.getEstimateContractRateAmmount().doubleValue()/synchedAccountLine.getEstimateContractExchangeRate().doubleValue())));
 						 
 						String estSellExchangeRate="1"; 
 				        BigDecimal estSellExchangeRateBig=synchedAccountLine.getEstimatePayableContractExchangeRate();
 				        
 				        if(estimateBillingCurrencyFlag){
 				        List estSellRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),accountLine.getEstSellCurrency());
 						 if((estSellRate!=null)&&(!estSellRate.isEmpty())&& estSellRate.get(0)!=null && (!(estSellRate.get(0).toString().equals(""))))
 						 {
 							 estSellExchangeRate=estSellRate.get(0).toString();
 						 }
 						 estSellExchangeRateBig=new BigDecimal(estSellExchangeRate);
 						 //synchedAccountLine.setEstSellExchangeRate(estSellExchangeRateBig);
 						 synchedAccountLine.setEstExchangeRate(estSellExchangeRateBig);
 						 synchedAccountLine.setEstimatePayableContractExchangeRate(estSellExchangeRateBig);
 				        }
 				         synchedAccountLine.setEstimatePayableContractRate(accountLine.getEstSellLocalRate());
 				         if(accountLine.getEstimateSellDeviation()!=null && accountLine.getEstSellLocalAmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLine.getEstSellLocalAmount().doubleValue()>0){
 				            	synchedAccountLine.setEstimatePayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
 						 }else{
 				         synchedAccountLine.setEstimatePayableContractRateAmmount(accountLine.getEstSellLocalAmount());
 						 }
 				         synchedAccountLine.setEstLocalRate(accountLine.getEstSellLocalRate());
 				         if(accountLine.getEstimateSellDeviation()!=null && accountLine.getEstSellLocalAmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLine.getEstSellLocalAmount().doubleValue()>0){
 				             synchedAccountLine.setEstLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
 						 }else{
 						 synchedAccountLine.setEstLocalAmount(accountLine.getEstSellLocalAmount());
 						 }
 						 synchedAccountLine.setEstSellLocalRate(synchedAccountLine.getEstimateSellRate().multiply(synchedAccountLine.getEstSellExchangeRate()));
 						 synchedAccountLine.setEstSellLocalAmount(synchedAccountLine.getEstimateRevenueAmount().multiply(synchedAccountLine.getEstSellExchangeRate()));
 						 synchedAccountLine.setEstimateRate(new BigDecimal(decimalFormat.format(synchedAccountLine.getEstimatePayableContractRate().doubleValue()/synchedAccountLine.getEstimatePayableContractExchangeRate().doubleValue())));
 						 synchedAccountLine.setEstimateExpense(new BigDecimal(decimalFormat.format(synchedAccountLine.getEstimatePayableContractRateAmmount().doubleValue()/synchedAccountLine.getEstimatePayableContractExchangeRate().doubleValue())));
 						 
 						 
 						 String revisionContractExchangeRate="1";
 						 
 						 BigDecimal revisionContractExchangeRateBig=synchedAccountLine.getRevisionContractExchangeRate();
 				         if(revisionContractCurrencyFlag){
 				         List revisionContractRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getRevisionContractCurrency ());
 						 if((revisionContractRate!=null)&&(!revisionContractRate.isEmpty())&& revisionContractRate.get(0)!=null && (!(revisionContractRate.get(0).toString().equals(""))))
 						 {
 							 revisionContractExchangeRate=revisionContractRate.get(0).toString();
 						 
 						 } 
 						 revisionContractExchangeRateBig=new BigDecimal(revisionContractExchangeRate);	
 						 synchedAccountLine.setRevisionContractExchangeRate(revisionContractExchangeRateBig);
 						 //if(fXRateOnActualizationDate!=null && fXRateOnActualizationDate ){
 							 String revisionSellExchangeRate="1"; 
 							 List revisionSellRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getRevisionSellCurrency());
 							 if((revisionSellRate!=null)&&(!revisionSellRate.isEmpty())&& revisionSellRate.get(0)!=null && (!(revisionSellRate.get(0).toString().equals(""))))
 							 {
 								 revisionSellExchangeRate=revisionSellRate.get(0).toString();
 							 }
 							 BigDecimal revisionSellExchangeRateBig=new BigDecimal(revisionSellExchangeRate);
 							 synchedAccountLine.setRevisionSellExchangeRate(revisionSellExchangeRateBig);
 							 synchedAccountLine.setRevisionSellValueDate(new Date());
 							  
 						 //}
 				         }
 						 synchedAccountLine.setRevisionSellRate(new BigDecimal(decimalFormat.format(synchedAccountLine.getRevisionContractRate().doubleValue()/synchedAccountLine.getRevisionContractExchangeRate().doubleValue())));
 						 if(accountLine.getRevisionSellDeviation()!=null && synchedAccountLine.getRevisionContractRateAmmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && synchedAccountLine.getRevisionContractRateAmmount().doubleValue()>0){
 							 synchedAccountLine.setRevisionContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getRevisionContractRateAmmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue())));	 
 							 }else{
 								 
 							 }
 						 synchedAccountLine.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format(synchedAccountLine.getRevisionContractRateAmmount().doubleValue()/synchedAccountLine.getRevisionContractExchangeRate().doubleValue())));
 						 
 						 
 						String revisionSellExchangeRate="1"; 
 				        BigDecimal revisionSellExchangeRateBig=synchedAccountLine.getRevisionPayableContractExchangeRate();
 				        
 				        if(revisionBillingCurrencyFlag){
 				        List revisionSellRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),accountLine.getRevisionSellCurrency());
 						 if((revisionSellRate!=null)&&(!revisionSellRate.isEmpty())&& revisionSellRate.get(0)!=null && (!(revisionSellRate.get(0).toString().equals(""))))
 						 {
 							 revisionSellExchangeRate=revisionSellRate.get(0).toString();
 						 }
 						 revisionSellExchangeRateBig=new BigDecimal(revisionSellExchangeRate);
 						 synchedAccountLine.setRevisionExchangeRate(revisionSellExchangeRateBig);
 						 synchedAccountLine.setRevisionPayableContractExchangeRate(revisionSellExchangeRateBig);
 				        } 
 				         synchedAccountLine.setRevisionPayableContractRate(accountLine.getRevisionSellLocalRate());
 				         if(accountLine.getRevisionSellDeviation()!=null && accountLine.getRevisionSellLocalAmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLine.getRevisionSellLocalAmount().doubleValue()>0){
 				        	 synchedAccountLine.setRevisionPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue()))	 );	 
 						 }else{
 				         synchedAccountLine.setRevisionPayableContractRateAmmount(accountLine.getRevisionSellLocalAmount());
 						 }
 				         synchedAccountLine.setRevisionLocalRate(accountLine.getRevisionSellLocalRate());
 				         if(accountLine.getRevisionSellDeviation()!=null && accountLine.getRevisionSellLocalAmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLine.getRevisionSellLocalAmount().doubleValue()>0){
 				          synchedAccountLine.setRevisionLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue()))	 );	 
 						 }else{
 						 synchedAccountLine.setRevisionLocalAmount(accountLine.getRevisionSellLocalAmount());
 						 }
 				         
 						 synchedAccountLine.setRevisionRate(new BigDecimal(decimalFormat.format(synchedAccountLine.getRevisionPayableContractRate().doubleValue()/synchedAccountLine.getRevisionPayableContractExchangeRate().doubleValue())));
 						 synchedAccountLine.setRevisionExpense(new BigDecimal(decimalFormat.format(synchedAccountLine.getRevisionPayableContractRateAmmount().doubleValue()/synchedAccountLine.getRevisionPayableContractExchangeRate().doubleValue())));
 						 synchedAccountLine.setRevisionSellLocalRate(synchedAccountLine.getRevisionSellRate().multiply(synchedAccountLine.getRevisionSellExchangeRate()));
 						 synchedAccountLine.setRevisionSellLocalAmount(synchedAccountLine.getRevisionRevenueAmount().multiply(synchedAccountLine.getRevisionSellExchangeRate()));
 						  
 				          BigDecimal contractExchangeRateBig=synchedAccountLine.getContractExchangeRate();
 				        if(contractCurrencyFlag){
 				        	synchedAccountLine.setContractValueDate(accountLine.getContractValueDate()); 
 				        	 String contractExchangeRate="1"; 
 				            List contractRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID() ,synchedAccountLine.getContractCurrency ());
 							 if((contractRate!=null)&&(!contractRate.isEmpty())&& contractRate.get(0)!=null && (!(contractRate.get(0).toString().equals(""))))
 							 {
 								 contractExchangeRate=contractRate.get(0).toString();
 							 
 							 } 
 							contractExchangeRateBig=new BigDecimal(contractExchangeRate);	
 							//if(fXRateOnActualizationDate!=null && fXRateOnActualizationDate ){
 								
 								String recExchangeRate="1";  
 					            List recRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),synchedAccountLine.getRecRateCurrency());
 								 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
 								 {
 									 recExchangeRate=recRate.get(0).toString();
 								 }
 								 BigDecimal recExchangeRateBig=new BigDecimal(recExchangeRate);
 								synchedAccountLine.setRecRateExchange(recExchangeRateBig);
 								synchedAccountLine.setRacValueDate(new Date());
 								 
 							//}
 				         }
 				            synchedAccountLine.setContractExchangeRate(contractExchangeRateBig); 
 							//accountLineNew.setContractRate(accountLine.getContractRate().divide(contractExchangeRateBig,2)); 
 							synchedAccountLine.setRecRate(new BigDecimal(decimalFormat.format(synchedAccountLine.getContractRate().doubleValue()/synchedAccountLine.getContractExchangeRate().doubleValue())));
 							if(accountLine.getReceivableSellDeviation()!=null && synchedAccountLine.getContractRateAmmount()!=null &&  accountLine.getReceivableSellDeviation().doubleValue()>0 && synchedAccountLine.getContractRateAmmount().doubleValue()>0){
 								synchedAccountLine.setContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getContractRateAmmount().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));	 
 								 }else{
 									 
 								 }
 							synchedAccountLine.setActualRevenue(new BigDecimal(decimalFormat.format(synchedAccountLine.getContractRateAmmount().doubleValue()/synchedAccountLine.getContractExchangeRate().doubleValue())));
 							BigDecimal recExchangeRateBig=synchedAccountLine.getPayableContractExchangeRate();
 							//BigDecimal payExchangeRateBig=synchedAccountLine.getRecRateExchange();
 							if(billingCurrencyFlag){
 								//synchedAccountLine.setRacValueDate(new Date());
 							  String recExchangeRate="1"; 
 				            
 				            List recRate=exchangeRateManager.findAccExchangeRate(synchedAccountLine.getCorpID(),accountLine.getRecRateCurrency());
 							 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
 							 {
 								 recExchangeRate=recRate.get(0).toString();
 							 }
 							recExchangeRateBig=new BigDecimal(recExchangeRate);
 							//synchedAccountLine.setRecRateExchange(recExchangeRateBig);
 							synchedAccountLine.setPayableContractExchangeRate(recExchangeRateBig); 
 							synchedAccountLine.setValueDate(new Date());
 							double payExchangeRate=0; 
 							payExchangeRate=Double.parseDouble(recExchangeRate);
 							synchedAccountLine.setExchangeRate(payExchangeRate);
 							
 							//payExchangeRateBig=new BigDecimal(payExchangeRate);	
 							}
 							synchedAccountLine.setRecCurrencyRate(synchedAccountLine.getRecRate().multiply(synchedAccountLine.getRecRateExchange()));
 							synchedAccountLine.setActualRevenueForeign(synchedAccountLine.getActualRevenue().multiply(synchedAccountLine.getRecRateExchange()));
 							if(accountLine.getReceivableSellDeviation()!=null && accountLine.getActualRevenueForeign()!=null &&  accountLine.getReceivableSellDeviation().doubleValue()>0 && accountLine.getActualRevenueForeign().doubleValue()>0){
 							synchedAccountLine.setPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));
 							synchedAccountLine.setLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));	
 							}else{
 							synchedAccountLine.setLocalAmount(accountLine.getActualRevenueForeign());
 							synchedAccountLine.setPayableContractRateAmmount(accountLine.getActualRevenueForeign());
 							}
 							 try{
 						    synchedAccountLine.setActualExpense(new BigDecimal(decimalFormat.format(synchedAccountLine.getPayableContractRateAmmount().doubleValue()/synchedAccountLine.getPayableContractExchangeRate().doubleValue())));
 								}catch(Exception e){
 									e.printStackTrace();
 					    			
 					    		}
 								try{
 									if(synchedAccountLine.getEstimateSellDeviation()!=null && synchedAccountLine.getEstimateSellDeviation().doubleValue()>0 ){
 										if(synchedAccountLine.getEstimateRevenueAmount()!=null && synchedAccountLine.getEstimateRevenueAmount().doubleValue()>0)
 											synchedAccountLine.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getEstimateRevenueAmount().doubleValue())*((synchedAccountLine.getEstimateSellDeviation().doubleValue())/100))));
 										if(synchedAccountLine.getEstSellLocalAmount()!=null && synchedAccountLine.getEstSellLocalAmount().doubleValue()>0)
 											synchedAccountLine.setEstSellLocalAmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getEstSellLocalAmount().doubleValue())*((synchedAccountLine.getEstimateSellDeviation().doubleValue())/100))));
 										if(synchedAccountLine.getEstimateContractRateAmmount()!=null && synchedAccountLine.getEstimateContractRateAmmount().doubleValue()>0)
 											synchedAccountLine.setEstimateContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getEstimateContractRateAmmount().doubleValue())*((synchedAccountLine.getEstimateSellDeviation().doubleValue())/100))));	
 									}
 									if(synchedAccountLine.getEstimateDeviation()!=null && synchedAccountLine.getEstimateDeviation().doubleValue()>0 ){
 										if(synchedAccountLine.getEstimateExpense()!=null && synchedAccountLine.getEstimateExpense().doubleValue()>0)
 											synchedAccountLine.setEstimateExpense(new BigDecimal(decimalFormat.format((synchedAccountLine.getEstimateExpense().doubleValue())*((synchedAccountLine.getEstimateDeviation().doubleValue())/100))));
 										if(synchedAccountLine.getEstLocalAmount()!=null && synchedAccountLine.getEstLocalAmount().doubleValue()>0)
 											synchedAccountLine.setEstLocalAmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getEstLocalAmount().doubleValue())*((synchedAccountLine.getEstimateDeviation().doubleValue())/100))));
 										if(synchedAccountLine.getEstimatePayableContractRateAmmount()!=null && synchedAccountLine.getEstimatePayableContractRateAmmount().doubleValue()>0)
 											synchedAccountLine.setEstimatePayableContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getEstimatePayableContractRateAmmount().doubleValue())*((synchedAccountLine.getEstimateDeviation().doubleValue())/100))));	
 									}
 									if(synchedAccountLine.getRevisionSellDeviation()!=null && synchedAccountLine.getRevisionSellDeviation().doubleValue()>0 ){
 										if(synchedAccountLine.getRevisionRevenueAmount()!=null && synchedAccountLine.getRevisionRevenueAmount().doubleValue()>0)
 											synchedAccountLine.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getRevisionRevenueAmount().doubleValue())*((synchedAccountLine.getRevisionSellDeviation().doubleValue())/100))));
 										if(synchedAccountLine.getRevisionSellLocalAmount()!=null && synchedAccountLine.getRevisionSellLocalAmount().doubleValue()>0)
 											synchedAccountLine.setRevisionSellLocalAmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getRevisionSellLocalAmount().doubleValue())*((synchedAccountLine.getRevisionSellDeviation().doubleValue())/100))));
 										if(synchedAccountLine.getRevisionContractRateAmmount()!=null && synchedAccountLine.getRevisionContractRateAmmount().doubleValue()>0)
 											synchedAccountLine.setRevisionContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getRevisionContractRateAmmount().doubleValue())*((synchedAccountLine.getRevisionSellDeviation().doubleValue())/100))));	
 									}
 									if(synchedAccountLine.getRevisionDeviation()!=null && synchedAccountLine.getRevisionDeviation().doubleValue()>0 ){
 										if(synchedAccountLine.getRevisionExpense()!=null && synchedAccountLine.getRevisionExpense().doubleValue()>0)
 											synchedAccountLine.setRevisionExpense(new BigDecimal(decimalFormat.format((synchedAccountLine.getRevisionExpense().doubleValue())*((synchedAccountLine.getRevisionDeviation().doubleValue())/100))));
 										if(synchedAccountLine.getRevisionLocalAmount()!=null && synchedAccountLine.getRevisionLocalAmount().doubleValue()>0)
 											synchedAccountLine.setRevisionLocalAmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getRevisionLocalAmount().doubleValue())*((synchedAccountLine.getRevisionDeviation().doubleValue())/100))));
 										if(synchedAccountLine.getRevisionPayableContractRateAmmount()!=null && synchedAccountLine.getRevisionPayableContractRateAmmount().doubleValue()>0)
 											synchedAccountLine.setRevisionPayableContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getRevisionPayableContractRateAmmount().doubleValue())*((synchedAccountLine.getRevisionDeviation().doubleValue())/100))));	
 									}
 									if(synchedAccountLine.getReceivableSellDeviation()!=null && synchedAccountLine.getReceivableSellDeviation().doubleValue()>0 ){
 										if(synchedAccountLine.getActualRevenue()!=null && synchedAccountLine.getActualRevenue().doubleValue()>0)
 											synchedAccountLine.setActualRevenue(new BigDecimal(decimalFormat.format((synchedAccountLine.getActualRevenue().doubleValue())*((synchedAccountLine.getReceivableSellDeviation().doubleValue())/100))));
 										if(synchedAccountLine.getActualRevenueForeign()!=null && synchedAccountLine.getActualRevenueForeign().doubleValue()>0)
 											synchedAccountLine.setActualRevenueForeign(new BigDecimal(decimalFormat.format((synchedAccountLine.getActualRevenueForeign().doubleValue())*((synchedAccountLine.getReceivableSellDeviation().doubleValue())/100))));
 										if(synchedAccountLine.getContractRateAmmount()!=null && synchedAccountLine.getContractRateAmmount().doubleValue()>0)
 											synchedAccountLine.setContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getContractRateAmmount().doubleValue())*((synchedAccountLine.getReceivableSellDeviation().doubleValue())/100))));	
 									}
 									if(synchedAccountLine.getPayDeviation()!=null && synchedAccountLine.getPayDeviation().doubleValue()>0 ){
 										if(synchedAccountLine.getActualExpense()!=null && synchedAccountLine.getActualExpense().doubleValue()>0)
 											synchedAccountLine.setActualExpense(new BigDecimal(decimalFormat.format((synchedAccountLine.getActualExpense().doubleValue())*((synchedAccountLine.getPayDeviation().doubleValue())/100))));
 										if(synchedAccountLine.getLocalAmount()!=null && synchedAccountLine.getLocalAmount().doubleValue()>0)
 											synchedAccountLine.setLocalAmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getLocalAmount().doubleValue())*((synchedAccountLine.getPayDeviation().doubleValue())/100))));
 										if(synchedAccountLine.getPayableContractRateAmmount()!=null && synchedAccountLine.getPayableContractRateAmmount().doubleValue()>0)
 											synchedAccountLine.setPayableContractRateAmmount(new BigDecimal(decimalFormat.format((synchedAccountLine.getPayableContractRateAmmount().doubleValue())*((synchedAccountLine.getPayDeviation().doubleValue())/100))));	
 									} 
 								}catch(Exception e){
 					    			
 					    		}	
 								 try{
 									
 								    String payVatPercent=synchedAccountLine.getPayVatPercent();
 								    BigDecimal payVatPercentBig=new BigDecimal("0.00");
 								    BigDecimal payVatAmmountBig=new BigDecimal("0.00");
 								    BigDecimal estPayVatAmmountBig=new BigDecimal("0.00");
 									BigDecimal revisionpayVatAmmountBig=new BigDecimal("0.00");
 								    if(payVatPercent!=null && (!(payVatPercent.trim().equals("")))){
 								    	payVatPercentBig=new BigDecimal(payVatPercent);
 								    }
 								    payVatAmmountBig= (new BigDecimal(decimalFormat.format(((synchedAccountLine.getLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
 								    try{
 									    estPayVatAmmountBig= (new BigDecimal(decimalFormat.format(((synchedAccountLine.getEstLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
 									    revisionpayVatAmmountBig= (new BigDecimal(decimalFormat.format(((synchedAccountLine.getRevisionLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
 									    
 									  }catch(Exception e){
 											logger.error("Exception Occour: "+ e.getStackTrace()[0]);
 									    }
 								    synchedAccountLine.setPayVatAmt(payVatAmmountBig);
 								    synchedAccountLine.setEstExpVatAmt(estPayVatAmmountBig);
 								    synchedAccountLine.setRevisionExpVatAmt(revisionpayVatAmmountBig);
 								    }catch(Exception e){
 								    	e.printStackTrace();
 								    }
 								   try{
 									    String recVatPercent=synchedAccountLine.getRecVatPercent();
 									    BigDecimal recVatPercentBig=new BigDecimal("0.00");
 									    BigDecimal recVatAmmountBig=new BigDecimal("0.00");
 									    BigDecimal revisionVatAmmountBig=new BigDecimal("0.00");
 									    BigDecimal estVatAmmountBig=new BigDecimal("0.00");
 									    if(recVatPercent!=null && (!(recVatPercent.trim().equals("")))){
 									    	recVatPercentBig=new BigDecimal(recVatPercent);
 									    }
 									   recVatAmmountBig= (new BigDecimal(decimalFormat.format(((synchedAccountLine.getActualRevenueForeign().multiply(recVatPercentBig)).doubleValue())/100)));
 									   try{
 										   estVatAmmountBig= (new BigDecimal(decimalFormat.format(((synchedAccountLine.getEstSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
 										   revisionVatAmmountBig = (new BigDecimal(decimalFormat.format(((synchedAccountLine.getRevisionSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
 										   }catch(Exception e){
 										    	
 										    }
 									   synchedAccountLine.setRecVatAmt(recVatAmmountBig);
 									   synchedAccountLine.setEstVatAmt(estVatAmmountBig); 
 									   synchedAccountLine.setRevisionVatAmt(revisionVatAmmountBig); 
 									  
 									    }catch(Exception e){
 									    	e.printStackTrace();
 									    	
 									    }			    
 					try{
 				    if(!(accountLine.getShipNumber().equals(synchedAccountLine.getShipNumber()))){
 				    	synchedAccountLine.setUpdatedBy(accountLine.getCorpID()+":"+getRequest().getRemoteUser());
 				    	synchedAccountLine.setUpdatedOn(new Date());
 				    }
 				    
 				    
 				    
 					}catch( Exception e){
 				    	e.printStackTrace();
 				    } 
 					try{
 						if(synchedAccountLine.getEstimateRevenueAmount() !=null && synchedAccountLine.getEstimateRevenueAmount().doubleValue()>0 && synchedAccountLine.getEstimateExpense()!=null && synchedAccountLine.getEstimateExpense().doubleValue()>0){
 							Double estimatePassPercentageValue=(synchedAccountLine.getEstimateRevenueAmount().doubleValue()/synchedAccountLine.getEstimateExpense().doubleValue())*100;
 							Integer estimatePassPercentage=estimatePassPercentageValue.intValue();
 							synchedAccountLine.setEstimatePassPercentage(estimatePassPercentage);
 				            }
 				            if(synchedAccountLine.getRevisionRevenueAmount() !=null && synchedAccountLine.getRevisionRevenueAmount().doubleValue()>0 && synchedAccountLine.getRevisionExpense()!=null &&  synchedAccountLine.getRevisionExpense().doubleValue()>0){
 				            	Double revisionPassPercentageValue=(synchedAccountLine.getRevisionRevenueAmount().doubleValue()/synchedAccountLine.getRevisionExpense().doubleValue())*100;
 				            	Integer revisionPassPercentage=revisionPassPercentageValue.intValue();
 				            	synchedAccountLine.setRevisionPassPercentage(revisionPassPercentage);
 				            }
 						}catch(Exception e){
 							e.printStackTrace();
 						}
 						try{
 							boolean billtotype=false; 
 							billtotype =accountLine.getBillToCode().trim().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode().trim());	
 							if(billtotype){
 								
 							}else{
 								synchedAccountLine.setStatus(false)	;
 							}
 						}catch(Exception e){
 							e.printStackTrace();
 						}
 					synchedAccountLine=accountLineManager.save(synchedAccountLine);
 				    serviceOrderRemote=serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
 				    updateExternalAccountLine(serviceOrderRemote,synchedAccountLine.getCorpID());
 				}
 			} catch (Exception e) {
 				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
 		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
 		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
 		    	 e.printStackTrace();
 			}
 	    		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
 		}		
    	 @SkipValidation
 	    private void synchornizeAccountLine(AccountLine synchedAccountLine, AccountLine accountLine) {
 			try {
 				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
 				Date todayDate=new Date();
 				SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd");
 				String dt11=sdfDestination.format(todayDate);
 			    todayDate=sdfDestination.parse(dt11);
 				Company companyUTSI =companyManager.findByCorpID(synchedAccountLine.getCorpID()).get(0);
 		  		boolean accEditable=true;
 		  		boolean accPayEditable=true;
 				 if(companyUTSI!=null && companyUTSI.getAccountLineNonEditable()!=null && companyUTSI.getAccountLineNonEditable().toString().trim().equalsIgnoreCase("Invoicing")){
 					 if(synchedAccountLine.getRecInvoiceNumber()!=null && (!(synchedAccountLine.getRecInvoiceNumber().toString().toString().equals("")))){
 						 accEditable=false;
 					 }
 					 if(synchedAccountLine.getInvoiceNumber()!=null && (!(synchedAccountLine.getInvoiceNumber().toString().toString().equals("")))){
 						 accPayEditable=false;
 					 }
 				 }else{ 
 		  		 if(synchedAccountLine.getRecAccDate()!=null ){
 		  			accEditable=false;
 		  		 }
 		  		if(synchedAccountLine.getPayAccDate()!=null ){
 		  			accPayEditable=false;
 		  		 }
 				 }
 				if(accEditable && accPayEditable){
 					Boolean fXRateOnActualizationDate=false;
 					try{
 					fXRateOnActualizationDate=billingManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId()).getfXRateOnActualizationDate();
 					}catch(Exception e){e.printStackTrace();}
 				boolean chargeCodeFlag=false;
 				if((!(accountLine.getChargeCode().equals(synchedAccountLine.getChargeCode())))){
 					   chargeCodeFlag=true;  
 				   }
 				boolean EstSellCurrencyFlag=false;
 				BigDecimal estExchangeRateBig=synchedAccountLine.getEstSellExchangeRate();
 				if((synchedAccountLine.getEstSellCurrency()!=null && accountLine.getEstSellCurrency()!=null && (!(synchedAccountLine.getEstSellCurrency().toString().trim().equalsIgnoreCase(accountLine.getEstSellCurrency().toString().trim())))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getEstSellLocalRate() !=null && synchedAccountLine.getEstSellLocalRate()!=null &&   (accountLine.getEstSellLocalRate().doubleValue() != synchedAccountLine.getEstSellLocalRate().doubleValue()))   || (accountLine.getEstSellValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getEstSellValueDate())))==0)){
 					EstSellCurrencyFlag=true;
 				}
 				boolean revisionSellCurrencyFlag=false;
 				BigDecimal revisionExchangeRateBig=synchedAccountLine.getRevisionSellExchangeRate();
 				if((synchedAccountLine.getRevisionSellCurrency()!=null && accountLine.getRevisionSellCurrency()!=null && (!(synchedAccountLine.getRevisionSellCurrency().toString().trim().equalsIgnoreCase(accountLine.getRevisionSellCurrency().toString().trim())))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getRevisionSellLocalRate() !=null && synchedAccountLine.getRevisionSellLocalRate()!=null &&   (accountLine.getRevisionSellLocalRate().doubleValue() != synchedAccountLine.getRevisionSellLocalRate().doubleValue()))  || (accountLine.getRevisionSellValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getRevisionSellValueDate())))==0)){
 					revisionSellCurrencyFlag=true;
 				}
 				boolean recRateCurrencyFlag=false;
 				BigDecimal recExchangeRateBig=synchedAccountLine.getRecRateExchange();
 				if((synchedAccountLine.getRecRateCurrency()!=null && accountLine.getRecRateCurrency()!=null && (!(synchedAccountLine.getRecRateCurrency().toString().trim().equalsIgnoreCase(accountLine.getRecRateCurrency().toString().trim())))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getRecCurrencyRate() !=null && synchedAccountLine.getRecCurrencyRate()!=null &&   (accountLine.getRecCurrencyRate().doubleValue() != synchedAccountLine.getRecCurrencyRate().doubleValue()))  || (accountLine.getRacValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getRacValueDate())))==0)){
 					 recRateCurrencyFlag=true;
 				}
 				String externalCorpID = synchedAccountLine.getCorpID();
 				UTSIeuVatPercentList=refMasterManager.findVatPercentList(externalCorpID, "EUVAT");
 				UTSIpayVatPercentList = refMasterManager.findVatPercentList(externalCorpID, "PAYVATDESC");
 				String externalBaseCurrency=accountLineManager.searchBaseCurrency(externalCorpID).get(0).toString();
 				List fieldToSync=customerFileManager.findAccFieldToSync("AccountLine","CMM"); 
 					Iterator it1=fieldToSync.iterator();
 					while(it1.hasNext()){
 						String field=it1.next().toString();
 						String[] splitField=field.split("~");
 						String fieldFrom=splitField[0];
 						String fieldTo=splitField[1];
 						try{
 							//BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
 							//beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
 							PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
 							try{
 								if(fieldFrom!=null && fieldTo!=null && fieldFrom.trim().equalsIgnoreCase("recRateExchange") && fieldTo.trim().equalsIgnoreCase("exchangeRate"))	{
 		    						BigDecimal payExchangeRateBig=accountLine.getRecRateExchange();  
 		    		            	if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
 		    		            		payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
 		    		            		synchedAccountLine.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
 		    							} 
 		    					}else{
 							         beanUtilsBean.setProperty(synchedAccountLine,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
 		    					}
 							}catch(Exception ex){
 								System.out.println("\n\n\n\n\n error while setting property--->"+ex);
 							}
 							
 						}catch(Exception ex){
 							System.out.println("\n\n\n\n Exception while copying");
 							ex.printStackTrace();
 						}
 						
 					}
 					try{
 			    	    if(!(accountLine.getShipNumber().equals(synchedAccountLine.getShipNumber()))){
 			    	    	synchedAccountLine.setUpdatedBy(accountLine.getCorpID()+":"+getRequest().getRemoteUser());
 			    	    	synchedAccountLine.setUpdatedOn(new Date());
 			    	    }}catch( Exception e){
 			    	    	
 			    	    }
 					try{
 						if(chargeCodeFlag){
 					   ServiceOrder  soObj = serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
 						billingRecods =billingManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
 						 
 							String chargeStr="";
 							List glList = accountLineManager.findChargeDetailFromSO(billingRecods.getContract(),synchedAccountLine.getChargeCode(),synchedAccountLine.getCorpID(),soObj.getJob(),soObj.getRouting(),soObj.getCompanyDivision());
 							if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
 								  chargeStr= glList.get(0).toString();
 							  }
 							   if(!chargeStr.equalsIgnoreCase("")){
 								  String [] chrageDetailArr = chargeStr.split("#");
 								  synchedAccountLine.setRecGl(chrageDetailArr[1]);
 								  synchedAccountLine.setPayGl(chrageDetailArr[2]);
 								}else{
 									synchedAccountLine.setRecGl("");
 									synchedAccountLine.setPayGl("");
 							 }
 						}
 						//}
 						}catch(Exception e){
 							e.printStackTrace();
 						}
 					
 					if(synchedAccountLine.getEstimateRevenueAmount() !=null && synchedAccountLine.getEstimateRevenueAmount().doubleValue()>0){
 						synchedAccountLine.setEstimatePassPercentage(100);
 				        }
 				        if(synchedAccountLine.getRevisionRevenueAmount() !=null && synchedAccountLine.getRevisionRevenueAmount().doubleValue()>0){
 				        	synchedAccountLine.setRevisionPassPercentage(100);
 				        }
 				    
 				       
 				            try{
 				            	BigDecimal payExchangeRateBig=accountLine.getRecRateExchange();  
 				            	if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
 				            		payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
 				            		synchedAccountLine.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
 									}
 				            	//synchedAccountLine.setActualExpense(synchedAccountLine.getLocalAmount().divide(accountLine.getRecRateExchange(),2));	
 				            }catch(Exception e){
 				            	e.printStackTrace();
 				            }
 					
 				            
 				            if(externalBaseCurrency !=null && baseCurrency!=null && (!(baseCurrency.trim().equalsIgnoreCase(externalBaseCurrency.trim())))){
 				            	DecimalFormat decimalFormatExchangeRate = new DecimalFormat("#.####");
 				            	if(EstSellCurrencyFlag){
 								String estExchangeRate="1"; 
 					            
 					            List estRate=exchangeRateManager.findAccExchangeRate(externalCorpID,synchedAccountLine.getEstSellCurrency());
 								 if((estRate!=null)&&(!estRate.isEmpty())&& estRate.get(0)!=null && (!(estRate.get(0).toString().equals(""))))
 								 {
 									 estExchangeRate=estRate.get(0).toString();
 								 
 								 } 
 								estExchangeRateBig=new BigDecimal(estExchangeRate);	
 				            	}
 				            	synchedAccountLine.setEstSellExchangeRate(estExchangeRateBig);
 				            	synchedAccountLine.setEstExchangeRate(estExchangeRateBig);
 				            	if(accountLine.getEstSellLocalRate() !=null && accountLine.getEstSellLocalRate().doubleValue()!=0){
 				            	synchedAccountLine.setEstimateSellRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
 				            	synchedAccountLine.setEstimateRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
 				            	}
 				            	if(accountLine.getEstSellLocalAmount() !=null && accountLine.getEstSellLocalAmount().doubleValue() !=0){
 				            	synchedAccountLine.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
 				            	synchedAccountLine.setEstimateExpense(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
 				            	} 
 				            	
 				            	BigDecimal estcontractExchangeRateBig=new BigDecimal("1");	
 				            	try{ 
 									estcontractExchangeRateBig=(new BigDecimal(decimalFormatExchangeRate.format((synchedAccountLine.getEstimateContractRate().doubleValue())/(synchedAccountLine.getEstimateSellRate().doubleValue())))); 
 								}catch(Exception es){
 									
 								} 
 				            	synchedAccountLine.setEstimateContractExchangeRate(estcontractExchangeRateBig);
 								synchedAccountLine.setEstimatePayableContractExchangeRate(estcontractExchangeRateBig);
 								
 								if(revisionSellCurrencyFlag){
 								String revisionExchangeRate="1"; 
 					             
 					            List revisionRate=exchangeRateManager.findAccExchangeRate(externalCorpID,synchedAccountLine.getRevisionSellCurrency());
 								 if((revisionRate!=null)&&(!revisionRate.isEmpty())&& revisionRate.get(0)!=null && (!(revisionRate.get(0).toString().equals(""))))
 								 {
 									 revisionExchangeRate=revisionRate.get(0).toString();
 								 
 								 } 
 								 revisionExchangeRateBig=new BigDecimal(revisionExchangeRate);	
 								}
 								synchedAccountLine.setRevisionSellExchangeRate(revisionExchangeRateBig);
 								synchedAccountLine.setRevisionExchangeRate(revisionExchangeRateBig);
 								if(accountLine.getRevisionSellLocalRate() !=null && accountLine.getRevisionSellLocalRate().doubleValue()!=0){
 								synchedAccountLine.setRevisionSellRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
 								synchedAccountLine.setRevisionRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
 								}
 								if(accountLine.getRevisionSellLocalAmount() !=null && accountLine.getRevisionSellLocalAmount().doubleValue()!=0){
 								synchedAccountLine.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
 								synchedAccountLine.setRevisionExpense(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
 								} 
 								BigDecimal revisioncontractExchangeRateBig=new BigDecimal("1");	
 								try{ 
 									revisioncontractExchangeRateBig=(new BigDecimal(decimalFormatExchangeRate.format((synchedAccountLine.getRevisionContractRate().doubleValue())/(synchedAccountLine.getRevisionSellRate().doubleValue())))); 
 								}catch(Exception es){
 									
 								}
 								synchedAccountLine.setRevisionContractExchangeRate(revisioncontractExchangeRateBig);
 								synchedAccountLine.setRevisionPayableContractExchangeRate(revisioncontractExchangeRateBig);  
 								if(recRateCurrencyFlag){
 								String recExchangeRate="1";  
 					            List recRate=exchangeRateManager.findAccExchangeRate(externalCorpID,synchedAccountLine.getRecRateCurrency());
 								 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
 								 {
 									 recExchangeRate=recRate.get(0).toString();
 								 
 								 } 
 								recExchangeRateBig=new BigDecimal(recExchangeRate);	
 								}
 								synchedAccountLine.setRecRateExchange(recExchangeRateBig);
 								BigDecimal payExchangeRateBig=synchedAccountLine.getRecRateExchange();
 								if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
 									payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
 									synchedAccountLine.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
 								}
 								if(accountLine.getRecCurrencyRate() !=null && accountLine.getRecCurrencyRate().doubleValue()!=0){
 								synchedAccountLine.setRecRate(new BigDecimal(decimalFormatExchangeRate.format((accountLine.getRecCurrencyRate().doubleValue())/(recExchangeRateBig.doubleValue()))));
 								}
 								if(accountLine.getActualRevenueForeign() !=null && accountLine.getActualRevenueForeign().doubleValue()!=0){
 								synchedAccountLine.setActualRevenue(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue())/(recExchangeRateBig.doubleValue()))));
 								try{
 									synchedAccountLine.setActualExpense(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue())/(recExchangeRateBig.doubleValue()))));
 								}catch(Exception e){
 					    			
 					    		}
 								} 
 								
 								BigDecimal contractExchangeRateBig=new BigDecimal("1");	
 								try{ 
 									contractExchangeRateBig=(new BigDecimal(decimalFormatExchangeRate.format((synchedAccountLine.getContractRate().doubleValue())/(synchedAccountLine.getRecRate().doubleValue())))); 
 								}catch(Exception es){
 									
 								}
 								synchedAccountLine.setContractExchangeRate(contractExchangeRateBig);
 								synchedAccountLine.setPayableContractExchangeRate(contractExchangeRateBig);

 							}        
 					try{
 				    if(!(accountLine.getShipNumber().equals(synchedAccountLine.getShipNumber()))){
 				    	synchedAccountLine.setUpdatedBy(accountLine.getCorpID()+":"+getRequest().getRemoteUser());
 				    	synchedAccountLine.setUpdatedOn(new Date());
 				    }}catch( Exception e){
 				    	e.printStackTrace();
 				    }  
 				    try{
 					    String partnertype="";
 					    if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
 				  		  partnertype=  partnerManager.checkPartnerType(accountLine.getBillToCode());
 				  	    }
 					    if(partnertype !=null && (!(partnertype.toString().trim().equals("")))){
 					    		
 					    }else{
 					    	synchedAccountLine.setStatus(false)	;
 					    }
 					    }catch(Exception e){
 					    	e.printStackTrace();
 					    }
 					if(accountLine.getRecVatDescr()!=null && (!(accountLine.getRecVatDescr().toString().trim().equals("")))){
 					if(UTSIeuVatPercentList.containsKey(sessionCorpID+"_"+accountLine.getRecVatDescr())){		
 					synchedAccountLine.setRecVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
 					}
 					else{
 		    	    	synchedAccountLine.setRecVatDescr("");
 		    	    	synchedAccountLine.setRecVatPercent("0");
 		    	    	synchedAccountLine.setRecVatAmt(new BigDecimal(0));
 		    	    	synchedAccountLine.setEstVatAmt(new BigDecimal(0));
 		    	    	synchedAccountLine.setRevisionVatAmt(new BigDecimal(0));
 						}
 				    if(UTSIpayVatPercentList.containsKey(sessionCorpID+"_"+accountLine.getRecVatDescr())){ 
 					synchedAccountLine.setPayVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
 				    }
 				    else{
 				    	synchedAccountLine.setPayVatDescr("");
 				    	synchedAccountLine.setPayVatPercent("0");
 				    	synchedAccountLine.setPayVatAmt(new BigDecimal(0));
 				    	synchedAccountLine.setEstExpVatAmt(new BigDecimal(0)); 
 				    	synchedAccountLine.setRevisionExpVatAmt(new BigDecimal(0));
 						}
 					}
 				    synchedAccountLine=accountLineManager.save(synchedAccountLine);
 				    serviceOrderRemote=serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
 				    updateExternalAccountLine(serviceOrderRemote,synchedAccountLine.getCorpID());
 				}
 				else{

 					Boolean fXRateOnActualizationDate=false;
 					try{
 					fXRateOnActualizationDate=billingManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId()).getfXRateOnActualizationDate();
 					}catch(Exception e){e.printStackTrace();}
 				boolean chargeCodeFlag=false;
 				if((!(accountLine.getChargeCode().equals(synchedAccountLine.getChargeCode())))){
 			    	   chargeCodeFlag=true;  
 			       }
 				boolean EstSellCurrencyFlag=false;
 				BigDecimal estExchangeRateBig=synchedAccountLine.getEstSellExchangeRate();
 				if((synchedAccountLine.getEstSellCurrency()!=null && accountLine.getEstSellCurrency()!=null && (!(synchedAccountLine.getEstSellCurrency().toString().trim().equalsIgnoreCase(accountLine.getEstSellCurrency().toString().trim())))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getEstSellLocalRate() !=null && synchedAccountLine.getEstSellLocalRate()!=null &&   (accountLine.getEstSellLocalRate().doubleValue() != synchedAccountLine.getEstSellLocalRate().doubleValue()))  || (accountLine.getEstSellValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getEstSellValueDate())))==0) ){
 					EstSellCurrencyFlag=true;
 				}
 				boolean revisionSellCurrencyFlag=false;
 				BigDecimal revisionExchangeRateBig=synchedAccountLine.getRevisionSellExchangeRate();
 				if((synchedAccountLine.getRevisionSellCurrency()!=null && accountLine.getRevisionSellCurrency()!=null && (!(synchedAccountLine.getRevisionSellCurrency().toString().trim().equalsIgnoreCase(accountLine.getRevisionSellCurrency().toString().trim())))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getRevisionSellLocalRate() !=null && synchedAccountLine.getRevisionSellLocalRate()!=null &&   (accountLine.getRevisionSellLocalRate().doubleValue() != synchedAccountLine.getRevisionSellLocalRate().doubleValue())) || (accountLine.getRevisionSellValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getRevisionSellValueDate())))==0)){
 					revisionSellCurrencyFlag=true;
 				}
 				boolean recRateCurrencyFlag=false;
 				BigDecimal recExchangeRateBig=synchedAccountLine.getRecRateExchange();
 				if((synchedAccountLine.getRecRateCurrency()!=null && accountLine.getRecRateCurrency()!=null && (!(synchedAccountLine.getRecRateCurrency().toString().trim().equalsIgnoreCase(accountLine.getRecRateCurrency().toString().trim())))) || (fXRateOnActualizationDate!=null && fXRateOnActualizationDate && accountLine.getRecCurrencyRate() !=null && synchedAccountLine.getRecCurrencyRate()!=null &&   (accountLine.getRecCurrencyRate().doubleValue() != synchedAccountLine.getRecCurrencyRate().doubleValue())) || (accountLine.getRacValueDate() !=null && todayDate.compareTo(sdfDestination.parse(sdfDestination.format(accountLine.getRacValueDate())))==0)){
 					 recRateCurrencyFlag=true;
 				}
 				String externalCorpID = synchedAccountLine.getCorpID();
 				UTSIeuVatPercentList=refMasterManager.findVatPercentList(externalCorpID, "EUVAT");
 				UTSIpayVatPercentList = refMasterManager.findVatPercentList(externalCorpID, "PAYVATDESC");
 				String externalBaseCurrency=accountLineManager.searchBaseCurrency(externalCorpID).get(0).toString();
 				List fieldToSync=customerFileManager.findAccEstimateRevisionFieldToSync("AccountLine","CMM"); 
 		    		Iterator it1=fieldToSync.iterator();
 		    		while(it1.hasNext()){
 		    			String field=it1.next().toString();
 						String[] splitField=field.split("~");
 						String fieldFrom=splitField[0];
 						String fieldTo=splitField[1];
 		    			try{ 
 		    				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
 		    				try{
 		    				beanUtilsBean.setProperty(synchedAccountLine,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
 		    				}catch(Exception ex){
 								System.out.println("\n\n\n\n\n error while setting property--->"+ex);
 							}
 		    				
 		    			}catch(Exception ex){
 		    				System.out.println("\n\n\n\n Exception while copying");
 		    				ex.printStackTrace();
 		    			}
 		    			
 		    		}
 		    		try{
 		        	    if(!(accountLine.getShipNumber().equals(synchedAccountLine.getShipNumber()))){
 		        	    	synchedAccountLine.setUpdatedBy(accountLine.getCorpID()+":"+getRequest().getRemoteUser());
 		        	    	synchedAccountLine.setUpdatedOn(new Date());
 		        	    }}catch( Exception e){
 		        	    	
 		        	    } 
 		    		
 		    		if(synchedAccountLine.getEstimateRevenueAmount() !=null && synchedAccountLine.getEstimateRevenueAmount().doubleValue()>0){
 		    			synchedAccountLine.setEstimatePassPercentage(100);
 			            }
 			            if(synchedAccountLine.getRevisionRevenueAmount() !=null && synchedAccountLine.getRevisionRevenueAmount().doubleValue()>0){
 			            	synchedAccountLine.setRevisionPassPercentage(100);
 			            }  
 				            try{
 				            	BigDecimal payExchangeRateBig=accountLine.getRecRateExchange();  
 				            	if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
 				            		payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
 				            		synchedAccountLine.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
 									} 
 				            }catch(Exception e){
 				            	
 				            }
 				            
 				            if(externalBaseCurrency !=null && baseCurrency!=null && (!(baseCurrency.trim().equalsIgnoreCase(externalBaseCurrency.trim())))){
 				            	DecimalFormat decimalFormatExchangeRate = new DecimalFormat("#.####");
 				            	if(EstSellCurrencyFlag){
 								String estExchangeRate="1"; 
 					            
 					            List estRate=exchangeRateManager.findAccExchangeRate(externalCorpID,synchedAccountLine.getEstSellCurrency());
 								 if((estRate!=null)&&(!estRate.isEmpty())&& estRate.get(0)!=null && (!(estRate.get(0).toString().equals(""))))
 								 {
 									 estExchangeRate=estRate.get(0).toString();
 								 
 								 } 
 								estExchangeRateBig=new BigDecimal(estExchangeRate);	
 				            	}
 				            	synchedAccountLine.setEstSellExchangeRate(estExchangeRateBig);
 				            	synchedAccountLine.setEstExchangeRate(estExchangeRateBig);
 				            	if(accountLine.getEstSellLocalRate() !=null && accountLine.getEstSellLocalRate().doubleValue()!=0){
 				            	synchedAccountLine.setEstimateSellRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
 				            	synchedAccountLine.setEstimateRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
 				            	}
 				            	if(accountLine.getEstSellLocalAmount() !=null && accountLine.getEstSellLocalAmount().doubleValue() !=0){
 				            	synchedAccountLine.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
 				            	synchedAccountLine.setEstimateExpense(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
 				            	} 
 				            	
 				            	BigDecimal estcontractExchangeRateBig=new BigDecimal("1");	
 				            	try{ 
 									estcontractExchangeRateBig=(new BigDecimal(decimalFormatExchangeRate.format((synchedAccountLine.getEstimateContractRate().doubleValue())/(synchedAccountLine.getEstimateSellRate().doubleValue())))); 
 								}catch(Exception es){
 									
 								} 
 				            	synchedAccountLine.setEstimateContractExchangeRate(estcontractExchangeRateBig);
 								synchedAccountLine.setEstimatePayableContractExchangeRate(estcontractExchangeRateBig);
 								
 								if(revisionSellCurrencyFlag){
 								String revisionExchangeRate="1"; 
 					             
 					            List revisionRate=exchangeRateManager.findAccExchangeRate(externalCorpID,synchedAccountLine.getRevisionSellCurrency());
 								 if((revisionRate!=null)&&(!revisionRate.isEmpty())&& revisionRate.get(0)!=null && (!(revisionRate.get(0).toString().equals(""))))
 								 {
 									 revisionExchangeRate=revisionRate.get(0).toString();
 								 
 								 } 
 								 revisionExchangeRateBig=new BigDecimal(revisionExchangeRate);	
 								}
 								synchedAccountLine.setRevisionSellExchangeRate(revisionExchangeRateBig);
 								synchedAccountLine.setRevisionExchangeRate(revisionExchangeRateBig);
 								if(accountLine.getRevisionSellLocalRate() !=null && accountLine.getRevisionSellLocalRate().doubleValue()!=0){
 								synchedAccountLine.setRevisionSellRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
 								synchedAccountLine.setRevisionRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
 								}
 								if(accountLine.getRevisionSellLocalAmount() !=null && accountLine.getRevisionSellLocalAmount().doubleValue()!=0){
 								synchedAccountLine.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
 								synchedAccountLine.setRevisionExpense(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
 								} 
 								BigDecimal revisioncontractExchangeRateBig=new BigDecimal("1");	
 								try{ 
 									revisioncontractExchangeRateBig=(new BigDecimal(decimalFormatExchangeRate.format((synchedAccountLine.getRevisionContractRate().doubleValue())/(synchedAccountLine.getRevisionSellRate().doubleValue())))); 
 								}catch(Exception es){
 									
 								}
 								synchedAccountLine.setRevisionContractExchangeRate(revisioncontractExchangeRateBig);
 								synchedAccountLine.setRevisionPayableContractExchangeRate(revisioncontractExchangeRateBig);  

 							}
 		    		
 		    		try{
 		    	    if(!(accountLine.getShipNumber().equals(synchedAccountLine.getShipNumber()))){
 		    	    	synchedAccountLine.setUpdatedBy(accountLine.getCorpID()+":"+getRequest().getRemoteUser());
 		    	    	synchedAccountLine.setUpdatedOn(new Date());
 		    	    }}catch( Exception e){
 		    	    	
 		    	    } 
 		    	    try{
 		    	    String partnertype="";
 		    	    if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
 		      		  partnertype=  partnerManager.checkPartnerType(accountLine.getBillToCode());
 		      	    }
 		    	    if(partnertype !=null && (!(partnertype.toString().trim().equals("")))){
 		    	    		
 		    	    }else{
 		    	    	synchedAccountLine.setStatus(false)	;
 		    	    }
 		    	    }catch(Exception e){
 		    	    	
 		    	    }
 		    	    /*if(accountLine.getRecVatDescr()!=null && (!(accountLine.getRecVatDescr().toString().trim().equals("")))){
 		    	    if(UTSIeuVatPercentList.containsKey(sessionCorpID+"_"+accountLine.getRecVatDescr())){	
 		    	    	synchedAccountLine.setRecVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
 		    	    }
 		    	    else{
 		    	    	synchedAccountLine.setRecVatDescr("");
 		    	    	synchedAccountLine.setRecVatPercent("0");
 		    	    	synchedAccountLine.setRecVatAmt(new BigDecimal(0));
 		    	    	synchedAccountLine.setEstVatAmt(new BigDecimal(0));
 		    	    	synchedAccountLine.setRevisionVatAmt(new BigDecimal(0));
 						}
 				    if(UTSIpayVatPercentList.containsKey(sessionCorpID+"_"+accountLine.getRecVatDescr())){ 
 		    	    synchedAccountLine.setPayVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
 				    }
 				    else{
 				    	synchedAccountLine.setPayVatDescr("");
 				    	synchedAccountLine.setPayVatPercent("0");
 				    	synchedAccountLine.setPayVatAmt(new BigDecimal(0));
 				    	synchedAccountLine.setEstExpVatAmt(new BigDecimal(0)); 
 				    	synchedAccountLine.setRevisionExpVatAmt(new BigDecimal(0));
 						}
 		    	    }*/
 		    	    synchedAccountLine=accountLineManager.save(synchedAccountLine);
 		    	    serviceOrderToRecods=serviceOrderManager.getForOtherCorpid(synchedAccountLine.getServiceOrderId());
 		    	    updateExternalAccountLine(serviceOrderToRecods,synchedAccountLine.getCorpID());
 				
 				}
 					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
 			} catch (Exception e) {
 				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
 		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
 		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
 		    	 e.printStackTrace();
 			}
 	    	    }
 		@SkipValidation
 		private void createDMMAccountLine(List<Object> records, ServiceOrder  serviceOrder,AccountLine accountLine,Billing billing){

 			try {
 				Iterator  it=records.iterator();
 				while(it.hasNext()){
 					  serviceOrderToRecods=(ServiceOrder)it.next();
 					try{
 					trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
 					 billingRecods=billingManager.getForOtherCorpid(serviceOrderToRecods.getId()); 
 					if(trackingStatusToRecods.getSoNetworkGroup() && !trackingStatusToRecods.getAccNetworkGroup() && (!(serviceOrder.getShipNumber().toString().equalsIgnoreCase(serviceOrderToRecods.getShipNumber().toString())))){
 						trackingStatus=trackingStatusManager.get(serviceOrder.getId()); 
 						String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getNetworkPartnerCode()); 
 						UTSIpayVatList=	refMasterManager.findByParameterUTSIPayVat (sessionCorpID, "PAYVATDESC",externalCorpID);
 						UTSIpayVatPercentList = refMasterManager.findVatPercentList(externalCorpID, "PAYVATDESC");
 						UTSIeuVatPercentList=refMasterManager.findVatPercentList(externalCorpID, "EUVAT");
 						List fieldToSync=customerFileManager.findAccFieldToSyncCreate("AccountLine","DMM");
 						AccountLine accountLineNew= new AccountLine();
 						try{
 							Iterator it1=fieldToSync.iterator();
 							while(it1.hasNext()){
 							String field=it1.next().toString();
 							String[] splitField=field.split("~");
 							String fieldFrom=splitField[0];
 							String fieldTo=splitField[1];
 							PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
 							try{
 							beanUtilsBean.setProperty(accountLineNew,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
 							}catch(Exception ex){
 								logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
 							}
 							
 							}
 						}catch(Exception e){
 							
 						}
 						try{
 							if(accountLine.getRecVatDescr()!=null && (!(accountLine.getRecVatDescr().toString().trim().equals("")))){ 
 	 					    	
 	 					    	String companyDivisionUTSI=accountLineManager.getUTSICompanyDivision(externalCorpID,accountLine.getBillToCode()) ;
 	 					    	accountLineNew.setPayVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
 	 					        String payVatDescr="";
 	 					    	if(UTSIpayVatList.containsKey(accountLineNew.getPayVatDescr()+"~"+companyDivisionUTSI)){
 	 					    		payVatDescr=UTSIpayVatList.get(accountLineNew.getPayVatDescr()+"~"+companyDivisionUTSI);
 	 					    		accountLineNew.setPayVatDescr(payVatDescr);
 	 					    		String payVatPercent="0";
 	 					    		if(UTSIpayVatPercentList.containsKey(payVatDescr)){ 
 	 								payVatPercent=UTSIpayVatPercentList.get(payVatDescr);
 	 					    		}
 	 								accountLineNew.setPayVatPercent(payVatPercent);
 	 					    	}else{
 	 					    		if(UTSIpayVatList.containsKey(accountLineNew.getPayVatDescr()+"~NO")){
 	 						    		payVatDescr=UTSIpayVatList.get(accountLineNew.getPayVatDescr()+"~NO");
 	 						    		accountLineNew.setPayVatDescr(payVatDescr);
 	 						    		String payVatPercent="0";
 	 						    		if(UTSIpayVatPercentList.containsKey(payVatDescr)){ 
 	 				    				payVatPercent=UTSIpayVatPercentList.get(payVatDescr);
 	 						    		}
 	 				    				accountLineNew.setPayVatPercent(payVatPercent);
 	 						    	}else{
 	 						    		accountLineNew.setPayVatDescr("");
 	 						    		accountLineNew.setPayVatPercent("0");
 	 						    	}
 	 					    	}
 	 					    	}
 						}catch(Exception e){
 							
 						}
 						  /*
 					     * Added new code for setters some field value as mentioned in Bug# 6124
 					     * DMM Payable Mapping reported by : Neha Singh
 					     * Code by Gautam Verma
 					     */
 					    accountLineNew.setPayableContractCurrency(accountLine.getRecRateCurrency());
 					    accountLineNew.setCountry(accountLine.getRecRateCurrency());
 					    accountLineNew.setEstimatePayableContractCurrency(accountLine.getEstSellCurrency());
 					    accountLineNew.setEstCurrency(accountLine.getEstSellCurrency());
 					    accountLineNew.setRevisionPayableContractCurrency(accountLine.getRevisionSellCurrency());
 					    accountLineNew.setRevisionCurrency(accountLine.getRevisionSellCurrency());
 					    String billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLineNew.getBillToCode(),externalCorpID);
 						if(!billingCurrency.equalsIgnoreCase(""))
 						{
 							accountLineNew.setRecRateCurrency(billingCurrency);
 							accountLineNew.setEstSellCurrency(billingCurrency);
 							accountLineNew.setRevisionSellCurrency(billingCurrency);
 						}else{ 
 							String baseCurrencyExternalCorpID="";
 							String baseCurrencyCompanyDivisionExternalCorpID=""; 
 							baseCurrencyExternalCorpID=accountLineManager.searchBaseCurrency(externalCorpID).get(0).toString();
 							baseCurrencyCompanyDivisionExternalCorpID=	 companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrderToRecods.getCompanyDivision(),externalCorpID);
 							if(baseCurrencyCompanyDivisionExternalCorpID==null ||baseCurrencyCompanyDivisionExternalCorpID.equals(""))
 							{
 								accountLineNew.setRecRateCurrency(baseCurrencyExternalCorpID);
 								accountLineNew.setEstSellCurrency(baseCurrencyExternalCorpID);
 								accountLineNew.setRevisionSellCurrency(baseCurrencyExternalCorpID);
 							}else{
 								accountLineNew.setRecRateCurrency(baseCurrencyCompanyDivisionExternalCorpID);
 								accountLineNew.setEstSellCurrency(baseCurrencyCompanyDivisionExternalCorpID);
 								accountLineNew.setRevisionSellCurrency(baseCurrencyCompanyDivisionExternalCorpID);
 							}
 						}
 					    
 				        accountLineNew.setNetworkSynchedId(accountLine.getId());
 				        
 				        try{
 				        if(accountLineNew.getBillToCode()!=null && (!(accountLineNew.getBillToCode().toString().equals(""))) && billingRecods.getBillToCode()!=null && (!(billingRecods.getBillToCode().toString().equals(""))) && accountLineNew.getBillToCode().toString().equals(billingRecods.getBillToCode().toString()) )	{
 				         if(!accountLineNew.getVATExclude()){ 
 				        	accountLineNew.setRecVatDescr(billingRecods.getPrimaryVatCode());
 				    		if(billingRecods.getPrimaryVatCode()!=null && (!(billingRecods.getPrimaryVatCode().toString().equals("")))){
 					    		String recVatPercent="0";
 					    		if(UTSIeuVatPercentList.containsKey(billingRecods.getPrimaryVatCode())){
 						    		recVatPercent=UTSIeuVatPercentList.get(billingRecods.getPrimaryVatCode());
 						    		}
 					    		accountLineNew.setRecVatPercent(recVatPercent);
 					    		}
 				         }	
 				        }
 				        }catch(Exception e){
 				        	
 				        }
 				        //}
 				        accountLineNew.setAccountLineNumber(accountLine.getAccountLineNumber()); 
 				        accountLineNew.setCompanyDivision(serviceOrderToRecods.getCompanyDivision());
 				        List bookingAgentCodeList=companyDivisionManager.getBookingAgentCode(accountLine.getCompanyDivision(),sessionCorpID);
 				        if(bookingAgentCodeList!=null && (!(bookingAgentCodeList.isEmpty())) && bookingAgentCodeList.get(0)!=null && (!(bookingAgentCodeList.get(0).toString().trim().equals(""))))
 				        {
 				        	accountLineNew.setVendorCode(bookingAgentCodeList.get(0).toString());
 				        	List bookingAgentNamelist=	serviceOrderManager.findByBooking(bookingAgentCodeList.get(0).toString(), sessionCorpID);
 				            
 				        	if(bookingAgentNamelist!=null && (!(bookingAgentNamelist.isEmpty())) && bookingAgentNamelist.get(0)!=null && (!(bookingAgentNamelist.get(0).toString().trim().equals("")))){
 				            accountLineNew.setEstimateVendorName(bookingAgentNamelist.get(0).toString());
 				            }
 				        	
 				        }
 				         String actCode="";
 				         String  companyDivisionAcctgCodeUnique1="";
 						 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(externalCorpID);
 						 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
 							 companyDivisionAcctgCodeUnique1 =companyDivisionAcctgCodeUniqueList.get(0).toString();
 						 }
 				        if(companyDivisionAcctgCodeUnique1.equalsIgnoreCase("Y")){
 							actCode=partnerManager.getAccountCrossReference(accountLineNew.getVendorCode(),serviceOrderToRecods.getCompanyDivision(),externalCorpID);
 						}else{
 							actCode=partnerManager.getAccountCrossReferenceUnique(accountLineNew.getVendorCode(),externalCorpID);
 						}
 				        accountLineNew.setActgCode(actCode);
 				        Boolean extCostElement =  accountLineManager.getExternalCostElement(externalCorpID);
 				        if(!extCostElement){
 				        List  agentGLList=accountLineManager.getGLList(accountLineNew.getChargeCode(),billingRecods.getContract(),externalCorpID);
 						if(agentGLList!=null && (!(agentGLList.isEmpty())) && agentGLList.get(0)!=null){
 							try{
 							String[] GLarrayData=agentGLList.get(0).toString().split("#"); 
 							String recGl=GLarrayData[0];
 							String payGl=GLarrayData[1];
 							if(!(recGl.equalsIgnoreCase("NO"))){
 								accountLineNew.setRecGl(recGl);
 							}
 							if(!(payGl.equalsIgnoreCase("NO"))){
 								accountLineNew.setPayGl(payGl);
 							}
 							String VATExclude=GLarrayData[2];
 							if(VATExclude.equalsIgnoreCase("Y")){
 								accountLineNew.setVATExclude(true);
 							}else{
 								accountLineNew.setVATExclude(false);
 							}
 						}catch(Exception e){
 							
 						}
 						}
 					}else{
 						String chargeStr="";
 						List glList = accountLineManager.findChargeDetailFromSO(billingRecods.getContract(),accountLineNew.getChargeCode(),externalCorpID,serviceOrderToRecods.getJob(),serviceOrderToRecods.getRouting(),serviceOrderToRecods.getCompanyDivision());
 						if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
 							  chargeStr= glList.get(0).toString();
 						  }
 						   if(!chargeStr.equalsIgnoreCase("")){
 							  String [] chrageDetailArr = chargeStr.split("#");
 							  accountLineNew.setRecGl(chrageDetailArr[1]);
 							  accountLineNew.setPayGl(chrageDetailArr[2]);
 							}else{
 								accountLineNew.setRecGl("");
 								accountLineNew.setPayGl("");
 						  }
 					}
 				        accountLineNew.setId(null);
 				        accountLineNew.setCorpID(externalCorpID);
 				        accountLineNew.setCreatedBy("Networking");
 				        accountLineNew.setUpdatedBy("Networking");
 				        accountLineNew.setCreatedOn(new Date());
 				        accountLineNew.setUpdatedOn(new Date()); 
 				      
 				        
 				        
 				        String estimatePayableContractExchangeRate="1"; 
 				        BigDecimal estimatePayableContractExchangeRateBig=new BigDecimal("1.0000");
 				        List estimatePayableContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstimatePayableContractCurrency ());
 						 if((estimatePayableContractRate!=null)&&(!estimatePayableContractRate.isEmpty())&& estimatePayableContractRate.get(0)!=null && (!(estimatePayableContractRate.get(0).toString().equals(""))))
 						 {
 							 estimatePayableContractExchangeRate=estimatePayableContractRate.get(0).toString();
 						 
 						 }
 						 estimatePayableContractExchangeRateBig=new BigDecimal(estimatePayableContractExchangeRate);	
 						 accountLineNew.setEstimatePayableContractExchangeRate(estimatePayableContractExchangeRateBig);
 						 accountLineNew.setEstimatePayableContractValueDate(new Date());
 						 accountLineNew.setEstExchangeRate(estimatePayableContractExchangeRateBig);
 						 accountLineNew.setEstValueDate(new Date());
 						 if(accountLine.getEstimateSellDeviation()!=null && accountLine.getEstSellLocalAmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLine.getEstSellLocalAmount().doubleValue()>0){
 						  accountLineNew.setEstimatePayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
 						 }else{
 						 accountLineNew.setEstimatePayableContractRateAmmount(accountLine.getEstSellLocalAmount());
 						 }
 						 accountLineNew.setEstimatePayableContractRate(accountLine.getEstSellLocalRate());
 						 if(accountLine.getEstimateSellDeviation()!=null && accountLine.getEstSellLocalAmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLine.getEstSellLocalAmount().doubleValue()>0){
 							 accountLineNew.setEstLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
 						 }else{
 						 accountLineNew.setEstLocalAmount(accountLine.getEstSellLocalAmount());
 						 }
 						 accountLineNew.setEstLocalRate(accountLine.getEstSellLocalRate());
 						 accountLineNew.setEstimateRate(new BigDecimal(decimalFormat.format(accountLine.getEstSellLocalRate().doubleValue()/accountLineNew.getEstimatePayableContractExchangeRate().doubleValue())));
 						 accountLineNew.setEstimateExpense(new BigDecimal(decimalFormat.format(accountLineNew.getEstimatePayableContractRateAmmount().doubleValue()/accountLineNew.getEstimatePayableContractExchangeRate().doubleValue())));
 						
 						 
 						 
 						 String estimateContractExchangeRate="1"; 
 				        BigDecimal estimateContractExchangeRateBig=new BigDecimal("1.0000");
 				        List estimateContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstimateContractCurrency ());
 						 if((estimateContractRate!=null)&&(!estimateContractRate.isEmpty())&& estimateContractRate.get(0)!=null && (!(estimateContractRate.get(0).toString().equals(""))))
 						 {
 							 estimateContractExchangeRate=estimateContractRate.get(0).toString();
 						 
 						 } 
 						 estimateContractExchangeRateBig=new BigDecimal(estimateContractExchangeRate);	
 						 accountLineNew.setEstimateContractExchangeRate(estimateContractExchangeRateBig);
 						 accountLineNew.setEstimateContractValueDate(new Date()); 
 						 accountLineNew.setEstimateSellRate(new BigDecimal(decimalFormat.format(accountLineNew.getEstimateContractRate().doubleValue()/accountLineNew.getEstimateContractExchangeRate().doubleValue())));
 						 if(accountLine.getEstimateSellDeviation()!=null && accountLineNew.getEstimateContractRateAmmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLineNew.getEstimateContractRateAmmount().doubleValue()>0){
 							  accountLineNew.setEstimateContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateContractRateAmmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
 							 }else{
 								 
 							 }
 						 accountLineNew.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format(accountLineNew.getEstimateContractRateAmmount().doubleValue()/ accountLineNew.getEstimateContractExchangeRate().doubleValue())));
 						 
 						 
 						String estSellExchangeRate="1"; 
 				        BigDecimal estSellExchangeRateBig=new BigDecimal("1.0000");
 				        List estSellRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstSellCurrency());
 						 if((estSellRate!=null)&&(!estSellRate.isEmpty())&& estSellRate.get(0)!=null && (!(estSellRate.get(0).toString().equals(""))))
 						 {
 							 estSellExchangeRate=estSellRate.get(0).toString();
 						 }
 						 estSellExchangeRateBig=new BigDecimal(estSellExchangeRate);
 						accountLineNew.setEstSellExchangeRate(estSellExchangeRateBig);
 						accountLineNew.setEstSellValueDate(new Date());
 						
 						accountLineNew.setEstSellLocalRate(accountLineNew.getEstimateSellRate().multiply(estSellExchangeRateBig));
 						accountLineNew.setEstSellLocalAmount(accountLineNew.getEstimateRevenueAmount().multiply(estSellExchangeRateBig));
 						 
 						 
 						
 						
 						String revisionPayableContractExchangeRate="1"; 
 				        BigDecimal revisionPayableContractExchangeRateBig=new BigDecimal("1.0000");
 				        List revisionPayableContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionPayableContractCurrency ());
 						 if((revisionPayableContractRate!=null)&&(!revisionPayableContractRate.isEmpty())&& revisionPayableContractRate.get(0)!=null && (!(revisionPayableContractRate.get(0).toString().equals(""))))
 						 {
 							 revisionPayableContractExchangeRate=revisionPayableContractRate.get(0).toString();
 						 
 						 }
 						 revisionPayableContractExchangeRateBig=new BigDecimal(revisionPayableContractExchangeRate);	
 						 accountLineNew.setRevisionPayableContractExchangeRate(revisionPayableContractExchangeRateBig);
 						 accountLineNew.setRevisionPayableContractValueDate(new Date());
 						 accountLineNew.setRevisionExchangeRate(revisionPayableContractExchangeRateBig);
 						 accountLineNew.setRevisionValueDate(new Date());
 						 if(accountLine.getRevisionSellDeviation()!=null && accountLine.getRevisionSellLocalAmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLine.getRevisionSellLocalAmount().doubleValue()>0){
 						  accountLineNew.setRevisionPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue()))	 );	 
 						 }else{
 						 accountLineNew.setRevisionPayableContractRateAmmount(accountLine.getRevisionSellLocalAmount());
 						 }
 						 accountLineNew.setRevisionPayableContractRate(accountLine.getRevisionSellLocalRate());
 						 if(accountLine.getRevisionSellDeviation()!=null && accountLine.getRevisionSellLocalAmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLine.getRevisionSellLocalAmount().doubleValue()>0){
 							 accountLineNew.setRevisionLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue()))	 );	 
 						 }else{
 						 accountLineNew.setRevisionLocalAmount(accountLine.getRevisionSellLocalAmount());
 						 }
 						 accountLineNew.setRevisionLocalRate(accountLine.getRevisionSellLocalRate());
 						 accountLineNew.setRevisionRate(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionPayableContractRate().doubleValue()/ accountLineNew.getRevisionPayableContractExchangeRate().doubleValue())));
 						 accountLineNew.setRevisionExpense(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionPayableContractRateAmmount().doubleValue()/ accountLineNew.getRevisionPayableContractExchangeRate().doubleValue())));
 						
 						String revisionContractExchangeRate="1"; 
 				        BigDecimal revisionContractExchangeRateBig=new BigDecimal("1.0000");
 				        List revisionContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionContractCurrency ());
 						 if((revisionContractRate!=null)&&(!revisionContractRate.isEmpty())&& revisionContractRate.get(0)!=null && (!(revisionContractRate.get(0).toString().equals(""))))
 						 {
 							 revisionContractExchangeRate=revisionContractRate.get(0).toString();
 						 
 						 } 
 						 revisionContractExchangeRateBig=new BigDecimal(revisionContractExchangeRate);	
 						 accountLineNew.setRevisionContractExchangeRate(revisionContractExchangeRateBig);
 						 accountLineNew.setRevisionContractValueDate(new Date());
 						 accountLineNew.setRevisionSellRate(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionContractRate().doubleValue()/  accountLineNew.getRevisionContractExchangeRate().doubleValue())));
 						 if(accountLine.getRevisionSellDeviation()!=null && accountLineNew.getRevisionContractRateAmmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLineNew.getRevisionContractRateAmmount().doubleValue()>0){
 							  accountLineNew.setRevisionContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionContractRateAmmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue())));	 
 							 }else{
 								 
 							 }
 						 accountLineNew.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionContractRateAmmount().doubleValue()/  accountLineNew.getRevisionContractExchangeRate().doubleValue())));
 						 
 						 
 						String revisionSellExchangeRate="1"; 
 				        BigDecimal revisionSellExchangeRateBig=new BigDecimal("1.0000");
 				        List revisionSellRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionSellCurrency());
 						 if((revisionSellRate!=null)&&(!revisionSellRate.isEmpty())&& revisionSellRate.get(0)!=null && (!(revisionSellRate.get(0).toString().equals(""))))
 						 {
 							 revisionSellExchangeRate=estSellRate.get(0).toString();
 						 }
 						 revisionSellExchangeRateBig=new BigDecimal(revisionSellExchangeRate);
 						 accountLineNew.setRevisionSellExchangeRate(revisionSellExchangeRateBig);
 						 accountLineNew.setRevisionSellValueDate(new Date());
 						 
 						 accountLineNew.setRevisionSellLocalRate(accountLineNew.getRevisionSellRate().multiply(revisionSellExchangeRateBig));
 						 accountLineNew.setRevisionSellLocalAmount(accountLineNew.getRevisionRevenueAmount().multiply(revisionSellExchangeRateBig));
 						
 						 
 						  
 						String contractPayableExchangeRate="1"; 
 				        BigDecimal contractPayableExchangeRateBig=new BigDecimal("1.0000");
 				        List contractPayableRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getPayableContractCurrency ());
 						if((contractPayableRate!=null)&&(!contractPayableRate.isEmpty())&& contractPayableRate.get(0)!=null && (!(contractPayableRate.get(0).toString().equals(""))))
 							 {
 							contractPayableExchangeRate=contractPayableRate.get(0).toString();
 							 
 							 } 
 						contractPayableExchangeRateBig=new BigDecimal(contractPayableExchangeRate);
 						accountLineNew.setPayableContractExchangeRate(contractPayableExchangeRateBig);
 						accountLineNew.setPayableContractValueDate(new Date()); 
 						accountLineNew.setValueDate(new Date());
 					    double payExchangeRate=0; 
 				        //BigDecimal payExchangeRateBig=new BigDecimal("1.0000");  
 						payExchangeRate=Double.parseDouble(contractPayableExchangeRate);
 						accountLineNew.setExchangeRate(payExchangeRate);
 						if(accountLine.getReceivableSellDeviation()!=null && accountLine.getActualRevenueForeign()!=null &&  accountLine.getReceivableSellDeviation().doubleValue()>0 && accountLine.getActualRevenueForeign().doubleValue()>0){
 							accountLineNew.setPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));
 							accountLineNew.setLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));	
 						}else{
 						accountLineNew.setPayableContractRateAmmount(accountLine.getActualRevenueForeign());
 						accountLineNew.setLocalAmount(accountLine.getActualRevenueForeign());
 						}
 						try{
 							accountLineNew.setActualExpense(new BigDecimal(decimalFormat.format(accountLineNew.getPayableContractRateAmmount().doubleValue()/accountLineNew.getPayableContractExchangeRate().doubleValue())));
 						}catch(Exception e){
 				    			
 				    	}
 							//payExchangeRateBig=new BigDecimal(payExchangeRate);
 						 
 						 
 					    String contractExchangeRate="1"; 
 				        BigDecimal contractExchangeRateBig=new BigDecimal("1.0000");
 				        List contractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getContractCurrency ());
 						 if((contractRate!=null)&&(!contractRate.isEmpty())&& contractRate.get(0)!=null && (!(contractRate.get(0).toString().equals(""))))
 						 {
 							 contractExchangeRate=contractRate.get(0).toString();
 						 
 						 } 
 						contractExchangeRateBig=new BigDecimal(contractExchangeRate);	
 						accountLineNew.setContractExchangeRate(contractExchangeRateBig);
 						accountLineNew.setContractValueDate(new Date()); 
 						accountLineNew.setRecRate(new BigDecimal(decimalFormat.format(accountLineNew.getContractRate().doubleValue()/accountLineNew.getContractExchangeRate().doubleValue())));
 						 if(accountLine.getReceivableSellDeviation()!=null && accountLineNew.getContractRateAmmount()!=null &&  accountLine.getReceivableSellDeviation().doubleValue()>0 && accountLineNew.getContractRateAmmount().doubleValue()>0){
 							  accountLineNew.setContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getContractRateAmmount().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));	 
 							 }else{
 								 
 							 }
 						accountLineNew.setActualRevenue(new BigDecimal(decimalFormat.format(accountLineNew.getContractRateAmmount().doubleValue()/accountLineNew.getContractExchangeRate().doubleValue())));
 						accountLineNew.setRacValueDate(new Date());
 						String recExchangeRate="1"; 
 				        BigDecimal recExchangeRateBig=new BigDecimal("1.0000");
 				        List recRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRecRateCurrency());
 						 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
 						 {
 							 recExchangeRate=recRate.get(0).toString();
 						 }
 						recExchangeRateBig=new BigDecimal(recExchangeRate);
 						accountLineNew.setRecRateExchange(recExchangeRateBig);
 						accountLineNew.setRacValueDate(new Date());
 						accountLineNew.setRecCurrencyRate(accountLineNew.getRecRate().multiply(recExchangeRateBig));
 						accountLineNew.setActualRevenueForeign(accountLineNew.getActualRevenue().multiply(recExchangeRateBig));
 						try{
 							if(accountLineNew.getEstimateSellDeviation()!=null && accountLineNew.getEstimateSellDeviation().doubleValue()>0 ){
 								if(accountLineNew.getEstimateRevenueAmount()!=null && accountLineNew.getEstimateRevenueAmount().doubleValue()>0)
 									accountLineNew.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateRevenueAmount().doubleValue())*((accountLineNew.getEstimateSellDeviation().doubleValue())/100))));
 								if(accountLineNew.getEstSellLocalAmount()!=null && accountLineNew.getEstSellLocalAmount().doubleValue()>0)
 									accountLineNew.setEstSellLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstSellLocalAmount().doubleValue())*((accountLineNew.getEstimateSellDeviation().doubleValue())/100))));
 								if(accountLineNew.getEstimateContractRateAmmount()!=null && accountLineNew.getEstimateContractRateAmmount().doubleValue()>0)
 									accountLineNew.setEstimateContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateContractRateAmmount().doubleValue())*((accountLineNew.getEstimateSellDeviation().doubleValue())/100))));	
 							}
 							if(accountLineNew.getEstimateDeviation()!=null && accountLineNew.getEstimateDeviation().doubleValue()>0 ){
 								if(accountLineNew.getEstimateExpense()!=null && accountLineNew.getEstimateExpense().doubleValue()>0)
 									accountLineNew.setEstimateExpense(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateExpense().doubleValue())*((accountLineNew.getEstimateDeviation().doubleValue())/100))));
 								if(accountLineNew.getEstLocalAmount()!=null && accountLineNew.getEstLocalAmount().doubleValue()>0)
 									accountLineNew.setEstLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstLocalAmount().doubleValue())*((accountLineNew.getEstimateDeviation().doubleValue())/100))));
 								if(accountLineNew.getEstimatePayableContractRateAmmount()!=null && accountLineNew.getEstimatePayableContractRateAmmount().doubleValue()>0)
 									accountLineNew.setEstimatePayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimatePayableContractRateAmmount().doubleValue())*((accountLineNew.getEstimateDeviation().doubleValue())/100))));	
 							}
 							if(accountLineNew.getRevisionSellDeviation()!=null && accountLineNew.getRevisionSellDeviation().doubleValue()>0 ){
 								if(accountLineNew.getRevisionRevenueAmount()!=null && accountLineNew.getRevisionRevenueAmount().doubleValue()>0)
 									accountLineNew.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionRevenueAmount().doubleValue())*((accountLineNew.getRevisionSellDeviation().doubleValue())/100))));
 								if(accountLineNew.getRevisionSellLocalAmount()!=null && accountLineNew.getRevisionSellLocalAmount().doubleValue()>0)
 									accountLineNew.setRevisionSellLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionSellLocalAmount().doubleValue())*((accountLineNew.getRevisionSellDeviation().doubleValue())/100))));
 								if(accountLineNew.getRevisionContractRateAmmount()!=null && accountLineNew.getRevisionContractRateAmmount().doubleValue()>0)
 									accountLineNew.setRevisionContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionContractRateAmmount().doubleValue())*((accountLineNew.getRevisionSellDeviation().doubleValue())/100))));	
 							}
 							if(accountLineNew.getRevisionDeviation()!=null && accountLineNew.getRevisionDeviation().doubleValue()>0 ){
 								if(accountLineNew.getRevisionExpense()!=null && accountLineNew.getRevisionExpense().doubleValue()>0)
 									accountLineNew.setRevisionExpense(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionExpense().doubleValue())*((accountLineNew.getRevisionDeviation().doubleValue())/100))));
 								if(accountLineNew.getRevisionLocalAmount()!=null && accountLineNew.getRevisionLocalAmount().doubleValue()>0)
 									accountLineNew.setRevisionLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionLocalAmount().doubleValue())*((accountLineNew.getRevisionDeviation().doubleValue())/100))));
 								if(accountLineNew.getRevisionPayableContractRateAmmount()!=null && accountLineNew.getRevisionPayableContractRateAmmount().doubleValue()>0)
 									accountLineNew.setRevisionPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionPayableContractRateAmmount().doubleValue())*((accountLineNew.getRevisionDeviation().doubleValue())/100))));	
 							}
 							if(accountLineNew.getReceivableSellDeviation()!=null && accountLineNew.getReceivableSellDeviation().doubleValue()>0 ){
 								if(accountLineNew.getActualRevenue()!=null && accountLineNew.getActualRevenue().doubleValue()>0)
 									accountLineNew.setActualRevenue(new BigDecimal(decimalFormat.format((accountLineNew.getActualRevenue().doubleValue())*((accountLineNew.getReceivableSellDeviation().doubleValue())/100))));
 								if(accountLineNew.getActualRevenueForeign()!=null && accountLineNew.getActualRevenueForeign().doubleValue()>0)
 									accountLineNew.setActualRevenueForeign(new BigDecimal(decimalFormat.format((accountLineNew.getActualRevenueForeign().doubleValue())*((accountLineNew.getReceivableSellDeviation().doubleValue())/100))));
 								if(accountLineNew.getContractRateAmmount()!=null && accountLineNew.getContractRateAmmount().doubleValue()>0)
 									accountLineNew.setContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getContractRateAmmount().doubleValue())*((accountLineNew.getReceivableSellDeviation().doubleValue())/100))));	
 							}
 							if(accountLineNew.getPayDeviation()!=null && accountLineNew.getPayDeviation().doubleValue()>0 ){
 								if(accountLineNew.getActualExpense()!=null && accountLineNew.getActualExpense().doubleValue()>0)
 									accountLineNew.setActualExpense(new BigDecimal(decimalFormat.format((accountLineNew.getActualExpense().doubleValue())*((accountLineNew.getPayDeviation().doubleValue())/100))));
 								if(accountLineNew.getLocalAmount()!=null && accountLineNew.getLocalAmount().doubleValue()>0)
 									accountLineNew.setLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getLocalAmount().doubleValue())*((accountLineNew.getPayDeviation().doubleValue())/100))));
 								if(accountLineNew.getPayableContractRateAmmount()!=null && accountLineNew.getPayableContractRateAmmount().doubleValue()>0)
 									accountLineNew.setPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getPayableContractRateAmmount().doubleValue())*((accountLineNew.getPayDeviation().doubleValue())/100))));	
 							} 
 						}catch(Exception e){
 							
 						}	
 						
 						
 						  try{
 					    		if(accountLineNew.getEstimateRevenueAmount() !=null && accountLineNew.getEstimateRevenueAmount().doubleValue()>0 && accountLineNew.getEstimateExpense()!=null && accountLineNew.getEstimateExpense().doubleValue()>0){
 					    			Double estimatePassPercentageValue=(accountLineNew.getEstimateRevenueAmount().doubleValue()/accountLineNew.getEstimateExpense().doubleValue())*100;
 					    			Integer estimatePassPercentage=estimatePassPercentageValue.intValue();
 					    			accountLineNew.setEstimatePassPercentage(estimatePassPercentage);
 						            }
 						            if(accountLineNew.getRevisionRevenueAmount() !=null && accountLineNew.getRevisionRevenueAmount().doubleValue()>0 && accountLineNew.getRevisionExpense()!=null &&  accountLineNew.getRevisionExpense().doubleValue()>0){
 						            	Double revisionPassPercentageValue=(accountLineNew.getRevisionRevenueAmount().doubleValue()/accountLineNew.getRevisionExpense().doubleValue())*100;
 						            	Integer revisionPassPercentage=revisionPassPercentageValue.intValue();
 						            	accountLineNew.setRevisionPassPercentage(revisionPassPercentage);
 						            }
 					    		}catch(Exception e){
 					    			
 					    		}	
 						 
 					    try{
 					    	
 					    	
 					    String payVatPercent=accountLineNew.getPayVatPercent();
 					    BigDecimal payVatPercentBig=new BigDecimal("0.00");
 					    BigDecimal payVatAmmountBig=new BigDecimal("0.00");
 					    BigDecimal estPayVatAmmountBig=new BigDecimal("0.00");
 						BigDecimal revisionpayVatAmmountBig=new BigDecimal("0.00");
 					    if(payVatPercent!=null && (!(payVatPercent.trim().equals("")))){
 					    	payVatPercentBig=new BigDecimal(payVatPercent);
 					    }
 					    payVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
 					    try{
 						    estPayVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getEstLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
 						    revisionpayVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getRevisionLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
 						    
 						  }catch(Exception e){
 								logger.error("Exception Occour: "+ e.getStackTrace()[0]);
 						    }
 					    accountLineNew.setPayVatAmt(payVatAmmountBig);
 					    accountLineNew.setEstExpVatAmt(estPayVatAmmountBig);
 					    accountLineNew.setRevisionExpVatAmt(revisionpayVatAmmountBig);
 					  
 					    }catch(Exception e){
 					    	
 					    }
 				        accountLineNew.setServiceOrderId(serviceOrderToRecods.getId());
 				        accountLineNew.setSequenceNumber(serviceOrderToRecods.getSequenceNumber());
 				        accountLineNew.setShipNumber(serviceOrderToRecods.getShipNumber());
 				        accountLineNew.setServiceOrder(serviceOrderToRecods);
 				        
 				        try{
 				        	String recVatPercent="0";
 				        	recVatPercent=accountLineNew.getRecVatPercent();
 						    BigDecimal recVatPercentBig=new BigDecimal("0.00");
 						    BigDecimal recVatAmmountBig=new BigDecimal("0.00");
 						    BigDecimal revisionVatAmmountBig=new BigDecimal("0.00");
 						    BigDecimal estVatAmmountBig=new BigDecimal("0.00");
 						    if(recVatPercent!=null && (!(recVatPercent.trim().equals("")))){
 						    	recVatPercentBig=new BigDecimal(recVatPercent);
 						    }
 						    DecimalFormat decimalFormat = new DecimalFormat("#.####");
 						    recVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getActualRevenueForeign().multiply(recVatPercentBig)).doubleValue())/100)));
 						    try{
 							    estVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getEstSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
 							    revisionVatAmmountBig = (new BigDecimal(decimalFormat.format(((accountLineNew.getRevisionSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
 							    }catch(Exception e){
 							    	
 							    }
 						    accountLineNew.setRecVatAmt(recVatAmmountBig);
 						    accountLineNew.setEstVatAmt(estVatAmmountBig); 
 						    accountLineNew.setRevisionVatAmt(revisionVatAmmountBig);
 						    }catch(Exception e){
 						    	
 						    }
 				        
 				        accountLineNew=accountLineManager.save(accountLineNew);
 				        updateExternalAccountLine(serviceOrderToRecods,externalCorpID);
 					}
 					}catch(Exception e){
 						
 					}
 				}
 			} catch (Exception e) {
 				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
 		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
 		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
 		    	 e.printStackTrace();
 			} 
 		}
 	   	@SkipValidation
    	public String defaultPricingLineAjax(){ 
    		try {
				serviceOrder = serviceOrderManager.get(id);
				billing= billingManager.get(id);
				miscellaneous = miscellaneousManager.get(id);
				if(serviceOrder.getJob().trim().equalsIgnoreCase("RLO")){
					reloServiceType = serviceOrder.getServiceType();
					while(reloServiceType.indexOf("#")>-1){
						reloServiceType=reloServiceType.replace('#',',');
					}
					serviceTypeSO = reloServiceType;
				}else{
					serviceTypeSO = serviceOrder.getServiceType();
				}
				List defaultList = defaultAccountLineManager.getDefaultAccountList(serviceOrder.getJob(), serviceOrder.getRouting(), serviceOrder.getMode(),billing.getContract(),serviceOrder.getBillToCode(),serviceOrder.getPackingMode(),serviceOrder.getCommodity(),serviceTypeSO,serviceOrder.getCompanyDivision(),serviceOrder.getOriginCountryCode(),serviceOrder.getDestinationCountryCode(),miscellaneous.getEquipment(),serviceOrder.getOriginCity(),serviceOrder.getDestinationCity());
				if(defaultList != null && defaultList.size() > 0 && !defaultList.isEmpty() && defaultList.get(0) != null && !"".equals(defaultList.get(0).toString().trim())){
					returnAjaxStringValue = "YES";
					//accountLineManager.updateSOExtFalg(serviceOrder.getId());
				}else {
					returnAjaxStringValue = "NO";
				}
				
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
		    	 return CANCEL;
			}
    		return SUCCESS;
    	}
 	   
 	private String clickType;
 	private String fieldValues;
 	private  List getCommissionList;
 	
 	@SkipValidation
	    public String findPayableSectionDetails(){
		 try{
		  	accountLine  = accountLineManager.get(id);
		  	if(fieldValues!=null && (!(fieldValues.equals("")))){
			  String str[]=fieldValues.split("~");
			  
			  try{
				  accountLine.setVendorCode(str[0]);
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setEstimateVendorName(str[1]);
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setChargeCode(str[2]);
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setActgCode(str[3]);
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setVarianceExpenseAmount(new BigDecimal(str[4]));
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setActualExpense(new BigDecimal(str[5]));
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setRevisionExpense(new BigDecimal(str[6]));
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setLocalAmount(new BigDecimal(str[7]));
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setCategory(str[8]);
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setActualRevenue(new BigDecimal(str[9]));
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setEstimateRevenueAmount(new BigDecimal(str[10]));
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setRevisionRevenueAmount(new BigDecimal(str[11]));
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setRecRate(new BigDecimal(str[12]));
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setRecQuantity(new BigDecimal(str[13]));
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setPayingStatus(str[14]);
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setPayGl(str[15]);
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  try{
				  accountLine.setRecGl(str[16]);
			  }catch(Exception e){
		    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
			  }
			  if(contractType){
				  try{
					  accountLine.setPayableContractRateAmmount(new BigDecimal(str[17]));
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setContractRate(new BigDecimal(str[18]));
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setContractRateAmmount(new BigDecimal(str[19]));
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
			  }
			  if(multiCurrency.trim().equalsIgnoreCase("Y")){
				  try{
					  accountLine.setRecCurrencyRate(new BigDecimal(str[20]));
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setActualRevenueForeign(new BigDecimal(str[21]));
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
			  }
	  	}
		  	billing = billingManager.get(sid);
	  	/*double actualExpenseVal=0.00;
		double revisionExpenseVal=0.00;
		double varianceExpenseAmountVal=0.00;
		if(actualExpense!=null && (!(actualExpense.toString().trim().equals(""))))
			actualExpenseVal=Double.parseDouble(actualExpense.toString());
		if(revisionExpense!=null && (!(revisionExpense.toString().trim().equals(""))))
			revisionExpenseVal=Double.parseDouble(revisionExpense.toString());
		if(varianceExpenseAmount!=null && (!(varianceExpenseAmount.toString().trim().equals(""))))
			varianceExpenseAmountVal=Double.parseDouble(varianceExpenseAmount.toString());
		if(revisionExpenseVal !=0.00 && actualExpenseVal!=revisionExpenseVal+varianceExpenseAmountVal){
			writeOffReasonFlag=true;
		}*/
		//System.out.println("\n\n\n\nwriteOffReasonFlag"+writeOffReasonFlag);
		  	writeOffReasonFlag=true;
	  	if(vendorCode!=null && !vendorCode.equals("")){
			myfileDocumentList=accountLineManager.getDocumentList(sessionCorpID,vendorCode,accountLine.getShipNumber());
		}
	  	if(chargeCode!=null && !chargeCode.equals("")){
	  		getCommissionList =new ArrayList(accountLineManager.findCommissionList(billing.getContract(), chargeCode));	
	  	}else{
	  		getCommissionList =new ArrayList(accountLineManager.findCommissionList(billing.getContract(), accountLine.getChargeCode()));  
	  	}
	 	if(getCommissionList.isEmpty()||getCommissionList==null){
	 		getCommissionList=new ArrayList(); 	
	 	}
		  	getGlTypeList=new ArrayList(accountLineManager.findGlTypeList(accountLine.getContract(), accountLine.getChargeCode())); 
		  	getPricingHelper();
		}catch(Exception e){
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
 		String logMethod =   e.getStackTrace()[0].getMethodName().toString();
 		errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
 		e.printStackTrace();
		}
	    	return SUCCESS;
	    }


    @SkipValidation
	  public void savePricingPayable(){
		try{
	 		serviceOrder=serviceOrderManager.get(sid);
	 		accountLineOld  = accountLineManager.get(aid);
	 		
	 		accountLineOld.setInvoiceNumber(accountLine.getInvoiceNumber());
	 		accountLineOld.setVendorCode(accountLine.getVendorCode());
	 		accountLineOld.setEstimateVendorName(accountLine.getEstimateVendorName());
	 		accountLineOld.setActgCode(accountLine.getActgCode());
	 		accountLineOld.setWriteOffReason(accountLine.getWriteOffReason());
	 		accountLineOld.setVarianceExpenseAmount(accountLine.getVarianceExpenseAmount());
	 		accountLineOld.setRevisionExpense(accountLine.getRevisionExpense());
	 		accountLineOld.setActualExpense(accountLine.getActualExpense());
	 		accountLineOld.setLocalAmount(accountLine.getLocalAmount());
	 		accountLineOld.setInvoiceDate(accountLine.getInvoiceDate());
	 		accountLineOld.setServiceTaxInput(accountLine.getServiceTaxInput());
	 		accountLineOld.setMyFileFileName(accountLine.getMyFileFileName());
	 		accountLineOld.setReceivedDate(accountLine.getReceivedDate());
	 		accountLineOld.setPayingStatus(accountLine.getPayingStatus());
	 		accountLineOld.setNote(accountLine.getNote());
	 		accountLineOld.setPayPayableStatus(accountLine.getPayPayableStatus());
	 		accountLineOld.setPayPayableDate(accountLine.getPayPayableDate());
	 		accountLineOld.setPayPayableVia(accountLine.getPayPayableVia());
	 		accountLineOld.setPayPayableAmount(accountLine.getPayPayableAmount());
	 		accountLineOld.setPayGl(accountLine.getPayGl());
	 		accountLineOld.setPayPostDate(accountLine.getPayPostDate());
	 		accountLineOld.setAccruePayable(accountLine.getAccruePayable());
	 		accountLineOld.setAccruePayableManual(accountLine.isAccruePayableManual());
	 		accountLineOld.setAccruePayableReverse(accountLine.getAccruePayableReverse());
	 		accountLineOld.setPayAccDate(accountLine.getPayAccDate());
	 		accountLineOld.setPayXfer(accountLine.getPayXfer());
	 		accountLineOld.setPayXferUser(accountLine.getPayXferUser());
	 		accountLineOld.setSettledDate(accountLine.getSettledDate());
	 		accountLineOld.setCostTransferred(accountLine.getCostTransferred());
	 		accountLineOld.setPayVatGl(accountLine.getPayVatGl());
	 		accountLineOld.setQstPayVatGl(accountLine.getQstPayVatGl());
	 		accountLineOld.setPayableContractCurrency(accountLine.getPayableContractCurrency());
	 		accountLineOld.setPayableContractValueDate(accountLine.getPayableContractValueDate());
	 		accountLineOld.setPayableContractExchangeRate(accountLine.getPayableContractExchangeRate());
	 		accountLineOld.setPayableContractRateAmmount(accountLine.getPayableContractRateAmmount());
	 		accountLineOld.setCountry(accountLine.getCountry());
	 		accountLineOld.setValueDate(accountLine.getValueDate());
	 		accountLineOld.setExchangeRate(accountLine.getExchangeRate());
	 		accountLineOld.setManagerApprovalDate(accountLine.getManagerApprovalDate());
	 		accountLineOld.setManagerApprovalName(accountLine.getManagerApprovalName());
	 		
	 		accountLineOld.setUpdatedBy(getRequest().getRemoteUser());
	 		accountLineOld.setUpdatedOn(new Date());
	 		
	 		accountLineOld.setAuthorization(accountLine.getAuthorization());
	 		accountLineOld.setPaymentStatus(accountLine.getPaymentStatus());
	 		accountLineOld.setInvoiceCreatedBy(accountLine.getInvoiceCreatedBy());
	 		accountLineOld.setRecInvoiceNumber(accountLine.getRecInvoiceNumber());
	 		accountLineOld.setReceivedInvoiceDate(accountLine.getReceivedInvoiceDate());
	 		accountLineOld.setExternalReference(accountLine.getExternalReference());
	 		accountLineOld.setCreditInvoiceNumber(accountLine.getCreditInvoiceNumber());
	 		accountLineOld.setReceivedAmount(accountLine.getReceivedAmount());
	 		accountLineOld.setStatusDate(accountLine.getStatusDate());
	 		accountLineOld.setStorageDateRangeFrom(accountLine.getStorageDateRangeFrom());
	 		accountLineOld.setStorageDateRangeTo(accountLine.getStorageDateRangeTo());
	 		accountLineOld.setStorageDays(accountLine.getStorageDays());
	 		accountLineOld.setOnHand(accountLine.getOnHand());
	 		accountLineOld.setPaymentSent(accountLine.getPaymentSent());
	 		accountLineOld.setSendEstToClient(accountLine.getSendEstToClient());
	 		accountLineOld.setSendActualToClient(accountLine.getSendActualToClient());
	 		accountLineOld.setDoNotSendtoClient(accountLine.getDoNotSendtoClient());
	 		accountLineOld.setRecGl(accountLine.getRecGl());
	 		accountLineOld.setAccrueRevenue(accountLine.getAccrueRevenue());
	 		accountLineOld.setAccrueRevenueManual(accountLine.isAccrueRevenueManual());
	 		accountLineOld.setAccrueRevenueReverse(accountLine.getAccrueRevenueReverse());
	 		accountLineOld.setRecPostDate(accountLine.getRecPostDate());
	 		accountLineOld.setRecAccDate(accountLine.getRecAccDate());
	 		accountLineOld.setRecXfer(accountLine.getRecXfer());
	 		accountLineOld.setRecXferUser(accountLine.getRecXferUser());
	 		accountLineOld.setQstRecVatAmt(accountLine.getQstRecVatAmt());
	 		accountLineOld.setDistributionAmount(accountLine.getDistributionAmount());
	 		accountLineOld.setGlType(accountLine.getGlType());
	 		accountLineOld.setDescription(accountLine.getDescription());
	 		accountLineOld.setContractCurrency(accountLine.getContractCurrency());
	 		accountLineOld.setContractRate(accountLine.getContractRate());
	 		accountLineOld.setContractExchangeRate(accountLine.getContractExchangeRate());
	 		accountLineOld.setContractRateAmmount(accountLine.getContractRateAmmount());
	 		accountLineOld.setRecRateCurrency(accountLine.getRecRateCurrency());
	 		accountLineOld.setRecCurrencyRate(accountLine.getRecCurrencyRate());
	 		accountLineOld.setRecRateExchange(accountLine.getRecRateExchange());
	 		accountLineOld.setActualRevenueForeign(accountLine.getActualRevenueForeign());
	 		accountLineOld.setRecVatGl(accountLine.getRecVatGl());
	 		accountLineOld.setQstRecVatGl(accountLine.getQstRecVatGl());
	 		
	 		
	 		if((accountInterface.trim().equalsIgnoreCase("Y"))&&(systemDefaultmiscVl.contains(serviceOrder.getJob()))&&(accountLine.getPayingStatus().equals("A"))&&(accountLine.getActgCode().trim().equalsIgnoreCase("TEMP"))&&(accountLine.getInvoiceDate()==null||accountLine.getInvoiceDate().toString().equals(""))){
	 			accountLineOld.setInvoiceDate(new Date());
			}
	 		
	 		Boolean temp=false;
			try {
				if(pageFieldVisibilityMap.containsKey(sessionCorpID +"-component.field.PayableInvoiceNumber.SetServiceOrderId")) {
					Integer i= (Integer) pageFieldVisibilityMap.get(sessionCorpID +"-component.field.PayableInvoiceNumber.SetServiceOrderId") ;
					if(i>=2){
					temp=true;
					}
				} 
				} catch (Exception e) {
					log.error("Error in CorpComponentPermission the  Field Visibility cache",e);
				}				
			if(accountLine.getVendorCode()!= null && (!(accountLine.getVendorCode().toString().trim().equals(""))) && accountLine.getActualExpense()!=null && accountLine.getActualExpense().doubleValue()!=0 && (accountLine.getInvoiceNumber() == null || accountLine.getInvoiceNumber().toString().trim().equals("")) && accountLine.getActgCode()!=null &&  (!(accountLine.getActgCode().trim().equals("")))){
				boolean isowneroperator =false;
				isowneroperator=accountLineManager.findOwneroperator(accountLine.getVendorCode());
				if(isowneroperator){						
					if(autuPopPostDate){
						String invoiceNumber="A"+sid+accountLine.getVendorCode();
						accountLineOld.setInvoiceNumber(invoiceNumber);
						accountLineOld.setReceivedDate(new Date());
						accountLineOld.setPayingStatus("A");
						accountLineOld.setInvoiceDate(new Date());
					}else if(temp){
						String invoiceNumber=sid+accountLine.getVendorCode();
						accountLineOld.setInvoiceNumber(invoiceNumber);
						accountLineOld.setReceivedDate(new Date());
						accountLineOld.setPayingStatus("A");
						accountLineOld.setInvoiceDate(new Date());							
					}
				}else{
					if(setRegistrationNumber!=null &&(!setRegistrationNumber.equalsIgnoreCase("")) && (accountLine.getActualExpense()!=null && accountLine.getActualExpense().doubleValue()!=0)){
						if(serviceOrder.getRegistrationNumber()!=null && (!(serviceOrder.getRegistrationNumber().equalsIgnoreCase("")))){
								accountLineOld.setInvoiceNumber(serviceOrder.getRegistrationNumber().trim());
							}else{
								accountLineOld.setInvoiceNumber(serviceOrder.getShipNumber());
								}
							if(accountLine.getInvoiceDate()!=null && (!accountLine.getInvoiceDate().equals(""))){
								accountLineOld.setInvoiceDate(accountLine.getInvoiceDate());
							}else{
								accountLineOld.setInvoiceDate(new Date());
							}
								accountLineOld.setReceivedDate(new Date());
						}
					}
				}
			if(setRegistrationNumber!=null &&(!setRegistrationNumber.equalsIgnoreCase("")) && (accountLine.getActualExpense()!=null && accountLine.getActualExpense().doubleValue()!=0) && (accountLine.getInvoiceNumber()!= null && (!accountLine.getInvoiceNumber().toString().trim().equals("")))){
				if(accountLine.getInvoiceDate()!=null && (!accountLine.getInvoiceDate().equals(""))){
					accountLineOld.setInvoiceDate(accountLine.getInvoiceDate());
				}else{
					accountLineOld.setInvoiceDate(new Date());
				}
				if(accountLine.getReceivedDate()!=null){
				}else{
					accountLineOld.setReceivedDate(new Date());
				}
			}
			if(setRegistrationNumber!=null &&(!setRegistrationNumber.equalsIgnoreCase("")) && (accountLine.getActualExpense()==null || accountLine.getActualExpense().doubleValue()==0) && (accountLine.getInvoiceNumber()!= null && (!accountLine.getInvoiceNumber().toString().trim().equals(""))) && (accountLine.getPayingStatus()!=null)&&(accountLine.getPayingStatus().equalsIgnoreCase("A"))){
				accountLineOld.setInvoiceNumber("");
				accountLineOld.setReceivedDate(null);
				accountLineOld.setPayingStatus("");
				accountLineOld.setInvoiceDate(null);
			}
			if((accountLine.getActualExpense()==null || accountLine.getActualExpense().doubleValue()==0  || accountLine.getActgCode()==null || (accountLine.getActgCode().trim().equals(""))) && (accountLine.getInvoiceNumber()!= null && (!accountLine.getInvoiceNumber().toString().trim().equals(""))) && (accountLine.getPayingStatus()!=null)&&(accountLine.getPayingStatus().equalsIgnoreCase("A"))){
				accountLineOld.setInvoiceNumber("");
				accountLineOld.setReceivedDate(null);
				accountLineOld.setPayingStatus("");
				accountLineOld.setInvoiceDate(null);					
			}
			if((accountLine.getPayingStatus()!=null)&&(accountLine.getPayingStatus().equalsIgnoreCase("A"))&&(accountLine.getLocalAmount()!=null)&&(accountLine.getLocalAmount().floatValue()!=0)&& (accountLine.getActualExpense()!=null && accountLine.getActualExpense().doubleValue()!=0)){
				if(autuPopPostDate){
					//change for posting date flexiblity Start	
					try{
			    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
				    		Date dt1=new Date();
				    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				    		String dt11=sdfDestination.format(dt1);
				    		dt1=sdfDestination.parse(dt11);
				    		Date dt2 =null;
				    		try{
				    		dt2 = new Date();
				    		}catch(Exception e){
				    			System.out.println("Getting exception in date is : "+e+"\n\n\n\n Date value is :"+dt2);
				    		}				    		
				    		
				    		dt2=payDate;
				    		if(dt2!=null){
					    		if(dt1.compareTo(dt2)>0){
					    			payDate=dt2;
					    		}else{
					    			payDate=dt1;
					    		}
				    		}
			    		}
				}catch(Exception e){}			
				//change for posting date flexiblity End		
					if(payDate!=null){
						if((accountLine.getPayPostDate()==null)&&(accountLine.getPayAccDate()==null)){
							String permKey = sessionCorpID +"-"+"component.accountLine.varienceAmount";
							checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
							if(checkFieldVisibility){
								double actualExpense=0.00;
								double revisionExpense=0.00;
								double varianceExpenseAmount=0.00;
								if(accountLine.getActualExpense()!=null && (!(accountLine.getActualExpense().toString().trim().equals(""))))
									actualExpense=Double.parseDouble(accountLine.getActualExpense().toString());
								if(accountLine.getRevisionExpense()!=null && (!(accountLine.getRevisionExpense().toString().trim().equals(""))))
									revisionExpense=Double.parseDouble(accountLine.getRevisionExpense().toString());
								if(accountLine.getVarianceExpenseAmount()!=null && (!(accountLine.getVarianceExpenseAmount().toString().trim().equals(""))))
									varianceExpenseAmount=Double.parseDouble(accountLine.getVarianceExpenseAmount().toString());
								if(revisionExpense == 0.00 || actualExpense==revisionExpense+varianceExpenseAmount){
									accountLineOld.setPayPostDate(payDate);
								}else{
									if(writeoffReasonWithFlex1List.contains(accountLine.getWriteOffReason())){
										accountLineOld.setPayPostDate(payDate);
									}
								}
							}else{
								accountLineOld.setPayPostDate(payDate);
							}
						}
					}
					 
				}
			}
			if((accountLineOld.getPayingStatus()!=null && !accountLineOld.getPayingStatus().equals("")) && (accountLineOld.getPayingStatus().equalsIgnoreCase("MH") && accountLine.getPayingStatus().equalsIgnoreCase("A"))){
				accountLineManager.updatePayingSatus(accountLine.getVendorCode(),accountLine.getInvoiceNumber(),accountLineOld.getShipNumber(),getRequest().getRemoteUser(),sessionCorpID);
			}
	 		accountLineManager.save(accountLineOld);
	 		
	 		if(accountLine.getSendActualToClient()!=null && dateUpdate.trim().equalsIgnoreCase("Y")){
				SimpleDateFormat sendActualToClient = new SimpleDateFormat("yyyy-MM-dd");
			    StringBuilder sendActualToClientDates = new StringBuilder(sendActualToClient.format(accountLine.getSendActualToClient())); 
			    int i=accountLineManager.updateInvActSentToDate(serviceOrder.getShipNumber(),sessionCorpID,accountLine.getRecInvoiceNumber(),sendActualToClientDates.toString(),getRequest().getRemoteUser(),accountLine.getId());
			}
	 		
	 		if(accountLine.getVendorCode()!=null && !accountLine.getVendorCode().equals("") && accountLine.getMyFileFileName()!=null && !accountLine.getMyFileFileName().equals("") ){
				String myfileId[]=accountLine.getMyFileFileName().split(":");
				if(!myfileId[0].equals("")){
					String AccPayingStatus="";
					if(accountLine.getPayingStatus().equals("A")){
						AccPayingStatus="Approved";
					}else if(accountLine.getPayingStatus().equals("I")){
						AccPayingStatus="Internal Cost";
					}else if(accountLine.getPayingStatus().equals("N")){
						AccPayingStatus="Not Authorized";
					}else if(accountLine.getPayingStatus().equals("P")){
						AccPayingStatus="Pending with note";
					}else if(accountLine.getPayingStatus().equals("S")){
						AccPayingStatus="Short pay with notes";
					}
					accountLineManager.updateMyfileVender(accountLine.getVendorCode(),AccPayingStatus,myfileId[0],sessionCorpID);	
				}        		
			}
	 		System.out.println(">>>>>>>>>>>>>Receivable Section Save Completed Successfully>>>>>>>>>>>>>>>>>>>");
		 	}catch(Exception e){
		 		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		 		String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		 		errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		 		e.printStackTrace();
		 	}
	  }
 	
 	@SkipValidation
	    public String findReceivableSectionDetails(){
 		try{
		  	accountLine  = accountLineManager.get(id);
		  	if(fieldValues!=null && (!(fieldValues.equals("")))){
				  String str[]=fieldValues.split("~");
				  
				  try{
					  accountLine.setVendorCode(str[0]);
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setEstimateVendorName(str[1]);
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setChargeCode(str[2]);
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setActgCode(str[3]);
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setVarianceExpenseAmount(new BigDecimal(str[4]));
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setActualExpense(new BigDecimal(str[5]));
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setRevisionExpense(new BigDecimal(str[6]));
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setLocalAmount(new BigDecimal(str[7]));
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setCategory(str[8]);
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setActualRevenue(new BigDecimal(str[9]));
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setEstimateRevenueAmount(new BigDecimal(str[10]));
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setRevisionRevenueAmount(new BigDecimal(str[11]));
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setRecRate(new BigDecimal(str[12]));
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setRecQuantity(new BigDecimal(str[13]));
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setPayingStatus(str[14]);
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setPayGl(str[15]);
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  try{
					  accountLine.setRecGl(str[16]);
				  }catch(Exception e){
			    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
				  }
				  if(contractType){
					  try{
						  accountLine.setPayableContractRateAmmount(new BigDecimal(str[17]));
					  }catch(Exception e){
				    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
					  }
					  try{
						  accountLine.setContractRate(new BigDecimal(str[18]));
					  }catch(Exception e){
				    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
					  }
					  try{
						  accountLine.setContractRateAmmount(new BigDecimal(str[19]));
					  }catch(Exception e){
				    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
					  }
				  }
				  if(multiCurrency.trim().equalsIgnoreCase("Y")){
					  try{
						  accountLine.setRecCurrencyRate(new BigDecimal(str[20]));
					  }catch(Exception e){
				    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
					  }
					  try{
						  accountLine.setActualRevenueForeign(new BigDecimal(str[21]));
					  }catch(Exception e){
				    		logger.error("Exception Occour: "+ e.getStackTrace()[0]); 
					  }
				  }
		  	}	  		
		  	billing = billingManager.get(sid);
		  	/*double actualExpenseVal=0.00;
			double revisionExpenseVal=0.00;
			double varianceExpenseAmountVal=0.00;
			if(actualExpense!=null && (!(actualExpense.toString().trim().equals(""))))
				actualExpenseVal=Double.parseDouble(actualExpense.toString());
			if(revisionExpense!=null && (!(revisionExpense.toString().trim().equals(""))))
				revisionExpenseVal=Double.parseDouble(revisionExpense.toString());
			if(varianceExpenseAmount!=null && (!(varianceExpenseAmount.toString().trim().equals(""))))
				varianceExpenseAmountVal=Double.parseDouble(varianceExpenseAmount.toString());
			if(revisionExpenseVal !=0.00 && actualExpenseVal!=revisionExpenseVal+varianceExpenseAmountVal){
				writeOffReasonFlag=true;
			}*/
			//System.out.println("\n\n\n\nwriteOffReasonFlag"+writeOffReasonFlag);
		  	writeOffReasonFlag=true;
		  	getPricingHelper();
		  	if(vendorCode!=null && !vendorCode.equals("")){
				myfileDocumentList=accountLineManager.getDocumentList(sessionCorpID,vendorCode,accountLine.getShipNumber());
			}
		  	if(chargeCode!=null && !chargeCode.equals("")){
		  		getCommissionList =new ArrayList(accountLineManager.findCommissionList(billing.getContract(), chargeCode));	
		  	}else{
		  		getCommissionList =new ArrayList(accountLineManager.findCommissionList(billing.getContract(), accountLine.getChargeCode()));  
		  	}
		 	if(getCommissionList.isEmpty()||getCommissionList==null){
		 		getCommissionList=new ArrayList(); 	
		 	}
		 	getGlTypeList=new ArrayList(accountLineManager.findGlTypeList(accountLine.getContract(), accountLine.getChargeCode()));
	 		}catch(Exception e){
	  			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		 		String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		 		errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		 		e.printStackTrace();
	  		}
 		return SUCCESS;
	    }
    private String payableDataList;
 	@SkipValidation
	  public void savePricingReceivable(){
 		try{
 			
 			if(payableDataList != null && payableDataList.trim().length() > 0){
				JSONObject objectInArray = null;
				JSONArray tempObj=new JSONObject(payableDataList).getJSONArray("records");
			    for (int j = 0, size = tempObj.length(); j < size; j++){
				      	objectInArray = tempObj.getJSONObject(j);
				        Long id = Long.valueOf(objectInArray.optString("aid").trim());
				        Long sid = Long.valueOf(objectInArray.optString("sid").trim());
 			
				 		serviceOrder=serviceOrderManager.get(sid);
				 		accountLineOld  = accountLineManager.get(id);
				 		
				 		accountLineOld.setInvoiceNumber(objectInArray.optString("invoiceNumber"));
				 		accountLineOld.setVendorCode(objectInArray.optString("vendorCode"));
				 		accountLineOld.setEstimateVendorName(objectInArray.optString("estimateVendorName"));
				 		accountLineOld.setActgCode(objectInArray.optString("actgCode"));
				 		accountLineOld.setWriteOffReason(objectInArray.optString("writeOffReason"));
				 		accountLineOld.setVarianceExpenseAmount((objectInArray.optString("varianceExpenseAmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("varianceExpenseAmount").trim()));
				 		accountLineOld.setRevisionExpense((objectInArray.optString("revisionExpense").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("revisionExpense").trim()));
				 		accountLineOld.setActualExpense((objectInArray.optString("actualExpense").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("actualExpense").trim()));
				 		accountLineOld.setLocalAmount((objectInArray.optString("localAmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("localAmount").trim()));
				 		accountLineOld.setInvoiceDate(((objectInArray.optString("invoiceDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("invoiceDate"))));
				 		accountLineOld.setServiceTaxInput((objectInArray.optString("serviceTaxInput").isEmpty()) ? false : Boolean.parseBoolean(objectInArray.optString("serviceTaxInput")));
				 		accountLineOld.setMyFileFileName(objectInArray.optString("myFileFileName"));
				 		accountLineOld.setReceivedDate(((objectInArray.optString("receivedDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("receivedDate"))));
				 		accountLineOld.setPayingStatus(objectInArray.optString("payingStatus"));
				 		accountLineOld.setNote(objectInArray.optString("note"));
				 		accountLineOld.setPayPayableStatus(objectInArray.optString("payPayableStatus"));
				 		accountLineOld.setPayPayableDate(((objectInArray.optString("payPayableDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("payPayableDate"))));
				 		accountLineOld.setPayPayableVia(objectInArray.optString("payPayableVia"));
				 		accountLineOld.setPayPayableAmount((objectInArray.optString("payPayableAmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("payPayableAmount").trim()));
				 		accountLineOld.setPayGl(objectInArray.optString("payGl"));
				 		accountLineOld.setPayPostDate(((objectInArray.optString("payPostDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("payPostDate"))));
				 		accountLineOld.setAccruePayable(((objectInArray.optString("accruePayable").isEmpty()) ? null : getDateFormat(objectInArray.optString("accruePayable"))));
				 		accountLineOld.setAccruePayableManual((objectInArray.optString("accruePayableManual").isEmpty()) ? false : Boolean.parseBoolean(objectInArray.optString("accruePayableManual")));
				 		accountLineOld.setAccruePayableReverse(((objectInArray.optString("accruePayableReverse").isEmpty()) ? null : getDateFormat(objectInArray.optString("accruePayableReverse"))));
				 		accountLineOld.setPayAccDate(((objectInArray.optString("payAccDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("payAccDate"))));
				 		accountLineOld.setPayXfer(objectInArray.optString("payXfer"));
				 		accountLineOld.setPayXferUser(objectInArray.optString("payXferUser"));
				 		accountLineOld.setSettledDate(((objectInArray.optString("settledDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("settledDate"))));
				 		accountLineOld.setCostTransferred((objectInArray.optString("costTransferred").isEmpty()) ? false : Boolean.parseBoolean(objectInArray.optString("costTransferred")));
				 		accountLineOld.setPayVatGl(objectInArray.optString("payVatGl"));
				 		accountLineOld.setQstPayVatGl(objectInArray.optString("qstPayVatGl"));
				 		accountLineOld.setPayableContractCurrency(objectInArray.optString("payableContractCurrency"));
				 		accountLineOld.setPayableContractValueDate(((objectInArray.optString("payableContractValueDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("payableContractValueDate"))));
				 		accountLineOld.setPayableContractExchangeRate((objectInArray.optString("payableContractExchangeRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("payableContractExchangeRate").trim()));
				 		accountLineOld.setPayableContractRateAmmount((objectInArray.optString("payableContractRateAmmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("payableContractRateAmmount").trim()));
				 		accountLineOld.setCountry(objectInArray.optString("country"));
				 		accountLineOld.setValueDate(((objectInArray.optString("valueDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("valueDate"))));
				 		accountLineOld.setExchangeRate((objectInArray.optString("exchangeRate").isEmpty()) ? new Double(0.00) : new Double(objectInArray.optString("exchangeRate").trim()));
				 		accountLineOld.setManagerApprovalDate(((objectInArray.optString("managerApprovalDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("managerApprovalDate"))));
				 		accountLineOld.setManagerApprovalName(objectInArray.optString("managerApprovalName"));
				 		
				 		accountLineOld.setUpdatedBy(getRequest().getRemoteUser());
				 		accountLineOld.setUpdatedOn(new Date());
				 		
				 		accountLineOld.setAuthorization(objectInArray.optString("authorization"));
				 		accountLineOld.setPaymentStatus(objectInArray.optString("paymentStatus"));
				 		accountLineOld.setInvoiceCreatedBy(objectInArray.optString("invoiceCreatedBy"));
				 		accountLineOld.setRecInvoiceNumber(objectInArray.optString("recInvoiceNumber"));
				 		accountLineOld.setReceivedInvoiceDate(((objectInArray.optString("receivedInvoiceDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("receivedInvoiceDate"))));
				 		accountLineOld.setExternalReference(objectInArray.optString("externalReference"));
				 		accountLineOld.setCreditInvoiceNumber(objectInArray.optString("creditInvoiceNumber"));
				 		accountLineOld.setReceivedAmount((objectInArray.optString("receivedAmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("receivedAmount").trim()));
				 		accountLineOld.setStatusDate(((objectInArray.optString("statusDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("statusDate"))));
				 		accountLineOld.setStorageDateRangeFrom(((objectInArray.optString("storageDateRangeFrom").isEmpty()) ? null : getDateFormat(objectInArray.optString("storageDateRangeFrom"))));
				 		accountLineOld.setStorageDateRangeTo(((objectInArray.optString("storageDateRangeTo").isEmpty()) ? null : getDateFormat(objectInArray.optString("storageDateRangeTo"))));
				 		accountLineOld.setStorageDays((objectInArray.optString("storageDays").isEmpty()) ? null : new Integer(objectInArray.optString("storageDays").trim()));
				 		accountLineOld.setOnHand((objectInArray.optString("onHand").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("onHand").trim()));
				 		accountLineOld.setPaymentSent(((objectInArray.optString("paymentSent").isEmpty()) ? null : getDateFormat(objectInArray.optString("paymentSent"))));
				 		accountLineOld.setSendEstToClient(((objectInArray.optString("sendEstToClient").isEmpty()) ? null : getDateFormat(objectInArray.optString("sendEstToClient"))));
				 		accountLineOld.setSendActualToClient(((objectInArray.optString("sendActualToClient").isEmpty()) ? null : getDateFormat(objectInArray.optString("sendActualToClient"))));
				 		accountLineOld.setDoNotSendtoClient(((objectInArray.optString("doNotSendtoClient").isEmpty()) ? null : getDateFormat(objectInArray.optString("doNotSendtoClient"))));
				 		accountLineOld.setRecGl(objectInArray.optString("recGl"));
				 		accountLineOld.setAccrueRevenue(((objectInArray.optString("accrueRevenue").isEmpty()) ? null : getDateFormat(objectInArray.optString("accrueRevenue"))));
				 		accountLineOld.setAccrueRevenueManual((objectInArray.optString("accrueRevenueManual").isEmpty()) ? false : Boolean.parseBoolean(objectInArray.optString("accrueRevenueManual")));
				 		accountLineOld.setAccrueRevenueReverse(((objectInArray.optString("accrueRevenueReverse").isEmpty()) ? null : getDateFormat(objectInArray.optString("accrueRevenueReverse"))));
				 		accountLineOld.setRecPostDate(((objectInArray.optString("recPostDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("recPostDate"))));
				 		accountLineOld.setRecAccDate(((objectInArray.optString("recAccDate").isEmpty()) ? null : getDateFormat(objectInArray.optString("recAccDate"))));
				 		accountLineOld.setRecXfer(objectInArray.optString("recXfer"));
				 		accountLineOld.setRecXferUser(objectInArray.optString("recXferUser"));
				 		accountLineOld.setQstRecVatAmt((objectInArray.optString("qstRecVatAmt").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("qstRecVatAmt").trim()));
				 		accountLineOld.setDistributionAmount((objectInArray.optString("distributionAmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("distributionAmount").trim()));
				 		accountLineOld.setGlType(objectInArray.optString("glType"));
				 		accountLineOld.setDescription(objectInArray.optString("description"));
				 		accountLineOld.setContractCurrency(objectInArray.optString("contractCurrency"));
				 		accountLineOld.setContractRate((objectInArray.optString("contractRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("contractRate").trim()));
				 		accountLineOld.setContractExchangeRate((objectInArray.optString("contractExchangeRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("contractExchangeRate").trim()));
				 		accountLineOld.setContractRateAmmount((objectInArray.optString("contractRateAmmount").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("contractRateAmmount").trim()));
				 		accountLineOld.setRecRateCurrency(objectInArray.optString("recRateCurrency"));
				 		accountLineOld.setRecCurrencyRate((objectInArray.optString("recCurrencyRate").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("recCurrencyRate").trim()));
				 		accountLineOld.setRecRateExchange((objectInArray.optString("recRateExchange").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("recRateExchange").trim()));
				 		accountLineOld.setActualRevenueForeign((objectInArray.optString("actualRevenueForeign").isEmpty()) ? new BigDecimal(0.00) : new BigDecimal(objectInArray.optString("actualRevenueForeign").trim()));
				 		accountLineOld.setRecVatGl(objectInArray.optString("recVatGl"));
				 		accountLineOld.setQstRecVatGl(objectInArray.optString("qstRecVatGl"));
				 		
				 		if((accountInterface.trim().equalsIgnoreCase("Y"))&&(systemDefaultmiscVl.contains(serviceOrder.getJob()))&&(accountLineOld.getPayingStatus().equals("A"))&&(accountLineOld.getActgCode().trim().equalsIgnoreCase("TEMP"))&&(accountLineOld.getInvoiceDate()==null||accountLineOld.getInvoiceDate().toString().equals(""))){
				 			accountLineOld.setInvoiceDate(new Date());
						}
				 		
				 		Boolean temp=false;
						try {
							if(pageFieldVisibilityMap.containsKey(sessionCorpID +"-component.field.PayableInvoiceNumber.SetServiceOrderId")) {
								Integer i= (Integer) pageFieldVisibilityMap.get(sessionCorpID +"-component.field.PayableInvoiceNumber.SetServiceOrderId") ;
								if(i>=2){
								temp=true;
								}
							} 
							} catch (Exception e) {
								log.error("Error in CorpComponentPermission the  Field Visibility cache",e);
							}				
						if(accountLineOld.getVendorCode()!= null && (!(accountLineOld.getVendorCode().toString().trim().equals(""))) && accountLineOld.getActualExpense()!=null && accountLineOld.getActualExpense().doubleValue()!=0 && (accountLineOld.getInvoiceNumber() == null || accountLineOld.getInvoiceNumber().toString().trim().equals("")) && accountLineOld.getActgCode()!=null &&  (!(accountLineOld.getActgCode().trim().equals("")))){
							boolean isowneroperator =false;
							isowneroperator=accountLineManager.findOwneroperator(accountLineOld.getVendorCode());
							if(isowneroperator){						
								if(autuPopPostDate){
									String invoiceNumber="A"+sid+accountLineOld.getVendorCode();
									accountLineOld.setInvoiceNumber(invoiceNumber);
									accountLineOld.setReceivedDate(new Date());
									accountLineOld.setPayingStatus("A");
									accountLineOld.setInvoiceDate(new Date());
								}else if(temp){
									String invoiceNumber=sid+accountLineOld.getVendorCode();
									accountLineOld.setInvoiceNumber(invoiceNumber);
									accountLineOld.setReceivedDate(new Date());
									accountLineOld.setPayingStatus("A");
									accountLineOld.setInvoiceDate(new Date());							
								}
							}else{
								if(setRegistrationNumber!=null &&(!setRegistrationNumber.equalsIgnoreCase("")) && (accountLineOld.getActualExpense()!=null && accountLineOld.getActualExpense().doubleValue()!=0)){
									if(serviceOrder.getRegistrationNumber()!=null && (!(serviceOrder.getRegistrationNumber().equalsIgnoreCase("")))){
											accountLineOld.setInvoiceNumber(serviceOrder.getRegistrationNumber().trim());
										}else{
											accountLineOld.setInvoiceNumber(serviceOrder.getShipNumber());
											}
										if(accountLineOld.getInvoiceDate()!=null && (!accountLineOld.getInvoiceDate().equals(""))){
											accountLineOld.setInvoiceDate(accountLineOld.getInvoiceDate());
										}else{
											accountLineOld.setInvoiceDate(new Date());
										}
											accountLineOld.setReceivedDate(new Date());
									}
								}
							}
						if(setRegistrationNumber!=null &&(!setRegistrationNumber.equalsIgnoreCase("")) && (accountLineOld.getActualExpense()!=null && accountLineOld.getActualExpense().doubleValue()!=0) && (accountLineOld.getInvoiceNumber()!= null && (!accountLineOld.getInvoiceNumber().toString().trim().equals("")))){
							if(accountLineOld.getInvoiceDate()!=null && (!accountLineOld.getInvoiceDate().equals(""))){
								accountLineOld.setInvoiceDate(accountLineOld.getInvoiceDate());
							}else{
								accountLineOld.setInvoiceDate(new Date());
							}
							if(accountLineOld.getReceivedDate()!=null){
							}else{
								accountLineOld.setReceivedDate(new Date());
							}
						}
						if(setRegistrationNumber!=null &&(!setRegistrationNumber.equalsIgnoreCase("")) && (accountLineOld.getActualExpense()==null || accountLineOld.getActualExpense().doubleValue()==0) && (accountLineOld.getInvoiceNumber()!= null && (!accountLineOld.getInvoiceNumber().toString().trim().equals(""))) && (accountLineOld.getPayingStatus()!=null)&&(accountLineOld.getPayingStatus().equalsIgnoreCase("A"))){
							accountLineOld.setInvoiceNumber("");
							accountLineOld.setReceivedDate(null);
							accountLineOld.setPayingStatus("");
							accountLineOld.setInvoiceDate(null);
						}
						if((accountLineOld.getActualExpense()==null || accountLineOld.getActualExpense().doubleValue()==0  || accountLineOld.getActgCode()==null || (accountLineOld.getActgCode().trim().equals(""))) && (accountLineOld.getInvoiceNumber()!= null && (!accountLineOld.getInvoiceNumber().toString().trim().equals(""))) && (accountLineOld.getPayingStatus()!=null)&&(accountLineOld.getPayingStatus().equalsIgnoreCase("A"))){
							accountLineOld.setInvoiceNumber("");
							accountLineOld.setReceivedDate(null);
							accountLineOld.setPayingStatus("");
							accountLineOld.setInvoiceDate(null);					
						}
						if((accountLineOld.getPayingStatus()!=null)&&(accountLineOld.getPayingStatus().equalsIgnoreCase("A"))&&(accountLineOld.getLocalAmount()!=null)&&(accountLineOld.getLocalAmount().floatValue()!=0)&& (accountLineOld.getActualExpense()!=null && accountLineOld.getActualExpense().doubleValue()!=0)){
							if(autuPopPostDate){
								//change for posting date flexiblity Start	
								try{
						    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
							    		Date dt1=new Date();
							    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
							    		String dt11=sdfDestination.format(dt1);
							    		dt1=sdfDestination.parse(dt11);
							    		Date dt2 =null;
							    		try{
							    		dt2 = new Date();
							    		}catch(Exception e){
							    			System.out.println("Getting exception in date is : "+e+"\n\n\n\n Date value is :"+dt2);
							    		}				    		
							    		
							    		dt2=payDate;
							    		if(dt2!=null){
								    		if(dt1.compareTo(dt2)>0){
								    			payDate=dt2;
								    		}else{
								    			payDate=dt1;
								    		}
							    		}
						    		}
							}catch(Exception e){}			
							//change for posting date flexiblity End		
								if(payDate!=null){
									if((accountLineOld.getPayPostDate()==null)&&(accountLineOld.getPayAccDate()==null)){
										String permKey = sessionCorpID +"-"+"component.accountLine.varienceAmount";
										checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
										if(checkFieldVisibility){
											double actualExpense=0.00;
											double revisionExpense=0.00;
											double varianceExpenseAmount=0.00;
											if(accountLineOld.getActualExpense()!=null && (!(accountLineOld.getActualExpense().toString().trim().equals(""))))
												actualExpense=Double.parseDouble(accountLineOld.getActualExpense().toString());
											if(accountLineOld.getRevisionExpense()!=null && (!(accountLineOld.getRevisionExpense().toString().trim().equals(""))))
												revisionExpense=Double.parseDouble(accountLineOld.getRevisionExpense().toString());
											if(accountLineOld.getVarianceExpenseAmount()!=null && (!(accountLineOld.getVarianceExpenseAmount().toString().trim().equals(""))))
												varianceExpenseAmount=Double.parseDouble(accountLineOld.getVarianceExpenseAmount().toString());
											if(revisionExpense == 0.00 || actualExpense==revisionExpense+varianceExpenseAmount){
												accountLineOld.setPayPostDate(payDate);
											}else{
												if(writeoffReasonWithFlex1List.contains(accountLineOld.getWriteOffReason())){
													accountLineOld.setPayPostDate(payDate);
												}
											}
										}else{
											accountLineOld.setPayPostDate(payDate);
										}
									}
								}
								 
							}
						}
						if((accountLineOld.getPayingStatus()!=null && !accountLineOld.getPayingStatus().equals("")) && (accountLineOld.getPayingStatus().equalsIgnoreCase("MH") && accountLineOld.getPayingStatus().equalsIgnoreCase("A"))){
							accountLineManager.updatePayingSatus(accountLineOld.getVendorCode(),accountLineOld.getInvoiceNumber(),accountLineOld.getShipNumber(),getRequest().getRemoteUser(),sessionCorpID);
						}
				 		accountLineManager.save(accountLineOld);
				 		
				 		if(accountLineOld.getSendActualToClient()!=null && dateUpdate.trim().equalsIgnoreCase("Y")){
							SimpleDateFormat sendActualToClient = new SimpleDateFormat("yyyy-MM-dd");
						    StringBuilder sendActualToClientDates = new StringBuilder(sendActualToClient.format(accountLineOld.getSendActualToClient())); 
						    int i=accountLineManager.updateInvActSentToDate(serviceOrder.getShipNumber(),sessionCorpID,accountLineOld.getRecInvoiceNumber(),sendActualToClientDates.toString(),getRequest().getRemoteUser(),accountLineOld.getId());
						}
				 		
				 		if(accountLineOld.getVendorCode()!=null && !accountLineOld.getVendorCode().equals("") && accountLineOld.getMyFileFileName()!=null && !accountLineOld.getMyFileFileName().equals("") ){
							String myfileId[]=accountLineOld.getMyFileFileName().split(":");
							if(!myfileId[0].equals("")){
								String AccPayingStatus="";
								if(accountLineOld.getPayingStatus().equals("A")){
									AccPayingStatus="Approved";
								}else if(accountLineOld.getPayingStatus().equals("I")){
									AccPayingStatus="Internal Cost";
								}else if(accountLineOld.getPayingStatus().equals("N")){
									AccPayingStatus="Not Authorized";
								}else if(accountLineOld.getPayingStatus().equals("P")){
									AccPayingStatus="Pending with note";
								}else if(accountLineOld.getPayingStatus().equals("S")){
									AccPayingStatus="Short pay with notes";
								}
								accountLineManager.updateMyfileVender(accountLineOld.getVendorCode(),AccPayingStatus,myfileId[0],sessionCorpID);	
							}        		
						}
				 		System.out.println(">>>>>>>>>>>>>Payable Section Save Completed Successfully>>>>>>>>>>>>>>>>>>>");
					}
			}
	 	}catch(Exception e){
	 		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	 		String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	 		errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	 		e.printStackTrace();
	 	}
 	}
 	
 	@SkipValidation
	  public String findCheckBoxesInACListAjax(){
		  accountLine  = accountLineManager.get(aid);
		  serviceOrder = serviceOrderManager.get(accountLine.getServiceOrderId());
		  trackingStatus=trackingStatusManager.get(serviceOrder.getId());
		  billing = billingManager.get(serviceOrder.getId());
		  if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			  billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
			}
		  if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			  	billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
			}		  
		  return SUCCESS;
 	}
 	private String updateQuery;
 	@SkipValidation
 	public void updateCheckBoxValueInAcListAjax(){
 		accountLineManager.updateCheckBoxValueInAcListAjax(updateQuery);
 	}
 	
 	private String writeOffReasonValue;
 	@SkipValidation
 	public String findBucket2WriteOffReasonAjax(){ 
 		returnAjaxStringValue = refMasterManager.findBucket2Value(writeOffReasonValue,"WRITEOFF_REASON");
 		return SUCCESS;
 	}
 	
 	public Boolean getAcctDisplayEntitled() {
		return acctDisplayEntitled;
	}

	public void setAcctDisplayEntitled(Boolean acctDisplayEntitled) {
		this.acctDisplayEntitled = acctDisplayEntitled;
	}

	public Boolean getAcctDisplayEstimate() {
		return acctDisplayEstimate;
	}

	public void setAcctDisplayEstimate(Boolean acctDisplayEstimate) {
		this.acctDisplayEstimate = acctDisplayEstimate;
	}

	public Boolean getAcctDisplayRevision() {
		return acctDisplayRevision;
	}

	public void setAcctDisplayRevision(Boolean acctDisplayRevision) {
		this.acctDisplayRevision = acctDisplayRevision;
	}

	public boolean isAccountLineAccountPortalFlag() {
		return accountLineAccountPortalFlag;
	}

	public void setAccountLineAccountPortalFlag(boolean accountLineAccountPortalFlag) {
		this.accountLineAccountPortalFlag = accountLineAccountPortalFlag;
	}

	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public Billing getBillingRecods() {
		return billingRecods;
	}

	public void setBillingRecods(Billing billingRecods) {
		this.billingRecods = billingRecods;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public ServiceOrder getServiceOrderToRecods() {
		return serviceOrderToRecods;
	}

	public void setServiceOrderToRecods(ServiceOrder serviceOrderToRecods) {
		this.serviceOrderToRecods = serviceOrderToRecods;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public TrackingStatus getTrackingStatusToRecods() {
		return trackingStatusToRecods;
	}

	public void setTrackingStatusToRecods(TrackingStatus trackingStatusToRecods) {
		this.trackingStatusToRecods = trackingStatusToRecods;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public CustomerFile getCustomerFileToRecods() {
		return customerFileToRecods;
	}

	public void setCustomerFileToRecods(CustomerFile customerFileToRecods) {
		this.customerFileToRecods = customerFileToRecods;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getEstVatFlag() {
		return estVatFlag;
	}

	public void setEstVatFlag(String estVatFlag) {
		this.estVatFlag = estVatFlag;
	}

	public boolean isNetworkAgent() {
		return networkAgent;
	}

	public void setNetworkAgent(boolean networkAgent) {
		this.networkAgent = networkAgent;
	}

	public Boolean getChargeDiscountFlag() {
		return chargeDiscountFlag;
	}

	public void setChargeDiscountFlag(Boolean chargeDiscountFlag) {
		this.chargeDiscountFlag = chargeDiscountFlag;
	}

	public String getCommissionJobName() {
		return commissionJobName;
	}

	public void setCommissionJobName(String commissionJobName) {
		this.commissionJobName = commissionJobName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getSystemDefaultmiscVl() {
		return systemDefaultmiscVl;
	}

	public void setSystemDefaultmiscVl(String systemDefaultmiscVl) {
		this.systemDefaultmiscVl = systemDefaultmiscVl;
	}

	public List getCompDivForBookingAgentList() {
		return compDivForBookingAgentList;
	}

	public void setCompDivForBookingAgentList(List compDivForBookingAgentList) {
		this.compDivForBookingAgentList = compDivForBookingAgentList;
	}

	public void setCompanyDivisionManager(
			CompanyDivisionManager companyDivisionManager) {
		this.companyDivisionManager = companyDivisionManager;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public String getSystemDefaultVatCalculationNew() {
		return systemDefaultVatCalculationNew;
	}

	public void setSystemDefaultVatCalculationNew(
			String systemDefaultVatCalculationNew) {
		this.systemDefaultVatCalculationNew = systemDefaultVatCalculationNew;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

	public String getPayVatDescrList() {
		return payVatDescrList;
	}

	public void setPayVatDescrList(String payVatDescrList) {
		this.payVatDescrList = payVatDescrList;
	}

	public String getRecVatDescrList() {
		return recVatDescrList;
	}

	public void setRecVatDescrList(String recVatDescrList) {
		this.recVatDescrList = recVatDescrList;
	}

	public Integer getCompanyDivisionGlobal() {
		return companyDivisionGlobal;
	}

	public void setCompanyDivisionGlobal(Integer companyDivisionGlobal) {
		this.companyDivisionGlobal = companyDivisionGlobal;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public String getBaseCurrencyCompanyDivision() {
		return baseCurrencyCompanyDivision;
	}

	public void setBaseCurrencyCompanyDivision(String baseCurrencyCompanyDivision) {
		this.baseCurrencyCompanyDivision = baseCurrencyCompanyDivision;
	}

	public String getMultiCurrency() {
		return multiCurrency;
	}

	public void setMultiCurrency(String multiCurrency) {
		this.multiCurrency = multiCurrency;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getEuVatPercentList() {
		return euVatPercentList;
	}

	public void setEuVatPercentList(Map<String, String> euVatPercentList) {
		this.euVatPercentList = euVatPercentList;
	}

	public Map<String, String> getPayVatPercentList() {
		return payVatPercentList;
	}

	public void setPayVatPercentList(Map<String, String> payVatPercentList) {
		this.payVatPercentList = payVatPercentList;
	}

	public Map<String, String> getEstVatList() {
		return estVatList;
	}

	public void setEstVatList(Map<String, String> estVatList) {
		this.estVatList = estVatList;
	}

	public Map<String, String> getPayVatList() {
		return payVatList;
	}

	public void setPayVatList(Map<String, String> payVatList) {
		this.payVatList = payVatList;
	}

	public Date getPricePointUserStartDate() {
		return pricePointUserStartDate;
	}

	public void setPricePointUserStartDate(Date pricePointUserStartDate) {
		this.pricePointUserStartDate = pricePointUserStartDate;
	}

	public boolean isPricePointFlag() {
		return pricePointFlag;
	}

	public void setPricePointFlag(boolean pricePointFlag) {
		this.pricePointFlag = pricePointFlag;
	}

	public boolean isPricePointexpired() {
		return pricePointexpired;
	}

	public void setPricePointexpired(boolean pricePointexpired) {
		this.pricePointexpired = pricePointexpired;
	}

	public Map<String, String> getCategory() {
		return category;
	}

	public void setCategory(Map<String, String> category) {
		this.category = category;
	}

	public boolean isContractType() {
		return contractType;
	}

	public void setContractType(boolean contractType) {
		this.contractType = contractType;
	}

	public boolean isBillingCMMContractType() {
		return billingCMMContractType;
	}

	public void setBillingCMMContractType(boolean billingCMMContractType) {
		this.billingCMMContractType = billingCMMContractType;
	}

	public boolean isBillingDMMContractType() {
		return billingDMMContractType;
	}

	public void setBillingDMMContractType(boolean billingDMMContractType) {
		this.billingDMMContractType = billingDMMContractType;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public int getBillToLength() {
		return billToLength;
	}

	public void setBillToLength(int billToLength) {
		this.billToLength = billToLength;
	}

	public Map<String, String> getBasis() {
		return basis;
	}

	public void setBasis(Map<String, String> basis) {
		this.basis = basis;
	}

	public List getCompanyDivis() {
		return companyDivis;
	}

	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}

	public String getAccountLineStatus() {
		return accountLineStatus;
	}

	public void setAccountLineStatus(String accountLineStatus) {
		this.accountLineStatus = accountLineStatus;
	}

	public List getAccountLineList() {
		return accountLineList;
	}

	public void setAccountLineList(List accountLineList) {
		this.accountLineList = accountLineList;
	}
	public String getOldRecRateCurrency() {
		return oldRecRateCurrency;
	}

	public void setOldRecRateCurrency(String oldRecRateCurrency) {
		this.oldRecRateCurrency = oldRecRateCurrency;
	}

	public String getAddPriceLine() {
		return addPriceLine;
	}

	public void setAddPriceLine(String addPriceLine) {
		this.addPriceLine = addPriceLine;
	}

	public String getInActiveLine() {
		return inActiveLine;
	}

	public void setInActiveLine(String inActiveLine) {
		this.inActiveLine = inActiveLine;
	}

	/*public String getIsSOExtract() {
		return isSOExtract;
	}

	public void setIsSOExtract(String isSOExtract) {
		this.isSOExtract = isSOExtract;
	}*/

	public AccountLine getAccountLine() {
		return accountLine;
	}

	public void setAccountLine(AccountLine accountLine) {
		this.accountLine = accountLine;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDivisionFlag() {
		return divisionFlag;
	}

	public void setDivisionFlag(String divisionFlag) {
		this.divisionFlag = divisionFlag;
	}

	public String getSoContract() {
		return SoContract;
	}

	public void setSoContract(String soContract) {
		SoContract = soContract;
	}

	public String getSoBillToCode() {
		return SoBillToCode;
	}

	public void setSoBillToCode(String soBillToCode) {
		SoBillToCode = soBillToCode;
	}

	public String getReloServiceType() {
		return reloServiceType;
	}

	public void setReloServiceType(String reloServiceType) {
		this.reloServiceType = reloServiceType;
	}

	public String getServiceTypeSO() {
		return serviceTypeSO;
	}

	public void setServiceTypeSO(String serviceTypeSO) {
		this.serviceTypeSO = serviceTypeSO;
	}

	public String getRollUpInvoiceFlag() {
		return rollUpInvoiceFlag;
	}

	public void setRollUpInvoiceFlag(String rollUpInvoiceFlag) {
		this.rollUpInvoiceFlag = rollUpInvoiceFlag;
	}

	public List getChargeCodeList() {
		return chargeCodeList;
	}

	public void setChargeCodeList(List chargeCodeList) {
		this.chargeCodeList = chargeCodeList;
	}

	public Boolean getIgnoreForBilling() {
		return ignoreForBilling;
	}

	public void setIgnoreForBilling(Boolean ignoreForBilling) {
		this.ignoreForBilling = ignoreForBilling;
	}

	public int getBillingFlag() {
		return billingFlag;
	}

	public void setBillingFlag(int billingFlag) {
		this.billingFlag = billingFlag;
	}

	public String getVanLineAccountView() {
		return vanLineAccountView;
	}

	public void setVanLineAccountView(String vanLineAccountView) {
		this.vanLineAccountView = vanLineAccountView;
	}

	public String getVanLineAccCategoryURL() {
		return vanLineAccCategoryURL;
	}

	public void setVanLineAccCategoryURL(String vanLineAccCategoryURL) {
		this.vanLineAccCategoryURL = vanLineAccCategoryURL;
	}

	public void setChargesManager(ChargesManager chargesManager) {
		this.chargesManager = chargesManager;
	}

	public void setDefaultAccountLineManager(
			DefaultAccountLineManager defaultAccountLineManager) {
		this.defaultAccountLineManager = defaultAccountLineManager;
	}

	public String getPricingData() {
		return pricingData;
	}

	public void setPricingData(String pricingData) {
		this.pricingData = pricingData;
	}

	public Map<String, Map<String, String>> getDiscountMap() {
		return discountMap;
	}

	public void setDiscountMap(Map<String, Map<String, String>> discountMap) {
		this.discountMap = discountMap;
	}

	public String getAccUpdateAjax() {
		return accUpdateAjax;
	}

	public void setAccUpdateAjax(String accUpdateAjax) {
		this.accUpdateAjax = accUpdateAjax;
	}

	public void setDiscountManager(DiscountManager discountManager) {
		this.discountManager = discountManager;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getCommodityForUser() {
		return commodityForUser;
	}

	public void setCommodityForUser(String commodityForUser) {
		this.commodityForUser = commodityForUser;
	}

	public Boolean getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(Boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public String getBookingAppUserCode() {
		return bookingAppUserCode;
	}

	public void setBookingAppUserCode(String bookingAppUserCode) {
		this.bookingAppUserCode = bookingAppUserCode;
	}

	public String getDefaultSoURL() {
		return defaultSoURL;
	}

	public void setDefaultSoURL(String defaultSoURL) {
		this.defaultSoURL = defaultSoURL;
	}

	public User getDiscountUserFlag() {
		return discountUserFlag;
	}

	public void setDiscountUserFlag(User discountUserFlag) {
		this.discountUserFlag = discountUserFlag;
	}

	public String getServiceTypeForUser() {
		return serviceTypeForUser;
	}

	public void setServiceTypeForUser(String serviceTypeForUser) {
		this.serviceTypeForUser = serviceTypeForUser;
	}

	public String getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}

	public void setReturnAjaxStringValue(String returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	public String getCheckContractChargesMandatory() {
		return checkContractChargesMandatory;
	}

	public void setCheckContractChargesMandatory(
			String checkContractChargesMandatory) {
		this.checkContractChargesMandatory = checkContractChargesMandatory;
	}

	public boolean isCheckAccessQuotation() {
		return checkAccessQuotation;
	}

	public void setCheckAccessQuotation(boolean checkAccessQuotation) {
		this.checkAccessQuotation = checkAccessQuotation;
	}

	public Boolean getCostElementFlag() {
		return costElementFlag;
	}

	public void setCostElementFlag(Boolean costElementFlag) {
		this.costElementFlag = costElementFlag;
	}

	public String getAccountInterface() {
		return accountInterface;
	}

	public void setAccountInterface(String accountInterface) {
		this.accountInterface = accountInterface;
	}

	public String getCompanyDivisionAcctgCodeUnique() {
		return companyDivisionAcctgCodeUnique;
	}

	public void setCompanyDivisionAcctgCodeUnique(
			String companyDivisionAcctgCodeUnique) {
		this.companyDivisionAcctgCodeUnique = companyDivisionAcctgCodeUnique;
	}

	public Map<String, String> getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	public void setCurrencyExchangeRate(Map<String, String> currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	public void setExchangeRateManager(ExchangeRateManager exchangeRateManager) {
		this.exchangeRateManager = exchangeRateManager;
	}

	public Map<String, String> getEuvatRecGLMap() {
		return euvatRecGLMap;
	}

	public void setEuvatRecGLMap(Map<String, String> euvatRecGLMap) {
		this.euvatRecGLMap = euvatRecGLMap;
	}

	public Map<String, String> getPayVatPayGLMap() {
		return payVatPayGLMap;
	}

	public void setPayVatPayGLMap(Map<String, String> payVatPayGLMap) {
		this.payVatPayGLMap = payVatPayGLMap;
	}

	public Map<String, String> getQstEuvatRecGLMap() {
		return qstEuvatRecGLMap;
	}

	public void setQstEuvatRecGLMap(Map<String, String> qstEuvatRecGLMap) {
		this.qstEuvatRecGLMap = qstEuvatRecGLMap;
	}

	public Map<String, String> getQstPayVatPayGLMap() {
		return qstPayVatPayGLMap;
	}

	public void setQstPayVatPayGLMap(Map<String, String> qstPayVatPayGLMap) {
		this.qstPayVatPayGLMap = qstPayVatPayGLMap;
	}

	public Map<String, String> getQstEuvatRecGLAmtMap() {
		return qstEuvatRecGLAmtMap;
	}

	public void setQstEuvatRecGLAmtMap(Map<String, String> qstEuvatRecGLAmtMap) {
		this.qstEuvatRecGLAmtMap = qstEuvatRecGLAmtMap;
	}

	public Map<String, String> getQstPayVatPayGLAmtMap() {
		return qstPayVatPayGLAmtMap;
	}

	public void setQstPayVatPayGLAmtMap(Map<String, String> qstPayVatPayGLAmtMap) {
		this.qstPayVatPayGLAmtMap = qstPayVatPayGLAmtMap;
	}

	public Map<String, String> getEstVatPersentList() {
		return estVatPersentList;
	}

	public void setEstVatPersentList(Map<String, String> estVatPersentList) {
		this.estVatPersentList = estVatPersentList;
	}

	public AccountLine getAccountLinePopup() {
		return accountLinePopup;
	}

	public void setAccountLinePopup(AccountLine accountLinePopup) {
		this.accountLinePopup = accountLinePopup;
	}

	public Map<String, String> getPayingStatus() {
		return payingStatus;
	}

	public void setPayingStatus(Map<String, String> payingStatus) {
		this.payingStatus = payingStatus;
	}

	public Map<String, String> getCountry() {
		return country;
	}

	public void setCountry(Map<String, String> country) {
		this.country = country;
	}

	public List getQuotationDiscriptionList() {
		return quotationDiscriptionList;
	}

	public void setQuotationDiscriptionList(List quotationDiscriptionList) {
		this.quotationDiscriptionList = quotationDiscriptionList;
	}

	public String getSetDescriptionChargeCode() {
		return setDescriptionChargeCode;
	}

	public void setSetDescriptionChargeCode(String setDescriptionChargeCode) {
		this.setDescriptionChargeCode = setDescriptionChargeCode;
	}

	public String getSetDefaultDescriptionChargeCode() {
		return setDefaultDescriptionChargeCode;
	}

	public void setSetDefaultDescriptionChargeCode(
			String setDefaultDescriptionChargeCode) {
		this.setDefaultDescriptionChargeCode = setDefaultDescriptionChargeCode;
	}

	public String getActualDiscountFlag() {
		return actualDiscountFlag;
	}

	public void setActualDiscountFlag(String actualDiscountFlag) {
		this.actualDiscountFlag = actualDiscountFlag;
	}

	public String getRevisionDiscountFlag() {
		return revisionDiscountFlag;
	}

	public void setRevisionDiscountFlag(String revisionDiscountFlag) {
		this.revisionDiscountFlag = revisionDiscountFlag;
	}

	public AccountLine getSynchedAccountLine() {
		return synchedAccountLine;
	}

	public void setSynchedAccountLine(AccountLine synchedAccountLine) {
		this.synchedAccountLine = synchedAccountLine;
	}

	public String getWeightType() {
		return weightType;
	}

	public void setWeightType(String weightType) {
		this.weightType = weightType;
	}

	public Map<String, String> getUTSIpayVatPercentList() {
		return UTSIpayVatPercentList;
	}

	public void setUTSIpayVatPercentList(Map<String, String> uTSIpayVatPercentList) {
		UTSIpayVatPercentList = uTSIpayVatPercentList;
	}

	public Map<String, String> getUTSIeuVatPercentList() {
		return UTSIeuVatPercentList;
	}

	public void setUTSIeuVatPercentList(Map<String, String> uTSIeuVatPercentList) {
		UTSIeuVatPercentList = uTSIeuVatPercentList;
	}

	public Map<String, String> getUTSIpayVatList() {
		return UTSIpayVatList;
	}

	public void setUTSIpayVatList(Map<String, String> uTSIpayVatList) {
		UTSIpayVatList = uTSIpayVatList;
	}

	public ServiceOrder getServiceOrderRemote() {
		return serviceOrderRemote;
	}

	public void setServiceOrderRemote(ServiceOrder serviceOrderRemote) {
		this.serviceOrderRemote = serviceOrderRemote;
	}

	public String getShipSize() {
		return shipSize;
	}

	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}

	public String getMinShip() {
		return minShip;
	}

	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}

	public String getCountShip() {
		return countShip;
	}

	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}

	public boolean isSingleCompanyDivisionInvoicing() {
		return singleCompanyDivisionInvoicing;
	}

	public void setSingleCompanyDivisionInvoicing(
			boolean singleCompanyDivisionInvoicing) {
		this.singleCompanyDivisionInvoicing = singleCompanyDivisionInvoicing;
	}

	public Boolean getoAdAWeightVolumeValidationForInv() {
		return oAdAWeightVolumeValidationForInv;
	}

	public void setoAdAWeightVolumeValidationForInv(
			Boolean oAdAWeightVolumeValidationForInv) {
		this.oAdAWeightVolumeValidationForInv = oAdAWeightVolumeValidationForInv;
	}

	public Boolean getSubOADAValidationForInv() {
		return subOADAValidationForInv;
	}

	public void setSubOADAValidationForInv(Boolean subOADAValidationForInv) {
		this.subOADAValidationForInv = subOADAValidationForInv;
	}

	public List getOwnBookingAgentCode() {
		return ownBookingAgentCode;
	}

	public void setOwnBookingAgentCode(List ownBookingAgentCode) {
		this.ownBookingAgentCode = ownBookingAgentCode;
	}

	public Boolean getOwnBookingAgentFlag() {
		return ownBookingAgentFlag;
	}

	public void setOwnBookingAgentFlag(Boolean ownBookingAgentFlag) {
		this.ownBookingAgentFlag = ownBookingAgentFlag;
	}

	public String getButtonType() {
		return buttonType;
	}

	public void setButtonType(String buttonType) {
		this.buttonType = buttonType;
	}

	public String getCompanyDivisionForTicket() {
		return companyDivisionForTicket;
	}

	public void setCompanyDivisionForTicket(String companyDivisionForTicket) {
		this.companyDivisionForTicket = companyDivisionForTicket;
	}

	public String getBillToCodeForTicket() {
		return billToCodeForTicket;
	}

	public void setBillToCodeForTicket(String billToCodeForTicket) {
		this.billToCodeForTicket = billToCodeForTicket;
	}

	public String getEmptyList() {
		return emptyList;
	}

	public void setEmptyList(String emptyList) {
		this.emptyList = emptyList;
	}

	public String getAccCorpID() {
		return accCorpID;
	}

	public void setAccCorpID(String accCorpID) {
		this.accCorpID = accCorpID;
	}

	public String getSalesCommisionRate() {
		return salesCommisionRate;
	}

	public void setSalesCommisionRate(String salesCommisionRate) {
		this.salesCommisionRate = salesCommisionRate;
	}

	public String getGrossMarginThreshold() {
		return grossMarginThreshold;
	}

	public void setGrossMarginThreshold(String grossMarginThreshold) {
		this.grossMarginThreshold = grossMarginThreshold;
	}

	public Date getAccrualDate() {
		return accrualDate;
	}

	public void setAccrualDate(Date accrualDate) {
		this.accrualDate = accrualDate;
	}

	public String getRoleSupervisor() {
		return roleSupervisor;
	}

	public void setRoleSupervisor(String roleSupervisor) {
		this.roleSupervisor = roleSupervisor;
	}

	public String getSystemDefaultVatCalculation() {
		return systemDefaultVatCalculation;
	}

	public void setSystemDefaultVatCalculation(String systemDefaultVatCalculation) {
		this.systemDefaultVatCalculation = systemDefaultVatCalculation;
	}

	public Map<String, String> getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(Map<String, String> paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public boolean isAccNonEditFlag() {
		return accNonEditFlag;
	}

	public void setAccNonEditFlag(boolean accNonEditFlag) {
		this.accNonEditFlag = accNonEditFlag;
	}

	public Boolean getVisibilityForReceivableDetail() {
		return visibilityForReceivableDetail;
	}

	public void setVisibilityForReceivableDetail(
			Boolean visibilityForReceivableDetail) {
		this.visibilityForReceivableDetail = visibilityForReceivableDetail;
	}

	public Boolean getReceivableSectionDetail() {
		return receivableSectionDetail;
	}

	public void setReceivableSectionDetail(Boolean receivableSectionDetail) {
		this.receivableSectionDetail = receivableSectionDetail;
	}

	public String getUserRoleExecutive() {
		return userRoleExecutive;
	}

	public void setUserRoleExecutive(String userRoleExecutive) {
		this.userRoleExecutive = userRoleExecutive;
	}

	public String getSystemDefaultstorage() {
		return systemDefaultstorage;
	}

	public void setSystemDefaultstorage(String systemDefaultstorage) {
		this.systemDefaultstorage = systemDefaultstorage;
	}

	public Long getAid() {
		return aid;
	}

	public void setAid(Long aid) {
		this.aid = aid;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Map<String, String> getMyfileDocumentList() {
		return myfileDocumentList;
	}

	public void setMyfileDocumentList(Map<String, String> myfileDocumentList) {
		this.myfileDocumentList = myfileDocumentList;
	}

	public List getGetCommissionList() {
		return getCommissionList;
	}

	public void setGetCommissionList(List getCommissionList) {
		this.getCommissionList = getCommissionList;
	}

	public String getSetRegistrationNumber() {
		return setRegistrationNumber;
	}

	public void setSetRegistrationNumber(String setRegistrationNumber) {
		this.setRegistrationNumber = setRegistrationNumber;
	}

	public String getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(String dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public Map<String, String> getCompanyDivisionMap() {
		return companyDivisionMap;
	}

	public void setCompanyDivisionMap(Map<String, String> companyDivisionMap) {
		this.companyDivisionMap = companyDivisionMap;
	}

	public String getCompDivFlag() {
		return compDivFlag;
	}

	public void setCompDivFlag(String compDivFlag) {
		this.compDivFlag = compDivFlag;
	}

	public String getUpdateQuery() {
		return updateQuery;
	}

	public void setUpdateQuery(String updateQuery) {
		this.updateQuery = updateQuery;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isPayablesXferWithApprovalOnly() {
		return payablesXferWithApprovalOnly;
	}

	public void setPayablesXferWithApprovalOnly(boolean payablesXferWithApprovalOnly) {
		this.payablesXferWithApprovalOnly = payablesXferWithApprovalOnly;
	}

	public Boolean getAutuPopPostDate() {
		return autuPopPostDate;
	}

	public void setAutuPopPostDate(Boolean autuPopPostDate) {
		this.autuPopPostDate = autuPopPostDate;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public List getCloseCompanyDivision() {
		return closeCompanyDivision;
	}

	public void setCloseCompanyDivision(List closeCompanyDivision) {
		this.closeCompanyDivision = closeCompanyDivision;
	}

	public Map<String, String> getWriteoffReasonList() {
		return writeoffReasonList;
	}

	public void setWriteoffReasonList(Map<String, String> writeoffReasonList) {
		this.writeoffReasonList = writeoffReasonList;
	}


	public List getContractBillList() {
		return contractBillList;
	}

	public void setContractBillList(List contractBillList) {
		this.contractBillList = contractBillList;
	}

	public Map<String, String> getBillingContractMap() {
		return billingContractMap;
	}

	public void setBillingContractMap(Map<String, String> billingContractMap) {
		this.billingContractMap = billingContractMap;
	}

	public boolean isWriteOffReasonFlag() {
		return writeOffReasonFlag;
	}

	public void setWriteOffReasonFlag(boolean writeOffReasonFlag) {
		this.writeOffReasonFlag = writeOffReasonFlag;
	}

	public String getVarianceExpenseAmount() {
		return varianceExpenseAmount;
	}

	public void setVarianceExpenseAmount(String varianceExpenseAmount) {
		this.varianceExpenseAmount = varianceExpenseAmount;
	}

	public String getRevisionExpense() {
		return revisionExpense;
	}

	public void setRevisionExpense(String revisionExpense) {
		this.revisionExpense = revisionExpense;
	}

	public String getActualExpense() {
		return actualExpense;
	}

	public void setActualExpense(String actualExpense) {
		this.actualExpense = actualExpense;
	}

	public String getLocalAmount() {
		return localAmount;
	}

	public void setLocalAmount(String localAmount) {
		this.localAmount = localAmount;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getEstimateVendorName() {
		return estimateVendorName;
	}

	public void setEstimateVendorName(String estimateVendorName) {
		this.estimateVendorName = estimateVendorName;
	}

	public String getActgCode() {
		return actgCode;
	}

	public void setActgCode(String actgCode) {
		this.actgCode = actgCode;
	}

	public String getAccrualDateDel() {
		return accrualDateDel;
	}

	public void setAccrualDateDel(String accrualDateDel) {
		this.accrualDateDel = accrualDateDel;
	}

	public ServicePartner getServicePartner() {
		return servicePartner;
	}

	public void setServicePartner(ServicePartner servicePartner) {
		this.servicePartner = servicePartner;
	} 
	
	public boolean isAccrualReadyContractFlag() {
		return accrualReadyContractFlag;
	}

	public void setAccrualReadyContractFlag(boolean accrualReadyContractFlag) {
		this.accrualReadyContractFlag = accrualReadyContractFlag;
	} 

	public Double getMinMargin() {
		return minMargin;
	}

	public void setMinMargin(Double minMargin) {
		this.minMargin = minMargin;
	}

	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	public String getAccountLineQuery() {
		return accountLineQuery;
	}

	public void setAccountLineQuery(String accountLineQuery) {
		this.accountLineQuery = accountLineQuery;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public List getFieldList() {
		return fieldList;
	}

	public void setFieldList(List fieldList) {
		this.fieldList = fieldList;
	}

	public Map<String, String> getFieldNameList() {
		return fieldNameList;
	}

	public void setFieldNameList(Map<String, String> fieldNameList) {
		this.fieldNameList = fieldNameList;
	}

	public boolean isVanlineSettleColourStatus() {
		return vanlineSettleColourStatus;
	}

	public void setVanlineSettleColourStatus(boolean vanlineSettleColourStatus) {
		this.vanlineSettleColourStatus = vanlineSettleColourStatus;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getAccountLineSortValue() {
		return accountLineSortValue;
	}

	public void setAccountLineSortValue(String accountLineSortValue) {
		this.accountLineSortValue = accountLineSortValue;
	}

	public String getAccountLineOrderByVal() {
		return accountLineOrderByVal;
	}

	public void setAccountLineOrderByVal(String accountLineOrderByVal) {
		this.accountLineOrderByVal = accountLineOrderByVal;
	}

	public List getGetGlTypeList() {
		return getGlTypeList;
	}

	public void setGetGlTypeList(List getGlTypeList) {
		this.getGlTypeList = getGlTypeList;
	}

	public Boolean getDriverCommissionSetUp() {
		return driverCommissionSetUp;
	}

	public void setDriverCommissionSetUp(Boolean driverCommissionSetUp) {
		this.driverCommissionSetUp = driverCommissionSetUp;
	}

	public AccountLine getAccountLineOld() {
		return accountLineOld;
	}

	public void setAccountLineOld(AccountLine accountLineOld) {
		this.accountLineOld = accountLineOld;
	}

	public String getFieldValues() {
		return fieldValues;
	}

	public void setFieldValues(String fieldValues) {
		this.fieldValues = fieldValues;
	}

	public String getWriteOffReasonValue() {
		return writeOffReasonValue;
	}

	public void setWriteOffReasonValue(String writeOffReasonValue) {
		this.writeOffReasonValue = writeOffReasonValue;
	}

	public String getClickType() {
		return clickType;
	}

	public void setClickType(String clickType) {
		this.clickType = clickType;
	}
	
	public String getPayableDataList() {
		return payableDataList;
	}

	public void setPayableDataList(String payableDataList) {
		this.payableDataList = payableDataList;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}
	
}
