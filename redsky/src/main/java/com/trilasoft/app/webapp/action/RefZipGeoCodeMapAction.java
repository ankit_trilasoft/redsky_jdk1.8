package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.RefZipGeoCodeMap;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.RefZipGeoCodeMapManager;

public class RefZipGeoCodeMapAction extends BaseAction implements Preparable{
	private RefZipGeoCodeMapManager refZipGeoCodeMapManager;
	private CustomerFileManager  customerFileManager;
	private List refZipGeoCodeMaps;
	private RefZipGeoCodeMap refZipGeoCodeMap;
	private Long id;
	private String sessionCorpID;
	private RefMasterManager refMasterManager;
	private static List mode;
	private Map<String, String> ostates;
	private Map<String, String> ocountry;
	private List portList;
	private List countryCode;
	private String zipCode;
	private String city;
	private String state;
	private String country;
	private String modeType;
	private String pMode;
	private String hitFlag="";
	private String Status;
	private String detailPage;
	private List portNameList;
	private String primaryRecordValue;
	private String primaryRecordValueValidate;
	private String enbState;
    private Map<String, String> countryCod;
    
    public void prepare() throws Exception {
		enbState = customerFileManager.enableStateList1(sessionCorpID);
	}

	public RefZipGeoCodeMapAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	
	public List getRefZipGeoCodeMaps() {
		return refZipGeoCodeMaps;
	}

	public String list() {
		refZipGeoCodeMaps = refZipGeoCodeMapManager.refZipGeoCodeMapByCorpId(sessionCorpID);
		return SUCCESS;
	}
	/*public String portModelist() {
		portList = portManager.findPortByMode(pMode);
		return SUCCESS;
	}*/
	public void setId(Long id) {
		this.id = id;
	}
	@SkipValidation
    public String searchRefZipGeoCodeMap() {
		refZipGeoCodeMaps=refZipGeoCodeMapManager.searchByRefZipGeoCodeMap(zipCode, city, state,country);
        if(refZipGeoCodeMaps.size()==1){
        	detailPage="true";
        }
		return SUCCESS;   
    }
	
   
    /*
	public String getComboList(String corpID){
		 ostates = refMasterManager.findByParameterPortCountry(corpID, "COUNTRY");
	 	 mode = refMasterManager.findByParameters(corpID, "MODE");
	 	 return SUCCESS;
	}*/
//method for country code
	@SkipValidation
	public String portCountryCode() {
				countryCode = refMasterManager.findCountryCode(country, sessionCorpID);
				 countryCod = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
		return SUCCESS;
	}
	
	/*@SkipValidation
	public String findPortName(){
		portNameList=portManager.getPortName(country,sessionCorpID,portCode);
		return SUCCESS;	
	}*/
	
//  ****for auto save******
    private String gotoPageString;
    private String validateFormNav;
    public String getValidateFormNav() {

        return validateFormNav;

        }

        public void setValidateFormNav(String validateFormNav) {

        this.validateFormNav = validateFormNav;

        }
    public String getGotoPageString() {

    return gotoPageString;

    }

    public void setGotoPageString(String gotoPageString) {

    this.gotoPageString = gotoPageString;

    }
    /*public String saveOnTabChange() throws Exception {
        //if (option enabled for this company in the system parameters table then call save) {
    		validateFormNav = "OK";
            String s = save();    // else simply navigate to the requested page)
            return gotoPageString;
    }*/
////////********************************************
 @SkipValidation
 public String edit() {
	    ocountry= refMasterManager.findCountryForPartner(sessionCorpID, "COUNTRY");
	    countryCod = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
		if (id != null) {
			refZipGeoCodeMap = refZipGeoCodeMapManager.get(id);
			ostates = customerFileManager.findDefaultStateList(refZipGeoCodeMap.getCountry(), sessionCorpID);
			primaryRecordValue=refZipGeoCodeMap.getPrimaryRecord();
		} else {
			refZipGeoCodeMap = new RefZipGeoCodeMap();
			//refZipGeoCodeMap.setCountry("USA");
			ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
			refZipGeoCodeMap.setCreatedOn(new Date());
			refZipGeoCodeMap.setUpdatedOn(new Date());
		}
		List isexisted=refZipGeoCodeMapManager.zipCodeExisted(refZipGeoCodeMap.getZipcode(), sessionCorpID);
		if(!isexisted.isEmpty())
		{
			primaryRecordValueValidate="vadidate";
		}
		
        //port.setUpdatedOn(new Date());
		return SUCCESS;
	}
 @SkipValidation
 public String validatePrimaryRecordThroughEdit(){
	 List isexisted=refZipGeoCodeMapManager.zipCodeExisted(zipCode, sessionCorpID);
	 if(!isexisted.isEmpty())
		{
			primaryRecordValueValidate="vadidate";
		}
 	return SUCCESS;
 }
 
 @SkipValidation
	public String save() throws Exception {
		boolean isNew = (refZipGeoCodeMap.getId() == null);
		 countryCod = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
		ostates = customerFileManager.findDefaultStateList(refZipGeoCodeMap.getCountry(), sessionCorpID);
		if(refZipGeoCodeMap.getZipcode().trim().equalsIgnoreCase("") && isNew){
			errorMessage("Postal Code is required.");
			return INPUT;
		}else{	
				if(refZipGeoCodeMap.getZipcode().trim().equalsIgnoreCase("") && !isNew){
					errorMessage("Postal Code is required.");
					return INPUT;
				}else{
				if (isNew) { 
					refZipGeoCodeMap.setCreatedOn(new Date());
		        }
				//List list =refZipGeoCodeMapManager.refZipGeoCodeMapState(refZipGeoCodeMap.getStateabbreviation());
				/*if(!list.isEmpty() && list!=null){
				refZipGeoCodeMap.setState(list.get(0).toString());
				}*/
				 //ostates = refZipGeoCodeMapManager.findByParameter(refZipGeoCodeMap.getCountry(),sessionCorpID);
				refZipGeoCodeMap.setState(refZipGeoCodeMapManager.getStateName(refZipGeoCodeMap.getStateabbreviation(),sessionCorpID, refZipGeoCodeMap.getCountry()));
				refZipGeoCodeMap.setCorpID(sessionCorpID);
				refZipGeoCodeMap.setUpdatedOn(new Date());
				refZipGeoCodeMap.setUpdatedBy(getRequest().getRemoteUser());
				refZipGeoCodeMapManager.save(refZipGeoCodeMap);
	        
	        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
			String key = (isNew) ? "refZipGeoCodeMap.added" : "refZipGeoCodeMap.updated";
			saveMessage(getText(key));
	        }
	       	if (!isNew) {
				//oldCreatedOn = port.getCreatedOn();
	       		hitFlag="1";
				return "refZipGeoCodeMapDetail";
			} else {
				Long i = Long.parseLong(refZipGeoCodeMapManager.findMaximumId().get(0).toString());
				refZipGeoCodeMap = refZipGeoCodeMapManager.get(i);
				hitFlag="1";
				return SUCCESS;
			}
				}
			}
		}
	
 @SkipValidation
public String validatePrimaryRecord(){
	 refZipGeoCodeMapManager.updatePrimaryRecord(zipCode, sessionCorpID);
	return SUCCESS;
}
	public void setRefZipGeoCodeMapManager(RefZipGeoCodeMapManager refZipGeoCodeMapManager) {
		this.refZipGeoCodeMapManager = refZipGeoCodeMapManager;
	}


	public void setRefZipGeoCodeMaps(List refZipGeoCodeMaps) {
		this.refZipGeoCodeMaps = refZipGeoCodeMaps;
	}


	public void setPort(RefZipGeoCodeMap refZipGeoCodeMap) {
		this.refZipGeoCodeMap = refZipGeoCodeMap;
	}


	public static List getMode() {
		return mode;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}


	public List getPortList() {
		return portList;
	}


	public void setPortList(List portList) {
		this.portList = portList;
	}




	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}


	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}


	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}


	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}


	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}


	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getModeType() {
		return modeType;
	}


	public void setModeType(String modeType) {
		this.modeType = modeType;
	}


	public String getPMode() {
		return pMode;
	}


	public void setPMode(String mode) {
		pMode = mode;
	}


	/**
	 * @return the hitFlag
	 */
	public String getHitFlag() {
		return hitFlag;
	}


	/**
	 * @param hitFlag the hitFlag to set
	 */
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}


	/**
	 * @return the countryCode
	 */
	public List getCountryCode() {
		return countryCode;
	}


	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(List countryCode) {
		this.countryCode = countryCode;
	}


	/**
	 * @return the status
	 */
	public String getStatus() {
		return Status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}


	public List getPortNameList() {
		return portNameList;
	}


	public void setPortNameList(List portNameList) {
		this.portNameList = portNameList;
	}


	/**
	 * @return the refZipGeoCodeMap
	 */
	public RefZipGeoCodeMap getRefZipGeoCodeMap() {
		return refZipGeoCodeMap;
	}


	/**
	 * @param refZipGeoCodeMap the refZipGeoCodeMap to set
	 */
	public void setRefZipGeoCodeMap(RefZipGeoCodeMap refZipGeoCodeMap) {
		this.refZipGeoCodeMap = refZipGeoCodeMap;
	}


	/**
	 * @return the ostates
	 */
	public Map<String, String> getOstates() {
		return ostates;
	}


	/**
	 * @return the detailPage
	 */
	public String getDetailPage() {
		return detailPage;
	}


	/**
	 * @param detailPage the detailPage to set
	 */
	public void setDetailPage(String detailPage) {
		this.detailPage = detailPage;
	}


	public String getPrimaryRecordValue() {
		return primaryRecordValue;
	}


	public void setPrimaryRecordValue(String primaryRecordValue) {
		this.primaryRecordValue = primaryRecordValue;
	}


	public String getPrimaryRecordValueValidate() {
		return primaryRecordValueValidate;
	}


	public void setPrimaryRecordValueValidate(String primaryRecordValueValidate) {
		this.primaryRecordValueValidate = primaryRecordValueValidate;
	}


	public Map<String, String> getOcountry() {
		return ocountry;
	}


	public void setOcountry(Map<String, String> ocountry) {
		this.ocountry = ocountry;
	}


	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public String getEnbState() {
		return enbState;
	}

	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}

	public Map<String, String> getCountryCod() {
		return countryCod;
	}

	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}


}
