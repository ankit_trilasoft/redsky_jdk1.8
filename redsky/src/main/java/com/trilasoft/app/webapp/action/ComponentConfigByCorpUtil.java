package com.trilasoft.app.webapp.action;

import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;

import com.trilasoft.app.model.Company;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CorpComponentPermissionManager;
import com.trilasoft.app.service.DataCatalogManager;

public class ComponentConfigByCorpUtil {
	
	private DataCatalogManager dataCatalogManager;
	
	private CorpComponentPermissionManager corpComponentPermissionManager;

	private CompanyManager companyManager;
	
	public void setCorpComponentPermissionManager(CorpComponentPermissionManager corpComponentPermissionManager) {
		this.corpComponentPermissionManager = corpComponentPermissionManager;
	}
	
	public void setDataCatalogManager(DataCatalogManager dataCatalogManager) {
		this.dataCatalogManager = dataCatalogManager;
	}
	
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	
	public boolean getComponentVisibilityAttrbute(String userCorpID, String componentId){
		if (componentId.startsWith("component.")){
			return corpComponentPermissionManager.getFieldVisibilityForComponent(userCorpID, componentId);
		}
		
		return false;
	}
	
	public boolean getFieldVisibilityAttrbute(String userCorpID, String componentId){
		return dataCatalogManager.getFieldVisibilityAttrbute(userCorpID, componentId);
	}
	
	public String getTimeZone(String userCorpID){
        Company company= companyManager.findByCorpID(userCorpID).get(0);
        return company.getTimeZone();
	}	
	
	public String getAutoSavePrompt(String userCorpID){
        Company company= companyManager.findByCorpID(userCorpID).get(0);
        return company.getAutoSavePrompt();
	}

	public Map<String, Integer> getComponentVisibilityAttrbuteDetail(String userCorpID, String componentId) {
		return corpComponentPermissionManager.getComponentVisibilityAttrbuteDetail(userCorpID, componentId);
	}
	
	

}
