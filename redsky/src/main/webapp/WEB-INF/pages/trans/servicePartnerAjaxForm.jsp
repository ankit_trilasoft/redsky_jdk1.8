<%--
/**
 * Implementation of View that contains add and edit details.
 * This file represents the basic view on "Service Partners" in Redsky.
 * @File Name	servicePartnerForm
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        1-Dec-2008
 * --%>




<%@ include file="/common/taglibs.jsp"%>   
  
<head>   
    <title><fmt:message key="servicePartnerDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='servicePartnerDetail.heading'/>"/>
    <style>
<%@ include file="/common/calenderStyle.css"%>
<%@page import="java.util.*" %>
<%@page import="java.text.*" %>
<%@page import="com.trilasoft.app.model.Company" %>
</style>
    <% 
    Company company = (Company)request.getAttribute("company");
	Date currentDateForSP = new Date();
	SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	format1.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
	String currentDateForService=format1.format(currentDateForSP);
	
	format1 = new SimpleDateFormat("yyyy-MM-dd"); 
	currentDateForSP =  format1.parse(currentDateForService);
	currentDateForService=format1.format(currentDateForSP);

	%>

<style type=text/css>
div.autocomplete {
      position:absolute;      
      background-color:white;     
      margin:0px;
      padding:0px;
      z-index:99999;
      
      
    }
    div.autocomplete ul {
      list-style-type:none;
      border-top:1px solid #219DD1;
      border-bottom:1px solid #219DD1;
      margin:0px;
      padding:0px;
    }
    div.autocomplete ul li.selected { background-color: #ffb;}
    div.autocomplete ul li {
      list-style-type:none;
      display:block;
      border-top:1px solid #dfdfdf;
      border-bottom:none;
      border-left:1px solid #219DD1;
      border-right:1px solid #219DD1;
      margin:0;
      padding:0px;
      padding-left:3px;   
      cursor:pointer;
    }
    
    div#autocomplete_choices{ width:200px !important; }
</style>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<!-- Modification closed here -->

<script type="text/javascript">
var type='';
function findPortDetails(nameId,DivId,typeTemp){
	var carrierDeparture=document.getElementById(nameId).value;
	var mode='${serviceOrder.mode}';
	if(carrierDeparture.length>1){
		type=typeTemp;
		$.get("routingPOLAutoComplete.html?ajax=1&decorator=simple&popup=true", 
				{carrierDeparture: carrierDeparture,serviceMode:mode,autocompletePortDivId:DivId,nameId:nameId},
				function(data){
					 document.getElementById(DivId).style.display = "block";
					$("#"+DivId).html(data);
			});	
	     }else{
		document.getElementById(DivId).style.display = "none";	
	    }
 }
function checkValue(code,des){
	if(type=='POL'){
		document.forms['servicePartnerForm'].elements['servicePartner.polCode'].value=code;
		document.forms['servicePartnerForm'].elements['servicePartner.carrierDeparture'].value=des;
		document.getElementById("servicePartnerCarrierDepartureDivId").style.display = "none";
	}else if(type=='POE'){
		document.forms['servicePartnerForm'].elements['servicePartner.poeCode'].value=code;
		document.forms['servicePartnerForm'].elements['servicePartner.carrierArrival'].value=des;
		document.getElementById("servicePartnerCarrierArrivalDivId").style.display = "none";
	}
	type="";
} 
 function checkBlank(){
			var polName = document.forms['servicePartnerForm'].elements['servicePartner.carrierDeparture'].value;
			polName=polName.trim();
		    if(polName=='') {
		       	 document.forms['servicePartnerForm'].elements['servicePartner.polCode'].value=''; 
		    }
		    var poeName=document.forms['servicePartnerForm'].elements['servicePartner.carrierArrival'].value;
		    poeName=poeName.trim();
		    if(poeName==''){
		    	document.forms['servicePartnerForm'].elements['servicePartner.poeCode'].value='';
		    }
 }

</script>
<script type="text/javascript"> 

// function to check the form submittion.

<sec-auth:authComponent componentId="module.script.form.agentScript">

	window.onload = function() { 
	 trap();
		}
 </sec-auth:authComponent>
function trap() 
		  {
		  
		  if(document.images)
		    {
		    
		    	for(i=0;i<document.images.length;i++)
		      {
		      	
		      	if(document.images[i].src.indexOf('nav')>0)
						{
							document.images[i].onclick= right; 
		        			document.images[i].src = 'images/navarrow.gif';  
						}
		        	 

		      }
		    }
		  }
		  
		  function right(e) {
		
		//var msg = "Sorry, you don't have permission.";
		if (navigator.appName == 'Netscape' && e.which == 1) {
		//alert(msg);
		return false;
		}
		
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
		//alert(msg);
		return false;
		}
		
		else return true;
		}

var form_submitted = false;

function submit_form()
{
  if (form_submitted)
  {
    alert ("Your form has already been submitted. Please wait...");
    return false;
  }
  else
  {
    form_submitted = true;
    return true;
  }
}

// End of function.

</script>
 <script language="javascript" type="text/javascript">
var valueatfocus;
function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
	var tim1=document.forms['servicePartnerForm'].elements['servicePartner.etArrival'].value;
	var tim2=document.forms['servicePartnerForm'].elements['servicePartner.etDepart'].value;
	if(tim1.length == 5 || tim2.length == 5 ) {
		tim1=tim1.replace(":","");
		tim2=tim2.replace(":","");
	}
	 var f = document.getElementById('servicePartnerForm'); 
	 f.setAttribute("autocomplete", "off");
	valueatfocus=document.forms['servicePartnerForm'].elements['servicePartner.etDepart'].value;
	
}
 </script>  
 
 <script>
 
// function for auto save.

function ContainerAutoSave(clickType){
progressBarAutoSave('1');
	if ('${autoSavePrompt}' == 'No'){
	var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
	    var id1 = document.forms['servicePartnerForm'].elements['serviceOrder.id'].value;
		if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.accounting'){
                noSaveAction = 'accountLineList.html?sid='+id1;
                }
if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
    noSaveAction = 'pricingList.html?sid='+id1;
    }
if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
                noSaveAction = 'containers.html?id='+id1;
                }
if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.billing'){
                noSaveAction = 'editBilling.html?id='+id1;
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.domestic'){
                noSaveAction = 'editMiscellaneous.html?id='+id1;
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.status'){
	<c:if test="${serviceOrder.job=='RLO'}">
	noSaveAction = 'editDspDetails.html?id='+id1; 
   </c:if>
   <c:if test="${serviceOrder.job!='RLO'}">
	noSaveAction =  'editTrackingStatus.html?id='+id1;
    </c:if>
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.ticket'){
                noSaveAction = 'customerWorkTickets.html?id='+id1;
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.claims'){
                noSaveAction = 'claims.html?id='+id1;
                }
                if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.cartons'){
                noSaveAction = 'cartons.html?id='+id1;
                }
                if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.vehicles'){
                noSaveAction = 'vehicles.html?id='+id1;
                }
                if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.servicepartners'){
                noSaveAction = 'servicePartnerss.html?id='+id1;
                }
                if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.containers'){
                noSaveAction = 'containers.html?id='+id1;
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				var cidVal='${customerFile.id}';
                noSaveAction = 'editCustomerFile.html?id='+cidVal;
                }
                processAutoSave(document.forms['servicePartnerForm'], 'saveServicePartner!saveOnTabChange.html', noSaveAction);
      	}
	else{

    if(!(clickType == 'save')){
    var id1 = document.forms['servicePartnerForm'].elements['serviceOrder.id'].value;
    var jobNumber = document.forms['servicePartnerForm'].elements['serviceOrder.shipNumber'].value;

    if (document.forms['servicePartnerForm'].elements['formStatus'].value == '1'){
        var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='servicePartnerDetail.heading'/>");
        if(agree){
            document.forms['servicePartnerForm'].action = 'saveServicePartner!saveOnTabChange.html';
            document.forms['servicePartnerForm'].submit();
        }else{
            if(id1 != ''){

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                location.href = 'editServiceOrderUpdate.html?id='+id1;
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.accounting'){
                location.href = 'accountLineList.html?sid='+id1;
                }
if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
    location.href = 'pricingList.html?sid='+id1;
    }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.billing'){
                location.href = 'editBilling.html?id='+id1;
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.domestic'){
                location.href = 'editMiscellaneous.html?id='+id1;
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.status'){
	<c:if test="${serviceOrder.job=='RLO'}">
	location.href = 'editDspDetails.html?id='+id1; 
   </c:if>
   <c:if test="${serviceOrder.job!='RLO'}">
   location.href =  'editTrackingStatus.html?id='+id1;
    </c:if>
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.ticket'){
                location.href = 'customerWorkTickets.html?id='+id1;
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.claims'){
                location.href = 'claims.html?id='+id1;
                }
                if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.cartons'){
                location.href = 'cartons.html?id='+id1;
                }
                if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.vehicles'){
                location.href = 'vehicles.html?id='+id1;
                }
                if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.servicepartners'){
                location.href = 'servicePartnerss.html?id='+id1;
                }
                if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.containers'){
                location.href = 'containers.html?id='+id1;
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				var cidVal='${customerFile.id}';
                location.href = 'editCustomerFile.html?id='+cidVal;
                }
        }
        }
    }else{
    if(id1 != ''){

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                location.href = 'editServiceOrderUpdate.html?id='+id1;
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.accounting'){
                location.href = 'accountLineList.html?sid='+id1;
                }
if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
    location.href = 'pricingList.html?sid='+id1;
    }


if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.billing'){
                location.href = 'editBilling.html?id='+id1;
                }
if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.domestic'){
                location.href = 'editMiscellaneous.html?id='+id1;
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.status'){
	<c:if test="${serviceOrder.job=='RLO'}">
	location.href = 'editDspDetails.html?id='+id1; 
   </c:if>
   <c:if test="${serviceOrder.job!='RLO'}">
   location.href =  'editTrackingStatus.html?id='+id1;
    </c:if>
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.ticket'){
                location.href = 'customerWorkTickets.html?id='+id1;
                }

if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.claims'){
                location.href = 'claims.html?id='+id1;
                }
if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.servicepartners'){
                location.href = 'servicePartnerss.html?id='+id1;
                }
                if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.vehicles'){
                location.href = 'vehicles.html?id='+id1;
                }
                if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.cartons'){
                location.href = 'cartons.html?id='+id1;
                }
                if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.containers'){
                location.href = 'containers.html?id='+id1;
                }
if(document.forms['servicePartnerForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				var cidVal='${customerFile.id}';
                location.href = 'editCustomerFile.html?id='+cidVal;
                }
    }
    }
}
}
}

// End of function.

// function to check the status for auto save.

function changeStatus(){
    document.forms['servicePartnerForm'].elements['formStatus'].value = '1';
}
// End of function.
</script>
</script>  
<script language="javascript" type="text/javascript">
var valueafterchange;
function test1()
{
valueafterchange=document.forms['servicePartnerForm'].elements['servicePartner.etDepart'].value;
}
function test2()
{
valueafterchange=document.forms['servicePartnerForm'].elements['servicePartner.etArrival'].value;
}  
</script>
<script type="text/javascript">

// function to find the polCode.

    function findPortName(){
    var portCode = document.forms['servicePartnerForm'].elements['servicePartner.polCode'].value;
    portCode=portCode.trim();
    if(portCode=='')
    {
       	 document.forms['servicePartnerForm'].elements['servicePartner.carrierDeparture'].value=''; 
    }
    if(portCode!=''){
    	document.forms['servicePartnerForm'].elements['polFlag'].value="N";
    var url="portName.html?ajax=1&decorator=simple&popup=true&portCode=" + encodeURI(portCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4;
     http2.send(null);
    } else{
    	document.forms['servicePartnerForm'].elements['polFlag'].value='';
    }       
}

function handleHttpResponse4(){
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                document.forms['servicePartnerForm'].elements['polFlag'].value='';
                if(results.length>=1)
                {                	
                	document.forms['servicePartnerForm'].elements['servicePartner.carrierDeparture'].value = results;
 				}else{  
 	 				alert('Port Code entered not valid, please click on the magnifying glass to select port code.');
 	 				document.forms['servicePartnerForm'].elements['servicePartner.carrierDeparture'].value='';
 	 				document.forms['servicePartnerForm'].elements['servicePartner.polCode'].value='';
 	 				return false;
                 }
             }
        }

  // End of function.
// function to check vendor name for Agent.

function checkCorpVendorName(){
	var vendorId = document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value;
    	if(vendorId == ''){
		document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value="";
	}
	if(vendorId != ''){
		document.forms['servicePartnerForm'].elements['carFlag'].value="N";		
    var url="checkVendorNameForAgent.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId)+"&soCorpID=${serviceOrder.corpID}&pType=CR";
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse23;
     http3.send(null);
     progressBarAutoSave('1');
 	}else{
 		document.forms['servicePartnerForm'].elements['carFlag'].value='';
 	} 
} 

function handleHttpResponse23(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                document.forms['servicePartnerForm'].elements['carFlag'].value='';                
                var res = results.split("#"); 
               // alert(res)               
		           if(res.size() >= 2){ 
		        	   
		           		if(res[2] == 'Approved'){
		           			document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value = res[1];
		           			document.forms['servicePartnerForm'].elements['trackingUrl'].value = res[5];
		           			document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].select();
		           		 	progressBarAutoSave('0');
		           			validateListFroCarrier();
		           		  		}else{
		           			alert("Carrier code is not approved" ); 
						    document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value="";
						    document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value="";
						    document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].select();
						    progressBarAutoSave('0');
		           		}
                }else{
                     alert("Carrier code not valid" );
                     document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value="";
                     document.forms['servicePartnerForm'].elements['trackingUrl'].value = "";
					 document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value="";
					 document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].select();
					 progressBarAutoSave('0');
			   }
     }
}      
// End of function.
    
// function to check vendor name.

function checkVendorName(){
	var vendorId = document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value;
    	if(vendorId == ''){
		document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value="";
	}
	if(vendorId != ''){
		document.forms['servicePartnerForm'].elements['carFlag'].value="N";		
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId)+"&pType=CR";
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
     progressBarAutoSave('1');
 	}else{
 		document.forms['servicePartnerForm'].elements['carFlag'].value='';
 	} 
} 

function handleHttpResponse2(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                document.forms['servicePartnerForm'].elements['carFlag'].value='';                
                var res = results.split("#");                
		           if(res.size() >= 2){ 
		        	   
		           		if(res[2] == 'Approved'){
		           			document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value = res[1];
		           			document.forms['servicePartnerForm'].elements['trackingUrl'].value = res[5];
		           			document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].select();
		           		 	progressBarAutoSave('0');
		           			validateListFroCarrier();
		           		  		}else{
		           			alert("Carrier code is not approved" ); 
						    document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value="";
						    document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value="";
						    document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].select();
						    progressBarAutoSave('0');
		           		}
                }else{
                     alert("Carrier code not valid" );
                     document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value="";
                     document.forms['servicePartnerForm'].elements['trackingUrl'].value = "";
					 document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value="";
					 document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].select();
					 progressBarAutoSave('0');
			   }
     }
}      
// End of function.

//Validate expiration for Carrier Code

function validateListFroCarrier(){ 
 if(document.forms['servicePartnerForm'].elements['validateFlagTrailer'].value=='OK'){
        var parentId=document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value;
        var deliveryLastDay=document.forms['servicePartnerForm'].elements['deliveryLastDay'].value;                
	    var url="controlExpirationsListInDomestic.html?ajax=1&decorator=simple&popup=true&parentId=" + encodeURI(parentId)+"&deliveryLastDay=" + encodeURI(deliveryLastDay);
	    httpValidate.open("GET", url, true);
	    httpValidate.onreadystatechange = handleHttpResponse;
	    httpValidate.send(null);	    
	      }
}

function handleHttpResponse(){
		if (httpValidate.readyState == 4){
                var results = httpValidate.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                               
                if(res !=''){        
                var totalElement= res.indexOf(',');                
                if(totalElement>0)
                {
                var res = res.split(",");
                var res11 = res[0].split("#");
                var res22 = res[1].split("#");  
                var res33 = "";
                if(res[2] != undefined)
                {
                	res33 = res[2].split("#");
                }
                var ExpiryFor = "";
                 if(res11[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res11[1]+":   "+res11[2];
                 }
                 if(res22[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res22[1]+":   "+res22[2];
                 }
                 if(res33[1] != "" &&  res33[1] != undefined)
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res33[1]+":   "+res33[2];
                 }
                if(res11[0] =='Yes' || res22[0].trim() =='Yes')
                 {
                 alert("This Carrier Code cannot be selected due to the folllowing expirations: "+ExpiryFor);
                 document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value="";
                 document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value="";
			     document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].select();
                 }else{
                 var agree =confirm("This Carrier Code has the following expirations: "+ExpiryFor+"\nDo you wish to proceed?");                 
                 if(agree) 
			     {
			     } 
			     else 
			     { 
			         document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value="";	
			         document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value="";		        
			         document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].select();
			         
			     }
                 }
                 } 
                 else{
                 var res = res.split("#");
                 var ExpiryFor = ""; 
                 ExpiryFor = ExpiryFor+"\n"+res[1]+":   "+res[2];               
                 if(res[0] =='Yes')
                 {
                 alert("This Carrier Code cannot be selected due to the expirations: "+ExpiryFor);
                 document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value="";	
                 document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value="";		     
			     document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].select();			     
                 }else{                 
                 var agree =confirm("This Carrier Code has expirations: "+ExpiryFor+"\nDo you wish to proceed?");
			     if(agree) 
			     {			     
			     } 
			     else 
			     { 
			         document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value="";
			         document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value="";
			         document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].select();			         
			     }
                 }
                 }
                 }               
      }
      document.forms['servicePartnerForm'].elements['validateFlagTrailer'].value='';
      
      }
 
 function okValidateListFromTrailer(){
document.forms['servicePartnerForm'].elements['validateFlagTrailer'].value='OK';
}
 
      
//End of method.      

function checkVendorForTrackingUrl(){

    var vendorId = document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value;
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2009;
     http2.send(null);
      } 

function handleHttpResponse2009(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();                
                var res = results.split("#");
		           if(res.size() >= 2){ 
		           		if(res[5] != undefined && res[5] != ''){
		           			document.forms['servicePartnerForm'].elements['trackingUrl'].value = res[5];		           			
		           			var url="";
		           			if(res[5].indexOf("http")==-1)
		           			{
		           				url = "http://"+res[5]
		           			}
		           			else
		           			{
		           				url=res[5];
		           			}
		           			
		           			window.open(url);
                }
                else{
                alert("Please select tracking url from partner.");
                }
                }
                }
           
     }
     
// End of function.

// function to check and populate the actg code.

function vendorCodeForActgCode(){
    var vendorId = document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value;
	if(vendorId == ''){ 
		document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value = "";
	}
    if(vendorId!=''){
    var url="vendorNameForActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse222;
     http2.send(null);
    }
} 
function handleHttpResponse222(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.size() >= 2){
	                if(res[3] == 'Approved'){
	                if(res[2] != "null" && res[2] !=''){
	                   document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value = res[1];
	                   document.forms['servicePartnerForm'].elements['servicePartner.actgCode'].value = res[2]; 
	                }  
	                else {
	                    alert("Carrier code Approved but missing Accounting Code" );
	                    document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value=""; 
	           			document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value = "";
	                	document.forms['servicePartnerForm'].elements['servicePartner.actgCode'].value = ""; 
	                }
	           		}else{
	           			alert("Carrier code is not approved" ); 
					    document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value="";
						document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value="";
						document.forms['servicePartnerForm'].elements['servicePartner.actgCode'].value="";
						document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].select();
	           		}
          		}else{
                  alert("Carrier code not valid" ); 
				  document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value="";
				  document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value="";
				  document.forms['servicePartnerForm'].elements['servicePartner.actgCode'].value="";
				  document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].select();
				  return false;
			   }
       }
}

// End of function.


// function to check VendorNameValidation.


function checkVendorNameValidation()
{
    var vendorId =document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value;
    if(vendorId == ''){ 
		document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value = "";
	}
    if(vendorId!=''){
    	var url="vendorNameForActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse8;
     	http2.send(null);
     }
}


function handleHttpResponse8(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.size() >= 2){
	                if(res[3] == 'Approved'){
	           			document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value = res[1];
	                	document.forms['servicePartnerForm'].elements['servicePartner.actgCode'].value = res[2]; 
	           		}else{
	           			alert("Carrier code is not approved" ); 
					    document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value="";
						document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value="";
						document.forms['servicePartnerForm'].elements['servicePartner.actgCode'].value="";
						document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].select();
	           		}
          		}else{
                  alert("Carrier code not valid" ); 
				  document.forms['servicePartnerForm'].elements['servicePartner.carrierName'].value="";
				  document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value="";
				  document.forms['servicePartnerForm'].elements['servicePartner.actgCode'].value="";
				  document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].select();
				  return false;
			   }
             }
        }
        
// End of function.


// function to find the poeCode.


function findPortNamePoe(){
	var portCode = document.forms['servicePartnerForm'].elements['servicePartner.poeCode'].value;
	portCode=portCode.trim();
    if(portCode=='')
    {
       	 document.forms['servicePartnerForm'].elements['servicePartner.carrierArrival'].value=''; 
    }
    if(portCode!=''){
    	document.forms['servicePartnerForm'].elements['poeFlag'].value="N";	
    var url="portName.html?ajax=1&decorator=simple&popup=true&portCode=" + encodeURI(portCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse5;
     http2.send(null);     
	}else{
		document.forms['servicePartnerForm'].elements['poeFlag'].value='';
	}
}
function handleHttpResponse5()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                document.forms['servicePartnerForm'].elements['poeFlag'].value='';
                if(results.length>=1){
 					document.forms['servicePartnerForm'].elements['servicePartner.carrierArrival'].value = results;                    	
 				}else{
 					alert('Port Code entered not valid, please click on the magnifying glass to select port code.');
 	 				document.forms['servicePartnerForm'].elements['servicePartner.carrierArrival'].value='';
 	 				document.forms['servicePartnerForm'].elements['servicePartner.poeCode'].value='';
 	 				return false;
                 }
             }
        }
        
// End of function.


function copyATD(){
    var carrierATD=document.forms['servicePartnerForm'].elements['servicePartner.atDepart'].value;	
	var shipNumber = document.forms['servicePartnerForm'].elements['serviceOrder.shipNumber'].value;
	var carrierATA1=document.forms['servicePartnerForm'].elements['servicePartner.atArrival'].value;	
    var carrierATD1=document.forms['servicePartnerForm'].elements['servicePartner.atDepart'].value;
	
	var mySplitResult = carrierATD1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = carrierATA1.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  carrierATD1 = finalDate.split("-");
  carrierATA1 = finalDate2.split("-");
  var sDate = new Date(carrierATD1[0]+"/"+carrierATD1[1]+"/"+carrierATD1[2]);
  var eDate = new Date(carrierATA1[0]+"/"+carrierATA1[1]+"/"+carrierATA1[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);
	var agree = confirm("Do you want to Copy Actual Departure? Press OK for YES or Cancel for NO.");
			if(agree)
			{
			if(daysApart>0){
			alert("Actual date of arrival should be greater or equal to Actual date of departure.");
			}else{
			var url="copyCarrierAtd.html?ajax=1&decorator=simple&popup=true&carrierATD=" + encodeURI(carrierATD)+"&shipNumber="+ encodeURI(shipNumber);
		     http22.open("GET", url, true);
             http22.onreadystatechange = handleHttpResponse111;
             http22.send(null);
			}
			}
		}	
function copyATA(){
    var carrierATA=document.forms['servicePartnerForm'].elements['servicePartner.atArrival'].value;	
    var carrierATD=document.forms['servicePartnerForm'].elements['servicePartner.atDepart'].value;
    var shipNumber = document.forms['servicePartnerForm'].elements['serviceOrder.shipNumber'].value;
    var carrierATA1=document.forms['servicePartnerForm'].elements['servicePartner.atArrival'].value;	
    var carrierATD1=document.forms['servicePartnerForm'].elements['servicePartner.atDepart'].value;
	
	var mySplitResult = carrierATD1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = carrierATA1.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  carrierATD1 = finalDate.split("-");
  carrierATA1 = finalDate2.split("-");
  var sDate = new Date(carrierATD1[0]+"/"+carrierATD1[1]+"/"+carrierATD1[2]);
  var eDate = new Date(carrierATA1[0]+"/"+carrierATA1[1]+"/"+carrierATA1[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);
	var agree = confirm("Do you want to Copy Actual Arrival? Press OK for YES or Cancel for NO.");
			if(agree)
			{
			if(daysApart>0){
			alert("Actual date of arrival should be greater or equal to Actual date of departure.");
			}else{
			var url="copyCarrierAta.html?ajax=1&decorator=simple&popup=true&carrierATA=" + encodeURI(carrierATA)+"&shipNumber="+ encodeURI(shipNumber);
		     http22.open("GET", url, true);
             http22.onreadystatechange = handleHttpResponse222;
             http22.send(null);
			}
			}
			}
			

function handleHttpResponse111(){
      if (http22.readyState == 4){
                 var result= http22.responseText
                 result=result.trim();
                 if(result>0){
                 alert('Actual Departure has been copied successfully.')
                 }
                 else
                 {
                 alert('No record found to copy Actual Departure.')
                 }
      }
}	

function handleHttpResponse222(){
      if (http22.readyState == 4){
                 var result= http22.responseText
                 result=result.trim();
                 if(result>0){
                 alert('Actual Arrival has been copied successfully.')
                 }
                 else
                 {
                 alert('No record found to copy Actual Arrival.')
                 }
      }
}	

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

        
        function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject();
    var http2 = getHTTPObject();
    var http5 = getHTTPObject();
    var http22 = getHTTPObject();
var httpValidate = getHTTPObjectValidate();
function getHTTPObjectValidate(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}       
</script>
<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==35) || (keyCode==36); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
	}	
	

	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
	function onlyPhoneNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==190) || (keyCode==189) ; 
	}
</script>

<script type="text/javascript">
	function autoFilter_PartnerType(targetElement) {
		var partnerType=targetElement.options[targetElement.selectedIndex].value;
		document.forms['servicePartnerForm'].submit();
	}
function notExists(){
	alert("The Routing information has not been saved yet.");
}
	
	
	  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['servicePartnerForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['servicePartnerForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['servicePartnerForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['servicePartnerForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'servicePartnerss.html?id='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.forms['servicePartnerForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['servicePartnerForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  }   
function goToUrl(id)
	{
		location.href = "servicePartnerss.html?id="+id;
	}
	
	
	
function goPrevChild() {
	progressBarAutoSave('1');
	var sidNum =document.forms['servicePartnerForm'].elements['serviceOrder.id'].value;
	var soIdNum =document.forms['servicePartnerForm'].elements['servicePartner.id'].value;
	var url="servicePartnerPrev.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
     http5.send(null); 
   }
   
 function goNextChild() {
	progressBarAutoSave('1');
	var sidNum =document.forms['servicePartnerForm'].elements['serviceOrder.id'].value;
	var soIdNum =document.forms['servicePartnerForm'].elements['servicePartner.id'].value;
	var url="servicePartnerNext.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
     http5.send(null); 
   }
   
 function handleHttpResponseOtherShipChild(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'editServicePartner.html?id='+results;
             }
       }     
function findCustomerOtherSOChild(position) {
 var sidNum=document.forms['servicePartnerForm'].elements['serviceOrder.id'].value;
 var soIdNum=document.forms['servicePartnerForm'].elements['servicePartner.id'].value;
 var url="servicePartnerSO.html?ajax=1&decorator=simple&popup=true&sidNum=" + encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  }   
function goToUrlChild(id)
	{
		location.href = "editServicePartner.html?id="+id;
	}
	
function trackingUrlLink()
{
var trackingUrl=document.forms['servicePartnerForm'].elements['trackingUrl'].value;
var carrierCode=document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value;
if(carrierCode !=''){
if(trackingUrl==''){
alert("Please select tracking url from partner.");
}else{
//alert("in>>>>trackingUrl"+trackingUrl);
window.open(trackingUrl);
}
}
else{
alert("Please select Code.");
}
}

function checkOne(targetElement){
var clickName = targetElement.name;
var clickValue = targetElement.value;
	if(clickName == 'servicePartner.transhipped' && clickValue == 'true'){
		if(document.forms['servicePartnerForm'].elements['servicePartner.clone'].checked == true){
			alert('Please select any one of them either T-Ship or Clone');
			document.forms['servicePartnerForm'].elements['servicePartner.transhipped'].checked = false;
		}
	}
	if(clickName == 'servicePartner.clone' && clickValue == 'true'){
		if(document.forms['servicePartnerForm'].elements['servicePartner.transhipped'].checked == true){
			alert('Please select any one of them either T-Ship or Clone');
			document.forms['servicePartnerForm'].elements['servicePartner.clone'].checked = false;
		}
	}
} 

function displayField(){
	if(document.forms['servicePartnerForm'].elements['servicePartner.clone'].checked == true){
			document.getElementById("disDiv1").style.display = "block";
	}else{
			document.getElementById("disDiv1").style.display = "none";
	}	
}

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    return true;
}
function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
	document.getElementById(autocompleteDivId).style.display = "none";
	if(document.getElementById(paertnerCodeId).value==''){
		document.getElementById(partnerNameId).value="";	
	}
	}

	function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
		document.getElementById(partnerNameId).value=lastName;
		document.getElementById(paertnerCodeId).value=partnercode;
		document.getElementById(autocompleteDivId).style.display = "none";	
		if(paertnerCodeId=='servicePartnerCarrierCodeId'){
			checkVendorName();
		}
	}
</script>

<script language="javascript" type="text/javascript">
		function confirmUpdate(){
		var agree=confirm("Would you like to update the same information for all service orders for this groupage order?  Click Ok to confirm or Cancel to update information for this order only.");
		if(agree){
	
		document.forms['servicePartnerForm'].elements['updateRecords'].value='updateAll';
		}
		else{
		document.forms['servicePartnerForm'].elements['updateRecords'].value='updateCurrent';
		}
		}

		function confirmUpdate1(){
			document.forms['servicePartnerForm'].elements['updateRecords'].value='updateCurrent';
		}		
		
		
		function confirmUpdateByChild(){
         alert("Update is not allowed as this service order is a part of a groupage order.");
         return false;
		}
	
	function checkCodeBlank(){
		var str="0";
		<configByCorp:fieldVisibility componentId="component.forward.ContainerRouting.fieldMandatory">
		str="1";
		</configByCorp:fieldVisibility>
		var carCode= document.forms['servicePartnerForm'].elements['servicePartner.carrierCode'].value;
		var carArrival= document.forms['servicePartnerForm'].elements['servicePartner.carrierArrival'].value;
		var poeCode= document.forms['servicePartnerForm'].elements['servicePartner.poeCode'].value;
		var carDeparture= document.forms['servicePartnerForm'].elements['servicePartner.carrierDeparture'].value;
		var polCode= document.forms['servicePartnerForm'].elements['servicePartner.polCode'].value;
		var carFlag= document.forms['servicePartnerForm'].elements['carFlag'].value;
		var poeFlag= document.forms['servicePartnerForm'].elements['poeFlag'].value;
		var polFlag= document.forms['servicePartnerForm'].elements['polFlag'].value;
		var contractStr = document.forms['servicePartnerForm'].elements['servicePartner.omni'].value;
		var blNumberVal = document.forms['servicePartnerForm'].elements['servicePartner.blNumber'].value;
		var hblNumberVal = document.forms['servicePartnerForm'].elements['servicePartner.hblNumber'].value;
		var bookNumberVal = document.forms['servicePartnerForm'].elements['servicePartner.bookNumber'].value;
		var carrierVesselsVal = document.forms['servicePartnerForm'].elements['servicePartner.carrierVessels'].value;
		
		var estDepart = document.forms['servicePartnerForm'].elements['servicePartner.etDepart'].value;
		var estArv = document.forms['servicePartnerForm'].elements['servicePartner.etArrival'].value;
		
		var actDepart = document.forms['servicePartnerForm'].elements['servicePartner.atDepart'].value;		
		var actArv = document.forms['servicePartnerForm'].elements['servicePartner.atArrival'].value;
		var routingVal="${serviceOrder.routing}";
	 	   var day = '';
		   var month = '';
		   var year = '';
		   var finalDate ='';
		   var eDate = null;
	    	if(actArv!='' && actArv.indexOf('-')>-1){
				 var mySplitResult = actArv.split("-");
		  	    day = mySplitResult[0];
		  	    month = mySplitResult[1];
		  	    year = mySplitResult[2];
		  	  if(month == 'Jan')  { month = "01";
		  	   }  else if(month == 'Feb') { month = "02";
		  	   } else if(month == 'Mar') { month = "03"
		  	   }   else if(month == 'Apr')  { month = "04"
		  	   }  else if(month == 'May') { month = "05"
		  	   }  else if(month == 'Jun') { month = "06"
		  	   }  else if(month == 'Jul') { month = "07"
		  	   } else if(month == 'Aug') {month = "08"
		  	   } else if(month == 'Sep')  {month = "09"
		  	   } else if(month == 'Oct')  {month = "10"
		  	   }  else if(month == 'Nov')  { month = "11"
		  	   } else if(month == 'Dec')  {month = "12";
		  	   }
	    	    finalDate = month+"-"+day+"-"+year;
	        	  date1 = finalDate.split("-");
	      	   eDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);		  	   
	    	}	
	   	   var day2 = '';
		   var month2 = '';
		   var year2 = '';
		   var finalDate2 ='';
		   var sDate = null;
		   if(actDepart!='' && actDepart.indexOf('-')>-1){
				var mySplitResult2 = actDepart.split("-");
		 	    day2 = mySplitResult2[0];
		 	    month2 = mySplitResult2[1];
		 	    year2 = mySplitResult2[2];
		 	   if(month2 == 'Jan')  { month2 = "01";
		 	   }  else if(month2 == 'Feb') { month2 = "02";
		 	   } else if(month2 == 'Mar') {month2 = "03"
		 	   } else if(month2 == 'Apr') {month2 = "04"
		 	   }  else if(month2 == 'May')  { month2 = "05"
		 	   }  else if(month2 == 'Jun') { month2 = "06"
		 	   } else if(month2 == 'Jul') { month2 = "07"
		 	   } else if(month2 == 'Aug')  {month2 = "08"
		 	   } else if(month2 == 'Sep')  { month2 = "09"
		 	   } else if(month2 == 'Oct')  { month2 = "10"
		 	   }  else if(month2 == 'Nov') { month2 = "11"
		 	   } else if(month2 == 'Dec')  {month2 = "12";
		 	   }
	    	   finalDate2 = month2+"-"+day2+"-"+year2;
	     	  date2 = finalDate2.split("-");
	     	   sDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);		 	   
		   }
	  
	    var currentD = '<%= currentDateForService %>';
	    var date3SplitResult = currentD.split("-");
 		var day3 = date3SplitResult[2];
 		var month3 = date3SplitResult[1];
 		var year3 = date3SplitResult[0];
 		var currentDate = new Date(month3+"/"+day3+"/"+year3);
		var futureDateVal = ${futureDateFlag};
		      
        var diff =0;
        if(finalDate!='' && finalDate.indexOf('-')>-1){
          var actArvDate = finalDate.split("-");
		  var actArvDD = actArvDate[0];
		  var actArvMM = actArvDate[1];
		  var actArvYY = actArvDate[2];
		  var yr="20";
		  var actArvFinalDate = actArvDD+"/"+actArvMM+"/"+yr+actArvYY;
		  var date5 = new Date(actArvFinalDate)
		  var diff = Math.round((date5-currentDate)/86400000);
        }
        var diff1 =0;
        if(finalDate2!='' && finalDate2.indexOf('-')>-1){        
		  var estArvDate = finalDate2.split("-");
		  var estArvDD = estArvDate[0];
		  var estArvMM = estArvDate[1];
		  var estArvYY = estArvDate[2];
		  var yr="20";
		  var estArvFinalDate = estArvDD+"/"+estArvMM+"/"+yr+estArvYY;
		  var date6 = new Date(estArvFinalDate);
		  var diff1 = Math.round((date6-currentDate)/86400000);
        }	
  	   var day3 = '';
	   var month3 = '';
	   var year3 = '';
	   var finalDate3 ='';
	   var e3Date = null;
    	if(estDepart!='' && estDepart.indexOf('-')>-1){
			   var mySplitResult3 = estDepart.split("-");
		   	    day3 = mySplitResult3[0];
		   	    month3 = mySplitResult3[1];
		   	    year3 = mySplitResult3[2];
		   	  if(month3 == 'Jan')  { month3 = "01";
		   	   }  else if(month3 == 'Feb') { month3 = "02";
		   	   } else if(month3 == 'Mar') { month3 = "03"
		   	   }   else if(month3 == 'Apr')  { month3 = "04"
		   	   }  else if(month3 == 'May') { month3 = "05"
		   	   }  else if(month3 == 'Jun') { month3 = "06"
		   	   }  else if(month3 == 'Jul') { month3 = "07"
		   	   } else if(month3 == 'Aug') {month3 = "08"
		   	   } else if(month3 == 'Sep')  {month3 = "09"
		   	   } else if(month3 == 'Oct')  {month3 = "10"
		   	   }  else if(month3 == 'Nov')  { month3 = "11"
		   	   } else if(month3 == 'Dec')  {month3 = "12";
		   	   }
		   	    finalDate3 = month3+"-"+day3+"-"+year3;
		    	  date3 = finalDate3.split("-");
		    	  e3Date = new Date(date3[0]+"/"+date3[1]+"/"+date3[2]);
    	}
   	   var day4 = '';
	   var month4 = '';
	   var year4 = '';
	   var finalDate4 ='';
	   var s4Date = null;
	   if(estArv!='' && estArv.indexOf('-')>-1){
		   	   var mySplitResult4 = estArv.split("-");
		   	    day4 = mySplitResult4[0];
		   	    month4 = mySplitResult4[1];
		   	    year4 = mySplitResult4[2];
		   	   if(month4 == 'Jan')  { month4 = "01";
		   	   }  else if(month4 == 'Feb') { month4 = "02";
		   	   } else if(month4 == 'Mar') {month4 = "03"
		   	   } else if(month4 == 'Apr') {month4 = "04"
		   	   }  else if(month4 == 'May')  { month4 = "05"
		   	   }  else if(month4 == 'Jun') { month4 = "06"
		   	   } else if(month4 == 'Jul') { month4 = "07"
		   	   } else if(month4 == 'Aug')  {month4 = "08"
		   	   } else if(month4 == 'Sep')  { month4 = "09"
		   	   } else if(month4 == 'Oct')  { month4 = "10"
		   	   }  else if(month4 == 'Nov') { month4 = "11"
		   	   } else if(month4 == 'Dec')  {month4 = "12";
		   	   }
		   	  	 finalDate4 = month4+"-"+day4+"-"+year4;
		 	 	 date4 = finalDate4.split("-");
		 	 	 s4Date = new Date(date4[0]+"/"+date4[1]+"/"+date4[2]);
	   }

		  if(carCode==''){
			alert("Carrier Code is required field.");  
			return false;
		}else if(carArrival!='' && poeCode==''){
			alert('Please select Port code.');
			return false;
		}else if(carDeparture!='' && polCode==''){
			alert('Please select Port code.');
			return false;
		}else if(carFlag!=''|| poeFlag!=''|| polFlag!=''){
			return false;
		}else if((str=="1") && (routingVal =='EXP') && (contractStr=='')){
			alert("Service Contract is a required field.");
			return false;
		}else if((str=="1") && (routingVal =='EXP') && (carrierVesselsVal=='')){
			alert("Vessel/Flight # is a required field.");
			return false;
		}else if((str=="1") && (routingVal =='EXP') && (bookNumberVal=='')){
			alert("Booking # is a required field.");
			return false;
		}else if((str=="1") && (routingVal =='EXP') && (hblNumberVal=='')){
			alert("HBL# is a required field.");
			return false;
		}
		else if((str=="1") && (routingVal =='EXP') && (blNumberVal=='')){
			alert("BL #/AWB # is a required field.");
			return false;
		}else if((str=="1") && (routingVal =='EXP') && (polCode=='')){
			alert("Please select Pol code.");
			return false;
		}else if((str=="1") && (routingVal =='EXP') && (estDepart=='')){
			alert("Estimated Departure is a required field.");
			return false;
		}else if((str=="1") && (routingVal =='EXP') && (actDepart=='')){
			alert("Actual Departure is a required field.");
			return false;
		}else if((str=="1") && (routingVal =='EXP') && (poeCode=='')){
			alert("Please select Poe code.");
			return false;
		}else if((str=="1") && (routingVal =='EXP') && (estArv=='')){
			alert("Estimated Arrival is a required field.");
			return false;
		}else if((str=="1") && (routingVal =='EXP') && (actArv=='')){
			alert("Actual Arrival is a required field.");
			return false;
		}else if((e3Date!=null && s4Date!=null) && e3Date > s4Date){
	    		alert("Estimated date of arrival should be greater or equal to Estimated date of departure.")
	    		return false;
		}else if((sDate!=null && eDate!=null) && sDate > eDate){
	        	alert("Actual date of arrival should be greater or equal to Actual date of departure.")
	        	return false;
		}else if((futureDateVal==false) && (diff > 0)){
    		alert("System cannot accept actualizing future dates.Please edit Actual Arrival date.")
    		return false;
		}else if((futureDateVal==false) && (diff1 > 0)){
    		alert("System cannot accept actualizing future dates.Please edit Actual Departure date.")
    		return false;
		}else{
			return submit_form();
		}	
	}
	
	function checkMandatoryFieldUGCA(){
		var str="0";
		<configByCorp:fieldVisibility componentId="component.forward.ContainerRouting.fieldMandatory">
		str="1";
		</configByCorp:fieldVisibility>		
      var routingVal="${serviceOrder.routing}";
		var ort = document.getElementById('ominiRequiredTrue');
		var orf = document.getElementById('ominiRequiredFalse');
		var plrt = document.getElementById('polRequiredTrue');
		var plrf = document.getElementById('polRequiredFalse');
		var pert = document.getElementById('poeRequiredTrue');
		var perf = document.getElementById('poeRequiredFalse');
		var etDptrt = document.getElementById('estDptRequiredTrue');
		var etDptrf = document.getElementById('estDptRequiredFalse');
		var actDptrt = document.getElementById('actDptRequiredTrue');
		var actDptrf = document.getElementById('actDptRequiredFalse');		
		var etArvrt = document.getElementById('estArvRequiredTrue');
		var etArvrf = document.getElementById('estArvRequiredFalse');		
		var actArvrt = document.getElementById('actArvRequiredTrue');
		var actArvrf = document.getElementById('actArvRequiredFalse');
		if((str=="1") && (routingVal =='EXP')){
			ort.style.display = 'block';		
			orf.style.display = 'none';
			plrt.style.display = 'block';		
			plrf.style.display = 'none';
			pert.style.display = 'block';		
			perf.style.display = 'none';
			etDptrt.style.display = 'block';		
			etDptrf.style.display = 'none';
			actDptrt.style.display = 'block';		
			actDptrf.style.display = 'none';
			etArvrt.style.display = 'block';		
			etArvrf.style.display = 'none';
			actArvrt.style.display = 'block';		
			actArvrf.style.display = 'none';
		}else if((str=="1") && (routingVal !='EXP')){
			ort.style.display = 'none';		
			orf.style.display = 'block';
			plrt.style.display = 'none';		
			plrf.style.display = 'block';
			pert.style.display = 'none';		
			perf.style.display = 'block';
			etDptrt.style.display = 'none';		
			etDptrf.style.display = 'block';
			actDptrt.style.display = 'none';		
			actDptrf.style.display = 'block';
			etArvrt.style.display = 'none';		
			etArvrf.style.display = 'block';
			actArvrt.style.display = 'none';		
			actArvrf.style.display = 'block';
		}else if(str=="0"){
			ort.style.display = 'none';
			orf.style.display = 'block';
			plrt.style.display = 'none';
			plrf.style.display = 'block';
			pert.style.display = 'none';
			perf.style.display = 'block';
			etDptrt.style.display = 'none';
			etDptrf.style.display = 'block';
			actDptrt.style.display = 'none';
			actDptrf.style.display = 'block';
			etArvrt.style.display = 'none';
			etArvrf.style.display = 'block';
			actArvrt.style.display = 'none';
			actArvrf.style.display = 'block';
		}else{
			
		}
}
	
	
</script> 

</head>   

<s:hidden name="serviceOrder.originCountryCode"/>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="servicePartnerForm" action="saveServicePartnerAjax.html?decorator=popup&popup=true" onsubmit="return checkCodeBlank();" method="post" validate="true">   
<s:hidden name="updateRecords" value=""/>
<s:hidden name="serviceOrder.routing"/>
<s:hidden name="servicePartner.servicePartnerId" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="validateFlagTrailer"/>
<s:hidden name ="deliveryLastDay"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/> 
<s:hidden name="servicePartner.actgCode" />
<s:hidden name="servicePartner.id" />
<s:hidden name="servicePartner.corpID" />
<s:hidden  name="servicePartner.ugwIntId" />
<s:hidden name="servicePartner.serviceOrderId" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.shipNumber"/> 
<s:hidden name="serviceOrder.registrationNumber"/> 
<s:hidden name="serviceOrder.id"/> 
<s:hidden name="carFlag" value="" />
<s:hidden name="poeFlag" value="" />
<s:hidden name="polFlag" value="" />
<configByCorp:fieldVisibility componentId="component.field.Alternative.Division">
	<s:hidden name="divisionFlag" value="YES"/>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.forward.ContainerRouting.fieldMandatory">		
		<c:set var="fieldMandatory" value="Y" />
</configByCorp:fieldVisibility>
<s:hidden name="shipSize" />
    <s:hidden name="minShip" />
    <s:hidden name="countShip" />
    <s:hidden name="minChild" />
    <s:hidden name="maxChild" />
    <s:hidden name="countChild" />
<s:hidden name="customerFile.id" />
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/>
<s:hidden name="servicePartner.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="servicePartner.partnerType" value="CR"/>
<s:hidden name="serviceOrder.sid" value="%{serviceOrder.id}"/>
<s:hidden name="soId" value="%{serviceOrder.id}"/> 
<s:hidden id="countServiceNotes" name="countServiceNotes" value="<%=request.getParameter("countServiceNotes") %>"/>
<s:hidden id="countCarrierNotes" name="countCarrierNotes" value="<%=request.getParameter("countCarrierNotes") %>"/>
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<c:set var="field" value="<%=request.getParameter("field") %>"/>
<s:hidden name="field" value="<%=request.getParameter("field") %>" />
<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
<c:set var="field1" value="<%=request.getParameter("field1") %>"/>

		
		
<c:set var="countServiceNotes" value="<%=request.getParameter("countServiceNotes") %>" />
<c:set var="countCarrierNotes" value="<%=request.getParameter("countCarrierNotes") %>" />

<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>

 <div id="Layer4" style="width:100%;">
 <div id="newmnav" style="!float:left;"> 
 <ul> 
  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Routing</span></a></li>   
  </ul>
</div>


<div class="spn">&nbsp;</div>

</div>
<div id="Layer1"  onkeydown="changeStatus();" style="width:100%">
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0" >
	<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
				<tbody>
					<tr><td height="5px"></td></tr> 
					<tr>
					<td align="right" width="50px" class="listwhitetext">Status</td>
				                    <c:set var="isStatusFlag" value="false"/>
									<c:if test="${servicePartner.status}">
										<c:set var="isStatusFlag" value="true"/>
									</c:if>
				                    <td align="left" width="20px" valign="bottom"><s:checkbox key="servicePartner.status" value="${isStatusFlag}" onchange="statusValidation(this);"  cssStyle="margin:0px;" fieldValue="true"  tabindex="3" /></td>
					</tr> 
					<tr>                  
                       	<td align="right" class="listwhitetext">#</td>
                       	<td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-textUpper" key="servicePartner.carrierNumber" required="true" cssStyle="width:65px;" readonly="true" maxlength="2" tabindex="1"/></td>
                       	<td colspan="2" style="margin: 0px;">					
								<table style="margin:0px" width="100%">
								<tr>
									<td align="right" width="50px" class="listwhitetext"><fmt:message key="servicePartner.dropPick"/></td>
									<c:set var="isVipFlag" value="false"/>
									<c:if test="${servicePartner.dropPick}">
										<c:set var="isVipFlag" value="true"/>
									</c:if>
									<td align="left" width="20px" valign="bottom"><s:checkbox key="servicePartner.dropPick" value="${isVipFlag}" fieldValue="true" tabindex="2" /></td>                
				                    
				                    <td align="right" width="50px" class="listwhitetext"><fmt:message key="servicePartner.liveLoad"/></td>
				                    <c:set var="isVipFlag" value="false"/>
									<c:if test="${servicePartner.liveLoad}">
										<c:set var="isVipFlag" value="true"/>
									</c:if>
				                    <td align="left" width="20px" valign="bottom"><s:checkbox key="servicePartner.liveLoad" value="${isVipFlag}" fieldValue="true"  tabindex="3" /></td>
                   					 
                   					<td align="right" width="30px" class="listwhitetext"><fmt:message key="servicePartner.transhipped"/></td>
				                    <c:set var="isVipFlag" value="false"/>
									<c:if test="${servicePartner.transhipped}">
										<c:set var="isVipFlag" value="true"/>
									</c:if>
				                    <td align="left" width="20px" valign="bottom"><s:checkbox key="servicePartner.transhipped" value="${isVipFlag}" fieldValue="true" onclick="checkOne(this)"  tabindex="4" /></td>
									
									<c:if test="${serviceOrder.mode == 'Overland'}">
										<td align="right" width="30px" class="listwhitetext">Clone</td>
										<c:set var="isCloneFlag" value="false"/>
										<c:if test="${servicePartner.clone}">
											<c:set var="isCloneFlag" value="true"/>
										</c:if>
	                      				<td align="left" width="20px" valign="bottom"><s:checkbox key="servicePartner.clone" value="${isCloneFlag}" fieldValue="true" onclick="checkOne(this), displayField();" tabindex="5" /></td>
									</c:if>
								</tr>
					</tr>
					
					</table>
					</td>						
	                       	<c:if test="${serviceOrder.mode == 'Overland'}">
		                       	<td align="left" colspan="2" class="listwhitetext">
		                       	<div id="disDiv1" >Total Clone #&nbsp;&nbsp;<s:textfield cssClass="input-text" key="totalClone" required="true" size="3" maxlength="2" onchange="onlyNumeric(this);" tabindex="6"/></div></td>
	                       	</c:if>
	                       	<c:if test="${serviceOrder.mode != 'Overland'}">
	                       		<s:hidden name="totalClone" value="0"></s:hidden>
	                       	</c:if>                       	
                   </tr>
					<tr>  
					<c:if test="${userType!='AGENT'}">
						<c:if test="${accountInterface!='Y'}">                  
							<td align="right" class="listwhitetext">Carrier Code<font color="red" size="2">*</font></td>
							<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="servicePartnerCarrierCodeId" key="servicePartner.carrierCode" required="true" cssStyle="width:65px;" maxlength="8" onchange="valid(this,'special');checkVendorName()" onfocus = "myDate();" tabindex="7" onkeypress="okValidateListFromTrailer();"/></td>
							<td align="left"><img class="openpopup" width="17" height="20" onclick="changeStatus();okValidateListFromTrailer();javascript:openWindow('servicePartnerlistPopUp.html?id=${serviceOrder.id}&partnerType=CR&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=trackingUrl&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=servicePartner.carrierName&fld_code=servicePartner.carrierCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						</c:if>
						<c:if test="${accountInterface=='Y'}">
							<td align="right" class="listwhitetext">Carrier Code<font color="red" size="2">*</font></td>
							<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="servicePartnerCarrierCodeId" key="servicePartner.carrierCode" required="true" cssStyle="width:65px;" maxlength="8" onchange="valid(this,'special');checkVendorName();" onfocus = "myDate();" tabindex="7" onkeypress="okValidateListFromTrailer();"/></td>
							<td align="left"><img class="openpopup" width="17" height="20" onclick="changeStatus();okValidateListFromTrailer();javascript:openWindow('servicePartnerlistPopUp.html?id=${serviceOrder.id}&partnerType=CR&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=trackingUrl&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=servicePartner.carrierName&fld_code=servicePartner.carrierCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						</c:if>
						</c:if>
						<c:if test="${userType=='AGENT'}">
						<c:if test="${accountInterface!='Y'}">                  
							<td align="right" class="listwhitetext">Carrier Code<font color="red" size="2">*</font></td>
							<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="newcharge" key="servicePartner.carrierCode" required="true" cssStyle="width:65px;" maxlength="8" onchange="checkCorpVendorName();" onfocus = "myDate();" tabindex="7" onkeypress="okValidateListFromTrailer();"/></td>
							<td align="left"><img class="openpopup" width="17" height="20" onclick="changeStatus();okValidateListFromTrailer();javascript:openWindow('servicePartnerAgentPopUp.html?soCorpID=${serviceOrder.corpID}&code=newcharge&Name=newDescription&decorator=popup&popup=true');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						</c:if>
						<c:if test="${accountInterface=='Y'}">
							<td align="right" class="listwhitetext">Carrier Code<font color="red" size="2">*</font></td>
							<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="newcharge" key="servicePartner.carrierCode" required="true" cssStyle="width:65px;" maxlength="8" onchange="checkCorpVendorName();" onfocus = "myDate();" tabindex="7" onkeypress="okValidateListFromTrailer();"/></td>
							<td align="left"><img class="openpopup" width="17" height="20" onclick="changeStatus();okValidateListFromTrailer();javascript:openWindow('servicePartnerAgentPopUp.html?soCorpID=${serviceOrder.corpID}&code=newcharge&Name=newDescription&decorator=popup&popup=true');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						</c:if>
						</c:if>
							<td align="left" colspan="3" class="listwhitetext"><s:textfield cssClass="input-text" id="newDescription" key="servicePartner.carrierName"  required="true" cssStyle="width:242px;" maxlength="65" onkeyup="findPartnerDetails('newDescription','servicePartnerCarrierCodeId','servicePartnerCcarrierNameDivId',' and isCarrier is true','',event);" onfocus = "myDate();" tabindex="8"/>
							<div id="servicePartnerCcarrierNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
							<input type="button" class="cssbutton" value="Tracking" style="width:60px; height:22px;padding-bottom:1px;" onclick="checkVendorForTrackingUrl()" tabindex="9"/></td>

					<td colspan="1" style="margin: 0px;"></td>
					 
					 <sec-auth:authComponent componentId="module.tab.container.customerFileTab">		
							<c:if test="${empty servicePartner.id}">
									<td colspan="2" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
									</c:if>
									<c:if test="${not empty servicePartner.id}">
									<c:choose>
										<c:when test="${countCarrierNotes == '0' || countCarrierNotes == '' || countCarrierNotes == null}">
										<td colspan="2" align="right"><img id="countCarrierNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${servicePartner.id }&notesId=${servicePartner.id }&noteFor=ServicePartner&subType=Carrier&imageId=countCarrierNotesImage&fieldId=countCarrierNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${servicePartner.id }&notesId=${servicePartner.id }&noteFor=ServicePartner&subType=Carrier&imageId=countCarrierNotesImage&fieldId=countCarrierNotes&decorator=popup&popup=true',755,500);" ></a></td>
										</c:when>
										<c:otherwise>
										<td colspan="2" align="right"><img id="countCarrierNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${servicePartner.id }&notesId=${servicePartner.id}&noteFor=ServicePartner&subType=Carrier&imageId=countCarrierNotesImage&fieldId=countCarrierNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${servicePartner.id }&notesId=${servicePartner.id }&noteFor=ServicePartner&subType=Carrier&imageId=countCarrierNotesImage&fieldId=countCarrierNotes&decorator=popup&popup=true',755,500);" ></a></td>
										</c:otherwise>
									</c:choose> 
									</c:if>
					</sec-auth:authComponent>	
					</tr>
					
					<tr>
						<td align="right" class="listwhitetext" id="ominiRequiredFalse"><fmt:message key="servicePartner.omini"/></td>
						<td align="right" class="listwhitetext" id="ominiRequiredTrue"><fmt:message key="servicePartner.omini"/><font color="red" size="2">*</font></td>
                       	<td  align="left" class="listwhitetext" colspan="5"><configByCorp:customDropDown listType="map" list="${omni_isactive}" fieldValue="${servicePartner.omni}"
		       attribute="id=servicePartnerForm_servicePartner_omni class=list-menu name=servicePartner.omni  style=width:344px  headerKey='' headerValue='' onchange='changeStatus();' tabindex='10' "/></td>
<%--                        	<s:select cssClass="list-menu" name="servicePartner.omni" list="%{omni_isactive}" cssStyle="width:340px" onchange="changeStatus();" tabindex="10" /></td>
 --%>                       	
                    
                     <c:if test="${serviceOrder.routing == 'EXP' && (serviceOrder.originCountryCode == 'USA' || serviceOrder.originCountryCode == 'CAN') }">
                     <td align="right" class="listwhitetext" id="caedField">CAED&nbsp;#</td>
                    <td align="left" class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" id="caedField1" name="servicePartner.caed" required="true" cssStyle="width:180px;" maxlength="30" tabindex="15" /></td>
                    </c:if>
                       	
                       	<c:if test="${serviceOrder.mode == 'Overland'}">
	                       	<td align="right" class="listwhitetext">Skids</td>
	                       	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="servicePartner.skids" required="true" size="16" maxlength="20" tabindex="11"/></td>
                       	</c:if>
                       	<c:if test="${serviceOrder.mode != 'Overland'}">
                       		<s:hidden name="servicePartner.skids" />
                       		<s:hidden name="servicePartner.clone" value="false"/>
                       	</c:if>
					</tr>
					<tr>                  
                       	<td align="right" width="10px" class="listwhitetext"><fmt:message key="servicePartner.houseBillIssued"/></td>
						<c:if test="${not empty servicePartner.houseBillIssued}">
							<s:text id="servicePartnerCarrierTADownFormattedValue" name="${FormDateValue}"><s:param name="value" value="servicePartner.houseBillIssued"/></s:text>
							<td colspan="3"><s:textfield cssClass="input-text" id="houseBillIssued" name="servicePartner.houseBillIssued" value="%{servicePartnerCarrierTADownFormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true" />
								<img id="houseBillIssued_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</td>
						</c:if>
						<c:if test="${empty servicePartner.houseBillIssued}">
							<td colspan="3"><s:textfield cssClass="input-text" id="houseBillIssued" name="servicePartner.houseBillIssued" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true" />
								<img id="houseBillIssued_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</td>
						</c:if>
						<configByCorp:fieldVisibility componentId="component.field.show.HouseBillNumber">
							<td align="right" class="listwhitetext">House&nbsp;Bill&nbsp;Number</td>
	                       <td align="left" ><s:textfield  name="servicePartner.houseBillNumber" cssClass="input-text" size="23" maxlength="30" /></td>
	                  	</configByCorp:fieldVisibility>	
						<c:if test="${serviceOrder.mode == 'Overland' || serviceOrder.mode == 'Truck'}">
						<configByCorp:fieldVisibility componentId="component.field.UGHK.aesNumber">
							<td align="right" class="listwhitetext">							
							Team #							
							</td>
							</configByCorp:fieldVisibility>
						</c:if>
						<c:if test="${serviceOrder.mode != 'Overland' && serviceOrder.mode != 'Truck'}">
						<configByCorp:fieldVisibility componentId="component.field.UGHK.aesNumber">
							<td align="right" class="listwhitetext">							
							<fmt:message key="servicePartner.aesNumber"/>												
							</td>
							</configByCorp:fieldVisibility>		
						</c:if>
						
				       <configByCorp:fieldVisibility componentId="component.field.UGHK.aesNumber"> <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="servicePartner.aesNumber" required="true" cssStyle="width:65px;" tabindex="11" maxlength="30"/></td></configByCorp:fieldVisibility>
				        <td align="right" class="listwhitetext" width="90px"><fmt:message key="container.containerNumber"/></td>
                       <td align="left" class="listwhitetext" ><s:select  id="cntnrNumber" cssClass="list-menu" key="servicePartner.cntnrNumber"  list="%{containerNumberList}" headerKey="" headerValue="" tabindex="12" cssStyle="width:67px;" onchange="changeStatus();" /></td>
                        <configByCorp:fieldVisibility componentId="component.section.forwarding.Housebilloflading">
                        <td align="right" class="listwhitetext"><fmt:message key="servicePartner.hblNumber"/></td>
                       <td align="left" class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" name="servicePartner.hblNumber" required="true" cssStyle="width:65px;" maxlength="30" tabindex="15" /></td>
                   </configByCorp:fieldVisibility>
                   </tr>
                   <tr>
                   <c:if test="${fieldMandatory=='Y' && serviceOrder.routing == 'EXP'}">
                   		<c:if test="${serviceOrder.mode == 'Overland' || serviceOrder.mode == 'Truck'}">
							<td align="right" class="listwhitetext">Seal #<font color="red" size="2">*</font></td>
						</c:if>
						<c:if test="${serviceOrder.mode != 'Overland' && serviceOrder.mode != 'Truck'}">
	                  		<td align="right" class="listwhitetext"><fmt:message key="servicePartner.carrierVessels"/><font color="red" size="2">*</font></td>
	                  	</c:if>
	                </c:if>
	                <c:if test="${fieldMandatory=='Y' && serviceOrder.routing != 'EXP'}">
	                	<c:if test="${serviceOrder.mode == 'Overland' || serviceOrder.mode == 'Truck'}">
							<td align="right" class="listwhitetext">Seal #</td>
						</c:if>
						<c:if test="${serviceOrder.mode != 'Overland' && serviceOrder.mode != 'Truck'}">
	                  		<td align="right" class="listwhitetext"><fmt:message key="servicePartner.carrierVessels"/></td>
	                  	</c:if>
	                </c:if>
	                <c:if test="${fieldMandatory!='Y'}">
	                	<c:if test="${serviceOrder.mode == 'Overland' || serviceOrder.mode == 'Truck'}">
							<td align="right" class="listwhitetext">Seal #</td>
						</c:if>
						<c:if test="${serviceOrder.mode != 'Overland' && serviceOrder.mode != 'Truck'}">
	                  		<td align="right" class="listwhitetext"><fmt:message key="servicePartner.carrierVessels"/></td>
	                  	</c:if>
	                </c:if>
	                  	<td align="left" class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" name="servicePartner.carrierVessels" required="true" cssStyle="width:216px;" maxlength="35" tabindex="13" /></td>
	                  	
	                  	<c:if test="${fieldMandatory=='Y' && serviceOrder.routing == 'EXP'}">
	                  	<c:if test="${serviceOrder.mode == 'Overland' || serviceOrder.mode == 'Truck'}">
							<td align="right" class="listwhitetext">Trailer #<font color="red" size="2">*</font></td>
						</c:if>
						<c:if test="${serviceOrder.mode != 'Overland' && serviceOrder.mode != 'Truck'}">
	                  		<td align="right" class="listwhitetext"><fmt:message key="servicePartner.bookNumber"/><font color="red" size="2">*</font></td>
	                  	</c:if>
	                  	</c:if>
	                  	<c:if test="${fieldMandatory=='Y' && serviceOrder.routing != 'EXP'}">
	                  	<c:if test="${serviceOrder.mode == 'Overland' || serviceOrder.mode == 'Truck'}">
							<td align="right" class="listwhitetext">Trailer #</td>
						</c:if>
						<c:if test="${serviceOrder.mode != 'Overland' && serviceOrder.mode != 'Truck'}">
	                  		<td align="right" class="listwhitetext"><fmt:message key="servicePartner.bookNumber"/></td>
	                  	</c:if>
	                  	</c:if>
	                  	<c:if test="${fieldMandatory!='Y'}">
	                  	<c:if test="${serviceOrder.mode == 'Overland' || serviceOrder.mode == 'Truck'}">
							<td align="right" class="listwhitetext">Trailer #</td>
						</c:if>
						<c:if test="${serviceOrder.mode != 'Overland' && serviceOrder.mode != 'Truck'}">
	                  		<td align="right" class="listwhitetext"><fmt:message key="servicePartner.bookNumber"/></td>
	                  	</c:if>
	                  	</c:if>
	                  	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="servicePartner.bookNumber" required="true" cssStyle="width:65px;" maxlength="30" tabindex="14" /></td>
	                  	<c:if test="${fieldMandatory=='Y' && serviceOrder.routing == 'EXP'}">
	                  	<c:if test="${serviceOrder.mode == 'Overland' || serviceOrder.mode == 'Truck'}">
							<td align="right" class="listwhitetext">PRO #<font color="red" size="2">*</font></td>
						</c:if>
						<c:if test="${serviceOrder.mode != 'Overland' && serviceOrder.mode != 'Truck'}">
	                  		<td align="right" class="listwhitetext"><fmt:message key="servicePartner.blNumber"/><font color="red" size="2">*</font></td>
	                  	</c:if>
	                  	</c:if>
	                  	<c:if test="${fieldMandatory=='Y' && serviceOrder.routing != 'EXP'}">
	                  	<c:if test="${serviceOrder.mode == 'Overland' || serviceOrder.mode == 'Truck'}">
							<td align="right" class="listwhitetext">PRO #</td>
						</c:if>
						<c:if test="${serviceOrder.mode != 'Overland' && serviceOrder.mode != 'Truck'}">
	                  		<td align="right" class="listwhitetext"><fmt:message key="servicePartner.blNumber"/></td>
	                  	</c:if>
	                  	</c:if>
	                  	<c:if test="${fieldMandatory!='Y'}">
	                  	<c:if test="${serviceOrder.mode == 'Overland' || serviceOrder.mode == 'Truck'}">
							<td align="right" class="listwhitetext">PRO #</td>
						</c:if>
						<c:if test="${serviceOrder.mode != 'Overland' && serviceOrder.mode != 'Truck'}">
	                  		<td align="right" class="listwhitetext"><fmt:message key="servicePartner.blNumber"/></td>
	                  	</c:if>
	                  	</c:if>	                  
	                  	<td align="left" class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" name="servicePartner.blNumber" required="true" cssStyle="width:65px;" maxlength="30" tabindex="15" /></td>
					</tr>
                 	<tr>
                  		<td align="right" class="listwhitetext" id="polRequiredFalse"><fmt:message key="servicePartner.carrierDeparture"/></td>
                  		<td align="right" class="listwhitetext" id="polRequiredTrue"><fmt:message key="servicePartner.carrierDeparture"/><font color="red" size="2">*</font></td>
                  		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="servicePartner.polCode" cssStyle="width:65px;" onchange="valid(this,'special');findPortName();" tabindex="16" /></td>
                  		
                  		<td><img align="right" class="openpopup" width="17" height="20" onclick="javascript:openWindow('searchPortList.html?portCode=&portName=&country=&modeType=${serviceOrder.mode}&active=true&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=servicePartner.carrierDeparture&fld_code=servicePartner.polCode')" src="<c:url value='/images/open-popup.gif'/>" /></td>
                 		
                 		<td  width="150px"><s:textfield cssClass="input-text" id="autocompletePOL"  name="servicePartner.carrierDeparture"  cssStyle="width:120px;" tabindex="17" onchange="checkBlank();" onkeyup="findPortDetails('autocompletePOL','servicePartnerCarrierDepartureDivId','POL');"/>
	  			        <div id="servicePartnerCarrierDepartureDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>               
	  			</td>              

             <td align="right" class="listwhitetext" id="estDptRequiredFalse"><fmt:message key="servicePartner.ETDTA1"/></td>
             <td align="right" class="listwhitetext" id="estDptRequiredTrue"><fmt:message key="servicePartner.ETDTA1"/><font color="red" size="2">*</font></td>			
			<c:if test="${not empty servicePartner.etDepart}">
			<s:text id="servicePartnerCarrierTDDownFormattedValue" name="${FormDateValue}"><s:param name="value" value="servicePartner.etDepart"/></s:text>
			<td><s:textfield cssClass="input-text" id="etDepart" name="servicePartner.etDepart" value="%{servicePartnerCarrierTDDownFormattedValue}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true" /><a href="javascript:void(0);" onblur="test1();">
				<img id="etDepart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty servicePartner.etDepart}">
			<td><s:textfield cssClass="input-text" id="etDepart" name="servicePartner.etDepart" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true"  /><a href="javascript:void(0);" onblur="test1();">
				<img id="etDepart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			
 			<td align="right" width="100px" class="listwhitetext" id="actDptRequiredFalse"><fmt:message key="servicePartner.atDepart1"/></td>
 			<td align="right" width="100px" class="listwhitetext" id="actDptRequiredTrue"><fmt:message key="servicePartner.atDepart1"/><font color="red" size="2">*</font></td>			
			<c:if test="${not empty servicePartner.atDepart}">
			<s:text id="servicePartnerCarrierTDDownFormattedValue" name="${FormDateValue}">
            <s:param name="value" value="servicePartner.atDepart"/></s:text>
            <td><s:textfield cssClass="input-text" id="atDepart" name="servicePartner.atDepart" value="%{servicePartnerCarrierTDDownFormattedValue}" 
            cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true" /><a href="javascript:void(0);" onblur="test1();">
            <c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
				<img id="atDepart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test='${serviceOrder.status == "CNCL"}'>
				<img id="atDepart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/>
			</c:if>
			<c:if test='${serviceOrder.status == "HOLD"}'>
				<img id="atDepart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/>
			</c:if></a></td>
			</c:if>
			<c:if test="${empty servicePartner.atDepart}">
			<td><s:textfield cssClass="input-text" id="atDepart" name="servicePartner.atDepart" required="true" cssStyle="width:65px;" maxlength="11" 
             onkeydown="return onlyDel(event,this)" readonly="true"  /><a href="javascript:void(0);" onblur="test1();">
             <c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
             	<img id="atDepart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
             </c:if>
             <c:if test='${serviceOrder.status == "CNCL"}'>
						<img id="atDepart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/>
					</c:if>
					<c:if test='${serviceOrder.status == "HOLD"}'>
						<img id="atDepart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/>
					</c:if></a></td>
			</c:if>
			<c:if test="${not empty servicePartner.id && (serviceOrder.mode == 'Overland' || serviceOrder.mode == 'Truck' || serviceOrder.mode == 'Road')}">  
     		<td><input type="button" class="cssbutton" style="height:20px;padding:0px 2px;" onclick="copyATD();" value="Copy ATD" />  </td>
            </c:if>
			</tr>
			<tr>			
			<td align="right" class="listwhitetext" id="poeRequiredFalse"><fmt:message key="servicePartner.carrierArrival"/></td>
			<td align="right" class="listwhitetext" id="poeRequiredTrue"><fmt:message key="servicePartner.carrierArrival"/><font color="red" size="2">*</font></td>
			<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="servicePartner.poeCode" cssStyle="width:65px;" onchange="valid(this,'special');findPortNamePoe()" tabindex="18" /></td>
			<td><img align="left" class="openpopup" width="17" height="20" onclick="javascript:openWindow('searchPortList.html?portCode=&portName=&country=&modeType=${serviceOrder.mode}&active=true&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=servicePartner.carrierArrival&fld_code=servicePartner.poeCode')" src="<c:url value='/images/open-popup.gif'/>" /></td>
			<td  width="150px"><s:textfield cssClass="input-text" id="autocompletePOE"  name="servicePartner.carrierArrival" onchange="checkBlank();" onkeyup="findPortDetails('autocompletePOE','servicePartnerCarrierArrivalDivId','POE');" cssStyle="width:120px;" tabindex="19" />
                <div id="servicePartnerCarrierArrivalDivId" class="autocomplete"  ></div> 
	  			</td>			
			 
			<td align="right" class="listwhitetext" id="estArvRequiredFalse"><fmt:message key="servicePartner.ETATA"/></td>
			<td align="right" class="listwhitetext" id="estArvRequiredTrue"><fmt:message key="servicePartner.ETATA"/><font color="red" size="2">*</font></td>           
            <c:if test="${not empty servicePartner.etArrival}">
			<s:text id="servicePartnerCarrierTADownFormattedValue" name="${FormDateValue}"><s:param name="value" value="servicePartner.etArrival"/></s:text>
			<td><s:textfield cssClass="input-text" id="etArrival" name="servicePartner.etArrival" value="%{servicePartnerCarrierTADownFormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true" /><a href="javascript:void(0);" onblur="test2();">
				<img id="etArrival_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></a></td>
			</c:if>
			<c:if test="${empty servicePartner.etArrival}">
			<td><s:textfield cssClass="input-text" id="etArrival" name="servicePartner.etArrival" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true"  /><a href="javascript:void(0);" onblur="test2();">
				<img id="etArrival_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></a></td>
			</c:if>			

			<td align="right" width="100px" class="listwhitetext" id="actArvRequiredFalse"><fmt:message key="servicePartner.atArrival1"/></td>
			<td align="right" width="100px" class="listwhitetext" id="actArvRequiredTrue"><fmt:message key="servicePartner.atArrival1"/><font color="red" size="2">*</font></td>		
			<c:if test="${not empty servicePartner.atArrival}">
			<s:text id="servicePartnerCarrierTADownFormattedValue" name="${FormDateValue}"><s:param name="value" value="servicePartner.atArrival"/></s:text>
			<td><s:textfield cssClass="input-text" id="atArrival" name="servicePartner.atArrival" value="%{servicePartnerCarrierTADownFormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true" /><a href="javascript:void(0);" onblur="test2();">
			<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
			<img id="atArrival_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></a></td>
			</c:if>
			<c:if test='${serviceOrder.status == "CNCL"}'>
             <img id="atArrival_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/>
            </c:if>
            <c:if test='${serviceOrder.status == "HOLD"}'>
             <img id="atArrival_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/>
            </c:if></a></td>
		    </c:if>
			<c:if test="${empty servicePartner.atArrival}">
			<td><s:textfield cssClass="input-text" id="atArrival" name="servicePartner.atArrival" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true" /><a href="javascript:void(0);" onblur="test2();">
			<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
			<img id="atArrival_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test='${serviceOrder.status == "CNCL"}'>
             <img id="atArrival_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/>
            </c:if>
            <c:if test='${serviceOrder.status == "HOLD"}'>
             <img id="atArrival_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/>
            </c:if></a></td>		
	        </c:if>		
			<c:if test="${not empty servicePartner.id && (serviceOrder.mode == 'Overland' || serviceOrder.mode == 'Truck' || serviceOrder.mode == 'Road')}">  
     		<td><input type="button" class="cssbutton" style="height:20px;padding:0px 2px;" onclick="copyATA();" value="Copy ATA" />  </td>
            </c:if>
			</tr>
		<tr>
	
		<td colspan="5" align="right" class="listwhitetext">Revised&nbsp;ETD</td>
						<c:if test="${not empty servicePartner.revisedETD}">
						<s:text id="servicePartnerRevisedETDFormattedValue" name="${FormDateValue}"><s:param name="value" value="servicePartner.revisedETD"/></s:text>
						<td ><s:textfield cssClass="input-text" id="revisedETD"  name="servicePartner.revisedETD" value="%{servicePartnerRevisedETDFormattedValue}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"readonly="true" />
						<img id="revisedETD_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					<c:if test="${empty servicePartner.revisedETD}">
					<td><s:textfield cssClass="input-text" id="revisedETD" readonly="true" name="servicePartner.revisedETD" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
					<img id="revisedETD_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					
		<td align="right" class="listwhitetext">Revised&nbsp;ETA</td>
						<c:if test="${not empty servicePartner.revisedETA}">
						<s:text id="servicePartnerRevisedETAFormattedValue" name="${FormDateValue}"><s:param name="value" value="servicePartner.revisedETA"/></s:text>
						<td ><s:textfield cssClass="input-text" id="revisedETA"  name="servicePartner.revisedETA" value="%{servicePartnerRevisedETAFormattedValue}" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"readonly="true" />
						<img id="revisedETA_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					<c:if test="${empty servicePartner.revisedETA}">
					<td><s:textfield cssClass="input-text" id="revisedETA" readonly="true" name="servicePartner.revisedETA" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
					<img id="revisedETA_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					
		</tr>
			<configByCorp:fieldVisibility componentId="component.field.add.servicePartner">
			<tr>
			
							<td align="right" class="listwhitetext">Sub&nbsp;Carrier&nbsp;Code</td>
							<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="subcarrierCode" key="servicePartner.subcarrierCode" required="true" cssStyle="width:65px;" maxlength="8" onchange="checksubCarrierName();" onfocus = "myDate();" tabindex="7" /></td>
							<td align="left"><img class="openpopup" width="17" height="20" onclick="changeStatus();javascript:openWindow('servicePartnerAgentPopUp.html?soCorpID=${serviceOrder.corpID}&code=subcarrierCode&Name=subcarrierNameDescription&decorator=popup&popup=true');" src="<c:url value='/images/open-popup.gif'/>" /></td>
						
							<td align="left" colspan="3" class="listwhitetext"><s:textfield cssClass="input-text" id="subcarrierNameDescription" key="servicePartner.subcarrierName"  required="true" cssStyle="width:237px;" maxlength="65" onkeyup="findPartnerDetails('subcarrierNameDescription','subcarrierCode','subcarrierNameId',' and isCarrier is true','',event);" onfocus = "myDate();" tabindex="8"/>
							<div id="subcarrierNameId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
							</td>
							<td align="right" class="listwhitetext">Sub&nbsp;carrier&nbsp;booking&nbsp;#</td>
							<td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  key="servicePartner.subcarrierBooking" required="true" cssStyle="width:65px;" maxlength="45"  tabindex="" /></td>
			</tr>
			
			<tr>
							<td align="right" class="listwhitetext">Booking&nbsp;Date</td>
							<c:if test="${not empty servicePartner.bookingdate}">
							<s:text id="bookingdateFormattedValue" name="${FormDateValue}"><s:param name="value" value="servicePartner.bookingdate"/></s:text>
							<td colspan="3"><s:textfield cssClass="input-text" id="bookingdate" name="servicePartner.bookingdate" value="%{bookingdateFormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true" />
								<img id="bookingdate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</td>
						</c:if>
						<c:if test="${empty servicePartner.bookingdate}">
							<td colspan="3"><s:textfield cssClass="input-text" id="bookingdate" name="servicePartner.bookingdate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true" />
								<img id="bookingdate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</td>
						</c:if>
						<td></td>
						<td></td>
							<td align="right" class="listwhitetext">Cost</td>
							<td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  key="servicePartner.cost" required="true" cssStyle="width:65px;" maxlength="45" onchange="checkCorpVendorName();" onfocus = "myDate();" tabindex="7" /></td>
			</tr>
			</configByCorp:fieldVisibility>
			
<tr><td height="5px"></td></tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
 </div>
<div class="bottom-header" style="!padding-top:20px;"><span></span></div>
</div>
</div> 		
				<table>
					<tbody>
						<tr>
				        	
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='servicePartner.createdOn'/></td>
						<fmt:formatDate var="servicePartnerCreatedOnFormattedValue" value="${servicePartner.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="servicePartner.createdOn" value="${servicePartnerCreatedOnFormattedValue}" />
						<td><fmt:formatDate value="${servicePartner.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='servicePartner.createdBy' /></td>
						
						
						<c:if test="${not empty servicePartner.id}">
								<s:hidden name="servicePartner.createdBy"/>
								<td><s:label name="createdBy" value="%{servicePartner.createdBy}"/></td>
							</c:if>
							<c:if test="${empty servicePartner.id}">
								<s:hidden name="servicePartner.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
												
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='servicePartner.updatedOn'/></td>
						<fmt:formatDate var="servicePartnerUpdatedOnFormattedValue" value="${servicePartner.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="servicePartner.updatedOn" value="${servicePartnerUpdatedOnFormattedValue}"/>
						<td><fmt:formatDate value="${servicePartner.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='servicePartner.updatedBy' /></td>
						<c:if test="${not empty servicePartner.id}">
							<s:hidden name="servicePartner.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{servicePartner.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty servicePartner.id}">
							<s:hidden name="servicePartner.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
	 </tr>
	  </tbody>
</table>								
									
  </div> 
  
  
  <c:choose>
		<c:when test="${serviceOrder.controlFlag =='G'  &&  serviceOrder.grpStatus=='Finalized'}">													              
        <s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" onclick="confirmUpdate();"/>   
        </c:when>
		<c:when test="${serviceOrder.controlFlag =='G'  && serviceOrder.grpStatus=='Draft'}">													              
        <s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" onclick="confirmUpdate1();"/>   
        </c:when>
        <c:when test="${serviceOrder.grpID != null && (serviceOrder.grpStatus=='Draft' || serviceOrder.grpStatus=='Finalized' ) && (not empty servicePartner.servicePartnerId)}">													              
        <s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" onclick="return confirmUpdateByChild();"/>   
        </c:when>
        <c:otherwise>
        <s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" />   
        </c:otherwise>
  </c:choose>
  
   <s:reset cssClass="cssbutton1" key="Reset" cssStyle="width:55px; height:25px"/> 
    <s:hidden name="servicePartner.miles"/>
    <s:hidden name="secondDescription" />    
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="firstDescription" />
<s:hidden name="trackingUrl"/>
<s:hidden name="serviceOrder.controlFlag"/>
<s:hidden name="serviceOrder.grpStatus"/>
<s:hidden name="serviceOrder.grpID"/>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
</div>
</s:form>
 <script type="text/javascript">
 
 function statusValidation(targetElementStatus){
		var massage="";
		if(targetElementStatus.checked==false){
			massage="Are you sure you wish to deactivate this row?"
		}else{
			massage="Are you sure you wish to activate this row?"
		}  
		var agree=confirm(massage);
		if (agree){
			
		}else{
			if(targetElementStatus.checked==false){
				document.forms['servicePartnerForm'].elements['servicePartner.status'].checked=true;
				}else{
					document.forms['servicePartnerForm'].elements['servicePartner.status'].checked=false;
				}
		}
	}

 
 	try{
 	if(document.forms['servicePartnerForm'].elements['servicePartner.clone'].checked == true){
		document.getElementById("disDiv1").style.display = "block";
	}else{
		document.getElementById("disDiv1").style.display = "none";
	}	
}
catch(e){}
     Form.focusFirstElement($("servicePartnerForm"));
</script>
<script type="text/javascript">

	var fieldName = document.forms['servicePartnerForm'].elements['field'].value;

	var fieldName1 = document.forms['servicePartnerForm'].elements['field1'].value;
	if(fieldName!=''){
	document.forms['servicePartnerForm'].elements[fieldName].className = 'rules-textUpper';
	}
	if(fieldName1!=''){
	document.forms['servicePartnerForm'].elements[fieldName1].className = 'rules-textUpper';
	}
</script>
<script type="text/javascript">
	<c:if test="${hitFlag=='1'}">
	  window.close();
	  try {
			window.opener.getRoutingDetails();
		}catch(e){
			window.opener.document.forms[0].submit();
		}
	</c:if>
</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
	checkMandatoryFieldUGCA();
</script>


<script type="text/javascript">
var temp = "${serviceOrder.routing}";
var temp1 = "${serviceOrder.originCountryCode}";
try{
if((temp=='EXP') && (temp1=='USA' || temp1=='CAN'))	{
	document.getElementById('caedField').style.display="block";
	   document.getElementById('caedField1').style.display="block";
	}else{
	document.getElementById('caedField').style.display="none";
	   document.getElementById('caedField1').style.display="none";
	}
}catch(e){}


function checksubCarrierName(){
	var vendorId = document.forms['servicePartnerForm'].elements['servicePartner.subcarrierCode'].value;
    	if(vendorId == ''){
		document.forms['servicePartnerForm'].elements['servicePartner.subcarrierName'].value="";
	}
	if(vendorId != ''){
    var url="checkVendorNameForAgent.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId)+"&soCorpID=${serviceOrder.corpID}&pType=CR";
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse234;
     http3.send(null);
     progressBarAutoSave('1');
 	}else{
 	} 
} 

function handleHttpResponse234(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                var res = results.split("#"); 
               // alert(res)               
		           if(res.size() >= 2){ 
		        	   
		           		if(res[2] == 'Approved'){
		           			document.forms['servicePartnerForm'].elements['servicePartner.subcarrierName'].value = res[1];
		           			document.forms['servicePartnerForm'].elements['servicePartner.subcarrierCode'].select();
		           		 	progressBarAutoSave('0');
		           		  		}else{
		           			alert("Sub Carrier code is not approved" ); 
						    document.forms['servicePartnerForm'].elements['servicePartner.subcarrierName'].value="";
						    document.forms['servicePartnerForm'].elements['servicePartner.subcarrierCode'].value="";
						    document.forms['servicePartnerForm'].elements['servicePartner.subcarrierCode'].select();
						    progressBarAutoSave('0');
		           		}
                }else{
                     alert("Sub Carrier code not valid" );
                     document.forms['servicePartnerForm'].elements['servicePartner.subcarrierName'].value="";
                     document.forms['servicePartnerForm'].elements['trackingUrl'].value = "";
					 document.forms['servicePartnerForm'].elements['servicePartner.subcarrierCode'].value="";
					 document.forms['servicePartnerForm'].elements['servicePartner.subcarrierCode'].select();
					 progressBarAutoSave('0');
			   }
     }
} 
</script>