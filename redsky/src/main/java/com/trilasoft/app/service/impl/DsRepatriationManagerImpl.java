package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.DsRepatriationDao;
import com.trilasoft.app.model.DsRepatriation;
import com.trilasoft.app.service.DsRepatriationManager;

public class DsRepatriationManagerImpl extends GenericManagerImpl<DsRepatriation, Long>  implements DsRepatriationManager{
	DsRepatriationDao dsRepatriationDao;
	
	public DsRepatriationManagerImpl(DsRepatriationDao dsRepatriationDao) {
		super(dsRepatriationDao);
		this.dsRepatriationDao=dsRepatriationDao;
			}
	
	public List getListById(Long id) {
				return dsRepatriationDao.getListById(id);
	}
}
