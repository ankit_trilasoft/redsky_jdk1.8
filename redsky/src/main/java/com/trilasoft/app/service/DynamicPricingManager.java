package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.DynamicPricing;

public interface DynamicPricingManager extends GenericManager<DynamicPricing, Long>{
	public DynamicPricing getDynamicPricingByType(String type,String sessionCorpID);
	public List getDynamicPricingCalenderByType(String type,String sessionCorpID);
}
