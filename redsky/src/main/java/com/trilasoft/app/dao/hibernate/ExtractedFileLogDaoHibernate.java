package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ExtractedFileLogDao;
import com.trilasoft.app.model.ExtractedFileLog;



public class ExtractedFileLogDaoHibernate extends GenericDaoHibernate<ExtractedFileLog, Long> implements ExtractedFileLogDao  {

	public ExtractedFileLogDaoHibernate() {
		super(ExtractedFileLog.class);
		
	}

	public List<ExtractedFileLog> searchExtractedFileLog(String sessionCorpID, String moduleName, String extractCreatedbyFirstName, String extractCreatedbyLastName, String fromExtractDate, String toExtractDate) {
		StringBuffer condition = new StringBuffer();
		String usernames="";
		String username="";
		List users = new ArrayList();
		if ( fromExtractDate ==null || fromExtractDate.toString().equals("")) {

		} else { 
			condition.append(" and (DATE_FORMAT(createdOn,'%Y-%m-%d') >= '" + fromExtractDate + "') ");
			
		}
		if ( toExtractDate ==null || toExtractDate.toString().equals("")) {

		} else { 
				condition.append(" and (DATE_FORMAT(createdOn,'%Y-%m-%d') <= '" + toExtractDate + "') ");  
		}
        if((extractCreatedbyFirstName!=null && (!(extractCreatedbyFirstName.trim().equals("")))) || (extractCreatedbyLastName!=null && (!(extractCreatedbyLastName.trim().equals(""))))){
		users = getHibernateTemplate().find("select username from User where userType like 'USER' AND  firstName like '%" + extractCreatedbyFirstName.replaceAll("'", "''").replaceAll( ":", "''") + "%' AND  lastName like '%" + extractCreatedbyLastName.replaceAll("'", "''").replaceAll( ":", "''") + "%' AND enabled=true ");
		if(users!=null && (!users.isEmpty())){
			Iterator jobListIterator=users.iterator(); 
			while(jobListIterator.hasNext()){
				username=(String)jobListIterator.next();
				if(!(usernames.lastIndexOf("'") == usernames.length() - 1))
					usernames=usernames+"','"+username+"','";
				else if(usernames.lastIndexOf("'") == usernames.length() - 1) 
					usernames=usernames+username+"','";
			}
			
		if(usernames.lastIndexOf("'") == usernames.length() - 1) 
		   {
			usernames = usernames.substring(0, usernames.length() - 1);
		    }
		if(usernames.lastIndexOf(",") == usernames.length() - 1) 
		   {
			usernames = usernames.substring(0, usernames.length() - 1);
		    } 
		if(usernames.lastIndexOf("'") == usernames.length() - 1) 
		   {
			usernames = usernames.substring(0, usernames.length() - 1);
		    }
        }
        }
		String query="";
		if(usernames==null || usernames.trim().equals("")){
			query="from ExtractedFileLog  where corpID ='"+sessionCorpID+"' and module like '" + moduleName.replaceAll("'", "''") + "%'  "+condition;	
		}else{
		   query="from ExtractedFileLog  where corpID ='"+sessionCorpID+"' and module like '" + moduleName.replaceAll("'", "''") + "%' and createdBy in ('" + usernames + "') "+condition;
		} 
		return getHibernateTemplate().find(query); 
	}
	
	public List getFileLogListByModule(String sessionCorpID,String moduleName){
		String query="from ExtractedFileLog where corpID='"+sessionCorpID+"' and module='"+moduleName+"' order by id desc";
		List list = getHibernateTemplate().find(query);
		return list;
	}

}
