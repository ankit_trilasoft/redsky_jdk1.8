<%@page import="java.util.SortedMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.lang.*"%>

<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
 <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr>
	<td align="left"  style="padding-left:5px;min-width:120px;">
		<b>Resource Detail List</b>
	</td>
	<td align="right"  style="padding-right:5px;">
		<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>


<table style="width:350px" cellpadding="0" cellspacing="0">
		<tr>
				<td valign="top" style="padding-right:0px;">
					<table class="table bleft">
						<thead>
							<tr>
								<th style="border-right:none;">Crew</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="individualItem" items="${crewDetailList}" >	
								 <tr>
								 <td class="listwhitetext bright" align="right">${individualItem.descript}&nbsp;&nbsp;${individualItem.qty}</td>
								 </tr>		 
							</c:forEach>
						</tbody>		 		  
					</table>
				</td>
				
				<td valign="top" style="padding-left:0px;">
					<table class="table bleft" >
						<thead>
							<tr>
								<th style="border-right:none;">Vehicle</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="individualItem" items="${vehicleDetailList}" >	
								<tr>
								<td  class="listwhitetext bright" align="right" >${individualItem.descript}&nbsp;&nbsp;${individualItem.qty}</td>
								</tr>		 
							</c:forEach>
						</tbody>		 		  
					</table>
				</td>
				
				<td valign="top" style="padding-left:0px;">
					<table class="table bleft" >
						<thead>
							<tr>
								 <th style="border-right:none;">Equipment</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="individualItem" items="${equipmentDetailList}" >	
								<tr>
								<td  class="listwhitetext bright" align="right" >${individualItem.descript}&nbsp;&nbsp;${individualItem.qty}</td>
								</tr>		 
							</c:forEach>
						</tbody>		 		  
					</table>
				</td>
				
				<td valign="top" style="padding-left:0px;">
					<table class="table bleft" >
						<thead>
							<tr>
								<th width="">Material</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="individualItem" items="${materialDetailList}" >	
								<tr>
								<td  class="listwhitetext bright" align="right" >${individualItem.descript}&nbsp;&nbsp;${individualItem.qty}</td>
								</tr>		 
							</c:forEach>
						</tbody>		 		  
					</table>
				</td>
		</tr>
</table>

