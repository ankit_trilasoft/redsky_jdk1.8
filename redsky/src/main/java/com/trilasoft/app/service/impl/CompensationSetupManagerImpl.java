package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CompensationSetupDao;
import com.trilasoft.app.model.CompensationSetup;
import com.trilasoft.app.service.CompensationSetupManager;

public class CompensationSetupManagerImpl extends GenericManagerImpl<CompensationSetup,Long > implements CompensationSetupManager {
	CompensationSetupDao  compensationSetupDao;
	public CompensationSetupManagerImpl(CompensationSetupDao  compensationSetupDao) {
		super(compensationSetupDao);
		this.compensationSetupDao=compensationSetupDao;
	}
	public List getListFromCompensationSetup(String corpId, Long contractId,String contractName){
		return compensationSetupDao.getListFromCompensationSetup( corpId, contractId,contractName);
	}
	
}
