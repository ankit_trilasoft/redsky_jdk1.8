package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ScreenConfigurationDao;
import com.trilasoft.app.model.ScreenConfiguration;
import com.trilasoft.app.service.ScreenConfigurationManager;




public class ScreenConfigurationManagerImpl  extends GenericManagerImpl<ScreenConfiguration, Long> implements ScreenConfigurationManager {

	ScreenConfigurationDao screenConfigurationDao; 
    
    
    public ScreenConfigurationManagerImpl(ScreenConfigurationDao screenConfigurationDao) { 
        super(screenConfigurationDao); 
        this.screenConfigurationDao = screenConfigurationDao; 
    } 

    public List checkById(Long id) { 
        return screenConfigurationDao.checkById(id); 
    }
    public List<ScreenConfiguration>searchByCorpIdTableAndField(String corpId,String tableName,String fieldName) 
    {
    	return screenConfigurationDao.searchByCorpIdTableAndField(corpId, tableName, fieldName);
    }
}  