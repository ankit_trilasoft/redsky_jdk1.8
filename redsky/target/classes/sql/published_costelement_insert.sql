DELIMITER $$

DROP PROCEDURE IF EXISTS `redsky`.`published_costelement_insert`$$
CREATE DEFINER=`root`@`%` PROCEDURE  `redsky`.`published_costelement_insert`(l_contract varchar(500), fromcorpid varchar(4), tocorpid varchar(4),l_createdby varchar(100))
BEGIN
DECLARE bDone INT;
DECLARE l_recGL VARCHAR(16);
DECLARE l_paygl VARCHAR(16);
DECLARE l_costelement VARCHAR(50);
DECLARE l_description VARCHAR(50);
DECLARE l_reportingAlias VARCHAR(50);


declare curs CURSOR FOR  select recGl,payGl,costelement,description,reportingAlias from costelement where corpId=fromcorpid and trim(costElement) not in
(select trim(costElement) from costelement where corpID=tocorpid and costElement <>'' and costElement is not null);

declare CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;

OPEN curs;

  SET bDone = 0;
  REPEAT
    FETCH curs INTO l_recGL,l_paygl, l_costelement,l_description,l_reportingAlias;

    update charges set gl=l_recGL,expgl=l_paygl where contract=l_contract and corpid=tocorpid and costElement=l_costElement;
    if(l_costelement<>'') then
    insert into costelement (corpid,costelement,description,createdBy,createdon,updatedby,updatedon,reportingAlias)
    VALUES (tocorpid,l_costelement,l_description,l_createdby,now(),l_createdby,now(),l_reportingAlias);
    end if;
  UNTIL bDone END REPEAT;

  CLOSE curs;


END $$

DELIMITER ;