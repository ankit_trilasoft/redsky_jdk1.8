ALTER TABLE `redsky`.`company` ADD COLUMN `billToExtractDate` DATETIME
NULL  AFTER `corporateId` , ADD COLUMN `vendorExtractDate` DATETIME
NULL  AFTER `billToExtractDate` ; 

ALTER TABLE `redsky`.`operationsintelligence` ADD INDEX `Index_shipNumber` (`shipNumber`) ;

ALTER TABLE `redsky`.`claim` ADD COLUMN `claimUID` VARCHAR(45) NULL  AFTER `goodWillPayment` ;