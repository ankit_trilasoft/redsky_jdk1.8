package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.InventoryArticleDao;
import com.trilasoft.app.model.InventoryArticle;

public class InventoryArticleDaoHibernate extends GenericDaoHibernate<InventoryArticle,Long> implements InventoryArticleDao{
	public InventoryArticleDaoHibernate() {
		super(InventoryArticle.class);
	}
	class DTO{
		private Object article;
		private  Object volume;
		public Object getArticle() {
			return article;
		}
		public void setArticle(Object article) {
			this.article = article;
		}
		public Object getVolume() {
			return volume;
		}
		public void setVolume(Object volume) {
			this.volume = volume;
		}
}
	 public List getArticleAutoComplete(String sessionCorpID,String article){
		List dtoList=new ArrayList();		
		 List list = this.getSession().createSQLQuery("select distinct atricle,concat(volume,'~',weight) from inventoryarticle where corpID='"+sessionCorpID+"' and atricle like '"+article.replaceAll("'", "\\\\'")+"%';").list();
	    Iterator itr=list.iterator();
	    while(itr.hasNext()){
	    DTO dto=new DTO();
	    Object[] obj=(Object[])itr.next();
	    dto.setArticle(obj[0].toString());
	    dto.setVolume(obj[1].toString());
	    dtoList.add(obj);
	    }
	    return dtoList;
	 }
}
