package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.ServiceOrder;

public interface NotesManager extends GenericManager<Notes, Long> {

	public List<Notes> findByNotes(String noteStatus, String noteSubType, String notesId);

	public List findMaxId();

	public List getListByNotesId(String notesId, String subType);

	public List getListByAccountNotesId(String notesId, String subType, String type, Long kId);

	public List getListByCustomerNumber(String customerNumber, String subType);

	public List<Notes> findByReleventNotes(String noteStatus, String noteSubType, String noteId);

	public List countForNotes(String seqNum);

	public List countForOriginNotes(String seqNum);

	public List countForDestinationNotes(String seqNum);

	public List countForSurveyNotes(String seqNum);

	public List countForVipNotes(String seqNum);

	public List countForVipDetailNotes(String shipNum);

	public List countForOriginDetailNotes(String shipNum);

	public List countForDestinationDetailNotes(String shipNum);

	public List countForWeightDetailNotes(String shipNum);

	public List countForBillingNotes(String seqNum);

	public List countForEntitlementNotes(String seqNum);

	public List countForContactNotes(String seqNum);

	public List countForSpouseNotes(String seqNum);

	public List countForCportalNotes(String seqNum);

	public List countForServiceOrderNotes(String shipNum);

	public List countForServiceNotes(Long id);
	public List countForCrewNotes(Long id);

	public List countForCarrierNotes(Long id);

	public List countForContainerNotes(Long id);

	public List countForVehicleNotes(Long id);

	public List countForCartonNotes(Long id);

	public List countForCategoryTypeNotes(Long id);

	public List countForEntitleDetailNotes(Long id);

	public List countForEstimateDetailNotes(Long id);

	public List countForRevisionDetailNotes(Long id);

	public List countForPayableDetailNotes(Long id);

	public List countForReceivableDetailNotes(Long id);

	public List countBillingAuthorizedNotes(String shipNum);

	public List countBillingValuationNotes(String shipNum);

	public List countBillingSecondaryNotes(String shipNum);

	public List countBillingPrimaryNotes(String shipNum);

	public List countBillingDomesticNotes(String shipNum);

	public List countBillingPrivatePartyNotes(String shipNum);

	public List notesBillingCompleteStatus(String shipNum);

	public List countForTicketBillingNotes(Long ticket);

	public List countForTicketStaffNotes(Long ticket);

	public List countForTicketSchedulingNotes(Long ticket);

	public List countForTicketOriginNotes(Long ticket);

	public List countForTicketDestinationNotes(Long ticket);

	public List countForTicketNotes(Long ticket);

	public List countTicketLocationNotes(Long ticket);

	public List countTicketStorageNotes(Long ticket);

	public List countOriginPackoutNotes(String shipNum);

	public List countSitOriginNotes(String shipNum);

	public List countOriginForwardingNotes(String shipNum);

	public List countInterStateNotes(String shipNum);

	public List countGSTForwardingNotes(String shipNum);

	public List countDestinationStatusNotes(String shipNum);

	public List countSitDestinationNotes(String shipNum);

	public List countDestinationImportNotes(String shipNum);

	public List countServiceOrderNotes(String shipNum);
	
	public List<Notes> countNotesByShipNumAndSubType(String shipNum,String subType);

	public List countForClaimNotes(Long claimNum);

	public List countClaimValuationNotes(Long claimNum);

	public List countLossNotes(Long claimNum);

	public List countLossTicketNotes(Long claimNum);

	public List countForDomesticNotes(String shipNum);

	public List countForDomesticServiceNotes(String shipNum);

	public List countForDomesticStateCountryNotes(String shipNum);

	public List countCreditCardNotes(Long creditCardId);

	public List<CustomerFile> getCustomerFilesDetails(String customerNumber);
	
	public List<ServiceOrder> getServiceOrederDetils(String notesId);

	public void updateNotesFromRules(String note,String subject, Long id, String forwardUser, String updatedBy, String remindInterval,String noteType, String noteSubType, String linkedTo,String supplier, String grading, String issueType);

	public void updateForwardDateFromRules(String forwardDate, String remindTime, Long id);

	public void dismissReminder(Long id);

	public List findByUserActivity(String userName, String timeZone);

	public List countAccountProfileNotes(String partnerCode, String sessionCorpID);

	public List countAccountContactNotes(String partnerCode, String sessionCorpID,Long kId);

	public List countForIMFEntitlementNotes(String shipNum);

	public List countCustomerFeedbackNotes(String shipNum);

	public List getRulesNotes(String fileNumber, String tdrID, String sessionCorpID);

	public void updateResult(String resultRecordId, String sessionCorpID);
	
	public void updateCheckListResult(String resultRecordId, String sessionCorpID);

	public List userSignatureNotes(String username);

	public List findMaximumId(String fileNumber);

	public List findMinimumId(String fileNumber);

	public List findCountId(String fileNumber);

	public List goNotes(String fileNumber, Long id, String corpID);

	public List goNextNotes(String fileNumber, Long id, String corpID);

	public List goPrevNotes(String fileNumber, Long id, String corpID);

	public List notesNextPrev(String fileNumber, Long id, String corpID);

	public List findMaximumIdIsRal(String fileNumber);

	public List findMinimumIdIsRal(String fileNumber);

	public List findCountIdIsRal(String fileNumber);

	public List goNextNotesIsRal(String fileNumber, Long id, String corpID);

	public List goPrevNotesIsRal(String fileNumber, Long id, String corpID);

	public List notesNextPrevIsRal(String fileNumber, Long id, String corpID);

	public List countForPartnerNotes(String partnerCode);

	public List getPartnerNotesListByCustomerNumber(CustomerFile customerFile);

	public List findPartnerAlertList(String notesId);
	
	public List countForDSNotes(String sequenceNumber);

	public List countForDSPreviewTripNotes(String sequenceNumber);
	public List countForDSTemporaryAccommodationNotes(String sequenceNumber);
	public List countForDSOngoingSupportNotes(String sequenceNumber);
	public List countForDSColaServiceNotes(String sequenceNumber);
	public List countForDspDetailsNotes(String sequenceNumber,String dspNoteType);	
	public List countForDSAutomobileAssistanceNotes(String sequenceNumber);
	public List countForDSMoveMgmtNotes(String sequenceNumber);
	public List countForDSTenancyManagementNotes(String sequenceNumber);
	public List countForDSSchoolEducationalCounselingNotes(String sequenceNumber);
	
	public List countForDSRelocationExpenseManagementNotes(String sequenceNumber);	
		
	public List countForDSHomeFindingAssistancePurchaseNotes(String sequenceNumber);	

	public List countForDSVisaImmigrationNotes(String sequenceNumber);

	public List countRelocationAreaInfoOrientationNotes(String shipNumber);

	public List countForDSTaxServicesNotes(String shipNumber);

	public List countForDSHomeRentalNotes(String shipNumber);
	
	public List countForDSLanguageNotes(String shipNumber);
	public List countForDSCrossCulturalTrainingNotes(String shipNumber);
	public List countForCountDSRepatriationNotes(String shipNumber);

	public List countForAccountPortalNotes(String sequenceNumber);

	public void updateNoteFileList(Long id ,String myFileIds, String sessionCorpID);

	public List getListById(Long id, String sessionCorpID);

	public List getMyFileList(Long id);

	public Map<String, String> findSupplier(Long id1);
	
	public String findDistinctAgentName(String corpId, String spNumber, String cid);

	public List getServiceOrderNotesList(String shipNumber, String type, Boolean bookingAgent, Boolean networkAgent, Boolean originAgent, Boolean subOriginAgent, Boolean destAgent,	Boolean subDestAgent);

	public void upDateNetworkLinkId(String networkLinkId, Long id);

	public List findlinkedIdList(String networkLinkId);

	public List getSurveyNotes(String sequenceNumber, Long cid, String corpID);
	public String findSubjectDescForNotes(Long id, String sessionCorpID);
	public String findNoteDescForNotes(Long id, String sessionCorpID);
	public List countForMssNotes(Long id);
	public List getListForMss(Long notesId, String subType);
	
	public List<Notes> getNoteByNotesId(Long notesId, String corpId,String notesSubType);
	public List getLinkedId(String networkLinkId, String sessionCorpID);

	public Map<String, String> countSoDetailNotes(String shipNumber,String noteSubType,String sessionCorpID);
	public Map<String, String> getSubCategoryRlo(String shipNumber,String sessionCorpID);
	public Map<String, String> getNoteGrading(String cfBillToCode,String sessionCorpID);
	public Map<String, String> getNoteGradinglinkedTo(String spNumber,String sessionCorpID);
	public List getALLListByNotesId(String notesId, String subType);
	public void disableNotesForGivenOrder(String corpId,String idList);
	public List<Notes> findByNotesAll(String noteStatus, String noteSubType, String notesId);
	public List findPartnerAlertListForCf(String notesId);
	public List countForOINotes(Long sId);
    public List getListForOI(Long id, String subType); 
	//13674 - Create dashboard
	public String getNotesCount(String sessionCorpID,String shipNumber,Long id);
	public String getnoteDestCount(String sessionCorpID,String shipNumber,Long id);
	public String getNoteForwardCount(String sessionCorpID,Long id);
	//End dashboard
	public String accountLineNotesStatus(String id,String sessionCorpID);
	public List getALLListByFamilyDetails(String notesId, String subType);
}
