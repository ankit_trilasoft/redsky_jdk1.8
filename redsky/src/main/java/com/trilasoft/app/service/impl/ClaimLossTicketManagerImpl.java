package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Set;

import com.trilasoft.app.dao.ClaimLossTicketDao; 
import com.trilasoft.app.model.Claim;
import com.trilasoft.app.model.ClaimLossTicket; 
import com.trilasoft.app.model.Loss;
import com.trilasoft.app.service.ClaimLossTicketManager; 
import org.appfuse.service.impl.GenericManagerImpl;  

 
public class ClaimLossTicketManagerImpl extends GenericManagerImpl<ClaimLossTicket, Long> implements ClaimLossTicketManager { 
	ClaimLossTicketDao claimLossTicketDao; 
    
 
    public ClaimLossTicketManagerImpl(ClaimLossTicketDao claimLossTicketDao) { 
        super(claimLossTicketDao); 
        this.claimLossTicketDao = claimLossTicketDao; 
    } 
    
    public List findClaimLossTickets(String lossNumber, Long claimNum, String corpID) {
    	return claimLossTicketDao.findClaimLossTickets(lossNumber, claimNum, corpID);
    	}
	 
    public boolean findTicketbyLoss(Long ticket, String seqNum, String lossNum){
		return claimLossTicketDao.findTicketbyLoss(ticket, seqNum, lossNum);
	}
	
    public int applyTicketToAllLoss(Long ticket, String seqNum, String lossNum){
		return claimLossTicketDao.applyTicketToAllLoss(ticket, seqNum, lossNum);
	}
    public List<Claim> findClaimBySeqNum(String sequenceNumber){
    	return claimLossTicketDao.findClaimBySeqNum(sequenceNumber);
    }
    public List<Loss> findLossBySeqNum(String sequenceNumber){
    	return claimLossTicketDao.findLossBySeqNum(sequenceNumber);
    	
    }
    public boolean findLossTicket(Long ticket, String seqNum, Long claimNum, String lossNum){
    	return claimLossTicketDao.findLossTicket(ticket, seqNum, claimNum, lossNum);
    }
    public List findCustLossTickets(Long ticket, String seqNum, boolean isAppliedToAllLoss){
    	return claimLossTicketDao.findCustLossTickets(ticket, seqNum, isAppliedToAllLoss);
    	
    }
	public List findCustClmLossTickets(String seqNum, String corpID){
    	return claimLossTicketDao.findCustClmLossTickets(seqNum, corpID);
    	
    }
	
	public List findCustClmLossPercent(Long claimNum, Long ticket, String corpID){
    	return claimLossTicketDao.findCustClmLossPercent(claimNum, ticket, corpID);
    }
	public List findCustClmLossChargeBack(Long claimNum, Long ticket, String corpID){
    	return claimLossTicketDao.findCustClmLossChargeBack(claimNum, ticket, corpID);
    }
	public int applyTicketToAllLossOldAlso(Long ticket, Long claimNum, String lossNum, String corpID){
		return claimLossTicketDao.applyTicketToAllLossOldAlso(ticket, claimNum, lossNum, corpID);	
	}
	public List<Loss> findLossByClaimNum(Long claimNum, String corpID) {
		return claimLossTicketDao.findLossByClaimNum(claimNum, corpID);
	}
	public void removeCLTByAction(String lossNum, String corpID){
		 claimLossTicketDao.removeCLTByAction(lossNum, corpID);
	}
}

