package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.CompareToBuilder;
@Entity
@Table(name="worktickettimemanagement")
public class WorkTicketTimeManagement extends BaseObject implements Comparable<WorkTicketTimeManagement> {
	private Long id;
	private Long workTicketId;
	private String corpId;
	private Long ticket;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private String startTime;
	private String endtime;
	private String timeRequirements;
	
	
	
	public int compareTo(final WorkTicketTimeManagement other) {
		return new CompareToBuilder().append(id, other.id)
				.append(workTicketId, other.workTicketId)
				.append(corpId, other.corpId).append(ticket, other.ticket)
				.append(createdOn, other.createdOn)
				.append(createdBy, other.createdBy)
				.append(updatedOn, other.updatedOn)
				.append(updatedBy, other.updatedBy)
				.append(startTime, other.startTime)
				.append(endtime, other.endtime)
				.append(timeRequirements, other.timeRequirements)
				.toComparison();
	}
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("workTicketId", workTicketId).append("corpId", corpId)
				.append("ticket", ticket).append("createdOn", createdOn)
				.append("createdBy", createdBy).append("updatedOn", updatedOn)
				.append("updatedBy", updatedBy).append("startTime", startTime)
				.append("endtime", endtime)
				.append("timeRequirements", timeRequirements).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof WorkTicketTimeManagement))
			return false;
		WorkTicketTimeManagement castOther = (WorkTicketTimeManagement) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(workTicketId, castOther.workTicketId)
				.append(corpId, castOther.corpId)
				.append(ticket, castOther.ticket)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(startTime, castOther.startTime)
				.append(endtime, castOther.endtime)
				.append(timeRequirements, castOther.timeRequirements)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(workTicketId)
				.append(corpId).append(ticket).append(createdOn)
				.append(createdBy).append(updatedOn).append(updatedBy)
				.append(startTime).append(endtime).append(timeRequirements)
				.toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Long getWorkTicketId() {
		return workTicketId;
	}
	public void setWorkTicketId(Long workTicketId) {
		this.workTicketId = workTicketId;
	}
	@Column
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column
	public Long getTicket() {
		return ticket;
	}
	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	@Column
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	@Column
	public String getTimeRequirements() {
		return timeRequirements;
	}
	public void setTimeRequirements(String timeRequirements) {
		this.timeRequirements = timeRequirements;
	}
	
}
