package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name = "entitlement")

public class Entitlement extends BaseObject {

	private Long id;

	private String eOption;

	private String eDescription;

	private Long partnerPrivateId;
	
	private String partnerCode;
	
	private Long customerFileId;

	private String corpID;

	private String updatedBy;

	private String createdBy;

	private Date createdOn;

	private Date updatedOn;
	private Long parentId=0L;

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("eOption", eOption)
				.append("eDescription", eDescription)
				.append("partnerPrivateId", partnerPrivateId)
				.append("partnerCode", partnerCode)
				.append("corpID", corpID)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("createdBy", createdBy)
		        .append("createdOn",createdOn).append("parentId",parentId).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Entitlement))
			return false;
		Entitlement castOther = (Entitlement) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(eOption, castOther.eOption)
				.append(eDescription, castOther.eDescription)
				.append(partnerPrivateId, castOther.partnerPrivateId)
				.append(partnerCode, castOther.partnerCode)
				.append(corpID, castOther.corpID)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(parentId, castOther.parentId)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(eOption)
				.append(eDescription).append(partnerPrivateId).append(corpID)
				.append(updatedBy)
				.append(createdBy)
				.append(createdOn).append(updatedOn).append(partnerCode).append(parentId)
				.toHashCode();
	}

	@Column(name="corpID", length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name="eOption", length = 200)
	public String geteOption() {
		return eOption;
	}

	public void seteOption(String eOption) {
		this.eOption = eOption;
	}

	@Column(name="eDescription", length = 500)
	public String geteDescription() {
		return eDescription;
	}

	public void seteDescription(String eDescription) {
		this.eDescription = eDescription;
	}

	@Column(name="partnerPrivateId", length = 82)
	public Long getPartnerPrivateId() {
		return partnerPrivateId;
	}

	public void setPartnerPrivateId(Long partnerPrivateId) {
		this.partnerPrivateId = partnerPrivateId;
	}
	
	@Column(length=20)
	public Long getCustomerFileId() {
		return customerFileId;
	}

	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}
	
	@Column(length=20)
	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

}
