<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<meta name="heading" content="Email"/> 
<title>Email</title> 
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
</head>

<style type="text/css">h2 {background-color: #FBBFFF}</style>
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>	
	
<s:form id="emailPage" name="emailPage" action="saveAgentEmailPage.html?bypass_sessionfilter=YES&decorator=popup&popup=true" method="post" validate="true" >
<c:set var="sessionCorpID" value="<%=request.getParameter("sessionCorpID") %>" />
<s:hidden name="sessionCorpID" value="<%=request.getParameter("sessionCorpID") %>" />
<div id="layer1"  style="width:100%;margin-bottom:1px; ">
 	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:5px;"><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="1" cellpadding="1" border="0" style="" width="100%">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext" width="100%">
	  	<table class="detailTabLabel" border="0">
			<tbody>  	
				  	<tr>
				  		<td align="left" height="3px" colspan="4"></td>
				  	</tr>
				  								
				</tbody>
			</table>
		  
		<table class="detailTabLabel" border="0" width="100%">
		<tbody> 
			  			   
			
			  <tr><td class="listwhitetext" colspan="6" style="font-size: 14px; font-weight: bold">Company Information:</td></tr>
			  <tr><td class="vertlinedata" colspan="6"></td></tr>
			  <tr>
			  	<td align="left" colspan="6" class="listwhitetext" height="5px"></td>		  		
			  	</tr>
			  	<tr>
			  	<td align="left" colspan="6" class="listwhitetext"><b>Company Name</b><font color="red" size="2">*</font></td>		  		
			  	</tr>
			  	<tr>			  	
		  		<td align="left" colspan="6"><s:textfield name="userBranch"  maxlength="100" size="40" cssClass="input-text" readonly="false" tabindex="1"/></td>
			  	</tr>
			  	<tr><td class="listwhitetext"><b>Address</b><font color="red" size="2">*</font></td>
				<tr>
					<td colspan="2"><s:textfield cssClass="input-text" name="userAddress1"  size="40" maxlength="30" tabindex="2" /></td>
					<td align="right" class="listwhitetext"><fmt:message key='partner.billingCountryCode'/><font color="red" size="2">*</font></td>
					<td ><s:select cssClass="list-menu" id="userCountry" name="userCountry" list="%{ocountry}" tabindex="3" headerKey="" headerValue="" cssStyle="width:163px"  onchange="displayState();getState(this);"/></td>
					<td align="right" class="listwhitetext"><fmt:message key='partner.billingFax'/></td>
					<td><s:textfield cssClass="input-text" name="userFax" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="4" /></td>
				</tr>
				<tr>
					
					<td colspan="2"><s:textfield cssClass="input-text" name="userAddress2" size="40" maxlength="30" tabindex="5"/></td>
					
					<td align="right" class="listwhitetext"><fmt:message key='partner.billingState'/></td>
					<td><s:select cssClass="list-menu" id="userState" name="userState" list="%{states}" headerKey="" headerValue="" cssStyle="width:163px" tabindex="6"/></td>
					
					
					<td align="right" class="listwhitetext"><fmt:message key='partner.billingPhone'/><font color="red" size="2">*</font></td>
					<td><s:textfield cssClass="input-text" name="userPhone" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="7" /></td>
				</tr>
				<tr>
					
					<td colspan="2"><s:textfield cssClass="input-text" name="userAddress3" size="40" maxlength="30" tabindex="8" /></td>
					
					<td align="right" class="listwhitetext"><fmt:message key='partner.billingCity'/><font color="red" size="2">*</font></td>
					<td><s:textfield cssClass="input-text" name="userCity" cssStyle="width:160px" maxlength="20" onkeydown="return onlyCharsAllowed(event)" tabindex="9" /></td>
												
					<td align="right" class="listwhitetext"><fmt:message key='partner.billingTelex'/></td>
					<td><s:textfield cssClass="input-text" name="userTelex" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex="10"/></td>
				</tr>
				<tr>
					
					<td colspan="2"><s:textfield cssClass="input-text" name="userAddress4" size="40" maxlength="30" tabindex="11" /></td>
					<td align="right" class="listwhitetext"><fmt:message key='partner.billingZip'/></td>
					<td><s:textfield cssClass="input-text" name="userZip" cssStyle="width:160px"  maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="12" /></td>
					<td align="right" class="listwhitetext"><fmt:message key='partner.billingEmail'/><font color="red" size="2">*</font></td>
					<td><s:textfield cssClass="input-text" name="userEmail"  size="30" maxlength="65" tabindex="13" onchange="return echeck()"/></td>
				</tr>
			  	<tr>
			  	<td align="left" colspan="6" class="listwhitetext"><b>Message</b></td>		  		
			  	</tr>
			  	<tr>
			  		<td align="left" colspan="6"><s:textarea name="emailMessage"  tabindex="14" onkeypress="return imposeMaxLength(this,1900);" rows="10" cols="130" readonly="false" cssClass="textarea"/></td>
			  	</tr>
		</tbody>
  </table>	  
		
		
		
		</td>
		</tr>
		
		</tbody>
		
	</table>
		 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
	
	<table class="detailTabLabel" border="0">
				<tbody>
					
				<tr align="center">
				<td align="left"> 
					<input type="submit" class="cssbutton1" name="Send" style="width: 50px" value="Send" onclick="return submitAction()"/>
				</td>   
			</tr>
		</tbody>
	</table>

<div id="mydiv" style="position:absolute"></div>
</s:form>

<%-- Script Shifted from Top to Botton on 10-Sep-2012 By Kunal --%>
<script>
	function submitAction(){
		if(document.forms['emailPage'].elements['userBranch'].value==''){
			alert('Please enter company name');
			return false;
		}
		if(document.forms['emailPage'].elements['userAddress1'].value==''){
			alert('Please enter address');
			return false;
		}
		if(document.forms['emailPage'].elements['userCountry'].value==''){
			alert('Please select country');
			return false;
		}
		if(document.forms['emailPage'].elements['userCity'].value==''){
			alert('Please enter city');
			return false;
		}
		if(document.forms['emailPage'].elements['userPhone'].value==''){
			alert('Please enter phone number');
			return false;
		}
		if(document.forms['emailPage'].elements['userEmail'].value==''){
			alert('Please enter email address');
			return false;
		}
		
		return echeck();
	}
	
	function echeck() {
		var str=document.forms['emailPage'].elements['userEmail'].value;
		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   alert("Invalid E-mail ID");
		   document.forms['emailPage'].elements['userEmail'].focus();
		   return false;
		}
		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   alert("Invalid E-mail ID");
		    document.forms['emailPage'].elements['userEmail'].focus();
		   return false;
		}
		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    alert("Invalid E-mail ID");
		     document.forms['emailPage'].elements['userEmail'].focus();
		    return false;
		}
		 if (str.indexOf(at,(lat+1))!=-1){
		    alert("Invalid E-mail ID");
		    document.forms['emailPage'].elements['userEmail'].focus();
		    return false;
		 }
		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    alert("Invalid E-mail ID");
		     document.forms['emailPage'].elements['userEmail'].focus();
		    return false;
		 }
		 if (str.indexOf(dot,(lat+2))==-1){
		    alert("Invalid E-mail ID");
		    document.forms['emailPage'].elements['userEmail'].focus();
		    return false;
		 }
		 if (str.indexOf(" ")!=-1){
		    alert("Invalid E-mail ID");
		    document.forms['emailPage'].elements['userEmail'].focus();
		    return false;
		 }
 		 return true;					
	}
	
	function displayState(){
		var country=document.forms['emailPage'].elements['userCountry'].value;
		var enbState = '${enbState}';
		  var index = (enbState.indexOf(country)> -1);
	 	if(index != '' && country!=''){
			document.forms['emailPage'].elements['userState'].disabled=false;
		}else{
			document.forms['emailPage'].elements['userState'].disabled=true;
		}
	
	}
	function getState(targetElement) {
		var country = targetElement.value;
		var enbState = '${enbState}';
		 var index = (enbState.indexOf(country)> -1);
		 if(index != ''){
	     var url="findStateService.html?bypass_sessionfilter=YES&sessionCorpID=${sessionCorpID}&decorator=simple&popup=true&bucket2="+encodeURI(country); 
	     httpState.open("GET", url, true);
	     httpState.onreadystatechange = handleHttpResponse5;
	     httpState.send(null);
		 }
	}
	function handleHttpResponse5(){
        if (httpState.readyState == 4){
           var results = httpState.responseText
           results = results.trim();
           res = results.split("@");
           targetElement = document.forms['emailPage'].elements['userState'];
			targetElement.length = res.length;
			for(i=0;i<res.length;i++){
				if(res[i] == ''){
					document.forms['emailPage'].elements['userState'].options[i].text = '';
					document.forms['emailPage'].elements['userState'].options[i].value = '';
				}else{
					stateVal = res[i].split("#");
					document.forms['emailPage'].elements['userState'].options[i].text = stateVal[1];
					document.forms['emailPage'].elements['userState'].options[i].value = stateVal[0]; 
        		}
   			} 
        }
	}
	   var httpState = getHTTPObjectState()
	  function getHTTPObjectState()
	  {
	      var xmlhttp;
	      if(window.XMLHttpRequest)
	      {
	          xmlhttp = new XMLHttpRequest();
	      }
	      else if (window.ActiveXObject)
	      {
	          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	          if (!xmlhttp)
	          {
	              xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	          }
	      }
	      return xmlhttp;
	  } 
		
</script>	

<script language="javascript" type="text/javascript">

function imposeMaxLength(Object, MaxLen)
{
	return (Object.value.length <= MaxLen);
}

function onlyPhoneNumsAllowed(evt)
{
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==190) || (keyCode==189) ; 
}
</script>
<%-- Shifting Closed Here --%>

<script>
try{
displayState();
<c:if test="${hitFlag == 2}">
alert('Error while sending email, please try once again.');
</c:if>
<c:if test="${hitFlag == 1}">
alert('Email has been successfully sent.');
window.close();
</c:if>
}
catch(e){}

</script>