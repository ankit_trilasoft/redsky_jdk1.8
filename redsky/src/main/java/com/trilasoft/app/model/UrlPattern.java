package com.trilasoft.app.model;

import java.util.List;

import org.appfuse.model.BaseObject;
import org.appfuse.model.Role;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class UrlPattern extends BaseObject{
	
	private String urlPattern;
	private List<Role> roles;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("urlPattern", urlPattern)
				.append("roles", roles).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof UrlPattern))
			return false;
		UrlPattern castOther = (UrlPattern) other;
		return new EqualsBuilder().append(urlPattern, castOther.urlPattern)
				.append(roles, castOther.roles).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(urlPattern).append(roles)
				.toHashCode();
	}
	public String getUrlPattern() {
		return urlPattern;
	}
	public void setUrlPattern(String urlPattern) {
		this.urlPattern = urlPattern;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}


	
	

}
