<%@ include file="/common/taglibs.jsp"%>   

<head>   
    <title><fmt:message key="preferredAgent.title"/></title>   
    <meta name="heading" content="<fmt:message key='preferredAgent.heading'/>"/>
    
 </head>
   <s:form id="preferredAgentForm" name="preferredAgentForm" action="saveAgent" method="post" validate="true" >
   <s:hidden name="secondDescription" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription" />
	<s:hidden name="sixthDescription" />
	<s:hidden name="description" /> 
	<s:hidden name="agentValidationValue" value="" id="agentValidationValue"/> 
    <s:hidden name="preferredAgent.id" value="%{preferredAgent.id}"/> 
    <s:hidden name="preferredAgent.partnerCode" id="partnerCode"/>
    <s:hidden name="partnerId" value="%{partnerId}"/>   
	<s:hidden name="partnerType" value="%{partnerType}"/>
    <s:hidden name="id" value="<%=request.getParameter("id")%>"/>
    <c:set var="preferredAgent.id" value="<%=request.getParameter("id")%>"/>   
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
 	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="formStatus" value=""/>
	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
	<s:hidden name="preferredAgent.agentGroup"  value="%{preferredAgent.agentGroup}" />

		<div id="layer4" style="width:100%;">
			<div id="newmnav">
		  		<ul>
		    		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Agent Form</span></a></li>
					<li><a href="searchPreferredAgentList.html?partnerCode=${preferredAgent.partnerCode}&partnerId=${partnerId}&partnerType=${partnerType}"><span>Preferred Agent List</span></a></li>
				<c:if test="${partnerType == 'AC'}">
					<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=AC"><span>Account Detail</span></a></li>
					<li><a href="editNewAccountProfile.html?id=${partnerId}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Account Profile</span></a></li>
					<li><a href="editPartnerPrivate.html?partnerId=${partnerId}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Additional Info</span></a></li>
					<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
				<c:if test="${not empty partnerPrivate.id}">
						<configByCorp:fieldVisibility componentId="component.standard.accountContactTab">
					<li><a href="accountContactList.html?id=${partnerId}&partnerType=${partnerType}"><span>Account Contact</span></a></li>
						</configByCorp:fieldVisibility>
				<c:if test="${checkTransfereeInfopackage==true}">
					<li><a href="editContractPolicy.html?id=${partnerId}&partnerType=${partnerType}"><span>Policy</span></a></li>
				</c:if>
				</c:if>
				<c:if test="${partnerPublic.partnerPortalActive == true}">
			 		<configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">
				<li><a href="partnerUsersList.html?id=${partnerId}&partnerType=${partnerType}"><span>Portal Users</span></a></li>
					</configByCorp:fieldVisibility>
				</c:if>
				<c:if test="${paramValue == 'View'}">
					<li><a href="searchPartnerView.html"><span>Public List</span></a></li>
				</c:if>
				<c:if test="${paramValue != 'View'}">
					<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
				</c:if>
				<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'AC'}">
					<li><a href="partnerReloSvcs.html?id=${partnerId}&partnerType=${partnerType}"><span>Services</span></a></li>
				</c:if>
					<c:if test="${partnerType == 'AC'}">
				 <c:if test="${not empty partnerPublic.id}">
				<c:url value="frequentlyAskedQuestionsList.html" var="url">
				<c:param name="partnerCode" value="${partnerPublic.partnerCode}"/>
				<c:param name="partnerType" value="AC"/>
				<c:param name="partnerId" value="${partnerPublic.id}"/>
				<c:param name="lastName" value="${partnerPublic.lastName}"/>
				<c:param name="status" value="${partnerPublic.status}"/>
				</c:url>				 
				<li><a href="${url}"><span>FAQ</span></a></li>
				<c:if test="${partnerPublic.partnerPortalActive == true}">
					<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
				</c:if>
				  </c:if>
				</c:if>	
				<c:if test="${not empty partnerPrivate.id}">
					<li><a onclick="openNotesPopupTab(this);"><span>Notes</span></a></li>
				</c:if>
				</c:if>

		<c:if test="${partnerType == 'AG'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=${partnerType}"><span>Agent Detail</span></a></li>
			<li><a href="findPartnerProfileList.html?code=${partnerPrivate.partnerCode}&partnerType=${partnerType}&id=${partnerId}"><span>Agent Profile</span></a></li>
			<li id="newmnav1" style="background:#FFF"><a class="current"><span>Additional Info<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			
			<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Vanline Ref</span></a></li>
			<li><a href="baseList.html?id=${partnerId}"><span>Base</span></a></li>
			<c:if test="${partnerType == 'AG'}">
				<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
			</c:if>
			<c:if test="${partnerType == 'AG'}">
				<li><a href="partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Services</span></a></li>
			</c:if>
			<c:if test="${partnerPublic.partnerPortalActive == true}">
			   <configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
	       			<li><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & contacts</span></a></li>
	       			<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
			    </configByCorp:fieldVisibility>
			</c:if>
			     <li id="AGR"><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
		</c:if>
		<c:if test="${partnerType == 'VN'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=${partnerType}"><span>Vendor Detail</span></a></li>
			<li id="newmnav1" style="background:#FFF"><a class="current"><span>Additional Info<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			<li><a href="partnerUsersList.html?id=${partnerId}&partnerType=${partnerType}"><span>Users & contacts</span></a></li>		
		</c:if>
		<c:if test="${partnerType == 'CR'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=${partnerType}"><span>Carrier Detail</span></a></li>
		</c:if>
		<c:if test="${partnerType == 'CR' || partnerType == 'VN'}">
			<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
		</c:if>
  		</ul>
		</div><div class="spn">&nbsp;</div>
		
		</div>
 	<div id="Layer1" style="width:100%" > 
 		<div id="content" align="center">
			<div id="liquid-round-top">
   				<div class="top" ><span></span></div>
   					<div class="center-content">
  						<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%">
							<tbody>
								<tr>
									<td>
										<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
											<tbody>
												<tr><td height="4px"></td></tr>
												<tr>
													<td align="right" class="listwhitetext">
														<fmt:message key='preferredAgent.preferredAgentCode'/>
													</td>
													<td align="left" class="listwhitetext">
													
													<input type="text" class="input-text" oncopy="return false" onpaste="return false" name="preferredAgent.preferredAgentCode" id="agentCode" value="${preferredAgent.preferredAgentCode}" size="25" maxlength="65" required="true"  onchange="checkAgentCode();checkPartnerCodeExists();">
														<%-- <s:textfield name="preferredAgent.preferredAgentCode" id='agentCode'   size="25" maxlength="65" required="true"   cssClass="input-text" tabindex="1"/> --%>
													</td>
													<td width="17"><img align="left" class="openpopup" width="18" height="20" onclick="javascript:getAgentCode()" id="openpopup8.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
													<td align="right" class="listwhitetext" width="57px">
														<fmt:message key='preferredAgent.preferredAgentName'/>
													</td>
													<td align="left" class="listwhitetext" colspan="2">
														<s:textfield name="preferredAgent.preferredAgentName" id='agentName' onkeyup="autoCompleterAjaxCallAgentDetails('agentNameDivId','agentName',event,'agentCode');" onchange="checkAgentCode();checkPartnerCodeExists();" onfocus="tabOutChange()" size="29" maxlength="65"   cssClass="input-text" tabindex="2"/>
														<div id="agentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
													</td>
													<td align="right" class="listwhitetext" width="57px">
														<fmt:message key='preferredAgent.status'/>
													</td>
													<td align="left" class="listwhitetext" colspan="2">
													<c:if test="${empty preferredAgent.id}">
														<c:set var="ischeckedStatus" value="true" />
														<s:checkbox key="preferredAgent.status" id="status" value="${ischeckedStatus}" fieldValue="true" cssStyle="margin:0px;" tabindex="3" onfocus="tabOutChange()"/>
													</c:if>
													<c:if test="${not empty preferredAgent.id}">
													<c:choose>
														<c:when test="${preferredAgent.status}">
															<c:set var="ischeckedStatus" value="true" />
															<s:checkbox key="preferredAgent.status" id="status" value="${ischeckedStatus}" fieldValue="true" cssStyle="margin:0px;" tabindex="3" onfocus="tabOutChange()"/>
														</c:when>
														<c:otherwise>
														<c:set var="ischeckedStatus" value="false" />
															<s:checkbox key="preferredAgent.status" id="status" value="${ischeckedStatus}" fieldValue="true" cssStyle="margin:0px;" tabindex="3" onfocus="tabOutChange()"/>
														</c:otherwise>
													</c:choose>
													</c:if>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
						</tbody>
					</table>
				</div>
				<div class="bottom-header"><span></span></div>
			</div>
		</div>
 		  <table class="detailTabLabel" border="0" style="width:700px">
				<tbody>
					<tr>
						<td align="left" class="listwhitetext" width="30px"></td>
						<td colspan="5"></td>
					</tr>
					<tr>
						<td align="left" class="listwhitetext" width="30px"></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='systemDefault.createdOn'/></b></td>
							<s:text id="contractcreatedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="preferredAgent.createdOn" /></s:text>
						<td valign="top"><s:hidden name="preferredAgent.createdOn" value="%{contractcreatedOnFormattedValue}" /></td>
						<td style="width:130px"><fmt:formatDate value="${preferredAgent.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='systemDefault.createdBy' /></b></td>
						<c:if test="${not empty preferredAgent.id}">
						<s:hidden name="preferredAgent.createdBy"/>
						<td ><s:label name="createdBy" value="%{preferredAgent.createdBy}"/></td>
						</c:if>
						<c:if test="${empty preferredAgent.id}">
						<s:hidden name="preferredAgent.createdBy" value="${pageContext.request.remoteUser}"/>
						<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
						</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='systemDefault.updatedOn'/></b></td>
							<s:text id="contractupdatedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="preferredAgent.updatedOn" /></s:text>
						<td valign="top"><s:hidden name="preferredAgent.updatedOn" value="%{contractupdatedOnFormattedValue}" /></td>
						<td style="width:130px"><fmt:formatDate value="${preferredAgent.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='systemDefault.updatedBy' /></b></td>
						<c:if test="${not empty preferredAgent.id}">
							<s:hidden name="preferredAgent.updatedBy"/>
						<td ><s:label name="updatedBy" value="%{preferredAgent.updatedBy}"/></td>
						</c:if>
						<c:if test="${empty preferredAgent.id}">
							<s:hidden name="preferredAgent.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td ><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
						</c:if>
					</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table></td></tr>
        	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="save" key="button.save" onfocus="checkOnSave();" tabindex="" />
        		<c:if test="${not empty preferredAgent.id}"> 
        		<input type="button"  tabindex="13" class="cssbuttonA"  onclick="location.href='<c:url value="/editPreferredAgent.html?partnerCode=${preferredAgent.partnerCode}&partnerId=${partnerId}&partnerType=${partnerType}"/>'" value="Add"/>   
        		</c:if>   
       		<s:reset cssClass="cssbutton" cssStyle="width:55px; height:25px" key="Reset" onclick="resetFieldValue()"/>
	</div>
   </s:form> 
   <script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
	<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
	<script type="text/javascript">
	animatedcollapse.addDiv('valuation', 'fade=1,hide=1')
	animatedcollapse.init()
	</script>
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
	<script type="text/javascript">
	function getAgentCode(){
		openWindow('bookingAgentPopupNew.html?partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=preferredAgent.preferredAgentName&fld_code=preferredAgent.preferredAgentCode');
		document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentCode'].select();	
	}
	function autoCompleterAjaxCallAgentDetails(divid,searchfield,evt,agntCode)
	{
		var keyCode = evt.which ? evt.which : evt.keyCode;
		if(evt.keyCode != 9)
		{
			document.forms['preferredAgentForm'].elements['agentValidationValue'].value="AutoAjaxFilled";
			var agentName = document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentName'].value;
			var u=document.getElementById(searchfield).value;
			if(u.length<=1)
				{
					document.getElementById(agntCode).value=""
				}
			var partnerCode="";
			var idval="";
			if(agentName.length>=3)
			{
           		$.get("agentDetailAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", {
           		searchName:agentName,
           		partnerType: "AG",
           		fieldType:searchfield,
           		partnerCode:partnerCode,
           		autocompleteDivId: divid
           	}, function(idval) {
               document.getElementById(divid).style.display = "block";
               $("#" + divid).html(idval)
           })
       		} else {
           			document.getElementById(divid).style.display = "none"
       				}
		}
		else
		{
			document.getElementById(divid).style.display="none"
		}
	}
	function copyAgentDetails(partnerCode,partnerName,autocompleteDivId){
		document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentName'].value=partnerName;
		document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentCode'].value=partnerCode;
		document.getElementById(autocompleteDivId).style.display = "none";
		document.forms['preferredAgentForm'].elements['agentValidationValue'].value='';
		checkAgentCode();
	}

function closeMyAgentDiv(partnerCode,partnerName,autocompleteDivId)
		{
			document.getElementById(autocompleteDivId).style.display = "none";
	 		if(document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentCode'].value=='')
	 		{
				document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentName'].value="";
			}
	 			document.forms['preferredAgentForm'].elements['agentValidationValue'].value='';
	 			checkAgentCode();
		}
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http5 = getHTTPObject();
    var http6 = getHTTPObject();
	
function checkAgentCode(){
	if(document.forms['preferredAgentForm'].elements['agentValidationValue'].value==''){ 
	var partnerCode= document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentCode'].value;
	if(partnerCode==''){
    	document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentName'].value="";
    }
	if(partnerCode.trim()!=""){
	var myRegxp = /^[a-zA-Z0-9-_\'('&'\)']+$/;
	if(partnerCode.match(myRegxp)==false){
	   alert( "Please enter a valid Agent code." );
	   document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentCode'].value='';
	}else{
		var url="checkAgentCode.html?ajax=1&decorator=simple&popup=true&agentCode="+partnerCode; 
		http6.open("GET", url, true);
		http6.onreadystatechange =function(){handleHttpResponsepartnerCode1(partnerCode);} ;
		http6.send(null);
		}
	}}}
	
function handleHttpResponsepartnerCode1(partnerCode) {
    if (http6.readyState == 4) {
       var results = http6.responseText
       results = results.trim();
       results = results.replace('[','');
       results=results.replace(']','');
       var res = results.split("#");
       if(results.length>4)  {
    	   if(res!=undefined){
    	   document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentCode'].value=partnerCode;  
    	   document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentName'].value=res[1];
    	   checkPartnerCodeExists();
    	   }
           }
        else{
        	document.getElementById("agentNameDivId").style.display = "none";
        	document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentCode'].value='';
        	document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentName'].value='';
       		document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentCode'].focus();
           	alert("Agent Code is Not Valid." );
            return false;
		   } }
  	 // progressBarAutoSave('0');
    return true;
}

function resetFieldValue(){	
	document.getElementById("agentNameDivId").style.display = "none";
	document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentCode'].value='';
 	document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentName'].value='';
 	document.getElementById("status").checked =true;
	 }
/* function onChangeCheckValue(){
	var chkId= '${preferredAgent.id}';
	if(chkId==''){
		document.getElementById("status").checked =true;
	}
}
try{onChangeCheckValue();}
catch(e){} */

function checkOnSave(){
	document.getElementById("agentNameDivId").style.display = "none";
	document.forms['preferredAgentForm'].elements['agentValidationValue'].value='';
}

function tabOutChange(){
		document.getElementById("agentNameDivId").style.display = "none";
}

function checkPartnerCodeExists(){
	<c:if test="${empty preferredAgent.id}">
	if(document.forms['preferredAgentForm'].elements['agentValidationValue'].value=='')
	{ 
		var partnerCode=document.getElementById("partnerCode").value;
		var agentCode= document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentCode'].value;
	 	if(agentCode.trim()!="")
	 	{
			var myRegxp = /^[a-zA-Z0-9-_\'('&'\)']+$/;
			if(partnerCode.match(myRegxp)==false)
			{
	   			alert( "Please enter a valid Agent code." );
	   			document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentCode'].value='';
			}else{
				var url="checkAgentForExists.html?ajax=1&decorator=simple&popup=true&partnerCode="+partnerCode+"&agentCode="+agentCode; 
	     		http5.open("GET", url, true);
	     		http5.onreadystatechange =function(){handleHttpResponsepartnerCodeExists(partnerCode);} ;
	     		http5.send(null);
				}
			}}
	</c:if>
	}
	
function handleHttpResponsepartnerCodeExists(partnerCode) {
    if (http5.readyState == 4) {
       var results = http5.responseText
       results = results.trim();
       results = results.replace('[','');
       results=results.replace(']','');
       var res = results.split("#");
       if(results.length>4)  {
    	   if(res!=undefined){
    		   if(res[2]=="Exists")
    			   alert("Agent Code is Already Exists.");
    	   document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentName'].value="";
    	   document.forms['preferredAgentForm'].elements['preferredAgent.preferredAgentCode'].value="";
    	   return false;
    	   }
           }
        else{
        	
		   } }
    return true;
}

		try{  	
			window.onload =function ModifyPlaceHolder () {		 
		     var input1 = document.getElementById ("agentName");
		     input1.placeholder = "Preferred Agent Search";
		    		   
	    }
	}catch(e){}
	
	
	function validateListForPreferedAgent(){
		checkAgentCode();
		//checkPartnerCodeExists();
	}
</script>
