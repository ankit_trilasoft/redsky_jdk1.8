<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="operationResource.title"/></title>   
    <meta name="heading" content="<fmt:message key='O&I.heading' />"/>   
    <style> #ajax_tooltipObj .ajax_tooltip_content {background-color:#FFF !important;}</style>
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top">   
     <td align="left"><b>Work Order</b></td>
    <td align="right"  style="width:30px;">
    <img  valign="top"align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
   <table class="notesDetailTable" cellspacing="0" cellpadding="1" border="0" width="250" style="!margin-top:5px;"> 
    <tbody> 
    <tr><td class="subcontent-tab" style="height:17px;font-size:12px;background:url(images/basic-inner-blue.png) repeat-x scroll 0 0 #3dafcb;border-bottom: 1px solid #3dafcb;border-top: 1px solid #3dafcb; color: #fff;font-family: arial,verdana;"><font>Insert Work Order </font></td></tr>
    <tr>
    <td valign="top">           
    <table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%;padding-top:10px;" >
    <tbody>
    <tr>
    <td style="width:11px;margin-top:5px;"> </td>
     <td><s:select cssClass="list-menu" id="template" name="template" list="{'Line','Template'}" cssStyle="width:113px;margin-right: 6px;" onchange="" tabindex="" /></td>
    <td><s:select cssClass="list-menu" id="action" name="action" list="{'Before','After'}" cssStyle="width:113px;margin-right: 6px;" onchange="changeStatus();" tabindex="" /></td>
    <td> 
     
      <c:if test="${showDistinctWorkOrder!='[]'}">    
   <select class="list-menu" id="wo" name="wo" style="width: 95px;">
  <c:forEach items="${showDistinctWorkOrder}" var="databaseValue">
     <option value="${databaseValue.workorder}">
        ${databaseValue.workorder}
    </option>
  </c:forEach>
</select>
</c:if>
  <c:if test="${showDistinctWorkOrder=='[]'}">
   <input type="text" class="input-textUpper" id="wo" name="wo"  style="width:65px;">
</select>
  </c:if> 
  </td>
  <td>
    <c:if test="${showDistinctWorkOrder!='[]'}"> 
 <input type="button"  Class="update-btn-OI" align="center" style="width:80px; font-size: 11px; margin: 4px 7px 5px; padding: 0px; height: 21px;" id="okButton"  value="Insert" onclick="insertWorkOrder();"/>    
    </c:if>
    <c:if test="${showDistinctWorkOrder=='[]'}"> 
   <input type="button"  Class="update-btn-dis-OI" align="center" readonly="true" style="width:80px; font-size: 11px; margin: 4px 7px 5px; padding: 0px; height: 21px;" id="okButton"  value="Insert" />   
    </c:if> 
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
</table>
    
     