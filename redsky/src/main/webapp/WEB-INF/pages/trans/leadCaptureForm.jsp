<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<head> 
<title>Lead Capture Information</title> 
<meta name="heading" content="Lead Capture Information"/>

<style type="text/css">
span.pagelinks {   
    margin-bottom: -21px; 
}
</style>
<style type=text/css>
div.autocomplete {
      position:absolute;      
      background-color:white;     
      margin:0px;
      padding:0px;
      z-index:99999;
      
      
    }
    div.autocomplete ul {
      list-style-type:none;
      border-top:1px solid #219DD1;
      border-bottom:1px solid #219DD1;
      margin:0px;
      padding:0px;
    }
    div.autocomplete ul li.selected { background-color: #ffb;}
    div.autocomplete ul li {
      list-style-type:none;
      display:block;
      border-top:1px solid #dfdfdf;
      border-bottom:none;
      border-left:1px solid #219DD1;
      border-right:1px solid #219DD1;
      margin:0;
      padding:0px;
      padding-left:3px;   
      cursor:pointer;
    }
    
    div#autocomplete_choices{ width:200px !important; }
    

  .ui-autocomplete {
    max-height: 250px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 100px;
  }

    
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>	
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>	
	 <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
  
<script language="javascript" type="text/javascript"> 
  function getState(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode); 
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse5;
     httpState.send(null);
}	  
  function handleHttpResponse5(){
      if (httpState.readyState == 4){
         var results = httpState.responseText
         results = results.trim();
         res = results.split("@");
         targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
			targetElement.length = res.length;
			for(i=0;i<res.length;i++)
				{
				if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = '';
				}else{
				stateVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = stateVal[1];
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = stateVal[0]; 
				if (document.getElementById("originState").options[i].value == '${customerFile.originState}'){
				   document.getElementById("originState").options[i].defaultSelected = true;
				}
				}
				}
				if ('${stateshitFlag}'=="1"){
				document.getElementById("originState").value = '';
				}
				else{ 
				document.getElementById("originState").value = '${customerFile.originState}';
				}
      }
 } 
  var httpState = getHTTPObjectState()
    function getHTTPObjectState()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 
  function enableStateListOrigin(){ 
		var oriCon=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
		  var enbState = '${enbState}';
		  var index = (enbState.indexOf(oriCon)> -1);
		  if(index != ''){
		  		document.forms['customerFileForm'].elements['customerFile.originState'].disabled = false;
		  }else{
			  document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
		  }	        
	}
  function enableStateListDestin(){ 
		var desCon=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
		  var enbState = '${enbState}';
		  var index = (enbState.indexOf(desCon)> -1);
		  if(index != ''){
		  		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = false;
		  }else{
			  document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
		  }	        
	}
  function getDestinationState(targetElement){ 
		var country = targetElement.value; 
		var countryCode = "";
		<c:forEach var="entry" items="${countryCod}">
		if(country=="${entry.value}"){
			countryCode="${entry.key}";
		}
		</c:forEach>
		var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	     http31111.open("GET", url, true);
	     http31111.onreadystatechange = handleHttpResponse6;
	     http31111.send(null);
	}

  function handleHttpResponse6(){
		if (http31111.readyState == 4){
              var results = http31111.responseText
              results = results.trim();
              res = results.split("@");
              targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
				targetElement.length = res.length;
				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = stateVal[0]; 
					if (document.getElementById("destinationState").options[i].value == '${customerFile.destinationState}'){
					   document.getElementById("destinationState").options[i].defaultSelected = true;
					} } }
					if ('${stateshitFlag}'=="1"){
					document.getElementById("destinationState").value = '';
					}
					else{ document.getElementById("destinationState").value = '${customerFile.destinationState}';
					   }
      }
} 
  var http31111 = getHTTPObject1();
  function getHTTPObject1()
  {
      var xmlhttp;
      if(window.XMLHttpRequest)
      {
          xmlhttp = new XMLHttpRequest();
      }
      else if (window.ActiveXObject)
      {
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          if (!xmlhttp)
          {
              xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
          }
      }
      return xmlhttp;
  }
  function state(){
      var destinationCountryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value; 
		var ds1 = document.getElementById('destinationStateRequiredTrue');
		var ds2 = document.getElementById('destinationStateRequiredFalse');
		if(destinationCountryCode == 'United States'){
		ds1.style.display = 'block';		
		ds2.style.display = 'none';	
		}else{
		ds1.style.display = 'none';
		ds2.style.display = 'block';
		}
}

  function zipCode(){
      var originCountryCode=document.forms['customerFileForm'].elements['customerFile.originCountry'].value; 		
		var st1=document.getElementById('originStateRequiredFalse');
		var st2=document.getElementById('originStateRequiredTrue');
		if(originCountryCode == 'United States'){		
		st2.style.display = 'block';		
		st1.style.display = 'none';	
		}else{		
		st1.style.display = 'block';		
		st2.style.display = 'none';	
		
		}
}
  
  function notExists(){
		alert("The lead capture information has not been saved yet, please save lead information to continue");
	}

  function getOriginCountryCode(){
		var countryName=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
		var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
	    http4.open("GET", url, true);
	    http4.onreadystatechange = handleHttpResponseCountryName;
	    http4.send(null);
	}
  function handleHttpResponseCountryName(){
      if (http4.readyState == 4){
         var results = http4.responseText
         results = results.trim();
         var res=results.split('#');
         if(res.length>=1){
         	document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value = res[0]; 
         	if(res[1]=='Y'){
         	document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='Y';				
			 	}else{
			 	document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='N';
			 	}try{
			 	document.forms['customerFileForm'].elements['customerFile.originCountry'].select();
			 	}catch(e){}		     	 
			}else{ 
          }  }}	
  function getDestinationCountryCode(){
		var countryName=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
		var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
	    http4.open("GET", url, true);
	    http4.onreadystatechange = httpDestinationCountryName;
	    http4.send(null);
	}
  function httpDestinationCountryName(){
      if (http4.readyState == 4){
         var results = http4.responseText
         results = results.trim();
         var res = results.split("#");
         if(res.length>=1){
				document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value = res[0];
				if(res[1]=='Y'){
				document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='Y';
				}else{
				document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='N';
				}
				try{	
				document.forms['customerFileForm'].elements['customerFile.destinationCountry'].select();
				}catch(e){}
			}}}
  var http4 = getHTTPObject3();
  function getHTTPObject3(){
	    var xmlhttp;
	    if(window.XMLHttpRequest){
	        xmlhttp = new XMLHttpRequest();
	    }else if (window.ActiveXObject){
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp){
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}	
  function autoPopulate_customerFile_originCityCode(targetElement, w) {
		var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};

		if(document.forms['customerFileForm'].elements['customerFile.originCity'].value != '')
		{
			if(document.forms['customerFileForm'].elements['customerFile.originState'].value != '')
			{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value+','+document.forms['customerFileForm'].elements['customerFile.originState'].value;
			 } else
			{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value;
				
			} } }
	function autoPopulate_customerFile_destinationCityCode(targetElement, w) {
		var r={
			 'special':/[\W]/g,
			 'quotes':/['\''&'\"']/g,
			 'notnumbers':/[^\d]/g
			};
			if(document.forms['customerFileForm'].elements['customerFile.destinationCity'].value != ''){
				if(document.forms['customerFileForm'].elements['customerFile.destinationState'].value == ''){
					document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
				}else{
					document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value+','+document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
				} } }

	function autoCompleterAjaxCall(){
		var stateNameOri = document.forms['customerFileForm'].elements['customerFile.originState'].value;
		var countryName = document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
		var cityNameOri = document.forms['customerFileForm'].elements['customerFile.originCity'].value;
		var countryNameOri = "";
		<c:forEach var="entry" items="${countryCod}">
		if(countryName=="${entry.value}"){
			countryNameOri="${entry.key}";
		}
		</c:forEach>
		if(cityNameOri!=''){
			var data = 'leadCaptureAutocompleteOriAjax.html?ajax=1&stateNameOri='+stateNameOri+'&countryNameOri='+countryNameOri+'&cityNameOri='+cityNameOri+'&decorator=simple&popup=true';
			$( "#originCity" ).autocomplete({				 
			      source: data		      
			    });
		}
	}
	
	function autoCompleterAjaxCallDest(){
		var stateNameDes = document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
		var countryName = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
		var cityNameDes = document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
		var countryNameDes = "";
		<c:forEach var="entry" items="${countryCod}">
		if(countryName=="${entry.value}"){
			countryNameDes="${entry.key}";
		}
		</c:forEach>
		if(cityNameDes!=''){
		var data = 'leadCaptureAutocompleteDestAjax.html?ajax=1&stateNameDes='+stateNameDes+'&countryNameDes='+countryNameDes+'&cityNameDes='+cityNameDes+'&decorator=simple&popup=true';
		$( "#destinationCity" ).autocomplete({				 
		      source: data		      
		    });
		}
	}
	
	function newFunctionForCountryState(){
		var destinationAbc ='${customerFile.destinationCountry}';
		var originAbc ='${customerFile.originCountry}';
		var enbState = '${enbState}';
		var index = (enbState.indexOf(originAbc)> -1);
		var index1 = (enbState.indexOf(destinationAbc)> -1);
		if(index != ''){
		document.forms['customerFileForm'].elements['customerFile.originState'].disabled =false;
		getOriginStateReset(originAbc);
		}
		else{
		document.forms['customerFileForm'].elements['customerFile.originState'].disabled =true;
		}
		if(index1 != ''){
		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =false;
		getDestinationStateReset(destinationAbc);
		} 
		else{
		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =true;
		}
		}
	function getOriginStateReset(target) {
		var country = target;
	 	var countryCode = "";
		<c:forEach var="entry" items="${countryCod}">
		if(country=="${entry.value}"){
			countryCode="${entry.key}";
		}
		</c:forEach>
		 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	     httpState.open("GET", url, true);
	     httpState.onreadystatechange = handleHttpResponse555555;
	     httpState.send(null);
	}
	 function handleHttpResponse555555(){
         if (httpState.readyState == 4){
            var results = httpState.responseText
            results = results.trim();
            res = results.split("@");
            targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++) {
				if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = '';
				}else{
				stateVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = stateVal[1];
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = stateVal[0];
				
				if (document.getElementById("originState").options[i].value == '${customerFile.originState}'){
				   document.getElementById("originState").options[i].defaultSelected = true;
				} } }
				document.getElementById("originState").value = '${customerFile.originState}';
         }
    }
	 function getDestinationStateReset(target){
			var country = target;
		 	var countryCode = "";
			<c:forEach var="entry" items="${countryCod}">
			if(country=="${entry.value}"){
				countryCode="${entry.key}";
			}
			</c:forEach>
			var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
		     http3.open("GET", url, true);
		     http3.onreadystatechange = handleHttpResponse6666666666;
		     http3.send(null);
		}
	 function handleHttpResponse6666666666(){
			if (http3.readyState == 4){
	                var results = http3.responseText
	                results = results.trim();
	                res = results.split("@");
	                targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
					targetElement.length = res.length;
	 				for(i=0;i<res.length;i++) {
						if(res[i] == ''){
						document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = '';
						document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = '';
						}else{
						stateVal = res[i].split("#");
						document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
						document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = stateVal[0];
						
						if (document.getElementById("destinationState").options[i].value == '${customerFile.destinationState}'){
						   document.getElementById("destinationState").options[i].defaultSelected = true;
						} } }
						document.getElementById("destinationState").value = '${customerFile.destinationState}';
	        } } 
	 var http3 = getHTTPObject();

	 function findLeadStatusOnResetButton(){
		 var leadStatusValue='${customerFile.leadStatus}';
		  	document.getElementById('leadStatusVal').value=leadStatusValue.toUpperCase();
		 }
	 function findLeadStatusOnReset(){
			setTimeout("findLeadStatusOnResetButton()",500);
		}
	
  </script>
</head>
<s:form id="customerFileForm" name="customerFileForm" action="leadCaptureSave" method="post" validate="true">
<s:hidden name="originCountryFlex3Value"  id="originCountryFlex3Value"/>
<s:hidden name="DestinationCountryFlex3Value" id="DestinationCountryFlex3Value" />
<s:hidden name="customerFile.id" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="field" value="<%=request.getParameter("field") %>"/>
<s:hidden name="field" value="<%=request.getParameter("field") %>" />
<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
<c:set var="field1" value="<%=request.getParameter("field1") %>"/>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Lead Screen</span></a></li>
		      <sec-auth:authComponent componentId="module.tab.customerFile.auditTab">
		   	    <c:if test="${not empty customerFile.id}"> 
		    <li>
              <a onclick="window.open('auditList.html?id=${customerFile.id}&tableName=customerfile&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')">
              <span>Audit</span></a></li>
		    </c:if>
		    <c:if test="${empty customerFile.id}">
		    	<li><a><span>Audit</span></a></li>
		    </c:if>
		    </sec-auth:authComponent>
		  </ul>
		</div>
		<div class="spnblk" style="line-height: 22px;">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div> 
   <div class="center-content">
   <table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Call Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
<table class="detailTabLabel" width="100%" >
	<tr>	 
	  <td align="left">
			<table width="" border="0" cellpadding="0" cellspacing="0" class="detailTabLabel" style="padding-left:30px;">
					<tbody>
					<tr><td style="height:10px"></td></tr>
					<c:if test="${not empty customerFile.id}">
						<tr>
							<td align="left" width="30" class="listwhitetext" valign="bottom" >Lead File #</td>
							<td align="left" class="listwhitetext" width="10"></td>
							<td align="left" width="119" class="listwhitetext" valign="bottom">Lead Status</td>
													
							<td align="left" width="" class="listwhitetext" valign="bottom">Status Date</td>
							</td>
						</tr>
						
						<tr>						
							<td align="left" class="listwhitetext" valign="top" > <s:textfield cssClass="input-textUpper" name="customerFile.sequenceNumber" cssStyle="width:100px;" readonly="true" required="true"/></td>
							<td align="left" class="listwhitetext" width="10"></td>
							<td align="left" class="listwhitetext" valign="top" width=""> <s:select cssClass="list-menu" name="customerFile.leadStatus" id="leadStatusVal" cssStyle="width:105px" list="%{leadCaptuteStatus}" headerKey="" headerValue=""/> </td>
							
							<c:if test="${not empty customerFile.leadStatusDate}">
							<s:text id="customerFileStatusDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.leadStatusDate" /></s:text>
							<td width="70"  valign="top"><s:textfield cssClass="input-textUpper" name="customerFile.leadStatusDate" value="%{customerFileStatusDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="" /></td>
							</c:if>
							<c:if test="${empty customerFile.leadStatusDate}">
							<td width="70" valign="top"><s:textfield cssClass="input-textUpper"  name="customerFile.leadStatusDate" cssStyle="width:65px" readonly="true"  tabindex=""/></td>
							</c:if>						
						</tr>
						</c:if>	
						<tr><td style="height:10px"></td></tr>			
						<tr>
							<td align="left" width="30" class="listwhitetext" valign="bottom" ><fmt:message key='customerFile.prefix'/></td>
							<td align="left" class="listwhitetext" width="10"></td>
							<td align="left" width="60" class="listwhitetext" colspan="2"><fmt:message key='customerFile.firstName' /></td>
							<td align="left" class="listwhitetext" width="12"></td>							
							<td align="left" width="" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.lastName'/></td>
							<td align="left" class="listwhitetext" width="10" ></td>
							</td>
						</tr>
						<tr>						
							<td align="left" class="listwhitetext" valign="top" > <s:select cssClass="list-menu" name="customerFile.prefix" id="prefix" cssStyle="width:103px" list="%{preffix}" headerKey="" headerValue="" tabindex="1"/></td>
							<td align="left" class="listwhitetext" width="10"></td>
							<td align="left" class="listwhitetext" colspan="2" width=""> <s:textfield cssClass="input-text upper-case" name="customerFile.firstName" onblur="" onkeypress=""  required="true" cssStyle="width:185px;" maxlength="80" tabindex="2"/> </td>
							<td align="left" class="listwhitetext" width="10"></td>							
							<td align="left" class="listwhitetext" valign="top" > <s:textfield cssClass="input-text upper-case"  key="customerFile.lastName" onblur="" onkeypress="" required="true" size="25" cssStyle="width:245px;" maxlength="80" tabindex="4"/> </td>
							<td align="left" class="listwhitetext" width="10" ></td>							
						</tr>
						<tr><td style="height:10px"></td></tr>		
						</table>
						<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px; padding: 0px;">
						<tr class="subcontenttabChild">
						<td style="border:1px solid #DFDFDF"><b>&nbsp;Origin</b></td>											
						</tr>
						</table>
						<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="" style="padding-left:30px;">
						<tbody>
						<tr><td style="height:10px"></td></tr>									
						<tr>							
							<td align="left" class="listwhitetext" colspan="3"><fmt:message key='customerFile.originCountry'/><font color="red" size="2">*</font></td>
							<td align="left" class="listwhitetext" width="10" ></td>
							<td align="left" id="originStateRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originState' /></td>
							<td align="left" id="originStateRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.originState' /><font color="red" size="2">*</font></td>
							<td align="left" class="listwhitetext" width="10" ></td>
							<td align="left" class="listwhitetext" onmouseover =""><fmt:message key='customerFile.originCity'/><font color="red" size="2">*</font></td>
						</tr>
						<tr>							
							<td colspan="3"><s:select cssClass="list-menu" name="customerFile.originCountry" list="%{ocountry}" id="ocountry" cssStyle="width:245px"  onchange="zipCode();getState(this);enableStateListOrigin();"  headerKey="" headerValue="" tabindex="5"/></td>
							<td align="left" class="listwhitetext" width="15" ></td>
							<td colspan="0"><s:select cssClass="list-menu" id="originState" name="customerFile.originState" list="%{ostates}" cssStyle="width:153px" onchange="autoPopulate_customerFile_originCityCode(this);" headerKey="" headerValue="" tabindex="6"/></td>
							<td align="left" class="listwhitetext" width="15" ></td>
							<td><s:textfield cssClass="input-text upper-case" name="customerFile.originCity" id="originCity" cssStyle="width:132px" size="20" maxlength="30" onchange="getOriginCountryCode();"  onkeyup="validCheck(this,'quotes');autoPopulate_customerFile_originCityCode(this,'special');autoCompleterAjaxCall();" tabindex="7"/>
				             </td>				  			
						</tr>								
						<tr><td style="height:10px"></td></tr>	
						</table>
						<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px; padding: 0px;">
						<tr class="subcontenttabChild">
						<td style="border:1px solid #DFDFDF"><b>&nbsp;Destination</b></td>											
						</tr>
						</table>
						<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="" style="padding-left:30px;">
						<tbody>
						<tr><td style="height:10px"></td></tr>									
						<tr>							
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCountry'/><font color="red" size="2">*</font></td>
							<td align="left" class="listwhitetext"></td>							
							<td align="left" class="listwhitetext" width="10" ></td>
							<td align="left" id="destinationStateRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.destinationState' /></td>
							<td align="left" id="destinationStateRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.destinationState' /><font color="red" size="2">*</font></td>
							<td align="left" class="listwhitetext" width="10" ></td>
							 <td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCity'/><font color="red" size="2">*</font></td>
						</tr>
						<tr>							
							<td><s:select cssClass="list-menu" name="customerFile.destinationCountry" list="%{dcountry}" id="dcountry" cssStyle="width:245px" onkeydown="" onchange="state();getDestinationState(this);enableStateListDestin();"  headerKey="" headerValue="" tabindex="8"/></td><td>
							<td align="left" class="listwhitetext" width="15" ></td>
							<td><s:select cssClass="list-menu" id="destinationState" name="customerFile.destinationState" list="%{dstates}" cssStyle="width:153px" value="%{customerFile.destinationState}" onkeydown="" onchange="autoPopulate_customerFile_destinationCityCode(this)" headerKey="" headerValue="" tabindex="9"/></td>
							<td align="left" class="listwhitetext" width="15" ></td>
							<td><s:textfield cssClass="input-text upper-case"  name="customerFile.destinationCity" id="destinationCity" cssStyle="width:132px" size="20" maxlength="30" onchange="getDestinationCountryCode();" onkeyup="validCheck(this,'quotes');autoPopulate_customerFile_destinationCityCode(this,'special');autoCompleterAjaxCallDest();" tabindex="10"/>
				             </td>
						</tr>
						</table>
						
					
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="padding-left:30px;">
						<tbody>	
						<tr><td style="height:10px"></td></tr>
					<tr>
							<td width="" align="left" class="listwhitetext"><fmt:message key='customerFile.job'/><font color="red" size="2">*</font></td>
							<td style="width:10px"></td>
							<td width="" align="left" class="listwhitetext"><fmt:message key='customerFile.salesMan'/><font color="red" size="2">*</font></td>
														
							<td style="width:10px"></td>
							<td width="30"  align="left" class="listwhitetext" ><fmt:message key='customerFile.moveDate'/></td>
							
						  </tr>
						<tr>
							<td align="left"><s:select cssClass="list-menu" id="job" name="customerFile.job"  list="%{jobs}" cssStyle="width:245px" headerKey="" headerValue="" onchange="" tabindex="11"/></td>
							<td align="left" class="listwhitetext" style="width:15px"></td>
							<td align="left"><s:select cssClass="list-menu" name="customerFile.salesMan" list="%{sale}"  cssStyle="width:153px" headerKey="" headerValue="" onchange="" tabindex="12"/></td>
							<td align="left" class="listwhitetext" style="width:15px"></td>
							
							 
							<c:if test="${not empty customerFile.moveDate}">
							<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.moveDate"/></s:text>
							<td><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" value="%{customerFileMoveDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="25" onkeydown="return onlyDel(event,this)"/></td>
							<td><img id="moveDate_trigger" style="vertical-align:bottom;margin-left:2px;" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 tabindex="25"/></td>
							</c:if>
							<c:if test="${empty customerFile.moveDate}">
							<td><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="" onkeydown="return onlyDel(event,this)" tabindex="25"/></td>
							<td><img id="moveDate_trigger" style="vertical-align:bottom;margin-left:2px;" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 tabindex="25"/></td>
							</c:if>
					</tr>
					<tr>
					<td colspan="15">
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" >
						<tbody>	
						<tr>	
						<tr><td style="height:10px"></td></tr>					
							
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.originHomePhone'/></td>
							<td align="left" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.originMobile'/></td>
							<td align="left" class="listwhitetext" style="width:15px"></td>
							<td align="left" class="listwhitetext">How You Found Us</td>							
						</tr>
						<tr>								
							<td><s:textfield cssClass="input-text" name="customerFile.originHomePhone" cssStyle="width:114px" size="18" maxlength="20"  tabindex="15" onkeydown=""/></td>
							<td align="left" class="listwhitetext" style="width:10px"></td>
							<td><s:textfield cssClass="input-text" name="customerFile.originMobile" cssStyle="width:114px" size="17" maxlength="25"  tabindex="16" onkeydown=""/></td>
							<td align="left" class="listwhitetext" style="width:15px"></td>
							<td><s:select cssClass="list-menu" name="customerFile.source" list="%{lead}" cssStyle="width:153px" headerKey="" headerValue="" tabindex="17"/></td>
					</tr>
					
							<s:hidden  name="customerFile.originCityCode" />
							<s:hidden name="customerFile.originCountryCode"/>
							<s:hidden name="customerFile.destinationCityCode"/>
							<s:hidden name="customerFile.destinationCountryCode"/>
					
					</table>
					</td>
					</tr>
					</table>					
					
			</td>
		</tr>
		</table>		
		<div style="!margin-top:7px;"></div>			
		</div>
		<div class="bottom-header" style="margin-top:45px;"><span></span></div>
		</div>
		</div> 
		<table class="detailTabLabel" cellspacing="0" cellpadding="0">
			<tbody>	
				<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>							
							<td valign="top"></td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${customerFile.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="customerFile.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${customerFile.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty customerFile.id}">
								<s:hidden name="customerFile.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{customerFile.createdBy}"/></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<s:hidden name="customerFile.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${customerFile.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="customerFile.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${customerFile.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty customerFile.id}">
								<s:hidden name="customerFile.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{customerFile.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<s:hidden name="customerFile.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>	
					</tbody>
				</table>	
			<table class="detailTabLabel" border="0">
		<tbody> 	
			<tr>
				<td align="left" height="3px"></td>
			</tr>	  	
			<tr>
				<td align="left"><s:submit cssClass="cssbutton1" type="button" key="Submit" onclick=""/></td>
		       	<td align="right"><s:reset cssClass="cssbutton1" type="button" key="Reset" onclick="newFunctionForCountryState();findLeadStatusOnReset();"/></td>
		       	<!--<c:if test="${not empty customerFile.id}">
		       	<td><input type="button" class="cssbuttonA" style="width:55px; height:25px;" onclick="location.href='<c:url value="/addLeadFile.html"/>'"  
        		value="<fmt:message key="button.add"/>"/>  </td>
        		</c:if>-->
			</tr>		  	
		</tbody>
	</table>
</s:form>   
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
	
	try{  	
  		document.forms['customerFileForm'].elements['customerFile.moveDate'].value = '${moveDateTemp}';   
  		getState(document.forms['customerFileForm'].elements['customerFile.originCountry']);  	
  		getDestinationState(document.forms['customerFileForm'].elements['customerFile.destinationCountry']);
  		document.getElementById('prefix').focus();
  	var leadStatusValue='${customerFile.leadStatus}';
  	document.getElementById('leadStatusVal').value=leadStatusValue.toUpperCase();
  	}catch(e){}
  	zipCode();
	state()
</script>
<script type="text/javascript">
try{
	 var enbState = '${enbState}';
	 var oriCon=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	 if(oriCon=="")	 {
			document.getElementById('originState').disabled = true;
			document.getElementById('originState').value ="";
	 }else{
	  		if(enbState.indexOf(oriCon)> -1){
				document.getElementById('originState').disabled = false; 
				document.getElementById('originState').value='${customerFile.originState}';
			}else{
				document.getElementById('originState').disabled = true;
				document.getElementById('originState').value ="";
			} 
		}
	 }
catch(e){}
try{
	var desCon=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	 var enbState = '${enbState}';
	 if(desCon=="")	 {
			document.getElementById('destinationState').disabled = true;
			document.getElementById('destinationState').value ="";
	 }else{
	  		if(enbState.indexOf(desCon)> -1){
				document.getElementById('destinationState').disabled = false;		
				document.getElementById('destinationState').value='${customerFile.destinationState}';
			}else{
				document.getElementById('destinationState').disabled = true;
				document.getElementById('destinationState').value ="";
			}
	 	}
	}
catch(e){}
</script>
<script type="text/javascript">
    var fieldName = document.forms['customerFileForm'].elements['field'].value;
    var fieldName1 = document.forms['customerFileForm'].elements['field1'].value;
	if(fieldName!=''){
		document.forms['customerFileForm'].elements[fieldName].className = 'rules-textUpper';		
	}
	if(fieldName1!=''){
		document.forms['customerFileForm'].elements[fieldName1].className = 'rules-textUpper';		
	}
 </script>