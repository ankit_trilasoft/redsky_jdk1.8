DELIMITER $$
create trigger redsky.trigger_add_history_exchangerate
BEFORE UPDATE on redsky.exchangerate
for each row BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

      
 

      
      IF (OLD.currency <> NEW.currency ) THEN
       select CONCAT(fieldNameList,'exchangerate.currency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.currency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.currency,'~') into newValueList;
   END IF;


      
      IF (OLD.baseCurrency <> NEW.baseCurrency ) THEN
       select CONCAT(fieldNameList,'exchangerate.baseCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.baseCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.baseCurrency,'~') into newValueList;
   END IF;


      
      IF (OLD.manualUpdate <> NEW.manualUpdate ) THEN
       select CONCAT(fieldNameList,'exchangerate.manualUpdate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.manualUpdate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.manualUpdate,'~') into newValueList;
   END IF;


  



 IF ((OLD.valueDate <> NEW.valueDate) or (OLD.valueDate is null and NEW.valueDate is not null)
      or (OLD.valueDate is not null and NEW.valueDate is null)) THEN

       select CONCAT(fieldNameList,'exchangerate.valueDate~') into fieldNameList;
        IF(OLD.valueDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.valueDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.valueDate is not null) THEN
        	select CONCAT(oldValueList,OLD.valueDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.valueDate is not null) THEN
        	select CONCAT(newValueList,NEW.valueDate,'~') into newValueList;
      	END IF;

   END IF;





   IF ((OLD.baseCurrencyRate <> NEW.baseCurrencyRate) or (OLD.baseCurrencyRate is null and NEW.baseCurrencyRate is not null)
      or (OLD.baseCurrencyRate is not null and NEW.baseCurrencyRate is null)) THEN
        select CONCAT(fieldNameList,'exchangerate.baseCurrencyRate~') into fieldNameList;
        IF(OLD.baseCurrencyRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.baseCurrencyRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.baseCurrencyRate is not null) then
            select CONCAT(oldValueList,OLD.baseCurrencyRate,'~') into oldValueList;
        END IF;
        IF(NEW.baseCurrencyRate is not null) then
            select CONCAT(newValueList,NEW.baseCurrencyRate,'~') into newValueList;
        END IF;
    END IF;
    

   IF ((OLD.currencyBaseRate <> NEW.currencyBaseRate) or (OLD.currencyBaseRate is null and NEW.currencyBaseRate is not null)
      or (OLD.currencyBaseRate is not null and NEW.currencyBaseRate is null)) THEN
        select CONCAT(fieldNameList,'exchangerate.currencyBaseRate~') into fieldNameList;
        IF(OLD.currencyBaseRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.currencyBaseRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.currencyBaseRate is not null) then
            select CONCAT(oldValueList,OLD.currencyBaseRate,'~') into oldValueList;
        END IF;
        IF(NEW.currencyBaseRate is not null) then
            select CONCAT(newValueList,NEW.currencyBaseRate,'~') into newValueList;
        END IF;
    END IF;
    

   IF ((OLD.officialRate <> NEW.officialRate) or (OLD.officialRate is null and NEW.officialRate is not null)
      or (OLD.officialRate is not null and NEW.officialRate is null)) THEN
        select CONCAT(fieldNameList,'exchangerate.officialRate~') into fieldNameList;
        IF(OLD.officialRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.officialRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.officialRate is not null) then
            select CONCAT(oldValueList,OLD.officialRate,'~') into oldValueList;
        END IF;
        IF(NEW.officialRate is not null) then
            select CONCAT(newValueList,NEW.officialRate,'~') into newValueList;
        END IF;
    END IF;
    

   IF ((OLD.marginApplied <> NEW.marginApplied) or (OLD.marginApplied is null and NEW.marginApplied is not null)
      or (OLD.marginApplied is not null and NEW.marginApplied is null)) THEN
        select CONCAT(fieldNameList,'exchangerate.marginApplied~') into fieldNameList;
        IF(OLD.marginApplied is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.marginApplied is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.marginApplied is not null) then
            select CONCAT(oldValueList,OLD.marginApplied,'~') into oldValueList;
        END IF;
        IF(NEW.marginApplied is not null) then
            select CONCAT(newValueList,NEW.marginApplied,'~') into newValueList;
        END IF;
    END IF;
    
   CALL add_tblHistory (OLD.id,"exchangerate", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
END
$$

