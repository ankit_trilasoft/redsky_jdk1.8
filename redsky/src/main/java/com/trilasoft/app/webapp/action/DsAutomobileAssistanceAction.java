package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DsAutomobileAssistance;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DsAutomobileAssistanceManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;




public class DsAutomobileAssistanceAction extends BaseAction implements Preparable {
	
	private Long id;
	private String sessionCorpID;
	private  DsAutomobileAssistance dsAutomobileAssistance;
	private DsAutomobileAssistanceManager dsAutomobileAssistanceManager;
	private Long serviceOrderId;
	private CustomerFile customerFile;
	private CustomerFileManager customerFileManager;
	private  Miscellaneous  miscellaneous;
	private MiscellaneousManager miscellaneousManager ;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private String countDSAutomobileAssistanceNotes;	
	private NotesManager notesManager;
	private String gotoPageString;
	private String validateFormNav;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(DsAutomobileAssistanceAction.class);

	 

	
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
	}
	
	
    public DsAutomobileAssistanceAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}    
	private String shipSize;
	private String minShip;
	private String countShip;
	private Map<String, String> relocationServices;
	private RefMasterManager refMasterManager;
    
    
   	public String edit() {
   		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
   		if (id != null) {
			trackingStatus=trackingStatusManager.get(id);
			serviceOrder=serviceOrderManager.get(id);
	    	customerFile = serviceOrder.getCustomerFile();
	    	miscellaneous = miscellaneousManager.get(id); 
	    	dsAutomobileAssistance = dsAutomobileAssistanceManager.get(id);
		} else {
			dsAutomobileAssistance = new DsAutomobileAssistance(); 
			dsAutomobileAssistance.setCreatedOn(new Date());
			dsAutomobileAssistance.setUpdatedOn(new Date());
			dsAutomobileAssistance.setServiceOrderId(serviceOrderId);
		} 
		shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();		
		relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
		
		getNotesForIconChange();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
   	
   	
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (dsAutomobileAssistance.getId() == null);  
				if (isNew) { 
					dsAutomobileAssistance.setCreatedOn(new Date());
		        }
				trackingStatus=trackingStatusManager.get(id);
				serviceOrder=serviceOrderManager.get(id);
		    	customerFile = serviceOrder.getCustomerFile();
		    	miscellaneous = miscellaneousManager.get(id); 
		    	dsAutomobileAssistance.setCorpID(sessionCorpID);
		    	dsAutomobileAssistance.setUpdatedOn(new Date());
		    	dsAutomobileAssistance.setUpdatedBy(getRequest().getRemoteUser()); 
		    	dsAutomobileAssistanceManager.save(dsAutomobileAssistance);  
		    	if(!(validateFormNav=="OK"))
		    	{
		    	String key = (isNew) ? "dsAutomobileAssistance.added" : "dsAutomobileAssistance.updated";
			    saveMessage(getText(key));
		    	}
			    getNotesForIconChange();
			    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
				
			
	}
	@SkipValidation
	public String saveOnTabChange() throws Exception {
		validateFormNav = "OK";
		String s = save();
		return SUCCESS;
	}
	
	@SkipValidation
	public String getNotesForIconChange() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List noteDS = notesManager.countForDSAutomobileAssistanceNotes(serviceOrder.getShipNumber());
	
		
		if (noteDS.isEmpty()) {
			countDSAutomobileAssistanceNotes = "0";
		} else {
			countDSAutomobileAssistanceNotes = ((noteDS).get(0)).toString();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String getCountDSAutomobileAssistanceNotes() {
		return countDSAutomobileAssistanceNotes;
	}
	public void setCountDSAutomobileAssistanceNotes(String countDSAutomobileAssistanceNotes) {
		this.countDSAutomobileAssistanceNotes = countDSAutomobileAssistanceNotes;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public DsAutomobileAssistance getDsAutomobileAssistance() {
		return dsAutomobileAssistance;
	}
	public void setDsAutomobileAssistance(DsAutomobileAssistance dsAutomobileAssistance) {
		this.dsAutomobileAssistance = dsAutomobileAssistance;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public Long getServiceOrderId() {
		return serviceOrderId;
	}

	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public void setDsAutomobileAssistanceManager(DsAutomobileAssistanceManager dsAutomobileAssistanceManager) {
		this.dsAutomobileAssistanceManager = dsAutomobileAssistanceManager;
	}


	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}
	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}


	/**
	 * @return the countShip
	 */
	public String getCountShip() {
		return countShip;
	}


	/**
	 * @param countShip the countShip to set
	 */
	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}


	/**
	 * @return the minShip
	 */
	public String getMinShip() {
		return minShip;
	}


	/**
	 * @param minShip the minShip to set
	 */
	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}


	/**
	 * @return the relocationServices
	 */
	public Map<String, String> getRelocationServices() {
		return relocationServices;
	}


	/**
	 * @param relocationServices the relocationServices to set
	 */
	public void setRelocationServices(Map<String, String> relocationServices) {
		this.relocationServices = relocationServices;
	}


	/**
	 * @return the shipSize
	 */
	public String getShipSize() {
		return shipSize;
	}


	/**
	 * @param shipSize the shipSize to set
	 */
	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}


	/**
	 * @param refMasterManager the refMasterManager to set
	 */
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}	

}


