package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.AgentBaseSCACDao;
import com.trilasoft.app.model.AgentBaseSCAC;

public class AgentBaseSCACDaoHibernate extends GenericDaoHibernate<AgentBaseSCAC, Long> implements AgentBaseSCACDao{
	
	public AgentBaseSCACDaoHibernate() {
		super(AgentBaseSCAC.class);
	}
	
	public List getbaseSCACList(String partnerCode, String baseCode, String sessionCorpID) {
		try{
		return getHibernateTemplate().find("from AgentBaseSCAC where baseCode = '"+baseCode+"' AND corpID ='"+sessionCorpID+"' AND partnerCode like '"+partnerCode+"%'");
		}catch (Exception e) {
    		logger.error("Error executing query"+ e,e);
    	}
    	return null;
	}

	public List getSCACList(String sessionCorpID) {
		try{
		return getHibernateTemplate().find("select distinct SCAC from CompanyDivision where corpID ='"+sessionCorpID+"' AND SCAC != '' AND SCAC != null");
		}catch (Exception e) {
    		logger.error("Error executing query"+ e,e);
    	}
    	return null;
	}

	public List isExisted(String baseCode, String partnerCode, String code, String sessionCorpID) {
		try{
		return getHibernateTemplate().find("from AgentBaseSCAC where baseCode = '"+baseCode+"' AND corpID ='"+sessionCorpID+"' AND partnerCode = '"+partnerCode+"' AND SCACCode = '"+code+"'");
		}catch (Exception e) {
    		logger.error("Error executing query"+ e,e);
    	}
    	return null;
	}

}
