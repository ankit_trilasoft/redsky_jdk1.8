package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.persistence.Transient;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;

import com.trilasoft.app.dao.ContractDao;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTOForPDeatil;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTOForToolTipRevenue;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.Contract;
import com.trilasoft.app.model.ListLinkData;

public class ContractDaoHibernate extends GenericDaoHibernate<Contract, Long> implements ContractDao {
	private HibernateUtil hibernateUtil;
	
	public ContractDaoHibernate() {
		super(Contract.class);
	}

	public List<Contract> findByJobType(String jobType) {
		return getHibernateTemplate().find("from Contract where jobType=?", jobType);
	}

	public List<Contract> findByBillingInstructionCode(String billingInstructionCode) {
		return getHibernateTemplate().find("from Contract where billingInstructionCode=?", billingInstructionCode);
	}

	public Map<String, String> findForContract(String corpID, String jobType) {
		Date endDate = null;
		String s = null;
		List<Contract> list = getHibernateTemplate().find("from Contract where endDate is " + endDate + " AND jobType='" + jobType + "' AND corpID='" + corpID + "' order by description");
		Map<String, String> parameterMap = new TreeMap<String, String>();
		for (Contract contract : list) {
			parameterMap.put(contract.getContract(), contract.getDescription());
		}
		return parameterMap;

	}

	public List findContracts(String contractType, String description, String jobType,String companyDivision, Boolean activeCheck, String corpId) {
		String query="";		
		if (activeCheck == false) {			
			query="select contract,description,jobType,companyDivision,begin,ending,updatedOn,id " +
				  " from contract where corpid = '"+corpId+"' and " +
				  "contract like '%" + contractType.replaceAll("'", "''") + "%' " 
				  	+ "AND description like '%" + description.replaceAll("'", "''")
					+ "%' AND jobType like '%" + jobType.replaceAll("'", "''") + "%' " 
					+ " AND companyDivision like '%" + companyDivision.replaceAll("'", "''") + "%'  " 
					 + "AND ((DATE_FORMAT(ending,'%Y-%m-%d')< DATE_FORMAT(now(),'%Y-%m-%d')) " 
					 + " OR (DATE_FORMAT(begin,'%Y-%m-%d')> DATE_FORMAT(now(),'%Y-%m-%d')))";
			
					/*return getHibernateTemplate().find(
					"from Contract where contract like '%" + contractType.replaceAll("'", "''") + "%' AND description like '%" + description.replaceAll("'", "''")
					+ "%' AND jobType like '%" + jobType.replaceAll("'", "''") + "%' AND companyDivision like '%" + companyDivision.replaceAll("'", "''") + "%'  AND ((DATE_FORMAT(ending,'%Y-%m-%d')< DATE_FORMAT(now(),'%Y-%m-%d')) OR (DATE_FORMAT(begin,'%Y-%m-%d')> DATE_FORMAT(now(),'%Y-%m-%d')))");
*/
		} else {
			/*return getHibernateTemplate().find(
					"from Contract where contract like '%" + contractType.replaceAll("'", "''") + "%' AND description like '%" + description.replaceAll("'", "''")
					+ "%' AND jobType like '%" + jobType.replaceAll("'", "''") + "%' AND companyDivision like '%" + companyDivision.replaceAll("'", "''") + "%'  AND ((begin is null OR begin <= DATE_FORMAT(now(),'%Y-%m-%d')) AND (ending is null OR DATE_FORMAT(ending,'%Y-%m-%d')>= DATE_FORMAT(now(),'%Y-%m-%d')))"); 
		*/
			query="select contract,description,jobType,companyDivision,begin,ending,updatedOn,id " +
			  " from contract where corpid = '"+corpId+"' and " 
			  + " contract like '%" + contractType.replaceAll("'", "''") + "%' AND description like '%" + description.replaceAll("'", "''")
			  + "%' AND jobType like '%" + jobType.replaceAll("'", "''") + "%' " 
			  + " AND companyDivision like '%" + companyDivision.replaceAll("'", "''") + "%'  " 
			  +	" AND ((begin is null OR begin <= DATE_FORMAT(now(),'%Y-%m-%d')) " 
			  +	" AND (ending is null OR DATE_FORMAT(ending,'%Y-%m-%d')>= DATE_FORMAT(now(),'%Y-%m-%d')))";		
		}
		List list= this.getSession().createSQLQuery(query).list();
		List <Object> contractList = new ArrayList<Object>();
		 Iterator it=list.iterator();
		 DTOForContract dTOForContract=null;
		  while(it.hasNext()){
			  String listVal ="";
			  Object [] row=(Object[])it.next();
			  dTOForContract=new DTOForContract(); 
			  dTOForContract.setId(Long.parseLong(row[7].toString()));  
			  if(row[0]==null){
				  dTOForContract.setContract("");
			  }else{
				  dTOForContract.setContract(row[0]);  
			  }
			  if(row[1]==null){
				  dTOForContract.setDescription("");
			  }else{
				  dTOForContract.setDescription(row[1]);  
			  } 
			  if(row[2]==null){
				  dTOForContract.setJobType("");
			  }else{
				  dTOForContract.setJobType(row[2]);  
			  } 
			  if(row[3]==null){
				  dTOForContract.setCompanyDivision("");
			  }else{
				  dTOForContract.setCompanyDivision(row[3]);  
			  } 
 			  if(row[0]!=null){
				List al=this.getSession().createSQLQuery("select count(*) from charges where contract ='"+row[0]+"'  and corpid = '"+corpId+"'").list();
				 if((!al.isEmpty())&&(al!=null)&&(al.get(0)!=null)){
						dTOForContract.setNoOfCharge(al.get(0).toString());
				 }else{
						dTOForContract.setNoOfCharge("0");
				 }
			  }else{
				  dTOForContract.setNoOfCharge("0");
			  }			  
				  dTOForContract.setBegin(row[4]);  
				  
				  dTOForContract.setEnding(row[5]);
				  
				  dTOForContract.setUpdatedOn(row[6]);
				  
			  contractList.add(dTOForContract);
			} 
		return contractList;		
	}

	public List findForJobContract(String corpID, String jobType) {
		Date endDate = null;
		return getHibernateTemplate().find("select CONCAT(contract,'#',description) from Contract where endDate is " + endDate + " AND jobType='" + jobType + "' AND corpID='" + corpID + "' order by description");
	}

	public List findMaximumId() {
		return getHibernateTemplate().find("select max(id) from Contract");
	}
		
	public List findAllContract(String corpId) {		
		String query="select contract,description,jobType,companyDivision,begin,ending,updatedOn,id " +
				" from contract where corpid = '"+corpId+"' and " +
				"((begin is null OR begin <= DATE_FORMAT(now(),'%Y-%m-%d'))"+
				" AND ("+" ending is null "+" OR "+
				" DATE_FORMAT(ending,'%Y-%m-%d')"+" >= "+" DATE_FORMAT(now(),'%Y-%m-%d')))"+
				" group by contract";
		
		List list= this.getSession().createSQLQuery(query).list();
		List <Object> contractList = new ArrayList<Object>();
		 Iterator it=list.iterator();
		 DTOForContract dTOForContract=null;
		  while(it.hasNext()){
			  String listVal ="";
			  Object [] row=(Object[])it.next();
			  dTOForContract=new DTOForContract(); 
			  dTOForContract.setId(Long.parseLong(row[7].toString()));  
			  if(row[0]==null){
				  dTOForContract.setContract("");
			  }else{
				  dTOForContract.setContract(row[0]);  
			  }
			  if(row[1]==null){
				  dTOForContract.setDescription("");
			  }else{
				  dTOForContract.setDescription(row[1]);  
			  } 
			  if(row[2]==null){
				  dTOForContract.setJobType("");
			  }else{
				  dTOForContract.setJobType(row[2]);  
			  } 
			  if(row[3]==null){
				  dTOForContract.setCompanyDivision("");
			  }else{
				  dTOForContract.setCompanyDivision(row[3]);  
			  } 
 			  if(row[0]!=null){
				List al=this.getSession().createSQLQuery("select count(*) from charges where contract ='"+row[0]+"'  and corpid = '"+corpId+"'").list();
				 if((!al.isEmpty())&&(al!=null)&&(al.get(0)!=null)){
						dTOForContract.setNoOfCharge(al.get(0).toString());
				 }else{
						dTOForContract.setNoOfCharge("0");
				 }
			  }else{
				  dTOForContract.setNoOfCharge("0");
			  }			  
				  dTOForContract.setBegin(row[4]);  
			 
				  dTOForContract.setEnding(row[5]);  
			 
				  dTOForContract.setUpdatedOn(row[6]);  
			 
			  contractList.add(dTOForContract);
		  }
		return contractList;
	}
	public  class DTOForContract implements ListLinkData{	
	 	private Object contract;
		private Object description;
		private Object jobType;
		private Object companyDivision;
		private Object noOfCharge;
		private Object begin;
		private Object ending;
		private Object updatedOn;
		private Object id;
		
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getContract() {
			return contract;
		}
		public void setContract(Object contract) {
			this.contract = contract;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getJobType() {
			return jobType;
		}
		public void setJobType(Object jobType) {
			this.jobType = jobType;
		}
		public Object getCompanyDivision() {
			return companyDivision;
		}
		public void setCompanyDivision(Object companyDivision) {
			this.companyDivision = companyDivision;
		}		
		public Object getNoOfCharge() {
			return noOfCharge;
		}
		public void setNoOfCharge(Object noOfCharge) {
			this.noOfCharge = noOfCharge;
		}
		public Object getBegin() {
			return begin;
		}
		public void setBegin(Object begin) {
			this.begin = begin;
		}
		public Object getEnding() {
			return ending;
		}
		public void setEnding(Object ending) {
			this.ending = ending;
		}
		public Object getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(Object updatedOn) {
			this.updatedOn = updatedOn;
		}
		@Transient
		public String getListCode() {
			String contract1="";
			if(contract!=null){
				return this.contract.toString();
				}else{
					return contract1.toString();	
				} 
		}
		@Transient
		public String getListDescription() {
			// TODO Auto-generated method stub
			String contactValue="";
			return contactValue;
		}
		@Transient
		public String getListEigthDescription() {
			String contactValue="";
			return contactValue;
		}
		@Transient
		public String getListFifthDescription() {
			String contactValue="";
			return contactValue;
		}
		@Transient
		public String getListFourthDescription() {
			String contactValue="";
			return contactValue;
		}
		@Transient
		public String getListNinthDescription() {
			String contactValue="";
			return contactValue;
		}
		@Transient
		public String getListSecondDescription() {
			String contactValue="";
			return contactValue;
		}
		@Transient
		public String getListSeventhDescription() {
			String contactValue="";
			return contactValue;
		}
		@Transient
		public String getListSixthDescription() {
			String contactValue="";
			return contactValue;
		}
		@Transient
		public String getListTenthDescription() {
			String contactValue="";
			return contactValue;
		}
		@Transient
		public String getListThirdDescription() {
			String contactValue="";
			return contactValue;
		}
 }
	public Contract getContactListByContarct(String corpid,String contact){
		List contacts=getHibernateTemplate().find("from Contract where contract='"+contact+"' and corpID='"+corpid+"'");
		Contract cont;
		if(contacts!=null && (!contacts.isEmpty()) && contacts.get(0)!=null){
			cont=(Contract)contacts.get(0);
		}else{
			cont=new Contract();
		}
		return cont;
	}
	public List findComanyDivision(String corpID){
		 List compCode = new ArrayList();
		 compCode = getHibernateTemplate().find("select distinct c.companyCode  from CompanyDivision c where  c.corpID='"+corpID+"' and c.companyCode != '' order by c.companyCode ");
		return compCode;
	}
	String toDate;

	String fromDate;

	public List calculateCommission(Date openfromdate, Date opentodate, String corpId) {
		if (!(openfromdate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(openfromdate));
			fromDate = nowYYYYMMDD1.toString();
		}

		if (!(opentodate == null)) {
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(opentodate));
			toDate = nowYYYYMMDD2.toString();
		}

		/*
		 * List calcList = this.getSession().createSQLQuery("select s.shipnumber, s.registrationnumber, concat(s.lastname,', ',s.firstname) 'Shipper', s.job, s.status, c.estimator, b.contract, c.bookingagentcode, " +
				"sum(a.actualrevenue) 'Period Revenue', sum(a.distributionamount) 'Period Distribution', " +
				"round(if(c.source='H',sum(if(s.job in ('UVL','MVL'),a.distributionamount,a.actualrevenue) * ch.commission )*.5, sum(if(s.job in ('UVL','MVL'),a.distributionamount,a.actualrevenue) * ch.commission ) ),2) 'Commission', t.loadA " +
				"from customerfile c, serviceorder s, accountline a, billing b, trackingstatus t, contract ct, charges ch " +
				"where c.id = s.customerfileid and s.id = a.serviceorderid and s.id = t.id and b.contract = ct.contract and ch.charge = a.chargecode and ch.contract = b.contract and ch.commission <> 0 and s.id = b.id and " +
				"c.corpid = '" + corpId + "' and s.status not in ('CNCL','DWNLD') and ct.domesticSalesCommission = true and a.status is true and c.comptetive = 'Y' and a.recpostdate between '"
				+ fromDate + "' and '" + toDate + "' group by s.shipnumber").list();
		
		*/
		
		List calcList = this.getSession().createSQLQuery("select s.shipnumber, concat(s.lastname,', ',s.firstname) 'Shipper', s.job,  s.status, " +
				"c.estimator, b.contract, c.bookingagentcode, replace(replace(r.description,'\r\n',' '),'\t',' ') descriptionRef, a.chargecode, " +
				"if(s.job in ('UVL','MVL'),a.distributionamount,a.actualrevenue) distributionamount, " +
				"round(ch.commission*100,0) commPer, " +
				"round(if(c.source='H',if(s.job in ('UVL','MVL'),a.distributionamount,a.actualrevenue) * ch.commission *.5, if(s.job in ('UVL','MVL'),a.distributionamount,a.actualrevenue) * ch.commission  ),2) comm, " +
				"replace(replace(replace(a.description,'\r\n',' '),'\r',' '),'\t',' ') descriptionAcc, t.loadA, round(m.estimatednetweight,0) estWt, round(m.actualnetweight,0) actWt " +
				"from customerfile c " +
				"inner join serviceorder s on c.id = s.customerfileid " +
				"inner join accountline a on s.id = a.serviceorderid " +
				"inner join billing b on s.id = b.id  and b.corpid = '"+corpId+"' " +
				"inner join contract ct on b.contract = ct.contract and ct.corpid = '"+corpId+"' " +
				"inner join charges ch on ch.charge = a.chargecode and ch.contract = b.contract and ch.corpid = '"+corpId+"' " +
				"inner join trackingstatus t on s.id = t.id " +
				"inner join miscellaneous m on  s.id = m.id " +
				"left outer join refmaster r on c.source = r.code and r.parameter = 'LEAD' and r.corpid in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"') " +
				"where ch.commission <> 0 " +
				"and if(s.job in ('UVL','MVL'), a.distributionamount, a.actualrevenue) <> 0 " +
				"and c.corpid = '" + corpId + "' " +
				"and s.status not in ('CNCL','DWNLD') " +
				"and ct.domesticSalesCommission = true " +
				"and a.status is true and c.comptetive = 'Y' " +
				"and a.recpostdate between '" + fromDate + "' and '" + toDate + "' " +
				"order by c.estimator, s.shipnumber, a.chargecode")
				.addScalar("s.shipnumber", Hibernate.STRING)
				.addScalar("Shipper", Hibernate.STRING)
				.addScalar("s.job", Hibernate.STRING)
				.addScalar("s.status", Hibernate.STRING)
				.addScalar("c.estimator", Hibernate.STRING)
				.addScalar("b.contract", Hibernate.STRING)
				.addScalar("c.bookingagentcode", Hibernate.STRING)
				.addScalar("descriptionRef", Hibernate.TEXT)
				.addScalar("a.chargecode", Hibernate.STRING)
				.addScalar("distributionamount", Hibernate.STRING)
				.addScalar("commPer", Hibernate.STRING)
				.addScalar("comm", Hibernate.STRING)
				.addScalar("descriptionAcc", Hibernate.TEXT)
				.addScalar("t.loadA", Hibernate.DATE)
				.addScalar("estWt", Hibernate.STRING)
				.addScalar("actWt", Hibernate.STRING)
				.list();
		return calcList;

	}

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	public List agentContract(String sessionCorpID, String contract){
		return getHibernateTemplate().find("from ContractAccount where contract='"+contract+"' AND corpID='" +sessionCorpID+ "' and accountType='Agent'");
	}
	public List countRow(String contractName, String agentID){
		List check = this.getSession().createSQLQuery("select id from contract where contract = '"+contractName+"' and corpID='"+agentID+"'").list(); 
		return check;
	}
	public List findCostElement(String sessionCorpID){
		return getSession().createSQLQuery("select id from costelement where corpID='"+sessionCorpID+"'").list();
	}
	public String getOwnerContract(Long id){
		String contract=""; 
		List list=new ArrayList();
	    list =getSession().createSQLQuery("select concat(owner,'~',origContract) from contract where id='"+id+"'").list();
		if(list!=null && (!(list.isEmpty())) && list.get(0)!=null){
			contract=list.get(0).toString();
		}
		return contract;
	}
	public List countRowOwner(String owner, Long orginId , String agentID){
		List check = this.getSession().createSQLQuery("select concat(id,'~',contract) from contract where owner = '"+owner+"' and corpID='"+agentID+"' and origContract='"+orginId+"'").list(); 
		return check;
	}
	public int findContractCount(String corpId, String contract, String agentsCorpId, Long id){
		int countContract = 0;
		List temp =  this.getSession().createSQLQuery("select count(*) from contract where contract = '"+contract+"' and corpID='"+agentsCorpId+"' and origContract is null and owner='"+agentsCorpId+"'").list();
		if(temp!=null && !(temp.isEmpty())){
			String tempContract = temp.get(0).toString();
			countContract = Integer.parseInt(tempContract);
		}
		return countContract;
	}
	public List findAgentCodeList(String agentCorpId){
		List <Object> agentList = new ArrayList<Object>();
		List list =this.getSession().createSQLQuery("select bookingAgentCode from companydivision where  corpID='"+agentCorpId+"'")
								.addScalar("bookingAgentCode", Hibernate.STRING)
								.list();
		 Iterator it=list.iterator();
		 DTOForPCode dto=null;
		  while(it.hasNext()) 
		  {
			  Object  row=it.next();
			  dto=new DTOForPCode(); 
			  dto.setBookingAgentCode(row);
			  agentList.add(dto);
		  }
		  return agentList;
	}
	 public  class DTOForPCode{
		 private Object bookingAgentCode;

		public Object getBookingAgentCode() {
			return bookingAgentCode;
		}

		public void setBookingAgentCode(Object bookingAgentCode) {
			this.bookingAgentCode = bookingAgentCode;
		}		
	 }	
	 public List checkContract(String sessionCorpID, String contract){			
			List check = this.getSession().createSQLQuery("select contract from contract where contract='"+contract+"' AND corpID='" +sessionCorpID+ "'").list(); 
			return check;
		}	
	 public List getAllContractbyCorpId(String sessionCorpID){
		 return getHibernateTemplate().find("select contract from Contract where corpId='"+sessionCorpID+"' order by contract");
	 }
	 public void findDeleteContractOfOtherCorpId(String contractName, String agentCorpId){
		 try {
				List contractList = this.getSession().createSQLQuery("select id from contract where contract = '"+contractName+"' and corpID='"+agentCorpId+"'").list(); 
				if(contractList.size()>0){
					String contractId = contractList.toString().replace("[", "").replace("]", "");
					String query = "delete from contract where id in("+contractId+") and corpid='"+agentCorpId+"'";
					this.getSession().createSQLQuery(query).executeUpdate();
				}
			 } catch (Exception e) {
					e.printStackTrace();
			}
	 }
	 
	 public String callPublishContractProcedure(String contractName,String sessionCorpID,String agentCorpId,String updatedBy){
		 String returnVal="";
		 try {
			 Query q = this.getSession().createSQLQuery(" { call published_contract(?,?,?,?) }");
			 q.setParameter(0,contractName);
			 q.setParameter(1,sessionCorpID);
			 q.setParameter(2,agentCorpId);
			 q.setParameter(3,updatedBy);
			 q.executeUpdate();
		 } catch (Exception e) {
				e.printStackTrace();
		}
		 System.out.println("\n\n\n update contract set storageBillingGroup='',payMethod='',emailPrintOption='',creditTerms='' where contract='"+contractName+"' and corpID='"+agentCorpId+"' and contractType='DMM'");
		 getSession().createSQLQuery("update contract set storageBillingGroup='',payMethod='',emailPrintOption='',creditTerms='' where contract='"+contractName+"' and corpID='"+agentCorpId+"' and contractType='DMM'").executeUpdate();
		 return returnVal;
	 }
	 public List findCmmDmmAgentDromCompany(){
		 List agentList = new ArrayList();
		 try {
			 agentList = this.getSession().createSQLQuery("select corpid from company where cmmdmmAgent is true").list(); 
		 } catch (Exception e) {
				e.printStackTrace();
		}
		 return agentList;
	 }
	 public Map<String, String> getContractAndContractTypeMap(String sessionCorpID){
		 	List list = new ArrayList();
			Map<String, String> contractMap = new LinkedHashMap<String, String>(); 
		    try{	
				list = getSession() .createSQLQuery("select contract,contracttype from contract where corpid= '"+sessionCorpID+"'")
				.addScalar("contract", Hibernate.STRING)
				.addScalar("contracttype", Hibernate.STRING).list();
			 
			    Iterator iterator = list.iterator();
			     while (iterator.hasNext()) {
			      String contract = "";
			      String contracttype = "";
			      Object[] row = (Object[]) iterator.next();
			     // if (row[0] != null) {
			        if ((row[0] != null) && (!row[0].toString().equals(""))){
			           contract = row[0].toString();
			      }else{
			    	  contract = "NAA";
			      }
			      //if (row[1] != null) {
			        if ((row[1] != null) && (!row[1].toString().equals(""))){
			    	  contracttype = row[1].toString();
			      }else{
			    	  contracttype = "NAA";
			      } 
			      contractMap.put(contract, contracttype);
			     } 
			}catch (Exception e) {
				logger.error("Error executing query "+ e.getStackTrace()[0]);
			}
		return contractMap;
	 }
}