<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
<title>Label Tool Tip</title>   
<meta name="heading" content="Label Tool Tip"/> 
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>Label Tool Tip</b></td>
	<td align="right"  style="width:30px;">
		<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>  
<display:table name="tooltip" class="table" requestURI="" id="tooltipList" export="false" defaultsort="1" >
	<display:column title="Field Info"><c:out value="${tooltipList.description}" /></display:column>      
</display:table>