/**
 * 
 */
package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CustomDao;
import com.trilasoft.app.model.Custom;
import com.trilasoft.app.service.CustomManager;

/**
 * @author admin
 *
 */

public class CustomManagerImpl extends GenericManagerImpl<Custom, Long> implements CustomManager {   
    CustomDao customDao;   
    public CustomManagerImpl(CustomDao customDao) {   
        super(customDao);   
        this.customDao = customDao;   
    }
    public List findBySO(Long shipNumber, String corpId){
    return customDao.findBySO(shipNumber, corpId);	
    }
    public List countBondedGoods(Long soId, String sessionCorpID){
    	return customDao.countBondedGoods(soId, sessionCorpID);	
    }
	public List countForTicket(Long ticket, Long serviceOrderId, String movement, String sessionCorpID) {
		return customDao.countForTicket(ticket,serviceOrderId, movement,sessionCorpID);
	}
	public List customsInList(Long sid, String movement) {
		return customDao.customsInList(sid, movement);
	}
	public int checkCustomsId(Long transactionId, String movement, String sessionCorpID){
		return customDao.checkCustomsId(transactionId, movement, sessionCorpID);
	}
	public Long findRemoteCustom(Long networkId, Long serviceOrderId){
		return customDao.findRemoteCustom( networkId, serviceOrderId);
	}
	public void deleteNetworkCustom(Long serviceOrderId, Long networkId) {
		customDao.deleteNetworkCustom(serviceOrderId, networkId);
	}
	public List getTransactionIdCount(String sessionCorpID,Long customId){
		return customDao.getTransactionIdCount(sessionCorpID,customId);
	}
} 
