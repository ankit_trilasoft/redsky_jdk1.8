/**
 * This class represents the basic "CustomerFile" object in Redsky that allows for CustomerFile management.
 * @Class Name	CustomerFile
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.model;

import java.util.Date;
import java.util.Set; 
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table; 
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;


@Entity
@Table ( name="customerfile")
@FilterDefs({
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")}),
@FilterDef(name = "customerfileOriginDestinationCompanyCodeFilter", parameters = { @ParamDef(name = "origincompanycode", type = "string"), @ParamDef(name = "destinationcompanycode", type = "string")}),
@FilterDef(name = "serviceOrderBillToCodeFilter", parameters = { @ParamDef(name = "billtocode", type = "string")}),
@FilterDef(name = "serviceOrderCompanyDivisionFilter", parameters = { @ParamDef(name = "companydivision", type = "string")}),
@FilterDef(name = "serviceOrderBookingAgentCodeFilter", parameters = { @ParamDef(name = "bookingagentcode", type = "string")})
})
@Filters( {
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)"),
@Filter(name = "customerfileOriginDestinationCompanyCodeFilter", condition = " ( (originCompanyCode in (:origincompanycode))  or (destinationCompanyCode in (:destinationcompanycode))) "),
@Filter(name = "serviceOrderBillToCodeFilter", condition = " billToCode in (:billtocode)  "),
@Filter(name = "serviceOrderCompanyDivisionFilter", condition = " companyDivision in (:companydivision)  "),
@Filter(name = "serviceOrderBookingAgentCodeFilter", condition = "bookingAgentCode in (:bookingagentcode) ")
} )
public class CustomerFile extends BaseObject implements ToDoRuleRecord {
	private Long id;
	private String corpID;
	private String auditor;
	private String sequenceNumber;
	private String status;
	private String salesMan;
	private String prefix;
	private String firstName;
	private String lastName;
	private String middleInitial;
	private String contactName;
	private String contactNameExt;
	private String contactPhone;
	private String billToCode;
	private String billToName;
	private String billToReference;
	private String billToAuthorization;
	private String surveyTime;
	private String surveyTime2;
	private String sourceCode;
	private String source;
	private String comptetive;
	private String entitled;
	private String billPayMethod;
	private String originAddress1;
	private String originAddress2;
	private String originAddress3;
	private String originCity;
	private String originState;
	private String originZip;
	private String 	ugwIntId;
	private String originCountryCode;
	private String originCountry;
	private String destinationAddress1;
	private String destinationAddress2;
	private String destinationAddress3;
	private String destinationCity;
	private String destinationState;
	private String destinationZip;
	private String destinationCountryCode;
	private String destinationCountry;
	private String originDayPhone;
	private String originHomePhone;
	private String originFax;
	private String destinationDayPhone;
	private String destinationHomePhone;
	private String destinationFax;
	private String job;
	private String contactEmail;
	private String contract;
	private String socialSecurityNumber;
	private String coordinator;
	private String estimator;
	private String orderBy;
	private String orderPhone;
	private String email;
	private String organization;
	private String originCompany;
	private String destinationCompany;
	private String email2;
	private Date statusDate;
	private Date survey;
	/* Added as per requirement Bugzilla ID: 6228*/
	private Date actualSurveyDate;
	private Date moveDate;
	private String originDayPhoneExt;
	private String destinationDayPhoneExt;
	private int statusNumber;
	private String statusReason;
	private String customerPortalId;
	private Boolean portalIdActive;
	private Boolean secondaryEmailFlag;
	private String customerEmployer;
	private String destinationEmail;
	private String destinationEmail2;
	private String custPartnerLastName;
	private String custPartnerFirstName;
	private String custPartnerPrefix;
	private String custPartnerEmail;
	private String originCityCode;
	private String destinationCityCode;
	private Boolean vip;
	private String destinationContactName;
	private String destinationContactNameExt;
	private String destinationContactPhone;
	private String destinationContactEmail;
	private String destinationOrganization;
	private String originMobile;
	private String destinationMobile;
	private Boolean noCharge;
	private Date billApprovedDate;
	private String custPartnerContact;
	private String custPartnerMobile;
	private Date bookingDate;
	private Date initialContactDate;
	private Date priceSubmissionToAccDate;
	private Date priceSubmissionToTranfDate;
	private Date quoteAcceptenceDate;
	private Date SystemDate;
	private String personPricing;
	private String personBilling;
	private String personPayable;
	private String coordinatorName;
	private String estimatorName;
	private String assignmentType;
	private Boolean privileges;
	private String bookingAgentCode;
	private String bookingAgentName;
	private String rank;
	private String controlFlag;
	private String quotationStatus;
	private String approvedBy;
	private Date approvedOn;
	private String accountCode;
	private String accountName;
	private String companyDivision;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String createdBy;
	private String salesStatus;
	private String orderAction;
	private Boolean serviceAir;
	private Boolean serviceSurface;
	private Boolean serviceAuto;
	private Boolean serviceStorage;
	private Boolean servicePet;
	private String serviceRelo;
	private String orderComment;
	private String visaRequired;
	private String visaStatus;
	private String orderIntiationStatus;
	private Boolean isNetworkRecord;
	private String bookingAgentSequenceNumber;
	private Date corporateBenefitsReview;
	private Date exceptionRequest ;
	private Date exceptionRequestSentToCorporateContact  ;
	private Date exceptionResponse  ;
	private String iLeadNumber;
	//private String externalRefNumber;
    private String registrationNumber;
    private String orderEmail;
    private String homeCountry;
    private Date contractStart;
    private Date contractEnd;
    private String duration;
    private String homeCity;
    private String homeState;
    private String familySitiation;
    private String otherContract;
    private String localHrEmail;
    private String localHrPhone;
    private String localHr;
    private String parentAgent;
    private Boolean petsInvolved;
    private String petType;
    private String homeCountryCode;
    private Boolean serviceDomestic;
    private Boolean quotesToGoFlag;
    private String estimateNumber;
    private String  voxmeFlag;
    private Boolean doNotSendQRequest;
    private String originAgentEmail;
    private String originAgentName;
	private String originAgentCode;
	private String costCenter;
	private String partnerEntitle;
	private Integer familysize;
	private Boolean isNetworkGroup= new Boolean(false);
	private String contractType=new String("");
	private Boolean isUpdater;
	private String originPreferredContactTime;
	private String destPreferredContactTime;
	private String emailSentDate;
	private String qfStatusReason;
	private String noInsurance;
	private String  leadStatus;
	private Date leadStatusDate ;
	private String  surveyEmailLanguage;
	private String  cportalEmailLanguage;
	private Date prefSurveyDate1;
	private Date prefSurveyDate2;
	private String prefSurveyTime1;
	private String prefSurveyTime2;
	private String prefSurveyTime3;
	private String prefSurveyTime4;
	private String moveType;
	private String jobFunction;
	private String backToBackAssignment;
	private Date lastRunMMTime;
	private Date priceSubmissionToBookerDate;
	private String customerLanguagePreference;
	private String surveyReferenceNumber;
	private String orgLocId;
	private String destLocId;
	private String taskId;
	// Mapping Variables
	private Set<Quote> quotes;				//One To Many RelationShip With Quote
	private Set<PartnerQuote> partnerQuotes; //One To Many RelationShip With PartnerQuote
	private Set<ServiceOrder> serviceOrders; //One To Many RelationShip With ServiceOrder
	private Set<Billing> agentQuotesbillings;
	private String internalBillingPerson;
	private Date welcomeEmailOn;
    private String originAgentPhoneNumber;
    private String originAgentContact;
	private String surveyObservation;
	private Boolean jimExtract = new Boolean(false);
	private String originalCompanyCode;
	private String originalCompanyName;
	private Date originalCompanyHiringDate;
	private Boolean firstInternationalMove = new Boolean(false);
	private String authorizedBy;
	private String authorizedPhone;
	private String authorizedEmail;
	private String originCompanyCode;
	private String destinationCompanyCode;
	private String assignmentEndReason;
	private String currentEmploymentCode;
	private String currentEmploymentName;
	private String surveyType;
	private Boolean servicePov;
	private String introCall;
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("corpID", corpID)
				.append("auditor", auditor)
				.append("sequenceNumber", sequenceNumber)
				.append("status", status)
				.append("salesMan", salesMan)
				.append("prefix", prefix)
				.append("firstName", firstName)
				.append("lastName", lastName)
				.append("middleInitial", middleInitial)
				.append("contactName", contactName)
				.append("contactNameExt", contactNameExt)
				.append("contactPhone", contactPhone)
				.append("billToCode", billToCode)
				.append("billToName", billToName)
				.append("billToReference", billToReference)
				.append("billToAuthorization", billToAuthorization)
				.append("surveyTime", surveyTime)
				.append("surveyTime2", surveyTime2)
				.append("sourceCode", sourceCode)
				.append("source", source)
				.append("comptetive", comptetive)
				.append("entitled", entitled)
				.append("billPayMethod", billPayMethod)
				.append("originAddress1", originAddress1)
				.append("originAddress2", originAddress2)
				.append("originAddress3", originAddress3)
				.append("originCity", originCity)
				.append("originState", originState)
				.append("originZip", originZip)
				.append("ugwIntId", ugwIntId)
				.append("originCountryCode", originCountryCode)
				.append("originCountry", originCountry)
				.append("destinationAddress1", destinationAddress1)
				.append("destinationAddress2", destinationAddress2)
				.append("destinationAddress3", destinationAddress3)
				.append("destinationCity", destinationCity)
				.append("destinationState", destinationState)
				.append("destinationZip", destinationZip)
				.append("destinationCountryCode", destinationCountryCode)
				.append("destinationCountry", destinationCountry)
				.append("originDayPhone", originDayPhone)
				.append("originHomePhone", originHomePhone)
				.append("originFax", originFax)
				.append("destinationDayPhone", destinationDayPhone)
				.append("destinationHomePhone", destinationHomePhone)
				.append("destinationFax", destinationFax)
				.append("job", job)
				.append("contactEmail", contactEmail)
				.append("contract", contract)
				.append("socialSecurityNumber", socialSecurityNumber)
				.append("coordinator", coordinator)
				.append("estimator", estimator)
				.append("orderBy", orderBy)
				.append("orderPhone", orderPhone)
				.append("email", email)
				.append("organization", organization)
				.append("originCompany", originCompany)
				.append("destinationCompany", destinationCompany)
				.append("email2", email2)
				.append("statusDate", statusDate)
				.append("survey", survey)
				.append("actualSurveyDate", actualSurveyDate)
				.append("moveDate", moveDate)
				.append("originDayPhoneExt", originDayPhoneExt)
				.append("destinationDayPhoneExt", destinationDayPhoneExt)
				.append("statusNumber", statusNumber)
				.append("statusReason", statusReason)
				.append("customerPortalId", customerPortalId)
				.append("portalIdActive", portalIdActive)
				.append("secondaryEmailFlag", secondaryEmailFlag)
				.append("customerEmployer", customerEmployer)
				.append("destinationEmail", destinationEmail)
				.append("destinationEmail2", destinationEmail2)
				.append("custPartnerLastName", custPartnerLastName)
				.append("custPartnerFirstName", custPartnerFirstName)
				.append("custPartnerPrefix", custPartnerPrefix)
				.append("custPartnerEmail", custPartnerEmail)
				.append("originCityCode", originCityCode)
				.append("destinationCityCode", destinationCityCode)
				.append("vip", vip)
				.append("destinationContactName", destinationContactName)
				.append("destinationContactNameExt", destinationContactNameExt)
				.append("destinationContactPhone", destinationContactPhone)
				.append("destinationContactEmail", destinationContactEmail)
				.append("destinationOrganization", destinationOrganization)
				.append("originMobile", originMobile)
				.append("destinationMobile", destinationMobile)
				.append("noCharge", noCharge)
				.append("billApprovedDate", billApprovedDate)
				.append("custPartnerContact", custPartnerContact)
				.append("custPartnerMobile", custPartnerMobile)
				.append("bookingDate", bookingDate)
				.append("initialContactDate", initialContactDate)
				.append("priceSubmissionToAccDate", priceSubmissionToAccDate)
				.append("priceSubmissionToTranfDate",
						priceSubmissionToTranfDate)
				.append("quoteAcceptenceDate", quoteAcceptenceDate)
				.append("SystemDate", SystemDate)
				.append("personPricing", personPricing)
				.append("personBilling", personBilling)
				.append("personPayable", personPayable)
				.append("coordinatorName", coordinatorName)
				.append("estimatorName", estimatorName)
				.append("assignmentType", assignmentType)
				.append("privileges", privileges)
				.append("bookingAgentCode", bookingAgentCode)
				.append("bookingAgentName", bookingAgentName)
				.append("rank", rank)
				.append("controlFlag", controlFlag)
				.append("quotationStatus", quotationStatus)
				.append("approvedBy", approvedBy)
				.append("approvedOn", approvedOn)
				.append("accountCode", accountCode)
				.append("accountName", accountName)
				.append("companyDivision", companyDivision)
				.append("updatedBy", updatedBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("createdBy", createdBy)
				.append("salesStatus", salesStatus)
				.append("orderAction", orderAction)
				.append("serviceAir", serviceAir)
				.append("serviceSurface", serviceSurface)
				.append("serviceAuto", serviceAuto)
				.append("serviceStorage", serviceStorage)
				.append("servicePet", servicePet)
				.append("serviceRelo", serviceRelo)
				.append("orderComment", orderComment)
				.append("visaRequired", visaRequired)
				.append("visaStatus", visaStatus)
				.append("orderIntiationStatus", orderIntiationStatus)
				.append("isNetworkRecord", isNetworkRecord)
				.append("bookingAgentSequenceNumber",
						bookingAgentSequenceNumber)
				.append("corporateBenefitsReview", corporateBenefitsReview)
				.append("exceptionRequest", exceptionRequest)
				.append("exceptionRequestSentToCorporateContact",
						exceptionRequestSentToCorporateContact)
				.append("exceptionResponse", exceptionResponse)
				.append("iLeadNumber", iLeadNumber)
				.append("registrationNumber", registrationNumber)
				.append("orderEmail", orderEmail)
				.append("homeCountry", homeCountry)
				.append("contractStart", contractStart)
				.append("contractEnd", contractEnd)
				.append("duration", duration)
				.append("homeCity", homeCity)
				.append("homeState", homeState)
				.append("familySitiation", familySitiation)
				.append("otherContract", otherContract)
				.append("localHrEmail", localHrEmail)
				.append("localHrPhone", localHrPhone)
				.append("localHr", localHr)
				.append("parentAgent", parentAgent)
				.append("petsInvolved", petsInvolved)
				.append("petType", petType)
				.append("homeCountryCode", homeCountryCode)
				.append("serviceDomestic", serviceDomestic)
				.append("quotesToGoFlag", quotesToGoFlag)
				.append("estimateNumber", estimateNumber)
				.append("voxmeFlag", voxmeFlag)
				.append("doNotSendQRequest", doNotSendQRequest)
				.append("originAgentEmail", originAgentEmail)
				.append("originAgentName", originAgentName)
				.append("originAgentCode", originAgentCode)
				.append("costCenter", costCenter)
				.append("partnerEntitle", partnerEntitle)
				.append("familysize", familysize)
				.append("isNetworkGroup", isNetworkGroup)
				.append("contractType", contractType)
				.append("isUpdater", isUpdater)
				.append("originPreferredContactTime",
						originPreferredContactTime)
				.append("destPreferredContactTime", destPreferredContactTime)
				.append("emailSentDate", emailSentDate)
				.append("qfStatusReason", qfStatusReason)
				.append("noInsurance", noInsurance)
				.append("leadStatus", leadStatus)
				.append("leadStatusDate", leadStatusDate)
				.append("surveyEmailLanguage", surveyEmailLanguage)
				.append("cportalEmailLanguage", cportalEmailLanguage)
				.append("prefSurveyDate1", prefSurveyDate1)
				.append("prefSurveyDate2", prefSurveyDate2)
				.append("prefSurveyTime1", prefSurveyTime1)
				.append("prefSurveyTime2", prefSurveyTime2)
				.append("prefSurveyTime3", prefSurveyTime3)
				.append("prefSurveyTime4", prefSurveyTime4)
				.append("moveType", moveType)
				.append("jobFunction", jobFunction)
				.append("backToBackAssignment", backToBackAssignment)
				.append("lastRunMMTime",lastRunMMTime)
				.append("priceSubmissionToBookerDate",priceSubmissionToBookerDate)
				.append("customerLanguagePreference",customerLanguagePreference)
				.append("internalBillingPerson",internalBillingPerson)
				.append("surveyReferenceNumber",surveyReferenceNumber)
				.append("orgLocId",orgLocId)
				.append("destLocId",destLocId)
				.append("taskId",taskId)
				.append("welcomeEmailOn",welcomeEmailOn)
				.append("originAgentPhoneNumber",originAgentPhoneNumber)
				.append("originAgentContact",originAgentContact)
				.append("surveyObservation",surveyObservation)
				.append("jimExtract",jimExtract).append("originalCompanyCode",originalCompanyCode).append("originalCompanyName",originalCompanyName).append("originalCompanyHiringDate",originalCompanyHiringDate)
				.append("firstInternationalMove",firstInternationalMove).append("authorizedBy",authorizedBy).append("authorizedPhone",authorizedPhone).append("authorizedEmail",authorizedEmail)
				.append("originCompanyCode",originCompanyCode)
				.append("destinationCompanyCode",destinationCompanyCode)
				.append("assignmentEndReason",assignmentEndReason)
				.append("currentEmploymentCode",currentEmploymentCode)
				.append("currentEmploymentName",currentEmploymentName).append("surveyType",surveyType)
				.append("servicePov",servicePov)
				.append("introCall",introCall)
				.toString();
	}

	

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CustomerFile))
			return false;
		CustomerFile castOther = (CustomerFile) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(auditor, castOther.auditor)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(status, castOther.status)
				.append(salesMan, castOther.salesMan)
				.append(prefix, castOther.prefix)
				.append(firstName, castOther.firstName)
				.append(lastName, castOther.lastName)
				.append(middleInitial, castOther.middleInitial)
				.append(contactName, castOther.contactName)
				.append(contactNameExt, castOther.contactNameExt)
				.append(contactPhone, castOther.contactPhone)
				.append(billToCode, castOther.billToCode)
				.append(billToName, castOther.billToName)
				.append(billToReference, castOther.billToReference)
				.append(billToAuthorization, castOther.billToAuthorization)
				.append(surveyTime, castOther.surveyTime)
				.append(surveyTime2, castOther.surveyTime2)
				.append(sourceCode, castOther.sourceCode)
				.append(source, castOther.source)
				.append(comptetive, castOther.comptetive)
				.append(entitled, castOther.entitled)
				.append(billPayMethod, castOther.billPayMethod)
				.append(originAddress1, castOther.originAddress1)
				.append(originAddress2, castOther.originAddress2)
				.append(originAddress3, castOther.originAddress3)
				.append(originCity, castOther.originCity)
				.append(originState, castOther.originState)
				.append(originZip, castOther.originZip)
				.append(ugwIntId, castOther.ugwIntId)
				.append(originCountryCode, castOther.originCountryCode)
				.append(originCountry, castOther.originCountry)
				.append(destinationAddress1, castOther.destinationAddress1)
				.append(destinationAddress2, castOther.destinationAddress2)
				.append(destinationAddress3, castOther.destinationAddress3)
				.append(destinationCity, castOther.destinationCity)
				.append(destinationState, castOther.destinationState)
				.append(destinationZip, castOther.destinationZip)
				.append(destinationCountryCode,
						castOther.destinationCountryCode)
				.append(destinationCountry, castOther.destinationCountry)
				.append(originDayPhone, castOther.originDayPhone)
				.append(originHomePhone, castOther.originHomePhone)
				.append(originFax, castOther.originFax)
				.append(destinationDayPhone, castOther.destinationDayPhone)
				.append(destinationHomePhone, castOther.destinationHomePhone)
				.append(destinationFax, castOther.destinationFax)
				.append(job, castOther.job)
				.append(contactEmail, castOther.contactEmail)
				.append(contract, castOther.contract)
				.append(socialSecurityNumber, castOther.socialSecurityNumber)
				.append(coordinator, castOther.coordinator)
				.append(estimator, castOther.estimator)
				.append(orderBy, castOther.orderBy)
				.append(orderPhone, castOther.orderPhone)
				.append(email, castOther.email)
				.append(organization, castOther.organization)
				.append(originCompany, castOther.originCompany)
				.append(destinationCompany, castOther.destinationCompany)
				.append(email2, castOther.email2)
				.append(statusDate, castOther.statusDate)
				.append(survey, castOther.survey)
				.append(actualSurveyDate, castOther.actualSurveyDate)
				.append(moveDate, castOther.moveDate)
				.append(originDayPhoneExt, castOther.originDayPhoneExt)
				.append(destinationDayPhoneExt,
						castOther.destinationDayPhoneExt)
				.append(statusNumber, castOther.statusNumber)
				.append(statusReason, castOther.statusReason)
				.append(customerPortalId, castOther.customerPortalId)
				.append(portalIdActive, castOther.portalIdActive)
				.append(secondaryEmailFlag, castOther.secondaryEmailFlag)
				.append(customerEmployer, castOther.customerEmployer)
				.append(destinationEmail, castOther.destinationEmail)
				.append(destinationEmail2, castOther.destinationEmail2)
				.append(custPartnerLastName, castOther.custPartnerLastName)
				.append(custPartnerFirstName, castOther.custPartnerFirstName)
				.append(custPartnerPrefix, castOther.custPartnerPrefix)
				.append(custPartnerEmail, castOther.custPartnerEmail)
				.append(originCityCode, castOther.originCityCode)
				.append(destinationCityCode, castOther.destinationCityCode)
				.append(vip, castOther.vip)
				.append(destinationContactName,
						castOther.destinationContactName)
				.append(destinationContactNameExt,
						castOther.destinationContactNameExt)
				.append(destinationContactPhone,
						castOther.destinationContactPhone)
				.append(destinationContactEmail,
						castOther.destinationContactEmail)
				.append(destinationOrganization,
						castOther.destinationOrganization)
				.append(originMobile, castOther.originMobile)
				.append(destinationMobile, castOther.destinationMobile)
				.append(noCharge, castOther.noCharge)
				.append(billApprovedDate, castOther.billApprovedDate)
				.append(custPartnerContact, castOther.custPartnerContact)
				.append(custPartnerMobile, castOther.custPartnerMobile)
				.append(bookingDate, castOther.bookingDate)
				.append(initialContactDate, castOther.initialContactDate)
				.append(priceSubmissionToAccDate,
						castOther.priceSubmissionToAccDate)
				.append(priceSubmissionToTranfDate,
						castOther.priceSubmissionToTranfDate)
				.append(quoteAcceptenceDate, castOther.quoteAcceptenceDate)
				.append(SystemDate, castOther.SystemDate)
				.append(personPricing, castOther.personPricing)
				.append(personBilling, castOther.personBilling)
				.append(personPayable, castOther.personPayable)
				.append(coordinatorName, castOther.coordinatorName)
				.append(estimatorName, castOther.estimatorName)
				.append(assignmentType, castOther.assignmentType)
				.append(privileges, castOther.privileges)
				.append(bookingAgentCode, castOther.bookingAgentCode)
				.append(bookingAgentName, castOther.bookingAgentName)
				.append(rank, castOther.rank)
				.append(controlFlag, castOther.controlFlag)
				.append(quotationStatus, castOther.quotationStatus)
				.append(approvedBy, castOther.approvedBy)
				.append(approvedOn, castOther.approvedOn)
				.append(accountCode, castOther.accountCode)
				.append(accountName, castOther.accountName)
				.append(companyDivision, castOther.companyDivision)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(createdBy, castOther.createdBy)
				.append(salesStatus, castOther.salesStatus)
				.append(orderAction, castOther.orderAction)
				.append(serviceAir, castOther.serviceAir)
				.append(serviceSurface, castOther.serviceSurface)
				.append(serviceAuto, castOther.serviceAuto)
				.append(serviceStorage, castOther.serviceStorage)
				.append(servicePet, castOther.servicePet)
				.append(serviceRelo, castOther.serviceRelo)
				.append(orderComment, castOther.orderComment)
				.append(visaRequired, castOther.visaRequired)
				.append(visaStatus, castOther.visaStatus)
				.append(orderIntiationStatus, castOther.orderIntiationStatus)
				.append(isNetworkRecord, castOther.isNetworkRecord)
				.append(bookingAgentSequenceNumber,
						castOther.bookingAgentSequenceNumber)
				.append(corporateBenefitsReview,
						castOther.corporateBenefitsReview)
				.append(exceptionRequest, castOther.exceptionRequest)
				.append(exceptionRequestSentToCorporateContact,
						castOther.exceptionRequestSentToCorporateContact)
				.append(exceptionResponse, castOther.exceptionResponse)
				.append(iLeadNumber, castOther.iLeadNumber)
				.append(registrationNumber, castOther.registrationNumber)
				.append(orderEmail, castOther.orderEmail)
				.append(homeCountry, castOther.homeCountry)
				.append(contractStart, castOther.contractStart)
				.append(contractEnd, castOther.contractEnd)
				.append(duration, castOther.duration)
				.append(homeCity, castOther.homeCity)
				.append(homeState, castOther.homeState)
				.append(familySitiation, castOther.familySitiation)
				.append(otherContract, castOther.otherContract)
				.append(localHrEmail, castOther.localHrEmail)
				.append(localHrPhone, castOther.localHrPhone)
				.append(localHr, castOther.localHr)
				.append(parentAgent, castOther.parentAgent)
				.append(petsInvolved, castOther.petsInvolved)
				.append(petType, castOther.petType)
				.append(homeCountryCode, castOther.homeCountryCode)
				.append(serviceDomestic, castOther.serviceDomestic)
				.append(quotesToGoFlag, castOther.quotesToGoFlag)
				.append(estimateNumber, castOther.estimateNumber)
				.append(voxmeFlag, castOther.voxmeFlag)
				.append(doNotSendQRequest, castOther.doNotSendQRequest)
				.append(originAgentEmail, castOther.originAgentEmail)
				.append(originAgentName, castOther.originAgentName)
				.append(originAgentCode, castOther.originAgentCode)
				.append(costCenter, castOther.costCenter)
				.append(partnerEntitle, castOther.partnerEntitle)
				.append(familysize, castOther.familysize)
				.append(isNetworkGroup, castOther.isNetworkGroup)
				.append(contractType, castOther.contractType)
				.append(isUpdater, castOther.isUpdater)
				.append(originPreferredContactTime,
						castOther.originPreferredContactTime)
				.append(destPreferredContactTime,
						castOther.destPreferredContactTime)
				.append(emailSentDate, castOther.emailSentDate)
				.append(qfStatusReason, castOther.qfStatusReason)
				.append(noInsurance, castOther.noInsurance)
				.append(leadStatus, castOther.leadStatus)
				.append(leadStatusDate, castOther.leadStatusDate)
				.append(surveyEmailLanguage, castOther.surveyEmailLanguage)
				.append(cportalEmailLanguage, castOther.cportalEmailLanguage)
				.append(prefSurveyDate1, castOther.prefSurveyDate1)
				.append(prefSurveyDate2, castOther.prefSurveyDate2)
				.append(prefSurveyTime1, castOther.prefSurveyTime1)
				.append(prefSurveyTime2, castOther.prefSurveyTime2)
				.append(prefSurveyTime3, castOther.prefSurveyTime3)
				.append(prefSurveyTime4, castOther.prefSurveyTime4)
				.append(moveType, castOther.moveType)
				.append(jobFunction, castOther.jobFunction)
				.append(backToBackAssignment, castOther.backToBackAssignment)
				.append(lastRunMMTime, castOther.lastRunMMTime)
				.append(priceSubmissionToBookerDate, castOther.priceSubmissionToBookerDate)
				.append(customerLanguagePreference, castOther.customerLanguagePreference)
				.append(internalBillingPerson, castOther.internalBillingPerson)
				.append(surveyReferenceNumber, castOther.surveyReferenceNumber)
				.append(orgLocId, castOther.orgLocId)
				.append(destLocId, castOther.destLocId)
				.append(taskId, castOther.taskId)
				.append(welcomeEmailOn, castOther.welcomeEmailOn).append(originAgentPhoneNumber, castOther.originAgentPhoneNumber)
				.append(originAgentContact, castOther.originAgentContact)
				.append(surveyObservation, castOther.surveyObservation).append(jimExtract, castOther.jimExtract)
				.append(originalCompanyCode, castOther.originalCompanyCode).append(originalCompanyName, castOther.originalCompanyName).append(originalCompanyHiringDate, castOther.originalCompanyHiringDate)
				.append(firstInternationalMove, castOther.firstInternationalMove).append(authorizedBy, castOther.authorizedBy).append(authorizedPhone, castOther.authorizedPhone).append(authorizedEmail, castOther.authorizedEmail)
				.append(originCompanyCode,castOther.originCompanyCode)
				.append(destinationCompanyCode,castOther.destinationCompanyCode)
				.append(assignmentEndReason,castOther.assignmentEndReason)
				.append(currentEmploymentCode,castOther.currentEmploymentCode)
				.append(currentEmploymentName,castOther.currentEmploymentName)
				.append(surveyType,castOther.surveyType)
				.append(servicePov,castOther.servicePov)
				.append(introCall,castOther.introCall)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(auditor)
				.append(sequenceNumber).append(status).append(salesMan)
				.append(prefix).append(firstName).append(lastName)
				.append(middleInitial).append(contactName)
				.append(contactNameExt).append(contactPhone).append(billToCode)
				.append(billToName).append(billToReference)
				.append(billToAuthorization).append(surveyTime)
				.append(surveyTime2).append(sourceCode).append(source)
				.append(comptetive).append(entitled).append(billPayMethod)
				.append(originAddress1).append(originAddress2)
				.append(originAddress3).append(originCity).append(originState)
				.append(originZip).append(ugwIntId).append(originCountryCode)
				.append(originCountry).append(destinationAddress1)
				.append(destinationAddress2).append(destinationAddress3)
				.append(destinationCity).append(destinationState)
				.append(destinationZip).append(destinationCountryCode)
				.append(destinationCountry).append(originDayPhone)
				.append(originHomePhone).append(originFax)
				.append(destinationDayPhone).append(destinationHomePhone)
				.append(destinationFax).append(job).append(contactEmail)
				.append(contract).append(socialSecurityNumber)
				.append(coordinator).append(estimator).append(orderBy)
				.append(orderPhone).append(email).append(organization)
				.append(originCompany).append(destinationCompany)
				.append(email2).append(statusDate).append(survey)
				.append(actualSurveyDate).append(moveDate)
				.append(originDayPhoneExt).append(destinationDayPhoneExt)
				.append(statusNumber).append(statusReason)
				.append(customerPortalId).append(portalIdActive)
				.append(secondaryEmailFlag).append(customerEmployer)
				.append(destinationEmail).append(destinationEmail2)
				.append(custPartnerLastName).append(custPartnerFirstName)
				.append(custPartnerPrefix).append(custPartnerEmail)
				.append(originCityCode).append(destinationCityCode).append(vip)
				.append(destinationContactName)
				.append(destinationContactNameExt)
				.append(destinationContactPhone)
				.append(destinationContactEmail)
				.append(destinationOrganization).append(originMobile)
				.append(destinationMobile).append(noCharge)
				.append(billApprovedDate).append(custPartnerContact)
				.append(custPartnerMobile).append(bookingDate)
				.append(initialContactDate).append(priceSubmissionToAccDate)
				.append(priceSubmissionToTranfDate).append(quoteAcceptenceDate)
				.append(SystemDate).append(personPricing).append(personBilling)
				.append(personPayable).append(coordinatorName)
				.append(estimatorName).append(assignmentType)
				.append(privileges).append(bookingAgentCode)
				.append(bookingAgentName).append(rank).append(controlFlag)
				.append(quotationStatus).append(approvedBy).append(approvedOn)
				.append(accountCode).append(accountName)
				.append(companyDivision).append(updatedBy).append(createdOn)
				.append(updatedOn).append(createdBy).append(salesStatus)
				.append(orderAction).append(serviceAir).append(serviceSurface)
				.append(serviceAuto).append(serviceStorage).append(servicePet)
				.append(serviceRelo).append(orderComment).append(visaRequired)
				.append(visaStatus).append(orderIntiationStatus)
				.append(isNetworkRecord).append(bookingAgentSequenceNumber)
				.append(corporateBenefitsReview).append(exceptionRequest)
				.append(exceptionRequestSentToCorporateContact)
				.append(exceptionResponse).append(iLeadNumber)
				.append(registrationNumber).append(orderEmail)
				.append(homeCountry).append(contractStart).append(contractEnd)
				.append(duration).append(homeCity).append(homeState)
				.append(familySitiation).append(otherContract)
				.append(localHrEmail).append(localHrPhone).append(localHr)
				.append(parentAgent).append(petsInvolved).append(petType)
				.append(homeCountryCode).append(serviceDomestic)
				.append(quotesToGoFlag).append(estimateNumber)
				.append(voxmeFlag).append(doNotSendQRequest)
				.append(originAgentEmail).append(originAgentName)
				.append(originAgentCode).append(costCenter)
				.append(partnerEntitle).append(familysize)
				.append(isNetworkGroup).append(contractType).append(isUpdater)
				.append(originPreferredContactTime)
				.append(destPreferredContactTime).append(emailSentDate)
				.append(qfStatusReason).append(noInsurance).append(leadStatus)
				.append(leadStatusDate).append(surveyEmailLanguage)
				.append(cportalEmailLanguage).append(prefSurveyDate1)
				.append(prefSurveyDate2).append(prefSurveyTime1)
				.append(prefSurveyTime2).append(prefSurveyTime3)
				.append(prefSurveyTime4).append(moveType).append(jobFunction)
				.append(backToBackAssignment)
				.append(lastRunMMTime).append(priceSubmissionToBookerDate)
				.append(customerLanguagePreference).append(internalBillingPerson)
				.append(surveyReferenceNumber)
				.append(orgLocId)
				.append(destLocId)
				.append(taskId).append(welcomeEmailOn).append(originAgentPhoneNumber).append(originAgentContact).append(surveyObservation)
				.append(jimExtract).append(originalCompanyCode).append(originalCompanyName).append(originalCompanyHiringDate).append(firstInternationalMove).append(authorizedBy).append(authorizedPhone).append(authorizedEmail)
				.append(originCompanyCode)
				.append(destinationCompanyCode)
				.append(assignmentEndReason)
				.append(currentEmploymentCode)
				.append(currentEmploymentName)
				.append(surveyType)
				.append(servicePov)
				.append(introCall)
				.toHashCode();
	}

	@Column( length=8)
public String getAccountCode() {
	return accountCode;
}

@Column
public String getAccountName() {
	return accountName;
}

@Column( length=82)
public String getApprovedBy() {
	return approvedBy;
}
@Column
public Date getApprovedOn() {
	return approvedOn;
}

@Column( length=20 )
public String getAssignmentType() {
	return assignmentType;
}
@Column
public Date getBillApprovedDate() {
	return billApprovedDate;
}
@Column( length=5 )
	public String getBillPayMethod() {
		return billPayMethod;
	}
@Column( length=50 )
public String getBillToAuthorization() {
	return billToAuthorization;
}

@Column( length=8 )
public String getBillToCode() {
	return billToCode;
}
	@Column
	public String getBillToName() {
		return billToName;
	}
	@Column( length=20 )
	public String getBillToReference() {
		return billToReference;
	}
	@Column( length=8 )
	public String getBookingAgentCode() {
		return bookingAgentCode;
	}
	@Column
	public String getBookingAgentName() {
		return bookingAgentName;
	}
	@Column
	public Date getBookingDate() {
		return bookingDate;
	}
	@Column( length=10)
	public String getCompanyDivision() {
		return companyDivision;
	}
	@Column( length=1 )
	public String getComptetive() {
		return comptetive;
	}
	@Column( length=65 )
	public String getContactEmail() {
		return contactEmail;
	}
	@Column( length=95 )
	public String getContactName() {
		return contactName;
	}
	@Column( length=20 )
	public String getContactPhone() {
		return contactPhone;
	}
	@Column( length=100 )
	public String getContract() {
		return contract;
	}
	@Column( length=4 )
	public String getControlFlag() {
		return controlFlag;
	}
	@Column( length=82 )
	public String getCoordinator() {
		return coordinator;
	}
	@Column( length=55 )
	public String getCoordinatorName() {
		return coordinatorName;
	}
	@Column( length=15 )
	public String getCorpID() {
		return corpID;
	}
	@Column( length=82 )
	public String getCreatedBy() {
		return createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	@Column
	public String getCustomerEmployer() {
		return customerEmployer;
	}
	@Column( length=82 )
	public String getCustomerPortalId() {
		return customerPortalId;
	}
	@Column( length=20 )
	public String getCustPartnerContact() {
		return custPartnerContact;
	}
	@Column( length=65 )
	public String getCustPartnerEmail() {
		return custPartnerEmail;
	}
	@Column( length=80 )
	public String getCustPartnerFirstName() {
		return custPartnerFirstName;
	}
	@Column( length=80 )
	public String getCustPartnerLastName() {
		return custPartnerLastName;
	}
	@Column( length=25 )
	public String getCustPartnerMobile() {
		return custPartnerMobile;
	}
	@Column( length=15 )
	public String getCustPartnerPrefix() {
		return custPartnerPrefix;
	}
	@Column( length=50 )
	public String getDestinationAddress1() {
		return destinationAddress1;
	}
	@Column( length=50 )
	public String getDestinationAddress2() {
		return destinationAddress2;
	}
	@Column( length=50 )
	public String getDestinationAddress3() {
		return destinationAddress3;
	}
	@Column( length=100)
	public String getDestinationCity() {
		return destinationCity;
	}
	@Column( length=75 )
	public String getDestinationCityCode() {
		return destinationCityCode;
	}
	@Column( length=280 )
	public String getDestinationCompany() {
		return destinationCompany;
	}
	@Column( length=65 )
	public String getDestinationContactEmail() {
		return destinationContactEmail;
	}
	@Column( length=95 )
	public String getDestinationContactName() {
		return destinationContactName;
	}
	@Column( length=20 )
	public String getDestinationContactPhone() {
		return destinationContactPhone;
	}
	@Column( length=45 )
	public String getDestinationCountry() {
		return destinationCountry;
	}
	@Column( length=3 )
	public String getDestinationCountryCode() {
		return destinationCountryCode;
	}
	@Column( length=20 )
	public String getDestinationDayPhone() {
		return destinationDayPhone;
	}
	@Column
	public String getDestinationDayPhoneExt() {
		return destinationDayPhoneExt;
	}
	@Column( length=65 )
	public String getDestinationEmail() {
		return destinationEmail;
	}
	@Column( length=65 )
	public String getDestinationEmail2() {
		return destinationEmail2;
	}
	@Column( length=20 )
	public String getDestinationFax() {
		return destinationFax;
	}
	@Column( length=20 )
	public String getDestinationHomePhone() {
		return destinationHomePhone;
	}
	@Column( length=25 )
	public String getDestinationMobile() {
		return destinationMobile;
	}
	@Column( length=10 )
	public String getDestinationOrganization() {
		return destinationOrganization;
	}
	@Column( length=2 )
	public String getDestinationState() {
		return destinationState;
	}
	@Column( length=10 )
	public String getDestinationZip() {
		return destinationZip;
	}
	@Column( length=65 )
	public String getEmail() {
		return email;
	}
	@Column( length=65 )
	public String getEmail2() {
		return email2;
	}
	@Column(length=5000)
	public String getEntitled() {
		return entitled;
	}
	@Column( length=82 )
	public String getEstimator() {
		return estimator;
	}
	@Column( length=55 )
	public String getEstimatorName() {
		return estimatorName;
	}
	@Column( length=80 )
	public String getFirstName() {
		return firstName;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	@Column
	public Date getInitialContactDate() {
		return initialContactDate;
	}
	@Column( length=3 )
	public String getJob() {
		return job;
	}
	@Column( length=80 )
	public String getLastName() {
		return lastName;
	}
	@Column( length=1 )
	public String getMiddleInitial() {
		return middleInitial;
	}
	@Column
	public Date getMoveDate() {
		return moveDate;
	}
	@Column
	public Boolean getNoCharge() {
		return noCharge;
	}
	@Column( length=30 )
	public String getOrderBy() {
		return orderBy;
	}
	@Column( length=255 )
	public String getOrderPhone() {
		return orderPhone;
	}
	@Column( length=10 )
	public String getOrganization() {
		return organization;
	}
	@Column( length=50 )
	public String getOriginAddress1() {
		return originAddress1;
	}
	@Column( length=50 )
	public String getOriginAddress2() {
		return originAddress2;
	}

	@Column( length=50 )
	public String getOriginAddress3() {
		return originAddress3;
	}
	@Column( length=100)
	public String getOriginCity() {
		return originCity;
	}
	@Column( length=75 )
	public String getOriginCityCode() {
		return originCityCode;
	}
	@Column( length=280 )
	public String getOriginCompany() {
		return originCompany;
	}
	@Column( length=45 )
	public String getOriginCountry() {
		return originCountry;
	}
	@Column( length=3 )
	public String getOriginCountryCode() {
		return originCountryCode;
	}
	@Column( length=20 )
	public String getOriginDayPhone() {
		return originDayPhone;
	}
	@Column
	public String getOriginDayPhoneExt() {
		return originDayPhoneExt;
	}
	@Column( length=20 )
	public String getOriginFax() {
		return originFax;
	}
	@Column( length=20 )
	public String getOriginHomePhone() {
		return originHomePhone;
	}
	@Column( length=25 )
	public String getOriginMobile() {
		return originMobile;
	}
	@Column( length=2 )
	public String getOriginState() {
		return originState;
	}
	@Column( length=10 )
	public String getOriginZip() {
		return originZip;
	}
	@OneToMany(mappedBy="customerFile", cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	
	public Set<PartnerQuote> getPartnerQuotes() {
	return partnerQuotes;
	}
	@Column( length=82 )
	public String getPersonBilling() {
		return personBilling;
	}
	@Column( length=82 )
	public String getPersonPayable() {
		return personPayable;
	}
	@Column( length=82 )
	public String getPersonPricing() {
		return personPricing;
	}
	@Column
	public Boolean getPortalIdActive() {
		return portalIdActive;
	}
	@Column( length=15 )
	public String getPrefix() {
		return prefix;
	}
	@Column
	public Date getPriceSubmissionToAccDate() {
		return priceSubmissionToAccDate;
	}
	@Column
	public Date getPriceSubmissionToTranfDate() {
		return priceSubmissionToTranfDate;
	}
	@Column( length=20 )
	public Boolean getPrivileges() {
		return privileges;
	}
	@Column( length=20 )
	public String getQuotationStatus() {
		return quotationStatus;
	}
	@Column
	public Date getQuoteAcceptenceDate() {
		return quoteAcceptenceDate;
	}
	@OneToMany(mappedBy="customerFile", cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	
	public Set<Quote> getQuotes() {
	return quotes;
	}
	@Column( length=7 )
	public String getRank() {
		return rank;
	}
	@Column( length=15 )
	public String getSalesMan() {
		return salesMan;
	}
	@Column( length=20, unique= true )
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	@OneToMany(mappedBy="customerFile", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	
	public Set<ServiceOrder> getServiceOrders() {
	return serviceOrders;
	}
	/**
	 * @return the agentQuotesbillings
	 */
	@OneToMany(mappedBy="customerFile", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Billing> getAgentQuotesbillings() {
		return agentQuotesbillings;
	}

	/**
	 * @param agentQuotesbillings the agentQuotesbillings to set
	 */
	public void setAgentQuotesbillings(Set<Billing> agentQuotesbillings) {
		this.agentQuotesbillings = agentQuotesbillings;
	}
	
	
	@Column( length=30)
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}
	@Column( length=40 )
	public String getSource() {
		return source;
	}
	@Column( length=2 )
	public String getSourceCode() {
		return sourceCode;
	}
	@Column( length=6 )
	public String getStatus() {
		return status;
	}
	@Column
	public Date getStatusDate() {
		return statusDate;
	}
	@Column
	public int getStatusNumber() {
		return statusNumber;
	}
	@Column( length=20 )
	public String getStatusReason() {
		return statusReason;
	}
	@Column
	public Date getSurvey() {
		return survey;
	}
	@Column( length=5 )
	public String getSurveyTime() {
		return surveyTime;
	}
	@Column( length=5 )
	public String getSurveyTime2() {
		return surveyTime2;
	}
	@Column
	public Date getSystemDate() {
		return SystemDate;
	}
	@Column( length=82 )
	public String getUpdatedBy() {
		return updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	@Column
	public Boolean getVip() {
		return vip;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}
	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

	public void setAssignmentType(String assignmentType) {
		this.assignmentType = assignmentType;
	}
	public void setBillApprovedDate(Date billApprovedDate) {
		this.billApprovedDate = billApprovedDate;
	}

	public void setBillPayMethod(String billPayMethod) {
		this.billPayMethod = billPayMethod;
	}
	public void setBillToAuthorization(String billToAuthorization) {
		this.billToAuthorization = billToAuthorization;
	}

	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}

	public void setBillToReference(String billToReference) {
		this.billToReference = billToReference;
	}
	public void setBookingAgentCode(String bookingAgentCode) {
		this.bookingAgentCode = bookingAgentCode;
	}

	public void setBookingAgentName(String bookingAgentName) {
		this.bookingAgentName = bookingAgentName;
	}
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	public void setComptetive(String comptetive) {
		this.comptetive = comptetive;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}
	public void setControlFlag(String controlFlag) {
		this.controlFlag = controlFlag;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}
	public void setCoordinatorName(String coordinatorName) {
		this.coordinatorName = coordinatorName;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public void setCustomerEmployer(String customerEmployer) {
		this.customerEmployer = customerEmployer;
	}
	public void setCustomerPortalId(String customerPortalId) {
		this.customerPortalId = customerPortalId;
	}

	public void setCustPartnerContact(String custPartnerContact) {
		this.custPartnerContact = custPartnerContact;
	}
	public void setCustPartnerEmail(String custPartnerEmail) {
		this.custPartnerEmail = custPartnerEmail;
	}

	public void setCustPartnerFirstName(String custPartnerFirstName) {
		this.custPartnerFirstName = custPartnerFirstName;
	}
	public void setCustPartnerLastName(String custPartnerLastName) {
		this.custPartnerLastName = custPartnerLastName;
	}

	public void setCustPartnerMobile(String custPartnerMobile) {
		this.custPartnerMobile = custPartnerMobile;
	}

	public void setCustPartnerPrefix(String custPartnerPrefix) {
		this.custPartnerPrefix = custPartnerPrefix;
	}
	public void setDestinationAddress1(String destinationAddress1) {
		this.destinationAddress1 = destinationAddress1;
	}
	public void setDestinationAddress2(String destinationAddress2) {
		this.destinationAddress2 = destinationAddress2;
	}

	public void setDestinationAddress3(String destinationAddress3) {
		this.destinationAddress3 = destinationAddress3;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	public void setDestinationCityCode(String destinationCityCode) {
		this.destinationCityCode = destinationCityCode;
	}
	public void setDestinationCompany(String destinationCompany) {
		this.destinationCompany = destinationCompany;
	}
	public void setDestinationContactEmail(String destinationContactEmail) {
		this.destinationContactEmail = destinationContactEmail;
	}

	public void setDestinationContactName(String destinationContactName) {
		this.destinationContactName = destinationContactName;
	}

	public void setDestinationContactPhone(String destinationContactPhone) {
		this.destinationContactPhone = destinationContactPhone;
	}
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	public void setDestinationCountryCode(String destinationCountryCode) {
		this.destinationCountryCode = destinationCountryCode;
	}
	public void setDestinationDayPhone(String destinationDayPhone) {
		this.destinationDayPhone = destinationDayPhone;
	}
	public void setDestinationDayPhoneExt(String destinationDayPhoneExt) {
		this.destinationDayPhoneExt = destinationDayPhoneExt;
	}

	public void setDestinationEmail(String destinationEmail) {
		this.destinationEmail = destinationEmail;
	}
	public void setDestinationEmail2(String destinationEmail2) {
		this.destinationEmail2 = destinationEmail2;
	}

	public void setDestinationFax(String destinationFax) {
		this.destinationFax = destinationFax;
	}
	public void setDestinationHomePhone(String destinationHomePhone) {
		this.destinationHomePhone = destinationHomePhone;
	}

	public void setDestinationMobile(String destinationMobile) {
		this.destinationMobile = destinationMobile;
	}
	public void setDestinationOrganization(String destinationOrganization) {
		this.destinationOrganization = destinationOrganization;
	}
	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}

	public void setDestinationZip(String destinationZip) {
		this.destinationZip = destinationZip;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public void setEntitled(String entitled) {
		this.entitled = entitled;
	}
	public void setEstimator(String estimator) {
		this.estimator = estimator;
	}
	public void setEstimatorName(String estimatorName) {
		this.estimatorName = estimatorName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public void setInitialContactDate(Date initialContactDate) {
		this.initialContactDate = initialContactDate;
	}
	public void setJob(String job) {
		this.job = job;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}
	public void setMoveDate(Date moveDate) {
		this.moveDate = moveDate;
	}

	public void setNoCharge(Boolean noCharge) {
		this.noCharge = noCharge;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public void setOrderPhone(String orderPhone) {
		this.orderPhone = orderPhone;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public void setOriginAddress1(String originAddress1) {
		this.originAddress1 = originAddress1;
	}
	public void setOriginAddress2(String originAddress2) {
		this.originAddress2 = originAddress2;
	}

	public void setOriginAddress3(String originAddress3) {
		this.originAddress3 = originAddress3;
	}
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	public void setOriginCityCode(String originCityCode) {
		this.originCityCode = originCityCode;
	}

	public void setOriginCompany(String originCompany) {
		this.originCompany = originCompany;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	public void setOriginCountryCode(String originCountryCode) {
		this.originCountryCode = originCountryCode;
	}
	public void setOriginDayPhone(String originDayPhone) {
		this.originDayPhone = originDayPhone;
	}
	public void setOriginDayPhoneExt(String originDayPhoneExt) {
		this.originDayPhoneExt = originDayPhoneExt;
	}

	public void setOriginFax(String originFax) {
		this.originFax = originFax;
	}
	public void setOriginHomePhone(String originHomePhone) {
		this.originHomePhone = originHomePhone;
	}

	public void setOriginMobile(String originMobile) {
		this.originMobile = originMobile;
	}

	public void setOriginState(String originState) {
		this.originState = originState;
	}
	public void setOriginZip(String originZip) {
		this.originZip = originZip;
	}

	public void setPartnerQuotes(Set<PartnerQuote> partnerQuotes){
	this.partnerQuotes = partnerQuotes;
	}
	public void setPersonBilling(String personBilling) {
		this.personBilling = personBilling;
	}

	public void setPersonPayable(String personPayable) {
		this.personPayable = personPayable;
	}
	public void setPersonPricing(String personPricing) {
		this.personPricing = personPricing;
	}

	public void setPortalIdActive(Boolean portalIdActive) {
		this.portalIdActive = portalIdActive;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setPriceSubmissionToAccDate(Date priceSubmissionToAccDate) {
		this.priceSubmissionToAccDate = priceSubmissionToAccDate;
	}
	public void setPriceSubmissionToTranfDate(Date priceSubmissionToTranfDate) {
		this.priceSubmissionToTranfDate = priceSubmissionToTranfDate;
	}

	public void setPrivileges(Boolean privileges) {
		this.privileges = privileges;
	}
	public void setQuotationStatus(String quotationStatus) {
		this.quotationStatus = quotationStatus;
	}

	public void setQuoteAcceptenceDate(Date quoteAcceptenceDate) {
		this.quoteAcceptenceDate = quoteAcceptenceDate;
	}
	public void setQuotes(Set<Quote> quotes){
	this.quotes = quotes;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public void setServiceOrders(Set<ServiceOrder> serviceOrders){
	this.serviceOrders = serviceOrders;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public void setSource(String source) {
		this.source = source;
	}
	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public void setStatus(String flag) {
		status = flag;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	public void setStatusNumber(int statusNumber) {
		this.statusNumber = statusNumber;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}
	
	public void setSurvey(Date survey) {
		this.survey = survey;
	}
	public void setSurveyTime(String surveyTime) {
		this.surveyTime = surveyTime;
	}

	public void setSurveyTime2(String surveyTime2) {
		this.surveyTime2 = surveyTime2;
	}
	public void setSystemDate(Date systemDate) {
		SystemDate = systemDate;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public void setVip(Boolean vip) {
		this.vip = vip;
	}

	/**
	 * @return the contactNameExt
	 */
	@Column
	public String getContactNameExt() {
		return contactNameExt;
	}

	/**
	 * @param contactNameExt the contactNameExt to set
	 */
	public void setContactNameExt(String contactNameExt) {
		this.contactNameExt = contactNameExt;
	}

	/**
	 * @return the destinationContactNameExt
	 */
	@Column
	public String getDestinationContactNameExt() {
		return destinationContactNameExt;
	}

	/**
	 * @param destinationContactNameExt the destinationContactNameExt to set
	 */
	public void setDestinationContactNameExt(String destinationContactNameExt) {
		this.destinationContactNameExt = destinationContactNameExt;
	}

	/**
	 * @return the salesStatus
	 */
	@Column(length=10)
	public String getSalesStatus() {
		return salesStatus;
	}

	/**
	 * @param salesStatus the salesStatus to set
	 */
	public void setSalesStatus(String salesStatus) {
		this.salesStatus = salesStatus;
	}

	/**
	 * @return the secondaryEmailFlag
	 */
	@Column
	public Boolean getSecondaryEmailFlag() {
		return secondaryEmailFlag;
	}

	/**
	 * @param secondaryEmailFlag the secondaryEmailFlag to set
	 */
	public void setSecondaryEmailFlag(Boolean secondaryEmailFlag) {
		this.secondaryEmailFlag = secondaryEmailFlag;
	}
    
	@Column
	public String getAuditor() {
		return auditor;
	}

	public void setAuditor(String auditor) {
		this.auditor = auditor;
	}
	@Column(length=2)
	public String getOrderAction() {
		return orderAction;
	}

	public void setOrderAction(String orderAction) {
		this.orderAction = orderAction;
	}
	@Column( length=5000)
	public String getOrderComment() {
		return orderComment;
	}

	public void setOrderComment(String orderComment) {
		this.orderComment = orderComment;
	}
	@Column
	public Boolean getServiceAir() {
		return serviceAir;
	}

	public void setServiceAir(Boolean serviceAir) {
		this.serviceAir = serviceAir;
	}
	@Column
	public Boolean getServiceAuto() {
		return serviceAuto;
	}

	public void setServiceAuto(Boolean serviceAuto) {
		this.serviceAuto = serviceAuto;
	}
	@Column
	public Boolean getServicePet() {
		return servicePet;
	}

	public void setServicePet(Boolean servicePet) {
		this.servicePet = servicePet;
	}
	@Column( length=100 )
	public String getServiceRelo() {
		return serviceRelo;
	}

	public void setServiceRelo(String serviceRelo) {
		this.serviceRelo = serviceRelo;
	}
	
	@Column
	public Boolean getServiceStorage() {
		return serviceStorage;
	}

	public void setServiceStorage(Boolean serviceStorage) {
		this.serviceStorage = serviceStorage;
	}
	@Column
	public Boolean getServiceSurface() {
		return serviceSurface;
	}

	public void setServiceSurface(Boolean serviceSurface) {
		this.serviceSurface = serviceSurface;
	}
	@Column(length=3)
	public String getVisaRequired() {
		return visaRequired;
	}

	public void setVisaRequired(String visaRequired) {
		this.visaRequired = visaRequired;
	}
	@Column(length=50)
	public String getVisaStatus() {
		return visaStatus;
	}

	public void setVisaStatus(String visaStatus) {
		this.visaStatus = visaStatus;
	}

	@Column(length = 20)
	public String getOrderIntiationStatus() {
		return orderIntiationStatus;
	}

	public void setOrderIntiationStatus(String orderIntiationStatus) {
		this.orderIntiationStatus = orderIntiationStatus;
	}

	public Boolean getIsNetworkRecord() {
		return isNetworkRecord;
	}

	public void setIsNetworkRecord(Boolean isNetworkRecord) {
		this.isNetworkRecord = isNetworkRecord;
	}

	public String getBookingAgentSequenceNumber() {
		return bookingAgentSequenceNumber;
	}

	public void setBookingAgentSequenceNumber(String bookingAgentSequenceNumber) {
		this.bookingAgentSequenceNumber = bookingAgentSequenceNumber;
	}

	@Column
	public Date getCorporateBenefitsReview() {
		return corporateBenefitsReview;
	}

	public void setCorporateBenefitsReview(Date corporateBenefitsReview) {
		this.corporateBenefitsReview = corporateBenefitsReview;
	}

	@Column
	public Date getExceptionRequest() {
		return exceptionRequest;
	}

	public void setExceptionRequest(Date exceptionRequest) {
		this.exceptionRequest = exceptionRequest;
	}

	@Column
	public Date getExceptionRequestSentToCorporateContact() {
		return exceptionRequestSentToCorporateContact;
	}

	public void setExceptionRequestSentToCorporateContact(
			Date exceptionRequestSentToCorporateContact) {
		this.exceptionRequestSentToCorporateContact = exceptionRequestSentToCorporateContact;
	}

	@Column
	public Date getExceptionResponse() {
		return exceptionResponse;
	}

	public void setExceptionResponse(Date exceptionResponse) {
		this.exceptionResponse = exceptionResponse;
	}
	@Column(length=14)
	public String getILeadNumber() {
		return iLeadNumber;
	}

	public void setILeadNumber(String leadNumber) {
		iLeadNumber = leadNumber;
	}
	/*@Column(length=20)
	public String getExternalRefNumber() {
		return externalRefNumber;
	}

	public void setExternalRefNumber(String externalRefNumber) {
		this.externalRefNumber = externalRefNumber;
	}*/
    @Column(length=20)
	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	@Column
	public Date getContractEnd() {
		return contractEnd;
	}

	public void setContractEnd(Date contractEnd) {
		this.contractEnd = contractEnd;
	}

	@Column
	public Date getContractStart() {
		return contractStart;
	}

	public void setContractStart(Date contractStart) {
		this.contractStart = contractStart;
	}

	
	

	@Column( length=100)
	public String getHomeCity() {
		return homeCity;
	}

	public void setHomeCity(String homeCity) {
		this.homeCity = homeCity;
	}

	@Column( length=45 )
	public String getHomeCountry() {
		return homeCountry;
	}

	public void setHomeCountry(String homeCountry) {
		this.homeCountry = homeCountry;
	}

	@Column( length=65 )
	public String getOrderEmail() {
		return orderEmail;
	}

	public void setOrderEmail(String orderEmail) {
		this.orderEmail = orderEmail;
	}

	@Column(length=5)
	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}
	@Column( length=2 )
	public String getHomeState() {
		return homeState;
	}

	public void setHomeState(String homeState) {
		this.homeState = homeState;
	}
	@Column( length=30 )
	public String getFamilySitiation() {
		return familySitiation;
	}

	public void setFamilySitiation(String familySitiation) {
		this.familySitiation = familySitiation;
	}
	
	@Column( length=30 )
	public String getOtherContract() {
		return otherContract;
	}

	public void setOtherContract(String otherContract) {
		this.otherContract = otherContract;
	}
	@Column( length=30 )
	public String getLocalHr() {
		return localHr;
	}

	public void setLocalHr(String localHr) {
		this.localHr = localHr;
	}
	
	@Column( length=30 )
	public String getLocalHrEmail() {
		return localHrEmail;
	}

	public void setLocalHrEmail(String localHrEmail) {
		this.localHrEmail = localHrEmail;
	}
	@Column( length=255 )
	public String getLocalHrPhone() {
		return localHrPhone;
	}

	public void setLocalHrPhone(String localHrPhone) {
		this.localHrPhone = localHrPhone;
	}
	@Column( length=10 )
	public String getParentAgent() {
		return parentAgent;
	}

	public void setParentAgent(String parentAgent) {
		this.parentAgent = parentAgent;
	}
	@Column
	public Boolean getPetsInvolved() {
		return petsInvolved;
	}

	public void setPetsInvolved(Boolean petsInvolved) {
		this.petsInvolved = petsInvolved;
	}

	@Column( length=45 )
	public String getPetType() {
		return petType;
	}

	public void setPetType(String petType) {
		this.petType = petType;
	}
	
	@Column( length=3 )
	public String getHomeCountryCode() {
		return homeCountryCode;
	}

	public void setHomeCountryCode(String homeCountryCode) {
		this.homeCountryCode = homeCountryCode;
	}
	
	@Column
	public Boolean getServiceDomestic() {
		return serviceDomestic;
	}

	public void setServiceDomestic(Boolean serviceDomestic) {
		this.serviceDomestic = serviceDomestic;
	}

	public Boolean getQuotesToGoFlag() {
		return quotesToGoFlag;
	}

	public void setQuotesToGoFlag(Boolean quotesToGoFlag) {
		this.quotesToGoFlag = quotesToGoFlag;
	}

	public String getEstimateNumber() {
		return estimateNumber;
	}

	public void setEstimateNumber(String estimateNumber) {
		this.estimateNumber = estimateNumber;
	}
	@Column( length=1 )
	public String getVoxmeFlag() {
		return voxmeFlag;
	}

	public void setVoxmeFlag(String voxmeFlag) {
		this.voxmeFlag = voxmeFlag;
	}
	
	public Boolean getDoNotSendQRequest() {
		return doNotSendQRequest;
	}

	public void setDoNotSendQRequest(Boolean doNotSendQRequest) {
		this.doNotSendQRequest = doNotSendQRequest;
	}

	public String getOriginAgentName() {
		return originAgentName;
	}

	public void setOriginAgentName(String originAgentName) {
		this.originAgentName = originAgentName;
	}

	public String getOriginAgentCode() {
		return originAgentCode;
	}

	public void setOriginAgentCode(String originAgentCode) {
		this.originAgentCode = originAgentCode;
	}
	
	@Column( length=50 )
	public String getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	@Column( length=200 )
	public String getPartnerEntitle() {
		return partnerEntitle;
	}

	public void setPartnerEntitle(String partnerEntitle) {
		this.partnerEntitle = partnerEntitle;
	}
	
	@Column
	public Integer getFamilysize() {
		return familysize;
	}

	public void setFamilysize(Integer familysize) {
		this.familysize = familysize;
	}

	public Date getActualSurveyDate() {
		return actualSurveyDate;
	}

	public void setActualSurveyDate(Date actualSurveyDate) {
		this.actualSurveyDate = actualSurveyDate;
	}

	@Column
	public Boolean getIsNetworkGroup() {
		return isNetworkGroup;
	}

	public void setIsNetworkGroup(Boolean isNetworkGroup) {
		this.isNetworkGroup = isNetworkGroup;
	}
	@Column( length=3 )
	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	@Column
	public String getOriginPreferredContactTime() {
		return originPreferredContactTime;
	}

	public void setOriginPreferredContactTime(String originPreferredContactTime) {
		this.originPreferredContactTime = originPreferredContactTime;
	}
	@Column
	public String getDestPreferredContactTime() {
		return destPreferredContactTime;
	}

	public void setDestPreferredContactTime(String destPreferredContactTime) {
		this.destPreferredContactTime = destPreferredContactTime;
	}
	@Column
	public String getUgwIntId() {
		return ugwIntId;
	}

	public void setUgwIntId(String ugwIntId) {
		this.ugwIntId = ugwIntId;
	}
	@Column(length=50)
	public String getEmailSentDate() {
		return emailSentDate;
	}

	public void setEmailSentDate(String emailSentDate) {
		this.emailSentDate = emailSentDate;
	}
	@Column(length=30)
	public String getQfStatusReason() {
		return qfStatusReason;
	}

	public void setQfStatusReason(String qfStatusReason) {
		this.qfStatusReason = qfStatusReason;
	}
	@Column
	public String getNoInsurance() {
		return noInsurance;
	}

	public void setNoInsurance(String noInsurance) {
		this.noInsurance = noInsurance;
	}

	public String getLeadStatus() {
		return leadStatus;
	}

	public void setLeadStatus(String leadStatus) {
		this.leadStatus = leadStatus;
	}

	public Date getLeadStatusDate() {
		return leadStatusDate;
	}

	public void setLeadStatusDate(Date leadStatusDate) {
		this.leadStatusDate = leadStatusDate;
	}
	@Column(length=200)
	public String getSurveyEmailLanguage() {
		return surveyEmailLanguage;
	}

	public void setSurveyEmailLanguage(String surveyEmailLanguage) {
		this.surveyEmailLanguage = surveyEmailLanguage;
	}
	@Column(length=200)
	public String getCportalEmailLanguage() {
		return cportalEmailLanguage;
	}

	public void setCportalEmailLanguage(String cportalEmailLanguage) {
		this.cportalEmailLanguage = cportalEmailLanguage;
	}

	@Column
	public Date getPrefSurveyDate1() {
		return prefSurveyDate1;
	}

	public void setPrefSurveyDate1(Date prefSurveyDate1) {
		this.prefSurveyDate1 = prefSurveyDate1;
	}

	@Column
	public Date getPrefSurveyDate2() {
		return prefSurveyDate2;
	}

	public void setPrefSurveyDate2(Date prefSurveyDate2) {
		this.prefSurveyDate2 = prefSurveyDate2;
	}

	@Column
	public String getPrefSurveyTime1() {
		return prefSurveyTime1;
	}

	public void setPrefSurveyTime1(String prefSurveyTime1) {
		this.prefSurveyTime1 = prefSurveyTime1;
	}

	@Column
	public String getPrefSurveyTime2() {
		return prefSurveyTime2;
	}

	public void setPrefSurveyTime2(String prefSurveyTime2) {
		this.prefSurveyTime2 = prefSurveyTime2;
	}

	@Column
	public String getPrefSurveyTime3() {
		return prefSurveyTime3;
	}

	public void setPrefSurveyTime3(String prefSurveyTime3) {
		this.prefSurveyTime3 = prefSurveyTime3;
	}

	@Column
	public String getPrefSurveyTime4() {
		return prefSurveyTime4;
	}

	public void setPrefSurveyTime4(String prefSurveyTime4) {
		this.prefSurveyTime4 = prefSurveyTime4;
	}

	public String getMoveType() {
		return moveType;
	}

	public void setMoveType(String moveType) {
		this.moveType = moveType;
	}

	public String getJobFunction() {
		return jobFunction;
	}

	public void setJobFunction(String jobFunction) {
		this.jobFunction = jobFunction;
	}
	@Column(length=65)
	public String getOriginAgentEmail() {
		return originAgentEmail;
	}

	public void setOriginAgentEmail(String originAgentEmail) {
		this.originAgentEmail = originAgentEmail;
	}

	public String getBackToBackAssignment() {
		return backToBackAssignment;
	}

	public void setBackToBackAssignment(String backToBackAssignment) {
		this.backToBackAssignment = backToBackAssignment;
	}

	@Column
	public Date getLastRunMMTime() {
		return lastRunMMTime;
	}

	public void setLastRunMMTime(Date lastRunMMTime) {
		this.lastRunMMTime = lastRunMMTime;
	}
	@Column
	public Date getPriceSubmissionToBookerDate() {
		return priceSubmissionToBookerDate;
	}

	public void setPriceSubmissionToBookerDate(Date priceSubmissionToBookerDate) {
		this.priceSubmissionToBookerDate = priceSubmissionToBookerDate;
	}
	@Column
	public String getCustomerLanguagePreference() {
		return customerLanguagePreference;
	}

	public void setCustomerLanguagePreference(String customerLanguagePreference) {
		this.customerLanguagePreference = customerLanguagePreference;
	}
    @Column
	public Boolean getIsUpdater() {
		return isUpdater;
	}

	public void setIsUpdater(Boolean isUpdater) {
		this.isUpdater = isUpdater;
	}

	public String getInternalBillingPerson() {
		return internalBillingPerson;
	}

	public void setInternalBillingPerson(String internalBillingPerson) {
		this.internalBillingPerson = internalBillingPerson;
	}
	@Column
	public String getSurveyReferenceNumber() {
		return surveyReferenceNumber;
	}

	public void setSurveyReferenceNumber(String surveyReferenceNumber) {
		this.surveyReferenceNumber = surveyReferenceNumber;
	}
	@Column
	public String getOrgLocId() {
		return orgLocId;
	}

	public void setOrgLocId(String orgLocId) {
		this.orgLocId = orgLocId;
	}
	@Column
	public String getDestLocId() {
		return destLocId;
	}

	public void setDestLocId(String destLocId) {
		this.destLocId = destLocId;
	}
    @Column
	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public Date getWelcomeEmailOn() {
		return welcomeEmailOn;
	}

	public void setWelcomeEmailOn(Date welcomeEmailOn) {
		this.welcomeEmailOn = welcomeEmailOn;
	}
	@Column
	public String getOriginAgentPhoneNumber() {
		return originAgentPhoneNumber;
	}

	public void setOriginAgentPhoneNumber(String originAgentPhoneNumber) {
		this.originAgentPhoneNumber = originAgentPhoneNumber;
	}
	@Column(length = 200)
	public String getOriginAgentContact() {
		return originAgentContact;
	}

	public void setOriginAgentContact(String originAgentContact) {
		this.originAgentContact = originAgentContact;
	}
	@Column
	public String getSurveyObservation() {
		return surveyObservation;
	}
	public void setSurveyObservation(String surveyObservation) {
		this.surveyObservation = surveyObservation;
	}

	public Boolean getJimExtract() {
		return jimExtract;
	}

	public void setJimExtract(Boolean jimExtract) {
		this.jimExtract = jimExtract;
	}



	public String getOriginalCompanyCode() {
		return originalCompanyCode;
	}



	public void setOriginalCompanyCode(String originalCompanyCode) {
		this.originalCompanyCode = originalCompanyCode;
	}



	public String getOriginalCompanyName() {
		return originalCompanyName;
	}



	public void setOriginalCompanyName(String originalCompanyName) {
		this.originalCompanyName = originalCompanyName;
	}


	public Boolean getFirstInternationalMove() {
		return firstInternationalMove;
	}



	public void setFirstInternationalMove(Boolean firstInternationalMove) {
		this.firstInternationalMove = firstInternationalMove;
	}



	public String getAuthorizedBy() {
		return authorizedBy;
	}



	public void setAuthorizedBy(String authorizedBy) {
		this.authorizedBy = authorizedBy;
	}



	public String getAuthorizedPhone() {
		return authorizedPhone;
	}



	public void setAuthorizedPhone(String authorizedPhone) {
		this.authorizedPhone = authorizedPhone;
	}



	public String getAuthorizedEmail() {
		return authorizedEmail;
	}



	public void setAuthorizedEmail(String authorizedEmail) {
		this.authorizedEmail = authorizedEmail;
	}



	public Date getOriginalCompanyHiringDate() {
		return originalCompanyHiringDate;
	}



	public void setOriginalCompanyHiringDate(Date originalCompanyHiringDate) {
		this.originalCompanyHiringDate = originalCompanyHiringDate;
	}



	public String getOriginCompanyCode() {
		return originCompanyCode;
	}



	public void setOriginCompanyCode(String originCompanyCode) {
		this.originCompanyCode = originCompanyCode;
	}



	public String getDestinationCompanyCode() {
		return destinationCompanyCode;
	}



	public void setDestinationCompanyCode(String destinationCompanyCode) {
		this.destinationCompanyCode = destinationCompanyCode;
	}



	public String getAssignmentEndReason() {
		return assignmentEndReason;
	}



	public void setAssignmentEndReason(String assignmentEndReason) {
		this.assignmentEndReason = assignmentEndReason;
	}



	public String getCurrentEmploymentCode() {
		return currentEmploymentCode;
	}



	public void setCurrentEmploymentCode(String currentEmploymentCode) {
		this.currentEmploymentCode = currentEmploymentCode;
	}



	public String getCurrentEmploymentName() {
		return currentEmploymentName;
	}



	public void setCurrentEmploymentName(String currentEmploymentName) {
		this.currentEmploymentName = currentEmploymentName;
	}


    @Column
	public String getSurveyType() {
		return surveyType;
	}



	public void setSurveyType(String surveyType) {
		this.surveyType = surveyType;
	}
	
	  @Column
	public Boolean getServicePov() {
		return servicePov;
	}



	public void setServicePov(Boolean servicePov) {
		this.servicePov = servicePov;
	}


	@Column( length=1 )
	public String getIntroCall() {
		return introCall;
	}



	public void setIntroCall(String introCall) {
		this.introCall = introCall;
	}

	
}
