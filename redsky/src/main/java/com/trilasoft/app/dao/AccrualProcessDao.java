package com.trilasoft.app.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.AccountLine;

public interface AccrualProcessDao extends GenericDao<AccountLine, Long> {
	public List payableReverseAccruedList(Date endDate, String sessionCorpID);
	public List payableNewAccruals(Date endDate, String sessionCorpID);
	public List payableBalance(String sessionCorpID);
	public List receivableReverseAccruedList(Date endDate, String sessionCorpID);
	public List receivableNewAccruals(Date endDate, String sessionCorpID);
	public List receivableBalance(String sessionCorpID);
	
}