


DELIMITER $$
create trigger
redsky.trigger_add_history_refmaster
BEFORE
UPDATE
on
redsky.refmaster
for each row BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
     IF (OLD.code <> NEW.code ) THEN
      select CONCAT(fieldNameList,'refmaster.code~') into fieldNameList;
      select CONCAT(oldValueList,OLD.code,'~') into oldValueList;
      select CONCAT(newValueList,NEW.code,'~') into newValueList;
  END IF;

  IF ((OLD.fieldLength <> NEW.fieldLength) or (OLD.fieldLength is null and NEW.fieldLength is not null)
      or (OLD.fieldLength is not null and NEW.fieldLength is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.fieldLength~') into fieldNameList;
        IF(OLD.fieldLength is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.fieldLength is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.fieldLength is not null) then
            select CONCAT(oldValueList,OLD.fieldLength,'~') into oldValueList;
        END IF;
        IF(NEW.fieldLength is not null) then
            select CONCAT(newValueList,NEW.fieldLength,'~') into newValueList;
        END IF;
    END IF;


  IF ((OLD.number <> NEW.number) or (OLD.number is null and NEW.number is not null)
      or (OLD.number is not null and NEW.number is null)) THEN
        select CONCAT(fieldNameList,'miscellaneous.number~') into fieldNameList;
        IF(OLD.number is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.number is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.number is not null) then
            select CONCAT(oldValueList,OLD.number,'~') into oldValueList;
        END IF;
        IF(NEW.number is not null) then
            select CONCAT(newValueList,NEW.number,'~') into newValueList;
        END IF;
    END IF;

  IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'refmaster.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;

  IF (OLD.parameter <> NEW.parameter ) THEN
      select CONCAT(fieldNameList,'refmaster.parameter~') into fieldNameList;
      select CONCAT(oldValueList,OLD.parameter,'~') into oldValueList;
      select CONCAT(newValueList,NEW.parameter,'~') into newValueList;
  END IF;

  IF (OLD.bucket <> NEW.bucket ) THEN
      select CONCAT(fieldNameList,'refmaster.bucket~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bucket,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bucket,'~') into newValueList;
  END IF;

  IF (OLD.bucket2 <> NEW.bucket2 ) THEN
      select CONCAT(fieldNameList,'refmaster.bucket2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bucket2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bucket2,'~') into newValueList;
  END IF;

  IF ((OLD.stopDate <> NEW.stopDate) or (OLD.stopDate is null and NEW.stopDate is not null)
      or (OLD.stopDate is not null and NEW.stopDate is null)) THEN
      select CONCAT(fieldNameList,'refmaster.stopDate~') into fieldNameList;
      	IF(OLD.stopDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.stopDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;
    	IF(OLD.stopDate is not null) THEN
        	select CONCAT(oldValueList,OLD.stopDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.stopDate is not null) THEN
        	select CONCAT(newValueList,NEW.stopDate,'~') into newValueList;
      	END IF;
  END IF;

  IF (OLD.billCrew <> NEW.billCrew ) THEN
      select CONCAT(fieldNameList,'refmaster.billCrew~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billCrew,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billCrew,'~') into newValueList;
  END IF;

  IF (OLD.address1 <> NEW.address1 ) THEN
      select CONCAT(fieldNameList,'refmaster.address1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.address1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.address1,'~') into newValueList;
  END IF;

  IF (OLD.address2 <> NEW.address2 ) THEN
      select CONCAT(fieldNameList,'refmaster.address2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.address2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.address2,'~') into newValueList;
  END IF;

  IF (OLD.city <> NEW.city ) THEN
      select CONCAT(fieldNameList,'refmaster.city~') into fieldNameList;
      select CONCAT(oldValueList,OLD.city,'~') into oldValueList;
      select CONCAT(newValueList,NEW.city,'~') into newValueList;
  END IF;

  IF (OLD.state <> NEW.state ) THEN
      select CONCAT(fieldNameList,'refmaster.state~') into fieldNameList;
      select CONCAT(oldValueList,OLD.state,'~') into oldValueList;
      select CONCAT(newValueList,NEW.state,'~') into newValueList;
  END IF;

  IF (OLD.zip <> NEW.zip ) THEN
      select CONCAT(fieldNameList,'refmaster.zip~') into fieldNameList;
      select CONCAT(oldValueList,OLD.zip,'~') into oldValueList;
      select CONCAT(newValueList,NEW.zip,'~') into newValueList;
  END IF;

  IF (OLD.lastReset <> NEW.lastReset ) THEN
      select CONCAT(fieldNameList,'refmaster.lastReset~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lastReset,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lastReset,'~') into newValueList;
  END IF;
     IF (OLD.branch <> NEW.branch ) THEN
      select CONCAT(fieldNameList,'refmaster.branch~') into fieldNameList;
      select CONCAT(oldValueList,OLD.branch,'~') into oldValueList;
      select CONCAT(newValueList,NEW.branch,'~') into newValueList;
  END IF;


  
  
  
  
  
  
  
  
  
  IF (OLD.flex1 <> NEW.flex1 ) THEN
      select CONCAT(fieldNameList,'refmaster.flex1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.flex1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.flex1,'~') into newValueList;
  END IF;
  
  IF (OLD.globallyUniqueIdentifier <> NEW.globallyUniqueIdentifier ) THEN
      select CONCAT(fieldNameList,'refmaster.globallyUniqueIdentifier~') into fieldNameList;
      select CONCAT(oldValueList,OLD.globallyUniqueIdentifier,'~') into oldValueList;
      select CONCAT(newValueList,NEW.globallyUniqueIdentifier,'~') into newValueList;
  END IF;
  
  
  IF (OLD.flex2 <> NEW.flex2 ) THEN
      select CONCAT(fieldNameList,'refmaster.flex2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.flex2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.flex2,'~') into newValueList;
  END IF;
  
  
  IF (OLD.flex3 <> NEW.flex3 ) THEN
      select CONCAT(fieldNameList,'refmaster.flex3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.flex3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.flex3,'~') into newValueList;
  END IF;
  
  IF (OLD.flex4 <> NEW.flex4 ) THEN
      select CONCAT(fieldNameList,'refmaster.flex4~') into fieldNameList;
      select CONCAT(oldValueList,OLD.flex4,'~') into oldValueList;
      select CONCAT(newValueList,NEW.flex4,'~') into newValueList;
  END IF;
  
  
  IF (OLD.label1 <> NEW.label1 ) THEN
      select CONCAT(fieldNameList,'refmaster.label1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.label1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.label1,'~') into newValueList;
  END IF;
  
  
   IF (OLD.label2 <> NEW.label2 ) THEN
      select CONCAT(fieldNameList,'refmaster.label2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.label2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.label2,'~') into newValueList;
  END IF;
  
  
   IF (OLD.label3 <> NEW.label3 ) THEN
      select CONCAT(fieldNameList,'refmaster.label3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.label3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.label3,'~') into newValueList;
  END IF;
  
  
  
   IF (OLD.label4 <> NEW.label4 ) THEN
      select CONCAT(fieldNameList,'refmaster.label4~') into fieldNameList;
      select CONCAT(oldValueList,OLD.label4,'~') into oldValueList;
      select CONCAT(newValueList,NEW.label4,'~') into newValueList;
  END IF;
  
  
   IF (OLD.language <> NEW.language ) THEN
      select CONCAT(fieldNameList,'refmaster.language~') into fieldNameList;
      select CONCAT(oldValueList,OLD.language,'~') into oldValueList;
      select CONCAT(newValueList,NEW.language,'~') into newValueList;
  END IF;
  
  
  IF (OLD.flex5 <> NEW.flex5 ) THEN
      select CONCAT(fieldNameList,'refmaster.flex5~') into fieldNameList;
      select CONCAT(oldValueList,OLD.flex5,'~') into oldValueList;
      select CONCAT(newValueList,NEW.flex5,'~') into newValueList;
  END IF; 
  
  IF (OLD.flex6 <> NEW.flex6 ) THEN
      select CONCAT(fieldNameList,'refmaster.flex6~') into fieldNameList;
      select CONCAT(oldValueList,OLD.flex6,'~') into oldValueList;
      select CONCAT(newValueList,NEW.flex6,'~') into newValueList;
  END IF; 
  
  IF (OLD.label5 <> NEW.label5 ) THEN
      select CONCAT(fieldNameList,'refmaster.label5~') into fieldNameList;
      select CONCAT(oldValueList,OLD.label5,'~') into oldValueList;
      select CONCAT(newValueList,NEW.label5,'~') into newValueList;
  END IF; 
  
  IF (OLD.label6 <> NEW.label6 ) THEN
      select CONCAT(fieldNameList,'refmaster.label6~') into fieldNameList;
      select CONCAT(oldValueList,OLD.label6,'~') into oldValueList;
      select CONCAT(newValueList,NEW.label6,'~') into newValueList;
  END IF; 
  
  
  IF (OLD.status <> NEW.status ) THEN
      select CONCAT(fieldNameList,'refmaster.status~') into fieldNameList;
      select CONCAT(oldValueList,OLD.status,'~') into oldValueList;
      select CONCAT(newValueList,NEW.status,'~') into newValueList;
  END IF; 
  
  IF (OLD.flex7 <> NEW.flex7 ) THEN
      select CONCAT(fieldNameList,'refmaster.flex7~') into fieldNameList;
      select CONCAT(oldValueList,OLD.flex7,'~') into oldValueList;
      select CONCAT(newValueList,NEW.flex7,'~') into newValueList;
  END IF; 
  
  IF (OLD.flex8 <> NEW.flex8 ) THEN
      select CONCAT(fieldNameList,'refmaster.flex8~') into fieldNameList;
      select CONCAT(oldValueList,OLD.flex8,'~') into oldValueList;
      select CONCAT(newValueList,NEW.flex8,'~') into newValueList;
  END IF; 
  
  IF (OLD.label7 <> NEW.label7 ) THEN
      select CONCAT(fieldNameList,'refmaster.label7~') into fieldNameList;
      select CONCAT(oldValueList,OLD.label7,'~') into oldValueList;
      select CONCAT(newValueList,NEW.label7,'~') into newValueList;
  END IF; 
  
   IF (OLD.label8 <> NEW.label8 ) THEN
      select CONCAT(fieldNameList,'refmaster.label8~') into fieldNameList;
      select CONCAT(oldValueList,OLD.label8,'~') into oldValueList;
      select CONCAT(newValueList,NEW.label8,'~') into newValueList;
  END IF;

  
  
  CALL add_tblHistory (OLD.id,"refmaster", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
END
$$
