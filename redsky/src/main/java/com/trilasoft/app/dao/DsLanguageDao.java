package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DsLanguage;

public interface DsLanguageDao extends GenericDao<DsLanguage, Long>{
	List getListById(Long id);

}
