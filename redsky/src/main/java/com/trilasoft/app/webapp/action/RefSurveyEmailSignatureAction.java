package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.RefSurveyEmailSignature;
import com.trilasoft.app.service.RefSurveyEmailSignatureManager;
public class RefSurveyEmailSignatureAction extends BaseAction{
	private RefSurveyEmailSignatureManager refSurveyEmailSignatureManager;
	private RefSurveyEmailSignature refSurveyEmailSignature;
	private List refSurveyEmailSignatureList;
	private String sessionCorpID;
	private Long id;
	static final Logger logger = Logger.getLogger(RefSurveyEmailSignature.class);

	public RefSurveyEmailSignatureAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID=user.getCorpID();		
	}

	public RefSurveyEmailSignature getRefSurveyEmailSignature() {
		return refSurveyEmailSignature;
	}

	public void setRefSurveyEmailSignature(
			RefSurveyEmailSignature refSurveyEmailSignature) {
		this.refSurveyEmailSignature = refSurveyEmailSignature;
	}

	public List getRefSurveyEmailSignatureList() {
		return refSurveyEmailSignatureList;
	}

	public void setRefSurveyEmailSignatureList(List refSurveyEmailSignatureList) {
		this.refSurveyEmailSignatureList = refSurveyEmailSignatureList;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setRefSurveyEmailSignatureManager(
			RefSurveyEmailSignatureManager refSurveyEmailSignatureManager) {
		this.refSurveyEmailSignatureManager = refSurveyEmailSignatureManager;
	}

}
