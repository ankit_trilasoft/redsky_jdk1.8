package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CorpComponentPermission;

import java.util.*;

	
public interface CorpComponentPermissionDao extends GenericDao<CorpComponentPermission, Long>{
	
	public List findMaximumId();
	public List searchcompanyPermission(String componentId, String description,String corpId);
	public boolean getFieldVisibilityForComponent(String userCorpID, String componentId);
	public List findPermissionsByCorpId(String corpId);
	public List findCorpID();
	public Map<String, Integer> getComponentVisibilityAttrbuteDetail(String userCorpID, String componentId);
	public void updateCurrentFieldVisibilityMap();
}