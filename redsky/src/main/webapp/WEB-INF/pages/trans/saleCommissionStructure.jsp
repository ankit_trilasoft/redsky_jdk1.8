<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Sale Commission Structure</title>   
    <meta name="heading" content="Sale Commission Structure"/>  
    
<style>
#overlay111 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
.tab{
	border:1px solid #74B3DC;
}

span.pagelinks {
display:block;
font-size:0.9em;
margin-top:-14px;
text-align:right;
}

</style> 
<script type="text/javascript">
function clear_fields(){
	document.forms['saleCommissionStructurePage'].elements['commissionContract'].value = '';
	document.forms['saleCommissionStructurePage'].elements['commissionChargeCode'].value = '';
	document.forms['saleCommissionStructurePage'].elements['commissionJobType'].value = '';
	document.forms['saleCommissionStructurePage'].elements['commissionCompanyDivision'].value = '';
	document.forms['saleCommissionStructurePage'].elements['commissionIsActive'].checked = false;
	document.forms['saleCommissionStructurePage'].elements['commissionIsCommissionable'].checked = false;
 }
</script>
</head>
<s:form id="saleCommissionStructurePage" action="salesCommissionMgmt" method="post" validate="true">
<div id="overlay111">

<div id="layerLoading">

<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
  
   </div>
   </div>
<div id="layer1" style="width:100%;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>	
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/>     
</c:set>
<div id="content" align="center">
<div id="liquid-round" style="margin:0px;">
     <div class="top" style="margin-top: 11px;!margin-top: -4px;"><span></span></div>
    <div class="center-content">
		<table class="table" border="0">
		<thead>
		<tr>
		<th>Contract</th>
		<th>Charge Code</th>
		<th>Job Type</th>
		<th>Company Division</th>
		<th>Commissionable</th>
		<th>Active</th>		
		</tr>
		</thead>	
		<tbody>
				<tr>
  				    <td align="left">
					    <s:select cssClass="list-menu" name="commissionContract" list="%{contractList}" headerKey="" headerValue="" cssStyle="width:200px" />
					</td>
					<td align="left">
					    <s:textfield name="commissionChargeCode" required="true" cssClass="input-text" size="35"/>
					    <img id="commissionChargeCodeImg" align="middle" style="vertical-align:top;" class="openpopup" width="17" height="20" onclick="openChargesPopWindow();" src="<c:url value='/images/open-popup.gif'/>" />
					</td>					
  				    <td align="left">
					    <s:select cssClass="list-menu" name="commissionJobType" list="%{job}" headerKey="" headerValue="" cssStyle="width:140px" />
					</td>
					<td align="left">
					    <s:select cssClass="list-menu" name="commissionCompanyDivision" list="%{companyDivis}" headerKey="" headerValue="" cssStyle="width:90px" />
					</td>
					<td align="center">
					<s:checkbox key="commissionIsCommissionable" cssStyle="vertical-align:middle; margin:5px 23px 3px 38px;"/>
					</td>		
					<td align="center">
					<s:checkbox key="commissionIsActive" cssStyle="vertical-align:middle; margin:5px 13px 3px 15px;"/>
					</td>
					</tr>		
				<tr>								
				<td align="right" colspan="6" style="text-align:right;"><c:out value="${searchbuttons}" escapeXml="false"/></td></tr>			
		</tbody>
		</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 

<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Sale Commission List</span></a></li>
		    <li><a href="distinctContractList.html"><span>Commission Structure</span></a></li>
		  </ul>
		</div>
	<div class="spnblk">&nbsp;</div>	
	<c:if test="${saleCommissionList!='[]'}"> 
<display:table name="saleCommissionList" class="table" id="saleCommissionList" requestURI="" pagesize="25" style="width:100%;"> 
	
	<display:column property="contract" sortable="true" title="Contract"  style="width:90px"/>		
	<display:column property="charge" sortable="true" title="Charge Code"  maxLength="100" style="width:100px"/>		
	<display:column property="description" sortable="true" title="Description" maxLength="25" style="width:200px"/>				
	<display:column title="Commissionable" style="width:75px;text-align:center;">
		<s:checkbox id="commissionable${saleCommissionList.id}" name="commissionable" value="${saleCommissionList.commissionable}"  onclick="updateSalesCommission('${saleCommissionList.id}',this);" required="true" />
	</display:column>
	<display:column title="Flat %" style="width:75px;text-align:left;">
		<s:textfield id="commission${saleCommissionList.id}" cssStyle="border:1px solid #219DD1;font-size:12px;height:15px;width:75px;text-align:right;" name="commission" value="${saleCommissionList.commission}"  onchange="updateSalesCommission('${saleCommissionList.id}',this);" required="true" />
	</display:column>
	<display:column title="Third Party" style="width:60px;text-align:center;">
		<s:checkbox id="thirdParty${saleCommissionList.id}" name="thirdParty" value="${saleCommissionList.thirdParty}"  onclick="updateSalesCommission('${saleCommissionList.id}',this);" required="true" />
	</display:column>
	<display:column title="Gross Margin" style="width:60px;text-align:center;">
		<s:checkbox id="grossMargin${saleCommissionList.id}" name="grossMargin" value="${saleCommissionList.grossMargin}"  onclick="updateSalesCommission('${saleCommissionList.id}',this);" required="true" />
	</display:column>
	<display:column property="jobType" title="Variable" style="width:200px"/>
	<configByCorp:fieldVisibility componentId="component.field.Charge.EarnoutShow">
		<display:column property="earnout" sortable="true" title="Earnout%" style="width:80px"/>
	</configByCorp:fieldVisibility>
</display:table>
	</c:if>	
	
	<c:if test="${empty saleCommissionList}">
	<div id="content" align="center">
<div id="liquid-round" style="margin:0px;">
     <div class="top" style="margin-top:0px;!margin-top: -4px;"><span></span></div>
    <div class="center-content">
    <table>
		<tr>
		    <td width="50px"></td>
		    <td align="center" width="700px">
			<span class="listwhitetext" style="font-size:17px;">Data Not Available.</span></td>
		</tr>
		</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	</c:if>
</s:form>
<script type="text/javascript"> 
	   	try{
	   		if('${commissionIsActive}'=='true'){
   				document.forms['saleCommissionStructurePage'].elements['commissionIsActive'].checked = true;
   			}
   		}catch(e){}
  try{
  	 if('${commissionIsActive}'=='false') {
   		document.forms['saleCommissionStructurePage'].elements['commissionIsActive'].checked = false;
   	 }
   }catch(e){}
   	
   	function updateSalesCommission(id, targetElement){      
   		var commissionable = document.getElementById("commissionable"+id).checked;
   		var commission = document.getElementById("commission"+id).value;
   		var thirdParty= document.getElementById("thirdParty"+id).checked;
   		var grossMargin = document.getElementById("grossMargin"+id).checked;
   		showOrHide(1);
   		var url = "autoSaveSaleCommissionAjax.html?ajax=1&decorator=simple&popup=true&commisionId="+id+"&commissionIsCommissionable="+commissionable+"&commission="+commission+"&isThirdParty="+thirdParty+"&isGrossMargin="+grossMargin;
		http.open("GET", url, true);
	    http.onreadystatechange = handleHttpResponse;
	    http.send(null);
   	}
   	function handleHttpResponse(){
   		if (http.readyState == 4){
   			showOrHide(0);
   	            var results = http.responseText
   	            results = results.trim();
   	            if(results == '[]'){
   	            	alert('Resource already exist for this day');
   	            	document.getElementById(resoure_ID).value='';
   	            	resoure_ID='';
   	            }else if(results == ''){
   	            	document.getElementById(resoure_ID).value='';
   	            	alert('Resource already exist for this day');
   	            }} }
   	
   	var http = getHTTPObject();
   	function getHTTPObject() {
   	    var xmlhttp;
   	    if(window.XMLHttpRequest) {
   	        xmlhttp = new XMLHttpRequest();
   	    }else if (window.ActiveXObject) {
   	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
   	        if (!xmlhttp){
   	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
   	        }
   	    }
   	    return xmlhttp;
   	}
   	function showOrHide(value) { 
   	    if (value==0) {
   	       	if (document.layers)
   	           document.layers["overlay111"].visibility='hide';
   	        else
   	           document.getElementById("overlay111").style.visibility='hidden';
   	   	}else if (value==1) {
   	   		if (document.layers)
   	          document.layers["overlay111"].visibility='show';
   	       	else
   	          document.getElementById("overlay111").style.visibility='visible';
   	   	} }
   	</script>
   	 <script>
   	setTimeout("showOrHide(0)",2000);
   	function commisionRateGrid(jobType, id, comDiv,internalCost, contract,charge){
		  window.open('saleCommisionRateGrid.html?companyDiv='+comDiv+'&contract='+encodeURI(contract)+'&commissionChargeCode='+encodeURI(charge)+'&decorator=popup&popup=true&jobType='+jobType,'','width=670,height=550,scrollbars=yes');
	 }
   	function openChargesPopWindow(str,id){
			var contractVal = document.forms['saleCommissionStructurePage'].elements['commissionContract'].value;
			if(contractVal.trim().length > 0)
				javascript:openWindow('chargeForSaleCommision.html?contract='+escape(contractVal)+'&decorator=popup&popup=true');
			else{
				alert("Please Select a Contract Type");
				return false;
			}
   	}
</script>
