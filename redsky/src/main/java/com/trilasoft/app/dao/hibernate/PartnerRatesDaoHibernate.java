package com.trilasoft.app.dao.hibernate;

import java.util.List;   

import org.appfuse.dao.hibernate.GenericDaoHibernate;   

import com.trilasoft.app.dao.PartnerRatesDao;
import com.trilasoft.app.model.PartnerRates;   


public class PartnerRatesDaoHibernate extends GenericDaoHibernate<PartnerRates, Long> implements PartnerRatesDao {

	public  PartnerRatesDaoHibernate() {   
        super(PartnerRates.class);   
    }   
  	
	public List findMaximum() {
    	return getHibernateTemplate().find("select max(id) from PartnerRates");
    }
}
