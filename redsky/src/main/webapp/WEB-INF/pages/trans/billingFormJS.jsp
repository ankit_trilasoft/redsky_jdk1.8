<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script language="javascript" type="text/javascript">
function checkShowHideNetworkBillToCode(temp){
	<c:if test="${(!(networkAgent))}">
	var billingDMMContractTypePage = document.forms['billingForm'].elements['billingDMMContractTypeCheck'].value; 
	if(billingDMMContractTypePage=='true'){
		document.getElementById('showHideNetworkBillToCode').style.display = 'block'; 
		var billingNetworkBillToCode=document.forms['billingForm'].elements['billing.networkBillToCode'].value;
		billingNetworkBillToCode=billingNetworkBillToCode.trim();
		if(billingNetworkBillToCode=='' && temp=='changeData'){
			document.forms['billingForm'].elements['billing.networkBillToCode'].value="${customerFile.accountCode}";	
			document.forms['billingForm'].elements['billing.networkBillToName'].value="${customerFile.accountName}";
			document.forms['billingForm'].elements['formStatus'].value = '1';
		}
	}else{
		document.getElementById('showHideNetworkBillToCode').style.display = 'none';
	}
	</c:if>
	
}
function showDetailed(){
	 var noInsurance11 = document.forms['billingForm'].elements['billing.noInsurance'].value;
	
	 var noInsurance=noInsurance11.trim();
	 if(noInsurance!='' && noInsurance!='No Insurance'){
		document.getElementById('detailed12').style.display = 'block';
	 }else if(noInsurance=='No Insurance'){
		 //document.forms['billingForm'].elements['billing.insuranceHas'].value = 'N';
		 document.getElementById('detailed12').style.display = 'none';
	 }else {
		 document.getElementById('detailed12').style.display = 'none';
	 }
}
function InsuranceValue(){
	var billToCode = document.forms['billingForm'].elements['billing.billToCode'].value;
	var url="InsuranceGetValue.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
    http257.open("GET", url, true);
    http257.onreadystatechange = handleHttpResponseInsuranceValue;
    http257.send(null);	
}
function handleHttpResponseInsuranceValue(){
	 var results = http257.responseText
      results = results.trim();
	 var res = results.replace("[",'');
     res = res.replace("]",'');
	 res = res.split(",");
   var targetElement=document.forms['billingForm'].elements['billing.noInsurance'];
	     targetElement.length= res.length;
		 for(i=0; i<res.length; i++){
	     		if(res[i] == ''){
	     		}else{
	     		document.forms['billingForm'].elements['billing.noInsurance'].options[i].text =res[i].trim();
	     		document.forms['billingForm'].elements['billing.noInsurance'].options[i].value= res[i].trim();
	     		if (document.getElementById("insuranceNo").options[i].value == '${billing.noInsurance}'){
					   document.getElementById("insuranceNo").options[i].defaultSelected = true;
					}
		         }
    }
}
function opendetailedList(){
	 var noInsurance11= document.forms['billingForm'].elements['billing.noInsurance'].value;
	 var noInsurance=noInsurance11.trim();
	 if(noInsurance=='Detailed List' && noInsurance!='' && noInsurance!='No Insurance'){
		 openWindow('insuranceDetailedList.html?sid=${serviceOrder.id}&commodity=${serviceOrder.commodity}&decorator=popup&popup=true',800,450);
	 }else if(noInsurance!='Detailed List' && noInsurance!='' && noInsurance!='No Insurance'){
		 openWindow('insuranceLumpSumAmountList.html?sid=${serviceOrder.id}&commodity=${serviceOrder.commodity}&noInsuranceValue='+noInsurance11+'&decorator=popup&popup=true',500,450); 
	 }
  
   return true;
	}
 function test()
		{
			return false;
		}
 function test1()
	{
		var img = this;
		//var textElm = document.getElementById(img.getAttribute('textelementname'));
		//cal.select(textElm,img.id,document.forms['trackingStatusForm'].dateFormat.value); 
		img.onclick = false;
		return false;
	}		
		function trap1(){
			 if(document.images)
			    {
			      var totalImages = document.images.length;
			      	for (var i=0;i<totalImages;i++) {
				      	try{
							if(document.images[i].src.indexOf('images/navarrow.gif')>0) { 
								var el = document.getElementById(document.images[i].id);
								if((el.getAttribute("id")).indexOf('-trigger')>0){
									//alert(el.getAttribute("id"));
									el.removeAttribute("id");
								} } }
			      	catch(e){}
						} }  }
		function trap(){
		  if(document.images){
		      var totalImages = document.images.length;
		       	for (var i=0;i<totalImages;i++)
					{
						if(document.images[i].src.indexOf('calender.png')>0) 	{ 
							var el = document.getElementById(document.images[i].id);
							el.onclick = test;
							document.images[i].src = 'images/navarrow.gif';
						}
						if(document.images[i].src.indexOf('open-popup.gif')>0) { 
								var el = document.getElementById(document.images[i].id);
								//alert(document.images[i].id);
								if(document.images[i].id!='openpopup4.img' && document.images[i].id!='openpopup5.img' &&  document.images[i].id!='openpopup6.img'  &&  document.images[i].id!='openpopup9.img')
								{
										el.onclick = test;
										document.images[i].src = 'images/navarrow.gif'; 
								} 
						}
						if(document.images[i].src.indexOf('notes_empty1.jpg')>0) 	{
								var el = document.getElementById(document.images[i].id);
								el.onclick = test;
						        document.images[i].src = 'images/navarrow.gif';
						}
						if(document.images[i].src.indexOf('notes_open1.jpg')>0)
						{
							var el = document.getElementById(document.images[i].id);
							el.onclick = test;
							document.images[i].src = 'images/navarrow.gif';
						}  
					}
		    }
		  }
 function accordingBillToCode(){	
	 var billToContr = document.forms['billingForm'].elements['billing.billToCode'].value;
	  document.forms['billingForm'].elements['billToContract'].value = billToContr;
	 var job = document.forms['billingForm'].elements['serviceOrder.job'].value;
	     document.forms['billingForm'].elements['billJob'].value = job;
	 var create = document.forms['billingForm'].elements['billing.createdOn'].value;
	   document.forms['billingForm'].elements['billCreatedOn'].value = create;
	 var companyDivision=document.forms['billingForm'].elements['serviceOrder.companyDivision'].value;
	 var htsContract; 
	 var bookingAgentContr = document.forms['billingForm'].elements['serviceOrder.bookingAgentCode'].value;
	  document.forms['billingForm'].elements['bookingAgentContract'].value = bookingAgentContr;
	 try{
		htsContract= document.forms['billingForm'].elements['billing.historicalContracts'].checked;
	 }catch(e){
		 htsContract=false;
	}
	 var historicalContractsValue;
	 if(htsContract){
		 historicalContractsValue="true";
		 //alert("1"+historicalContractsValue);
	 }else{
		 historicalContractsValue="false";
		// alert("2"+historicalContractsValue)
		 }
     var url="getContract.html?ajax=1&decorator=simple&popup=true&billToContract="+encodeURI(billToContr)+"&billJob="+encodeURI(job)+"&billCreatedOn="+encodeURI(create)+"&custCompanyDivision="+encodeURI(companyDivision)+"&historicalContractsValue="+historicalContractsValue+"&bookingAgentContract="+encodeURI(bookingAgentContr);
//alert(url)
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse49;
     http3.send(null); 
}    
function findQuantity()
{
	document.getElementById('rateImage').src = "<c:url value='/images/loader-move.gif'/>";
    var ship = document.forms['billingForm'].elements['billing.shipNumber'].value;
    var url="maxLineNumberList.html?ajax=1&decorator=simple&popup=true&shipNumber="+encodeURI(ship);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse9;
     http2.send(null);
} 
// function to check the review status from work ticket. 
function findReviewStatus()
{	
	 var ship = document.forms['billingForm'].elements['billing.shipNumber'].value;
     var url="reviewStatusList.html?ajax=1&decorator=simple&popup=true&shipNumber="+encodeURI(ship);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse999;
     http2.send(null);
}
function handleHttpResponse999()
        { 
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');
                if(results.length >=1)
                {	
                	document.forms['billingForm'].elements['billing.billComplete'].readOnly = true;
                	var oimageDiv=document.getElementById('myimageDiv')
					oimageDiv.style.display=(oimageDiv.style.display=='none')?'none':'none'
                }
               }
        }
function findBillingCompleteA()
{
     var ship = document.forms['billingForm'].elements['billing.shipNumber'].value;
     var url="reviewStatusList.html?ajax=1&decorator=simple&popup=true&shipNumber="+encodeURI(ship);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse9999;
     http2.send(null);
}

function handleHttpResponse9999(){ 
             if (http2.readyState == 4)   {
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results.length >=1)  {	
                	alert('Billing cannot be marked complete as there are work tickets which have not been cleared. Please click the Ticket List to see open tickets and clear them first before marking the billing complete.');
		            document.forms['billingForm'].elements['billingBillCompleteVar'].value='0';
		            document.forms['billingForm'].elements['billing.billComplete'].value ='';
		            document.getElementById("billingForm").reset();
                }else{
                	findBillingCompleteAccrueAudit();
                }
               }
        } 
        
// End of function. 
 function findBillingCompleteAudit()
{
     var ship = document.forms['billingForm'].elements['billing.shipNumber'].value;
     var url="reviewStatusList.html?ajax=1&decorator=simple&popup=true&shipNumber="+encodeURI(ship);
     http555.open("GET", url, true);
     http555.onreadystatechange = handleHttpResponse9991;
     http555.send(null);
} 
function handleHttpResponse9991()
        { 
             if (http555.readyState == 4)  {
                var results = http555.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results.length >=1)
                {	
                	alert('Billing cannot be marked complete as there are work tickets which have not been cleared. Please click the Ticket List to see open tickets and clear them first before marking the billing complete.');
		            document.forms['billingForm'].elements['billingBillCompleteVar'].value='0';
		            new Calendar().hide();
		            document.forms['billingForm'].elements['billing.auditComplete'].focus();
		            document.getElementById("billingForm").reset();
                }else{
                	findBillingCompleteAccrueAudit();
                }
               }
        }

function findBillingCompleteAccrueAudit(){ 
	<configByCorp:fieldVisibility componentId="component.billing.accuralBillingComplete">
	var sid = document.forms['billingForm'].elements['sid'].value;
    var url="reviewAccrueStatusList.html?ajax=1&decorator=simple&popup=true&sid="+encodeURI(sid);
    http5555.open("GET", url, true);
    http5555.onreadystatechange = handleHttpResponseAccrue;
    http5555.send(null);
    </configByCorp:fieldVisibility>
	
}

function handleHttpResponseAccrue()
{ 
     if (http5555.readyState == 4)  {
        var results = http5555.responseText
        results = results.trim();
        results = results.replace('[','');
        results=results.replace(']',''); 
        if(results.length >=1)
        {	
        	alert('There are open accrual(s) on line # '+results);
            document.forms['billingForm'].elements['billingBillCompleteVar'].value='0';
            new Calendar().hide();
            document.forms['billingForm'].elements['billing.auditComplete'].focus();
            document.getElementById("billingForm").reset();
        } 
       }
}
        
// End of function. 
// auth updated date  
function authUpdatedDate() {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		document.forms['billingForm'].elements['billing.authUpdated'].value=datam;
	} 
// End of function  
function computeCurrentCharges(fieldId){
    var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
    var onHand = document.forms['billingForm'].elements['billing.onHand'].value;
    var cycle = document.forms['billingForm'].elements['billing.cycle'].value;
    var postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
    var payableRate = document.forms['billingForm'].elements['billing.payableRate'].value;
    var vendorStoragePerMonth= document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value;
    var storagePerMonth =document.forms['billingForm'].elements['billing.storagePerMonth'].value;
    var totalInsrVal = document.forms['billingForm'].elements['billing.totalInsrVal'].value;
    var insurancePerMonth =document.forms['billingForm'].elements['billing.insurancePerMonth'].value;
    var insuranceValueEntitle=document.forms['billingForm'].elements['billing.insuranceValueEntitle'].value;
    if(vendorStoragePerMonth == null || vendorStoragePerMonth == ''){ 
    	vendorStoragePerMonth = 0;
    }
    if(storagePerMonth == null || storagePerMonth == ''){ 
    	storagePerMonth = 0;
    }
    if(totalInsrVal == null || totalInsrVal == ''){ 
    	totalInsrVal = 0;
    }
    if(insurancePerMonth == null || insurancePerMonth == ''){ 
    	insurancePerMonth = 0;
    }
    if(insuranceValueEntitle == null || insuranceValueEntitle == ''){ 
    	insuranceValueEntitle = 0;
    }
    var inputBuildFormula="&vendorStoragePerMonth=" + encodeURI(vendorStoragePerMonth) +"&storagePerMonth=" + encodeURI(storagePerMonth)+"&totalInsrVal=" + encodeURI(totalInsrVal) + "&insurancePerMonth=" + encodeURI(insurancePerMonth) +"&insuranceValueEntitle=" + encodeURI(insuranceValueEntitle);
    if(payableRate == null || payableRate == ''){ 
		payableRate = 0;
    }
    if(postGrate !=null && postGrate !=''){ 
        postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
    }else{
         postGrate = document.forms['billingForm'].elements['billing.postGrate'].value=0*1;
    }
    if(fieldId == 'StoragePerMonth1'){
		var charge = document.forms['billingForm'].elements['billing.charge'].value;
		if(charge=='' || charge==' '){
			alert("Please select charge.");
			return false;
		}
     	var url="chargeRateList.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract) + "&onHand=" + encodeURI(onHand) + "&cycle=" + encodeURI(cycle)  + "&postGrate=" + encodeURI(postGrate)+ "&payableRate=" + encodeURI(payableRate)+inputBuildFormula ;
     	http2.open("GET", url, true);
        http2.onreadystatechange = handleHttpResponse11;
        http2.send(null);
    }else if(fieldId == 'StoragePerMonth2'){
    	//var vendorCharge = document.forms['billingForm'].elements['billing.insuranceCharge1'].value;
    	var vendorCharge = document.forms['billingForm'].elements['billing.charge'].value;
    	if(vendorCharge == ''){
    		alert('Charge Type is empty');
    		return false;
    	} 
    	var url="getVendorChargeRateList.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(vendorCharge)+"&billingContract=" + encodeURI(billingContract) + "&onHand=" + encodeURI(onHand) + "&cycle=" + encodeURI(cycle) + "&postGrate=" + encodeURI(postGrate)  + "&payableRate=" + encodeURI(payableRate)+inputBuildFormula;
    	http2.open("GET", url, true);
        http2.onreadystatechange = handleHttpResponseForVendorCharges;
        http2.send(null);
    }else if(fieldId == 'InsurancePerMonth'){
    	var insurCharge = document.forms['billingForm'].elements['billing.insuranceCharge'].value;
    	if(insurCharge == '' || insurCharge == null){
    		alert('Insurance Charge Type is empty');
    		return false;
    	}
    	var insuranceRate = document.forms['billingForm'].elements['billing.insuranceRate'].value;
        var insuranceValueActual = document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
        var baseInsuranceValue = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value;
        
    	 if(insuranceValueActual !=null && insuranceValueActual !=''){
          	insuranceValueActual = document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
         }else {
            insuranceValueActual = document.forms['billingForm'].elements['billing.insuranceValueActual'].value=0*1;
         }
         if(baseInsuranceValue !=null && baseInsuranceValue !=''){
        	 baseInsuranceValue = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value;
         }else{
        	 baseInsuranceValue = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value=0*1;
         }
         if(insuranceRate !=null && insuranceRate !=''){
          	insuranceRate = document.forms['billingForm'].elements['billing.insuranceRate'].value;
         }else{
            insuranceRate = document.forms['billingForm'].elements['billing.insuranceRate'].value=0*1;
         }
    	var url="chargeInsuranceRateList.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(insurCharge)+"&billingContract=" + encodeURI(billingContract) + "&onHand=" + encodeURI(onHand) + "&cycle=" + encodeURI(cycle) + "&insuranceValueActual=" + encodeURI(insuranceValueActual) + "&baseInsuranceValue="+ encodeURI(baseInsuranceValue) + "&insuranceRate=" + encodeURI(insuranceRate) + "&postGrate=" + encodeURI(postGrate)+ "&payableRate=" + encodeURI(payableRate)+inputBuildFormula;
    	http2.open("GET", url, true);
        http2.onreadystatechange = handleHttpResponse19;
        http2.send(null);
    }
}
function findChargeRate(){	
     var charge = document.forms['billingForm'].elements['billing.charge'].value;
     var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
     var onHand = document.forms['billingForm'].elements['billing.onHand'].value;
     var cycle = document.forms['billingForm'].elements['billing.cycle'].value;
     var postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
     
     if(postGrate !=null && postGrate !='')  { 
       postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
     }else {
        postGrate = document.forms['billingForm'].elements['billing.postGrate'].value=0*1;
     }
     
     if(charge=='' || charge==' '){
		alert("Please select charge.");
	 }else{
     	var url="chargeRateList.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract) + "&onHand=" + encodeURI(onHand) + "&cycle=" + encodeURI(cycle)  + "&postGrate=" + encodeURI(postGrate);
     }
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse11;
     http2.send(null);
}

function findInsuranceChargeRate(){	
     var charge = document.forms['billingForm'].elements['billing.insuranceCharge'].value;
     var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
     var onHand = document.forms['billingForm'].elements['billing.onHand'].value;
     var cycle = document.forms['billingForm'].elements['billing.cycle'].value;
     
     var insuranceRate = document.forms['billingForm'].elements['billing.insuranceRate'].value;
     var insuranceValueActual = document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
     var baseInsuranceValue = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value;
     var postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
     
     if(postGrate !=null && postGrate !=''){ 
       postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
     }else{
        postGrate = document.forms['billingForm'].elements['billing.postGrate'].value=0*1;
     }
     
     if(insuranceValueActual !=null && insuranceValueActual !=''){
      	insuranceValueActual = document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
     }else {
        insuranceValueActual = document.forms['billingForm'].elements['billing.insuranceValueActual'].value=0*1;
     }
     if(baseInsuranceValue !=null && baseInsuranceValue !=''){
    	 baseInsuranceValue = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value;
     }else{
    	 baseInsuranceValue = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value=0*1;
     }
     if(insuranceRate !=null && insuranceRate !=''){
      	insuranceRate = document.forms['billingForm'].elements['billing.insuranceRate'].value;
     }else{
        insuranceRate = document.forms['billingForm'].elements['billing.insuranceRate'].value=0*1;
     }
     
    if(charge=='' || charge==' '){
		alert("Please select charge.");
	}else{
     var url="chargeInsuranceRateList.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract) + "&onHand=" + encodeURI(onHand) + "&cycle=" + encodeURI(cycle) + "&insuranceValueActual=" + encodeURI(insuranceValueActual) + "&baseInsuranceValue="+ encodeURI(baseInsuranceValue) + "&insuranceRate=" + encodeURI(insuranceRate) + "&postGrate=" + encodeURI(postGrate);
     }
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse19;
     http2.send(null);
}
function findChargeRateFast()
{	
	var chargeCheck=document.forms['billingForm'].elements['chargeCodeValidationVal'].value;
	if(chargeCheck=='' || chargeCheck==null){
     var charge = document.forms['billingForm'].elements['billing.charge'].value;
     var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
     var url="chargeRateList.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse12;
     http2.send(null);
	}
}
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
String.prototype.lntrim = function() {
    return this.replace(/^\s+/,"","\n");
}

function handleHttpResponse49()
        {
 	    if (http3.readyState == 4)
             {
                var results = http3.responseText
                results = results.trim();
                 res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.split("@");
                targetElement = document.forms['billingForm'].elements['billing.contract'];
				targetElement.length = res.length;
				var tempContract='${billing.contract}';
				try{			
				tempContract=tempContract.trim();
				}catch(e){}
				for(i=0;i<res.length;i++)
					{
				
					  if(res[i] == ''){
					  document.forms['billingForm'].elements['billing.contract'].options[i].text = '';
					  document.forms['billingForm'].elements['billing.contract'].options[i].value = '';
					}
					else
					{
					  document.forms['billingForm'].elements['billing.contract'].options[i].value=res[i].trim();
					  document.forms['billingForm'].elements['billing.contract'].options[i].text=res[i].trim();
					}
             }
           document.forms['billingForm'].elements['billing.contract'].value=tempContract; 
           findDMMTypeContract();
           findDefaultSettingFromPartner();
       }
   }
function getHTTPObjectActualizationDate()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var actualizationDate = getHTTPObjectActualizationDate(); 
function findActualizationDateOfContract(){
    var contracts = document.forms['billingForm'].elements['billing.contract'].value;
    if(contracts!=null && contracts.trim()!=''){
	    var url="findActualizationDateOfContract.html?ajax=1&decorator=simple&popup=true&contracts=" + encodeURI(contracts);
	    actualizationDate.open("GET", url, true);
	    actualizationDate.onreadystatechange = handleHttpResponseActualizationDate;
	    actualizationDate.send(null);
    }else{
    	checkPriceContract();
    }
}
function handleHttpResponseActualizationDate(){
     if (actualizationDate.readyState == 4) {
        var results = actualizationDate.responseText
        results = results.trim();
        if(results.length>1){ 
	       	 if(results=='TRUE'){
	       		document.forms['billingForm'].elements['billing.fXRateOnActualizationDate'].value=true;
	       	 }else{
	       		document.forms['billingForm'].elements['billing.fXRateOnActualizationDate'].value=false;
	       	 } 
	       	checkPriceContract();
        }
     }
}
function getHTTPObjectDMMtypeContract()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var dmmTypeContractHTTP = getHTTPObjectDMMtypeContract(); 
function findDMMTypeContract(){ 
	<c:if test="${(!(networkAgent))}">
	var contracts = document.forms['billingForm'].elements['billing.contract'].value;
    if(contracts!=null && contracts.trim()!='' && contracts!='${billing.contract}'){
	    var url="findDMMTypeContract.html?ajax=1&decorator=simple&popup=true&contracts=" + encodeURI(contracts);
	    dmmTypeContractHTTP.open("GET", url, true);
	    dmmTypeContractHTTP.onreadystatechange = handleHttpResponseDmmTypeContract;
	    dmmTypeContractHTTP.send(null);
    }
    </c:if>
}
function handleHttpResponseDmmTypeContract(){
     if (dmmTypeContractHTTP.readyState == 4) {
        var results = dmmTypeContractHTTP.responseText
        results = results.trim(); 
        if(results.length>1){ 
	       	 if(results=='TRUE'){
	       		document.forms['billingForm'].elements['billingDMMContractTypeCheck'].value=true;
   			    checkShowHideNetworkBillToCode('changeData');
	       	 }else{
	       		document.forms['billingForm'].elements['billingDMMContractTypeCheck'].value=false;
	      	       checkShowHideNetworkBillToCode('changeData');
	       	 } 
        }
     }
}

function checkNetworkBillToName(){ 
	<c:if test="${(!(networkAgent))}">
	var networkBillToCode = document.forms['billingForm'].elements['billing.networkBillToCode'].value;
	networkBillToCode=networkBillToCode.trim();
    if(networkBillToCode==''){
      document.forms['billingForm'].elements['billing.networkBillToName'].value="";
    }
	if(networkBillToCode!=''){
	    var url="findNetworkBillToName.html?ajax=1&decorator=simple&popup=true&networkPartnerCode=${customerFile.accountCode}&networkBillToCode=" + encodeURI(networkBillToCode);
	    dmmTypeContractHTTP.open("GET", url, true);
	    dmmTypeContractHTTP.onreadystatechange = handleHttpResponseNetworkBillToName;
	    dmmTypeContractHTTP.send(null);
    }
    </c:if>
}
function handleHttpResponseNetworkBillToName(){
	if (dmmTypeContractHTTP.readyState == 4){
        var results = dmmTypeContractHTTP.responseText
        results = results.trim();
        var res = results.split("#"); 
        if(res.length >= 2){ 
        		if(res[2] == 'Approved'){
        			document.forms['billingForm'].elements['billing.networkBillToName'].value = res[1];
        		}else{
        			alert("Network Bill to code is not approved" ); 
        			document.forms['billingForm'].elements['billing.networkBillToName'].value=""; 
        			document.forms['billingForm'].elements['billing.networkBillToCode'].value=""; 
        		}			
        }else{
            alert("Network Bill to code not valid");    
            document.forms['billingForm'].elements['billing.networkBillToName'].value="";
            document.forms['billingForm'].elements['billing.networkBillToCode'].value=""; 
		   }  } 
}

function checkPriceContract()
{
    var contracts = document.forms['billingForm'].elements['billing.contract'].value;
    var url="priceContract.html?ajax=1&decorator=simple&popup=true&contracts=" + encodeURI(contracts);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse222333;
     http2.send(null);
}

function setDefaultvalue(target){
	var eleValue= target.value;
	if(eleValue==null || eleValue==''){
		target.value='0.00';
	}
}
<%--
function checkBillingInstructionCode()
{
	
     var contract = document.forms['billingForm'].elements['billing.contract'].value;
     var billingInstructionCodeWithDesc = document.forms['billingForm'].elements['billing.billingInstructionCodeWithDesc'].value;
     var billingInstruction = document.forms['billingForm'].elements['billing.billingInstruction'].value;
     if(billingInstructionCodeWithDesc =='' && billingInstruction ==''){
     var url="billingInstuctionCode.html?decorator=simple&popup=true&contract=" + encodeURI(contract);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponseCheckBillingInstructionCode;
     http9.send(null);
     }
}
--%>

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
<%--
function handleHttpResponseCheckBillingInstructionCode()
        {

             if (http9.readyState == 4)
             {
                var results = http9.responseText
                results = results.trim();
                res = results.replace("[",'');
                res = res.replace("]",'');
                res1 = res.split(":");
                if(res1[1] != undefined){
                document.forms['billingForm'].elements['billing.billingInstructionCodeWithDesc'].value=res;
			    //document.forms['billingForm'].elements['billing.billingInstructionCodeWithDesc'].options[0].text=res1[1].trim();
			    document.forms['billingForm'].elements['billing.billingInstruction'].value=res1[1].trim();
			    document.forms['billingForm'].elements['billing.specialInstruction'].value=res1[1].trim();
			    }else{
			    document.forms['billingForm'].elements['billing.billingInstructionCodeWithDesc'].value='';
			    //document.forms['billingForm'].elements['billing.billingInstructionCodeWithDesc'].options[0].text=res1[1].trim();
			    document.forms['billingForm'].elements['billing.billingInstruction'].value='';
			    document.forms['billingForm'].elements['billing.specialInstruction'].value='';
			    }
             }
        }
--%>


function handleHttpResponse222333(){
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#");                               
                if(res.length>=1)
                {
                	 var billPayMethod = document.getElementById('billPayMethod').selectedIndex;
                	 var partnerCode = document.forms['billingForm'].elements['billing.billToCode'].value;
                     if(billPayMethod == 0){
                 document.forms['billingForm'].elements['billing.billTo1Point'].value= res[5];
                     }else if(billPayMethod != 0 && (partnerCode==null || partnerCode=='')){
    			    	 document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value= res[5]; 
    			     }
                 document.forms['billingForm'].elements['billing.billingInstructionCodeWithDesc'].value= res[6];
                 if(res[6] !=undefined && res[6]!=''){
                 var resValue= res[6].split(":");
                 document.forms['billingForm'].elements['billing.billingInstruction'].value= resValue[1];                 
			     document.forms['billingForm'].elements['billing.specialInstruction'].value=resValue[1];
                 }else{
                 document.forms['billingForm'].elements['billing.billingInstruction'].value='';
                 document.forms['billingForm'].elements['billing.billingInstructionCodeWithDesc'].value='';
                 document.forms['billingForm'].elements['billing.specialInstruction'].value='';
                 }
                 if(document.forms['billingForm'].elements['billing.transportationDiscount'].value=='' || document.forms['billingForm'].elements['billing.transportationDiscount'].value=='0.00' || document.forms['billingForm'].elements['billing.transportationDiscount'].value=='0')
                 {
                 	document.forms['billingForm'].elements['billing.transportationDiscount'].value= res[0]; 
                 }
                 
                 if(document.forms['billingForm'].elements['billing.discount'].value=='' || document.forms['billingForm'].elements['billing.discount'].value=='0.00' || document.forms['billingForm'].elements['billing.discount'].value=='0')
                 {
                 	document.forms['billingForm'].elements['billing.discount'].value= res[1]; 
                 }
                 
                 if(document.forms['billingForm'].elements['billing.packDiscount'].value=='' || document.forms['billingForm'].elements['billing.packDiscount'].value=='0.00' || document.forms['billingForm'].elements['billing.packDiscount'].value=='0')
                 {
                 	document.forms['billingForm'].elements['billing.packDiscount'].value= res[2]; 
                 }
                 
                 if(document.forms['billingForm'].elements['billing.sitDiscount'].value=='' || document.forms['billingForm'].elements['billing.sitDiscount'].value=='0.00' || document.forms['billingForm'].elements['billing.sitDiscount'].value=='0')
                 {
                 	document.forms['billingForm'].elements['billing.sitDiscount'].value= res[3]; 
                 }
                 
                 if(document.forms['billingForm'].elements['billing.otherDiscount'].value=='' || document.forms['billingForm'].elements['billing.otherDiscount'].value=='0.00' || document.forms['billingForm'].elements['billing.otherDiscount'].value=='0')
                 {
                 	document.forms['billingForm'].elements['billing.otherDiscount'].value= res[4];       
                 }                 
			     }
             }
        }
        
function handleHttpResponse9(){
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');
                if(results.length>=1)
                {
 					 if (results == 'null')
 					 {
 					   alert("No work ticket available/Storage section in the work ticket is not setup")
                       document.forms['billingForm'].elements['billing.onHand'].value = "";
 					 }
 					 else
 					 {
 					    var newRes="";
 					    newRes=Math.round(results*100)/100;
 					    document.forms['billingForm'].elements['billing.onHand'].value=newRes;
 					 }
                }
                 else
                 { 
                    document.forms['billingForm'].elements['billing.onHand'].value = "";
                 }
                 document.getElementById('rateImage').src = "<c:url value='/images/loader-move.gif'/>";
             }
        }
 function handleHttpResponse12()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#");
                if(results.length>=1)
                {
                if(res[1]=='Preset') 
                {              
                var postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
					if(postGrate == '0' || postGrate == '0.0' || postGrate == ''){ 
                  		document.forms['billingForm'].elements['billing.postGrate'].value= res[0];
                  	}	
                } 
               }
               } 
        }
      function handleHttpResponse19(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#");
                var quantityOnHand=document.forms['billingForm'].elements['billing.onHand'].value;
		        var billingCycle=document.forms['billingForm'].elements['billing.cycle'].value;
		        document.forms['billingForm'].elements['billing.onHand'].value=(Math.round(quantityOnHand*100)/100).toFixed(2)
                if(results.length>=1){
                if(res[1]=='BuildFormula'){
                    if(res[3]=='Multiply'){
                        if(billingCycle!=0){
            	          	document.forms['billingForm'].elements['billing.insurancePerMonth'].value = (Math.round(((res[2]*res[4])/billingCycle)*100)/100).toFixed(2);
            	        }else{
            	           	document.forms['billingForm'].elements['billing.insurancePerMonth'].value=(0*1).toFixed(2);
            	        }
                     }else if(res[3]=='Division'){
                         if(billingCycle!=0){
            	          	document.forms['billingForm'].elements['billing.insurancePerMonth'].value = (Math.round((((res[2])/(res[4]))/billingCycle)*100)/100).toFixed(2);
            	          }else{
            	             document.forms['billingForm'].elements['billing.insurancePerMonth'].value=(0*1).toFixed(2);
            	          }
                      }else{
                    	  	if(billingCycle!=0){
		                	  document.forms['billingForm'].elements['billing.insurancePerMonth'].value = (Math.round((res[2]/billingCycle)*100)/100).toFixed(2);
		               		}else{
		               			document.forms['billingForm'].elements['billing.insurancePerMonth'].value=(0*1).toFixed(2);
		                	 }
	                  }
               }
           } 
        }  
       } 
      function handleHttpResponseForVendorCharges(){
          if (http2.readyState == 4){
             var results = http2.responseText
             results = results.trim();  
             var res = results.split("#");
                 if(results.length>=1){
	                if(res[1]=='Preset'){               
		                document.forms['billingForm'].elements['billing.payableRate'].value= res[0];
		            }
		                document.forms['billingForm'].elements['billing.payableRate'].value;
		                var rateCharge=document.forms['billingForm'].elements['billing.payableRate'].value;
		                var quantityOnHand=document.forms['billingForm'].elements['billing.onHand'].value;
		                document.forms['billingForm'].elements['billing.onHand'].value=(Math.round(quantityOnHand*100)/100).toFixed(2);
		                document.forms['billingForm'].elements['billing.payableRate'].value=(Math.round(rateCharge*100000)/100000).toFixed(5);
		                rateCharge=(Math.round(rateCharge*100000)/100000).toFixed(5);
		                var billingCycle=document.forms['billingForm'].elements['billing.cycle'].value;
		                var storagePerMonth=rateCharge*quantityOnHand*1;
		                var storagePerMonthRound="";
		                storagePerMonthRound=Math.round(storagePerMonth*100)/100;
		                document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value=storagePerMonthRound.toFixed(2);
	                
	                    if(res[1]=='BuildFormula'){
	                          if(res[3]=='Multiply'){
	                             if(billingCycle!=0){
	                	          	document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value = (Math.round(((res[2]*res[4])/billingCycle)*100)/100).toFixed(2);
	                	          }else{
	                	             document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value=(0*1).toFixed(2);
	                	          }
	                          }else if(res[3]=='Division'){
	                             if(billingCycle!=0){
	                	          	document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value = (Math.round((((res[2])/(res[4]))/billingCycle)*100)/100).toFixed(2);
	                	         }else{
	                	             document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value=(0*1).toFixed(2);
	                	          }
	                            }else{
		                           if(billingCycle!=0){
		                	          	document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value = (Math.round((res[2]/billingCycle)*100)/100).toFixed(2);
		                	       }else{
		                	             document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value=(0*1).toFixed(2);
		                	       }
	                            }
	                     }
				}
	        }
      }
        function handleHttpResponse11(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();  
                var res = results.split("#");
                    if(results.length>=1){
	                if(res[1]=='Preset'){               
		                document.forms['billingForm'].elements['billing.postGrate'].value= res[0];
		            }
		                document.forms['billingForm'].elements['billing.postGrate'].value;
		                var rateCharge=document.forms['billingForm'].elements['billing.postGrate'].value;
		                var quantityOnHand=document.forms['billingForm'].elements['billing.onHand'].value;
		                document.forms['billingForm'].elements['billing.onHand'].value=(Math.round(quantityOnHand*100)/100).toFixed(2);
		                document.forms['billingForm'].elements['billing.postGrate'].value=(Math.round(rateCharge*100000)/100000).toFixed(5);
		                rateCharge=(Math.round(rateCharge*100000)/100000).toFixed(5);
		                var billingCycle=document.forms['billingForm'].elements['billing.cycle'].value;
		                var storagePerMonth=rateCharge*quantityOnHand*1;
		                var storagePerMonthRound="";
		                storagePerMonthRound=Math.round(storagePerMonth*100)/100;
		                document.forms['billingForm'].elements['billing.storagePerMonth'].value=storagePerMonthRound.toFixed(2);
	                    if(res[1]=='BuildFormula'){
	                          if(res[3]=='Multiply'){
	                             if(billingCycle!=0){
	                	          	document.forms['billingForm'].elements['billing.storagePerMonth'].value = (Math.round(((res[2]*res[4])/billingCycle)*100)/100).toFixed(2);
	                	          }else{
	                	             document.forms['billingForm'].elements['billing.storagePerMonth'].value=(0*1).toFixed(2);
	                	          }
	                          }else if(res[3]=='Division'){
	                             if(billingCycle!=0){
	                	          	document.forms['billingForm'].elements['billing.storagePerMonth'].value = (Math.round((((res[2])/(res[4]))/billingCycle)*100)/100).toFixed(2);
	                	         }else{
	                	             document.forms['billingForm'].elements['billing.storagePerMonth'].value=(0*1).toFixed(2);
	                	          }
	                            }else{
		                           if(billingCycle!=0){
		                	          	document.forms['billingForm'].elements['billing.storagePerMonth'].value = (Math.round((res[2]/billingCycle)*100)/100).toFixed(2);
		                	       }else{
		                	             document.forms['billingForm'].elements['billing.storagePerMonth'].value=(0*1).toFixed(2);
		                	       }
	                            }
	                     }
				}
	        }
        }
        
function autoPopulate_billingForm_insuranceOption(targetElement) {
		var insuranceOptionCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['billingForm'].elements['billing.insuranceOptionCode'].value=insuranceOptionCode.substring(0,insuranceOptionCode.indexOf(":")-1);
		targetElement.form.elements['billing.insuranceOption'].value=insuranceOptionCode.substring(insuranceOptionCode.indexOf(":")+2,insuranceOptionCode.length);
	 }
function autoPopulate_billingInstruction(targetElement) {
		var billingInstruction=targetElement.options[targetElement.selectedIndex].value;
		targetElement.form.elements['billing.billingInstruction'].value=billingInstruction.substring(billingInstruction.indexOf(":")+2,billingInstruction.length);
	}
	
function autoLinkCreditCard() {
		var creditCardType=document.forms['billingForm'].elements['billing.billTo1Point'].value;
		var creditCardTypeSec=document.forms['billingForm'].elements['billing.billTo2Point'].value;
		var creditCardTypePr=document.forms['billingForm'].elements['billing.payMethod'].value
		var creditCard;
		creditCard=0;
		if(creditCardType=='AE' || creditCardType=='MC' || creditCardType=='VC' || creditCardType=='DD' || creditCardTypePr=='AE' || creditCardTypePr=='MC' || creditCardTypePr=='VC' || creditCardTypePr=='DD' || creditCardTypeSec=='AE' || creditCardTypeSec=='MC' || creditCardTypeSec=='VC' || creditCardTypeSec=='DD')
		{
			creditCard=1;
		}
		
   if(creditCard==1 && document.forms['billingForm'].elements['billing.isCreditCard'].value!='1')
  	{
  		document.forms['billingForm'].elements['billing.isCreditCard'].value='1';
  	}
  	else if(document.forms['billingForm'].elements['billing.isCreditCard'].value=='1' && creditCard!=1)
  	{
  		document.forms['billingForm'].elements['billing.isCreditCard'].value='0';
  	}
  }
function assignDateApproved()
	{
		var d = new Date();
		var tempdate;
		tempdate=d.getMonth();
		tempdate=Number(tempdate)+1;
		tempdate=tempdate+"/"+d.getDate();
		tempdate=tempdate+"/"+d.getFullYear();
		document.forms['billingForm'].elements['billing.dateApproved'].value=tempdate;
	}
 function checkData()
  {
	 var f = document.getElementById('billingForm');
   		 f.setAttribute("autocomplete", "off");
	
	var imfCode;
	imfCode=0;
	if(document.forms['billingForm'].elements['billing.billToCode'].value=='A21629' || document.forms['billingForm'].elements['billing.billToCode'].value=='A18942' || document.forms['billingForm'].elements['billing.billToCode'].value=='A21109' || document.forms['billingForm'].elements['billing.billToCode'].value=='500132' || document.forms['billingForm'].elements['billing.billToCode'].value=='500130' || document.forms['billingForm'].elements['billing.billToCode'].value=='500139')
	{
		imfCode=1;
	}
	var MF="<%=request.getParameter("matchField")%>";
  	if(imfCode==1 && document.forms['billingForm'].elements['billing.imfJob'].value!='1')
  	{
  		document.forms['billingForm'].elements['billing.imfJob'].value='1';
  	}
  	else if(document.forms['billingForm'].elements['billing.imfJob'].value=='1' && imfCode!=1)
  	{
  		document.forms['billingForm'].elements['billing.imfJob'].value='0';
  	}
  }
  
  function assignBillToCode(){ 
	    //window.openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.billToName&fld_code=customerFile.billToCode');
	    //window.openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billToName&fld_code=billing.billToCode');
		javascript:openWindow("partnersPopup.html?partnerType=AC&flag=0&firstName=${customerFile.firstName}&lastName=${customerFile.lastName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billToName&fld_code=billing.billToCode&fld_seventhDescription=billing.billingEmail");
		////document.forms['billingForm'].elements['billing.billToCode'].select();
		
	}
  function setBillToCode(){ 
	    //window.openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.billToName&fld_code=customerFile.billToCode');
	    window.openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billToName&fld_code=billing.billToCode');
		//javascript:openWindow("partnersPopup.html?partnerType=AC&flag=0&firstName=${customerFile.firstName}&lastName=${customerFile.lastName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billToName&fld_code=billing.billToCode&fld_seventhDescription=billing.billingEmail");
		////document.forms['billingForm'].elements['billing.billToCode'].select();
	}
  function setBillToCode1(){ 
		<c:choose>
		<c:when test="${hasDataSecuritySet}">
		window.openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billName&fld_code=billing.billingId');
		</c:when>
		<c:otherwise>
		javascript:openWindow("partnersPopup.html?partnerType=AC&flag=0&firstName=${customerFile.firstName}&lastName=${customerFile.lastName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billName&fld_code=billing.billingId&fld_seventhDescription=billing.billingEmail");
		</c:otherwise>
		</c:choose>
	}
function assignSecBillToCode()
{
	javascript:openWindow("partnersPopup.html?partnerType=AC&flag=1&firstName=${customerFile.firstName}&lastName=${customerFile.lastName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billTo2Name&fld_code=billing.billTo2Code");
}
function assignPrivateBillToCode(){
javascript:openWindow("partnersPopup.html?partnerType=AC&flag=2&firstName=${customerFile.firstName}&lastName=${customerFile.lastName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.privatePartyBillingName&fld_code=billing.privatePartyBillingCode");
}
function check(){}

function validate_email(field)
{
with (field)
{
apos=value.indexOf("@")
dotpos=value.lastIndexOf(".")
if (apos<1||dotpos-apos<2) 
  {alert("Not a valid e-mail address!");return false}
else {return true}
}
}
 		  
function checkHasValuation(){
	try{
		var chargeType = document.forms['billingForm'].elements['billing.charge'].value;
		var hasValuation = document.forms['billingForm'].elements['billing.insuranceHas'].value;
		if(chargeType != "" && hasValuation == ""){
			alert("Please confirm the data for Has Valuation Field");
			return false;
		}
	}catch(e){}
	}
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==67) || (keyCode==86) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==110) || (keyCode==67) || (keyCode==86); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
	function onlyAlphaNumericWithDot(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110); 
	}
	
		
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        return false;
	        }
	    }
	    return true;
	}

var digits = "0123456789";
var phoneNumberDelimiters = "()- ";
var validWorldPhoneChars = phoneNumberDelimiters + "+";
var minDigitsInIPhoneNumber = 10;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    return true;
}

function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function notExists(){
	alert("The Billing  is not saved yet");
}
/**
 * method makeMandIssueDt  added Regarding #9594
 */
function makeMandIssueDt(){	
	<configByCorp:fieldVisibility componentId="component.billing.issueDate.mandatory">
	var hasVal=document.forms['billingForm'].elements['billing.insuranceHas'].value;
	var insuranceValue=document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
	var certnum=document.forms['billingForm'].elements['billing.certnum'].value;
	if((hasVal=='Y') && (insuranceValue!=null && insuranceValue!='' && insuranceValue!='0.00') && (certnum!=null && certnum!='')){
		document.getElementById("isuDtDiv1").style.display = "none";
		document.getElementById("isuDtDiv2").style.display = "block";
	}else{
		document.getElementById("isuDtDiv1").style.display = "block";
		document.getElementById("isuDtDiv2").style.display = "none";
	}
	</configByCorp:fieldVisibility>
}
/**
 * method resetIssueDt  added Regarding #9594
 */
function resetIssueDt(){	
	<configByCorp:fieldVisibility componentId="component.billing.issueDate.mandatory">
	var hasVal='${billing.insuranceHas}';	
	var insuranceValue='${billing.insuranceValueActual}';
	var certnum='${billing.certnum}';
	if((hasVal=='Y') && (insuranceValue!=null && insuranceValue!='' && insuranceValue!='0.00') && (certnum!=null && certnum!='')){
		document.getElementById("isuDtDiv1").style.display = "none";
		document.getElementById("isuDtDiv2").style.display = "block";
	}else{
		document.getElementById("isuDtDiv1").style.display = "block";
		document.getElementById("isuDtDiv2").style.display = "none";
	}
	</configByCorp:fieldVisibility>
}

function displayField(targetElement){
	try{
		var additionalNoVal = 'N';
				try{
				additionalNoVal = document.forms['billingForm'].elements['additionalNoFlag'].value;
				}catch(e){}
	if(targetElement.value == 'N'){
	    document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].selectedIndex =0;
		document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].disabled = true;
		document.getElementById("disDiv").style.display = "block"; 
		if(document.getElementById("displayDiv") != null){
			document.getElementById("displayDiv").style.display = "none";
			document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
		}
		if(document.getElementById("displayDivAdditionalNo") != null){
		document.getElementById("displayDivAdditionalNo").style.display = "none";
		}
		if(additionalNoVal=='Y'){
			document.getElementById("disDiv1").style.display = "block";
			document.getElementById("disDiv2").style.display = "block";
		}else{
			if(targetElement.value == 'Y'){
				document.getElementById("disDiv1").style.display = "block";
				document.getElementById("disDiv2").style.display = "block";
			}else if(targetElement.value == 'V'){
					document.getElementById("disDiv1").style.display = "block";
					document.getElementById("disDiv2").style.display = "block";
			}else{
				document.getElementById("disDiv1").style.display = "none";
				document.getElementById("disDiv2").style.display = "none";
			}
		}
	}else{
	    document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].disabled = false;
		document.getElementById("disDiv").style.display = "none";
		if(targetElement.value == 'Y'){
			document.getElementById("disDiv1").style.display = "block";
			document.getElementById("disDiv2").style.display = "block";
			try{
			    checkVendorName();
			    }catch(e){}
			document.forms['billingForm'].elements['billing.vendorCode'].value="${billing.vendorCode}";
			document.forms['billingForm'].elements['billing.vendorName'].value="${billing.vendorName}";
			//document.forms['billingForm'].elements['billing.estMoveCost'].value="${billing.estMoveCost}";
		}else if(targetElement.value == 'V'){
			document.getElementById("disDiv1").style.display = "block";
			document.getElementById("disDiv2").style.display = "block";
			document.forms['billingForm'].elements['billing.vendorCode'].value="${billing.vendorCode}";
			document.forms['billingForm'].elements['billing.vendorName'].value="${billing.vendorName}";
     		if(document.getElementById("displayDiv") != null){
				document.getElementById("displayDiv").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
			}	
       		if(document.getElementById("displayDivAdditionalNo") != null){
				document.getElementById("displayDivAdditionalNo").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
			}	
		}else if(targetElement.value == '' ){
			document.forms['billingForm'].elements['billing.vendorCode'].value="";
			//document.forms['billingForm'].elements['billing.vendorName'].value="";
			document.getElementById("disDiv1").style.display = "none";
			document.getElementById("disDiv2").style.display = "none";	
			if(document.getElementById("displayDiv") != null){
				document.getElementById("displayDiv").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
			}
			if(document.getElementById("displayDivAdditionalNo") != null){
				document.getElementById("displayDivAdditionalNo").style.display = "none";
				}			
		}else{
			if(additionalNoVal=='N'){
			document.forms['billingForm'].elements['billing.vendorCode'].value="";
			//document.forms['billingForm'].elements['billing.vendorName'].value="";
			document.getElementById("disDiv1").style.display = "none";
			document.getElementById("disDiv2").style.display = "none";
			if(document.getElementById("displayDiv") != null){
				document.getElementById("displayDiv").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
			}
			if(document.getElementById("displayDivAdditionalNo") != null){
				document.getElementById("displayDivAdditionalNo").style.display = "none";
				}			
		}else{
			document.getElementById("disDiv1").style.display = "block";
			document.getElementById("disDiv2").style.display = "block";
			if(document.getElementById("displayDivAdditionalNo") != null){
				document.getElementById("displayDivAdditionalNo").style.display = "none";
				}
			}
		}
	}
	}catch(e){}
}
function checkdate(clickType){
if(document.forms['billingForm'].elements['billing.billCompleteA'] !=undefined){
	if((document.forms['billingForm'].elements['billing.billCompleteA'].value == 'T' || document.forms['billingForm'].elements['billing.billCompleteA'].value == 'A') && document.forms['billingForm'].elements['billing.billComplete'].value == ''){
		 document.forms['billingForm'].elements['billing.billComplete'].select();
		return false;
	}
	}
	}

function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
	document.getElementById(autocompleteDivId).style.display = "none";
	if(document.getElementById(paertnerCodeId).value==''){
		document.getElementById(partnerNameId).value="";	
	}
	}

	function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
		lastName=lastName.replace("~","'");
		document.getElementById(partnerNameId).value=lastName;
		document.getElementById(paertnerCodeId).value=partnercode;
		document.getElementById(autocompleteDivId).style.display = "none";	
		if(paertnerCodeId=='billingBillToCodeId'){
			getBillToCode();
		}else if(paertnerCodeId=='billingBillTo2CodeId'){
			getBillTo2Code();
		}else if(paertnerCodeId=='billingBrivatePartyBillingCodeId'){
			getBillTo3Code();
		}else if(paertnerCodeId=='billingVendorCode1Id'){
			checkVendorName1();
		}else if(paertnerCodeId=='billingLocationAgentCodeId'){
			checkAgentCode();
		}else if(paertnerCodeId=='billingVendorCodeId'){
			checkVendorName();
			findPayableCurrencyCodeAjax();
		}
	}
// function to populate billingComplete Date.

function populateBillingCompleteDate(){

	    var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		if(document.forms['billingForm'].elements['billing.billCompleteA'].value!='')
		{
		  document.forms['billingForm'].elements['billing.billComplete'].value=datam;
	     }
	    else
	    {
	     document.forms['billingForm'].elements['billing.billComplete'].value='';
	    } 
	}
	
// End of function

function autoSaveFunc(clickType){
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
enableCheckBox();
progressBarAutoSave('1');
if ('${autoSavePrompt}' == 'No'){
	var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
      		var id1 =document.forms['billingForm'].elements['serviceOrder.id'].value;
      		
           if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.serviceorder')
           {
             noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.billing')
           {
             noSaveAction = 'editBilling.html?id='+id1;
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.accounting')
           {
              noSaveAction = 'accountLineList.html?sid='+id1;
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.newAccounting')
          {
             noSaveAction = 'pricingList.html?sid='+id1;
          }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.forwarding')
           {
        	  <c:if test="${forwardingTabVal!='Y'}">
	  			noSaveAction = 'containers.html?id='+id1;
	  		</c:if>
		  	<c:if test="${forwardingTabVal=='Y'}">
		  		noSaveAction = 'containersAjaxList.html?id='+id1;
		  	</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.domestic')
           {
             noSaveAction = 'editMiscellaneous.html?id='+id1;
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.status')
           {
                <c:if test="${serviceOrder.job =='RLO'}">
                	noSaveAction = 'editDspDetails.html?id='+id1; 
				</c:if>
                <c:if test="${serviceOrder.job !='RLO'}">
                	noSaveAction =  'editTrackingStatus.html?id='+id1; 
				</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.ticket')
           {
             noSaveAction = 'customerWorkTickets.html?id='+id1;
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.OI')
          {
            noSaveAction = 'operationResource.html?id='+id1;
          }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.claims')
           {
             noSaveAction = 'claims.html?id='+id1;
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.customerfile')
           {
        	  var cidVal='${customerFile.id}';
             noSaveAction ='editCustomerFile.html?id='+cidVal;
           }
           if(document.forms['billingForm'].elements['billing.isCreditCard'].value=='1'){
				if(document.forms['billingForm'].elements['gotoPageString'].value == 'gototab.creditcard'){
					noSaveAction = 'creditCards.html?id='+id1;
					}
           }
           if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.criticaldate')
           {
             noSaveAction ='soAdditionalDateDetails.html?sid='+id1;
           }           
          processAutoSave(document.forms['billingForm'], 'saveBilling!saveOnTabChange.html', noSaveAction);
	}
	else{
	
   if(!(clickType == 'save'))
   {
     var id1 =document.forms['billingForm'].elements['serviceOrder.id'].value;
     if (document.forms['billingForm'].elements['formStatus'].value == '1')
     {
       var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='billingDetail.heading'/>");
       if(agree)
        {
           document.forms['billingForm'].action ='saveBilling!saveOnTabChange.html';
           document.forms['billingForm'].submit();
        }
       else
        {
         if(id1 != '')
          {
           if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.serviceorder')
           {
             location.href = 'editServiceOrderUpdate.html?id='+id1;
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.billing')
           {
             location.href = 'editBilling.html?id='+id1;
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.OI')
          {
            noSaveAction = 'operationResource.html?id='+id1;
          }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.accounting')
           {
              location.href = 'accountLineList.html?sid='+id1;
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.newAccounting')
          {
             location.href = 'pricingList.html?sid='+id1;
          }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.forwarding')
           {
        	  <c:if test="${forwardingTabVal!='Y'}">
				 	location.href = 'containers.html?id='+id1;
	  		</c:if>
		  	<c:if test="${forwardingTabVal=='Y'}">
		  			location.href = 'containersAjaxList.html?id='+id1;
		  	</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.domestic')
           {
             location.href = 'editMiscellaneous.html?id='+id1;
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.status')
           {
              <c:if test="${serviceOrder.job =='RLO'}">
              location.href = 'editDspDetails.html?id='+id1; 
			</c:if>
          <c:if test="${serviceOrder.job !='RLO'}">
          location.href =  'editTrackingStatus.html?id='+id1; 
			</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.ticket')
           {
             location.href = 'customerWorkTickets.html?id='+id1;
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.claims')
           {
             location.href = 'claims.html?id='+id1;
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.customerfile')
           {
        	  var cidVal='${customerFile.id}';
             location.href ='editCustomerFile.html?id='+cidVal;
           }
           if(document.forms['billingForm'].elements['billing.isCreditCard'].value=='1'){
				if(document.forms['billingForm'].elements['gotoPageString'].value == 'gototab.creditcard'){
					location.href = 'creditCards.html?id='+id1;
					}
           }
           if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.criticaldate')
           {
        	   location.href ='soAdditionalDateDetails.html?sid='+id1;
           }
           if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.OI')
           {
             noSaveAction = 'operationResource.html?id='+id1;
           }
         }
       }
    }
   else
   {
     if(id1 != '')
      {
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.serviceorder')
        {
          location.href = 'editServiceOrderUpdate.html?id='+id1;
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.billing')
        {
          location.href = 'editBilling.html?id='+id1;
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.accounting')
        {
          location.href = 'accountLineList.html?sid='+id1;
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.newAccounting')
       {
         location.href = 'pricingList.html?sid='+id1;
       }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.OI')
       {
         noSaveAction = 'operationResource.html?id='+id1;
       }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.forwarding')
        {
    	   <c:if test="${forwardingTabVal!='Y'}">
		 		location.href = 'containers.html?id='+id1;
 			</c:if>
	  		<c:if test="${forwardingTabVal=='Y'}">
	  			location.href = 'containersAjaxList.html?id='+id1;
	  		</c:if>
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.domestic')
        {
           location.href = 'editMiscellaneous.html?id='+id1;
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.status')
        {
           <c:if test="${serviceOrder.job =='RLO'}">
              location.href = 'editDspDetails.html?id='+id1; 
			</c:if>
           <c:if test="${serviceOrder.job !='RLO'}">
              location.href =  'editTrackingStatus.html?id='+id1; 
			</c:if>
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.ticket')
        {
           location.href = 'customerWorkTickets.html?id='+id1;
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.claims')
        {
           location.href = 'claims.html?id='+id1;
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.customerfile')
        {
    	   var cidVal='${customerFile.id}';
          location.href ='editCustomerFile.html?id='+cidVal;
        }
        if(document.forms['billingForm'].elements['billing.isCreditCard'].value=='1'){
				if(document.forms['billingForm'].elements['gotoPageString'].value == 'gototab.creditcard'){
					location.href = 'creditCards.html?id='+id1;
					}
          }
        if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.criticaldate')
        {
     	   location.href ='soAdditionalDateDetails.html?sid='+id1;
        }
        if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.OI')
        {
          noSaveAction = 'operationResource.html?id='+id1;
        }
      }
   }
  }
  }
 }

function changeStatus(){
    document.forms['billingForm'].elements['formStatus'].value = '1';
}

function findBillToName(){ 
    var billToCode = document.forms['billingForm'].elements['billing.billToCode'].value;
    billToCode=billToCode.trim();
    if(billToCode==''){
      document.forms['billingForm'].elements['billing.billToName'].value="";
    }
    if(billToCode!=''){
    	//var url="vendorName.html?decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
     	var url="findBillToCodeCheckCredit.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
     	http2.open("GET", url, true);
     	http2.onreadystatechange = function(){ handleHttpResponse2(billToCode);};
     	http2.send(null);
    }
}
function findBillTo2Name() { 
    var billTo2Code = document.forms['billingForm'].elements['billing.billTo2Code'].value;
    billTo2Code=billTo2Code.trim();
    if(billTo2Code=='') { 
      	document.forms['billingForm'].elements['billing.billTo2Name'].value="";
    }
    if(billTo2Code!=''){
    	//var url="vendorName.html?decorator=simple&popup=true&partnerCode=" +  encodeURI(billTo2Code);
    	var url="findBillToCodeCheckCredit.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billTo2Code);
     	http2.open("GET", url, true);
     	http2.onreadystatechange = function(){ handleHttpResponse3(billTo2Code);};
     	http2.send(null);
	}
}

function checkVendorName(){
    var vendorId = document.forms['billingForm'].elements['billing.vendorCode'].value;
    if(vendorId!=''){
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + 
	encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse5;
     http2.send(null);
     } else {
     //document.forms['billingForm'].elements['billing.vendorName'].value="";
     
     if(document.getElementById("displayDiv") != null){
			document.getElementById("displayDiv").style.display = "none";
			document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
		}
     if(document.getElementById("displayDivAdditionalNo") != null){
			document.getElementById("displayDivAdditionalNo").style.display = "none";
			//document.forms['billingForm'].elements['billing.additionalNo'].value='';
		}		
     }
}
function vendorCodeForActgCode()
   {
   //alert("ashish");
    var vendorId = document.forms['billingForm'].elements['billing.vendorCode'].value;
    if(vendorId!='')
    {
    var url="vendorNameForActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse222;
     http2.send(null);
    }
} 

function handleHttpResponse222(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length >= 2){ 
		                if(res[3]=='Approved'){
		                	if(res[2] != "null" && res[2] !=''){
		                		document.forms['billingForm'].elements['billing.vendorName'].value = res[1];
                 				document.forms['billingForm'].elements['billing.actgCode'].value=res[2]; 
				            }
				            else{
				                alert("Vendor Approved but missing Accounting Code" ); 
								document.forms['billingForm'].elements['billing.vendorName'].value="";
								document.forms['billingForm'].elements['billing.vendorCode'].value="";
								document.forms['billingForm'].elements['billing.actgCode'].value="";
								document.forms['billingForm'].elements['billing.vendorCode'].select();
				            }	
				        }else{
				            alert("Vendor code is not approved" ); 
							document.forms['billingForm'].elements['billing.vendorName'].value="";
							document.forms['billingForm'].elements['billing.vendorCode'].value="";
							document.forms['billingForm'].elements['billing.actgCode'].value="";
							document.forms['billingForm'].elements['billing.vendorCode'].select();
				        }     
		        }else{
                 alert("Vendor code not valid" ); 
				 document.forms['billingForm'].elements['billing.vendorName'].value="";
				 document.forms['billingForm'].elements['billing.vendorCode'].value="";
				 document.forms['billingForm'].elements['billing.actgCode'].value="";
				 document.forms['billingForm'].elements['billing.vendorCode'].select();
			   }
      }
}


function findBillingName(){
    var billingCode = document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value;
    billingCode = billingCode.trim();
    if(billingCode==''){
    	document.forms['billingForm'].elements['billing.privatePartyBillingName'].value="";
    }
    if(billingCode!=''){
    	//var url="vendorName.html?decorator=simple&popup=true&partnerCode=" + encodeURI(billingCode);
    	var url="findBillToCodeCheckCredit.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billingCode);
     	http2.open("GET", url, true);
     	http2.onreadystatechange = function(){ handleHttpResponse4(billingCode);};
     	http2.send(null);
    }
}
function findBillingIdName(){
	<configByCorp:fieldVisibility componentId="component.field.Alternative.billName">
    var billingCode = document.forms['billingForm'].elements['billing.billingId'].value;
    billingCode = billingCode.trim();
    if(billingCode!=''){
    	var url="findBillingIdNameAjax.html?ajax=1&decorator=simple&popup=true&billToCode=" + encodeURI(billingCode);
     	http21984.open("GET", url, true);
     	http21984.onreadystatechange = function(){ handleHttpResponse41984(billingCode);};
     	http21984.send(null);
    }else{
		 document.forms['billingForm'].elements['billing.billingId'].value="";
		 document.forms['billingForm'].elements['billing.billName'].value="";
		 document.forms['billingForm'].elements['billing.billingId'].focus();    
    }
    </configByCorp:fieldVisibility>
}
function handleHttpResponse41984(billingCode)
{
     if (http21984.readyState == 4) {
        var results = http21984.responseText
        results = results.trim();
        if(results.length>=1){
       	 document.forms['billingForm'].elements['billing.billName'].value=results;
        }else{
            alert("Billing ID not Approved" ); 
			 document.forms['billingForm'].elements['billing.billingId'].value="";
			 document.forms['billingForm'].elements['billing.billName'].value="";
			 document.forms['billingForm'].elements['billing.billingId'].focus();
        }
     }
}
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

function handleHttpResponse2(billToCode){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var companyDivision=document.forms['billingForm'].elements['serviceOrder.companyDivision'].value; 
                var res = results.split("~"); 
                var billCurr = document.forms['billingForm'].elements['billing.billingCurrency'];
                     if(res.length >= 2){ 
           			if(res[5]== 'Approved'){
           				var test = res[4];
	           			//alert(test);
							var x=document.getElementById("billPayMethod");
							var billPayMethod = document.getElementById('billPayMethod').selectedIndex;	
							
								for(var a = 0; a < x.length; a++){
									if(test == document.forms['billingForm'].elements['billing.billTo1Point'].options[a].value){
										document.forms['billingForm'].elements['billing.billTo1Point'].options[a].selected="true";
									}}
           			//alert('0'+res[0]+'---1'+res[1]+'---2'+res[2]+'---3'+res[3]+'---4'+res[4]+'---5'+res[5]+'---6'+res[6]+'---7'+res[7]+'---8'+res[8]+'--9'+res[9]);
           				if(res[2] == '0'){
		           			if(res[1] == '' || res[1] == companyDivision || res[1] == 'null' || res[1] == '#'){
		           				document.forms['billingForm'].elements['billing.billToName'].value = res[0];
		           				var billToCode = document.forms['billingForm'].elements['billing.billToCode'].value; 
		           				var dataBaseBillToCode = '${billing.billToCode}';	
		           				if(dataBaseBillToCode!=billToCode){
			           				if(res[7]!='#'){
			           				  	document.forms['billingForm'].elements['billing.creditTerms'].value = res[7];
			           				}else{
			           					document.forms['billingForm'].elements['billing.creditTerms'].value ="";
			           				}
		           				}else{
		           					document.forms['billingForm'].elements['billing.creditTerms'].value='${billing.creditTerms}';
		           				}
		           				if(res[9]!='#'){
		           					document.forms['billingForm'].elements['billing.billingEmail'].value = res[9];
			           				}else{
			           			document.forms['billingForm'].elements['billing.billingEmail'].value="";
			           				}
		           				if(res[6] == '1' && document.forms['billingForm'].elements['billing.insuranceHas'].value == '' && '${serviceOrder.job}' != 'RLO'){
		           					document.forms['billingForm'].elements['billing.insuranceHas'].value = 'Y';
		           				}
		           				if(document.forms['billingForm'].elements['billing.billToReference'].value =="" && res[8]!='#'){
		           					document.forms['billingForm'].elements['billing.billToReference'].value= res[8];
		           				}
		           				showOrHide(0);
		 						showPartnerAlert('onchange',billToCode,'hidBBillTo');
	 						}else{
			           			if(companyDivision != '' || res[1] != companyDivision ){
			           				alert("Company Division of selected bill to code is not "+ companyDivision); 
			           			}else if(companyDivision == '' || companyDivision == 'null' || res[1] == '#'){
			           				alert('Please select company division in service order.');
			           			}
			           			if(billCurr != null && billCurr != 'undefind'){
			                  		 document.forms['billingForm'].elements['billing.billingCurrency'].value="";
			                  	 }
			           			document.forms['billingForm'].elements['billing.billToName'].value="";
								document.forms['billingForm'].elements['billing.billToCode'].value="";
								document.forms['billingForm'].elements['billing.creditTerms'].value="";
								document.forms['billingForm'].elements['billing.billToCode'].select();
								showOrHide(0);
		 						showPartnerAlert('onchange','','hidBBillTo');
			           		}	
			           	}else if(res[2] == '1'){
			           			if(res[3] <= 0){
			           				showOrHide(0);		
			           				alert('The Vendor cannot be selected as credit limit exceeded.');
			           				if(billCurr != null && billCurr != 'undefind'){
			                      		 document.forms['billingForm'].elements['billing.billingCurrency'].value="";
			                      	 }
								    document.forms['billingForm'].elements['billing.billToName'].value="";
									document.forms['billingForm'].elements['billing.billToCode'].value="";
									document.forms['billingForm'].elements['billing.creditTerms'].value="";
									document.forms['billingForm'].elements['billing.billToCode'].select();
									showPartnerAlert('onchange','','hidBBillTo');
			           			}
			           			if(res[3] > 0){
			           				if(res[1] == '' || res[1] == companyDivision || res[1] == 'null' || res[1] == '#'){
				           				document.forms['billingForm'].elements['billing.billToName'].value = res[0];
				           				if(document.forms['billingForm'].elements['billing.billToReference'].value =="" &&  res[8]!='#'){
				           					document.forms['billingForm'].elements['billing.billToReference'].value= res[8];
				           				}
			 							alert("The Vendor currently has available credit of "+res[3]+", please ensure that this s/o will be for less than this amount before selecting this vendor.");
		 								showOrHide(0);
			 							showPartnerAlert('onchange',billToCode,'hidBBillTo');	
		 							}else{
		 								showOrHide(0);
					           			if(companyDivision != '' || res[1] != companyDivision ){
				           					alert("Company Division of selected bill to code is not "+ companyDivision); 
				           				}else if(companyDivision == '' || companyDivision == 'null' || res[1] == '#'){
				           					alert('Please select company division in service order.');
				           				}
					           			if(billCurr != null && billCurr != 'undefind'){
					                  		 document.forms['billingForm'].elements['billing.billingCurrency'].value="";
					                  	 }
					           			document.forms['billingForm'].elements['billing.billToName'].value="";
										document.forms['billingForm'].elements['billing.billToCode'].value="";
										document.forms['billingForm'].elements['billing.creditTerms'].value="";
										document.forms['billingForm'].elements['billing.billToCode'].select();
										showPartnerAlert('onchange','','hidBBillTo');	
				           			}
				           		}
			           		}
           			 }else{
           				showOrHide(0);
	           			alert("Bill To code not approved");
	           			if(billCurr != null && billCurr != 'undefind'){
	                  		 document.forms['billingForm'].elements['billing.billingCurrency'].value="";
	                  	 }
	                  	document.forms['billingForm'].elements['billing.billToName'].value="";
			 			document.forms['billingForm'].elements['billing.billToCode'].value="";
			 			document.forms['billingForm'].elements['billing.creditTerms'].value="";
			 			document.forms['billingForm'].elements['billing.billToCode'].select();
			 			showPartnerAlert('onchange','','hidBBillTo');
			 		 }
           		}else{
               		showOrHide(0);
           			alert("Bill To code not valid");
           			if(billCurr != null && billCurr != 'undefind'){
                  		 document.forms['billingForm'].elements['billing.billingCurrency'].value="";
                  	 }
                  	document.forms['billingForm'].elements['billing.billToName'].value="";
		 			document.forms['billingForm'].elements['billing.billToCode'].value="";
		 			document.forms['billingForm'].elements['billing.creditTerms'].value="";
		 			document.forms['billingForm'].elements['billing.billToCode'].select();
		 			showPartnerAlert('onchange','','hidBBillTo');
            	}
       	}
}

function handleHttpResponse3(billTo2Code){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim(); 
                var companyDivision=document.forms['billingForm'].elements['serviceOrder.companyDivision'].value; 
                var res = results.split("~"); 
                if(res.length >= 2){ 
                	if(res[5]== 'Approved'){
           				if(res[2] == '0'){
		           			if(res[1] == '' || res[1] == companyDivision || res[1] == 'null' || res[1] == '#'){
		           				document.forms['billingForm'].elements['billing.billTo2Name'].value = res[0];
		           				if(document.forms['billingForm'].elements['billing.billToReference'].value =="" &&  res[8]!='#'){
		           					document.forms['billingForm'].elements['billing.billToReference'].value= res[8];
		           				}
		           				showOrHide(0);
		 						showPartnerAlert('onchange',billTo2Code,'hidBBillTo2');
	 						}else{
	 							showOrHide(0);	
			           			if(companyDivision != '' || res[1] != companyDivision ){
			           				alert("Company Division of selected bill to code is not "+ companyDivision); 
			           			}else if(companyDivision == '' || companyDivision == 'null' || res[1] == '#'){
			           				alert('Please select company division in service order.');
			           			}
			           			document.forms['billingForm'].elements['billing.billTo2Name'].value="";
						 		document.forms['billingForm'].elements['billing.billTo2Code'].value="";
						 		document.forms['billingForm'].elements['billing.billTo2Code'].select();
						 		showPartnerAlert('onchange','','hidBBillTo2');
			           		}	
		           		}else if(res[2] == '1'){
		           			if(res[3] <= 0){
		           				showOrHide(0);
		           				alert('The Vendor cannot be selected as credit limit exceeded.');
							    document.forms['billingForm'].elements['billing.billTo2Name'].value="";
								document.forms['billingForm'].elements['billing.billTo2Code'].value="";
								document.forms['billingForm'].elements['billing.billTo2Code'].select();
								showPartnerAlert('onchange','','hidBBillTo2');
		           			}
		           			if(res[3] > 0){
		           				if(res[1] == '' || res[1] == companyDivision || res[1] == 'null' || res[1] == '#'){
			           				document.forms['billingForm'].elements['billing.billTo2Name'].value = res[0];
			           				if(document.forms['billingForm'].elements['billing.billToReference'].value =="" &&  res[8]!='#'){
			           					document.forms['billingForm'].elements['billing.billToReference'].value= res[8];
			           				}
		 							alert("The Vendor currently has available credit of "+res[3]+", please ensure that this s/o will be for less than this amount before selecting this vendor.");
	 								showOrHide(0);
		 							showPartnerAlert('onchange',billTo2Code,'hidBBillTo2');
	 							}else{
	 								showOrHide(0);
				           			if(companyDivision != '' || res[1] != companyDivision ){
			           					alert("Company Division of selected bill to code is not "+ companyDivision); 
				           			}else if(companyDivision == '' || companyDivision == 'null' || res[1] == '#'){
				           				alert('Please select company division in service order.');
				           			}
				           			document.forms['billingForm'].elements['billing.billTo2Name'].value="";
									document.forms['billingForm'].elements['billing.billTo2Code'].value="";
									document.forms['billingForm'].elements['billing.billTo2Code'].select();
									showPartnerAlert('onchange','','hidBBillTo2');
			           			}
		           			}
		           		}
           			}else{
           				showOrHide(0);
	           			alert("Bill To code not approved");
	                  	document.forms['billingForm'].elements['billing.billToName'].value="";
			 			document.forms['billingForm'].elements['billing.billTo2Code'].value="";
			 			document.forms['billingForm'].elements['billing.billTo2Code'].select();
			 			showPartnerAlert('onchange','','hidBBillTo2');
			 		}
           		}else if(res.length < 2){
           			 showOrHide(0);	 
                     alert("Bill To code not valid");
                     document.forms['billingForm'].elements['billing.billTo2Name'].value="";
					 document.forms['billingForm'].elements['billing.billTo2Code'].value="";
					 document.forms['billingForm'].elements['billing.billTo2Code'].select();
					 showPartnerAlert('onchange','','hidBBillTo2');
				}
        }
}
        
 function handleHttpResponse4(billingCode){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var companyDivision=document.forms['billingForm'].elements['serviceOrder.companyDivision'].value; 
                var res = results.split("~"); 
                if(res.length >= 2){
                	if(res[5]== 'Approved'){
                		if(res[2] == '0'){
		           			if(res[1] == '' || res[1] == companyDivision || res[1] == 'null' || res[1] == '#'){
		           				document.forms['billingForm'].elements['billing.privatePartyBillingName'].value = res[0];
		           				showOrHide(0);
		 			 			showPartnerAlert('onchange',billingCode,'hidBBillTo3');
	 						}else{
	 							showOrHide(0);
			           			if(companyDivision != '' || res[1] != companyDivision ){
			           				alert("Company Division of selected bill to code is not "+ companyDivision); 
			           			}else if(companyDivision == '' || companyDivision == 'null' || res[1] == '#'){
			           				alert('Please select company division in service order.');
			           			}
			           			document.forms['billingForm'].elements['billing.privatePartyBillingName'].value="";
						 		document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value="";
						 		document.forms['billingForm'].elements['billing.privatePartyBillingCode'].select();
						 		showPartnerAlert('onchange','','hidBBillTo3');
			           		}	
		           		}else if(res[2] == '1'){
		           			if(res[3] <= 0){
		           				showOrHide(0);
		 			 			alert('The Vendor cannot be selected as credit limit exceeded.');
							    document.forms['billingForm'].elements['billing.privatePartyBillingName'].value="";
						 		document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value="";
						 		document.forms['billingForm'].elements['billing.privatePartyBillingCode'].select();
						 		showPartnerAlert('onchange','','hidBBillTo3');
		           			}
		           			if(res[3] > 0){
		           				if(res[1]== '' || res[1] == companyDivision || res[1] == 'null' || res[1]== '#'){
			           				document.forms['billingForm'].elements['billing.privatePartyBillingName'].value = res[0];
		 							alert("The Vendor currently has available credit of "+res[3]+", please ensure that this s/o will be for less than this amount before selecting this vendor.");
	 								showOrHide(0);
		 			 				showPartnerAlert('onchange',billingCode,'hidBBillTo3');
	 							}else{
	 								showOrHide(0);
		 			 				if(companyDivision != '' || res[1] != companyDivision ){
			           					alert("Company Division of selected bill to code is not "+ companyDivision); 
				           			}else if(companyDivision == ''||companyDivision == 'null' || res[1] == '#'){
				           				alert('Please select company division in service order.');
				           			}
				           			document.forms['billingForm'].elements['billing.privatePartyBillingName'].value="";
									document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value="";
									document.forms['billingForm'].elements['billing.privatePartyBillingCode'].select();
									showPartnerAlert('onchange','','hidBBillTo3');
			           			}
		           			}
		           			
		           		}
                	}else{
                		showOrHide(0);
		 			 	alert("Bill To code not approved");
                     	document.forms['billingForm'].elements['billing.privatePartyBillingName'].value="";
					 	document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value="";
					 	document.forms['billingForm'].elements['billing.privatePartyBillingCode'].select();
					 	showPartnerAlert('onchange','','hidBBillTo3');
                	} 
           		 }else{
                  	 showOrHide(0);
		 			 alert("Bill To code not valid");
                     document.forms['billingForm'].elements['billing.privatePartyBillingName'].value="";
					 document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value="";
					 document.forms['billingForm'].elements['billing.privatePartyBillingCode'].select();
					 showPartnerAlert('onchange','','hidBBillTo3');
                 }
       }
}

function handleHttpResponse5(){ 
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
           				document.forms['billingForm'].elements['billing.vendorName'].value = res[1];
           				
           				if(res[10]=='Y' && '${serviceOrder.job}' != 'STO' && '${serviceOrder.job}' != 'RLO'){
           					var hasValuationNew = document.forms['billingForm'].elements['billing.insuranceHas'].value;
           					hasValuationNew=hasValuationNew.trim();
           					var additionalNoVal = 'N';
           						try{
           						additionalNoVal = document.forms['billingForm'].elements['additionalNoFlag'].value;
           						}catch(e){}
	           				 var vendorId = document.forms['billingForm'].elements['billing.vendorCode'].value;
	           				 var contract = document.forms['billingForm'].elements['billing.contract'].value;
    	           			if(document.getElementById("displayDivAdditionalNo") != null){
        	           			if(additionalNoVal=='Y'){
        	           				//if(vendorId=='T18817' && (contract=='BZ 2013' || contract=='IKEA 2013-2014')){
        	           					//<c:if test="${printCertificateFlag=='true'}">
                   						//document.getElementById("displayDivAdditionalNo").style.display = "block";
                   						//</c:if>	
        	           				//}else if(vendorId!='T18817'){
        	           					//<c:if test="${printCertificateFlag=='true'}">
                   						//document.getElementById("displayDivAdditionalNo").style.display = "block";
                   						//</c:if>
                   						<c:if test="${printCertificateFlag=='true'}">
                   						document.getElementById("displayDivAdditionalNo").style.display = "block";
                   						</c:if>	        	           			
		        	           			<c:if test="${printCertificateFlag!='true'}">
		           						document.getElementById("displayDivAdditionalNo").style.display = "none";
           								</c:if>
        	           			}else{        	           				
		        	           			if((hasValuationNew=='Y') && (additionalNoVal=='N')){        	           				
		                   						<c:if test="${printCertificateFlag=='true'}">
		                   						document.getElementById("displayDivAdditionalNo").style.display = "block";
		                   						</c:if>	        	           			
				        	           			<c:if test="${printCertificateFlag!='true'}">
				           						document.getElementById("displayDivAdditionalNo").style.display = "none";
		           								</c:if>
		        	           			}else{
		        	           				document.getElementById("displayDivAdditionalNo").style.display = "none";
		        	           			}
    	           					}	
        	           			}
    	           			if(document.getElementById("displayDiv") != null){
    	           				if(hasValuationNew=='Y'){
           						document.getElementById("displayDiv").style.display = "block";
    	           				}else{
    	           					document.getElementById("displayDiv").style.display = "none";
    	           					document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
    	           				}
           					}	
           				}else{
    	           			if(document.getElementById("displayDiv") != null){
           						document.getElementById("displayDiv").style.display = "none";
           						document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
           					}	
    	           			if(document.getElementById("displayDivAdditionalNo") != null){
           						document.getElementById("displayDivAdditionalNo").style.display = "none";
           						document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
           					}	
           				}
	           		}else{
	           			alert("Vendor Code is not approved" ); 
					    document.forms['billingForm'].elements['billing.vendorName'].value="";
				 		document.forms['billingForm'].elements['billing.vendorCode'].value="";
				 		document.forms['billingForm'].elements['billing.billingInsurancePayableCurrency'].value="";
				 		if(document.getElementById("displayDiv") != null){
							document.getElementById("displayDiv").style.display = "none";
							document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
						}
	           			if(document.getElementById("displayDivAdditionalNo") != null){
       						document.getElementById("displayDivAdditionalNo").style.display = "none";
       					}	
				 		//document.forms['billingForm'].elements['billing.vendorCode'].select();
	           		}
               	}else{
                     alert("Vendor Code not valid" );
                 	 document.forms['billingForm'].elements['billing.vendorName'].value="";
				 	 document.forms['billingForm'].elements['billing.vendorCode'].value="";
				 	document.forms['billingForm'].elements['billing.billingInsurancePayableCurrency'].value="";
				 	if(document.getElementById("displayDiv") != null){
						document.getElementById("displayDiv").style.display = "none";
						document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
					}
           			if(document.getElementById("displayDivAdditionalNo") != null){
   						document.getElementById("displayDivAdditionalNo").style.display = "none";
   					}					
				 	 //document.forms['billingForm'].elements['billing.vendorCode'].select();
			   }
       }
}


function autoStorageBilling(val){
	var  permissionTest  = "";
	<sec-auth:authComponent componentId="module.field.billing.isAutomatedStorageBillingProcessing">  
		  permissionTest  = <%=(Integer)request.getAttribute("module.field.billing.isAutomatedStorageBillingProcessing" + "Permission")%>;
		
	</sec-auth:authComponent>
	if(permissionTest > 2){
		try{
		 var payMethod =document.forms['billingForm'].elements['billing.billTo1Point'].value;
		 var billToCode = document.forms['billingForm'].elements['billing.billToCode'].value;
		 if(('${serviceOrder.job}' != 'STO' && '${serviceOrder.job}' != 'STF' && '${serviceOrder.job}' != 'TPS') || payMethod == '' || billToCode == ''){
		 	document.forms['billingForm'].elements['billing.isAutomatedStorageBillingProcessing'].disabled = true;
		 }
		 
		 if(payMethod != 'AE' || payMethod != 'VC' || payMethod != 'MC' || payMethod != 'DEB'){
		 	document.forms['billingForm'].elements['billing.isAutomatedStorageBillingProcessing'].disabled = true;
		 }
		 
		 if(('${serviceOrder.job}' == 'STO' || '${serviceOrder.job}' == 'STF' || '${serviceOrder.job}' == 'TPS') && billToCode != '' && (payMethod == 'AE' || payMethod == 'VC' || payMethod == 'MC' || payMethod == 'DEB')){
			var url="autoStorageBilling.html?ajax=1&decorator=simple&popup=true&billToCode=" + encodeURI(billToCode);
		    httpAutoStorageBilling.open("GET", url, true);
		    httpAutoStorageBilling.onreadystatechange = function() {autoStorageBillingRes(val);};
		    httpAutoStorageBilling.send(null);
	     } else{
     	document.forms['billingForm'].elements['billing.isAutomatedStorageBillingProcessing'].disabled = true;
     }
	}catch(e){}
	}
}

function autoStorageBillingRes(val){
	if (httpAutoStorageBilling.readyState == 4){
		var results = httpAutoStorageBilling.responseText;
		results = results.trim();
		var res = results.split("#"); 
		if(res.length >= 2){
			if(res[1] =='null' || res[1] =='' || res[2] == 'yes'){
				document.forms['billingForm'].elements['billing.isAutomatedStorageBillingProcessing'].disabled = false;
				if(val != '1'){
					alert('Please confirm automated storage billing processing flag.');
				}
			}else{
				document.forms['billingForm'].elements['billing.isAutomatedStorageBillingProcessing'].disabled = true; 
			}
		}else{
			document.forms['billingForm'].elements['billing.isAutomatedStorageBillingProcessing'].disabled = false;
			if(val != '1'){
				alert('Please confirm automated storage billing processing flag.');
			}
		}
	}
}
function findAdditionalNo()
{
	  document.getElementById('autoAdditionalImage').src = "<c:url value='/images/loader-move.gif'/>";
	  var addNo=document.forms['billingForm'].elements['billing.additionalNo'].value;
	  if(addNo.trim()==''){
     var url="maxAdditionalNumberList.html?ajax=1&decorator=simple&popup=true";
     http9321.open("GET", url, true);
     http9321.onreadystatechange = handleHttpResponse9321;
     http9321.send(null);
	  }
} 
function handleHttpResponse9321()
{
     if (http9321.readyState == 4) {
        var results = http9321.responseText
        results = results.trim();
        if(results.length>=1){
       	 document.forms['billingForm'].elements['billing.additionalNo'].value=results;
        }
        document.getElementById('autoAdditionalImage').src = "<c:url value='/images/loader-move.gif'/>";
     }
}
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http9321 = getHTTPObject();
var http2 = getHTTPObject();
var http21984 = getHTTPObject();
var httpAutoStorageBilling = getHTTPObjectAutoStorageBilling();
var http3 = getHTTPObject();
var http4 = getHTTPObject();
var http5 = getHTTPObject();
var http6 = getHTTPObject();
function getHTTPObject6(){
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
    
 var http55 = getHTTPObject55();
 var http9 = getHTTPObject9();  
 function getHTTPObject9()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
var http555 = getHTTPObject555();  
var http5555 = getHTTPObject555();
var http257= getHTTPObject555(); 
var http5551984=getHTTPObject555(); 
 function getHTTPObject555()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
  function getHTTPObject55()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function getHTTPObjectAutoStorageBilling()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http222 = getHTTPObjectPricingBilling();
var http77 = getHTTPObjectPricingBilling();
function getHTTPObjectPricingBilling()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function chk()
{
	document.forms['billingForm'].elements['billing.charge'].focus();
	var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
	if(billingContract=='' || billingContract==' ')
	{
	alert("There is no pricing contract in billing: Please select.");
	}
	else{
		var mode;
		if(document.forms['billingForm'].elements['serviceOrder.mode']!=undefined){
			 mode=document.forms['billingForm'].elements['serviceOrder.mode'].value; }else{mode="";}
		var categoryFlag="F";
		<configByCorp:fieldVisibility componentId="component.billingTab.categoryType">
		var categoryFlag='T';
		</configByCorp:fieldVisibility>   
		var	val='Storage';
		if( categoryFlag=='T'){
			javascript:openWindow('chargess.html?contract='+billingContract+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_ninthDescription=billing.storageVatExclude&fld_eigthDescription=eigthDescription&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_thirdDescription=thirdDescription&fld_code=billing.charge&categoryType='+val);
		}else{
			javascript:openWindow('chargess.html?contract='+billingContract+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_ninthDescription=billing.storageVatExclude&fld_eigthDescription=eigthDescription&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_thirdDescription=thirdDescription&fld_code=billing.charge');
		}
		findChargeRateFast();
		document.forms['billingForm'].elements['billing.postGrate'].focus();
	}
}
function chkInsurance()
{	
	var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
	if(billingContract=='' || billingContract==' ')
	{
	alert("There is no pricing contract in billing: Please select.");
	}
	else{
		var mode;
		if(document.forms['billingForm'].elements['serviceOrder.mode']!=undefined){
			 mode=document.forms['billingForm'].elements['serviceOrder.mode'].value; }else{mode="";}
		var categoryFlag="F";
		<configByCorp:fieldVisibility componentId="component.billingTab.categoryType">
		var categoryFlag='T';
		</configByCorp:fieldVisibility>	
		val='Insurance';
		if( categoryFlag=='T'){
		javascript:openWindow('chargess.html?contract='+billingContract+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_ninthDescription=billing.insuranceVatExclude&fld_eigthDescription=eigthDescription&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_thirdDescription=thirdDescription&fld_code=billing.insuranceCharge&categoryType='+val);
		}else{
			javascript:openWindow('chargess.html?contract='+billingContract+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_ninthDescription=billing.insuranceVatExclude&fld_eigthDescription=eigthDescription&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_thirdDescription=thirdDescription&fld_code=billing.insuranceCharge');	
		}
}
}
function chkInsurance1()
{	
	var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
	if(billingContract=='' || billingContract==' ')
	{
	alert("There is no pricing contract in billing: Please select.");
	}
	else{
		var mode;
		if(document.forms['billingForm'].elements['serviceOrder.mode']!=undefined){
			 mode=document.forms['billingForm'].elements['serviceOrder.mode'].value; }else{mode="";}
		var categoryFlag="F";
		<configByCorp:fieldVisibility componentId="component.billingTab.categoryType">
		var categoryFlag='T';
		</configByCorp:fieldVisibility>   
		var val='Insurance';
		if( categoryFlag=='T'){
			javascript:openWindow('chargess.html?contract='+billingContract+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_ninthDescription=billing.insuranceVatExclude&fld_eigthDescription=eigthDescription&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_thirdDescription=thirdDescription&fld_code=billing.insuranceCharge1&categoryType='+val);
		}else{
			javascript:openWindow('chargess.html?contract='+billingContract+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_ninthDescription=billing.insuranceVatExclude&fld_eigthDescription=eigthDescription&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_thirdDescription=thirdDescription&fld_code=billing.insuranceCharge1');
		}}
}

function winOpen()
 {
		finalVal='Other';
 		openWindow('brokerPartners.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.vendorName&fld_code=billing.vendorCode');
 		 }

function winOpen1()
{
		finalVal='Other';
		openWindow('agentPartners.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.vendorName1&fld_code=billing.vendorCode1');
}
function winOpen2()
{
		finalVal='Other';
		openWindow('agentPartners.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.locationAgentName&fld_code=billing.locationAgentCode');
}
 
 function winOpenForActgCode()
 {
 		finalVal='Other'; 
 		openWindow('brokerForActgCode.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.vendorName&fld_code=billing.vendorCode&fld_seventhDescription=billing.actgCode');
        //document.forms['billingForm'].elements['billing.vendorCode'].select();
 }
	function calculateTotal(){
	////////////alert("ssssssssssssssss");
	var insrVal = document.forms['billingForm'].elements['billing.insuranceValueActual'].value*1;
	var insrRate = document.forms['billingForm'].elements['billing.insuranceRate'].value*1;
	var s=document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].value;
		if( !(document.forms['billingForm'].elements['billing.insuranceRate'].value == '') ){
			var valu = (insrVal*insrRate)/100;
			var valu1 = Math.round(valu*100)/100;
			document.forms['billingForm'].elements['billing.totalInsrVal'].value = (valu1).toFixed(2);
    		//if((valu1 < document.forms['billingForm'].elements['billing.insuranceRate'].value) && ((s=="GS-A : General Storage - Option A - Full Replacement Cost") || (s=="GS-B : General Storage - Option B - Actual Cash Value") || (s=="LMS-A : Local/Storage - Option A Full Replacement Cost") || (s=="LMS-B : Local/Storage Option B - Actual Cash Value") || (s=="LMS-C : Local/Storage - Option C - Carrier's Liability of $0.30 per lbs")))
			//{
			//}
			}
			else {
			document.forms['billingForm'].elements['billing.totalInsrVal'].value = '';
			}
	}
	
	function goToCreditcard(clickType){
   		if(document.forms['billingForm'].elements['billing.isCreditCard'].value=='1')
  		{
  			checkdate(clickType);
  			autoSaveFunc(clickType);
  		}
	}
function handleHttpResponse6()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                 alert(http2.responseText);
                results = results.trim();
                if(results.length>=1)
                {
                 alert(results);
                 
                 }
                 else
                 {
                     alert("Vendor code does not exist, Please select another" );
			   }
             }
        }
<%--	
function insOption(target){
	var insrVal = document.forms['billingForm'].elements['billing.insuranceValueActual'].value*1;
	var insrRate = document.forms['billingForm'].elements['billing.insuranceRate'].value*1;
	var s=document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].value;
	var t=document.forms['billingForm'].elements['billing.totalInsrVal'].value;
			if((document.forms['billingForm'].elements['billing.insuranceHas'].value == 'Y') && t <= 0)
			{
			if (target == 'notsave'){
		alert("Insurance Total Amount cannot be zero");
		     //document.forms['billingForm'].elements['billing.totalInsrVal'].value ='';
		     // document.forms['billingForm'].elements['billing.insuranceValueActual'].value ='';
		      return false;
			}
			}
	}
	
function insOptionSave(target){
	var insrVal = document.forms['billingForm'].elements['billing.insuranceValueActual'].value*1;
	var insrRate = document.forms['billingForm'].elements['billing.insuranceRate'].value*1;
	var s=document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].value;
	var t=document.forms['billingForm'].elements['billing.totalInsrVal'].value;
			if((document.forms['billingForm'].elements['billing.insuranceHas'].value == 'Y') && t <=0)
			{
			if (target == 'save'){
		 alert("Insurance Total Amount cannot be zero");		 
		      return false;
		      }
			}else{
			return checkdate('save');
			}
	}
--%>
function insValues(targetElement){
var optionCode = document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].value;
 if(optionCode!=''){
    var url="bucketNumber.html?ajax=1&decorator=simple&popup=true&optionCode="+optionCode;  
    url=url.replace('%','trila')
     http55.open("GET", url, true); 
     http55.onreadystatechange = handleHttpResponse411111; 
     http55.send(null); 
       ///calculateTotal();
     setTimeout(function(){calculateTotal();},600); 
     }
}

function handleHttpResponse411111() {    
if (http55.readyState == 4)
             {
                var results = http55.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');
                results=results.replace(',','');
                 if(results.length>=1){
                document.forms['billingForm'].elements['billing.insuranceRate'].value = results;
                 } else {
                  document.forms['billingForm'].elements['billing.insuranceRate'].value = '';
                 } 
             }
        }

function baseRateValues(targetElement){
	var optionCode = document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].value;
	 if(optionCode!=''){
	    var url="getFlex1OfParameter.html?ajax=1&decorator=simple&popup=true&optionCode="+optionCode;
	  	url=url.replace('%','trila');
	     httpFlex1.open("GET", url, true); 
	     httpFlex1.onreadystatechange = handleHttpResponseFlex1; 
	     httpFlex1.send(null); 
	     }
	}

	function handleHttpResponseFlex1() {    
	if (httpFlex1.readyState == 4){
	                var results = httpFlex1.responseText
	                results = results.trim();
	                results = results.replace('[','');
	                results=results.replace(']','');
	                results=results.replace(',','');
	                 if(results.length>=1){
	                document.forms['billingForm'].elements['billing.insuranceBuyRate'].value = results;
	                 } else {
	                  document.forms['billingForm'].elements['billing.insuranceBuyRate'].value = '';
	                 }           
	      }
    calBaseTotal();
    }

function calBaseTotal(){
	var buyRate =0
	buyRate= document.forms['billingForm'].elements['billing.insuranceBuyRate'].value ;
	var baseInsVal = 0
	baseInsVal=	document.forms['billingForm'].elements['billing.baseInsuranceValue'].value ;
	if(buyRate!='' && baseInsVal!=''){
		var temp = (buyRate*baseInsVal)/100;
		temp = Math.round(temp*100)/100;
		document.forms['billingForm'].elements['billing.baseInsuranceTotal'].value = temp;
	}else{
		document.forms['billingForm'].elements['billing.baseInsuranceTotal'].value='';
	}
}



function authorizationNos(){
	var authNo = document.forms['billingForm'].elements['billing.billToAuthority'].value;
	var billId = document.forms['billingForm'].elements['billing.id'].value;
	var billingId = document.forms['billingForm'].elements['billing.shipNumber'].value;
	var serviceId = document.forms['billingForm'].elements['serviceOrder.id'].value;
	var serviceJob = document.forms['billingForm'].elements['serviceOrder.job'].value;
	var serviceCom = document.forms['billingForm'].elements['serviceOrder.commodity'].value;
	javascript:openWindow('authorizationNos.html?billId=${billing.id}&billingId=${billing.shipNumber}&authNo='+authNo+'&serviceJob=${serviceOrder.job}&serviceId=${serviceOrder.id}&serviceCom=${serviceOrder.commodity}&decorator=popup&popup=true');
	
}
 function checkMultiAuthorization()
    {  
    var billCode = document.forms['billingForm'].elements['billing.billToCode'].value; 
    var url="checkMultiAuthorization.html?ajax=1&decorator=simple&popup=true&accPartnerCode="+billCode;  
     http55.open("GET", url, true); 
     http55.onreadystatechange = handleHttpResponse41; 
     http55.send(null);
}

function handleHttpResponse41()
        {    if (http55.readyState == 4)
             {
                var results = http55.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results=='true') 
                {
                document.getElementById("hid").style.display="block";
                }
                else{
                document.getElementById("hid").style.display="none";
                
                }
             }
        }
 function authorizationNo2()
 {
	 var billingIdOut='';
	try{
 		var billingIdOut = document.forms['billingForm'].elements['billing.billingIdOut'].value;
	}catch(e){}

 if(billingIdOut!='') 
                {
                document.getElementById("hid1").style.display = 'block';
                }
                else{
                document.getElementById("hid1").style.display = 'none';
                }
 }
        
function enableCheckBox(){
	try{
	document.forms['billingForm'].elements['billing.isAutomatedStorageBillingProcessing'].disabled = false;
	var elementsLen=document.forms['billingForm'].elements.length;
			for(i=0;i<=elementsLen-1;i++){				
						document.forms['billingForm'].elements[i].disabled=false;
			}
	}catch(e){}
}

  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['billingForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['billingForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['billingForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['billingForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'editBilling.html?id='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.forms['billingForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['billingForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
  function goToUrl(id)
	{
		location.href = "editBilling.html?id="+id;
	}

function findWorkTicketList()
{
  var shipNumber=document.forms['billingForm'].elements['serviceOrder.shipNumber'].value;
  var sid=document.forms['billingForm'].elements['serviceOrder.id'].value;
  openWindow('activeWorkTicketList.html?sid='+sid+'&shipNumber='+shipNumber+'&decorator=popup&popup=true',900,300);
}


function chargePerValue(){
	var chargeCheck=document.forms['billingForm'].elements['chargeCodeValidationVal'].value;
	if(chargeCheck=='' || chargeCheck==null){	
	 var charge = document.forms['billingForm'].elements['billing.charge'].value;
	 var postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
	 var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
	 if(charge !=''){
	 	if(postGrate == '0' || postGrate == '0.0' || postGrate == '' || postGrate == '0.00000'){
		     var url="chargePerValue.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract);
		     http6.open("GET", url, true);
		     http6.onreadystatechange = handleHttpResponsePerValue;
		     http6.send(null);
     	}
	 }}
}
function handleHttpResponsePerValue(){
     if(http6.readyState == 4){
        var results = http6.responseText
        results = results.trim();               
        if(results.length>=1){
	     	document.forms['billingForm'].elements['billing.postGrate'].value= results;         
		}
	 }
}
function billingCompleteStorageOutControl()
{
if(document.forms['billingForm'].elements['billingBillCompleteVar'].value== '1'){
if('${serviceOrder.job}' == 'STO' || '${serviceOrder.job}' == 'STF')
{
var agree = confirm("Does repetitive billing needs to be stopped YES/NO? press OK to YES OR CANCEL to NO.");
			if(agree){
			document.forms['billingForm'].elements['billing.storageOut'].value = document.forms['billingForm'].elements['billing.billComplete'].value;
			}
			}
			}
	document.forms['billingForm'].elements['billingBillCompleteVar'].value='0';		
}
function changeBillingBillCompleteVar(){

document.forms['billingForm'].elements['billingBillCompleteVar'].value= '1';      
}

function checkBillToAuthority(){
	if(document.forms['billingForm'].elements['billing.billToAuthority'].value == ""){
 		alert("Enter Authorization No to continue.....");
 		return false;
 		}else{
 		return authorizationNos();
 	}
}
var http52 = getHTTPObject52();
	function getHTTPObject52()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http53 = getHTTPObject53();
	function getHTTPObject53()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http54 = getHTTPObject54();
	function getHTTPObject54()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http55 = getHTTPObject55();
 httpFlex1 = getHTTPObject55();
	function getHTTPObject55()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 
 function getPricing(){
    var job = '${serviceOrder.job}';
   	if(job!=''){
     	var url = "findPricing.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http52.open("GET", url, true);
     	http52.onreadystatechange = handleHttpResponse52;
     	http52.send(null);
    }
    
}
function handleHttpResponse52(){
		if (http52.readyState == 4){
                var results = http52.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['billingForm'].elements['billing.personPricing'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['billingForm'].elements['billing.personPricing'].options[i].text = '';
					document.forms['billingForm'].elements['billing.personPricing'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['billingForm'].elements['billing.personPricing'].options[i].text = stateVal[1];
					document.forms['billingForm'].elements['billing.personPricing'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("personPricing").value = '${billing.personPricing}';
        getBilling();
        }
  }
 
  function getBilling(){
     var job = '${serviceOrder.job}';
   	if(job!=''){
     	var url = "findBilling.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http53.open("GET", url, true);
     	http53.onreadystatechange = handleHttpResponse53;
     	http53.send(null);
    }
    
}
function handleHttpResponse53(){
		if (http53.readyState == 4){
                var results = http53.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['billingForm'].elements['billing.personBilling'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['billingForm'].elements['billing.personBilling'].options[i].text = '';
					document.forms['billingForm'].elements['billing.personBilling'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['billingForm'].elements['billing.personBilling'].options[i].text = stateVal[1];
					document.forms['billingForm'].elements['billing.personBilling'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("personBilling").value = '${billing.personBilling}';
        getPayable();
        }
  }
  
  function getPayable(){
    var job = '${serviceOrder.job}';
   	if(job!=''){
     	var url = "findPayable.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http54.open("GET", url, true);
     	http54.onreadystatechange = handleHttpResponse54;
     	http54.send(null);
    }
}
function handleHttpResponse54(){
		if (http54.readyState == 4){
                var results = http54.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['billingForm'].elements['billing.personPayable'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['billingForm'].elements['billing.personPayable'].options[i].text = '';
					document.forms['billingForm'].elements['billing.personPayable'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['billingForm'].elements['billing.personPayable'].options[i].text = stateVal[1];
					document.forms['billingForm'].elements['billing.personPayable'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("personPayable").value = '${billing.personPayable}';
        getApprovedBy();
        }
  }
   function getApprovedBy(){
     var job = '${serviceOrder.job}';
   	if(job!=''){
     	var url = "findApprovedBy.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http55.open("GET", url, true);
     	http55.onreadystatechange = handleHttpResponse55;
     	http55.send(null);
    }
}
function handleHttpResponse55(){
		
		if (http55.readyState == 4){
                var results = http55.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['billingForm'].elements['billing.approvedBy'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['billingForm'].elements['billing.approvedBy'].options[i].text = '';
					document.forms['billingForm'].elements['billing.approvedBy'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['billingForm'].elements['billing.approvedBy'].options[i].text = stateVal[1];
					document.forms['billingForm'].elements['billing.approvedBy'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("approvedBy").value = '${billing.approvedBy}';
        }
  }
  

function getBAPartner(display,code,position){
	var url = "findPartnerAlertList.html?ajax=1&decorator=simple&popup=true&notesId=" + encodeURI(code);
     httpPA.open("GET", url, true);
     httpPA.onreadystatechange = function() {handleHttpResponsePartner(display,code,position)};
     httpPA.send(null);
}
function handleHttpResponsePartner(display,code,position){
 	if (httpPA.readyState == 4){
		var results = httpPA.responseText
        results = results.trim();
       // alert(code+'----'+position+'----'+results.length);
        if(results.length > 565){
          	document.getElementById(position).style.display="block";
          	if(display != 'onload'){
          		getPartnerAlert(code,position);
          	}
      	}else{
      		document.getElementById(position).style.display="none";
      		ajax_hideTooltip();
      	}
	}
}

var httpPA = getHTTPObjectPA();
function getHTTPObjectPA(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function validateBillToCode(){
var billToCode=document.forms['billingForm'].elements['billing.billToCode'].value;
var contract=document.forms['billingForm'].elements['billing.contract'].value;
var billTo1Point=document.forms['billingForm'].elements['billing.billTo1Point'].value;
var billingInstructionCodeWithDesc=document.forms['billingForm'].elements['billing.billingInstructionCodeWithDesc'].value;
var job = document.forms['billingForm'].elements['serviceOrder.job'].value;
var insHas = document.forms['billingForm'].elements['billing.insuranceHas'].value; 
var insCharge = document.forms['billingForm'].elements['billing.insuranceCharge'].value;
var billThru = document.forms['billingForm'].elements['billing.billThrough'].value;
var storageMeasure = document.forms['billingForm'].elements['billing.storageMeasurement'].value;
var insValAct = document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
var cycle = document.forms['billingForm'].elements['billing.cycle'].value;
var charge = document.forms['billingForm'].elements['billing.charge'].value;
var onHand = document.forms['billingForm'].elements['billing.onHand'].value;
if(billToCode==''){
     alert("Bill To Code is Required Field");
   return false;
   }else if(contract==''){
     alert("Contract is Required Field");
    return false;
   }else if(billTo1Point==''){
     alert("Pay Method is Required Field");
    return false;
   }else if(billingInstructionCodeWithDesc==''){
     alert("Instruction is Required Field");
    return false;   
	} else
   return true;
   }

function checkFloat(temp)
    { 
    var check='';  
    var i; 
	var s = temp.value;
	var fieldName = temp.name;  
	if(temp.value>100){
	alert("You cannot enter more than 100% VAT ")
	document.forms['billingForm'].elements[fieldName].select();
    document.forms['billingForm'].elements[fieldName].value='0.00'; 
	return false;
	} 
	
	var count = 0;
	var countArth = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i); 
        if(c == '.')  {
        	count = count+1
        }
        if(c == '-')    {
        	countArth = countArth+1
        }
        if((i!=0)&&(c=='-')) 	{
       	  alert("Invalid data in vat%." ); 
          document.forms['billingForm'].elements[fieldName].select();
          document.forms['billingForm'].elements[fieldName].value=''; 
       	  return false;
       	} 
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
        	check='Invalid'; 
        }  } 
    if(check=='Invalid'){ 
    alert("Invalid data in vat%." ); 
    document.forms['billingForm'].elements[fieldName].select();
    document.forms['billingForm'].elements[fieldName].value=''; 
    return false;
    }  else{
    s=Math.round(s*100)/100;
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".00";
    } 
    if((value.indexOf(".")+3 != value.length)){
    value=value+"0";
    }
    document.forms['billingForm'].elements[fieldName].value=value; 
    return true;
    }  }
   function getStorageVatPercent(fromFieldId,targetFieldId){
	<c:if test="${systemDefaultVatCalculation=='true'}">
	 var relo="" 
     <c:forEach var="entry" items="${euVatPercentList}">
        if(relo==""){ 
	        if(document.forms['billingForm'].elements[fromFieldId].value=='${entry.key}') {
	        	document.forms['billingForm'].elements[targetFieldId].value='${entry.value}'; 
	        	relo="yes"; 
	       }
	    }
    </c:forEach>
      if(document.forms['billingForm'].elements[fromFieldId].value==''){
      document.forms['billingForm'].elements[targetFieldId].value=0;
      calculateVatAmt();
      }
      </c:if>
	} 
   function getStoragePayVatPercent(fromFieldId,targetFieldId){
		<c:if test="${systemDefaultVatCalculation=='true'}">
		 var relo="" 
	     <c:forEach var="entry" items="${payVatPercentList}">
	        if(relo==""){ 
		        if(document.forms['billingForm'].elements[fromFieldId].value=='${entry.key}') {
		        	document.forms['billingForm'].elements[targetFieldId].value='${entry.value}'; 
		        	relo="yes"; 
		       }
		    }
	    </c:forEach>
	      if(document.forms['billingForm'].elements[fromFieldId].value==''){
	      document.forms['billingForm'].elements[targetFieldId].value=0;
	      calculateVatAmt();
	      }
	      </c:if>
		}
	function getInsuranceVatPercent(){ 
	<c:if test="${systemDefaultVatCalculation=='true'}">
	 var relo="" 
     <c:forEach var="entry" items="${euVatPercentList}">
        if(relo==""){ 
        if(document.forms['billingForm'].elements['billing.insuranceVatDescr'].value=='${entry.key}') {
        document.forms['billingForm'].elements['billing.insuranceVatPercent'].value='${entry.value}'; 
        relo="yes"; 
       }  }
    </c:forEach>
      if(document.forms['billingForm'].elements['billing.insuranceVatDescr'].value==''){
      document.forms['billingForm'].elements['billing.insuranceVatPercent'].value=0;
      calculateVatAmt();
      }
      </c:if>
	} 
function historicalContract(){
		   var job='${serviceOrder.job}';
		   var createdon='${billing.createdOn}';
		   var companyDivision='${serviceOrder.companyDivision}';
		   var billing='${billing.billToCode}'; 
		   var hsContract=document.forms['billingForm'].elements['Historical'].value;
	       var url="historicalContracts.html?ajax=1&decorator=simple&popup=true&hsContract="+ encodeURI(hsContract)+"&job="+ encodeURI(job)+"&createdon="+ encodeURI(createdon)+"&companyDivision="+encodeURI(companyDivision)+"&billing="+encodeURI(billing);
	       httpHC.open("GET", url, true);
	       httpHC.onreadystatechange = handleHttpResponseHC;
	       httpHC.send(null);
	}

	function handleHttpResponseHC(){
			if (httpHC.readyState == 4){
	                var results = httpHC.responseText
	                results = results.trim();
	                results=results.replace("[","")
	                var res=results.replace("]","")
	                
	               	}
	       }
	var httpHC = getHTTPObjectHC();
	function getHTTPObjectHC(){
	    var xmlhttp;
	    if(window.XMLHttpRequest){
	        xmlhttp = new XMLHttpRequest();
	    }else if (window.ActiveXObject){
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp){
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}
	function sendEmail(target){
		var reg = /^([A-Za-z0-9_\-\.\'])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		if(reg.test(target) == false){
		alert('Invalid Email Address');
		}else{
     	var billingEmail = target;
   		var subject = 'S/O# ${serviceOrder.shipNumber}';
   		subject = subject+"  "+"${serviceOrder.firstName} ";
   		subject = subject+"${serviceOrder.lastName}"; 
		var mailto_link = 'mailto:'+encodeURI(billingEmail)+'?subject='+encodeURI(subject);		
		win = window.open(mailto_link,'emailWindow'); 
		if (win && win.open &&!win.closed) win.close(); 
		}
}

	function billingEmailImage(){
		var billingEmailCode=document.forms['billingForm'].elements['billing.billingEmail'].value;
	     if(billingEmailCode=='')
	     { 
	     document.getElementById("billingEmailImage").style.display="none";
	     }
	     else if(billingEmailCode!='')
	     {
	      document.getElementById("billingEmailImage").style.display="block";
	     }
	}

	function fillVendorName(){		
	    var insuranceHas = document.forms['billingForm'].elements['billing.insuranceHas'].value;
	    if(insuranceHas!=''){
	    var url="vendorNameDetails.html?ajax=1&decorator=simple&popup=true&code="+encodeURI(insuranceHas);
	     http55555.open("GET", url, true);
	     http55555.onreadystatechange = handleHttpResponse55555;
	     http55555.send(null);	     
	     }
	}
	function handleHttpResponse55555(){
        if (http55555.readyState == 4){
           var selected = http55555.responseText
           selected = selected.trim();
           if(selected!="")
           {
           var sel = selected.split("~"); 
				    document.forms['billingForm'].elements['billing.vendorName'].value=sel[1];
			 		document.forms['billingForm'].elements['billing.vendorCode'].value=sel[0];
           }
  		}
	}
	var http55555 = getHTTPObject55555();
	function getHTTPObject55555()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function calInsval(){
	var baseIns = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value;
	var exchngeRate = document.forms['billingForm'].elements['billing.exchangeRate'].value;
	var temp= baseIns*exchngeRate;
	temp = Math.round(temp*1000)/1000;
	document.forms['billingForm'].elements['billing.insuranceValueActual'].value = temp;
}

function checkVendorName1(){
    var vendorId = document.forms['billingForm'].elements['billing.vendorCode1'].value;
    if(vendorId!=''){
    	showHide("block");
    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse555;
	    http2.send(null);
     } else {
	     document.forms['billingForm'].elements['billing.vendorName1'].value="";
	     document.forms['billingForm'].elements['billing.vendorBillingCurency'].value="";
	     if(document.getElementById("displayDiv") != null){
				document.getElementById("displayDiv").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
		}
		if(document.getElementById("displayDivAdditionalNo") != null){
				document.getElementById("displayDivAdditionalNo").style.display = "none";
		}			
    }
}
function handleHttpResponse555(){
    if (http2.readyState == 4){
       var results = http2.responseText
       results = results.trim();
       var res = results.split("#"); 
       if(res.length>2){
       	if(res[2] == 'Approved'){
  				document.forms['billingForm'].elements['billing.vendorName1'].value = res[1];  		
  				findVendorCurrencyCodeAjax();
       		}else{
      			showHide("none");
      			alert("Vendor Code is not approved" ); 
			    document.forms['billingForm'].elements['billing.vendorName1'].value="";
		 		document.forms['billingForm'].elements['billing.vendorCode1'].value="";
		 		document.forms['billingForm'].elements['billing.vendorBillingCurency'].value="";
      		}
      	}else{
      		showHide("none");
            alert("Vendor Code not valid" );
        	 document.forms['billingForm'].elements['billing.vendorName1'].value="";
		 	 document.forms['billingForm'].elements['billing.vendorCode1'].value="";
		 	document.forms['billingForm'].elements['billing.vendorBillingCurency'].value="";
	   }
}
}

function checkAgentCode(){
    var vendorId = document.forms['billingForm'].elements['billing.locationAgentCode'].value;
    if(vendorId!=''){
    	showHide("block");
    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse5555;
	    http2.send(null);
     }
    document.forms['billingForm'].elements['billing.locationAgentName'].value="";
}
function handleHttpResponse5555(){
    if (http2.readyState == 4){
       var results = http2.responseText
       results = results.trim();
       var res = results.split("#"); 
       if(res.length>2){
       	if(res[2] == 'Approved'){
       		showHide("none");
  				document.forms['billingForm'].elements['billing.locationAgentName'].value = res[1];  		
       		}else{
      			showHide("none");
      			alert("Location Agent Code is not approved" ); 
			    document.forms['billingForm'].elements['billing.locationAgentCode'].value="";
		 		document.forms['billingForm'].elements['billing.locationAgentName'].value="";
      		}
      	}else{
      		showHide("none");
            alert("Location Agent Code is not valid" );
            document.forms['billingForm'].elements['billing.locationAgentCode'].value="";
	 		document.forms['billingForm'].elements['billing.locationAgentName'].value="";
	   }
	}
}

function getContact(partnerCode){
		openWindow('searchBillingEmail.html?billToCode='+partnerCode+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billingEmail&fld_code=billing.billingEmail');
}
function fillBillToAuthority(){
	var bCode = document.forms['billingForm'].elements['billing.billToCode'].value;
	if(bCode!='') {
	    var url="fillBillToAuthority.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http51.open("GET", url, true); 
	   	http51.onreadystatechange = handleHttp90; 
	   	http51.send(null);	     
     }else{
    	 var billCurr = document.forms['billingForm'].elements['billing.billingCurrency'];
    	 if(billCurr != null && billCurr != 'undefind'){
    		 document.forms['billingForm'].elements['billing.billingCurrency'].value="";
    	 }
     }
}
function handleHttp90(){
    if (http51.readyState == 4){
    	try{
    		findBillToCurrencyCodeAjax();
    	}catch(e){}
    	var results = http51.responseText
    	results = results.trim(); 
    	if(results=="undefined"){
    		results="";
		}
		 	document.forms['billingForm'].elements['billing.billToAuthority'].value = results;
    	}
	}

var http51 = getHTTPObject();
function getHTTPObject()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)
    {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
}
return xmlhttp;
}
function billToCodeFieldValueCopy(){
	var bCode=document.forms['billingForm'].elements['billing.billToCode'].value;
	if(bCode==''){
		document.forms['billingForm'].elements['billing.billToAuthority'].value="";
		document.forms['billingForm'].elements['billing.billToReference'].value="";
		document.forms['billingForm'].elements['billing.billingEmail'].value="";
	}
}
var http511 = getHTTPObject();

function fillInvoiceNumber(){
	if(document.getElementById("emailInvoice").checked){ 
	var bCode = document.forms['billingForm'].elements['billing.billToCode'].value;
	if(bCode!='') {
	    var url="getEmailInvoiceAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http511.open("GET", url, true); 
	   	http511.onreadystatechange = handleHttp905; 
	   	http511.send(null);	     
     }
   }
}
function handleHttp905(){
    if (http511.readyState == 4){    	
    	var results = http511.responseText
    		results = results.trim();
		 	document.forms['billingForm'].elements['billing.storageEmail'].value = results;
    	}
       }


function getHTTPObject()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)
    {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
}
return xmlhttp;
}

storageForVatExclude();
insuranceForVatExclude();

function storageForVatExclude(){
	var forStorageVat=document.forms['billingForm'].elements['billing.storageVatExclude'].value; 
	if(forStorageVat =='true' || forStorageVat =='True'){ 
		document.forms['billingForm'].elements['billing.storageVatDescr'].value="";
		document.forms['billingForm'].elements['billing.storageVatDescr'].disabled = true;
		document.forms['billingForm'].elements['billing.storageVatPercent'].value="0";
		document.forms['billingForm'].elements['billing.storageVatPercent'].disabled = true;
		document.forms['billingForm'].elements['billing.vendorStorageVatDescr'].value="";
		document.forms['billingForm'].elements['billing.vendorStorageVatDescr'].disabled = true;
		document.forms['billingForm'].elements['billing.vendorStorageVatPercent'].value="0";
		document.forms['billingForm'].elements['billing.vendorStorageVatPercent'].disabled = true;
	}
	
	}
	
	
function insuranceForVatExclude(){
	var forInsuranceVat=document.forms['billingForm'].elements['billing.insuranceVatExclude'].value;
	if(forInsuranceVat == 'true' || forInsuranceVat == 'True'){
		document.forms['billingForm'].elements['billing.insuranceVatDescr'].value="";
		document.forms['billingForm'].elements['billing.insuranceVatDescr'].disabled = true;
		document.forms['billingForm'].elements['billing.insuranceVatPercent'].value="0";
		document.forms['billingForm'].elements['billing.insuranceVatPercent'].disabled = true;
	}
	
	}
	


</script>
<script type="text/javascript">
window.onload= function(){
	if (document.getElementById('autoProcessing').checked){
		payMethodType();
	}
}
try{
	checkShowHideNetworkBillToCode('onload');
}catch(e){}
</script>

<script type="text/javascript">

var http111 = getHTTPObject();

function payMethodType(){
	if(document.getElementById("autoProcessing").checked){ 
	    var url="getAutoProcessingAjax.html?ajax=1&decorator=simple&popup=true&bCode";
	    http111.open("GET", url, true); 
	    http111.onreadystatechange = handleHttp905111; 
	    http111.send(null);	     
   }else{
	   var targetElement = document.forms['billingForm'].elements['billing.billTo1Point'];
       var targetElement2 = document.forms['billingForm'].elements['billing.payMethod'];
		targetElement.length = "${fn:length(paytype)+1}";
		targetElement2.length = "${fn:length(paytype)+1}";
			
	   	var i = 1;
	    targetElement.options[0].text = '';
	 	targetElement.options[0].value = '';
	 		
	 	targetElement2.options[0].text = '';
	 	targetElement2.options[0].value = '';
	 	   
	   <c:forEach items="${paytype}" var="entry">
		    targetElement.options[i].text = '${entry.value}';
			targetElement.options[i].value = '${entry.key}';
					
			targetElement2.options[i].text = '${entry.value}';
			targetElement2.options[i].value = '${entry.key}';
			i++;
	  </c:forEach>
	  document.forms['billingForm'].elements['billing.billTo1Point'].value = '${billing.billTo1Point}';	
		document.forms['billingForm'].elements['billing.payMethod'].value ='${billing.payMethod}';
   }
}
function handleHttp905111(){
    if (http111.readyState == 4){    	
            var results = http111.responseText
            results = results.trim();
            var res = results.split("@");
            var targetElement = document.forms['billingForm'].elements['billing.billTo1Point'];
            var targetElement2 = document.forms['billingForm'].elements['billing.payMethod'];
				targetElement.length = res.length;
				targetElement2.length = res.length;
					for(i=1;i<res.length;i++)
					{
						var ss=res[i].split('~');
						document.forms['billingForm'].elements['billing.billTo1Point'].options[i].text = ss[1];
						document.forms['billingForm'].elements['billing.billTo1Point'].options[i].value = ss[0];
						
						document.forms['billingForm'].elements['billing.payMethod'].options[i].text = ss[1];
						document.forms['billingForm'].elements['billing.payMethod'].options[i].value = ss[0];
					}
					if(${billing.billTo1Point!=null && billing.billTo1Point!=''}){
						document.forms['billingForm'].elements['billing.billTo1Point'].value = '${billing.billTo1Point}';	
						document.forms['billingForm'].elements['billing.payMethod'].value = '${billing.payMethod}';	
					}
    	}
 }


function getHTTPObject()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)
    {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
}
return xmlhttp;
}

</script>
<%-- Shifting Complete --%>

<script type="text/javascript">
<c:if test="${redirectflag=='1'}">
 <c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
</c:if>
showOrHide(0);
try{
	if('${hitflag}'!= 1){	
		getPricing();
	}
}
catch(e){}

try {
	accordingBillToCode();
}catch(e){
}finally {
	authorizationNo2();
}
 
try {
	checkBillingInstructionCode();
} catch(e) {
} finally {
 	//autoStorageBilling('1');
} 

try {
	var additionalNoVal = 'N';
		try{
		additionalNoVal = document.forms['billingForm'].elements['additionalNoFlag'].value;
		}catch(e){}
	if(document.forms['billingForm'].elements['billing.insuranceHas'].value == 'N'){
	    document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].selectedIndex =0;
		document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].disabled = true;
		document.getElementById("disDiv").style.display = "block";
		if(document.getElementById("displayDiv") != null){
			document.getElementById("displayDiv").style.display = "none";
			document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
		}
			if(document.getElementById("displayDivAdditionalNo") != null){
					document.getElementById("displayDivAdditionalNo").style.display = "none";
				}
			if(additionalNoVal=='N'){		
				if(document.forms['billingForm'].elements['billing.insuranceHas'].value == 'Y'){
					document.getElementById("disDiv1").style.display = "block";
					document.getElementById("disDiv2").style.display = "block";
				}else if(document.forms['billingForm'].elements['billing.insuranceHas'].value == 'V'){
					document.getElementById("disDiv1").style.display = "block";
					document.getElementById("disDiv2").style.display = "block";
				}else{
					document.getElementById("disDiv1").style.display = "none";
					document.getElementById("disDiv2").style.display = "none";
				}
			}else{
				document.getElementById("disDiv1").style.display = "block";
				document.getElementById("disDiv2").style.display = "block";
				checkVendorName();
			}	
	}else{
	    document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].disabled = false;
		document.getElementById("disDiv").style.display = "none";
		if(document.forms['billingForm'].elements['billing.insuranceHas'].value == 'Y'){
			document.getElementById("disDiv1").style.visibility = "visible";
			document.getElementById("disDiv2").style.visibility = "visible";
			try{
			    checkVendorName();
			    }catch(e){}
		}else if(document.forms['billingForm'].elements['billing.insuranceHas'].value == 'V'){
			if(additionalNoVal=='N'){
			document.getElementById("disDiv1").style.display = "block";
			document.getElementById("disDiv2").style.display = "block";
			if(document.getElementById("displayDiv") != null){				
				document.getElementById("displayDiv").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
			}
   			if(document.getElementById("displayDivAdditionalNo") != null){
					document.getElementById("displayDivAdditionalNo").style.display = "none";
				}
			}else{
				document.getElementById("disDiv1").style.display = "block";
				document.getElementById("disDiv2").style.display = "block";
				checkVendorName();
			}
		}else if(document.forms['billingForm'].elements['billing.insuranceHas'].value == ''){
			document.getElementById("disDiv1").style.display = "none";
			document.getElementById("disDiv2").style.display = "none";
			if(document.getElementById("displayDiv") != null){
				document.getElementById("displayDiv").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
			}
   			if(document.getElementById("displayDivAdditionalNo") != null){
					document.getElementById("displayDivAdditionalNo").style.display = "none";
				}					
		}else{
			if(additionalNoVal=='N'){
			document.getElementById("disDiv1").style.display = "none";
			document.getElementById("disDiv2").style.display = "none";
			if(document.getElementById("displayDiv") != null){
				document.getElementById("displayDiv").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
			}
   			if(document.getElementById("displayDivAdditionalNo") != null){
					document.getElementById("displayDivAdditionalNo").style.display = "none";
				}			
			}else{
				document.getElementById("disDiv1").style.display = "block";
				document.getElementById("disDiv2").style.display = "block";
				checkVendorName();
			}
		}
	}	
} finally {
	autoLinkCreditCard();
}   
 
function showOrHide(value) {
    if (value == 0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value == 1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	}
}

function getBillToCode(){
	
	
	var billToCode = document.forms['billingForm'].elements['billing.billToCode'].value;
    //alert('${billing.billToCode}' +"---" +billToCode)
    //if(billToCode != '${billing.billToCode}')
	//{
	findBillToName();
	//InsuranceValue();
    if(billToCode!=''){
      showOrHide(1);
    }
    
	accordingBillToCode();
	checkMultiAuthorization();
	findPricingBillingPaybaleBILLName();
	//autoStorageBilling('0');
	
	//}
}
// var http222 = getHTTPObjectPricingBilling();
function findPricingBillingPaybaleBILLName(){
    var billToCode = document.forms['billingForm'].elements['billing.billToCode'].value;
    var job = '${serviceOrder.job}';
    var compDivision='${serviceOrder.companyDivision}';
    var url="pricingBillingPaybaleName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode) + "&jobType=" + encodeURI(job) +"&compDivision="+encodeURI(compDivision);
    http222.open("GET", url, true);
    http222.onreadystatechange = handleHttpResponse2001;
    http222.send(null);
}
function handleHttpResponse2001(){	
             if (http222.readyState == 4){
                var results = http222.responseText
                results = results.trim();
                var res = results.split("#");                 
                       	if(res.length >= 3){ 
                			var test = res[0];
  							var x=document.getElementById("personPricing")
  							var personPricing = document.getElementById('personPricing').selectedIndex;
  						
 							for(var a = 0; a < x.length; a++)
 							{
 								if(test == document.forms['billingForm'].elements['billing.personPricing'].options[a].value)
 								{
 									document.forms['billingForm'].elements['billing.personPricing'].options[a].selected="true";
 								}
 							}
 							var testBilling = res[1];
  							var y=document.getElementById("personBilling")
  							var personBilling = document.getElementById('personBilling').selectedIndex;
  							
 							for(var a = 0; a < y.length; a++)
 							{
 								if(testBilling == document.forms['billingForm'].elements['billing.personBilling'].options[a].value)
 								{
 									document.forms['billingForm'].elements['billing.personBilling'].options[a].selected="true";
 								}
 							
 							}
 							var testPayable = res[2];
  							var z=document.getElementById("personPayable")
  							var personPayable = document.getElementById('personPayable').selectedIndex;
  							
 							for(var a = 0; a < z.length; a++)
 							{
 								if(testPayable == document.forms['billingForm'].elements['billing.personPayable'].options[a].value)
 								{
 									document.forms['billingForm'].elements['billing.personPayable'].options[a].selected="true";
 								}
 							
 							}
		           		    
		           		    var testAuditor = res[3];
		           		    var t=document.getElementById("auditorBilling")
  							var personAuditor = document.getElementById('auditorBilling').selectedIndex;
  							
 							for(var a = 0; a < t.length; a++)
 							{
 								if(testAuditor == document.forms['billingForm'].elements['billing.auditor'].options[a].value)
 								{
 									document.forms['billingForm'].elements['billing.auditor'].options[a].selected="true";
 								}
 							
 							}
 							var testClaimHandler = res[5];
 							if(document.getElementById("claimHandler")!=undefined){
 							var c=document.getElementById("claimHandler");
 							var claimPerson=document.getElementById('claimHandler').selectedIndex;
 							for(var a=0; a<c.length;a++)
		           			{
 								if(testClaimHandler == document.forms['billingForm'].elements['billing.claimHandler'].options[a].value){
 									document.forms['billingForm'].elements['billing.claimHandler'].options[a].selected="true";
 								}
 								
							}
 							}
 							if(res[6]!= null && res[6]!='' && res[6]!= '#'){
 			               		document.forms['billingForm'].elements['billing.internalBillingPerson'].value=res[6];
 		                    }
		           		}
		           		
           }
             billingEmailImage();
}
function getBillTo2Code(){
	findBillTo2Name();
	
	var billTo2Code = document.forms['billingForm'].elements['billing.billTo2Code'].value;
    if(billTo2Code!='') { 
      	showOrHide(1);
    }
	
}
function getBillTo3Code(){
	findBillingName();
	
	var billingCode = document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value;
    if(billingCode!=''){
    	showOrHide(1);
    }
	
}

function findExchangeEstRate(){
    var country =document.forms['billingForm'].elements['billing.currency'].value; 
    var url="findExchangeRate.html?ajax=1&decorator=simple&popup=true&country="+encodeURI(country);
    http77.open("GET", url, true);
    http77.onreadystatechange = handleHttpResponseEstRate;
    http77.send(null);
}
function handleHttpResponseEstRate() { 
         if (http77.readyState == 4) {
            var results = http77.responseText
            results = results.trim(); 
            results = results.replace('[','');
            results=results.replace(']','');  
            if(results.length>1) {
              document.forms['billingForm'].elements['billing.exchangeRate'].value=results ;
              } else {
              document.forms['billingForm'].elements['billing.exchangeRate'].value=1;
              document.forms['billingForm'].elements['billing.exchangeRate'].readOnly=false; 
              }
              var mydate=new Date();
	          var daym;
	          var year=mydate.getFullYear()
	          var y=""+year;
	          if (year < 1000)
	          year+=1900
	          var day=mydate.getDay()
	          var month=mydate.getMonth()+1
	          if(month == 1)month="Jan";
	          if(month == 2)month="Feb";
	          if(month == 3)month="Mar";
			  if(month == 4)month="Apr";
			  if(month == 5)month="May";
			  if(month == 6)month="Jun";
			  if(month == 7)month="Jul";
			  if(month == 8)month="Aug";
			  if(month == 9)month="Sep";
			  if(month == 10)month="Oct";
			  if(month == 11)month="Nov";
			  if(month == 12)month="Dec";
			  var daym=mydate.getDate()
			  if (daym<10)
			  daym="0"+daym
			  var datam = daym+"-"+month+"-"+y.substring(2,4); 
			  document.forms['billingForm'].elements['billing.valueDate'].value=datam;  
			 }
         calBaseInsVal();  
		 }
function calBaseInsVal(){
	var insActVal = document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
	var exchangeRate = document.forms['billingForm'].elements['billing.exchangeRate'].value;
	if(insActVal!='' && exchangeRate!='' ){
		var temp =Math.round((insActVal/exchangeRate)*100)/100;
		document.forms['billingForm'].elements['billing.baseInsuranceValue'].value = temp; 
	}
	calBaseTotal();
}

</script>
<script type="text/javascript">
var fieldName = document.forms['billingForm'].elements['field'].value; 
var fieldName1 = document.forms['billingForm'].elements['field1'].value;
if(fieldName!=''){
document.forms['billingForm'].elements[fieldName].className = 'rules-textUpper';
animatedcollapse.addDiv('status', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('sec', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('private', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('domestic', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('gstAutho', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('storage', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('insurance', 'fade=0,persist=0,show=1')
animatedcollapse.init()
}
if(fieldName1!=''){
document.forms['billingForm'].elements[fieldName1].className = 'rules-textUpper';
animatedcollapse.addDiv('status', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('sec', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('private', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('domestic', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('gstAutho', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('storage', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('insurance', 'fade=0,persist=0,show=1')
animatedcollapse.init()
}

try{
    billingEmailImage();
}catch(e){}
</script>
<script type="text/javascript">
var varName = '';
	function checkBillingCompleteStatus(elementName)
	{
		if(elementName.indexOf('-') > 0)
			varName = elementName.split('-')[0];
		else
			varName = elementName;
		return varName;
	}
	function hitBilling(){
		if(varName == 'auditComplete')
		{
			findBillingCompleteAudit();
			varName='';
		}
		else if(varName == 'billing.billCompleteA' || varName == 'billComplete')
		{
			findBillingCompleteA();
			varName='';
		}
		else
		{
			varName='';
			return false;
		}
	}
	setOnSelectBasedMethods(["hitBilling(),changeBillingBillCompleteVar()"]);
	setCalendarFunctionality();
</script>
<script type="text/javascript">
function setChargeCdProperty(){
	var billingInsuranceCrg = document.forms['billingForm'].elements['billing.insuranceCharge'].value;
	var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
    var url="findChargeInsuranceAjax.html?ajax=1&decorator=simple&popup=true&charge="+billingInsuranceCrg+"&contract="+billingContract;
	http17.open("GET", url, true);
    http17.onreadystatechange = handleHttpResponse17;
    http17.send(null);
}
function handleHttpResponse17(){
	if (http17.readyState == 4){
            var results = http17.responseText
            results = results.trim();
            //results = results.split('[').join('').split(']').join('');
            var split = results.split("-");
            if(split[0]>0 ||split[1]>0 ){
            document.forms['billingForm'].elements['billing.insuranceBuyRate'].value=split[0];
            document.forms['billingForm'].elements['billing.insuranceRate'].value=split[1];

            calculateTotal();
            calBaseTotal();
            }
	} 
}
var http17 = getHTTPObject17();
function getHTTPObject17() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

String.prototype.replaceAt=function(index, char) {
    return this.substr(0, index) + char + this.substr(index+char.length);
 }
</script>
<script type="text/javascript">
var httpVendorCurr = getHTTPObject();
var httpInsPayableCurr = getHTTPObject();
function findVendorCurrencyCodeAjax() {
	showHide("block");
    var partnerCode = document.forms['billingForm'].elements['billing.vendorCode1'].value;  
    var url="findVendorCurrencyCodeAjax.html?ajax=1&decorator=simple&popup=true&partnerCode="+partnerCode;  
    httpVendorCurr.open("GET", url, true); 
    httpVendorCurr.onreadystatechange = responseVendorCurrencyCode; 
    httpVendorCurr.send(null); 
} 
function responseVendorCurrencyCode() {
	if (httpVendorCurr.readyState == 4){
	   showHide("none");
       var results = httpVendorCurr.responseText
       results = results.trim();
       document.forms['billingForm'].elements['billing.vendorBillingCurency'].value=results;
	}
}
function findPayableCurrencyCodeAjax(){ 
    var partnerCode = document.forms['billingForm'].elements['billing.vendorCode'].value;  
    if(partnerCode!=''){
    showHide("block");
    var url="findVendorCurrencyCodeAjax.html?ajax=1&decorator=simple&popup=true&partnerCode="+partnerCode;
    httpInsPayableCurr.open("GET", url, true);
    httpInsPayableCurr.onreadystatechange = responsePayableCurrencyCode; 
    httpInsPayableCurr.send(null); 
    }else{
    	document.forms['billingForm'].elements['billing.billingInsurancePayableCurrency'].value="";
    }
    
}
function responsePayableCurrencyCode() {
	if (httpInsPayableCurr.readyState == 4){
	   showHide("none");
       var results = httpInsPayableCurr.responseText
       results = results.trim();
       document.forms['billingForm'].elements['billing.billingInsurancePayableCurrency'].value=results;
	}
}
function findBillToCurrencyCodeAjax() {
	showHide("block");
    var partnerCode = document.forms['billingForm'].elements['billing.billToCode'].value;  
    var url="findVendorCurrencyCodeAjax.html?ajax=1&decorator=simple&popup=true&partnerCode="+partnerCode;  
    httpVendorCurr.open("GET", url, true); 
    httpVendorCurr.onreadystatechange = findBillToCurrencyCode; 
    httpVendorCurr.send(null); 
} 

function findBillToCurrencyCode() {
	try{

	if (httpVendorCurr.readyState == 4){
	   showHide("none");
       var results = httpVendorCurr.responseText
       results = results.trim();
       document.forms['billingForm'].elements['billing.billingCurrency'].value=results;
	}
	}catch(e){}
}
function showHide(action){
	document.getElementById("loader").style.display = action;
}

function openPopWindow(target){
	var targetName=target.name;
	var first = '${serviceOrder.firstName}';
	var last = '${serviceOrder.lastName}';
	var orgAdd1= '${serviceOrder.originAddressLine1}';
	var orgAdd2= '${serviceOrder.originAddressLine2}';
	var orgAdd3= '${serviceOrder.originAddressLine3}';
	var orgCountry= '${serviceOrder.originCountry}';
	var orgState= '${serviceOrder.originState}';
	var orgCity= '${serviceOrder.originCity}';
	var orgZip= '${serviceOrder.originZip}';
	var phone = '${serviceOrder.originHomePhone}';
	var orgphone = '${serviceOrder.originHomePhone}';
	var orgemail = '${serviceOrder.email}';
	var email = '${serviceOrder.email}';
	var orgeFax= '${serviceOrder.originFax}';
	var prefix= '${serviceOrder.prefix}';
	var companyDiv='${serviceOrder.companyDivision}';
	var status='${serviceOrder.status}'; 
	var orgAddress=orgAdd1+'~'+orgAdd2+'~'+orgAdd3+'~'+orgCountry+'~'+orgState+'~'+orgCity+'~'+orgZip+'~'+orgphone+'~'+orgemail+'~'+orgeFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
	var destAdd1= '${serviceOrder.destinationAddressLine1}';
	var destAdd2= '${serviceOrder.destinationAddressLine2}';
	var destAdd3= '${serviceOrder.destinationAddressLine3}';
	var destCountry= '${serviceOrder.destinationCountry}';
	var destState= '${serviceOrder.destinationState}';
	var destCity= '${serviceOrder.destinationCity}';
	var destZip= '${serviceOrder.destinationZip}';
	var destphone = '${serviceOrder.destinationHomePhone}';
	var destemail = '${serviceOrder.destinationEmail}';
	var destFax= '${serviceOrder.destinationFax}';
	var destAddress=destAdd1+'~'+destAdd2+'~'+destAdd3+'~'+destCountry+'~'+destState+'~'+destCity+'~'+destZip+'~'+destphone+'~'+destemail+'~'+destFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
	var compDiv= '${serviceOrder.companyDivision}';
	javascript:openWindow('partnersPopup.html?decorator=popup&popup=true&partnerType=AC&flag=3&firstName=${serviceOrder.firstName}&lastName=${serviceOrder.lastName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&orgAddress='+encodeURIComponent(orgAddress)+'&destAddress='+encodeURIComponent(destAddress)+'&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=serviceOrder.billToName&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billToName&fld_code=billing.billToCode');
}
function openPopWindowSec(target){
	var targetName=target.name;
	var first = '${serviceOrder.firstName}';
	var last = '${serviceOrder.lastName}';
	var orgAdd1= '${serviceOrder.originAddressLine1}';
	var orgAdd2= '${serviceOrder.originAddressLine2}';
	var orgAdd3= '${serviceOrder.originAddressLine3}';
	var orgCountry= '${serviceOrder.originCountry}';
	var orgState= '${serviceOrder.originState}';
	var orgCity= '${serviceOrder.originCity}';
	var orgZip= '${serviceOrder.originZip}';
	var phone = '${serviceOrder.originHomePhone}';
	var orgphone = '${serviceOrder.originHomePhone}';
	var orgemail = '${serviceOrder.email}';
	var email = '${serviceOrder.email}';
	var orgeFax= '${serviceOrder.originFax}';
	var prefix= '${serviceOrder.prefix}';
	var companyDiv='${serviceOrder.companyDivision}';
	var status='${serviceOrder.status}'; 
	var orgAddress=orgAdd1+'~'+orgAdd2+'~'+orgAdd3+'~'+orgCountry+'~'+orgState+'~'+orgCity+'~'+orgZip+'~'+orgphone+'~'+orgemail+'~'+orgeFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
	var destAdd1= '${serviceOrder.destinationAddressLine1}';
	var destAdd2= '${serviceOrder.destinationAddressLine2}';
	var destAdd3= '${serviceOrder.destinationAddressLine3}';
	var destCountry= '${serviceOrder.destinationCountry}';
	var destState= '${serviceOrder.destinationState}';
	var destCity= '${serviceOrder.destinationCity}';
	var destZip= '${serviceOrder.destinationZip}';
	var destphone = '${serviceOrder.destinationHomePhone}';
	var destemail = '${serviceOrder.destinationEmail}';
	var destFax= '${serviceOrder.destinationFax}';
	var destAddress=destAdd1+'~'+destAdd2+'~'+destAdd3+'~'+destCountry+'~'+destState+'~'+destCity+'~'+destZip+'~'+destphone+'~'+destemail+'~'+destFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
	var compDiv= '${serviceOrder.companyDivision}';
	javascript:openWindow('partnersPopup.html?decorator=popup&popup=true&partnerType=AC&flag=1&firstName=${serviceOrder.firstName}&lastName=${serviceOrder.lastName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&orgAddress='+encodeURIComponent(orgAddress)+'&destAddress='+encodeURIComponent(destAddress)+'&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=serviceOrder.billToName&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billTo2Name&fld_code=billing.billTo2Code');
}
function openPopWindowPrivate(target){
	var targetName=target.name;
	var first = '${serviceOrder.firstName}';
	var last = '${serviceOrder.lastName}';
	var orgAdd1= '${serviceOrder.originAddressLine1}';
	var orgAdd2= '${serviceOrder.originAddressLine2}';
	var orgAdd3= '${serviceOrder.originAddressLine3}';
	var orgCountry= '${serviceOrder.originCountry}';
	var orgState= '${serviceOrder.originState}';
	var orgCity= '${serviceOrder.originCity}';
	var orgZip= '${serviceOrder.originZip}';
	var phone = '${serviceOrder.originHomePhone}';
	var orgphone = '${serviceOrder.originHomePhone}';
	var orgemail = '${serviceOrder.email}';
	var email = '${serviceOrder.email}';
	var orgeFax= '${serviceOrder.originFax}';
	var prefix= '${serviceOrder.prefix}';
	var companyDiv='${serviceOrder.companyDivision}';
	var status='${serviceOrder.status}'; 
	var orgAddress=orgAdd1+'~'+orgAdd2+'~'+orgAdd3+'~'+orgCountry+'~'+orgState+'~'+orgCity+'~'+orgZip+'~'+orgphone+'~'+orgemail+'~'+orgeFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
	var destAdd1= '${serviceOrder.destinationAddressLine1}';
	var destAdd2= '${serviceOrder.destinationAddressLine2}';
	var destAdd3= '${serviceOrder.destinationAddressLine3}';
	var destCountry= '${serviceOrder.destinationCountry}';
	var destState= '${serviceOrder.destinationState}';
	var destCity= '${serviceOrder.destinationCity}';
	var destZip= '${serviceOrder.destinationZip}';
	var destphone = '${serviceOrder.destinationHomePhone}';
	var destemail = '${serviceOrder.destinationEmail}';
	var destFax= '${serviceOrder.destinationFax}';
	var destAddress=destAdd1+'~'+destAdd2+'~'+destAdd3+'~'+destCountry+'~'+destState+'~'+destCity+'~'+destZip+'~'+destphone+'~'+destemail+'~'+destFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
	var compDiv= '${serviceOrder.companyDivision}';
	javascript:openWindow('partnersPopup.html?decorator=popup&popup=true&partnerType=AC&flag=2&firstName=${serviceOrder.firstName}&lastName=${serviceOrder.lastName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&orgAddress='+encodeURIComponent(orgAddress)+'&destAddress='+encodeURIComponent(destAddress)+'&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=serviceOrder.billToName&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.privatePartyBillingName&fld_code=billing.privatePartyBillingCode');
}
</script>
<script type="text/javascript"> 
try{
checkMultiAuthorization();	
}catch(e){
}         
 </script>
 <script type="text/javascript"> 
 var httpContractCurr = getHTTPObject();
	function getContractCurrency(){
		var charge = document.forms['billingForm'].elements['billing.charge'].value;
		var contract = document.forms['billingForm'].elements['billing.contract'].value;
	    var url="findContractCurrencyFromChargeAjax.html?ajax=1&decorator=simple&popup=true&contract=${billing.contract}&charge="+charge;
	    httpContractCurr.open("GET", url, true);
	    httpContractCurr.onreadystatechange = getContractCurrencyResponse;
	    httpContractCurr.send(null);
	}
	
	function getContractCurrencyResponse(){
		if (httpContractCurr.readyState == 4){
		       var results = httpContractCurr.responseText;
		       results = results.trim();
		       var curr = results.split('@-');
		       document.forms['billingForm'].elements['billing.contractCurrency'].value = curr[0];
		       document.forms['billingForm'].elements['billing.payableContractCurrency'].value = curr[1];
		}
	}
	function fillRecAccAuditDate(){
		 var sid = document.forms['billingForm'].elements['billing.id'].value;
		 var auditComplete =null;
		 if(document.forms['billingForm'].elements['billing.auditComplete'].value!=''){
			 auditComplete= document.forms['billingForm'].elements['billing.auditComplete'].value;
		 }
		if(sid!=''){
			var agree = confirm("Update the Audit Complete Date for non-invoiced accounting lines");
		       if(agree)
		        {
		    	   progressBarAutoSave('1');
		    	   var url="updateAccAuditDate.html?ajax=1&decorator=simple&popup=true&sid="+encodeURI(sid)+"&auditComplete="+auditComplete;
		    	     http4.open("GET", url, true);
		    	     http4.onreadystatechange = handleHttpResponseUpdateAccAuditDate;
		    	     http4.send(null);   
		        }
		       else
		        {
			        return false;
		        }
		}
	}
	function handleHttpResponseUpdateAccAuditDate(){
	     if (http4.readyState == 4){
	           var result= http4.responseText 
	           progressBarAutoSave('0');        
	     }
	}
	 
	function fillRecAccVat(temp){
		var billtoCode="";
		var check="";
		var billingVatCode="";
		
		if(temp=='primaryVatCode'){
			billtoCode=document.forms['billingForm'].elements['billing.billToCode'].value
			billingVatCode=document.forms['billingForm'].elements['billing.primaryVatCode'].value
			check="1";
		}
		if(temp=='secondaryVatCode'){
			billtoCode=document.forms['billingForm'].elements['billing.billTo2Code'].value
			billingVatCode=document.forms['billingForm'].elements['billing.secondaryVatCode'].value
			check="1";
		}
		if(temp=='privatePartyVatCode'){
			billtoCode=document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value
			billingVatCode=document.forms['billingForm'].elements['billing.privatePartyVatCode'].value
			check="1";
		}
		if(billtoCode!='' && check=='1' && billingVatCode!=''){
			var agree = confirm("Update the VAT code for "+ billtoCode);
		       if(agree)
		        {
		    	   progressBarAutoSave('1');
		    	   var sid = document.forms['billingForm'].elements['billing.id'].value;
		    	   var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
		    	   var url="updateAccRecVat.html?ajax=1&decorator=simple&popup=true&sid="+encodeURI(sid)+"&billToCode="+encodeURI(billtoCode)+"&billingContract="+encodeURI(billingContract)+"&billingVatCode="+encodeURI(billingVatCode);
		    	 //alert(url)
		    	      http3.open("GET", url, true);
		    	      http3.onreadystatechange = handleHttpResponseUpdateAccRecVat;
		    	      http3.send(null);   
		        }
		       else
		        {
		        }
		}
	}
	function handleHttpResponseUpdateAccRecVat(){
	     if (http3.readyState == 4){
	           var result= http3.responseText 
	           progressBarAutoSave('0');        
	     }
	}

	function fillPayAccVat(temp){
		var vendorCode="";
		var check="";
		var vendorVatCode="";
		if(temp=='originAgentVatCode'){
			vendorCode=document.forms['billingForm'].elements['trackingStatus.originAgentCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.originAgentVatCode'].value
			check="1";
		}	
		if(temp=='destinationAgentVatCode'){
			vendorCode=document.forms['billingForm'].elements['trackingStatus.destinationAgentCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.destinationAgentVatCode'].value
			check="1";
		}
		if(temp=='originSubAgentVatCode'){
			vendorCode=document.forms['billingForm'].elements['trackingStatus.originSubAgentCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.originSubAgentVatCode'].value
			check="1";
		}
		if(temp=='destinationSubAgentVatCode'){
			vendorCode=document.forms['billingForm'].elements['trackingStatus.destinationSubAgentCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.destinationSubAgentVatCode'].value
			check="1";
		}
		if(temp=='forwarderVatCode'){
			vendorCode=document.forms['billingForm'].elements['trackingStatus.forwarderCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.forwarderVatCode'].value
			check="1";
		}
		if(temp=='brokerVatCode'){
			vendorCode=document.forms['billingForm'].elements['trackingStatus.brokerCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.brokerVatCode'].value
			check="1";
		}
		if(temp=='networkPartnerVatCode'){
			vendorCode=document.forms['billingForm'].elements['trackingStatus.networkPartnerCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.networkPartnerVatCode'].value
			check="1";
		}
		if(temp=='bookingAgentVatCode'){ 
			vendorCode=document.forms['billingForm'].elements['serviceOrder.bookingAgentCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.bookingAgentVatCode'].value
			check="1";
		}
		if(temp=='vendorCodeVatCode'){
			vendorCode=document.forms['billingForm'].elements['billingVendorCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.vendorCodeVatCode'].value
			check="1";
		}
		if(temp=='vendorCodeVatCode1'){
			vendorCode=document.forms['billingForm'].elements['billingVendorCode1'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.vendorCodeVatCode1'].value
			check="1";
		}  
		if(vendorCode!='' && check=='1' && vendorVatCode!=''){
			var agree = confirm("Update the VAT code for "+ vendorCode);
		       if(agree)
		        {
		    	   progressBarAutoSave('1');
		    	   var sid = document.forms['billingForm'].elements['billing.id'].value;
		    	   var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
		    	   var url="updateAccPayVat.html?ajax=1&decorator=simple&popup=true&sid="+encodeURI(sid)+"&vendorCode="+encodeURI(vendorCode)+"&billingContract="+encodeURI(billingContract)+"&vendorVatCode="+encodeURI(vendorVatCode);
		    	 //alert(url)
		    	      http3.open("GET", url, true);
		    	      http3.onreadystatechange = handleHttpResponseUpdateAccPayVat;
		    	      http3.send(null);   
		        }
		       else
		        {
		        }
		}
	}
	function handleHttpResponseUpdateAccPayVat(){
	     if (http3.readyState == 4){
	           var result= http3.responseText 
	           progressBarAutoSave('0');        
	     }
	}


	function checkPayAccVat(temp,vendorCode){ 
		var vendorVatCode=temp.value;
		<c:if test="${trackingStatus.originAgentCode !='' && not empty trackingStatus.originAgentCode}">
		if(vendorCode==document.forms['billingForm'].elements['trackingStatus.originAgentCode'].value){
			  document.forms['billingForm'].elements['billing.originAgentVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${trackingStatus.destinationAgentCode !='' && not empty trackingStatus.destinationAgentCode}">	
		if(vendorCode==document.forms['billingForm'].elements['trackingStatus.destinationAgentCode'].value){
			 document.forms['billingForm'].elements['billing.destinationAgentVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${trackingStatus.originSubAgentCode !='' && not empty trackingStatus.originSubAgentCode}">	
		if(vendorCode==document.forms['billingForm'].elements['trackingStatus.originSubAgentCode'].value){
			 document.forms['billingForm'].elements['billing.originSubAgentVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${trackingStatus.destinationSubAgentCode !='' && not empty trackingStatus.destinationSubAgentCode}">
		if(vendorCode==document.forms['billingForm'].elements['trackingStatus.destinationSubAgentCode'].value){
			 document.forms['billingForm'].elements['billing.destinationSubAgentVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${trackingStatus.forwarderCode !='' && not empty trackingStatus.forwarderCode}">
		if(vendorCode==document.forms['billingForm'].elements['trackingStatus.forwarderCode'].value){
			 document.forms['billingForm'].elements['billing.forwarderVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${trackingStatus.brokerCode !='' && not empty trackingStatus.brokerCode}">
		if(vendorCode==document.forms['billingForm'].elements['trackingStatus.brokerCode'].value){
			 document.forms['billingForm'].elements['billing.brokerVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${trackingStatus.networkPartnerCode !='' && not empty trackingStatus.networkPartnerCode}">
		if(vendorCode==document.forms['billingForm'].elements['trackingStatus.networkPartnerCode'].value){
			 document.forms['billingForm'].elements['billing.networkPartnerVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${serviceOrder.bookingAgentCode !='' && not empty serviceOrder.bookingAgentCode}">
		if(vendorCode==document.forms['billingForm'].elements['serviceOrder.bookingAgentCode'].value){ 
			 document.forms['billingForm'].elements['billing.bookingAgentVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${billing.vendorCode !='' && not empty billing.vendorCode}">
		if(vendorCode==document.forms['billingForm'].elements['billingVendorCode'].value){
			 document.forms['billingForm'].elements['billing.vendorCodeVatCode'].value=vendorVatCode
			 
		} 
		</c:if>
		<c:if test="${billing.vendorCode1 !='' && not empty billing.vendorCode1}">
		if(vendorCode==document.forms['billingForm'].elements['billingVendorCode1'].value){
			 document.forms['billingForm'].elements['billing.vendorCodeVatCode1'].value=vendorVatCode
			 
		} 
		</c:if>
		 
	}
	
	
	var baseInsV = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value;
	var InsValA = document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
	var InsBuyR = document.forms['billingForm'].elements['billing.insuranceBuyRate'].value;	
	if(baseInsV=='0.000'){
		document.forms['billingForm'].elements['billing.baseInsuranceValue'].value='';
	}
	if(InsValA=='0.000'){ 
		document.forms['billingForm'].elements['billing.insuranceValueActual'].value='';
	}
	if(InsBuyR=='0.000'){
		document.forms['billingForm'].elements['billing.insuranceBuyRate'].value='';
	}
	
	try{
	var vendorVatP = document.forms['billingForm'].elements['billing.vendorStorageVatPercent'].value;
	if(vendorVatP=='0.0000'){
		document.forms['billingForm'].elements['billing.vendorStorageVatPercent'].value='';
	}
	}catch(e){}
	try{
		//InsuranceValue()
		showDetailed();
	}catch(e){
		
	}
</script> 
 
 <script type="text/javascript">
 function checkBillingPerson(){
	 
	 <configByCorp:fieldVisibility componentId="component.button.CWMS.commission">
	var saleManType=document.forms['billingForm'].elements['billing.personBilling'].value;
		if(document.forms['billingForm'].elements['invoiceCount'].value!='0'){
			var oldPersonBilling='${billing.personBilling}';
			var newPersonBilling=saleManType;
			<c:if test="${serviceOrder.companyDivision == 'CAJ' || serviceOrder.companyDivision == 'AIF'}">
			if(newPersonBilling!=oldPersonBilling){
			var url="checkParentAgentAjax.html?ajax=1&decorator=simple&popup=true&oldPersonBilling="+encodeURI(oldPersonBilling)+"&newPersonBilling="+encodeURI(newPersonBilling);			
			 http5551984.open("GET", url, true);
			 http5551984.onreadystatechange = handleHttpResponse9991984;
			 http5551984.send(null);
			}
			</c:if>
  	}
	</configByCorp:fieldVisibility> 
}	    

 function handleHttpResponse9991984()
 { 
      if (http5551984.readyState == 4)  {
         var results = http5551984.responseText
         results = results.trim();
         if(results=='NO'){
		 		alert("Invoice Already Generated.Cannot change the Billing Person.") 
	 	       document.forms['billingForm'].elements['billing.personBilling'].value='${billing.personBilling}';
         }
         
      }
 }
 
function checkEmail() {
	
		var status = false;     
		var emailRegEx = /^[A-Z0-9.'_%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
		     if (document.forms['billingForm'].elements['billing.storageEmail'].value.search(emailRegEx) == -1) {
		    	 document.forms['billingForm'].elements['billing.storageEmail'].value="";
		          alert("Please enter a valid email address.");
		     }else {
		        status = true;
		     }
		     return status;
		}
		
function blankEmailField(){
	if(!document.getElementById("emailInvoice").checked){
		 document.forms['billingForm'].elements['billing.storageEmail'].value="";
		 document.getElementById("storageEmail").readOnly = true;
	}else{
		document.getElementById("storageEmail").readOnly = false;
	}
}

function findDefaultSettingFromPartner(){
	var bCode = document.forms['billingForm'].elements['billing.billToCode'].value;
	if(bCode!='') {
	    var url="findDefaultValuesFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http521.open("GET", url, true); 
	   	http521.onreadystatechange = handleHttp921; 
	   	http521.send(null);	     
     }
}
function handleHttp921(){
    if (http521.readyState == 4){    	
    	var results = http521.responseText
    	results = results.trim();
    	if(results!=''){
	    	var	res = results.split("~");
		    	if(document.forms['billingForm'].elements['billing.contract'].value==''){
		    		if(res[0]!='NA'){
						document.forms['billingForm'].elements['billing.contract'].value=res[0];
		    		}
		    	}
		    	if(document.forms['billingForm'].elements['billing.billToAuthority'].value==''){
		    		if(res[9]!='NA'){
		    			if(res[9]!="undefined" || res[9]!=undefined){
		    				document.forms['billingForm'].elements['billing.billToAuthority'].value=res[9];
		    			}else{
		    				document.forms['billingForm'].elements['billing.billToAuthority'].value="";
		    			}
		    		}
		    	}
		    	if(document.forms['billingForm'].elements['billing.vendorCode'].value==''){
		    		if(res[4]!='NA'){
		    			document.forms['billingForm'].elements['billing.vendorCode'].value = res[4];
		    		}
		    	}
		    	var insOps=document.forms['billingForm'].elements['billing.insuranceOption'].value;
		    	if(insOps==''){
		    		if(res[7]!='NA'){
		    			document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].value = res[7];
		    		}
		    	}
		    	if(res[8]!='NA'){
			    	if(document.forms['billingForm'].elements['billing.primaryVatCode'].value==''){
					 	document.forms['billingForm'].elements['billing.primaryVatCode'].value = res[8];
			    	}			    	
		    }
    	}
   	}
}
var http521 = getHTTPObject();

<c:if test="${(billing.invoiceByEmail != 'true' || billing.invoiceByEmail != true) && serviceOrder.job == 'STO'}">
document.getElementById("storageEmail").readOnly = true;
</c:if>
 
    </script>
<script type="text/javascript">
var anchor = document.getElementsByTagName("a");
for( var i = 0, j =  anchor.length; i < j; i++ ) {
anchor[i].setAttribute( 'tabindex', '-1' );
}

function isSoExtFlag(){
    document.forms['billingForm'].elements['isSOExtract'].value="yes";
    //alert(document.forms['billingForm'].elements['isSOExtract'].value);
  }
try{
makeMandIssueDt();
}catch(e){
}
function autoCompleterAjaxCallChargeCode(divid,fieldName,idval){
	 <configByCorp:fieldVisibility componentId="component.charges.Autofill.chargeCode">
	document.getElementById('chargeCodeValidationVal').value='BillingCharge';
	var ChargeName="";
		ChargeName = document.forms['billingForm'].elements['billing.'+fieldName].value;
	var contract = document.forms['billingForm'].elements['billing.contract'].value;
	contract=contract.trim();
	var category = "";
	var companyDiv="";
	companyDiv="";
	var pageCondition='TemplateNonInternalCategory';
	var id='';
	if(contract!=""){
		if(ChargeName.length>=3){
           $.get("ChargeCodeAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", {
           	searchName:ChargeName,
           	contract: contract,
           	chargeCodeId:idval,
           	autocompleteDivId: divid,
           	id:id,
           	idCondition:fieldName,
           	categoryCode:category,
           	pageCondition:pageCondition,
           	searchcompanyDivision:companyDiv
           }, function(idval) {
               document.getElementById(divid).style.display = "block";
               $("#" + divid).html(idval)
           })
       } else {
           document.getElementById(divid).style.display = "none"
       }
	}
	else{
		alert("There is no Billing contract: Please select.");
		document.forms['billingForm'].elements['billing.'+fieldName].value="";
		document.forms['billingForm'].elements['billing.contract'].focus();
	}
	</configByCorp:fieldVisibility>	
}
function copyChargeDetails(chargecode,chargecodeId,autocompleteDivId,id,fieldName){
		document.forms['billingForm'].elements['billing.'+fieldName].value=chargecode;
		document.getElementById(autocompleteDivId).style.display = "none";
		document.forms['billingForm'].elements['chargeCodeValidationVal'].value='';
		checkValueAfterChargeCode(fieldName,chargecodeId);
		findChargeRateFast();
		if(chargecodeId=='A'){
		chargePerValue();
		}
		changeStatus();
	
}
function closeMyChargeDiv(autocompleteDivId,chargecode,chargecodeId,id,fieldName){
	document.getElementById(autocompleteDivId).style.display = "none";
	 if(document.forms['billingForm'].elements['billing.'+fieldName].value==''){
		 document.forms['billingForm'].elements['billing.'+fieldName].value="";	
	}
	 document.forms['billingForm'].elements['chargeCodeValidationVal'].value='';
	 checkValueAfterChargeCode(fieldName,chargecodeId);
	 findChargeRateFast();
	 if(chargecodeId=='A'){
		chargePerValue();
			}
	 changeStatus();
	}
function closeMyChargeDiv2(chargecodeId,id){
	 document.forms['billingForm'].elements['chargeCodeValidationVal'].value='';
	}
function checkValueAfterChargeCode(fieldName,chargecodeId){
	var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
	var chargeCode="";
		chargeCode =document.forms['billingForm'].elements['billing.'+fieldName].value;
	if(billingContract=='' || billingContract==' ')
	{
	alert("There is no pricing contract in billing: Please select.");
	}
	else{ 
     var url="checkChargeCodeNew.html?ajax=1&decorator=simple&popup=true&contractCde="+encodeURIComponent(billingContract)+"&chargeCde="+encodeURIComponent(chargeCode); 
     http5.open("GET", url, true);
     http5.onreadystatechange = function(){ handleHttpResponseValue(fieldName,chargecodeId);};
     http5.send(null);
	}
}
function handleHttpResponseValue(fieldName,chargecodeId){
		if (http5.readyState == 4){
                var results = http5.responseText
                results = results.trim(); 
                 results = results.replace('[','');
                results=results.replace(']',''); 
                var res = results.split("#"); 
    	        if(results.length>25)  {
                var vatExclude=false;
                if(results[0]=='N'){
                	vatExclude=false;
                }
                if(results[0]=='Y'){
                	vatExclude=true;
                }
                if(chargecodeId=='A'){
                document.forms['billingForm'].elements['billing.storageVatExclude'].value =vatExclude;
                }
                if(chargecodeId=='C'){
                	document.forms['billingForm'].elements['billing.insuranceVatExclude'].value =vatExclude;
                }
				progressBarAutoSave('0'); 
         }
    	        else{
    	        		document.forms['billingForm'].elements['billing.'+fieldName].value='';
    		            alert("Charge code does not exist according the pricing contract in Pricing Detail, Please select another" );
    				   
    	        }}
}


</script>