package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.DsSchoolEducationalCounselingDao;
import com.trilasoft.app.model.DsSchoolEducationalCounseling;
import com.trilasoft.app.service.DsSchoolEducationalCounselingManager;

public class DsSchoolEducationalCounselingManagerImpl extends GenericManagerImpl<DsSchoolEducationalCounseling, Long> implements DsSchoolEducationalCounselingManager {
	
	DsSchoolEducationalCounselingDao dsSchoolEducationalCounselingDao;
	
	public DsSchoolEducationalCounselingManagerImpl(DsSchoolEducationalCounselingDao dsSchoolEducationalCounselingDao) {
		super(dsSchoolEducationalCounselingDao); 
        this.dsSchoolEducationalCounselingDao = dsSchoolEducationalCounselingDao; 
	}
	public List getListById(Long id) {
		return dsSchoolEducationalCounselingDao.getListById(id);
	}
}
