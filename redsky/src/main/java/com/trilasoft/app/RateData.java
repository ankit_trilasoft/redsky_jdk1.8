package com.trilasoft.app;

import java.math.BigDecimal;

public class RateData{
	public String quotecurrency;
	public BigDecimal mid;
	public String getQuotecurrency() {
		return quotecurrency;
	}
	public void setQuotecurrency(String quotecurrency) {
		this.quotecurrency = quotecurrency;
	}
	public BigDecimal getMid() {
		return mid;
	}
	public void setMid(BigDecimal mid) {
		this.mid = mid;
	}
}