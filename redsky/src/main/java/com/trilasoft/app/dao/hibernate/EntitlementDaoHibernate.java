package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.EntitlementDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.Entitlement;


public class EntitlementDaoHibernate extends GenericDaoHibernate<Entitlement, Long> implements EntitlementDao {
	
	private HibernateUtil hibernateUtil;
	
	public EntitlementDaoHibernate() {
		super(Entitlement.class);
	}

	public Map<String, String> findEntitlementAll() {
//		List<Entitlement> entileList = this.getAll();
		List<Entitlement> entileList = getHibernateTemplate().find("from Entitlement");
		Map<String, String> map = new HashMap<String, String>();
		
		for (Entitlement entitlement : entileList) {
			map.put(entitlement.geteOption(), entitlement.geteDescription());
		}
		return map;
	}

	public List findEntitlementByPartnerId(String corpID, Long partnerId) {
//		String query = "select * from entitlement where partnerPrivateId='"+partnerId+"' and corpID = '"+corpID+"'";
//		System.out.println(query);
//		return getSession().createSQLQuery(query).list();
		String query = "from Entitlement where partnerPrivateId='"+partnerId+"' and corpID in('"+corpID+"','TSFT') ";
		return getHibernateTemplate().find("from Entitlement where partnerPrivateId='"+partnerId+"' and corpID in('"+corpID+"','TSFT') ");
		
	}

	public Map<String, String> findEntitlementByParentAgent(String corpId,String parentAgent, Boolean networkRecord, String contractType) {
		//String query = "from Entitlement where partnerCode='"+parentAgent+"' and corpID='"+corpId+"' ";
		Map<String, String> map = new TreeMap<String, String>();
		try{
		if(networkRecord!=null && networkRecord && contractType !=null &&  (!(contractType.equals("")))){
			List entileList = getSession().createSQLQuery("select eOption,eDescription from entitlement where partnerCode='"+parentAgent+"'  ").list();
			Iterator it = entileList.iterator();
			while (it.hasNext()) {
				try{
				Object[] row = (Object[]) it.next();
				map.put(row[0].toString(), row[1].toString());
				}catch(Exception e){
					
				}
			}
			
		}else{
			List entileList = getSession().createSQLQuery("select eOption,eDescription from entitlement where partnerCode='"+parentAgent+"'  ").list();
			Iterator it = entileList.iterator();
			while (it.hasNext()) {
				try{
				Object[] row = (Object[]) it.next();
				map.put(row[0].toString(), row[1].toString());
				}catch(Exception e){
					
				}
			}
			
		}
		}catch(Exception e){
			
		}
		return map;
	}
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}

	public List checkOptionDuplicate(String PartnerCode, String corpId,String Option) {
		String query = "from Entitlement where partnerCode='"+PartnerCode+"' and eOption='"+Option+"' ";
		List<Entitlement> entileList = getHibernateTemplate().find(query);
		return entileList;
	}
	public List getEntitlementReference(String partnerCode, String corpID,Long parentId){
			List al=getSession().createSQLQuery("select id from entitlement where partnerCode='"+ partnerCode +"' and corpID = '"+ corpID +"' and parentId='"+parentId+"' limit 1").list();
			return al;
	}

	public List findLinkEntitlementByPartner(long parseLong) {
		List al=getSession().createSQLQuery("select id,eOption,eDescription,partnerPrivateId,partnerCode,customerFileId,corpID,updatedBy,createdBy,createdOn,updatedOn,parentId  from entitlement where partnerPrivateId='"+parseLong+"'").list(); 
		Iterator it = al.iterator();
		List newList = new ArrayList();
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			Entitlement entitlementDTO = new Entitlement();
			if(row[0] != null){
				entitlementDTO.setId(Long.parseLong(row[0].toString()));	
			}
			if(row[1] != null){
				entitlementDTO.seteOption(row[1].toString());	
			}
			if(row[2] != null){
				entitlementDTO.seteDescription(row[2].toString());	
			}
			if(row[3] != null){
				entitlementDTO.setPartnerPrivateId(Long.parseLong(row[3].toString()));	
			}
			if(row[4] != null){
				entitlementDTO.setPartnerCode(row[4].toString());	
			}
			if(row[5] != null){
				entitlementDTO.setCustomerFileId(Long.parseLong(row[5].toString()));	
			}
			if(row[6] != null){
				entitlementDTO.setCorpID(row[6].toString());	
			}
			if(row[7] != null){
				entitlementDTO.setUpdatedBy(row[7].toString());	
			}
			if(row[8] != null){
				entitlementDTO.setCreatedBy(row[8].toString());	
			}
			if(row[9] != null){
				entitlementDTO.setCreatedOn((Date)(row[9]));	
			}
			if(row[10] != null){
				entitlementDTO.setUpdatedOn((Date)(row[10]));	
			}
			if(row[11] != null){
				entitlementDTO.setParentId(Long.parseLong(row[11].toString()));	
			}
			newList.add(entitlementDTO);
		}
		return newList;
	}
}
