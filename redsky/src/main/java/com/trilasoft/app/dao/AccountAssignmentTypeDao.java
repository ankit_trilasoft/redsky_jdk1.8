package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.AccountAssignmentType;
import com.trilasoft.app.model.TableCatalog;

public interface AccountAssignmentTypeDao extends GenericDao <AccountAssignmentType, Long>{
	
	public List getaccountAssignmentTypeReference(String partnerCode, String corpID,Long parentId);
	public List findAssignmentByParentAgent(String corpId,String parentAgent);
	public List getAssignmentTypeList(String partnerCode,Long partnerId, String corpId);
	public List checkAssignmentDuplicate(String partnerCode, String corpId,String assignment) ;

}
