package com.trilasoft.app.service;

import java.util.List;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.GLCommissionType;
import com.trilasoft.app.model.VanLineGLType;

public interface GLCommissionTypeManager extends GenericManager<GLCommissionType, Long> {

	public List<GLCommissionType> gLCommissionTypeList(String contract, String charge);
	public List<GLCommissionType> glList(String gl);
	
	
}