package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


import org.acegisecurity.userdetails.UserDetails;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.RoleManager;
import org.appfuse.service.UserManager;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.MenuItemPermissionDao;
import com.trilasoft.app.dao.UrlPatternRolesDefinitionSource;
import com.trilasoft.app.model.MenuItemPermission;

public class MenuItemPermissionDaoHibernate extends GenericDaoHibernate<MenuItemPermission, Long> implements MenuItemPermissionDao {
	private UserManager userManager;
	private RoleManager roleManager;
	private Role role;
	private User user;
	private Set<Role> roles;
	/*private UrlPatternRolesDefinitionSource objectDefinitionSource;
	
	public UrlPatternRolesDefinitionSource getObjectDefinitionSource() {
		return objectDefinitionSource;
	}

	public void setObjectDefinitionSource(
			UrlPatternRolesDefinitionSource objectDefinitionSource) {
		this.objectDefinitionSource = objectDefinitionSource;
	}*/

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setRoleManager(RoleManager roleManager) {
		this.roleManager = roleManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
	public MenuItemPermissionDaoHibernate() {
		super(MenuItemPermission.class);
	}

	public List<MenuItemPermission> findByCorpID(String corpId) {
		return getHibernateTemplate().find("from MenuItemPermission where corpID=?",corpId);
	}
	public List checkById(Long id) {
		return getHibernateTemplate().find("select id from MenuItemPermission where id=?",id);
	}
	
	/*@Override
	public MenuItemPermission save(MenuItemPermission object) {
		MenuItemPermission mip = super.save(object);
		objectDefinitionSource.init(); 
		return mip;
	}*/

public List<MenuItemPermission> findByUser(User user, Set<Role> roles) {
		/*Steps
		 * 1. Using UserDao get the User
		 * 2. Get the users roles
		 * 3. Add permissions for the User and all of the users roles to the permissionlist
		 * 4. Return list
		 * 
		 */
		//user = userManager.getUserByUsername(userName);
		//System.out.println("\n\n Roles-->");
		//roles = user.getRoles();
		//String q = "select from Role where id in (select role_id from user_role where user_id="+userName+")";
		
		//roles = user.getRoleList();
		//System.out.println("\n\n Roles-->"+user.getRoleList());
		//System.out.println("\n\n Roles-->");
		//roles = new HashSet(((User) roleManager).getRoleList());
		//roles = user.getRoles();
		String roleList = "";
		for (Role role : roles){
			if (!roleList.equals("")) roleList += ",";
			roleList += "'" + role.getName() + "'" ;
		}
		String q = "from MenuItemPermission where recipient in ('"+user.getUsername() + "'," + roleList +")";
		List<MenuItemPermission> menuItemPermission = getHibernateTemplate().find("from MenuItemPermission where recipient in ('"+user.getUsername() + "'," + roleList +")");
		return menuItemPermission;
		//return getHibernateTemplate().find("from MenuItemPermission where recipient=?", userName);
		
	}
	public List<User> findByUserPermission(String userName){
		user = userManager.getUserByUsername(userName);
		
		Set<Role> roles = user.getRoles();
		String roleList = "";
		for (Role role : roles){
			if (!roleList.equals("")) roleList += ",";
			roleList += "'" + role.getName() + "'" ;
		}
	List<User> menuItemPermission = getHibernateTemplate().find("from MenuItemPermission where recipient in =('", userName + "'," + roleList +")");
		return menuItemPermission;
	}

	public List<Long> findMenuIdsByUser(User user, Set<Role> roles) {
		String roleList = "";
		for (Role role : roles){
			if (!roleList.equals("")) roleList += ",";
			roleList += "'" + role.getName() + "'" ;
		}
		String q = "from MenuItemPermission where recipient in ('"+user.getUsername() + "'," + roleList +")";
		List<Long> menuItemNames = getHibernateTemplate().find("Select menuItemId from MenuItemPermission where permission <> 0 and recipient in ('"+
				user.getUsername() + "'," + roleList +")");
		return menuItemNames;
	}

	
}