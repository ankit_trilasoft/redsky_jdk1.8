package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.CommissionStructure;
import com.trilasoft.app.service.CommissionStructureManager;
import com.trilasoft.app.service.DefaultAccountLineManager;

public class CommissionStructureAction extends BaseAction implements Preparable {
	private Long id;
	private CommissionStructure commissionStructure;
	private CommissionStructureManager commissionStructureManager;
	private String sessionCorpID;
	private List commissionStructureList;
	private DefaultAccountLineManager defaultAccountLineManager;
	private List contractList;
	static final Logger logger = Logger.getLogger(CommissionStructureAction.class);
	public void prepare() throws Exception { 
		getComboList(sessionCorpID);
	}
    public CommissionStructureAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}
    
	public String getComboList(String corpID) {
		logger.warn("USER AUDIT: ID: getComboList ## User: "+getRequest().getRemoteUser()+" Start");
		contractList=defaultAccountLineManager.findContractList(corpID);
	
		logger.warn("USER AUDIT: ID: getComboList ## User: "+getRequest().getRemoteUser()+" Start");
		return SUCCESS;
	}
	private String sContarct;
	private String sChargeCode;
	public String list()
	{ 
		logger.warn("USER AUDIT: ID: list() ## User: "+getRequest().getRemoteUser()+" Start");
		commissionStructureList=commissionStructureManager.getCommPersentAcRange(sessionCorpID,sContarct,"");
		logger.warn("USER AUDIT: ID: list() ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String save() throws Exception {
		String value="";
		CommissionStructure commissionStructure=null;
		  value=getRequest().getParameter("grossMF");
		  String str[]=value.split("~");	
		  if(!value.equalsIgnoreCase("")){
		  try {
			for(int i=0;i<str.length;i++){
				  String rec[]=str[i].split(":");			  
				  commissionStructure=commissionStructureManager.get(Long.parseLong(rec[0]));
				  commissionStructure.setGrossMarginFrom(BigDecimal.valueOf(Double.parseDouble(rec[1].toString())));
				  commissionStructure.setUpdatedOn(new Date());			
				  commissionStructure.setUpdatedBy(getRequest().getRemoteUser());
				  if((commissionStructure.getGrossMarginFrom().doubleValue()!=0)||(commissionStructure.getGrossMarginTo().doubleValue()!=0)){
				  commissionStructureManager.save(commissionStructure);
				  }
			  }
		} catch (Exception e) {
			e.printStackTrace();
		}}
		  value=getRequest().getParameter("grossMT");
		  if(!value.equalsIgnoreCase("")){
		  str=value.split("~");
		  try {
			for(int i=0;i<str.length;i++){
				  String rec[]=str[i].split(":");			  
				  commissionStructure=commissionStructureManager.get(Long.parseLong(rec[0]));
				  commissionStructure.setGrossMarginTo(BigDecimal.valueOf(Double.parseDouble(rec[1].toString())));				  
				  commissionStructure.setUpdatedOn(new Date());			
				  commissionStructure.setUpdatedBy(getRequest().getRemoteUser());
				  if((commissionStructure.getGrossMarginFrom().doubleValue()!=0)||(commissionStructure.getGrossMarginTo().doubleValue()!=0)){				  
				  commissionStructureManager.save(commissionStructure);
				  }
			  }
		} catch (Exception e) {
			e.printStackTrace();
		}}
		  value=getRequest().getParameter("salesBA");
		  if(!value.equalsIgnoreCase("")){
		  str=value.split("~");
		  try {
			for(int i=0;i<str.length;i++){
				  String rec[]=str[i].split(":");			  
				  commissionStructure=commissionStructureManager.get(Long.parseLong(rec[0]));
				  commissionStructure.setSalesBa(BigDecimal.valueOf(Double.parseDouble(rec[1].toString())));
				  commissionStructure.setUpdatedOn(new Date());			
				  commissionStructure.setUpdatedBy(getRequest().getRemoteUser());
				  
				  commissionStructureManager.save(commissionStructure);
			  }
		} catch (Exception e) {
			e.printStackTrace();
		}}
		  value=getRequest().getParameter("salesBO");
		  if(!value.equalsIgnoreCase("")){
		  str=value.split("~");
		  try {
			for(int i=0;i<str.length;i++){
				  String rec[]=str[i].split(":");			  
				  commissionStructure=commissionStructureManager.get(Long.parseLong(rec[0]));
				  commissionStructure.setBackOffice(BigDecimal.valueOf(Double.parseDouble(rec[1].toString())));
				  commissionStructure.setUpdatedOn(new Date());			
				  commissionStructure.setUpdatedBy(getRequest().getRemoteUser());
				  
				  commissionStructureManager.save(commissionStructure);
			  }
		} catch (Exception e) {
			e.printStackTrace();
		}}
		  value=getRequest().getParameter("forwaDR");
		  if(!value.equalsIgnoreCase("")){
		  str=value.split("~");
		  try {
			for(int i=0;i<str.length;i++){
				  String rec[]=str[i].split(":");			  
				  commissionStructure=commissionStructureManager.get(Long.parseLong(rec[0]));
				  commissionStructure.setForwarding(BigDecimal.valueOf(Double.parseDouble(rec[1].toString())));
				  commissionStructure.setUpdatedOn(new Date());			
				  commissionStructure.setUpdatedBy(getRequest().getRemoteUser());
				  
				  commissionStructureManager.save(commissionStructure);
			  }
		} catch (Exception e) {
			e.printStackTrace();
		}}
		  value=getRequest().getParameter("commiNL");
		  if(!value.equalsIgnoreCase("")){
		  str=value.split("~");
		  try {
			for(int i=0;i<str.length;i++){
				  String rec[]=str[i].split(":");
				  commissionStructure = new CommissionStructure();
				  commissionStructure.setCorpID(sessionCorpID);
				  commissionStructure.setChargeCode(sChargeCode);
				  commissionStructure.setContractName(sContarct);
				  commissionStructure.setCreatedOn(new Date());
				  commissionStructure.setUpdatedOn(new Date());			
				  commissionStructure.setCreatedBy(getRequest().getRemoteUser());
				  commissionStructure.setUpdatedBy(getRequest().getRemoteUser());
				  commissionStructure.setGrossMarginFrom(BigDecimal.valueOf(Double.parseDouble(rec[0].toString())));				  
				  commissionStructure.setGrossMarginTo(BigDecimal.valueOf(Double.parseDouble(rec[1].toString())));
				  commissionStructure.setSalesBa(BigDecimal.valueOf(Double.parseDouble(rec[2].toString())));
				  commissionStructure.setBackOffice(BigDecimal.valueOf(Double.parseDouble(rec[3].toString())));
				  commissionStructure.setForwarding(BigDecimal.valueOf(Double.parseDouble(rec[4].toString())));
				  if((commissionStructure.getGrossMarginFrom().doubleValue()!=0)||(commissionStructure.getGrossMarginTo().doubleValue()!=0)){
				  commissionStructureManager.save(commissionStructure);
				  }
			  }
		} catch (Exception e) {
			e.printStackTrace();
		}}
		return SUCCESS;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public CommissionStructure getCommissionStructure() {
		return commissionStructure;
	}
	public void setCommissionStructure(CommissionStructure commissionStructure) {
		this.commissionStructure = commissionStructure;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public List getCommissionStructureList() {
		return commissionStructureList;
	}
	public void setCommissionStructureList(List commissionStructureList) {
		this.commissionStructureList = commissionStructureList;
	}
	public void setCommissionStructureManager(
			CommissionStructureManager commissionStructureManager) {
		this.commissionStructureManager = commissionStructureManager;
	}
	public List getContractList() {
		return contractList;
	}
	public void setContractList(List contractList) {
		this.contractList = contractList;
	}
	public void setDefaultAccountLineManager(
			DefaultAccountLineManager defaultAccountLineManager) {
		this.defaultAccountLineManager = defaultAccountLineManager;
	}
	public String getsContarct() {
		return sContarct;
	}
	public void setsContarct(String sContarct) {
		this.sContarct = sContarct;
	}
	public String getsChargeCode() {
		return sChargeCode;
	}
	public void setsChargeCode(String sChargeCode) {
		this.sChargeCode = sChargeCode;
	}
}
