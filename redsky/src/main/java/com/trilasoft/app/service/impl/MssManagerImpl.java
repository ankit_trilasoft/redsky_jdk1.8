package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.MssDao;
import com.trilasoft.app.model.Mss;
import com.trilasoft.app.service.MssManager;

public class MssManagerImpl extends GenericManagerImpl<Mss, Long> implements MssManager{
	MssDao mssDao;
	public MssManagerImpl(MssDao mssDao) {
		super(mssDao); 
        this.mssDao = mssDao; 
	}
	
	public List findListByShipNumber(String shipNumber,String sessionCorpID){
		return mssDao.findListByShipNumber(shipNumber,sessionCorpID);
	}
	public List checkById(Long wid){
		return mssDao.checkById(wid);
	}
	public List findRecord(Long id,String sessionCorpID){
		return mssDao.findRecord(id,sessionCorpID);
	}
	public Map<String, String> findParentRecord(String sessionCorpID,String parameter){
		return mssDao.findParentRecord(sessionCorpID,parameter);
	}
	public Map<String, String> findChildRecord(String sessionCorpID,String parameter,String parentCode){
		return mssDao.findChildRecord(sessionCorpID,parameter,parentCode);
	}
	public List findAllOriServiceRecord(Long id,String sessionCorpID){
		return mssDao.findAllOriServiceRecord(id,sessionCorpID);
	}
	public List findAllDestinationServiceList(Long id,String sessionCorpID){
		return mssDao.findAllDestinationServiceList(id,sessionCorpID);
	}
	public Map<String, String> findDestinationParentRecord(String sessionCorpID,String parameter){
		return mssDao.findDestinationParentRecord(sessionCorpID,parameter);
	}
	public Map<String, String> findDestinationChildRecord(String sessionCorpID,String parameter,String parentCode){
		return mssDao.findDestinationChildRecord(sessionCorpID,parameter,parentCode);
	}
	public List findValuesForQuotePDF(Long id,String TransportTypeRadio,String PurchaseOrderNumber,Long workTicketId, String sessionCorpID){
		return mssDao.findValuesForQuotePDF( id, TransportTypeRadio, PurchaseOrderNumber, workTicketId, sessionCorpID);
	}
	public List findCratingValue(Long id,String sessionCorpID){
		return mssDao.findCratingValue(id,sessionCorpID);
	}
	public List findOriginValue(Long id,String sessionCorpID){
		return mssDao.findOriginValue(id,sessionCorpID);
	}
	public List findDestinationValue(Long id,String sessionCorpID){
		return mssDao.findDestinationValue(id,sessionCorpID);
	}
	public List getMssNotes(Long id,String PurchaseOrderNumber,String noteType,String sessionCorpID){
		return mssDao.getMssNotes( id, PurchaseOrderNumber, noteType, sessionCorpID);
	}
	public List getOriginPhone(Long workTicketId,String PurchaseOrderNumber,String sessionCorpID){
		return mssDao.getOriginPhone( workTicketId, PurchaseOrderNumber, sessionCorpID);
	}
	public List getdestinationPhone(Long workTicketId,String PurchaseOrderNumber,String sessionCorpID){
		return mssDao.getdestinationPhone(workTicketId, PurchaseOrderNumber, sessionCorpID);
	}
	public void updateplaceOrderNumber(Long id, String soNumber,String workTicketNumber,String placeOrderNumber,String sessionCorpID){
		mssDao.updateplaceOrderNumber( id, soNumber, workTicketNumber,placeOrderNumber, sessionCorpID);
	}
	public void updatefilePath(Long id, String soNumber,String workTicketNumber,String filePath,String sessionCorpID){
		mssDao.updatefilePath( id, soNumber, workTicketNumber,filePath, sessionCorpID);
	}
	public void updateRefmaster(String submitAs,String userName,String sessionCorpID){
		mssDao.updateRefmaster(submitAs,userName,sessionCorpID);
	}
	public List getdefaultSubmitAs(String userName,String sessionCorpID){
		return mssDao.getdefaultSubmitAs(userName,sessionCorpID);
	}
	public List getMsscustomerID(String companyDivision,String sessionCorpID){
		return mssDao.getMsscustomerID(companyDivision,sessionCorpID);
	}
	public List getSoDetails(String PurchaseOrderNumber,String sessionCorpID){
		return mssDao.getSoDetails(PurchaseOrderNumber,sessionCorpID);
	}
}
