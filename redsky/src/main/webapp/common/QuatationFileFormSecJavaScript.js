<script>
function getJobList(){
	var comDiv = "";
	if(document.forms['customerFileForm'].elements['customerFile.companyDivision'].value != null){
		comDiv = document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
	}
  	var url = "findJobs.html?ajax=1&decorator=simple&popup=true&compDivision="+encodeURI(comDiv);
   	httpJob.open("GET", url, true);
   	httpJob.onreadystatechange = handleHttpResponseJob;
   	httpJob.send(null);
}

function handleHttpResponseJob(){
	if (httpJob.readyState == 4){
    	var results = httpJob.responseText
        results = results.trim();
        res = results.split("@");
        targetElementValue=document.forms['customerFileForm'].elements['customerFile.job'].value;
        targetElement = document.forms['customerFileForm'].elements['customerFile.job'];
        targetElement.length = res.length;
		for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.job'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.job'].options[i].value = '';
			}else{
				stateVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.job'].options[i].text = stateVal[1];
				document.forms['customerFileForm'].elements['customerFile.job'].options[i].value = stateVal[0];
			}
		}
		document.getElementById("jobQuote").value = '${customerFile.job}';
		if(document.getElementById("reset").value!='reset'){
			document.forms['customerFileForm'].elements['customerFile.job'].value=targetElementValue;
			}else{
			document.getElementById("reset").value='';
			}
	}
} 
var httpJob=getHTTPObject7();
function getHTTPObject7(){
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
} 
var httpPer=getHTTPObject();
var httpVoxme = getHTTPObject();
function checkForBlank(){
var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
 var compDivision=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
	  if(billToCode==''){
  
   	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
   	var url="pricingBillingPaybaleNameByJob.html?ajax=1&decorator=simple&popup=true&jobType="+encodeURI(job) +"&compDivision="+encodeURI(compDivision);
   	httpPer.open("GET", url, true);
   httpPer.onreadystatechange=httppricingBillingPaybaleByJob;
   httpPer.send(null);
   }
}

function httppricingBillingPaybaleByJob(){
if (httpPer.readyState == 4){
               var results = httpPer.responseText
               results = results.trim();
               res = results.split("#");
               for(i=0;i<res.length;i++){
               	if(res[0]!= null && res[0]!='' && res[0]!= 'undefined'){
	               		 document.forms['customerFileForm'].elements['customerFile.personPricing'].value=res[0];
               	}
               	if(res[1]!= null && res[1]!='' && res[1]!= 'undefined'){
	               		document.forms['customerFileForm'].elements['customerFile.personBilling'].value=res[1];
               	}
               	if(res[2]!= null && res[2]!='' && res[2]!= 'undefined'){
	            		 document.forms['customerFileForm'].elements['customerFile.personPayable'].value=res[2];
               	}
                   if(res[3]!= null && res[3]!='' && res[3]!= 'undefined'){
	               		document.forms['customerFileForm'].elements['customerFile.auditor'].value=res[3];
                   }
               }
                }
}
function scheduleSurvey()
           {
           var jobTypeSchedule =document.forms['customerFileForm'].elements['customerFile.job'].value;
           var addressLine1=document.forms['customerFileForm'].elements['customerFile.originAddress1'].value;
           var addressLine2=document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
          	addressLine1= addressLine1.replace('#','');
          	addressLine2= addressLine2.replace('#','');
          	if(addressLine2!=''){
          	addressLine1=addressLine1+', '+addressLine2;}
           var city=document.forms['customerFileForm'].elements['customerFile.originCity'].value;
           var country=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
           var zipCode=document.forms['customerFileForm'].elements['customerFile.originZip'].value;
           var state = document.forms['customerFileForm'].elements['customerFile.originState'].value;
           var survey=document.forms['customerFileForm'].elements['customerFile.survey'].value;
           var targetAddress=addressLine1+', '+city+', '+state+', '+zipCode+', '+country;
           var surveyTime="";
	       var surveyTime2="";
	       var estimated="";
           if(jobTypeSchedule=='' || addressLine1=='' || city =='' || country=='' || zipCode=='')
           { 
           alert("Please select job type, address line1, country, city, postal code in origin section.");
           }
           else if(survey !='')
           {
           var agree= confirm("WARNING: Survey already scheduled, do you want to reschedule? Hit OK to reschedule and Cancel to keep current date.");
           if(agree){
           window.open("sendSurveySchedule.html?jobTypeSchedule="+jobTypeSchedule+"&targetAddress="+targetAddress+"&surveyTime="+surveyTime+"&surveyTime2="+surveyTime2+"&estimated="+estimated+"&decorator=popup&popup=true","forms","height=600,width=950,top=1, left=200, scrollbars=yes,resizable=yes");
           }           
           }
           else{
           window.open("sendSurveySchedule.html?jobTypeSchedule="+jobTypeSchedule+"&targetAddress="+targetAddress+"&surveyTime="+surveyTime+"&surveyTime2="+surveyTime2+"&estimated="+estimated+"&decorator=popup&popup=true","forms","height=600,width=950,top=1, left=200, scrollbars=yes,resizable=yes");
           }
           }
      function comptetiveSurveyEmail(val) {
          var comp = document.forms['customerFileForm'].elements['customerFile.comptetive'].value;
		if(comp=="Y"){
		document.forms['customerFileForm'].Button1.disabled=false;
		} else {
		document.forms['customerFileForm'].Button1.disabled=true;
		}
        }     
      function sendSurveyEmail(){
      	if(document.forms['customerFileForm'].elements['customerFile.email'].value !=''){
      		var emailSurveyFlag=false;
    		<configByCorp:fieldVisibility componentId="component.customerfile.field.emailflagForSSCW">
    			emailSurveyFlag = true;
    		</configByCorp:fieldVisibility>
    		    if (validate_email(document.forms['customerFileForm'].elements['customerFile.email'])== true) {
             var estimator = document.forms['customerFileForm'].elements['customerFile.estimator'].value;
             var survey = document.forms['customerFileForm'].elements['customerFile.survey'].value;
             var surveyGerman = document.forms['customerFileForm'].elements['customerFile.survey'].value;
             var surveyDutch = document.forms['customerFileForm'].elements['customerFile.survey'].value;
             var surveyBourEnglish = document.forms['customerFileForm'].elements['customerFile.survey'].value;
             var surveyTime = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
             var surveyGermanTime = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
             var surveyDutchTime = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
             var surveyTime2 = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
             var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
             var hour = surveyTime.substring(0, surveyTime.indexOf(":"));
             var hour2 = surveyTime2.substring(0, surveyTime.indexOf(":"));
             var min = surveyTime.substring(surveyTime.indexOf(":")+1, surveyTime.length);
             var min2 = surveyTime2.substring(surveyTime2.indexOf(":")+1, surveyTime2.length);
             if (hour<12){
             		surveyTime=hour+":"+min+"AM";
             } else if ((surveyTime.substring(0, surveyTime.indexOf(":")))>12) {
                 surveyTime=(hour-12)+":"+min+"PM";
             }else{
            	//Putting if block for #7598(14 Feb 2013)
            	 var emaitime=false;
          	   <configByCorp:fieldVisibility componentId="component.tab.customerFile.surveyTime">        	  
          	   emaitime=true;        	   
          	   </configByCorp:fieldVisibility>
          	   if(emaitime){
          		   if(hour==12){
                    	  hour=0;                    	  
                      }  
          	   }
            	 surveyTime=hour+":"+min+"PM";
            	 //alert(surveyTime);
             }
              if (hour2<12){
             		surveyTime2=hour2+":"+min2+"AM";
             } else if (hour2>12) {
                 surveyTime2=(hour2-12)+":"+min2+"PM";
             }else{
            	//Putting if block for #7598(14 Feb 2013)
            	 var emaitime1=false;
          	   <configByCorp:fieldVisibility componentId="component.tab.customerFile.surveyTime">        	  
          	   emaitime1=true;
          	   </configByCorp:fieldVisibility>
          	   if(emaitime1){
          	   if(hour2==12){
                	  hour2=0;              	
                  }
          	   }
                 surveyTime2=hour2+":"+min2+"PM";
                // alert(surveyTime2);
             }
             if(estimator=='' || survey=='' || surveyTime =='00:00AM' || surveyTime2=='00:00AM' || surveyTime =='' || surveyTime2=='') { 
             alert("Survey is not yet Scheduled - Cannot send Survey Email to the customer");
             } else if (survey !='' && surveyTime!='00:00AM' && surveyTime2 !='00:00AM' && estimator !='' && surveyTime2 !='' && surveyTime2 !=''){
  	        var systemDate = new Date();
  			var mySplitResult = survey.split("-");
  		   	var day = mySplitResult[0];
  		   	var month = mySplitResult[1];
  			var month13 = mySplitResult[1];
  		   	var year = mySplitResult[2];
  		 	if(month == 'Jan'){
  		       month = "01";
  		   	}else if(month == 'Feb'){
  		       month = "02";
  		   	}else if(month == 'Mar'){
  		       month = "03"
  		   	}else if(month == 'Apr'){
  		       month = "04"
  		   	}else if(month == 'May'){
  		       month = "05"
  		   	}else if(month == 'Jun'){
  		       month = "06"
  		   	}else if(month == 'Jul'){
  		       month = "07"
  		   	}else if(month == 'Aug'){
  		       month = "08"
  		   	}else if(month == 'Sep'){
  		       month = "09"
  		   	}else if(month == 'Oct'){
  		       month = "10"
  		   	}else if(month == 'Nov'){
  		       month = "11"
  		   	}else if(month == 'Dec'){
  		       month = "12";
  		   	}
  		   	var weekday=new Array(7);
  			weekday[0]="Sunday";
  			weekday[1]="Monday";
  			weekday[2]="Tuesday";
  			weekday[3]="Wednesday";
  			weekday[4]="Thursday";
  			weekday[5]="Friday";
  			weekday[6]="Saturday";
  			var finalDate = month+"/"+day+"/"+year;
  			var finalDate11 = month13+"/"+day+"/"+year;
  		   	survey = finalDate.split("/");
  		   	var enterDate = new Date(survey[0]+"/"+survey[1]+"/20"+survey[2]);
  		  	var sendDate = weekday[enterDate.getDay()]+" "+finalDate;
  		  	
  		  var sentMonthDate= weekday[enterDate.getDay()]+" "+finalDate11;
  		  	
  		  	var weekDutchday=new Array(7);
  		  	weekDutchday[0]="zondag";
  		  	weekDutchday[1]="maandag";
  		  	weekDutchday[2]="dinsdag";
  		  	weekDutchday[3]="woensdag";
  		  	weekDutchday[4]="donderdag";
  		  	weekDutchday[5]="vrijdag";
  		  	weekDutchday[6]="zaterdag";
  		  	surveyDutch=surveyDutch.replace("-"," ");
  		  	surveyDutch=surveyDutch.replace("-"," ");
  		  	var sendDutchDate = weekDutchday[enterDate.getDay()]+" "+surveyDutch;  	

		  	
			// For German mail
		  	var mySplitGermanResult = surveyGerman.split("-");
		   	var dayGerman = mySplitGermanResult[0];
		   	var monthGerman = mySplitGermanResult[1];
		   	var yearGerman = mySplitGermanResult[2];
		  	if(monthGerman == 'Jan'){ monthGerman = "Januar";
			}else if(monthGerman == 'Feb'){ monthGerman = "Februar";
			}else if(monthGerman == 'Mar'){ monthGerman = "M�rz"
			}else if(monthGerman == 'Apr'){ monthGerman = "April"
			}else if(monthGerman == 'May'){ monthGerman = "Mai"
			}else if(monthGerman == 'Jun'){ monthGerman = "Juni"
			}else if(monthGerman == 'Jul'){ monthGerman = "Juli"
			}else if(monthGerman == 'Aug'){ monthGerman = "August"
			}else if(monthGerman == 'Sep'){ monthGerman = "September"
			}else if(monthGerman == 'Oct'){ monthGerman = "Oktober"
			}else if(monthGerman == 'Nov'){ monthGerman = "November"
			}else if(monthGerman == 'Dec'){ monthGerman = "Dezember";
			}
		  	var weekGermanday=new Array(7);
		  	weekGermanday[0]="Montag";
		  	weekGermanday[1]="Dienstag";
		  	weekGermanday[2]="Mittwoch";
		  	weekGermanday[3]="Donnerstag";
		  	weekGermanday[4]="Freitag";
		  	weekGermanday[5]="Samstag";
		  	weekGermanday[6]="Sonntag";
		  	surveyGerman=surveyGerman.replace("-"," ");
		  	surveyGerman=surveyGerman.replace("-"," ");		  	
		  	var finalDate1 = dayGerman+"."+month+".20"+yearGerman; 
		  	var sendGermanDate = weekGermanday[enterDate.getDay()]+", "+finalDate1+"/ "+surveyGermanTime+" Uhr";  
  		  	
  		  	
  		  // For BOUR English mail 
		  	var mySplitResult11 = surveyBourEnglish.split("-");
		   	var day11 = mySplitResult11[0];
		   	var month11 = mySplitResult11[1];
		   	var year11 = mySplitResult11[2];
		 	if(month11 == 'Jan'){  month11 = "January";
		   	}else if(month11 == 'Feb'){ month11 = "February";
		   	}else if(month11 == 'Mar'){ month11 = "March"
		   	}else if(month11 == 'Apr'){ month11 = "April"
		   	}else if(month11 == 'May'){ month11 = "May"
		   	}else if(month11 == 'Jun'){ month11 = "June"
		   	}else if(month11 == 'Jul'){ month11 = "July"
		   	}else if(month11 == 'Aug'){ month = "August"
		   	}else if(month11 == 'Sep'){ month11 = "September"
		   	}else if(month11 == 'Oct'){ month11 = "October"
		   	}else if(month11 == 'Nov'){ month11 = "November"
		   	}else if(month11 == 'Dec'){ month11 = "December";
		   	}
		   	var dayth =day11;
		   	var weekday11=new Array(7);
			weekday11[0]="Sunday";
			weekday11[1]="Monday";
			weekday11[2]="Tuesday";
			weekday11[3]="Wednesday";
			weekday11[4]="Thursday";
			weekday11[5]="Friday";
			weekday11[6]="Saturday";
			if(day11==11||day11==12||day11==13){
				day11=day11+"th";
			}else{
				var rem=day11%10;
			switch(rem){
			case 1:
				day11=day11+"st";
			    break;
			case 2:
				day11=day11+"nd";
				break;
			case 3:
				day11=day11+"rd";
				break;
             default :
            	 day11=day11+"th";			
			}
			}
		    var finalDate111 = month11+" "+day11+" 20"+year11;
			var finalDate11 = month11+"/"+dayth+"/"+year11;
			surveyBourEnglish = finalDate11.split("/");
		   	var enterDate11 = new Date(surveyBourEnglish[0]+"/"+surveyBourEnglish[1]+"/20"+surveyBourEnglish[2]);
		  	var sendDate11 = weekday[enterDate11.getDay()]+", "+finalDate111; 
  		  	
  		  	var newsystemDate = new Date(systemDate.getMonth()+1+"/"+systemDate.getDate()+"/"+systemDate.getFullYear());
  		  	var daysApart = Math.round((enterDate-newsystemDate)/86400000);
  		  	if(daysApart < 0){
  		    	alert("The Survey Schedule date is past the current date-The Survey is already over or Please Schedule a new survey.");
  		    	return false;
  		  	}
  		  	if(survey != ''){
  	  		if(daysApart == 0){
  	  			var time = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
  				var hour = time.substring(0, time.indexOf(":"))
  				var min = time.substring(time.indexOf(":")+1, time.length);
  	  			if (hour < 0  || hour > 23) {
  					document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
  					return false;
  				}
  				if (min<0 || min > 59) {
  					document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
  					return false;
  				}
  				if(systemDate.getHours() > hour){
  					alert("The Survey Schedule time is past the current time-The Survey is already over or Please Schedule a new survey.");
  					return false;
  				} else if(systemDate.getHours() == hour && systemDate.getMinutes() > min){
  					alert("The Survey Schedule time is past the current time-The Survey is already over or Please Schedule a new survey.");
  					return false;
  				   }				
  			  }
  	  	   }
  		    try{
  		    var checkBourEnglish = document.forms['customerFileForm'].elements['checkBourEnglish'].checked;  
  		    }catch(e){} 
		    var checkGerman=false;
		    try{
	            checkGerman = document.forms['customerFileForm'].elements['checkGerman'].checked;
			    }catch(e){}   		    
  		    try{
  		  	var checkEnglish = document.forms['customerFileForm'].elements['checkEnglish'].checked;
              var checkDutch = document.forms['customerFileForm'].elements['checkDutch'].checked;
  		    }catch(e){}       
  	  	    var agree= confirm("Press OK to Send Survey Email, else press Cancel.");	  	   
            if(agree){
            var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
            var email2 = document.forms['customerFileForm'].elements['customerFile.email2'].value;
            //email = email+","+email2;
            if(email !='' && email2 !='' ){
				 email = email+";"+email2;
			}else if(email2 !=''){
				 email =email2;
			}else{
				email =email;
			}
  			var firstName = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
  			var lastName = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
  			var surveyDate = document.forms['customerFileForm'].elements['customerFile.survey'].value; 
  			var updatedBy = document.forms['customerFileForm'].elements['customerFile.updatedBy'].value;
  			var subject = "Confirmation for "+firstName+"'s survey on "+surveyDate;
  			if(checkEnglish==true || checkDutch==true || checkGerman==true){
				window.open('sendSurveyEmail.html?id=${customerFile.id}&emailTo='+email+'&estimator='+estimator+'&surveyDates='+sendDate+'&surveyMonths='+sentMonthDate+'&firstName='+firstName+'&lastName='+lastName+'&surveyTime='+surveyTime+'&surveyDutchTime='+surveyDutchTime+'&sendDutchDate='+sendDutchDate+'&surveyGermanTime='+surveyGermanTime+'&sendGermanDate='+sendGermanDate+'&surveyTime2='+surveyTime2+'&emailSurveyFlag='+emailSurveyFlag+'&checkEnglish='+checkEnglish+'&checkDutch='+checkDutch+'&checkGerman='+checkGerman+'&decorator=popup&popup=true&from=file','','width=550,height=170') ;
			}else if(checkBourEnglish == true){ 
				var url="sendSurveyClientEmail.html?ajax=1&decorator=simple&popup=true&id=${customerFile.id}&estimator="+estimator+'&checkBourEnglish='+checkBourEnglish+'&surveyDates='+sendDate11+'&firstName='+firstName+'&lastName='+lastName+'&surveyTime='+surveyTime+'&surveyTime2='+surveyTime2;
				http221.open("GET", url, true);
		        http221.onreadystatechange = function(){handleHttpResponseSentMail(email,sendDate11,firstName,lastName);};
		        http221.send(null);	
			}else{
				alert("Please select Language!");
				}
  			  } else {             
  	  	    }
  	  	   
              } else {
              alert("Survey is not yet Scheduled - Cannot send Survey Email to the customer");
              } 
              
              } else {
               alert("Invalid Origin's Primary Email Id");
              } 
              } else {
              alert("Primary Email is not yet entered - Cannot send Survey Email to the customer");
              }
              }
      function handleHttpResponseSentMail(emailTo,surveyDates,firstName, lastName){
    	  var checkBourEnglish = document.forms['customerFileForm'].elements['checkBourEnglish'].checked; 
          if (http221.readyState == 4){
        	var textMessage ="";
    	    var results= http221.responseText
    	    results = results.trim();     
    	    if(results!=''){ textMessage = results;}else{textMessage = '';}	      
          	if(checkBourEnglish==true){
    			var msgText1=textMessage;
    	        var subject = "Confirmation for "+firstName+" "+lastName+"'s survey on "+surveyDates;
    			var mailto_link = 'mailto:'+encodeURIComponent(emailTo)+'?subject='+encodeURIComponent(subject)+'&body='+encodeURIComponent(msgText1);		
    			win = window.open(mailto_link,'emailWindow'); 
    			if (win && win.open &&!win.closed) win.close(); 
    		}		
         }
      }
      function getHTTPObject221() {
    	  var xmlhttp;
    	    if(window.XMLHttpRequest)
    	    {
    	        xmlhttp = new XMLHttpRequest();
    	    }
    	    else if (window.ActiveXObject)
    	    {
    	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    	        if (!xmlhttp)
    	        {
    	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    	        }
    	    }
    	    return xmlhttp;
      }
      var http221 = getHTTPObject221();
function findCityStateNotPrimary(targetField, position){
     if(targetField=='OZ'){
	 var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
	 var zipCode=document.forms['customerFileForm'].elements['customerFile.originZip'].value;
	 } else if (targetField=='DZ'){
	 var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
	 var zipCode=document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
	 }
	 if(zipCode!=''){
	 var url="findCityStateNotPrimary.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode)+"&zipType="+encodeURI(targetField)+"&countryForZipCode="+encodeURI(countryCode);
     ajax_showTooltip(url,position);	
 }	}  
 
      function findCityState1(targetElement,targetField){
	 var zipCode = targetElement.value;
	 if(targetField=='OZ'){
     var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
     if (countryCode!='USA'){
     document.getElementById('zipCodeList').style.display = 'none';
     } else if (zipCode =='' || zipCode == undefined){
     document.getElementById('zipCodeList').style.display = 'none';
     } else if (countryCode=='USA' && (zipCode =='' || zipCode == undefined)){
      document.getElementById('zipCodeList').style.display = 'none';
     }   } else if (targetField=='DZ'){
     var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
     if (countryCode!='USA'){
     document.getElementById('zipDestCodeList').style.display = 'none';
     } else if (zipCode =='' || zipCode == undefined){
      document.getElementById('zipDestCodeList').style.display = 'none';
     } else if (countryCode=='USA' && (zipCode =='' || zipCode == undefined)){
      document.getElementById('zipDestCodeList').style.display = 'none';
     } }
     
      var url1="findCityStateFlex.html?ajax=1&decorator=simple&popup=true&countryCodeFlex="+ encodeURI(countryCode);
     http99.open("GET", url1, true);
     http99.onreadystatechange = function(){ handleHttpResponseCityStateFlex(targetElement,targetField);};
     http99.send(null);
       }
     function handleHttpResponseCityStateFlex(targetElement,targetField){
              if (http99.readyState == 4){
                var results = http99.responseText
                results = results.trim();
                if(results.length>0)
                    {
                     if(results=='Y' ){
                          findCityState(targetElement,targetField);
                     }
                   if(results=='N'){                 
                    }
                    } 
                    else{
                    
                    } 
                                               
                   }
                   }
     
     function findCityState(targetElement,targetField){
	 var zipCode = targetElement.value;
	 if(targetField=='OZ'){
     var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
     if (countryCode!='USA'){
     document.getElementById('zipCodeList').style.display = 'none';
     } else if (zipCode =='' || zipCode == undefined){
     document.getElementById('zipCodeList').style.display = 'none';
     } else if (countryCode=='USA' && (zipCode =='' || zipCode == undefined)){
      document.getElementById('zipCodeList').style.display = 'none';
     }   } else if (targetField=='DZ'){
     var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
     if (countryCode!='USA'){
     document.getElementById('zipDestCodeList').style.display = 'none';
     } else if (zipCode =='' || zipCode == undefined){
      document.getElementById('zipDestCodeList').style.display = 'none';
     } else if (countryCode=='USA' && (zipCode =='' || zipCode == undefined)){
      document.getElementById('zipDestCodeList').style.display = 'none';
     } }
      var url="findCityState.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode);
     http33.open("GET", url, true);
     http33.onreadystatechange = function(){ handleHttpResponseCityState(targetField);};
     http33.send(null);
      }
      
 function findCityStateOfZipCode(targetElement,targetField){
      var zipCode = targetElement.value;
     
      if(targetField=='OZ'){
       var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
        if(document.forms['customerFileForm'].elements['originCountryFlex3Value'].value=='Y'){
         var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
          http33.open("GET", url, true);
          http33.onreadystatechange = function(){ handleHttpResponseCityState('OZ');};
          http33.send(null);
        }else{
        
         }
       }else if(targetField=='DZ'){
        var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
          if( document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value=='Y'){
           var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
              http33.open("GET", url, true);
              http33.onreadystatechange = function(){ handleHttpResponseCityState('DZ');};
              http33.send(null);
             }else{
        
             }
      
        }  
      }     
 function handleHttpResponseCityState(targetField){
             if (http33.readyState == 4){
                var results = http33.responseText
                results = results.trim();
                var resu = results.replace("[",'');
                resu = resu.replace("]",'');
                var resuL = resu.split(",");
				for(var i = 0; i < resuL.length+1; i++) {
				var res = resuL[i];
                var resL = res.split("#");
                if(targetField=='OZ'){
	           	if(resL[3]=='P') {	            
	           	 document.forms['customerFileForm'].elements['customerFile.originCity'].value=resL[0];
	           	 document.forms['customerFileForm'].elements['customerFile.originState'].value=resL[2];
	             document.getElementById('zipCodeList').style.display = 'none';
	             } else if(resL[3]!='P') {
	             document.getElementById('zipCodeList').style.display = 'block';
	           	 } 
	           	 document.forms['customerFileForm'].elements['customerFile.originCity'].focus();
	           	 } 	else if (targetField=='DZ'){
	           	if(resL[3]=='P') {
	           	 document.forms['customerFileForm'].elements['customerFile.destinationCity'].value=resL[0];
	           	 document.forms['customerFileForm'].elements['customerFile.destinationState'].value=resL[2];
	             document.getElementById('zipDestCodeList').style.display = 'none';
	           	 }	else if(resL[3]!='P') {
	           	document.getElementById('zipDestCodeList').style.display = 'block';
	           	}
	           	document.forms['customerFileForm'].elements['customerFile.destinationCity'].focus();
	           	} } } else { } }  
	           	
 
function goToUrlZip(id,targetField){
if(targetField=='OZ'){
	document.forms['customerFileForm'].elements['customerFile.originCity'].value = id;
	document.forms['customerFileForm'].elements['customerFile.originCity'].focus();
	 } else if (targetField=='DZ'){
	document.forms['customerFileForm'].elements['customerFile.destinationCity'].value = id;
	document.forms['customerFileForm'].elements['customerFile.destinationCity'].focus();
	} }
function PricingContract(){ 
if(document.forms['customerFileForm'].elements['customerFile.contract'].value =='' && document.forms['customerFileForm'].elements['defaultcontract'].value ==''){
alert("For pricing quotations, please enter Pricing Contract in the billing section. (Please note, you can also set a default value in My Account - Configuration Defaults.)")
}

}

function checkQuotation(){ 
var bookCode=document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
 var  quotationStatus=document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value ;
if(bookCode!=''&& quotationStatus=='Accepted'){
	var url="findBookAgentCodeStatus.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode);
	http45.open("GET", url, true);
	http45.onreadystatechange = handleHttpResponse45;
	http45.send(null);	
}
}
function handleHttpResponse45(){
	if (http45.readyState == 4) {
          var results = http45.responseText
          results = results.trim();
          var res = results.split("@"); 
       		if(res.size() >= 2){
       			if(res[2] == 'Approved'){ 
       				var billToCode=document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
       			 var comp = document.forms['customerFileForm'].elements['customerFile.comptetive'].value;
       			
       				//checkQuotationBillToCode();
       				if(comp=='Y'){
	   					if(billToCode.trim()==''){
	   						checkQuotesBaCheck();
	   					}else{
	   						checkQuotationBillToCode();
	   					}
       				}
       				if(comp!='Y'){
       					checkQuotationBillToCode();
       				}
           		}else{
           			alert("Quote cannot be accepted as selected partners have not been approved yet");	
           			document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value='${customerFile.quotationStatus}';
           		}			
      		}else{
           		alert("Booking Agent code not valid");
	   		}
     
  }
} 
function checkQuotationBillToCode(){
	var billToCode=document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
		var url="findBookAgentCodeStatus.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(billToCode);
		http46.open("GET", url, true);
		http46.onreadystatechange = handleHttpResponse46;
		http46.send(null);	
	}
function handleHttpResponse46(){
	if (http46.readyState == 4) {
          var results = http46.responseText
          results = results.trim();
          var res = results.split("@"); 
       		if(res.size() >= 2){ 
       			if(res[2] == 'Approved'){
       				checkQuotesBaCheck();
           		}else{
           			alert("Quote cannot be accepted as selected partners have not been approved yet");	
           			document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value='${customerFile.quotationStatus}';
           		}			
      		}else{
           		alert("Bill To code not valid");
	   		}
  }
}
function checkQuotesBaCheck(){
	var id='${customerFile.id}';
	var url="findQuotesBookAgentStatus.html?ajax=1&decorator=simple&popup=true&id="+encodeURI(id);
	http49.open("GET", url, true);
	http49.onreadystatechange = handleHttpResponse49;
	http49.send(null);
} 
function handleHttpResponse49(){
	if (http49.readyState == 4) {
          var results = http49.responseText
          results = results.trim();
          //alert(results);
          //var res = results.split("@"); 
       		if(results!=''){ 
           			alert(results);
           			document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value='${customerFile.quotationStatus}';
           				
      		}else{
      		if('${QuotesNumber}'>1 && document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value =='Accepted'){
           	openWindow('quotationServiceOrdersStatus.html?id=${customerFile.id}&forQuotation=QC&decorator=popup&popup=true',900,300);
           	}
	   		}
     
  }
}
</script>

<script type="text/javascript">  
function ostateMandatory(){

    var originCountry=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
    var reqTrue=document.getElementById("originStateRequiredTrue");
    var reqFalse=document.getElementById("originStateRequiredFalse");	   
    if(originCountry=='United States' || originCountry=='India'){
	    reqTrue.style.display='block';
	    reqFalse.style.display='none';
    }else{
	    reqFalse.style.display='block';
	    reqTrue.style.display='none';
    }
}


function dstateMandatory(){

    var destinationCountry=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
    var reqTrue=document.getElementById("destinationStateRequiredTrue");
    var reqFalse=document.getElementById("destinationStateRequiredFalse");
    if(destinationCountry=='United States' || destinationCountry=='India'){
	    reqTrue.style.display='block';
	    reqFalse.style.display='none';
    }else{
	    reqFalse.style.display='block';
	    reqTrue.style.display='none';
    }
}

function quotesToGoMethod(){
       var fname=document.forms['customerFileForm'].elements['customerFile.firstName'].value;
       var lastname=document.forms['customerFileForm'].elements['customerFile.lastName'].value;
       var originDayPhone=document.forms['customerFileForm'].elements['customerFile.originDayPhone'].value;
       var originHomePhone=document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
       var originMobile=document.forms['customerFileForm'].elements['customerFile.originMobile'].value;

        if(fname!='' && lastname!='' && (originDayPhone!='' || originHomePhone!='' || originMobile!=''))
       {
        	var url = "saveQuotationFileToGoMethod.html";
        	document.forms['customerFileForm'].action = url;
           document.forms['customerFileForm'].submit();
       }
       else
       {
           alert("Please enter the missing information for: \nFirst Name\nLast Name\nWork Phone or Home Phone or Mobile Phone");
       }          
       
}

function sendMailFromVoxme(){         
		var estimator = document.forms['customerFileForm'].elements['customerFile.estimator'].value;
       var survey = document.forms['customerFileForm'].elements['customerFile.survey'].value;
       var surveyDutch = document.forms['customerFileForm'].elements['customerFile.survey'].value;
       var surveyTime = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
       var sendsurveyTime = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
       var surveyDutchTime = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
       var surveyTime2 = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
       var sendsurveyTime2 = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
       var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
       var voxmeInvTag = 'No';
       <configByCorp:fieldVisibility componentId="component.customerFile.voxme.invTag">
       		voxmeInvTag = 'Yes';
       </configByCorp:fieldVisibility>
       var hour = surveyTime.substring(0, surveyTime.indexOf(":"));
       var hour2 = surveyTime2.substring(0, surveyTime.indexOf(":"));
       var min = surveyTime.substring(surveyTime.indexOf(":")+1, surveyTime.length);
       var min2 = surveyTime2.substring(surveyTime2.indexOf(":")+1, surveyTime2.length);
      if (hour<12){
       		surveyTime=hour+":"+min+"AM";
       } else if ((surveyTime.substring(0, surveyTime.indexOf(":")))>12) {
           surveyTime=(hour-12)+":"+min+"PM";
       }else{
           surveyTime=hour+":"+min+"PM";
       }
        if (hour2<12){
       		surveyTime2=hour2+":"+min2+"AM";
       } else if (hour2>12) {
           surveyTime2=(hour2-12)+":"+min2+"PM";
       }else{
           surveyTime2=hour2+":"+min2+"PM";
       }
       if(estimator=='' || survey=='' || surveyTime =='00:00AM' || surveyTime2=='00:00AM' || surveyTime =='' || surveyTime2=='') { 
       alert("Survey is not yet Scheduled - Cannot send Survey Email");
       } else if (survey !='' && surveyTime!='00:00AM' && surveyTime2 !='00:00AM' && estimator !='' && surveyTime2 !='' && surveyTime2 !=''){
        var systemDate = new Date();
		var mySplitResult = survey.split("-");
	   	var day = mySplitResult[0];
	   	var month = mySplitResult[1];
	   	var year = mySplitResult[2];
	 	if(month == 'Jan'){
	       month = "01";
	   	}else if(month == 'Feb'){
	       month = "02";
	   	}else if(month == 'Mar'){
	       month = "03"
	   	}else if(month == 'Apr'){
	       month = "04"
	   	}else if(month == 'May'){
	       month = "05"
	   	}else if(month == 'Jun'){
	       month = "06"
	   	}else if(month == 'Jul'){
	       month = "07"
	   	}else if(month == 'Aug'){
	       month = "08"
	   	}else if(month == 'Sep'){
	       month = "09"
	   	}else if(month == 'Oct'){
	       month = "10"
	   	}else if(month == 'Nov'){
	       month = "11"
	   	}else if(month == 'Dec'){
	       month = "12";
	   	}
	 	var finalDate = month+"/"+day+"/"+year;
	   	survey = finalDate.split("/");
	   	var enterDate = new Date(month+"/"+day+"/20"+year);
	  	var newsystemDate = new Date(systemDate.getMonth()+1+"/"+systemDate.getDate()+"/"+systemDate.getFullYear());
	   	var daysApart = Math.round((enterDate-newsystemDate)/86400000);
	   	if(daysApart < 0){
	    	alert("The Survey Schedule date is past the current date-The Survey is already over or Please Schedule a new survey.");
	    	return false;
	  	}
	  	if(survey != ''){
  		if(daysApart == 0){
  			var time = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
			var hour = time.substring(0, time.indexOf(":"))
			var min = time.substring(time.indexOf(":")+1, time.length);
  			if (hour < 0  || hour > 23) {
				document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
				alert('Please enter correct time.');
				return false;
			}
			if (min<0 || min > 59) {
				document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
				alert('Please enter correct time.');
				return false;
			}
			if(systemDate.getHours() > hour){
				alert("The Survey Schedule time is past the current time-The Survey is already over or Please Schedule a new survey.");
				return false;
			} else if(systemDate.getHours() == hour && systemDate.getMinutes() > min){
				alert("The Survey Schedule time is past the current time-The Survey is already over or Please Schedule a new survey.");
				return false;
			   }  }   }
	  	var agree= confirm("Press OK to Send Survey Email, else press Cancel.");	  	   
        if(agree){
        	var url='sendVoxmeMail.html?ajax=1&id=${customerFile.id}&estimator='+estimator+'&surveyDates='+finalDate+'&surveyTime='+sendsurveyTime+'&surveyTime2='+sendsurveyTime2+'&voxmeInvTag='+voxmeInvTag+'&decorator=simple&popup=true';
    		httpVoxme.open("GET", url, true);
    		httpVoxme.onreadystatechange=httpVoxmeMailSender;
    		httpVoxme.send(null);
       //	window.open('sendVoxmeMail.html?id=${customerFile.id}&estimator='+estimator+'&surveyDates='+finalDate+'&surveyTime='+sendsurveyTime+'&surveyTime2='+sendsurveyTime2+'&decorator=popup&popup=true','','width=550,height=170,top=200,left=200') ; 
       	}else {             
  	    }	  	   
        } else {
        alert("Survey is not yet Scheduled - Cannot send Survey Email.");
        }       
} 
function httpVoxmeMailSender(){
if (httpVoxme.readyState == 4){
	      var results = httpVoxme.responseText
           results = results.trim();
           if(results=='sent'){
        	  // alert('Mail Sent Successfully.');
        	}else{
        		//alert('Mail Not Send.');
        	}
           location.reload();
        }
}

function sendToMOL(target){
	var seqNum = document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
	var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
	var lastName = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
	var subject = "";
	if(target=='Starline'){
		subject = "Starline MovesOnline PIN Quote Link"; 
	}else{
		subject="Highland MovesOnline PIN Quote Link";
	}
	var body = "Please follow the link below to fill out your household inventory using MovesOnline:<br/><br/>";
	
	var agree= confirm("Press OK to Send Email to "+target+" MOL, else press Cancel.");	  	   
    if(agree){
    	var url="sendMOLMail.html?sequenceNumber="+seqNum+"&email="+email+"&lastName="+lastName+"&subject="+subject+"&body="+body+"&filter="+target+"&decorator=simple&popup=true";
    	httpVoxme.open("GET", url, true);
		httpVoxme.onreadystatechange=httpMOLMailSender;
		httpVoxme.send(null);
   	}else {             
	    }
}

function httpMOLMailSender(){
	if (httpVoxme.readyState == 4){
		      var results = httpVoxme.responseText
	           results = results.trim();
	           if(results=='sent'){
	        	  // alert('Mail Sent Successfully.');
	        	}else{
	        		//alert('Mail Not Send.');
	        	}
	        }
	}
<%--
function quotesToGoMethodAction()
 {
       var qid=document.forms['customerFileForm'].elements['customerFile.id'].value;
       var url="sendToQuotesToGo.html?decorator=simple&popup=true&id="+qid;
         httpSendToQuotes.open("GET", url, true);
    	 httpSendToQuotes.onreadystatechange = handleHttpResponseQuotesToGo;
    	 httpSendToQuotes.send(null); 
 }
function handleHttpResponseQuotesToGo(){
	
	if (httpSendToQuotes.readyState == 4){
            var results = httpSendToQuotes.responseText
            results = results.trim();
            var res=results.split("~");
            var disableQuoteValue=res[0];
            if(res[1]!=undefined)
            document.forms['customerFileForm'].elements['customerFile.estimateNumber'].value=res[1];
            showOrHide(0);
            disableQuotesToGo(disableQuoteValue);
    }
}	

function updateCustomerRequiredInformation()
{
	var fname=document.forms['customerFileForm'].elements['customerFile.firstName'].value;
    var lastname=document.forms['customerFileForm'].elements['customerFile.lastName'].value;
    var originDayPhone=document.forms['customerFileForm'].elements['customerFile.originDayPhone'].value;
    var originHomePhone=document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
    var originMobile=document.forms['customerFileForm'].elements['customerFile.originMobile'].value;
    var id=document.forms['customerFileForm'].elements['customerFile.id'].value;
    var url="updateCustomerRequiredField.html?decorator=simple&popup=true&id="+id+"&fname=" + encodeURI(fname)+"&lastname=" + encodeURI(lastname)+"&originDayPhone=" + encodeURI(originDayPhone)+"&originHomePhone=" + encodeURI(originHomePhone)+"&originMobile=" + encodeURI(originMobile);
  
      httpUpdateCust.open("GET", url, true);
      httpUpdateCust.onreadystatechange = handleHttpResponsehttpUpdateCust;
      httpUpdateCust.send(null);
}
function handleHttpResponsehttpUpdateCust(){

if (httpUpdateCust.readyState == 4)
	{
       var results = httpUpdateCust.responseText
       quotesToGoMethodAction();
    }
}	



var httpSendToQuotes = getHTTPObjectSendToQuotes();
function getHTTPObjectSendToQuotes()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var httpUpdateCust = getHTTPObjectUpdateCust();
function getHTTPObjectUpdateCust()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
--%>
function disableQuotesToGo(disableQuoteValue){

   var qoute1=document.getElementById('quotesToGoID');
   var qouteCh=document.getElementById('quotesBox');
   var qouteCh1=document.getElementById('quotesText');
   
   if(disableQuoteValue=='True'){
	   qoute1.style.display = 'none';
	   qouteCh.style.display = 'block';
	   qouteCh1.style.display = 'block';
	   document.forms['customerFileForm'].elements['customerFile.quotesToGoFlag'].checked=true;
    }
    else{
    	qoute1.style.display = 'block';
    	qouteCh.style.display = 'none';
	    qouteCh1.style.display = 'none';
	    }
}

function enableCheckBox(){
	document.forms['customerFileForm'].elements['customerFile.quotesToGoFlag'].disabled = false;
	var elementsLen=document.forms['customerFileForm'].elements.length;
			for(i=0;i<=elementsLen-1;i++)
			{
				
						document.forms['customerFileForm'].elements[i].disabled=false;
			}
}

function limitText() {
limitField= document.forms['customerFileForm'].elements['customerFile.entitled'];
limitCount=document.forms['customerFileForm'].elements['countdown'];
limitNum=254;
if (limitField.value.length > limitNum) {
	limitField.value = limitField.value.substring(0,limitNum);
} else {
	limitCount.value = limitNum - limitField.value.length;
}
}

function sendEmail(target){
var originEmail = target;
	var subject = 'Q/F# '+document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;;
	var firstName = document.forms['customerFileForm'].elements['customerFile.firstName'].value;		 
    var lastName = document.forms['customerFileForm'].elements['customerFile.lastName'].value;        
		subject = subject+' '+ firstName+' '+lastName;   		
var mailto_link = 'mailto:'+originEmail+'?subject='+subject;		
win = window.open(mailto_link,'emailWindow'); 
if (win && win.open &&!win.closed) win.close(); 
}
function openStandardAddressesPopWindow(){
var jobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=customerFile.originMobile&fld_ninthDescription=customerFile.originCity&fld_eigthDescription=customerFile.originFax&fld_seventhDescription=customerFile.originHomePhone&fld_sixthDescription=customerFile.originDayPhone&fld_fifthDescription=customerFile.originZip&fld_fourthDescription=stdAddOriginState&fld_thirdDescription=customerFile.originCountry&fld_secondDescription=customerFile.originAddress3&fld_description=customerFile.originAddress2&fld_code=customerFile.originAddress1');
document.forms['customerFileForm'].elements['checkConditionForOriginDestin'].value="originAddSection";
}
function openStandardAddressesDestinationPopWindow(){
var jobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=customerFile.destinationMobile&fld_ninthDescription=customerFile.destinationCity&fld_eigthDescription=customerFile.destinationFax&fld_seventhDescription=customerFile.destinationHomePhone&fld_sixthDescription=customerFile.destinationDayPhone&fld_fifthDescription=customerFile.destinationZip&fld_fourthDescription=stdAddDestinState&fld_thirdDescription=customerFile.destinationCountry&fld_secondDescription=customerFile.destinationAddress3&fld_description=customerFile.destinationAddress2&fld_code=customerFile.destinationAddress1');
document.forms['customerFileForm'].elements['checkConditionForOriginDestin'].value="destinAddSection";
}
function enableState(){
var id=document.forms['customerFileForm'].elements['customerFile.id'].value;
var AddSection = document.forms['customerFileForm'].elements['checkConditionForOriginDestin'].value;
if(AddSection=='originAddSection'){
var originCountry = document.forms['customerFileForm'].elements['customerFile.originCountry'].value
getOriginCountryCode();
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(originCountry=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponse12;
     http9.send(null);
}
if(AddSection=='destinAddSection'){
var country = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value
getDestinationCountryCode();
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponse11;
     http9.send(null);
} 
}
function handleHttpResponse11(){
	if (http9.readyState == 4){
            var results = http9.responseText
            results = results.trim();
            res = results.split("@");
            targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
            var stdAddDestinState=document.forms['customerFileForm'].elements['stdAddDestinState'].value;
            targetElement.length = res.length;
				for(i=0;i<res.length;i++) {
				if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = '';
				}else{
				stateVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
				document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = stateVal[0]; 
				if (document.getElementById("destinationState").options[i].value == '${customerFile.destinationState}'){
				   document.getElementById("destinationState").options[i].defaultSelected = true;
				} } }
				document.getElementById("destinationState").value = stdAddDestinState;
				if(document.forms['customerFileForm'].elements['customerFile.destinationCity'].value != ''){
		if(document.forms['customerFileForm'].elements['customerFile.destinationState'].value == ''){
			document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
		}else{
			document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value+', '+document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
		}}
		}enableStateListDestin();state();
	}
function handleHttpResponse12(){
	if (http9.readyState == 4){
            var results = http9.responseText
            results = results.trim();
            res = results.split("@");
            targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
            var stdAddOriginState=document.forms['customerFileForm'].elements['stdAddOriginState'].value;
            targetElement.length = res.length;
				for(i=0;i<res.length;i++)
				{
				if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = '';
				}else{
				stateVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = stateVal[1];
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = stateVal[0];
				
				if (document.getElementById("originState").options[i].value == '${customerFile.originState}'){
				   document.getElementById("originState").options[i].defaultSelected = true;
				} } }
				document.getElementById("originState").value = stdAddOriginState;
				if(document.forms['customerFileForm'].elements['customerFile.originCity'].value != '')
	{
		if(document.forms['customerFileForm'].elements['customerFile.originState'].value != '')
		{
			document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value+', '+document.forms['customerFileForm'].elements['customerFile.originState'].value;
		 }else{
			document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value;
		 	}
		} 
	 }enableStateListOrigin();ostateMandatory();
}

function buttondisabled(){
	var billToCode=document.forms['customerFileForm'].elements['customerFile.billToCode'].value
	if(billToCode!=''){
		document.forms['customerFileForm'].elements['P'].disabled=true;
	}else{
	    document.forms['customerFileForm'].elements['P'].disabled=false;
	}
}
</script>


<script language="JavaScript" type="text/javascript">
var httpRefJobType = getHTTPObjectRefJobType();
function getHTTPObjectRefJobType(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
} 
function setCountry(){
	if(document.forms['customerFileForm'].elements['customerFile.id'].value ==''){		
		var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
		var compDivision=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
		var url="getRefJobTypeCountryList.html?ajax=1&decorator=simple&popup=true&job="+encodeURI(job) +"&compDivision="+encodeURI(compDivision);		
        httpRefJobType.open("GET", url, true);
        httpRefJobType.onreadystatechange = httpRefJobTypeCountryName;
        httpRefJobType.send(null);		
	} }

function httpRefJobTypeCountryName(){
             if (httpRefJobType.readyState == 4){
                var results = httpRefJobType.responseText;
                results = results.trim();                           
                var res = results.replace("[",'');
                res = res.replace("]",'');
                res=res.split("~");                              
               if(res!=null && res.length>1 && res[0]!='' && res[1]!=''){
	 					document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value = res[0];	 					 					
	 					document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value=res[1];	 					
	 					document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value = res[0];	 						 					
	 					document.forms['customerFileForm'].elements['customerFile.originCountry'].value=res[1];
	 					enableStateListOrigin();ostateMandatory();
	 					enableStateListDestin();dstateMandatory();
	 					 getState(document.forms['customerFileForm'].elements['customerFile.originCountry']);
	 	                 getDestinationState(document.forms['customerFileForm'].elements['customerFile.destinationCountry']);
	 					if(res[2]=='Y'){
	 	 					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='Y';
	 	 					document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='Y';
	 	 					}else{
	 	 					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='N';
	 	 					document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='N';
	 	 					}	 					
				}else{
					document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value = '';
					document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value=''; 					
 					document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value = ''; 					
 					document.forms['customerFileForm'].elements['customerFile.originCountry'].value='';
	                 document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
				     document.forms['customerFileForm'].elements['customerFile.originState'].value = '';
				     document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
				     document.forms['customerFileForm'].elements['customerFile.destinationState'].value = '';
                }
		  }    
   	} 
function setStatas(){
	if(document.forms['customerFileForm'].elements['customerFile.id'].value ==''){
		var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
		
		if(job=='UVL' || job=='MVL' || job=='DOM' || job=='LOC' || job=='OFF' || job=='VAI' || job=='STO' || job=='MLL' || job=='HVY'){
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled =false;
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =false;
		 } }	 }

/*function zipCode(){
    var originCountryCode=document.forms['customerFileForm'].elements['customerFile.originCountry'].value; 
	var el = document.getElementById('zipCodeRequiredTrue');
	var el1 = document.getElementById('zipCodeRequiredFalse');
	var st1=document.getElementById('originStateRequiredFalse');
	var st2=document.getElementById('originStateRequiredTrue');
	if(originCountryCode == 'United States'){
	el.style.display = 'block';		
	el1.style.display = 'none';	
		
	st2.style.display = 'block';		
	st1.style.display = 'none';	
	
	}else{
	el.style.display = 'none';
	el1.style.display = 'block';
	st1.style.display = 'block';		
	st2.style.display = 'none';	
	
	}
}
*/
function state(){
    var destinationCountryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value; 
	var ds1 = document.getElementById('destinationStateRequiredTrue');
	var ds2 = document.getElementById('destinationStateRequiredFalse');
	if(destinationCountryCode == 'United States' || destinationCountryCode == 'India'){
	ds1.style.display = 'block';		
	ds2.style.display = 'none';	
		
		
	}else{
	ds1.style.display = 'none';
	ds2.style.display = 'block';
	
	}
}
function setStateReset(){
	var destinationAbc ='${customerFile.destinationCountry}';
	var originAbc ='${customerFile.originCountry}';
	var enbState = '${enbState}';
	var index = (enbState.indexOf(originAbc)> -1);
	var index1 = (enbState.indexOf(destinationAbc)> -1);

	if(index != ''){
		document.forms['customerFileForm'].elements['customerFile.originState'].disabled =false;
		getOriginStateReset(originAbc);
			var el = document.getElementById('zipCodeRequiredTrue');
			var el1 = document.getElementById('zipCodeRequiredFalse');
			var st1=document.getElementById('originStateRequiredFalse');
			var st2=document.getElementById('originStateRequiredTrue');
			if(originAbc == 'United States'){
				el.style.display = 'block';		
				el1.style.display = 'none';	
				st2.style.display = 'block';		
				st1.style.display = 'none';
			}else if(originAbc == 'India'){
				st2.style.display = 'block';		
				st1.style.display = 'none';
				el.style.display = 'none';
			}else{
				el.style.display = 'none';
				el1.style.display = 'block';
				st1.style.display = 'block';		
				st2.style.display = 'none';	
			
			}
	}else{
		document.forms['customerFileForm'].elements['customerFile.originState'].disabled =true;
	}

	if(index1 != ''){
		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =false;
		getDestinationStateReset(destinationAbc);
		var ds1 = document.getElementById('destinationStateRequiredTrue');
		var ds2 = document.getElementById('destinationStateRequiredFalse');
		if(destinationAbc == 'United States' || destinationAbc == 'India'){
			ds1.style.display = 'block';		
			ds2.style.display = 'none';	
		}else{
			ds1.style.display = 'none';
			ds2.style.display = 'block';
		}
	}else{
		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =true;
	}
}
function massageAddress(){
	 alert("Please Save Quotation File first before adding Additional address.");
}

function changeReset(){
	   document.getElementById("reset").value='reset';
	 }
function resetbuttondisabled(){
	document.forms['customerFileForm'].elements['P'].disabled=false;
	
}
function buttondisabledext(){
	var billToCode='${customerFile.billToCode}';
	if(billToCode!=''){
		document.forms['customerFileForm'].elements['P'].disabled=true;
	}else{
	    document.forms['customerFileForm'].elements['P'].disabled=false;
	}
}
function enableStateListOrigin(){ 
	var oriCon=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(oriCon)> -1);
	  if(index != ''){
	  		document.forms['customerFileForm'].elements['customerFile.originState'].disabled = false;
	  }else{
		  document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
	  }	        
}
function enableStateListDestin(){ 
	var desCon=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	  var enbState = '${enbState}';	
	  var index = (enbState.indexOf(desCon)> -1);
	  if(index != ''){
	  		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = false;
	  }else{
		  document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
	  }	        
}
  function EmailPortDate(){
	var EmailPort = document.forms['customerFileForm'].elements['customerFile.email'].value;
	document.forms['customerFileForm'].elements['customerFile.email'].value = EmailPort;
	if(document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value==''){
		document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value=EmailPort;
	} }
  function autoPopulateSecondryEmail(){
	  if(document.forms['customerFileForm'].elements['customerFile.destinationEmail2'].value == '')
	     {
	        document.forms['customerFileForm'].elements['customerFile.destinationEmail2'].value=document.forms['customerFileForm'].elements['customerFile.email2'].value;
	     }
	}
  function checkPortalBoxId() {
	    var checkPortalStatus=document.forms['customerFileForm'].elements['customerFile.status'].value;
		var checkPortalBox = document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked;
		if(checkPortalBox == true){
		if(checkPortalStatus =='CNCL' || checkPortalStatus =='CLOSED')
		{
		alert("You can't Activate the ID for Customer Portal because job is Closed/Cancelled");
		document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked=false;
		} } } 

 
 function newFunctionForCountryState(){
	 var destinationAbc ='${customerFile.destinationCountry}';
	 var originAbc ='${customerFile.originCountry}';
	 var enbState = '${enbState}';
	 var index = (enbState.indexOf(originAbc)> -1);
	 var index1 = (enbState.indexOf(destinationAbc)> -1);
	 if(index != ''){
		 document.forms['customerFileForm'].elements['customerFile.originState'].disabled =false;
	 getOriginStateReset(originAbc);
	 }
	 else{
		 document.forms['customerFileForm'].elements['customerFile.originState'].disabled =true;
	 }
	 if(index1 != ''){
		 document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =false;
	 getDestinationStateReset(destinationAbc);
	 }
	 else{
		 document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =true;
	 }
	 }
 function getDestinationStateReset(target){
		var country = target;
	 	var countryCode = "";
		<c:forEach var="entry" items="${countryCod}">
		if(country=="${entry.value}"){
			countryCode="${entry.key}";
		}
		</c:forEach>
		var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
		httpDState1.open("GET", url, true);
	     httpDState1.onreadystatechange = handleHttpResponse6666666666;
	     httpDState1.send(null);
	}
	function getOriginStateReset(target) {
		var country = target;
	 	var countryCode = "";
		<c:forEach var="entry" items="${countryCod}">
		if(country=="${entry.value}"){
			countryCode="${entry.key}";
		}
		</c:forEach>
		 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	     httpState1.open("GET", url, true);
	     httpState1.onreadystatechange = handleHttpResponse555555;
	     httpState1.send(null);
	}
	function handleHttpResponse6666666666(){
		if (httpDState1.readyState == 4){
                var results = httpDState1.responseText                
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++) {
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = stateVal[0];
					
					if (document.getElementById("destinationState").options[i].value == '${customerFile.destinationState}'){
					   document.getElementById("destinationState").options[i].defaultSelected = true;
					} } }
					document.getElementById("destinationState").value = '${customerFile.destinationState}';
        } }          
	 function handleHttpResponse555555(){
         if (httpState1.readyState == 4){
            var results = httpState1.responseText
            results = results.trim();
            res = results.split("@");
            targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++) {
				if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = '';
				}else{
				stateVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = stateVal[1];
				document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = stateVal[0];
				
				if (document.getElementById("originState").options[i].value == '${customerFile.originState}'){
				   document.getElementById("originState").options[i].defaultSelected = true;
				} } }
				document.getElementById("originState").value = '${customerFile.originState}';
         }
    }  
	  var httpState1 = getHTTPObjectState1();
	  var httpDState1 = getHTTPObjectDState1();
  function getHTTPObjectState1()	 {
	     var xmlhttp;
	     if(window.XMLHttpRequest)
	     {
	         xmlhttp = new XMLHttpRequest();
	     }
	     else if (window.ActiveXObject)
	     {
	         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	         if (!xmlhttp)
	         {
	             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	         }
	     }
	     return xmlhttp;
	 } 
 function getHTTPObjectDState1()	 {
	     var xmlhttp;
	     if(window.XMLHttpRequest)
	     {
	         xmlhttp = new XMLHttpRequest();
	     }
	     else if (window.ActiveXObject)
	     {
	         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	         if (!xmlhttp)
	         {
	             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	         }
	     }
	     return xmlhttp;
	 }
 function findAssignmentFromParentAgentOnLoad (){
	 //alert("findAssignmentFromParentAgentOnLoad");
	 var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value; 
	   if(bCode!='') { 
		 //ajax work start
		   //alert("findAssignmentFromParentAgentOnLoad11111111 result......");
		   var results ='${assignmentList}';
		  // alert(results);
	         results = results.trim();
	         results=results.replace("[","");
	         results=results.replace("]","");
	        if(results.length >= 1){
	        	var res = results.split(",");          	
	 	         var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
	 					targetElement.length = res.length+1;
	 					document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = '';
						document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = '';
	 					for(i=1;i<res.length+1;i++){
	 						//alert(res[i]);
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[i].text = res[i-1].trim();
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[i].value = res[i-1].trim();
							if(document.getElementById("assignmentType").options[i].value=='${customerFile.assignmentType}'){
								document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
							} 
						} 
	 	 		}else {
	 	 			var j=1;
			         var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
			         var len='${fn1:length(assignmentTypes)}';
						targetElement.length = parseInt(len)+1;
			 	     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
						document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
						<c:forEach var="assignment1" items="${assignmentTypes}" varStatus="loopStatus">
			 	       	var entKey = "<c:out value="${assignment1.key}"/>";
			 	      	var entvalue = "<c:out value="${assignment1.value}"/>";
			 	     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text =entvalue ;
						document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value =entKey ;					
						if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
						}
						j++;
		 	 		</c:forEach>
		 	 		//document.forms['customerFileForm'].elements['customerFile.assignmentType'].value = ' ';
		 		}
		   //ajax work end
	   }else{
		    var j=1;
	          var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
	          var len='${fn1:length(assignmentTypes)}';
	    targetElement.length = parseInt(len)+1;
	         document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
	    document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
	    <c:forEach var="assignment1" items="${assignmentTypes}" varStatus="loopStatus">
	           var entKey = "<c:out value="${assignment1.key}"/>";
	          var entvalue = "<c:out value="${assignment1.value}"/>";
	         document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text = entvalue;
	    document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value =entKey ;     
	    if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
	     document.getElementById("assignmentType").options[j].defaultSelected=true;
	    }
	    j++;
	     </c:forEach>
	    // document.forms['customerFileForm'].elements['customerFile.assignmentType'].value = ' ';
	    }
		 }
 function findAssignmentFromParentAgent(){	
	   var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value; 
	   if(bCode!='') { 
		   var url="findAssignmentAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
		   http551.open("GET", url, true); 
		   http551.onreadystatechange = handleHttp90; 
		   http551.send(null); 
	   }else{
		    var j=1;
	          var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
	          var len='${fn1:length(assignmentTypes)}';
	    targetElement.length = parseInt(len)+1;
	    document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
	    document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
	    <c:forEach var="assignment1" items="${assignmentTypes}" varStatus="loopStatus">
	           var entKey = "<c:out value="${assignment1.key}"/>";
	          var entvalue = "<c:out value="${assignment1.value}"/>";
	         document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text = entvalue;
	    document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value =entKey ;     
	    if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
	     document.getElementById("assignmentType").options[j].defaultSelected=true;
	    }
	    j++;
	     </c:forEach>
	     document.forms['customerFileForm'].elements['customerFile.assignmentType'].value = ' ';
	    }
	 }
 function handleHttp90(){
	 if (http551.readyState == 4){	 
	        var results = http551.responseText
	        results = results.trim();
	        if(results.length >= 1){
	        	var res = results.split("@");          	
	 	         var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
	 					targetElement.length = res.length;
	 					for(i=0;i<res.length;i++){
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[i].text = res[i];
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[i].value = res[i];
							if(document.getElementById("assignmentType").options[i].value=='${customerFile.assignmentType}'){
								document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
							} 
						} 
	 	 		}else {
	 	 			var j=1;
			         var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
			         var len='${fn1:length(assignmentTypes)}';
						targetElement.length = parseInt(len)+1;
			 	     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
						document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
						<c:forEach var="assignment1" items="${assignmentTypes}" varStatus="loopStatus">
			 	       	var entKey = "<c:out value="${assignment1.key}"/>";
			 	      	var entvalue = "<c:out value="${assignment1.value}"/>";
			 	     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text =entvalue ;
						document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value =entKey ;					
						if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
						}
						j++;
		 	 		</c:forEach>
		 		}
	 		}
	 	}

 function fillAccountCodeByBillToCode(){
	    var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	    if(bCode!='') {
		var url="fillAccountCodeByBillToCode.html?ajax=1&decorator=simple&popup=true&bCode=" + encodeURI(bCode);
	    http44.open("GET", url, true);
	    http44.onreadystatechange = handleHttpResponseFillAcc;
	    http44.send(null);
	   }
	}
<c:set var="accountHollValidation" value="N" />
<configByCorp:fieldVisibility componentId="component.field.accountCode.Validation">
<c:set var="accountHollValidation" value="Y" />
</configByCorp:fieldVisibility>
	
	function handleHttpResponseFillAcc(){
		 if (http44.readyState == 4){
	           var results = http44.responseText
	           results = results.trim();
	           results=results.replace("[","");
	           results=results.replace("]","");
	           				if(results!=''){
	           					if((results!='')&&(results!='NOTAC')&&(results!='AC')){
	           						var	res = results.split("~");
	    							document.forms['customerFileForm'].elements['customerFile.accountCode'].value=res[0];
	    							document.forms['customerFileForm'].elements['customerFile.accountName'].value=res[1];
	           					}else{
	           						if((results!='')&&(results=='AC')){
	    								document.forms['customerFileForm'].elements['customerFile.accountCode'].value = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	    						        document.forms['customerFileForm'].elements['customerFile.accountName'].value = document.forms['customerFileForm'].elements['customerFile.billToName'].value;
	           						}else{
	           						   <c:if test="${accountHollValidation=='N'}">
           								document.forms['customerFileForm'].elements['customerFile.accountCode'].value = '';
	    						        document.forms['customerFileForm'].elements['customerFile.accountName'].value = '';
	           							</c:if>	           						}
	           					}    
		 		}
		   }
	}
	 
</script>


<script type="text/javascript"> 

try{	
	ostateMandatory();
    dstateMandatory();
	limitText();
}
catch(e){}
<configByCorp:fieldVisibility componentId="customerFile.contactName">
enableDisableAccountContact();
</configByCorp:fieldVisibility>
try{
	autoPopulate_customerFile_originCountry(document.forms['customerFileForm'].elements['customerFile.originCountry']);
  	}
  	catch(e){}
  	try{
  		getStateOnLoad();	 
	}
	catch(e){}
	try{
	autoPopulate_customerFile_destinationCountry(document.forms['customerFileForm'].elements['customerFile.destinationCountry']);
 	//getDestinationState(document.forms['customerFileForm'].elements['customerFile.destinationCountry']);
 	}
 	catch(e){}
 	try{
 		getDestinationStateOnLoad();
 	}
 	catch(e){}
 	try{
 	<c:if test="${empty customerFile.id}">
	autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
	</c:if> 
    }
    catch(e){}
	try{
	getContractONLOad();	
     }
	 catch(e){		 
	 }
	 try{
	//getNewCoordAndSale('load');
		}
	catch(e){}
	try{
		<c:if test="${not empty customerFile.id}">
		checkExpContract();
		</c:if>
		 }
		 catch(e){}
	try{
		 var ocountry=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
		 }
		 catch(e){}
		 try{
		 var dcountry=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
		 }
		 catch(e){}
		 try{
			 var enbState = '${enbState}';
			 var oriCon=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
			 if(oriCon=="")	 {
					document.getElementById('originState').disabled = true;
					document.getElementById('originState').value ="";
			 }else{
			  		if(enbState.indexOf(oriCon)> -1){
						document.getElementById('originState').disabled = false; 
						document.getElementById('originState').value='${customerFile.originState}';
			  		}else{
						document.getElementById('originState').disabled = true;
						document.getElementById('originState').value ="";
				}
			 }
		 }
	catch(e){}
		try{
			var desCon=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
			var enbState = '${enbState}';
			if(desCon=="")	 {
					document.getElementById('destinationState').disabled = true;
					document.getElementById('destinationState').value ="";
			 }else{
			  		if(enbState.indexOf(desCon)> -1){
						document.getElementById('destinationState').disabled = false;		
						document.getElementById('destinationState').value='${customerFile.destinationState}';
			  		}else{
						document.getElementById('destinationState').disabled = true;
						document.getElementById('destinationState').value ="";
				}
			 }
		}
	catch(e){}
	/*function billtoCheck(){
					var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
					if(billToCode != '${customerFile.billToCode}')
					{
				    if(billToCode!=''){
				    	showOrHide(1)
					}
					changeStatus();
					//showOrHide(1)
					findBillToName();
					//setTimeout("showOrHide(0)",6000);					
					getContract();
					findPricingBillingPaybaleBILLName();
					showOrHide(1)
					}
				}*/	
	function showOrHide(value) {
				    if (value==0) {
				       	if (document.layers)
				           document.layers["overlay"].visibility='hide';
				        else
				           document.getElementById("overlay").style.visibility='hidden';
				   	}else if (value==1) {
				   		if (document.layers)
				          document.layers["overlay"].visibility='show';
				       	else
				          document.getElementById("overlay").style.visibility='visible';
				   	} }    
				showOrHide(0);
</script>


<script type="text/javascript">
var fieldName = document.forms['customerFileForm'].elements['field'].value;
var fieldName1 = document.forms['customerFileForm'].elements['field1'].value;
if(fieldName!=''){
	document.forms['customerFileForm'].elements[fieldName].className = 'rules-textUpper';
	animatedcollapse.addDiv('address', 'fade=0,persist=0,show=1')
	animatedcollapse.addDiv('billing', 'fade=0,persist=0,show=1')
	animatedcollapse.addDiv('entitlement', 'fade=0,persist=0,show=1')
	animatedcollapse.addDiv('acc_contact', 'fade=0,persist=0,show=1')
	animatedcollapse.addDiv('additional', 'fade=0,persist=0,show=1')
	animatedcollapse.addDiv('cportal', 'fade=0,persist=0,show=1')
	animatedcollapse.addDiv('alternative', 'fade=0,persist=0,show=1')
	animatedcollapse.init()
}
if(fieldName1!=''){
	document.forms['customerFileForm'].elements[fieldName1].className = 'rules-textUpper';
	animatedcollapse.addDiv('address', 'fade=0,persist=0,show=1')
	animatedcollapse.addDiv('billing', 'fade=0,persist=0,show=1')
	animatedcollapse.addDiv('entitlement', 'fade=0,persist=0,show=1')
	animatedcollapse.addDiv('acc_contact', 'fade=0,persist=0,show=1')
	animatedcollapse.addDiv('additional', 'fade=0,persist=0,show=1')
	animatedcollapse.addDiv('cportal', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('alternative', 'fade=0,persist=0,show=1')
	animatedcollapse.init()
}

</script>


<script type="text/javascript">

function winOpenOrigin(){					
   	openWindow('originPartners.html?origin=${customerFile.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.originAgentName&fld_code=customerFile.originAgentCode');	
	document.forms['customerFileForm'].elements['customerFile.originAgentCode'].select();
}
var http3 = getHTTPObject1();

function showPartnerAlert(display, code, position){
    if(code == ''){ 
    	
 	document.getElementById(position).style.display="none";
 
    }else if(code != ''){

    }  }
function getBAPartner(display,code,position){
	var url = "findPartnerAlertList.html?ajax=1&decorator=simple&popup=true&notesId=" + encodeURI(code);
    http3.open("GET", url, true);
    http3.onreadystatechange = function() {handleHttpResponsePartner(display,code,position)};
    http3.send(null);
}
function handleHttpResponsePartner(display,code,position){
	if (http3.readyState == 4){
		var results = http3.responseText
       results = results.trim();
         if(results.indexOf("Nothing found to display")>1)
         { 
          exit();
         }else{
        if(results.length > 565){
         	document.getElementById(position).style.display="block";
         	if(display != 'onload'){
         		getPartnerAlert(code,position);
         	}
     	}else{
     		document.getElementById(position).style.display="none";
     		ajax_hideTooltip();
     	} } }
} 


</script>




<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>

<script type="text/javascript">
function findOriginAgentName(){
    var originAgentCode = document.forms['customerFileForm'].elements['customerFile.originAgentCode'].value;
    if(originAgentCode.length == 0){
    	document.forms['customerFileForm'].elements['customerFile.originAgentName'].value="";
    }else if(originAgentCode.length > 0){
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(originAgentCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse44;
     http2.send(null);
    } 
}
function handleHttpResponse44(){
	if (http2.readyState == 4) {
          var results = http2.responseText
          results = results.trim();
          var res = results.split("#"); 
            if(res.length>2){
            	if(res[2] == 'Approved'){
           			document.forms['customerFileForm'].elements['customerFile.originAgentName'].value = res[1];
               		//document.forms['customerFileForm'].elements['customerFile.originAgentCode'].select();
            	}else{
           			alert("Origin Agent code is not approved" ); 
				    document.forms['customerFileForm'].elements['customerFile.originAgentName'].value="";
				 document.forms['customerFileForm'].elements['customerFile.originAgentCode'].value="";
				 document.forms['customerFileForm'].elements['customerFile.originAgentCode'].select();
           		}  
            }else{
                 alert("Origin Agent code not valid");
                 document.forms['customerFileForm'].elements['customerFile.originAgentName'].value="";
				 document.forms['customerFileForm'].elements['customerFile.originAgentCode'].value="";
				 document.forms['customerFileForm'].elements['customerFile.originAgentCode'].select();
           }
  }
}    

</script>

<script type="text/javascript">
function fillBillToAuthority(){
	var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	if(bCode!='') {
	    var url="fillBillToAuthority.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http51.open("GET", url, true); 
	   	http51.onreadystatechange = handleHttp901; 
	   	http51.send(null);	     
     }
 }
function handleHttp901(){
    if (http51.readyState == 4){    	
    	var results = http51.responseText
    	results = results.trim();
    	if(results == undefined)
		{
		results='';
		}
    	if(document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value==''){
		 	document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value = results;
    	}
       }
	}

var http51 = getHTTPObject();
function getHTTPObject()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)
    {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
}
return xmlhttp;
}
function billToCodeFieldValueCopy(){
	var bCode=document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	if(bCode==''){
		document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value="";
	}
}
</script>

<script type="text/javascript">
	/* Methods added for ticket number: 6092*/
	function setVipImageReset(){
			if('${customerFile.vip}'.toString() == 'true')
				document.getElementById('customerFile.vip').checked = true;	
			else
				document.getElementById('customerFile.vip').checked = false;	
			setVipImage();
	}
	function setVipImage(){
		var imgElement = document.getElementById('vipImage');
		if(document.getElementById('customerFile.vip').checked == true){
			imgElement.src = '${pageContext.request.contextPath}'+'/images/vip_icon.png';
			imgElement.style.display = 'block';
		}
		else{
			if('${customerFile.vip}'.toString() == 'true'  && document.getElementById('customerFile.vip').checked == true){
				imgElement.src = '${pageContext.request.contextPath}'+'/images/vip_icon.png';
				imgElement.style.display = 'block';
			}
			else{
				imgElement.src = '';
				imgElement.style.display = 'none';
			}
		}
	}
	window.onload = setVipImageReset();

if(window.addEventListener)
		window.addEventListener('load',function(){
			getDestinationStateOnLoad();
			},false);

function clickOnRequest(){
	document.getElementById("customerFileForm_button_save").click();
}
try{
	findAssignmentFromParentAgentOnLoad();
}catch(e){}
function setAssignmentReset(){
	setTimeout("findAssignmentFromParentAgentOnLoad()",500);
}
</script>

<script language="JavaScript" type="text/javascript"
	SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('address', 'fade=1,hide=1')
animatedcollapse.addDiv('billing', 'fade=1,hide=1')
animatedcollapse.addDiv('entitlement', 'fade=1,hide=1')
animatedcollapse.addDiv('acc_contact', 'fade=1,hide=1')
animatedcollapse.addDiv('additional', 'fade=1,hide=1')
animatedcollapse.addDiv('cportal','fade=1,hide=1')
animatedcollapse.addDiv('alternative','fade=1,hide=1')
animatedcollapse.init()
</script>

<script language="JavaScript" type="text/javascript">
function setTimeMask() {
//alert();
oTimeMask1 = new Mask("##:##", "surveyTime");

oTimeMask1.attach(document.forms['customerFileForm'].surveyTime);
//alert("gg");
oTimeMask2 = new Mask("##:##", "surveyTime2");
oTimeMask2.attach(document.forms['customerFileForm'].surveyTime2);
}
</script>
<script type="text/javascript">
setTimeout("setOriginCityCode()",500);
function setOriginCityCode(){
try{
	document.forms['customerFileForm'].elements['customerFile.originCityCode'].value ='${customerFile.originCityCode}';
	<configByCorp:fieldVisibility componentId="component.field.qfStatusReason.showVOER">
	showStatusReason();
	</configByCorp:fieldVisibility>
}catch(e){}
}
function findCustomInfo(position) {
	 var custid=document.forms['customerFileForm'].elements['customerFile.id'].value;
	 var url="customInfo.html?ajax=1&decorator=simple&popup=true&custid=" + encodeURI(custid)
	  ajax_showTooltip(url,position);	
	  } 
</script>
<script type="text/javascript">
function autoCompleterAjaxCallOriCity(){
var stateNameOri = document.forms['customerFileForm'].elements['customerFile.originState'].value;
var countryName = document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
var cityNameOri = document.forms['customerFileForm'].elements['customerFile.originCity'].value;
var countryNameOri = "";
<c:forEach var="entry" items="${countryCod}">
if(countryName=="${entry.value}"){
	countryNameOri="${entry.key}";
}
</c:forEach>	
if(cityNameOri!=''){
	var data = 'leadCaptureAutocompleteOriAjax.html?ajax=1&stateNameOri='+stateNameOri+'&countryNameOri='+countryNameOri+'&cityNameOri='+cityNameOri+'&decorator=simple&popup=true';
		$("#originCity").autocomplete({
			source: data	
		});
}
}
function autoCompleterAjaxCallDestCity(){
	var stateNameDes = document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
	var countryName = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	var cityNameDes = document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
	var countryNameDes = "";
	<c:forEach var="entry" items="${countryCod}">
	if(countryName=="${entry.value}"){
		countryNameDes="${entry.key}";
	}
	</c:forEach>
	if(cityNameDes!=''){
	var data = 'leadCaptureAutocompleteDestAjax.html?ajax=1&stateNameDes='+stateNameDes+'&countryNameDes='+countryNameDes+'&cityNameDes='+cityNameDes+'&decorator=simple&popup=true';
	$( "#destinationCity" ).autocomplete({				 
	      source: data		      
	    });
	}
}
</script>
<script language="JavaScript" type="text/javascript">
window.onload = function() {
	try{
		var tem1=document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value;
		var tem2=document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value;
		var tem3=document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value;
		var tem4=document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value;

		if(tem1=='' )
		{
			document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value='00:00';
		}
		if(tem2=='' )
		{
			document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value='00:00';	
		}
		if(tem3=='' )
		{
			document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value='00:00';
			}
		if(tem4=='' )
		{
			document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value='00:00';
		}
	}catch(e){}
}
</script>

<script language="JavaScript" type="text/javascript">
function calendarICSFunction(){	
	 var str="0";
	<configByCorp:fieldVisibility componentId="component.field.prefSurveyDate.showCorpId">
		str="1";
	</configByCorp:fieldVisibility>
	var cfId = '${customerFile.sequenceNumber}';
	var surveyType = 'CF';
   var surveyDateCF = document.forms['customerFileForm'].elements['customerFile.survey'].value;
   var consultantName = document.forms['customerFileForm'].elements['customerFile.estimator'].value;
   var surveyTimeFrom = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
   var surveyTimeTo = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
   if(str=="0"){
	    if(cfId==''){
	    	alert('Please save Quotation File to continue.');
	    }else if(consultantName==''){
	    	alert('Please select Consultant to continue.');
	    }else if(surveyDateCF==''){
	    	alert('Please select Survey date to continue.');
	    }else if(surveyTimeFrom==''){
	    	alert('Please fill Survey From Time to continue.');
	    }else if(surveyTimeTo==''){
	        	alert('Please fill Survey To Time to continue.');
	    }else{
	 		var url = "appointmentICSCF.html?cfId="+cfId+"&consultantName="+consultantName+"&surveyDateCF="+surveyDateCF+"&surveyTimeFrom="+surveyTimeFrom+"&surveyTimeTo="+surveyTimeTo+"&surveyType="+surveyType+"";
	    	location.href=url;
	    	}
   	}else{
   	    var surveyDate1CF = document.forms['customerFileForm'].elements['customerFile.prefSurveyDate1'].value;
   	    var surveyDate2CF = document.forms['customerFileForm'].elements['customerFile.prefSurveyDate2'].value;
   	    var prefSurveyTime1 = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value;
   	    var prefSurveyTime2 = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value;
   	    var prefSurveyTime3 = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value;
   	    var prefSurveyTime4 = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value;
   		 if(cfId==''){
   		    	alert('Please save Quotation File to continue.');
   		    }else if(consultantName==''){
   		    	alert('Please select Consultant to continue.');
   		    }else if(surveyDateCF==''){
   		    	alert('Please select Survey date to continue.');
   		    }else if(surveyTimeFrom==''){
   		    	alert('Please fill Survey From Time to continue.');
   		    }else if(surveyTimeTo==''){
   		        	alert('Please fill Survey To Time to continue.');    		    
   		    }else{
   		 		var url = "appointmentICSCF.html?cfId="+cfId+"&consultantName="+consultantName+"&surveyDateCF="+surveyDateCF+"&surveyTimeFrom="+surveyTimeFrom+"&surveyTimeTo="+surveyTimeTo+"&surveyType="+surveyType+"" +
   		 				"&surveyDate1CF="+surveyDate1CF+"&surveyDate2CF="+surveyDate2CF+"&prefSurveyTime1="+prefSurveyTime1+"&prefSurveyTime2="+prefSurveyTime2+"&prefSurveyTime3="+prefSurveyTime3+"&prefSurveyTime4="+prefSurveyTime4+"";
   		    	location.href=url;
   		    	}
   	}
	}
function findDefaultSettingFromPartnerOnload(){
	var cfContract = document.forms['customerFileForm'].elements['customerFile.contract'].value;
	var cfSource = document.forms['customerFileForm'].elements['customerFile.source'].value;
	if(document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value=='undefined'){
		document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value='';
	}
	var cfBillToAuth = document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value;	
	var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	if((cfContract=='' || cfSource=='' || cfBillToAuth=='') && bCode!='') {
	     var results='${agentParentList1}';	     
	     if(results!=''){
		    	var	res = results.split("~");
			    	if(document.forms['customerFileForm'].elements['customerFile.contract'].value==''){
			    		if(res[0]!='NA'){
			    			document.forms['customerFileForm'].elements['customerFile.contract'].value=res[0];
			    		}
			    	}
			    	if(document.forms['customerFileForm'].elements['customerFile.source'].value==''){
			    		if(res[3]!='NA'){
			    			document.forms['customerFileForm'].elements['customerFile.source'].value = res[3];
			    		}
			    	}
			    	if(document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value==''){
			    		if(res[9]!='NA'){
			    			if(res[9]==undefined || res[9]=="undefined"){
			    				res[9]="";
			    			}
			    			document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value = res[9];
			    		}
			    	}
	    		}
     }
}
function findDefaultSettingFromPartner(){
	var cfContract = document.forms['customerFileForm'].elements['customerFile.contract'].value;
	var cfSource = document.forms['customerFileForm'].elements['customerFile.source'].value;
	var cfBillToAuth = document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value;
	var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	if((cfContract=='' || cfSource=='' || cfBillToAuth=='') && bCode!='') {
	    var url="findDefaultValuesFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http521.open("GET", url, true); 
	   	http521.onreadystatechange = handleHttp921; 
	   	http521.send(null);	     
     }
}
function handleHttp921(){
    if (http521.readyState == 4){    	
    	var results = http521.responseText
    	results = results.trim();    	
	    	var	res = results.split("~");
		    	if(document.forms['customerFileForm'].elements['customerFile.contract'].value==''){
		    		if(res[0]!='NA'){
		    			document.forms['customerFileForm'].elements['customerFile.contract'].value=res[0];
		    		}
		    	}
		    	if(document.forms['customerFileForm'].elements['customerFile.source'].value==''){
		    		if(res[3]!='NA'){
		    			document.forms['customerFileForm'].elements['customerFile.source'].value = res[3];
		    		}
		    	}
		    	if(document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value==''){
		    		if(res[9]==undefined || res[9]=='NA'){
		    			document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value = '';
		    		}else{
		    			if(res[9]==undefined || res[9]=="undefined"){
		    				res[9]="";
		    			}
		    			document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value = res[9];
		    		}
		    	}
    		}
       	}

var http521 = getHTTPObject();
</script>