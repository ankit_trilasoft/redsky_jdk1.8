<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>Check List</title> 
<meta name="heading" content="Check List"/>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-17px;
!margin-top:-20px;
padding:2px 0px;
text-align:right;
width:100%;
}

.table thead th, .tableHeaderTable td .headerClassLeft .sortable{
text-align:left;
}

.table thead th, .tableHeaderTable td .headerClassRight .sortable
{
text-align:left;
}
#overlay11 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>


</head>
	<s:form id="CheckList" name="CheckList" action="CheckListSearch" method="post" validate="true">   
	<c:set var="buttons">  
     <input type="button" class="cssbutton1" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/checkListForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	</c:set>
	<div id="Layer1" style="width:80%;">
	<td colspan="7" style="margin:0px;">
			<DIV ID="overlay11">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Executing Rule</font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
			</div>
			</td>
		 	</td>
	<!--<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   --><!--<div class="center-content">		
	<table class="table" style="width:98%;">
	<thead>
	<tr>
	<th><fmt:message key="forwardingMarkUpList.name"/></th>
	<th><fmt:message key="forwardingMarkUpList.tableName"/></th>
	<th><fmt:message key="forwardingMarkUpList.fieldName"/></th>
	<th><fmt:message key="forwardingMarkUpList.filterValues"/></th>
	
	
	</tr></thead>
	
	<tbody>
		<tr>
		    <td width="20" align="left"><s:textfield name="dataSecurityFilter.name" required="true" cssClass="input-text" size="40"/></td>
			<td width="20" align="left"><s:textfield name="dataSecurityFilter.tableName" required="true" cssClass="input-text" size="20"/></td>
			<td width="20" align="left"><s:textfield name="dataSecurityFilter.fieldName" required="true" cssClass="input-text" size="20"/></td>
			<td width="20" align="left"><s:textfield name="dataSecurityFilter.filterValues" required="true" cssClass="input-text" size="20"/></td>
			</tr>
			 <tr>
			 <td colspan="3"></td>
			 <td style="border-left: hidden;">
       		<s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" key="button.search"/>  
       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
       
   			</td>
		</tr>
		</tbody>
	</table>
	</div>
--><!--<div class="bottom-header"><span></span></div>
</div>
</div>
	<c:out value="${searchresults}" escapeXml="false" /> 
	-->
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Check Lists</span></a></li>
		    <li><a href="toDoRuleLists.html"><span>To Do List</span></a></li>
		    <li><a href="docCheckLists.html" style="cursor: pointer;"><span>Doc Check List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	
	<display:table name="checksList" class="table" requestURI="" id="checksListId" export="false" defaultsort="1" pagesize="10" style="width:100%" >   
	<display:column property="id"  href="checkListForm.html" paramId="id" paramProperty="id" sortable="true" title="ID"/>
	<display:column property="jobType"   sortable="true" title="Job Type"/>
	<display:column property="partnerCode"   paramId="id" paramProperty="id" sortable="true" title="Partner Code"/>
   	<display:column property="documentType"   sortable="true" title="Document Type"/>
   	<display:column property="messageDisplayed"  sortable="true" title="Message Displayed" style="width:30%;"/>
   	<c:if test="${checksListId.status==true}">
   	<display:column  titleKey="toDoRule.status" style="text-align: center"><img src="${pageContext.request.contextPath}/images/tick01.gif"  /></display:column> 
   	</c:if>
   	<c:if test="${checksListId.status==false || checksListId.status==null}">
   	<display:column  titleKey="toDoRule.status" style="text-align: center"><img src="${pageContext.request.contextPath}/images/cancel001.gif"  /></display:column> 
   	</c:if>
   	
   	<c:if test="${checksListId.enable==true}">
   	<display:column  titleKey="toDoRule.checkEnable" style="text-align: center"><img src="${pageContext.request.contextPath}/images/tick01.gif"  /></display:column> 
   	</c:if>
   	
   	<c:if test="${checksListId.enable==false || checksListId.enable==null}">
   	<display:column  titleKey="toDoRule.checkEnable" style="text-align: center"><img src="${pageContext.request.contextPath}/images/cancel001.gif"  /></display:column> 
   	</c:if>
   	
   	<display:column title="Remove" style="width:7%;">
		<a>
			<img align="middle" title="" onclick="confirmSubmit('${checksListId.id}');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>
		</a>
	</display:column>
   	</display:table>
	
	<c:out value="${buttons}" escapeXml="false" /> 
	<td align="right">
	<input type="button" class="cssbutton1" value="Execute" style="width:100px; height:25px" onclick="showOrHide(1);location.href='<c:url value="/executeCheckList.html?from=checkList"/>'" />
	</td>  
	</div>
</s:form> 

<script language="javascript" type="text/javascript">
function clear_fields(){
	var i;
	for(i=0;i<=3;i++)
	{
		document.forms['forwardingMarkUpList'].elements[i].value = "";
	}
}

function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to delete this row?");
	if (agree){
		location.href = "deleteCheckList.html?id="+encodeURI(targetElement);
	}else{
		return false;
	}
}
</script>

<script type="text/javascript"> 
try{
<c:if test="${hitFlag == 1}" >
		<c:redirect url="/checkLists.html"  />
</c:if>
}
catch(e){}
showOrHide(0);
function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["overlay11"].visibility='hide';
        else
           document.getElementById("overlay11").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["overlay11"].visibility='show';
       else
          document.getElementById("overlay11").style.visibility='visible';
   }
}
</script> 