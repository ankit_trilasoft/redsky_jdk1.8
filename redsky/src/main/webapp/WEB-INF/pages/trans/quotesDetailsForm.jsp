<%@ include file="/common/taglibs.jsp"%>

<style>
	.wrap {
    width:529px;
}

  .HMtable {    
   width:533px;
  border-collapse: collapse;
}

.HMtable tr td {
    border: 1px solid #E0E0E0;
    border-collapse: collapse;
    padding: 4px 3px 5px 5px;
    width: 97px;
    word-wrap: break-word;
    font-family: arial,verdana;
    font-size: 12px;
    color:#000000;
    
}
.HMtable tr.even {
    background-color: #E4E6F2;
    border-top: 1px solid #C0C0C0;
    color: #000000;
}
.HMtable tr.odd {
    background: none repeat scroll 0 0 #FFFFFF;
    border-top: 1px solid #C0C0C0;
    color: #000E4D;
}
.HMtable tr td.head {    
 background: url("/redsky/images/bg_listheader.png") repeat-x scroll 0 0 #BCD2EF;
    border-color: #3DAFCB;
    border-style: solid;
    border-width: 1px;
    color: #15428B;
    font-family: arial,verdana;
    font-size: 11px;
    font-weight: bold;
    height: 20px;
    padding: 2px 3px 3px 5px;
    text-decoration: none;
}

.inner_table {
    height: 404px;
    overflow-y: auto;
 width:551px;
 
}
.EmptyHM {
	background: none repeat scroll 0 0 #FFFFFF;
    border:2px solid #74B3DC;
    border-top:none;;
    float: left;
    height: 10px;
    padding: 5px;
    text-align: left;
    width: 507px;
}
</style>
<script type="text/javascript">
function cabinetUpload(jobNumber,marketAreaName,pricingVolume,pricingWeight,pricePointVolumeUnit,pricePointWeightUnit,pricePointMinimum_density,pricePointTariff_published,pricePointRate,pricePoint_price,pricePoint_cctotal,pricePoint_thc,packService,MarketagentId,pricePoint_currency,pricePoint_rate_basis,corpIdBaseCurrency,weight_unit,volume_unit,pricePoint_WeightUnit,pricePoint_VolumeUnit){	
        var agentcustoms= document.forms['pricePoint'].elements['agentcustoms'].value;
        agentcustoms=encodeURI(agentcustoms)
       // alert(agentcustoms);
        var agentConsignment= document.forms['pricePoint'].elements['agentConsignment'].value;
        agentConsignment=encodeURI(agentConsignment)
        //alert(agentConsignment)
        var agentAdditional= document.forms['pricePoint'].elements['agentAdditional'].value;
        agentAdditional=encodeURI(agentAdditional)
        var pricePointAgent=document.forms['pricePoint'].elements['pricePointAgent'].value;
        pricePointAgent=encodeURI(pricePointAgent)
         var SupplementRate_basis=document.forms['pricePoint'].elements['SupplementRate_basis'].value;
        var SupplementRate=document.forms['pricePoint'].elements['SupplementRate'].value;
        var SupplementType=document.forms['pricePoint'].elements['SupplementType'].value;
        //alert(SupplementRate_basis)
     
	 document.getElementById("overlayMarket").style.display = "block";
  /* new Ajax.Request('pricePointToCabinetAjax.html?formReportFlag=F&fileType=pdf&jasperName=PricePointForm.jrxml&jobNumber='+jobNumber+'&reportParameter_Corporate ID=${sessionCorpID}&reportParameter_Service Order Number='+jobNumber+'&reportParameter_Agent='+pricePointAgent+'&MarketagentId='+MarketagentId+'&reportParameter_Market Area='+marketAreaName+'&reportParameter_pricePoint_currency='+pricePoint_currency+'&reportParameter_pricePoint_Base_currency='+corpIdBaseCurrency+'&reportParameter_pricePoint_rate_basis='+pricePoint_rate_basis+'&reportParameter_weight_unit='+weight_unit+'&reportParameter_volume_unit='+volume_unit+'&reportParameter_Service='+packService+'&reportParameter_Declared Volume='+pricingVolume+'&reportParameter_Declared Weight='+pricingWeight+'&reportParameter_Chargeable Volume='+pricePointVolumeUnit+'&reportParameter_Chargeable Weight='+pricePointWeightUnit+'&reportParameter_Minimum Density='+pricePointMinimum_density+'&reportParameter_Tariff Published='+pricePointTariff_published+'&reportParameter_Rate='+pricePointRate+'&reportParameter_Destination THC='+pricePoint_thc+'&reportParameter_Invoice Total='+pricePoint_price+'&reportParameter_Converted Currency Total='+pricePoint_cctotal+'&reportParameter_Type='+encodeURI(SupplementType)+'&reportParameter_Rate Supplemental Charges='+encodeURI(SupplementRate)+'&reportParameter_Rate Basis='+encodeURI(SupplementRate_basis)+'&reportParameter_pricePoint_WeightUnit='+pricePoint_WeightUnit+'&reportParameter_pricePoint_VolumeUnit='+pricePoint_VolumeUnit+'&reportParameter_AgentConsignment='+agentConsignment+'&reportParameter_AgentAdditional='+agentAdditional+'&reportParameter_AgentCustoms='+agentcustoms+'&decorator=simple&popup=true',
   		{
	    method:'POST',
	    onSuccess: function(transport){
   	
	      var response = transport.responseText || "no response text";
	      document.getElementById("overlayMarket").style.display = "none";
	      alert("Price Quote Uploaded Successfully");	      		      
	      
	    },
	    
	    onLoading: function()
	       {
	    	//var loading = document.getElementById("loading").style.display = "block";
	    	
	    	 loading.innerHTML;
	    },
	    
	    onFailure: function(){ 
		    //alert('Something went wrong...')
		     }
	  }); */

	  var url = "pricePointToCabinetAjax.html";
	  var parameters ='formReportFlag=F&fileType=pdf&jasperName=PricePointForm.jrxml&jobNumber='+jobNumber+'&reportParameter_Corporate ID=${sessionCorpID}&reportParameter_Service Order Number='+jobNumber+'&reportParameter_Agent='+pricePointAgent+'&MarketagentId='+MarketagentId+'&reportParameter_Market Area='+marketAreaName+'&reportParameter_pricePoint_currency='+pricePoint_currency+'&reportParameter_pricePoint_Base_currency='+corpIdBaseCurrency+'&reportParameter_pricePoint_rate_basis='+pricePoint_rate_basis+'&reportParameter_weight_unit='+weight_unit+'&reportParameter_volume_unit='+volume_unit+'&reportParameter_Service='+packService+'&reportParameter_Declared Volume='+pricingVolume+'&reportParameter_Declared Weight='+pricingWeight+'&reportParameter_Chargeable Volume='+pricePointVolumeUnit+'&reportParameter_Chargeable Weight='+pricePointWeightUnit+'&reportParameter_Minimum Density='+pricePointMinimum_density+'&reportParameter_Tariff Published='+pricePointTariff_published+'&reportParameter_Rate='+pricePointRate+'&reportParameter_Destination THC='+pricePoint_thc+'&reportParameter_Invoice Total='+pricePoint_price+'&reportParameter_Converted Currency Total='+pricePoint_cctotal+'&reportParameter_Type='+encodeURI(SupplementType)+'&reportParameter_Rate Supplemental Charges='+encodeURI(SupplementRate)+'&reportParameter_Rate Basis='+encodeURI(SupplementRate_basis)+'&reportParameter_pricePoint_WeightUnit='+pricePoint_WeightUnit+'&reportParameter_pricePoint_VolumeUnit='+pricePoint_VolumeUnit+'&reportParameter_AgentConsignment='+agentConsignment+'&reportParameter_AgentAdditional='+agentAdditional+'&reportParameter_AgentCustoms='+agentcustoms+'&decorator=simple&popup=true';
	  http2.open("POST", url, true); 
	  http2.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	  http2.setRequestHeader("Content-length", parameters .length);
	  http2.setRequestHeader("Connection", "close");
	  http2.onreadystatechange = handleHttpResponse2;
	  http2.send(parameters);
}
function handleHttpResponse2(){
	if (http2.readyState == 4){
            var results = http2.responseText
            results = results.trim();     	  
            document.getElementById("overlayMarket").style.display = "none";
            //alert(results)
  	      alert("Price Quote Uploaded Successfully");
	}

}
var http2 = getXMLHttpRequestObject();
function getXMLHttpRequestObject()
{
  var xmlhttp;
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    try {
      xmlhttp = new XMLHttpRequest();
    } catch (e) {
      xmlhttp = false;
    }
  }
  return xmlhttp;
}
</script>
<table class="detailTabLabel" cellspacing="3" cellpadding="0" style="margin:0px;">
<tr>
<td colspan="5" >
<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin:0px;width:100%;">
<tr>
<td align="left" width="90"><img align="left" id="cabinetDiv" onclick="cabinetUpload('${pricePointShipNumber}','${marketAreaName}','${pricingVolume}','${pricingWeight}','${pricePointVolumeUnit}','${pricePointWeightUnit}','${pricePointMinimum_density}','${pricePointTariff_published}','${pricePointRate}','${pricePoint_price}','${pricePoint_cctotal}','${pricePoint_thc}','${packService}','${MarketagentId}','${pricePoint_currency}','${pricePoint_rate_basis}','${corpIdBaseCurrency}','${weight_unit}','${volume_unit}','${pricePoint_WeightUnit}','${pricePoint_VolumeUnit}');" class="openpopup"  src="<c:url value='/images/file-cabinet-market.png'/>" /></td>
<td align="left"><img align="left" id="printDiv" onclick="callPrint();" class="openpopup"  src="<c:url value='/images/print_16.gif'/>" /></td>
<td></td>
<td></td>
<td  align="right"><img align="right" class="openpopup" onclick="closeMyDiv('${pricePoint_price}','${MarketagentId}','${pricePointTariff}')" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="bgblue" colspan="5">
<table cellspacing="0" cellpadding="0" style="margin:0px;width:100%;" class="">
<tbody><tr>
<td align="left">Price&nbsp;Quote</td>
<td colspan="3" align="right">
<div style="float:right;padding-right:3px;">
<table cellspacing="0" cellpadding="0" style="margin:0px;width:100%;" class="">
<tr>
<td align="right">Agent/Service:&nbsp;</td>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='name'}">
<td width="" class=""><c:out value="${entry.value}"/>/<c:forEach var="entry" items="${detailsQuotesValue}"><c:if test="${entry.key=='packService'}"><c:if test="${entry.value=='origin'}">Origin</c:if><c:if test="${entry.value=='destination'}">Destination</c:if></c:if></c:forEach>
</td>
</c:if>
</c:forEach>
</tr>
</table>
</div>
</td>
</tr>
</tbody></table>
</td>
</tr>
<!--<tr>
<c:set var="now" value="<%=new java.util.Date()%>" />
<td class="listwhitetext" >As on:&nbsp;<fmt:formatDate type="date" value="${now}" /></td>
</tr>
--><tr>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='ShipNumber'}">
<td width="" class="listwhitetext" valign=""><c:out value="${entry.value}"/>:&nbsp;<c:forEach var="entry" items="${detailsQuotesValue}"><c:if test="${entry.key=='Transferee'}"><c:out value="${entry.value}"/></c:if></c:forEach></td>
</c:if>
</c:forEach>
<td width="200"></td>

<td valign="top">
<table style="margin:0px;">
<tr>
<td width="" class="listwhitetext">Origin:&nbsp;</td>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='originResidence'}">
<td width="" class="listwhitetext"><c:out value="${entry.value}"/></td>
</c:if>
</c:forEach>
</tr>
<tr>
<td width="" class="listwhitetext">Destination:&nbsp;</td>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='Residence'}">
<td width="" class="listwhitetext"><c:out value="${entry.value}"/></td>
</c:if>
</c:forEach>
</tr>
</table>
</td>

</tr>

<!--<tr>
<td class="listwhitetext">Status</td>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='Status'}">
<td width="100" class="listwhitetext" ><c:out value="${entry.value}"/></td>
</c:if>
</c:forEach>
<td width="" class="listwhitetext">Service</td>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='packService'}">
<c:if test="${entry.value=='origin'}">
<td width="" class="listwhitetext">Origin</td>
</c:if>
<c:if test="${entry.value=='destination'}">
<td width="" class="listwhitetext">Destination</td>
</c:if>
</c:if>
</c:forEach>
</tr>
<tr>
<td class="listwhitetext">Account</td>
<td width="100"></td>
<td width="" class="listwhitetext"></td>
<td width="" class="listwhitetext"></td>
</tr>
--></table>
<table class="detailTabLabel" cellspacing="3" cellpadding="0" style="margin:0px;">
<tr>
<td class="bgblue" colspan="4">Shipment</td>
</tr>
<tr>
<td></td>
</tr>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='pricingForMode'}">
<tr>
<td class="listwhitetext">Mode</td>
<td width="50"></td>
<td width="" class="listwhitetext" style="text-align:right;"><c:out value="${entry.value}"/></td>
<td width="" class="listwhitetext"></td>
</tr>
</c:if>
</c:forEach>
<tr>
<td class="listwhitetext">Market Area</td>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='marketAreaName'}">
<td width=""></td>
<td width="102" class="listwhitetext" style="text-align:right;"><c:out value="${entry.value}"/></td>
</c:if>
</c:forEach>
<td width="" class="listwhitetext"></td>
<td width="" class="listwhitetext"></td>
</tr>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='pricingVolume'}">
<tr>
<td class="listwhitetext">Declared Volume&nbsp;(<c:if test="${volume_unit=='mt'}">cbm</c:if><c:if test="${volume_unit=='cft'}">cft</c:if>)</td>
<td width=""></td>
<td width="" class="listwhitetext" style="text-align:right;"><c:out value="${entry.value}"/></td>
<td width="" class="listwhitetext"></td>
</tr>
</c:if>
</c:forEach>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='pricingWeight'}">
<tr>
<td class="listwhitetext">Declared Weight&nbsp;(${weight_unit})</td>
<td width=""></td>
<td width="" class="listwhitetext" style="text-align:right;"><c:out value="${entry.value}"/></td>
<td width="" class="listwhitetext"></td>
</tr>
</c:if>
</c:forEach>
<tr>
<td class="listwhitetext">Chargeable Volume&nbsp;(<c:forEach var="entry" items="${detailsQuotesValue}"><c:if test="${entry.key=='volumeUnit'}"><c:out value="${entry.value}"/></c:if></c:forEach>)</td>
<td width=""></td>
<td width="" class="listwhitetext" style="text-align:right;" ><c:forEach var="entry" items="${detailsQuotesValue}"><c:if test="${entry.key=='chargeable_volume'}"><fmt:formatNumber value="${entry.value}" maxFractionDigits="2" minFractionDigits="2" /></c:if></c:forEach></td>
<td width="" class="listwhitetext"></td>
</tr>
<tr>
<td class="listwhitetext">Chargeable Weight&nbsp;(<c:forEach var="entry" items="${detailsQuotesValue}"><c:if test="${entry.key=='weightUnit'}"><c:out value="${entry.value}"/></c:if></c:forEach>)</td>
<td width=""></td>
<td width="" class="listwhitetext" style="text-align:right;" ><c:forEach var="entry" items="${detailsQuotesValue}"><c:if test="${entry.key=='chargeable_weight'}"><fmt:formatNumber value="${entry.value}" maxFractionDigits="2" minFractionDigits="2"/></c:if></c:forEach></td>
<td width="" class="listwhitetext"></td>
</tr>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='minimum_density'}">
<tr>
<td class="listwhitetext">Minimum Density</td>
<td width=""></td>
<td width="" class="listwhitetext" style="text-align:right;"><c:out value="${entry.value}"/></td>
<td width="" class="listwhitetext"></td>
</tr>
</c:if>
</c:forEach>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='tariff_published'}">
<tr>
<td class="listwhitetext">Tariff Published</td>
<td width=""></td>
<td width="" class="listwhitetext" style="text-align:right;"><c:out value="${entry.value}"/></td>
<td width="" class="listwhitetext"></td>
</tr>
</c:if>
</c:forEach>
<tr>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='rate_basis'}">
<td class="listwhitetext" ><c:out value="${entry.value}"/></td>
</c:if>
</c:forEach>
<td width=""></td>
<td style="text-align:right;" class="listwhitetext">
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='currency'}">
<c:out value="${entry.value}"/>
</c:if>
</c:forEach>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='rate'}">
<fmt:formatNumber value="${entry.value}" maxFractionDigits="2" minFractionDigits="2" />
</c:if>
</c:forEach>
</td>
<td width="" class="listwhitetext"></td>
</tr>
<c:if test="${packService=='destination'}">
<tr>
<td class="listwhitetext">Destination THC</td>
<td width=""></td>
<td style="text-align:right;" class="listwhitetext">
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='currency'}"><c:out value="${entry.value}"/></c:if>
</c:forEach>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='thc'}">
<fmt:formatNumber value="${entry.value}" maxFractionDigits="2" minFractionDigits="2" />
</c:if>
</c:forEach>
</td>
<td width="" class="listwhitetext"></td>
</tr>
</c:if>
<tr>
<td class="listwhitetext">Invoice Total</td>
<td width=""></td>
<td style="text-align:right;" class="listwhitetext">
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='currency'}"><c:out value="${entry.value}"/></c:if>
</c:forEach>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='price'}">
<fmt:formatNumber value="${entry.value}" maxFractionDigits="2" minFractionDigits="2" />
</c:if>
</c:forEach>
</td>
<td width="" class="listwhitetext"></td>
</tr>
<tr>
<td class="listwhitetext">Converted Currency Total</td>
<td width=""></td>
<td style="text-align:right;" class="listwhitetext">
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='corpIdBaseCurrency'}"><c:out value="${entry.value}"/></c:if>
</c:forEach>
<c:forEach var="entry" items="${detailsQuotesValue}">
<c:if test="${entry.key=='cctotal'}">
<fmt:formatNumber value="${entry.value}" maxFractionDigits="2" minFractionDigits="2" />
</c:if>
</c:forEach>

</td>
<td width="" class="listwhitetext"></td>
</tr>
<tr>
<td width="" class="listwhitetext"></td>
</tr>
<tr>
<tr>
<td class="listwhitetext" colspan="4">*Destination THC is estimate only, to be Charged at actual. 3rd party invoice backup required if higher amount. </td>
</tr>
</table>
<table class="" cellspacing="0" cellpadding="0" style="margin:0px;">
<!--<tr>
<td colspan="4" align="right""><img align="right" class="openpopup" onclick="closeMyDiv()" src="<c:url value='/images/closetooltip.gif'/>" /></td></tr>
<tr>
--><td class="bgblue" colspan="4">Supplemental Charges</td>

</tr>
</table>
<div id="supplementQuotesDetails12" >
<table class="" cellspacing="0" cellpadding="0" style="margin:0px;width:100%;">
<tr>
<td>
    <table class="HMtable" cellspacing="1" style="margin:0px;">
        <tr>
            <td class="head" style="width:161px;">Type</td>
            <td class="head" style="width:9px;">Rate(<c:forEach var="entry" items="${detailsQuotesValue}"><c:if test="${entry.key=='currency'}"><c:out value="${entry.value}"/></c:if></c:forEach>)</td>
            <td class="head" style="width:201px;">Rate Basis</td>
        </tr>
    </table>
      <div class="wrap">
    <div class="inner_table">
        <table class="HMtable" style="margin:0px;border:2px solid #74B3DC;border-top:none;">
         <c:if test="${supplementQuotesList!='[]'}"> 
			<c:forEach var="individualItem" items="${supplementQuotesList}" varStatus="rowCounter">
			<c:if test="${rowCounter.count % 2 == 0}">
			 	<tr class="even">
			</c:if>
			<c:if test="${rowCounter.count % 2 != 0}">
				<tr class="odd">
			</c:if>
			 		<td style="width:118px;" >					
						<c:out value="${individualItem.type}"/>
					</td>
			 		<td align="right" style="width:11px;" ><c:out value="${individualItem.rate}"/></td>
			 		<td style="width:148px;"><c:out value="${individualItem.basis}"/></td>
				</tr>
			</c:forEach>
		 </c:if>
	     <c:if test="${supplementQuotesList=='[]'}"> 
	     <div class="EmptyHM">Nothing found to display.</div>
	     </c:if>
    </table>
    </div>
</div>
</td>
</tr>
</table>
</div>
<div style="font-family: arial,verdana;height:auto;border:2px solid #3DAFCB;border-radius:5px;padding:0px;margin:3px 0px;">
<table width="100%" cellspacing="0" cellpadding="0" style="margin:0px;padding:0px;background:#BCD2EF;border-bottom:1px solid #3DAFCB;">
<tr><td class="listwhitetext" style="padding:3px;padding-bottom:4px;font-size: 12px;"><b>&nbsp;Agent Notes</b></td></tr>
</table>
<table class="" cellspacing="0" cellpadding="0" style="margin:0px;padding-left:5px;padding-top:3px;padding-right:3px;">
<c:if test="${agentConsignment!=''}">
<tr>
<td class="listwhitetext" style="padding-bottom:8px;line-height:15px;font-size:11px;" >${agentConsignment}</td>
</tr>
</c:if>
<c:if test="${agentAdditional!=''}">
<tr>
<td class="listwhitetext" style="padding-bottom:8px;line-height:15px;font-size:11px;" >${agentAdditional}</td>
</tr>
</c:if>
<c:if test="${agentcustoms!=''}">
<tr>
<td class="listwhitetext" style="padding-bottom:2px;line-height:15px;font-size:11px;" >${agentcustoms}</td>
</tr>
</c:if>
</table>
</div>
<div id="supplementQuotesDetails123" >
</div>
<s:form name="pricePoint">
<s:hidden name="agentcustoms" value="${agentcustoms}"></s:hidden>
<s:hidden name="agentConsignment" value="${agentConsignment}"></s:hidden>
<s:hidden name="agentAdditional" value="${agentAdditional}"></s:hidden>
<s:hidden name="pricePointAgent" value="${pricePointAgent}"></s:hidden>
<s:hidden name="SupplementRate_basis" value="${SupplementRate_basis}"></s:hidden>
<s:hidden name="SupplementRate" value="${SupplementRate}"></s:hidden>
<s:hidden name="SupplementType" value="${SupplementType}"></s:hidden>

</s:form>