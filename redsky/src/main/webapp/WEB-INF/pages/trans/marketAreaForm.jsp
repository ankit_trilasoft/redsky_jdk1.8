<%@ include file="/common/taglibs.jsp"%>
<%@ include file="/common/tooltip.jsp"%> 
<title>Pricing Market Form</title>
<meta name="heading" content="Pricing Market Form" />
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.10.1.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-migrate-1.0.0.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery.printElement.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery.tablesorter.js"></script> 
<script>
function callPrint() {
	//$("#quotesDetails").addClass("class1");	
	//$("#quotesDetails").printThis();
	$('#quotesDetails').printElement(
			{
	            printBodyOptions:
	            {
	            styleToAdd:'border: 5px solid #74B3DC',
	            classNameToAdd : 'class1'
	            }
	            }
			);		
	}
</script>

<style >
#overlayMarket {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}

#overlay {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:90; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
.class1 {
   display: block; /* ensures it’s invisible until it’s called */
    position: absolute; /* makes the div go into a position that’s absolute to the browser viewing area */
    left: 20%; /* positions the div half way horizontally */
    top: 2%; /* positions the div half way vertically */
    padding: 25px; 
    border: 5px solid #74B3DC;
    background-color: #ffffff;
    width: 60%;
    height: 500px;
    overflow:auto; 
    z-index: 100; /* makes the div the top layer, so it’ll lay on top of the other content */
}
.class2 {
    display: none;  /* ensures it’s invisible until it’s called */
  position:relative; ;  /*makes the div go into a position that’s absolute to the browser viewing area */
    left: 0%; /* makes the div span all the way across the viewing area */
    top: 0%; /* makes the div span all the way across the viewing area */
   background-color: black; 
    -moz-opacity: 1; /* makes the div transparent, so you have a cool overlay effect */
    opacity: 1.70;
     width: 100%;
    height: 100%;
    filter:alpha(opacity=70);
	-khtml-opacity: 1.7;
  	margin:0px auto -1px auto;
   	z-index: 90;  /* makes the div the second most top layer, so it’ll lay on top of everything else EXCEPT for divs with a higher z-index (meaning the #overlay ruleset) */
}
.class3 {
   display: block; /* ensures it’s invisible until it’s called */
    position: absolute; /* makes the div go into a position that’s absolute to the browser viewing area */
    left: 25%; /* positions the div half way horizontally */
    top: 5%; /* positions the div half way vertically */
    padding: 25px; 
    border: 5px solid #74B3DC;
    background-color: #ffffff;
    width: 50%;
    height: 450px; 
    z-index: 100; /* makes the div the top layer, so it’ll lay on top of the other content */
}
</style>
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
<s:form id="marketPlaceForm" name="marketPlaceForm" action="" onsubmit="" method="post">
<s:hidden name="pricingMode" value="<%=request.getParameter("pricingMode") %>" />
<s:hidden name="pricingWeight" value="<%=request.getParameter("pricingWeight") %>" />
<s:hidden name="pricingVolume" value="<%=request.getParameter("pricingVolume") %>" />
<s:hidden name="pricingStorageUnit" value="<%=request.getParameter("pricingStorageUnit") %>" />
<s:hidden name="packService" value="<%=request.getParameter("packService") %>" />
<s:hidden name="vendorCodeId" value="<%=request.getParameter("vendorCodeId") %>" />
<s:hidden name="vendorNameId" value="<%=request.getParameter("vendorNameId") %>" />
<s:hidden name="estimateRateid" value="<%=request.getParameter("estimateRateid") %>" />
<s:hidden name="packingMode" value="<%=request.getParameter("packingMode") %>" />
<s:hidden name="jobType" value="<%=request.getParameter("jobType") %>" />
<s:hidden name="soid" value="<%=request.getParameter("soid") %>" />
<s:hidden name="aid" value="<%=request.getParameter("aid") %>" />
<s:hidden name="contractType" value="<%=request.getParameter("contractType") %>" />
<s:hidden name="countryMarket"></s:hidden>
<s:hidden name="marketAreaName"></s:hidden>
<div id="marketAreaDetailsFrom">
 <div id="otabs">
  <ul>
    <li><a class="current"><span>Market&nbsp;Area</span></a></li>
  </ul>
</div>
<div class="spnblk">&nbsp;</div>        
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:10px; "><span></span></div>
    <div class="center-content">    
      <table border="0" style="margin:0px;padding:0px;">
      <tr>
      <td class="listwhitetext">Country</td>
      <td width="30"></td>
      <td class="listwhitetext">Market Area</td>
      </tr>
         <tr>
         <td align="left" ><s:select id="countryDD" name="ocountry" cssClass="list-menu" cssStyle="width:200px" list="%{ocountry}" headerKey="" onchange="getMarketArea(this);" headerValue="" tabindex="1"  /></td>
         <td width="30"></td>
         <td align="left" ><s:select id="countryDD1" name="marketArea" cssClass="list-menu" cssStyle="width:200px" list="%{marketAreaList}" headerKey=""  headerValue="" tabindex="1"  /></td>
         </tr>
         <tr>
         <td></td>
         </tr>
           <tr>
         <td></td>
         </tr>
         </table>
    </div>
<div class="bottom-header" style="margin-top:40px;"><span></span></div>
    </div>
    </div>
<div id="mydiv" style="position:absolute;margin-top:-28px;"></div>
<table>
<tr>
<!--<td><input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Submit" onclick="return checkValidation();"/></td>
--><td><input type="button"  value="Cancel" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();"></td>
</tr>
</table>
</div>
<div id="priceDetailsView" style="height:100%;">
</div>
<div id="quotesDetails" > </div>
<div id="quotesSupplement" > </div>
<div id="temp" > </div>
<div id="overlay" style="display:none">
  
   </div>
<div id="overlayMarket" style="display:none">
<div id="layerLoading">
<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
       <img src="<c:url value='/images/ajax-loader-hr.gif'/>" />       
       </td>
       </tr>
     </table>
   </td>
  </tr>
</table>  
   </div>   
   </div>
</s:form>

<script type="text/javascript">
function mappedAgent(){
	alert('Agent is not mapped in the system and cannot be selected.\n Please contact support@redskymobility.com.');
}
function closeMyDiv(price,marketagent,pricePointTariff){
	//alert(price)
	//alert(marketagent)
	document.getElementById('tariffId'+marketagent).value=pricePointTariff;
	document.getElementById('invoiceTotal'+marketagent).value=price;
	document.getElementById("overlay").style.display = "none";
	document.getElementById("quotesDetails").style.display = "none";
	document.getElementById("quotesSupplement").style.display = "none";
	document.getElementById("temp").style.display = "none";
	document.getElementById("priceDetailsView").style.display = "block";
	document.getElementById("quotesDetails").className  = "";
	
    document.getElementById("temp").className  = "";
}

function checkQuoteDetails(agentId,marketArea,currency,valueTotal){	
	var packService = document.forms['marketPlaceForm'].elements['packService'].value;
	var packingMode = document.forms['marketPlaceForm'].elements['packingMode'].value;
	var pricingMode = document.forms['marketPlaceForm'].elements['pricingMode'].value;
	var pricingWeight = document.forms['marketPlaceForm'].elements['pricingWeight'].value;
    var pricingVolume = document.forms['marketPlaceForm'].elements['pricingVolume'].value;
    var pricingStorageUnit = document.forms['marketPlaceForm'].elements['pricingStorageUnit'].value;
    var jobType = document.forms['marketPlaceForm'].elements['jobType'].value;
    var soid = document.forms['marketPlaceForm'].elements['soid'].value;  
    var countryCode=document.forms['marketPlaceForm'].elements['countryMarket'].value; 
    var marketAreaName =document.forms['marketPlaceForm'].elements['marketAreaName'].value;
    //alert("marketArea "+marketAreaName)
      //var url = 'findSupplementAjax.html?ajax=1&marketAreaCode='+marketArea+'&MarketagentId='+agentId+'&packService='+packService+'&packingModePricing='+packingMode+'&pricingMode='+pricingMode+'&pricingWeight='+pricingWeight+'&pricingVolume='+pricingVolume+'&pricingStorageUnit='+pricingStorageUnit+'&decorator=simple&popup=true';
   //ajax_showTooltip(url,position);
   
   	$.get("findQuoteDetailsAjax.html?ajax=1&decorator=simple&popup=true", 
					{ corpIdBaseCurrency:currency,marketAreaCode:marketArea,marketAreaName:marketAreaName,jobType:jobType,MarketagentId:agentId,marketCountryCode:countryCode,packService:packService,packingModePricing:packingMode,pricingMode:pricingMode,pricingWeight:pricingWeight,pricingVolume:pricingVolume,pricingStorageUnit:pricingStorageUnit,soId:soid,cctotalValue:valueTotal},
					function(data){
						document.getElementById("quotesDetails").style.display = "block";
					       document.getElementById("quotesDetails").className  = "class1";
					      document.getElementById("overlay").style.display = "block";
					      document.getElementById("overlayMarket").style.display = "none";
						$("#quotesDetails").html(data);
					
				});
   
   
    //new Ajax.Request('findQuoteDetailsAjax.html?ajax=1&corpIdBaseCurrency='+currency+'&marketAreaCode='+marketArea+'&marketAreaName='+marketAreaName+'&jobType='+jobType+'&MarketagentId='+agentId+'&marketCountryCode='+countryCode+'&packService='+packService+'&packingModePricing='+packingMode+'&pricingMode='+pricingMode+'&pricingWeight='+pricingWeight+'&pricingVolume='+pricingVolume+'&pricingStorageUnit='+pricingStorageUnit+'&soId='+soid+'&decorator=simple&popup=true',
    		//{
	    //method:'get',
	   // onSuccess: function(transport){
    	//document.getElementById("quotesDetails").style.display = "block";
    	// document.getElementById("quotesDetails").className  = "class1";
	      //document.getElementById("temp").className  = "class2";
	        // document.getElementById("overlay").style.display = "block";
	      //var response = transport.responseText || "no response text";
	      //alert(response);
	     // var container = document.getElementById("quotesDetails");
	      //pricingValue='';
	      //findAllPricingAjax();
	     // container.innerHTML = response;
	     // container.show();		  
	      //container.update(response);
	      //document.getElementById("marketArea").style.display = "none";
	      //document.getElementById("priceDetailsView").style.display = "Block";
	       //showOrHide(0);
	      		      
	      
	   // },
	    
	    //onLoading: function()
	    //   {
	    	//var loading = document.getElementById("loading").style.display = "block";
	    	
	    	// loading.innerHTML;
	   // },

	  //}); 
}

function getMarketArea(targetElement){
	var country = targetElement.value;
	var url="findMarketPlaceList.html?ajax=1&decorator=simple&popup=true&marketCountryCode=" + encodeURI(country);
    //alert(url);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse9;
     http3.send(null);
}
function handleHttpResponse9()
{

     if (http3.readyState == 4)
     {
        var results = http3.responseText
        //alert(results)
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['marketPlaceForm'].elements['marketArea'];
		targetElement.length = res.length;
			for(i=0;i<res.length;i++)
			{
			if(res[i] == ''){
			document.forms['marketPlaceForm'].elements['marketArea'].options[i].text = '';
			document.forms['marketPlaceForm'].elements['marketArea'].options[i].value = '';
			}else{
			stateVal = res[i].split("#");
			document.forms['marketPlaceForm'].elements['marketArea'].options[i].text = stateVal[1];
			document.forms['marketPlaceForm'].elements['marketArea'].options[i].value = stateVal[0];
			}
			}
		
     }
}
var http3 = getHTTPObject1();
function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}    

</script>
<script language="javascript">
try{
	document.getElementById("countryDD").value = '<%=request.getParameter("marketCountryCode")%>';
}catch(e){}
</script>
<script type="text/javascript">
function currentDateGlobal(){
	  var mydate=new Date();
	    var daym;
	    var year=mydate.getFullYear()
	    var y=""+year;
	    if (year < 1000)
	    year+=1900
	    var day=mydate.getDay()
	    var month=mydate.getMonth()+1
	    if(month == 1)month="Jan";
	    if(month == 2)month="Feb";
	    if(month == 3)month="Mar";
	   if(month == 4)month="Apr";
	   if(month == 5)month="May";
	   if(month == 6)month="Jun";
	   if(month == 7)month="Jul";
	   if(month == 8)month="Aug";
	   if(month == 9)month="Sep";
	   if(month == 10)month="Oct";
	   if(month == 11)month="Nov";
	   if(month == 12)month="Dec";
	   var daym=mydate.getDate()
	   if (daym<10)
	   daym="0"+daym
	   var datam = daym+"-"+month+"-"+y.substring(2,4); 
	   return datam;
	}
function setParentValue(charge,description,total,currUnit,foRate,focurr,agentId,marketArea,baseCurrency){
	var invoiceTotal="";
	invoiceTotal=document.getElementById('invoiceTotal'+agentId).value;
	var contractType=document.forms['marketPlaceForm'].elements['contractType'].value;
	//var invoiceTotalRate="";
	//alert(foRate)
    if(invoiceTotal!=''){
    //invoiceTotalRate=invoiceTotal/total;
    	//alert(new Date())
       	var vendorCodeId=document.forms['marketPlaceForm'].elements['vendorCodeId'].value;
    	var vendorNameId=document.forms['marketPlaceForm'].elements['vendorNameId'].value;
    	var estimateRateid=document.forms['marketPlaceForm'].elements['estimateRateid'].value;
    	var aid=document.forms['marketPlaceForm'].elements['aid'].value;	
    	window.opener.document.getElementById('pricePointAgent'+aid).value=agentId;
    	window.opener.document.getElementById('pricePointMarket'+aid).value=marketArea;
    	window.opener.document.getElementById('pricePointTariff'+aid).value=document.getElementById('tariffId'+agentId).value;    	
    	window.opener.document.getElementById('estExchangeRateNew'+aid).value=foRate;
    	window.opener.document.getElementById('estCurrencyNew'+aid).value=currUnit;
    	window.opener.document.getElementById('estimateRate'+aid).value=focurr;
    	//window.opener.document.getElementById('estLocalRateNew'+aid).value=focurr;
    	window.opener.document.getElementById('basis'+aid).value="flat";
    	window.opener.document.getElementById('estimateQuantity'+aid).value="1.00";	
    	window.opener.document.getElementById(vendorCodeId).value=charge; 
    	window.opener.document.getElementById(vendorNameId).value=description;
    	window.opener.document.getElementById(estimateRateid).value=focurr;
    	window.opener.document.getElementById('estValueDateNew'+aid).value=currentDateGlobal();
    	if(contractType=='true'){
    	window.opener.document.getElementById('estimatePayableContractValueDateNew'+aid).value=currentDateGlobal();
    	window.opener.document.getElementById('estimatePayableContractCurrencyNew'+aid).value=currUnit;
    	window.opener.document.getElementById('estimatePayableContractExchangeRateNew'+aid).value=foRate;
    	window.opener.document.getElementById('estimatePayableContractRateNew'+aid).value=focurr;
    	}
    	var estimateRate='estimateRate'+aid;
    	var estimateRevenueAmount='estimateRevenueAmount'+aid;
    	var estimateExpense='estimateExpense'+aid;
    	var estimatePassPercentage='estimatePassPercentage'+aid;
    	var displayOnQuote='displayOnQuote'+aid;
    	var vatPers='vatPers'+aid;
    	var vatAmt='vatAmt'+aid;
    	// creating variables for calculateRevenue method
    	var besis = 'basis'+aid;
    	var estQuty= 'estimateQuantity'+aid;
    	//window.opener.calculateRevenue(besis,estQuty,estimateSellRate,estimateRevenueAmount,estimateExpense,estimatePassPercentage,displayOnQuote,vatPers,vatAmt,aid);
    	window.opener.calculateExpense(besis,estQuty,estimateRate,estimateExpense,estimateRevenueAmount,estimatePassPercentage,displayOnQuote,vatPers,vatAmt,aid);
    	//changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}')"
    	//window.opener.setContractValue(); 
    	 self.close();
     }else{
    		var packService = document.forms['marketPlaceForm'].elements['packService'].value;    		
    		var packingMode = document.forms['marketPlaceForm'].elements['packingMode'].value;    	
    		var pricingMode = document.forms['marketPlaceForm'].elements['pricingMode'].value;    	
    		var pricingWeight = document.forms['marketPlaceForm'].elements['pricingWeight'].value;    	
    	    var pricingVolume = document.forms['marketPlaceForm'].elements['pricingVolume'].value;    	 
    	    var pricingStorageUnit = document.forms['marketPlaceForm'].elements['pricingStorageUnit'].value;    	
    	    var jobType = document.forms['marketPlaceForm'].elements['jobType'].value;    	   
    	    var soid = document.forms['marketPlaceForm'].elements['soid'].value; 
    	    var countryCode=document.forms['marketPlaceForm'].elements['countryMarket'].value; 
    	    var aid=document.forms['marketPlaceForm'].elements['aid'].value;
    	    //alert(countryCode)
    	   	 new Ajax.Request('findQuoteDetailsInvoice.html?ajax=1&corpIdBaseCurrency='+baseCurrency+'&marketAreaCode='+marketArea+'&marketCountryCode='+countryCode+'&jobType='+jobType+'&MarketagentId='+agentId+'&packService='+packService+'&packingModePricing='+packingMode+'&pricingMode='+pricingMode+'&pricingWeight='+pricingWeight+'&pricingVolume='+pricingVolume+'&pricingStorageUnit='+pricingStorageUnit+'&soId='+soid+'&decorator=simple&popup=true',
    	    		{
    		    method:'get',
    		    onSuccess: function(transport){    	    	
    		      var response = transport.responseText || "no response text";
    		      response=response.trim();
    		      //alert(response)
    		      var res=response.split("#");  
    		      if(res.length>1 && res[0]!=''&&res[1]!=''){
    		      invoiceTotal=res[0];
    		      //billingValue=focurr/foRate;
    		      //alert(invoiceTotal); 		   
    		    //  invoiceTotalRate=invoiceTotal/total;	
    		      //alert(invoiceTotalRate)
    		      window.opener.document.getElementById('pricePointTariff'+aid).value=res[1];
    		      ///alert('Tariff'+res[1])
    		      }
    		    window.opener.document.getElementById('pricePointAgent'+aid).value=agentId;
    		    window.opener.document.getElementById('pricePointMarket'+aid).value=marketArea;
    		  	var vendorCodeId=document.forms['marketPlaceForm'].elements['vendorCodeId'].value;
    		  	var vendorNameId=document.forms['marketPlaceForm'].elements['vendorNameId'].value;
    		  	var estimateRateid=document.forms['marketPlaceForm'].elements['estimateRateid'].value;    		  		
    		  	window.opener.document.getElementById('estExchangeRateNew'+aid).value=foRate;
    		  	window.opener.document.getElementById('estCurrencyNew'+aid).value=currUnit;
    		  	window.opener.document.getElementById('estimateRate'+aid).value=focurr;
    		  	//window.opener.document.getElementById('estLocalRateNew'+aid).value=focurr;
    		  	window.opener.document.getElementById('estValueDateNew'+aid).value=currentDateGlobal();
    		  	window.opener.document.getElementById('basis'+aid).value="flat";
    		  	window.opener.document.getElementById('estimateQuantity'+aid).value="1.00";	    		  	
    		  	window.opener.document.getElementById(vendorCodeId).value=charge; 
    		  	window.opener.document.getElementById(vendorNameId).value=description;
    		  	window.opener.document.getElementById(estimateRateid).value=focurr;
    			if(contractType=='true'){
    		    	window.opener.document.getElementById('estimatePayableContractValueDateNew'+aid).value=currentDateGlobal();
    		    	window.opener.document.getElementById('estimatePayableContractCurrencyNew'+aid).value=currUnit;
    		    	window.opener.document.getElementById('estimatePayableContractExchangeRateNew'+aid).value=foRate;
    		    	window.opener.document.getElementById('estimatePayableContractRateNew'+aid).value=focurr;
    		    	}
    		  	var estimateRate='estimateRate'+aid;
    		  	var estimateRevenueAmount='estimateRevenueAmount'+aid;
    		  	var estimateExpense='estimateExpense'+aid;
    		  	var estimatePassPercentage='estimatePassPercentage'+aid;
    		  	var displayOnQuote='displayOnQuote'+aid;
    		  	var vatPers='vatPers'+aid;
    		  	var vatAmt='vatAmt'+aid;
    		  	// creating variables for calculateRevenue method
    		  	var besis = 'basis'+aid;
    		  	var estQuty= 'estimateQuantity'+aid;
    		  	//window.opener.calculateRevenue(besis,estQuty,estimateSellRate,estimateRevenueAmount,estimateExpense,estimatePassPercentage,displayOnQuote,vatPers,vatAmt,aid);
    		  	window.opener.calculateExpense(besis,estQuty,estimateRate,estimateExpense,estimateRevenueAmount,estimatePassPercentage,displayOnQuote,vatPers,vatAmt,aid);
    		  	//changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}')"
    		  	//window.opener.setContractValue(); 
    		  	 self.close();     
    		     
    		    },    		    
    		    onLoading: function()
    		       {
    		    	 loading.innerHTML;
    		    },
    		    
    		    onFailure: function(){ 
    			    //alert('Something went wrong...')
    			     }
    		  });    	 
    	  // var defaultCurrency =document.getElementById("CurrencyDD1243").value;
    	
     }

}
function goBackTo(){
	 document.getElementById("marketArea").style.display = "Block";
	 document.getElementById("priceDetailsView").style.display = "none";
	 document.getElementById("quotesDetails").style.display = "none";
	 document.getElementById("quotesSupplement").style.display = "none";
}
</script>
<script language="javascript">
///mainPage on change
$(document).ready(function (){	
	$("#countryDD1").change(function(){
		//alert('sandha')
		 document.getElementById("overlayMarket").style.display = "block";
			var country = document.forms['marketPlaceForm'].elements['ocountry'].value;
			//alert("country "+country)
			var marketArea = document.forms['marketPlaceForm'].elements['marketArea'].value;
			if(country!='' && marketArea!=''){				
			var marketName =document.getElementById('countryDD1').options[document.getElementById('countryDD1').selectedIndex].text;
		    //alert("marketArea "+marketName)
			var pricingMode = document.forms['marketPlaceForm'].elements['pricingMode'].value;
		    // alert("pricingMode "+pricingMode)
			var pricingWeight = document.forms['marketPlaceForm'].elements['pricingWeight'].value;
		     //alert("pricingWeight "+pricingWeight)
			var pricingVolume = document.forms['marketPlaceForm'].elements['pricingVolume'].value;
		     //alert("pricingVolume "+pricingVolume)
			var pricingStorageUnit = document.forms['marketPlaceForm'].elements['pricingStorageUnit'].value;
		     //alert("pricingStorageUnit "+pricingStorageUnit)
			var packService = document.forms['marketPlaceForm'].elements['packService'].value;
			var packingMode = document.forms['marketPlaceForm'].elements['packingMode'].value;
			var jobType = document.forms['marketPlaceForm'].elements['jobType'].value;
			document.forms['marketPlaceForm'].elements['countryMarket'].value=document.forms['marketPlaceForm'].elements['ocountry'].value;
			document.forms['marketPlaceForm'].elements['marketAreaName'].value=marketName;
			var textValue="";
			if(country=='' && marketArea ==''){
				alert("Please select Country and Market Area.");
				textValue="AA";
			     document.getElementById("overlayMarket").style.display = "none";
				return false;
			}else if(country==''){
				alert("Please select Country.");
				textValue="AA";
			     document.getElementById("overlayMarket").style.display = "none";
				return false;
			}else if(marketArea ==''){
				alert("Please select Market Area.");
				textValue="AA";
			     document.getElementById("overlayMarket").style.display = "none";
				return false;
			}			 
			      var container = document.getElementById("marketAreaDetailsFrom");
			if(textValue==''){
				//var result = document.getElementById("marketAreaDetailsFrom");
				
				$.get("findMarketplaceAjax.html?ajax=1&decorator=simple&popup=true&page="+page, 
					{ marketArea: marketArea, marketCountryCode: country,jobType:jobType, packingModePricing: packingMode, packService: packService, pricingMode: pricingMode, pricingWeight: pricingWeight, pricingVolume: pricingVolume, pricingStorageUnit: pricingStorageUnit},
					function(data){
						
						$("#marketAreaDetailsFrom").html(data);
				document.getElementById("overlayMarket").style.display = "none";
				});
			}		
			}else{
				alert("Please select Country and Market Area.....");
				document.getElementById("overlayMarket").style.display = "none";
			}
	});
	
})
</script>
<script language="javascript">
function priceDetailsChange(target){
		//alert('sandha1')
	    document.getElementById("overlayMarket").style.display = "block";
		var country =document.forms['marketPlaceForm'].elements['countryMarket'].value;
		//alert("country "+country)
		var marketArea =document.getElementById("countryMarketAreaDD1243").value;
		//alert(marketArea)				
		var marketName =document.getElementById('countryMarketAreaDD1243').options[document.getElementById('countryMarketAreaDD1243').selectedIndex].text;
		var defaultCurrency =document.getElementById("CurrencyDD1243").value;
		if(marketArea!='' && defaultCurrency!=''){	
	    //alert("marketArea "+marketArea)
	    //var defaultCurrency ='';
		var pricingMode = document.forms['marketPlaceForm'].elements['pricingMode'].value;
	    // alert("pricingMode "+pricingMode)
		var pricingWeight = document.forms['marketPlaceForm'].elements['pricingWeight'].value;
	     //alert("pricingWeight "+pricingWeight)
		var pricingVolume = document.forms['marketPlaceForm'].elements['pricingVolume'].value;
	     //alert("pricingVolume "+pricingVolume)
		var pricingStorageUnit = document.forms['marketPlaceForm'].elements['pricingStorageUnit'].value;
	     //alert("pricingStorageUnit "+pricingStorageUnit)
		var packService = document.forms['marketPlaceForm'].elements['packService'].value;
		var packingMode = document.forms['marketPlaceForm'].elements['packingMode'].value;
		var jobType = document.forms['marketPlaceForm'].elements['jobType'].value;
		document.forms['marketPlaceForm'].elements['marketAreaName'].value=marketName;		
				$.get("findMarketplaceAjax.html?ajax=1&decorator=simple&popup=true&page="+page, 
					{corpIdBaseCurrency: defaultCurrency, marketArea: marketArea, marketCountryCode: country,jobType:jobType, packingModePricing: packingMode, packService: packService, pricingMode: pricingMode, pricingWeight: pricingWeight, pricingVolume: pricingVolume, pricingStorageUnit: pricingStorageUnit},
					function(data){
						
						$("#marketAreaDetailsFrom").html(data);
				document.getElementById("overlayMarket").style.display = "none";
				});
		}else{
			alert("Please select Market Area and Quote Currency.....");
			document.getElementById("overlayMarket").style.display = "none";
		}
					
}	
function priceDetailsCurrencyChange(target){
	//alert('sandha2')
	 document.getElementById("overlayMarket").style.display = "block";
		var country =document.forms['marketPlaceForm'].elements['countryMarket'].value;
		//alert("country "+country)
		var marketArea=document.getElementById("countryMarketAreaDD1243").value;
		var defaultCurrency = target.value;
	    //alert("marketArea "+marketArea)
	    if(marketArea!='' && defaultCurrency!=''){	
		var pricingMode = document.forms['marketPlaceForm'].elements['pricingMode'].value;
	    // alert("pricingMode "+pricingMode)
		var pricingWeight = document.forms['marketPlaceForm'].elements['pricingWeight'].value;
	     //alert("pricingWeight "+pricingWeight)
		var pricingVolume = document.forms['marketPlaceForm'].elements['pricingVolume'].value;
	     //alert("pricingVolume "+pricingVolume)
		var pricingStorageUnit = document.forms['marketPlaceForm'].elements['pricingStorageUnit'].value;
	     //alert("pricingStorageUnit "+pricingStorageUnit)
		var packService = document.forms['marketPlaceForm'].elements['packService'].value;
		var packingMode = document.forms['marketPlaceForm'].elements['packingMode'].value;
		var jobType = document.forms['marketPlaceForm'].elements['jobType'].value;
		//document.forms['marketPlaceForm'].elements['countryMarket'].value=document.forms['marketPlaceForm'].elements['ocountry'].value;
			$.get("findMarketplaceAjax.html?ajax=1&decorator=simple&popup=true&page="+page, 
					{corpIdBaseCurrency: defaultCurrency, marketArea: marketArea, marketCountryCode: country,jobType:jobType, packingModePricing: packingMode, packService: packService, pricingMode: pricingMode, pricingWeight: pricingWeight, pricingVolume: pricingVolume, pricingStorageUnit: pricingStorageUnit},
					function(data){						
						$("#marketAreaDetailsFrom").html(data);
				document.getElementById("overlayMarket").style.display = "none";
				});
	    }else{
	    	alert("Please select Market Area and Quote Currency.....");
			document.getElementById("overlayMarket").style.display = "none";
	    }
}
</script>

