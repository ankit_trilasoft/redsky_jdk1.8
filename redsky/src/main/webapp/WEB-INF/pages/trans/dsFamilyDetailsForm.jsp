<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<head>
    <title>Family Details</title>
    <meta name="heading" content="Family Details:"/>
    <style><%@ include file="/common/calenderStyle.css"%>
    .textareaUpper {
    background-color: #E3F1FE; border: 1px solid #ABADB3; color: #000000; font-family: arial,verdana;font-size: 12px; text-decoration: none;}
    </style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->   


<script>
function iehack(){
	if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){ 
		var ieversion=new Number(RegExp.$1) 
			var elementList = document.getElementsByTagName('*');
			var imageList = document.getElementsByTagName('img');
			 for(var i = 0 ; i < imageList.length ; i++){
				 if(imageList[i].nodeName == 'IMG'){
					if(imageList[i].src.indexOf('calender.png') > 0){
						imageList[i].src = 'images/navarrow.gif'; 
						var elementID  = imageList[i].id;
						if(elementID.indexOf('trigger')>0){
							imageList[i].style.display = 'none';
						}
					}
				 }
			 }
			 for(var i in elementList){
				 if(elementList[i].type == 'text'){
						 elementList[i].readOnly = true;
						 elementList[i].className = 'input-textUpper';
					}
				 if(elementList[i].type == 'checkbox'){
					 elementList[i].disabled =true;
					}
				 var radioElement = document.getElementsByName('dsFamilyDetails.gender');
				 radioElement[0].disabled = true;
				 radioElement[1].disabled = true;
				 if(elementList[i].type == 'select-one')
					{
						 elementList[i].disabled =true;
						 elementList[i].style.className = 'list-menu';
					}
				 if(elementList[i].type=='button'){
						 elementList[i].style.display = 'none';
						var fieldSetArray = document.getElementsByTagName('fieldset');
						for(var j = 0 ; j < fieldSetArray.length ; j++)
								fieldSetArray[j].style.display = 'none'; 
					}	
			}
	  }
}

function trap() 
{ 
if(document.images)
  {
    var totalImages = document.images.length;
    	for (var i=0;i<totalImages;i++)
			{
				if(document.images[i].src.indexOf('calender.png')>0)
				{
					
					var el = document.getElementById(document.images[i].id);  
					document.images[i].src = 'images/navarrow.gif'; 
					if((el.getAttribute("id")).indexOf('trigger')>0){
						el.removeAttribute("id");
					}
				}
				if(document.images[i].src.indexOf('open-popup.gif')>0)
				{ 
					
						var el = document.getElementById(document.images[i].id);
						//alert(el)
						try{
						el.onclick = false;
						}catch(e){}
					    document.images[i].src = 'images/navarrow.gif';
				}
				if(document.images[i].src.indexOf('notes_empty1.jpg')>0)
				{
						var el = document.getElementById(document.images[i].id);
						//alert(el)
						try{
						el.onclick = false;
						}catch(e){}
				        document.images[i].src = 'images/navarrow.gif';
				}
				if(document.images[i].src.indexOf('notes_open1.jpg')>0)
				{
					var el = document.getElementById(document.images[i].id);
					try{
					el.onclick =false;
					}catch(e){}
					document.images[i].src = 'images/navarrow.gif';
				}
				
									
				if(document.images[i].src.indexOf('images/nav')>0)
				{
					var el = document.getElementById(document.images[i].id);
					try{
					el.onclick = false;
					}catch(e){}
					document.images[i].src = 'images/navarrow.gif';
				}
			
			}
			
			var elementsLen=document.forms['dsFamilyDetailsForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['dsFamilyDetailsForm'].elements[i].type=='text')
					{
						document.forms['dsFamilyDetailsForm'].elements[i].readOnly =true;
						document.forms['dsFamilyDetailsForm'].elements[i].className = 'input-textUpper';						
					}
					else if(document.forms['dsFamilyDetailsForm'].elements[i].type=='textarea'){
						document.forms['dsFamilyDetailsForm'].elements[i].readOnly =true;
						document.forms['dsFamilyDetailsForm'].elements[i].className = 'textareaUpper';												
					}else					{
					 document.forms['dsFamilyDetailsForm'].elements[i].disabled=true;
					}	
			}  
		  }
  	document.forms['dsFamilyDetailsForm'].setAttribute('bypassDirtyCheck',false);
}
function pick() {
	
	try{
		parent.window.opener.document.forms['customerFileForm'].elements['customerFile.custPartnerFirstName'].value = '${customerFile.custPartnerFirstName}' ;
   		parent.window.opener.document.forms['customerFileForm'].elements['customerFile.custPartnerLastName'].value = '${customerFile.custPartnerLastName}';
   		parent.window.opener.document.forms['customerFileForm'].elements['customerFile.custPartnerContact'].value = '${customerFile.custPartnerContact}';
   		parent.window.opener.document.forms['customerFileForm'].elements['customerFile.custPartnerEmail'].value = '${customerFile.custPartnerEmail}';
   		parent.window.opener.document.forms['customerFileForm'].elements['customerFile.custPartnerPrefix'].value = '${customerFile.custPartnerPrefix}';
	}catch(e){
		
	}
//  		parent.window.opener.document.location.reload();
		try {
			window.opener.getFamilyDetails();
			// window.opener.clickOnRequest();
		}catch(e){
			
			//window.opener.document.forms[0].submit();
			parent.window.opener.document.location.reload();
		}
  		window.close();
	}
	
 function valButton(btn) {
    var cnt = -1;
    for (var i=btn.length-1; i > -1; i--) {
        if (btn[i].checked) {cnt = i; i = -1;}
    }
    if (cnt > -1) return btn[cnt].value;
    else return null;
}  
function validation(){

	var nationality = document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.nationality'].value;
	var dateOfBirth = document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.dateOfBirth'].value;
var btn = valButton(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.gender']);
var firstName = document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.firstName'].value;
var lastName = document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.lastName'].value;
if(firstName.trim() == ''){
	   alert("First Name is a required field ")
	   return false;
	 }else if(lastName.trim() == ''){
	   alert("Last Name is a required field ")
	   return false;
	 } 
	 else if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relationship'].value==''){
	   alert("Relationship is a required field ")
	   return false;
	 } else  if (btn == null) 
	 {
		 alert("Please select Gender"); 
		  return false;
	 } 
  <configByCorp:fieldVisibility componentId="component.customerFile.IkeaRequest">
	 else if(dateOfBirth == '')	{
		 alert("Date Of Birth is a required field ")
		 return false;
	 }
	  else if(nationality == '') {
         alert("Nationality is a required field ");
         return false;
     }  
     if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relationship'].value!='' && document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relationship'].value !='Self'){
		 if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value == '')
			 {
	   alert("Relocating with Co-Worker is a required field ")
	   return false;
	 }
	 } 
     </configByCorp:fieldVisibility>
 setDsFamilyDetails();
 if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relationship'].value =='Self'){

	 var ex=document.forms['dsFamilyDetailsForm'].elements['selfValueCount'].value
	 if(ex>0){
    	 alert("Relationship value as Self is already available") 
  	   return false;
	 }
 }
     
     
	}

	function checkEmail() {
if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.email'].value!=''){
var emailFilter2=/^([a-zA-Z0-9_\.\-\'])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var emailValue2=document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.email'].value;
if (!(emailFilter2.test(emailValue2))) {
alert("Please enter valid Email.")
document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.email'].value='';
document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.email'].select();
return false;
}else{
return true;
}
}
}
function onlyNumsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
function changeDOBFlag(Temp){
 document.getElementById('dateOfBirthValidFlag').value=Temp;
}
function triggerCalendar(targetElementID){
	if(targetElementID == 'invoiceDate' && document.getElementById('invoiceDate').value != ''){
		calcDays();
		
	}
	if(targetElementID == 'dateOfDeath' && document.getElementById('dateOfDeath').value != ''){
		calcDayss();
	}
}
// Birth Date could be specified like Date.UTC(2002,8,16,17,42,0)
function qryHowOld(varAsOfDate, varBirthDate,realdata)
   {

   var dtAsOfDate;
   var dtBirth;
   var dtAnniversary;
   var intSpan;
   var intYears;
   var intMonths;
   var intWeeks;
   var intDays;
   var intHours;
   var intMinutes;
   var intSeconds;
   var strHowOld;

   // get born date
   dtBirth = new Date(realdata);

   // get as of date
   dtAsOfDate = new Date(varAsOfDate);

   
   // if as of date is on or after born date
   if ( dtAsOfDate >= dtBirth )
      {

      // get time span between as of time and birth time
      intSpan = ( dtAsOfDate.getUTCHours() * 3600000 +
                  dtAsOfDate.getUTCMinutes() * 60000 +
                  dtAsOfDate.getUTCSeconds() * 1000    ) -
                ( dtBirth.getUTCHours() * 3600000 +
                  dtBirth.getUTCMinutes() * 60000 +
                  dtBirth.getUTCSeconds() * 1000       )

      // start at as of date and look backwards for anniversary 

      // if as of day (date) is after birth day (date) or
      //    as of day (date) is birth day (date) and
      //    as of time is on or after birth time
      if ( dtAsOfDate.getUTCDate() > dtBirth.getUTCDate() ||
           ( dtAsOfDate.getUTCDate() == dtBirth.getUTCDate() && intSpan >= 0 ) )
         {

         // most recent day (date) anniversary is in as of month
         dtAnniversary = 
            new Date( Date.UTC( dtAsOfDate.getUTCFullYear(),
                                dtAsOfDate.getUTCMonth(),
                                dtBirth.getUTCDate(),
                                dtBirth.getUTCHours(),
                                dtBirth.getUTCMinutes(),
                                dtBirth.getUTCSeconds() ) );

         }

      // if as of day (date) is before birth day (date) or
      //    as of day (date) is birth day (date) and
      //    as of time is before birth time
      else
         {

         // most recent day (date) anniversary is in month before as of month
         dtAnniversary = 
            new Date( Date.UTC( dtAsOfDate.getUTCFullYear(),
                                dtAsOfDate.getUTCMonth() - 1,
                                dtBirth.getUTCDate(),
                                dtBirth.getUTCHours(),
                                dtBirth.getUTCMinutes(),
                                dtBirth.getUTCSeconds() ) );

         // get previous month
         intMonths = dtAsOfDate.getUTCMonth() - 1;
         if ( intMonths == -1 )
            intMonths = 11;

         // while month is not what it is supposed to be (it will be higher)
         while ( dtAnniversary.getUTCMonth() != intMonths )

            // move back one day
            dtAnniversary.setUTCDate( dtAnniversary.getUTCDate() - 1 );

         }

      // if anniversary month is on or after birth month
      if ( dtAnniversary.getUTCMonth() >= dtBirth.getUTCMonth() )
         {

         // months elapsed is anniversary month - birth month
         intMonths = dtAnniversary.getUTCMonth() - dtBirth.getUTCMonth();

         // years elapsed is anniversary year - birth year
         intYears = dtAnniversary.getUTCFullYear() - dtBirth.getUTCFullYear();

         }

      // if birth month is after anniversary month
      else
         {

         // months elapsed is months left in birth year + anniversary month
         intMonths = (11 - dtBirth.getUTCMonth()) + dtAnniversary.getUTCMonth() + 1;

         // years elapsed is year before anniversary year - birth year
         intYears = (dtAnniversary.getUTCFullYear() - 1) - dtBirth.getUTCFullYear();

         }

      // to calculate weeks, days, hours, minutes and seconds
      // we can take the difference from anniversary date and as of date

      // get time span between two dates in milliseconds
      intSpan = dtAsOfDate - dtAnniversary;

      // get number of weeks
     // intWeeks = Math.floor(intSpan / 604800000);

      // subtract weeks from time span
    //  intSpan = intSpan - (intWeeks * 604800000);
      
      // get number of days
      intDays = Math.floor(intSpan / 86400000);

      // subtract days from time span
      intSpan = intSpan - (intDays * 86400000);

      // get number of hours
      intHours = Math.floor(intSpan / 3600000);
    
      // subtract hours from time span
      intSpan = intSpan - (intHours * 3600000);

      // get number of minutes
      intMinutes = Math.floor(intSpan / 60000);

      // subtract minutes from time span
      intSpan = intSpan - (intMinutes * 60000);

      // get number of seconds
      intSeconds = Math.floor(intSpan / 1000);

      // create output string     
      if ( intYears >= 0 )
         if ( intYears > 1 )
            strHowOld = intYears.toString() + 'Y';
         else
            strHowOld = intYears.toString() + 'Y';
      else
         strHowOld = '';

      if ( intMonths > 0 )
         if ( intMonths > 1 )
            strHowOld = strHowOld + '-' + intMonths.toString() + 'M';
         else
            strHowOld = strHowOld + '-' + intMonths.toString() + 'M';
           
     // if ( intWeeks > 0 )
      //   if ( intWeeks > 1 )
       //     strHowOld = strHowOld + ' ' + intWeeks.toString() + ' W';
       //  else
        //    strHowOld = strHowOld + ' ' + intWeeks.toString() + ' W';

      if ( intDays > 0 )
         if ( intDays > 1 )
            strHowOld = strHowOld + '-' + intDays.toString() + 'D';
         else
            strHowOld = strHowOld + '-' + intDays.toString() + 'D';
      }
   else
      strHowOld = 'Not Born Yet'

   // return string representation
   return strHowOld
   }   	
function checkBlank() {
	var date2 = document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.dateOfBirth'].value;	
	 if(date2.trim() == ''){
		 document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.age'].value = '';
		 return;
	 }
}
function calcDays() {
 document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.age'].value="";
 var date2 = document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.dateOfBirth'].value;	
 var date3 = document.forms['dsFamilyDetailsForm'].elements['fulldate'].value;
 if(date2.trim() == ''){
	 document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.age'].value = '';
	 return;
 }
   var mySplitResult2 = date3.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }  else if(month2 == 'Feb')
   {
       month2 = "02";
   }  else if(month2 == 'Mar')
   {
       month2 = "03"
   }  else if(month2 == 'Apr')
   {
       month2 = "04"
   }  else if(month2 == 'May')
   {
       month2 = "05"
   }  else if(month2 == 'Jun')
   {
       month2 = "06"
   }   else if(month2 == 'Jul')
   {
       month2 = "07"
   }  else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }  else if(month2 == 'Oct')
   {
       month2 = "10"
   }  else if(month2 == 'Nov')
   {
       month2 = "11"
   }  else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = day2+"-"+month2+"-"+year2;
  var eDate = Calendar.parseDate(finalDate2,false);//new Date(ageDate[0]+"/"+ageDate[1]+"/"+ageDate[2]);
  var dDate = new Date(year2, month2-1, day2);
  daysApart=qryHowOld(new Date(), eDate,dDate);
  document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.age'].value = daysApart; 
  document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.age'].readOnly=true;
  document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.age'].setAttribute('class','input-textUpper');
  if(daysApart<0)
  {
    alert("Contract Start Date must be less than Contract End Date");
    document.forms['customerFileForm'].elements['dsFamilyDetails.age'].value=''; 
    //document.forms['customerFileForm'].elements['customerFile.contractEnd'].value='';
    //document.forms['customerFileForm'].elements['customerFile.contractStart'].value='';
  }
  if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.age'].value=='NaN')
   {
     document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.age'].value = '';
   } 
  if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.dateOfDeath'].value)
  {
    document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.age'].value = '';
  }
 document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetailsForDaysClick'].value = '';
}

function forDays(){

 document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetailsForDaysClick'].value ='1';
} 	
function calcDayss(){
	
	 document.getElementById('dsFamilyDetailsAge').value = "";
} 	

</script> 	 
   
</head>

<s:form id="dsFamilyDetailsForm" action="saveDsFamilyDetails.html?btntype=yes&decorator=popup&popup=true" method="post" validate="true">
<c:set var="soCoordinator" value="${fn:trim(customerFile.coordinator)}" />
<c:set var="appUserName" value="${fn:trim(userName)}" />
<c:set var="soCoordinator" value="${fn:toUpperCase(soCoordinator)}" />
<c:set var="appUserName" value="${fn:toUpperCase(appUserName)}" />
<c:set var="passportNumberEncrypt" value="false"/>
<c:if test="${appUserName==soCoordinator}">
<c:set var="passportNumberEncrypt" value="true"/>
</c:if>
<c:set var="rolesAssignFlag" value="N" />
<c:forEach var="item" items="${roles}">
  <c:if test="${item eq 'ROLE_SUPERVISOR' || item eq 'ROLE_ADMIN'}">
    <c:set var="rolesAssignFlag" value="Y" />
  </c:if>
</c:forEach>
<configByCorp:fieldVisibility componentId="component.customerFile.IkeaRequest">
<c:set var="requiredfields" value="Y"/>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.familydetails.passport.encrypted">
<c:set var="passportNumberEncryptShow" value="Y"/>
</configByCorp:fieldVisibility>
<s:hidden  name="dsFamilyDetailsForDaysClick" id="dsFamilyDetailsForDaysClick"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="isSOExtract"/>
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<c:set var="field" value="<%=request.getParameter("field") %>"/>
<s:hidden name="field" value="<%=request.getParameter("field") %>" />
<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
<c:set var="field1" value="<%=request.getParameter("field1") %>"/>

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<c:set var="FormDateValue1" value="{0,date,dd-MMM-yyyy}"/>
<s:hidden id="dateFormat" name="dateFormat1" value="dd-NNN-yy"/>
<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
<s:hidden name="dsFamilyDetails.id" />
<s:hidden name="dsFamilyDetails.networkId" />
<s:hidden name="id" value="${dsFamilyDetails.id}"/>
<s:hidden name="dsFamilyDetails.customerFileId" /> 
<s:hidden name="lastName" value="${lastName}"/>
<s:hidden name="email" value="${email}"/>
<s:hidden name="cellNumber" value="${cellNumber}"/>
<s:hidden name="custPartnerEmail" value="${custPartnerEmail}"/>
<s:hidden name="selfValueCount" value="${selfValueCount}"/>
<c:set var="selfValueCount" value="${selfValueCount}"/>
<s:hidden name="formStatus" />
	 		<c:if test="${not empty dsFamilyDetails.dateOfBirth}">
	 		<s:text id="FormatedInvoiceDate" name="${FormDateValue1}"><s:param name="value" value="dsFamilyDetails.dateOfBirth"/></s:text>
		 <s:hidden name="fulldate" id="fulldate" value="%{FormatedInvoiceDate}"/>
	 </c:if>
	 <c:if test="${empty dsFamilyDetails.dateOfBirth}">
		 <s:hidden name="fulldate" id="fulldate" />
	 </c:if>
<s:hidden name="dateOfBirthYear"  id="dateOfBirthYear" value="" />
<s:hidden name="dateOfBirthValidFlag"  id="dateOfBirthValidFlag" value="Y" />
<c:set var="lastName" value="${lastName}"/>
<c:set var="email" value="${email}"/>
<c:set var="cellNumber" value="${cellNumber}"/>
<c:set var="custPartnerEmail" value="${custPartnerEmail}"/>
<div id="layer1" style=" margin-top: 10px;">
<div id="otabs">
	<ul>
	<li><a class="current"><span>Family Details</span></a></li>
	</ul>
</div>
<div class="spn" >&nbsp;</div>      
<div id="Layer5" style="width:100%">
<table class="" cellspacing="0" cellpadding="0" border="0" width="100%" >
<tbody>
<tr>
<td>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
<table class="detailTabLabel" style="width:100%;" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr><td height="5px"></td></tr>
<tr>
<td align="left" width="130px" class="listwhitetext" valign="bottom">Prefix</td>
<td align="left" width="175px" class="listwhitetext" valign="bottom">First Name<font color="red" size="2">*</font>(as per passport)</td> 
<td align="left" width="130px" class="listwhitetext" valign="bottom">Middle Name(as per passport)</td> 
<td align="left" width="150px" class="listwhitetext" valign="bottom">Last Name<font color="red" size="2">*</font>(as per passport)</td>
<td align="left" width="150px"  class="listwhitetext" valign="bottom">Relationship<font color="red" size="2">*</font></td>

					
</tr>
<tr>
<td  align="left" valign="top">
<configByCorp:customDropDown listType="map" list="${preffix_isactive}" fieldValue="${dsFamilyDetails.prefix}" 
	attribute="class=list-menu name=dsFamilyDetails.prefix onchange=relation(this); style=width:178px  headerKey='' headerValue=''"/>							
</td>
<td align="left" class="listwhitetext" valign="top"><s:textfield name="dsFamilyDetails.firstName"  maxlength="80"  required="true" cssClass="input-text" size="25" cssStyle="width:175px;"  /></td>
<td align="left" class="listwhitetext" valign="top"><s:textfield name="dsFamilyDetails.middleName"  maxlength="25"  required="true" cssClass="input-text" cssStyle="width:175px;" /></td>
<td align="left" class="listwhitetext" valign="top"><s:textfield name="dsFamilyDetails.lastName"  maxlength="80"  required="true" cssClass="input-text" size="25"  cssStyle="width:175px;"/></td>
<td  align="left" valign="top">
<configByCorp:customDropDown listType="map" list="${faimaly_isactive}" fieldValue="${dsFamilyDetails.relationship}" 
	attribute="class=list-menu name=dsFamilyDetails.relationship onchange=relation(this); style=width:178px  headerKey='' headerValue=''"/>							
</td>
<c:if test="${empty dsFamilyDetails.id}">
<td align="left" style="width:75px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dsFamilyDetails.id}"> 
<c:choose>
<c:when test="${countFamilyDetailsNotes == '0' || countFamilyDetailsNotes == '' || countFamilyDetailsNotes == null}">
<td align="left" style="width:75px"><img id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${dsFamilyDetails.id }&noteFor=FamilyDetails&subType=FamilyDetails&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${dsFamilyDetails.id }&noteFor=FamilyDetails&subType=FamilyDetails&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td align="left" style="width:75px"><img id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${dsFamilyDetails.id }&noteFor=FamilyDetails&subType=FamilyDetails&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${dsFamilyDetails.id }&noteFor=FamilyDetails&subType=FamilyDtails&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>

 </tr>
 <tr>
 
<c:if test="${requiredfields=='Y'}">	
 <td align="left" class="listwhitetext" valign="bottom">Date Of Birth<font color="red" size="2">*</font></td>
 </c:if> 
 <c:if test="${requiredfields!='Y'}">	
 <td align="left" class="listwhitetext" valign="bottom">Date Of Birth</td>
 </c:if> 
<td align="left" width="150px" class="listwhitetext" valign="bottom">Country Of Birth</td>
<td align="left" width="150px" class="listwhitetext" valign="bottom">Age</td>
 <td align="left" class="listwhitetext" valign="bottom" id="dateOfDeath2">Date Of Death</td>
 <td align="left" class="listwhitetext" valign="bottom"  >Gender<font color="red" size="2">*</font></td>

 </tr>
 <tr>
 
 <c:if test="${not empty dsFamilyDetails.dateOfBirth}">
	<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="dsFamilyDetails.dateOfBirth"/></s:text>
	<td valign="top">
	<table cellspacing="0" cellpadding="0" class="detailTabLabel">
	<tr>
	<td>
	<s:textfield id="invoiceDate" name="dsFamilyDetails.dateOfBirth" value="%{FormatedInvoiceDate}" onblur="checkBlank()" onkeydown="return onlyDel(event,this)" cssClass="input-text" cssStyle="width:65px;" readonly="true" onselect="changeDOBFlag('Y');calcDays();"/>
	</td>
	<td>
	<img id="invoiceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/>
	</td>
	</tr>
	</table>
	</td> 
 </c:if>
 <c:if test="${empty dsFamilyDetails.dateOfBirth}"> 
	<td valign="top">
	<table cellspacing="1" cellpadding="0" class="detailTabLabel">
	<tr>
	<td>
	<s:textfield id="invoiceDate" name="dsFamilyDetails.dateOfBirth" onblur="checkBlank()" onkeydown="return onlyDel(event,this)"  cssClass="input-text" cssStyle="width:65px;" readonly="true" onselect="changeDOBFlag('Y');calcDays();"/>
	</td>
	<td>
	<img id="invoiceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/>
	</td>
	</tr>
	</table>
	</td> 
 </c:if> 
 <td  align="left" valign="top">
<configByCorp:customDropDown listType="map" list="${country_isactive}" fieldValue="${dsFamilyDetails.countryOfBirth}" 
	attribute="class=list-menu name=dsFamilyDetails.countryOfBirth style=width:178px  headerKey='' headerValue=''"/>							
 </td>
<td align="left" class="listwhitetext" valign="top"><s:textfield name="dsFamilyDetails.age" maxlength="6"  id="dsFamilyDetailsAge" required="true" onkeydown="return onlyNumsAllowed(event)" cssClass="input-text" size="25" cssStyle="width:175px;"  /></td>
	<td valign="top" id="dateOfDeath1">
	<table cellspacing="1" cellpadding="0" class="detailTabLabel">
	<tr>
 <c:if test="${not empty dsFamilyDetails.dateOfDeath}">
	<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="dsFamilyDetails.dateOfDeath"/></s:text>
	<td valign="top">
	<table cellspacing="0" cellpadding="0" class="detailTabLabel">
	<tr>
	<td>
	<s:textfield id="dateOfDeath" name="dsFamilyDetails.dateOfDeath" value="%{FormatedInvoiceDate}" onblur="checkBlank()" onkeydown="return onlyDel(event,this)" cssClass="input-text" cssStyle="width:65px;" readonly="true" onselect="calcDayss();"/>
	</td>
	<td>
	<img id="dateOfDeath_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"  />
	</td>
	</tr>
	</table>
	</td> 
 </c:if>
 <c:if test="${empty dsFamilyDetails.dateOfDeath}"> 
	<td valign="top">
	<table cellspacing="1" cellpadding="0" class="detailTabLabel">
	<tr>
	<td>
	<s:textfield id="dateOfDeath" name="dsFamilyDetails.dateOfDeath" onblur="checkBlank()" onkeydown="return onlyDel(event,this)"  cssClass="input-text" cssStyle="width:65px;" readonly="true" onselect="calcDayss();" />
	</td>
	<td>
	<img id="dateOfDeath_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/>
	</td>
	</tr>
	</table>
	</td> 
 </c:if> 
	
	</tr>
	</table>
	</td> 
<td class="listwhitetext" >
 <c:if test="${dsFamilyDetails.gender=='M'}">
  <INPUT TYPE="radio" NAME="dsFamilyDetails.gender" checked="checked" value="M">Male
  <INPUT TYPE="radio" NAME="dsFamilyDetails.gender" value="F">Female
    <INPUT TYPE="radio" NAME="dsFamilyDetails.gender" value="NB">Non-Binary
  </c:if>
  <c:if test="${dsFamilyDetails.gender=='F'}">
  <INPUT TYPE="radio" NAME="dsFamilyDetails.gender"  value="M">Male
  <INPUT TYPE="radio" NAME="dsFamilyDetails.gender" value="F" checked="checked">Female
   <INPUT TYPE="radio" NAME="dsFamilyDetails.gender" value="NB">Non-Binary
  </c:if>
    <c:if test="${dsFamilyDetails.gender=='NB'}">
  <INPUT TYPE="radio" NAME="dsFamilyDetails.gender"  value="M">Male
  <INPUT TYPE="radio" NAME="dsFamilyDetails.gender" value="F" >Female
   <INPUT TYPE="radio" NAME="dsFamilyDetails.gender" value="NB" checked="checked">Non-Binary
  </c:if>
  <c:if test="${dsFamilyDetails.gender==''||dsFamilyDetails.gender==null}">
  <INPUT TYPE="radio" NAME="dsFamilyDetails.gender" value="M">Male
  <INPUT TYPE="radio" NAME="dsFamilyDetails.gender" value="F">Female
   <INPUT TYPE="radio" NAME="dsFamilyDetails.gender" value="NB">Non-Binary
  </c:if>
 </td>	
</tr>
<tr> 
<td align="left" class="listwhitetext" valign="bottom">Passport #</td>
<td align="left" class="listwhitetext" valign="bottom">Country Of Issue</td>
<td align="left" class="listwhitetext" valign="bottom">Passport Issue Date</td>
<td align="left" class="listwhitetext" valign="bottom">Passport Expiry Date</td>
</tr>
<tr>
<c:choose>
			<c:when test="${passportNumberEncryptShow=='Y'}">	
			  <c:choose>
				<c:when test="${(usertype=='ACCOUNT')||(passportNumberEncrypt =='true') || (rolesAssignFlag=='Y')}">
					<td align="left" valign="top">
						<s:textfield name="dsFamilyDetails.passportNumber" maxlength="14"  required="true" cssClass="input-text" size="25" cssStyle="width:175px;"  />
					</td>			
				</c:when>
				<c:otherwise>
					<s:hidden name="dsFamilyDetails.passportNumber"  required="true" cssClass="input-text" />
					<td align="left" valign="top"><s:textfield name="passportNumber1"  maxlength="14"  required="true" cssClass="input-text" size="25" cssStyle="width:175px;" disabled="true"   /></td>
				</c:otherwise>
			</c:choose>
			</c:when>
			<c:otherwise>
			<td align="left" valign="top"><s:textfield name="dsFamilyDetails.passportNumber" maxlength="14"  required="true" cssClass="input-text" size="25" cssStyle="width:175px;" /></td>
			</c:otherwise>
</c:choose>
<td  align="left" valign="top">
<configByCorp:customDropDown listType="map" list="${country_isactive}" fieldValue="${dsFamilyDetails.countryOfIssue}" 
	attribute="class=list-menu name=dsFamilyDetails.countryOfIssue style=width:178px  headerKey='' headerValue=''"/>							
</td>					
<c:if test="${not empty dsFamilyDetails.passportIssuedDate}">
	<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="dsFamilyDetails.passportIssuedDate"/></s:text>
	<td align="left" valign="top">
	<table cellspacing="1" cellpadding="0" class="detailTabLabel">
	<tr>
	<td>
	 <s:textfield id="passportIssuedDate1" name="dsFamilyDetails.passportIssuedDate" value="%{FormatedInvoiceDate}" onkeydown="return onlyDel(event,this)" cssClass="input-text" cssStyle="width:65px;" readonly="true" onselect="changeDOBFlag('N');"/>
	 </td>
	 <td>
	 <img id="passportIssuedDate1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
	</td>
	</tr>
	</table>
	</td> 
 </c:if>
 <c:if test="${empty dsFamilyDetails.passportIssuedDate}">
	<td align="left">
	<table cellspacing="1" cellpadding="0" class="detailTabLabel">
	<tr>
	<td>
	<s:textfield id="passportIssuedDate1" name="dsFamilyDetails.passportIssuedDate" onkeydown="return onlyDel(event,this)"  cssClass="input-text" cssStyle="width:65px;" readonly="true" onselect="changeDOBFlag('N');"/>
	</td>
	 <td>
	<img id="passportIssuedDate1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
	</td>
	</tr>
	</table>
	</td> 
 </c:if> 
<c:if test="${not empty dsFamilyDetails.expiryDate}">
	<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="dsFamilyDetails.expiryDate"/></s:text>
	<td align="left" valign="top">
	<table cellspacing="1" cellpadding="0" class="detailTabLabel">
	<tr>
	<td>
	 <s:textfield id="invoiceDate1" name="dsFamilyDetails.expiryDate" value="%{FormatedInvoiceDate}" onkeydown="return onlyDel(event,this)" cssClass="input-text" cssStyle="width:65px;" readonly="true" onselect="changeDOBFlag('N');"/>
	 </td>
	 <td>
	 <img id="invoiceDate1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
	</td>
	</tr>
	</table>
	</td> 
 </c:if>
 <c:if test="${empty dsFamilyDetails.expiryDate}">
	<td align="left">
	<table cellspacing="1" cellpadding="0" class="detailTabLabel">
	<tr>
	<td>
	<s:textfield id="invoiceDate1" name="dsFamilyDetails.expiryDate" onkeydown="return onlyDel(event,this)"  cssClass="input-text" cssStyle="width:65px;" readonly="true" onselect="changeDOBFlag('N');"/>
	</td>
	 <td>
	<img id="invoiceDate1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
	</td>
	</tr>
	</table>
	</td> 
 </c:if> 
 
</tr>
<tr> 
 <td align="left" class="listwhitetext" valign="bottom">Email</td>
  <td align="left" class="listwhitetext" valign="bottom">Mobile Phone</td>
  <td align="left" class="listwhitetext" valign="bottom">Grade</td>
  <td align="left" class="listwhitetext" valign="bottom">Native Language</td>
 </tr>
 <tr>
 <td><s:textfield name="dsFamilyDetails.email" maxlength="65"  required="true" cssClass="input-text" size="25" onchange="checkEmail();" cssStyle="width:175px;" /></td>
 <td align="left"><s:textfield name="dsFamilyDetails.cellNumber" maxlength="25"  required="true" cssClass="input-text" size="25" cssStyle="width:175px;"  /></td>
 <td align="left"><s:textfield name="dsFamilyDetails.grade" maxlength="45"  required="true" cssClass="input-text" size="25" cssStyle="width:175px;"  /></td>
 <td align="left"><s:textfield name="dsFamilyDetails.nativeLanguage" maxlength="45"  required="true" cssClass="input-text" size="25" cssStyle="width:175px;"  /></td>
 
</tr>
<tr>
<c:if test="${requiredfields=='Y'}">	
<td align="left" class="listwhitetext" valign="top">Nationality<font color="red" size="2">*</font><br><s:textfield name="dsFamilyDetails.nationality" maxlength="82"  required="true" cssClass="input-text" size="25" cssStyle="width:175px;" /></td>
</c:if>
<c:if test="${requiredfields !='Y'}">
<td align="left" class="listwhitetext" valign="top">Nationality<br><s:textfield name="dsFamilyDetails.nationality" maxlength="82"  required="true" cssClass="input-text" size="25" cssStyle="width:175px;" /></td>
</c:if>	
<configByCorp:fieldVisibility componentId="component.dsFamilyDetails.comment.showHSRG">
<td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="3" cols="38" name="dsFamilyDetails.comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" tabindex=""/> </td>
</configByCorp:fieldVisibility>
<td  width="260px">
<configByCorp:fieldVisibility componentId="component.customerFile.IkeaRequest">
<table><tr>
<td class="listwhitetext" id="relwcolist"  width="260px">Relocating with Co-Worker <font color="red" size="2">*</font><span style="margin-left: 15px;" id="relocationDate1">Relocation Date</span> <br>
<configByCorp:customDropDown listType="map" list="${relocatingWithCoWorkerList}" fieldValue="${dsFamilyDetails.relocatingWithCoWorker}" 
	attribute="class=list-menu name=dsFamilyDetails.relocatingWithCoWorker onchange=relocatingDate(this); style=width:100px  headerKey='' headerValue=''"/>
	<span id="relocationDate2">
	  <c:if test="${not empty dsFamilyDetails.relocationDate}">
	<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="dsFamilyDetails.relocationDate"/></s:text>
	 <s:textfield id="relocationDate" name="dsFamilyDetails.relocationDate" value="%{FormatedInvoiceDate}" onblur="checkBlank()" onkeydown="return onlyDel(event,this)" cssClass="input-text" cssStyle="width:65px; margin-left: 50px;" readonly="true" />
	<img id="relocationDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/>

	</c:if>
	 <c:if test="${ empty dsFamilyDetails.relocationDate}">
     <s:textfield id="relocationDate" name="dsFamilyDetails.relocationDate" onblur="checkBlank()" onkeydown="return onlyDel(event,this)"  cssClass="input-text" cssStyle="width:65px; margin-left: 50px;" readonly="true" />
     <img id="relocationDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/>
</c:if>
</span>
	</td> </tr></table>
</configByCorp:fieldVisibility>
</td>
<td valign="top" width="260px">
<configByCorp:fieldVisibility componentId="component.customerFile.IkeaRequest">
<table><tr>
	<td valign="top"  id="reasonlist1" width="260px">
       Benefits End Date<span style="margin-left:15px;"> Benefits End Reason</span></br>
        <c:if test="${not empty dsFamilyDetails.benefitDate}">
	<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="dsFamilyDetails.benefitDate"/></s:text>
 <s:textfield id="benefitDate" name="dsFamilyDetails.relocationDate" value="%{FormatedInvoiceDate}" onblur="checkBlank()" onkeydown="return onlyDel(event,this)" cssClass="input-text" cssStyle="width:65px;" readonly="true" />
	<img id="benefitDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/>

	</c:if>	
 	 <c:if test="${not empty dsFamilyDetails.benefitReasion}">
<configByCorp:customDropDown listType="map" list="${benefitsEndReasonList}" fieldValue="${dsFamilyDetails.benefitReasion}" 
	attribute="class=list-menu name=dsFamilyDetails.benefitReasion style=width:110px;margin-left:23px  headerKey='' headerValue=''"/>	
		</c:if>
	 <c:if test="${ empty dsFamilyDetails.benefitDate}">
<s:textfield id="benefitDate" name="dsFamilyDetails.benefitDate" onblur="checkBlank()" onkeydown="return onlyDel(event,this)"  cssClass="input-text" cssStyle="width:65px;" readonly="true" />
<img id="benefitDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/>

	</c:if>	
	 <c:if test="${ empty dsFamilyDetails.benefitReasion}">
 <configByCorp:customDropDown listType="map" list="${benefitsEndReasonList}" fieldValue="${dsFamilyDetails.benefitReasion}" 
	attribute="class=list-menu name=dsFamilyDetails.benefitReasion style=width:110px;margin-left:23px  headerKey='' headerValue=''"/>	 	
</c:if>
	</td></tr></table>
</configByCorp:fieldVisibility>
	</td>  	
		 			
</tr>
<tr>
<td></td>
</tr>
<tr><td align="left" height="15px"></td></tr>
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
</div>
</td>
</tr>
</tbody>
</table> 
<table width="700px">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${dsFamilyDetails.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="dsFamilyDetails.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${dsFamilyDetails.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:60px"><b><fmt:message key='carton.createdBy' /></td>
						
						
						<c:if test="${not empty dsFamilyDetails.id}">
								<s:hidden name="dsFamilyDetails.createdBy"/>
								<td ><s:label name="createdBy" value="%{dsFamilyDetails.createdBy}"/></td>
							</c:if>
							<c:if test="${empty dsFamilyDetails.id}">
								<s:hidden name="dsFamilyDetails.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${dsFamilyDetails.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="dsFamilyDetails.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${dsFamilyDetails.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty dsFamilyDetails.id}">
							<s:hidden name="dsFamilyDetails.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{dsFamilyDetails.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty dsFamilyDetails.id}">
							<s:hidden name="dsFamilyDetails.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:80px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
</div> 
   <table><tr><td>   
   <c:set var="isOrderInitiation" value="<%=request.getParameter("isOrderInitiation") %>"/>
   <c:choose>
	<c:when test="${(usertype=='ACCOUNT' || usertype=='AGENT') and not isOrderInitiation}">	
	</c:when>
	<c:otherwise>
		<s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" method="save" key="button.save" theme="simple" onclick="return validation();"/>
        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" />
	</c:otherwise>
   </c:choose>
	</td></tr></table>
</div>
</s:form> 
<div id="mydiv" style="position:absolute;top:110px;margin-top:-15px;"></div>
<script type="text/javascript">
function encryptPassportNumber(){
	var actValue=document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.passportNumber'].value;
	if(actValue.length>0){
	var encrypValue=actValue.substring(actValue.length-4,actValue.length);
	var actEncrypValue="XXXXXXXXXXXX-"+encrypValue;
	document.forms['dsFamilyDetailsForm'].elements['passportNumber1'].value=actEncrypValue;
	}
}
try{
	<c:if test="${not empty dsFamilyDetails.id && usertype!='ACCOUNT'}">
	encryptPassportNumber();
	</c:if>
}catch(e){
	
}
try{
	 
	 var billingChargeLstas =encodeURI(window.opener.location);
    var DateOfDeathcheck =billingChargeLstas.indexOf("editCustomerFile.html"); 
    if(DateOfDeathcheck >-1){ 
       document.getElementById('dateOfDeath1').style.display = 'block'; 
       document.getElementById('dateOfDeath2').style.display = 'block';
      } 
    else{
       document.getElementById('dateOfDeath1').style.display = 'none'; 
       document.getElementById('dateOfDeath2').style.display = 'none';
     }  
 }catch(e){}
   if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.age'].value!=""){ 
  document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.age'].readOnly=true;
  document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.age'].setAttribute('class','input-textUpper');
  }
   try{
if(document.forms['dsFamilyDetailsForm'].elements['btntype'].value=='yes'){
pick();
}
}
catch(e){} 
<c:if test="${empty dsFamilyDetails.id}"> 
document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.lastName'].value=document.forms['dsFamilyDetailsForm'].elements['lastName'].value;
</c:if>

	var fieldName = document.forms['dsFamilyDetailsForm'].elements['field'].value;
	var fieldName1 = document.forms['dsFamilyDetailsForm'].elements['field1'].value;
	if(fieldName!=''){
	document.forms['dsFamilyDetailsForm'].elements[fieldName].className = 'rules-textUpper';
	}
	if(fieldName1!=''){
	document.forms['dsFamilyDetailsForm'].elements[fieldName1].className = 'rules-textUpper';
	}
 </script>
<script type="text/javascript">
	setOnSelectBasedMethods(["triggerCalendar('invoiceDate'),changeDOBFlag('Y'),changeDOBFlag('N')"]);
	setCalendarFunctionality();
	Calendar.setup({
	     trigger  : "invoiceDate_trigger",
	    inputField : "fulldate",
	    animation : false,
	    dateFormat: "%d-%b-%y",
		onSelect: function() {
	var date = this.selection.get();
	date = Calendar.intToDate(date);
	document.getElementById('fulldate').value = Calendar.printDate(date,"%d-%b-%Y");
	date = Calendar.printDate(date,"%d-%b-%y");
	document.getElementById('invoiceDate').value = date;
	var yrold = document.getElementById('fulldate').value;
	var dob = yrold.split('-');

	var month = dob[1];
	var dobmonth = convertMonthNameToNumber(month);


	var d = new Date();
	var curyear = d.getFullYear();


	var curmonth = d.getMonth()+1; 


	var curday = d.getDate(); 

	var dobyr = curyear - dob[2];
	var dobmo = curmonth - dobmonth;
	var dobday = curday - dob[0];
    document.forms['dsFamilyDetailsForm'].elements['fulldate'].value=yrold;
	//document.getElementById('dsFamilyDetailsAge').value =dobyr+'Y'+dobmo+'M'+dobday+'D' ;

	
	this.hide();
	calcDays();
	}
	})
	
	function convertMonthNameToNumber(monthName) {
   var myDate = new Date(monthName + " 1, 2000");
   var monthDigit = myDate.getMonth();
   return isNaN(monthDigit) ? 0 : (monthDigit + 1);
}
	function isSoExtFlag(){
	    document.forms['dsFamilyDetailsForm'].elements['isSOExtract'].value="yes";
	    //alert(document.forms['billingForm'].elements['isSOExtract'].value);
	  }
	 function setDsFamilyDetails()
	  {
	if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relationship'].value == 'Spouse'){
	try{
		var firstName=document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.firstName'].value;
   		var lastName=document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.lastName'].value;
   		var phone=document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.cellNumber'].value;
   		 
   		parent.window.opener.document.forms['customerFileForm'].elements['customerFile.custPartnerFirstName'].value = firstName;
   		parent.window.opener.document.forms['customerFileForm'].elements['customerFile.custPartnerLastName'].value = lastName;
   		parent.window.opener.document.forms['customerFileForm'].elements['customerFile.custPartnerContact'].value = phone;
	}
    catch(e){} 
	}
	}

 	 function relocatingDate(temp)
	 {
	 	 if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value ==''){
			 document.getElementById('relocationDate1').style.display = 'none'; 
			 document.getElementById('relocationDate2').style.display = 'none'; 
	    } 
	 	 else if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value =='Later'){
	 		document.getElementById('relocationDate1').style.display = 'inline-block';   
			 document.getElementById('relocationDate2').style.display = 'inline-block'; 
	       }
	 	 else
	 		 {
	 		 document.getElementById('relocationDate1').style.display = 'none'; 
			 document.getElementById('relocationDate2').style.display = 'none'; 
	 		 }
	 } 
	 /* function for ikea to hide Relocating with Co-Worker */
	 function relation(temp)
	 {

			<configByCorp:fieldVisibility componentId="component.customerFile.IkeaRequest">
			if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relationship'].value == 'Self'){
		
		          document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.email'].value=document.forms['dsFamilyDetailsForm'].elements['email'].value
				document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.cellNumber'].value=document.forms['dsFamilyDetailsForm'].elements['cellNumber'].value
				 document.getElementById('relwcolist').style.display = 'none'; 
				 document.getElementById('reasonlist1').style.display = 'none'; 
				 
		  	 if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value ==''){
        			 document.getElementById('relocationDate1').style.display = 'none'; 
        			 document.getElementById('relocationDate2').style.display = 'none'; 
        	    } 
        	 	 else if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value =='Later'){
        	 		document.getElementById('relocationDate1').style.display = 'inline-block';   
        			 document.getElementById('relocationDate2').style.display = 'inline-block'; 
        	       }
        	 	 else
        	 		 {
        	 		 document.getElementById('relocationDate1').style.display = 'none'; 
        			 document.getElementById('relocationDate2').style.display = 'none'; 
        	 		 }
				}
				
			if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relationship'].value == 'Spouse')
			{
				document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.email'].value=parent.window.opener.document.forms['customerFileForm'].elements['customerFile.custPartnerEmail'].value
				 document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.cellNumber'].value=parent.window.opener.document.forms['customerFileForm'].elements['customerFile.originMobile'].value	   
				 document.getElementById('relwcolist').style.display = 'block'; 
				 document.getElementById('reasonlist1').style.display = 'table-cell'; 
				 if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value ==''){
					 document.getElementById('relocationDate1').style.display = 'none'; 
					 document.getElementById('relocationDate2').style.display = 'none'; 
			    } 
			 	 else if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value =='Later'){
			 		document.getElementById('relocationDate1').style.display = 'inline-block';   
					 document.getElementById('relocationDate2').style.display = 'inline-block'; 
			       }
			 	 else
			 		 {
			 		 document.getElementById('relocationDate1').style.display = 'none'; 
					 document.getElementById('relocationDate2').style.display = 'none'; 
			 		 }

			}
			if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relationship'].value == 'Other')
				{
			
				document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.email'].value="";
				document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.cellNumber'].value="";
				var abc=document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.cellNumber'].value;
                 document.getElementById('relwcolist').style.display = 'block'; 
                 document.getElementById('reasonlist1').style.display = 'table-cell'; 
            	 if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value ==''){
        			 document.getElementById('relocationDate1').style.display = 'none'; 
        			 document.getElementById('relocationDate2').style.display = 'none'; 
        	    } 
        	 	 else if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value =='Later'){
        	 		document.getElementById('relocationDate1').style.display = 'inline-block';   
        			 document.getElementById('relocationDate2').style.display = 'inline-block'; 
        	       }
        	 	 else
        	 		 {
        	 		 document.getElementById('relocationDate1').style.display = 'none'; 
        			 document.getElementById('relocationDate2').style.display = 'none'; 
        	 		 }
				}
			
			if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relationship'].value == 'Child')
			{
			document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.email'].value="";
			document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.cellNumber'].value="";
			 document.getElementById('relwcolist').style.display = 'block';
			 document.getElementById('reasonlist1').style.display = 'table-cell'; 
			 if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value ==''){
				 document.getElementById('relocationDate1').style.display = 'none'; 
				 document.getElementById('relocationDate2').style.display = 'none'; 
		    } 
		 	 else if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value =='Later'){
		 		document.getElementById('relocationDate1').style.display = 'inline-block';   
				 document.getElementById('relocationDate2').style.display = 'inline-block'; 
		       }
		 	 else
		 		 {
		 		 document.getElementById('relocationDate1').style.display = 'none'; 
				 document.getElementById('relocationDate2').style.display = 'none'; 
		 		 }
			}
			if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relationship'].value =='')
			{
			document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.email'].value="";
			document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.cellNumber'].value="";
			 document.getElementById('relwcolist').style.display = 'block';
			 document.getElementById('reasonlist1').style.display = 'table-cell'; 
			 if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value ==''){
				 document.getElementById('relocationDate1').style.display = 'none'; 
				 document.getElementById('relocationDate2').style.display = 'none'; 
		    } 
		 	 else if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value =='Later'){
		 		document.getElementById('relocationDate1').style.display = 'inline-block';   
				 document.getElementById('relocationDate2').style.display = 'inline-block'; 
		       }
		 	 else
		 		 {
		 		 document.getElementById('relocationDate1').style.display = 'none'; 
				 document.getElementById('relocationDate2').style.display = 'none'; 
		 		 }
			}
			</configByCorp:fieldVisibility>
	 }
	 if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relationship'].value =='Self'){
        if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.email'].value ==''){
		document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.email'].value=document.forms['dsFamilyDetailsForm'].elements['email'].value
        }
        if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.cellNumber'].value ==''){
		document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.cellNumber'].value=document.forms['dsFamilyDetailsForm'].elements['cellNumber'].value
        }
	<configByCorp:fieldVisibility componentId="component.customerFile.IkeaRequest">
		document.getElementById('relwcolist').style.display = 'none'; 
		 document.getElementById('reasonlist1').style.display = 'none';
		 if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value ==''){
			 document.getElementById('relocationDate1').style.display = 'none'; 
			 document.getElementById('relocationDate2').style.display = 'none'; 
	    } 
	 	 else if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relocatingWithCoWorker'].value =='Later'){
	 		document.getElementById('relocationDate1').style.display = 'inline-block';   
			 document.getElementById('relocationDate2').style.display = 'inline-block'; 
	       }
	 	 else
	 		 {
	 		 document.getElementById('relocationDate1').style.display = 'none'; 
			 document.getElementById('relocationDate2').style.display = 'none'; 
	 		 }
	 </configByCorp:fieldVisibility> 
		 
	 }
	 if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relationship'].value== 'Spouse'){
	   if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.email'].value ==''){   
	   document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.email'].value=parent.window.opener.document.forms['customerFileForm'].elements['customerFile.custPartnerEmail'].value
	   document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.cellNumber'].value=parent.window.opener.document.forms['customerFileForm'].elements['customerFile.originMobile'].value	   
	   }
	 }
	 if(document.forms['dsFamilyDetailsForm'].elements['dsFamilyDetails.relationship'].value !='Self'){
		 <configByCorp:fieldVisibility componentId="component.customerFile.IkeaRequest">
		 document.getElementById('relwcolist').style.display = 'block'; 
	     document.getElementById('reasonlist1').style.display = 'table-cell';
	     </configByCorp:fieldVisibility>
	 }
	 
	 <c:if test="${usertype=='ACCOUNT' || usertype=='AGENT' || visibilityForSalesPortla=='true'}">	
	  var isOrderInitiation = <%= request.getParameter("isOrderInitiation") %>
	  if(!isOrderInitiation){
		    iehack();
			trap() ;
	  }
	 </c:if>
	 function notExists(){
			alert("The FamilyDetails information has not been saved yet, please save FamilyDetails information to continue");
		}
</script>
    