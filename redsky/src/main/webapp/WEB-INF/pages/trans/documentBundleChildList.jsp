<%@ include file="/common/taglibs.jsp"%>  
 <head>  

</head>   
<s:form id="documentBundleChildForm" method="post" >
 <display:table name="documentBundleFormsList" class="table" id="documentBundleFormsList" style="width:100%;margin-bottom:0px;"> 
    <display:column title="#" style="width: 5px;"><c:out value="${documentBundleFormsList_rowNum}"></c:out> </display:column>
   	<display:column property="reportName" title="Jrxml Name" style="width: 10px;"></display:column>
	<display:column property="description" title="Form Desc" style="width: 15px;"></display:column>
	<display:column title="PDF" headerClass="centeralign" style="width: 5px;">
		<c:if test="${documentBundleFormsList.pdf=='true' }">
			<div ALIGN="center">
				<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
			</div>
		</c:if>
		<c:if test="${documentBundleFormsList.pdf=='false' || documentBundleFormsList.pdf==''}">
			<div ALIGN="center">
				<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
			</div>
		</c:if>
	</display:column>
	<display:column title="Docx" headerClass="centeralign" style="width: 5px;">
		<c:if test="${documentBundleFormsList.docx=='true' }">
			<div ALIGN="center">
				<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
			</div>
		</c:if>
		<c:if test="${documentBundleFormsList.docx=='false' || documentBundleFormsList.docx==''}">
			<div ALIGN="center">
				<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
			</div>
		</c:if>
	</display:column>
</display:table>
</s:form>