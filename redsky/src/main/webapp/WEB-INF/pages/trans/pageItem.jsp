<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
  
<head>   
    <title>Page Item List</title>   
    <meta name="heading" content="Page Item List"/>
<style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

</style>
<style type="text/css">
#overlay111 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
#loader {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
.rounded-top { 
-moz-border-radius-topleft: .8em;
   -webkit-border-top-left-radius:.8em;
   -moz-border-radius-topright: .8em;
   -webkit-border-top-right-radius:.8em;
   border:1px solid #AFAFAF;
   border-bottom:none;}
.rounded-bottom {   
   -moz-border-radius-bottomleft: .8em;
   -webkit-border-bottom-left-radius:.8em;
   -moz-border-radius-bottomright: .8em;
   -webkit-border-bottom-right-radius:.8em;
   border:1px solid #AFAFAF;
   border-top:none;}
</style>
<script language="javascript" type="text/javascript">
function clear_fields(){
	document.forms['form2'].elements['pageMenu'].value = '';
	document.forms['form2'].elements['pageDesc'].value = '';
	document.forms['form2'].elements['pageUrl'].value = '';
}
function showOrHide(value) {
    if (value == 0) {
       	if (document.layers)
           document.layers["overlay111"].visibility='hide';
        else
           document.getElementById("overlay111").style.visibility='hidden';
   	}else if (value == 1) {
   		if (document.layers)
          document.layers["overlay111"].visibility='show';
       	else
          document.getElementById("overlay111").style.visibility='visible';
   	}
}
function findPageItemList(){	
	 document.getElementById("loader").style.display = "block"
	 document.forms['form2'].action ="pageUrlList.html?corp=${corp}&decorator=popup&popup=true";
	document.forms['form2'].submit();

}
function editPageAction(target,url){
	var menuId=document.forms['form2'].elements['menuId'].value;
	var menuUrl=document.forms['form2'].elements['menuUrl'].value;
	var menuName=document.forms['form2'].elements['menuName'].value;	
	var parentN=document.forms['form2'].elements['parentN'].value;
	 document.getElementById("loader").style.display = "block"						
	if(target=='null'){
		
		 location.href = "editPageActionDetails.html?corp=${corp}&menuId="+menuId+"&menuUrl="+menuUrl+"&menuName="+menuName+"&parentN="+parentN+"&decorator=popup&popup=true";
	}else{
		
		location.href = "editPageActionDetails.html?pageId="+target+"&pageUrl="+url+"&corp=${corp}&menuId="+menuId+"&menuUrl="+menuUrl+"&menuName="+menuName+"&parentN="+parentN+"&decorator=popup&popup=true";		
	}
}
function confirmSubmit(targetElement){
	var menuId=document.forms['form2'].elements['menuId'].value;
	var menuUrl=document.forms['form2'].elements['menuUrl'].value;
	var menuName=document.forms['form2'].elements['menuName'].value;	
	var parentN=document.forms['form2'].elements['parentN'].value;

	var agree=confirm("Are you sure you wish to remove it?");
	var id = targetElement;
	if (agree){
		 document.getElementById("loader").style.display = "block"	
		location.href="deletePageActionDetails.html?pageId="+id+"&corp=${corp}&menuId="+menuId+"&menuUrl="+menuUrl+"&menuName="+menuName+"&parentN="+parentN+"&decorator=popup&popup=true";
	}else{
		return false;
	}
}
</script>

<s:form id="form2" action="" method="post">
<div id="overlay111">

<div id="layerLoading">

<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
  
   </div>
   </div>
<div id="newmnav" style="!padding-bottom:5px;">
	  <ul>
	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>	  	
	  </ul>
</div>
<div class="spn">&nbsp;</div> 	
<c:set var="searchbuttons">   
    <input type="button" class="cssbutton" value="Search" style="width:55px; height:25px;" onclick="findPageItemList();"/>
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/>     
</c:set>
<div id="content" align="center">
<div id="liquid-round" style="margin:0px;">
    <div class="top"><span></span></div>
    <div class="center-content">
		<table class="table" border="0">
		<thead>
		<tr>
		<th>Parent Menu Name</th>
		<th>Description</th>
		<th>URL</th>		
		</tr>
		</thead>	
		<tbody>
				<tr>
  				    <td align="left">
					    <s:textfield name="pageMenu" required="true" cssClass="input-text" size="30"/>
					</td>
					<td align="left">
					    <s:textfield name="pageDesc" required="true" cssClass="input-text" size="30"/>
					</td>					
  				    <td align="left">
					    <s:textfield name="pageUrl" required="true" cssClass="input-text" size="30"/>
					</td>
				</tr>		
				<tr><td></td><td></td><td align="right" colspan="0"><c:out value="${searchbuttons}" escapeXml="false"/></td></tr>			
		</tbody>
		</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
<s:hidden name="menuId"/>
<s:hidden name="menuUrl"/>
<s:hidden name="menuName"/>
<s:hidden name="parentN"/>
 <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
  <td align="left">
  	<div id="newmnav">
	  <ul>
		<li id="newmnav1" style="background:#FFF ;margin-bottom:2px"><a class="current" ><span>Page Action List</span></a></li>					  	
	  </ul>
	</div>

</tr>
</td></tr>
</tbody></table> 
<display:table name="pageItems" class="table" requestURI="" defaultsort="1" id="pageItemList" export="false">
<display:column property="id" sortable="true" style="width:10px" title="Id" />
<display:column property="parentMenu" sortable="true" style="width:100px" title="Parent Menu Name" />
<display:column property="description" sortable="true" style="width:150px" titleKey="menuItem.description" />
<display:column sortable="true" sortProperty="url" titleKey="menuItem.url" style="width:100px">
			<a href="#" onclick="editPageAction('${pageItemList.id}','${pageItemList.url}')"/>
			<c:out value="${pageItemList.url}" /></a>
</display:column>
		<display:column title="Remove" style="text-align:center; width:20px;">
				<a><img align="middle" onclick="confirmSubmit(${pageItemList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
		</display:column>	
</display:table>
<table><tr><td align="left">

<input type="button" class="cssbutton" value="Add" style="width:65px; height:25px;margin-left:20px;" onclick="editPageAction('null','null')"/>
</td></tr>
</table>
<div id="loader" style="text-align:center; display:none">
<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
</div>
</s:form> 
 <script>
setTimeout("showOrHide(0)",2000);
</script>