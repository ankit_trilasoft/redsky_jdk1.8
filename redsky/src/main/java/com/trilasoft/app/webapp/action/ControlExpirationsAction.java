package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.util.Date;
import java.util.Map;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.trilasoft.app.model.ControlExpirations;
import com.trilasoft.app.service.ControlExpirationsManager;
import com.trilasoft.app.service.RefMasterManager;


public class ControlExpirationsAction extends BaseAction{
	private ControlExpirations controlExpirations;
	private ControlExpirationsManager controlExpirationsManager;
	private RefMasterManager refMasterManager;
	private String sessionCorpID;
	private String gotoPageString;
	private String bucket;
	private String type;
	private String validateString;
	private Long id;
	private Map<String, String> expirations;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(ControlExpirationsAction.class);
	 

	public ControlExpirationsAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	
	public String getComboList(String corpId){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		expirations = refMasterManager.findNoteSubType(sessionCorpID,bucket, "EXPIRYCONTROL");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		
		if (id != null) {
			controlExpirations = controlExpirationsManager.get(id);
			validateString=controlExpirations.getFieldName();
			//states = customerFileManager.findDefaultStateList(truck.getCountry(), sessionCorpID);
		} else {
			controlExpirations = new ControlExpirations();
			//states = customerFileManager.findDefaultStateList("", sessionCorpID);
			controlExpirations.setCorpID(sessionCorpID);
			controlExpirations.setTableName(bucket);
			controlExpirations.setType(type);
			controlExpirations.setCreatedOn(new Date());
			controlExpirations.setUpdatedOn(new Date());
		}
		//driverList =truckManager.findDriverList(truck.getWarehouse(),sessionCorpID);
		//ownerOperatorList=truckManager.findOwnerOperatorList(parentId, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String save() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		controlExpirations.setCorpID(sessionCorpID);
		boolean isNew = (controlExpirations.getId() == null);
        //Date D=controlExpirations.getCreatedOn();
        //System.out.println("\n\n\n\n\n\n\nhello Mr. Singh "+D);
		if (isNew) {
			controlExpirations.setCreatedOn(new Date());
			Long i=Long.parseLong(controlExpirationsManager.getDriverValidation(controlExpirations.getFieldName(), sessionCorpID, controlExpirations.getParentId()).get(0).toString());
			if(i>=1){
				String key1 ="Expiry already exists for this field name.";
				saveMessage(getText(key1));
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
				
				}
		} 
		String newValidateString=controlExpirations.getFieldName();
		if(!validateString.equals(newValidateString)){
			Long i=Long.parseLong(controlExpirationsManager.getDriverValidation(controlExpirations.getFieldName(), sessionCorpID, controlExpirations.getParentId()).get(0).toString());
			if(i>=1){
				String key1 ="Expiry already exists for this field name.";
				saveMessage(getText(key1));
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
				}
		}
		controlExpirations.setUpdatedOn(new Date());
		controlExpirations.setUpdatedBy(getRequest().getRemoteUser());
		controlExpirationsManager.save(controlExpirations);
		//states = customerFileManager.findDefaultStateList(truck.getCountry(), sessionCorpID);
		//driverList =truckManager.findDriverList(truck.getWarehouse(),sessionCorpID);
		if (gotoPageString.equalsIgnoreCase("") || gotoPageString == null) {
			String key = (isNew) ? "Expiry has been added successfully." : "Expiry has been updated successfully.";
			saveMessage(getText(key));
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}
	
	
	/**
	 * @return the sessionCorpID
	 */
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	/**
	 * @param sessionCorpID the sessionCorpID to set
	 */
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	/**
	 * @return the controlExpirations
	 */
	public ControlExpirations getControlExpirations() {
		return controlExpirations;
	}
	/**
	 * @param controlExpirations the controlExpirations to set
	 */
	public void setControlExpirations(ControlExpirations controlExpirations) {
		this.controlExpirations = controlExpirations;
	}
	/**
	 * @param controlExpirationsManager the controlExpirationsManager to set
	 */
	public void setControlExpirationsManager(
			ControlExpirationsManager controlExpirationsManager) {
		this.controlExpirationsManager = controlExpirationsManager;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the gotoPageString
	 */
	public String getGotoPageString() {
		return gotoPageString;
	}

	/**
	 * @param gotoPageString the gotoPageString to set
	 */
	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	/**
	 * @return the expirations
	 */
	public Map<String, String> getExpirations() {
		return expirations;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
	       this.refMasterManager = refMasterManager;
	   }

	/**
	 * @return the bucket
	 */
	public String getBucket() {
		return bucket;
	}

	/**
	 * @param bucket the bucket to set
	 */
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the validateString
	 */
	public String getValidateString() {
		return validateString;
	}

	/**
	 * @param validateString the validateString to set
	 */
	public void setValidateString(String validateString) {
		this.validateString = validateString;
	}
}
