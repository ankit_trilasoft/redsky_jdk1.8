<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="StandardDeduction"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="1210"
		 pageHeight="595"
		 columnWidth="1150"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="begindate" isForPrompting="true" class="java.util.Date"/>
	<parameter name="enddate" isForPrompting="true" class="java.util.Date"/>
	<parameter name="Pay To" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[select "StandardDeductions.jrxml", p.partnerCode AS 'Owner Operator',sd.chargeCode AS 'Charge Code',sd.`chargeCodeDesc` AS 'Charge Description',
sd.status AS 'Deduction Status',date(sd.lastDeductionDate) AS 'Deduction Date',sd.amount AS 'Total Amount',sd.rate AS 'Interest Rate',
sd.deduction AS 'Deduction',
sd.maxDeductAmt AS 'Total Deduction',sd.amtDeducted AS 'Amount Deducted',(sd.maxDeductAmt-sd.amtDeducted) AS 'Amount Pending',c.deductionType AS 'Deduction Type',
s.shipNumber AS 'Trip Number',s.companyDivision AS 'Company Division',s.status AS 'Trip Status',a.truckNumber AS 'Truck',m.driverId AS Driver ,m.driverName AS 'Driver Name',m.status AS 'Settlement Status'
FROM `serviceorder` s
left outer join `accountline` a ON a.serviceorderid = s.id  AND s.corpID = 'SSCW'
left outer join `miscellaneous` m ON m.id = s.id  AND m.corpID = 'SSCW'
left outer join `partnerpublic` p ON p.partnerCode = a.vendorCode AND p.corpID = 'SSCW'
left outer join `standarddeductions` sd ON sd.partnerCode=p.partnerCode AND sd.corpID = 'SSCW'
left outer join `charges` c ON c.`charge`= sd.`chargeCode` AND c.corpID = 'SSCW'
WHERE
p.`isOwnerOp` is true
AND if(c.deductionType='Per Trip',a.vendorCode=p.partnerCode,a.chargeCode=c.charge)
AND sd.lastDeductiondate >= $P{begindate} 
AND sd.lastDeductiondate<= $P{enddate}
group by  s.`shipNumber`;]]></queryString>

	<field name="StandardDeductions.jrxml" class="java.lang.String"/>
	<field name="Owner Operator" class="java.lang.String"/>
	<field name="Charge Code" class="java.lang.String"/>
	<field name="Charge Description" class="java.lang.String"/>
	<field name="Deduction Status" class="java.lang.String"/>
	<field name="Deduction Date" class="java.sql.Date"/>
	<field name="Total Amount" class="java.math.BigDecimal"/>
	<field name="Interest Rate" class="java.math.BigDecimal"/>
	<field name="Deduction" class="java.math.BigDecimal"/>
	<field name="Total Deduction" class="java.math.BigDecimal"/>
	<field name="Amount Deducted" class="java.math.BigDecimal"/>
	<field name="Amount Pending" class="java.math.BigDecimal"/>
	<field name="Deduction Type" class="java.lang.String"/>
	<field name="Trip Number" class="java.lang.String"/>
	<field name="Company Division" class="java.lang.String"/>
	<field name="Trip Status" class="java.lang.String"/>
	<field name="Truck" class="java.lang.String"/>
	<field name="Driver" class="java.lang.String"/>
	<field name="Driver Name" class="java.lang.String"/>
	<field name="Settlement Status" class="java.lang.String"/>

		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="3"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="24"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="1"
						width="1150"
						height="23"
						key="staticText-39"/>
					<box></box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" size="14" isBold="true"/>
					</textElement>
				<text><![CDATA[Standard Deductions Details]]></text>
				</staticText>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="41"  isSplitAllowed="true" >
				<line direction="TopDown">
					<reportElement
						x="0"
						y="2"
						width="1150"
						height="1"
						key="line-1"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						mode="Opaque"
						x="510"
						y="4"
						width="56"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-1"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Total]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="618"
						y="4"
						width="56"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-2"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Amount]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="104"
						y="4"
						width="141"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-3"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Charge]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="295"
						y="4"
						width="50"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-4"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Deduction]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="345"
						y="4"
						width="50"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-5"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Total]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="566"
						y="4"
						width="52"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-6"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Amount]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="445"
						y="4"
						width="65"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-7"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="395"
						y="4"
						width="50"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-8"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Interest]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="4"
						width="52"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-9"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Owner]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="245"
						y="4"
						width="50"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-10"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Deduction]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="510"
						y="20"
						width="56"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-11"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Deduction]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="618"
						y="20"
						width="56"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-12"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Pending]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="104"
						y="20"
						width="141"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-13"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Description]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="295"
						y="20"
						width="50"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-14"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="345"
						y="20"
						width="50"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-15"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Amount]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="566"
						y="20"
						width="52"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-16"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Deducted]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="445"
						y="20"
						width="65"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-17"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Deduction]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="395"
						y="20"
						width="50"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-18"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Rate]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="20"
						width="52"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-19"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Operator]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="245"
						y="20"
						width="50"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-20"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Status]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="52"
						y="4"
						width="52"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-21"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Charge]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="52"
						y="20"
						width="52"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-22"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Code]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="866"
						y="4"
						width="38"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-23"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Trip
]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="1093"
						y="4"
						width="57"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-24"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Settlement
]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="674"
						y="4"
						width="59"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-25"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Deduction
]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="991"
						y="4"
						width="102"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-26"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Driver
]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="813"
						y="4"
						width="53"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-27"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Company]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="733"
						y="4"
						width="80"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-28"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Trip
]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="866"
						y="20"
						width="38"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-29"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Status
]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="1093"
						y="20"
						width="57"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-30"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Status]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="674"
						y="20"
						width="59"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-31"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Type
]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="991"
						y="20"
						width="102"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-32"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Name

]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="813"
						y="20"
						width="53"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-33"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Division
]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="733"
						y="20"
						width="80"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-34"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Number
]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="949"
						y="4"
						width="42"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-35"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[
]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="904"
						y="4"
						width="45"
						height="16"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-36"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[
]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="949"
						y="20"
						width="42"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-37"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Driver
]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="904"
						y="20"
						width="45"
						height="18"
						forecolor="#3300CC"
						backcolor="#CCCCCC"
						key="staticText-38"/>
					<box leftPadding="1" rightPadding="1"></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[Truck]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="39"
						width="1150"
						height="1"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</columnHeader>
		<detail>
			<band height="20"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="2"
						y="2"
						width="50"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Owner Operator}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="52"
						y="2"
						width="50"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Charge Code}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="102"
						y="2"
						width="143"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Charge Description}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="245"
						y="2"
						width="50"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Deduction Status}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="295"
						y="2"
						width="50"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{Deduction Date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="345"
						y="2"
						width="50"
						height="18"
						key="textField"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{Total Amount}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="395"
						y="2"
						width="50"
						height="18"
						key="textField"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{Interest Rate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="445"
						y="2"
						width="65"
						height="18"
						key="textField"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{Deduction}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="510"
						y="2"
						width="56"
						height="18"
						key="textField"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{Total Deduction}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="566"
						y="2"
						width="52"
						height="18"
						key="textField"/>
					<box rightPadding="3"></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{Amount Deducted}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="618"
						y="2"
						width="56"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{Amount Pending}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="674"
						y="2"
						width="59"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Deduction Type}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="733"
						y="2"
						width="80"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Trip Number}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="813"
						y="2"
						width="53"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Center">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Company Division}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="950"
						y="2"
						width="36"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Driver}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="986"
						y="2"
						width="127"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Driver Name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1113"
						y="2"
						width="37"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Settlement Status}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="866"
						y="2"
						width="38"
						height="18"
						key="textField-1"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Trip Status}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="904"
						y="2"
						width="46"
						height="18"
						key="textField-2"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Truck}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="17"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="938"
						y="1"
						width="170"
						height="16"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1110"
						y="1"
						width="36"
						height="16"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-4"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="1"
						width="209"
						height="16"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-5"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" size="10"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="291"
						y="0"
						width="298"
						height="17"
						key="staticText-40"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial"/>
					</textElement>
				<text><![CDATA[StandardDeductions.jrxml]]></text>
				</staticText>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
