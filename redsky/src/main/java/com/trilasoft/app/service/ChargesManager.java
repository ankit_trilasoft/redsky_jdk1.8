package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;


import org.appfuse.service.GenericManager;


import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.CountryDeviation;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.RateGrid;


public interface ChargesManager extends GenericManager<Charges, Long> {
	//public List<Charges> findByEstimateActual(String estimateActual,String contract);
	public List<Charges> findForCharges(String charge,String contract,String description,String corpID,String gl,String expGl,String com);
	public List<Charges> findByEstimateActual(String estimateActual, String contract);
	public List findMaxId();
	public List<Charges> findByChargeCode(String charge,String description);
	public List<Charges> findForContract(String contract,String corpID);
	public List<Charges> findForContractChargeList(String contract,String corpID,String originCountry,String destCountry,String serviceMode,String category);
	public List<Charges> findForChargesContract(String charge,String description,String contract,String corpID,String originCountry,String destCountry,String serviceMode);
	public Map<String,String>  findQuantityField(String sessionCorpID,String charge);
	public List<RateGrid> findRateGrid(Long chargeId);
	public List getCharge(String charge,String billingContract,String corpId);
	public List getChargeRadio(String charge,String billingContract, String sessionCorpID);
	public List getChargeEntitle(String charge,String billingContract);
	public List getChargeEstimate(String charge,String billingContract,String sessionCorpID);
	public Map<String,String> findPriceField(String sessionCorpID,String charge);
	public List getQuantity(String fieldName,String shipNumber);
	public List<RateGrid> findRateFromRateGrid(String quantity,String charge, String multquantity, String distance, String twoDGridUnit);
	public List findChargeId(String charge,String contract);
	public List findChargeCode(String charge,String contract);
	public List searchID();
	public List findChargesByContract(String sessionCorpID);
	public List getBreakPoints(String chargeId, String sessionCorpID);
	public List getFirstQuantity(String chargeId, String sessionCorpID, String multquantity, String stringDistance, String qty);
	public List findCount(String chargeCode, String contractName);
	public List findDefaultTemplateContract(String contract, String sessionCorpID);
	public List findDefaultTemplateChargeCode(String chargeCode, String contract);
	public List checkChargeForContractAvailability(String chargeCde, String contractCde, String sessionCorpID);
	public List findForInternalCostChargeList(String accountCompanyDivision, String sessionCorpID, String originCountry, String destinationCountry, String serviceMode);
	public List findForChargesInternalCost(String chargeCode, String description, String accountCompanyDivision, String sessionCorpID, String originCountry, String destinationCountry, String serviceMode);
	public List findInternalCostChargeCodeChargeCode(String chargeCode, String accountCompanyDivision, String sessionCorpID);
	public List findRadioValueInternalCost(String charge, String accountCompanyDivision, String sessionCorpID);
	public List getInternalCostsCharge(String charge, String accountCompanyDivision, String sessionCorpID);
	public List findInternalCostsChargeId(String charge, String accountCompanyDivision, String sessionCorpID);
	public List getInternalCostsChargeEntitle(String charge, String accountCompanyDivision, String sessionCorpID);
	public List getInternalCostChargeEstimate(String charge, String accountCompanyDivision, String sessionCorpID);
	public List findInsuranceLHFAmount(String insuranceLHFAmount, String chargeid);
	public void deleteRateGridInvalidRecord(Long chargeId);
	public List defaultChargesByComDiv(String comDiv, String corpId);
	public List findChargeCodeCostElement(String chargeCode, String contract, String sessionCorpID, String jobType, String routing, String soCompanyDivision);
	public List findInternalCostChargeCodeCostElement(String chargeCode, String accountCompanyDivision, String sessionCorpID, String jobType, String routing, String soCompanyDivision);
	public String findDeviation(String chargeid, String originCountryCode, String purchase, String sessionCorpID); 
	public String findMaxDeviation(String chargeid,String originCountryCode,String destinationCountryCode, String purchase, String sessionCorpID); 
	public String findSumDeviation(String chargeid,String originCountryCode,String destinationCountryCode, String purchase, String sessionCorpID);
	public List findVatCalculationForCharge(String corpId);
	public List findChargesRespectToContract(String contract,String sessionCorpID);
	public List getAllGridForCharges(String chargeId , String corpId,	String contract);
	public List findBuyRateFromRateGrid(String quantity,String charge, String multquantity, String distance, String twoDGridUnit);
	public int countContractCharges(String contract,String sessionCorpID);
	public List findChargeIdForOtherCorpid(String charge, String contract, String externalCorpID);
	public List<CountryDeviation> findCountryDeviation(Long chargeId);
	public String findCharges(String charge,String contract,String sessionCorpID);
	public List getSaleCommisionableList(String commissionContract,String commissionChargeCode,String commissionJobType,String commissionCompanyDivision,Boolean commissionIsActive,Boolean commissionIsCommissionable,String sessionCorpID);
	public void updateSaleCommission(Long commisionId,Boolean commissionIsCommissionable,String commission,Boolean isThirdParty,Boolean isGrossMargin,String sessionCorpID, String userName);
	public List<Charges> findChargeForSaleCommission(String contract,String chargeCode,String description,String sessionCorpID);
	public List<Charges> findChargesList(String charge,String contract,String sessionCorpID);
	public String findPopulateCostElementData(String charge, String contract,String sessionCorpID);
	public Map<String,String> findChargeValExcludeMap(String contract,String sessionCorpID,String shipNumber);
	public void updateChargeStatus(Long soIdNum, Boolean status);
	public List<Charges> findContractForCopyCharges(String contract,String corpID);
	public List checkChargeCodeListValueNew(String chargeCode,String contract,String corpID);
	public void findDeleteChargeRateGridOfOtherCorpId(String contract,String agentsCorpId);
	public List getTotalChargeCount(String contractName,String sessionCorpID);
	public List getPresentChargeCountInAgentInstance(String contractName,String agentCorpId);
	public String callCopyCharge(String sessionCorpID,String contract,String contractCopy, String updatedBy,String copyAgentAccount );
	public List findForPopupChargesContract(String charge,String description,String contract,String corpID,String originCountry,String destCountry,String serviceMode);
}
