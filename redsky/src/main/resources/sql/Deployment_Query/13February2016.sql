// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`servicepartner` ADD COLUMN `status` bit(1) DEFAULT b'0';
update servicepartner set status=true; 

ALTER TABLE `redsky`.`customerfile` MODIFY COLUMN `destinationDayPhoneExt` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
 MODIFY COLUMN `originDayPhoneExt` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

 ALTER TABLE `redsky`.`workticket` MODIFY COLUMN `destinationPhoneExt` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
 MODIFY COLUMN `phoneExt` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci;
 
 ALTER TABLE `redsky`.`container` ADD COLUMN `status` bit(1) DEFAULT b'0';
update container set status=true;
ALTER TABLE `redsky`.`carton` ADD COLUMN `status` bit(1) DEFAULT b'0';
update carton set status=true;
ALTER TABLE `redsky`.`vehicle` ADD COLUMN `status` bit(1) DEFAULT b'0';
update `vehicle set status=true;

ALTER TABLE `redsky`.`app_user` ADD COLUMN 
`networkCoordinatorStatus` bit(1) DEFAULT b'0' ;

update app_user set networkCoordinatorStatus = false;

update app_user set networkCoordinatorStatus = true where networkCoordinator is not null and networkCoordinator <> '';

CREATE  TABLE `redsky`.`documentbundle` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `corpId` VARCHAR(10) NULL ,
  `updatedOn` DATETIME NULL ,
  `updatedBy` VARCHAR(82) NULL ,
  `createdOn` DATETIME NULL ,
  `createdBy` VARCHAR(82) NULL ,
  `bundleName` VARCHAR(200) NULL ,
  `sendPointTableName` VARCHAR(50) NULL ,
  `sendPointFieldName` VARCHAR(50) NULL ,
  `formsId` BIGINT(20) NULL ,
  `routing` VARCHAR(200) NULL ,
  `mode` VARCHAR(45) NULL ,
  `job` VARCHAR(200) NULL ,
  `description` TEXT NULL ,
  `printSeq` INT(11) NULL ,
  PRIMARY KEY (`id`) );
  
  ALTER TABLE `redsky`.`companydivision` MODIFY COLUMN `beneficiaryName` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci;
  
  ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `agentClassification` VARCHAR(45) AFTER `privateVatNumber`;
  
  Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values

('TSFT', 'Primary', 'Primary', 50, 'AGENTCLASSIFICATION','en',now(),'Admin',now(),'Admin'),

('TSFT', 'Secondary', 'Secondary', 50, 'AGENTCLASSIFICATION','en',now(),'Admin',now(),'Admin'),

('TSFT', 'COD', 'COD', 50, 'AGENTCLASSIFICATION','en',now(),'Admin',now(),'Admin');


INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','AGENTCLASSIFICATION',50,true,now(),'Admin',now(),'Admin',1,1,'');


alter table operationsintelligence add estmatedValuation decimal(19,2) DEFAULT NULL;
alter table operationsintelligence add revisionValuation decimal(19,2) DEFAULT NULL;
alter table operationsintelligence add estmatedConsumables decimal(19,2) DEFAULT NULL;
alter table operationsintelligence add revisionConsumables decimal(19,2) DEFAULT NULL;
alter table workticket add ticketAssignedStatus varchar(45);

ALTER TABLE `redsky`.`dspdetails`
 ADD COLUMN `RNT_homeSearchType` VARCHAR(25) AFTER `RNT_leaseExtensionNeeded`;
 
 ALTER TABLE `redsky`.`charges` ADD COLUMN `excludeSalesAdditionalCommission` bit(1) DEFAULT b'1' AFTER `status`;
 
 ALTER TABLE `redsky`.`itemdata` ADD COLUMN `totalActQuantity` VARCHAR(40);
 
 CREATE TABLE `redsky`.`preferredAgentforAccount` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `corpId` VARCHAR(15),
  `partnerCode` VARCHAR(8),
  `partnerName` VARCHAR(255),
  `status` BOOLEAN,
  `createdBy` VARCHAR(82),
  `createdOn` DATETIME,
  `updatedBy` VARCHAR(82),
  `updatedOn` DATETIME,
  PRIMARY KEY(`id`)
)
ENGINE = InnoDB;

ALTER TABLE `redsky`.`preferredAgentforAccount` CHANGE COLUMN `partnerCode` `preferredAgentCode` VARCHAR(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
 CHANGE COLUMN `partnerName` `preferredAgentName` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
 ADD COLUMN `partnerCode` VARCHAR(8) AFTER `preferredAgentName`;
 
  update app_user set networkCoordinator = '' ;
  
ALTER TABLE `redsky`.`documentbundle` ADD COLUMN `email` VARCHAR(65) NULL  AFTER `printSeq` ;
ALTER TABLE `redsky`.`documentbundle` CHANGE COLUMN `formsId` `formsId` VARCHAR(200) NULL DEFAULT NULL  ;
ALTER TABLE `redsky`.`documentbundle` ADD COLUMN `status` BIT(1) NULL DEFAULT b'1'  AFTER `email` ;
ALTER TABLE `redsky`.`documentbundle` ADD COLUMN `serviceType` VARCHAR(500) NULL  AFTER `status` ;
ALTER TABLE `redsky`.`documentbundle` ADD COLUMN `fileTypePdf` BIT(1) NULL  AFTER `serviceType` , ADD COLUMN `fileTypeDocx` BIT(1) NULL  AFTER `fileTypePdf` ;
alter table app_user add recordsForSoDashboard varchar(5);

 ALTER TABLE `redsky`.`charges` ADD COLUMN `servicedetail` VARCHAR(15) AFTER `excludeSalesAdditionalCommission`;
 
 ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `minimumMargin` DECIMAL(10,2) AFTER `agentClassification`;
ALTER TABLE `redsky`.`charges` MODIFY COLUMN `servicedetail` VARCHAR(35) CHARACTER SET utf8 COLLATE utf8_unicode_ci;
ALTER TABLE `redsky`.`preferredAgentforAccount` RENAME TO `redsky`.`preferredagentforaccount`,
 CHANGE COLUMN `corpId` `corpID` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci;
 
 ALTER TABLE `redsky`.`dspdetails`
 ADD COLUMN `FRL_vendorCode` VARCHAR(25) AFTER `RNT_homeSearchType`,
 ADD COLUMN `FRL_vendorName` VARCHAR(255) AFTER `FRL_vendorCode`,
 ADD COLUMN `FRL_serviceStartDate` DATETIME AFTER `FRL_vendorName`,
 ADD COLUMN `FRL_vendorCodeEXSO` VARCHAR(25) AFTER `FRL_serviceStartDate`,
 ADD COLUMN `FRL_vendorContact` VARCHAR(255) AFTER `FRL_vendorCodeEXSO`,
 ADD COLUMN `FRL_serviceEndDate` DATETIME AFTER `FRL_vendorContact`,
 ADD COLUMN `FRL_emailSent` DATETIME AFTER `FRL_serviceEndDate`,
 ADD COLUMN `FRL_vendorEmail` VARCHAR(65) AFTER `FRL_emailSent`,
 ADD COLUMN `FRL_displyOtherVendorCode` bit(1) DEFAULT b'0' AFTER `FRL_vendorEmail`,
 ADD COLUMN `FRL_paymentResponsibility` VARCHAR(65) AFTER `FRL_displyOtherVendorCode`,
 ADD COLUMN `FRL_rentalRate` VARCHAR(65) AFTER `FRL_paymentResponsibility`,
 ADD COLUMN `FRL_deposit` VARCHAR(65) AFTER `FRL_rentalRate`,
 ADD COLUMN `FRL_rentalCurrency` VARCHAR(3) AFTER `FRL_deposit`,
 ADD COLUMN `FRL_depositCurrency` VARCHAR(3) AFTER `FRL_rentalCurrency`,
 ADD COLUMN `FRL_month` VARCHAR(25) AFTER `FRL_depositCurrency`,
 ADD COLUMN `FRL_rentalStartDate` DATETIME AFTER `FRL_month`,
 ADD COLUMN `FRL_rentalEndDate` DATETIME AFTER `FRL_rentalStartDate`,
 ADD COLUMN `FRL_terminationNotice` VARCHAR(255) AFTER `FRL_rentalEndDate`,
 ADD COLUMN `FRL_comment` mediumtext AFTER `FRL_terminationNotice`,
 ADD COLUMN `APU_vendorCode` VARCHAR(25) AFTER `FRL_comment`,
 ADD COLUMN `APU_vendorName` VARCHAR(255) AFTER `APU_vendorCode`,
 ADD COLUMN `APU_serviceStartDate` DATETIME AFTER `APU_vendorName`,
 ADD COLUMN `APU_vendorCodeEXSO` VARCHAR(25) AFTER `APU_serviceStartDate`,
 ADD COLUMN `APU_vendorContact` VARCHAR(255) AFTER `APU_vendorCodeEXSO`,
 ADD COLUMN `APU_serviceEndDate` DATETIME AFTER `APU_vendorContact`,
 ADD COLUMN `APU_emailSent` DATETIME AFTER `APU_serviceEndDate`,
 ADD COLUMN `APU_vendorEmail` VARCHAR(65) AFTER `APU_emailSent`,
 ADD COLUMN `APU_displyOtherVendorCode` bit(1) DEFAULT b'0' AFTER `APU_vendorEmail`,
 ADD COLUMN `APU_paymentResponsibility` VARCHAR(65) AFTER `APU_displyOtherVendorCode`,
 ADD COLUMN `APU_flightDetails` VARCHAR(255) AFTER `APU_paymentResponsibility`,
 ADD COLUMN `APU_driverName` VARCHAR(255) AFTER `APU_flightDetails`,
 ADD COLUMN `APU_driverPhoneNumber` VARCHAR(65) AFTER `APU_driverName`,
 ADD COLUMN `APU_arrivalDate` DATETIME AFTER `APU_driverPhoneNumber`,
 ADD COLUMN `APU_carMake` VARCHAR(65) AFTER `APU_arrivalDate`,
 ADD COLUMN `APU_carModel` VARCHAR(65) AFTER `APU_carMake`,
 ADD COLUMN `APU_carColor` VARCHAR(65) AFTER `APU_carModel`;
 
 ALTER TABLE `redsky`.`documentbundle` DROP COLUMN `fileTypeDocx` , DROP COLUMN `fileTypePdf` ;

 ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `FRL_vendorCode` VARCHAR(8) AFTER `clientId`,
 ADD COLUMN `APU_vendorCode` VARCHAR(8) AFTER `FRL_vendorCode`;

 Insert into refmaster
    (corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby)values
    ('IMSB', 'FRL', 'Furniture Rental', 5, 'SERVICE','en','Relo','dspDetails.FRL_serviceEndDate',now(),'dkumar',now(),'dkumar'),
    ('IMSB', 'APU', 'Airport Pick Up', 5, 'SERVICE','en','Relo','dspDetails.APU_serviceEndDate',now(),'dkumar',now(),'dkumar');

    Insert into refmaster
    (corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby)values
    ('IMSB', 'DsFurnitureRental', 'DsFurnitureRental', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar'),
    ('IMSB', 'DsAirportPickUp', 'DsAirportPickUp', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar');


Insert into dspnetworkfield(corpid,createdon,createdby,updatedon,updatedby,tableName,type,fieldName)values
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_arrivalDate'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_carColor'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_carMake'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_carModel'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_displyOtherVendorCode'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_driverName'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_driverPhoneNumber'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_emailSent'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_flightDetails'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_paymentResponsibility'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_serviceEndDate'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_serviceStartDate'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorCode'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorCodeEXSO'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorContact'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorEmail'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorName'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_comment'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_deposit'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_depositCurrency'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_displyOtherVendorCode'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_emailSent'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_month'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_paymentResponsibility'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_rentalCurrency'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_rentalEndDate'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_rentalRate'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_rentalStartDate'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_serviceEndDate'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_serviceStartDate'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_terminationNotice'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorCode'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorCodeEXSO'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorContact'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorEmail'),
('IMSB', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorName');


Insert into dspnetworkfield(corpid,createdon,createdby,updatedon,updatedby,tableName,type,fieldName)values
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_arrivalDate'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_carColor'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_carMake'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_carModel'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_displyOtherVendorCode'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_driverName'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_driverPhoneNumber'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_emailSent'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_flightDetails'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_paymentResponsibility'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_serviceEndDate'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_serviceStartDate'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorCode'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorCodeEXSO'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorContact'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorEmail'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorName'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_comment'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_deposit'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_depositCurrency'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_displyOtherVendorCode'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_emailSent'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_month'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_paymentResponsibility'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_rentalCurrency'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_rentalEndDate'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_rentalRate'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_rentalStartDate'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_serviceEndDate'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_serviceStartDate'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_terminationNotice'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorCode'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorCodeEXSO'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorContact'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorEmail'),
('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorName');

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `rddBased` VARCHAR(50) NULL  AFTER `minimumMargin` , ADD COLUMN `rddDays` VARCHAR(10) NULL  AFTER `rddBased` ;

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('TSFT', 'trackingstatus.packA', 'Pack date', 50, 'RDDBASED','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', 'trackingstatus.loadA', 'Load date', 50, 'RDDBASED','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','RDDBASED',50,true,now(),'TSFT_SETUP',now(),'TSFT_SETUP',1,1,'');

 ALTER TABLE `redsky`.`systemdefault` MODIFY COLUMN `contractChargesMandatory` VARCHAR(1);
-- 


 
