package com.trilasoft.app.dao.hibernate.dto;

import java.math.BigDecimal;

public class WorkPlanDTO {
 	private Object ticket;
  	private Object date1;
	private Object date2; 
	private Object warehouse;
	private Object estimatedweight; 
	private Object service;
	private Object duration;
	private Object avgWt;
	private Object dateDisplay;	
	private Object jobCont;
	private Object avgWtTotal;
	private Object originCountry;
	private Object destinationCountry; 
	private Object shipper;
	private Object trucks;
	private Object originCity; 
	private Object destinationCity;
	private Object actualWeight;
	private Object id;
	private String aTime;
	private String dTime;
	private Object estimatedCartoons;
	private Object dailyOpHubLimit;
	private Object dailyMaxEstWt;
	private Object requiredcrew;
	
	public Object getAvgWtTotal() {
		return avgWtTotal;
	}
	public void setAvgWtTotal(Object avgWtTotal) {
		this.avgWtTotal = avgWtTotal;
	}
	public Object getJobCont() {
		return jobCont;
	}
	public void setJobCont(Object jobCont) {
		this.jobCont = jobCont;
	}
	
	public Object getAvgWt() {
		return avgWt;
	}
	public void setAvgWt(Object avgWt) {
		this.avgWt = avgWt;
	}
	public Object getDate1() {
		return date1;
	}
	public void setDate1(Object date1) {
		this.date1 = date1;
	}
	public Object getDate2() {
		return date2;
	}
	public void setDate2(Object date2) {
		this.date2 = date2;
	}
	public Object getDuration() {
		return duration;
	}
	public void setDuration(Object duration) {
		this.duration = duration;
	}
	public Object getEstimatedweight() {
		return estimatedweight;
	}
	public void setEstimatedweight(Object estimatedweight) {
		this.estimatedweight = estimatedweight;
	}
	public Object getService() {
		return service;
	}
	public void setService(Object service) {
		this.service = service;
	}
	public Object getTicket() {
		return ticket;
	}
	public void setTicket(Object ticket) {
		this.ticket = ticket;
	}
	public Object getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(Object warehouse) {
		this.warehouse = warehouse;
	}
	public Object getDateDisplay() {
		return dateDisplay;
	}
	public void setDateDisplay(Object dateDisplay) {
		this.dateDisplay = dateDisplay;
	}
	public Object getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(Object originCountry) {
		this.originCountry = originCountry;
	}
	public Object getDestinationCountry() {
		return destinationCountry;
	}
	public void setDestinationCountry(Object destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	public Object getShipper() {
		return shipper;
	}
	public void setShipper(Object shipper) {
		this.shipper = shipper;
	}
	public Object getTrucks() {
		return trucks;
	}
	public void setTrucks(Object trucks) {
		this.trucks = trucks;
	}
	public Object getOriginCity() {
		return originCity;
	}
	public void setOriginCity(Object originCity) {
		this.originCity = originCity;
	}
	public Object getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(Object destinationCity) {
		this.destinationCity = destinationCity;
	}
	public Object getActualWeight() {
		return actualWeight;
	}
	public void setActualWeight(Object actualWeight) {
		this.actualWeight = actualWeight;
	}
	public Object getId() {
		return id;
	}
	public void setId(Object id) {
		this.id = id;
	}
	public String getaTime() {
		return aTime;
	}
	public void setaTime(String aTime) {
		this.aTime = aTime;
	}
	public String getdTime() {
		return dTime;
	}
	public void setdTime(String dTime) {
		this.dTime = dTime;
	}
	public Object getEstimatedCartoons() {
		return estimatedCartoons;
	}
	public void setEstimatedCartoons(Object estimatedCartoons) {
		this.estimatedCartoons = estimatedCartoons;
	}
	public Object getDailyOpHubLimit() {
		return dailyOpHubLimit;
	}
	public void setDailyOpHubLimit(Object dailyOpHubLimit) {
		this.dailyOpHubLimit = dailyOpHubLimit;
	}
	public Object getDailyMaxEstWt() {
		return dailyMaxEstWt;
	}
	public void setDailyMaxEstWt(Object dailyMaxEstWt) {
		this.dailyMaxEstWt = dailyMaxEstWt;
	}
	public Object getRequiredcrew() {
		return requiredcrew;
	}
	public void setRequiredcrew(Object requiredcrew) {
		this.requiredcrew = requiredcrew;
	}
	

}
