package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.PartnerRefCharges;

public interface PartnerRefChargesManager  extends GenericManager<PartnerRefCharges, Long> {

	public List getRefCharesList(String value, String tariffApplicability, String parameter, String countryCode, String grid, String basis, String label, String sessionCorpID);

	
}
