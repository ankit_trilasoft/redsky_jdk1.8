<!-- *File Name:subcontractorCharges.jsp 
 * Created By:Ashish Mishra 
 * Created Date:08-Aug-2008
 * Summary: This jsp is created for showing Detail Subcontractor Charges.
 -->
<%@ include file="/common/taglibs.jsp"%>   
 <%@ taglib prefix="s" uri="/struts-tags" %> 
<head>   
    <title><fmt:message key="subcontractorCharges.title"/></title>   
    <meta name="heading" content="<fmt:message key='subcontractorCharges.heading'/>"/>   
    
    
<style type="text/css">

.openpopup { cursor:pointer;

			
}
#ajax_tooltipObj .ajax_tooltip_content {
width:auto !important;
}
/* collapse */


</style> 
<style type="text/css">h2 {background-color: #FBBFFF}</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->
	
	<script language="JavaScript">
	 function changeStatus(){
	document.forms['subcontractorChargesForm'].elements['formStatus'].value = '1';
}
	 function checkBalanceForward(){
		 if(document.getElementById("balanceForwardId").checked==true){
			 document.forms['subcontractorChargesForm'].elements['subcontractorCharges.glCode'].value='';
			 hideCalenderImg();
			 document.forms['subcontractorChargesForm'].elements['subcontractorCharges.glCode'].disabled=true;
		 }else{		 
			 document.forms['subcontractorChargesForm'].elements['subcontractorCharges.glCode'].disabled=false;
			 hideCalenderImg();
		 }
		 	 
	 }
	 function autoSave(clickType){
			
			if(!(clickType == 'save')){ 
				enablefield();
			if ('${autoSavePrompt}' == 'No'){	
				var noSaveAction;
				
				if(document.forms['subcontractorChargesForm'].elements['gotoPageString'].value == 'gototab.subcontractList'){
						noSaveAction = 'subcontractorChargesList.html';
				}
				processAutoSave(document.forms['subcontractorChargesForm'],'saveSpliteSubcontractor!saveOnTabChange.html', noSaveAction );
			
			}else{
			var id1 = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.id'].value;
			
			if (document.forms['subcontractorChargesForm'].elements['formStatus'].value == '1'){
				var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the subcontractorCharges Form");
				if(agree){
					document.forms['subcontractorChargesForm'].action = 'saveSpliteSubcontractor!saveOnTabChange.html';
					document.forms['subcontractorChargesForm'].submit();
				}else{
					if(id1 != ''){
					if(document.forms['subcontractorChargesForm'].elements['gotoPageString'].value == 'gototab.subcontractList'){
						location.href = 'subcontractorChargesList.html';
						}
					
							
				}
				}
			}else{
			if(id1 != ''){
				if(document.forms['subcontractorChargesForm'].elements['gotoPageString'].value == 'gototab.subcontractList'){
						location.href = 'subcontractorChargesList.html';
						}
					
					
			}
			}
		}
		}
		}
	
	
	
	function chkSelect()
	{
	if (checkFloatWithArth('subcontractorChargesForm','subcontractorCharges.amount','Invalid data in amount') == false)
           {
              document.forms['subcontractorChargesForm'].elements['subcontractorCharges.amount'].value='0';
              
              return false
           }
	}
	function chkSelectSave()
	{
	document.forms['subcontractorChargesForm'].elements['subcontractorCharges.amount'].select();
	if (checkFloatWithArth('subcontractorChargesForm','subcontractorCharges.amount','Invalid data in amount') == false)
           {
              document.forms['subcontractorChargesForm'].elements['subcontractorCharges.amount'].value='0';
              
              return false
           }
	}
	</script>
	<script language="JavaScript">
	
	function openAccountPopWindow(){
	var persId = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value;
	javascript:openWindow('carrierCodeSub.html?&partnerType=OO&decorator=popup&popup=true&fld_eigthDescription=subcontractorCharges.vlCode&fld_seventhDescription=seventhDescription&fld_ninthDescription=ninthDescription&fld_tenthDescription=tenthDescription&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=subcontractorCharges.personName&fld_code=subcontractorCharges.personId');
	}
	
	function openAccountPopWindow1(){
	var persId = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value;
	javascript:openWindow('truckList.html?&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=subcontractorCharges.personName&fld_secondDescription=subcontractorCharges.personId&fld_description=description&fld_code=subcontractorCharges.truckNo');
	}
	function openAccountPopWindow2(){
	var persId = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value;
	javascript:openWindow('truckList.html?&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=subcontractorCharges.truckNo');
	}

	function checkRegNo()
	{
	var regNo = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.regNumber'].value;
	if(regNo!="")
	{
    //alert(vendorId);
    var url="checkSubcontractRegNo.html?ajax=1&decorator=simple&popup=true&regNo=" + encodeURI(regNo);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    
	}
	}
	
	function checkRegNoSave()
	{
	document.forms['subcontractorChargesForm'].elements['subcontractorCharges.description'].select();
	var regNo = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.regNumber'].value;
	if(regNo!="")
	{
    //alert(vendorId);
    var url="checkSubcontractRegNo.html?ajax=1&decorator=simple&popup=true&regNo=" + encodeURI(regNo);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    
	}
	}
        function handleHttpResponse2()
        {
			//alert("a");
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                //alert("a"+results);
                results = results.trim();
                //alert(results);
                if(results.length>0)
                {
                 	//alert("a"); 
                  document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personType'].select(); 					 				
                  
                
                 }else{
                 document.forms['subcontractorChargesForm'].elements['subcontractorCharges.description'].select();
                     alert("Registration# does not exist in ServiceOrder.");
                     document.forms['subcontractorChargesForm'].elements['subcontractorCharges.regNumber'].value="";
                     document.forms['subcontractorChargesForm'].elements['subcontractorCharges.serviceOrder'].value="";
					 document.forms['subcontractorChargesForm'].elements['subcontractorCharges.regNumber'].select();
					 return false;
                      }
             }
        }
        
     function checkValidNationalCode(){

      var vlCode = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.vlCode'].value;
	  var perId = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value;      
      	
    	if(vlCode != ""){
    		var url="checkValidNationalCode.html?ajax=1&decorator=simple&popup=true&vlCode=" + encodeURIComponent(vlCode);
    		http5.open("GET", url, true);
     		http5.onreadystatechange = handleHttpResponse55;
     		http5.send(null);
	    }
     }

	function handleHttpResponse55(){
				
      if (http5.readyState == 4){
              var results = http5.responseText
      			results=results.trim(); 
       			if(results!=''){    	
              	var res=results.split(",");              	
                   document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personName'].value=res[0];
                   document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value=res[1];
                   }
                 	 }
	}    

     
        
     function checkPerId(){
     var perId = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value;
     if(perId == ""){
     	document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personName'].value = "";
     }
    	if(perId != ""){
    		var url="matchCarrierCode.html?ajax=1&decorator=simple&popup=true&perId=" + encodeURI(perId);
    		http4.open("GET", url, true);
     		http4.onreadystatechange = handleHttpResponse44;
     		http4.send(null);
	    }
	}
	
	
	function handleHttpResponse44(){
     if (http4.readyState == 4){
              var results = http4.responseText
                results = results.trim();
                
                var res = results.split("#"); 
           		if(res.size() >= 2){ 
	           		if(res[2] == 'Approved'){
	           			 document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personName'].value = res[1];
	           			 try{
	 	           			pickComDiv();
	 	           			}catch(e){}	           			 
	           		}else{
	           			alert("Person ID is not approved" ); 
					    document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value = "";
                    	document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personName'].value = "";
						document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].select();
	           		}			
           }else{
                	alert("Person ID not valid");    
			    	document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value = "";
                    document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personName'].value = "";
					document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].select();
		   }
               
     }
}    
	function pickComDiv(){
		<configByCorp:fieldVisibility componentId="component.field.Alternative.actgCodeFlag">
			var vendorCode = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value;
   			if(vendorCode != ''){
   			    var url="vendorNameForActgCodeLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorCode)+"&companyDivision=&actgCodeFlag=YES";
   			     http21.open("GET", url, true);
   			     http21.onreadystatechange = handleHttpResponse21;
   			     http21.send(null);
   			    }
   	   </configByCorp:fieldVisibility>
	}
    function handleHttpResponse21(){
         if (http21.readyState == 4){
            var results = http21.responseText;
            var res = results.split("#");  
            if(res.length>2){
	                	if(res[8] != "null" && res[8].trim() !=''){
	                		document.forms['subcontractorChargesForm'].elements['subcontractorCharges.companyDivision'].value=res[8].trim();
	                		document.forms['subcontractorChargesForm'].elements['subcontractorCharges.companyDivision'].disabled=true;
	                	}
            }
         }
   }    
        
  function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
  var http2 = getHTTPObject();
  var http4 = getHTTPObject();
  var http5 = getHTTPObject();
  var http6 = getHTTPObject();
  var http21 = getHTTPObject();
  function checkPersionId()
  {

   var personType= document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personType'].value;
   
   var personID=document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value;
   if(personType=='')
   {
    alert('Type is a required field Please fill Type');
    return false;
   }
   if (personType!='A' && personType!=''&& personID=='')
   {
   
   	alert('Please fill personId');
   	document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].focus;
   	return false;
   }
   var companyDivision=document.forms['subcontractorChargesForm'].elements['subcontractorCharges.companyDivision'].value;
   if(companyDivision==''){
	alert('Please fill company Division');
	return false;
   }
  }
 
 function pick() { 
  		window.close();
	}
	
function displayRow()
{
	//alert('hi');
	var notRed = document.getElementById("captionRow1");
	var red = document.getElementById("captionRow2");
	var type=document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personType'].value;
	//alert(type);
	if(type=='A' || type=='')
	{
	red.style.display = 'none';
	notRed.style.display = '';
	}
	if(type!='A' && type!='')
	{
		red.style.display = '';
		notRed.style.display = 'none';
	}
	if(type=='D')
	{
	   document.getElementById("hid1").style.display="block";
	}
    if(type!='D')
    {
      document.getElementById("hid1").style.display="none";
    }
    var el = document.getElementById('truckhide');
	if(type=='T')	{
	   el.style.display = 'block';
		el.style.visibility = 'visible';
	}    
    else{
    document.forms['subcontractorChargesForm'].elements['subcontractorCharges.truckNo'].value='';
     el.style.display = 'none';
   }
}
	function hideCalenderImg()
	{
	var glCode=document.forms['subcontractorChargesForm'].elements['subcontractorCharges.glCode'].value;
	var amount=document.forms['subcontractorChargesForm'].elements['subcontractorCharges.amount'].value;
	if(glCode==''|| (amount==0 ||amount==0.00))
	{
	//alert("ashish in if");
	document.getElementById("hid2").style.display="none";
	}
	else if(glCode!=''&& (amount!=0 ||amount!=0.00))
	{
	  //alert("ashish in else ");
	  document.getElementById("hid2").style.display="block";
	}
	}
 
	</script>
	<script language="JavaScript">	
	function fillPastDate(){
    document.forms['subcontractorChargesForm'].elements['checkApprovedClick'].value ='1';
    }
    
    function findSystemPastDate(){
    var approved=document.forms['subcontractorChargesForm'].elements['subcontractorCharges.approved'].value;
    var post=document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].value; 
    if(approved!=''&&post=='')
    {
        var url="findSystemDate.html?ajax=1&decorator=simple&popup=true&approvedDate="+approved;
    		http4.open("GET", url, true);
     		http4.onreadystatechange = handleHttpResponse4;
     		http4.send(null); 
    }
    }
    
    function handleHttpResponse4(){
     if (http4.readyState == 4){
              var results = http4.responseText
                results = results.trim();
                 //alert(results);
                if(results.length>=1){
                	document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].value = results;
                	document.forms['subcontractorChargesForm'].elements['payAccDateDammy'].value = results;
                }else{
                document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].value ='';
                }
                document.forms['subcontractorChargesForm'].elements['checkApprovedClick'].value ='0';
                document.forms['subcontractorChargesForm'].elements['subcontractorCharges.amount'].select();
     	}
	}

function disablefield() { 
    if (document.forms['subcontractorChargesForm'].elements['subcontractorCharges.accountSent'].value!='')
    { 
    	
		var elementsLen=document.forms['subcontractorChargesForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['subcontractorChargesForm'].elements[i].type=='text')
					{
						document.forms['subcontractorChargesForm'].elements[i].readOnly =true;
						document.forms['subcontractorChargesForm'].elements[i].className = 'input-textUpper';						
					}
					else
					{						document.forms['subcontractorChargesForm'].elements[i].disabled=true;
					}						
			}
    
  }
}  
function enablefield() { 
	var elementsLen=document.forms['subcontractorChargesForm'].elements.length;
	for(i=0;i<=elementsLen-1;i++)
		{
			if(document.forms['subcontractorChargesForm'].elements[i].type=='text')
				{
					document.forms['subcontractorChargesForm'].elements[i].readOnly =false;
					document.forms['subcontractorChargesForm'].elements[i].className = 'input-text';						
				}
				else
				{						document.forms['subcontractorChargesForm'].elements[i].disabled=false;
				}						
		}
}
   function checkSenttoAccounting()
    {
		alert('You can not change the value as Sent to Accounting has been already filled'); 
		document.getElementById("subcontractorChargesForm").reset(); 
    }
 function getNonSettledAmount(position)
 {
 var name = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.personId'].value;
 if(name!=''){
 var url="findNonsettledAmount.html?ajax=1&decorator=simple&popup=true&subContcPersonId=" + encodeURI(name);
  ajax_showTooltip(url,position);	
  }
  else{
  alert("Please select Person's ID.");
  }
 }   	
 var varNameAcct = '';
 function checkAcctStatus(elementName)
 {
 	if(elementName.indexOf('_') > 0)
 		varNameAcct = elementName.split('_')[0];
 	else
 		varNameAcct = elementName;
 	return varNameAcct;
 }
 function hitAcct(){
	<c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">	 
 	if(varNameAcct == 'post')
 	{
 		calcDateFlexibility();
 		varNameAcct='';
 	}else{
 		varNameAcct='';
 		return false;
 	}
 	</c:if>	
 	<c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
 	if(varNameAcct == 'post')
 	{
 		calcPostDate();
 		varNameAcct='';
 	}else{
 		varNameAcct='';
 		return false;
 	}
 	</c:if>
 	
 }
 function calcDateFlexibility(){
	
		  if(document.forms['subcontractorChargesForm'].elements['subcontractorCharges.approved'].value==''||document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].value==''||document.forms['subcontractorChargesForm'].elements['payAccDateDammy'].value=='')
		  {	try{
			  if(document.forms['subcontractorChargesForm'].elements['subcontractorCharges.approved'].value==''){
				     alert('Approved Date Is Empty');
		   			document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].value='';
			  }}catch(e){}
		  }else{ 
			 var date4 = document.forms['subcontractorChargesForm'].elements['payAccDateDammy'].value; 
			 var date1 = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].value; 
			 var date1SplitResult = date1.split("-");
			 var day1 = date1SplitResult[0];
			 var month1 = date1SplitResult[1];
			 var year1 = date1SplitResult[2];
			   	 year1 = '20'+year1;
				 if(month1 == 'Jan'){ month1 = "01";  }
		    else if(month1 == 'Feb'){ month1 = "02";  }
			else if(month1 == 'Mar'){ month1 = "03";  }
			else if(month1 == 'Apr'){ month1 = "04";  }
			else if(month1 == 'May'){ month1 = "05";  }
			else if(month1 == 'Jun'){ month1 = "06";  }
			else if(month1 == 'Jul'){ month1 = "07";  }
			else if(month1 == 'Aug'){ month1 = "08";  }
			else if(month1 == 'Sep'){ month1 = "09";  }
			else if(month1 == 'Oct'){ month1 = "10";  }																					
			else if(month1 == 'Nov'){ month1 = "11";  }
			else if(month1 == 'Dec'){ month1 = "12";  }
					
			 var date2 = document.forms['subcontractorChargesForm'].elements['payAccDateDammy'].value; 
			 var date2SplitResult = date2.split("-");
			 var day2 = date2SplitResult[0];
			 var month2 = date2SplitResult[1];
			 var year2 = date2SplitResult[2];
			   	 year2 = '20'+year2;
				 if(month2 == 'Jan'){ month2 = "01";  }
		    else if(month2 == 'Feb'){ month2 = "02";  }
			else if(month2 == 'Mar'){ month2 = "03";  }
			else if(month2 == 'Apr'){ month2 = "04";  }
			else if(month2 == 'May'){ month2 = "05";  }
			else if(month2 == 'Jun'){ month2 = "06";  }
			else if(month2 == 'Jul'){ month2 = "07";  }
			else if(month2 == 'Aug'){ month2 = "08";  }
			else if(month2 == 'Sep'){ month2 = "09";  }
			else if(month2 == 'Oct'){ month2 = "10";  }																					
			else if(month2 == 'Nov'){ month2 = "11";  }
			else if(month2 == 'Dec'){ month2 = "12";  }


			 var currentD = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.approved'].value; 
			 var date3SplitResult = currentD.split("-");
			 var day3 = date3SplitResult[0];
			 var month3 = date3SplitResult[1];
			 var year3 = date3SplitResult[2];
			   	 year3 = '20'+year3;
				 if(month3 == 'Jan'){ month3 = "01";  }
		    else if(month3 == 'Feb'){ month3 = "02";  }
			else if(month3 == 'Mar'){ month3 = "03";  }
			else if(month3 == 'Apr'){ month3 = "04";  }
			else if(month3 == 'May'){ month3 = "05";  }
			else if(month3 == 'Jun'){ month3 = "06";  }
			else if(month3 == 'Jul'){ month3 = "07";  }
			else if(month3 == 'Aug'){ month3 = "08";  }
			else if(month3 == 'Sep'){ month3 = "09";  }
			else if(month3 == 'Oct'){ month3 = "10";  }																					
			else if(month3 == 'Nov'){ month3 = "11";  }
			else if(month3 == 'Dec'){ month3 = "12";  }
	 		
	 		
	 		var selectedDate = new Date(month1+"/"+day1+"/"+year1);
	 		var sysDefaultDate = new Date(month2+"/"+day2+"/"+year2);		
	 		var currentDate = new Date(month3+"/"+day3+"/"+year3);
	 		var daysApartNew = Math.round((sysDefaultDate-currentDate)/86400000);
	 		var daysApart = Math.round((selectedDate-currentDate)/86400000);
			var daysApartCurrent = Math.round((sysDefaultDate-selectedDate)/86400000);
			  if(daysApartNew<=0){
		 		  if((daysApart<0)&&(daysApartCurrent!=0)){
			 			 document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].value=date4;
			 			document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].focus();
			 		    alert("You cannot enter posting date less than the approved date."); 
			 		  }
	 		  }else{
		 		  if(daysApart<0){
		 				 document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].value=date4;
	 					document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].focus();
	 			    	alert("You cannot enter posting date less than the approved date."); 
	 		  			}
	 		  }
		}
	}
 
 
 
 function calcPostDate(){ 
	  if(document.forms['subcontractorChargesForm'].elements['subcontractorCharges.approved'].value==''||document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].value==''||document.forms['subcontractorChargesForm'].elements['payAccDateDammy'].value=='')
	  {	try{
		  if(document.forms['subcontractorChargesForm'].elements['subcontractorCharges.approved'].value==''){
			     alert('Approved Date Is Empty');
	   			document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].value='';
		  }}catch(e){}
	  }else{ 
		 var date4 = document.forms['subcontractorChargesForm'].elements['payAccDateDammy'].value; 
		 var date1 = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].value; 
		 var date1SplitResult = date1.split("-");
		 var day1 = date1SplitResult[0];
		 var month1 = date1SplitResult[1];
		 var lastmonth1 = 0;
		 var lastyear=date1SplitResult[2];
		 var lastmonth=month1;
		 var year1 = date1SplitResult[2];
		   	 year1 = '20'+year1;
			 if(month1 == 'Jan'){ month1 = "01";  lastmonth1=1; }
	    else if(month1 == 'Feb'){ month1 = "02";  lastmonth1=2; }
		else if(month1 == 'Mar'){ month1 = "03";  lastmonth1=3; }
		else if(month1 == 'Apr'){ month1 = "04";  lastmonth1=4;}
		else if(month1 == 'May'){ month1 = "05";  lastmonth1=5; }
		else if(month1 == 'Jun'){ month1 = "06";  lastmonth1=6; }
		else if(month1 == 'Jul'){ month1 = "07";  lastmonth1=7; }
		else if(month1 == 'Aug'){ month1 = "08";  lastmonth1=8; }
		else if(month1 == 'Sep'){ month1 = "09";  lastmonth1=9; }
		else if(month1 == 'Oct'){ month1 = "10";  lastmonth1=10; }																					
		else if(month1 == 'Nov'){ month1 = "11";  lastmonth1=11; }
		else if(month1 == 'Dec'){ month1 = "12";  lastmonth1=12; }
		var day = getLastDayOfMonth(lastmonth1,year1); 
		var lastdate4 = day+"-"+lastmonth+"-"+lastyear; 		
		 var date2 = document.forms['subcontractorChargesForm'].elements['payAccDateDammy'].value; 
		 var date2SplitResult = date2.split("-");
		 var day2 = date2SplitResult[0];
		 var month2 = date2SplitResult[1];
		 var year2 = date2SplitResult[2];
		   	 year2 = '20'+year2;
			 if(month2 == 'Jan'){ month2 = "01";  }
	    else if(month2 == 'Feb'){ month2 = "02";  }
		else if(month2 == 'Mar'){ month2 = "03";  }
		else if(month2 == 'Apr'){ month2 = "04";  }
		else if(month2 == 'May'){ month2 = "05";  }
		else if(month2 == 'Jun'){ month2 = "06";  }
		else if(month2 == 'Jul'){ month2 = "07";  }
		else if(month2 == 'Aug'){ month2 = "08";  }
		else if(month2 == 'Sep'){ month2 = "09";  }
		else if(month2 == 'Oct'){ month2 = "10";  }																					
		else if(month2 == 'Nov'){ month2 = "11";  }
		else if(month2 == 'Dec'){ month2 = "12";  } 
		var selectedDate = new Date(month1+"/"+day1+"/"+year1);
		var sysDefaultDate = new Date(month2+"/"+day2+"/"+year2); 
		var daysApartCurrent = Math.round((sysDefaultDate-selectedDate)/86400000);
		 var flag = 0;  
		if(daysApartCurrent>0){ 
		 			 document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].value=date4;
		 			document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].focus();
		 		    alert("You cannot enter Posting Date less than system default Posting Date."); 
		 		   flag = 1;
		  }else{
	 		   
		  }
		 if(flag == 0)
		  {
		   if(day1 != day)
		   {
		    alert("Posting date should be last day of the month.\n\nSystem will change the posting date as "+lastdate4); 
		   	document.forms['subcontractorChargesForm'].elements['subcontractorCharges.post'].value = lastdate4;
		   }
		  }	 
	}
}
 function getLastDayOfMonth(month,year)
 {
     var day;
     switch(month)
     {
         case 1 :
         case 3 :
         case 5 :
         case 7 :
         case 8 :
         case 10 :
         case 12 :
             day = 31;
             break;
         case 4 :
         case 6 :
         case 9 :
         case 11 :
                day = 30;
             break;
         case 2 :
             if( ( (year % 4 == 0) && ( year % 100 != 0) )
                            || (year % 400 == 0) )
                 day = 29;
             else
                 day = 28;
             break;

     }
     return day;

 }
</script>
<script>
this.onclick = function() {
   new Draggable('ajax_tooltipObj', 
                {starteffect: effectFunction('ajax_tooltipObj')});
   ajax_tooltipObj.style.cursor = "move";
}

function effectFunction(element)
{
   new Effect.Opacity(element, {from:0, to:1.0, duration:0.8});
}

function findServiceOrder1(position){
var regNo = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.regNumber'].value;
if(regNo==''){
alert('Please Enter Registration# first.');
return false;
}else{
var url="findServiceOrder.html?ajax=1&regNo="+regNo+"&decorator=simple&popup=true";
ajax_showTooltip(url,position);	
 }	
 }


 function findRegNo1(){
	 var serviceOrder=document.forms['subcontractorChargesForm'].elements['subcontractorCharges.serviceOrder'].value;
		 if(serviceOrder!=''){
			 var url="findRegNo.html?ajax=1&decorator=simple&popup=true&serviceOrder="+encodeURI(serviceOrder);
			 http6.open("GET", url, true);
		     http6.onreadystatechange = handleHttpResponse6;
		     http6.send(null);
	     }
 }
 function handleHttpResponse6(){
			 if (http6.readyState == 4){
	              var results = http6.responseText;             
	      			results=results.trim();
					results = results.replace('[','');
					results = results.replace(']','');					
    	  			if(results!=''){   	  			    	
	    	          var res=results.split("~"); 
	    	          if(res!=''){       	
    	              document.forms['subcontractorChargesForm'].elements['subcontractorCharges.regNumber'].value=res[0];
    	              } 	             
                   }else
                   {
                    alert('ServiceOrder# does not exist.');
                    document.forms['subcontractorChargesForm'].elements['subcontractorCharges.serviceOrder'].value='';
                   }
           }
 }
 
 function goToUrlZip(id,targetField){

	document.forms['subcontractorChargesForm'].elements['subcontractorCharges.serviceOrder'].value = id;
	document.forms['subcontractorChargesForm'].elements['subcontractorCharges.serviceOrder'].focus();
	 }
 function checkDivisionField(){
	 enablefield();
	 <configByCorp:fieldVisibility componentId="component.field.SubContChrg.divisionSoftValidation">	 		
		var divValue = document.forms['subcontractorChargesForm'].elements['subcontractorCharges.division'].value;
		if(divValue == '00'){			
			var header = '\t\t\t\t\tWarning';
			var type = 'This record has GL division as 00 would you like to proceed?';
			var msg = 'Click Ok to save or Cancel to change the value.';
			var agree = confirm(header+"\n"+type+"\n"+msg);
	     	if(agree){
	     		document.forms['subcontractorChargesForm'].submit();
	     	}else{		
	     		disablefield();     	   		
	     		return false;	
	     	}
		}
		</configByCorp:fieldVisibility>	
 }
 
</script>
</head> 
<s:form id="subcontractorChargesForm" action="saveSpliteSubcontractor.html?btntype=yes&decorator=popup&popup=true&jspPageFlag=${jspPageFlag}" onsubmit="return checkDivisionField();" method="post" validate="true">
     <s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
     <s:hidden name="jspPageFlag" value="<%=request.getParameter("jspPageFlag") %>"/>
    <c:set var="btntype"  value="<%=request.getParameter("btntype") %>"/>
    <c:set var="jspPageFlag"  value="<%=request.getParameter("jspPageFlag") %>"/>
<s:hidden id="checkApprovedClick" name="checkApprovedClick" />
	 <configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
	<s:hidden name="glCodeFlag" value="${glCodeFlag}"/>
</configByCorp:fieldVisibility>
	<s:hidden name="subcontractorCharges.deductTempFlag" value="false"/>
	<s:hidden name="subcontractorCharges.source" />

    <s:hidden name="description" />
	<s:hidden name="secondDescription" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription" />
	<s:hidden name="sixthDescription" />	
	<s:hidden name="seventhDescription" />
	<s:hidden name="ninthDescription" />
	<s:hidden name="tenthDescription" />


	
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
 	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 	<s:hidden  name="subcontractorCharges.id"/>
 	<s:hidden  name="subcontractorCharges.vanLineNumber"/>  
 	<s:hidden name="dueFormAmount" />
 	
 	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
    <s:hidden name="formStatus" value=""/> 
     <c:if test="${errorValidator == 'errorMethod' && validateFormNav != 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.subcontractList' }">
    <c:redirect url="/saveSubcontractorCharges.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
    
   <c:if test="${validateFormNav == 'OK' && errorValidator != 'errorMethod'} " >
<c:choose>
<c:when test="${gotoPageString == 'gototab.subcontractList' }">
    <c:redirect url="/subcontractorChargesList.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>

 	<div id="Layer1" style="width:98%" onkeydown="changeStatus()">
<div id="newmnav">
	<ul> 
	<li id="newmnav1" style="background:#FFF "> <a  class="current"><span>Subcontractor Charges Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
   <c:choose>
   <c:when test="${jspPageFlag=='miscellaneousVanLineForm'}">
    <li><a href="editMiscellaneousVanLine.html?id=${subcontractorCharges.parentId}&dueFormAmount=${dueFormAmount}&glCodeFlag=${glCodeFlag}"><span>Subcontractor Charges List</span></a></li>
   </c:when>
   <c:when test="${jspPageFlag=='miscellaneousWithoutVanLineForm'}">
   <li><a href="editMiscellaneousWithoutVanLine.html?id=${subcontractorCharges.parentId}&dueFormAmount=${dueFormAmount}&glCodeFlag=${glCodeFlag}"><span>Subcontractor Charges List</span></a></li>
   </c:when>
   <c:when test="${jspPageFlag=='vanLineForm'}">
   <li><a href="editVanLine.html?id=${subcontractorCharges.parentId}&dueFormAmount=${dueFormAmount}&glCodeFlag=${glCodeFlag}"><span>Subcontractor Charges List</span></a></li>
   </c:when> 
   <c:when test="${jspPageFlag=='vanLineWithOutForm'}">
   <li><a href="editWithOutVanLine.html?id=${subcontractorCharges.parentId}&dueFormAmount=${dueFormAmount}&glCodeFlag=${glCodeFlag}"><span>Subcontractor Charges List</span></a></li>
    </c:when>
   <c:otherwise>
   <li><a href="splitCharge.html?id=${subcontractorCharges.parentId}&dueFormAmount=${dueFormAmount}&glCodeFlag=${glCodeFlag}"><span>Subcontractor Charges List</span></a></li> 
   </c:otherwise>
   </c:choose> 
	</ul>
</div> 	
<div class="spn">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" ><span></span></div>
   <div class="center-content">
    <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="width:970px;">
    <tbody>
    <tr>
  	<td align="left" class="listwhitetext">
  
  	<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	<tbody>
	<tr><td height="5"></td></tr>
  	<tr>
    <td align="right" class="listwhitetext" style="padding-right:5px;" width="115px" ><fmt:message key="subcontractorCharges.branch"/></td>
    <c:if test="${empty subcontractorCharges.accountSent}">
    <td align="left" height="28px" class="listwhitetext" width="150"><s:select cssClass="list-menu" name="subcontractorCharges.branch" list="%{BranchList}" cssStyle="width:100px" headerKey="" headerValue=" "  tabindex="1" /></td>
    </c:if>
    <c:if test="${not empty subcontractorCharges.accountSent}">
    <td align="left" height="28px" class="listwhitetext" width="150"><s:select cssClass="list-menu" name="subcontractorCharges.branch" list="%{BranchList}" cssStyle="width:100px" headerKey="" headerValue=" "  tabindex="1" onclick="checkSenttoAccounting();"/></td>
    </c:if>
    
     <td align="right" class="listwhitetext" style="padding-right:5px;" width="100px" ><fmt:message key="subcontractorCharges.parentID"/></td>
    <td align="left" colspan="2" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="width:96px" maxlength="20" name="subcontractorCharges.parentId" readonly="true" tabindex="2"/></td>
    <td colspan="4">
    <configByCorp:fieldVisibility componentId="component.field.subconCharges.job">
    <div id="job1" >
    <table style="margin:0px;padding:0px;">
    <tr>
    <c:if test="${empty subcontractorCharges.accountSent}">
    <td align="left" class="listwhitetext" colspan="2" style="padding-left:45px;">Job&nbsp;Type&nbsp; 
     <s:select id="jobService" cssClass="list-menu" key="subcontractorCharges.job" list="%{job}" cssStyle="width:166px" headerKey="" headerValue="" /></td>
    </c:if>
    <c:if test="${not empty subcontractorCharges.accountSent}">
    <td align="left" class="listwhitetext" colspan="2" style="padding-left:45px;">Job&nbsp;Type&nbsp; 
     <s:select id="jobService" cssClass="list-menu" key="subcontractorCharges.job" list="%{job}" cssStyle="width:166px" headerKey="" headerValue=""  onclick="checkSenttoAccounting();"/></td>
    </c:if>
    </tr>
    </table>
    </div>  
    </configByCorp:fieldVisibility>  
     <configByCorp:fieldVisibility componentId="component.field.subcontractorCharges.validation">     
    <div id="division2" style="padding-left:56px;" >
     Division&nbsp;<s:select cssClass="list-menu" list="%{division}" name="subcontractorCharges.division" headerKey="00" headerValue="" cssStyle="width:60px" />
    </div>    
    </configByCorp:fieldVisibility>
    </td>
    </tr> 
    
  <tr>
    <td align="right" class="listwhitetext" style="padding-right:5px;"><fmt:message key="subcontractorCharges.regNumber"/><!--<font color="red" size="2">*</font></td>--></td>
    <td align="left"  height="28px" class="listwhitetext"><s:textfield cssClass="input-text" key="subcontractorCharges.regNumber" cssStyle="width:96px" maxlength="25" onchange="checkRegNo();" tabindex="3"/></td>
    <td align="right" class="listwhitetext" style="padding-right:5px;">Service Order#</td>
    <td align="left"   class="listwhitetext"><s:textfield cssClass="input-text" key="subcontractorCharges.serviceOrder" cssStyle="width:96px" maxlength="25" onchange="findRegNo1();" tabindex="4"/></td>
    <td align="left" class="listwhitetext" style="width:10px"><div id="serviceOrderList" style="vertical-align:middle;"><a><img class="openpopup" id="navigation8" onclick="findServiceOrder1(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Service Order List" title="Service Order List" /></a></td>
    
     <td colspan="3">
    <table style="margin: 0px; padding: 0px; width: 165px;">
    <tr>
    <c:if test="${empty subcontractorCharges.accountSent}">
     <td align="right" class="listwhitetext">Company&nbsp;Division <font color="red" size="2">*</font></td>
     <td><s:select  name="subcontractorCharges.companyDivision" id="compdiv"  cssClass="list-menu"  cssStyle="width:60px" headerKey="" headerValue=""  list= "%{subcontrCompanyCodeList}"   />
     </td>
    </c:if>    
    <c:if test="${not empty subcontractorCharges.accountSent}">
     <td align="left" class="listwhitetext" colspan="2">Company&nbsp;Division<font color="red" size="2">*</font> &nbsp;
     <s:select  name="subcontractorCharges.companyDivision"  id="compdiv" cssClass="list-menu"  cssStyle="width:60px" headerKey="" headerValue=""  list= "%{subcontrCompanyCodeList}"   onclick="checkSenttoAccounting();"/></td>
    </c:if> 
     </tr>
    </table> 
    </td>
    
  </tr>  
  
  <tr>
   <td align="right" class="listwhitetext" style="padding-right:5px;"><fmt:message key="subcontractorCharges.personType"/><font color="red" size="2">*</font></td>
   <c:if test="${empty subcontractorCharges.accountSent}">
   <td ><s:select cssClass="list-menu" name="subcontractorCharges.personType" list="%{personTypeList}" cssStyle="width:100px" headerKey="" headerValue=" " onchange="displayRow();changeStatus();"  tabindex="5"/></td> 
  </c:if>
  <c:if test="${not empty subcontractorCharges.accountSent}">
  <td ><s:select cssClass="list-menu" name="subcontractorCharges.personType" list="%{personTypeList}" cssStyle="width:100px" headerKey="" headerValue=" " onchange="displayRow();changeStatus();" tabindex="5" onclick="checkSenttoAccounting();"/></td> 
  </c:if>
  
  <td colspan="3">
  <div id="truckhide">
	   <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin: 0px;padding:0px;">
		<tbody>
		<tr>
		<td align="right" class="listwhitetext" width="12px"></td>
	    <c:if test="${empty subcontractorCharges.personId}">
	   <td align="right" class="listwhitetext" style="padding-right:5px;" width="88px" >Truck#</td>
	   <td align="left"  class="listwhitetext" ><s:textfield cssClass="input-text" cssStyle="width:96px" maxlength="20" name="subcontractorCharges.truckNo" onkeydown="return onlyDel(event,this)" readonly="true" tabindex="6"/><img class="openpopup" align="top" width="17" height="20" onclick="openAccountPopWindow1();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	   </c:if>
	   <c:if test="${not empty subcontractorCharges.personId}">
	   <td align="right" class="listwhitetext" style="padding-right:5px;" width="88px" >Truck#</td>
	   <td align="left"  class="listwhitetext" ><s:textfield cssClass="input-text" cssStyle="width:96px" maxlength="20" name="subcontractorCharges.truckNo" onkeydown="return onlyDel(event,this)" readonly="true" tabindex="6"/><img class="openpopup" align="top" width="17" height="20" onclick="openAccountPopWindow2();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	   </c:if>
 	</tr>
	</tbody>
	</table>
    </div>
   </td>
  </tr> 
  
  <tr>
   <td align="right" id="captionRow2" class="listwhitetext" style="padding-right:5px;" ><fmt:message key="subcontractorCharges.personId"/><font color="red" size="2">*</font></td>
   <td align="right" id="captionRow1" class="listwhitetext" style="padding-right:5px;" ><fmt:message key="subcontractorCharges.personId"/></td>
   <td align="left" height="30px" class="listwhitetext" width="130px"><s:textfield cssClass="input-text" key="subcontractorCharges.personId" cssStyle="width:96px" maxlength="14" onchange="checkPerId();" tabindex="7"/>
   <c:if test="${empty subcontractorCharges.accountSent}">
   <img class="openpopup" align="top" width="17" height="20" onclick="openAccountPopWindow();" src="<c:url value='/images/open-popup.gif'/>" />
   </c:if>
   <c:if test="${not empty subcontractorCharges.accountSent}">
   <img class="openpopup" align="top" width="17" height="20" onclick="javascript:alert('You can not change the Person ID as Sent to Accounting has been already filled')" src="<c:url value='/images/open-popup.gif'/>" />
   </c:if>
   </td>
   <td align="right" class="listwhitetext" style="padding-right:5px;" width="100px" >VL Code</td>
   <td align="left"  class="listwhitetext" width="10px"><s:textfield cssClass="input-text" cssStyle="width:96px;" maxlength="20" name="subcontractorCharges.vlCode" onchange="checkValidNationalCode();" tabindex="8"/></td>
    
   <td align="right" class="listwhitetext" style="padding-right:5px;" width="70px" >Name</td>
   <td align="left"  class="listwhitetext" width="20"><s:textfield cssClass="input-text" cssStyle="width:196px;" maxlength="50" name="subcontractorCharges.personName" readonly="true" tabindex="9"/></td>
   <td width="200px">
   		<div style="width:200px;">
	   <div id="hid1">
		   <table class="detailTabLabel" cellspacing="0" cellpadding="0"  border="0" width="190px">
			   <tr> 
				   	<c:if test="${empty subcontractorCharges.accountSent}">
				   		<td align="right" class="listwhitetext" style="padding-right:5px;"><s:checkbox key="subcontractorCharges.flag1099"  tabindex="10" /></td>
				   	</c:if>
				   	<c:if test="${not empty subcontractorCharges.accountSent}">
				   		<td align="right" class="listwhitetext" style="padding-right:5px;"><s:checkbox key="subcontractorCharges.flag1099"  tabindex="10" onclick="checkSenttoAccounting();"/></td>
				   	</c:if>
				   	<td align="left" class="listwhitetext" ><fmt:message key="subcontractorCharges.flag1099"/></td>
			  
			  		<c:set var="isSettled" value="false" />
					<c:if test="${subcontractorCharges.isSettled}">
						<c:set var="isSettled" value="true" />
					</c:if>
					<td align="center" class="listwhitetext">Settled<s:checkbox key="subcontractorCharges.isSettled" value="${isSettled}" fieldValue="true" onclick="changeStatus();" cssStyle="vertical-align:middle;" tabindex="11"/></td>
			  </tr>
		  </table>
	  </div>
	  </div>
  </td>
  </tr>
  
  <tr>
   <td align="right" class="listwhitetext" style="padding-right:5px;"><fmt:message key="subcontractorCharges.glCode"/></td>
   <c:if test="${empty subcontractorCharges.accountSent}">
   <td colspan="3"><s:select cssClass="list-menu" name="subcontractorCharges.glCode" list="%{glCode}" cssStyle="width:215px" headerKey="" headerValue=" " onchange="hideCalenderImg();checkBalanceForward();changeStatus();" tabindex="12"/></td> 
  </c:if>
  <c:if test="${not empty subcontractorCharges.accountSent}">
  <td colspan="3"><s:select cssClass="list-menu" name="subcontractorCharges.glCode" list="%{glCode}" cssStyle="width:215px" headerKey="" headerValue=" " onchange="hideCalenderImg();checkBalanceForward();changeStatus();" tabindex="12" onclick="checkSenttoAccounting();"/></td> 
  </c:if>
  </tr> 
  
  
  <tr>
  <td align="right" class="listwhitetext" style="padding-right:5px;"><fmt:message key="subcontractorCharges.amount"/></td>
   <td align="left" height="28px" class="listwhitetext"><s:textfield cssClass="input-text" key="subcontractorCharges.amount" cssStyle="width:96px;text-align:right;" maxlength="14"  onblur="chkSelect();hideCalenderImg();" tabindex="13" onchange="hideCalenderImg();" /></td>
  <td colspan="10">
  <table class="detailTabLabel" border="0" style="margin-left:42px">
  <tr>   
  <td align="right" class="listwhitebox" valign="middle"><fmt:message key='subcontractorCharges.balanceForward'/></td>
  <c:if test="${empty subcontractorCharges.accountSent}">
  <td align="left" width="38"><s:checkbox name="subcontractorCharges.balanceForward" id="balanceForwardId" value="${subcontractorCharges.balanceForward}" fieldValue="true" disabled="disabled" onclick="checkBalanceForward();" tabindex="14"/></td>
  </c:if>
  <c:if test="${not empty subcontractorCharges.accountSent}">
  <td align="left" width="38"><s:checkbox name="subcontractorCharges.balanceForward" id="balanceForwardId" value="${subcontractorCharges.balanceForward}" fieldValue="true" disabled="disabled" onclick="checkBalanceForward();checkSenttoAccounting();" tabindex="14"/></td>
  </c:if>
  
  <c:set var="personalEscrow" value="false" />
					<c:if test="${subcontractorCharges.personalEscrow}">
						<c:set var="personalEscrow" value="true" />
					</c:if>
					<td align="left" class="listwhitetext">Personal&nbsp;Escrow</td>
					<td width="47"><s:checkbox key="subcontractorCharges.personalEscrow" value="${personalEscrow}" fieldValue="true" onclick="changeStatus();" cssStyle="vertical-align:middle;" tabindex="15"/></td>
					<c:set var="onAccount" value="false" />
					<c:if test="${subcontractorCharges.onAccount}">
						<c:set var="onAccount" value="true" />
					</c:if>
					<td align="left" class="listwhitetext" >On Account&nbsp;<s:checkbox key="subcontractorCharges.onAccount" value="${onAccount}" fieldValue="true" onclick="changeStatus();" cssStyle="vertical-align:middle;" tabindex="15"/></td>
  </tr>
  </table>
  </td>
  </tr>
  
  <tr>
   <td align="right" class="listwhitetext" style="padding-right:5px;"><fmt:message key="subcontractorCharges.description"/></td>
   <td align="left" colspan="2"  class="listwhitetext"><s:textfield cssClass="input-text" key="subcontractorCharges.description" cssStyle="width:210px" maxlength="225" tabindex="16" id="fillPostDate" onblur="" onselect="findSystemPastDate();"/></td>
  <td>
  <input type="button" class="cssbutton1" value="Check Amt" style="width:100px;" tabindex="17" onclick="getNonSettledAmount(this);">
  </td>
  <td align="right" class="listwhitetext">Advance&nbsp;Date</td>
	    <c:if test="${not empty subcontractorCharges.advanceDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="subcontractorCharges.advanceDate"/></s:text>
			 <td>&nbsp;<s:textfield id="advanceDate" cssClass="input-textUpper"  name="subcontractorCharges.advanceDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px;vertical-align:top;" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="advanceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty subcontractorCharges.advanceDate}">
		<td>&nbsp;<s:textfield id="advanceDate"  cssClass="input-textUpper" name="subcontractorCharges.advanceDate" cssStyle="width:65px;vertical-align:top;" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="advanceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
  </tr>
  
  <tr><td height="5"></td></tr>
  
  <tr> 
  <td class="listwhitetext" colspan="5">
  <div id="hid2"> 
   <table class="detailTabLabel" cellspacing="0" cellpadding="0"  border="0"><tr>
     <td align="right" class="listwhitetext" width="115"><fmt:message key="subcontractorCharges.approved" /></td>
     <td width="5"></td>
    <c:if test="${not empty subcontractorCharges.approved}"> 
	   <s:text id="subcontractorChargesApprovedDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="subcontractorCharges.approved"/></s:text>
	   <td><s:textfield cssClass="input-text" id="approved" name="subcontractorCharges.approved" value="%{subcontractorChargesApprovedDateFormattedValue}" readonly="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" onselect="fillPastDate(); return false;"/>
	   <c:if test="${empty subcontractorCharges.accountSent}">
	  	<img id="approved_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
	   </c:if>
	   <c:if test="${not empty subcontractorCharges.accountSent}">
	 	  <img id="" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent to Accounting has been already filled'); return false;"/>
	   </c:if>
	   </td>
	 </c:if>
	 <c:if test="${empty subcontractorCharges.approved}"> 
	   <td><s:textfield cssClass="input-text" id="approved" name="subcontractorCharges.approved"  cssStyle="width:65px;" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="" onselect="fillPastDate(); return false;"/>
	   <c:if test="${empty subcontractorCharges.accountSent}">
	   	<img id="approved_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
	   </c:if>
	   <c:if test="${not empty subcontractorCharges.accountSent}">
	  	 <img id="" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent to Accounting has been already filled'); return false;"/>
	   </c:if>
	   </td>
	  </c:if>
	</tr></table>
   </div></td>
  </tr>	
 <%-- <tr> 
   <td align="right" class="listwhitetext" style="padding-right:5px;"><s:checkbox key="subcontractorCharges.flag1099"  tabindex="" /></td>
  <td align="left" height="28px" class="listwhitetext" ><fmt:message key="subcontractorCharges.flag1099"/></td>
  </tr>--%> 
  
  <tr style="">
   <td align="right" class="listwhitetext" style="padding-right:5px; margin-bottom:0;"><fmt:message key="subcontractorCharges.post"/></td>
   <c:if test="${not empty subcontractorCharges.post}">
	 <s:text id="subcontractorChargesApprovedDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="subcontractorCharges.post" /></s:text>
	 <td height="28px" style="margin-bottom:0;"><s:textfield cssClass="input-text" id="post" name="subcontractorCharges.post" value="%{subcontractorChargesApprovedDateFormattedValue}" cssStyle="width:65px;" maxlength="11" onfocus="changeStatus();" readonly="true"/>
	 
	 	<c:if test="${empty subcontractorCharges.accountSent}">	 
		 <img id="post_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="checkAcctStatus(this.id);"/>
	   </c:if>
	   <c:if test="${not empty subcontractorCharges.accountSent}">
	 	  <img id="" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent to Accounting has been already filled'); return false;"/>
	   </c:if>		
	
	</td>
   </c:if>
   <c:if test="${empty subcontractorCharges.post}">
	<td height="28px" style=" margin-bottom:0;"><s:textfield cssClass="input-text" id="post" name="subcontractorCharges.post"  cssStyle="width:65px;" maxlength="11" readonly="true"/> 
	 		
		<c:if test="${empty subcontractorCharges.accountSent}">
		<img id="post_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="checkAcctStatus(this.id);"/>
		</c:if>
	   <c:if test="${not empty subcontractorCharges.accountSent}">
	 	  <img id="" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent to Accounting has been already filled'); return false;"/>
	   </c:if>		
		
	</td>
  </c:if> 
  <td ></td>
  <td ></td>
  <td ></td>
  <td ></td>
</tr>

  <tr >
   <td align="right" class="listwhitetext" style="padding-right:5px;"><fmt:message key="subcontractorCharges.accountSent"/></td>
     <c:if test="${not empty subcontractorCharges.accountSent}">
		<s:text id="subcontractorChargesApprovedDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="subcontractorCharges.accountSent" /></s:text>
		<td height="28px" style=""><s:textfield cssClass="input-textUpper" id="accountSent" name="subcontractorCharges.accountSent" value="%{subcontractorChargesApprovedDateFormattedValue}" cssStyle="width:96px;" maxlength="11" onkeydown=" " onfocus="changeStatus();" readonly="true"/>
		<%-- <img id="calender3" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['subcontractorChargesForm'].accountSent,'calender3',document.forms['subcontractorChargesForm'].dateFormat.value); return false;" />--%>
		</td>
	</c:if>
	<c:if test="${empty subcontractorCharges.accountSent}">
		<td height="28px" style=""><s:textfield cssClass="input-textUpper" id="accountSent" name="subcontractorCharges.accountSent" cssStyle="width:96px;" maxlength="11" onkeydown="" readonly="true"/>
		<%--<img id="calender3" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['subcontractorChargesForm'].accountSent,'calender3',document.forms['subcontractorChargesForm'].dateFormat.value); return false;" />--%>
		</td>
	</c:if> 
	<td align="right" class="listwhitetext" style="padding-right:5px;"><fmt:message key="subcontractorCharges.sentToAccountVia"/></td>
    <td align="left" class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" key="subcontractorCharges.sentToAccountVia" cssStyle="width:175px;" maxlength="100" tabindex="18" readonly="true"/></td>
   
  </tr>  
  <tr><td style="!height:70px;height:20px;"></td></tr> 
  </tbody>
  </table>
  </td> 
  </tr>
  </tbody>
  </table>
  </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
  <table border="0" style="width:721px">
	<tbody>
	  <tr><td align="left" rowspan="1"></td></tr>
	  <tr>
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='subcontractorCharges.createdOn'/></td>
		<fmt:formatDate var="containerCreatedOnFormattedValue" value="${subcontractorCharges.createdOn}"  pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="subcontractorCharges.createdOn" value="${containerCreatedOnFormattedValue}" />
		 <td><fmt:formatDate value="${subcontractorCharges.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
		 <td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='subcontractorCharges.createdBy' /></td>
		  <c:if test="${not empty subcontractorCharges.id}">
		    <s:hidden name="subcontractorCharges.createdBy"/>
		    <td><s:label name="createdBy" value="%{subcontractorCharges.createdBy}"/></td>
		 </c:if>
		 <c:if test="${empty subcontractorCharges.id}">
			 <s:hidden name="subcontractorCharges.createdBy" value="${pageContext.request.remoteUser}"/>
			 <td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
		</c:if>
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='subcontractorCharges.updatedOn'/></td>
		<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${subcontractorCharges.updatedOn}"  pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="subcontractorCharges.updatedOn" value="${containerUpdatedOnFormattedValue}" />
		<td><fmt:formatDate value="${subcontractorCharges.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
		<td align="right" class="listwhitetext" style="width:85px"><b><fmt:message key='subcontractorCharges.updatedBy' /></td>
			<c:if test="${not empty subcontractorCharges.id}">
			    <s:hidden name="subcontractorCharges.updatedBy"/>
			    <td><s:label name="updatedBy" value="%{subcontractorCharges.updatedBy}"/></td>
			 </c:if>
			 <c:if test="${empty subcontractorCharges.id}">
				 <s:hidden name="subcontractorCharges.updatedBy" value="${pageContext.request.remoteUser}"/>
				 <td><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
    </tr>
   </tbody>
   </table>
   <c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
   	<s:text id="subcontractorChargesApprovedDateFormattedValue1" name="${FormDateValue}"><s:param name="value" value="subcontractorCharges.post"/></s:text>	
	<s:hidden id="payAccDateDammy"  name="payAccDateDammy" value="%{subcontractorChargesApprovedDateFormattedValue1}"/> 
    </c:if>
   <c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
   <s:text id="subcontractorChargesApprovedDateFormattedValue1" name="${FormDateValue}"><s:param name="value" value="systemDefault.postDate1"/></s:text>	
	<s:hidden id="payAccDateDammy"  name="payAccDateDammy" value="%{subcontractorChargesApprovedDateFormattedValue1}"/> 
   </c:if>
   <s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" onmouseover="return chkSelectSave();" onclick="return checkPersionId();" />   
   <s:reset cssClass="cssbuttonA" key="Reset" onmousemove="displayRow();hideCalenderImg();displayComp();" onclick="displayRow();hideCalenderImg();displayComp();" cssStyle="width:55px; height:25px" />
  </div> 
<c:if test="${btntype=='yes'}"> 
<c:choose>
  <c:when test="${jspPageFlag=='miscellaneousVanLineForm'}">
  <c:redirect url="/editMiscellaneousVanLine.html?id=${subcontractorCharges.parentId}&glCodeFlag=${glCodeFlag}"></c:redirect>
  </c:when>
  <c:when test="${jspPageFlag=='miscellaneousWithoutVanLineForm'}">
  <c:redirect url="/editMiscellaneousWithoutVanLine.html?id=${subcontractorCharges.parentId}&glCodeFlag=${glCodeFlag}"></c:redirect>
  </c:when>
  <c:when test="${jspPageFlag=='vanLineForm'}">
  <c:redirect url="/editVanLine.html?id=${subcontractorCharges.parentId}&glCodeFlag=${glCodeFlag}"></c:redirect>
  </c:when> 
  <c:when test="${jspPageFlag=='vanLineWithOutForm'}">
  <c:redirect url="/editWithOutVanLine.html?id=${subcontractorCharges.parentId}&glCodeFlag=${glCodeFlag}"></c:redirect>
  </c:when>
  <c:otherwise>
 <c:redirect url="/splitCharge.html?id=${subcontractorCharges.parentId}&dueFormAmount=${dueFormAmount}&glCodeFlag=${glCodeFlag}"></c:redirect>
</c:otherwise>
  </c:choose> 
</c:if> 
  </s:form>   
  
<script type="text/javascript"> 
//if(document.forms['subcontractorChargesForm'].elements['btntype'].value=='yes'){
//pick();

//}
 function displayComp(){
	 	var cDiv = 'N';
	 	 <configByCorp:fieldVisibility componentId="component.field.Alternative.actgCodeFlag">
	 		 cDiv='Y'
	 	 </configByCorp:fieldVisibility>
	 		 var comDiv ="${subcontractorCharges.companyDivision}";
	 		 if((cDiv.trim()=='Y')&&(comDiv != null)&&(comDiv != undefined)&&(comDiv.trim() != '')){
	 			 document.getElementById('compdiv').disabled=true;
	 		 }
	 }
try{
disablefield();
}
catch(e){}
try{
displayRow();
}
catch(e){}
try{
hideCalenderImg();
}
catch(e){}
try{
	displayComp();	
}catch(e){}
try{
	checkBalanceForward();	
}catch(e){}
try{
	var jobtype = 0;
	 <configByCorp:fieldVisibility componentId="component.field.subcontractorCharges.validation">
	 jobtype = 14;	 
	 </configByCorp:fieldVisibility>
	    if(jobtype>=14){
		 document.getElementById('job1').style.display = 'none';
		 }else{
		 document.getElementById('job1').style.display = 'block';
			}
}catch(e){
	
}
function disableComDiv(){
	var com=document.forms['subcontractorChargesForm'].elements['subcontractorCharges.companyDivision'].value;
	if(com!=''){
			document.forms['subcontractorChargesForm'].elements['subcontractorCharges.companyDivision'].disabled=true;		
	}else{
		document.forms['subcontractorChargesForm'].elements['subcontractorCharges.companyDivision'].disabled=false;
	}
}
</script>  


<script type="text/javascript">
	<c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
	setOnSelectBasedMethods(["findSystemPastDate(),hitAcct()"]);
	</c:if>
	<c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
	setOnSelectBasedMethods(["findSystemPastDate(),hitAcct()"]);
	</c:if>
	setCalendarFunctionality();
</script>
