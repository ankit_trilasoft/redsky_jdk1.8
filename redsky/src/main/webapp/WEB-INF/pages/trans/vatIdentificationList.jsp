<%@ include file="/common/taglibs.jsp"%>  
<head>  
<title>Vat Identification List</title>
<script type="text/javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to Delete this Vat?");
	var did = targetElement;
	if (agree){
		location.href='vatIdentificationDelete.html?id='+did;
	}else{
		return false;
	}
}

function clear_fields(){
    document.forms['vatIdentificationList'].elements['vatCountryCode'].value = "";				
}
</script>
</head>
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editVatIdentification.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set> 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="searchByCountryCode" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/>  
</c:set> 
<s:form id="vatIdentificationList" action="searchVatCountryCode"  method="post"  validate="true">
  <div id="otabs">
				  <ul>
				    <li><a><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div>
				<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="table" border="0" style="width:100%;">
<thead>
<tr>
<th>Country</th>

<th style="border-left: hidden;"></th>
</tr></thead>	
		<tbody>
		<tr>
			<td>
				<s:select name="vatCountryCode" list="%{country}" cssClass="list-menu" cssStyle="width:195px" headerKey="" headerValue="" onchange="changeStatus();"/>
			   
			</td>
			<td width="230px" style="border-left: hidden;">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header" style="!margin-top:50px;"><span></span></div>
</div>
</div>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<div id="otabs" style="margin-top:-15px; ">
		  <ul>
		    <li><a class="current"><span>Vat Identification List</span></a></li>
		  </ul>
		</div>
		</tbody>
		</table>
<display:table name="vatList" class="table" pagesize="10" requestURI="" export="true" id="vatList">
<display:column property="countryCode" title="Country Code"  href="editVatIdentification.html" paramId="id" paramProperty="id"></display:column>
<display:column property="startWith" title="Start With"></display:column>
<display:column property="endWith" title="End With"></display:column>
<display:column property="numberOfBlock" title="Number Of Block"></display:column>
<display:column property="eachBlock" title="Each Block"></display:column>
<display:column property="blockValue" title="Block Value"></display:column>
<display:column title="Remove" style="width: 15px;">
<a><img align="middle" title="Remove" onclick="confirmSubmit(${vatList.id});"  style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>	</a> 
</display:column>
<display:setProperty name="export.excel.filename" value="Contract List.xls"/>   
<display:setProperty name="export.csv.filename" value="Contract List.csv"/>   
<display:setProperty name="export.pdf.filename" value="Contract List.pdf"/> 
</display:table>
<c:if test="${empty param.popup}">
<c:out value="${buttons}" escapeXml="false" />
</c:if>
</s:form>