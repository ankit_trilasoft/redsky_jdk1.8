<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
<head>
<script language="JavaScript"> 
function sendRequestWithdrawal(){ 
document.forms['requestWithdrawal'].action ='sendRequestWithdrawal.html?requestWithdrawalButton=yes&decorator=popup&popup=true';
document.forms['requestWithdrawal'].submit(); 
//self.close(); 
return true;
} 
</script>
</head> 
<s:form id="requestWithdrawal" name="requestWithdrawal" method="post"  action="" validate="true">
<s:hidden name="id" value="${partnerRateGrid.id}"/>
<s:hidden name="requestWithdrawalButton" value="${requestWithdrawalButton}"/>  
<table border="0" width="100%">
<tr>
<td align="center">

</td>
</tr>
</table>
<div id="layer1" style="width: 100%">
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style=""><span></span></div>
   <div class="center-content">
<table border="0" cellpadding="2" cellspacing="0" style=" " align="center">
<tr> 
<td align="left" style="" class="" colspan="2">
<fieldset style="width:90%; border:2px solid#efefef;"><legend>Info</legend>
<div style="line-height:16px; width:100%; padding:2px; color:#000" class="listwhitetext">
You have chosen to Withdraw this tariff. Doing so will take your rates out of the RedSky system. You will not be able to file new rates to take effect for 60 days from the Withdrawal date. If you wish to proceed, please state the reason that you wish to Withdraw below. Your request to Withdraw will be submitted to Security for review.
</div>
</fieldset>
</td>
</tr>
<tr>
<td align="left" class="listwhitetext"><s:textarea  name="emailMessage" value="${emailMessage}" rows="4" cols="110" cssClass="textarea"/></td>
</tr>
<tr height="3px"></tr>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<table><tr> 
<td><div id="buttonList">  
<input type="button" class="cssbutton" style="width:165px; height:25px" value="Send Withdrawal Request" onClick="return  sendRequestWithdrawal();">
<input type="button" class="cssbutton" style="width:55px; height:25px" value="close" onclick="window.close();"/> 
</div>
</td> 
<td>

</td>
</tr></table>
</div> 
</s:form> 
<script type="text/javascript"> 
try{
if(document.forms['requestWithdrawal'].elements['requestWithdrawalButton'].value=='OK'){ 
alert("Your withdrawal request has been sent successfully.");
self.close(); 
}
}
catch(e){}
</script>
</html>
