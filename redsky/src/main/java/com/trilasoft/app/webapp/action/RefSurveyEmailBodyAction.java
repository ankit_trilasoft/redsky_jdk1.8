package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.RefSurveyEmailBody;
import com.trilasoft.app.service.RefSurveyEmailBodyManager;

public class RefSurveyEmailBodyAction extends BaseAction{
	private RefSurveyEmailBodyManager refSurveyEmailBodyManager;
	private RefSurveyEmailBody refSurveyEmailBody;
	private List refSurveyEmailBodyList;
	private String sessionCorpID;
	private Long id;
	static final Logger logger = Logger.getLogger(RefSurveyEmailBody.class);	
	public RefSurveyEmailBodyAction()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID=user.getCorpID();		
	}
	public RefSurveyEmailBody getRefSurveyEmailBody() {
		return refSurveyEmailBody;
	}
	public void setRefSurveyEmailBody(RefSurveyEmailBody refSurveyEmailBody) {
		this.refSurveyEmailBody = refSurveyEmailBody;
	}
	public List getRefSurveyEmailBodyList() {
		return refSurveyEmailBodyList;
	}
	public void setRefSurveyEmailBodyList(List refSurveyEmailBodyList) {
		this.refSurveyEmailBodyList = refSurveyEmailBodyList;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setRefSurveyEmailBodyManager(
			RefSurveyEmailBodyManager refSurveyEmailBodyManager) {
		this.refSurveyEmailBodyManager = refSurveyEmailBodyManager;
	}

}
