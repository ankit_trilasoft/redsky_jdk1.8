<%@ include file="/common/taglibs.jsp"%>
<head>
   <title>Parameter Control List</title>
   <script language="javascript" type="text/javascript">
 
 function confirmDelete(targetElement){

var agree=confirm("Are you sure you want to remove this record?");

var did = targetElement;

if (agree){

location.href="deleteParameterControl.html?id="+did;

}else{

return false;

}

} 

function clear_fields(){
	document.forms['parameterControl'].elements['parameterControl.parameter'].value = "";
	document.forms['parameterControl'].elements['parameterControl.customType'].value = "";
	
	
}
function goToSearch(){
        document.forms['parameterControl'].action = 'searchParameterControls.html';
        document.forms['parameterControl'].submit();
}
function selectSearchField(){
		var parameter=document.forms['parameterControl'].elements['parameterControl.parameter'].value; 
		var customType= document.forms['parameterControl'].elements['parameterControl.customType'].value;
		var active=document.forms['parameterControl'].elements['parameterControl.active'].checked;
		var tsftFlag=document.forms['parameterControl'].elements['parameterControl.tsftFlag'].checked;
		var hybridFlag=document.forms['parameterControl'].elements['parameterControl.hybridFlag'].checked;
		
		}

</script>
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:0px;!margin-bottom:2px;margin-top:-16px;!margin-top:-17px;
padding:2px 0px;text-align:right;width:100%;}
form {margin-top:-30px;!margin-top:-5px;}
</style>    
</head>
<s:form name="parameterControl" id="parameterControl" action="searchParameterControls" >
<div id="otabs">
	<ul>
	<li><a class="current"><span>Search</span></a></li>
	</ul>
</div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
   <div id="liquid-round">
   <div class="top" style="margin-top:10px;!margin-top:-5px;"><span></span></div>
   <div class="center-content">
   <c:set var="buttons">
      <input type="button" class="cssbutton1" style="width:55px;" value="<fmt:message key="button.add"/>" onclick="location.href='<c:url value="editParameterControl.html"/>'"/>
   </c:set>
   <c:set var="searchbuttons">   
     <input type="button" class="cssbutton1"  value="Search" style="width:58px;" onclick="goToSearch();"/> 
     <input type="button" class="cssbutton1" value="Clear" style="width:58px;" onclick="clear_fields();"/> 
  </c:set>
  <c:set var="getAllButton">
  <s:submit name="GetAll" value="GetAll" method="list" action="getAllParameterControls" cssStyle="width:58px;" cssClass="cssbutton1"/>
  </c:set>
<table class="table" style="width:99%;margin-top:2px;" border="1" cellpadding="0" cellspacing="0">
<thead>
<tr>
<th>Parameter</th>
<th colspan="7">Custom Type</th>
</tr></thead>
<tbody>
		<tr>			
			<td>
			    <s:textfield name="parameterControl.parameter" size="35" required="true" cssStyle="width:231px;" cssClass="input-text"/>
			</td>
			<td width="35" colspan="10">
			    <s:select name="parameterControl.customType"  cssStyle="width:182px;" required="true" cssClass="list-menu" list="{'PAYROLL','VANLINE'}" headerKey="" headerValue=""  />
			</td>
	    </tr>				
		<tr>
		<td class="listwhitetext" colspan="7">Active<s:checkbox name="parameterControl.active" cssStyle="vertical-align:sub;" value="${parameterControl.active}"/>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TSFT Flag <s:checkbox name="parameterControl.tsftFlag" cssStyle="vertical-align:sub;" value="${parameterControl.tsftFlag}" />
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hybrid Flag <s:checkbox name="parameterControl.hybridFlag" cssStyle="vertical-align:sub;" value="${parameterControl.hybridFlag}"/>
	</td>
		
		<td>		
			<c:out value="${searchbuttons}" escapeXml="false" />
		    <c:out value="${getAllButton}" escapeXml="false"/></td>
			</tr>
		</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div> 
</div>
</div>
   <s:set name="parameterControls" value="parameterControls" scope="session"/>
   <div id="otabs">
	<ul>
		<li><a class="current"><span>Control List</span></a></li>
	</ul>
</div>
<div class="spnblk">&nbsp;</div>
      <display:table name="parameterControls" id="parameterControls" defaultsort="1" requestURI="" pagesize="20" class="table"  export="true" style="!margin-top:-3px;">
      <display:column property="parameter" sortable="true" href="editParameterControl.html" paramId="id" paramProperty="id" title="Parameter"/>
      <display:column property="comments" title="Comments"/>
      <display:column property="fieldLength" headerClass="containeralign" style="text-align:right;width:80px;" title="Field Length"/>
      <display:column  title="Active"  style="text-align: center;width:70px;" >
	    <c:if test="${parameterControls.active=='true'}">
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/tick01.gif" />
		</c:if>
		<c:if test="${parameterControls.active=='false'}">
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/cancel001.gif"/>
		</c:if>
	</display:column> 
	<display:column  title="TSFT Flag"  style="text-align: center;width:70px;" >
	    <c:if test="${parameterControls.tsftFlag=='true'}">
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/tick01.gif" />
		</c:if>
		<c:if test="${parameterControls.tsftFlag=='false'}">
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/cancel001.gif"/>
		</c:if>
	</display:column> 
	<display:column  title="Hybrid Flag"  style="text-align: center;width:70px;" >
	    <c:if test="${parameterControls.hybridFlag=='true'}">
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/tick01.gif" />
		</c:if>
		<c:if test="${parameterControls.hybridFlag=='false'}">
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/cancel001.gif"/>
		</c:if>
	</display:column> 
	 <display:column property="customType" style="width:180px;" title="Custom Type"/>
      <display:column title="Remove" style="text-align: center;width:50px;"><img src="${pageContext.request.contextPath}/images/recycle.gif" onclick="confirmDelete(${parameterControls.id});"/></display:column>
   </display:table>
<c:out value="${buttons}" escapeXml="false"/>
</s:form>