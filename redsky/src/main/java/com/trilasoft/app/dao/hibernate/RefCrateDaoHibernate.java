package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.RefCrateDao;
//import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.RefCrate;

public class RefCrateDaoHibernate extends GenericDaoHibernate<RefCrate, Long> implements RefCrateDao {

	private List refCrates;

	public RefCrateDaoHibernate() {
		super(RefCrate.class);
	}

	public List<RefCrate> findByLastName(String lastName) {
		return getHibernateTemplate().find("from RefCrate where lastName=?", lastName);
	}

	public List<RefCrate> findByBillingCountryCode(String billingCountryCode) {
		return getHibernateTemplate().find("from RefCrate where billingCountryCode=?", billingCountryCode);
	}

	public List<RefCrate> findForRefCrate(String lastName, String partnerCode, String billingCountryCode) {
		if (lastName != "" && partnerCode != "" && billingCountryCode != "") {
			refCrates = getHibernateTemplate().find("from Partner where lastName='" + lastName + "' AND partnerCode='" + partnerCode + "' AND billingCountryCode=?",
					billingCountryCode);
		}
		if (lastName != "" && partnerCode != "" && billingCountryCode.equals("")) {
			refCrates = getHibernateTemplate().find("from Partner where lastName='" + lastName + "' AND partnerCode=?", partnerCode);
		}
		if (lastName != "" && partnerCode.equals("") && billingCountryCode != "") {
			refCrates = getHibernateTemplate().find("from Partner where lastName='" + lastName + "' AND billingCountryCode=?", billingCountryCode);
		}
		if (lastName.equals("") && partnerCode != "" && billingCountryCode != "") {
			refCrates = getHibernateTemplate().find("from Partner where partnerCode='" + partnerCode + "' AND billingCountryCode=?", billingCountryCode);
		}

		if (lastName.equals("") && partnerCode != "" && billingCountryCode.equals("")) {
			refCrates = getHibernateTemplate().find("from Partner where partnerCode=?", partnerCode);
		}
		if (lastName.equals("") && partnerCode.equals("") && billingCountryCode != "") {
			refCrates = getHibernateTemplate().find("from Partner where billingCountryCode=?", billingCountryCode);
		}
		if (lastName != "" && partnerCode.equals("") && billingCountryCode.equals("")) {
			refCrates = getHibernateTemplate().find("from Partner where lastName=?", lastName);
		}
		return refCrates;
	}

	public List refCrateList(String corpID) {
		
		return getHibernateTemplate().find("from RefCrate where corpID = ?",corpID);
	}

}
