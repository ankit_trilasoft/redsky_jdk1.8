package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.DsVisaImmigrationDao;
import com.trilasoft.app.model.DsVisaImmigration;
import com.trilasoft.app.service.DsVisaImmigrationManager;

public class DsVisaImmigrationManagerImpl extends GenericManagerImpl<DsVisaImmigration, Long>  implements DsVisaImmigrationManager {
	
	DsVisaImmigrationDao dsVisaImmigrationDao;
	
	public DsVisaImmigrationManagerImpl(DsVisaImmigrationDao dsVisaImmigrationDao) {
		super(dsVisaImmigrationDao); 
        this.dsVisaImmigrationDao = dsVisaImmigrationDao; 
	}

	public List getListById(Long id) {
		return dsVisaImmigrationDao.getListById(id);
	}
}
