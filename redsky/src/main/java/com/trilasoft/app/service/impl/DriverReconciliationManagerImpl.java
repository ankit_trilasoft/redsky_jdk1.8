package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.AccessInfoDao;
import com.trilasoft.app.dao.DriverReconciliationDao;
import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.VanLine;
import com.trilasoft.app.service.AccessInfoManager;
import com.trilasoft.app.service.DriverReconciliationManager;

public class DriverReconciliationManagerImpl extends GenericManagerImpl<VanLine, Long> implements DriverReconciliationManager {
	DriverReconciliationDao driverReconciliationDao;
	public DriverReconciliationManagerImpl(DriverReconciliationDao driverReconciliationDao) {
		super(driverReconciliationDao);
		this.driverReconciliationDao =driverReconciliationDao;
	}
	public List getVanLineCodeCompDiv(String sessionCorpID){
	return 	driverReconciliationDao.getVanLineCodeCompDiv(sessionCorpID);
	}
	public List getWeekendingList(String agent,String ownerCode, String corpId){
		return 	driverReconciliationDao.getWeekendingList(agent,ownerCode, corpId);
	}
	public List getDriverPreview(String agent,String ownerCode,Date weekending,String sessionCorpID){
		return 	driverReconciliationDao.getDriverPreview(agent,ownerCode,weekending, sessionCorpID);
	}
	public List getDriverAccPreview(String vanRegNum ,String agent,String ownerCode,Date weekending,String sessionCorpID){
		return 	driverReconciliationDao.getDriverAccPreview(vanRegNum , agent,ownerCode, weekending, sessionCorpID);
	}
	public List findPartnerDetailsForAutoComplete(String partnerCodeAutoCopmlete,String sessionCorpID){
		return 	driverReconciliationDao.findPartnerDetailsForAutoComplete(partnerCodeAutoCopmlete,sessionCorpID);
	}
}
