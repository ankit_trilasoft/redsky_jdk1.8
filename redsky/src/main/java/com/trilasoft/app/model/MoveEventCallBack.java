package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "moveeventcallback")
public class MoveEventCallBack {
	private Long id;
	private String EventId;
	private Date TimestampUtc;
	private String PostId;
	private String MoveId;
	private String TaskId;
	private String DocumentId;
	private String DocumentType;
	private String PieceId;
	private String UserEmail;
	private String Event;
	private String ExternalReference;
	private String DossierNumber;
	private String EventMessage;
	private String EventTimeZone;
	private String createdBy; 
	private Date createdOn;
	private String updatedBy; 
	private Date updatedOn;  
	private String corpID;
	private String taskRedSkyStatus; 
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id",id)
				.append("EventId",EventId)
				.append("TimestampUtc", TimestampUtc).append("PostId", PostId)
				.append("MoveId", MoveId).append("TaskId", TaskId)
				.append("DocumentId", DocumentId)
				.append("DocumentType", DocumentType)
				.append("PieceId",PieceId)
				.append("UserEmail", UserEmail)
				.append("Event", Event)
				.append("ExternalReference", ExternalReference)
				.append("DossierNumber",DossierNumber)
				.append("EventMessage",EventMessage)
				.append("EventTimeZone",EventTimeZone)
				.append("createdBy",createdBy).append("createdOn",createdOn)
				.append("updatedBy",updatedBy).append("updatedOn",updatedOn)
				.append("corpID",corpID).append("taskRedSkyStatus",taskRedSkyStatus)
				.toString();
						
	}
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof MoveEventCallBack))
			return false;
		MoveEventCallBack castOther = (MoveEventCallBack) other;
		return new EqualsBuilder().append(id, castOther.id).append(EventId,castOther.EventId).append(TimestampUtc, castOther.TimestampUtc).append(PostId, castOther.PostId)
				.append(MoveId,castOther.MoveId).append(TaskId, castOther.TaskId).append(DocumentId, castOther.DocumentId).append(DocumentType,castOther.DocumentType)
				.append(PieceId,castOther.PieceId).append(UserEmail,castOther.UserEmail).append(Event,castOther.Event)
				.append(ExternalReference,castOther.ExternalReference).append(DossierNumber,castOther.DossierNumber)
				.append(EventMessage, castOther.EventMessage).append(EventTimeZone, castOther.EventTimeZone)
				.append(createdBy,castOther.createdBy).append(createdOn,castOther.createdOn)
				.append(updatedBy, castOther.updatedBy).append(updatedOn, castOther.updatedOn)
				.append(corpID, castOther.corpID).append(taskRedSkyStatus, castOther.taskRedSkyStatus).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(EventId).append(TimestampUtc).append(PostId).append(MoveId).append(TaskId)
				.append(DocumentId).append(DocumentType).append(PieceId).append(UserEmail).append(Event).append(ExternalReference)
				.append(DossierNumber).append(EventMessage).append(EventTimeZone).append(createdBy).append(createdOn).append(updatedBy)
				.append(updatedOn).append(corpID).append(taskRedSkyStatus).toHashCode();
	}
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the eventId
	 */
	@Column
	public String getEventId() {
		return EventId;
	}
	/**
	 * @param eventId the eventId to set
	 */
	public void setEventId(String eventId) {
		EventId = eventId;
	}
	/**
	 * @return the timestampUtc
	 */
	@Column
	public Date getTimestampUtc() {
		return TimestampUtc;
	}
	/**
	 * @param timestampUtc the timestampUtc to set
	 */
	public void setTimestampUtc(Date timestampUtc) {
		TimestampUtc = timestampUtc;
	}
	/**
	 * @return the postId
	 */
	@Column
	public String getPostId() {
		return PostId;
	}
	/**
	 * @param postId the postId to set
	 */
	public void setPostId(String postId) {
		PostId = postId;
	}
	/**
	 * @return the moveId
	 */
	@Column
	public String getMoveId() {
		return MoveId;
	}
	/**
	 * @param moveId the moveId to set
	 */
	public void setMoveId(String moveId) {
		MoveId = moveId;
	}
	/**
	 * @return the taskId
	 */
	@Column
	public String getTaskId() {
		return TaskId;
	}
	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(String taskId) {
		TaskId = taskId;
	}
	/**
	 * @return the documentId
	 */
	@Column
	public String getDocumentId() {
		return DocumentId;
	}
	/**
	 * @param documentId the documentId to set
	 */
	public void setDocumentId(String documentId) {
		DocumentId = documentId;
	}
	/**
	 * @return the documentType
	 */
	@Column
	public String getDocumentType() {
		return DocumentType;
	}
	/**
	 * @param documentType the documentType to set
	 */
	public void setDocumentType(String documentType) {
		DocumentType = documentType;
	}
	/**
	 * @return the pieceId
	 */
	@Column
	public String getPieceId() {
		return PieceId;
	}
	/**
	 * @param pieceId the pieceId to set
	 */
	public void setPieceId(String pieceId) {
		PieceId = pieceId;
	}
	/**
	 * @return the userEmail
	 */
	@Column
	public String getUserEmail() {
		return UserEmail;
	}
	/**
	 * @param userEmail the userEmail to set
	 */
	public void setUserEmail(String userEmail) {
		UserEmail = userEmail;
	}
	/**
	 * @return the event
	 */
	@Column
	public String getEvent() {
		return Event;
	}
	/**
	 * @param event the event to set
	 */
	public void setEvent(String event) {
		Event = event;
	}
	/**
	 * @return the externalReference
	 */
	@Column
	public String getExternalReference() {
		return ExternalReference;
	}
	/**
	 * @param externalReference the externalReference to set
	 */
	public void setExternalReference(String externalReference) {
		ExternalReference = externalReference;
	}
	/**
	 * @return the dossierNumber
	 */
	@Column
	public String getDossierNumber() {
		return DossierNumber;
	}
	/**
	 * @param dossierNumber the dossierNumber to set
	 */
	public void setDossierNumber(String dossierNumber) {
		DossierNumber = dossierNumber;
	}
	/**
	 * @return the eventMessage
	 */
	@Column
	public String getEventMessage() {
		return EventMessage;
	}
	/**
	 * @param eventMessage the eventMessage to set
	 */
	public void setEventMessage(String eventMessage) {
		EventMessage = eventMessage;
	}
	/**
	 * @return the eventTimeZone
	 */
	@Column
	public String getEventTimeZone() {
		return EventTimeZone;
	}
	/**
	 * @param eventTimeZone the eventTimeZone to set
	 */
	public void setEventTimeZone(String eventTimeZone) {
		EventTimeZone = eventTimeZone;
	}
	
	/**
	 * @return the createdBy
	 */
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the updatedBy
	 */
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the updatedOn
	 */
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the corpID
	 */
	@Column
	public String getCorpID() {
		return corpID;
	}

	/**
	 * @param corpID the corpID to set
	 */
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	/**
	 * @return the taskRedSkyStatus
	 */
	@Column
	public String getTaskRedSkyStatus() {
		return taskRedSkyStatus;
	}

	/**
	 * @param taskRedSkyStatus the taskRedSkyStatus to set
	 */
	public void setTaskRedSkyStatus(String taskRedSkyStatus) {
		this.taskRedSkyStatus = taskRedSkyStatus;
	}
}
