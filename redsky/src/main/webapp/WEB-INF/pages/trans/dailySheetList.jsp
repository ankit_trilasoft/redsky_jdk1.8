<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Daily Sheet</title>   
    <meta name="heading" content="Daily Sheet"/> 
    <script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>  
   <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:0px;
!margin-bottom:2px;
margin-top:-18px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:100%;
!width:98%;
}

div.error, span.error, li.error, div.message {

width:450px;
margin-top:0px; 
}
form {
margin-top:-40px;
!margin-top:-5px;
}

div#main {
margin:-5px 0 0;
}
.table td, .table th, .tableHeaderTable td {   
    padding: 0.4em;
    font-size:11px;
}

.table thead th, .tableHeaderTable td {   
    font-size: 11px;   
    height: 20px;
    padding: 1px 3px 1px 5px;  
}

.table-DS {
    background: none repeat scroll 0 0 #FFE680;
    border: 2px solid #74B3DC;
    border-collapse: collapse;
    color: #3333FF;
    width: 100%;
    font-size:1.1em;
    margin-bottom:5px;
}
.table-DS thead th, .tableHeaderTable td {
  	background: repeat-x scroll 0 0 #BCD2EF; 
    border-color: #3DAFCB;
    border-style: solid;
    border-width: 1px;
    color: #15428B;
    font-family: arial,verdana;
    font-size: 11px;
    font-weight: bold;
    height: 20px;
    padding: 1px 3px 1px 5px;
    text-decoration: none;
    text-align: left;
}
.table-DS th.sorted a, .table-DS th.sortable a {
    background-position: right center;
    display: block;
    color:#FFFFFF !important;
}

.table-DS tr.odd {
    background: none repeat scroll 0 0 #eefafd;
    border: 1px dotted #A6A6A6;
    color: #000000;
}

.table-DS tr.even {
    background-color: #ffffff;
    border: 1px dotted #A6A6A6;
    color: #000000;
}

table-DS tbody th, .table tbody td {
    text-align: left;
}
.table-DS td, .table th, .tableHeaderTable td {
    border: 1px dotted #A6A6A6;
    padding: 0.5em;
}

.table-DS tbody td a {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
    text-decoration: underline;
    cursor:pointer;
}

.table-DS th.sorted a, th.sortable a {
    background-position: right;
    display: block;
}

.table-DS th.order1 a {
    background-image: url(../images/arrow_down-fc.png) !important;
    background-repeat:no-repeat;
    padding-right:8px;
}

.table-DS th.order2 a {
    background-image: url(../images/arrow_up-fc.png) !important;
    background-repeat:no-repeat;
    padding-right:8px;
}

.table-DS th.sorted {
    background-color: #BCD2EF;
    color: #ffffff;
}

.listwhitetextBlue {
    color: #3333FF;
    font-family: arial,verdana;
    font-size: 10.5px;
    font-weight: normal;
    text-decoration: none;  
} 
.listwhitetextBlueLink {
    color: #3333FF;
    font-family: arial,verdana;
    font-size: 10.5px;
    font-weight: normal;
    text-decoration: none;
    cursor:pointer;
}
.listwhitetextWhite {
    background-color: #FFFFFF;
    color: #003366;
    font-family: arial,verdana;
    font-size: 10.5px;
   }
.listwhitetextCrew {
	font-family: arial,verdana;
    font-size: 10.5px;
}

div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
<style type="text/css">
#Style {
	position:absolute; 
	visibility:hidden;
	border:solid 1px #CCC;
	padding:5px;
	
}
</style>
<script language="javascript" type="text/javascript">
function selectSearchField(){		
	var wareHouse=document.forms['dailySheetForm'].elements['wRHouse'].value; 
	var date= document.forms['dailySheetForm'].elements['dailySheetDate'].value;
		
	if(wareHouse==''){
		alert('Please select Warehouse to continue.....!');	
		return false;
	}else if(date==''){
		alert('Please select Date to continue.....!');	
		return false;
	}else{
		return true;
	}
}
function goToSearch(){
		var selectedSearch= selectSearchField();
		if(selectedSearch){	
			 document.forms['dailySheetForm'].action = 'dailySheetSearch.html';
			 document.forms['dailySheetForm'].submit();					
		}
	}

function checkOriginFieldRequired(){
	var oriCountry = document.forms['dailySheetForm'].elements['wOriginCountry'].value;
  	var oriCity = document.forms['dailySheetForm'].elements['wOCity'].value;
  	var oriZip = document.forms['dailySheetForm'].elements['wOZip'].value;
  	var OriState = document.forms['dailySheetForm'].elements['wOState'].value
  	var destCountry = document.forms['dailySheetForm'].elements['wDestinCountry'].value;
	var destState = document.forms['dailySheetForm'].elements['wDState'].value;
  	var destCity = document.forms['dailySheetForm'].elements['wDCity'].value;
  	if(oriCountry=='United States'){
  	  	if(OriState==''){
  	  	  	alert('Origin State is a required field.');
  	  		return false;
  	  	}else if(oriCity==''){
  	  		alert('Origin City is a required field.');
  	  		return false;
  	  	}else if(oriZip==''){
  	  		alert('Origin Postal Code is a required field.');
  	  		return false;
  	  	}
  	  	}else{
  	  	  	if(oriCountry==''){
  	  	  		alert('Origin Country is a required field.');
  	  	  		return false;
  	  	  	}else if(oriCity==''){
  	  	  		alert('Origin City is a required field.');
  	  	  		return false;
  	  	  	}
  	  	}
  	if(destCountry=='United States'){
  	  	if(destState==''){
  	  		alert('Destination State is a required field.');
	  		return false;
	  	}else if(destCity==''){
	  		alert('Destination City is a required field.');
	  		return false;
  	  	  }
  	  }else{
  			if(destCountry==''){
	  	  		alert('Destination Country is a required field.');
	  	  		return false;
	  	  	}else if(destCity==''){
	  	  		alert('Destination City is a required field.');
	  	  		return false;
	  	  	}
  	  	 }
  	return true;
}

function updateWTOriAdd(){
	var ok = checkOriginFieldRequired();
	if(ok){
		 document.forms['dailySheetForm'].action = 'updateWTAddressDetails.html';
		 document.forms['dailySheetForm'].submit();
	}else{
		return false;
		}		
	}
function hideFromAddress(){		
	document.getElementById('workTicketFromAdd').style.display="none";	
}
function updateOriginAddress(oId){
	var url="findWorkTicketAddDetailAjax.html?ajax=1&decorator=simple&popup=true&oId=" + encodeURI(oId); 
    httpState.open("GET", url, true);
    httpState.onreadystatechange = function(){ handleHttpResponse52(oId)};
    httpState.send(null);
}

function handleHttpResponse52(oId){
    if (httpState.readyState == 4){
       var results = httpState.responseText
       results = results.trim();
       results = results.replace('[','');
       results=results.replace(']',''); 
       res = results.split("`");      
       document.forms['dailySheetForm'].elements['wOAddress1'].value=res[0];
	  	document.forms['dailySheetForm'].elements['wOAddress2'].value=res[1];
	  	document.forms['dailySheetForm'].elements['wOAddress3'].value=res[2];	  	
	  	document.forms['dailySheetForm'].elements['wOriginCountry'].value=res[3];
	  	document.forms['dailySheetForm'].elements['wOCity'].value=res[5];
	  	document.forms['dailySheetForm'].elements['wOZip'].value=res[6];	  	
	  	document.forms['dailySheetForm'].elements['wOId'].value=oId;	  	
	  	getState1(res[3],res[4]);
	  	ostateMandatory();
	  	zipCode();
	  	enableStateListOrigin();
	  	document.forms['dailySheetForm'].elements['wDAddress1'].value=res[7];
	  	document.forms['dailySheetForm'].elements['wDAddress2'].value=res[8];
	  	document.forms['dailySheetForm'].elements['wDAddress3'].value=res[9];	  	
	  	document.forms['dailySheetForm'].elements['wDestinCountry'].value=res[10];
	  	document.forms['dailySheetForm'].elements['wDCity'].value=res[12];
	  	document.forms['dailySheetForm'].elements['wDZip'].value=res[13];
	  	document.forms['dailySheetForm'].elements['wActWt'].value=res[14];	  
	  	dstateMandatory();
	  	getState2(res[10],res[11]);
	  	enableStateListDest();
	  	document.getElementById('workTicketFromAdd').style.display="block";
    }
}
</script>
<script language="javascript" type="text/javascript">
function getState(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode); 
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse5;
     httpState.send(null);
}
function handleHttpResponse5(){
    if (httpState.readyState == 4){
       var results = httpState.responseText
       results = results.trim();
       res = results.split("@");
       targetElement = document.forms['dailySheetForm'].elements['wOState'];
		targetElement.length = res.length;
		for(i=0;i<res.length;i++)
			{
			if(res[i] == ''){
			document.forms['dailySheetForm'].elements['wOState'].options[i].text = '';
			document.forms['dailySheetForm'].elements['wOState'].options[i].value = '';
			}else{
			stateVal = res[i].split("#");
			document.forms['dailySheetForm'].elements['wOState'].options[i].text = stateVal[1];
			document.forms['dailySheetForm'].elements['wOState'].options[i].value = stateVal[0]; 			
			}
			document.getElementById("ostate").value = '';
			}			
    }
}

function getState1(targetElement,temp) {
	var country = targetElement;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode); 
     httpState.open("GET", url, true);
     httpState.onreadystatechange = function(){ handleHttpResponse51(temp)}; 
     httpState.send(null);
}
function handleHttpResponse51(temp){
    if (httpState.readyState == 4){
       var results = httpState.responseText
       results = results.trim();
       res = results.split("@");
       targetElement = document.forms['dailySheetForm'].elements['wOState'];
		targetElement.length = res.length;
		for(i=0;i<res.length;i++)
			{
			if(res[i] == ''){
			document.forms['dailySheetForm'].elements['wOState'].options[i].text = '';
			document.forms['dailySheetForm'].elements['wOState'].options[i].value = '';
			}else{
			stateVal = res[i].split("#");
			document.forms['dailySheetForm'].elements['wOState'].options[i].text = stateVal[1];
			document.forms['dailySheetForm'].elements['wOState'].options[i].value = stateVal[0]; 			
			}
			}
		document.forms['dailySheetForm'].elements['wOState'].value=temp;
    }
} 

var httpState = getHTTPObjectState()
  function getHTTPObjectState()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 


function getState2(targetElement,temp) {
	var country = targetElement;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode); 
     httpState1.open("GET", url, true);
     httpState1.onreadystatechange = function(){ handleHttpResponse54(temp)}; 
     httpState1.send(null);
}
function handleHttpResponse54(temp){
    if (httpState1.readyState == 4){
       var results = httpState1.responseText
       results = results.trim();
       res = results.split("@");
       targetElement = document.forms['dailySheetForm'].elements['wDState'];
		targetElement.length = res.length;
		for(i=0;i<res.length;i++)
			{
			if(res[i] == ''){
			document.forms['dailySheetForm'].elements['wDState'].options[i].text = '';
			document.forms['dailySheetForm'].elements['wDState'].options[i].value = '';
			}else{
			stateVal = res[i].split("#");
			document.forms['dailySheetForm'].elements['wDState'].options[i].text = stateVal[1];
			document.forms['dailySheetForm'].elements['wDState'].options[i].value = stateVal[0]; 			
			}
			}
		document.forms['dailySheetForm'].elements['wDState'].value=temp;
    }
} 
function enableStateListOrigin(){ 
	var oriCon=document.forms['dailySheetForm'].elements['wOriginCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(oriCon)> -1);
	  if(index != ''){
	  		document.forms['dailySheetForm'].elements['wOState'].disabled = false;
	  }else{
		  document.forms['dailySheetForm'].elements['wOState'].disabled = true;
	  }	        
}

function enableStateListDest(){ 
	var destCon=document.forms['dailySheetForm'].elements['wDestinCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(destCon)> -1);
	  if(index != ''){
	  		document.forms['dailySheetForm'].elements['wDState'].disabled = false;
	  }else{
		  document.forms['dailySheetForm'].elements['wDState'].disabled = true;
	  }	        
}

var httpState1 = getHTTPObjectState()
function getStateDestin(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode); 
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse6;
     httpState.send(null);
}
function handleHttpResponse6(){
    if (httpState.readyState == 4){
       var results = httpState.responseText
       results = results.trim();
       res = results.split("@");
       targetElement = document.forms['dailySheetForm'].elements['wDState'];
		targetElement.length = res.length;
		for(i=0;i<res.length;i++)
			{
			if(res[i] == ''){
			document.forms['dailySheetForm'].elements['wDState'].options[i].text = '';
			document.forms['dailySheetForm'].elements['wDState'].options[i].value = '';
			}else{
			stateVal = res[i].split("#");
			document.forms['dailySheetForm'].elements['wDState'].options[i].text = stateVal[1];
			document.forms['dailySheetForm'].elements['wDState'].options[i].value = stateVal[0]; 			
			}
			document.getElementById("dstate").value = '';
			}			
    }
}

function ostateMandatory(){
    var originCountry=document.forms['dailySheetForm'].elements['wOriginCountry'].value;
    var reqTrue=document.getElementById("originStateRequiredTrue");
    var reqFalse=document.getElementById("originStateRequiredFalse");
    if(originCountry=='United States'){
    	reqTrue.style.display='block';
    	reqFalse.style.display='none';
    }
    else{
    	reqFalse.style.display='block';
    	reqTrue.style.display='none';
    }
}

function dstateMandatory(){
    var destinationCountry=document.forms['dailySheetForm'].elements['wDestinCountry'].value;
    var reqTrue=document.getElementById("destinationStateRequiredTrue");
    var reqFalse=document.getElementById("destinationStateRequiredFalse");
    if(destinationCountry=='United States'){
    	reqTrue.style.display='block';
    	reqFalse.style.display='none';
    }
    else{
    	reqFalse.style.display='block';
    	reqTrue.style.display='none';
    }
}
function zipCode(){
    var originCountryCode=document.forms['dailySheetForm'].elements['wOriginCountry'].value; 
	var el = document.getElementById('zipCodeRequiredTrue');
	var el1 = document.getElementById('zipCodeRequiredFalse');
	if(originCountryCode == 'United States'){
	el.style.display = 'block';		
	el1.style.display = 'none';		
	}else{
	el.style.display = 'none';
	el1.style.display = 'block';
	}
}

function updateDailySheetNote(){	
	 document.forms['dailySheetForm'].action = 'saveDailySheetNotes.html';
	 document.forms['dailySheetForm'].submit();
}

function findCrewDetails(wareHouse,wTicket,dSSNumber,dSCName,job,service,callingVal){
	var date1 = document.forms['dailySheetForm'].elements['dailySheetDate'].value;
	document.forms['dailySheetForm'].elements['dSTicket'].value=wTicket;
	document.forms['dailySheetForm'].elements['dSSNumber'].value=dSSNumber;
	dSCName = dSCName.replace('<BR>','');
	document.forms['dailySheetForm'].elements['dSCName'].value=dSCName;
	document.forms['dailySheetForm'].elements['dSWHouse'].value=wareHouse;	
	document.forms['dailySheetForm'].elements['dSJob'].value=job;
	document.forms['dailySheetForm'].elements['dSService'].value=service;	
	var url="findAssignedCrewsForEditingAjax.html?ajax=1&decorator=simple&popup=true&wRHouse="+wareHouse+"&dailySheetDate="+date1+"&wTicket=" + encodeURI(wTicket); 
    httpCrew.open("GET", url, true);
    httpCrew.onreadystatechange = function(){ handleHttpResponseCrew(wareHouse,wTicket,dSSNumber,dSCName,job,service,callingVal)};
    httpCrew.send(null);
  }
function handleHttpResponseCrew(wareHouse,wTicket,dSSNumber,dSCName,job,service,callingVal){
    if (httpCrew.readyState == 4){
    	 var results = httpCrew.responseText
    	 results = results.trim();
    	 results = results.replace('[','');
         results=results.replace(']',''); 
         res = results.split(","); 
    	 document.getElementById('assignedCrews').innerHTML = res;
    	 document.getElementById('crewDetails').style.display="block";   	
   	 if(callingVal++<2){
   		 findTruckDetails(wareHouse,wTicket,dSSNumber,dSCName,job,service,callingVal);
   		 findAvailableCrewDetails(wareHouse,wTicket)
       }
    }    
}
var httpCrew = getHTTPObjectState();

function findAvailableCrewDetails(wareHouse,wTicket){
	var date1 = document.forms['dailySheetForm'].elements['dailySheetDate'].value;	
	var url="findAvailableCrewAjax.html?ajax=1&decorator=simple&popup=true&wRHouse="+wareHouse+"&dailySheetDate="+date1+"&wTicket=" + encodeURI(wTicket); 
	httpAvailCrew.open("GET", url, true);
    httpAvailCrew.onreadystatechange = handleHttpResponseAvailCrew;
    httpAvailCrew.send(null);
  }
function handleHttpResponseAvailCrew(){
    if (httpAvailCrew.readyState == 4){
    	 var results = httpAvailCrew.responseText
    	 results = results.trim();
    	 results = results.replace('[','');
         results=results.replace(']',''); 
         res = results.split(","); 
    	 document.getElementById('availableCrews').innerHTML = res;   	 
    }    
}
var httpAvailCrew = getHTTPObjectState();

function hideCrewDetails(){
	document.getElementById('crewDetails').style.display="none";
}

function findTruckDetails(wareHouse,wTicket,dSSNumber,dSCName,job,service,callingVal){
	var date1 = document.forms['dailySheetForm'].elements['dailySheetDate'].value;
	var url="findAssignedTruckForEditingAjax.html?ajax=1&decorator=simple&popup=true&wRHouse="+wareHouse+"&dailySheetDate="+date1+"&wTicket=" + encodeURI(wTicket); 
    httpTruck.open("GET", url, true);
    httpTruck.onreadystatechange = function(){handleHttpResponseTruck(wareHouse,wTicket,dSSNumber,dSCName,job,service,callingVal)};
    httpTruck.send(null);
}
function handleHttpResponseTruck(wareHouse,wTicket,dSSNumber,dSCName,job,service,callingVal){
    if (httpTruck.readyState == 4 ){
    	 var results = httpTruck.responseText
    	 results = results.trim();
    	 results = results.replace('[','');
         results=results.replace(']',''); 
         res = results.split(","); 
    	 document.getElementById('assignedTrucks').innerHTML = res;
    	 document.getElementById('crewDetails').style.display="block";   
   	    if(callingVal++<2){	
   	        findCrewDetails(wareHouse,wTicket,dSSNumber,dSCName,job,service,callingVal);
   	        findAvailableTruckDetails(wareHouse,wTicket);
   	    }	
    }

}
var httpTruck = getHTTPObjectState();

function findAvailableTruckDetails(wareHouse,wTicket){
	var date1 = document.forms['dailySheetForm'].elements['dailySheetDate'].value;
	var url="findAvailableTruckAjax.html?ajax=1&decorator=simple&popup=true&wRHouse="+wareHouse+"&dailySheetDate="+date1+"&wTicket=" + encodeURI(wTicket); 
    httpAvailTruck.open("GET", url, true);
    httpAvailTruck.onreadystatechange = handleHttpResponseAvailTruck;
    httpAvailTruck.send(null);
}
function handleHttpResponseAvailTruck(){
    if (httpAvailTruck.readyState == 4){
    	 var results = httpAvailTruck.responseText
    	 results = results.trim();
    	 results = results.replace('[','');
         results=results.replace(']',''); 
         res = results.split(","); 
    	 document.getElementById('availableTrucksEdit').innerHTML = res;    		
    }   
  } 
var httpAvailTruck = getHTTPObjectState();

function updateCrewDetails(){
	selectAll('assignedCrews');
	selectAll('absentCrews');
	selectAll('assignedTrucks');
	document.forms['dailySheetForm'].action = 'saveCrewDetails.html';
	document.forms['dailySheetForm'].submit();
}

</script>
</head>

<s:form id="dailySheetForm" action="" method="post" onsubmit="" enctype="multipart/form-data"> 
<s:hidden name="dSTicket" value=""/>
<s:hidden name="dSSNumber" value=""/>
<s:hidden name="dSCName" value=""/>
<s:hidden name="dSWHouse" value=""/>
<s:hidden name="dSJob" value=""/>
<s:hidden name="dSService" value=""/>

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div style="padding-bottom:23px"></div>
		<div class="spnblk">&nbsp;</div>
	
<div id="crewDetails">		
<table style="width:100%;margin:0px;margin-bottom:5px">
<tr>
<td class="colored_bg">
<fieldset>
			            <legend>Reassign Crew</legend>
			            <table class="pickList11" style="margin:0px;margin-left:2%">
			                <tr>
			                    <th class="pickLabel">
			                        <label class="required">Available Crew</label>
			                    </th>
			                    <td></td>
			                    <th class="pickLabel">
			                        <label class="required">Assigned Crew</label>
			                    </th>
			                    <td></td>
			                    <th class="pickLabel">
			                        <label class="required">Absent Crew</label>
			                    </th>			                    
			                </tr>			                
			                 <c:set var="rightList" value="${absentCrewsEdit}" scope="request"/>			                 
			                <c:import url="/WEB-INF/pages/dailySheetCrewPickList.jsp">
			            		<c:param name="listCount" value="1"/>
			                    <c:param name="leftId" value="availableCrews"/>
			                    <c:param name="middleId" value="assignedCrews"/>
			                    <c:param name="rightId" value="absentCrews"/>
			                </c:import>
			               
			            </table>			             
			        </fieldset>
			        </td>
</tr>

				<tr>			 		  	       
			 	 <td class="colored_bg"  >       	 
			           <fieldset >
			            <legend>Reassign Truck</legend>
			                <table class="pickList11" style="margin:0px;margin-left:2%">
			                   <tr>
			                     <th class="pickLabel">
			                        <label class="required">Available Truck</label>
			                    </th>
			                    <td></td>
			                    <th class="pickLabel">
			                        <label class="required">Assigned Truck</label>
			                     </th>
			                  </tr>			                
			                <c:import url="/WEB-INF/pages/dailySheetTruckPickList.jsp">
			                    <c:param name="listCount" value="1"/>
			                    <c:param name="leftId" value="availableTrucksEdit"/>
			                    <c:param name="rightId" value="assignedTrucks"/>
			                </c:import>
			                 <tr>
			                <td align="center" colspan="4">
			                <input type="button" class="cssbutton" style="width:55px;margin-left:42%;margin-top:5px;"  value="Save" onclick="updateCrewDetails();"/> 
							<input type="button" class="cssbutton" style="width:55px;" value="Cancel" onclick="return goToSearch();hideCrewDetails();"/> 
						</td>
						</tr>
			                </table>
			                </fieldset>
			                   </td>
			 		  	</tr>

</table>
		</div>
<div id="workTicketFromAdd" style="margin: 0px; vertical-align: top; margin-bottom:5px">
<table class="table" border="0" cellpadding="0" cellspacing="0" style="margin-bottom:20px;margin:0px;">
<tr>
<td style="padding:0px;">
<table cellspacing="0" cellpadding="0" border="0" style="margin:0px;">
<tr>
<td class="subcontenttabChild" style="height:15px;text-align:center;" align="center" colspan="6"><b>From Address Details</b></td>
</tr>
<s:hidden name="wOId" />
<tr>
<td style="text-align:right;" class="listwhitetext">Address1</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="wOAddress1" size="25" maxlength="50" tabindex="" /></td>
<td style="text-align:right;" class="listwhitetext">Address2</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="wOAddress2" size="25" maxlength="50" tabindex="" /></td>
<td style="text-align:right;" class="listwhitetext">Address3</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="wOAddress3" size="23" maxlength="50" tabindex="" /></td>
</tr>
<tr>
<td style="text-align:right;" class="listwhitetext"><fmt:message key='customerFile.originCountry'/><font color="red" size="2">*</font></td>
	<td><s:select cssClass="list-menu" name="wOriginCountry" id="ocountry" list="%{country}" cssStyle="width:147px" onchange="getState(this);ostateMandatory();zipCode();enableStateListOrigin();"  headerKey="" headerValue="" tabindex="" /></td>
	
	<td style="text-align:right;" id="originStateRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originState' /></td>
	<td style="text-align:right;" id="originStateRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.originState' /><font color="red" size="2">*</font></td>
	<td><s:select cssClass="list-menu" name="wOState" id="ostate" list="%{ostates}" cssStyle="width:147px" onchange="" headerKey="" headerValue="" tabindex="" /></td>
	
	<td style="text-align:right;" class="listwhitetext"><fmt:message key='customerFile.originCity'/><font color="red" size="2">*</font></td>
	<td><s:textfield cssClass="input-text upper-case" name="wOCity" size="20" maxlength="30" tabindex="38" onblur="" onkeypress="" /></td>	
</tr>
<tr>
 <td style="text-align:right;" id="zipCodeRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originZip' /></td>
<td style="text-align:right;" id="zipCodeRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.originZip' /><font color="red" size="2">*</font></td>
	<td colspan="5"><s:textfield cssClass="input-text" name="wOZip" size="17" maxlength="10" onchange="" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="" /></td>
</tr>
</table>

</td>

<td style="padding:0px;" valign="top">
<table cellspacing="0" cellpadding="0" border="0" style="margin:0px;">
<tr>
<td class="subcontenttabChild" style="height:15px;text-align:center;" align="center" colspan="6"><b>To Address Details</b></td>
</tr>
<tr>
<td style="text-align:right;" class="listwhitetext">Address1</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="wDAddress1" size="25" maxlength="50" tabindex="" /></td>
<td style="text-align:right;" class="listwhitetext">Address2</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="wDAddress2" size="25" maxlength="50" tabindex="" /></td>
<td style="text-align:right;" class="listwhitetext">Address3</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="wDAddress3" size="23" maxlength="50" tabindex="" /></td>
</tr>
<tr>
<td style="text-align:right;" class="listwhitetext"><fmt:message key='customerFile.destinationCountry'/><font color="red" size="2">*</font></td>
	<td><s:select cssClass="list-menu" name="wDestinCountry" id="dcountry" list="%{country}" cssStyle="width:147px" onchange="getStateDestin(this);enableStateListDest();"  headerKey="" headerValue="" tabindex="" /></td>
	
	<td style="text-align:right;" id="destinationStateRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.destinationState' /></td>
	<td style="text-align:right;" id="destinationStateRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.destinationState' /><font color="red" size="2">*</font></td>
	<td><s:select cssClass="list-menu" name="wDState" id="dstate" list="%{dstates}" cssStyle="width:145px" onchange="" headerKey="" headerValue="" tabindex="" /></td>
	
	<td style="text-align:right;" class="listwhitetext"><fmt:message key='customerFile.destinationCity'/><font color="red" size="2">*</font></td>
	<td><s:textfield cssClass="input-text upper-case" name="wDCity" size="20" maxlength="30" tabindex="38" onblur="" onkeypress="" /></td>	
</tr>
<tr>
<td style="text-align:right;height:25px;" class="listwhitetext"><fmt:message key='customerFile.destinationZip' /></td>
	<td colspan="5"><s:textfield cssClass="input-text" name="wDZip" size="17" maxlength="10" onchange="" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="" /></td>
</tr>
</table>

</td>
</tr>
<tr>
<td colspan="2" valign="top" style="padding:0px;">
<table  cellspacing="0" cellpadding="0" border="0" style="margin:0px;width:100%;">
<tr>
<td class="subcontenttabChild" style="text-align: left; padding-left: 21%; height: 10px;" colspan="6"><b>Weight</b></td>
</tr>
<tr>
<td style="text-align: right; width: 48px;" class="listwhitetext">Weight</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="wActWt" size="35" maxlength="50" tabindex="28" />&nbsp;&nbsp;&nbsp;&nbsp;

		<input type="button" class="cssbutton" style="width:55px;margin-left:26%;"  value="Save" onclick="updateWTOriAdd();"/> 
		<input type="button" class="cssbutton" style="width:55px;"  value="Cancel" onclick="hideFromAddress();"/> 
	</td>
</tr>	
</table>
</td>
</tr>
</table>
</div>

<table class="table" border="1" cellpadding="0" cellspacing="0" style="margin-bottom:0px;">
		
	<tbody>
		<tr>
		<td style="text-align:right;width:47px;" class="listwhitetext" >View:</td>			
			<td width="30">
			     <s:select cssClass="list-menu" name="wRHouse" list="%{house}" cssStyle="width:115px" headerKey="" headerValue="" />
			</td>			
				<th width="70" class="listwhitetext">Selected date:</th>				
			 <c:if test="${not empty dailySheetDate}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="dailySheetDate" /></s:text>
				<td width="100" ><s:textfield cssClass="input-text" id="date1" name="dailySheetDate" value="%{customerFiledate1FormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/>
				<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty dailySheetDate}">
				<td width="100"><s:textfield cssClass="input-text" id="date1" name="dailySheetDate" required="true" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/>
				<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>				
	<td width="200" >  
		<input type="button" class="cssbutton" style="width:55px;"  value="Search" onclick="return goToSearch();"/> 	 
		<input type="button" class="cssbutton" style="width:125px;"  value="Print Daily Sheet" onclick=""/> 
	</td>
		<td align="left" class="listwhitetext">Total Men:&nbsp;${crewNumList}</td>
		<td align="left" width="89" class="listwhitetext">Total OMD Men:&nbsp;${omdCrewsNumList}</td>
	<td style="text-align:center;">
		<input type="button" class="cssbutton" style="width:115px;" value="Print Used Crew" onclick="return ;"/> 
	</td>
	<td style="text-align:right;width:179px;"> 
		<input type="button" class="cssbutton" style="width:75px;"  value="Save Notes" onclick="updateDailySheetNote() ;"/> 
	</td> 
		</tr>
	</tbody>
</table>
 

<table style="margin:0px;padding:0px;border:none;width:100%" cellpadding="0" cellspacing="0">
<tr>
<td valign="top">
<table cellspacing="0" cellpadding="4" style="margin: 0px; padding: 0px; width: 97%; text-align: center; border:none;border-left:2px solid #74B3DC;">
<tr>
<td class="listwhitetext" style="height:15px;font-size:12px;">Moves</td>
</tr>
</table>
<table cellspacing="0" cellpadding="4" style="margin: 0px; padding: 0px; width: 100%; text-align: center;border:2px solid #74B3DC;border-bottom:none;border-right:none;">
<tr>
<td><b>
<c:forEach var="entry" items="${house}">
							<c:if test="${wRHouse==entry.key}">
							<c:out value="${entry.value}" />
							</c:if>
							</c:forEach>
				&nbsp;Daily Sheet For&nbsp;${dailySheetDateDisplay}</b></td>
				</tr>
	</table>
<div style="width:835px;height:400px;overflow:auto;">
<table class="table-DS" id="dataTable" style="width:100%;margin-top:0px;">
					 <thead>					 
					 <tr>
					<th width="">Service</th>
					 <th width="">Reg #</th>
					 <th width="">Ticket</th>
					 <th width="">Customer</th>
					 <th width="">From Address</th>
					 <th width="">To Address</th>
					 <th width="">Account</th>
					 <th width="">Weight</th>
					 <th width="">Crew Name</th>
					 <th width="">Truck</th>
					 <th width="">Time</th>
					 <th width="">Men</th>
					 <th width="">Notes</th>
					 </tr>
					 </thead>
					 <tbody>
					 <c:if test="${dailySheetList!='[]'}">					
					 <c:forEach var="individualItem" items="${dailySheetList}" >
					 <tr>
					 <td  class="listwhitetextBlue" >${individualItem.service}</td>
					 <td  class="listwhitetextBlue" >${individualItem.regNumber}</td>
					 <td  class="listwhitetextBlue" >${individualItem.ticket}</td>
					 <td  class="listwhitetextBlue" >${individualItem.customerName}</td>
					 <td  class="listwhitetextBlueLink" onclick="updateOriginAddress('${individualItem.id}');document.getElementById('crewDetails').style.display='none';">${individualItem.fromAddress}</td>
					 <td  class="listwhitetextBlueLink" onclick="updateOriginAddress('${individualItem.id}');document.getElementById('crewDetails').style.display='none';">${individualItem.toAddress}</td>
					 <td  class="listwhitetextBlue" >${individualItem.accountName}</td>
					 <td  class="listwhitetextBlueLink" onclick="updateOriginAddress('${individualItem.id}');document.getElementById('crewDetails').style.display='none';">${individualItem.actualWeight}</td>
					 <td  class="listwhitetextBlueLink" onclick="findCrewDetails('${individualItem.wareHouse}','${individualItem.ticket}','${individualItem.shipNumber}','${individualItem.customerName}','${individualItem.jobType}','${individualItem.service}',0);document.getElementById('workTicketFromAdd').style.display='none';">${individualItem.crewName}</td>
					 <td  class="listwhitetextBlueLink" onclick="findTruckDetails('${individualItem.wareHouse}','${individualItem.ticket}','${individualItem.shipNumber}','${individualItem.customerName}','${individualItem.jobType}','${individualItem.service}',0);document.getElementById('workTicketFromAdd').style.display='none';">${individualItem.truckNo}</td>
					 <td  class="listwhitetextBlue" >${individualItem.dates}</td>
					 <td  class="listwhitetextBlue" >${individualItem.noOfCrews}</td>
					 <td  class="listwhitetextBlue" >
					 <c:if test="${individualItem.notes==''}">
					<img id="open${individualItem.id}" src="${pageContext.request.contextPath}/images/open.png" onclick="javascript:openWindow('notess.html?id=${individualItem.id }&notesId=${individualItem.ticket}&noteFor=WorkTicket&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
				</c:if>
				<c:if test="${individualItem.notes!=''}">
					<img id="close${individualItem.id}" src="${pageContext.request.contextPath}/images/closed.png" onclick="javascript:openWindow('notess.html?id=${individualItem.id }&notesId=${individualItem.ticket}&noteFor=WorkTicket&imageId=countForTicketNotesImage&fieldId=countForTicketNotes&decorator=popup&popup=true',800,520);"/>
				</c:if>
					 </td>
					 </tr>					 
					 </c:forEach>
					 </c:if>
					  <c:if test="${dailySheetList=='[]'}">
					  <td  class="listwhitetextWhite" colspan="15">Nothing Found To Display.</td>
					  </c:if>
					 </tbody>		 		  
					 </table>
					 </div>
					</td>
			<td valign="top">
			<div style="height:444px;overflow:auto;">
	<table class="table" id="dataTable" style="width:100%;border-top:none;margin-top:0px;vertical-align:top;">
					 <thead>
					 <tr>
					 <th width=""></th>
					<th width="">Crew Name</th>
					 <th width="">Agent</th>
					 <th width="">Lic</th>
					 <th width="">Hours</th>					
					 
					 </tr>
					 </thead>
					 <tbody>
					 <c:if test="${availableCrewsDetails!='[]'}">	
					 <c:forEach var="individualItem" items="${availableCrewsDetails}" >	
					 <tr>
					 <c:if test="${individualItem.isCrewUsed==false}">
					 <td  class="listwhitetext" align="right" ></td>
					 <td  style="color:#003366;" class="listwhitetextCrew" align="right" >${individualItem.lastName}&nbsp;${individualItem.firstName}</td>
					 <td  style="color:#003366;" class="listwhitetextCrew" >${individualItem.typeofWork}</td>
					 <td  style="color:#003366;" class="listwhitetextCrew" >${individualItem.licenceNumber}</td>
					 <td  style="color:#003366;" class="listwhitetextCrew" align="right" >
					 <c:if test="${individualItem.availableHours==''}">0.00 </c:if>
					 <c:if test="${individualItem.availableHours!=''}">
					 <c:out value="${individualItem.availableHours}" />
					 </c:if>
					 </td>					 
					  </c:if>
					  
					  <c:if test="${individualItem.isCrewUsed==true}">
					  <td  style="color:red;" align="right" >
					 	<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=10 WIDTH=10 ALIGN=TOP />
					 </td>
					 <td  style="color:red;" class="listwhitetextCrew" align="right" >${individualItem.lastName}&nbsp;${individualItem.firstName}</td>
					 <td  style="color:red;" class="listwhitetextCrew" align="right" >${individualItem.typeofWork}</td>
					 <td  style="color:red;" class="listwhitetextCrew" align="right" >${individualItem.licenceNumber}</td>
					 <td  style="color:red;" class="listwhitetextCrew" align="right" >
					 <c:if test="${individualItem.availableHours==''}">0.00</c:if>
					 <c:if test="${individualItem.availableHours!=''}">
					 <c:out value="${individualItem.availableHours}" />
					 </c:if>
					 </td>					
					  </c:if>					  				 
					 </tr>		 
					</c:forEach>
					</c:if>
					 <c:if test="${availableCrewsDetails=='[]'}">
					  <td  class="listwhitetextWhite" colspan="5">Nothing Found To Display.</td>
					  </c:if>
					 </tbody>		 		  
					 </table>
					 </div>
					</td>
				<td valign="top">
				<div style="height:444px;overflow:auto;">
		<table class="table" id="dataTable" style="width:100%;border-top:none;margin-top:0px;vertical-align:top;">
					 <thead>
					 <tr>
					 <th width=""></th>
					<th width="">Truck</th>					 
					 </tr>
					 </thead>
					 <tbody>	
					  <c:forEach var="individualItem" items="${availableTrucks}" >
					 <tr>
					  <c:if test="${individualItem.isTruckUsed==false}">
					   <td  class="listwhitetext" align="right" ></td>
					 <td  style="color:#003366;" class="listwhitetextCrew" align="right" >${individualItem.truckNumber}</td>	
					 </c:if>	
					 <c:if test="${individualItem.isTruckUsed==true}">
					 <td  style="color:red;" class="listwhitetextCrew" align="right" >
					 	<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=10 WIDTH=10 ALIGN=TOP />
					 </td>
					 <td  style="color:red;" class="listwhitetextCrew" align="right" >${individualItem.truckNumber}</td>	
					 </c:if>						 
					 </tr>		 
					</c:forEach>	 
				
					 </tbody>		 		  
					 </table>
					 </div>
					</td>
		<td valign="top">
	<table class="table" id="dataTable" style="width:100%;border-top:none;;margin-top:0px;vertical-align:top;">
					 <thead>
					 <tr>
					<th width="">Notes</th>					 
					 </tr>
					 </thead>
					 <tbody>	
					 <tr>
					 <td  class="listwhitetext" align="right" width="100px">
					 <s:textarea cssClass="textarea" name="wDailySheetNotes"  onchange="" cssStyle="width:100px;min-height:405px;font-size:11px;border:none;" tabindex="" />
					 </td>								 
					 </tr>		 
				
					 </tbody>		 		  
					 </table>
					</td>
					</tr>
					</table>
		
</s:form>
<script type="text/javascript"> 
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
	enableStateListOrigin();
	enableStateListDest();
	document.getElementById('workTicketFromAdd').style.display="none";
	document.getElementById('crewDetails').style.display="none";
	try{
		var dSNote="${dailySheetNotesValue}";		
		document.forms['dailySheetForm'].elements['wDailySheetNotes'].value=dSNote.replace(RegExp('<br/>', 'g'), '\n');
	}catch(e){} 	
</script>

  