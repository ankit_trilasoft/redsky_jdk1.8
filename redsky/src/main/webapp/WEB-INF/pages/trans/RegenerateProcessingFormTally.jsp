<%@ include file="/common/taglibs.jsp"%>   
 <%@ taglib prefix="s" uri="/struts-tags" %> 

<head>

	<title>Regenerate Sales Invoice</title>   
    <meta name="heading" content="Regenerate Sales Invoice"/> 
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
	function validateEnteredFile()
	{
var fileName=document.forms['tallyRegenerateExtractForm'].elements['invoiceFileName'].value;
	if(fileName!='')
	{
    var url='validateEnteredFile.html?ajax=1&decorator=simple&popup=true&fileName='+encodeURI(fileName);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    
	}
	}
	function handleHttpResponse2()
        {
			
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
				results = results.replace('[','');
				results = results.replace(']','');					
				var count = parseInt(results);
				if(count>0)
				{}			    
				else
				{
				alert('No Such File Exist');
			 	}
             } 
  	   }
   var http2 = getHTTPObject();
function getHTTPObject()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function validatingField()
{
var fileName=document.forms['tallyRegenerateExtractForm'].elements['invoiceFileName'].value;
	fileName=fileName.trim();
	if(fileName=='')
	{
	alert('Please enter the file name');
	return false;
	}
	if(fileName.length>30)
	{
	alert('Please enter valid file name');
	document.forms['tallyRegenerateExtractForm'].elements['invoiceFileName'].value='';
	return false;
	}
	if(fileName.indexOf("'")>-1)
	{
	alert('Please enter valid file name');
	document.forms['tallyRegenerateExtractForm'].elements['invoiceFileName'].value='';
	return false;
	}	
	
	if(fileName!='')
	{
       var agree = confirm("Press OK to continue for regenerate or Press Cancel to Continue without regenerate ");
						       if(agree)
						        {
						    	   document.forms['tallyRegenerateExtractForm'].action = 'regenerateInvoiceExtractsTally.html';
						            document.forms['tallyRegenerateExtractForm'].submit()
						        }
						       else
						        {
						        return false;
								}
	}
}
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">

</script>
	<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script>  
</head>
<body style="background-color:#444444;">
<s:form id="tallyRegenerateExtractForm" name="tallyRegenerateExtractForm" action="tallyRegenerateInvoiceExtracts" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:85% " >
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top: 10px;!margin-top: -2px"><span></span></div>
   <div class="center-content">
<table cellspacing="1" cellpadding="1" border="0" > <tr>
<td class="listwhitetext">Enter The Extract File Name</td>
<td><s:textfield cssClass="input-text" name="invoiceFileName" value="" cssStyle="width:200px" /></td><td></td></tr>
<tr><td class=""></td>
	<td align="left" colspan="2">
<input type="button" class="cssbutton" value="Regenerate Sales Invoice" align="right" onclick="return validatingField();">
</td><td></td></tr>

</table>
</div>
<div class="bottom-header"><span></span></div> 

</div></div></div></s:form></body> 