package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ToDoResult;
import com.trilasoft.app.model.ToDoRule;

public interface ToDoResultDao extends GenericDao<ToDoResult, Long>{
	
	public List findMaximumId();
	public void updateToDoResult(String sessionCorpID);
	public List getTaskList(String filenumber,String sessionCorpID,Long id);
}
