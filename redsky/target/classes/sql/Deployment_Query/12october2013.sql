// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

ALTER TABLE `redsky`.`cportalresourcemgmt` MODIFY COLUMN 
`childBillToCode` VARCHAR(1000) CHARACTER SET utf8 COLLATE 
utf8_unicode_ci DEFAULT NULL;

ALTER TABLE serviceorder ADD COLUMN projectedGrossMarginPercentage DECIMAL(19,2) DEFAULT '0.00' AFTER noOfEmailsSent,ADD COLUMN projectedGrossMargin DECIMAL(19,2) DEFAULT '0.00' AFTER projectedGrossMarginPercentage;

ALTER TABLE `redsky`.`todoresult` ADD COLUMN `recordToDel` VARCHAR(5) DEFAULT 0 AFTER `billToName`;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `serviceCompleteDate` DATETIME DEFAULT NULL AFTER `VIS_comment`;

ALTER TABLE `redsky`.`todoresult` ADD COLUMN `mailStauts` VARCHAR(10) AFTER `recordToDel`;

ALTER TABLE `redsky`.`emailsetup` MODIFY COLUMN `signaturePart` TEXT DEFAULT NULL;

ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `missedRDDNotification` DATETIME DEFAULT NULL AFTER `podToBooker`, ADD COLUMN `reason` VARCHAR(45) DEFAULT NULL AFTER `missedRDDNotification`;
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('SSCW','RDDREASON',20,true,now(),'adminsscw',now(),'adminsscw',1,1,'');
 
Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('SSCW', 'ABC', 'ABC', 40, 'RDDREASON','en',now(),'adminsscw',now(),'adminsscw');

ALTER table app_user add index `parentAgent_idx` (`parentAgent`);

ALTER table trackingstatus add index `originAgentCode_idx` (`originAgentCode`);

ALTER table trackingstatus add index `destinationAgentCode_idx` (`destinationAgentCode`);
 
ALTER table trackingstatus add index `destinationSubAgentCode_idx` (`destinationSubAgentCode`);
 
ALTER table trackingstatus add index `OriginSubAgentCode_idx` (`OriginSubAgentCode`);

ALTER TABLE `redsky`.`companydivision` ADD COLUMN `lastUpdatedLeave` DATETIME DEFAULT NULL AFTER `mssPassword`;

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `serviceOrderFlagCarrier` VARCHAR(20) DEFAULT 'NULL' AFTER `projectedGrossMargin`;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `usaFlagRequired` bit(1) DEFAULT b'0' AFTER `localName`;

ALTER TABLE `redsky`.`vanline` ADD COLUMN `accountLineNumber` VARCHAR(04) DEFAULT '' AFTER `division`;

ALTER TABLE `redsky`.`subcontractorcharges` MODIFY COLUMN `description` VARCHAR(225);

ALTER TABLE `redsky`.`vanline` MODIFY COLUMN `distributionCodeDescription` VARCHAR(225);

ALTER TABLE `redsky`.`claim` ADD COLUMN `clmsClaimNbr` BIGINT DEFAULT NULL AFTER `noOfDaysForSettelment`;

ALTER TABLE `redsky`.`claim` ADD COLUMN `origClmsAmt` DECIMAL(10,2) DEFAULT NULL AFTER `clmsClaimNbr`;

ALTER TABLE `redsky`.`claim` ADD COLUMN `pmntAmt` DECIMAL(10,2) DEFAULT NULL AFTER `origClmsAmt`;

ALTER TABLE `redsky`.`loss` ADD COLUMN `itmItemCode` VARCHAR(45) DEFAULT NULL AFTER `idNumber`;

ALTER TABLE `redsky`.`loss` ADD COLUMN `itmClmsItemSeqNbr` VARCHAR(20) DEFAULT NULL AFTER `itmItemCode`;

ALTER TABLE `redsky`.`loss` ADD COLUMN `itmDnlRsnCode` VARCHAR(45) DEFAULT NULL AFTER `itmClmsItemSeqNbr`;

ALTER TABLE `redsky`.`loss` MODIFY COLUMN `lossAction` VARCHAR(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;

ALTER TABLE `redsky`.`loss` ADD COLUMN `itrPmntId` BIGINT DEFAULT NULL;

ALTER TABLE `redsky`.`loss` ADD COLUMN `itmAltItemDesc` VARCHAR(45) ;

ALTER TABLE `redsky`.`loss` ADD COLUMN `itrRespExcpCode` VARCHAR(4) ;

ALTER TABLE `redsky`.`loss` ADD COLUMN `itrAssessdLiabilityAmt` DECIMAL(10,2) DEFAULT NULL ;

ALTER TABLE `redsky`.`loss` ADD COLUMN `itrTotalLiabilityAmt` DECIMAL(10,2) DEFAULT NULL ;

insert into refmaster (code,description,parameter,corpid,createdby,createdon,updatedby,updatedon,language,fieldlength)
values('01','Pre-existing Damage - Res','DENIALREASON','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('02','Pre-existing Damage - Whse','DENIALREASON','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('04','Owner Packed','DENIALREASON','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('05','No Exceptions at Delivery','DENIALREASON','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('06','No Exceptions at Origin','DENIALREASON','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('07','Mechanical Failure','DENIALREASON','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('08','Deductible Not Met','DENIALREASON','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('11','Nine Months Period Expired','DENIALREASON','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('13','Not Available for Inspection','DENIALREASON','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('14','Moved Prior to Filing','DENIALREASON','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('21','Inherent Vice','DENIALREASON','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('22','Item Not On Inventory','DENIALREASON','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('23','No Timely Notification','DENIALREASON','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5);

insert into parametercontrol (corpid,parameter,comments,fieldlength,active,createdon,createdby,updatedon,updatedby,tsftflag,hybridflag,customtype)
values ('SSCW','DENIALREASON','Denial Reason',5,true,now(),'SSCW_SETUP',now(),'SSCW_SETUP',1,1,'');

insert into refmaster (code,description,parameter,corpid,createdby,createdon,updatedby,updatedon,language,fieldlength)
values ('BOKR','Booker Agent','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('DLYB','Delay Charged Booker','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('DLYD','Delay Charged Destination Agent','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('DLYO','Delay Charged Origin Agent','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('DNAG','Delay Non Agent','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('DS01','Dedicated Service','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('G-11','Pickup at Carrier\'s Convenience','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('HAUL','Hauling','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('I200','Item 200','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('NAGT','Non Agent','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('PACK','Packing','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('SETO','Set-off','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('SUBR','Subrogator','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('UNPK','Unpacking','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('WHSE','Warehouse','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('XDRV','Drvr that trns since claim ord','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5);

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `ownbillingBilltoCodes` VARCHAR(225) DEFAULT '' AFTER `paymentApplicationExtractSeq`;

insert into refmaster (code,description,parameter,corpid,createdby,createdon,updatedby,updatedon,language,fieldlength)
values ('CASH','Cashed Out','CLM_ACTN','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CLSD','Closed','CLM_ACTN','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('FND','Found','CLM_ACTN','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('INSP','Inspected','CLM_ACTN','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('IRVA','Insepct/Repair Vendor Approval','CLM_ACTN','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('PDMA','Pending Mgmt. Approval','CLM_ACTN','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('PDTP','Pending Release Letter','CLM_ACTN','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('REBT','Rebuttal','CLM_ACTN','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('REPL','Replaced','CLM_ACTN','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('REPR','Repaired','CLM_ACTN','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('REPT','Reported','CLM_ACTN','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('RPVA','Replacement Vendor Assigned','CLM_ACTN','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('SHIP','Shipment Authorized','CLM_ACTN','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('WTHD','Withdrawn','CLM_ACTN','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5);

insert into refmaster (code,description,parameter,corpid,createdby,createdon,updatedby,updatedon,language,fieldlength)
values ('CA01','Drinking and/or drugs','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA02','Bad Equipment','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA03','Failure to inventory','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA04','Alteration of documentation','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA05','Preventable accidents','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA06','Van fires resulting from equip repairs','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA07','Loading/unloading in rain/snow without care','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA08','Tailgating','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA09','Unauthorized user of customer vehicle','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA10','Not padding/not using onboard equipment','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA11','Blant and irresponsible mishandling','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA12','Delivery to an incorrect address','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA13','Theft due to unsecured van','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA14','Failure to supervise/provide help','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA15','Improper and insufficient packing','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA16','Failure to report accident/fire/smoke/water damage','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA17','Misrepresentation of valuation','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA18','Using a non-qualified driver','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA19','Warehouse damages due to careless action','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('CA20','Failure to use Clear Guard','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('MI35','Missing items - Proper procedures not followed','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('MS21','Policy Payment','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('MS25','Customer Service Payment','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('PS40','Permanent Storage','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5),
('DS01','Dedicated Service','RESPEXPCODE','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5);

insert into parametercontrol (corpid,parameter,comments,fieldlength,active,createdon,createdby,updatedon,updatedby,tsftflag,hybridflag,customtype)
values ('SSCW','RESPEXPCODE','Response Except10/4/2013ion Code',5,true,now(),'SSCW_SETUP',now(),'SSCW_SETUP',1,1,'');

insert into refmaster (code,description,parameter,corpid,createdby,createdon,updatedby,updatedon,language,fieldlength)
values ('CONT','Containerized shipment','DAMAGED','SSCW','SSCW_SETUP',now(),'SSCW_SETUP',now(),'en',5);

ALTER TABLE `redsky`.`emailsetup` MODIFY COLUMN `recipientTo` TEXT DEFAULT NULL,
 MODIFY COLUMN `recipientCc` TEXT DEFAULT NULL,
 MODIFY COLUMN `recipientBcc` TEXT DEFAULT NULL;

delete from refmaster where parameter='QSURVEYRECIPIENT';

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,flex1)values
('UTSI', 'Transferee', '^$emailTransferee$^', 200, 'QSURVEYRECIPIENT','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Transferee'),
('UTSI', 'Move Initiator', '^$accountContactEmail$^', 200, 'QSURVEYRECIPIENT','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Move Initiator'),
('UTSI', 'Transferee & Move Initiator', '^$emailTransferee$^,^$accountContactEmail$^', 200,'QSURVEYRECIPIENT','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Transferee & Move Initiator'),
('UTSI', 'Account Manager', '^$accountManager$^', 200, 'QSURVEYRECIPIENT','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Account Manager'),
('UTSI', 'HR Contact', '^$localHrEmail$^', 200, 'QSURVEYRECIPIENT','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','HR Contact'),
('UTSI', 'Transferee & HR Contact', '^$emailTransferee$^,^$localHrEmail$^', 200, 'QSURVEYRECIPIENT','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Transferee & HR Contact');

delete from parametercontrol where parameter='QSURVEYRECIPIENT';

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','QSURVEYRECIPIENT',200,true,now(),'UTSI_SETUP',now(),'UTSI_SETUP',1,1,'');

delete from refmaster where parameter='QSURVEYBODY';

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('UTSI', 'BuZa', '<p>Geachte ^$title_NL$^ ^$transferee$^,</p><p>Om de kwaliteit van onze dienstverlening te meten en waar mogelijk te verbeteren zouden we u willen vragen om onze online enquete in te vullen. Deze enquete heeft betrekking op de ^$modality$^ zending van ^$origin$^ naar ^$destination$^. U kunt de enquete openen door op de volgende link in de adres balk van uw browser te plakken:</p><p>^$url$^</p><p>Voor eventuele andere trajecten ontvangt u een separaat verzoek. Zowel het Ministerie van Buitenlandse Zaken als wij stellen uw medewerking zeer op prijs.</p><p>Met vriendelijke groet,</p><p>UniGroup UTS</p>', 50, 'QSURVEYBODY','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI', 'Continental AG/MI request', '<p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">Dear ^$lastNameMoveInitiator$^,</span></p><p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">The delivery of the ^$modality$^ shipment from ^$origin$^ to ^$destination$^ was on ^$actualDelivery$^. Up to today ^$title_UK$^ ^$transferee$^ did not fill out the quality survey. The personalized questionnaire can be found <a href="^$url$^">[Here]</a>.</span></p>', 50, 'QSURVEYBODY','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI', 'Continental Basis', '<p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">Dear ^$title_UK$^ ^$transferee$^,</span></p><p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">The delivery of your ^$modality$^ shipment from ^$origin$^ to ^$destination$^ was on ^$actualDelivery$^. We as well as your employer are interested in your experiences. We would like to ask for 5 minutes of your valued time to fill out an online questionnaire. The personalized questionnaire for this specific shipment can be found <a href=^$url$^>[Here]</a>.</span></p><p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">Please note that in case of multiple shipments (i.e. an AIR and a SEA shipment) a separate questionnaire will be send for each shipment. We ask you to limit this evaluation to the ^$modality$^ shipment from ^$origin$^ to ^$destination$^</span></p><p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">Kind regards,</span></p><table><tr><td colspan =3>UniGroup Worldwide UTS</td></tr><tr><td colspan =3>Headquarters Germany</td></tr><tr><td colspan =3>Wilhelm-Leuschner-Str. 41</td></tr><tr><td colspan =3>60329 Frankfurt am Main</td></tr><tr><td><!--Spacer--></td></tr><tr><td>Tel</td><td>:</td><td>+49 (0)69 244 50 490</td></tr><tr><td>Email</td><td>:</td><td><a href="mailto:moveandrelo@uts-continental.com">moveandrelo@uts-continental.com</a></td></tr><tr><td>Internet</td><td>:</td><td><a href="http://www.uts-germany.de">www.uts-germany.de</a></td></tr></table><p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana"><em>If above link does not work please paste the following link in the address bar of your browser<br />^$url$^</em></span></p>', 50, 'QSURVEYBODY','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI', 'Solvay central Follow-Up to Mrs Durieux', '<p><span style="font-size: small" style="font-family: Verdana">Dear Anne-Marie,<span></p><p><span style="font-size: small" style="font-family: Verdana">Please be informed that after two attempts the above mentioned Transferee did not react to our request to complete the quality questionnaire.<span></p><p><span style="font-size: small" style="font-family: Verdana">Your kind assistance will be appreciated.<span></p><p><span style="font-size: small" style="font-family: Verdana">Kind regards,<span></p><p><span style="font-size: small" style="font-family: Verdana">Anneke da Silva<br />Manager Client Relations<span><p><table><tr><td>t</td><td>:</td><td>+ 31 (294) 461 017</td></tr><tr><td>f</td><td>:</td><td>+ 31 (294) 417 014</td></tr><tr><td>e</td><td>:</td><td>a.silva@uts-intl.com</td></tr><tr><td colspan=3><!-- spacer --></td></tr><tr><td colspan=3>UniGroup Worldwide UTS Head Office</td></tr><tr><td colspan=3>Leeuwenveldseweg 16m, 1382 LX Weesp</td></tr><tr><td colspan=3>P.O. Box 284, 1380 AG Weesp</td></tr><tr><td colspan=3>The Netherlands</td></tr><tr><td colspan=3>www.uts-international.com</td></tr></table>', 50, 'QSURVEYBODY','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI', 'UTS DE Follow-Up', '<p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">Dear ^$lastNameMoveInitiator$^,</span></p><p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">The delivery of the ^$modality$^ shipment from ^$origin$^ to ^$destination$^ was on ^$actualDelivery$^. Up to today ^$title_UK$^ ^$transferee$^ did not fill out the quality survey. The personalized questionnaire can be found <a href="^$url$^">[Here]</a>.</span></p>', 50, 'QSURVEYBODY','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI', 'Default', '<p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">Dear ^$title_UK$^ ^$transferee$^,</span></p><p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">The delivery of your ^$modality$^ shipment from ^$origin$^ to ^$destination$^ was on ^$actualDelivery$^. We as well as your employer are interested in your experiences. We would like to ask for 1 minutes of your valued time to fill out an online questionnaire. The personalized questionnaire for this specific shipment can be found <a href=^$url$^>[Here]</a>.</span></p><p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">Please note that in case of multiple shipments (i.e. an AIR and a SEA shipment) a separate questionnaire will be send for each shipment. We ask you to limit this evaluation to the ^$modality$^ shipment from ^$origin$^ to ^$destination$^</span></p><p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">Kind regards,</span></p><table><tr><td colspan =3>UniGroup Worldwide UTS</td></tr><tr><td colspan =3>Leeuwenveldseweg 16m</td></tr><tr><td colspan =3>1382LX Weesp</td></tr><tr><td colspan =3>The Netherlands</td></tr><tr><td><!--Spacer--></td></tr><tr><td>Tel</td><td>:</td><td>+31 294 415012</td></tr><tr><td>Email</td><td>:</td><td><a href="mailto:customerservice@uts-intl.com">customerservice@uts-intl.com</a></td></tr><tr><td>Internet</td><td>:</td><td><a href="http://www.uts-international.com">www.uts-international.com</a></td></tr></table><p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana"><em>If above link does not work please paste the following link in the address bar of your browser<br />^$url$^</em></span></p>', 50, 'QSURVEYBODY','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI', 'UTS DE Basis', '<p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">Dear ^$title_UK$^ ^$transferee$^,</span></p><p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">The delivery of your ^$modality$^ shipment from ^$origin$^ to ^$destination$^ was on ^$actualDelivery$^. We as well as your employer are interested in your experiences. We would like to ask for 5 minutes of your valued time to fill out an online questionnaire. The personalized questionnaire for this specific shipment can be found <a href=^$url$^>[Here]</a>.</span></p><p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">Please note that in case of multiple shipments (i.e. an AIR and a SEA shipment) a separate questionnaire will be send for each shipment. We ask you to limit this evaluation to the ^$modality$^ shipment from ^$origin$^ to ^$destination$^</span></p><p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana">Kind regards,</span></p><table><tr><td colspan =3>UniGroup Worldwide UTS</td></tr><tr><td colspan =3>Headquarters Germany</td></tr><tr><td colspan =3>Wilhelm-Leuschner-Str. 41</td></tr><tr><td colspan =3>60329 Frankfurt am Main</td></tr><tr><td><!--Spacer--></td></tr><tr><td>Tel</td><td>:</td><td>+49 (0)69 244 50 490</td></tr><tr><td>Email</td><td>:</td><td><a href="mailto:info@uts-germany.com">info@uts-germany.com</a></td></tr><tr><td>Internet</td><td>:</td><td><a href="http://www.uts-germany.de">www.uts-germany.de</a></td></tr></table><p style="text-align: justify"><span style="font-size: small" style="font-family: Verdana"><em>If above link does not work please paste the following link in the address bar of your browser<br />^$url$^</em></span></p>', 200, 'QSURVEYBODY','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP');

delete from parametercontrol where parameter='QSURVEYBODY';

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','QSURVEYBODY',50,true,now(),'UTSI_SETUP',now(),'UTSI_SETUP',1,1,'');

delete from refmaster where parameter='QSURVEYSIGNATURE';

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('UTSI', 'UniGroup Worldwide UTS', 'customerservice@uts-int.com', 50, 'QSURVEYSIGNATURE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP');

delete from parametercontrol where parameter='QSURVEYSIGNATURE';

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','QSURVEYSIGNATURE',50,true,now(),'UTSI_SETUP',now(),'UTSI_SETUP',1,1,'');

delete from refmaster where parameter='QSURVEYSUBJECT';

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,flex1)values
('UTSI', 'BuZa', 'UniGroup UTS ::: J0^$jobId$^ ::: Kwaliteitsbeoordeling van uw verhuizing', 200, 'QSURVEYSUBJECT','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','BuZa'),
('UTSI', 'Conti - Transportation', 'UniGroup UTS ::: J0^$jobId$^ ::: ^$title_NL$^ ^$transferee$^ ::: Evaluation of removal', 200, 'QSURVEYSUBJECT','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Conti - Transportation'),
('UTSI', 'Default', 'UniGroup UTS ::: J0^$jobId$^::: Evaluation of your ^$modality$^ shipment from ^$origin$^ to ^$destination$^', 200, 'QSURVEYSUBJECT','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Default'),
('UTSI', 'Solvay central Follow-Up to Mrs Durieux', 'Quality questionnaire for the removal of ^$title_NL$^ ^$transferee$^- UTS ref.# J^$jobId$^::: Move from ^$origin$^ to ^$destination$^', 200, 'QSURVEYSUBJECT','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Solvay central Follow-Up to Mrs Durieux'),
('UTSI', 'Conti - Relo', 'UniGroup UTS ::: J0^$jobId$^ ::: ^$title_NL$^ ^$transferee$^ ::: Evaluation of relocation services', 200, 'QSURVEYSUBJECT','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Conti - Relo');

delete from parametercontrol where parameter='QSURVEYSUBJECT';

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','QSURVEYSUBJECT',200,true,now(),'UTSI_SETUP',now(),'UTSI_SETUP',1,1,'');

delete from refmaster where parameter='QUALITYSURVEYURL';

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,flex1)values
('UTSI', '265842', 'http://www.keysurvey.co.uk/survey/265842/1ae7/?LQID=1&mail=^$emailTransferee$^&name=^$title_NL$^%20^$transferee$^&o=^$origin$^&d=^$destination$^&oa=^$originAgent$^&da=^$destinationAgent$^&jobid=^$jobId$^', 200, 'QUALITYSURVEYURL','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','BuZa'),
('UTSI', '308624', 'http://www.keysurvey.co.uk/survey/308624/2061/?LQID=1&mail=^$emailTransferee$^&name=^$title_UK$^%20^$transferee$^&o=^$origin$^&d=^$destination$^&oa=^$originAgent$^&da=^$destinationAgent$^&jobid=^$jobId$^', 200, 'QUALITYSURVEYURL','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Continental'),
('UTSI', '340829', 'http://www.keysurvey.co.uk/survey/340829/f6ec/?LQID=1&mail=^$emailTransferee$^&name=^$title_UK$^%20^$transferee$^&o=^$origin$^&d=^$destination$^&mm=^$moveManager$^&jobid=^$jobId$^&accid=^$accountID$^&acc=^$account$^', 200,'QUALITYSURVEYURL','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Standard'),
('UTSI', '353780', 'http://www.keysurvey.co.uk/survey/353780/2fc8/?LQID=1&mail=^$emailTransferee$^&name=^$transferee$^&o=^$origin$^&d=^$destination$^&oa=^$originAgent$^&da=^$destinationAgent$^&jobid=^$jobId$^&accid=^$accountID$^&tt=^$modality$^', 200,'QUALITYSURVEYURL','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Schaeffler'),
('UTSI', '554352', 'http://www.keysurvey.co.uk/f/554352/4ac4/?LQID=1&mail=^$emailTransferee$^&name=^$transferee$^&o=^$origin$^&d=^$destination$^&oa=^$originAgent$^&da=^$destinationAgent$^&jobid=^$jobId$^&accid=^$accountID$^&tt=^$modality$^', 200,'QUALITYSURVEYURL','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','Quality Questionnaire UniGroup UTS (v2) TEST'),
('UTSI', '414033', 'http://www.keysurvey.co.uk/survey/414033/266a/?LQID=1&mail=^$emailTransferee$^&name=^$transferee$^&o=^$origin$^&d=^$destination$^&oa=^$originAgent$^&da=^$destinationAgent$^&jobid=^$jobId$^&accid=^$accountID$^&tt=^$modality$^', 200, 'QUALITYSURVEYURL','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','TEST URL');

delete from parametercontrol where parameter='QUALITYSURVEYURL';

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','QUALITYSURVEYURL',200,true,now(),'UTSI_SETUP',now(),'UTSI_SETUP',1,1,'');

delete from refmaster where parameter='ANSWERIDPARAM';

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,flex4,flex2,flex3)values
('UTSI', '414033', 'JOBID', 20, 'ANSWERIDPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','[Q1.A7]','TEXT','1'),
('UTSI', '414033', 'TRANSFERRE', 20, 'ANSWERIDPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','47940553','TEXT','1'),
('UTSI', '414033', 'RECOMMEND', 20, 'ANSWERIDPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','47940521','RADIO','3'),
('UTSI', '414033', 'DESTINATION', 20, 'ANSWERIDPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','47940489','RADIO','3'),
('UTSI', '414033', 'ORIGIN', 20, 'ANSWERIDPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','47940648','RADIO','3'),
('UTSI', '414033', 'OVERALL', 20, 'ANSWERIDPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','47940646,47940487,47940519,47940551,47940583,47940615,47940647,47940488,47940520,47940552','DROPDOWN','10'),
('UTSI', '414033', 'PREMOVE', 20, 'ANSWERIDPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','47940414,47940446,47940474,47940480,47940512,47940544,47940576,47940608,47940640,47940475','DROPDOWN','10'),
('UTSI', '554352', 'JOBID', 20, 'ANSWERIDPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','[Q2.A6]','TEXT','1'),
('UTSI', '554352', 'TRANSFERRE', 20, 'ANSWERIDPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','66619430','TEXT','1'),
('UTSI', '554352', 'RECOMMEND', 20, 'ANSWERIDPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','66619429','RADIO','3'),
('UTSI', '554352', 'DESTINATION', 20, 'ANSWERIDPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','66619411','RADIO','6'),
('UTSI', '554352', 'ORIGIN', 20, 'ANSWERIDPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','66619410','RADIO','6'),
('UTSI', '554352', 'OVERALL', 20, 'ANSWERIDPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','66619413','RADIO','6'),
('UTSI', '554352', 'PREMOVE', 20, 'ANSWERIDPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','66619409','RADIO','6');

delete from parametercontrol where parameter='ANSWERIDPARAM';

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','ANSWERIDPARAM',20,true,now(),'UTSI_SETUP',now(),'UTSI_SETUP',1,1,'');

delete from refmaster where parameter='QUALITYUSERPARAM';

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('UTSI', 'KeySurvey2013!', 'w.heijden@uts-intl.com', 50, 'QUALITYUSERPARAM','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP');

delete from parametercontrol where parameter='QUALITYUSERPARAM';

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','QUALITYUSERPARAM',50,true,now(),'UTSI_SETUP',now(),'UTSI_SETUP',1,1,'');

delete from refmaster where parameter='ANSWERVALUE';

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,flex1,flex2,flex3)values
('UTSI', '47940521', 'RECOMMEND', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','1','10','11'),
('UTSI', '47940521', 'RECOMMEND', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','2','20','21'),
('UTSI', '47940521', 'RECOMMEND', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','3','30','31'),
('UTSI', '47940648', 'ORIGIN', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','1','10','11'),
('UTSI', '47940648', 'ORIGIN', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','2','20','21'),
('UTSI', '47940648', 'ORIGIN', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','3','30','31'),
('UTSI', '47940489', 'DESTINATION', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','1','10','11'),
('UTSI', '47940489', 'DESTINATION', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','2','20','21'),
('UTSI', '47940489', 'DESTINATION', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','3','30','31'),
('UTSI', '47940616', 'TRANSFERRE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','1','10','11'),
('UTSI', '47940616', 'TRANSFERRE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','2','20','21'),
('UTSI', '47940616', 'TRANSFERRE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','3','30','31'),
('UTSI', '47940646', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','10','99','91'),
('UTSI', '47940487', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','9','90','91'),
('UTSI', '47940519', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','8','80','81'),
('UTSI', '47940551', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','7','70','71'),
('UTSI', '47940583', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','6','60','61'),
('UTSI', '47940615', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','5','50','51'),
('UTSI', '47940647', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','4','40','41'),
('UTSI', '47940488', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','3','30','31'),
('UTSI', '47940520', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','2','20','21'),
('UTSI', '47940552', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','1','10','11'),
('UTSI', '47940414', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','10','99','91'),
('UTSI', '47940446', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','9','90','91'),
('UTSI', '47940474', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','8','80','81'),
('UTSI', '47940480', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','7','70','71'),
('UTSI', '47940512', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','6','60','61'),
('UTSI', '47940544', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','5','50','51'),
('UTSI', '47940576', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','4','40','41'),
('UTSI', '47940608', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','3','30','31'),
('UTSI', '47940640', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','2','20','21'),
('UTSI', '47940475', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','1','10','11'),
('UTSI', '66619429', 'RECOMMEND', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','1','10','1'),
('UTSI', '66619429', 'RECOMMEND', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','2','9','2'),
('UTSI', '66619429', 'RECOMMEND', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','3','8','3'),
('UTSI', '66619410', 'ORIGIN', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','1','10','1'),
('UTSI', '66619410', 'ORIGIN', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','2','9','2'),
('UTSI', '66619410', 'ORIGIN', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','3','8','3'),
('UTSI', '66619410', 'ORIGIN', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','4','7','4'),
('UTSI', '66619410', 'ORIGIN', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','5','6','5'),
('UTSI', '66619410', 'ORIGIN', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','6','5','6'),
('UTSI', '66619411', 'DESTINATION', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','1','10','1'),
('UTSI', '66619411', 'DESTINATION', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','2','9','2'),
('UTSI', '66619411', 'DESTINATION', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','3','8','3'),
('UTSI', '66619411', 'DESTINATION', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','4','7','4'),
('UTSI', '66619411', 'DESTINATION', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','5','6','5'),
('UTSI', '66619411', 'DESTINATION', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','6','5','6'),
('UTSI', '66619432', 'TRANSFERRE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','1','10','11'),
('UTSI', '66619413', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','1','10','1'),
('UTSI', '66619413', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','2','9','2'),
('UTSI', '66619413', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','3','8','3'),
('UTSI', '66619413', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','4','7','4'),
('UTSI', '66619413', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','5','6','5'),
('UTSI', '66619413', 'OVERALL', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','6','5','6'),
('UTSI', '66619409', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','1','10','1'),
('UTSI', '66619409', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','2','9','2'),
('UTSI', '66619409', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','3','8','3'),
('UTSI', '66619409', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','4','7','4'),
('UTSI', '66619409', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','5','6','5'),
('UTSI', '66619409', 'PREMOVE', 20, 'ANSWERVALUE','en',now(),'UTSI_SETUP',now(),'UTSI_SETUP','6','5','6');

delete from parametercontrol where parameter='ANSWERVALUE';

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','ANSWERVALUE',20,true,now(),'UTSI_SETUP',now(),'UTSI_SETUP',1,1,'');

ALTER TABLE `redsky`.`commission` MODIFY COLUMN `salesPersonAmount` 
DECIMAL(19,4) DEFAULT '0.00',
  MODIFY COLUMN `consultantAmount` DECIMAL(19,4) DEFAULT '0.00';

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','AATL','Revenue Tracker','3','AATL_SETUP','AATL_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','ADAM','Revenue Tracker','3','ADAM_SETUP','ADAM_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','BONG','Revenue Tracker','3','BONG_SETUP','BONG_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','BOUR','Revenue Tracker','3','BOUR_SETUP','BOUR_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','BRSH','Revenue Tracker','3','BRSH_SETUP','BRSH_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','CWMS','Revenue Tracker','3','CWMS_SETUP','CWMS_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','DECA','Revenue Tracker','3','DECA_SETUP','DECA_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','GERS','Revenue Tracker','3','GERS_SETUP','GERS_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','HIGH','Revenue Tracker','3','HIGH_SETUP','HIGH_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','HOLL','Revenue Tracker','3','HOLL_SETUP','HOLL_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','HSRG','Revenue Tracker','3','HSRG_SETUP','HSRG_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','ICMG','Revenue Tracker','3','ICMG_SETUP','ICMG_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','INTM','Revenue Tracker','3','INTM_SETUP','INTM_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','JOHN','Revenue Tracker','3','JOHN_SETUP','JOHN_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','RSKY','Revenue Tracker','3','RSKY_SETUP','RSKY_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','SSCW','Revenue Tracker','3','SSCW_SETUP','SSCW_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','STAR','Revenue Tracker','3','STAR_SETUP','STAR_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','TSFT','Revenue Tracker','3','TSFT_SETUP','TSFT_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','UGCA','Revenue Tracker','3','UGCA_SETUP','UGCA_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','UGCN','Revenue Tracker','3','UGCN_SETUP','UGCN_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','UGHK','Revenue Tracker','3','UGHK_SETUP','UGHK_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','UGJP','Revenue Tracker','3','UGJP_SETUP','UGJP_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','UGMY','Revenue Tracker','3','UGMY_SETUP','UGMY_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','UGSG','Revenue Tracker','3','UGSG_SETUP','UGSG_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','UGWW','Revenue Tracker','3','UGWW_SETUP','UGWW_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','UTSI','Revenue Tracker','3','UTSI_SETUP','UTSI_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','VOAO','Revenue Tracker','3','VOAO_SETUP','VOAO_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','VOCZ','Revenue Tracker','3','VOCZ_SETUP','VOCZ_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','VOER','Revenue Tracker','3','VOER_SETUP','VOER_SETUP',now(),now());

insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn) values ('Finance','Revenue Tracker','Revenue Tracker','/revenueTracker.html','VORU','Revenue Tracker','3','VORU_SETUP','VORU_SETUP',now(),now());









