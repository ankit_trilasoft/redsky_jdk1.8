<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<style>
 .textarea-comment1 {border:none;color:#000;height:55px; width:200px}
/*.textarea-comment:focus{height:45px; width: 200px;} */
div#content {margin: 0 0 8px;margin-top:0px;}
p{display:none;}
#mainPopup{padding:0px 4px;}
</style>
<head>  
<meta name="heading" content="Tool Tip"/> 
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="float:right;">
<tr valign="top">   
    <td align="left"></td>
    <td align="right"  style="width:88px;">
    <img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
 <s:form action="" name="scopePopupOI">
<table class="notesDetailTable" cellspacing="0" cellpadding="1" border="0" width="150" style="margin:0px;margin-bottom:5px;">    
    <tbody> 
    <tr><td class="subcontent-tab" style="height:14px;padding-left:5px;font-size:12px;background:url(images/basic-inner-blue.png) repeat-x scroll 0 0 #3dafcb;border-bottom: 1px solid #3dafcb;border-top: 1px solid #3dafcb; color: #fff;font-family: arial,verdana;"><font>Comment:</font></td></tr>
    <tr>
    <td valign="top">           
    <table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%;padding-top:0px;margin:0px;" >
    <tbody>
    <tr>
    <td style="width:11px;margin-top:5px;"> </td>
    <td valign="top">
    <%--<s:textarea cssStyle="width:420px;height:150px;resize:vertical;" cssClass="input-text pr-f11" id="scopeOfWorkOrder" value="${scopeOfWorkOrder}" name="scopeOfWorkOrder1"> </s:textarea>  --%>   
     <textarea id="valueOfCommentForRevised" name="revisionComment"  class="textarea-comment1 pr-f11" maxlength="200"><c:out value="${valueOfCommentForRevised}"></c:out></textarea>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
</table>
</s:form >
<c:if test="${flagForCmt !='onlyShow' }">
<input type="button"  class="skybtn" Style="width:auto;padding:2px 4px;font-size:11px;" id="okButton"  value="Save" onclick="saveCmtForRevision('${id }','${commentField}');"/>
<input type="button"  class="skybtn" Style="width:auto;padding:2px 4px;font-size:11px; margin-left:5px;"  value="Cancel" onclick="ajax_hideTooltip();"/>       
</c:if>