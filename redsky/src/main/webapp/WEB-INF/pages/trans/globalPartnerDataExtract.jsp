<%@page import="java.util.List"%>
<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <meta name="heading" content="Partner Extract"/>  
     <title>Extraction Details</title>   
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/calenderStyle.css"%>
	</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
	
	<script language="javascript" type="text/javascript">
		function getSelectedValue(){
			/* var prep = document.getElementById('prepaid');
			if(prep != null && prep != undefined){
				if(document.getElementById('prepaid').checked){
					return true;
				}
			} */
			var childParents='';
			<c:forEach var="childMap" items="${childMap}">
				if(document.getElementById("${childMap.key}").checked == true){
					childParents = "${childMap.value}"+','+childParents;
				}
			</c:forEach>
			if(childParents.length > 0){
			 	childParents = childParents.substring(0, childParents.length-1);
			 	document.getElementById("utsiPartnerExtractForm_childParentIdList").value=childParents;
			 	return validateBlankDt();
			}else{
				alert('Select any Child');
				return false;
			}
		}

		function getSelectAll(str){
			if(str == 'selectAll'){
				document.getElementById("deselectAll").checked=false;
				<c:forEach var="childMap" items="${childMap}">
					document.getElementById("${childMap.key}").checked=true;
				</c:forEach>
			}else if(str == 'deselectAll'){
				document.getElementById("selectAll").checked=false;
				<c:forEach var="childMap" items="${childMap}">
					document.getElementById("${childMap.key}").checked=false;
				</c:forEach>
			}else if(str == 'CHECKALL'){
				document.getElementById("selectAll").checked=false;
				document.getElementById("deselectAll").checked=false;
			}
		}

		function validateReportType(str){
			if(str == 'flatFile'){
				document.getElementById("invoice_Report_from").value='';
			    document.getElementById("invoice_Report_to").value='';
				document.getElementById("claim_Report_from").value='';
				document.getElementById("claim_Report_to").value='';
				document.getElementById("additionalCharges_from").value='';
				document.getElementById("additionalCharges_to").value='';
				document.getElementById("customerFileInitiation_from").value='';
				document.getElementById("customerFileInitiation_to").value='';
				document.getElementById('additionalChargesTable').style.display = 'none';
			}else if(str == 'claimFile'){
				document.getElementById("invoice_Report_from").value='';
			    document.getElementById("invoice_Report_to").value='';
				document.getElementById("flat_Report_from").value='';
				document.getElementById("flat_Report_to").value='';
				document.getElementById("additionalCharges_from").value='';
				document.getElementById("additionalCharges_to").value='';
				document.getElementById("customerFileInitiation_from").value='';
				document.getElementById("customerFileInitiation_to").value='';
				document.getElementById('additionalChargesTable').style.display = 'none';
			}else if(str == 'additionalcharges'){
				document.getElementById("invoice_Report_from").value='';
			    document.getElementById("invoice_Report_to").value='';
				document.getElementById("flat_Report_from").value='';
				document.getElementById("flat_Report_to").value='';
				document.getElementById("claim_Report_from").value='';
				document.getElementById("claim_Report_to").value='';
				document.getElementById('additionalChargesTable').style.display = 'block';
			}else if(str == 'customerFileInitiation'){
				document.getElementById("additionalCharges_from").value='';
				document.getElementById("additionalCharges_to").value='';
			}else if(str == 'accountlineInvoice'){
				document.getElementById("customerFileInitiation_from").value='';
				document.getElementById("customerFileInitiation_to").value='';
			}else if(str == 'prepaid'){
				document.getElementById("invoice_Report_from").value='';
			    document.getElementById("invoice_Report_to").value='';
				document.getElementById("flat_Report_from").value='';
				document.getElementById("flat_Report_to").value='';
				document.getElementById("claim_Report_from").value='';
				document.getElementById("claim_Report_to").value='';
				document.getElementById("additionalCharges_from").value='';
				document.getElementById("additionalCharges_to").value='';
				document.getElementById("customerFileInitiation_from").value='';
				document.getElementById("customerFileInitiation_to").value='';
				document.getElementById('additionalChargesTable').style.display = 'none';
			}else if(str=='invoice'){
				document.getElementById("claim_Report_from").value='';
				document.getElementById("claim_Report_to").value='';
				document.getElementById("flat_Report_from").value='';
				document.getElementById("flat_Report_to").value='';
				document.getElementById("additionalCharges_from").value='';
				document.getElementById("additionalCharges_to").value='';
				document.getElementById("customerFileInitiation_from").value='';
				document.getElementById("customerFileInitiation_to").value='';
				document.getElementById('additionalChargesTable').style.display = 'none';
			}
		}

/* 		function checkDt(fromDt, ToDt){

			   var str1  = 	fromDt;		// document.forms['workTicketDataExtractsForm'].elements['beginDateW'].value;
			   var str2  = 	ToDt;		//document.forms['workTicketDataExtractsForm'].elements['endDateW'].value;
			   var mySplitResult = str1.split("-");
   				var day = mySplitResult[0];
   				var month = mySplitResult[1];
  				 var year = mySplitResult[2];
		  if(month == 'Jan') {
		       month = "01";
		   } else if(month == 'Feb') {
		       month = "02";
		   } else if(month == 'Mar')
		   {
		       month = "03"
		   } else if(month == 'Apr') {
		       month = "04"
		   } else if(month == 'May') {
		       month = "05"
		   } else if(month == 'Jun') {
		       month = "06"
		   }else if(month == 'Jul'){
		       month = "07"
		   }else if(month == 'Aug'){
		       month = "08"
		   } else if(month == 'Sep'){
		       month = "09"
		   }else if(month == 'Oct'){
		       month = "10"
		   } else if(month == 'Nov'){
		       month = "11"
		   } else if(month == 'Dec'){
		       month = "12";
		   }
		   
		   var finalDate = month+"-"+day+"-"+year;
		   
		   var mySplitResult2 = str2.split("-");
		   var day2 = mySplitResult2[0];
		   var month2 = mySplitResult2[1];
		   var year2 = mySplitResult2[2];
		   if(month2 == 'Jan'){
		       month2 = "01";
		   } else if(month2 == 'Feb') {
		       month2 = "02";
		   } else if(month2 == 'Mar') {
		       month2 = "03"
		   } else if(month2 == 'Apr'){
		       month2 = "04"
		   }else if(month2 == 'May'){
		       month2 = "05"
		   }else if(month2 == 'Jun'){
		       month2 = "06"
		   }else if(month2 == 'Jul'){
		       month2 = "07"
		   }else if(month2 == 'Aug'){
		       month2 = "08"
		   }else if(month2 == 'Sep'){
		       month2 = "09"
		   } else if(month2 == 'Oct') {
		       month2 = "10"
		   }else if(month2 == 'Nov'){
		       month2 = "11"
		   }else if(month2 == 'Dec'){
		       month2 = "12";
		   }
  			var finalDate2 = month2+"-"+day2+"-"+year2;
 			 date1 = finalDate.split("-");
  
  			date2 = finalDate2.split("-");
   
  			var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
 			 var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
    		var daysApart = Math.round((sDate-eDate)/86400000);
			   
		   if(daysApart>0) {
		      alert("From date cannot be greater than To date");
		      return false;
		   }
		} */

		function validateBlankDt(){
			for (var i=0; i < document.utsiPartnerExtractForm.reportType.length; i++){
			   if (document.utsiPartnerExtractForm.reportType[i].checked){
			      var rad_val = document.utsiPartnerExtractForm.reportType[i].value;
			      if(rad_val == 'claimFile'){
			    	 var claimFrom = document.getElementById("claim_Report_from").value;
					 var claimTo =	document.getElementById("claim_Report_to").value;
					 if(claimFrom.length == 0 && claimTo.length == 0 ){
						 alert('Enter From & To date for Claim Overview');
						 return false;
					 }else if(claimFrom.length == 0 && claimTo.length > 0){
						 alert('Enter From date for Claim Overview');
						 return false;
					 }else if(claimFrom.length > 0 && claimTo.length == 0){
						 alert('Enter To date for Claim Overview');
						 return false;
					 }else{
						 var daysApart = getDateCompare(claimFrom, claimTo);
						 if(daysApart < 0) {
						      alert("From date cannot be greater than To date");
						      return false;
						 }else{
							 return true;
						 }
						 //return checkDt(claimFrom, claimTo);
					 }
						 
			      }else if(rad_val == "flatFile"){
			    	  var flatFrom = document.getElementById("flat_Report_from").value;
					  var flatTo =	document.getElementById("flat_Report_to").value;
			    	  if(flatFrom.length == 0 && flatTo.length == 0 ){
							 alert('Enter From & To date for Flat file');
							 return false;
						 }else if(flatFrom.length == 0 && flatTo.length > 0){
							 alert('Enter From date for Flat file');
							 return false;
						 }else if(flatFrom.length > 0 && flatTo.length == 0){
							 alert('Enter To date for Flat file');
							 return false;
						 }else{
							 var daysApart = getDateCompare(flatFrom, flatTo);
							 if(daysApart < 0) {
							      alert("From date cannot be greater than To date");
							      return false;
							 }else{
								 return true;
							 }
							 //return checkDt(flatFrom, flatTo);
						 }
			      }else if(rad_val == "additionalCharges"){
			    	  for (var i=0; i < document.utsiPartnerExtractForm.addChargesType.length; i++){
						   if (document.utsiPartnerExtractForm.addChargesType[i].checked){
						      var addCh_val = document.utsiPartnerExtractForm.addChargesType[i].value;
						      if(addCh_val == "accountlineInvoice"){
						    	  var additionalChargesFrom = document.getElementById("additionalCharges_from").value;
								  var additionalChargesTo =	document.getElementById("additionalCharges_to").value;
						    	  if(additionalChargesFrom.length == 0 && additionalChargesTo.length == 0 ){
										 alert('Enter From & To date for Accountline Invoice ');
										 return false;
									 }else if(additionalChargesFrom.length == 0 && additionalChargesTo.length > 0){
										 alert('Enter From date for Accountline Invoice');
										 return false;
									 }else if(additionalChargesFrom.length > 0 && additionalChargesTo.length == 0){
										 alert('Enter To date for Accountline Invoice');
										 return false;
									 }else{
										 var daysApart = getDateCompare(additionalChargesFrom, additionalChargesTo);
										 if(daysApart<0) {
										      alert("From date cannot be greater than To date");
										      return false;
										 }else{
											 return true;
										 }
										 //return checkDt(additionalChargesFrom, additionalChargesTo);
									 }
						      }else if(addCh_val == "customerFileInitiation"){
						    	  var customerFileInitiationFrom = document.getElementById("customerFileInitiation_from").value;
								  var customerFileInitiationTo =	document.getElementById("customerFileInitiation_to").value;
						    	  if(customerFileInitiationFrom.length == 0 && customerFileInitiationTo.length == 0 ){
										 alert('Enter From & To date for Customer File Initiation');
										 return false;
									 }else if(customerFileInitiationFrom.length == 0 && customerFileInitiationTo.length > 0){
										 alert('Enter From date for Customer File Initiation');
										 return false;
									 }else if(customerFileInitiationFrom.length > 0 && customerFileInitiationTo.length == 0){
										 alert('Enter To date for Customer File Initiation');
										 return false;
									 }else{
										 var daysApart = getDateCompare(customerFileInitiationFrom, customerFileInitiationTo);
										 if(daysApart < 0) {
										      alert("From date cannot be greater than To date");
										      return false;
										 }else{
											 return true;
										 }
										 //return checkDt(customerFileInitiationFrom, customerFileInitiationTo);
									 }
						      }
						   }
			    	  }
			      }
			      else if(rad_val == "invoiceFile"){
			    	  var flatFrom = document.getElementById("invoice_Report_from").value;
					  var flatTo =	document.getElementById("invoice_Report_to").value;
			    	  if(flatFrom.length == 0 && flatTo.length == 0 ){
							 alert('Enter From & To date for Costs per invoice');
							 return false;
						 }else if(flatFrom.length == 0 && flatTo.length > 0){
							 alert('Enter From date for Costs per invoice');
							 return false;
						 }else if(flatFrom.length > 0 && flatTo.length == 0){
							 alert('Enter To date for Costs per invoice');
							 return false;
						 }else{
							 var daysApart = getDateCompare(flatFrom, flatTo);
							 if(daysApart < 0) {
							      alert("From date cannot be greater than To date");
							      return false;
							 }else{
								 return true;
							 }
							 //return checkDt(flatFrom, flatTo);
						 }
			      }
			      
			      }
			   }
		}
	</script>

</head>

<c:set var="buttons"> 
    <input type="submit"" class="cssbutton1" onclick=" return getSelectedValue();" value="View Reports" style="width:120px;" tabindex="83"/> 
	 <input type="reset" class="cssbutton1" onclick="getReset();" value="Reset" style="width:60px;" tabindex="83"/>
	
</c:set>

<s:form name="utsiPartnerExtractForm" id="utsiPartnerExtractForm" action="viewGlobalPartnerExtract" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>

<s:hidden name="childParentIdList"/>
      
<div id="Layer5" style="width:100%">
<table class="" cellspacing="0" cellpadding="0" border="0" width="95%" >
<tbody>
<tr>
<td>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
   
<table style="margin-bottom:0px"  border="0">
	<tr><td height="5px"></td></tr>	  
	<tr><td  class="bgblue" >Extract Parameters</td></tr>
	<tr><td height="2px"></td></tr>
	<tr>
		<td align="left" class="listwhitetext" style="padding-left:10px;">
				
		  <fieldset>
		          <legend>Available List</legend>
		          <c:if test="${fn:length(childMap) > 0 }">
		          <table class="pickList11" style="margin:0px">
		          	<tr>
		   				<td  width="22%"><s:checkbox name="selectAll" id="selectAll" value="selectAll" fieldValue="true" tabindex="3" onclick="getSelectAll('selectAll')" /> Select All</td>
		   				<td><s:checkbox name="deselectAll" id="deselectAll" value="deselectAll" fieldValue="true" tabindex="3" onclick="getSelectAll('deselectAll')" /> Deselect All</td>
		          	</tr>
		          	<tr>
		          	<td></td>
		          	<td></td>
		          	</tr>
		          	<tr>
		          	<td colspan="2">
		          	<div id="teste" style="overflow:auto;height:60px;width:400px;">
		          	<c:forEach var="childMap" items="${childMap}">								
					<s:checkbox name="childMap.key" id="${childMap.key}" value="${childMap.value}" fieldValue="true" tabindex="3" onclick="getSelectAll('CHECKALL')" />
	                <s:label title="${childMap.value}" value="${childMap.key}"></s:label><br>
					</c:forEach>					
					</div>
					</td>
					</tr>
		          </table>
		          </c:if>
		          <c:if test="${fn:length(childMap) == 0 }">
			          <table> 
				          <tr>
					          <td class="listwhitetext">Data not available.</td>
				          </tr>
			          </table>
		          </c:if>
		  </fieldset>
		  
		  <fieldset>
		          <legend>Report Type</legend>
		          <table border="0" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">
		          <tr>
		          <td>		          
		          <table class="pickList11"  border="0" cellpadding="0" cellspacing="0">
		          	  <tr>
		          	  	  <td></td>
		                  <td width="30px"></td>
		                  <td></td>
		                  <td align="center" class="listwhitetext">From</td>
		                  <td></td>
		                  <td align="center" class="listwhitetext">To</td>
		           		  <td></td>
		                  
		          	  </tr>
		              <tr>
		                  <td class="listwhitetext"><input type="radio" name="reportType" value="flatFile" checked="checked" onclick="validateReportType('flatFile')"/> Flat File</td>
		                  <td width="30px" colspan="1"></td>
		                  <td class="listwhitetext" style="padding-left:1px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Customer&nbsp;File&nbsp;Created&nbsp;</td>
		                  <td><s:textfield cssClass="input-text" name="flat_Report_from" id="flat_Report_from" readonly="true" size="8" maxlength="12" /></td>
		                  <td>&nbsp;<img id="flat_Report_from-trigger" onclick="setReportTypeRadioButton('reportType','flatFile')" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>&nbsp;&nbsp;</td>
		                  <td><s:textfield cssClass="input-text" name="flat_Report_to"  id="flat_Report_to" readonly="true" size="8" maxlength="12" /></td>
		           		  <td>&nbsp;<img id="flat_Report_to-trigger" onclick="setReportTypeRadioButton('reportType','flatFile')" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		                  
		              </tr>
		             <tr>	          	  	
		                  <td height="5px"></td>
		              </tr>
		              <tr>
		                  <td class="listwhitetext"><input type="radio" name="reportType" value="claimFile" onclick="validateReportType('claimFile')"/> Claim Overview</td>
		                   <td width="30px" colspan="1"></td>
		                  <td class="listwhitetext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Claim&nbsp;Notification&nbsp;Date&nbsp;</td>
		                  <td><s:textfield cssClass="input-text" name="claim_Report_from"  id="claim_Report_from" readonly="true" size="8" maxlength="12" /></td>
		                  <td>&nbsp;<img id="claim_Report_from-trigger" onclick="setReportTypeRadioButton('reportType','claimFile')" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>&nbsp;&nbsp;</td>
		                  <td><s:textfield cssClass="input-text"  name="claim_Report_to" id="claim_Report_to" readonly="true" size="8" maxlength="12" /></td>
		           		  <td>&nbsp;<img id="claim_Report_to-trigger" onclick="setReportTypeRadioButton('reportType','claimFile')" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		                  
		              </tr>
		               <tr>	          	  	
		                  <td height="5px"></td>
		              </tr>
		              <tr>
		                  <td class="listwhitetext"><input type="radio" name="reportType" value="invoiceFile" onclick="validateReportType('invoice')"/> Costs per invoice</td>
		                   <td width="30px" colspan="1"></td>
		                  <td class="listwhitetext" align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invoice&nbsp;&nbsp;Date&nbsp;</td>
		                  <td><s:textfield cssClass="input-text" name="invoice_Report_from"  id="invoice_Report_from" readonly="true" size="8" maxlength="12" /></td>
		                  <td>&nbsp;<img id="invoice_Report_from-trigger" onclick="setReportTypeRadioButton('reportType','invoiceFile')" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>&nbsp;&nbsp;</td>
		                  <td><s:textfield cssClass="input-text"  name="invoice_Report_to" id="invoice_Report_to" readonly="true" size="8" maxlength="12" /></td>
		           		  <td>&nbsp;<img id="invoice_Report_to-trigger" onclick="setReportTypeRadioButton('reportType','invoiceFile')" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		                  
		              </tr>
		                <tr>	          	  	
		                  <td height="5px"></td>
		              </tr>
		              <configByCorp:fieldVisibility componentId="component.field.globalPartnerExtracts.visibility">
					<tr >
					<td class="listwhitetext" valign=""><input type="radio" name="reportType" value="additionalCharges" onclick="validateReportType('additionalcharges')"/> Additional Charges</td>
		                   <td width="30px"></td>
		                   <td colspan="5">
							<table id="additionalChargesTable" style="display: none; margin:0px;padding:0px;" cellpadding="0" cellspacing="0">
							<tr>
				                  <td class="listwhitetext" nowrap="nowrap"><input type="radio" checked="checked" name="addChargesType" style="margin:0px;vertical-align:top;" value="accountlineInvoice" onclick="validateReportType('accountlineInvoice')"/>&nbsp;Accountline Invoice&nbsp;</td>
				                  <td><s:textfield cssClass="input-text" name="additionalCharges_from"  id="additionalCharges_from" readonly="true" size="8" maxlength="12" /></td>
				                  <td>&nbsp;<img id="additionalCharges_from-trigger" onclick="setReportTypeRadioButton('addChargesType','accountlineInvoice')" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>&nbsp;&nbsp;</td>
				                  <td><s:textfield cssClass="input-text"  name="additionalCharges_to" id="additionalCharges_to" readonly="true" size="8" maxlength="12" /></td>
				           		  <td>&nbsp;<img id="additionalCharges_to-trigger" onclick="setReportTypeRadioButton('addChargesType','accountlineInvoice')" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			           	  </tr>
			           	   <tr>	          	  	
		                  <td height="5px"></td>
		              </tr>
			           	  <tr>
				           		  <td class="listwhitetext" nowrap="nowrap"><input type="radio" name="addChargesType" style="margin:0px;vertical-align:top;" value="customerFileInitiation" onclick="validateReportType('customerFileInitiation')"/>&nbsp;Customer File Initiation&nbsp;</td>
				                  <td><s:textfield cssClass="input-text" name="customerFileInitiation_from"  id="customerFileInitiation_from" readonly="true" size="8" maxlength="12" /></td>
				                  <td>&nbsp;<img id="customerFileInitiation_from-trigger" onclick="setReportTypeRadioButton('addChargesType','customerFileInitiation')" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>&nbsp;&nbsp;</td>
				                  <td><s:textfield cssClass="input-text"  name="customerFileInitiation_to" id="customerFileInitiation_to" readonly="true" size="8" maxlength="12" /></td>
				           		  <td>&nbsp;<img id="customerFileInitiation_to-trigger" onclick="setReportTypeRadioButton('addChargesType','customerFileInitiation')" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				           		</tr>
			           		  </table>
			           		  </td>
		            </tr>
		              
		              <tr>	          	  	
		                  <td height="5px"></td>
		              </tr>
		              <%-- <tr>
		                  <td class="listwhitetext"><input type="radio" name="reportType" value="prepaid" onclick="validateReportType('prepaid')"/> Prepaid Services</td>
		                   <td width="30px"></td>
		                  <td class="listwhitetext">Customer File Initiation </td>
		                  <td><s:textfield cssClass="input-text" name="prepaid_from"  id="prepaid_from" readonly="true" size="8" maxlength="12" /></td>
		                  <td><img id="prepaid_from-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		                  <td><s:textfield cssClass="input-text"  name="prepaid_to" id="prepaid_to" readonly="true" size="8" maxlength="12" /></td>
		           		  <td><img id="prepaid_to-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		                  
		              </tr> --%>
		              <tr>
		                  <td class="listwhitetext"><input type="radio"  id="prepaid" name="reportType" value="prepaid" onclick="validateReportType('prepaid')"/> Prepaid Services</td>
		              </tr>
		              </configByCorp:fieldVisibility>
		          </table>
		          </td>
		          </tr>
		          </table>
		  </fieldset>
		</td>
	</tr>
	<tr>
		<td align="center" class="listwhitetext" colspan="5" valign="bottom" style="">						
		<c:out value="${buttons}" escapeXml="false" />					
	</td>
	</tr>
</table>

<c:if test="${fn:length(qltyServeyResMap) > 0 }">
	<table style="margin-bottom:0px"  border="0" id="qserveyRes">
		<tr><td height="5px"></td></tr>	  
		<tr><td  class="bgblue" >Quality Survey Results</td></tr>
		<tr height="2px"></tr>
		<tr>
			<td align="left" colspan="0" width="">
				<c:forEach var="qltyServeyResMap" items="${qltyServeyResMap}">
					<input type="button" class="cssbutton1" onclick="window.open('http://${qltyServeyResMap.value}')" value="${qltyServeyResMap.key}" style="width:150px; " tabindex="83"/> 
				</c:forEach>
			</td>
		</tr>
	</table>
</c:if>

</div>
<div class="bottom-header" style="margin-top:50px;"><span></span></div>
</div>
</div> 

</td>
</tr>
</tbody>
</table> 

</div> 

</s:form> 
<div id="mydiv" style="position:absolute;top:0px;margin-top:-47px;"></div>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
function setReportTypeRadioButton(radioGrp,radioBtnId){
	validateReportType(radioBtnId);
	 var radioGroup = document['utsiPartnerExtractForm'][radioGrp];
	 for (var i=0; i<radioGroup.length; i++){
	    if(radioGroup[i].value == radioBtnId){
	    	radioGroup[i].checked=true;
	    }
	 }
}

function getReset(){
	setTimeout("validateReportType('flatFile')",100); 
}

</script>