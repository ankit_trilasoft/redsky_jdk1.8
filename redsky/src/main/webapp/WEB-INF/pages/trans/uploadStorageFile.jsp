<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
  
<head>   
    <title>Storage Library Update</title>   
    <meta name="heading" content="Storage Library Update"/>  
    <style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script>
function progressBar(tar){
showOrHide(tar);
}

function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["layerH"].visibility='hide';
        else
           document.getElementById("layerH").style.visibility='hidden';
   }
   else if (value==1) {
   try{
   document.getElementById("successMessages").style.visibility='hidden'; 
   }catch (e) { 
   }
      if (document.layers)
          document.layers["layerH"].visibility='show';
       else
          document.getElementById("layerH").style.visibility='visible';
   }
}
function payablesValidateUpload(){
	var fileValue = document.forms['locationUpdateForm'].elements['file'].value; 
	if(fileValue==null || fileValue==''){
		alert("Please select file to process.");
		return false;
	}
			
	else{ 
		var extension=fileValue.substring(fileValue.indexOf('.')+1, fileValue.length);
			if(extension=='xls'){
				progressBar('1');
				return true;
       		}else{
         		alert( "Please select a valid file format.xls are only allowed" ) ;
         		return false;
         	}
	}
} 


var basestring="window"+new Date().getTime();

function zfill(n){ //this function to fill in zeros so there will always be 2 digits for time display
n=n+'';
while(n.length<2)n="0"+n;
return n;
}

function writeWindow(){
var n=new Date();
var x=window.open('', 'newWin'+n.getTime(), "width=300,height=100");
var txt='';
txt+='<html>';
txt+='<head><title>A dynamic page</title></head>';
txt+='<body>';
txt+='This content was written to the window dynamically. ';
txt+='The time is '+zfill(n.getHours())+':'+zfill(n.getMinutes())+':'+zfill(n.getSeconds());
txt+='</body>';
txt+='</html>';
x.document.write(txt);
x.focus();
}

function openWindow(url,w,h,tb,stb,l,mb,sb,rs,x,y){
var pos=(document.layers)? ',screenX='+x+',screenY='+y: ',left='+x+',top='+y;
tb=(tb)?'yes':'no';
stb=(stb)?'yes':'no';
l=(l)?'yes':'no';
mb=(mb)?'yes':'no';
sb=(sb)?'yes':'no';
rs=(rs)?'yes':'no';
var txt='';
txt+='scrollbars='+sb;
txt+=',width='+w;
txt+=',height='+h;
txt+=',toolbar='+tb;
txt+=',status='+stb;
txt+=',menubar='+mb;
txt+=',links='+l;
txt+=',resizable='+rs;
var x=window.open(url, 'newWin'+new Date().getTime(), txt+pos);
x.focus();
}


</script>

<style type="text/css">
/* collapse */
div.error, span.error, li.error, div.message {
width:800px;
}
</style>
</head> 
 <DIV ID="layerH">
 <table border="0" width="100%" align="middle">
 <tr>
 <td align="center">
	<font size="4" color="#1666C9"><b><blink>Uploading...</blink></b></font>
	</td>
	</tr>
	</table>
</DIV> 
<s:form cssClass="form_magn" id="locationUpdateForm" name="locationUpdateForm" enctype="multipart/form-data" action="uploadStorageFile" method="post" validate="true">   
 <div id="Layer3" style="width:90%; margin:0px; padding:0px">
 <div id="newmnav">
 <ul>
 <li id="newmnav1" style="background:#FFF "><a class="current" ><span>Storage Library Update<img src="images/navarrow.gif" align="absmiddle"/></span></a></li> 
 <li><a href="storageLibraries.html"><span>Storage Library List</span></a></li>
 	</ul> 
</div>
		<div class="spn">&nbsp;</div> 
        <div id="content" align="center">
        <div id="liquid-round">
        <div class="top" style="margin-top:0px;!margin-top:5px;"><span></span></div>
        <div class="center-content"> 
				<table cellspacing="1" cellpadding="2" border="0" style="margin:0px; ">
					<tbody>  
						<tr>
							<td align="right" class="listwhitetext" width="30px"></td>
							<td align="right" class="listwhitetext">File To Upload <img src="${pageContext.request.contextPath}/images/external.png" style="cursor:auto;"/></td>
							<td colspan="4"> <s:file name="file" size="25"/></td>
							<td align="right" class="listwhitetext strbg" width="130px"><a href="${pageContext.request.contextPath}/images/Sample_Upload_File_For_StorageLibrary.xls" >Sample Template </a></td>
						</tr> 
						<tr><td height="20px"></td></tr>  
						<tr>
						<td align="right" class="listwhitetext" width="30px"></td>						
						<td align="right" colspan="2">
						<s:submit cssClass="cssbutton1" name="payables" value="Process" cssStyle="width:55px; height:27px" onclick="return payablesValidateUpload();"/>
                        </td>
                        <td>
                        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:27px" key="Reset"/></td>                       
                        </tr>
                        <tr><td height="10px"></td></tr>  
					</tbody>
				</table> 
	    </div>
       <div class="bottom-header"><span></span></div>
       </div>
       </div>
       </div> 
</s:form>

<script type="text/javascript">  
 showOrHide(0);
</script>