package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.ResourceContractCharges;

public interface ResourceContractChargeManager extends GenericManager<ResourceContractCharges, Long> {
	
	public List findResourceByPartnerId(String corpId,Long partnerId);
	public void deleteAll(Long parentId);
	
	public List createPrice(String corpId,String contract,String shipNumber,Long serviceOrderId,String divisionFlag);
	
}
