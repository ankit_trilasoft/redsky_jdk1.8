package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ResourceContractCharges;
import com.trilasoft.app.model.ResourceGrid;

public interface ResourceGridDao extends GenericDao<ResourceGrid, Long>{

	public List findResource(String hub, Date workDate,String sessionCorpID);	
	public List getListById(Long id, String sessionCorpID);
}
