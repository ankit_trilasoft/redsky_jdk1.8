
package com.trilasoft.app.model;
import com.opensymphony.xwork2.ActionSupport;
import java.util.*;


/**
 * <p> Validate a user login. </p>
 */
public  class DateBean  extends ActionSupport {

	 private Date todayDate;

    public String execute() throws Exception {

      setTodayDate(new Date());
      return SUCCESS;

  }


    // ---- Username property ----

    /**
     * <p>Field to store Today's Date.</p>
     * <p/>
     */
   


    /**
     * <p>Provide Today's Date.</p>
     *
     * @return Returns the Todays date.
     */
    public Date getTodayDate() {
        return todayDate;
    }

    /**
     * <p>Store new Date</p>
     *
     * @param value The username to set.
     */
    public void setTodayDate(Date value) {
        todayDate = value;
    }

}