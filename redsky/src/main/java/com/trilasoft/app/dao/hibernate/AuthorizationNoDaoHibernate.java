package com.trilasoft.app.dao.hibernate;


import java.util.HashSet;
import java.util.List;
import java.util.Set;


import java.util.List;


import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.AuthorizationNoDao;
import com.trilasoft.app.model.AuthorizationNo;
import com.trilasoft.app.model.WorkTicket;

public class AuthorizationNoDaoHibernate extends GenericDaoHibernate<AuthorizationNo, Long> implements AuthorizationNoDao{

	public AuthorizationNoDaoHibernate() {
		super(AuthorizationNo.class);
	}

	public List findByJobAuth(String authNo,  String shipNumber){
    	/*List an = getHibernateTemplate().find("from AuthorizationNo where shipNumber=?", shipNumber);
    	Set<AuthorizationNo> ans = new HashSet(an);
    	return ans;*/
		return getHibernateTemplate().find("from AuthorizationNo where shipNumber=?", shipNumber);
    }


	public List searchAccAuthorization(String shipNumber) {
		
		return getHibernateTemplate().find("from AuthorizationNo where shipNumber=?",shipNumber);
	}
	public List findMaximum(String shipNumber) {
    	return getHibernateTemplate().find("select max(authorizationSequenceNumber) from AuthorizationNo where shipNumber=?", shipNumber);
    }

	public List searchAuthorizationNo(String shipNumber) {
		
		return getHibernateTemplate().find("from AuthorizationNo where invoiceNumber ='' and invoiceDate is null and shipNumber=?",shipNumber);
	}

	public List findAuthorizationNo(String authNoInvoice) {
		
		return getHibernateTemplate().find("select authorizationNumber from AuthorizationNo where invoiceNumber ='' and invoiceDate is null and authorizationNumber=?",authNoInvoice);
	}
	
	public List findAuthNumber(String shipNumber, String sessionCorpID) {
	   List auths = this.getSession().createSQLQuery("select left(authorizationnumber,length(authorizationnumber)-2), right(authorizationnumber,2) from authorizationno where id = (select max(id) from authorizationno where corpid='"+sessionCorpID+"' and shipnumber = '"+shipNumber+"') ").list();
	   return auths;
	}
}
