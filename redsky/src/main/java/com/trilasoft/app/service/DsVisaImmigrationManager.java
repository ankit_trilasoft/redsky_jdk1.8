package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;


import com.trilasoft.app.model.DsVisaImmigration;

public interface DsVisaImmigrationManager extends GenericManager<DsVisaImmigration, Long>{

	List getListById(Long id);

}
