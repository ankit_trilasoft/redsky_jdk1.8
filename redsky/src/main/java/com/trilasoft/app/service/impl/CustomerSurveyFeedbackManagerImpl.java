package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CustomerSurveyFeedbackDao;
import com.trilasoft.app.model.CustomerSurveyFeedback;
import com.trilasoft.app.service.CustomerSurveyFeedbackManager;

public class CustomerSurveyFeedbackManagerImpl extends GenericManagerImpl<CustomerSurveyFeedback, Long> implements CustomerSurveyFeedbackManager {
	CustomerSurveyFeedbackDao customerSurveyFeedbackDao;
	public CustomerSurveyFeedbackManagerImpl(CustomerSurveyFeedbackDao customerSurveyFeedbackDao){
		super(customerSurveyFeedbackDao);
		this.customerSurveyFeedbackDao=customerSurveyFeedbackDao;
	}
}
