package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.AdAddressesDetails; 


public interface AdAddressesDetailsDao extends GenericDao<AdAddressesDetails, Long>{
	public List getAdAddressesDetailsList(Long id, String sessionCorpID);
	public Map<String, String> getAdAddressesDropDown(Long id, String sessionCorpID,String type,String jobType);
	public Long findRemoteAdAddressesDetails(Long networkId, Long id);
	public int updateAdAddressesDetailsNetworkId(Long id);
	public AdAddressesDetails getFirstAddressDetail(String sessionCorpID,Long id, String adType);
}
