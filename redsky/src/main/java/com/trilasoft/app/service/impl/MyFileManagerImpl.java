package com.trilasoft.app.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.appfuse.Constants;
import org.appfuse.service.impl.GenericManagerImpl;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.trilasoft.app.dao.MyFileDao;
import com.trilasoft.app.dao.hibernate.util.FileSizeUtil;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.service.MyFileManager;

public class MyFileManagerImpl extends GenericManagerImpl<MyFile, Long> implements MyFileManager {   
	MyFileDao myFileDao; 
	
	private PdfImageExtractor pdfImageExtractor;
	  
	public PdfImageExtractor getPdfImageExtractor() {
		return pdfImageExtractor;
	}

	public void setPdfImageExtractor(PdfImageExtractor pdfImageExtractor) {
		this.pdfImageExtractor = pdfImageExtractor;
	}

	  
    public MyFileManagerImpl(MyFileDao myFileDao) {   
        super(myFileDao);   
        this.myFileDao = myFileDao;   
    } 
	
    public List<MyFile> findByDocs( String fileType, String fileId) {   
        return myFileDao.findByDocs( fileType, fileId);   
    }
    public List findByDocs(String fileType, String fileId,String sessionCorpID){
    	return myFileDao.findByDocs(fileType, fileId, sessionCorpID);
    }
   
   public List<MyFile> findByReleventDocs( String fileType, String fileId){
	   return myFileDao.findByReleventDocs( fileType, fileId);   
   }
   
   public List getListByDocId(String fileId , String active, String secure ,String categorySearchValue){
	   return myFileDao.getListByDocId(fileId, active, secure,categorySearchValue);
   }
	public List getListByCustomerNumberForTicket(String sequenceNumber,String shipNumber, String active){
		return myFileDao.getListByCustomerNumberForTicket(sequenceNumber, shipNumber, active);
	}
   public List getListByCustomerNumber(String customerNumber, String active){
	   return myFileDao.getListByCustomerNumber(customerNumber, active);
   }
   public List getListByDesc(Long id){
	   return myFileDao.getListByDesc(id);
   }
   public List getMaxId(String corpid){
	   return myFileDao.getMaxId(corpid);
   }
   public int updateMyfileStatus(String Status,Long ids, String updatedBy){
	   return myFileDao.updateMyfileStatus(Status,ids,updatedBy);
   }

   public int updateMyfileAccStatus(String Status,Long ids, String updatedBy){
	   return myFileDao.updateMyfileAccStatus(Status, ids,updatedBy);
   }

   public List getAccountFiles(String seqNum, String sessionCorpID) {
	   return myFileDao.getAccountFiles(seqNum, sessionCorpID);
   }
   
   public int updateMyfilePartnerStatus(String status, Long ids, String updatedBy){
	   return myFileDao.updateMyfilePartnerStatus(status, ids,updatedBy);
   }
   
   public List searchMyFiles(String fileType, String description, String fileId, String updatedBy, Date updatedOn, String sessionCorpID, String index,String duplicates){
	   return myFileDao.searchMyFiles(fileType, description, fileId, updatedBy, updatedOn, sessionCorpID, index,duplicates);
   }
   
   public List getIndexedList(String sessionCorpID, String updatedBy){
	   return myFileDao.getIndexedList(sessionCorpID, updatedBy);
   }
   public List getDocAccessControl(String fileType,String corpID){
	   return myFileDao.getDocAccessControl(fileType,corpID);
   }
   
   public void stripPages(int[] stripPageIndex, MyFile inFileDocument) {
		String inFileName = inFileDocument.getLocation();
		if (inFileName.endsWith("pdf") || inFileDocument.getFileContentType().equals("application/pdf")){ 
			String inFileCopyName = inFileName.substring(0, inFileName.lastIndexOf("."))+ "_copy.pdf";
	
			try {
				copyFile(inFileName, inFileCopyName);
				(new File(inFileName)).delete();
	
				PdfReader reader = new PdfReader(inFileCopyName);
	
				Document document = new Document(reader.getPageSize(1), 50, 50, 50, 50);
	
				PdfCopy writer = new PdfCopy(document, new FileOutputStream(inFileName));
				document.open();
	
				for (int i = 0; i < reader.getNumberOfPages(); i++) {
					boolean stripPage = false;
	
					for (int stripPageNum : stripPageIndex)
						if (i == stripPageNum) {
							stripPage = true;
							break;
						}
					if (!stripPage) {
						PdfImportedPage pageCopy = writer.getImportedPage(reader, i+1);
						writer.addPage(pageCopy);
					}
				}
				document.close();
			} catch (Exception ex) {
				if (!((new File(inFileName)).exists()))
					try {
						copyFile(inFileCopyName, inFileName);
					} catch (IOException e) {
						e.printStackTrace();
					}
				ex.printStackTrace();
			}
		}
	}
   
   	public void splitPages(int splitIndex, MyFile inFileDocument, MyFile outFileDocument,String corpid) {
		String inFileName = inFileDocument.getLocation();
		if (inFileName.endsWith("pdf") || inFileDocument.getFileContentType().equals("application/pdf")){ 
	
			String inFileCopyName = inFileName.substring(0, inFileName.lastIndexOf("."))+ "_copy.pdf";
	
			PdfCopy origModifiedDocWriter = null;
			PdfCopy newSplitDocWriter = null;
			Document origModifiedDoc = null;
			Document newSplitDoc = null;
			try {
				copyFile(inFileName, inFileCopyName);
				(new File(inFileName)).delete();
	
				PdfReader reader = new PdfReader(inFileCopyName);
	
				origModifiedDoc = new Document(reader.getPageSize(1), 50, 50, 50, 50);
				newSplitDoc = new Document(reader.getPageSize(1), 50, 50, 50, 50);
	
				origModifiedDocWriter = new PdfCopy(origModifiedDoc, new FileOutputStream(inFileName));
				origModifiedDoc.open();

				Long lastId = Long.parseLong(myFileDao.getMaxId(corpid).get(0).toString()) ;
				int fileNameIdx = inFileName.lastIndexOf("_") < inFileName.lastIndexOf(Constants.FILE_SEP)?inFileName.lastIndexOf(Constants.FILE_SEP):inFileName.lastIndexOf("_");
				String splitFilePostfix = inFileName.substring(fileNameIdx+1);
				String splitFilePath = inFileName.substring(0, inFileName.lastIndexOf(Constants.FILE_SEP));
				String splitFileName = (lastId + 1) + "_" + inFileDocument.getFileId() + "_" + splitFilePostfix;

				newSplitDocWriter = new PdfCopy(newSplitDoc, new FileOutputStream(splitFilePath + Constants.FILE_SEP + splitFileName));
				newSplitDoc.open();

				for (int i = 0; i < reader.getNumberOfPages(); i++) {
				
					if (i <= splitIndex ) {
						PdfImportedPage pageCopy = newSplitDocWriter.getImportedPage(reader, i+1);
						newSplitDocWriter.addPage(pageCopy);
					} else {
						PdfImportedPage pageCopy = origModifiedDocWriter.getImportedPage(reader, i+1);
						origModifiedDocWriter.addPage(pageCopy);
					}
				}
				
				newSplitDoc.close();
				
				outFileDocument.setLocation(splitFilePath + Constants.FILE_SEP + splitFileName);
				outFileDocument.setFileFileName(splitFileName);
				
				FileSizeUtil fileSizeUtil = new FileSizeUtil(); 
				File newPDFFile = new File(splitFilePath + Constants.FILE_SEP + splitFileName);
				outFileDocument.setFileSize(fileSizeUtil.FindFileSize(newPDFFile));
				
				save(outFileDocument);
				
				if (reader.getNumberOfPages() != (splitIndex +1)) {
					origModifiedDoc.close();
				}
				
			} catch (Exception ex) {
				ex.printStackTrace();
				if (newSplitDocWriter != null && newSplitDoc.isOpen()){
					newSplitDoc = null;
					newSplitDocWriter.close();
				}
				if (origModifiedDocWriter != null && origModifiedDoc.isOpen()){
					origModifiedDoc = null;
					origModifiedDocWriter.close();
				}
				
				if (((new File(inFileName)).exists())) {
					(new File(inFileName)).delete();
				}
				try {
					copyFile(inFileCopyName, inFileName);
					} catch (IOException e) {
						e.printStackTrace();
					}
				ex.printStackTrace();
			}
		}
	}

	public void copyFile(String inFileName, String inFileCopyName) throws IOException {
		File inFile = new File(inFileName);
		File inFileCopy = new File(inFileCopyName);

		FileInputStream from = null;
		FileOutputStream to = null;
		try {
			from = new FileInputStream(inFile);
			to = new FileOutputStream(inFileCopy);
			byte[] buffer = new byte[4096];
			int bytesRead;

			while ((bytesRead = from.read(buffer)) != -1)
				to.write(buffer, 0, bytesRead); // write
		} finally {
			if (from != null)
				from.close();
			if (to != null)
				to.close();
		}
	}
	
	public void updateMyfileStripped(Long id){
		myFileDao.updateMyfileStripped(id);
	}
	
	public List findSOName(String fileId, String sessionCorpID){
		return myFileDao.findSOName(fileId, sessionCorpID);
	}
	
	public String getImageList(Long fileId, int pageCount) {
		StringBuilder imageJson = new StringBuilder("{ items: [") ;
		MyFile file = null;
		try {
			file = myFileDao.get(fileId);
			String fileLocation = file.getLocation();
			String[] extractImageArry = {fileLocation};
			List<String> imageList = pdfImageExtractor.extractImages(extractImageArry ,pageCount);
			
			for (int i = 0; i < imageList.size(); i++ ) {
				String imagefilePath = imageList.get(i); 
				imageJson.append("{");
				imageJson.append("\"thumb\":\"getImage.html?imageName=").append(imagefilePath.substring(imagefilePath.lastIndexOf(Constants.FILE_SEP)+1)).append("\",");
				imageJson.append("\"large\":\"getImage.html?imageName=").append(imagefilePath.substring(imagefilePath.lastIndexOf(Constants.FILE_SEP)+1)).append("\",");
				imageJson.append("\"title\":\"Page ").append(i+1).append("\"");
				imageJson.append("}").append(i==(imageList.size()-1)?"":",");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		imageJson.append("]}");
		return imageJson.toString();
		
	}

	public String getImageLocation(String imageName) {
		return pdfImageExtractor.getTempFolderPath() + Constants.FILE_SEP + imageName;
	}

	public void updateActive(Long id, String updatedBy) {
		myFileDao.updateActive(id,updatedBy);
	}

	public List getBasketList(Long fileId) {
		return myFileDao.getBasketList(fileId);
	}

	public void recoverDoc(Long id, String updatedBy) {
		myFileDao.recoverDoc(id,updatedBy);
	}

	public List getSequanceNum(String fileId, String sessionCorpID) {
		return myFileDao.getSequanceNum(fileId, sessionCorpID);
	}
	
	public List getSequanceNumByInvNum(String fileId, String sessionCorpID){
		return myFileDao.getSequanceNumByInvNum(fileId, sessionCorpID);
	}
	public List getSequanceNumByClm(String fileId, String sessionCorpID){
		return myFileDao.getSequanceNumByClm(fileId, sessionCorpID);
	}
	public List getSequanceNumByTkt(String fileId, String sessionCorpID){
		return myFileDao.getSequanceNumByTkt(fileId, sessionCorpID);
	}
	public OutputStream concatPDFs(List<InputStream> streamOfPDFFiles, OutputStream outputStream, boolean paginate) {
		
		Document document = new Document();
		PdfCopy newDocWriter = null;
		PdfImportedPage pageCopy = null;
		try {
			List<InputStream> pdfs = streamOfPDFFiles;
			List<PdfReader> readers = new ArrayList<PdfReader>();
			PdfReader pdfReader = null;
			Iterator<InputStream> iteratorPDFs = pdfs.iterator();

			while(iteratorPDFs.hasNext()) {
				InputStream pdf = iteratorPDFs.next();
				pdfReader = new PdfReader(pdf);
				document = new Document(pdfReader.getPageSize(1), 50, 50, 50, 50);
				readers.add(pdfReader);
			}
			
			newDocWriter = new PdfCopy(document, outputStream);
			document.open();
			
			Iterator<PdfReader> iteratorPDFReader = readers.iterator();
			while (iteratorPDFReader.hasNext()) {
				PdfReader PDFReader = iteratorPDFReader.next();
				for (int i = 0; i < PDFReader.getNumberOfPages(); i++) {
					pageCopy = newDocWriter.getImportedPage(PDFReader, i+1);
					newDocWriter.addPage(pageCopy);
				}
			}
			
			outputStream.flush();
			document.close();
			outputStream.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (document.isOpen())
				document.close();
			try {
				if (outputStream != null)
					outputStream.close();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
		return outputStream;
	}

	public void getUpdateTransDate(String id,String updatedBy) {
		myFileDao.getUpdateTransDate(id,updatedBy);
	}
	
	public List getMapFolder(String fileType,String corpID){
		return myFileDao.getMapFolder(fileType, corpID);
	}
	
	public void updateFileSize(String updateSize, Long id){
		myFileDao.updateFileSize(updateSize, id);
	}

	public List getIsSecure(String fileType, String sessionCorpID) {
		return myFileDao.getIsSecure(fileType, sessionCorpID);
	}
	public List getWasteBasketAll(String corpId){
		return myFileDao.getWasteBasketAll(corpId) ;
	}
	public List searchWasteFiles(String fileType, String description,String fileId,String updatedBy, Date updatedOn, String sessionCorpID){
		return myFileDao.searchWasteFiles(fileType, description,fileId, updatedBy, updatedOn, sessionCorpID);
	}

	public List findAccountPortalFileList(String sequenceNumber,String sessionCorpID) {
		return myFileDao.findAccountPortalFileList(sequenceNumber,sessionCorpID);
		
	}
	public List getAllMyfile(String fileId, String sessionCorpID){
		return myFileDao.getAllMyfile(fileId,sessionCorpID);
	}
	public List getMyFileList(String linkFile, String sessionCorpID){
		return myFileDao.getMyFileList(linkFile, sessionCorpID);
	}

	public List getLinkedShipNumber(String shipNumber, String type, Boolean isBookingAgent, Boolean isNetworkAgent,	Boolean isOriginAgent, Boolean isSubOriginAgent,Boolean isDestAgent, Boolean isSubDestAgent) {
		return myFileDao.getLinkedShipNumber(shipNumber, type, isBookingAgent, isNetworkAgent, isOriginAgent, isSubOriginAgent, isDestAgent, isSubDestAgent);
	}

	public List getParentShipNumber(String shipNumber) {
		return myFileDao.getParentShipNumber(shipNumber);
	}
	public String getAccountLinePayingStatus(String accountlineShipNumber,String accountlineVendorCode,String sessionCorpID){
		return myFileDao.getAccountLinePayingStatus(accountlineShipNumber,accountlineVendorCode,sessionCorpID);
	}
	public void upDateNetworkLinkId(String networkLinkId, Long id) {	
		myFileDao.upDateNetworkLinkId(networkLinkId, id);
	}
	public List findlinkedIdList(String networkLinkId){
		return myFileDao.findlinkedIdList(networkLinkId);
	}
	public List findMyFilePortalList(String extSO, String sessionCorpID){
		return myFileDao.findMyFilePortalList(extSO, sessionCorpID);
	}
	public void removeNetworkLinkId(String networkLinkId, String fileId,String sessionCorpID){
		myFileDao.removeNetworkLinkId(networkLinkId, fileId, sessionCorpID);
	}
	public List getLinkedAllShipNumber(String shipNumber, String checkFlagBA,String checkFlagNA, String checkFlagOA, String checkFlagSOA,String checkFlagDA, String checkFlagSDA, String type){
		return myFileDao.getLinkedAllShipNumber(shipNumber, checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA, checkFlagDA, checkFlagSDA, type);
	}
	public List getDefCheckedShipNumber(String shipNumber){
		return myFileDao.getDefCheckedShipNumber(shipNumber);
	}
	public void updateAgentFlag(String networkLinkId, String checkAgent){
		myFileDao.updateAgentFlag(networkLinkId, checkAgent);
	}
	public List getShipNumberList(String shipNumber){
		return myFileDao.getShipNumberList(shipNumber);
	}
	public List findTransDocSystemDefault(String corpID){
		return myFileDao.findTransDocSystemDefault(corpID);
	}
	public List searchMaxSizeFiles(String fileType, String description, String fileId, String updatedBy, Date updatedOn, String sessionCorpID, String docName, Integer docSize,String index){
		return myFileDao.searchMaxSizeFiles(fileType, description, fileId, updatedBy, updatedOn, sessionCorpID, docName, docSize,index);
	}
	public List getcompressListId(Long fid){
		return myFileDao.getcompressListId(fid);
	}
	public List findShipNumberBySequenceNumer(String sequenceNumber,String sessionCorpID,Boolean isNetworkRecord){
		return myFileDao.findShipNumberBySequenceNumer(sequenceNumber,sessionCorpID,isNetworkRecord);
	}
	public void updateNetworkLinkIdCheck(String networkLinkId, String linkedSeqNum,String valueTarget,String FileId,Long id,String checkValue){
		 myFileDao.updateNetworkLinkIdCheck(networkLinkId ,linkedSeqNum,valueTarget,FileId, id,checkValue);
	}
	public List getLinkedId(String networkLinkId, String sessionCorpID){
		return myFileDao.getLinkedId(networkLinkId, sessionCorpID );
	}
	public TreeMap<String, List<MyFile>> getdocTypeMapList(String shipNumber,String active, String secure, String categorySearchValue,String tempSortOrder,String sortingOrder) {
		return myFileDao.getdocTypeMapList(shipNumber, active, secure, categorySearchValue,tempSortOrder,sortingOrder);
	}
	public TreeMap<String, List<MyFile>> getListByCustomerNumberGrpByType(String sequenceNumber, String active,String tempSortOrder,String sortingOrder){
		return myFileDao.getListByCustomerNumberGrpByType(sequenceNumber, active, tempSortOrder,sortingOrder);
	}
	public int updateMyfileServiceProviderStatus(String Status,Long ids, String updatedBy){
		   return myFileDao.updateMyfileServiceProviderStatus(Status,ids,updatedBy);
	   }
	public List recipientWithEmailStatus(String jobNumber,String noteFor ,String sessionCorpID,String fileId) {
		return myFileDao.recipientWithEmailStatus(jobNumber,noteFor,sessionCorpID,fileId);
	}
	public List fileTypeForPartnerCode(String corpID)
	{
		return myFileDao.fileTypeForPartnerCode(corpID);
	}
	public Long findByLocation(String location,String sessionCorpID){
		return myFileDao.findByLocation(location, sessionCorpID);
	}
	public int updateInvoiceAttachment(String Status,Long ids, String updatedBy){
		   return myFileDao.updateInvoiceAttachment(Status, ids,updatedBy);
	   }

	public List accountPortalFiles(String securityQuery, String corpID) { 
		return myFileDao.accountPortalFiles(securityQuery, corpID);
	}
	//13674 - Create dashboard
	public List getDocumentCategoryList(String shipNumber, String documentCategory ,String sessionCorpID) {
		
		return myFileDao.getDocumentCategoryList(shipNumber, documentCategory,sessionCorpID);
	}

	public List getListByDesc(String shipNumber,String sessionCorpID){
		   return myFileDao.getListByDesc(shipNumber,sessionCorpID);
	   }
	//End dashboard

	public List findFileLocation(String openFrom, String closeFrom, String companyCorpID) {
		return myFileDao.findFileLocation(openFrom, closeFrom, companyCorpID);
	}
	
	public int updateMyfileDriverPortalStatus(String Status,Long ids, String updatedBy){
		   return myFileDao.updateMyfileDriverPortalStatus(Status,ids,updatedBy);
	   }
	
	public TreeMap<String, List<MyFile>> getDriverDocTypeMapList(String shipNumber,String active, String secure, String categorySearchValue,String tempSortOrder,String sortingOrder){
		return myFileDao.getDriverDocTypeMapList(shipNumber, active, secure, categorySearchValue, tempSortOrder, sortingOrder);
	}

	public List getdocAgentTypeMapList(String shipNumber,String vendorCode,String sessionCorpID) {
		return myFileDao.getdocAgentTypeMapList(shipNumber, vendorCode, sessionCorpID);
		
	}
	
	public List fileTypeForPayableProcessing(String sessionCorpID){
		return myFileDao.fileTypeForPayableProcessing(sessionCorpID);
	}
	
	public List getDocumentCode(String fileType, String sessionCorpID) {
		return myFileDao.getDocumentCode(fileType, sessionCorpID);
	}

}
