package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager; 
import com.trilasoft.app.model.PartnerRateGridContracts;

public interface PartnerRateGridContractsManager extends GenericManager<PartnerRateGridContracts, Long>{

	public List getRateGridContractsList(String sessionCorpID, Long partnerRateGridID);

	public List getdDiscountCompanyList();

	public List getContractList(String sessionCorpID);

	public int findUniqueCount(Long partnerRateGridID, String sessionCorpID, String discountCompany, String contract, String effectiveDate);

	public List findMaxEffectiveDate(String sessionCorpID, Long partnerRateGridID);

	public int findUniqueCount(Long partnerRateGridID,String sessionCorpID, String discountCompany, String contract, String effectiveDate, Long id);

}
