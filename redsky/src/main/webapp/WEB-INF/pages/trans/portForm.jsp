<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<head>
 
<link href='<s:url value="/css/main.css"/>' rel="stylesheet"
	type="text/css" />
<s:head theme="ajax" />
<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
	<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
 <%--
 <script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghRGCK2dYb6E9n4B1E2585HyXOYBiBTkm8DZjmBAmvbTx4LAPz_RRfRfSg" type="text/javascript"></script>


<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w" type="text/javascript"></script>
--%>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
	<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
</script>
<script language="javascript" type="text/javascript">
function myDate() {
	
	
		if(document.forms['portForm'].elements['port.corpID'].value == "null" ){
		document.forms['portForm'].elements['port.corpID'].value="";
	}
	

	}
	
</script>

<script>
function ContainerAutoSave(clickType){
if ('${autoSavePrompt}' == 'No'){
	var noSaveAction;
		if(document.forms['portForm'].elements['gotoPageString'].value == 'gototab.port'){
                noSaveAction = 'ports.html';
        }
		processAutoSave(document.forms['portForm'],'savePort!saveOnTabChange.html', noSaveAction );
	}
	else{
    if(!(clickType == 'save')){
    var id1 = document.forms['portForm'].elements['port.id'].value;
       if (document.forms['portForm'].elements['formStatus'].value == '1'){
        var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='portDetail.heading'/>");
        if(agree){
            document.forms['portForm'].action = 'savePort!saveOnTabChange.html';
            document.forms['portForm'].submit();
        }else{
if(document.forms['portForm'].elements['gotoPageString'].value == 'gototab.port'){
                location.href = 'ports.html';
               }
        }
    }else{
    
if(document.forms['portForm'].elements['gotoPageString'].value == 'gototab.port'){
                location.href = 'ports.html';
    }
    }
   }
   }
  }

function changeStatus()
{
    document.forms['portForm'].elements['formStatus'].value = '1';
}

function getPortCountryCode(){
	var country=document.forms['portForm'].elements['port.country'].value;
	var url="portCountryCode.html?ajax=1&decorator=simple&popup=true&country=" + encodeURI(country);
    http4.open("GET", url, true);
    http4.onreadystatechange = httpPortCountryCode;
    http4.send(null);
}

function httpPortCountryCode()
        {
             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                if(results.length>=1)
                	{
 					document.forms['portForm'].elements['port.countryCode'].value = results;
 					document.forms['portForm'].elements['port.country'].select();
					}
					else{
                    document.forms['portForm'].elements['port.countryCode'].value = '';
 					document.forms['portForm'].elements['port.country'].select();
                 }
             }
        }


String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}


function getHTTPObject4()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http4 = getHTTPObject4();

    function initLoader() { 
   	 var script = document.createElement("script");
        script.type = "text/javascript";
     	 script.src = "http://maps.google.com/maps?file=api&v=2&key="+googlekey+"&async=2&callback=loadMap";
        document.body.appendChild(script);
        setTimeout('',6000);
        }
var geocoder;
var map;
function loadMap()
   {
   	var portName=document.forms['portForm'].elements['port.portName'].value;
	var portCode=document.forms['portForm'].elements['port.portCode'].value;
	var country=document.forms['portForm'].elements['port.country'].value;
	
	var address = portCode+","+portName+","+country;
	
	
      // Create new map object
      map = new GMap2(document.getElementById('map'));
		
      // Create new geocoding object
      geocoder = new GClientGeocoder();

      // Retrieve location information, pass it to addToMap()
      geocoder.getLocations(address, addToMap);
   }
   function addToMap(response)
   {
      // Retrieve the object
     

      // Retrieve the latitude and longitude
      if('${port.latitude}'=='' || document.forms['portForm'].elements['port.latitude'].value=='' || document.forms['portForm'].elements['port.longitude'].value=='')
      {
       place = response.Placemark[0];
      point = new GLatLng(place.Point.coordinates[1],
                          place.Point.coordinates[0]);
        document.forms['portForm'].elements['port.latitude'].value = place.Point.coordinates[1];
		document.forms['portForm'].elements['port.longitude'].value = place.Point.coordinates[0];
		}
		else
		{
			point = new GLatLng('${port.latitude}',
                          '${port.longitude}');
		}
      // Center the map on this point
      map.setCenter(point, 13);
		map.setUIToDefault();
      // Create a marker
      marker = new GMarker(point);

      // Add the marker to map
      map.addOverlay(marker);

      // Add address information to marker
      //marker.openInfoWindowHtml(place.address);
   }
   


function findGeoCode(){
	window.open('http://www.gpsvisualizer.com/geocode','','width=750,height=570') ;
}
function validateAir()
{
      var distanceUnit=document.forms['portForm'].elements['port.mode'].value;
		var el = document.getElementById('hidDM');
		if(distanceUnit == 'Air'){
		el.style.display = 'block';
		el.style.visibility = 'visible';
		}else{
		el.style.display = 'none';
		}
		}
function validateSea()
{
      var distanceUnit=document.forms['portForm'].elements['port.mode'].value;
		var el = document.getElementById('hid');
		if(distanceUnit == 'Sea'){
		el.style.display = 'block';
		el.style.visibility = 'visible';
		}else{
		el.style.display = 'none';
		}
		}
</script>
  



<title><fmt:message key="portDetail.title" /></title>
  <meta name="heading" content="<fmt:message key='portDetail.heading'/>" />
	
</head>
<c:set var="fld_code" value="<%=request.getParameter("fld_code") %>" />
<c:set var="fld_description" value="<%=request.getParameter("fld_description") %>" />
<s:form name="portForm" id="portForm" action="savePort" method="post" validate="true">

<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.port' }">
    <c:redirect url="/ports.html?id=${port.id}"/>
</c:when>
</c:choose>
</c:if>
	<s:hidden name="port.id" value="%{port.id}"/>
	<s:hidden name="port.countryCode"/>
	<div id="layer4" style="width:100%;">
	<div id="newmnav">
		  <ul>
		      <li id="newmnav1" style="background:#FFF "><a class="current" ><span>Port&nbsp;Detail<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
              <li><a onclick="setReturnString('gototab.port');return  ContainerAutoSave('none');"><span>Port&nbsp;List</span></a></li>
         </ul>
        </div><div class="spn">&nbsp;</div>
        
        </div>
        <div id="Layer1"  onkeydown="changeStatus();" style="width:100%;">
     <div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">   
   
   <table cellspacing="0" cellpadding="0" border="0" style="width:100%;margin: 0px;padding: 0px;" >
   <tr>
   <td valign="top" style="margin: 0px;padding: 0px;">
	<table class="" cellspacing="1" cellpadding="2" border="0" style="width:100%">
	
	
	  <tr> 
	  <c:set var="isVipFlag" value="false"/>
								<c:if test="${port.active}">
									<c:set var="isVipFlag" value="true"/>
								</c:if>
	  	                    <td class="listwhitetext" align="right" width="40px"><fmt:message key='port.portCode'/><font color="red">*</font></td>
							<td align="left" width="40px"><s:textfield name="port.portCode" size="15" maxlength="20"  cssClass="input-text" onfocus="myDate();" /></td>
							<td class="listwhitetext" align="right" width="">Abbr</td>
							<td align="left" width="40px"><s:textfield name="port.refAbbreviation" size="15" maxlength="50"  cssClass="input-text"  /></td>
				      	
				      	</tr>
				      	<tr>			
							<td class="listwhitetext"  align="right" ><fmt:message key='port.modeType'/></td>
	 					    <td align="left" ><s:select cssClass="list-menu" key="port.mode" list="%{mode}" cssStyle="width:103px"  onchange="validateAir();validateSea();changeStatus();"/></td>
					  	<c:set var="isCargoAirport" value="false"/>
								<c:if test="${port.isCargoAirport}">
									<c:set var="isCargoAirport" value="true"/>
								</c:if>
								<td align="left" class="listwhitetext" colspan="2">
								<div id="hidDM">
								<table cellspacing="0" cellpadding="0" border="0" style="margin: 0px;padding: 0px;"><tr>
								<td align="right" class="listwhitetext" colspan="">Cargo&nbsp;Airport</td>
				      	   <td align="left" width="10px" valign="bottom" ><s:checkbox key="port.isCargoAirport" value="${isCargoAirport}" fieldValue="true" onclick="changeStatus()" /></td>
				      	   </tr>
				      	   </table>
				      	   </div>
				      	   </td>
				      	   
					  	</tr> 
						<tr>
							<td class="listwhitetext"align="right"  ><fmt:message key='port.country'/></td>
							<td align="left" colspan="3" ><s:select cssClass="list-menu" key="port.country" list="%{ocountry}" cssStyle="width:252px;" headerKey="" headerValue="" onchange="getPortCountryCode();changeStatus();"/></td>
						                    
						</tr>
						<tr>
							<td class="listwhitetext"  align="right" ><fmt:message key='port.portName'/></td>
							<td ><s:textfield name="port.portName" size="15" maxlength="200" cssClass="input-text" /></td>
							
							<td align="left" class="listwhitetext" colspan="2">
							<table cellspacing="0" cellpadding="0" border="0" style="margin: 0px;padding: 0px;"><tr>
							<td align="right" class="listwhitetext" width="35px"></td>
							<td align="right" class="listwhitetext" colspan="">Active</td>
				      	   <td align="left" width="10px" valign="bottom" ><s:checkbox key="port.active" value="${isVipFlag}" fieldValue="true" onclick="changeStatus()" /></td>
							</tr></table>
							</td>
							
						</tr>
						<tr>
						<td></td>
							<td align="left" class="listwhitetext" colspan="3">
								<input type="button" class="cssbutton" onclick="findGeoCode();" name="Find Geo Code" value="Manually Find Geo Code" style="width:175px; height:25px"/>
								</td>
						</tr>
						<tr>
						<td></td>
						
						 <td align="left" class="listwhitetext" >
						 <input type="button" class="cssbutton" onclick="initLoader();" name="Find Geo Code" value="Find Geo Code" style="width:105px; height:25px"/>
						 </td>
						 <td align="left" class="listwhitetext" colspan="2">
								<div id="hid">
								<table cellspacing="0" cellpadding="0" border="0" style="margin: 0px;padding: 0px;"><tr>
						 <td align="right" class="listwhitetext" width="49px"></td>
						 <td align="right" class="listwhitetext" colspan="">ICD</td>
				      	 <td align="left" width="10px" valign="absmiddle" ><s:checkbox key="port.ICD" value="${port.ICD}" fieldValue="true" /></td>
						</tr></table></div></td>
						</tr>
						<tr>
						<td align="right" class="listwhitetext" width="">Latitude</td>
						    <td width="20px"><s:textfield cssClass="input-text" name="port.latitude"  size="12"/></td>
						    <td align="right" class="listwhitetext" width="50px">Longitude</td>
						    <td><s:textfield cssClass="input-text" name="port.longitude"  size="12"/></td>
						   
							</tr>
						
						</table>
						
						</td>
						<td valign="top" style="margin: 0px;padding: 0px;">
						<div id="map" style="width: 580px; height: 200px"></div>
						</td>
						</tr>
						</table>
						</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
						

		   <s:hidden name="port.corpID"/>				
		<table class="detailTabLabel" cellpadding="0" cellspacing="3" border="0" style="width:680px;">
		   <tbody>
					<tr>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
						<td valign="top"></td>
						
						<td style="width:120px">
								<fmt:formatDate var="portCreatedOnFormattedValue" value="${port.createdOn}" pattern="${displayDateTimeEditFormat}"/>
								<s:hidden name="port.createdOn" value="${portCreatedOnFormattedValue}"/>
								<fmt:formatDate value="${port.createdOn}" pattern="${displayDateTimeFormat}"/>
						</td>
						
						<td align="right" class="listwhitetext" width="70"><b><fmt:message key='customerFile.createdBy' /></b></td>
						
						<c:if test="${not empty port.id}">
							<s:hidden name="port.createdBy" />
							<td ><s:label name="createdBy" value="%{port.createdBy}" /></td>
						</c:if>
						<c:if test="${empty port.id}">
							<s:hidden name="port.createdBy" value="${pageContext.request.remoteUser}" />
							<td ><s:label name="createdBy" 	value="${pageContext.request.remoteUser}" /></td>
						</c:if>
						
						<fmt:formatDate var="portUpdatedOnFormattedValue" value="${port.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
						<td style="width:70px" align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedOn'/></b></td>
						<s:hidden name="port.updatedOn"  value="${portUpdatedOnFormattedValue}"/>
						<td style="width:130px"><fmt:formatDate value="${port.updatedOn}" pattern="${displayDateTimeFormat}"/>
						<td style="width:70px" align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedBy' /></b></td>
						<c:if test="${not empty port.id}">
							<s:hidden name="port.updatedBy" />
							<td ><s:label name="updatedBy" value="%{port.updatedBy}" /></td>
						</c:if>
						<c:if test="${empty port.id}">
							<s:hidden name="port.updatedBy" value="${pageContext.request.remoteUser}" />
							<td ><s:label name="updatedBy" 	value="${pageContext.request.remoteUser}" /></td>
						</c:if>
						
					</tr>
				</tbody>
			</table>	
	</div>
	<div style="padding-left:15px; padding-top:10px;">
	<s:submit cssClass="cssbuttonA" method="save" key="button.save"
		cssStyle="width:55px; height:25px" tabindex="23"/>
		</div>
</s:form>

<script type="text/javascript">
validateAir();
validateSea()
try{
	initLoader();
    }
    catch(e){}
    Form.focusFirstElement($("portForm")); 
</script>


