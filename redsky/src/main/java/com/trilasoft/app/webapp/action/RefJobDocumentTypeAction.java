package com.trilasoft.app.webapp.action;




import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.servlet.http.HttpServletResponse;



import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;



import com.lowagie.text.Document;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;
import com.trilasoft.app.model.RefJobDocumentType;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.RefJobDocumentTypeManager;
import com.trilasoft.app.service.RefMasterManager;

public class RefJobDocumentTypeAction extends BaseAction{
	private RefJobDocumentTypeManager refJobDocumentTypeManager;
	private RefJobDocumentType refJobDocumentType;
	private List refJobDocumentTypes;
	private String sessionCorpID;
    private Long id;
	private Map<String, String> jobs;
	private RefMasterManager refMasterManager;
	private String jobType;
	private MyFileManager myFileManager;
	public RefJobDocumentTypeAction() {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID=user.getCorpID();

	}	
	
	private Map<String, String>  documentList;
	
	private SortedMap<String, String> documentList1= new TreeMap<String, String>();
	
	private SortedMap<String, String> documentList2= new TreeMap<String, String>();
	private ArrayList currentList= new ArrayList(); 
	
	@SkipValidation
	public String edit(){
		jobs = refMasterManager.findByParameter(sessionCorpID, "JOB"); 		
		   if (id != null) { 
			   refJobDocumentType = refJobDocumentTypeManager.get(id);  
			   StringTokenizer st = new StringTokenizer(refJobDocumentType.getRefJobDocs(),",");
			     while (st.hasMoreTokens()) {
			    	 String tok=st.nextToken();
			         currentList.add(tok);			      
			     } 
		   }
		   else
		   {
			   refJobDocumentType= new RefJobDocumentType(); 
			   refJobDocumentType.setCreatedOn(new Date());
			   refJobDocumentType.setCreatedBy(getRequest().getRemoteUser());
		   }
		   refJobDocumentType.setCorpID(sessionCorpID); 
		   refJobDocumentType.setUpdatedOn(new Date());
		   refJobDocumentType.setUpdatedBy(getRequest().getRemoteUser());		   
		documentList= refMasterManager.findByParameter(sessionCorpID, "DOCUMENT");
		Iterator mapIterator = documentList.entrySet().iterator();
		SortedMap<String, String> sm = new TreeMap<String, String>();
		while (mapIterator.hasNext()) {
			Map.Entry entry = (Map.Entry) mapIterator.next();
			String key = (String) entry.getKey();
	        String value=(String) entry.getValue();
	        sm.put(key,value);
		}

		mapIterator = sm.entrySet().iterator();
		int j=0;
		while (mapIterator.hasNext()) {
	    	  Map.Entry entry = (Map.Entry) mapIterator.next();
	          String key = (String) entry.getKey();
	          String value=(String) entry.getValue();
	          if(j==0)
				{
	        	  documentList1.put(key, value);
					j=1;
				}
				else if(j==1){
					documentList2.put(key, value);
					j=0;
				}
		
	   }
	 return SUCCESS;
	}
   
	public String save() throws Exception { 
		//System.out.println(refJobDocumentType.getId());
		boolean isNew = (refJobDocumentType.getId() == null);  
		if (isNew) { 
			refJobDocumentType.setCreatedOn(new Date());
			refJobDocumentType.setCreatedBy(getRequest().getRemoteUser());
        }
		String serviceDoc=refJobDocumentType.getRefJobDocs();
		if(serviceDoc.equals("")||serviceDoc.equals("#"))
	  	  {
	  	  }
	  	 else
	  	  {
	  		serviceDoc=serviceDoc.trim();
	  	    if(serviceDoc.indexOf("#")==0)
	  		 {
	  	    	serviceDoc=serviceDoc.substring(1);
	  		 }
	  		if(serviceDoc.lastIndexOf("#")==serviceDoc.length()-1)
	  		 {
	  			serviceDoc=serviceDoc.substring(0, serviceDoc.length()-1);
	  		 } 
	  	  } 
		serviceDoc=serviceDoc.replaceAll("#", ","); 
		refJobDocumentType.setRefJobDocs(serviceDoc);
	    refJobDocumentType.setCorpID(sessionCorpID); 
		List isexisted = refJobDocumentTypeManager.searchRefJobDocument(refJobDocumentType.getJobType(),refJobDocumentType.getCorpID());
		if(!isexisted.isEmpty() && isNew){
				edit();
				errorMessage("Job type for this CorpId already exist.");
				return INPUT; 
	  }else{  
			try
				{ 
						refJobDocumentType.setUpdatedOn(new Date());
						refJobDocumentType.setUpdatedBy(getRequest().getRemoteUser());
						if((refJobDocumentType.getJobType()!=null)&&(refJobDocumentType.getJobType()!=""))
							{
								refJobDocumentType.setJobType(refJobDocumentType.getJobType());
							}
						else
						{
								refJobDocumentType.setJobType("");
						}
						refJobDocumentType=refJobDocumentTypeManager.save(refJobDocumentType);
						String key=(isNew)?"RefJobDocumentTypeAdded":"RefJobDocumentTypeUpdated";
						saveMessage(getText(key)); 
					}catch(Exception e){} 
			 }
    	return SUCCESS; 
    	}

	
	public String list()
	{ 
		refJobDocumentTypes=refJobDocumentTypeManager.getAll();
		return SUCCESS;
	}	
	public String delete()
	{		
          refJobDocumentTypeManager.remove(id);    
	      return SUCCESS;		    
	}	
	
/*	
	public String list()
	{
		jobs = refMasterManager.findByParameter(sessionCorpID, "JOB");  
		refJobDocumentTypes=refJobDocumentTypeManager.getAll();
		return SUCCESS;
	}

	public String delete()
	{		
          refJobDocumentTypeManager.remove(id);    
	      return SUCCESS;		    
	}
	public String search()
	{
		jobs = refMasterManager.findByParameter(sessionCorpID, "JOB");
		System.out.println(jobType+""+sessionCorpID);
		refJobDocumentTypes=refJobDocumentTypeManager.searchRefJobDocument(jobType, sessionCorpID);
		return SUCCESS;
	}
*/	
	public Map<String, String> getDocumentList() {
		return documentList;
	}

	public void setDocumentList(Map<String, String> documentList) {
		this.documentList = documentList;
	}

	public Map<String, String> getDocumentList1() {
		return documentList1;
	}

	public void setDocumentList1(SortedMap<String, String> documentList1) {
		this.documentList1 = documentList1;
	}

	public Map<String, String> getDocumentList2() {
		return documentList2;
	}

	public void setDocumentList2(SortedMap<String, String> documentList2) {
		this.documentList2 = documentList2;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Map<String, String> getJobs() {
		return jobs;
	}

	public void setJobs(Map<String, String> jobs) {
		this.jobs = jobs;
	}

	public String getJobType() {
		return jobType;
	}
	
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public RefJobDocumentType getRefJobDocumentType() {
		return refJobDocumentType;
	}

	public void setRefJobDocumentType(RefJobDocumentType refJobDocumentType) {
		this.refJobDocumentType = refJobDocumentType;
	}

	public List getRefJobDocumentTypes() {
		return refJobDocumentTypes;
	}

	public void setRefJobDocumentTypes(List refJobDocumentTypes) {
		this.refJobDocumentTypes = refJobDocumentTypes;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	
	public void setRefJobDocumentTypeManager(
			RefJobDocumentTypeManager refJobDocumentTypeManager) {
		this.refJobDocumentTypeManager = refJobDocumentTypeManager;
	}
	
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public ArrayList getCurrentList() {
		return currentList;
	}
	
	public void setCurrentList(ArrayList currentList) {
		this.currentList = currentList;
	}

	
	
}