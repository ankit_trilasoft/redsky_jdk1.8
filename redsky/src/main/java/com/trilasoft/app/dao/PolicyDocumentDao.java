package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.PolicyDocument;

public interface PolicyDocumentDao extends GenericDao<PolicyDocument,Long>{

	public List checkPolicyId(Long fid);
	public List getMaxId();
	 public int checkPolicyDocumentName(String documentName,String sectionName,String language,String partnerCode,String sessionCorpID);
}
