package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.GenericSurveyAnswer;
import com.trilasoft.app.model.GenericSurveyQuestion;
import com.trilasoft.app.model.MssGrid;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SurveyAnswerByUser;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.GenericSurveyAnswerManager;
import com.trilasoft.app.service.GenericSurveyQuestionManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.SurveyAnswerByUserManager;

public class GenericSurveyQuestionAction extends BaseAction{
	
	private Long id;
	private String sessionCorpID;
	private GenericSurveyQuestion genericSurveyQuestion;
	private GenericSurveyQuestionManager genericSurveyQuestionManager;
	private RefMasterManager refMasterManager;
	private Map<String, String> jobTypeList;
	private Map<String, String> routingList;
	private Map<String, String> languageList;
	private Map<String, String> answerTypeList;
	private GenericSurveyAnswerManager genericSurveyAnswerManager;
	private GenericSurveyAnswer genericSurveyAnswer;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private SurveyAnswerByUser surveyAnswerByUser;
	private SurveyAnswerByUserManager surveyAnswerByUserManager;
	private CustomerFileManager customerFileManager;
	private CustomerFile customerFile;
	private Map<String, String> categoryList;
	
	public GenericSurveyQuestionAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}
	public String getComboList(String corpID){
		jobTypeList = refMasterManager.findByParameter(sessionCorpID, "JOB");
		routingList = refMasterManager.findByParameter(sessionCorpID, "ROUTING");
		languageList = refMasterManager.findByflexValue(sessionCorpID, "LANGUAGE");
		categoryList = refMasterManager.findByParameter(sessionCorpID, "GENERICSURVEYCATEGORY");
		answerTypeList =new LinkedHashMap<String, String>();
		answerTypeList.put("SingleAnswerRadio", "Single Answer(Radio button)");
		answerTypeList.put("SingleAnswerDropdown", "Single Answer(Dropdown)");
		answerTypeList.put("MultipleCheckbox", "Multiple(Checkbox)");
		answerTypeList.put("Text", "Text");
		
		return SUCCESS;
	}
	
	private List surveyQuestionList;
	public String edit() {
		getComboList(sessionCorpID);
		surveyQuestionList = genericSurveyQuestionManager.findRecords(sessionCorpID);
		return SUCCESS;
	}
	private String surveyQuestionRowlist;
	private String seqNumListVal;
	private String questionListVal;
	private String answerTypeListVal;
	private String jobTypeListVal;
	private String routingListVal;
	private String bookingAgentListVal;
	private String originAgentListVal;
	private String destinationAgentListVal;
	private String languageListVal;
	private String statusListVal;
	private String categoryListVal;
	public String save() throws Exception {
		getComboList(sessionCorpID);
		boolean isNew = false ;  
				
				if(surveyQuestionRowlist!=null && !surveyQuestionRowlist.equalsIgnoreCase("")){
					
						String[] str = surveyQuestionRowlist.split("~");
						for (String string : str) {
							try{
								String[] strRes=string.split(":");
								
								String tempSeq = strRes[0].toString().trim();
								String tempCat = strRes[1].toString().trim();
								String tempQues = strRes[2].toString().trim();
								String tempAns = strRes[3].toString().trim();
								String tempJob = strRes[4].toString().trim();
								String tempRout = strRes[5].toString().trim();
								Boolean tempBA = Boolean.parseBoolean(strRes[6].toString().trim());
								Boolean tempOA = Boolean.parseBoolean(strRes[7].toString().trim());
								Boolean tempDA = Boolean.parseBoolean(strRes[8].toString().trim());
								String tempLang = strRes[9].toString().trim();
								Boolean tempStatus = Boolean.parseBoolean(strRes[10].toString().trim());
								
								genericSurveyQuestion = new GenericSurveyQuestion();
								
								if(tempSeq!=null && !tempSeq.equalsIgnoreCase("")){
									genericSurveyQuestion.setSequenceNumber(Integer.parseInt(tempSeq));
								}else{
									genericSurveyQuestion.setSequenceNumber(1);
								}
								genericSurveyQuestion.setCategory(tempCat);
								genericSurveyQuestion.setQuestion(tempQues);
								genericSurveyQuestion.setAnswerType(tempAns);
								genericSurveyQuestion.setJobType(tempJob);
								genericSurveyQuestion.setRouting(tempRout);
								genericSurveyQuestion.setBookingAgent(tempBA);
								genericSurveyQuestion.setOriginAgent(tempOA);
								genericSurveyQuestion.setDestinationAgent(tempDA);
								genericSurveyQuestion.setLanguage(tempLang);
								genericSurveyQuestion.setStatus(tempStatus);
								genericSurveyQuestion.setCreatedBy(getRequest().getRemoteUser());
								genericSurveyQuestion.setUpdatedBy(getRequest().getRemoteUser());
								genericSurveyQuestion.setCreatedOn(new Date());
								genericSurveyQuestion.setUpdatedOn(new Date());
								genericSurveyQuestion.setCorpId(sessionCorpID);
								genericSurveyQuestionManager.save(genericSurveyQuestion);
							}catch(Exception e){
								e.printStackTrace();
							}
						}
			    }
				
				
				if(seqNumListVal!=null && (!seqNumListVal.equalsIgnoreCase(""))){
					String [] temp = seqNumListVal.split("~");
					for(String res : temp){
						try{
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							GenericSurveyQuestion surveyObj = genericSurveyQuestionManager.get(tempResId);
							if(resArr.length>1){
								surveyObj.setSequenceNumber(Integer.parseInt(resArr[1].toString().trim()));
							}else{
								surveyObj.setSequenceNumber(1);
							}
							surveyObj.setUpdatedBy(getRequest().getRemoteUser());
							surveyObj.setUpdatedOn(new Date());
							genericSurveyQuestionManager.save(surveyObj);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				
				if(categoryListVal!=null && (!categoryListVal.equalsIgnoreCase(""))){
					String [] temp = categoryListVal.split("~");
					for(String res : temp){
						try{
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							GenericSurveyQuestion surveyObj = genericSurveyQuestionManager.get(tempResId);
							if(resArr.length>1){
								surveyObj.setCategory(resArr[1].toString().trim());
							}else{
								surveyObj.setCategory("");
							}
							surveyObj.setUpdatedBy(getRequest().getRemoteUser());
							surveyObj.setUpdatedOn(new Date());
							genericSurveyQuestionManager.save(surveyObj);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				
				if(questionListVal!=null && (!questionListVal.equalsIgnoreCase(""))){
					String [] temp = questionListVal.split("~");
					for(String res : temp){
						try{
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							GenericSurveyQuestion surveyObj = genericSurveyQuestionManager.get(tempResId);
							if(resArr.length>1){
								surveyObj.setQuestion(resArr[1].toString().trim());
							}else{
								surveyObj.setQuestion("");
							}
							surveyObj.setUpdatedBy(getRequest().getRemoteUser());
							surveyObj.setUpdatedOn(new Date());
							genericSurveyQuestionManager.save(surveyObj);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				
				if(answerTypeListVal!=null && (!answerTypeListVal.equalsIgnoreCase(""))){
					String [] temp = answerTypeListVal.split("~");
					for(String res : temp){
						try{
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							GenericSurveyQuestion surveyObj = genericSurveyQuestionManager.get(tempResId);
							if(resArr.length>1){
								surveyObj.setAnswerType(resArr[1].toString().trim());
							}else{
								surveyObj.setAnswerType("");
							}
							surveyObj.setUpdatedBy(getRequest().getRemoteUser());
							surveyObj.setUpdatedOn(new Date());
							genericSurveyQuestionManager.save(surveyObj);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				if(jobTypeListVal!=null && (!jobTypeListVal.equalsIgnoreCase(""))){
					String [] temp = jobTypeListVal.split("~");
					for(String res : temp){
						try{
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							GenericSurveyQuestion surveyObj = genericSurveyQuestionManager.get(tempResId);
							if(resArr.length>1){
								surveyObj.setJobType(resArr[1].toString().trim());
							}else{
								surveyObj.setJobType("");
							}
							surveyObj.setUpdatedBy(getRequest().getRemoteUser());
							surveyObj.setUpdatedOn(new Date());
							genericSurveyQuestionManager.save(surveyObj);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				
				if(routingListVal!=null && (!routingListVal.equalsIgnoreCase(""))){
					String [] temp = routingListVal.split("~");
					for(String res : temp){
						try{
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							GenericSurveyQuestion surveyObj = genericSurveyQuestionManager.get(tempResId);
							if(resArr.length>1){
								surveyObj.setRouting(resArr[1].toString().trim());
							}else{
								surveyObj.setRouting("");
							}
							surveyObj.setUpdatedBy(getRequest().getRemoteUser());
							surveyObj.setUpdatedOn(new Date());
							genericSurveyQuestionManager.save(surveyObj);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				
				if(bookingAgentListVal!=null && (!bookingAgentListVal.equalsIgnoreCase(""))){
					String [] temp = bookingAgentListVal.split("~");
					for(String res : temp){
						try{
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							GenericSurveyQuestion surveyObj = genericSurveyQuestionManager.get(tempResId);
							surveyObj.setBookingAgent(Boolean.parseBoolean(resArr[1].toString().trim()));
							surveyObj.setUpdatedBy(getRequest().getRemoteUser());
							surveyObj.setUpdatedOn(new Date());
							genericSurveyQuestionManager.save(surveyObj);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				if(originAgentListVal!=null && (!originAgentListVal.equalsIgnoreCase(""))){
					String [] temp = originAgentListVal.split("~");
					for(String res : temp){
						try{
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							GenericSurveyQuestion surveyObj = genericSurveyQuestionManager.get(tempResId);
							surveyObj.setOriginAgent(Boolean.parseBoolean(resArr[1].toString().trim()));
							surveyObj.setUpdatedBy(getRequest().getRemoteUser());
							surveyObj.setUpdatedOn(new Date());
							genericSurveyQuestionManager.save(surveyObj);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				if(destinationAgentListVal!=null && (!destinationAgentListVal.equalsIgnoreCase(""))){
					String [] temp = destinationAgentListVal.split("~");
					for(String res : temp){
						try{
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							GenericSurveyQuestion surveyObj = genericSurveyQuestionManager.get(tempResId);
							surveyObj.setDestinationAgent(Boolean.parseBoolean(resArr[1].toString().trim()));
							surveyObj.setUpdatedBy(getRequest().getRemoteUser());
							surveyObj.setUpdatedOn(new Date());
							genericSurveyQuestionManager.save(surveyObj);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				if(languageListVal!=null && (!languageListVal.equalsIgnoreCase(""))){
					String [] temp = languageListVal.split("~");
					for(String res : temp){
						try{
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							GenericSurveyQuestion surveyObj = genericSurveyQuestionManager.get(tempResId);
							if(resArr.length>1){
								surveyObj.setLanguage(resArr[1].toString().trim());
							}else{
								surveyObj.setLanguage("");
							}
							surveyObj.setUpdatedBy(getRequest().getRemoteUser());
							surveyObj.setUpdatedOn(new Date());
							genericSurveyQuestionManager.save(surveyObj);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				if(statusListVal!=null && (!statusListVal.equalsIgnoreCase(""))){
					String [] temp = statusListVal.split("~");
					for(String res : temp){
						try{
							String [] resArr  =res.split(":");
							Long tempResId = Long.parseLong(resArr[0]);
							GenericSurveyQuestion surveyObj = genericSurveyQuestionManager.get(tempResId);
							surveyObj.setStatus(Boolean.parseBoolean(resArr[1].toString().trim()));
							surveyObj.setUpdatedBy(getRequest().getRemoteUser());
							surveyObj.setUpdatedOn(new Date());
							genericSurveyQuestionManager.save(surveyObj);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
			    String key = (isNew) ? "Survey Question has been added Sucessfully" : "Survey Question has been updated Sucessfully";
			    saveMessage(getText(key)); 
			    
				return SUCCESS;
	}
	
	private List surveyAnswerList;
	private Long parentId;
	@SkipValidation 
	public String findSurveyAnswerList(){
		try {
			surveyAnswerList = genericSurveyAnswerManager.getSurveyAnswerList(parentId,sessionCorpID);
		} catch (Exception e) {
	    	 e.printStackTrace();
		} 
		return SUCCESS;
	}
	
	private String childAnswerListVal;
	private String childSeqNumListVal;
	private String childStatusListVal;
	private String surveyAnswerRowlist;
	
	@SkipValidation
	public void saveSurveyAnswer(){
		if(surveyAnswerRowlist!=null && (!surveyAnswerRowlist.equalsIgnoreCase(""))){
			try{
				String[] str = surveyAnswerRowlist.split("~");
				for (String string : str) {
					String[] strRes=string.split(":");
					
					String tempSeq = strRes[0].toString().trim();
					String tempAns = strRes[1].toString().trim();
					Boolean tempStatus = Boolean.parseBoolean(strRes[2].toString().trim());
					
					genericSurveyAnswer = new GenericSurveyAnswer();
					
					genericSurveyAnswer.setSequenceNumber(Integer.parseInt(tempSeq));
					genericSurveyAnswer.setAnswer(tempAns);
					genericSurveyAnswer.setStatus(tempStatus.booleanValue());
					genericSurveyAnswer.setSurveyQuestionId(parentId);
					genericSurveyAnswer.setCreatedBy(getRequest().getRemoteUser());
					genericSurveyAnswer.setUpdatedBy(getRequest().getRemoteUser());
					genericSurveyAnswer.setCreatedOn(new Date());
					genericSurveyAnswer.setUpdatedOn(new Date());
					genericSurveyAnswer.setCorpId(sessionCorpID);
					genericSurveyAnswerManager.save(genericSurveyAnswer);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
			
		if(childStatusListVal!=null && (!childStatusListVal.equalsIgnoreCase(""))){
			try{
				String [] temp = childStatusListVal.split("~");
				for(String res : temp){
					String [] resArr  =res.split(":");
					Long tempResId = Long.parseLong(resArr[0]);
					GenericSurveyAnswer surveyAnsObj = genericSurveyAnswerManager.get(tempResId);
					surveyAnsObj.setStatus(Boolean.parseBoolean(resArr[1].toString().trim()));
					surveyAnsObj.setUpdatedBy(getRequest().getRemoteUser());
					surveyAnsObj.setUpdatedOn(new Date());
					genericSurveyAnswerManager.save(surveyAnsObj);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		if(childSeqNumListVal!=null && (!childSeqNumListVal.equalsIgnoreCase(""))){
			try{
				String [] temp = childSeqNumListVal.split("~");
				for(String res : temp){
					String [] resArr  =res.split(":");
					Long tempResId = Long.parseLong(resArr[0]);
					GenericSurveyAnswer surveyAnsObj = genericSurveyAnswerManager.get(tempResId);
					if(resArr.length>1){
						surveyAnsObj.setSequenceNumber(Integer.parseInt(resArr[1].toString().trim()));
					}else{
						surveyAnsObj.setSequenceNumber(1);
					}
					surveyAnsObj.setUpdatedBy(getRequest().getRemoteUser());
					surveyAnsObj.setUpdatedOn(new Date());
					genericSurveyAnswerManager.save(surveyAnsObj);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		if(childAnswerListVal!=null && (!childAnswerListVal.equalsIgnoreCase(""))){
			try{
				String [] temp = childAnswerListVal.split("~");
				for(String res : temp){
					String [] resArr  =res.split(":");
					Long tempResId = Long.parseLong(resArr[0]);
					GenericSurveyAnswer surveyAnsObj = genericSurveyAnswerManager.get(tempResId);
					if(resArr.length>1){
						surveyAnsObj.setAnswer(resArr[1].toString().trim());
					}else{
						surveyAnsObj.setAnswer("");
					}
					surveyAnsObj.setUpdatedBy(getRequest().getRemoteUser());
					surveyAnsObj.setUpdatedOn(new Date());
					genericSurveyAnswerManager.save(surveyAnsObj);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	private Long sid;
	private String surveyByUserVal;
	private String returnAjaxStringValue;
	private Long cid;
	@SkipValidation
	public void saveSurveyByUserOnCfOrSo(){
		if(surveyByUserVal!=null && !surveyByUserVal.equalsIgnoreCase("")){
			try{
				String []tempVal = surveyByUserVal.split("@");
				for(String surveyVal:tempVal){
					String[] surveyValTemp = surveyVal.split("~");
					String surveyByUserAnswerIdVal="";
					String surveyByUserIdVal="";
					List answerIdList = new ArrayList();
					if(surveyValTemp[0].toString().contains("chk")){
						String surveyValueId = surveyValTemp[0].toString().substring(3);
						answerIdList = surveyAnswerByUserManager.getRecordsByQuestionId(Long.parseLong(surveyValueId),sid,null);
						if(answerIdList!=null && !answerIdList.isEmpty() && answerIdList.get(0)!=null){
							String answerIdVal = answerIdList.get(0).toString();
							String [] surveyByUserAnswerAndIdVal = answerIdVal.split("~");
							surveyByUserIdVal = surveyByUserAnswerAndIdVal[0];
							surveyByUserAnswerIdVal = surveyByUserAnswerAndIdVal[1];
						}
						if(surveyByUserAnswerIdVal==null || surveyByUserAnswerIdVal.equalsIgnoreCase("")){
							surveyAnswerByUser = new SurveyAnswerByUser();
							surveyAnswerByUser.setServiceOrderId(sid);
							surveyAnswerByUser.setCustomerFileId(null);
							surveyAnswerByUser.setQuestionId(Long.parseLong(surveyValTemp[0].toString().substring(3)));
							surveyAnswerByUser.setAnswerId(surveyValTemp[1].toString());
							surveyAnswerByUser.setCreatedOn(new Date());
							surveyAnswerByUserManager.save(surveyAnswerByUser);
						}else{
							surveyByUserAnswerIdVal = surveyByUserAnswerIdVal+"#"+surveyValTemp[1];
							surveyAnswerByUser = surveyAnswerByUserManager.get(Long.parseLong(surveyByUserIdVal));
							surveyAnswerByUser.setAnswerId(surveyByUserAnswerIdVal);
							surveyAnswerByUserManager.save(surveyAnswerByUser);
						}
					}else{
						String surveyValueId = surveyValTemp[0].toString().substring(3);
						answerIdList = surveyAnswerByUserManager.getRecordsByQuestionId(Long.parseLong(surveyValueId),sid,null);
						if(answerIdList!=null && !answerIdList.isEmpty() && answerIdList.get(0)!=null){
							String answerIdVal = answerIdList.get(0).toString();
							String [] surveyByUserAnswerAndIdVal = answerIdVal.split("~");
							surveyByUserIdVal = surveyByUserAnswerAndIdVal[0];
							surveyByUserAnswerIdVal = surveyByUserAnswerAndIdVal[1];
						}
						if(surveyByUserAnswerIdVal==null || surveyByUserAnswerIdVal.equalsIgnoreCase("")){
							surveyAnswerByUser = new SurveyAnswerByUser();
							surveyAnswerByUser.setServiceOrderId(sid);
							surveyAnswerByUser.setCustomerFileId(null);
							surveyAnswerByUser.setQuestionId(Long.parseLong(surveyValTemp[0].toString().substring(3)));
							surveyAnswerByUser.setAnswerId(surveyValTemp[1].toString());
							surveyAnswerByUser.setCreatedOn(new Date());
							surveyAnswerByUserManager.save(surveyAnswerByUser);
						}else{
							surveyAnswerByUser = surveyAnswerByUserManager.get(Long.parseLong(surveyByUserIdVal));
							surveyAnswerByUser.setAnswerId(surveyValTemp[1]);
							surveyAnswerByUserManager.save(surveyAnswerByUser);
						}
					}
					
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public GenericSurveyQuestion getGenericSurveyQuestion() {
		return genericSurveyQuestion;
	}
	public void setGenericSurveyQuestion(GenericSurveyQuestion genericSurveyQuestion) {
		this.genericSurveyQuestion = genericSurveyQuestion;
	}
	public void setGenericSurveyQuestionManager(
			GenericSurveyQuestionManager genericSurveyQuestionManager) {
		this.genericSurveyQuestionManager = genericSurveyQuestionManager;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public Map<String, String> getJobTypeList() {
		return jobTypeList;
	}
	public void setJobTypeList(Map<String, String> jobTypeList) {
		this.jobTypeList = jobTypeList;
	}
	public Map<String, String> getRoutingList() {
		return routingList;
	}
	public void setRoutingList(Map<String, String> routingList) {
		this.routingList = routingList;
	}
	public Map<String, String> getLanguageList() {
		return languageList;
	}
	public void setLanguageList(Map<String, String> languageList) {
		this.languageList = languageList;
	}
	public Map<String, String> getAnswerTypeList() {
		return answerTypeList;
	}
	public void setAnswerTypeList(Map<String, String> answerTypeList) {
		this.answerTypeList = answerTypeList;
	}
	public List getSurveyQuestionList() {
		return surveyQuestionList;
	}
	public void setSurveyQuestionList(List surveyQuestionList) {
		this.surveyQuestionList = surveyQuestionList;
	}
	public String getSurveyQuestionRowlist() {
		return surveyQuestionRowlist;
	}
	public void setSurveyQuestionRowlist(String surveyQuestionRowlist) {
		this.surveyQuestionRowlist = surveyQuestionRowlist;
	}
	public String getSeqNumListVal() {
		return seqNumListVal;
	}
	public void setSeqNumListVal(String seqNumListVal) {
		this.seqNumListVal = seqNumListVal;
	}
	public String getQuestionListVal() {
		return questionListVal;
	}
	public void setQuestionListVal(String questionListVal) {
		this.questionListVal = questionListVal;
	}
	public String getAnswerTypeListVal() {
		return answerTypeListVal;
	}
	public void setAnswerTypeListVal(String answerTypeListVal) {
		this.answerTypeListVal = answerTypeListVal;
	}
	public String getJobTypeListVal() {
		return jobTypeListVal;
	}
	public void setJobTypeListVal(String jobTypeListVal) {
		this.jobTypeListVal = jobTypeListVal;
	}
	public String getRoutingListVal() {
		return routingListVal;
	}
	public void setRoutingListVal(String routingListVal) {
		this.routingListVal = routingListVal;
	}
	public String getBookingAgentListVal() {
		return bookingAgentListVal;
	}
	public void setBookingAgentListVal(String bookingAgentListVal) {
		this.bookingAgentListVal = bookingAgentListVal;
	}
	public String getOriginAgentListVal() {
		return originAgentListVal;
	}
	public void setOriginAgentListVal(String originAgentListVal) {
		this.originAgentListVal = originAgentListVal;
	}
	public String getDestinationAgentListVal() {
		return destinationAgentListVal;
	}
	public void setDestinationAgentListVal(String destinationAgentListVal) {
		this.destinationAgentListVal = destinationAgentListVal;
	}
	public String getLanguageListVal() {
		return languageListVal;
	}
	public void setLanguageListVal(String languageListVal) {
		this.languageListVal = languageListVal;
	}
	public String getStatusListVal() {
		return statusListVal;
	}
	public void setStatusListVal(String statusListVal) {
		this.statusListVal = statusListVal;
	}
	public void setGenericSurveyAnswerManager(
			GenericSurveyAnswerManager genericSurveyAnswerManager) {
		this.genericSurveyAnswerManager = genericSurveyAnswerManager;
	}
	public List getSurveyAnswerList() {
		return surveyAnswerList;
	}
	public void setSurveyAnswerList(List surveyAnswerList) {
		this.surveyAnswerList = surveyAnswerList;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public String getChildAnswerListVal() {
		return childAnswerListVal;
	}
	public void setChildAnswerListVal(String childAnswerListVal) {
		this.childAnswerListVal = childAnswerListVal;
	}
	public String getChildSeqNumListVal() {
		return childSeqNumListVal;
	}
	public void setChildSeqNumListVal(String childSeqNumListVal) {
		this.childSeqNumListVal = childSeqNumListVal;
	}
	public String getChildStatusListVal() {
		return childStatusListVal;
	}
	public void setChildStatusListVal(String childStatusListVal) {
		this.childStatusListVal = childStatusListVal;
	}
	public String getSurveyAnswerRowlist() {
		return surveyAnswerRowlist;
	}
	public void setSurveyAnswerRowlist(String surveyAnswerRowlist) {
		this.surveyAnswerRowlist = surveyAnswerRowlist;
	}
	public GenericSurveyAnswer getGenericSurveyAnswer() {
		return genericSurveyAnswer;
	}
	public void setGenericSurveyAnswer(GenericSurveyAnswer genericSurveyAnswer) {
		this.genericSurveyAnswer = genericSurveyAnswer;
	}
	public Long getSid() {
		return sid;
	}
	public void setSid(Long sid) {
		this.sid = sid;
	}
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	public String getSurveyByUserVal() {
		return surveyByUserVal;
	}
	public void setSurveyByUserVal(String surveyByUserVal) {
		this.surveyByUserVal = surveyByUserVal;
	}
	public SurveyAnswerByUser getSurveyAnswerByUser() {
		return surveyAnswerByUser;
	}
	public void setSurveyAnswerByUser(SurveyAnswerByUser surveyAnswerByUser) {
		this.surveyAnswerByUser = surveyAnswerByUser;
	}
	public void setSurveyAnswerByUserManager(
			SurveyAnswerByUserManager surveyAnswerByUserManager) {
		this.surveyAnswerByUserManager = surveyAnswerByUserManager;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public String getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}
	public void setReturnAjaxStringValue(String returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}
	public Map<String, String> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(Map<String, String> categoryList) {
		this.categoryList = categoryList;
	}
	public String getCategoryListVal() {
		return categoryListVal;
	}
	public void setCategoryListVal(String categoryListVal) {
		this.categoryListVal = categoryListVal;
	}
	public Long getCid() {
		return cid;
	}
	public void setCid(Long cid) {
		this.cid = cid;
	}

}
