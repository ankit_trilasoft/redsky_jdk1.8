package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.NetworkControl;

public interface NetworkControlManager extends GenericManager<NetworkControl, Long>{

}
