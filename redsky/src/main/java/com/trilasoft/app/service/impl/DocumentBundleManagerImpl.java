package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DocumentBundleDao;
import com.trilasoft.app.model.DocumentBundle;
import com.trilasoft.app.service.DocumentBundleManager;

public class DocumentBundleManagerImpl extends GenericManagerImpl<DocumentBundle, Long> implements DocumentBundleManager{

	DocumentBundleDao documentBundleDao; 
	
	public DocumentBundleManagerImpl(DocumentBundleDao documentBundleDao) {   
        super(documentBundleDao);   
        this.documentBundleDao = documentBundleDao;   
    }
	
	public List findFormsByFormsId(String formsId){
		return documentBundleDao.findFormsByFormsId(formsId);
	}
	
	public List getListByCorpId(String corpId, String tabId){
		return documentBundleDao.getListByCorpId(corpId, tabId);
	}
	
	public String getReportDescAndIdByJrxmlName(String jrxmlName,String corpId){
		return documentBundleDao.getReportDescAndIdByJrxmlName(jrxmlName,corpId);
	}
	
	public void updateFormsIdInDocumentBundle(String reportsId,long id){
		documentBundleDao.updateFormsIdInDocumentBundle(reportsId, id);
	}
	
	public List searchDocumentBundle(String bundleName,String serviceType,String serviceMode,String job,String tableName,String fieldName,String sessionCorpID,String routingType){
		return documentBundleDao.searchDocumentBundle(bundleName,serviceType,serviceMode,job,tableName,fieldName,sessionCorpID,routingType);
	}
	
	public List getRecordForEmail(String routingType,String serviceType,String serviceMode,String job,String fieldName,String sessionCorpID,String jobNumber,String sequenceNumber,String firstName,String lastName,String clickedField){
		return documentBundleDao.getRecordForEmail(routingType,serviceType,serviceMode,job,fieldName,sessionCorpID,jobNumber,sequenceNumber,firstName,lastName,clickedField);
	}
	
	public List findFileFromFileCabinet(String corpId, String jobNumber,String myFileIdTemp){
		return documentBundleDao.findFileFromFileCabinet(corpId,jobNumber,myFileIdTemp);
	}
	
}
