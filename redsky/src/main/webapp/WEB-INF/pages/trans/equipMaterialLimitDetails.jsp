<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>Resource Limit</title> 
<meta name="heading" content="Resource Limit"/>
<style type="text/css">
.text-area {
border:1px solid #219DD1;
}
</style>
<script language="JavaScript">
function closePopup(){
	//alert("hiii");
	document.getElementById('saveBox').style.display="none";
	document.getElementById('searchBox111').style.display="block";
}
	
	function assignResources(){
		var category = document.forms['operationsResourceLimits'].elements['equipMaterialsLimits.category'].value;
		if(category==''){
			alert('Please select the category.');
			return false;
		}
		window.open("assignEquipMaterial.html?category="+category+"&flagType=All&decorator=popup&popup=true","forms","scrollbars=1,resizable=yes, status=1,width=500, height=500,top=0, left=0,menubar=no")
	}
	
	
	function onlyNumberAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==36); 
	} 
	
	function validate(){
		
		//alert("hiookloml");
		if(document.forms['operationsResourceLimits'].elements['equipMaterialsLimits.category'].value==''){
			alert('Please select the category.');
			return false;
		}
		if(document.forms['operationsResourceLimits'].elements['equipMaterialsLimits.resource'].value==''){
			alert('Please select description.');
			return false;
		}
		if(document.forms['operationsResourceLimits'].elements['equipMaterialsLimits.resourceLimit'].value==''){
			alert('Please enter the default resource resourceLimit.');
			return false;
		}
		
	}

	var count=0;
	var shiftCount=0;
	function onlyDoubleValue(evt,targetElement){
	  var keyCode = evt.keyCode || evt.which;
	  targetVal=targetElement.value;
	  if(keyCode==16){
	  shiftCount++;
	  return true;
	  }
	  if(targetVal.indexOf('.')>-1){
	   
	  }
	  else{
	  count=0;
	  }
	  if(((keyCode==110)||((keyCode==190)))  && (count==0) && (targetVal!='')){
	   count++;
	   return true;
	  }
	  else if((keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ||  (keyCode==35) || (keyCode==36)){
	    return true;
	  }
	  else if(((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105)) && shiftCount ==0){
	  return true;
	  }
	  else{
	   return false;
	  }
	}

	function shiftValueUpdate(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	if(keyCode==16){
	shiftCount=0;
	}
	}
	function onlyNumberValue(evt){
		  var keyCode = evt.keyCode || evt.which;
		  if(keyCode==16){
		  shiftCount++;
		  return true;
		  }	  
		  if((keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ||  (keyCode==35) || (keyCode==36)){
		    return true;
		  }
		  else if(((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105)) && shiftCount ==0){
		  return true;
		  }
		  else{
		   return false;
		  }
		}

	function dotNotAllowed(target,fieldName){
		var dotValue=target.value;
		if(dotValue=='.' && dotValue.length==1 ){
			alert("Please enter valid Weight or Volume.");
			document.forms['operationsResourceLimits'].elements[fieldName].value="";
			document.forms['operationsResourceLimits'].elements[fieldName].value.focus();
			return false;
		    }
	     }
	function pickOldValue()
	{
	 document.forms['operationsResourceLimits'].elements['dummsectionName'].value='${operationsResourceLimits.resource}';
	}
</script>
</head>

<s:form id="operationsResourceLimits" name="operationsResourceLimits" action="saveEquipMaterialLimit.html" method="post" validate="true">
<s:hidden name="equipMaterialsLimits.id"/>
<s:hidden name="equipMaterialsCost.id"/>
<s:hidden name="equipMaterialsCost.equipMaterialsId"/>
<%-- <s:hidden name="equipMaterialsLimits.qty"/> --%>
<s:hidden name="hub" value="<%=request.getParameter("hub")%>" />
<s:hidden name="operationsResourceLimits.hubId" value="${hub}"/>
<s:hidden name="dummsectionName" />
<div id="Layer1" style="width:100%"> 
<div id="newmnav">
		  <ul>
		  		<%-- <li><a href="hubList.html"><span>Hub&nbsp;/&nbsp;WH List</span></a></li>
		  		<li><a href="editHubLimitOps.html?hubID=${hub}"><span>Operational&nbsp;/&nbsp;WH Management</span></a></li> --%>
		  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Resource Limit<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  		<%-- <li><a href="resourceControlList.html?hub=${hub}"><span>Resource Limit List</span></a></li> --%>
		  </ul>
</div><div class="spn">&nbsp;</div>
<div style="padding-bottom:0px;!margin-bottom:3px;"></div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%">
	  <tbody>
		  <tr>
		  	<td align="left" class="listwhitetext">
			  	<table class="detailTabLabel" border="0" cellpadding="2" >
					  <tbody>  	
					  	<tr>
					  		<td align="left" height="3px"></td>
					  	</tr>
					  	<%-- <tr>
							<td align="left" width="15px"></td>
					  		<td align="right" width="100px">Hub&nbsp;/&nbsp;WH Selected :</td><td><strong>
					  		 <c:forEach var="entry" items="${distinctHubDescription}">
									<c:if test="${hub==entry.key}">
									<c:out value="${entry.value}" />
									</c:if>
									</c:forEach>
					  		 </strong></td>
						</tr> --%>
						<tr>
							<td align="left" width="15px">
						    <td align="right" width="">Category<font color="red" size="2">*</font></td>
						    <td width="250px"><s:select cssClass="list-menu" name="equipMaterialsLimits.category" list="%{resourceCategory}" cssStyle="width:200px" headerKey="" headerValue="" /></td>
                    		<td align="right" width="15px">Description<font color="red" size="2">*</font></td>
				    		<td style="width:223px;!width:230px;" ><s:textfield name="equipMaterialsLimits.resource"  cssClass="input-text" cssStyle="width:200px;" readonly="false" onkeydown="return onlyDel(event,this);"/> 
				    		<%-- <img align="left" class="openpopup" width="17" height="20" style="float:right;!float:none;!vertical-align:bottom;" onclick="assignResources();changeStatus()" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /> --%></td>	
					  	    <td align="right" width="">Sales&nbsp;Price</td>
						    <td width="250px"><s:textfield cssClass="input-text" name="equipMaterialsCost.salestCost" cssStyle="text-align:right;width:120px;"   /></td>
					  	</tr>
					  	<tr>
					  		<td align="left" width="15px">
					  		<td align="right" width="">Max&nbsp;Limit<font color="red" size="2">*</font></td>
					  		<td width="235px"><s:textfield name="equipMaterialsLimits.maxResourceLimit"  maxlength="15" cssClass="input-text" cssStyle="text-align:right;width:120px;" onkeydown="return onlyNumberValue(event);" onkeyup="shiftValueUpdate(event)"/></td>
					  		<td align="right" width="">Warehouse<font color="red" size="2">*</font></td>
						    <td width="250px"><s:select cssClass="list-menu" name="equipMaterialsLimits.branch" list="%{house}" cssStyle="width:200px" headerKey="" headerValue="" /></td>
					  	    <td align="right" width="">OO&nbsp;Price</td>
						    <td width="250px"><s:textfield cssClass="input-text" name="equipMaterialsCost.ownerOpCost" cssStyle="text-align:right;width:120px;"   /></td>
					  	</tr>
					  	<tr>
					  		<td align="left" width="15px">
					  		<td align="right" width="">Min&nbsp;Limit<font color="red" size="2">*</font></td>
					  		<td width="235px"><s:textfield name="equipMaterialsLimits.minResourceLimit"  maxlength="15" cssClass="input-text" cssStyle="text-align:right;width:120px;" onkeydown="return onlyNumberValue(event);" onkeyup="shiftValueUpdate(event)"/></td>
					  		<td align="right" width="">Available&nbsp;Quantity</td>
						    <td width="250px"><s:textfield cssClass="input-text" name="equipMaterialsLimits.qty" maxlength="15" cssStyle="text-align:right;width:120px;" onkeydown="return onlyNumberValue(event);" onkeyup="shiftValueUpdate(event)"/></td>
					  		<%-- <td align="right" width="">Branch</td>
						    <td width="250px"><s:select cssClass="list-menu" name="equipMaterialsLimits.branch" list="{'Edmonton','Calgary'}" cssStyle="width:200px" headerKey="" headerValue="" /></td> --%>
					  	</tr>
					  	<tr>
					  		<td align="left" width="15px">
					  		<td align="right" width="">Division<font color="red" size="2">*</font></td>
					  		<td width="235px"><s:select cssClass="list-menu" name="equipMaterialsLimits.division" list="{'Domestic','Starline','OMD'}" cssStyle="width:200px" headerKey="" headerValue="" /></td>
					  		<td align="right" width="">Unit&nbsp;Cost</td>
						    <td width="250px"><s:textfield cssClass="input-text" name="equipMaterialsCost.unitCost" cssStyle="text-align:right;width:120px;"  /></td>
					  	</tr>
					 </tbody>
				</table>
			 </td>
		    </tr>		    		  	
		 </tbody>
	</table>
	<table class="detailTabLabel" border="0">
		<tbody> 	
			<tr>
				<td align="left" height="3px"></td>
			</tr>	  	
			<tr>
			<td align="left" width="77"></td>
				<td align="left"><s:submit id="scan" cssClass="cssbutton1" type="button" key="button.save" onclick="return validate();"/></td>
				<c:if test="${hub!='0'}">
		       	<td align="right"><s:reset cssClass="cssbutton1" type="button" key="Reset"/></td>
		       <!-- 	<td align="right"><input type="button" class="cssbutton1" type="button" value="Cancel" onclick="closePopup();"/></td> -->
		       	</c:if>
			</tr>
			<tr>
				<td align="left" height="13px"></td>
			</tr>		  	
		</tbody>
	</table>
	</div>
	<div class="bottom-header"><span></span></div>
</div>
</div>
	
	<%-- <table border="0" width="750px;">
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
				<td valign="top">
				
				</td>
				<td style="width:120px">
				<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${operationsResourceLimits.createdOn}" 
				pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="operationsResourceLimits.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
				<fmt:formatDate value="${operationsResourceLimits.createdOn}" pattern="${displayDateTimeFormat}"/>
				</td>		
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
				<c:if test="${not empty operationsResourceLimits.id}">
					<s:hidden name="operationsResourceLimits.createdBy"/>
					<td style="width:85px"><s:label name="createdBy" value="%{operationsResourceLimits.createdBy}"/></td>
				</c:if>
				<c:if test="${empty operationsResourceLimits.id}">
					<s:hidden name="operationsResourceLimits.createdBy" value="${pageContext.request.remoteUser}"/>
					<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
				</c:if>
				
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
				<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${operationsResourceLimits.updatedOn}" 
				pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="operationsResourceLimits.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
				<td style="width:120px"><fmt:formatDate value="${operationsResourceLimits.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
				<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
				<c:if test="${not empty operationsResourceLimits.id}">
					<s:hidden name="operationsResourceLimits.updatedBy"/>
					<td style="width:85px"><s:label name="updatedBy" value="%{operationsResourceLimits.updatedBy}"/></td>
				</c:if>
				<c:if test="${empty operationsResourceLimits.id}">
					<s:hidden name="operationsResourceLimits.updatedBy" value="${pageContext.request.remoteUser}"/>
					<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
				</c:if>
			</tr>
		</tbody>
	</table> --%>

	<%-- <table class="detailTabLabel" border="0">
		<tbody> 	
			<tr>
				<td align="left" height="3px"></td>
			</tr>	  	
			<tr>
				<td align="left"><s:submit id="scan" cssClass="cssbutton1" type="button" key="button.save" onclick="return validate();"/></td>
				<c:if test="${hub!='0'}">
		       	<td align="right"><s:reset cssClass="cssbutton1" type="button" key="Reset"/></td>
		       	</c:if>
			</tr>		  	
		</tbody>
	</table> --%>
</div>

</s:form>
<c:if test="${hitFlag == '1'}" >
	<c:redirect url="/equipMaterialControlList.html?hub=${hub}"  />
</c:if>
<script type="text/javascript"> 
var f = document.getElementById('operationsResourceLimits'); 
f.setAttribute("autocomplete", "off");
pickOldValue();
 </script>
 <script type="text/javascript">
	var hub = '${hub}';
	 if(hub=='0'){
		window.onload = function()
		{
			//Disable fields
			 	var inputs = document.getElementsByTagName("input");
			    for (var i = 0; i < inputs.length; i++) {
			    		inputs[i].disabled = true;														    	
			    }			    
			    var selects = document.getElementsByTagName("select");
			    for (var i = 0; i < selects.length; i++) {
			    	selects[i].disabled = true;
			    }
			    document.getElementById('openpopup4.img').style.display="none";
		      	document.getElementById("scan").disabled = true;
			}
	}
	</script>