package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.EmailSetupTemplate;


public interface EmailSetupTemplateManager extends GenericManager<EmailSetupTemplate, Long>{

	public List getListByCorpId(String corpId);
	public void updateFormsIdInEmailSetupTemplate(String reportsId,long id);
	public Map<String,String> getTableWithColumnsNameMap();
	public List getListByModuleName(String sessionCorpID,String moduleName,String job,String language,String moveType, String companyDivision,String mode,String routing,String serviceType,String fileNumber,Long id);
	public int getMaxOrderNumber(String sessionCorpID);
	public String findDocsFromFileCabinet(String fileType, String fileId,String sessionCorpID);
	public String testEmailSetupTestConditionQuery(String moduleFieldValue,String othersFieldValue,String sessionCorpID);
	public List getListBySearch(String moduleName,String languageName,String descriptionName,String corpId);
	public List findLastSentMail(String sessionCorpID,String moduleName,String fileNumber,String saveAsVal);
	public List findAllDocsFromFileCabinet(String fileType,String fileId,String sessionCorpID);
}
