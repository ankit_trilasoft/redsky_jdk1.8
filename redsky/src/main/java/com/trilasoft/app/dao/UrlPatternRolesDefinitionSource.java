package com.trilasoft.app.dao;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import org.acegisecurity.Authentication;
import org.acegisecurity.ConfigAttribute;
import org.acegisecurity.ConfigAttributeDefinition;
import org.acegisecurity.SecurityConfig;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.intercept.web.PathBasedFilterInvocationDefinitionMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import com.trilasoft.app.model.UrlPattern;

public class UrlPatternRolesDefinitionSource extends
		PathBasedFilterInvocationDefinitionMap {
	
	private static final Log logger = LogFactory.getLog(UrlPatternRolesDefinitionSource.class);
    private PathMatcher pathMatcher = new AntPathMatcher();
	
    private List requestMap = new CopyOnWriteArrayList();

	
    private UrlPatternDao urlPatternDao;

	public UrlPatternDao getUrlPatternDao() {
		return urlPatternDao;
	}

	public void setUrlPatternDao(UrlPatternDao urlPatternDao) {
		this.urlPatternDao = urlPatternDao;
	}

	public void init() {
		List<UrlPattern> urlPatternsList = urlPatternDao.listUrlPatterns();
		for (UrlPattern pattern : urlPatternsList) {
			String[] urls = pattern.getUrlPattern().split(",");
			for (String url : urls) {
				url=url.trim();
				
				url += "*";
				ConfigAttributeDefinition configDefinition = super.lookupAttributes(url);
				if (configDefinition == null)
					configDefinition = new ConfigAttributeDefinition();

				for (Role role : pattern.getRoles()) {
					ConfigAttribute config = new SecurityConfig(role.getAuthority());
					configDefinition.addConfigAttribute(config);
				}
				//ROLE_ADMIN should always have access to all URLs!
				ConfigAttribute config = new SecurityConfig("ROLE_ADMIN"); 
				configDefinition.addConfigAttribute(config);
				addSecureUrl(url, configDefinition);
				
				/*ConfigAttribute config1 = new SecurityConfig("ROLE_EMPLOYEE"); 
				configDefinition.addConfigAttribute(config1);
				addSecureUrl(url, configDefinition);
				
				ConfigAttribute config2 = new SecurityConfig("ROLE_CORP_ACCOUNT"); 
				configDefinition.addConfigAttribute(config2);
				addSecureUrl(url, configDefinition);*/
			}
		}
	}

	
    

	public ConfigAttributeDefinition lookupAttributes(String url) {
    	if (url.contains(".html")) {
        	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        	if (auth.getPrincipal() instanceof User){
                User user = (User)auth.getPrincipal();
                url = (new StringBuilder ("/").append(user.getCorpID()).append(url)).toString();
        	}
    	}
    	ConfigAttributeDefinition config = null;
        // Strip anything after a question mark symbol, as per SEC-161. See also SEC-321
        int firstQuestionMarkIndex = url.indexOf("?");

        if (firstQuestionMarkIndex != -1) {
            url = url.substring(0, firstQuestionMarkIndex);
        }

        if (isConvertUrlToLowercaseBeforeComparison()) {
            url = url.toLowerCase();

            /*if (logger.isDebugEnabled()) {
                logger.debug("Converted URL to lowercase, from: '" + url + "'; to: '" + url + "'");
            }*/
        }

        Iterator iter = requestMap.iterator();

        while (iter.hasNext()) {
            EntryHolder entryHolder = (EntryHolder) iter.next();

            boolean matched = pathMatcher.match(entryHolder.getAntPath(), url);

           /* if (logger.isDebugEnabled()) {
                logger.debug("Candidate is: '" + url + "'; pattern is " + entryHolder.getAntPath() + "; matched="
                    + matched);
            }*/

            if (matched) {
            	config =  entryHolder.getConfigAttributeDefinition();
            	break;
            }
        }

		//If no permissions have been defined for the URL, ROLE_ADMIN 
		//should still have access
		if (config == null && url.contains(".html")) {
			config = new ConfigAttributeDefinition();
			ConfigAttribute configAttr = new SecurityConfig("ROLE_ADMIN");
			config.addConfigAttribute(configAttr);
			
			/*config = new ConfigAttributeDefinition();
			ConfigAttribute configAttr1 = new SecurityConfig("ROLE_EMPLOYEE");
			config.addConfigAttribute(configAttr1);
			
			config = new ConfigAttributeDefinition();
			ConfigAttribute configAttr2 = new SecurityConfig("ROLE_CORP_ACCOUNT");
			config.addConfigAttribute(configAttr2);*/
		}
		
        return config;
    }
    
    public void addSecureUrl(String antPath, ConfigAttributeDefinition attr) {
        requestMap.add(new EntryHolder(antPath, attr));

        /*if (logger.isDebugEnabled()) {
            logger.debug("Added Ant path: " + antPath + "; attributes: " + attr);
        }*/
    }

    public Iterator getConfigAttributeDefinitions() {
        Set set = new HashSet();
        Iterator iter = requestMap.iterator();

        while (iter.hasNext()) {
            EntryHolder entryHolder = (EntryHolder) iter.next();
            set.add(entryHolder.getConfigAttributeDefinition());
        }

        return set.iterator();
    }
    
	
}