package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;


@Entity
@Table(name = "vatIdentification")
public class VatIdentification extends BaseObject{
	private Long id;
	private String corpID;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String countryCode;
	private String startWith;
	private String endWith;
	private String numberOfBlock;
	private Integer eachBlock;
	private String blockValue;
	private String dataType;
	private String message;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("countryCode", countryCode)
				.append("startWith", startWith).append("endWith", endWith)
				.append("numberOfBlock", numberOfBlock)
				.append("eachBlock", eachBlock)
				.append("blockValue", blockValue).append("dataType", dataType)
				.append("message", message).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof VatIdentification))
			return false;
		VatIdentification castOther = (VatIdentification) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(countryCode, castOther.countryCode)
				.append(startWith, castOther.startWith)
				.append(endWith, castOther.endWith)
				.append(numberOfBlock, castOther.numberOfBlock)
				.append(eachBlock, castOther.eachBlock)
				.append(blockValue, castOther.blockValue)
				.append(dataType, castOther.dataType)
				.append(message, castOther.message).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(createdBy).append(createdOn).append(updatedBy)
				.append(updatedOn).append(countryCode).append(startWith)
				.append(endWith).append(numberOfBlock).append(eachBlock)
				.append(blockValue).append(dataType).append(message)
				.toHashCode();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	@Column
	public String getStartWith() {
		return startWith;
	}
	public void setStartWith(String startWith) {
		this.startWith = startWith;
	}
	@Column
	public String getEndWith() {
		return endWith;
	}
	public void setEndWith(String endWith) {
		this.endWith = endWith;
	}
	
	@Column
	public Integer getEachBlock() {
		return eachBlock;
	}
	public void setEachBlock(Integer eachBlock) {
		this.eachBlock = eachBlock;
	}
	@Column
	public String getBlockValue() {
		return blockValue;
	}
	public void setBlockValue(String blockValue) {
		this.blockValue = blockValue;
	}
	@Column
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	@Column
	public String getNumberOfBlock() {
		return numberOfBlock;
	}
	public void setNumberOfBlock(String numberOfBlock) {
		this.numberOfBlock = numberOfBlock;
	}
	@Column
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
