<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Agent Base List</title>   
    <meta name="heading" content="Agent Base List"/>  
    
<style>
.tab{
border:1px solid #74B3DC;

}
</style> 

</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partner.id}" />
<c:set var="fileID" value="%{partner.id}"/>
<s:hidden name="ppType" id ="ppType" value="AG" />
<c:set var="ppType" value="AG"/>
<s:form id="agentBaseListForm" action="" method="post" validate="true">	
<s:hidden name="partner.id"/>
<div id="newmnav">
				  <ul>
				    <li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=AG" ><span>Agent Detail</span></a></li>
			  		<li><a href="findPartnerProfileList.html?code=${partner.partnerCode}&partnerType=AG&id=${partner.id}"><span>Agent Profile</span></a></li>
			  		<c:if test="${sessionCorpID!='TSFT' }">
			  		<li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=AG&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
			  		</c:if>
			  		<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=AG"><span>Acct Ref</span></a></li>
					<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=AG"><span>Vanline Ref</span></a></li>
			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Base List</span></a></li>
					
					<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
					<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
					<c:if test='${paramValue == "View"}'>
						<li><a href="searchPartnerView.html"><span>Partner List</span></a></li>
					</c:if>
					<c:if test='${paramValue != "View"}'>
						<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
						<%--<c:if test='${partner.latitude == ""  || partner.longitude == "" || partner.latitude == null  || partner.longitude == null}'>
							<li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>	
						</c:if>
						<c:if test='${partner.latitude != "" && partner.longitude != "" && partner.latitude != null && partner.longitude != null}'>
							<li><a href="partnerRateGrids.html?partnerId=${partner.id}"><span>Rate Matrix</span></a></li>
						</c:if> 
						--%><li><a href="partnerReloSvcs.html?id=${partner.id}&partnerType=AG"><span>Services</span></a></li>
					</c:if>
					<c:if test="${partner.partnerPortalActive == true}">
					         <configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
							<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=AG"><span>Portal Users & contacts</span></a></li>
							<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partner.id}&partnerType=AG&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
					       </configByCorp:fieldVisibility>
					</c:if>
			      <div id="AGR" >
			       <li><a href="partnerUsersList.html?id=${partner.id}&partnerType=AG"><span>Portal Users & Contacts</span></a></li>
			  </div>
				  </ul>
		</div>
		<div style="width: 100%" >
		<div class="spn">&nbsp;</div>
		
		</div>
		<div id="content" align="center">
   <div id="liquid-round-top" style="!margin-top:-50px;">
   <div class="top"><span></span></div>
   <div class="center-content">
		<table style="margin-bottom: 3px">
			<tr>
		  		<td align="right" class="listwhitetext">Code</td>
				<td><s:textfield key="partner.partnerCode" required="true" readonly="true" cssClass="text medium"/></td>
				<td align="right" class="listwhitetext" width="70px">Name</td>
				<td><s:textfield key="partner.lastName" required="true" size="35" readonly="true" cssClass="text medium"/></td>
			</tr>
			<tr>
			<td align="left" class="listwhite" height="10"></td>
			</tr>
		</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<div id="Layer1" style="width: 100%;">
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:60px; height:25px" 
        onclick="location.href='<c:url value="/editAgentBase.html?partnerId=${partner.id}"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  

<table  width="100%" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td >				
<s:set name="baseAgent" value="baseAgent" scope="request"/>  

					
				<display:table name="baseAgent" class="table" requestURI="" id="baseAgentList" export="true" defaultsort="1" pagesize="10" style="width: 100%; " >   
					<display:column property="baseCode" sortable="true" style="width: 150px;" 
					href="editAgentBase.html?partnerId=${partner.id}" paramId="id" paramProperty="id" 
					titleKey="agentBase.baseCode" />
					
					<display:column property="description" sortable="true" titleKey="agentBase.description"/>
					<display:column title="Remove" style="width: 15px;">
						<a>
							<img align="middle" title="" onclick="confirmSubmit('${baseAgentList.id}');" style="margin: 0px 0px 0px 8px;" src="images/recyle-trans.gif"/>
						</a>
					</display:column>	 
				    
				    <display:setProperty name="paging.banner.item_name" value="Base Agent"/>   
				    <display:setProperty name="paging.banner.items_name" value="Base Agent"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Base Agent List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Base Agent List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Base Agent List.pdf"/>   
				</display:table>  
				
 			</td>
		</tr>
	</tbody>
</table> 
<table> 
<c:out value="${buttons}" escapeXml="false" /> 

<tr><td style="height:70px; !height:100px"></td></tr></table>
</div>
</s:form> 

<script language="javascript" type="text/javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Base and all related Base SCAC?");
	var did = targetElement;
	if (agree){
		location.href="deleteBase.html?partnerId=${partner.id}&id="+did;
	}else{
		return false;
	}
}
</script>


<script type="text/javascript">   
		try{
			<c:if test="${hitflag == 1}" >
			<c:redirect url="/baseList.html?id=${partner.id}" ></c:redirect>
			</c:if>
		}
		catch(e){}
</script> 
<script type="text/javascript">
try{
	<c:if test="${partnerType=='AG'}">
	var checkRoleAG = 0;
	//Modified By subrat BUG #6471 
	<sec-auth:authComponent componentId="module.tab.partner.AgentDEV">
		checkRoleAG  = 14;
	</sec-auth:authComponent>
	if(checkRoleAG < 14){
		document.getElementById('AGR').style.visibility = 'visible';
	}else{
		document.getElementById('AGR').style.visibility = 'hidden';
	}
	</c:if>
}catch(e){}

</script>
