package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.AccountContactDao;
import com.trilasoft.app.model.AccountContact;

public class AccountContactDaoHibernate extends GenericDaoHibernate<AccountContact, Long> implements AccountContactDao {

	public AccountContactDaoHibernate() {   
        super(AccountContact.class);   
    }
	
	public List getAccountContacts(String partnerCode,String corpID){
		return getHibernateTemplate().find("from AccountContact where corpID = '"+corpID+"' AND partnerCode=?",partnerCode);
	}

	public List getNotesForContract(String partnerCode, String corpID) {
		return getHibernateTemplate().find("from Notes where notetype='Account' and notesId='"+partnerCode+"' and corpID='"+corpID+"'");
	}

	public List getRelatedNotesForContract(String corpID,String partnerCode) {
		return getHibernateTemplate().find("from Notes where notetype='AccountContact' and corpID='"+corpID+"' and notesId='"+partnerCode+"'");
	}

	public List getRelatedNotesForProfile(String corpID, String partnerCode) {
		return getHibernateTemplate().find("from Notes where notetype='AccountProfile' and corpID='"+corpID+"' and notesId='"+partnerCode+"'");
	}
}
