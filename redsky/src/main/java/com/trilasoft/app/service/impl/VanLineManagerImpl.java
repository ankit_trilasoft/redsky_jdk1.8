package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.service.VanLineManager;
import com.trilasoft.app.dao.VanLineDao;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.VanLine;
import com.trilasoft.app.model.SubcontractorCharges;


public class VanLineManagerImpl  extends GenericManagerImpl<VanLine, Long> implements VanLineManager {

	VanLineDao vanLineDao; 

    public VanLineManagerImpl(VanLineDao vanLineDao) { 
        super(vanLineDao); 
        this.vanLineDao = vanLineDao; 
    } 
    
    public List<VanLine> vanLineList(){
    	return vanLineDao.vanLineList();
    }
    
    public List<VanLine> searchVanLine(String agent,Date weekEnding, String glType, String reconcileStatus) {
		return vanLineDao.searchVanLine(agent, weekEnding,  glType,  reconcileStatus);
	}
	public int updateDownloadByFileName(String fileName, String sessionCorpID){
		return  vanLineDao.updateDownloadByFileName(fileName, sessionCorpID);
	}
    public List<VanLine> showLine(String regNum) {
		return vanLineDao.showLine(regNum);
	}
	
	 public List  getVanLineListWithReconcile(String agent,Date weekending,String sessionCorpID){
    	return vanLineDao.getVanLineListWithReconcile(agent, weekending,sessionCorpID);
    }
	 public int updateDownloadByID(String accID, String sessionCorpID){
		 return vanLineDao.updateDownloadByID(accID, sessionCorpID);
	 }
    public List getVanLineListWithoutReconcile(String agent,Date weekEnding,String sessionCorpID){
    	return vanLineDao.getVanLineListWithoutReconcile(agent, weekEnding,sessionCorpID);
    }
    
    public int vanLineListForReconcile(String agent, Date weekEnding,BigDecimal vanlineMinimumAmount,boolean automaticReconcile,String sessionCorpID, String uvlBillToCode){
    	return vanLineDao.vanLineListForReconcile(agent, weekEnding,vanlineMinimumAmount,automaticReconcile,sessionCorpID,  uvlBillToCode);
    }
    public int updateAccByID(String accID,String accVenderDetails,String venderDetailsList, String sessionCorpID)
    {
    	return vanLineDao.updateAccByID(accID,accVenderDetails, venderDetailsList ,sessionCorpID);
    }

    public List<SubcontractorCharges> splitCharge(Long id) {
		return vanLineDao.splitCharge(id);
	}
    public int updateAccByFileName(String fileName,String fileNoVenderDetails,String venderDetailsList, String sessionCorpID){
    	return vanLineDao.updateAccByFileName(fileName,fileNoVenderDetails,venderDetailsList, sessionCorpID);
    }
    public List<SubcontractorCharges> totalAmountSum(Long id) {
		return vanLineDao.totalAmountSum(id);
	}
    public List getDownloadXMLList(String shipNum, String sessionCorpID,String fileName){
    	return vanLineDao.getDownloadXMLList(shipNum, sessionCorpID, fileName);
    }
    public List getProcessedXMLList(String shipNum, String sessionCorpID){
    	return vanLineDao.getProcessedXMLList(shipNum, sessionCorpID);
    }
	public List chargeAllocation(Long id) {
		return vanLineDao.chargeAllocation(id);
	}
	
	public List getSerachedSOList(String shipNum, String regNum,String lastName, String firstName, String sessionCorpID){
		return vanLineDao.getSerachedSOList(shipNum, regNum, lastName, firstName, sessionCorpID);
	}
	public List getVanLineCodeCompDiv(String corpID){
		return vanLineDao.getVanLineCodeCompDiv(corpID);
	}
	
	public List getWeekendingList(String agent, String corpId){
		return vanLineDao.getWeekendingList(agent, corpId);
	}
	 public List getMiscellaneousSettlement(String agent,Date weekending,String sessionCorpID){
	    	return vanLineDao.getMiscellaneousSettlement(agent, weekending,sessionCorpID);
	    }
	 public List getCategoryWithSettlementList(String agent,Date weekending,String sessionCorpID){
	    	return vanLineDao.getCategoryWithSettlementList(agent, weekending,sessionCorpID);
	    }
	 public List getRegNumList(String vanRegNum,String agent,Date weekending){
		 return vanLineDao.getRegNumList(vanRegNum,agent, weekending);
	 }
	 
	 public List vanLineCategoryRegNum(String vanRegNum,String agent,String weekending,String sessionCorpID){
		 return vanLineDao.vanLineCategoryRegNum(vanRegNum,agent, weekending,sessionCorpID);
	 }
	 public List vanLineCategoryRegNumberListMethod(String category,String vanRegNum,String agent,String weekEnding ){
		 return vanLineDao.vanLineCategoryRegNumberListMethod(category,vanRegNum,agent, weekEnding);
	 }
	   
	public List getRegNumVanline(String agent, Date weekEnding){
	   return vanLineDao.getRegNumVanline(agent, weekEnding);
	  }
	public List getShipNumber(String vanRegNum){
		return vanLineDao.getShipNumber(vanRegNum);
	}
	public List<SystemDefault> findsysDefault(String corpID){
    	return vanLineDao.findsysDefault(corpID);
    }

	public String getCompanyCode(String accountingAgent, String sessionCorpID) {
		return vanLineDao.getCompanyCode(accountingAgent, sessionCorpID);
	}
	public List getvanLineExtract(String agent,Date weekending,String sessionCorpID){
		return vanLineDao.getvanLineExtract(agent,weekending,sessionCorpID);
		
	}

	public int updateAccountLineWithReconcile(String agent, Date weekEnding, BigDecimal vanlineMinimumAmount, boolean automaticReconcile, String sessionCorpID, String uvlBillToCode) {
		return vanLineDao.updateAccountLineWithReconcile(agent, weekEnding, vanlineMinimumAmount, automaticReconcile, sessionCorpID, uvlBillToCode);
		
	}

	public List checkInvoiceDateForWithReconcile(String agent, Date weekEnding, BigDecimal vanlineMinimumAmount, boolean automaticReconcile, String sessionCorpID, String uvlBillToCode) {
		return vanLineDao.checkInvoiceDateForWithReconcile(agent, weekEnding, vanlineMinimumAmount, automaticReconcile, sessionCorpID,  uvlBillToCode);
	}

	public int updateVanLineWithReconcile(Long id, String loginUser) { 
		return vanLineDao.updateVanLineWithReconcile(id,loginUser);
	}

	public void updateAccRecInvoiceNumberWithReconcile(Long id, String loginUser, String recAccDateToFormat, String recInvoiceNumber1) {
		vanLineDao.updateAccRecInvoiceNumberWithReconcile(id, loginUser, recAccDateToFormat, recInvoiceNumber1);
		
	}

	public int ownBillingVanLineListForReconcile(String agent, Date weekEnding,BigDecimal vanlineMinimumAmount,boolean automaticReconcile,String sessionCorpID, String uvlBillToCode,  BigDecimal vanlineMaximumAmount, String vanlineExceptionChargeCode) {
		return vanLineDao.ownBillingVanLineListForReconcile(agent, weekEnding, vanlineMinimumAmount, automaticReconcile, sessionCorpID, uvlBillToCode, vanlineMaximumAmount, vanlineExceptionChargeCode);
	}

	public int updateAccountLineWithOwnBillingReconcile(String agent, Date weekEnding, BigDecimal vanlineMinimumAmount, boolean automaticReconcile, String sessionCorpID, String uvlBillToCode, BigDecimal vanlineMaximumAmount, String vanlineExceptionChargeCode) {
		return vanLineDao.updateAccountLineWithOwnBillingReconcile(agent, weekEnding, vanlineMinimumAmount, automaticReconcile, sessionCorpID, uvlBillToCode, vanlineMaximumAmount, vanlineExceptionChargeCode);
	}

	public List checkInvoiceOwnBillingDateForWithReconcile(String agent, Date weekEnding, BigDecimal vanlineMinimumAmount, boolean automaticReconcile, String sessionCorpID, String uvlBillToCode, BigDecimal vanlineMaximumAmount, String vanlineExceptionChargeCode) {
		return vanLineDao.checkInvoiceOwnBillingDateForWithReconcile(agent, weekEnding, vanlineMinimumAmount, automaticReconcile, sessionCorpID,  uvlBillToCode, vanlineMaximumAmount, vanlineExceptionChargeCode);
	}

	public void updateAccRecInvoiceNumberWithOwnBillingReconcile(String idList, String loginUser, String recAccDateTo, String recInvoiceNumber1, String creditInvoiceNumber, Long soId, String sessionCorpID) {
		vanLineDao.updateAccRecInvoiceNumberWithOwnBillingReconcile(idList, loginUser, recAccDateTo, recInvoiceNumber1, creditInvoiceNumber, soId, sessionCorpID);
		
	}
	public Map<String, String> getChargeCodeList(String billingContract,String corpId){
		return vanLineDao.getChargeCodeList(billingContract,corpId);
	}

	public String findCompanyDivision(String agent, String sessionCorpID) {
		return vanLineDao.findCompanyDivision(agent, sessionCorpID);
	}

	public List findVanLineAgent(String agent, String sessionCorpID) {
		return vanLineDao.findVanLineAgent(agent, sessionCorpID);
	}

	public List getOwnBillingRegNumVanline(String agent, Date weekEnding) { 
		return vanLineDao.getOwnBillingRegNumVanline(agent, weekEnding);
	}

	public List getDSTROwnBillingRegNumVanline(String agent, Date weekEnding, String sessionCorpID) {
		return vanLineDao.getDSTROwnBillingRegNumVanline(agent, weekEnding, sessionCorpID);
	}

	public List getOwnBillingRegNumVanlineAmount(String agent, Date weekEnding, String regnum, String sessionCorpID) {
		return vanLineDao.getOwnBillingRegNumVanlineAmount(agent, weekEnding, regnum, sessionCorpID);
	}

	public List findByOwnerAndContact(String driverID, String sessionCorpID) {
		return vanLineDao.findByOwnerAndContact(driverID, sessionCorpID);
	}
	public Map getvenderCodeDetails(String shipNum,String corpId){	
		return vanLineDao.getvenderCodeDetails(shipNum, corpId);
	}
}
