<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ include file="/common/taglibs.jsp"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
  
        
    </head>
<body<decorator:getProperty property="body.id" writeEntireProperty="true"/><decorator:getProperty property="body.class" writeEntireProperty="true"/>>

        <div id="modalContent">
			<authz:authorize ifNotGranted="ROLE_ANONYMOUS">
	                <decorator:body/>
			</authz:authorize>
			<authz:authorize ifAnyGranted="ROLE_ANONYMOUS">
	                <%@ include file="/common/messages.jsp" %>
	                <decorator:body/>
			</authz:authorize>
    	</div>
</body>
</html>

