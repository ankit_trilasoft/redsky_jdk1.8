package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table ( name="operationsdailylimits")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class OperationsDailyLimits extends BaseObject{
	private String corpID;
	private Long id;
	private String hubID;
	private Date workDate;
	private String dailyLimits;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
    private BigDecimal dailyMaxEstWt;
	private BigDecimal dailyMaxEstVol;
	private String comment;	
	private Long parentId;
	private String dailyCrewCapacityLimit;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id).append("hubID", hubID)
				.append("workDate", workDate)
				.append("dailyLimits", dailyLimits)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("dailyMaxEstWt", dailyMaxEstWt)
				.append("dailyMaxEstVol", dailyMaxEstVol)
				.append("comment", comment)
				.append("parentId", parentId)
				.append("dailyCrewCapacityLimit", dailyCrewCapacityLimit)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof OperationsDailyLimits))
			return false;
		OperationsDailyLimits castOther = (OperationsDailyLimits) other;
		return new EqualsBuilder().append(corpID, castOther.corpID)
				.append(id, castOther.id).append(hubID, castOther.hubID)
				.append(workDate, castOther.workDate)
				.append(dailyLimits, castOther.dailyLimits)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(dailyMaxEstWt, castOther.dailyMaxEstWt)
				.append(dailyMaxEstVol, castOther.dailyMaxEstVol)
				.append(comment, castOther.comment)
				.append(parentId, castOther.parentId)
				.append(dailyCrewCapacityLimit, castOther.dailyCrewCapacityLimit)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(hubID)
				.append(workDate).append(dailyLimits).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(dailyMaxEstWt).append(dailyMaxEstVol).append(comment)
				.append(parentId)
				.append(dailyCrewCapacityLimit)
				.toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Date getWorkDate() {
		return workDate;
	}
	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=10)
	public String getDailyLimits() {
		return dailyLimits;
	}
	public void setDailyLimits(String dailyLimits) {
		this.dailyLimits = dailyLimits;
	}

	public String getHubID() {
		return hubID;
	}

	public void setHubID(String hubID) {
		this.hubID = hubID;
	}
	@Column(length=20)
	public BigDecimal getDailyMaxEstWt() {
		return dailyMaxEstWt;
	}
	public void setDailyMaxEstWt(BigDecimal dailyMaxEstWt) {
		this.dailyMaxEstWt = dailyMaxEstWt;
	}
	@Column(length=20)
	public BigDecimal getDailyMaxEstVol() {
		return dailyMaxEstVol;
	}
	public void setDailyMaxEstVol(BigDecimal dailyMaxEstVol) {
		this.dailyMaxEstVol = dailyMaxEstVol;
	}
	@Column(length=200)
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public String getDailyCrewCapacityLimit() {
		return dailyCrewCapacityLimit;
	}
	public void setDailyCrewCapacityLimit(String dailyCrewCapacityLimit) {
		this.dailyCrewCapacityLimit = dailyCrewCapacityLimit;
	}
		
}
