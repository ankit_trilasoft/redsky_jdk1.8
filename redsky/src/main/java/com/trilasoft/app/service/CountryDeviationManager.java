package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CountryDeviation;

public interface CountryDeviationManager extends GenericManager<CountryDeviation, Long> {

	public List findDeviation(String charge, String deviationType);
	public List findMaximumId();
	public void delSelectedDevitaion(String delId);
	public List findAllDeviation(String chargeId, String sessionCorpID,	String contract);

}
