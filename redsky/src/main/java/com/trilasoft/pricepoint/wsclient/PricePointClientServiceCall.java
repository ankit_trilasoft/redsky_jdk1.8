package com.trilasoft.pricepoint.wsclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;

import com.trilasoft.app.webapp.json.JSONException;
import com.trilasoft.app.webapp.json.JSONObject;
import com.trilasoft.app.webapp.json.JSONStringer;


public class PricePointClientServiceCall {
	public static String putDataToServer(String obj){
		

        // The ObjectRocket URL for the API operation that you are attempting to complete.
        String postUrl = "http://www.griprocure.com/api/quotes";
        // Create a HTTP Post Request.
        HttpPost postRequest = new HttpPost(postUrl);
        StringEntity postParamsEntity;
        StringBuilder sb = new StringBuilder();
		try {
			postParamsEntity = new StringEntity(obj);
			 postParamsEntity.setContentType("application/x-www-form-urlencoded");
			 
			 // Add the post parameters to the post request.
	            postRequest.setEntity(postParamsEntity);
	         // Create a HTTP Client.
	            DefaultHttpClient httpClient = new DefaultHttpClient();
	            
	            //Execute the HTTP POST 
	            System.out.println("Executing HTTP Post...\n");
	            try {
					HttpResponse response = httpClient.execute(postRequest);
					
					// Check the HTTP status of the post.
		            if (response.getStatusLine().getStatusCode() != 200 && response.getStatusLine().getStatusCode() != 201) {
		                throw new RuntimeException("Failed: HTTP error code: "
		                    + response.getStatusLine().getStatusCode());
		            }

		            // Create a reader to read in the HTTP post results.
		            BufferedReader br = new BufferedReader(
		                            new InputStreamReader((response.getEntity().getContent())));
					String output;
					System.out.println("Output from Server .... \n");
					while ((output = br.readLine()) != null) {
						System.out.println(output);
						 sb.append(output + "\n");
					}
					
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       

		return sb.toString();
	}
	public static String getSupplementCharges(String obj){
		  String postUrl = "http://www.griprocure.com/api/supplement_charges";
	        // Create a HTTP Post Request.
	        HttpPost postRequest = new HttpPost(postUrl);
	        StringEntity postParamsEntity;
	        StringBuilder sb = new StringBuilder();
	        
	         try {
				postParamsEntity = new StringEntity(obj);
				 postParamsEntity.setContentType("application/x-www-form-urlencoded");
				 // Add the post parameters to the post request.
				    postRequest.setEntity(postParamsEntity);
				 // Create a HTTP Client.
				    DefaultHttpClient httpClient = new DefaultHttpClient();
				    //Execute the HTTP POST 
				    System.out.println("Executing HTTP Post...\n");
					HttpResponse response = httpClient.execute(postRequest);
					
					// Check the HTTP status of the post.
				    if (response.getStatusLine().getStatusCode() != 200 && response.getStatusLine().getStatusCode() != 201) {
				        throw new RuntimeException("Failed: HTTP error code: "
				            + response.getStatusLine().getStatusCode());
				    }

				    // Create a reader to read in the HTTP post results.
				    BufferedReader br = new BufferedReader(
				                    new InputStreamReader((response.getEntity().getContent())));
					String output;
					System.out.println("Output from Server .... \n");
					while ((output = br.readLine()) != null) {
						System.out.println(output);
						 sb.append(output + "\n");
					}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return sb.toString();
		
	}
	public static String detailedQuotes(String obj){
		String postUrl = "http://www.griprocure.com/api/detailed_quotes";
        // Create a HTTP Post Request.
        HttpPost postRequest = new HttpPost(postUrl);
        StringEntity postParamsEntity;
        StringBuilder sb = new StringBuilder();
        
         try {
			postParamsEntity = new StringEntity(obj);
			 postParamsEntity.setContentType("application/x-www-form-urlencoded");
			 // Add the post parameters to the post request.
			    postRequest.setEntity(postParamsEntity);
			 // Create a HTTP Client.
			    DefaultHttpClient httpClient = new DefaultHttpClient();
			    //Execute the HTTP POST 
			    System.out.println("Executing HTTP Post...\n");
				HttpResponse response = httpClient.execute(postRequest);
				
				// Check the HTTP status of the post.
			    if (response.getStatusLine().getStatusCode() != 200 && response.getStatusLine().getStatusCode() != 201) {
			        throw new RuntimeException("Failed: HTTP error code: "
			            + response.getStatusLine().getStatusCode());
			    }

			    // Create a reader to read in the HTTP post results.
			    BufferedReader br = new BufferedReader(
			                    new InputStreamReader((response.getEntity().getContent())));
				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {
					System.out.println(output);
					 sb.append(output + "\n");
				}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb.toString();
		
	}
	public static String saveQuotesDetails(String obj){
		String postUrl = "http://www.griprocure.com/api/quote/save";
        // Create a HTTP Post Request.
        HttpPost postRequest = new HttpPost(postUrl);
        StringEntity postParamsEntity;
        StringBuilder sb = new StringBuilder();
        
         try {
			postParamsEntity = new StringEntity(obj);
			 postParamsEntity.setContentType("application/x-www-form-urlencoded");
			 // Add the post parameters to the post request.
			    postRequest.setEntity(postParamsEntity);
			 // Create a HTTP Client.
			    DefaultHttpClient httpClient = new DefaultHttpClient();
			    //Execute the HTTP POST 
			    System.out.println("Executing HTTP Post...\n");
				HttpResponse response = httpClient.execute(postRequest);
				
				// Check the HTTP status of the post.
			    if (response.getStatusLine().getStatusCode() != 200 && response.getStatusLine().getStatusCode() != 201) {
			        throw new RuntimeException("Failed: HTTP error code: "
			            + response.getStatusLine().getStatusCode());
			    }

			    // Create a reader to read in the HTTP post results.
			    BufferedReader br = new BufferedReader(
			                    new InputStreamReader((response.getEntity().getContent())));
				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {
					System.out.println(output);
					 sb.append(output + "\n");
				}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb.toString();
		
	}
	public static String readQuotesDetails(String obj){
		String postUrl = "http://www.griprocure.com/api/quote/read";
        // Create a HTTP Post Request.
        HttpPost postRequest = new HttpPost(postUrl);
        StringEntity postParamsEntity;
        StringBuilder sb = new StringBuilder();
        
         try {
			postParamsEntity = new StringEntity(obj);
			 postParamsEntity.setContentType("application/x-www-form-urlencoded");
			 // Add the post parameters to the post request.
			    postRequest.setEntity(postParamsEntity);
			 // Create a HTTP Client.
			    DefaultHttpClient httpClient = new DefaultHttpClient();
			    //Execute the HTTP POST 
			    System.out.println("Executing HTTP Post...\n");
				HttpResponse response = httpClient.execute(postRequest);
				
				// Check the HTTP status of the post.
			    if (response.getStatusLine().getStatusCode() != 200 && response.getStatusLine().getStatusCode() != 201) {
			        throw new RuntimeException("Failed: HTTP error code: "
			            + response.getStatusLine().getStatusCode());
			    }

			    // Create a reader to read in the HTTP post results.
			    BufferedReader br = new BufferedReader(
			                    new InputStreamReader((response.getEntity().getContent())));
				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {
					System.out.println(output);
					 sb.append(output + "\n");
				}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb.toString();
		
	}
	public static String udateQuotesDetails(String obj){
		String postUrl = "http://www.griprocure.com/api/quote/update";
        // Create a HTTP Post Request.
        HttpPost postRequest = new HttpPost(postUrl);
        StringEntity postParamsEntity;
        StringBuilder sb = new StringBuilder();
        
         try {
			postParamsEntity = new StringEntity(obj);
			 postParamsEntity.setContentType("application/x-www-form-urlencoded");
			 // Add the post parameters to the post request.
			    postRequest.setEntity(postParamsEntity);
			 // Create a HTTP Client.
			    DefaultHttpClient httpClient = new DefaultHttpClient();
			    //Execute the HTTP POST 
			    System.out.println("Executing HTTP Post...\n");
				HttpResponse response = httpClient.execute(postRequest);
				
				// Check the HTTP status of the post.
			    if (response.getStatusLine().getStatusCode() != 200 && response.getStatusLine().getStatusCode() != 201) {
			        throw new RuntimeException("Failed: HTTP error code: "
			            + response.getStatusLine().getStatusCode());
			    }

			    // Create a reader to read in the HTTP post results.
			    BufferedReader br = new BufferedReader(
			                    new InputStreamReader((response.getEntity().getContent())));
				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {
					System.out.println(output);
					 sb.append(output + "\n");
				}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb.toString();
		
	}

}
