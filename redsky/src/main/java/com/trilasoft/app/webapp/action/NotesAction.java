package com.trilasoft.app.webapp.action;
 
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.action.BaseAction;
import org.directwebremoting.util.HitMonitor;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;

import com.trilasoft.app.model.AccountContact;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.AuditTrail;
import com.trilasoft.app.model.Carton;
import com.trilasoft.app.model.CheckListResult;
import com.trilasoft.app.model.Claim;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.model.CreditCard;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DsFamilyDetails;
import com.trilasoft.app.model.Mss;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.AccountProfile;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.Payroll;
import com.trilasoft.app.model.ReminderJob;
import com.trilasoft.app.model.MyMessage;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.ToDoResult;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.Vehicle;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.AccountContactManager;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.AuditTrailManager;
import com.trilasoft.app.service.CartonManager;
import com.trilasoft.app.service.CheckListResultManager;
import com.trilasoft.app.service.ClaimManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ContainerManager;
import com.trilasoft.app.service.CreditCardManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DsFamilyDetailsManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.MssManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.MessageManager;
import com.trilasoft.app.service.AccountProfileManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.PayrollManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.ToDoResultManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.VehicleManager;
import com.trilasoft.app.service.WorkTicketManager;

public class NotesAction extends BaseAction {

	private NotesManager notesManager; 

	private CustomerFileManager customerFileManager;

	private ServiceOrderManager serviceOrderManager;

	private WorkTicketManager workTicketManager;

	private ClaimManager claimManager;

	private MessageManager messageManager;

	private List notess;
	
	private List customerFiles;
	private List<SystemDefault> sysDefaultDetail;
	private Long id;

	private Long id1;

	private String noteFor;

	private String noteForNextPrev;

	private String noteFrom;

	private Long maxNotesId;

	private String remindMe;

	private String jobFor;

	private Notes notes;

	private CustomerFile customerFile;

	private ServiceOrder serviceOrder;

	private WorkTicket workTicket;

	private Claim claim;

	private CreditCard creditCard;

	private MyMessage myMessage;

	private List refMasters;

	private List users;

	private List notesNextPrevList;

	private RefMasterManager refMasterManager;

	private Map<String, String> notestatus;
	 
	private Map<String, String> uvlmemocategory;
	private Map<String, String> uvlmemoxfer;

	private Map<String, String> forwardNoteStatus;

	private Map<String, String> notetype;

	private Map<String, String> notesubtype;

	private Map<String, String> notesActivity;
	
	private Map<String, String> notesubtypecontact;
	
	private Map<String, String> all_user;

	private List notesubtypeList;

	private Scheduler sched;

	private Calendar reminderDate;

	private String remindTime;

	private String reminderForUser;

	private String subType;

	private String popup;

	private String maxId;

	private String minId;

	private String countId;

	private String maxIdIsRal;

	private String minIdIsRal;

	private String countIdIsRal;

	private Long soIdNum;

	private String seqNm;

	private long tempID;

	private String sessionCorpID;

	private ServicePartnerManager servicePartnerManager;

	private AccountLineManager accountLineManager;

	private ContainerManager containerManager;

	private CartonManager cartonManager;

	private VehicleManager vehicleManager;

	private ServicePartner servicePartner;

	private AccountLine accountLine;

	private Container container;

	private Carton carton;

	private Vehicle vehicle;

	private String idNote;

	private Date forwardDateUpdate;

	private String remindTimeUpdate;

	private String signatureNote;

	private List activityDateList;

	private String newDefaultDate;

	private String activityDate;

	private String dateStatus;

	private String forwardToUser;

	private String remindInterval;

	private String notesFirstName;

	private String notesLastName;

	private CompanyManager companyManager;

	private AccountProfile accountProfile;

	private AccountContact accountContact;

	private CreditCardManager creditCardManager;

	private Set<Role> userRole;
	private String userType;
	private String activeStatus="";


	
	private String notesId;

	private PartnerPrivate partnerPrivate;
	
	private PartnerPublic partnerPublic;

	private PartnerPrivateManager partnerPrivateManager;

	private List partnerAlertList;

	private PayrollManager payrollManager;

	private Payroll payroll;
	
	private MyFileManager myFileManager;
	
	public List myFileList; 
	public String checkCF;
	private String imageId; 

	public String linkFile;
	public List linkFileList;
	public List linkedToList= new ArrayList();
	public Map<String, String> notetypeNotForCF;
	public String activity;
	private Long noteId;
	private Map<String, String> issueType;
	private Map<String, String> grading;
	private Map<String, String> supplier;
    private String checkAgent;
    private String bookingAgentFlag="";
	private String networkAgentFlag="";
	private String originAgentFlag="";
	private String subOriginAgentFlag="";
	private String destAgentFlag="";
	private String subDestAgentFlag="";
	
	private String checkFlagBA="";
	private String checkFlagNA="";
	private String checkFlagOA="";
	private String checkFlagSOA="";
	private String checkFlagDA="";
	private String checkFlagSDA="";

	private String bookingAgFlag;
	private String networkAgFlag;
	private String originAgFlag="";
	private String subOriginAgFlag;
	private String destAgFlag;
	private String subDestAgFlag;
	private String controlFlag;
	
	private AuditTrail auditTrail;
	private AuditTrailManager auditTrailManager;
	private String customerNumber;
	private String fileNumber;
	private Long PPID ;
	private MssManager mssManager;
	
	private String NetwokAccess;
	
	private String notesComp = "No";
	private Company company; 
	private String voxmeIntergartionFlag;
	private String mmValidation = "NO";
	private Map issueCausedBy;
	private  Map<String, String> roleCausedBy;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;	
	private String oiJobList;
	private String agentcorpID;
	private DsFamilyDetails dsFamilyDetails;
	private DsFamilyDetailsManager dsFamilyDetailsManager;
	private String accId;
	public NotesAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		userRole = user.getRoles();
		userType=user.getUserType();
	}
	
	static final Logger logger = Logger.getLogger(NotesAction.class);
	Date currentdate = new Date();
	
	public void prepare() throws Exception {
		/** This Method is calling before initialize the class. */
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
		if(getRequest().getParameter("ajax") == null){
			 getComboList(sessionCorpID);
			 notestatus = refMasterManager.findByParameter(sessionCorpID, "NOTESTATUS");
			 forwardNoteStatus = refMasterManager.findByParameter(sessionCorpID, "FWDSTATUS");
			 notetype = refMasterManager.findByParameter(sessionCorpID, "NOTETYPE");
			 notetypeNotForCF = refMasterManager.findByParameterNotCF(sessionCorpID,"NOTETYPE"); 
			 notesubtype = refMasterManager.findByParameter(sessionCorpID, "NOTESUBTYPE");
			 all_user = refMasterManager.findUserNotes(sessionCorpID, "ALL_USER");
			 remindIntervals = refMasterManager.findByParameter(sessionCorpID, "RMDINTRVL");
		}
	
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		 if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }
		 }

	@SkipValidation
	public String getNotesList() {
		//getComboList(sessionCorpID);
		if(sessionCorpID.equalsIgnoreCase("TSFT")){
		serviceOrder = serviceOrderManager.getForOtherCorpid(id);
		}else{
		serviceOrder = serviceOrderManager.get(id);	
		}
		customerFile = serviceOrder.getCustomerFile();
		
		notess = notesManager.countNotesByShipNumAndSubType(notesId,subType);
		return SUCCESS;
	}

	// Getting the notes list depending on the module
	@SkipValidation
	public String list() {
		//System.out.println(subType);
		getComboList(sessionCorpID);
		//if(subType == null){
		//}
		subType="";
		if (noteFor.equals("agentQuotes")) {
			notess = notesListFromCustomer(subType);
		}
		if (noteFor.equals("agentQuotesSO")) {
			notess = notesListFromServiceOrder(subType);
		}
		if (noteFor.equals("CustomerFile")) {
			notess = notesListFromCustomer(subType);
		}
		if (noteFor.equals("ServiceOrder")) {
			notess = notesListFromServiceOrder(subType);
		}
		if (noteFor.equals("WorkTicket")) {
			notess = notesListFromWorkTicket(subType);
		}
		if (noteFor.equals("Claim")) {
			notess = notesListFromClaim(subType);
		}
		if (noteFor.equals("CreditCard")) {
			notess = notesListFromCreditCard(subType);
		}
		if (noteFor.equals("ServicePartner")) {
			notess = notesListFromServicePartner(subType);
		}
		if (noteFor.equals("AccountLine")) {
			notess = notesListFromAccountLine(subType);
		}
		if (noteFor.equals("Carton")) {
			notess = notesListFromCarton(subType);
		}
		if (noteFor.equals("Container")) {
			notess = notesListFromContainer(subType);
		}
		if (noteFor.equals("Vehicle")) {
			notess = notesListFromVehicle(subType);
		}
		if (noteFor.equals("Partner")) {
			notess = notesListFromPartner(subType);
		}
		if (noteFor.equals("Crew")) {
			notess = notesListFromPayroll(subType);
		}
		if (noteFor.equals("MSS")) {
			notess = notesListFromMss(subType);
		}
		if (noteFor.equals("OperationsIntelligence")) {
			notess = notesListFromOI(subType);
		}
		if (noteFor.equals("AgentInvoiceLine")) {
			notess = notesListFromAgentInvoiceLine(subType);
		}
		if (noteFor.equals("FamilyDetails")) {
			notess = notesListFromFamilyDetails(subType);
		}
		//
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		if(company!=null){
			if(company.getVoxmeIntegration()!=null){
				voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: voxmeIntergartionFlag "+voxmeIntergartionFlag);
				
			}
			 if(company.getEnableMobileMover() && company.getMmStartDate()!=null && company.getMmStartDate().compareTo(getZeroTimeDate(new Date()))<=0 ){
				mmValidation= "Yes";
				if(company.getMmEndDate()!=null && company.getMmEndDate().compareTo(getZeroTimeDate(new Date()))<0 ){
					mmValidation= "No";
				}
			}
			}
		//
		return SUCCESS;
	}
	
	
	
	private String accportalFlag;
	// Getting the notes list depending on the module
	@SkipValidation
	public String allList() {
		//System.out.println(subType);
		getComboList(sessionCorpID);
		//if(subType == null){
		subType = "";
		//}
		 activeStatus="no";
		if (noteFor.equals("agentQuotes")) {
			notess = notesListFromCustomer(subType);
			
		}
		if (noteFor.equals("agentQuotesSO")) {
			notess = notesListFromServiceOrder(subType);
		}
		if (noteFor.equals("CustomerFile")) {
			notess = notesListFromCustomer(subType);
		}
		if (noteFor.equals("ServiceOrder")) {
			notess = notesListFromServiceOrder(subType);
		}
		if (noteFor.equals("WorkTicket")) {
			notess = notesListFromWorkTicket(subType);
		}
		if (noteFor.equals("Claim")) {
			notess = notesListFromClaim(subType);
		}
		if (noteFor.equals("CreditCard")) {
			notess = notesListFromCreditCard(subType);
		}
		if (noteFor.equals("ServicePartner")) {
			notess = notesListFromServicePartner(subType);
		}
		if (noteFor.equals("AccountLine")) {
			notess = notesListFromAccountLine(subType);
		}
		if (noteFor.equals("Carton")) {
			notess = notesListFromCarton(subType);
		}
		if (noteFor.equals("Container")) {
			notess = notesListFromContainer(subType);
		}
		if (noteFor.equals("Vehicle")) {
			notess = notesListFromVehicle(subType);
		}
		if (noteFor.equals("Partner")) {
			notess = notesListFromPartner(subType);
		}
		if (noteFor.equals("Crew")) {
			notess = notesListFromPayroll(subType);
		}
		if (noteFor.equals("MSS")) {
			notess = notesListFromMss(subType);
		}
		if (noteFor.equals("FamilyDetails")) {
			notess = notesListFromFamilyDetails(subType);
		}
		//
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		if(company!=null){
			if(company.getVoxmeIntegration()!=null){
				voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: voxmeIntergartionFlag "+voxmeIntergartionFlag);
				
			}
			 if(company.getEnableMobileMover() && company.getMmStartDate()!=null && company.getMmStartDate().compareTo(getZeroTimeDate(new Date()))<=0 ){
				mmValidation= "Yes";
				if(company.getMmEndDate()!=null && company.getMmEndDate().compareTo(getZeroTimeDate(new Date()))<0 ){
					mmValidation= "No";
				}
			}
			}
		//
		return SUCCESS;
	}
	
	@SkipValidation
	private List notesListFromPayroll(String subType) {
		payroll = payrollManager.get(id);
		if(activeStatus.equals("no")){
			notess = notesManager.getALLListByNotesId(String.valueOf(payroll.getId()), "Crew");
			}else{
		notess = notesManager.getListByNotesId(String.valueOf(payroll.getId()), "Crew");
			}
		return notess;
	}
	@SkipValidation
	private List notesListFromMss(String subType) {
		mss = mssManager.get(id);	
		notess = notesManager.getListForMss(mss.getId(), subType);
		return notess;
	}
	
	
	@SkipValidation
	private List notesListFromOI(String subType) {
		serviceOrder = serviceOrderManager.get(id);	
		notess = notesManager.getListForOI(serviceOrder.getId(), subType);
		return notess;
	}
	
	@SkipValidation
	private List notesListFromPartner(String subType2) {
		List partnerList = partnerPrivateManager.findPartnerCode(notesId);
	    if (partnerList!=null && !partnerList.isEmpty()) {
	      partnerPrivate = (PartnerPrivate) partnerList.get(0);
	    }
	    if(activeStatus.equals("no")){
			notess = notesManager.getALLListByNotesId(notesId, subType);
			}else{
	    notess = notesManager.getListByNotesId(notesId, subType2);
			}
		return notess;
	}

	//Getting the relavent notes list depending on the module
	@SkipValidation
	public String listRaleventNotes() {
		getComboList(sessionCorpID);
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		if(company!=null){
			if(company.getVoxmeIntegration()!=null){
				voxmeIntergartionFlag=company.getVoxmeIntegration().toString();			
				
			}
			 if(company.getEnableMobileMover() && company.getMmStartDate()!=null && company.getMmStartDate().compareTo(getZeroTimeDate(new Date()))<=0 ){
				mmValidation= "Yes";
				if(company.getMmEndDate()!=null && company.getMmEndDate().compareTo(getZeroTimeDate(new Date()))<0 ){
					mmValidation= "No";
				}
			}
			}
		subType = "";
		if (noteFrom.equals("CustomerFile")) {
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
			customerFile = customerFileManager.getForOtherCorpid(id);
			}else{
			customerFile = customerFileManager.get(id);	
			}
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getListByCustomerNumber(customerFile.getSequenceNumber(), subType);
		}
		if (noteFrom.equals("agentQuotes")) {
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
			customerFile = customerFileManager.getForOtherCorpid(id);
			}else{
				customerFile = customerFileManager.get(id);	
			}
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getListByCustomerNumber(customerFile.getSequenceNumber(), subType);
		}
		if (noteFrom.equals("ServiceOrder")) {
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
			serviceOrder = serviceOrderManager.getForOtherCorpid(id);
			}else{
				serviceOrder = serviceOrderManager.get(id);	
			}
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getListByCustomerNumber(customerFile.getSequenceNumber(), subType);
		}
		if (noteFrom.equals("agentQuotesSO")) {
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
			serviceOrder = serviceOrderManager.getForOtherCorpid(id);
			}else{
			serviceOrder = serviceOrderManager.get(id);	
			}
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getListByCustomerNumber(customerFile.getSequenceNumber(), subType);
		}
		if (noteFrom.equals("WorkTicket")) {
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
			workTicket = workTicketManager.getForOtherCorpid(id);
			}else{
			workTicket = workTicketManager.get(id);
			}
			serviceOrder = workTicket.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getListByCustomerNumber(customerFile.getSequenceNumber(), subType);
		}
		if (noteFrom.equals("Claim")) {
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
			claim = claimManager.getForOtherCorpid(id);
			}else{
			claim = claimManager.get(id);
			}
			serviceOrder = claim.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getListByCustomerNumber(customerFile.getSequenceNumber(), subType);
		}
		if (noteFrom.equals("Partner")) {
			partnerPrivate = partnerPrivateManager.get(id);
			notess = notesManager.getListByCustomerNumber(partnerPrivate.getPartnerCode(), subType);
			
		}
		if (noteFrom.equals("Crew")) {
			payroll = payrollManager.get(id);
			notess = notesManager.getListByNotesId(String.valueOf(payroll.getId()), subType);
		}	
		return SUCCESS;
	}
	public Date getZeroTimeDate(Date dt) {
	    Date res = dt;
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(dt);
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    res = calendar.getTime();
	    return res;
	}
	@SkipValidation
	public String listPartnerNotes() {
		getComboList(sessionCorpID);
		try{
		if (noteFrom.equals("CustomerFile")) {
			customerFile = customerFileManager.getForOtherCorpid(id);
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getPartnerNotesListByCustomerNumber(customerFile);
		}
		if (noteFrom.equals("agentQuotes")) {
			customerFile = customerFileManager.getForOtherCorpid(id);
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getPartnerNotesListByCustomerNumber(customerFile);
		}
		if (noteFrom.equals("ServiceOrder")) {
			serviceOrder = serviceOrderManager.getForOtherCorpid(id);
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getPartnerNotesListByCustomerNumber(customerFile);
		}
		if (noteFrom.equals("agentQuotesSO")) {
			serviceOrder = serviceOrderManager.getForOtherCorpid(id);
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getPartnerNotesListByCustomerNumber(customerFile);
		}
		if (noteFrom.equals("WorkTicket")) {
			workTicket = workTicketManager.get(id);
			serviceOrder = workTicket.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getPartnerNotesListByCustomerNumber(customerFile);
		}
		if (noteFrom.equals("Claim")) {
			claim = claimManager.get(id);
			serviceOrder = claim.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getPartnerNotesListByCustomerNumber(customerFile);
		}
		if (noteFrom.equals("FamilyDetails")) {
			dsFamilyDetails = dsFamilyDetailsManager.get(id);
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getPartnerNotesListByCustomerNumber(customerFile);
		}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		/*if (noteFrom.equals("Partner")) {
			partnerPrivate = partnerPrivateManager.get(id);
			notess = notesManager.getPartnerNotesListByCustomerNumber(partnerPrivate.getPartnerCode());
		}*/

		return SUCCESS;
	}
	
	
	@SkipValidation
	public List notesListFromCustomer(String subType) {
		if(sessionCorpID.equalsIgnoreCase("TSFT")){
		customerFile = customerFileManager.getForOtherCorpid(id);
		}else{
		customerFile = customerFileManager.get(id);	
		}
		getRequest().setAttribute("soLastName", customerFile.getLastName());
		if(activeStatus.equals("no")){
			notess = notesManager.getALLListByNotesId(customerFile.getSequenceNumber(), subType);
			}else{
		notess = notesManager.getListByNotesId(customerFile.getSequenceNumber(), subType);
			}
		return notess;
	}

	@SkipValidation
	public List notesListFromServiceOrder(String subType) {
		if(sessionCorpID.equalsIgnoreCase("TSFT")){
		serviceOrder = serviceOrderManager.getForOtherCorpid(id);
		}else{
		serviceOrder = serviceOrderManager.get(id);	
		}
		customerFile = serviceOrder.getCustomerFile();
		getRequest().setAttribute("soLastName", customerFile.getLastName());
		if(activeStatus.equals("no")){
		notess = notesManager.getALLListByNotesId(serviceOrder.getShipNumber(), subType);
		}else{
			notess = notesManager.getListByNotesId(serviceOrder.getShipNumber(), subType);
		}
		return notess;
	}

	@SkipValidation
	public List notesListFromWorkTicket(String subType) {
		workTicket = workTicketManager.get(id);
		serviceOrder = workTicket.getServiceOrder();
		customerFile = serviceOrder.getCustomerFile();
		getRequest().setAttribute("soLastName", customerFile.getLastName());
		if(activeStatus.equals("no")){
			notess = notesManager.getALLListByNotesId(workTicket.getTicket().toString(), subType);
			}else{
		notess = notesManager.getListByNotesId(workTicket.getTicket().toString(), subType);
			}
		return notess;
	}

	@SkipValidation
	public List notesListFromClaim(String subType) {
		if(sessionCorpID.equalsIgnoreCase("TSFT")){
		claim = claimManager.getForOtherCorpid(id);
		}else{
			claim = claimManager.get(id);	
		}
		serviceOrder = claim.getServiceOrder();
		customerFile = serviceOrder.getCustomerFile();
		getRequest().setAttribute("soLastName", customerFile.getLastName());
		if(activeStatus.equals("no")){
			notess = notesManager.getALLListByNotesId(String.valueOf(claim.getClaimNumber()), subType);
			}else{
		notess = notesManager.getListByNotesId(String.valueOf(claim.getClaimNumber()), subType);
			}
		return notess;
	}
	@SkipValidation
	public List notesListFromFamilyDetails(String subType) {
		if(sessionCorpID.equalsIgnoreCase("TSFT")){
		      dsFamilyDetails = dsFamilyDetailsManager.getForOtherCorpid(id);
			}else{
			dsFamilyDetails = dsFamilyDetailsManager.get(id);
			}
		if(activeStatus.equals("no")){
			notess = notesManager.getALLListByFamilyDetails(dsFamilyDetails.getCustomerFileId().toString(), subType);

			}else{
	       	notess = notesManager.getListByNotesId(dsFamilyDetails.getId().toString()+dsFamilyDetails.getCustomerFileId(), subType);
			}
			
			return notess;
	}


	private List notesListFromCreditCard(String subType) {
		creditCard = creditCardManager.get(id);
		serviceOrder = (ServiceOrder) creditCardManager.getServiceOrder(creditCard.getShipNumber(), sessionCorpID).get(0);
		customerFile = serviceOrder.getCustomerFile();
		getRequest().setAttribute("soLastName", customerFile.getLastName());
		if(activeStatus.equals("no")){
			notess = notesManager.getALLListByNotesId(String.valueOf(creditCard.getId()), subType);
			}else{
		notess = notesManager.getListByNotesId(String.valueOf(creditCard.getId()), subType);
			}
		return notess;
	}

	// After Feb 27, 2008 Enhancement

	@SkipValidation
	public List notesListFromServicePartner(String subType) {
		servicePartner = servicePartnerManager.get(id);
		serviceOrder = servicePartner.getServiceOrder();
		customerFile = serviceOrder.getCustomerFile();
		getRequest().setAttribute("soLastName", customerFile.getLastName());
		if(activeStatus.equals("no")){
			notess = notesManager.getALLListByNotesId(String.valueOf(servicePartner.getId()), subType);
			}else{
		notess = notesManager.getListByNotesId(String.valueOf(servicePartner.getId()), subType);
			}
		return notess;
	}

	@SkipValidation
	public List notesListFromAccountLine(String subType) {
		if(userType.equalsIgnoreCase("AGENT"))
		accountLine = accountLineManager.getForOtherCorpid(id);
		else
		accountLine = accountLineManager.get(id);
		serviceOrder = accountLine.getServiceOrder();
		customerFile = serviceOrder.getCustomerFile();
		getRequest().setAttribute("soLastName", customerFile.getLastName());
		if(accportalFlag!=null && accportalFlag.equals("true"))
		{	
			subType="EstimateDetail";
		
			if(activeStatus.equals("no")){
		
			notess = notesManager.getALLListByNotesId(String.valueOf(accountLine.getId()), subType);
			}else{
		notess = notesManager.getListByNotesId(String.valueOf(accountLine.getId()), subType);
			}}
		
		else
		{
			if(activeStatus.equals("no")){
				
				notess = notesManager.getALLListByNotesId(String.valueOf(accountLine.getId()), subType);
				}else{
			notess = notesManager.getListByNotesId(String.valueOf(accountLine.getId()), subType);
				}
			
		}
		return notess;
	}

	@SkipValidation
	public List notesListFromCarton(String subType) {
		carton = cartonManager.get(id);
		serviceOrder = carton.getServiceOrder();
		customerFile = serviceOrder.getCustomerFile();
		getRequest().setAttribute("soLastName", customerFile.getLastName());
		if(activeStatus.equals("no")){
			notess = notesManager.getALLListByNotesId(String.valueOf(carton.getId()), subType);
			}else{
		notess = notesManager.getListByNotesId(String.valueOf(carton.getId()), subType);
			}
		return notess;
	}

	@SkipValidation
	public List notesListFromContainer(String subType) {
		container = containerManager.get(id);
		serviceOrder = container.getServiceOrder();
		customerFile = serviceOrder.getCustomerFile();
		getRequest().setAttribute("soLastName", customerFile.getLastName());
		if(activeStatus.equals("no")){
			notess = notesManager.getALLListByNotesId(String.valueOf(container.getId()), subType);
			}else{
		notess = notesManager.getListByNotesId(String.valueOf(container.getId()), subType);
			}
		return notess;
	}

	@SkipValidation
	public List notesListFromVehicle(String subType) {
		vehicle = vehicleManager.get(id);
		serviceOrder = vehicle.getServiceOrder();
		customerFile = serviceOrder.getCustomerFile();
		getRequest().setAttribute("soLastName", customerFile.getLastName());
		if(activeStatus.equals("no")){
			notess = notesManager.getALLListByNotesId(String.valueOf(vehicle.getId()), subType);
			}else{
		notess = notesManager.getListByNotesId(String.valueOf(vehicle.getId()), subType);
			}
		return notess;
	}

	@SkipValidation
	public String notesListFromPartner() {
		accountProfile = accountProfileManager.get(id);
		notess = notesManager.getListByAccountNotesId(accountProfile.getPartnerCode(), noteFor, accountProfile.getType(), accountProfile.getId());
		return SUCCESS;
	}

	

	@SkipValidation
	public List notesListFromAgentInvoiceLine(String subType) {
		accountLine = accountLineManager.get(id);
		serviceOrder = accountLine.getServiceOrder();
		customerFile = serviceOrder.getCustomerFile();
		getRequest().setAttribute("soLastName", customerFile.getLastName());
		if(activeStatus.equals("no")){
			notess = notesManager.getALLListByNotesId(String.valueOf(accountLine.getId()), subType);
			}else{
		notess = notesManager.getListByNotesId(String.valueOf(accountLine.getId()), subType);
			}
		return notess;
	}

	//After Feb 27, 2008 Enhancement
	//Function for getting the values for all comboboxes. 
	private static Map<String, String> remindIntervals;
	private String systemDefaulttransDoc=""; 
	@SkipValidation
	public String getComboList(String corpId) {
		notestatus = refMasterManager.findByParameter(corpId, "NOTESTATUS");
		forwardNoteStatus = refMasterManager.findByParameter(corpId, "FWDSTATUS");
		notetype = refMasterManager.findByParameter(corpId, "NOTETYPE");
		notetypeNotForCF = refMasterManager.findByParameterNotCF(sessionCorpID,"NOTETYPE");
		notesubtype = refMasterManager.findByParameter(corpId, "NOTESUBTYPE");
		all_user = refMasterManager.findUserNotes(corpId, "ALL_USER");
		remindIntervals = refMasterManager.findByParameter(corpId, "RMDINTRVL");
		notesActivity = refMasterManager.findByParameter(corpId, "NOTESACTIVITY");
		notesubtypecontact=refMasterManager.findByParameter(corpId, "NOTESUBTYPECONTACT");
		issueType= refMasterManager.findByParameter(corpId, "ISSUE_TYPE");
		grading= refMasterManager.findByParameter(corpId, "GRADING");
		 uvlmemocategory=refMasterManager.findByParameter(sessionCorpID, "UVLMemoCategory");
		 uvlmemoxfer=refMasterManager.findByParameter(sessionCorpID, "UVLMemoXfer");
		   sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(agentcorpID);
		     if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					systemDefaulttransDoc=systemDefault.getTransDoc();
				}
			}
		     if(systemDefaulttransDoc==null){
		    	 systemDefaulttransDoc=""; 
		     }
		if(id1!=null){
		supplier=notesManager.findSupplier(id1);
		}else{
			supplier=notesManager.findSupplier(id);
		}
		
		try{
			roleCausedBy = refMasterManager.findByParameter(sessionCorpID, "RoleCausedBy");
			if(roleCausedBy==null || roleCausedBy.size()<0){
				roleCausedBy=new HashMap<String, String>();
			}
			}catch(NullPointerException np){
				np.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}
		
		/*remindIntervals = new ArrayList();
		 remindIntervals.add("0 minutes");remindIntervals.add("2");remindIntervals.add("5");remindIntervals.add("10");
		 remindIntervals.add("15");remindIntervals.add("30");
		 remindIntervals.add("1 hours");remindIntervals.add("2 hours");remindIntervals.add("3 hours");
		 remindIntervals.add("4 hours");remindIntervals.add("5 hours");remindIntervals.add("6 hours");
		 remindIntervals.add("7 hours");remindIntervals.add("8 hours");remindIntervals.add("9 hours");
		 remindIntervals.add("10 hours");remindIntervals.add("11 hours");remindIntervals.add("12 hours");
		 remindIntervals.add("13 hours");remindIntervals.add("14 hours");remindIntervals.add("15 hours");
		 remindIntervals.add("16 hours");remindIntervals.add("17 hours");remindIntervals.add("18 hours");
		 remindIntervals.add("19 hours");remindIntervals.add("20 hours");remindIntervals.add("21 hours");
		 remindIntervals.add("22 hours");remindIntervals.add("23 hours");*/
		return SUCCESS;
	}

	// Functions for edit the notes depending on the module

	@SkipValidation
	public String editNewNoteForPartnerPrivate() {
		getComboList(sessionCorpID);
			memoIntegrationFlag="N";
			checkCF="PP";
		if (id1 != null) {
			linkFileList=notesManager.getMyFileList(id);
			if(linkFileList!=null && (!(linkFileList.isEmpty())) && linkFileList.get(0)!=null){
				linkFile = linkFileList.get(0).toString();
				if( linkFile!= null && !(linkFile.equals(""))){
					myFileList = myFileManager.getMyFileList(linkFile,sessionCorpID);
				}
			}
			partnerPrivate = partnerPrivateManager.getForOtherCorpid(id1);
			notes = notesManager.get(id);
			noteFor="Partner";
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
			maxId = notesManager.findMaximumId(notes.getNotesId()).get(0).toString();
			minId = notesManager.findMinimumId(notes.getNotesId()).get(0).toString();
			countId = notesManager.findCountId(notes.getNotesId()).get(0).toString();
			maxIdIsRal = notesManager.findMaximumIdIsRal(notes.getCustomerNumber()).get(0).toString();
			minIdIsRal = notesManager.findMinimumIdIsRal(notes.getCustomerNumber()).get(0).toString();
			countIdIsRal = notesManager.findCountIdIsRal(notes.getCustomerNumber()).get(0).toString();
			notesFirstName = partnerPrivate.getFirstName();
			notesLastName = partnerPrivate.getLastName();
		} else {
			partnerPrivate = partnerPrivateManager.getForOtherCorpid(id);
			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(sessionCorpID);
			notes.setNotesId(partnerPrivate.getPartnerCode());
			notes.setCustomerNumber(partnerPrivate.getPartnerCode());
			notes.setNoteType("Partner");
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			notes.setRemindInterval("15");
			notes.setNoteSubType(subType);
			notes.setNotesKeyId(partnerPrivate.getId());
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Partner", "NOTESUBTYPE");
			notesFirstName = partnerPrivate.getFirstName();
			notesLastName = partnerPrivate.getLastName();
		}
		noteForNextPrev = "Partner";
		noteFor="Partner";
		List signatureNoteList = notesManager.userSignatureNotes(getRequest().getRemoteUser());
		signatureNote = signatureNoteList.get(0).toString();
		return SUCCESS;
	}
	private String memoIntegrationFlag="";
	@SkipValidation
	public String editNewNotes() {
		try{
		checkCF="CF";
		getComboList(sessionCorpID);
		if (id != null) {
			linkFileList=notesManager.getMyFileList(id);
			if(linkFileList!=null && (!(linkFileList.isEmpty())) && linkFileList.get(0)!=null){
				linkFile = linkFileList.get(0).toString();
				if( linkFile!= null && !(linkFile.equals(""))){
					myFileList = myFileManager.getMyFileList(linkFile,sessionCorpID);
				}
			}
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
			customerFile = customerFileManager.getForOtherCorpid(id1);
			}else{
				customerFile = customerFileManager.get(id1);	
			}
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = notesManager.get(id);
			if(notes.getFollowUpFor()!=null && !(notes.getFollowUpFor().trim().equalsIgnoreCase("")) && all_user !=null && (!(all_user.containsValue(notes.getFollowUpFor().trim().toUpperCase())))){
				List aliasNameList=userManager.findUserByUsername(notes.getFollowUpFor());
				if(((aliasNameList!=null)&&!aliasNameList.isEmpty())&&aliasNameList.get(0)!=null){
				String aliasName=aliasNameList.get(0).toString();
		        String coordCode=notes.getFollowUpFor().toUpperCase();
				String alias = aliasName.toUpperCase();
				if(!all_user.containsValue(coordCode)){
					all_user.put(coordCode,alias );
				} 
				}
				}
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
			maxId = notesManager.findMaximumId(notes.getNotesId()).get(0).toString();
			minId = notesManager.findMinimumId(notes.getNotesId()).get(0).toString();
			countId = notesManager.findCountId(notes.getNotesId()).get(0).toString();
			maxIdIsRal = notesManager.findMaximumIdIsRal(notes.getCustomerNumber()).get(0).toString();
			minIdIsRal = notesManager.findMinimumIdIsRal(notes.getCustomerNumber()).get(0).toString();
			countIdIsRal = notesManager.findCountIdIsRal(notes.getCustomerNumber()).get(0).toString();
			} else {
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
			customerFile = customerFileManager.getForOtherCorpid(id1);
			}else{
				customerFile = customerFileManager.get(id1);	
			}
			//Long noteId = Long.parseLong(notesManager.findIdByNotesId(notesId,sessionCorpID).get(0).toString());
			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(sessionCorpID);
			if(userType.trim().equalsIgnoreCase("AGENT")) {
			notes.setCorpID(customerFile.getCorpID());
			}
			notes.setNotesId(customerFile.getSequenceNumber());
			notes.setCustomerNumber(customerFile.getSequenceNumber());
			if(noteId!=null && !noteId.toString().equalsIgnoreCase("")){
				Notes notesOld = notesManager.get(noteId);
				notes.setNoteType(notesOld.getNoteType());
				notes.setNoteSubType(notesOld.getNoteSubType());
				notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notesOld.getNoteType() , "NOTESUBTYPE");
			}else{
				notes.setNoteType("Customer File");
				notes.setNoteSubType(subType);
				notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Customer File"  , "NOTESUBTYPE");
			}
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			if(userType.trim().equalsIgnoreCase("ACCOUNT")) {
				if(sessionCorpID.equalsIgnoreCase("UTSI")){
				  notes.setForwardDate(new Date());
				  if(customerFile.getCoordinator()!=null && !(customerFile.getCoordinator().equalsIgnoreCase("")) && customerFile.getControlFlag().trim().equalsIgnoreCase("C")){
					List aliasNameList=userManager.findUserByUsername(customerFile.getCoordinator());
					if(((aliasNameList!=null)&&!aliasNameList.isEmpty())&&aliasNameList.get(0)!=null){
					String aliasName=aliasNameList.get(0).toString();
			        String coordCode=customerFile.getCoordinator().toUpperCase();
					String alias = aliasName.toUpperCase();
					if(!all_user.containsValue(coordCode)){
						all_user.put(coordCode,alias );
					} 
					}
					notes.setFollowUpFor(customerFile.getCoordinator().toUpperCase());
				  }
				}
				else
				{
					notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());	
				}
			}else{
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			}
			notes.setRemindInterval("15");
			notes.setNotesKeyId(customerFile.getId());
				}
		notes.setBillToCode(customerFile.getBillToCode());
		notes.setBookingAgentCode(customerFile.getBookingAgentCode());
		if(systemDefaulttransDoc.indexOf(customerFile.getJob())>-1){
			memoIntegrationFlag="Y";
		}else{
			memoIntegrationFlag="N";
		}				  				
		notesFirstName = customerFile.getFirstName();
		notesLastName = customerFile.getLastName();
		linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
		noteForNextPrev = "CustomerFile";
		List signatureNoteList = notesManager.userSignatureNotes(getRequest().getRemoteUser());
		signatureNote = signatureNoteList.get(0).toString();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return SUCCESS;
	}
private String checkSO;
	@SkipValidation
	public String editNewNotesForServiceOrder() {
		try {
		getComboList(sessionCorpID);
		accountNotesFor = "SO";
		checkCF="SO";
		checkSO="SO";		
		if (id1 != null) {
			linkFileList=notesManager.getMyFileList(id);
			// linkFile=notesManager.getMyFileList(id);
			if(linkFileList!=null && (!(linkFileList.isEmpty())) && linkFileList.get(0)!=null){
				linkFile = linkFileList.get(0).toString();
				if( linkFile!= null && !(linkFile.equals(""))){
					myFileList = myFileManager.getMyFileList(linkFile,sessionCorpID);
				}
			}
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
			serviceOrder = serviceOrderManager.getForOtherCorpid(id1);
			}else{
				serviceOrder = serviceOrderManager.get(id1);	
			}
			trackingStatus = trackingStatusManager.get(id1);
			List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
			if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
				checkAgent = defCheckedShipNumber.get(0).toString();
				if(checkAgent.equalsIgnoreCase("BA")){
					bookingAgentFlag ="BA";
				}else if(checkAgent.equalsIgnoreCase("NA")){
					networkAgentFlag ="NA";
				}else if(checkAgent.equalsIgnoreCase("OA")){
					originAgentFlag ="OA";
				}else if(checkAgent.equalsIgnoreCase("SOA")){
					subOriginAgentFlag ="SOA";
				}else if(checkAgent.equalsIgnoreCase("DA")){
					destAgentFlag ="DA";
				}else if(checkAgent.equalsIgnoreCase("SDA")){
					subDestAgentFlag ="SDA";
				} 
			  }
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = notesManager.get(id);
			if(!(notes.getNotesId().contains(notes.getCorpID())))
			 checkCF="";
			if(notes.getFollowUpFor()!=null && !(notes.getFollowUpFor().trim().equalsIgnoreCase("")) && all_user !=null && (!(all_user.containsValue(notes.getFollowUpFor().trim().toUpperCase())))){
				List aliasNameList=userManager.findUserByUsername(notes.getFollowUpFor());
				if(((aliasNameList!=null)&&!aliasNameList.isEmpty())&&aliasNameList.get(0)!=null){
				String aliasName=aliasNameList.get(0).toString();
		        String coordCode=notes.getFollowUpFor().toUpperCase();
				String alias = aliasName.toUpperCase();
				if(!all_user.containsValue(coordCode)){
					all_user.put(coordCode,alias );
				} 
				}
				}
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
			maxId = notesManager.findMaximumId(notes.getNotesId()).get(0).toString();
			minId = notesManager.findMinimumId(notes.getNotesId()).get(0).toString();
			countId = notesManager.findCountId(notes.getNotesId()).get(0).toString();
			maxIdIsRal = notesManager.findMaximumIdIsRal(notes.getCustomerNumber()).get(0).toString();
			minIdIsRal = notesManager.findMinimumIdIsRal(notes.getCustomerNumber()).get(0).toString();
			countIdIsRal = notesManager.findCountIdIsRal(notes.getCustomerNumber()).get(0).toString();
		} else {
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
			serviceOrder = serviceOrderManager.getForOtherCorpid(id);
			}else{
				serviceOrder = serviceOrderManager.get(id);
			}
			trackingStatus = trackingStatusManager.get(id);
			List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
			if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
				checkAgent = defCheckedShipNumber.get(0).toString();
				if(checkAgent.equalsIgnoreCase("BA")){
					bookingAgentFlag ="BA";
				}else if(checkAgent.equalsIgnoreCase("NA")){
					networkAgentFlag ="NA";
				}else if(checkAgent.equalsIgnoreCase("OA")){
					originAgentFlag ="OA";
				}else if(checkAgent.equalsIgnoreCase("SOA")){
					subOriginAgentFlag ="SOA";
				}else if(checkAgent.equalsIgnoreCase("DA")){
					destAgentFlag ="DA";
				}else if(checkAgent.equalsIgnoreCase("SDA")){
					subDestAgentFlag ="SDA";
				} 
			}
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(serviceOrder.getCorpID());
			notes.setNotesId(serviceOrder.getShipNumber());
			notes.setCustomerNumber(customerFile.getSequenceNumber());
			notes.setNoteType("Service Order");
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			if(userType.trim().equalsIgnoreCase("ACCOUNT")) {
				if(sessionCorpID.equalsIgnoreCase("UTSI")){
					DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		           Date today = new Date();
		           Date todayWithZeroTime = formatter.parse(formatter.format(today));
				 notes.setForwardDate(todayWithZeroTime);
				  if(serviceOrder.getCoordinator()!=null && !(serviceOrder.getCoordinator().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(serviceOrder.getCoordinator());
					if(((aliasNameList!=null)&&!aliasNameList.isEmpty())&&aliasNameList.get(0)!=null){
					String aliasName=aliasNameList.get(0).toString();
			        String coordCode=serviceOrder.getCoordinator().toUpperCase();
					String alias = aliasName.toUpperCase();
					if(!all_user.containsValue(coordCode)){
						all_user.put(coordCode,alias );
					} 
					}
					notes.setFollowUpFor(serviceOrder.getCoordinator().toUpperCase());
				  }
				}
				else
				{
					notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());	
				}
			}else{
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			}


			notes.setRemindInterval("15");
			//notes.setForwardDate(new Date());
			notes.setNoteSubType(subType);
			notes.setNotesKeyId(serviceOrder.getId());
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Service Order", "NOTESUBTYPE");
		}
		if(systemDefaulttransDoc.indexOf(serviceOrder.getJob())>-1){
			memoIntegrationFlag="Y";
		}else{
			memoIntegrationFlag="N";
		}				  		
		
		List shipNumberList = myFileManager.getShipNumberList(serviceOrder.getShipNumber());
		if(shipNumberList!=null && !shipNumberList.isEmpty()){
		Iterator it = shipNumberList.iterator();
         while (it.hasNext()) {
        	String agentFlag=(String)it.next();
        	if(agentFlag.equalsIgnoreCase("BA")){
				bookingAgFlag ="BA";
			}else if(agentFlag.equalsIgnoreCase("NA")){
				networkAgFlag ="NA";
			}else if(agentFlag.equalsIgnoreCase("OA")){
				originAgFlag ="OA";
			}else if(agentFlag.equalsIgnoreCase("SOA")){
				subOriginAgFlag ="SOA";
			}else if(agentFlag.equalsIgnoreCase("DA")){
				destAgFlag ="DA";
			}else if(agentFlag.equalsIgnoreCase("SDA")){
				subDestAgFlag ="SDA";
			} 
         }
	    }
		notes.setBillToCode(serviceOrder.getBillToCode());
		notes.setBookingAgentCode(serviceOrder.getBookingAgentCode());
		notesFirstName = customerFile.getFirstName();
		notesLastName = customerFile.getLastName();
		linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
		supplier=notesManager.findSupplier(customerFile.getId());
		System.out.println("supplier is : "+supplier);
		noteForNextPrev = "ServiceOrder";
		List signatureNoteList = notesManager.userSignatureNotes(getRequest().getRemoteUser());
		signatureNote = signatureNoteList.get(0).toString();
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@SkipValidation
	public String editNewNotesForWorkTicket() {
		getComboList(sessionCorpID);
		if (id1 != null) {
			workTicket = workTicketManager.get(id1);
			serviceOrder = workTicket.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = notesManager.get(id);
			if(notes.getNoteType()!=null && notes.getNoteType().equals("Customer File"))
				checkCF="CF";
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
			maxId = notesManager.findMaximumId(notes.getNotesId()).get(0).toString();
			minId = notesManager.findMinimumId(notes.getNotesId()).get(0).toString();
			countId = notesManager.findCountId(notes.getNotesId()).get(0).toString();
			maxIdIsRal = notesManager.findMaximumIdIsRal(notes.getCustomerNumber()).get(0).toString();
			minIdIsRal = notesManager.findMinimumIdIsRal(notes.getCustomerNumber()).get(0).toString();
			countIdIsRal = notesManager.findCountIdIsRal(notes.getCustomerNumber()).get(0).toString();
		} else {
			workTicket = workTicketManager.get(id);
			serviceOrder = workTicket.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(sessionCorpID);
			notes.setNotesId(workTicket.getTicket().toString());
			notes.setCustomerNumber(customerFile.getSequenceNumber());
			notes.setNoteType("Work Tickets");
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			notes.setRemindInterval("15");
			notes.setNoteSubType(subType);
			notes.setNotesKeyId(workTicket.getId());
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Work Tickets", "NOTESUBTYPE");
			//notes.setForwardDate(new Date());
		}
		if(systemDefaulttransDoc.indexOf(serviceOrder.getJob())>-1){
			memoIntegrationFlag="Y";
		}else{
			memoIntegrationFlag="N";
		}				  		
		notes.setBillToCode(serviceOrder.getBillToCode());
		notes.setBookingAgentCode(serviceOrder.getBookingAgentCode());
		notesFirstName = customerFile.getFirstName();
		notesLastName = customerFile.getLastName();
		linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
		noteForNextPrev = "WorkTicket";
		List signatureNoteList = notesManager.userSignatureNotes(getRequest().getRemoteUser());
		signatureNote = signatureNoteList.get(0).toString();
		return SUCCESS;
	}

	@SkipValidation
	public String editNewNotesForClaim() {
		getComboList(sessionCorpID);
		if (id1 != null) {
			claim = claimManager.get(id1);
			serviceOrder = claim.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = notesManager.get(id);
			if(notes.getNoteType()!=null && notes.getNoteType().equals("Customer File"))
				checkCF="CF";
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
			//notes.setUpdatedOn(new Date());
		} else {
			claim = claimManager.get(id);
			serviceOrder = claim.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(sessionCorpID);
			notes.setNotesId(String.valueOf(claim.getClaimNumber()));
			notes.setCustomerNumber(customerFile.getSequenceNumber());
			notes.setNoteType("Claim");
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			notes.setRemindInterval("15");
			notes.setNotesKeyId(claim.getId());
			//notes.setForwardDate(new Date());
			notes.setNoteSubType(subType);
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Claim", "NOTESUBTYPE");
		}
		if(systemDefaulttransDoc.indexOf(serviceOrder.getJob())>-1){
			memoIntegrationFlag="Y";
		}else{
			memoIntegrationFlag="N";
		}				  		
		notes.setBillToCode(serviceOrder.getBillToCode());
		notes.setBookingAgentCode(serviceOrder.getBookingAgentCode());
		notesFirstName = customerFile.getFirstName();
		notesLastName = customerFile.getLastName();
		linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
		noteForNextPrev = "ServiceOrder";
		List signatureNoteList = notesManager.userSignatureNotes(getRequest().getRemoteUser());
		signatureNote = signatureNoteList.get(0).toString();
		return SUCCESS;
	}
	@SkipValidation
	public String editNewNoteForFamilyDetails() {
		getComboList(sessionCorpID);
		checkCF="CF";
		if (id1 != null) {
			dsFamilyDetails = dsFamilyDetailsManager.get(id1);
			getRequest().setAttribute("soLastName", dsFamilyDetails.getLastName());
			notes = notesManager.get(id);
			if(notes.getFollowUpFor()!=null && !(notes.getFollowUpFor().trim().equalsIgnoreCase("")) && all_user !=null && (!(all_user.containsValue(notes.getFollowUpFor().trim().toUpperCase())))){
				List aliasNameList=userManager.findUserByUsername(notes.getFollowUpFor());
				if(((aliasNameList!=null)&&!aliasNameList.isEmpty())&&aliasNameList.get(0)!=null){
				String aliasName=aliasNameList.get(0).toString();
		        String coordCode=notes.getFollowUpFor().toUpperCase();
				String alias = aliasName.toUpperCase();
				if(!all_user.containsValue(coordCode)){
					all_user.put(coordCode,alias );
				} 
				}
				}
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "FamilyDetails");
			customerFile = customerFileManager.get(dsFamilyDetails.getCustomerFileId());
			notesFirstName = customerFile.getFirstName();
			notesLastName = customerFile.getLastName();
		} else {
			dsFamilyDetails = dsFamilyDetailsManager.get(id);
			customerFile = customerFileManager.get(dsFamilyDetails.getCustomerFileId());
			getRequest().setAttribute("soLastName", dsFamilyDetails.getLastName());
			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(sessionCorpID);
			notes.setNotesId(dsFamilyDetails.getId().toString()+dsFamilyDetails.getCustomerFileId());
			notes.setNoteType("FamilyDetails");
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			if(userType.trim().equalsIgnoreCase("ACCOUNT")) {
				if(sessionCorpID.equalsIgnoreCase("UTSI")){
					DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		           Date today = new Date();
		           Date todayWithZeroTime;
				try {
					todayWithZeroTime = formatter.parse(formatter.format(today));
					 notes.setForwardDate(todayWithZeroTime);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				  if(customerFile.getCoordinator()!=null && !(customerFile.getCoordinator().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getCoordinator());
					if(((aliasNameList!=null)&&!aliasNameList.isEmpty())&&aliasNameList.get(0)!=null){
					String aliasName=aliasNameList.get(0).toString();
			        String coordCode=customerFile.getCoordinator().toUpperCase();
					String alias = aliasName.toUpperCase();
					if(!all_user.containsValue(coordCode)){
						all_user.put(coordCode,alias );
					} 
					}
					notes.setFollowUpFor(customerFile.getCoordinator().toUpperCase());
				  }
				}
				else
				{
					notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());	
				}
			}else{
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			}


			notes.setRemindInterval("15");
			notes.setNotesKeyId(dsFamilyDetails.getCustomerFileId());
			notes.setCustomerNumber(customerFile.getSequenceNumber());
			notesFirstName = customerFile.getFirstName();
			notesLastName = customerFile.getLastName();
			notes.setNoteSubType(subType);
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "FamilyDetails", "FamilyDetails");
		}
		notes.setBillToCode(customerFile.getBillToCode());
		notes.setBookingAgentCode(customerFile.getBookingAgentCode());	  		
	
		
		return SUCCESS;
		

		
	}
	@SkipValidation
	public String editNewNotesForCreditCard() {
		getComboList(sessionCorpID);
		if (id1 != null) {
			creditCard = creditCardManager.get(id1);
			serviceOrder = (ServiceOrder) creditCardManager.getServiceOrder(creditCard.getShipNumber(), sessionCorpID).get(0);
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = notesManager.get(id);
			if(notes.getNoteType()!=null && notes.getNoteType().equals("Customer File"))
				checkCF="CF";
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
			linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
			//notes.setUpdatedOn(new Date());
		} else {
			creditCard = creditCardManager.get(id);
			serviceOrder = (ServiceOrder) creditCardManager.getServiceOrder(creditCard.getShipNumber(), sessionCorpID).get(0);
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(sessionCorpID);
			notes.setNotesId(String.valueOf(creditCard.getId()));
			notes.setCustomerNumber(customerFile.getSequenceNumber());
			notes.setNoteType("CreditCard");
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			notes.setRemindInterval("15");
			//notes.setForwardDate(new Date());
			notes.setNoteSubType(subType);
			notes.setNotesKeyId(creditCard.getId());
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "CreditCard", "NOTESUBTYPE");
			linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
		}
		notes.setBillToCode(serviceOrder.getBillToCode());
		notes.setBookingAgentCode(serviceOrder.getBookingAgentCode());
		if(systemDefaulttransDoc.indexOf(serviceOrder.getJob())>-1){
			memoIntegrationFlag="Y";
		}else{
			memoIntegrationFlag="N";
		}				  		

		return SUCCESS;
	}

	@SkipValidation
	public String editNewNoteForServicePartner() {
		getComboList(sessionCorpID);
		if (id1 != null) {
			servicePartner = servicePartnerManager.get(id1);
			serviceOrder = servicePartner.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = notesManager.get(id);
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
			linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
			//notes.setUpdatedOn(new Date());
		} else {
			servicePartner = servicePartnerManager.get(id);
			serviceOrder = servicePartner.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(sessionCorpID);
			notes.setNotesId(String.valueOf(servicePartner.getId()));
			notes.setCustomerNumber(customerFile.getSequenceNumber());
			notes.setNoteType("Service Partner");
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			notes.setRemindInterval("15");
			notes.setNotesKeyId(servicePartner.getId());
			//notes.setForwardDate(new Date());
			notes.setNoteSubType(subType);
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Service Partner", "NOTESUBTYPE");
			notesFirstName = customerFile.getFirstName();
			notesLastName = customerFile.getLastName();
			linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
		}
		if(systemDefaulttransDoc.indexOf(serviceOrder.getJob())>-1){
			memoIntegrationFlag="Y";
		}else{
			memoIntegrationFlag="N";
		}				  		
		notes.setBillToCode(serviceOrder.getBillToCode());
		notes.setBookingAgentCode(serviceOrder.getBookingAgentCode());
		List signatureNoteList = notesManager.userSignatureNotes(getRequest().getRemoteUser());
		signatureNote = signatureNoteList.get(0).toString();
		return SUCCESS;
	}
    private String flagAcc;
	@SkipValidation
	public String editNewNoteForAccountLine() {
		getComboList(sessionCorpID);
		if (id1 != null) {
			if(userType.equalsIgnoreCase("AGENT")){
			accountLine = accountLineManager.getForOtherCorpid(id1);
			notes = notesManager.getForOtherCorpid(id);
			}
			else{
			accountLine = accountLineManager.get(id1);	
			notes = notesManager.get(id);
			} 
			serviceOrder = accountLine.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
			linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
			//notes.setUpdatedOn(new Date());
			if(notes.getFollowUpFor()!=null && !(notes.getFollowUpFor().trim().equalsIgnoreCase("")) && all_user !=null && (!(all_user.containsValue(notes.getFollowUpFor().trim().toUpperCase())))){
				List aliasNameList=userManager.findUserByUsername(notes.getFollowUpFor());
				if(((aliasNameList!=null)&&!aliasNameList.isEmpty())&&aliasNameList.get(0)!=null){
				String aliasName=aliasNameList.get(0).toString();
		        String coordCode=notes.getFollowUpFor().toUpperCase();
				String alias = aliasName.toUpperCase();
				if(!all_user.containsValue(coordCode)){
					all_user.put(coordCode,alias );
				} 
				}
				}
		
		} else {
			if(userType.equalsIgnoreCase("AGENT")){
			accountLine = accountLineManager.getForOtherCorpid(id);
			}else{
			 accountLine = accountLineManager.get(id);
			}
			serviceOrder = accountLine.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(sessionCorpID);
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
				notes.setCorpID(accountLine.getCorpID());
			}
			notes.setNotesId(String.valueOf(accountLine.getId()));
			notes.setCustomerNumber(customerFile.getSequenceNumber());
			notes.setNoteType("Account Line");
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			if(userType.trim().equalsIgnoreCase("ACCOUNT")) {
				if(sessionCorpID.equalsIgnoreCase("UTSI")){
					DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		           Date today = new Date();
		           Date todayWithZeroTime;
				try {
					todayWithZeroTime = formatter.parse(formatter.format(today));
					 notes.setForwardDate(todayWithZeroTime);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				  if(serviceOrder.getCoordinator()!=null && !(serviceOrder.getCoordinator().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(serviceOrder.getCoordinator());
					if(((aliasNameList!=null)&&!aliasNameList.isEmpty())&&aliasNameList.get(0)!=null){
					String aliasName=aliasNameList.get(0).toString();
			        String coordCode=serviceOrder.getCoordinator().toUpperCase();
					String alias = aliasName.toUpperCase();
					if(!all_user.containsValue(coordCode)){
						all_user.put(coordCode,alias );
					} 
					}
					notes.setFollowUpFor(serviceOrder.getCoordinator().toUpperCase());
				  }
				}
				else
				{
					notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());	
				}
			}else{
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			}
			notes.setRemindInterval("15");
			notes.setNotesKeyId(accountLine.getId());
			if (subType.equalsIgnoreCase("CostingDetail")) {
				notes.setDisplayQuote(true);
			}
			notes.setNoteSubType(subType);
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Account Line", "NOTESUBTYPE");
			linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
			//notes.setForwardDate(new Date());
		}
		if(systemDefaulttransDoc.indexOf(serviceOrder.getJob())>-1){
			memoIntegrationFlag="Y";
		}else{
			memoIntegrationFlag="N";
		}				  		
		notes.setBillToCode(serviceOrder.getBillToCode());
		notes.setBookingAgentCode(serviceOrder.getBookingAgentCode());
		List signatureNoteList = notesManager.userSignatureNotes(getRequest().getRemoteUser());
		signatureNote = signatureNoteList.get(0).toString();
		return SUCCESS;
	}

	@SkipValidation
	public String editNewNoteForCarton() {
		getComboList(sessionCorpID);
		if (id1 != null) {
			linkFileList=notesManager.getMyFileList(id);
			// linkFile=notesManager.getMyFileList(id);
			if(linkFileList!=null && (!(linkFileList.isEmpty())) && linkFileList.get(0)!=null){
				linkFile = linkFileList.get(0).toString();
				if( linkFile!= null && !(linkFile.equals(""))){
					myFileList = myFileManager.getMyFileList(linkFile,sessionCorpID);
				}
			}
			carton = cartonManager.get(id1);
			serviceOrder = carton.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = notesManager.get(id);
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
			linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
			//notes.setUpdatedOn(new Date());
		} else {
			carton = cartonManager.get(id);
			serviceOrder = carton.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(sessionCorpID);
			notes.setNotesId(String.valueOf(carton.getId()));
			notes.setCustomerNumber(customerFile.getSequenceNumber());
			notes.setNoteType("Carton");
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			notes.setRemindInterval("15");
			notes.setNotesKeyId(carton.getId());
			//notes.setForwardDate(new Date());
			notes.setNoteSubType(subType);
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Carton", "NOTESUBTYPE");
			notesFirstName = customerFile.getFirstName();
			notesLastName = customerFile.getLastName();
			linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
		}
		notes.setBillToCode(serviceOrder.getBillToCode());
		notes.setBookingAgentCode(serviceOrder.getBookingAgentCode());
		if(systemDefaulttransDoc.indexOf(serviceOrder.getJob())>-1){
			memoIntegrationFlag="Y";
		}else{
			memoIntegrationFlag="N";
		}				  		

		return SUCCESS;
	}

	@SkipValidation
	public String editNewNoteForContainer() {
		getComboList(sessionCorpID);
		if (id1 != null) {
			linkFileList=notesManager.getMyFileList(id);
			// linkFile=notesManager.getMyFileList(id);
			if(linkFileList!=null && (!(linkFileList.isEmpty())) && linkFileList.get(0)!=null){
				linkFile = linkFileList.get(0).toString();
				if( linkFile!= null && !(linkFile.equals(""))){
					myFileList = myFileManager.getMyFileList(linkFile,sessionCorpID);
				}
			}
			container = containerManager.get(id1);
			serviceOrder = container.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = notesManager.get(id);
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
			linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
			//notes.setUpdatedOn(new Date());
		} else {
			container = containerManager.get(id);
			serviceOrder = container.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(sessionCorpID);
			notes.setNotesId(String.valueOf(container.getId()));
			notes.setCustomerNumber(customerFile.getSequenceNumber());
			notes.setNoteType("Container");
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			notes.setRemindInterval("15");
			notes.setNotesKeyId(container.getId());
			//notes.setForwardDate(new Date());
			notes.setNoteSubType(subType);
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Container", "NOTESUBTYPE");
			notesFirstName = customerFile.getFirstName();
			notesLastName = customerFile.getLastName();
			linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
		}
		notes.setBillToCode(serviceOrder.getBillToCode());
		notes.setBookingAgentCode(serviceOrder.getBookingAgentCode());
		if(systemDefaulttransDoc.indexOf(serviceOrder.getJob())>-1){
			memoIntegrationFlag="Y";
		}else{
			memoIntegrationFlag="N";
		}				  		

		return SUCCESS;
	}

	@SkipValidation
	public String editNewNoteForVehicle() {
		getComboList(sessionCorpID);
		if (id1 != null) {
			linkFileList=notesManager.getMyFileList(id);
			// linkFile=notesManager.getMyFileList(id);
			if(linkFileList!=null && (!(linkFileList.isEmpty())) && linkFileList.get(0)!=null){
				linkFile = linkFileList.get(0).toString();
				if( linkFile!= null && !(linkFile.equals(""))){
					myFileList = myFileManager.getMyFileList(linkFile,sessionCorpID);
				}
			}
			vehicle = vehicleManager.get(id1);
			serviceOrder = vehicle.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = notesManager.get(id);
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
			linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
			//notes.setUpdatedOn(new Date());
		} else {
			vehicle = vehicleManager.get(id);
			serviceOrder = vehicle.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(sessionCorpID);
			notes.setNotesId(String.valueOf(vehicle.getId()));
			notes.setCustomerNumber(customerFile.getSequenceNumber());
			notes.setNoteType("Vehicle");
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			notes.setRemindInterval("15");
			notes.setNotesKeyId(vehicle.getId());
			//notes.setForwardDate(new Date());
			notes.setNoteSubType(subType);
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Vehicle", "NOTESUBTYPE");
			notesFirstName = customerFile.getFirstName();
			notesLastName = customerFile.getLastName();
			linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
		}
		notes.setBillToCode(serviceOrder.getBillToCode());
		notes.setBookingAgentCode(serviceOrder.getBookingAgentCode());
		if(systemDefaulttransDoc.indexOf(serviceOrder.getJob())>-1){
			memoIntegrationFlag="Y";
		}else{
			memoIntegrationFlag="N";
		}				  		

		return SUCCESS;
	}
	
	@SkipValidation
	public String editNewNoteForPayroll() {
		getComboList(sessionCorpID);
			memoIntegrationFlag="N";

		if (id1 != null) {
			noteFor="Crew";
			payroll = payrollManager.get(id1);
			notes = notesManager.get(id);
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");

			
			notesFirstName = payroll.getFirstName();
			notesLastName = payroll.getLastName();
		} else {
			payroll = payrollManager.get(id);
			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(sessionCorpID);
			notes.setNotesId(payroll.getId().toString());
			notes.setNoteType("Crew");
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			notes.setRemindInterval("15");
			notes.setNotesKeyId(payroll.getId());
			//notes.setForwardDate(new Date());
			notes.setNoteSubType("Crew");
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Crew", "NOTESUBTYPE");
			//linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
			noteFor="Crew";
			notesFirstName = payroll.getFirstName();
			notesLastName = payroll.getLastName();
		}
		return SUCCESS;
	}

	private String accountNotesFor;
	private String hitFlag;

	private String totalRevenue;

	private PartnerPublicManager partnerPublicManager; 

	@SkipValidation
	public String editNewNoteForPartner() {
		getComboList(sessionCorpID);
			memoIntegrationFlag="N";
		
		if (id1 != null) {
			if (accountNotesFor.equalsIgnoreCase("AP")) {
				accountProfile = accountProfileManager.get(id1);
				
				partnerPrivate = (PartnerPrivate)partnerPrivateManager.getPartnerPrivate(accountProfile.getPartnerCode(),sessionCorpID).get(0);
				String firstName = "";
				String lastName = "";
				if(partnerPrivate.getFirstName() != null){
					firstName = partnerPrivate.getFirstName();
				}
				if(partnerPrivate.getLastName() != null){
					lastName = partnerPrivate.getLastName();
				}
				
				notesFirstName = firstName;
				notesLastName = lastName;
			}
			if (accountNotesFor.equalsIgnoreCase("AC")) {
				accountContact = accountContactManager.get(id1);
				
				partnerPrivate = (PartnerPrivate)partnerPrivateManager.getPartnerPrivate(accountContact.getPartnerCode(),sessionCorpID).get(0);
				String firstName = "";
				String lastName = "";
				if(partnerPrivate.getFirstName() != null){
					firstName = partnerPrivate.getFirstName();
				}
				if(partnerPrivate.getLastName() != null){
					lastName = partnerPrivate.getLastName();
				}
				
				notesFirstName = firstName;
				notesLastName = lastName;
				
			}
			notes = notesManager.get(id);
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
			//notes.setUpdatedOn(new Date());
		} else {
			notes = new Notes();
			notes.setNoteStatus("NEW");
			if (accountNotesFor.equalsIgnoreCase("AP")) {
				accountProfile = accountProfileManager.get(id);
				notes.setNotesId(accountProfile.getPartnerCode());
				notes.setCustomerNumber("XXXXXXXX");
				notes.setNoteType("Partner");
				notes.setNoteSubType("AccountProfile");
				notes.setNotesKeyId(accountProfile.getId());
				
				partnerPrivate = (PartnerPrivate)partnerPrivateManager.getPartnerPrivate(accountProfile.getPartnerCode(),sessionCorpID).get(0);
				String firstName = "";
				String lastName = "";
				if(partnerPrivate.getFirstName() != null){
					firstName = partnerPrivate.getFirstName();
				}
				if(partnerPrivate.getLastName() != null){
					lastName = partnerPrivate.getLastName();
				}
				
				notesFirstName = firstName;
				notesLastName = lastName;
				
				notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Partner", "NOTESUBTYPE");
			}
			if (accountNotesFor.equalsIgnoreCase("AC")) {
				accountContact = accountContactManager.get(id);
				notes.setNotesId(accountContact.getPartnerCode());
				notes.setCustomerNumber("YYYYYYYY");
				notes.setNoteType("Partner");
				notes.setNoteSubType("AccountContact");
				notes.setNotesKeyId(accountContact.getId());
				notes.setNoteStatus("New");
				
				partnerPrivate = (PartnerPrivate)partnerPrivateManager.getPartnerPrivate(accountContact.getPartnerCode(),sessionCorpID).get(0);
				String firstName = "";
				String lastName = "";
				if(partnerPrivate.getFirstName() != null){
					firstName = partnerPrivate.getFirstName();
				}
				if(partnerPrivate.getLastName() != null){
					lastName = partnerPrivate.getLastName();
				}
				
				notesFirstName = firstName;
				notesLastName = lastName;
				
				notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Partner", "NOTESUBTYPE");
			}

			notes.setCorpID(sessionCorpID);
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			notes.setRemindInterval("15");
			//notes.setForwardDate(new Date());
		}
		return SUCCESS;
	}

	// Method for previous and next 

	/*@SkipValidation 
	 public String editOtherServiceOrder(){
	 tempID=Long.parseLong((serviceOrderManager.findIdByShipNumber(shipNm)).get(0).toString());
	 return SUCCESS;   
	 } */
	private Mss mss;
	@SkipValidation
	public String editNewNoteForMss() {
		getComboList(sessionCorpID);
		if (id1 != null) {
			mss = mssManager.get(id1);
			noteFor="MSS";
			notes = notesManager.get(id);
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");			
		} else {
			mss = mssManager.get(id);			
			//getRequest().setAttribute("soLastName", customerFile.getLastName());
			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(sessionCorpID);
			notes.setNotesId(mss.getSoNumber());
			notes.setCustomerNumber(mss.getSoNumber());
			notes.setNoteType("MSS");
			notes.setRemindTime("00:00");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			notes.setRemindInterval("15");
			notes.setNotesKeyId(mss.getId());
			noteFor="MSS";
			notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "MSS", "NOTESUBTYPE");			
		}		
			List signatureNoteList = notesManager.userSignatureNotes(getRequest().getRemoteUser());
			signatureNote = signatureNoteList.get(0).toString();
		return SUCCESS;
	}

	@SkipValidation
	public String editNextNote() {
		tempID = Long.parseLong((notesManager.goNextNotes(seqNm, soIdNum, sessionCorpID)).get(0).toString());
		return SUCCESS;
	}

	@SkipValidation
	public String editPrevNote() {
		tempID = Long.parseLong((notesManager.goPrevNotes(seqNm, soIdNum, sessionCorpID)).get(0).toString());
		return SUCCESS;
	}

	@SkipValidation
	public String notesNextPrev() {
		notesNextPrevList = notesManager.notesNextPrev(seqNm, soIdNum, sessionCorpID);
		return SUCCESS;
	}

	@SkipValidation
	public String notesNextPrevSo() {
		notesNextPrevList = notesManager.notesNextPrev(seqNm, soIdNum, sessionCorpID);
		return SUCCESS;
	}

	@SkipValidation
	public String notesNextPrevTicket() {
		notesNextPrevList = notesManager.notesNextPrev(seqNm, soIdNum, sessionCorpID);
		return SUCCESS;
	}

	@SkipValidation
	public String editNextNoteIsRal() {
		tempID = Long.parseLong((notesManager.goNextNotesIsRal(seqNm, soIdNum, sessionCorpID)).get(0).toString());
		return SUCCESS;
	}

	@SkipValidation
	public String editPrevNoteIsRal() {
		tempID = Long.parseLong((notesManager.goPrevNotesIsRal(seqNm, soIdNum, sessionCorpID)).get(0).toString());
		return SUCCESS;
	}

	@SkipValidation
	public String notesNextPrevIsRal() {
		notesNextPrevList = notesManager.notesNextPrevIsRal(seqNm, soIdNum, sessionCorpID);
		return SUCCESS;
	}

	@SkipValidation
	public String notesNextPrevIsRalServiceOrder() {
		notesNextPrevList = notesManager.notesNextPrevIsRal(seqNm, soIdNum, sessionCorpID);
		return SUCCESS;
	}

	@SkipValidation
	public String notesNextPrevIsRalTicket() {
		notesNextPrevList = notesManager.notesNextPrevIsRal(seqNm, soIdNum, sessionCorpID);
		return SUCCESS;
	}

	// End Functions for ) the notes depending on the module	
	//  Functions for edit(OLD) the notes depending on the module
	@SkipValidation
	public String edit() {
		getComboList(sessionCorpID);
		if (id != null) {
			notes = notesManager.get(id);
   		   	notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
		} else {
			notes = new Notes();
			notes.setNoteStatus("NEW");
		}

		return SUCCESS;
	}

	//  Functions for save the notes 
	private String saveFlag = "FALSE";

	private EmailSetupManager  emailSetupManager;
	
	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

	@SkipValidation
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		saveFlag = "TRUE";
		if(noteFor!=null && !noteFor.equals("")){
			if (noteFor.equals("CustomerFile")) {
				checkCF="CF";
				customerFile = customerFileManager.get(id1);
				linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
			}
		}
		/*if(id!=null){
			notes = notesManager.get(id);
		}*/
		if(noteFor!=null && !noteFor.equals("")){
			if (noteFor.contains("ServiceOrder")) {
				checkCF="SO";
				//customerFile = customerFileManager.get(id1);
				//linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
			}
		}
		boolean isNew = (notes.getId() == null);
		if (isNew) {
			maxNotesId = Long.parseLong(notesManager.findMaxId().get(0).toString());
			notes.setCreatedOn(new Date());
			notes.setCreatedBy(getRequest().getRemoteUser());
			maxNotesId = maxNotesId + 1;
			notes.setId(maxNotesId);
		}
		
		notes.setSystemDate(new Date());
		notes.setUpdatedOn(new Date());
		notes.setUpdatedBy(getRequest().getRemoteUser());		
		notes.setMemoUploadStatus("READY_TO_UPLOAD");
		if(((userType.trim().equalsIgnoreCase("AGENT"))|| (userType.trim().equalsIgnoreCase("PARTNER"))) ){
			notes.setPartnerPortal(true);
			notes.setNoteSubType("AgentNotes");
			notes.setNoteType("Service Order");
		}		
		if(flagAcc!=null && flagAcc.equals("true"))
		{
			notes.setPartnerPortal(true);
			notes.setNoteSubType("PayableDetail");
			notes.setNoteType("Account Line");
			
			
		}
		if(userType.trim().equalsIgnoreCase("ACCOUNT")){
			notes.setAccPortal(true);
			if(notes.getNoteType()!=null && (!(notes.getNoteType().trim().equals("")))){
				
			}else{
			notes.setNoteType("Service Order");
			notes.setNoteSubType(notes.getNoteSubType());
			notes.setNotesId(serviceOrder.getShipNumber());
		   }
		}

		try {
			if (isNew) {
				serviceOrder = serviceOrderManager.getForOtherCorpid(id);				
			}else{
				serviceOrder = serviceOrderManager.getForOtherCorpid(id1);	
			}
			//customerFile = (CustomerFile) customerFileManager.findSequenceNumber(serviceOrder.getSequenceNumber()).get(0);
			if((userType.trim().equalsIgnoreCase("AGENT"))|| (userType.trim().equalsIgnoreCase("PARTNER")) || (userType.trim().equalsIgnoreCase("ACCOUNT"))){
				linkedToList= serviceOrderManager.getShipmentNumber(serviceOrder.getCorpID(),serviceOrder.getCustomerFile().getSequenceNumber());
				String linkedList="";
				if((linkedToList!=null) &&(!linkedToList.isEmpty())&&(linkedToList.get(0)!=null)){
					linkedList=linkedToList.get(0).toString();
				}
			//notes.setNotesId(linkedList);
			}else{
				linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,serviceOrder.getCustomerFile().getSequenceNumber());
			}
			notesFirstName = serviceOrder.getCustomerFile().getFirstName();
			notesLastName = serviceOrder.getCustomerFile().getLastName();
		
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if(notes.getCorpID()==null || notes.getCorpID().toString().equals("") ){
			if((userType.trim().equalsIgnoreCase("AGENT"))|| (userType.trim().equalsIgnoreCase("PARTNER")) || (userType.trim().equalsIgnoreCase("ACCOUNT"))){
				String corpIdValue="";
				if(fileNumber!=null && !fileNumber.equals("")){
					List l=serviceOrderManager.findShipNumber(fileNumber);
					if(l!=null && !l.isEmpty()){serviceOrder=(ServiceOrder) l.get(0);
					corpIdValue=serviceOrder.getCorpID();
					}
					else{
						List l1=customerFileManager.findSequenceNumber(fileNumber);
						customerFile = (CustomerFile) l1.get(0);
						corpIdValue=customerFile.getCorpID();
					}
				}else{
					 if(notes.getNotesId()!=null && !notes.getNotesId().equals("")){
					      List l=serviceOrderManager.findShipNumber(notes.getNotesId());
					      if(l!=null && !l.isEmpty()){
					      serviceOrder=(ServiceOrder) l.get(0);
					      corpIdValue=serviceOrder.getCorpID();
					      }else{
					       List l1=customerFileManager.findSequenceNumber(notes.getNotesId());
					       customerFile = (CustomerFile) l1.get(0);
					       corpIdValue=customerFile.getCorpID();
					      }
					     
					     }
				}
				getComboList(corpIdValue);
				notesubtype = refMasterManager.findNoteSubType(corpIdValue, notes.getNoteType(), "NOTESUBTYPE");
				if((!userType.trim().equalsIgnoreCase("AGENT"))){
					notes.setCorpID(sessionCorpID);
				}else{
					notes.setCorpID(corpIdValue);
				}
			}else{
				getComboList(sessionCorpID);
				notesubtype = refMasterManager.findNoteSubType(sessionCorpID, notes.getNoteType(), "NOTESUBTYPE");
				notes.setCorpID(sessionCorpID);
			}
		}else{
			getComboList(notes.getCorpID());
		}
		if (notes.getNoteType()!=null && notes.getNoteType().equalsIgnoreCase("Partner")) {
			notes.setNoteGroup("Partner");
		} else {
			notes.setNoteGroup("Transaction");
		}
		String networkLinkId="";
		if(checkSO!=null && checkSO.equalsIgnoreCase("SO")){
			if ((notes.getBookingAgent()!=null && notes.getBookingAgent()==true) || (notes.getNetworkAgent()!=null && notes.getNetworkAgent()==true) || (notes.getOriginAgent()!=null && notes.getOriginAgent()==true) || (notes.getSubOriginAgent()!=null && notes.getSubOriginAgent()==true) || (notes.getDestinationAgent()!=null && notes.getDestinationAgent()==true) || (notes.getSubDestinationAgent()!=null && notes.getSubDestinationAgent()==true)) {			
				List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
				if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
					checkAgent = defCheckedShipNumber.get(0).toString();
					if(checkAgent.equalsIgnoreCase("BA")){
						notes.setBookingAgent(true);
					}else if(checkAgent.equalsIgnoreCase("NA")){
						notes.setNetworkAgent(true);
					}else if(checkAgent.equalsIgnoreCase("OA")){
						notes.setOriginAgent(true);
					}else if(checkAgent.equalsIgnoreCase("SOA")){
						notes.setSubOriginAgent(true);
					}else if(checkAgent.equalsIgnoreCase("DA")){
						notes.setDestinationAgent(true);
					}else if(checkAgent.equalsIgnoreCase("SDA")){
						notes.setSubDestinationAgent(true);
					} 				    
				}
			  }
		   }
		if(notes.getNoteStatus()!=null && notes.getNoteStatus().equals("CMP")){
			notes.setReminderStatus("OLD");
		}
		if (remindMe !=null && remindMe.equals("remind")) {
			notes.setReminderStatus("NEW");
		}
		if(notes.getNoteStatus()!=null && notes.getNoteStatus().equals("NEW") && notes.getForwardDate()!=null && notes.getFollowUpFor()!=null 
				&& !notes.getFollowUpFor().equals("") && (notes.getReminderStatus()==null || notes.getReminderStatus().equals(""))){
			notes.setReminderStatus("NEW");
		}
		if(notes.getFollowUpFor()!=null && !(notes.getFollowUpFor().trim().equalsIgnoreCase("")) && all_user !=null && (!(all_user.containsValue(notes.getFollowUpFor().trim().toUpperCase())))){
			List aliasNameList=userManager.findUserByUsername(notes.getFollowUpFor());
			if(((aliasNameList!=null)&&!aliasNameList.isEmpty())&&aliasNameList.get(0)!=null){
			String aliasName=aliasNameList.get(0).toString();
	        String coordCode=notes.getFollowUpFor().toUpperCase();
			String alias = aliasName.toUpperCase();
			if(!all_user.containsValue(coordCode)){
				all_user.put(coordCode,alias );
			} 
			}
			}
		notes = notesManager.save(notes);
		try{
		  if (isNew) {
			if((userType.trim().equalsIgnoreCase("AGENT"))|| (userType.trim().equalsIgnoreCase("PARTNER"))||(userType.trim().equalsIgnoreCase("ACCOUNT"))){	
				auditTrail = new AuditTrail();
				auditTrail.setCorpID(notes.getCorpID());
				auditTrail.setXtranId(notes.getNotesKeyId().toString());
				auditTrail.setTableName(notes.getNoteType().replaceAll(" ", "").toLowerCase());
				auditTrail.setFieldName("Notes");
				auditTrail.setDescription("Coordinator");
				auditTrail.setOldValue(notes.getSubject());
				auditTrail.setNewValue(notes.getSubject());
				auditTrail.setUser(getRequest().getRemoteUser());
				auditTrail.setDated(new Date());
				auditTrail.setAlertRole("Coordinator");
				auditTrail.setAlertShipperName(serviceOrder.getFirstName()+" "+serviceOrder.getLastName());
				auditTrail.setAlertFileNumber(notes.getNotesId());
				auditTrail.setAlertUser(serviceOrder.getCustomerFile().getCoordinator());
				auditTrail.setIsViewed(false);			
				auditTrailManager.save(auditTrail);
			 }
		  }
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//------------------For Network Notes------------------------------------------------------
	if(checkSO!=null && checkSO.equalsIgnoreCase("SO")){	
		if (isNew && ((notes.getBookingAgent()!=null && notes.getBookingAgent()==true) || (notes.getNetworkAgent()!=null && notes.getNetworkAgent()==true) || (notes.getOriginAgent()!=null && notes.getOriginAgent()==true) || (notes.getSubOriginAgent()!=null && notes.getSubOriginAgent()==true) || (notes.getDestinationAgent()!=null && notes.getDestinationAgent()==true) || (notes.getSubDestinationAgent()!=null && notes.getSubDestinationAgent()==true))) {	
			List serviceOrderNotesList = notesManager.getServiceOrderNotesList(serviceOrder.getShipNumber(), "Primary", notes.getBookingAgent(), notes.getNetworkAgent(), notes.getOriginAgent(), notes.getSubOriginAgent(), notes.getDestinationAgent(), notes.getSubDestinationAgent());
			Set set = new HashSet(serviceOrderNotesList);
			List linkedSeqNum = new ArrayList(set);
			if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){
				Iterator it = linkedSeqNum.iterator();
		         while (it.hasNext()) {
		        	 	 String shipNumber=(String)it.next();
		        	 	 String removeDatarow = shipNumber.substring(0, 6);
		        	 	 networkLinkId = sessionCorpID + (notes.getId()).toString();
		        	 	 notesManager.upDateNetworkLinkId(networkLinkId,notes.getId());
			        	 if(!(notes.getNotesId().equalsIgnoreCase(shipNumber)) && !removeDatarow.equals("remove")){	
			        		List linkedId = notesManager.getLinkedId(networkLinkId, shipNumber.substring(0, 4));
				     		if(linkedId ==null || linkedId.isEmpty()){
			        		Notes myNotes = new Notes();
			        		myNotes.setCorpID(shipNumber.substring(0, 4));
			        		myNotes.setCustomerNumber(shipNumber.substring(0, shipNumber.length()-2));
			        		myNotes.setNotesId(shipNumber);
			        		myNotes.setNoteStatus(notes.getNoteStatus());
			        		myNotes.setNote(notes.getNote());
			        		myNotes.setName(notes.getName());
			        		myNotes.setNotesActivity(notes.getNotesActivity());
			        		myNotes.setNoteGroup(notes.getNoteGroup());
			        		myNotes.setNotesKeyId(notes.getNotesKeyId());
			        		myNotes.setNoteSubType(notes.getNoteSubType());
			        		myNotes.setNoteType(notes.getNoteType());
			        		myNotes.setDateOfContact(notes.getDateOfContact());
			        		myNotes.setFollowUpFor(notes.getFollowUpFor());
			        		myNotes.setFollowUpId(notes.getFollowUpId());
			        		myNotes.setForwardDate(notes.getForwardDate());
			        		myNotes.setForwardStatus(notes.getForwardStatus());
			        		myNotes.setForwardToUser(notes.getForwardToUser());
			        		myNotes.setGrading(notes.getGrading());
			        		myNotes.setIssueType(notes.getIssueType());
			        		myNotes.setLinkedTo(notes.getLinkedTo());
			        		myNotes.setName(notes.getName());
			        		myNotes.setReminderStatus(notes.getReminderStatus());
			        		myNotes.setRemindInterval(notes.getRemindInterval());
			        		myNotes.setRemindTime(myNotes.getRemindTime());
			        		myNotes.setSubject(notes.getSubject());
			        		myNotes.setSupplier(notes.getSupplier());
			        		myNotes.setSystemDate(notes.getSystemDate());
			        		myNotes.setToDoRuleId(notes.getToDoRuleId());
			        		myNotes.setNetworkLinkId(networkLinkId);
			        		myNotes.setBookingAgent(notes.getBookingAgent());
			        		myNotes.setNetworkAgent(notes.getNetworkAgent());
			        		myNotes.setOriginAgent(notes.getOriginAgent());
			        		myNotes.setSubOriginAgent(notes.getSubOriginAgent());
			        		myNotes.setDestinationAgent(notes.getDestinationAgent());
			        		myNotes.setSubDestinationAgent(notes.getSubDestinationAgent());			        		
			        		myNotes.setCreatedBy(notes.getCreatedBy());
			        		myNotes.setCreatedOn(notes.getCreatedOn());
			        		myNotes.setUpdatedBy(notes.getUpdatedBy());
			        		myNotes.setUpdatedOn(notes.getUpdatedOn());
			        		myNotes.setActualValue(notes.getActualValue());
			        		myNotes.setRequestedValue(notes.getRequestedValue());
			        		myNotes.setRequestDate(notes.getRequestDate());
			        		myNotes.setSubmittedDate(notes.getSubmittedDate());
			        		myNotes.setComplitionDate(notes.getComplitionDate());
			        		notesManager.save(myNotes);
			        	 }
			        	 }
			          }
			      }
			 }
		
		
		//------------------For checked checkbox------------------------------------------------------		
	if (!isNew ){
		if(notes.getNetworkLinkId()==null || notes.getNetworkLinkId().equalsIgnoreCase("")){
			 networkLinkId = sessionCorpID + (notes.getId()).toString();
		}else{
			networkLinkId =  notes.getNetworkLinkId();
		}		
		if(((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("BA")) || ((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NA")) || ((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("OA")) || ((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("SOA")) || ((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("DA")) || ((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("SDA"))){						
			List linkedShipNumberList = myFileManager.getLinkedAllShipNumber(serviceOrder.getShipNumber(),checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "checked");
			if(linkedShipNumberList!=null && !linkedShipNumberList.isEmpty()){		
				notesManager.upDateNetworkLinkId(networkLinkId,notes.getId());
				 Iterator it = linkedShipNumberList.iterator();
		         while (it.hasNext()) {
		        	 	 String shipNumber=(String)it.next();
			        	 String removeDatarow = shipNumber.substring(0, 6);
			        	 if(!(notes.getNotesId().equalsIgnoreCase(shipNumber)) && !removeDatarow.equals("remove")){	
			        		 List linkedId = notesManager.getLinkedId(networkLinkId, shipNumber.substring(0, 4));
					     		if(linkedId ==null || linkedId.isEmpty()){
				        		Notes myNotes = new Notes();
				        		myNotes.setCorpID(shipNumber.substring(0, 4));
				        		myNotes.setCustomerNumber(shipNumber.substring(0, shipNumber.length()-2));
				        		myNotes.setNotesId(shipNumber);
				        		myNotes.setNoteStatus(notes.getNoteStatus());
				        		myNotes.setNote(notes.getNote());
				        		myNotes.setName(notes.getName());
				        		myNotes.setNotesActivity(notes.getNotesActivity());
				        		myNotes.setNoteGroup(notes.getNoteGroup());
				        		myNotes.setNotesKeyId(notes.getNotesKeyId());
				        		myNotes.setNoteSubType(notes.getNoteSubType());
				        		myNotes.setNoteType(notes.getNoteType());
				        		myNotes.setDateOfContact(notes.getDateOfContact());
				        		myNotes.setFollowUpFor(notes.getFollowUpFor());
				        		myNotes.setFollowUpId(notes.getFollowUpId());
				        		myNotes.setForwardDate(notes.getForwardDate());
				        		myNotes.setForwardStatus(notes.getForwardStatus());
				        		myNotes.setForwardToUser(notes.getForwardToUser());
				        		myNotes.setGrading(notes.getGrading());
				        		myNotes.setIssueType(notes.getIssueType());
				        		myNotes.setLinkedTo(notes.getLinkedTo());
				        		myNotes.setName(notes.getName());
				        		myNotes.setReminderStatus(notes.getReminderStatus());
				        		myNotes.setRemindInterval(notes.getRemindInterval());
				        		myNotes.setRemindTime(myNotes.getRemindTime());
				        		myNotes.setSubject(notes.getSubject());
				        		myNotes.setSupplier(notes.getSupplier());
				        		myNotes.setSystemDate(notes.getSystemDate());
				        		myNotes.setToDoRuleId(notes.getToDoRuleId());
				        		myNotes.setNetworkLinkId(networkLinkId);
				        		myNotes.setBookingAgent(notes.getBookingAgent());
				        		myNotes.setNetworkAgent(notes.getNetworkAgent());
				        		myNotes.setOriginAgent(notes.getOriginAgent());
				        		myNotes.setSubOriginAgent(notes.getSubOriginAgent());
				        		myNotes.setDestinationAgent(notes.getDestinationAgent());
				        		myNotes.setSubDestinationAgent(notes.getSubDestinationAgent());			        		
				        		myNotes.setCreatedBy(notes.getCreatedBy());
				        		myNotes.setCreatedOn(notes.getCreatedOn());
				        		myNotes.setUpdatedBy(notes.getUpdatedBy());
				        		myNotes.setUpdatedOn(notes.getUpdatedOn());
				        		myNotes.setActualValue(notes.getActualValue());
				        		myNotes.setRequestedValue(notes.getRequestedValue());
				        		myNotes.setRequestDate(notes.getRequestDate());
				        		myNotes.setSubmittedDate(notes.getSubmittedDate());
				        		myNotes.setComplitionDate(notes.getComplitionDate());
				        		notesManager.save(myNotes);
			        	 	}
		         		}
					}
				}
		}	
			//------------------For case of edit------------------------------------------------------
			if(notes.getNetworkLinkId()!=null && !(notes.getNetworkLinkId().equalsIgnoreCase(""))){	
				List linkedShipNumberList = null;
				List linkedIdList = notesManager.findlinkedIdList(networkLinkId);
				if(linkedIdList.size() > 1 && linkedIdList!=null && !linkedIdList.isEmpty()){
					if(((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("NBA")) || ((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NNA")) || ((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("NOA")) || ((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("NSOA")) || ((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("NDA")) || ((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("NSDA"))){
						linkedShipNumberList = myFileManager.getLinkedAllShipNumber(serviceOrder.getShipNumber(),checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "unchecked");
					}
					Iterator it = linkedIdList.iterator();
			         while (it.hasNext()) {
			        	 	Long id = Long.parseLong(it.next().toString());
			        	    Notes myNotes = notesManager.getForOtherCorpid(id);		        		
			        	    if(linkedShipNumberList!=null && !linkedShipNumberList.isEmpty() && linkedShipNumberList.contains(myNotes.getNotesId())){
			        	    	myNotes.setBookingAgent(false);
				        		myNotes.setNetworkAgent(false);
				        		myNotes.setOriginAgent(false);
				        		myNotes.setSubOriginAgent(false);
				        		myNotes.setDestinationAgent(false);
				        		myNotes.setSubDestinationAgent(false);
				        		myNotes.setNetworkLinkId("");
			        	     }else{			        	    	
			        	    	if((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("NBA")) {
			        	    		myNotes.setBookingAgent(false);
			        	    	}else{
			        	    		myNotes.setBookingAgent(notes.getBookingAgent());	
			        	    	}
			        	    	if((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NNA")) {
			        	    		myNotes.setNetworkAgent(false);
			        	    	}else{
			        	    		myNotes.setNetworkAgent(notes.getNetworkAgent());	
			        	    	}
			        	    	if((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("NOA")) {
			        	    		myNotes.setOriginAgent(false);
			        	    	}else{
			        	    		myNotes.setOriginAgent(notes.getOriginAgent());
			        	    	}
			        	    	if((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("NSOA")) {
			        	    		myNotes.setSubOriginAgent(false);
			        	    	}else{
			        	    		myNotes.setSubOriginAgent(notes.getSubOriginAgent());
			        	    	}
			        	    	if((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("NDA")) {
			        	    		myNotes.setDestinationAgent(false);
			        	    	}else{
			        	    		myNotes.setDestinationAgent(notes.getDestinationAgent());
			        	    	}
			        	    	if((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("NSDA")) {
			        	    		myNotes.setSubDestinationAgent(false);
			        	    	}else{
			        	    		myNotes.setSubDestinationAgent(notes.getSubDestinationAgent());
			        	    	}				        						        						        						        					        		
				        	    myNotes.setNoteSubType(notes.getNoteSubType());
				        		myNotes.setNoteType(notes.getNoteType());
				        		myNotes.setNoteStatus(notes.getNoteStatus());
				        		myNotes.setNote(notes.getNote());	
				        		myNotes.setFollowUpFor(notes.getFollowUpFor());			        		
				        		myNotes.setMyFileId(notes.getMyFileId());				        					 
				        		myNotes.setForwardDate(notes.getForwardDate());
				        		myNotes.setRemindInterval(notes.getRemindInterval());			        		
				        		myNotes.setRemindTime(notes.getRemindTime());			        		
				        		myNotes.setSubject(notes.getSubject());			        		
				        		myNotes.setNetworkLinkId(networkLinkId);				        		
				        		myNotes.setCreatedBy(notes.getCreatedBy());
				        		myNotes.setCreatedOn(notes.getCreatedOn());
				        		myNotes.setUpdatedBy(notes.getUpdatedBy());
				        		myNotes.setUpdatedOn(notes.getUpdatedOn());
				        		myNotes.setActualValue(notes.getActualValue());
				        		myNotes.setRequestedValue(notes.getRequestedValue());
				        		myNotes.setRequestDate(notes.getRequestDate());
				        		myNotes.setSubmittedDate(notes.getSubmittedDate());
				        		myNotes.setComplitionDate(notes.getComplitionDate());
			        	   }
			        	   notesManager.save(myNotes);
			         	}
					}			
				}
			}
	    }
	
	if(notes.getNoteType()!=null && !notes.getNoteType().equals("")){
		try{
				if ((noteFor.indexOf("ServiceOrder")>-1)||(notes.getNoteType().indexOf("Service")>-1)) {
					try{
						if(systemDefaulttransDoc.indexOf(serviceOrderManager.get(id1).getJob())>-1){
							memoIntegrationFlag="Y";
						}else{
							memoIntegrationFlag="N";
						}
					}catch(Exception e){
						e.printStackTrace();
						if(systemDefaulttransDoc.indexOf(serviceOrderManager.get(id).getJob())>-1){
							memoIntegrationFlag="Y";
						}else{
							memoIntegrationFlag="N";
						}
					}
				}
				if ((noteFor.indexOf("CustomerFile")>-1)||(notes.getNoteType().indexOf("Customer")>-1)) {
					try{
						if(systemDefaulttransDoc.indexOf(customerFileManager.get(id1).getJob())>-1){
							memoIntegrationFlag="Y";
						}else{
							memoIntegrationFlag="N";
						}			
					}catch(Exception e){
						e.printStackTrace();
						if(systemDefaulttransDoc.indexOf(customerFileManager.get(id).getJob())>-1){
							memoIntegrationFlag="Y";
						}else{
							memoIntegrationFlag="N";
						}			
					}
				}
		}catch(Exception ex)
		{ex.printStackTrace();}
	}
	

		try{
		maxId = notesManager.findMaximumId(notes.getNotesId()).get(0).toString();
		minId = notesManager.findMinimumId(notes.getNotesId()).get(0).toString();
		countId = notesManager.findCountId(notes.getNotesId()).get(0).toString();
		maxIdIsRal = notesManager.findMaximumIdIsRal(notes.getCustomerNumber()).get(0).toString();
		minIdIsRal = notesManager.findMinimumIdIsRal(notes.getCustomerNumber()).get(0).toString();
		countIdIsRal = notesManager.findCountIdIsRal(notes.getCustomerNumber()).get(0).toString();
		
		
		linkFileList=notesManager.getMyFileList(id);
		// linkFile=notesManager.getMyFileList(id);
		if(linkFileList!=null && (!(linkFileList.isEmpty())) && linkFileList.get(0)!=null){
			linkFile = linkFileList.get(0).toString();
			if( linkFile!= null && !(linkFile.equals(""))){
				myFileList = myFileManager.getMyFileList(linkFile,sessionCorpID);
			}
		}
		if (resultRecordId != null) {
			if (!resultRecordId.equalsIgnoreCase("")) 
					{
			if(checkListType.equalsIgnoreCase("ForCheckList")) {
				notesManager.updateCheckListResult(resultRecordId, sessionCorpID);
			}else{				
				notesManager.updateResult(resultRecordId, sessionCorpID);
			}
		}
		}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		//System.out.println(popup);
		if (popup == null) {
			String key = (isNew) ? "notes.added" : "notes.updated";
			saveMessage(getText(key));
		}
		try {
			//for(String role:roles){
				if((userType.trim().equalsIgnoreCase("AGENT"))||(userType.trim().equalsIgnoreCase("PARTNER"))||(userType.trim().equalsIgnoreCase("ACCOUNT"))){

					String shipNumber = notes.getNotesId();
					serviceOrder = (ServiceOrder) serviceOrderManager.findShipNumber(shipNumber).get(0);
					//System.out.println("\n\n\n\n\n serviceOrder------>" + serviceOrder.getShipNumber() + "---" + serviceOrder.getFirstName() + "----" + serviceOrder.getCoordinator());
					String emailAdd = "";
					String subject="";
					List coordEmail = customerFileManager.findCoordEmailAddress(serviceOrder.getCoordinator());
					if (!(coordEmail == null) && (!coordEmail.isEmpty())) {
						 emailAdd = coordEmail.get(0).toString().trim();
					}
				
					String from = "support@redskymobility.com";
					
					String tempRecipient="";
					String tempRecipientArr[]=emailAdd.split(",");
					for(String str1:tempRecipientArr){
						if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
							if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
							tempRecipient += str1;
						}
					}
					emailAdd=tempRecipient;
					subject=serviceOrder.getShipNumber() + "---" + serviceOrder.getLastName();
					String messageText1 = "Notes Subject-->" + notes.getSubject();
					messageText1 = messageText1 + "\n\n" + notes.getNote();
					messageText1 = messageText1 + "\n\n\n\n\n" + notes.getCreatedBy();
					 emailSetupManager.globalEmailSetupProcess(from, emailAdd, "", "", "", messageText1, subject, sessionCorpID,"",serviceOrder.getShipNumber(),"");
				}
			//}
				
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}

	//  Method used in if any other action is performed from notesform other than hitting the save button
	@SkipValidation
	public String saveForwardToMyMessage() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		try{
			linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
		}catch(Exception ex){ex.printStackTrace();}
		linkFileList=notesManager.getMyFileList(id);
		// linkFile=notesManager.getMyFileList(id);
		if(linkFileList!=null && (!(linkFileList.isEmpty())) && linkFileList.get(0)!=null){
			linkFile = linkFileList.get(0).toString();
			if( linkFile!= null && !(linkFile.equals(""))){
				myFileList = myFileManager.getMyFileList(linkFile,sessionCorpID);
			}
		}
		List signatureNoteList = notesManager.userSignatureNotes(getRequest().getRemoteUser());
		signatureNote = signatureNoteList.get(0).toString();
		boolean isNew = (notes.getId() == null);
		if (isNew) {
			maxNotesId = Long.parseLong(notesManager.findMaxId().get(0).toString());
			notes.setCreatedOn(new Date());
			notes.setUpdatedOn(new Date());
			notes.setCreatedBy(getRequest().getRemoteUser());
			maxNotesId = maxNotesId + 1;
			notes.setId(maxNotesId);
		} else {
			notes.setUpdatedOn(new Date());
			notes.setUpdatedBy(getRequest().getRemoteUser());
		}
		//System.out.println(save);
		if (save != null) {
			return save();
		}
		if (remindMe.equals("remind")) {
			notes.setReminderStatus("NEW");
			if(notes.getCorpID()==null || notes.getCorpID().toString().equals("") ){
			notes.setCorpID(sessionCorpID);
			}
			if(notes.getNoteType()!=null && notes.getNoteType().equals("FamilyDetails")){
				checkCF="CF";
			}
			if(notes.getNoteStatus()!=null && notes.getNoteStatus().equals("CMP")){
				notes.setReminderStatus("OLD");
			}
			if(userType.equalsIgnoreCase("ACCOUNT"))
			{
				notes.setAccPortal(true);
			}
			if(notes.getFollowUpFor()!=null && !(notes.getFollowUpFor().trim().equalsIgnoreCase("")) && all_user !=null && (!(all_user.containsValue(notes.getFollowUpFor().trim().toUpperCase())))){
				List aliasNameList=userManager.findUserByUsername(notes.getFollowUpFor());
				if(((aliasNameList!=null)&&!aliasNameList.isEmpty())&&aliasNameList.get(0)!=null){
				String aliasName=aliasNameList.get(0).toString();
		        String coordCode=notes.getFollowUpFor().toUpperCase();
				String alias = aliasName.toUpperCase();
				if(!all_user.containsValue(coordCode)){
					all_user.put(coordCode,alias );
				} 
				}
				}
			notes=notesManager.save(notes);
			SimpleDateFormat formatterOut = new SimpleDateFormat("dd-MMM-yyyy");
			if ((popup == null || !popup.equalsIgnoreCase("true")) && notes.getForwardDate() != null) {
				String key = "Follow up has been set for " + formatterOut.format(notes.getForwardDate()) + " at " + notes.getRemindTime() + " (hh:mm)";
				saveMessage(getText(key));
			}
			maxId = notesManager.findMaximumId(notes.getNotesId()).get(0).toString();
			minId = notesManager.findMinimumId(notes.getNotesId()).get(0).toString();
			countId = notesManager.findCountId(notes.getNotesId()).get(0).toString();
			maxIdIsRal = notesManager.findMaximumIdIsRal(notes.getCustomerNumber()).get(0).toString();
			minIdIsRal = notesManager.findMinimumIdIsRal(notes.getCustomerNumber()).get(0).toString();
			countIdIsRal = notesManager.findCountIdIsRal(notes.getCustomerNumber()).get(0).toString();
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
		
		notes.setCorpID(sessionCorpID);
		if(notes.getNoteStatus()!=null && notes.getNoteStatus().equals("CMP")){
			notes.setReminderStatus("OLD");
		}
		if(notes.getNoteStatus()!=null && notes.getNoteStatus().equals("NEW") && notes.getForwardDate()!=null && notes.getFollowUpFor()!=null 
				&& !notes.getFollowUpFor().equals("") && (notes.getReminderStatus()==null || notes.getReminderStatus().equals(""))){
			notes.setReminderStatus("NEW");
		}
		if(notes.getFollowUpFor()!=null && !(notes.getFollowUpFor().trim().equalsIgnoreCase("")) && all_user !=null && (!(all_user.containsValue(notes.getFollowUpFor().trim().toUpperCase())))){
			List aliasNameList=userManager.findUserByUsername(notes.getFollowUpFor());
			if(((aliasNameList!=null)&&!aliasNameList.isEmpty())&&aliasNameList.get(0)!=null){
			String aliasName=aliasNameList.get(0).toString();
	        String coordCode=notes.getFollowUpFor().toUpperCase();
			String alias = aliasName.toUpperCase();
			if(!all_user.containsValue(coordCode)){
				all_user.put(coordCode,alias );
			} 
			}
			}
		notesManager.save(notes);
		maxId = notesManager.findMaximumId(notes.getNotesId()).get(0).toString();
		minId = notesManager.findMinimumId(notes.getNotesId()).get(0).toString();
		countId = notesManager.findCountId(notes.getNotesId()).get(0).toString();
		maxIdIsRal = notesManager.findMaximumIdIsRal(notes.getCustomerNumber()).get(0).toString();
		minIdIsRal = notesManager.findMinimumIdIsRal(notes.getCustomerNumber()).get(0).toString();
		countIdIsRal = notesManager.findCountIdIsRal(notes.getCustomerNumber()).get(0).toString();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		if (!isNew) {
			return INPUT;
		} else {
			return SUCCESS;
		}

	}

	//  Method to search notes
	@SkipValidation
	public String search() {
		getComboList(sessionCorpID);
		boolean noteStatus = (notes.getNoteStatus() == null);
		boolean noteSubType = (notes.getNoteSubType() == null);
		if (!noteStatus || !noteSubType) {
			notess = notesManager.findByNotes(notes.getNoteStatus(), notes.getNoteSubType(), notes.getNotesId());
		}
		return SUCCESS;
	}  
	
	@SkipValidation
	public String searchAllNotes() {
		getComboList(sessionCorpID);
		boolean noteStatus = (notes.getNoteStatus() == null);
		boolean noteSubType = (notes.getNoteSubType() == null);
		if (!noteStatus || !noteSubType) {
			notess = notesManager.findByNotesAll(notes.getNoteStatus(), notes.getNoteSubType(), notes.getNotesId());
		}
		return SUCCESS;
	}

	//  Method to search relevent notes
	@SkipValidation
	public String searchRelevent() {
		getComboList(sessionCorpID);
		boolean noteStatus = (notes.getNoteStatus() == null);
		boolean noteSubType = (notes.getNoteSubType() == null);
		boolean noteId = (notes.getNotesId() == null);
		subType="";
		if (noteFrom.equals("ServiceOrder")) {
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
			serviceOrder = serviceOrderManager.getForOtherCorpid(id);
			}else{
				serviceOrder = serviceOrderManager.get(id);	
			}
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getListByCustomerNumber(customerFile.getSequenceNumber(), subType);
		}
		if (noteFrom.equals("Claim")) {
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
			claim = claimManager.getForOtherCorpid(id);
			}else{
			claim = claimManager.get(id);
			}
			serviceOrder = claim.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName", customerFile.getLastName());
			notess = notesManager.getListByCustomerNumber(customerFile.getSequenceNumber(), subType);
		}
		if (!noteStatus || !noteSubType || !noteId) {
			notess = notesManager.findByReleventNotes(notes.getNoteStatus(), notes.getNoteSubType(), notes.getNotesId());
		}
		return SUCCESS;
	}

	//  Method for reminder functionality no being used now
	public String remindMe() throws Exception {
		getComboList(sessionCorpID);
		if (notes.getForwardToUser().equals("")) {
			jobFor = "JOB NO." + (new Date()).getTime();
			reminderForUser = notes.getUpdatedBy();
		} else {
			jobFor = notes.getForwardToUser() + notes.getForwardDate() + (new Date()).getTime();
			reminderForUser = notes.getForwardToUser();
		}
		SchedulerFactory sf = new StdSchedulerFactory();
		sched = sf.getScheduler();
		JobDetail jd = new JobDetail(jobFor, "group1", ReminderJob.class);
		jd.getJobDataMap().put("message", notes.getNote());
		jd.getJobDataMap().put("toUser", reminderForUser);
		jd.getJobDataMap().put("fromUser", notes.getUpdatedBy());
		jd.getJobDataMap().put("subject", notes.getSubject());
		jd.getJobDataMap().put("sentOn", notes.getUpdatedOn());
		//jd.getJobDataMap().put("sentOn", customerFileNotes.getUpdatedOn());
		//jd.getJobDataMap().put("floatValue",5.14f);
		//CronTrigger ct=new CronTrigger(jobFor,"group2","0 0/1 * * * ?");
		remindTime = notes.getRemindTime();
		//System.out.println(remindTime.substring(0, remindTime.indexOf(":")));
		//System.out.println(remindTime.substring(remindTime.indexOf(":") + 1, remindTime.length()));
		reminderDate = Calendar.getInstance();
		reminderDate.setTime(notes.getForwardDate());
		//System.out.println(reminderDate);
		reminderDate.set(Calendar.HOUR, Integer.parseInt(remindTime.substring(0, remindTime.indexOf(":"))));
		reminderDate.set(Calendar.MINUTE, Integer.parseInt(remindTime.substring(remindTime.indexOf(":") + 1, remindTime.length())));
		//reminderDate.set(Calendar.SECOND, 02);
		//reminderDate.set(Calendar.MILLISECOND, 0);
		Date startTime = reminderDate.getTime();
		//System.out.println((new Date()).getTime());
		SimpleTrigger trigger = new SimpleTrigger(jobFor, null, startTime, null, 0, 0L);
		sched.scheduleJob(jd, trigger);
		//sched.start();
		return SUCCESS;
	}

	@SkipValidation
	public String editReminder() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		notes = notesManager.get(id);
		if (!notes.getCustomerNumber().equalsIgnoreCase("YYYYYYYY") || !notes.getCustomerNumber().equalsIgnoreCase("XXXXXXXX")) {
			List customerFileList = notesManager.getCustomerFilesDetails(notes.getCustomerNumber());
			if (!customerFileList.isEmpty()) {
				customerFile = (CustomerFile) notesManager.getCustomerFilesDetails(notes.getCustomerNumber()).get(0);
				linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
				supplier=notesManager.findSupplier(customerFile.getId());
				System.out.println("supplier is : "+supplier);
				linkFileList=notesManager.getMyFileList(id);
				if(linkFileList!=null && (!(linkFileList.isEmpty())) && linkFileList.get(0)!=null){
					linkFile = linkFileList.get(0).toString();
					if( linkFile!= null && !(linkFile.equals(""))){
						myFileList = myFileManager.getMyFileList(linkFile,sessionCorpID);
					}
				}
			}
		if(notes.getNoteType().equalsIgnoreCase("Service Order")|| notes.getNoteType().equalsIgnoreCase("Account Line") || notes.getNoteType().equalsIgnoreCase("Activity")){
			List serviceorderList = notesManager.getServiceOrederDetils(notes.getNotesId());
			if (!serviceorderList.isEmpty()) {
				serviceOrder = (ServiceOrder) notesManager.getServiceOrederDetils(notes.getNotesId()).get(0);
		         customerFile = customerFileManager.get(serviceOrder.getCustomerFileId());
			 }else{
				 customerFileList = notesManager.getCustomerFilesDetails(notes.getNotesId());
					if (!customerFileList.isEmpty()) {
						customerFile = (CustomerFile) notesManager.getCustomerFilesDetails(notes.getNotesId()).get(0);
					}
			 }
		   }
		
		}
		if(notes.getNoteType().equalsIgnoreCase("AccountContact") || notes.getNoteType().equalsIgnoreCase("AccountProfile")){
			partnerPublic = partnerPublicManager.getPartnerByCode(notes.getNotesId());
			
		}
		if(notes.getNoteType().equalsIgnoreCase("Partner") || notes.getNoteType().equalsIgnoreCase("Partner")){
			partnerPublic = partnerPublicManager.getPartnerByCode(notes.getNotesId());
			
		}
		notesubtype = refMasterManager.findNoteSubType(sessionCorpID, "Container", "NOTESUBTYPE");
		if(notes.getFollowUpFor()!=null && !(notes.getFollowUpFor().trim().equalsIgnoreCase("")) && all_user !=null && (!(all_user.containsValue(notes.getFollowUpFor().trim().toUpperCase())))){
			List aliasNameList=userManager.findUserByUsername(notes.getFollowUpFor());
			if(((aliasNameList!=null)&&!aliasNameList.isEmpty())&&aliasNameList.get(0)!=null){
			String aliasName=aliasNameList.get(0).toString();
	        String coordCode=notes.getFollowUpFor().toUpperCase();
			String alias = aliasName.toUpperCase();
			if(!all_user.containsValue(coordCode)){
				all_user.put(coordCode,alias );
			} 
			}
			}
		notes.setForwardToUser(notes.getCreatedBy());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String saveNotesfromReminder() {
		long idNotess = Long.parseLong(idNote);
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder newFwdDate = new StringBuilder(formats.format(forwardDateUpdate));
		getComboList(sessionCorpID);
		notesManager.updateNotesFromRules(notes.getNote(),notes.getSubject(), idNotess, notes.getFollowUpFor(), getRequest().getRemoteUser(), notes.getRemindInterval(),notes.getNoteType(),notes.getNoteSubType(),notes.getLinkedTo(),notes.getSupplier(),notes.getGrading(),notes.getIssueType());
		notesManager.updateForwardDateFromRules(newFwdDate.toString(), remindTimeUpdate, idNotess);
		linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
		supplier=notesManager.findSupplier(customerFile.getId());
		System.out.println("supplier is : "+supplier);
		linkFileList=notesManager.getMyFileList(id);
		if(linkFileList!=null && (!(linkFileList.isEmpty())) && linkFileList.get(0)!=null){
			linkFile = linkFileList.get(0).toString();
			if( linkFile!= null && !(linkFile.equals(""))){
				myFileList = myFileManager.getMyFileList(linkFile,sessionCorpID);
			}
		}
		notes = notesManager.get(idNotess);
		return SUCCESS;

	}

	@SkipValidation
	public String dismissReminder() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try {
			getComboList(sessionCorpID);
		//	linkedToList= serviceOrderManager.getShipmentNumber(sessionCorpID,customerFile.getSequenceNumber());
			linkedToList =  new ArrayList();
			//supplier = = new ArrayList();
			notesComp = "Yes";
			long idNotess = Long.parseLong(idNote);
			supplier=notesManager.findSupplier(idNotess);
			System.out.println("supplier is : "+supplier);
			logger.warn("idNote"+idNote);
			Notes newNotes = notesManager.get(idNotess);
			String sub =  notes.getSubject();
			String note = notes.getNote();
			Date frwdDate =  notes.getForwardDate();
			String remindTime = notes.getRemindTime();
			String remindIntrval = notes.getRemindInterval();
			String followUpFor = notes.getFollowUpFor();
			  newNotes.setSubject(sub);
			  newNotes.setNote(note);
			  newNotes.setFollowUpFor(followUpFor);
			  newNotes.setForwardDate(frwdDate);
			  newNotes.setRemindTime(remindTime);
			  newNotes.setRemindInterval(remindIntrval);
			  newNotes.setNoteStatus("CMP");
			  newNotes.setReminderStatus("OLD");
			  newNotes.setUpdatedBy(getRequest().getRemoteUser());
			  newNotes.setUpdatedOn(new Date());
			  newNotes.setNoteSubType(notes.getNoteSubType());
			  if(notes.getNoteType()!=null && notes.getNoteType().equals("Issue Resolution") ){
				  newNotes.setComplitionDate(new Date());
				  newNotes.setIssueType(notes.getIssueType());
				  newNotes.setLinkedTo(notes.getLinkedTo());
				  newNotes.setGrading(notes.getGrading());
				  newNotes.setSupplier(notes.getSupplier());
			  }
			  notesManager.save(newNotes);
			//notesManager.dismissReminder(idNotess);
			totalRevenue="1";
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}

	private String userId;

	/*Added by giten for activity managment check the date on 22-May-2008*/
	@SkipValidation
	public String checkActivityDate() throws Exception {
		try {
			//System.out.println("Uid: " + userId);
			Company company = companyManager.findByCorpID(sessionCorpID).get(0);
			activityDateList = notesManager.findByUserActivity(userId, company.getTimeZone());

			if (!activityDateList.isEmpty()) {
				newDefaultDate = activityDateList.get(0).toString();
			}
			dateStatus = newDefaultDate;
		} catch (Exception ex) {
			ex.printStackTrace();
			dateStatus = ex.toString() + "100";
		}
		return SUCCESS;
	}

	@SkipValidation
	public String getSubTypeList() {
		notesubtypeList = refMasterManager.findNoteSubTypeList(sessionCorpID, noteFor, "NOTESUBTYPE");
		if (notesubtypeList.isEmpty() || notesubtypeList == null) {
			notesubtypeList = new ArrayList();
		}
		return SUCCESS;
	}
	private List noteNoteStatuList=new ArrayList();
	
	@SkipValidation
	public String getNoteStatusDetails() {
		noteNoteStatuList = refMasterManager.findNoteSubTypeList(sessionCorpID, noteFor, "NOTESTATUS");	
		return SUCCESS;
	}
	private Map<String, String> SubCategoryDetails= new HashMap<String, String>();
	@SkipValidation
	public String getNoteSubCategoryDetails() {
		if(noteFor.equalsIgnoreCase("Exception Service")){
			issueType = refMasterManager.findServiceByJob("RLO",sessionCorpID, "SERVICE");	
		}else{
			issueType= refMasterManager.findByParameter(sessionCorpID, "ISSUE_TYPE");
		}
		return SUCCESS;
	}
	private String spNumberCorpId; 
	@SkipValidation
	public String getNoteSubCategorylinkedToDetails() {
		if(noteFor.equalsIgnoreCase("Exception Service")){
			issueType = notesManager.getSubCategoryRlo(spNumber,spNumberCorpId);	
		}else{
			issueType= refMasterManager.findByParameter(sessionCorpID, "ISSUE_TYPE");
		}
		return SUCCESS;
	}
	private Map<String, String> gradingDetails= new HashMap<String, String>();
	@SkipValidation
	public String getNoteGradingDetails() {
		if(noteFor.equalsIgnoreCase("Exception Service")){
			gradingDetails = notesManager.getNoteGrading(spNumber,spNumberCorpId);	
		}else{
			gradingDetails= refMasterManager.findByParameter(sessionCorpID, "GRADING");
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String getNoteGradinglinkedToDetails() {
		if(noteFor.equalsIgnoreCase("Exception Service")){
		gradingDetails = notesManager.getNoteGradinglinkedTo(spNumber,spNumberCorpId);	
		}
		return SUCCESS;
	}
	private String tdrID;

	private String resultRecordId;
	
	private String checkListType;

	private ToDoResult toDoResult;

	private ToDoResultManager toDoResultManager;

	private CheckListResult checkListResult;

	private CheckListResultManager checkListResultManager;
	
	@SkipValidation
	public String openRulesNotes() {
		saveFlag = "FALSE";
		remindIntervals = refMasterManager.findByParameter(sessionCorpID, "RMDINTRVL");
		all_user = refMasterManager.findUserNotes(sessionCorpID, "ALL_USER");
		if (!resultRecordId.equalsIgnoreCase("")) {
			List rulesNotes = notesManager.getRulesNotes(fileNumber, tdrID, sessionCorpID);
			if (!rulesNotes.isEmpty()) {
				notes = (Notes) notesManager.getRulesNotes(fileNumber, tdrID, sessionCorpID).get(0);
				notes.setName(toDoResult.getOwner());
			} else {
				notes = new Notes();
				toDoResult = toDoResultManager.get(Long.parseLong(resultRecordId));
				notes.setNoteStatus("NEW");
				notes.setCorpID(sessionCorpID);
				notes.setNotesId(toDoResult.getFileNumber());
				notes.setName(toDoResult.getOwner());
				notes.setToDoRuleId(toDoResult.getTodoRuleId().toString());
				notes.setNotesKeyId(Long.parseLong(toDoResult.getResultRecordId().toString()));
				if (toDoResult.getNoteType()==null && toDoResult.getNoteSubType()==null) {
					//notes.setCustomerNumber(customerFile.getSequenceNumber());

					if (toDoResult.getResultRecordType().equalsIgnoreCase("ServiceOrder")) {
						notes.setNoteType("Service Order");
					}
					if (toDoResult.getResultRecordType().equalsIgnoreCase("CustomerFile")) {
						notes.setNoteType("Customer File");
					}
					if (toDoResult.getResultRecordType().equalsIgnoreCase("AccountLine")) {
						notes.setNoteType("Account Line");
					}
					if (toDoResult.getResultRecordType().equalsIgnoreCase("WorkTicket")) {
						notes.setNoteType("Work Tickets");
					}

					if (toDoResult.getResultRecordType().equalsIgnoreCase("AccountLineList")) {
						notes.setNoteType("Account Line");
					}

					if (toDoResult.getResultRecordType().equalsIgnoreCase("ServicePartner")) {
						notes.setNoteType("Service Partner");
					}

					if (toDoResult.getResultRecordType().equalsIgnoreCase("Vehicle")) {
						notes.setNoteType("Vehicle");
					}
				}
				notes.setNoteSubType("ActivityManagement");
				notes.setNoteType("Activity");
				notes.setCreatedOn(new Date());
				notes.setUpdatedOn(new Date());
				notes.setSystemDate(new Date());
				notes.setRemindTime("00:00");
				notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
				notes.setRemindInterval("15");

			}
		}

		return SUCCESS;
	}

	@SkipValidation
	public String openCheckListNotes() {
		saveFlag = "FALSE";
		if (!resultRecordId.equalsIgnoreCase("")) {
			List rulesNotes = notesManager.getRulesNotes(fileNumber, tdrID, sessionCorpID);
			if (!rulesNotes.isEmpty()) {
				notes = (Notes) notesManager.getRulesNotes(fileNumber, tdrID, sessionCorpID).get(0);
			} else {
				notes = new Notes();
				checkListResult = checkListResultManager.get(Long.parseLong(resultRecordId));
				notes.setNoteStatus("NEW");
				notes.setCorpID(sessionCorpID);
				notes.setNotesId(checkListResult.getResultNumber());
				notes.setToDoRuleId(checkListResult.getCheckListId().toString());
				notes.setNotesKeyId(Long.parseLong(checkListResult.getResultId().toString()));
				
				notes.setNoteSubType("ActivityManagement");
				notes.setNoteType("Activity");
				notes.setCreatedOn(new Date());
				notes.setUpdatedOn(new Date());
				notes.setSystemDate(new Date());

			}
		}

		return SUCCESS;
	}

	
	@SkipValidation
	public String openRulesNotess() {
		remindIntervals = refMasterManager.findByParameter(sessionCorpID, "RMDINTRVL");
		all_user = refMasterManager.findUserNotes(sessionCorpID, "ALL_USER");
		notes = notesManager.get(id);
		return SUCCESS;
	}

	@SkipValidation
	public String findPartnerAlertList(){
		partnerAlertList = notesManager.findPartnerAlertList(notesId);
		return SUCCESS;
	}
	
	@SkipValidation
	public String partnerAlert(){
		notes = notesManager.get(id);
		return SUCCESS;
	}
	
	private String agentsList;
	private String spNumber;
	private String cid;
	@SkipValidation
	public String findDistinctAgents(){
		agentsList=notesManager.findDistinctAgentName(sessionCorpID, spNumber,cid);
		return SUCCESS;
		
	}
	@SkipValidation
	public String findNewAgentPortalDetails(){
		agentsList=notesManager.findDistinctAgentName(spNumberCorpId, spNumber,cid);
		return SUCCESS;
		
	}
	// After Feb 27, 2008
	//Method as per ticket 6699
	private String subjectDesc;
	@SkipValidation
	public String findToolTipSubjectForNotes(){		
		subjectDesc = notesManager.findSubjectDescForNotes(id,sessionCorpID);	
		return SUCCESS;
	}
	private String noteDesc;
	@SkipValidation
	public String findToolTipNoteForNotes(){		
		noteDesc = notesManager.findNoteDescForNotes(id,sessionCorpID);	
		return SUCCESS;
	}
	
	private String notesStatus;
	public String getNotesStatus() {
		return notesStatus;
	}

	public void setNotesStatus(String notesStatus) {
		this.notesStatus = notesStatus;
	}


	@SkipValidation
	public String notesStatus(){
		notesStatus = notesManager.accountLineNotesStatus(accId, sessionCorpID);
		return SUCCESS;
	}
	

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setMessageManager(MessageManager messageManager) {
		this.messageManager = messageManager;
	}

	public List getNotess() {
		return notess;
	}

	public List getRefMasters() {
		return refMasters;
	}

	public List getUsers() {
		return users;
	}

	public void setNoteFor(String noteFor) {
		this.noteFor = noteFor;
	}

	public void setNoteFrom(String noteFrom) {
		this.noteFrom = noteFrom;
	}

	public String getNoteFrom() {
		return noteFrom;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public void setPopup(String popup) {
		this.popup = popup;
	}
	
	public AccountLine getAccountLine() {
		return accountLine;
	}

	public void setAccountLine(AccountLine accountLine) {
		this.accountLine = accountLine;
	}

	public Carton getCarton() {
		return carton;
	}

	public void setCarton(Carton carton) {
		this.carton = carton;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public ServicePartner getServicePartner() {
		return servicePartner;
	}

	public void setServicePartner(ServicePartner servicePartner) {
		this.servicePartner = servicePartner;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public void setCartonManager(CartonManager cartonManager) {
		this.cartonManager = cartonManager;
	}

	public void setContainerManager(ContainerManager containerManager) {
		this.containerManager = containerManager;
	}

	public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
		this.servicePartnerManager = servicePartnerManager;
	}

	public void setVehicleManager(VehicleManager vehicleManager) {
		this.vehicleManager = vehicleManager;
	}

	private AccountProfileManager accountProfileManager;

	public void setAccountProfileManager(AccountProfileManager accountProfileManager) {
		this.accountProfileManager = accountProfileManager;
	}

	private AccountContactManager accountContactManager;

	public void setAccountContactManager(AccountContactManager accountContactManager) {
		this.accountContactManager = accountContactManager;
	}

	// After Feb 27, 2008

	public String getIdNote() {
		return idNote;
	}

	public void setIdNote(String idNote) {
		this.idNote = idNote;
	}

	public Date getForwardDateUpdate() {
		return forwardDateUpdate;
	}

	public void setForwardDateUpdate(Date forwardDateUpdate) {
		this.forwardDateUpdate = forwardDateUpdate;
	}

	public String getRemindTimeUpdate() {
		return remindTimeUpdate;
	}

	public void setRemindTimeUpdate(String remindTimeUpdate) {
		this.remindTimeUpdate = remindTimeUpdate;
	}

	public String getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}

	public String getDateStatus() {
		return dateStatus;
	}

	public void setDateStatus(String dateStatus) {
		this.dateStatus = dateStatus;
	}

	public String getNewDefaultDate() {
		return newDefaultDate;
	}

	public void setNewDefaultDate(String newDefaultDate) {
		this.newDefaultDate = newDefaultDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getForwardToUser() {
		return forwardToUser;
	}

	public void setForwardToUser(String forwardToUser) {
		this.forwardToUser = forwardToUser;
	}

	public Map<String, String> getRemindIntervals() {
		return remindIntervals;
	}

	public String getRemindInterval() {
		return remindInterval;
	}

	public void setRemindInterval(String remindInterval) {
		this.remindInterval = remindInterval;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public AccountProfile getAccountProfile() {
		return accountProfile;
	}

	public void setAccountProfile(AccountProfile accountProfile) {
		this.accountProfile = accountProfile;
	}

	public AccountContact getAccountContact() {
		return accountContact;
	}

	public void setAccountContact(AccountContact accountContact) {
		this.accountContact = accountContact;
	}

	public String getAccountNotesFor() {
		return accountNotesFor;
	}

	public void setAccountNotesFor(String accountNotesFor) {
		this.accountNotesFor = accountNotesFor;
	}

	private String gotoPageString;

	private String validateFormNav;

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getGotoPageString() {

		return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

		this.gotoPageString = gotoPageString;

	}

	public String saveOnTabChange() throws Exception {
		//if (option enabled for this company in the system parameters table then call save) {
		validateFormNav = "OK";
		String s = save(); // else simply navigate to the requested page)
		return gotoPageString;
	}

	public CreditCard getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	public void setCreditCardManager(CreditCardManager creditCardManager) {
		this.creditCardManager = creditCardManager;
	}

	public String getSubType() {
		return subType;
	}

	public List getNotesubtypeList() {
		return notesubtypeList;
	}

	public void setNotesubtypeList(List notesubtypeList) {
		this.notesubtypeList = notesubtypeList;
	}

	public String getFileNumber() {
		return fileNumber;
	}

	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}

	public String getTdrID() {
		return tdrID;
	}

	public void setTdrID(String tdrID) {
		this.tdrID = tdrID;
	}

	public String getResultRecordId() {
		return resultRecordId;
	}

	public void setResultRecordId(String resultRecordId) {
		this.resultRecordId = resultRecordId;
	}

	public ToDoResult getToDoResult() {
		return toDoResult;
	}

	public void setToDoResult(ToDoResult toDoResult) {
		this.toDoResult = toDoResult;
	}

	public void setToDoResultManager(ToDoResultManager toDoResultManager) {
		this.toDoResultManager = toDoResultManager;
	}

	/**
	 * @return the signatureNote
	 */
	public String getSignatureNote() {
		return signatureNote;
	}

	/**
	 * @param signatureNote the signatureNote to set
	 */
	public void setSignatureNote(String signatureNote) {
		this.signatureNote = signatureNote;
	}

	/**
	 * @return the notesFirstName
	 */
	public String getNotesFirstName() {
		return notesFirstName;
	}

	/**
	 * @param notesFirstName the notesFirstName to set
	 */
	public void setNotesFirstName(String notesFirstName) {
		this.notesFirstName = notesFirstName;
	}

	/**
	 * @return the notesLastName
	 */
	public String getNotesLastName() {
		return notesLastName;
	}

	/**
	 * @param notesLastName the notesLastName to set
	 */
	public void setNotesLastName(String notesLastName) {
		this.notesLastName = notesLastName;
	}

	public String getRemindMe() {
		return remindMe;
	}

	/**
	 * @return the maxId
	 */
	public String getMaxId() {
		return maxId;
	}

	/**
	 * @param maxId the maxId to set
	 */
	public void setMaxId(String maxId) {
		this.maxId = maxId;
	}

	/**
	 * @return the minId
	 */
	public String getMinId() {
		return minId;
	}

	/**
	 * @param minId the minId to set
	 */
	public void setMinId(String minId) {
		this.minId = minId;
	}

	/**
	 * @return the countId
	 */
	public String getCountId() {
		return countId;
	}

	/**
	 * @param countId the countId to set
	 */
	public void setCountId(String countId) {
		this.countId = countId;
	}

	/**
	 * @return the soIdNum
	 */
	public Long getSoIdNum() {
		return soIdNum;
	}

	/**
	 * @param soIdNum the soIdNum to set
	 */
	public void setSoIdNum(Long soIdNum) {
		this.soIdNum = soIdNum;
	}

	/**
	 * @return the seqNm
	 */
	public String getSeqNm() {
		return seqNm;
	}

	/**
	 * @param seqNm the seqNm to set
	 */
	public void setSeqNm(String seqNm) {
		this.seqNm = seqNm;
	}

	/**
	 * @return the tempID
	 */
	public long getTempID() {
		return tempID;
	}

	/**
	 * @param tempID the tempID to set
	 */
	public void setTempID(long tempID) {
		this.tempID = tempID;
	}

	/**
	 * @return the noteForNextPrev
	 */
	public String getNoteForNextPrev() {
		return noteForNextPrev;
	}

	/**
	 * @param noteForNextPrev the noteForNextPrev to set
	 */
	public void setNoteForNextPrev(String noteForNextPrev) {
		this.noteForNextPrev = noteForNextPrev;
	}

	/**
	 * @return the notesNextPrevList
	 */
	public List getNotesNextPrevList() {
		return notesNextPrevList;
	}

	/**
	 * @param notesNextPrevList the notesNextPrevList to set
	 */
	public void setNotesNextPrevList(List notesNextPrevList) {
		this.notesNextPrevList = notesNextPrevList;
	}

	/**
	 * @return the maxIdIsRal
	 */
	public String getMaxIdIsRal() {
		return maxIdIsRal;
	}

	/**
	 * @param maxIdIsRal the maxIdIsRal to set
	 */
	public void setMaxIdIsRal(String maxIdIsRal) {
		this.maxIdIsRal = maxIdIsRal;
	}

	/**
	 * @return the minIdIsRal
	 */
	public String getMinIdIsRal() {
		return minIdIsRal;
	}

	/**
	 * @param minIdIsRal the minIdIsRal to set
	 */
	public void setMinIdIsRal(String minIdIsRal) {
		this.minIdIsRal = minIdIsRal;
	}

	/**
	 * @return the countIdIsRal
	 */
	public String getCountIdIsRal() {
		return countIdIsRal;
	}

	/**
	 * @param countIdIsRal the countIdIsRal to set
	 */
	public void setCountIdIsRal(String countIdIsRal) {
		this.countIdIsRal = countIdIsRal;
	}

	public String getNotesId() {
		return notesId;
	}

	public void setNotesId(String notesId) {
		this.notesId = notesId;
	}

	public PartnerPrivate getPartnerPrivate() {
		return partnerPrivate;
	}

	public void setPartnerPrivate(PartnerPrivate partnerPrivate) {
		this.partnerPrivate = partnerPrivate;
	}

	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}
	
	public Map<String, String> getNotetype() {
		return notetype;
	}

	public Map<String, String> getNotesubtype() {
		return notesubtype;
	}

	public Map<String, String> getNotesActivity() {
		return notesActivity;
	}
	public Map<String, String> getNotesubtypecontact() {
		return notesubtypecontact;
	}
	
	public Map<String, String> getNotestatus() {
		return notestatus;
	}

	public Map<String, String> getForwardNoteStatus() {
		return forwardNoteStatus;
	}

	public Map<String, String> getAll_user() {
		return all_user;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setId1(Long id1) {
		this.id1 = id1;
	}

	public void setRemindMe(String remindMe) {
		this.remindMe = remindMe;
	}

	public List getCustomerFiles() {
		return customerFiles;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public Notes getNotes() {
		return notes;
	}

	public void setNotes(Notes notes) {
		this.notes = notes;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public WorkTicket getWorkTicket() {
		return workTicket;
	}

	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}

	public MyMessage getMyMessage() {
		return myMessage;
	}

	public void setMyMessage(MyMessage myMessage) {
		this.myMessage = myMessage;
	}

	public Claim getClaim() {
		return claim;
	}

	public void setClaim(Claim claim) {
		this.claim = claim;
	}

	public List getPartnerAlertList() {
		return partnerAlertList;
	}

	public void setPartnerAlertList(List partnerAlertList) {
		this.partnerAlertList = partnerAlertList;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public String getTotalRevenue() {
		return totalRevenue;
	}

	public void setTotalRevenue(String totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	public PartnerPublic getPartnerPublic() {
		return partnerPublic;
	}

	public void setPartnerPublic(PartnerPublic partnerPublic) {
		this.partnerPublic = partnerPublic;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public void setPayrollManager(PayrollManager payrollManager) {
		this.payrollManager = payrollManager;
	}

	public Payroll getPayroll() {
		return payroll;
	}

	public void setPayroll(Payroll payroll) {
		this.payroll = payroll;
	}
	
	public void setMyFileManager(MyFileManager myFileManager) {
		this.myFileManager = myFileManager;
	}

	public List getMyFileList() {
		return myFileList;
	}

	public void setMyFileList(List myFileList) {
		this.myFileList = myFileList;
	}
	
	public String getLinkFile() {
		return linkFile;
	}

	public void setLinkFile(String linkFile) {
		this.linkFile = linkFile;
	}

	

	public List getLinkFileList() {
		return linkFileList;
	}

	public void setLinkFileList(List linkFileList) {
		this.linkFileList = linkFileList;
	}

	public List getLinkedToList() {
		return linkedToList;
	}

	public void setLinkedToList(List linkedToList) {
		this.linkedToList = linkedToList;
	}

	public Map<String, String> getNotetypeNotForCF() {
		return notetypeNotForCF;
	}

	public void setNotetypeNotForCF(Map<String, String> notetypeNotForCF) {
		this.notetypeNotForCF = notetypeNotForCF;
	}

	public String getCheckCF() {
		return checkCF;
	}

	public void setCheckCF(String checkCF) {
		this.checkCF = checkCF;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Long getNoteId() {
		return noteId;
	}

	public void setNoteId(Long noteId) {
		this.noteId = noteId;
	}

	public Map<String, String> getIssueType() {
		return issueType;
	}

	public void setIssueType(Map<String, String> issueType) {
		this.issueType = issueType;
	}
	public Map<String, String> getGrading() {
		return grading;
	}

	public void setGrading(Map<String, String> grading) {
		this.grading = grading;
	}
	public Map<String, String> getSupplier() {
		return supplier;
	}

	public void setSupplier(Map<String, String> supplier) {
		this.supplier = supplier;
	}

	

	public String getSpNumber() {
		return spNumber;
	}

	public void setSpNumber(String spNumber) {
		this.spNumber = spNumber;
	}

	

	public String getAgentsList() {
		return agentsList;
	}

	public void setAgentsList(String agentsList) {
		this.agentsList = agentsList;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getCheckAgent() {
		return checkAgent;
	}

	public void setCheckAgent(String checkAgent) {
		this.checkAgent = checkAgent;
	}

	public String getBookingAgentFlag() {
		return bookingAgentFlag;
	}

	public void setBookingAgentFlag(String bookingAgentFlag) {
		this.bookingAgentFlag = bookingAgentFlag;
	}

	public String getNetworkAgentFlag() {
		return networkAgentFlag;
	}

	public void setNetworkAgentFlag(String networkAgentFlag) {
		this.networkAgentFlag = networkAgentFlag;
	}

	public String getOriginAgentFlag() {
		return originAgentFlag;
	}

	public void setOriginAgentFlag(String originAgentFlag) {
		this.originAgentFlag = originAgentFlag;
	}

	public String getSubOriginAgentFlag() {
		return subOriginAgentFlag;
	}

	public void setSubOriginAgentFlag(String subOriginAgentFlag) {
		this.subOriginAgentFlag = subOriginAgentFlag;
	}

	public String getDestAgentFlag() {
		return destAgentFlag;
	}

	public void setDestAgentFlag(String destAgentFlag) {
		this.destAgentFlag = destAgentFlag;
	}

	public String getSubDestAgentFlag() {
		return subDestAgentFlag;
	}

	public void setSubDestAgentFlag(String subDestAgentFlag) {
		this.subDestAgentFlag = subDestAgentFlag;
	}

	public String getCheckFlagBA() {
		return checkFlagBA;
	}

	public void setCheckFlagBA(String checkFlagBA) {
		this.checkFlagBA = checkFlagBA;
	}

	public String getCheckFlagNA() {
		return checkFlagNA;
	}

	public void setCheckFlagNA(String checkFlagNA) {
		this.checkFlagNA = checkFlagNA;
	}

	public String getCheckFlagOA() {
		return checkFlagOA;
	}

	public void setCheckFlagOA(String checkFlagOA) {
		this.checkFlagOA = checkFlagOA;
	}

	public String getCheckFlagSOA() {
		return checkFlagSOA;
	}

	public void setCheckFlagSOA(String checkFlagSOA) {
		this.checkFlagSOA = checkFlagSOA;
	}

	public String getCheckFlagDA() {
		return checkFlagDA;
	}

	public void setCheckFlagDA(String checkFlagDA) {
		this.checkFlagDA = checkFlagDA;
	}

	public String getCheckFlagSDA() {
		return checkFlagSDA;
	}

	public void setCheckFlagSDA(String checkFlagSDA) {
		this.checkFlagSDA = checkFlagSDA;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getCheckSO() {
		return checkSO;
	}

	public void setCheckSO(String checkSO) {
		this.checkSO = checkSO;
	}

	public String getBookingAgFlag() {
		return bookingAgFlag;
	}

	public void setBookingAgFlag(String bookingAgFlag) {
		this.bookingAgFlag = bookingAgFlag;
	}

	public String getNetworkAgFlag() {
		return networkAgFlag;
	}

	public void setNetworkAgFlag(String networkAgFlag) {
		this.networkAgFlag = networkAgFlag;
	}

	public String getOriginAgFlag() {
		return originAgFlag;
	}

	public void setOriginAgFlag(String originAgFlag) {
		this.originAgFlag = originAgFlag;
	}

	public String getSubOriginAgFlag() {
		return subOriginAgFlag;
	}

	public void setSubOriginAgFlag(String subOriginAgFlag) {
		this.subOriginAgFlag = subOriginAgFlag;
	}

	public String getDestAgFlag() {
		return destAgFlag;
	}

	public void setDestAgFlag(String destAgFlag) {
		this.destAgFlag = destAgFlag;
	}

	public String getSubDestAgFlag() {
		return subDestAgFlag;
	}

	public void setSubDestAgFlag(String subDestAgFlag) {
		this.subDestAgFlag = subDestAgFlag;
	}

	public String getControlFlag() {
		return controlFlag;
	}

	public void setControlFlag(String controlFlag) {
		this.controlFlag = controlFlag;
	}

	public AuditTrail getAuditTrail() {
		return auditTrail;
	}
	public Map<String, String> getUvlmemocategory() {
		return uvlmemocategory;
	}

	public void setUvlmemocategory(Map<String, String> uvlmemocategory) {
		this.uvlmemocategory = uvlmemocategory;
	}

	public Map<String, String> getUvlmemoxfer() {
		return uvlmemoxfer;
	}

	public void setUvlmemoxfer(Map<String, String> uvlmemoxfer) {
		this.uvlmemoxfer = uvlmemoxfer;
	}

	public String getMemoIntegrationFlag() {
		return memoIntegrationFlag;
	}

	public void setMemoIntegrationFlag(String memoIntegrationFlag) {
		this.memoIntegrationFlag = memoIntegrationFlag;
	}


	public void setAuditTrail(AuditTrail auditTrail) {
		this.auditTrail = auditTrail;
	}

	public void setAuditTrailManager(AuditTrailManager auditTrailManager) {
		this.auditTrailManager = auditTrailManager;
	}
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getSubjectDesc() {
		return subjectDesc;
	}

	public void setSubjectDesc(String subjectDesc) {
		this.subjectDesc = subjectDesc;
	}

	public String getNoteDesc() {
		return noteDesc;
	}

	public void setNoteDesc(String noteDesc) {
		this.noteDesc = noteDesc;
	}



	public String getSaveFlag() {
		return saveFlag;
	}



	public void setSaveFlag(String saveFlag) {
		this.saveFlag = saveFlag;
	}



	public String getNoteFor() {
		return noteFor;
	}



	public Long getPPID() {
		return PPID;
	}



	public void setPPID(Long pPID) {
		PPID = pPID;
	}



	public void setMssManager(MssManager mssManager) {
		this.mssManager = mssManager;
	}



	public Mss getMss() {
		return mss;
	}



	public void setMss(Mss mss) {
		this.mss = mss;
	}

	public String getNetwokAccess() {
		return NetwokAccess;
	}

	public void setNetwokAccess(String netwokAccess) {
		NetwokAccess = netwokAccess;
	}

	public String getNotesComp() {
		return notesComp;
	}

	public void setNotesComp(String notesComp) {
		this.notesComp = notesComp;
	}

	public List getNoteNoteStatuList() {
		return noteNoteStatuList;
	}

	public void setNoteNoteStatuList(List noteNoteStatuList) {
		this.noteNoteStatuList = noteNoteStatuList;
	}

	public Map<String, String> getSubCategoryDetails() {
		return SubCategoryDetails;
	}

	public void setSubCategoryDetails(Map<String, String> subCategoryDetails) {
		SubCategoryDetails = subCategoryDetails;
	}

	public Map<String, String> getGradingDetails() {
		return gradingDetails;
	}

	public void setGradingDetails(Map<String, String> gradingDetails) {
		this.gradingDetails = gradingDetails;
	}

	public String getSpNumberCorpId() {
		return spNumberCorpId;
	}

	public void setSpNumberCorpId(String spNumberCorpId) {
		this.spNumberCorpId = spNumberCorpId;
	}

	public CheckListResult getCheckListResult() {
		return checkListResult;
	}

	public void setCheckListResult(CheckListResult checkListResult) {
		this.checkListResult = checkListResult;
	}

	public void setCheckListResultManager(
			CheckListResultManager checkListResultManager) {
		this.checkListResultManager = checkListResultManager;
	}

	public String getCheckListType() {
		return checkListType;
	}

	public void setCheckListType(String checkListType) {
		this.checkListType = checkListType;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}

	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}

	public String getMmValidation() {
		return mmValidation;
	}

	public void setMmValidation(String mmValidation) {
		this.mmValidation = mmValidation;
	}

	public String getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}

	public Map<String, String> getRoleCausedBy() {
		return roleCausedBy;
	}

	public void setRoleCausedBy(Map<String, String> roleCausedBy) {
		this.roleCausedBy = roleCausedBy;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public TrackingStatusManager getTrackingStatusManager() {
		return trackingStatusManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}

	public String getAgentcorpID() {
		return agentcorpID;
	}

	public void setAgentcorpID(String agentcorpID) {
		this.agentcorpID = agentcorpID;
	}

	public String getFlagAcc() {
		return flagAcc;
	}

	public void setFlagAcc(String flagAcc) {
		this.flagAcc = flagAcc;
	}
	public DsFamilyDetailsManager getDsFamilyDetailsManager() {
		return dsFamilyDetailsManager;
	}

	public void setDsFamilyDetailsManager(DsFamilyDetailsManager dsFamilyDetailsManager) {
		this.dsFamilyDetailsManager = dsFamilyDetailsManager;
	}
	public DsFamilyDetails getDsFamilyDetails() {
		return dsFamilyDetails;
	}

	public void setDsFamilyDetails(DsFamilyDetails dsFamilyDetails) {
		this.dsFamilyDetails = dsFamilyDetails;
	}
	public String getAccId() {
	return accId;
}

public void setAccId(String accId) {
	this.accId = accId;
}

public String getAccportalFlag() {
	return accportalFlag;
}

public void setAccportalFlag(String accportalFlag) {
	this.accportalFlag = accportalFlag;
}

	
}
/*
 class MessageSender extends TimerTask {
 MyMessage myMessage;
 MessageManager messageManager;
 MessageDaoHibernate messageDaoHibernate;
 public void setMessageManager(MessageManager messageManager) {
 this.messageManager = messageManager;
 }
 public MyMessage getMyMessage() {   
 return myMessage;   
 }   

 public void setMyMessage(MyMessage myMessage) {   
 this.myMessage = myMessage;   
 } 
 public void run() {

 System.out.println("Performing Task");
 //myMessage.setMessage("Timer Message");
 messageDaoHibernate.remindMessage();
 }
 }*/