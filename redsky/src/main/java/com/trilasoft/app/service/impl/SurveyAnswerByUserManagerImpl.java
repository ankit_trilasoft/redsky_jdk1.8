package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.SurveyAnswerByUserDao;
import com.trilasoft.app.model.SurveyAnswerByUser;
import com.trilasoft.app.service.SurveyAnswerByUserManager;

public class SurveyAnswerByUserManagerImpl extends GenericManagerImpl<SurveyAnswerByUser, Long> implements SurveyAnswerByUserManager{
	SurveyAnswerByUserDao surveyAnswerByUserDao;
	
	public SurveyAnswerByUserManagerImpl(SurveyAnswerByUserDao surveyAnswerByUserDao) {
		super(surveyAnswerByUserDao);
		this.surveyAnswerByUserDao = surveyAnswerByUserDao;
	}
	
	public List getRecordsByQuestionId(Long qId,Long sid,Long cid){
		return surveyAnswerByUserDao.getRecordsByQuestionId(qId, sid, cid);
	}
	
	public List getRecordsBySOId(Long sid){
		return surveyAnswerByUserDao.getRecordsBySOId(sid);
	}
	public List getRecordsByCId(Long cid){
		return surveyAnswerByUserDao.getRecordsByCId(cid);
	}
	
}
