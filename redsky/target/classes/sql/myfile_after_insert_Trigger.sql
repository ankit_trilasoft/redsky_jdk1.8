

delimiter $$

CREATE  trigger redsky.trigger_add_history_myfile_add

after insert on redsky.myfile

for each row

BEGIN

  DECLARE fieldNameList LONGTEXT;

  DECLARE oldValueList LONGTEXT;

  DECLARE newValueList LONGTEXT;

  DECLARE oldCorpid LONGTEXT;

  DECLARE newUser LONGTEXT;

  DECLARE newRoleName LONGTEXT;

 

  SET fieldNameList = " ";

  SET oldValueList = " ";

  SET newValueList = " ";

  SET oldCorpid = " ";

  SET newUser = " ";

  SET newRoleName = " ";

 

 
IF (NEW.location <> '' or NEW.location is not null) THEN

       select CONCAT(fieldNameList,'myfile.location~') into fieldNameList;

       select CONCAT(newValueList,NEW.location,'~') into newValueList;

   END IF;

 

 IF (NEW.corpID <> '' or NEW.corpID is not null) THEN

       select CONCAT(fieldNameList,'myfile.corpID~') into fieldNameList;

       select CONCAT(newValueList,NEW.corpID,'~') into newValueList;

   END IF;

 

 

 IF (NEW.customerNumber <> '' or NEW.customerNumber is not null) THEN

       select CONCAT(fieldNameList,'myfile.customerNumber~') into fieldNameList;

       select CONCAT(newValueList,NEW.customerNumber,'~') into newValueList;

   END IF;

 

 IF (NEW.fileContentType <> '' or NEW.fileContentType is not null) THEN

       select CONCAT(fieldNameList,'myfile.fileContentType~') into fieldNameList;

       select CONCAT(newValueList,NEW.fileContentType,'~') into newValueList;

   END IF;

 

 IF (NEW.fileFileName <> '' or NEW.fileFileName is not null) THEN

       select CONCAT(fieldNameList,'myfile.fileFileName~') into fieldNameList;

       select CONCAT(newValueList,NEW.fileFileName,'~') into newValueList;

   END IF;

 

 IF (NEW.fileType <> '' or NEW.fileType is not null) THEN

       select CONCAT(fieldNameList,'myfile.fileType~') into fieldNameList;

       select CONCAT(newValueList,NEW.fileType,'~') into newValueList;

   END IF;

 

 


 IF (NEW.fileId <> '' or NEW.fileId is not null) THEN

       select CONCAT(fieldNameList,'myfile.fileId~') into fieldNameList;

       select CONCAT(newValueList,NEW.fileId,'~') into newValueList;

   END IF;

 

 


 IF (NEW.description <> '' or NEW.description is not null) THEN

       select CONCAT(fieldNameList,'myfile.description~') into fieldNameList;

       select CONCAT(newValueList,NEW.description,'~') into newValueList;

   END IF;

 

 


 IF (NEW.mapFolder <> '' or NEW.mapFolder is not null) THEN

       select CONCAT(fieldNameList,'myfile.mapFolder~') into fieldNameList;

       select CONCAT(newValueList,NEW.mapFolder,'~') into newValueList;

   END IF;

 

 


 IF (NEW.fileSize <> '' or NEW.fileSize is not null) THEN

       select CONCAT(fieldNameList,'myfile.fileSize~') into fieldNameList;

       select CONCAT(newValueList,NEW.fileSize,'~') into newValueList;

   END IF;

 

 


 IF (NEW.networkLinkId <> '' or NEW.networkLinkId is not null) THEN

       select CONCAT(fieldNameList,'myfile.networkLinkId~') into fieldNameList;

       select CONCAT(newValueList,NEW.networkLinkId,'~') into newValueList;

   END IF;

 

 


 IF (NEW.transDocStatus <> '' or NEW.transDocStatus is not null) THEN

       select CONCAT(fieldNameList,'myfile.transDocStatus~') into fieldNameList;

       select CONCAT(newValueList,NEW.transDocStatus,'~') into newValueList;

   END IF;

 

 

 IF (NEW.transferredBy <> '' or NEW.transferredBy is not null) THEN

       select CONCAT(fieldNameList,'myfile.transferredBy~') into fieldNameList;

       select CONCAT(newValueList,NEW.transferredBy,'~') into newValueList;

   END IF;

   

 IF (NEW.emailStatus <> '' or NEW.emailStatus is not null) THEN

       select CONCAT(fieldNameList,'myfile.emailStatus~') into fieldNameList;

       select CONCAT(newValueList,NEW.emailStatus,'~') into newValueList;

   END IF;


   

 IF (NEW.documentCategory <> '' or NEW.documentCategory is not null) THEN

       select CONCAT(fieldNameList,'myfile.documentCategory~') into fieldNameList;

       select CONCAT(newValueList,NEW.documentCategory,'~') into newValueList;

   END IF;


   

 
  

     IF (NEW.docSent <>'' or  NEW.docSent is not null) then

       select CONCAT(fieldNameList,'myfile.docSent~') into fieldNameList;

	select CONCAT(newValueList,NEW.docSent,'~') into newValueList;


   END IF;





    IF (NEW.isCportal <>'' or NEW.isCportal is not null) THEN

       select CONCAT(fieldNameList,'myfile.isCportal~') into fieldNameList;
	
	    IF(NEW.isCportal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isCportal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






    IF (NEW.isAccportal <>'' or NEW.isAccportal is not null) THEN

       select CONCAT(fieldNameList,'myfile.isAccportal~') into fieldNameList;
	
	    IF(NEW.isAccportal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isAccportal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   


    IF (NEW.isPartnerPortal <>'' or NEW.isPartnerPortal is not null) THEN

       select CONCAT(fieldNameList,'myfile.isPartnerPortal~') into fieldNameList;
	
	    IF(NEW.isPartnerPortal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isPartnerPortal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   


    IF (NEW.coverPageStripped <>'' or NEW.coverPageStripped is not null) THEN

       select CONCAT(fieldNameList,'myfile.coverPageStripped~') into fieldNameList;
	
	    IF(NEW.coverPageStripped = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.coverPageStripped = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   


    IF (NEW.active <>'' or NEW.active is not null) THEN

       select CONCAT(fieldNameList,'myfile.active~') into fieldNameList;
	
	    IF(NEW.active = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.active = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   


    IF (NEW.isSecure <>'' or NEW.isSecure is not null) THEN

       select CONCAT(fieldNameList,'myfile.isSecure~') into fieldNameList;
	
	    IF(NEW.isSecure = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isSecure = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   


    IF (NEW.isBookingAgent <>'' or NEW.isBookingAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isBookingAgent~') into fieldNameList;
	
	    IF(NEW.isBookingAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isBookingAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   


    IF (NEW.isNetworkAgent <>'' or NEW.isNetworkAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isNetworkAgent~') into fieldNameList;
	
	    IF(NEW.isNetworkAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isNetworkAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   


    IF (NEW.isOriginAgent <>'' or NEW.isOriginAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isOriginAgent~') into fieldNameList;
	
	    IF(NEW.isOriginAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isOriginAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   


    IF (NEW.isSubOriginAgent <>'' or NEW.isSubOriginAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isSubOriginAgent~') into fieldNameList;
	
	    IF(NEW.isSubOriginAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isSubOriginAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   


    IF (NEW.isDestAgent <>'' or NEW.isDestAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isDestAgent~') into fieldNameList;
	
	    IF(NEW.isDestAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isDestAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   


    IF (NEW.isSubDestAgent <>'' or NEW.isSubDestAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isSubDestAgent~') into fieldNameList;

	    IF(NEW.isSubDestAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isSubDestAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


    IF (NEW.isCportal <>'' or NEW.isCportal is not null) THEN

       select CONCAT(fieldNameList,'myfile.isCportal~') into fieldNameList;
	
	    IF(NEW.isCportal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isCportal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






    IF (NEW.isAccportal <>'' or NEW.isAccportal is not null) THEN

       select CONCAT(fieldNameList,'myfile.isAccportal~') into fieldNameList;

	    IF(NEW.isAccportal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isAccportal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   


    IF (NEW.isServiceProvider <>'' or NEW.isServiceProvider is not null) THEN

       select CONCAT(fieldNameList,'myfile.isServiceProvider~') into fieldNameList;
	
	    IF(NEW.isServiceProvider = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isServiceProvider = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   

    IF (NEW.isPartnerPortal <>'' or NEW.isPartnerPortal is not null) THEN

       select CONCAT(fieldNameList,'myfile.isPartnerPortal~') into fieldNameList;
	
	    IF(NEW.isPartnerPortal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isPartnerPortal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   


    IF (NEW.isBookingAgent <>'' or NEW.isBookingAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isBookingAgent~') into fieldNameList;
	
	    IF(NEW.isBookingAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isBookingAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   


    IF (NEW.isNetworkAgent <>'' or NEW.isNetworkAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isNetworkAgent~') into fieldNameList;
	
	    IF(NEW.isNetworkAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isNetworkAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





    IF (NEW.isOriginAgent <>'' or NEW.isOriginAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isOriginAgent~') into fieldNameList;
	
	    IF(NEW.isOriginAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isOriginAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   


    IF (NEW.isSubOriginAgent <>'' or NEW.isSubOriginAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isSubOriginAgent~') into fieldNameList;
	
	    IF(NEW.isSubOriginAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isSubOriginAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   


    IF (NEW.isDestAgent <>'' or NEW.isDestAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isDestAgent~') into fieldNameList;

	    IF(NEW.isDestAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isDestAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   


    IF (NEW.isSubDestAgent <>'' or NEW.isSubDestAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isSubDestAgent~') into fieldNameList;

	    IF(NEW.isSubDestAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;

	    IF(NEW.isSubDestAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   
   IF (NEW.invoiceAttachment <>'' or NEW.invoiceAttachment is not null) THEN

       select CONCAT(fieldNameList,'myfile.invoiceAttachment~') into fieldNameList;

	    IF(NEW.invoiceAttachment = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.invoiceAttachment = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   
   IF (NEW.accountLinePayingStatus <> '' or NEW.accountLinePayingStatus is not null) THEN

       select CONCAT(fieldNameList,'myfile.accountLinePayingStatus~') into fieldNameList;

       select CONCAT(newValueList,NEW.accountLinePayingStatus,'~') into newValueList;

   END IF;
   
   
   
   IF (NEW.accountLineVendorCode <> '' or NEW.accountLineVendorCode is not null) THEN

       select CONCAT(fieldNameList,'myfile.accountLineVendorCode~') into fieldNameList;

       select CONCAT(newValueList,NEW.accountLineVendorCode,'~') into newValueList;

   END IF;


CALL add_tblHistory (NEW.id,"myfile", fieldNameList, oldValueList, newValueList, NEW.updatedby, NEW.corpID, now());



END $$
delimiter;
 