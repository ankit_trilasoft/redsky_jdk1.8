package com.trilasoft.app.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name="inventorydata")
public class InventoryData {
	private String corpID;
	private String quantity;
	private String atricle;
	private String comment;
	private String mode;
	private String cft;
	private String total;
	private String type;
	private Long customerFileID;
	private Long id;
	private Long serviceOrderID;
	private Integer valuation;
	private String room;
    private String weight;
    private String location;
    private String other;
    private Integer width;
    private Integer height;
    private Integer length;
    private Boolean dismantling;
    private Boolean assembling;
    private Boolean specialContainer;
    private Boolean isValuable;
    private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	private Date createdOn;
	private String articleCondition;
	private Boolean ntbsFlag;
	private Boolean imagesAvailablityFlag;
	private String totalWeight;
	private String barCode;
	private Long networkSynchedId;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("quantity", quantity).append("atricle", atricle)
				.append("comment", comment).append("mode", mode)
				.append("cft", cft).append("total", total).append("type", type)
				.append("customerFileID", customerFileID).append("id", id)
				.append("serviceOrderID", serviceOrderID)
				.append("valuation", valuation).append("room", room)
				.append("weight", weight).append("location", location)
				.append("other", other).append("width", width)
				.append("height", height).append("length", length)
				.append("dismantling", dismantling)
				.append("assembling", assembling)
				.append("specialContainer", specialContainer)
				.append("isValuable", isValuable)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("createdOn", createdOn)
				.append("articleCondition", articleCondition)
				.append("ntbsFlag", ntbsFlag)
				.append("imagesAvailablityFlag", imagesAvailablityFlag)
				.append("totalWeight", totalWeight).append("barCode", barCode)
				.append("networkSynchedId", networkSynchedId).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof InventoryData))
			return false;
		InventoryData castOther = (InventoryData) other;
		return new EqualsBuilder().append(corpID, castOther.corpID)
				.append(quantity, castOther.quantity)
				.append(atricle, castOther.atricle)
				.append(comment, castOther.comment)
				.append(mode, castOther.mode).append(cft, castOther.cft)
				.append(total, castOther.total).append(type, castOther.type)
				.append(customerFileID, castOther.customerFileID)
				.append(id, castOther.id)
				.append(serviceOrderID, castOther.serviceOrderID)
				.append(valuation, castOther.valuation)
				.append(room, castOther.room).append(weight, castOther.weight)
				.append(location, castOther.location)
				.append(other, castOther.other).append(width, castOther.width)
				.append(height, castOther.height)
				.append(length, castOther.length)
				.append(dismantling, castOther.dismantling)
				.append(assembling, castOther.assembling)
				.append(specialContainer, castOther.specialContainer)
				.append(isValuable, castOther.isValuable)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(createdOn, castOther.createdOn)
				.append(articleCondition, castOther.articleCondition)
				.append(ntbsFlag, castOther.ntbsFlag)
				.append(imagesAvailablityFlag, castOther.imagesAvailablityFlag)
				.append(totalWeight, castOther.totalWeight)
				.append(barCode, castOther.barCode)
				.append(networkSynchedId, castOther.networkSynchedId)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(quantity)
				.append(atricle).append(comment).append(mode).append(cft)
				.append(total).append(type).append(customerFileID).append(id)
				.append(serviceOrderID).append(valuation).append(room)
				.append(weight).append(location).append(other).append(width)
				.append(height).append(length).append(dismantling)
				.append(assembling).append(specialContainer).append(isValuable)
				.append(createdBy).append(updatedBy).append(updatedOn)
				.append(createdOn).append(articleCondition).append(ntbsFlag)
				.append(imagesAvailablityFlag).append(totalWeight)
				.append(barCode).append(networkSynchedId).toHashCode();
	}
	@Column
	public String getAtricle() {
		return atricle;
	}
	public void setAtricle(String atricle) {
		this.atricle = atricle;
	}
	@Column
	public String getCft() {
		return cft;
	}
	public void setCft(String cft) {
		this.cft = cft;
	}
	@Column
	public String getComment() {
		return comment;
	}
	@Column
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public Long getCustomerFileID() {
		return customerFileID;
	}
	public void setCustomerFileID(Long customerFileID) {
		this.customerFileID = customerFileID;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	@Column
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	@Column
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	@Column
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	@Column
	public Long getServiceOrderID() {
		return serviceOrderID;
	}
	public void setServiceOrderID(Long serviceOrderID) {
		this.serviceOrderID = serviceOrderID;
	}
	@Column
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}
	@Column
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	@Column
	public Integer getValuation() {
		return valuation;
	}
	public void setValuation(Integer valuation) {
		this.valuation = valuation;
	}
	@Column
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	@Column
	public String getOther() {
		return other;
	}
	public void setOther(String other) {
		this.other = other;
	}
	@Column
	public Boolean getAssembling() {
		return assembling;
	}
	public void setAssembling(Boolean assembling) {
		this.assembling = assembling;
	}
	@Column
	public Boolean getDismantling() {
		return dismantling;
	}
	public void setDismantling(Boolean dismantling) {
		this.dismantling = dismantling;
	}
	@Column
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	@Column
	public Boolean getIsValuable() {
		return isValuable;
	}
	public void setIsValuable(Boolean isValuable) {
		this.isValuable = isValuable;
	}
	@Column
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	@Column
	public Boolean getSpecialContainer() {
		return specialContainer;
	}
	public void setSpecialContainer(Boolean specialContainer) {
		this.specialContainer = specialContainer;
	}
	@Column
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	@Column
	public String getArticleCondition() {
		return articleCondition;
	}
	public void setArticleCondition(String articleCondition) {
		this.articleCondition = articleCondition;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/*@OneToMany(mappedBy="inventoryData", cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	public Set<InventoryPath> getInvetoryPath() {
		return invetoryPath;
	}
	public void setInvetoryPath(Set<InventoryPath> invetoryPath) {
		this.invetoryPath = invetoryPath;
	}*/
	@Column
	public Boolean getNtbsFlag() {
		return ntbsFlag;
	}
	public void setNtbsFlag(Boolean ntbsFlag) {
		this.ntbsFlag = ntbsFlag;
	}
	@Column
	public Boolean getImagesAvailablityFlag() {
		return imagesAvailablityFlag;
	}
	public void setImagesAvailablityFlag(Boolean imagesAvailablityFlag) {
		this.imagesAvailablityFlag = imagesAvailablityFlag;
	}
	@Column
	public String getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(String totalWeight) {
		this.totalWeight = totalWeight;
	}
	/**
	 * @return the barCode
	 */
	@Column
	public String getBarCode() {
		return barCode;
	}
	/**
	 * @param barCode the barCode to set
	 */
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public Long getNetworkSynchedId() {
		return networkSynchedId;
	}
	public void setNetworkSynchedId(Long networkSynchedId) {
		this.networkSynchedId = networkSynchedId;
	}
	
	

}