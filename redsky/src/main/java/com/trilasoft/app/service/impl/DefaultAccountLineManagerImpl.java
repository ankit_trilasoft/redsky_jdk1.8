package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DefaultAccountLineDao;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.DefaultAccountLine;
import com.trilasoft.app.service.DefaultAccountLineManager;

	public class DefaultAccountLineManagerImpl extends GenericManagerImpl<DefaultAccountLine, Long> implements DefaultAccountLineManager { 
		DefaultAccountLineDao defaultAccountLineDao; 
	     
	  
	    public DefaultAccountLineManagerImpl(DefaultAccountLineDao defaultAccountLineDao) { 
	        super(defaultAccountLineDao); 
	        this.defaultAccountLineDao = defaultAccountLineDao; 
	    }
	    
	    public List<DefaultAccountLine> findDefaultAccountLineList(){
	    	return defaultAccountLineDao.findDefaultAccountLineList();
	    }


		public List getDefaultList(String jobType, String routing, String mode) {
			return defaultAccountLineDao.getDefaultList(jobType, routing, mode);
		}

		public List<DefaultAccountLine> findDefaultAccountLines(String jobType, String route, String mode, String serviceType, String categories, String billToCode,String contract,String packMode,String commodity, String compDiv, String oCountry, String dCountry,String equipment) {
		///	return defaultAccountLineDao.findDefaultAccountLines(jobType, route, mode, serviceType, categories);
			return defaultAccountLineDao.findDefaultAccountLines(jobType, route, mode, serviceType, categories,billToCode,contract, packMode,commodity,compDiv,oCountry,dCountry, equipment);
		} 
		public List defaultALUpdate(Long id){
			return defaultAccountLineDao.defaultALUpdate(id);
			
		}

		public List findAccountInterface(String corpID) {
			
			return defaultAccountLineDao.findAccountInterface(corpID);
		}

		public List getDefaultAccountList(String jobType, String routing, String mode, String contract, String billToCode,String packingMode, String commodity, String serviceType, String companyDivision,String originCountry,String destinationCountry,String equipment,String originCity,String destinationCity) {
			
			return defaultAccountLineDao.getDefaultAccountList(jobType, routing, mode, contract, billToCode,packingMode, commodity,serviceType,companyDivision,originCountry,destinationCountry, equipment, originCity, destinationCity);
		}

		public List findBillToCode(String corpID) {
			
			return defaultAccountLineDao.findBillToCode(corpID);
		}

		public List findContractList(String corpID) {
			
			return defaultAccountLineDao.findContractList(corpID);
		}

		public List getAcctTemplateList(String contract, String sessionCorpID) {
			return defaultAccountLineDao.getAcctTemplateList(contract, sessionCorpID);
			
		}

		public List findChargesByTemplate(String sessionCorpID) {
			
			return defaultAccountLineDao.findChargesByTemplate(sessionCorpID);
		}

		public List getChargeTemplate(String contract, String sessionCorpID) {
			
			return defaultAccountLineDao.getChargeTemplate(contract, sessionCorpID);
		}

		public String saveBulkUpdates(String id, String changedValue) {
			return defaultAccountLineDao.saveBulkUpdates(id, changedValue);
		}

		public List getAllValues(String sessionCorpID) {
			return defaultAccountLineDao.getAllValues(sessionCorpID);
		}

		public List findContracOnJobBasis(String billToCode, String job, Date createdon, String corpID) {
			return defaultAccountLineDao.findContracOnJobBasis(billToCode, job, createdon, corpID);
		}
		  
		public void saveUpdatesList(String allValue,String corpId,String updateby) {
			 defaultAccountLineDao.saveUpdatesList(allValue,corpId,updateby);
		}
		public List findSearchNameAutocompleteListFromDefaultAccountList(String searchName, String corpId,String populatedatafield){
			return defaultAccountLineDao.findSearchNameAutocompleteListFromDefaultAccountList(searchName,corpId,populatedatafield);
		}
		public List findSearchChargeCodeAutocompleteList(String searchName, String corpId,String contract,String formtype,String compDivision,String originCountry,String destinCountry,String mode,String category){
			return defaultAccountLineDao.findSearchChargeCodeAutocompleteList(searchName,corpId,contract,formtype,compDivision,originCountry,destinCountry,mode,category);
		}
		public List findContractListNEW(String corpID) {
			return defaultAccountLineDao.findContractListNEW(corpID);
		}
		public List findContractListFromContract(String corpID) {
			return defaultAccountLineDao.findContractListFromContract(corpID);
		}
		public List findContractListFromTemplate(String corpID) {
			return defaultAccountLineDao.findContractListFromTemplate(corpID);
		}
		public List<Charges> findChargeCodeForMissingDefaultAccountLineContract(String fromcontract,String corpID,String toContract){
	    	return defaultAccountLineDao.findChargeCodeForMissingDefaultAccountLineContract(fromcontract,corpID,toContract);
	    }
		public int getChargeTemplateCopy(String fromContract, String sessionCorpID ,String id,String toContract ) {
			return defaultAccountLineDao.getChargeTemplateCopy(fromContract, sessionCorpID,id,toContract);
		}
		public List findSameDefaultAccountLineContract(String fromcontract,String corpID,String toContract){
	    	return defaultAccountLineDao.findSameDefaultAccountLineContract(fromcontract,corpID,toContract);
	    }
		public List findChargeCodeForMisMatchDefaultAccountLine(String fromcontract,String corpID,String toContract){
	    	return defaultAccountLineDao.findChargeCodeForMisMatchDefaultAccountLine(fromcontract,corpID,toContract);
	    }
		public List findChargeCodeForMisMatchFromCharges(String fromcontract,String corpID,String toContract){
	    	return defaultAccountLineDao.findChargeCodeForMisMatchFromCharges(fromcontract,corpID,toContract);
	    }
		public List getDuplicatedData(String corpID){
	    	return defaultAccountLineDao.getDuplicatedData(corpID);
	    }
		public String deleteDuplicateTemplateData(String corpID,String listedId){
	    	return defaultAccountLineDao.deleteDuplicateTemplateData(corpID,listedId);
	    }
		public int findChargeCodeForMissingDefaultAccountLineContractWithoutValidation(String userName,String fromcontract,String corpID,String toContract)
		{ 	
			return defaultAccountLineDao.findChargeCodeForMissingDefaultAccountLineContractWithoutValidation(userName,fromcontract,corpID,toContract);
		}
		
		
		

}
