package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.DataSecurityFilter;
import com.trilasoft.app.model.ForwardingMarkUpControl;
import com.trilasoft.app.model.RoleBasedComponentPermission;
import com.trilasoft.app.service.DataSecurityFilterManager;
import com.trilasoft.app.service.DataSecurityFilterService;
import com.trilasoft.app.service.ForwardingMarkUpManager;
import com.trilasoft.app.service.PricingControlManager;
import com.trilasoft.app.service.RefMasterManager;

public class ForwardingMarkUpAction extends BaseAction{
	private Long id;
	private String sessionCorpID;
	private ForwardingMarkUpControl forwardingMarkUpControl;
	private ForwardingMarkUpManager forwardingMarkUpManager;
	private PricingControlManager pricingControlManager;
	private List forwardingMarkUpList;
	private RefMasterManager refMasterManager;
	private Map<String, String> currencyList;
	private String hitFlag;
	private List contract;
	
	
	public ForwardingMarkUpAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	
	public String getComboList(String corpId){
		 currencyList=refMasterManager.findCodeOnleByParameter(corpId, "CURRENCY");
		 contract=pricingControlManager.getContract(sessionCorpID);
		return SUCCESS;
	 }
	
	
	
	@SkipValidation
	public String list(){
		forwardingMarkUpList=forwardingMarkUpManager.getAll();
		return SUCCESS;
	}
	@SkipValidation
	public String edit(){
		getComboList(sessionCorpID);
		
		if (id!= null)
		{
			forwardingMarkUpControl=forwardingMarkUpManager.get(id);
		}
		else
		{
			forwardingMarkUpControl=new ForwardingMarkUpControl();
			forwardingMarkUpControl.setCreatedBy(getRequest().getRemoteUser());
			forwardingMarkUpControl.setCreatedOn(new Date());
			forwardingMarkUpControl.setUpdatedBy(getRequest().getRemoteUser());
			forwardingMarkUpControl.setUpdatedOn(new Date());
		}
		forwardingMarkUpControl.setCorpID(sessionCorpID);
		return SUCCESS;
	}
	
	public String save() throws Exception{
		getComboList(sessionCorpID);
		boolean isNew = (forwardingMarkUpControl.getId() == null);
			if(isNew)
			{
				forwardingMarkUpControl.setCreatedBy(getRequest().getRemoteUser());
				forwardingMarkUpControl.setCreatedOn(new Date());
			}
		forwardingMarkUpControl.setUpdatedBy(getRequest().getRemoteUser());
		forwardingMarkUpControl.setUpdatedOn(new Date());
		forwardingMarkUpControl.setCorpID(sessionCorpID);	
		forwardingMarkUpControl=forwardingMarkUpManager.save(forwardingMarkUpControl);
		String key = (isNew) ? "ForwardingMarkUp has been added successfully" : "ForwardingMarkUp has been updated successfully";
		saveMessage(getText(key));
	
            return SUCCESS;
 	}
	
	public String delete(){
		try{
			forwardingMarkUpManager.remove(id);
			hitFlag="1";
			list();
			}catch(Exception ex)
			{
				list();
			}
		return SUCCESS;
	}
	public String search()
	{
				
			return SUCCESS;
	}

	public ForwardingMarkUpControl getForwardingMarkUpControl() {
		return forwardingMarkUpControl;
	}

	public void setForwardingMarkUpControl(
			ForwardingMarkUpControl forwardingMarkUpControl) {
		this.forwardingMarkUpControl = forwardingMarkUpControl;
	}

	public List getForwardingMarkUpList() {
		return forwardingMarkUpList;
	}

	public void setForwardingMarkUpList(List forwardingMarkUpList) {
		this.forwardingMarkUpList = forwardingMarkUpList;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public void setForwardingMarkUpManager(
			ForwardingMarkUpManager forwardingMarkUpManager) {
		this.forwardingMarkUpManager = forwardingMarkUpManager;
	}

	public Map<String, String> getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(Map<String, String> currencyList) {
		this.currencyList = currencyList;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public void setPricingControlManager(PricingControlManager pricingControlManager) {
		this.pricingControlManager = pricingControlManager;
	}

	public List getContract() {
		return contract;
	}

	public void setContract(List contract) {
		this.contract = contract;
	}
	

}
