<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>  
  
<head>   
    <title>Customs Details</title>   
    <meta name="heading" content="Customs Details"/> 
    <script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
	</script>
    
<script language="JavaScript" type="text/javascript" >
     function setCountry() {
      var county1=document.forms['storageCustomForm'].elements['originCountryCode'].value; 
      var county2=document.forms['storageCustomForm'].elements['destinationCountryCode'].value; 
      var move = document.forms['storageCustomForm'].elements['storage.movement'].value; 
      if(move=="In"){
		county=county2;
		} else if (move=="Out"){
		county=county1;
		}
		if(move!='') {  
	var url="findDocType.html?ajax=1&decorator=simple&popup=true&county="+encodeURI(county);
    http6.open("GET", url, true);
    http6.onreadystatechange = handleHttpResponse4444;
    http6.send(null);
    } else {
    document.forms['storageCustomForm'].elements['storage.documentType'].value='';
    }
      }
		 
 function handleHttpResponse4444()  {
		    if (http6.readyState == 4)  {
                var results = http6.responseText
                results = results.trim();
                resu = results.replace("{",'');
                resu = resu.replace("}",'');                
		    	 var res = resu.split(",");
      			targetElement = document.forms['storageCustomForm'].elements['storage.documentType'];
				targetElement.length = res.length;
            	for(i=0;i<res.length;i++) {
            	if(res[i] == ''){
					document.forms['storageCustomForm'].elements['storage.documentType'].options[i].text = '';
					document.forms['storageCustomForm'].elements['storage.documentType'].options[i].value = '';
					}else{
 				var resd = res[i].split("=");
 				document.forms['storageCustomForm'].elements['storage.documentType'].options[i].text = resd[1];
				document.forms['storageCustomForm'].elements['storage.documentType'].options[i].value = resd[0];
				}
                } 
                }
                }
                

 var http6 = getHTTPObject2();
 function getHTTPObject2(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
   }
   
   function movet(){ 
   if(document.forms['storageCustomForm'].elements['storage.movement'].value==''){
       alert("Movement is required field");
	   return false;
       } else{
   return true;
   }
 }  		
</script>   
</head> 


<style type="text/css">	
legend {
font-family:arial,verdana,sans-serif;
font-size:11px;
font-weight:bold;
margin:0;
}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>

<style><%@ include file="/common/calenderStyle.css"%></style>
<script language="javascript" type="text/javascript"><%@ include file="/common/formCalender.js"%></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>

<s:form id="storageCustomForm" action="editSaveStorageCustom" method="post" validate="true">   
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<c:set var="wtId" value="<%=request.getParameter("wtId") %>" />
<s:hidden name="wtId" value="<%=request.getParameter("wtId") %>" />
<s:hidden name="originCountryCode" value="${serviceOrder.originCountryCode}"/>
<s:hidden name="destinationCountryCode" value="${serviceOrder.destinationCountryCode}"/>
<div id="Layer1" style="width:100%">
<div style="padding-bottom:0px;"></div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
   <div id="newmnav">
		    <ul>
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			  <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		          <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose>  
		      </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		      <c:choose>
			    
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		          <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose>
		      </sec-auth:authComponent>
			  <li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			  
			  <c:if test="${serviceOrder.job !='INT'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
			  <c:if test="${serviceOrder.job =='INT'}">
			    <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  </sec-auth:authComponent>
			  <c:if test="${serviceOrder.job =='RLO'}">
			  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			  </c:if>
			  <c:if test="${serviceOrder.job !='RLO'}">
			  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			  </c:if>
			  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Ticket</span></a></li>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </configByCorp:fieldVisibility>
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			</ul>
		</div>
		<div class="spn">&nbsp;</div>
	
	<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>   
   <div id="newmnav">
		   			 <ul>
					  	<li><a href="editWorkTicketUpdate.html?id=${workTicket.id}"><span>Work Ticket</span></a></li>
					    <li><a href="bookStorageLibraries.html?id=${workTicket.id}"><span>Storage List</span></a></li>
						<li><a href="searchUnitLocates.html?locationId=&type=&occupied=&warehouse=${workTicket.warehouse}&id=${workTicket.id}"><span>Add Storage</span></a></li>
					  	<li><a href="storageUnit.html?id=${workTicket.id}"><span>Access/Release Storage</span></a></li>
					  	<li><a href="storageUnitMove.html?id=${workTicket.id}"><span>Move Storage Location</span></a></li>
					  	<li id="newmnav1" style="background:#FFF"><a class="current"><span>Customs</span></a></li>
					  	<li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
					  	
					</ul>
		</div><div class="spn">&nbsp;</div>
	
	
	<table width="100%" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
			<div id="content" align="center">
   <div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
				<table>
					<tbody>
						<tr>
							
							<td class="listwhite">Ticket Number</td>
							<td><s:textfield name="workTicket.ticket" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
							<td width="10px"></td>
							<td class="listwhite">Warehouse</td>
							<td><s:textfield name="workTicket.warehouse" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
						</tr>
					</tbody>
				</table>
				</div>
 <div class="bottom-header"><span></span></div>
  </div>
</div> 
<div id="newmnav">
		   			 <ul>
					  	<li id="newmnav1" style="background:#FFF"><a class="current"><span>Customs Form</span></a></li>
					    <li><a href="storageCustoms.html?id=${workTicket.id}"><span>Customs List</span></a></li>
					</ul>
		</div><div class="spn">&nbsp;</div>
<div id="liquid-round-top">   
<div class="top"><span></span></div>
<div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0" style="width:100%">
<tbody>
<tr>
<td>
<table class="" cellspacing="0" cellpadding="3" border="0">
<tbody>
<tr>
<td align="right" class="listwhitetext">Movement<font color="red" size="2">*</font></td>
<td align="left"><s:select cssClass="list-menu" name="storage.movement" list="%{customsMovement}"  onchange="setCountry();" cssStyle="width:75px" /></td>
<td align="right" class="listwhitetext">Entry Date</td>
<c:if test="${not empty storage.entryDate}">
<s:text id="customEntryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="storage.entryDate"/></s:text>
<td><s:textfield id="entryDate" cssClass="input-text" name="storage.entryDate" value="%{customEntryDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"/>
<img id="calender71" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 style="vertical-align:top;" onclick="cal.select(document.forms['storageCustomForm'].entryDate,'calender71',document.forms['storageCustomForm'].dateFormat.value); return false;"/></td>
</c:if>
<c:if test="${empty storage.entryDate}">
<td><s:textfield id="entryDate" cssClass="input-text" name="storage.entryDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
<img id="calender71" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 style="vertical-align:top;" onclick="cal.select(document.forms['storageCustomForm'].entryDate,'calender71',document.forms['storageCustomForm'].dateFormat.value); return false;"/></td>
</c:if>
</tr>

<tr>
<td align="right" class="listwhitetext">Document Type</td>
<td align="left"><s:select cssClass="list-menu" name="storage.documentType" list="%{customsDocType}" cssStyle="width:90px" /></td>
<td align="right" class="listwhitetext">Document Ref</td>
<td align="left"><s:textfield cssClass="input-text" name="storage.documentRef" size="40" maxlength="130" required="true" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext">Volume</td>
<td align="left"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="storage.volume" onchange="onlyFloat(this);" size="7" maxlength="18" required="true" />

<td align="right" class="listwhitetext">Pieces</td>
<td align="left"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="storage.pieces" onchange="onlyNumeric(this);" size="7" maxlength="9" required="true" /></td>
</tr>

</tbody>
</table>
</td>
</tr>	
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div> 
</div>
</div>

<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='serviceOrder.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="customCreatedOnFormattedValue" value="${storage.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="storage.createdOn" value="${customCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${storage.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='serviceOrder.createdBy' /></b></td>
							<c:if test="${not empty storage.id}">
								<s:hidden name="storage.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{storage.createdBy}"/></td>
							</c:if>
							<c:if test="${empty storage.id}">
								<s:hidden name="storage.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='serviceOrder.updatedOn'/></b></td>
							<fmt:formatDate var="customupdatedOnFormattedValue" value="${storage.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="storage.updatedOn" value="${customupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${storage.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='serviceOrder.updatedBy' /></b></td>
							<c:if test="${not empty storage.id}">
								<s:hidden name="storage.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{storage.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty storage.id}">
								<s:hidden name="storage.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
<s:hidden name="serviceOrderId" value="%{serviceOrder.id}" />
<s:hidden name="serviceOrder.id" />
<s:submit cssClass="cssbutton" method="editSaveStorageCustom" key="button.save" onclick="return movet();" cssStyle="width:70px; height:25px"/>  
<s:reset cssClass="cssbutton" key="Reset" cssStyle="width:70px; height:25px "/>
 <div id="mydiv" style="position: absolute; margin-top:-48px;" ></div>
</s:form>
   
<c:if test="${hitFlag == 1}" >
	<c:redirect url="/storageCustoms.html?id=${serviceOrder.id}"/>
</c:if>
<script type="text/javascript">   
  // Form.focusFirstElement($("storageCustomForm"));
  //onloadSetCountry();
</script>  		