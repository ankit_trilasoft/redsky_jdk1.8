package com.trilasoft.app.model;

import java.util.Date;

import org.appfuse.model.BaseObject;
import javax.persistence.Entity;
import javax.persistence.GenerationType;   
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="itemsjequip")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class ItemsJEquip extends BaseObject implements Comparable<ItemsJEquip>,ListLinkData{
	private Long id;
	private String corpID;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private int idNum;
	private int idNumNew;
	private int orderNew;
	private Double otCharge;
	private Double packLabour;
	private String descript;
	private int qty;
	private String type;
	private Double cost;
	private String bucket;
	private Double charge;
	private String contract;
	private String payCat;
	private String gl;
	private Double dtCharge;
	private String billCrew;
	private String glType;
	private String lastReset;
	private String crewType;
	private String flag;
	private Double pounds;
	private String resourceCategory;
	private Boolean controlled;
	private Boolean template;
	private Boolean printOnForm;
	private Boolean frequentlyUsedResources;
	private Boolean resourceForAccPortal;
	private String typeDescript;
	
	public int compareTo(final ItemsJEquip other) {
		return new CompareToBuilder().append(id, other.id).append(corpID,
				other.corpID).append(createdBy, other.createdBy).append(
				createdOn, other.createdOn).append(updatedBy, other.updatedBy)
				.append(updatedOn, other.updatedOn).append(idNum, other.idNum)
				.append(idNumNew, other.idNumNew).append(orderNew,
						other.orderNew).append(otCharge, other.otCharge)
				.append(packLabour, other.packLabour).append(descript,
						other.descript).append(qty, other.qty).append(type,
						other.type).append(cost, other.cost).append(bucket,
						other.bucket).append(charge, other.charge).append(
						contract, other.contract).append(payCat, other.payCat)
				.append(gl, other.gl).append(dtCharge, other.dtCharge).append(
						billCrew, other.billCrew).append(glType, other.glType)
				.append(lastReset, other.lastReset).append(crewType,
						other.crewType).append(pounds, other.pounds)
				.append(resourceCategory, other.resourceCategory).append(template, other.template)
							.append(controlled, other.controlled).append(printOnForm, other.printOnForm).append(frequentlyUsedResources, other.frequentlyUsedResources)
							.append(resourceForAccPortal, other.resourceForAccPortal)
							.append(typeDescript, other.typeDescript)
							.toComparison();
	}
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("createdBy", createdBy).append("createdOn",
				createdOn).append("updatedBy", updatedBy).append("updatedOn",
				updatedOn).append("idNum", idNum).append("idNumNew", idNumNew).append("template",template)
				.append("orderNew", orderNew).append("otCharge", otCharge)
				.append("packLabour", packLabour).append("descript", descript)
				.append("qty", qty).append("type", type).append("cost", cost)
				.append("bucket", bucket).append("charge", charge).append(
						"contract", contract).append("payCat", payCat).append(
						"gl", gl).append("dtCharge", dtCharge).append(
						"billCrew", billCrew).append("glType", glType).append(
						"lastReset", lastReset).append("crewType", crewType)
				.append("pounds", pounds).append("resourceCategory", resourceCategory)
				.append("controlled", controlled).append("printOnForm",printOnForm).append("frequentlyUsedResources",frequentlyUsedResources)
				.append("resourceForAccPortal",resourceForAccPortal)
				.append("typeDescript",typeDescript)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ItemsJEquip))
			return false;
		ItemsJEquip castOther = (ItemsJEquip) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(createdBy, castOther.createdBy).append(template,castOther.template)
				.append(createdOn, castOther.createdOn).append(updatedBy,
						castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(idNum, castOther.idNum)
				.append(idNumNew, castOther.idNumNew).append(orderNew,
						castOther.orderNew)
				.append(otCharge, castOther.otCharge).append(packLabour,
						castOther.packLabour).append(descript,
						castOther.descript).append(qty, castOther.qty).append(
						type, castOther.type).append(cost, castOther.cost)
				.append(bucket, castOther.bucket).append(charge,
						castOther.charge).append(contract, castOther.contract)
				.append(payCat, castOther.payCat).append(gl, castOther.gl)
				.append(dtCharge, castOther.dtCharge).append(billCrew,
						castOther.billCrew).append(glType, castOther.glType)
				.append(lastReset, castOther.lastReset).append(crewType,
						castOther.crewType).append(pounds, castOther.pounds)
				.append(resourceCategory, castOther.resourceCategory)
				.append(controlled, castOther.controlled).append(printOnForm,castOther.printOnForm).append(frequentlyUsedResources,castOther.frequentlyUsedResources)
				.append(resourceForAccPortal, castOther.resourceForAccPortal)
				.append(typeDescript, castOther.typeDescript)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(template)
				.append(createdBy).append(createdOn).append(updatedBy).append(
						updatedOn).append(idNum).append(idNumNew).append(
						orderNew).append(otCharge).append(packLabour).append(
						descript).append(qty).append(type).append(cost).append(
						bucket).append(charge).append(contract).append(payCat)
				.append(gl).append(dtCharge).append(billCrew).append(glType)
				.append(lastReset).append(crewType).append(pounds).append(resourceCategory)
				.append(controlled).append(printOnForm).append(frequentlyUsedResources)
				.append(resourceForAccPortal)
				.append(typeDescript)
				.toHashCode();
	}
	
	@Column(length=2)
	public String getBillCrew() {
		return billCrew;
	}
	public void setBillCrew(String billCrew) {
		this.billCrew = billCrew;
	}
	@Column(length=10)
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	@Column
	public Double getCharge() {
		return charge;
	}
	public void setCharge(Double charge) {
		this.charge = charge;
	}
	@Column(length=100)
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	@Column(length=2)
	public String getPayCat() {
		return payCat;
	}
	public void setPayCat(String payCat) {
		this.payCat = payCat;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=15)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	@Column(length=2)
	public String getCrewType() {
		return crewType;
	}
	public void setCrewType(String crewType) {
		this.crewType = crewType;
	}
	@Column(length=50)
	public String getDescript() {
		return descript;
	}
	public void setDescript(String descript) {
		this.descript = descript;
	}
	@Column
	public Double getDtCharge() {
		return dtCharge;
	}
	public void setDtCharge(Double dtCharge) {
		this.dtCharge = dtCharge;
	}
	@Column(length=20)
	public String getGl() {
		return gl;
	}
	public void setGl(String gl) {
		this.gl = gl;
	}
	@Column(length=7)
	public String getGlType() {
		return glType;
	}
	public void setGlType(String glType) {
		this.glType = glType;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public int getIdNum() {
		return idNum;
	}
	public void setIdNum(int idNum) {
		this.idNum = idNum;
	}
	@Column
	public int getIdNumNew() {
		return idNumNew;
	}
	public void setIdNumNew(int idNumNew) {
		this.idNumNew = idNumNew;
	}
	@Column
	public int getOrderNew() {
		return orderNew;
	}
	public void setOrderNew(int orderNew) {
		this.orderNew = orderNew;
	}
	@Column
	public Double getPackLabour() {
		return packLabour;
	}
	public void setPackLabour(Double packLabour) {
		this.packLabour = packLabour;
	}
	@Column
	public Double getOtCharge() {
		return otCharge;
	}
	public void setOtCharge(Double otCharge) {
		this.otCharge = otCharge;
	}
	@Column(length=26)
	public String getLastReset() {
		return lastReset;
	}
	public void setLastReset(String lastReset) {
		this.lastReset = lastReset;
	}	
	@Column
	public Double getPounds() {
		return pounds;
	}
	public void setPounds(Double pounds) {
		this.pounds = pounds;
	}
	@Column
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	@Column(length=1)
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Column(length=2)
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	@Transient
	public String getListCode() {
		return billCrew;
	}
	@Transient
	public String getListDescription() {
		String fixError = "";
		return fixError;
	}
	@Transient
	public String getListSecondDescription() {
		String fixError = "";
		return fixError;
	}
	@Transient
	public String getListThirdDescription() {
		String fixError = "";
		return fixError;
	}
	@Transient
	public String getListFourthDescription() {
		String fixError = "";
		return fixError;
	}
	@Transient
	public String getListFifthDescription() {
		String fixError = "";
		return fixError;
	}
	@Transient
	public String getListSixthDescription() {
		String fixError = "";
		return fixError;
	}
	@Transient
	public String getListSeventhDescription() {
		String fixError = "";
		return fixError;
	}
	@Transient
	public String getListEigthDescription() {
		String fixError = "";
		return fixError;
	}
	@Transient
	public String getListNinthDescription() {
		String fixError = "";
		return fixError;
	}
	@Transient
	public String getListTenthDescription() {
		String fixError = "";
		return fixError;
	}
	

	public String getResourceCategory() {
		return resourceCategory;
	}
	public void setResourceCategory(String resourceCategory) {
		this.resourceCategory = resourceCategory;
	}
	public Boolean getControlled() {
		return controlled;
	}
	public void setControlled(Boolean controlled) {
		this.controlled = controlled;
	}
	public Boolean getTemplate() {
		return template;
	}
	public void setTemplate(Boolean template) {
		this.template = template;
	}
	@Column
	public Boolean getPrintOnForm() {
		return printOnForm;
	}
	public void setPrintOnForm(Boolean printOnForm) {
		this.printOnForm = printOnForm;
	}
	@Column
	public Boolean getFrequentlyUsedResources() {
		return frequentlyUsedResources;
	}
	public void setFrequentlyUsedResources(Boolean frequentlyUsedResources) {
		this.frequentlyUsedResources = frequentlyUsedResources;
	}
	@Column
	public Boolean getResourceForAccPortal() {
		return resourceForAccPortal;
	}
	public void setResourceForAccPortal(Boolean resourceForAccPortal) {
		this.resourceForAccPortal = resourceForAccPortal;
	}
	/**
	 * @return the typeDescript
	 */
	@Column
	public String getTypeDescript() {
		return typeDescript;
	}
	/**
	 * @param typeDescript the typeDescript to set
	 */
	public void setTypeDescript(String typeDescript) {
		this.typeDescript = typeDescript;
	}
		
}
	