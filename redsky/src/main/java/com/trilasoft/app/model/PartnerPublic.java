package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "partnerpublic")
@FilterDef(name = "partnerCorpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "partnerCorpID", condition = "corpID in (:forCorpID)")

public class PartnerPublic extends BaseObject implements ListLinkData{

	private Long id;

	private String corpID;

	private String partnerCode;

	private String partnerPrefix;

	private String firstName;

	private String lastName;

	private String middleInitial;

	private String partnerSuffix;

	private String terminalAddress1;

	private String terminalAddress2;

	private String terminalAddress3;

	private String terminalAddress4;

	private String mailingCountry;

	private String mailingTelex;

	private String mailingPhone;

	private String mailingFax;

	private String mailingEmail;

	private String mailingCountryCode;

	private String mailingAddress1;

	private String mailingAddress2;

	private String mailingAddress3;

	private String mailingAddress4;

	private String mailingCity;

	private String mailingState;

	private String mailingZip;

	private String terminalPhone;

	private String terminalTelex;

	private String terminalFax;

	private String terminalCountryCode;

	private String terminalCountry;

	private String terminalCity;

	private String terminalState;

	private String terminalZip;

	private String terminalEmail;

	private String billingCountryCode;

	private String billingAddress1;

	private String billingAddress2;

	private String billingAddress3;

	private String billingAddress4;

	private String billingCity;

	private String billingCountry;

	private String billingEmail;

	private String billingFax;

	private String billingPhone;

	private String billingState;

	private String billingTelex;

	private String billingZip;

	private Boolean isAccount;

	private Boolean isAgent;

	private Boolean isCarrier;

	private Boolean isVendor;

	private String updatedBy;

	private String createdBy;

	private Date createdOn;

	private Date updatedOn;

	private Date effectiveDate;

	private String status;

	private String parent;

	private Boolean sea;

	private Boolean surface;

	private Boolean air;

	private Boolean isOwnerOp;

	private Boolean isPrivateParty;

	private String typeOfVendor;

	private String agentParent;

	private String location1;

	private String location2;

	private String location3;

	private String location4;

	private String url;

	private String companyProfile;

	private String trackingUrl;

	private BigDecimal latitude;

	private BigDecimal longitude;

	private String yearEstablished;

	private String companyFacilities;

	private String companyCapabilities;

	private String companyDestiantionProfile;

	private String serviceRangeKms;

	private String serviceRangeMiles;

	private String fidiNumber;
	
	private String utsNumber;

	private String OMNINumber;

	private String IAMNumber;

	private String AMSANumber;
	
	private String movingCompanyType;
	
	private String WERCNumber;

	private String facilitySizeSQFT;

	private String facilitySizeSQMT;

	private String qualityCertifications;

	private String vanLineAffiliation;

	private String serviceLines;
	
	private String mergedCode;
	
	private String agentParentName;
	
	private Date startDate;
	
	private Boolean doNotMerge;
    private String reloContact;
    private String reloContactEmail;
    private String reloServices;
    private String contactName;
	private double vendorCommission;
	private Integer minimumDFB;
	private Boolean isNetworkPartner;
	private String  billingCurrency;
	private String bankCode;
	private String bankAccountNumber;
	private String vatNumber;

	// New fields are added fro driver locations
	
    private String vanLastLocation;
	
	private Date vanLastReportOn;
	
	private String vanLastReportTime;
	
	private Integer vanAvailCube;
	
    private String nextVanLocation;
    private String currentVanAgency;
    private String currentVanID;
    private String currentTractorAgency;
    private String currentTractorID;
	private Date nextReportOn;
	private Boolean networkGroup;
	private Boolean ugwwNetworkGroup  = new Boolean(false);
	private String aliasName;
	public String partnerType;
	
	private Boolean partnerPortalActive;
	private String associatedAgents;
	private Boolean viewChild;

	private String UTSmovingCompanyType;
	private String transPort;
	
	private String mergeInto;
	
	private String agentGroup;
	private Boolean isPartnerExtract=new Boolean(false);
	private Boolean sentToAccounting=new Boolean(false);
	private String ugwwCarrierCode;
	private Boolean doNotSendEmailtoAgentUser  = new Boolean(false);
	private String PAIMA ;
	private String LACMA ;
	private String eurovanNetwork;
	private String companyLogo;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("partnerCode", partnerCode)
				.append("partnerPrefix", partnerPrefix)
				.append("firstName", firstName).append("lastName", lastName)
				.append("middleInitial", middleInitial)
				.append("partnerSuffix", partnerSuffix)
				.append("terminalAddress1", terminalAddress1)
				.append("terminalAddress2", terminalAddress2)
				.append("terminalAddress3", terminalAddress3)
				.append("terminalAddress4", terminalAddress4)
				.append("mailingCountry", mailingCountry)
				.append("mailingTelex", mailingTelex)
				.append("mailingPhone", mailingPhone)
				.append("mailingFax", mailingFax)
				.append("mailingEmail", mailingEmail)
				.append("mailingCountryCode", mailingCountryCode)
				.append("mailingAddress1", mailingAddress1)
				.append("mailingAddress2", mailingAddress2)
				.append("mailingAddress3", mailingAddress3)
				.append("mailingAddress4", mailingAddress4)
				.append("mailingCity", mailingCity)
				.append("mailingState", mailingState)
				.append("mailingZip", mailingZip)
				.append("terminalPhone", terminalPhone)
				.append("terminalTelex", terminalTelex)
				.append("terminalFax", terminalFax)
				.append("terminalCountryCode", terminalCountryCode)
				.append("terminalCountry", terminalCountry)
				.append("terminalCity", terminalCity)
				.append("terminalState", terminalState)
				.append("terminalZip", terminalZip)
				.append("terminalEmail", terminalEmail)
				.append("billingCountryCode", billingCountryCode)
				.append("billingAddress1", billingAddress1)
				.append("billingAddress2", billingAddress2)
				.append("billingAddress3", billingAddress3)
				.append("billingAddress4", billingAddress4)
				.append("billingCity", billingCity)
				.append("billingCountry", billingCountry)
				.append("billingEmail", billingEmail)
				.append("billingFax", billingFax)
				.append("billingPhone", billingPhone)
				.append("billingState", billingState)
				.append("billingTelex", billingTelex)
				.append("billingZip", billingZip)
				.append("isAccount", isAccount).append("isAgent", isAgent)
				.append("isCarrier", isCarrier).append("isVendor", isVendor)
				.append("updatedBy", updatedBy).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("effectiveDate", effectiveDate)
				.append("status", status).append("parent", parent)
				.append("sea", sea).append("surface", surface)
				.append("air", air).append("isOwnerOp", isOwnerOp)
				.append("isPrivateParty", isPrivateParty)
				.append("typeOfVendor", typeOfVendor)
				.append("agentParent", agentParent)
				.append("location1", location1).append("location2", location2)
				.append("location3", location3).append("location4", location4)
				.append("url", url).append("companyProfile", companyProfile)
				.append("trackingUrl", trackingUrl)
				.append("latitude", latitude).append("longitude", longitude)
				.append("yearEstablished", yearEstablished)
				.append("companyFacilities", companyFacilities)
				.append("companyCapabilities", companyCapabilities)
				.append("companyDestiantionProfile", companyDestiantionProfile)
				.append("serviceRangeKms", serviceRangeKms)
				.append("serviceRangeMiles", serviceRangeMiles)
				.append("fidiNumber", fidiNumber)
				.append("utsNumber", utsNumber)
				.append("OMNINumber", OMNINumber)
				.append("IAMNumber", IAMNumber)
				.append("AMSANumber", AMSANumber)
				.append("movingCompanyType", movingCompanyType)
				.append("WERCNumber", WERCNumber)
				.append("facilitySizeSQFT", facilitySizeSQFT)
				.append("facilitySizeSQMT", facilitySizeSQMT)
				.append("qualityCertifications", qualityCertifications)
				.append("vanLineAffiliation", vanLineAffiliation)
				.append("serviceLines", serviceLines)
				.append("mergedCode", mergedCode)
				.append("agentParentName", agentParentName)
				.append("startDate", startDate)
				.append("doNotMerge", doNotMerge)
				.append("reloContact", reloContact)
				.append("reloContactEmail", reloContactEmail)
				.append("reloServices", reloServices)
				.append("contactName", contactName)
				.append("vendorCommission", vendorCommission)
				.append("minimumDFB", minimumDFB)
				.append("isNetworkPartner", isNetworkPartner)
				.append("billingCurrency", billingCurrency)
				.append("bankCode", bankCode)
				.append("bankAccountNumber", bankAccountNumber)
				.append("vatNumber", vatNumber)
				.append("vanLastLocation", vanLastLocation)
				.append("vanLastReportOn", vanLastReportOn)
				.append("vanLastReportTime", vanLastReportTime)
				.append("vanAvailCube", vanAvailCube)
				.append("nextVanLocation", nextVanLocation)
				.append("currentVanAgency", currentVanAgency)
				.append("currentVanID", currentVanID)
				.append("currentTractorAgency", currentTractorAgency)
				.append("currentTractorID", currentTractorID)
				.append("nextReportOn", nextReportOn)
				.append("networkGroup", networkGroup)
				.append("ugwwNetworkGroup", ugwwNetworkGroup)
				.append("aliasName", aliasName)
				.append("partnerType", partnerType)
				.append("partnerPortalActive", partnerPortalActive)
				.append("associatedAgents", associatedAgents)
				.append("viewChild", viewChild)
				.append("UTSmovingCompanyType", UTSmovingCompanyType)
				.append("transPort", transPort)
				.append("agentGroup", agentGroup)
				.append("isPartnerExtract", isPartnerExtract)
				.append("sentToAccounting", sentToAccounting)
				.append("mergeInto", mergeInto)
				.append("ugwwCarrierCode", ugwwCarrierCode)
				.append("doNotSendEmailtoAgentUser",doNotSendEmailtoAgentUser)
				.append("PAIMA",PAIMA)
				.append("LACMA",LACMA)
				.append("eurovanNetwork",eurovanNetwork).append("companyLogo",companyLogo).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PartnerPublic))
			return false;
		PartnerPublic castOther = (PartnerPublic) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(partnerCode, castOther.partnerCode)
				.append(partnerPrefix, castOther.partnerPrefix)
				.append(firstName, castOther.firstName)
				.append(lastName, castOther.lastName)
				.append(middleInitial, castOther.middleInitial)
				.append(partnerSuffix, castOther.partnerSuffix)
				.append(terminalAddress1, castOther.terminalAddress1)
				.append(terminalAddress2, castOther.terminalAddress2)
				.append(terminalAddress3, castOther.terminalAddress3)
				.append(terminalAddress4, castOther.terminalAddress4)
				.append(mailingCountry, castOther.mailingCountry)
				.append(mailingTelex, castOther.mailingTelex)
				.append(mailingPhone, castOther.mailingPhone)
				.append(mailingFax, castOther.mailingFax)
				.append(mailingEmail, castOther.mailingEmail)
				.append(mailingCountryCode, castOther.mailingCountryCode)
				.append(mailingAddress1, castOther.mailingAddress1)
				.append(mailingAddress2, castOther.mailingAddress2)
				.append(mailingAddress3, castOther.mailingAddress3)
				.append(mailingAddress4, castOther.mailingAddress4)
				.append(mailingCity, castOther.mailingCity)
				.append(mailingState, castOther.mailingState)
				.append(mailingZip, castOther.mailingZip)
				.append(terminalPhone, castOther.terminalPhone)
				.append(terminalTelex, castOther.terminalTelex)
				.append(terminalFax, castOther.terminalFax)
				.append(terminalCountryCode, castOther.terminalCountryCode)
				.append(terminalCountry, castOther.terminalCountry)
				.append(terminalCity, castOther.terminalCity)
				.append(terminalState, castOther.terminalState)
				.append(terminalZip, castOther.terminalZip)
				.append(terminalEmail, castOther.terminalEmail)
				.append(billingCountryCode, castOther.billingCountryCode)
				.append(billingAddress1, castOther.billingAddress1)
				.append(billingAddress2, castOther.billingAddress2)
				.append(billingAddress3, castOther.billingAddress3)
				.append(billingAddress4, castOther.billingAddress4)
				.append(billingCity, castOther.billingCity)
				.append(billingCountry, castOther.billingCountry)
				.append(billingEmail, castOther.billingEmail)
				.append(billingFax, castOther.billingFax)
				.append(billingPhone, castOther.billingPhone)
				.append(billingState, castOther.billingState)
				.append(billingTelex, castOther.billingTelex)
				.append(billingZip, castOther.billingZip)
				.append(isAccount, castOther.isAccount)
				.append(isAgent, castOther.isAgent)
				.append(isCarrier, castOther.isCarrier)
				.append(isVendor, castOther.isVendor)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(effectiveDate, castOther.effectiveDate)
				.append(status, castOther.status)
				.append(parent, castOther.parent)
				.append(sea, castOther.sea)
				.append(surface, castOther.surface)
				.append(air, castOther.air)
				.append(isOwnerOp, castOther.isOwnerOp)
				.append(isPrivateParty, castOther.isPrivateParty)
				.append(typeOfVendor, castOther.typeOfVendor)
				.append(agentParent, castOther.agentParent)
				.append(location1, castOther.location1)
				.append(location2, castOther.location2)
				.append(location3, castOther.location3)
				.append(location4, castOther.location4)
				.append(url, castOther.url)
				.append(companyProfile, castOther.companyProfile)
				.append(trackingUrl, castOther.trackingUrl)
				.append(latitude, castOther.latitude)
				.append(longitude, castOther.longitude)
				.append(yearEstablished, castOther.yearEstablished)
				.append(companyFacilities, castOther.companyFacilities)
				.append(companyCapabilities, castOther.companyCapabilities)
				.append(companyDestiantionProfile,
						castOther.companyDestiantionProfile)
				.append(serviceRangeKms, castOther.serviceRangeKms)
				.append(serviceRangeMiles, castOther.serviceRangeMiles)
				.append(fidiNumber, castOther.fidiNumber)
				.append(utsNumber, castOther.utsNumber)
				.append(OMNINumber, castOther.OMNINumber)
				.append(IAMNumber, castOther.IAMNumber)
				.append(AMSANumber, castOther.AMSANumber)
				.append(movingCompanyType, castOther.movingCompanyType)
				.append(WERCNumber, castOther.WERCNumber)
				.append(facilitySizeSQFT, castOther.facilitySizeSQFT)
				.append(facilitySizeSQMT, castOther.facilitySizeSQMT)
				.append(qualityCertifications, castOther.qualityCertifications)
				.append(vanLineAffiliation, castOther.vanLineAffiliation)
				.append(serviceLines, castOther.serviceLines)
				.append(mergedCode, castOther.mergedCode)
				.append(agentParentName, castOther.agentParentName)
				.append(startDate, castOther.startDate)
				.append(doNotMerge, castOther.doNotMerge)
				.append(reloContact, castOther.reloContact)
				.append(reloContactEmail, castOther.reloContactEmail)
				.append(reloServices, castOther.reloServices)
				.append(contactName, castOther.contactName)
				.append(vendorCommission, castOther.vendorCommission)
				.append(minimumDFB, castOther.minimumDFB)
				.append(isNetworkPartner, castOther.isNetworkPartner)
				.append(billingCurrency, castOther.billingCurrency)
				.append(bankCode, castOther.bankCode)
				.append(bankAccountNumber, castOther.bankAccountNumber)
				.append(vatNumber, castOther.vatNumber)
				.append(vanLastLocation, castOther.vanLastLocation)
				.append(vanLastReportOn, castOther.vanLastReportOn)
				.append(vanLastReportTime, castOther.vanLastReportTime)
				.append(vanAvailCube, castOther.vanAvailCube)
				.append(nextVanLocation, castOther.nextVanLocation)
				.append(currentVanAgency, castOther.currentVanAgency)
				.append(currentVanID, castOther.currentVanID)
				.append(currentTractorAgency, castOther.currentTractorAgency)
				.append(currentTractorID, castOther.currentTractorID)
				.append(nextReportOn, castOther.nextReportOn)
				.append(networkGroup, castOther.networkGroup)
				.append(ugwwNetworkGroup, castOther.ugwwNetworkGroup)
				.append(aliasName, castOther.aliasName)
				.append(partnerType, castOther.partnerType)
				.append(partnerPortalActive, castOther.partnerPortalActive)
				.append(associatedAgents, castOther.associatedAgents)
				.append(viewChild, castOther.viewChild)
				.append(UTSmovingCompanyType, castOther.UTSmovingCompanyType)
				.append(transPort, castOther.transPort)
				.append(agentGroup, castOther.agentGroup)
				.append(isPartnerExtract, castOther.isPartnerExtract)
				.append(sentToAccounting, castOther.sentToAccounting)
				.append(mergeInto, castOther.mergeInto)
				.append(ugwwCarrierCode, castOther.ugwwCarrierCode)
				.append(doNotSendEmailtoAgentUser, castOther.doNotSendEmailtoAgentUser)
				.append(PAIMA, castOther.PAIMA)
				.append(LACMA, castOther.LACMA).append(eurovanNetwork, castOther.eurovanNetwork).append(companyLogo, castOther.companyLogo).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(partnerCode).append(partnerPrefix).append(firstName)
				.append(lastName).append(middleInitial).append(partnerSuffix)
				.append(terminalAddress1).append(terminalAddress2)
				.append(terminalAddress3).append(terminalAddress4)
				.append(mailingCountry).append(mailingTelex)
				.append(mailingPhone).append(mailingFax).append(mailingEmail)
				.append(mailingCountryCode).append(mailingAddress1)
				.append(mailingAddress2).append(mailingAddress3)
				.append(mailingAddress4).append(mailingCity)
				.append(mailingState).append(mailingZip).append(terminalPhone)
				.append(terminalTelex).append(terminalFax)
				.append(terminalCountryCode).append(terminalCountry)
				.append(terminalCity).append(terminalState).append(terminalZip)
				.append(terminalEmail).append(billingCountryCode)
				.append(billingAddress1).append(billingAddress2)
				.append(billingAddress3).append(billingAddress4)
				.append(billingCity).append(billingCountry)
				.append(billingEmail).append(billingFax).append(billingPhone)
				.append(billingState).append(billingTelex).append(billingZip)
				.append(isAccount).append(isAgent).append(isCarrier)
				.append(isVendor).append(updatedBy).append(createdBy)
				.append(createdOn).append(updatedOn).append(effectiveDate)
				.append(status).append(parent).append(sea).append(surface)
				.append(air).append(isOwnerOp).append(isPrivateParty)
				.append(typeOfVendor).append(agentParent).append(location1)
				.append(location2).append(location3).append(location4)
				.append(url).append(companyProfile).append(trackingUrl)
				.append(latitude).append(longitude).append(yearEstablished)
				.append(companyFacilities).append(companyCapabilities)
				.append(companyDestiantionProfile).append(serviceRangeKms)
				.append(serviceRangeMiles).append(fidiNumber).append(utsNumber)
				.append(OMNINumber).append(IAMNumber).append(AMSANumber)
				.append(movingCompanyType).append(WERCNumber)
				.append(facilitySizeSQFT).append(facilitySizeSQMT)
				.append(qualityCertifications).append(vanLineAffiliation)
				.append(serviceLines).append(mergedCode)
				.append(agentParentName).append(startDate).append(doNotMerge)
				.append(reloContact).append(reloContactEmail)
				.append(reloServices).append(contactName)
				.append(vendorCommission).append(minimumDFB)
				.append(isNetworkPartner).append(billingCurrency)
				.append(bankCode).append(bankAccountNumber).append(vatNumber)
				.append(vanLastLocation).append(vanLastReportOn)
				.append(vanLastReportTime).append(vanAvailCube)
				.append(nextVanLocation).append(currentVanAgency)
				.append(currentVanID).append(currentTractorAgency)
				.append(currentTractorID).append(nextReportOn)
				.append(networkGroup).append(ugwwNetworkGroup)
				.append(aliasName).append(partnerType)
				.append(partnerPortalActive).append(associatedAgents)
				.append(viewChild).append(UTSmovingCompanyType)
				.append(transPort).append(agentGroup).append(isPartnerExtract)
				.append(sentToAccounting)
				.append(mergeInto)
				.append(ugwwCarrierCode).append(doNotSendEmailtoAgentUser)
				.append(LACMA).append(PAIMA).append(eurovanNetwork).append(companyLogo).toHashCode();
		
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column
	public String getBillingAddress1() {
		return billingAddress1;
	}

	public void setBillingAddress1(String billingAddress1) {
		this.billingAddress1 = billingAddress1;
	}

	@Column
	public String getBillingAddress2() {
		return billingAddress2;
	}

	public void setBillingAddress2(String billingAddress2) {
		this.billingAddress2 = billingAddress2;
	}

	@Column
	public String getBillingAddress3() {
		return billingAddress3;
	}

	public void setBillingAddress3(String billingAddress3) {
		this.billingAddress3 = billingAddress3;
	}

	@Column
	public String getBillingAddress4() {
		return billingAddress4;
	}

	public void setBillingAddress4(String billingAddress4) {
		this.billingAddress4 = billingAddress4;
	}

	@Column(length = 20)
	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	@Column(length = 45)
	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	@Column(length = 3)
	public String getBillingCountryCode() {
		return billingCountryCode;
	}

	public void setBillingCountryCode(String billingCountryCode) {
		this.billingCountryCode = billingCountryCode;
	}

	@Column(length = 70)
	public String getBillingEmail() {
		return billingEmail;
	}

	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}

	@Column(length = 20)
	public String getBillingFax() {
		return billingFax;
	}

	public void setBillingFax(String billingFax) {
		this.billingFax = billingFax;
	}

	@Column(length = 100)
	public String getBillingPhone() {
		return billingPhone;
	}

	public void setBillingPhone(String billingPhone) {
		this.billingPhone = billingPhone;
	}

	@Column(length = 2)
	public String getBillingState() {
		return billingState;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	@Column(length = 20)
	public String getBillingTelex() {
		return billingTelex;
	}

	public void setBillingTelex(String billingTelex) {
		this.billingTelex = billingTelex;
	}

	@Column(length = 50)
	public String getBillingZip() {
		return billingZip;
	}

	public void setBillingZip(String billingZip) {
		this.billingZip = billingZip;
	}

	@Column
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	@Column(length = 25)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column
	public Boolean getIsAccount() {
		return isAccount;
	}

	public void setIsAccount(Boolean isAccount) {
		this.isAccount = isAccount;
	}

	@Column
	public Boolean getIsAgent() {
		return isAgent;
	}

	public void setIsAgent(Boolean isAgent) {
		this.isAgent = isAgent;
	}

	@Column
	public Boolean getIsCarrier() {
		return isCarrier;
	}

	public void setIsCarrier(Boolean isCarrier) {
		this.isCarrier = isCarrier;
	}

	@Column
	public Boolean getIsVendor() {
		return isVendor;
	}

	public void setIsVendor(Boolean isVendor) {
		this.isVendor = isVendor;
	}

	@Column
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column
	public String getMailingAddress1() {
		return mailingAddress1;
	}

	public void setMailingAddress1(String mailingAddress1) {
		this.mailingAddress1 = mailingAddress1;
	}

	@Column
	public String getMailingAddress2() {
		return mailingAddress2;
	}

	public void setMailingAddress2(String mailingAddress2) {
		this.mailingAddress2 = mailingAddress2;
	}

	@Column
	public String getMailingAddress3() {
		return mailingAddress3;
	}

	public void setMailingAddress3(String mailingAddress3) {
		this.mailingAddress3 = mailingAddress3;
	}

	@Column
	public String getMailingAddress4() {
		return mailingAddress4;
	}

	public void setMailingAddress4(String mailingAddress4) {
		this.mailingAddress4 = mailingAddress4;
	}

	@Column(length = 50)
	public String getMailingCity() {
		return mailingCity;
	}

	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}

	@Column(length = 45)
	public String getMailingCountry() {
		return mailingCountry;
	}

	public void setMailingCountry(String mailingCountry) {
		this.mailingCountry = mailingCountry;
	}

	@Column(length = 3)
	public String getMailingCountryCode() {
		return mailingCountryCode;
	}

	public void setMailingCountryCode(String mailingCountryCode) {
		this.mailingCountryCode = mailingCountryCode;
	}

	@Column(length = 65)
	public String getMailingEmail() {
		return mailingEmail;
	}

	public void setMailingEmail(String mailingEmail) {
		this.mailingEmail = mailingEmail;
	}

	@Column(length = 20)
	public String getMailingFax() {
		return mailingFax;
	}

	public void setMailingFax(String mailingFax) {
		this.mailingFax = mailingFax;
	}

	@Column(length = 100)
	public String getMailingPhone() {
		return mailingPhone;
	}

	public void setMailingPhone(String mailingPhone) {
		this.mailingPhone = mailingPhone;
	}

	@Column(length = 2)
	public String getMailingState() {
		return mailingState;
	}

	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}

	@Column(length = 20)
	public String getMailingTelex() {
		return mailingTelex;
	}

	public void setMailingTelex(String mailingTelex) {
		this.mailingTelex = mailingTelex;
	}

	@Column(length = 50)
	public String getMailingZip() {
		return mailingZip;
	}

	public void setMailingZip(String mailingZip) {
		this.mailingZip = mailingZip;
	}

	@Column(length = 1)
	public String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(length = 7)
	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	@Column(length = 8)
	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	@Column(length = 10)
	public String getPartnerPrefix() {
		return partnerPrefix;
	}

	public void setPartnerPrefix(String partnerPrefix) {
		this.partnerPrefix = partnerPrefix;
	}

	@Column(length = 10)
	public String getPartnerSuffix() {
		return partnerSuffix;
	}

	public void setPartnerSuffix(String partnerSuffix) {
		this.partnerSuffix = partnerSuffix;
	}

	@Column
	public String getTerminalAddress1() {
		return terminalAddress1;
	}

	public void setTerminalAddress1(String terminalAddress1) {
		this.terminalAddress1 = terminalAddress1;
	}

	@Column
	public String getTerminalAddress2() {
		return terminalAddress2;
	}

	public void setTerminalAddress2(String terminalAddress2) {
		this.terminalAddress2 = terminalAddress2;
	}

	@Column
	public String getTerminalAddress3() {
		return terminalAddress3;
	}

	public void setTerminalAddress3(String terminalAddress3) {
		this.terminalAddress3 = terminalAddress3;
	}

	@Column
	public String getTerminalAddress4() {
		return terminalAddress4;
	}

	public void setTerminalAddress4(String terminalAddress4) {
		this.terminalAddress4 = terminalAddress4;
	}

	@Column(length = 50)
	public String getTerminalCity() {
		return terminalCity;
	}

	public void setTerminalCity(String terminalCity) {
		this.terminalCity = terminalCity;
	}

	@Column(length = 45)
	public String getTerminalCountry() {
		return terminalCountry;
	}

	public void setTerminalCountry(String terminalCountry) {
		this.terminalCountry = terminalCountry;
	}

	@Column(length = 3)
	public String getTerminalCountryCode() {
		return terminalCountryCode;
	}

	public void setTerminalCountryCode(String terminalCountryCode) {
		this.terminalCountryCode = terminalCountryCode;
	}

	@Column(length = 70)
	public String getTerminalEmail() {
		return terminalEmail;
	}

	public void setTerminalEmail(String terminalEmail) {
		this.terminalEmail = terminalEmail;
	}

	@Column(length = 20)
	public String getTerminalFax() {
		return terminalFax;
	}

	public void setTerminalFax(String terminalFax) {
		this.terminalFax = terminalFax;
	}

	@Column(length = 100)
	public String getTerminalPhone() {
		return terminalPhone;
	}

	public void setTerminalPhone(String terminalPhone) {
		this.terminalPhone = terminalPhone;
	}

	@Column(length = 2)
	public String getTerminalState() {
		return terminalState;
	}

	public void setTerminalState(String terminalState) {
		this.terminalState = terminalState;
	}

	@Column(length = 20)
	public String getTerminalTelex() {
		return terminalTelex;
	}

	public void setTerminalTelex(String terminalTelex) {
		this.terminalTelex = terminalTelex;
	}

	@Column(length = 50)
	public String getTerminalZip() {
		return terminalZip;
	}

	public void setTerminalZip(String terminalZip) {
		this.terminalZip = terminalZip;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public Boolean getIsPrivateParty() {
		return isPrivateParty;
	}

	public void setIsPrivateParty(Boolean isPrivateParty) {
		this.isPrivateParty = isPrivateParty;
	}

	@Column
	public Boolean getAir() {
		return air;
	}

	public void setAir(Boolean air) {
		this.air = air;
	}

	@Column
	public Boolean getSea() {
		return sea;
	}

	public void setSea(Boolean sea) {
		this.sea = sea;
	}

	@Column
	public Boolean getSurface() {
		return surface;
	}

	public void setSurface(Boolean surface) {
		this.surface = surface;
	}

	@Column
	public Boolean getIsOwnerOp() {
		return isOwnerOp;
	}

	public void setIsOwnerOp(Boolean isOwnerOp) {
		this.isOwnerOp = isOwnerOp;
	}

	@Transient
	public String getListCode() {
		return partnerCode;
	}

	@Transient
	public String getListDescription() {		
		return this.lastName;
	}

	@Transient
	public String getListSecondDescription() {
		String billingStateForPopup;
		String billingCountryCodeForPopup;
		if (this.billingCity == null) {
			this.billingCity = "";
		}
		if (this.billingState == null) {
			billingStateForPopup = "";
		} else {
			billingStateForPopup = "," + this.billingState;
		}
		if (this.billingCountryCode == null) {
			billingCountryCodeForPopup = "";
		} else {
			billingCountryCodeForPopup = "," + this.billingCountryCode;
		}
		String s = this.billingCity + billingStateForPopup
				+ billingCountryCodeForPopup;
		s = s.trim();
		if (s.indexOf(",") == (0 & s.lastIndexOf(","))) {
			s = s.replaceAll(",", "");
		}
		s = s.replaceAll(",,", ",");
		s = s.replaceAll(",,,", ",");
		return s;
	}

	@Transient
	public String getListThirdDescription() {
		String termEmail = this.reloContactEmail;
		if (termEmail == null) {
			termEmail = "";
		}
		return termEmail;
	}
	
	@Transient
	public String getListFourthDescription() {
		String mailingPhone = this.mailingPhone;
		 if (mailingPhone == null) {
			 mailingPhone = "";
		   }		
		 mailingPhone = mailingPhone.replaceAll("#", "");
		return mailingPhone; 
	}

	@Transient
	public String getListFifthDescription() {
		String termEmail = this.terminalEmail;
		if (termEmail == null) {
			termEmail = "";
		}
		return termEmail;
	}

	@Transient
	public String getListSixthDescription() {
		String lName = this.reloContact;
		if(lName== null){
			lName="";
		}
		lName=lName.replaceAll("&", "%26"); 
		return lName;

	}

	@Column(length = 50)
	public String getTypeOfVendor() {
		return typeOfVendor;
	}

	public void setTypeOfVendor(String typeOfVendor) {
		this.typeOfVendor = typeOfVendor;
	}

	@Column(length = 20)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(length = 8)
	public String getAgentParent() {
		return agentParent;
	}

	public void setAgentParent(String agentParent) {
		this.agentParent = agentParent;
	}

	public String getLocation1() {
		return location1;
	}

	public void setLocation1(String location1) {
		this.location1 = location1;
	}

	public String getLocation2() {
		return location2;
	}

	public void setLocation2(String location2) {
		this.location2 = location2;
	}

	public String getLocation3() {
		return location3;
	}

	public void setLocation3(String location3) {
		this.location3 = location3;
	}

	public String getLocation4() {
		return location4;
	}

	public void setLocation4(String location4) {
		this.location4 = location4;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(length = 1500)
	public String getCompanyProfile() {
		return companyProfile;
	}

	public void setCompanyProfile(String companyProfile) {
		this.companyProfile = companyProfile;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	@Column
	public String getAMSANumber() {
		return AMSANumber;
	}

	public void setAMSANumber(String number) {
		AMSANumber = number;
	}

	@Column
	public String getFidiNumber() {
		return fidiNumber;
	}

	public void setFidiNumber(String fidiNumber) {
		this.fidiNumber = fidiNumber;
	}

	@Column
	public String getIAMNumber() {
		return IAMNumber;
	}

	public void setIAMNumber(String number) {
		IAMNumber = number;
	}

	@Column
	public String getOMNINumber() {
		return OMNINumber;
	}

	public void setOMNINumber(String number) {
		OMNINumber = number;
	}

	@Column
	public String getQualityCertifications() {
		return qualityCertifications;
	}

	public void setQualityCertifications(String qualityCertifications) {
		this.qualityCertifications = qualityCertifications;
	}

	@Column
	public String getServiceRangeKms() {
		return serviceRangeKms;
	}

	public void setServiceRangeKms(String serviceRangeKms) {
		this.serviceRangeKms = serviceRangeKms;
	}

	@Column
	public String getServiceRangeMiles() {
		return serviceRangeMiles;
	}

	public void setServiceRangeMiles(String serviceRangeMiles) {
		this.serviceRangeMiles = serviceRangeMiles;
	}

	@Column
	public String getWERCNumber() {
		return WERCNumber;
	}

	public void setWERCNumber(String number) {
		WERCNumber = number;
	}

	@Column
	public String getYearEstablished() {
		return yearEstablished;
	}

	public void setYearEstablished(String yearEstablished) {
		this.yearEstablished = yearEstablished;
	}

	@Column(length = 1500)
	public String getCompanyCapabilities() {
		return companyCapabilities;
	}

	public void setCompanyCapabilities(String companyCapabilities) {
		this.companyCapabilities = companyCapabilities;
	}

	@Column(length = 1500)
	public String getCompanyDestiantionProfile() {
		return companyDestiantionProfile;
	}

	public void setCompanyDestiantionProfile(String companyDestiantionProfile) {
		this.companyDestiantionProfile = companyDestiantionProfile;
	}

	@Column(length = 1500)
	public String getCompanyFacilities() {
		return companyFacilities;
	}

	public void setCompanyFacilities(String companyFacilities) {
		this.companyFacilities = companyFacilities;
	}

	@Column
	public String getServiceLines() {
		return serviceLines;
	}

	public void setServiceLines(String serviceLines) {
		this.serviceLines = serviceLines;
	}

	@Column
	public String getVanLineAffiliation() {
		return vanLineAffiliation;
	}

	public void setVanLineAffiliation(String vanLineAffiliation) {
		this.vanLineAffiliation = vanLineAffiliation;
	}

	@Column
	public String getTrackingUrl() {
		return trackingUrl;
	}

	public void setTrackingUrl(String trackingUrl) {
		this.trackingUrl = trackingUrl;
	}

	@Column
	public String getFacilitySizeSQFT() {
		return facilitySizeSQFT;
	}

	public void setFacilitySizeSQFT(String facilitySizeSQFT) {
		this.facilitySizeSQFT = facilitySizeSQFT;
	}

	@Column
	public String getFacilitySizeSQMT() {
		return facilitySizeSQMT;
	}

	public void setFacilitySizeSQMT(String facilitySizeSQMT) {
		this.facilitySizeSQMT = facilitySizeSQMT;
	}

	@Transient
	public String getListEigthDescription() {
		return null;
	}

	@Transient
	public String getListNinthDescription() {
		return null;
	}

	@Transient
	public String getListTenthDescription() {
		return null;
	}

	@Transient
	public String getListSeventhDescription() {
		String billingEmail = this.billingEmail;
		if (billingEmail == null) { 
			billingEmail = "";
		}
		return billingEmail;
	
	}

	public String getMergedCode() {
		return mergedCode;
	}

	public void setMergedCode(String mergedCode) {
		this.mergedCode = mergedCode;
	}

	public String getAgentParentName() {
		return agentParentName;
	}

	public void setAgentParentName(String agentParentName) {
		this.agentParentName = agentParentName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Boolean getDoNotMerge() {
		return doNotMerge;
	}

	public void setDoNotMerge(Boolean doNotMerge) {
		this.doNotMerge = doNotMerge;
	}
    @Column(length=100)
	public String getReloContact() {
		return reloContact;
	}

	public void setReloContact(String reloContact) {
		this.reloContact = reloContact;
	}
	@Column(length=100)
	public String getReloContactEmail() {
		return reloContactEmail;
	}

	public void setReloContactEmail(String reloContactEmail) {
		this.reloContactEmail = reloContactEmail;
	}
	@Column(length=100)
	public String getReloServices() {
		return reloServices;
	}

	public void setReloServices(String reloServices) {
		this.reloServices = reloServices;
	}

	public Integer getMinimumDFB() {
		return minimumDFB;
	}

	public void setMinimumDFB(Integer minimumDFB) {
		this.minimumDFB = minimumDFB;
	}

	public double getVendorCommission() {
		return vendorCommission;
	}

	public void setVendorCommission(double vendorCommission) {
		this.vendorCommission = vendorCommission;
	}

	public Boolean getIsNetworkPartner() {
		return isNetworkPartner;
	}

	public void setIsNetworkPartner(Boolean isNetworkPartner) {
		this.isNetworkPartner = isNetworkPartner;
	}

	/**
	 * @return the utsNumber
	 */
	public String getUtsNumber() {
		return utsNumber;
	}

	/**
	 * @param utsNumber the utsNumber to set
	 */
	public void setUtsNumber(String utsNumber) {
		this.utsNumber = utsNumber;
	}
    @Column
	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	@Column
	public Integer getVanAvailCube() {
		return vanAvailCube;
	}

	public void setVanAvailCube(Integer vanAvailCube) {
		this.vanAvailCube = vanAvailCube;
	}
	@Column
	public String getVanLastLocation() {
		return vanLastLocation;
	}

	public void setVanLastLocation(String vanLastLocation) {
		this.vanLastLocation = vanLastLocation;
	}
	@Column
	public Date getVanLastReportOn() {
		return vanLastReportOn;
	}

	public void setVanLastReportOn(Date vanLastReportOn) {
		this.vanLastReportOn = vanLastReportOn;
	}
	@Column
	public String getVanLastReportTime() {
		return vanLastReportTime;
	}

	public void setVanLastReportTime(String vanLastReportTime) {
		this.vanLastReportTime = vanLastReportTime;
	}
	@Column
	public Date getNextReportOn() {
		return nextReportOn;
	}

	public void setNextReportOn(Date nextReportOn) {
		this.nextReportOn = nextReportOn;
	}
	@Column
	public String getNextVanLocation() {
		return nextVanLocation;
	}

	public void setNextVanLocation(String nextVanLocation) {
		this.nextVanLocation = nextVanLocation;
	}
	

	public String getCurrentTractorAgency() {
		return currentTractorAgency;
	}

	public void setCurrentTractorAgency(String currentTractorAgency) {
		this.currentTractorAgency = currentTractorAgency;
	}

	public String getCurrentTractorID() {
		return currentTractorID;
	}

	public void setCurrentTractorID(String currentTractorID) {
		this.currentTractorID = currentTractorID;
	}

	public String getCurrentVanAgency() {
		return currentVanAgency;
	}

	public void setCurrentVanAgency(String currentVanAgency) {
		this.currentVanAgency = currentVanAgency;
	}

	public String getCurrentVanID() {
		return currentVanID;
	}

	public void setCurrentVanID(String currentVanID) {
		this.currentVanID = currentVanID;
	}
	
	@Column
	public Boolean getNetworkGroup() {
		return networkGroup;
	}

	public void setNetworkGroup(Boolean networkGroup) {
		this.networkGroup = networkGroup;
	}
	@Column
	public String getAliasName() {
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}
	@Column
	public String getMovingCompanyType() {
		return movingCompanyType;
	}

	public void setMovingCompanyType(String movingCompanyType) {
		this.movingCompanyType = movingCompanyType;
	}

	@Column
	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	@Column
	public Boolean getPartnerPortalActive() {
		return partnerPortalActive;
	}

	public void setPartnerPortalActive(Boolean partnerPortalActive) {
		this.partnerPortalActive = partnerPortalActive;
	}
	@Column
	public String getAssociatedAgents() {
		return associatedAgents;
	}

	public void setAssociatedAgents(String associatedAgents) {
		this.associatedAgents = associatedAgents;
	}
	@Column
	public Boolean getViewChild() {
		return viewChild;
	}

	public void setViewChild(Boolean viewChild) {
		this.viewChild = viewChild;
	}
	
	@Column
	public String getUTSmovingCompanyType() {
		return UTSmovingCompanyType;
	}

	public void setUTSmovingCompanyType(String uTSmovingCompanyType) {
		UTSmovingCompanyType = uTSmovingCompanyType;
	}
@Column(length = 20)
	public String getTransPort() {
		return transPort;
	}

	public void setTransPort(String transPort) {
		this.transPort = transPort;
	}

	@Column
	public String getBillingCurrency() {
		return billingCurrency;
	}

	public void setBillingCurrency(String billingCurrency) {
		this.billingCurrency = billingCurrency;
	}

	@Column
	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	@Column
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	@Column
	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}
	
	@Column
	public Boolean getUgwwNetworkGroup () {
		return ugwwNetworkGroup ;
	}

	public void setUgwwNetworkGroup (Boolean ugwwNetworkGroup ) {
		this.ugwwNetworkGroup  = ugwwNetworkGroup ;
	}

	
	@Column
	public String getAgentGroup() {
		return agentGroup;
	}

	public void setAgentGroup(String agentGroup) {
		this.agentGroup = agentGroup;
	}
	@Column
	public Boolean getIsPartnerExtract() {
		return isPartnerExtract;
	}

	public void setIsPartnerExtract(Boolean isPartnerExtract) {
		this.isPartnerExtract = isPartnerExtract;
	}
	@Column
	public Boolean getSentToAccounting() {
		return sentToAccounting;
	}

	public void setSentToAccounting(Boolean sentToAccounting) {
		this.sentToAccounting = sentToAccounting;
	}
	@Column
	public String getMergeInto() {
		return mergeInto;
	}

	public void setMergeInto(String mergeInto) {
		this.mergeInto = mergeInto;
	}
	@Column
	public String getUgwwCarrierCode() {
		return ugwwCarrierCode;
	}

	public void setUgwwCarrierCode(String ugwwCarrierCode) {
		this.ugwwCarrierCode = ugwwCarrierCode;
	}
	@Column
	public Boolean getDoNotSendEmailtoAgentUser() {
		return doNotSendEmailtoAgentUser;
	}

	public void setDoNotSendEmailtoAgentUser(Boolean doNotSendEmailtoAgentUser) {
		this.doNotSendEmailtoAgentUser = doNotSendEmailtoAgentUser;
	} 

	@Column
	public String getEurovanNetwork() {
		return eurovanNetwork;
	}

	public void setEurovanNetwork(String eurovanNetwork) {
		this.eurovanNetwork = eurovanNetwork;
	}

	@Column
	public String getPAIMA() {
		return PAIMA;
	}

	public void setPAIMA(String pAIMA) {
		PAIMA = pAIMA;
	}

	@Column
	public String getLACMA() {
		return LACMA;
	}

	public void setLACMA(String lACMA) {
		LACMA = lACMA;
	}

	public String getCompanyLogo() {
		return companyLogo;
	}

	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}
	
	

}
