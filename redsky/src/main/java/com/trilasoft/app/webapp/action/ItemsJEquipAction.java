package com.trilasoft.app.webapp.action; 
 
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import org.bouncycastle.asn1.ocsp.Request;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Contract;
import com.trilasoft.app.model.Entitlement;
import com.trilasoft.app.model.ItemsJEquip;
import com.trilasoft.app.model.ItemsJbkEquip;
import com.trilasoft.app.model.RateGrid;
import com.trilasoft.app.model.ResourceContractCharges;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.ItemsJEquipManager;
import com.trilasoft.app.service.ItemsJbkEquipManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ResourceContractChargeManager;
import com.trilasoft.app.service.WorkTicketManager;
 
public class ItemsJEquipAction extends BaseAction implements Preparable {
	
    private ItemsJEquipManager itemsJEquipManager;
	private ItemsJbkEquipManager itemsJbkEquipManager;

	static private List itemsJEquips;
	private List itemsJbkEquips;
    private List ls;
    private WorkTicketManager workTicketManager;
	private List workTickets;
	private Long id2;
	private Long id3;
	private Long id4;
	private String mailtovendor;
	private List materialsList;
	private String cont;
	private ItemsJEquip itemsJEquip;
	/* Added by Kunal Sharma */
	private List contractList;
	private Contract objContract;
	private String contractType;
	private Charges charges;
	private List chargesList;
	/* Modifications Closed here */
    private WorkTicket workTicket;
    private Long id; 
    private Long id1; 
    private Long ticket; 
    private String itemType;
    private String wcontract;
    private String sessionCorpID;
	
	private String gotoPageString;
    private String validateFormNav;
    Date currentdate = new Date();
    private RefMasterManager refMasterManager;
    private Map<String, String> resourceCategory;
    private String controlled;
    private String type;
    private String descript;
    
    private List contractChargeList=new ArrayList();
    private ResourceContractChargeManager resourceContractChargeManager;
    private ResourceContractCharges resourceContractCharges;
    private int contractChargeListSize;
    private String chid;
    private String resourceContractURL;
    private String contractCharge;
    private Long parentId;
    private String flagAdd;
    private Long contractId;
    private String contractName;
    private String shipNumber;
    private Long serviceOrderId;
    private List resourceList = new ArrayList();
    
    private String chargeCode;
    private String charDesc;
    private String contractDesc;
    private String contractCd;
    private String tempId;
    private Company company;
    private CompanyManager companyManager;
    private String oiJobList;
	
    static final Logger logger = Logger.getLogger(ItemsJEquipAction.class);
    
    public String saveOnTabChange() throws Exception {
    	validateFormNav = "OK";
    	String s = saveMatEquip();
    	return gotoPageString;
    }
    
    public ItemsJEquipAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	private ErrorLogManager errorLogManager;	
    public void prepare() throws Exception {
		try{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			company=companyManager.findByCorpID(sessionCorpID).get(0);
	        if(company!=null && company.getOiJob()!=null){
					oiJobList=company.getOiJob();  
				  }
		}catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}	
		}
	
    public void setItemsJEquipManager(ItemsJEquipManager itemsJEquipManager) {
        this.itemsJEquipManager = itemsJEquipManager;
    }
 
    public void setItemsJbkEquipManager(ItemsJbkEquipManager itemsJbkEquipManager) {
        this.itemsJbkEquipManager = itemsJbkEquipManager;
    }
    public void setWorkTicketManager(WorkTicketManager workTicketManager) {
        this.workTicketManager = workTicketManager;
    }
 
	/*
    public void setWorkTicketManager(GenericManager<WorkTicket, Long> workTicketManager) { 
        this.workTicketManager = workTicketManager; 
    } 
 */
    public List getItemsJEquips() { 
        return itemsJEquips; 
    } 
    
    public List getItemsJbkEquips() { 
        return itemsJbkEquips; 
    }  
    public List getWorkTickets() { 
        return workTickets; 
    }
 
//////////////////////////////////////////////////////////////////////////////
    
    

	private ItemsJbkEquip itemsJbkEquip;
     
    public void setId(Long id) { 
        this.id = id; 
    }
    public void setId1(Long id1) { 
        this.id1 = id1; 
    }
    public void setId4(Long id4) { 
        this.id4 = id4; 
    }
    public void setTicket(Long ticket) { 
        this.ticket = ticket; 
    }
    public void setItemType(String itemType) { 
        this.itemType = itemType; 
    }
    public void setWcontract(String wcontract) {
		this.wcontract = wcontract;
	}
	
   // public void setLastName(String lastName) { 
     //   this.lastName = lastName; 
   // } 
     
    public ItemsJEquip getItemsJEquip() { 
        return itemsJEquip;
    } 
     
    public void setItemsJEquip(ItemsJEquip itemsJEquip) { 
        this.itemsJEquip = itemsJEquip; 
    } 
    
    public ItemsJbkEquip getItemsJbkEquip() { 
        return itemsJbkEquip;
    } 
     
    public void setItemsJbkEquip(ItemsJbkEquip itemsJbkEquip) { 
        this.itemsJbkEquip = itemsJbkEquip; 
    } 
    public WorkTicket getWorkTicket() { 
        return workTicket;
    } 
     
    public void setWorkTicket(WorkTicket workTicket) { 
        this.workTicket = workTicket; 
    }     
    public void setMailtovendor(String mailtovendor) {   
        this.mailtovendor = mailtovendor;   
    } 
    public String delete() { 
    	itemsJEquipManager.remove(id); 
        saveMessage(getText("itemsJEquip.deleted")); 
        return SUCCESS; 
    } 
     
    public String edit() { 
    	getComboList(sessionCorpID);
        if (id != null) { 
        	itemsJEquip = itemsJEquipManager.get(id); 
        } else { 
        	itemsJEquip = new ItemsJEquip(); 
        } 
     
        return SUCCESS; 
    } 
     
    public String save() throws Exception { 
        if (cancel != null) { 
            return "cancel"; 
        } 
     
        if (delete != null) { 
            return delete(); 
        } 
 //       itemsJbkEquip.setWorkTicket(workTicket);    
        boolean isNew = (itemsJEquip.getId() == null); 
        
        itemsJEquip.setCorpID(sessionCorpID);
        itemsJEquipManager.save(itemsJEquip); 
        //itemsJEquipManager.save(itemsJbkEquip); 
     
        String key = (isNew) ? "itemsJEquip.added" : "itemsJEquip.updated"; 
        saveMessage(getText(key)); 
     
        if (!isNew) { 
            return INPUT; 
        } else { 
            return SUCCESS; 
        }
    } 
    
    public String search() 
    {      
    	boolean myType = (itemsJEquip.getType() == null);
    	boolean myDescription = (itemsJEquip.getDescript() == null);
    	
    	if(!myType || !myDescription) {
    		itemsJEquips = itemsJEquipManager.findByDescript(itemsJEquip.getType(),itemsJEquip.getDescript(),wcontract, id1);
  }
    	workTicket = workTicketManager.get(id1); 
  return SUCCESS;     
   
    }
    
    public void listByType(){
    	itemsJEquips = itemsJEquipManager.getAll();
    	//itemsJEquips=itemsJEquipManager.findByType("M", wcontract);
    	//System.out.println("Type is M here");
    }
    
    
    //////////////////////   
       
       
       public String list() {
    	   //itemsJEquips = itemsJEquipManager.getAll();
    	   //itemsJEquips = itemsJEquipManager.findSection();
    	   
       		itemsJEquips=itemsJEquipManager.findByType("M", wcontract, id);
       		//System.out.println("Type is M here");
    	   //System.out.println(mailtovendor);
    		  String[] result = (mailtovendor.split("\\,"));
    			//System.out.println(id);
    			//System.out.println(result);
    			   int i;    			  
    			     for (int x=1; x<result.length; x++)
    			     {
    			    	 String s1 = result[x];
    			    	 if(s1==" ")
    			    	 {
    			    		 //System.out.println(s1);
    			    		continue;    			    		 
    			    	 }    			    
    			    	 else
    			    	 {
    			    	  //System.out.println(result[x]);    			    	  
    			    	 id2=Long.parseLong(s1);    			    	   	 
    			    	 itemsJEquip = itemsJEquipManager.get(id2);
    			       		itemsJEquip.setFlag("A");
    			       		// By Sid    			       		
    			       		//
    			       		itemsJEquipManager.save(itemsJEquip);
    			       		System.out.println(itemsJEquip);
    			       		itemsJbkEquip = new ItemsJbkEquip();
    			       		//itemsJbkEquip.setId(id);
    			       		itemsJbkEquip.setSeqNum(s1);
    			       		itemsJbkEquip.setType(itemsJEquip.getType());
    			       		itemsJbkEquip.setQty(itemsJEquip.getQty());
    			       		itemsJbkEquip.setDescript(itemsJEquip.getDescript());
    			       		itemsJbkEquip.setOtCharge(itemsJEquip.getOtCharge());
    			       		itemsJbkEquip.setPackLabour(itemsJEquip.getPackLabour());
    			       		itemsJbkEquip.setCost(itemsJEquip.getCost());
    			       		itemsJbkEquip.setCharge(itemsJEquip.getCharge());
    			       		itemsJbkEquip.setDtCharge(itemsJEquip.getDtCharge());
    			       		itemsJbkEquip.setFlag("A");
    			       		itemsJbkEquip.setTicket(ticket);
    			       		//System.out.println(itemsJbkEquip);
    			       		//System.out.println(itemsJbkEquip.getId());
    			       		itemsJbkEquipManager.save(itemsJbkEquip);
    			    	 }
    			    	
    			     }
     itemsJEquips = new ArrayList(itemsJEquipManager.findSection(itemType, wcontract));
     itemsJbkEquips = new ArrayList( itemsJbkEquipManager.findSection(ticket, itemType) );
     workTicket = workTicketManager.get(id);
           return SUCCESS; 
       } 
    
       @SkipValidation
       public String updatestatus() throws Exception { 
    	   //System.out.println(mailtovendor);
    		  String[] result = (mailtovendor.split("\\,"));
    			//System.out.println(id);
    			//System.out.println(result);
    			   int i;
    			  
    			     for (int x=1; x<result.length; x++)
    			     {
    			    	 String s1 = result[x];
    			    	 if(s1==" ")
    			    	 {
    			    		 //System.out.println(s1);
    			    		continue;    			    		 
    			    	 }    			    
    			    	 else
    			    	 {
    			    	  //System.out.println(result[x]);
    			    	  
    			    	 id3=Long.parseLong(s1);
    			       		itemsJbkEquip = itemsJbkEquipManager.get(id3);
    			       		long mn=itemsJbkEquip.getId();
    			       		long ms=Long.parseLong(itemsJbkEquip.getSeqNum());
    			       		//System.out.println("anuj"+mn);
    			       		//System.out.println("anuj12"+ms);
    			       	 itemsJEquip = itemsJEquipManager.get(ms);
 			       		itemsJEquip.setFlag("U");
 			       		itemsJEquipManager.save(itemsJEquip);
 			       	
    			       		itemsJbkEquip.setFlag("U");
    			       		itemsJbkEquipManager.save(itemsJbkEquip);    
    			    	 }    			    	
    			     }    		
       	
    			     //System.out.println("aman"+id4);			    
				     itemsJEquips = new ArrayList(itemsJEquipManager.findSection(itemType, wcontract));
				     workTicket = workTicketManager.get(id4); 
				     long ticket4= workTicket.getTicket();
				     //System.out.println("aman"+ticket4);
				     itemsJbkEquips = new ArrayList(itemsJbkEquipManager.findSection(workTicket.getTicket(),itemType));
            return SUCCESS; 
       
       }
       
       public String getComboList(String corpID) {
    	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	   resourceCategory = refMasterManager.findByParameter(corpID, "Resource_Category");
    	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
   		return SUCCESS;
       }
       @SkipValidation 
       public String materialsList() {
    	   	String contracts="";
       		//materialsList = itemsJEquipManager.findMaterialByContractTab(cont,itemType,sessionCorpID);
    	   	materialsList = itemsJEquipManager.findMaterialByContractTab(contracts, itemType, sessionCorpID);
       		return SUCCESS;
       }
       
    private List itemList;
   	public String itemList(){ 
   		getComboList(sessionCorpID);
   		itemList = itemsJEquipManager.getAll();
   		return SUCCESS;
   	}

   	private String cType;
   	private String cDescript;
   	private Boolean cControlled;
   	public String searchItemEquipRecord(){
   		getComboList(sessionCorpID);
   		itemList=itemsJEquipManager.getListValue(cDescript, cType, cControlled);
   		return SUCCESS;
   	}
       @SkipValidation 
       public String searchMatEquip() {
    	   String contracts="";
       	   //materialsList = itemsJEquipManager.findByDescription(itemsJEquip.getDescript(), cont, sessionCorpID, itemType);
    	   materialsList = itemsJEquipManager.findByDescription(itemsJEquip.getDescript(), contracts, sessionCorpID, itemType);
       	   return SUCCESS;
       }
       
       @SkipValidation 
       public String editMatEquip() {
    	   getComboList(sessionCorpID);
       	if (id != null) {   
       		itemsJEquip = itemsJEquipManager.get(id);
       		contractChargeList = resourceContractChargeManager.findResourceByPartnerId(sessionCorpID,id);
       	}else{   
           	itemsJEquip = new ItemsJEquip();
           	itemsJEquip.setCreatedOn(new Date());
           	itemsJEquip.setUpdatedOn(new Date());
           	itemsJEquip.setCost(0.0);
           }
       	return SUCCESS;
       }
       
       @SkipValidation 
       public String editMatEquipRowGrid() {
    	   getComboList(sessionCorpID);
       	if (id != null) {   
       		itemsJEquip = itemsJEquipManager.get(id);
       		contractChargeList = resourceContractChargeManager.findResourceByPartnerId(sessionCorpID,id);
       		resourceContractCharges=new ResourceContractCharges();
        	contractChargeList.add(resourceContractCharges);
        	resourceContractURL="?id="+id;
        	Long na = parentId;
       	}else{   
           	itemsJEquip = new ItemsJEquip();
           	itemsJEquip.setCreatedOn(new Date());
           	itemsJEquip.setUpdatedOn(new Date());
           	itemsJEquip.setCost(0.0);
           }
       	return SUCCESS;
       }
       
       @SkipValidation
       public String addRowToContractChargeGrid() throws Exception {
    	   getComboList(sessionCorpID);
     	   saveContractChargeGrid();
    	contractChargeList = resourceContractChargeManager.findResourceByPartnerId(sessionCorpID,id);
    	resourceContractCharges=new ResourceContractCharges();
    	contractChargeList.add(resourceContractCharges);
    	 if(parentId != null){
    		 resourceContractURL="?id="+parentId;
    	 }
    	 flagAdd = "1";
       	return SUCCESS;
   }
       
       public String saveContractChargeGrid1() throws Exception {
    	   if(itemsJEquip.getId() != null){
    	       	resourceContractChargeManager.deleteAll(itemsJEquip.getId());
    	   }
    	   if(contractCharge != null && itemsJEquip.getId() != null){
    		   contractCharge = contractCharge.replaceAll("~", "&");
    		   StringTokenizer st2 = new StringTokenizer(contractCharge,"@");	
    		   
    	    	while(st2.hasMoreElements()){
    	    		ResourceContractCharges resourceContractCharges = new ResourceContractCharges();
    	    		String contract = st2.nextElement().toString();
    	    		StringTokenizer con = new StringTokenizer(contract,"*");
    	    		while(con.hasMoreElements()){
    	    			resourceContractCharges.setContract(con.nextElement().toString());
    	    			if(con.countTokens() >0){
    	    				resourceContractCharges.setCharge(con.nextElement().toString());
    	    			}
    	    			resourceContractCharges.setCorpID(sessionCorpID);
    	    			resourceContractCharges.setUpdatedOn(new Date());
    	    			resourceContractCharges.setUpdatedBy(getRequest().getRemoteUser()); 
    	    	       	resourceContractCharges.setCreatedBy(getRequest().getRemoteUser());
    	    	       	resourceContractCharges.setCreatedOn(new Date());
    	    	       	resourceContractCharges.setParentId(itemsJEquip.getId());
    	    	    	resourceContractChargeManager.save(resourceContractCharges);
    	    		}
    	    	}
    	   }
       	return SUCCESS;
   }
       
       @SkipValidation
       public String saveContractChargeGrid() throws Exception {
//    	   getComboList(sessionCorpID);
    	   if(parentId != null){
    	       	 resourceContractURL="?id="+parentId;
    	       	resourceContractChargeManager.deleteAll(parentId);
    	   }
    	   if(contractCharge != null){
    		   contractCharge = contractCharge.replaceAll("~", "&");
    		   StringTokenizer st2 = new StringTokenizer(contractCharge,"@");	
    		   
    	    	while(st2.hasMoreElements()){
    	    		ResourceContractCharges resourceContractCharges = new ResourceContractCharges();
    	    		String contract = st2.nextElement().toString();
    	    		StringTokenizer con = new StringTokenizer(contract,"*");
    	    		while(con.hasMoreElements()){
    	    			resourceContractCharges.setContract(con.nextElement().toString());
    	    			if(con.countTokens() >0){
    	    				resourceContractCharges.setCharge(con.nextElement().toString());
    	    			}
    	    			resourceContractCharges.setCorpID(sessionCorpID);
    	    			resourceContractCharges.setUpdatedOn(new Date());
    	    			resourceContractCharges.setUpdatedBy(getRequest().getRemoteUser()); 
    	    	       	resourceContractCharges.setCreatedBy(getRequest().getRemoteUser());
    	    	       	resourceContractCharges.setCreatedOn(new Date());
    	    	       	resourceContractCharges.setParentId(parentId);
    	    	    	resourceContractChargeManager.save(resourceContractCharges);
    	    		}
    	    	}
    	   }
       	return SUCCESS;
   }
       
       public String deleteContractCharge() throws Exception {
    	resourceContractChargeManager.remove(contractId);
    	resourceContractURL="?id="+id;
   		return SUCCESS;
   	}
       private String divisionFlag;
       @SkipValidation
       public String createPrice() throws Exception {
       	try {
			resourceContractChargeManager.createPrice(sessionCorpID,contractName,shipNumber,serviceOrderId,divisionFlag);
		} catch (Exception e) {
			e.printStackTrace();
		}
       	resourceContractURL="?id="+tempId;
      	return SUCCESS;
      }
       
       @SkipValidation
       public String createPriceSO() throws Exception {
          	try {
				resourceContractChargeManager.createPrice(sessionCorpID,contractName,shipNumber,serviceOrderId,divisionFlag);
			} catch (Exception e) {
				e.printStackTrace();
			}
          	resourceContractURL="?id="+tempId;
         	return SUCCESS;
         }
       
       public String getResource() throws Exception {
//    	  String type =  itemsJEquip.getType();
//    	  String resource = itemsJEquip.getDescript();
          	resourceList = itemsJEquipManager.getResource(sessionCorpID,type,descript);
         	return SUCCESS;
         }
        
       public String saveMatEquip(){
    	   try {
    		   saveContractChargeGrid1();
    	   } catch (Exception e) {	}
    	   
    	   getComboList(sessionCorpID);
    	   	boolean isNew = (itemsJEquip.getId() == null);     	   	
    	   	if(isNew){
    	   		itemsJEquip.setCreatedOn(new Date());
   	     }else{
   	     	itemsJEquip.setCreatedOn(itemsJEquip.getCreatedOn());
   	     }
    	   	itemsJEquip.setUpdatedBy(getRequest().getRemoteUser());
    	   	itemsJEquip.setCorpID(sessionCorpID);
    	   	itemsJEquip.setUpdatedOn(new Date());
            itemsJEquip.setCorpID(sessionCorpID);
            itemsJEquip.setTypeDescript(refMasterManager.getDescription(itemsJEquip.getType(), "Resource_Category").get(0).toString());
          itemsJEquip = itemsJEquipManager.save(itemsJEquip); 
         
          String key = (isNew) ? "itemsJEquip.added" : "itemsJEquip.updated"; 
          saveMessage(getText(key)); 
        
         
          return SUCCESS; 
       }
       
       private String cCharge;
       @SkipValidation
       public String searchResourceCharges() { 
	       	try{
	       		chargesList = itemsJEquipManager.findCharges(contractType,cCharge,cDescript,sessionCorpID);
	       	}catch(Exception e){
	       		logger.warn(e);
	       	}
       		return SUCCESS;   
       }
       private String cContract;
       private String cDesc;
       @SkipValidation
       public String searchResourceContract() { 
	       	try{
	       		contractList = itemsJEquipManager.findContracts(cContract,cDesc,sessionCorpID);
	       	}catch(Exception e){
	       		logger.warn(e);
	       	}
       		return SUCCESS;   
       }

      /* Ticket Number: 5984 By Kunal Sharma */
       public String openContractPopup(){
    	   contractList = itemsJEquipManager.findContractAndType(sessionCorpID);
    	   return SUCCESS;
       }
       
       public String openChargesPopup(){
    	   chargesList = itemsJEquipManager.findChargesAndType(contractType,sessionCorpID);
    	   return SUCCESS;
       }
      /* Closed Here */
       
       private List dupliResource;
       private String dupliExists;
       public String duplicateResource(){
    	   dupliResource = itemsJEquipManager.checkDuplicateResource(descript, sessionCorpID);
    	   if(dupliResource!=null && !dupliResource.isEmpty()){
    		   dupliExists = "Y";
    	   }
    	   return SUCCESS;
       }
       
	public String getCont() { 
		return cont;
	}

	public void setCont(String cont) {
		this.cont = cont;
	}

	public List getMaterialsList() {
		return materialsList;
	}

	public void setMaterialsList(List materialsList) {
		this.materialsList = materialsList;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public List getContractList() {
		return contractList;
	}

	public void setContractList(List contractList) {
		this.contractList = contractList;
	}

	public Contract getObjContract() {
		return objContract;
	}

	public void setObjContract(Contract objContract) {
		this.objContract = objContract;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public Charges getCharges() {
		return charges;
	}

	public void setCharges(Charges charges) {
		this.charges = charges;
	}

	public List getChargesList() {
		return chargesList;
	}

	public void setChargesList(List chargesList) {
		this.chargesList = chargesList;
	}


	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getResourceCategory() {
		return resourceCategory;
	}

	public void setResourceCategory(Map<String, String> resourceCategory) {
		this.resourceCategory = resourceCategory;
	}
	
	public List getItemList() {
		return itemList;
	}

	public void setItemList(List itemList) {
		this.itemList = itemList;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescript() {
		return descript;
	}

	public void setDescript(String descript) {
		this.descript = descript;
	}

	public String getControlled() {
		return controlled;
	}

	public void setControlled(String controlled) {
		this.controlled = controlled;
	}

	public String getcType() {
		return cType;
	}

	public void setcType(String cType) {
		this.cType = cType;
	}

	public String getcDescript() {
		return cDescript;
	}

	public void setcDescript(String cDescript) {
		this.cDescript = cDescript;
	}

	public Boolean getcControlled() {
		return cControlled;
	}

	public void setcControlled(Boolean cControlled) {
		this.cControlled = cControlled;
	}

	public List getContractChargeList() {
		return contractChargeList;
	}

	public void setContractChargeList(List contractChargeList) {
		this.contractChargeList = contractChargeList;
	}

	public ResourceContractChargeManager getResourceContractChargeManager() {
		return resourceContractChargeManager;
	}

	public void setResourceContractChargeManager(
			ResourceContractChargeManager resourceContractChargeManager) {
		this.resourceContractChargeManager = resourceContractChargeManager;
	}

	public ResourceContractCharges getResourceContractCharges() {
		return resourceContractCharges;
	}

	public void setResourceContractCharges(
			ResourceContractCharges resourceContractCharges) {
		this.resourceContractCharges = resourceContractCharges;
	}

	public int getContractChargeListSize() {
		return contractChargeListSize;
	}

	public void setContractChargeListSize(int contractChargeListSize) {
		this.contractChargeListSize = contractChargeListSize;
	}

	public String getChid() {
		return chid;
	}

	public void setChid(String chid) {
		this.chid = chid;
	}

	public String getResourceContractURL() {
		return resourceContractURL;
	}

	public void setResourceContractURL(String resourceContractURL) {
		this.resourceContractURL = resourceContractURL;
	}

	public String getContractCharge() {
		return contractCharge;
	}

	public void setContractCharge(String contractCharge) {
		this.contractCharge = contractCharge;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getFlagAdd() {
		return flagAdd;
	}

	public void setFlagAdd(String flagAdd) {
		this.flagAdd = flagAdd;
	}

	public Long getContractId() {
		return contractId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	public Long getServiceOrderId() {
		return serviceOrderId;
	}

	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getCharDesc() {
		return charDesc;
	}

	public void setCharDesc(String charDesc) {
		this.charDesc = charDesc;
	}

	public String getContractDesc() {
		return contractDesc;
	}

	public void setContractDesc(String contractDesc) {
		this.contractDesc = contractDesc;
	}

	public String getContractCd() {
		return contractCd;
	}

	public void setContractCd(String contractCd) {
		this.contractCd = contractCd;
	}

	public List getResourceList() {
		return resourceList;
	}

	public void setResourceList(List resourceList) {
		this.resourceList = resourceList;
	}

	public List getDupliResource() {
		return dupliResource;
	}

	public void setDupliResource(List dupliResource) {
		this.dupliResource = dupliResource;
	}

	public String getDupliExists() {
		return dupliExists;
	}

	public void setDupliExists(String dupliExists) {
		this.dupliExists = dupliExists;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getcCharge() {
		return cCharge;
	}

	public void setcCharge(String cCharge) {
		this.cCharge = cCharge;
	}

	public String getcContract() {
		return cContract;
	}

	public void setcContract(String cContract) {
		this.cContract = cContract;
	}

	public String getcDesc() {
		return cDesc;
	}

	public void setcDesc(String cDesc) {
		this.cDesc = cDesc;
	}

	public String getDivisionFlag() {
		return divisionFlag;
	}

	public void setDivisionFlag(String divisionFlag) {
		this.divisionFlag = divisionFlag;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	} 

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	} 

	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}
    
} 