<%@ include file="/common/taglibs.jsp"%> 
<head>
	<meta name="heading" content="DataMaintainence"/> 
	<title>Data Maintenance</title>
</head>
<script type="text/javascript"> 
 function goToExecuteDataMaintainenceQuery(){
    var id=document.forms['dataMaintainenceForm'].elements['id'].value
   if(id!=''){
     window.location = 'executeDataMaintainenceQuery.html?id=${id}';
      }else{
             alert("Please Save The Data First");
            }
   }
 function checkEmptyQuery(){
  var flag=document.forms['dataMaintainenceForm'].elements['dataMaintainence.query'].value;
  if(flag!=''){
      
      return true;
      }else{
           alert("Enter Query is Required Field");
           return false; 
         }
  }
</script>
	

<s:form id="dataMaintainenceForm" name="dataMaintainenceForm" action="saveDataMaintainence" method="post" validate="true">
<s:hidden name="id" value="%{dataMaintainence.id}"/>
<s:hidden name="dataMaintainence.id" value="%{dataMaintainence.id}"/>
<div id="Layer1"  style="width:750px" >
<div id="newmnav">
	<ul>
	<li id="newmnav1" style="background:#FFF"><a  class="current"><span>Data Maintenance<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  </ul>
</div><div class="spn" style="!margin-bottom:2px;">&nbsp;</div>

<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:0px;!margin-top:0px;"><span></span></div>
<div class="center-content">
<table class="detailTabLabel" border="0" width="100%">
  <tbody>  	
		  	<tr>
		  		<td align="left" height="10px"></td>
		  	</tr>
		  	<tr>
		  	    <td align="right" class="listwhitetext">CorpID</td>
		  	    <td align="left"><s:select name="dataMaintainence.corpID" list="%{corpIDList}" cssClass="list-menu" headerKey="" headerValue="" /></td>
		  	    <td align="right" class="listwhitetext">Description</td>
		  	    <td align="left"><s:textfield name="dataMaintainence.description"    cssClass="input-text" cssStyle="width:379px;!width:339px;" /></td>
		  	</tr>		  	
		  	<tr>
		  	<td align="right" class="listwhitetext" valign="top">Enter Query<font color="red" size="2">*</font></td>
		  	<td colspan="4" align="left" class="listwhitetext"><s:textarea  id="dataMaintainence.query" name="dataMaintainence.query" rows="3" cols="100"  cssClass="textarea"/></td>		  	
		  	</tr>
		  	<tr>
		  	<td></td>
		  	<td align="left"><s:submit cssClass="cssbutton1" cssStyle="width:65px;" type="button" method="save" label="save"  onclick="return checkEmptyQuery()" /></td>
		  	<td align="left"><input type="button" class="cssbutton1" style="width:65px;" onclick="goToExecuteDataMaintainenceQuery();"  title="Execute" value="Execute" /></td>
		  	</tr>
		  	<tr>
		  	<td align="left" height="20px"></td>
		  	</tr>
</tbody>
</table>
</div>
<div class="bottom-header"><span>&nbsp;</span></div>
</div>
</s:form>
