package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.PayrollAllocationDao;
import com.trilasoft.app.model.PayrollAllocation;
import com.trilasoft.app.service.PayrollAllocationManager;


public class PayrollAllocationManagerImpl extends GenericManagerImpl<PayrollAllocation, Long> implements PayrollAllocationManager{
   
	PayrollAllocationDao payrollAllocationDao;
	
	public PayrollAllocationManagerImpl(PayrollAllocationDao payrollAllocationDao) { 
	   super(payrollAllocationDao); 
	   this.payrollAllocationDao = payrollAllocationDao; 
	}

    public List findMaximumId(){
	    return payrollAllocationDao.findMaximumId(); 
	}
	 
	public List findById(Long id) {
		return payrollAllocationDao.findById(id);
	}
	
	public List<PayrollAllocation> searchpayrollAllocation(String code,String job,String service,String calculation) {
		return payrollAllocationDao.searchpayrollAllocation(code,job,service,calculation);
	}

	public Map<String, String> findRecalc(String corpId){
		return payrollAllocationDao.findRecalc(corpId);
	}

}
