
package com.trilasoft.app.dao.hibernate;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;  

import org.appfuse.dao.hibernate.GenericDaoHibernate;   
import org.hibernate.Hibernate;

import com.trilasoft.app.model.BookStorage;   
import com.trilasoft.app.dao.BookStorageDao;   
  
public class BookStorageDaoHibernate extends GenericDaoHibernate<BookStorage, Long> implements BookStorageDao {   
  
    public BookStorageDaoHibernate() {   
        super(BookStorage.class);   
    }   
  
    public List<BookStorage> findByLocation(String locationId) {   
        return getHibernateTemplate().find("from BookStorage where locationId=?", locationId);   
    } 
    
    public List findMaximum() {
    	return getHibernateTemplate().find("select max(id) from BookStorage");
    }
    
    public List getStorageLibraryList(String ticket, String corpID){
    	List list1 = new ArrayList();
    	String storageList = "select bk.what, st.description, bk.locationId, st.pieces, st.containerId, st.itemTag, bk.oldLocation , bk.id,st.measQuantity,st.unit,st.volume,st.volUnit,st.storageId,bk.oldStorage from bookstorage bk, storage st WHERE bk.ticket = '"+ticket+"' AND bk.ticket = st.ticket AND bk.idNum = st.idNum AND bk.corpID = '"+corpID+"'";
    	//String storageList = "select bk.what, st.description, bk.locationId, st.pieces, st.containerId, st.itemTag, bk.oldLocation , bk.id from bookstorage bk, storage st, workticket wt WHERE wt.shipNumber = '"+ShipNumber+"' AND bk.ticket = st.ticket AND bk.idNum = st.idNum";
    	List listStorageList= this.getSession().createSQLQuery(storageList)
    	.addScalar("what",Hibernate.TEXT)
		.addScalar("description",Hibernate.STRING)
		.addScalar("locationId",Hibernate.STRING)
		.addScalar("pieces",Hibernate.INTEGER)
		.addScalar("containerId",Hibernate.STRING)
		.addScalar("itemTag",Hibernate.STRING)
		.addScalar("oldLocation",Hibernate.STRING)
		.addScalar("id",Hibernate.LONG)
		.addScalar("measQuantity",Hibernate.BIG_DECIMAL)
		.addScalar("unit",Hibernate.STRING)
		.addScalar("volume",Hibernate.BIG_DECIMAL)
		.addScalar("volUnit",Hibernate.STRING)
		.addScalar("storageId",Hibernate.STRING)
		.addScalar("oldStorage",Hibernate.STRING)
		.list();
    	
    	Iterator it=listStorageList.iterator();
		while(it.hasNext()){
	           Object []row= (Object [])it.next();
	           StroageDTO stroageDTO=new StroageDTO();
	           if(row[0] == null){
	        	   stroageDTO.setWhat("");  
	           }else{
	        	   stroageDTO.setWhat(row[0].toString()); 
	           }
	           if(row[1] == null){
	        	   stroageDTO.setDescription("");  
	           }else{
	        	   stroageDTO.setDescription(row[1].toString());
	           }
	           if(row[2] == null){
	        	   stroageDTO.setLocationId("");  
	           }else{
	        	   stroageDTO.setLocationId(row[2].toString());
	           }
	           if(row[3] == null){
	        	   stroageDTO.setPieces(0);  
	           }else{
	        	   stroageDTO.setPieces(row[3].toString()); 
	           }
	           if(row[4] == null){
	        	   stroageDTO.setContainerId("");  
	           }else{
	        	   stroageDTO.setContainerId(row[4].toString());
	           }
	           if(row[5] == null){
	        	   stroageDTO.setItemTag("");  
	           }else{
	        	   stroageDTO.setItemTag(row[5].toString());
	           }
	           if(row[6] == null){
	        	   stroageDTO.setOldLocation("");  
	           }else{
	        	   stroageDTO.setOldLocation(row[6].toString());
	           }
	           if(row[7] == null){
	        	   stroageDTO.setId("");  
	           }else{
	        	   stroageDTO.setId(Long.parseLong(row[7].toString()));
	           }
	           if(row[8] == null){
	        	   stroageDTO.setMeasQuantity("");  
	           }else{
	        	   stroageDTO.setMeasQuantity(row[8].toString());
	           }
	           if(row[9] == null){
	        	   stroageDTO.setUnit("");  
	           }else{
	        	   stroageDTO.setUnit(row[9].toString());
	           }
	           if(row[10] == null){
	        	   stroageDTO.setVolume("");  
	           }else{
	        	   stroageDTO.setVolume(row[10].toString());
	           }
	           if(row[11] == null){
	        	   stroageDTO.setVolUnit("");  
	           }else{
	        	   stroageDTO.setVolUnit(row[11].toString());
	           }
	           if(row[12] == null){
	        	   stroageDTO.setStorageId("");  
	           }else{
	        	   stroageDTO.setStorageId(row[12].toString());
	           }
	           if(row[13] == null){
	        	   stroageDTO.setOldStorage("");  
	           }else{
	        	   stroageDTO.setOldStorage(row[13].toString());
	           }
	           list1.add(stroageDTO);
	       }
		return list1;
    }
	
    public List getStorageList(String ticket, String corpID){
    	List list1 = new ArrayList();
    	String storageList = "select bk.what, st.description, bk.locationId, st.pieces, st.containerId, st.itemTag, bk.oldLocation , bk.id,st.measQuantity,st.unit,st.volume,st.volUnit,st.originalPieces,st.originalMeasQuantity,st.originalVolume,st.releaseDate from bookstorage bk, storage st WHERE bk.ticket = '"+ticket+"' AND bk.ticket = st.ticket AND bk.idNum = st.idNum AND bk.corpID = '"+corpID+"'";
    	//String storageList = "select bk.what, st.description, bk.locationId, st.pieces, st.containerId, st.itemTag, bk.oldLocation , bk.id from bookstorage bk, storage st, workticket wt WHERE wt.shipNumber = '"+ShipNumber+"' AND bk.ticket = st.ticket AND bk.idNum = st.idNum";
    	List listStorageList= this.getSession().createSQLQuery(storageList)
    	.addScalar("what",Hibernate.TEXT)
		.addScalar("description",Hibernate.STRING)
		.addScalar("locationId",Hibernate.STRING)
		.addScalar("pieces",Hibernate.INTEGER)
		.addScalar("containerId",Hibernate.STRING)
		.addScalar("itemTag",Hibernate.STRING)
		.addScalar("oldLocation",Hibernate.STRING)
		.addScalar("id",Hibernate.LONG)
		.addScalar("measQuantity",Hibernate.BIG_DECIMAL)
		.addScalar("unit",Hibernate.STRING)
		.addScalar("volume",Hibernate.BIG_DECIMAL)
		.addScalar("volUnit",Hibernate.STRING)
		.addScalar("originalPieces",Hibernate.STRING)
		.addScalar("originalMeasQuantity",Hibernate.STRING)
		.addScalar("originalVolume",Hibernate.STRING)
		.addScalar("releaseDate",Hibernate.DATE)
		.list();
    	
    	Iterator it=listStorageList.iterator();
		while(it.hasNext()){
	           Object []row= (Object [])it.next();
	           StroageDTO stroageDTO=new StroageDTO();
	           if(row[0] == null){
	        	   stroageDTO.setWhat("");  
	           }else{
	        	   stroageDTO.setWhat(row[0].toString()); 
	           }
	           if(row[1] == null){
	        	   stroageDTO.setDescription("");  
	           }else{
	        	   stroageDTO.setDescription(row[1].toString());
	           }
	           if(row[2] == null){
	        	   stroageDTO.setLocationId("");  
	           }else{
	        	   stroageDTO.setLocationId(row[2].toString());
	           }
	           if(row[3] == null){
	        	   stroageDTO.setPieces(0);  
	           }else{
	        	   stroageDTO.setPieces(row[3].toString()); 
	           }
	           if(row[4] == null){
	        	   stroageDTO.setContainerId("");  
	           }else{
	        	   stroageDTO.setContainerId(row[4].toString());
	           }
	           if(row[5] == null){
	        	   stroageDTO.setItemTag("");  
	           }else{
	        	   stroageDTO.setItemTag(row[5].toString());
	           }
	           if(row[6] == null){
	        	   stroageDTO.setOldLocation("");  
	           }else{
	        	   stroageDTO.setOldLocation(row[6].toString());
	           }
	           if(row[7] == null){
	        	   stroageDTO.setId("");  
	           }else{
	        	   stroageDTO.setId(Long.parseLong(row[7].toString()));
	           }
	           if(row[8] == null){
	        	   stroageDTO.setMeasQuantity("");  
	           }else{
	        	   stroageDTO.setMeasQuantity(row[8].toString());
	           }
	           if(row[9] == null){
	        	   stroageDTO.setUnit("");  
	           }else{
	        	   stroageDTO.setUnit(row[9].toString());
	           }
	           if(row[10] == null){
	        	   stroageDTO.setVolume("");  
	           }else{
	        	   stroageDTO.setVolume(row[10].toString());
	           }
	           if(row[11] == null){
	        	   stroageDTO.setVolUnit("");  
	           }else{
	        	   stroageDTO.setVolUnit(row[11].toString());
	           }
	           if(row[12] == null){
	        	   stroageDTO.setOriginalPieces(0);  
	           }else{
	        	   stroageDTO.setOriginalPieces(row[12].toString());
	           }
	           if(row[13] == null){
	        	   stroageDTO.setOriginalMeasQuantity(0);  
	           }else{
	        	   stroageDTO.setOriginalMeasQuantity(row[13].toString());
	           }
	           if(row[14] == null){
	        	   stroageDTO.setOriginalVolume(0);  
	           }else{
	        	   stroageDTO.setOriginalVolume(row[14].toString());
	           }
	           
	         if(stroageDTO.getWhat().equals("R"))stroageDTO.setReleaseDate((Date) row[15]);
	        
	         if(Double.parseDouble(stroageDTO.getOriginalVolume().toString())>0 && !stroageDTO.getWhat().equals("R")){
	        	   stroageDTO.setVolume(stroageDTO.getOriginalVolume());
	           }
	           if(Double.parseDouble(stroageDTO.getOriginalMeasQuantity().toString())>0 && !stroageDTO.getWhat().equals("R")){
	        	   stroageDTO.setMeasQuantity(stroageDTO.getOriginalMeasQuantity());
	           }
	           if(Double.parseDouble(stroageDTO.getOriginalPieces().toString())>0 && !stroageDTO.getWhat().equals("R")){
	        	   stroageDTO.setPieces(stroageDTO.getOriginalPieces());
	           }
	           list1.add(stroageDTO);
	       }
		return list1;
    }
	
	public class StroageDTO{
		private Object what; 
		private Object description;
		private Object locationId;
		private Object pieces;
		private Object containerId;
		private Object itemTag;
		private Object oldLocation;
		private Object id;
		private Object measQuantity;
		private Object unit;
		private Object volume;
		private Object volUnit;
		private Object storageId;
		private Object oldStorage;
		private Object originalPieces;
		private Object originalMeasQuantity;
		private Object originalVolume;
		private Date releaseDate;
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getContainerId() {
			return containerId;
		}
		public void setContainerId(Object containerId) {
			this.containerId = containerId;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getItemTag() {
			return itemTag;
		}
		public void setItemTag(Object itemTag) {
			this.itemTag = itemTag;
		}
		public Object getLocationId() {
			return locationId;
		}
		public void setLocationId(Object locationId) {
			this.locationId = locationId;
		}
		public Object getOldLocation() {
			return oldLocation;
		}
		public void setOldLocation(Object oldLocation) {
			this.oldLocation = oldLocation;
		}
		public Object getPieces() {
			return pieces;
		}
		public void setPieces(Object pieces) {
			this.pieces = pieces;
		}
		public Object getWhat() {
			return what;
		}
		public void setWhat(Object what) {
			this.what = what;
		}
		public Object getMeasQuantity() {
			return measQuantity;
		}
		public void setMeasQuantity(Object measQuantity) {
			this.measQuantity = measQuantity;
		}
		public Object getUnit() {
			return unit;
		}
		public void setUnit(Object unit) {
			this.unit = unit;
		}
		public Object getVolume() {
			return volume;
		}
		public void setVolume(Object volume) {
			this.volume = volume;
		}
		public Object getVolUnit() {
			return volUnit;
		}
		public void setVolUnit(Object volUnit) {
			this.volUnit = volUnit;
		}
		public Object getStorageId() {
			return storageId;
		}
		public void setStorageId(Object storageId) {
			this.storageId = storageId;
		}
		public Object getOldStorage() {
			return oldStorage;
		}
		public void setOldStorage(Object oldStorage) {
			this.oldStorage = oldStorage;
		}
		public Object getOriginalPieces() {
			return originalPieces;
		}
		public void setOriginalPieces(Object originalPieces) {
			this.originalPieces = originalPieces;
		}
		public Object getOriginalMeasQuantity() {
			return originalMeasQuantity;
		}
		public void setOriginalMeasQuantity(Object originalMeasQuantity) {
			this.originalMeasQuantity = originalMeasQuantity;
		}
		public Object getOriginalVolume() {
			return originalVolume;
		}
		public void setOriginalVolume(Object originalVolume) {
			this.originalVolume = originalVolume;
		}
		public Date getReleaseDate() {
			return releaseDate;
		}
		public void setReleaseDate(Date releaseDate) {
			this.releaseDate = releaseDate;
		}
			
	}	
}  