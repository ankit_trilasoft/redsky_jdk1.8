package org.appfuse.webapp.action;


import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContext;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.providers.UsernamePasswordAuthenticationToken;
import org.apache.commons.lang.time.DateUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.dao.UserDao;
import org.appfuse.dao.hibernate.SignUpServiceDaoHibernate.DTO;
import org.appfuse.dao.hibernate.SignUpServiceDaoHibernate.UserInfoDTO;
import org.appfuse.model.Address;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.SignUpService;
import org.appfuse.service.UserExistsException;
import org.appfuse.service.UserManager;
import org.appfuse.util.StringUtil;
import org.appfuse.webapp.util.RequestUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.dto.HibernateDTO;
import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.CustomerSurveyFeedback;
import com.trilasoft.app.model.DataSecuritySet;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SurveyAnswerByUser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.CustomerSurveyFeedbackManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.GenericSurveyQuestionManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.SurveyAnswerByUserManager;
import com.trilasoft.app.webapp.action.CommonUtil;
public class SignupAction extends BaseAction implements Preparable {
    private static final long serialVersionUID = 6558317334878272308L;
    private User user;
    private UserManager userManager;
    private CustomerSurveyFeedback customerSurveyFeedback;
    private CustomerSurveyFeedbackManager customerSurveyFeedbackManager; 
    private String cancel;
    private String list;
    private String email;
    private String fName;
    private String lName;
    private String target;
    private String username;
    private String usernamePersist;
    private String hintQuestion;
    private String hintAns;
    private String hintQues1;
    private List emailList;
    private List usernameHintList;
    private List corpIdList;
    private List corpIdUserList;
    private List<SystemDefault> sysDefEmailList;
    private int countVar=0;
    private int maxAttempt;
    private String enbState;
    private String bucket2; 
    private List states;
    String passwordNew;
    String confirmPasswordNew;
    private SignUpService signUpService;
    private EmailSetupManager emailSetupManager;
    private String mobileNumber;

	private List CustomerFeedbackStatus=new ArrayList();
	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}
	public void setCancel(String cancel) { 
        this.cancel = cancel;
    }
    /**
	 * @param list the list to set
	 */
	public void setList(String list) {
		this.list = list;
	}
    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public String execute() {
        if (cancel != null) {
            return CANCEL;
        }
        if (list != null) {
            return "list";
        }
        if (ServletActionContext.getRequest().getMethod().equals("GET")) {
            return INPUT;
        }
        return SUCCESS;
    }

    public String doDefault() {
        return INPUT;
    }
    public void prepare() throws Exception {
    states=signUpService.findStateService( bucket2 , sessionCorpID);
    enbState = signUpService.serviceStateList(sessionCorpID);
	     
	}
	
	private String name;
	private String eMail;
	private String hintQues;
	private String hintQuesreset;
	private String hintQues1reset;
	private String hintQuestion1;
	private String corpIdPwd1;
	private String corpIdPwd;
	private String systemDefaultEmail;
	private String nameOfCompany;
	private String key; 
	private String accountMessage;
	String key1 = "Your username and password are begin sent to your e-mail address. This could take up to 15 minutes.";
	String key2 = "Invalid username.";
    public String save() throws Exception {
    	if(target.equalsIgnoreCase("pwd1")){
    		usernameHintList=signUpService.customerHintQues(username);  
    		if(!usernameHintList.isEmpty() && (usernameHintList!=null)){
    			Iterator itsss = usernameHintList.iterator();
    			while (itsss.hasNext()) {
    			Object expt = (Object) itsss.next();
    			((DTO) expt).getDescription();
    			((DTO) expt).getPasswordHintQues();
    			String key11="";
    			boolean flag=false;
          		if(!((DTO) expt).getAccount_enabled()){
          			 key11="Your account is not enabled.";
          			flag=true;       		
          		}if(((DTO) expt).getAccount_expired()){
          			if(flag){
          			 key11=key11+"<br>Your account is expired.";
          			}else{
          			key11="Your account is expired.";
          			flag=true;
          			}
          		} if(((DTO) expt).getAccount_locked()){
          			if(flag){
          			key11=key11+"<br>Your account is locked.";
          			}else{
          			key11="Your account is locked.";
          			flag=true; 
          			}
          		} if(((DTO) expt).getCredentials_expired()){
          			if(flag){
          			key11=key11+"<br>Your credentials is expired.";
          			}else{
          			key11="Your credentials is expired.";
          			flag=true; 
          			}
          		} 
          		if(flag){
          			saveMessage(getText(key11)+"Please contact<br><a href='mailto:support@redskymobility.com'>support@redskymobility.com</a>");
          			return INPUT;
          		}else{
    			if(!((DTO) expt).getPasswordHintQues().equals("")){
    				hintQuesreset=	((DTO) expt).getDescription().toString();
        			hintQues1reset= ((DTO) expt).getPasswordHintQues().toString();
        		   hintQues=((DTO) expt).getDescription().toString();
        		   hintQues1=((DTO) expt).getPasswordHintQues().toString();
        		   target="pwd";
        		  usernamePersist = username;
        		  hintQuesreset=hintQues;
        		  hintQues1reset=hintQues1;
        		  countVar=0;
        		   return "HINTQUES";
    			}
    			else{
    				emailList=signUpService.customerForgotEmail(username);      	
    	          	if(!(emailList.isEmpty()) && emailList != null){
    	          //	eMail = emailList.get(0).toString();
    	          	UserInfoDTO userDTO=(UserInfoDTO)emailList.get(0);
              		 //flag=false;
              		//String key11="";
              		if(!userDTO.getAccount_enabled()){
              			 key11="Your account is not enabled.";
              			flag=true;       		
              		}if(userDTO.getAccount_expired()){
              			if(flag){
              			 key11=key11+"<br>Your account is expired.";
              			}else{
              			key11="Your account is expired.";
              			flag=true; 
              			}
              		} if(userDTO.getAccount_locked()){
              			if(flag){
              			key11=key11+"<br>Your account is locked.";
              			}else{
              			key11="Your account is locked.";
              			flag=true; 
              			}
              		} if(userDTO.getCredentials_expired()){
              			if(flag){
              			 key11=key11+"<br>Your credentials is expired.";
              			}else{
              				 key11="Your credentials is expired.";
              			flag=true;
              			}
              		} 
              		if(flag){
              			saveMessage(getText(key11+"Please contact<br><a href='mailto:support@redskymobility.com'>support@redskymobility.com</a>"));
              			return INPUT;
              		}else{
              			eMail=userDTO.getUserInfo();
    	        	String str=new  String("QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR");
    	    	     StringBuffer sb=new StringBuffer();
    	    	     Random r = new Random();
    	    	     int te=0;
    	    	     for(int i=1;i<=8;i++){
    	    	         te=r.nextInt(62);
    	    	         sb.append(str.charAt(te));
    	    	     }
    	        	getConfiguration().get(Constants.ENCRYPT_PASSWORD);
    	        	String algorithm = (String)
    	        	getConfiguration().get(Constants.ENC_ALGORITHM);
    	        	if (algorithm == null) { // should only happen for test case
    	    			  log.debug("assuming testcase, setting algorithm to 'SHA'");
    	    			  algorithm = "SHA"; 
    	    		}
    	        	String passwordNew = StringUtil.encodePassword(sb.toString(),algorithm);
    	        	confirmPasswordNew=passwordNew;
    	        	corpIdList=signUpService.customerCorpID(username);
    	        	corpIdPwd=corpIdList.get(0).toString();
    	        	sysDefEmailList=signUpService.supportEmail(corpIdPwd);
    	        	if(sysDefEmailList!=null && !(sysDefEmailList.isEmpty())){
    	        		for (SystemDefault systemDefault : sysDefEmailList) {
    	        	systemDefaultEmail=systemDefault.getEmail();
    	        	nameOfCompany=systemDefault.getCompany();
    	        	}
    	        	}
    	        	//signUpService.resetPassword(passwordNew, confirmPasswordNew, username);
    	        	/*try{
    	        		sendCustomerMail(eMail,username,sb.toString(),corpIdPwd,systemDefaultEmail,nameOfCompany);
    	        		}
    	        		catch(MessagingException mex)
    	        		{
    	        			mex.printStackTrace();
    	        		}*/
    	        		saveMessage(getText(key1));
    	        		return "SUCCESSMAIL";
    	          	}
    	          	}
    			}
          		}
    		}
    		}
    		else{
    			saveMessage(getText(key2));
    			return INPUT;
    		}
    	}
    	if(target.equalsIgnoreCase("pwd")){
    		emailList=signUpService.customerEmail(username,hintQuestion,hintAns,target);      	
          	if(!(emailList.isEmpty()) && emailList != null){
          		UserInfoDTO userDTO=(UserInfoDTO)emailList.get(0);
          		boolean flag=false;
          		String key11="";
          		if(!userDTO.getAccount_enabled()){
          			 key11="Your account is not enabled.";
          			flag=true;       		
          		}if(userDTO.getAccount_expired()){
          			if(flag){
          			 key11=key11+"<br>Your account is expired.";
          			}else{
          			key11="Your account is expired.";
          			flag=true; 
          			}
          		} if(userDTO.getAccount_locked()){
          			if(flag){
          			key11=key11+"<br>Your account is locked.";
          			}else{
          			key11="Your account is locked.";
          			flag=true; 
          			}
          		} if(userDTO.getCredentials_expired()){
          			if(flag){
          			 key11=key11+"<br>Your credentials is expired.";
          			}else{
          			 key11="Your credentials is expired.";
          			flag=true; 
          			}
          		} 
          		if(flag){
          			saveMessage(getText(key11)+"Please contact<br><a href='mailto:support@redskymobility.com'>support@redskymobility.com</a>");
          			return INPUT;
          		}else{
          	eMail = userDTO.getUserInfo();       	
        	String str=new  String("QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR");
    	     StringBuffer sb=new StringBuffer();
    	     Random r = new Random();
    	     int te=0;
    	     for(int i=1;i<=8;i++){
    	         te=r.nextInt(62);
    	         sb.append(str.charAt(te));
    	     }
        	getConfiguration().get(Constants.ENCRYPT_PASSWORD);
        	String algorithm = (String)
        	getConfiguration().get(Constants.ENC_ALGORITHM);
        	if (algorithm == null) { // should only happen for test case
    			  log.debug("assuming testcase, setting algorithm to 'SHA'");
    			  algorithm = "SHA"; 
    		}
        	String passwordNew = StringUtil.encodePassword(sb.toString(),algorithm);
        	confirmPasswordNew=passwordNew;
        	corpIdList=signUpService.customerCorpID(username);
        	corpIdPwd=corpIdList.get(0).toString();
        	sysDefEmailList=signUpService.supportEmail(corpIdPwd);
        	if(sysDefEmailList!=null && !(sysDefEmailList.isEmpty())){
        		for (SystemDefault systemDefault : sysDefEmailList) {
        	systemDefaultEmail=systemDefault.getEmail();
        	nameOfCompany=systemDefault.getCompany();
        	}
        	}
        	//signUpService.resetPassword(passwordNew, confirmPasswordNew, username);
        	/*try{
        		sendCustomerMail(eMail,username,sb.toString(),corpIdPwd,systemDefaultEmail,nameOfCompany);
        		}
        		catch(MessagingException mex)
        		{
        			mex.printStackTrace();
        		}*/
        		ServletContext sc =getRequest().getSession(true).getServletContext();
        		sc.setAttribute("globalMsg", key1);
        		//saveMessage(getText(key1));
        		return SUCCESS;
          		}
          	}
          	else{
          		target="pwd";
          		usernamePersist = username;
          		corpIdUserList=signUpService.customerCorpID(username);
            	corpIdPwd1=corpIdUserList.get(0).toString();
            	maxAttempt=Integer.parseInt(signUpService.maxAttempt(corpIdPwd1).get(0).toString());
          		hintQues=hintQuestion1;
        		hintQues1=hintQuestion;
        		String key3="Incorrect Information. Please try again.";
        		saveMessage(key3);
        		return INPUT;
        		/*if(maxAttempt>countVar){
        		String key3="Incorrect Information. Please try again.";
        		int i=maxAttempt-countVar;
        		String key4="You are left with "+i+" more attempts.";
        		countVar=countVar+1;
        		saveMessage(key3+key4);
        		return INPUT;
        		}
        		else{
        			accountMessage="You have exceeded the max. no. of atmpts. Login is disabled. Please contact support@redskymobility.com";
        			signUpService.resetAccount(username);
        			saveMessage(accountMessage);
        			return "ACCOUNTRESET";
        		}*/
                
          	}
    		
    	}
   /* 	if(target.equalsIgnoreCase("upwd")){
    	
      	emailList=signUpService.customerEmail(email,fName,lName,target);      	
      	if(!(emailList.isEmpty()) && emailList != null){
      		UserInfoDTO userDTO=(UserInfoDTO)emailList.get(0);
      		boolean flag=false;
      		String key11="";
      		if(!userDTO.getAccount_enabled()){
      			 key11="Your account is not enabled.";
      			flag=true;       		
      		}if(userDTO.getAccount_expired()){
      			if(flag){
      			 key11=key11+"<br>Your account is expired.";
      			}else{
      				key11="Your account is expired.";
      			    flag=true; 
      			}
      		} if(userDTO.getAccount_locked()){
      			if(flag){
      				key11=key11+"<br>Your account is locked.";
      			}else{
      			key11="Your account is locked.";
      			flag=true; 
      			}
      		} if(userDTO.getCredentials_expired()){
      			if(flag){
      				key11=key11+"<br>Your credentials is expired.";
      			}else{
      			 key11="Your credentials is expired.";
      			flag=true; 
      			}
      		} 
      		if(flag){
      			saveMessage(getText(key11)+"Please contact<br><a href='mailto:support@redskymobility.com'>support@redskymobility.com</a>");
      			return INPUT;
      		}else{
      			name = userDTO.getUserInfo();      	    	
      	    	String str=new  String("QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR");
      		     StringBuffer sb=new StringBuffer();
      		     Random r = new Random();
      		     int te=0;
      		     for(int i=1;i<=8;i++){
      		         te=r.nextInt(62);
      		         sb.append(str.charAt(te));
      		     }
      	    	getConfiguration().get(Constants.ENCRYPT_PASSWORD);
      	    	String algorithm = (String)
      	    	getConfiguration().get(Constants.ENC_ALGORITHM);
      	    	if (algorithm == null) { // should only happen for test case
      				  log.debug("assuming testcase, setting algorithm to 'SHA'");
      				  algorithm = "SHA"; 
      			}
      	    	String passwordNew = StringUtil.encodePassword(sb.toString(),algorithm);
      	    	confirmPasswordNew=passwordNew;
      	    	corpIdUserList=signUpService.customerCorpID(name);
      	    	corpIdPwd1=corpIdUserList.get(0).toString();
      	    	sysDefEmailList=signUpService.supportEmail(corpIdPwd1);
      	    	if(sysDefEmailList!=null && !(sysDefEmailList.isEmpty())){
      	    		for (SystemDefault systemDefault : sysDefEmailList) {
      	    	systemDefaultEmail=systemDefault.getEmail();
      	    	nameOfCompany=systemDefault.getCompany();
      	    	}
      	    	}
      	    	signUpService.resetPassword(passwordNew, confirmPasswordNew, name);
      	    	try{
      	    		sendCustomerMail(email,name,sb.toString(),corpIdPwd1,systemDefaultEmail,nameOfCompany);
      	    		}
      	    		catch(MessagingException mex)
      	    		{
      	    			mex.printStackTrace();
      	    		}
      	    		ServletContext sc =getRequest().getSession(true).getServletContext();
            		sc.setAttribute("globalMsg", key1);
      	    		//saveMessage(getText(key1));
      	    		return SUCCESS;	
      		}
    	
      	}
      	else{
      	if(countVar<=2){
    		String key3="Incorrect Information. Please try again.";
    		countVar=countVar+1;
    		saveMessage(key3);
    		//System.out.println("\n\n\n\n\n\n\n\n\n\n>>>>>>>>>>>>> Variable count"+countVar);
    		return INPUT;
    		}
    		else{
    			key="Incorrect Information. Please contact";
    			target="upwd";
    			countVar=0;
    			return INPUT;
    		}
      	}
    	}*/
    	else{
    		return INPUT;
    	}
    }
    @SkipValidation
	public String sendCustomerMail(String email,String name, String newPassword,String corpId,String supportEmail,String companyName) throws AddressException, MessagingException {
		try {   
		        String msgText1= "\n\nDear User,\n";
		        msgText1=msgText1+"\nBased on your request we have reset your User Name and Password details to the RedSky Move Management System.\n";
		        msgText1=msgText1+ "\nYour login details are as follows:\n";
			    msgText1=msgText1+"\nUser ID : "+name+"\n";
			    msgText1= msgText1+ "Password: "+newPassword+"\n";
			    msgText1= msgText1+ "\nUpon log in you will be prompted to change your password.\n" +
			    		"Please change it to a secure password that you will be able to remember.";
			    msgText1= msgText1+ "\n\nYou can access RedSky at:  http://www.skyrelo.com.\n";
			    msgText1= msgText1+ "\nIf you have any questions, please contact us at "+supportEmail+".\n";
			    msgText1=msgText1+"\n\nRegards,\n";
			    msgText1=msgText1+"\nRedSky Support Team\n";
			    String subject = "RedSky User Information";
                String from = "support@redskymobility.com"; 
               
                emailSetupManager.globalEmailSetupProcess(from, email, "", "", "", msgText1, subject, sessionCorpID,"",name,"");
                
        } 
		catch(Exception ex)
		{
			ex.printStackTrace();
        }
       return SUCCESS; 
    }
    private Partner partner;
    private Map<String, String> ocountry;
    private Map<String, String> ostates;
    private String userCountry;
    private String userState;
    private String userBranch;
    private String userLastName;
    private String userFirstName;
    private String userAlias;
    private String userJobType;
    private String userJobType1;
    private String userPhoneNumber;
    private String userFaxNumber;
    private String userEmail;
    private String phoneType1;
    private String phoneType2;
    private String partnerCode;
    private List jobFunction;
    
     @SkipValidation
    public String partnerRequestAccess(){
    	ocountry = signUpService.findCountry("COUNTRY");
    	jobFunction=signUpService.getJobFunction("JOBFUNCTION");
    	states=signUpService.findStateService(bucket2 , sessionCorpID);
    	enbState = signUpService.serviceStateList(sessionCorpID);
    	user= new User();
    	getSession().setAttribute("availableJobTypes", jobFunction);
        return SUCCESS; 
    }
    private String message;
    private String oldPortalId;
    private String portalIdCount;
    private List newPortalId;
    private String portalId;
    private List poratalIdCountList;
    private String userPortalId;
    private String userTitle;
    @SkipValidation
    public String savePartnerRequestAccess(){
    	ocountry = signUpService.findCountry("COUNTRY");
    	jobFunction=signUpService.getJobFunction("JOBFUNCTION");
    	if(sessionCorpID !=null && (!(sessionCorpID.trim().equals("")))){
    		
    	}else{
    		sessionCorpID="TSFT";
    	}
    	ostates=signUpService.findState("USA", sessionCorpID);
    	boolean isNew=(user==null && user.getId()==null);
    	
    	List checkUser=signUpService.checkUser(userEmail, sessionCorpID);
    	if(isNew && !checkUser.isEmpty())
    	{
    		accountMessage="User already exist with this email Id. Please change the email address.";
			saveMessage(accountMessage);
			return INPUT;
    	}
    	else{
    	List partnerList = signUpService.getPartner(partnerCode, sessionCorpID);
    	if(partnerList!=null && (!(partnerList.isEmpty()))){
    		partner=(Partner)partnerList.get(0);
	    	String[] job = getRequest().getParameterValues("userJobType");
	    	StringBuilder result = new StringBuilder();
	    	if (job.length > 0) {
	            result.append(job[0]);
	            for (int i=1; i < job.length; i++) {
	                result.append(",");
	                result.append(job[i]);
	            }
	        }
	    	user.setFunctionalArea(result.toString()); 
    	}

    	Address address = new Address();
    	address.setAddress(partner.getTerminalAddress1());
    	address.setCity(partner.getTerminalCity());
		address.setPostalCode(partner.getTerminalZip());
		address.setProvince(partner.getTerminalState());
    	address.setCountry(userCountry);
		user.setAddress(address);
    	user.setCorpID(sessionCorpID);
    	user.setFirstName(userFirstName);
    	user.setLastName(userLastName);
    	user.setAlias(userAlias);
    	user.setBranch(partnerCode);
    	user.setParentAgent(partnerCode);
    	user.setWebsite(partner.getUrl());
    	user.setDefaultSearchStatus(true);
        user.setPhoneNumber(userPhoneNumber);
        user.setMobileNumber(mobileNumber);
    	Role role=signUpService.getRole("ROLE_AGENT");
    	user.addRole(signUpService.getRole("ROLE_AGENT"));
    	user.addRole(signUpService.getRole("ROLE_USER"));
    	//user.setFaxNumber(userFaxNumber);
    	user.setEmail(userEmail.trim());
    	user.setUserTitle(userTitle);
    	oldPortalId = userFirstName.substring(0, 1).toLowerCase().trim() + userLastName.toLowerCase().trim();
		oldPortalId = oldPortalId.replace("'", "");
		oldPortalId = oldPortalId.replaceAll(" ", "");
		newPortalId = signUpService.findPortalId(oldPortalId);
		portalIdCount = newPortalId.get(0).toString();
			if (portalIdCount.equals("0")) {
				portalIdCount = "";
			}
			portalId = oldPortalId + portalIdCount;
			boolean userExists = signUpService.findUser(portalId);
			
			if (userExists) {
				if (portalIdCount.equals("")) {
					portalIdCount = "0";
				}
				portalIdCount = (new Long(Long.parseLong(portalIdCount) + 1)).toString();
				portalId = oldPortalId + portalIdCount;
			}
		
		poratalIdCountList = signUpService.findCustomerPortalListDetails(oldPortalId);
		if(!poratalIdCountList.isEmpty()){
			  Iterator it= poratalIdCountList.iterator();
				while(it.hasNext()){
					Object  row =(Object)it.next();
					
					if(row.equals(portalId)){
						portalIdCount = (new Long(Long.parseLong(portalIdCount) + 1)).toString();
						userPortalId = oldPortalId + portalIdCount;
						portalId=userPortalId;
					}
				}
				
		}
		user.setUsername(portalId.replaceAll(" ", "").trim());
    	String str=new  String("QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR");
	     StringBuffer sb=new StringBuffer();
	     Random r = new Random();
	     int te=0;
	     for(int i=1;i<=8;i++){
	         te=r.nextInt(62);
	         sb.append(str.charAt(te));
	     }
		   	getConfiguration().get(Constants.ENCRYPT_PASSWORD);
		   	String algorithm = (String)
		   	getConfiguration().get(Constants.ENC_ALGORITHM);
		   	if (algorithm == null) { // should only happen for test case
					  log.debug("assuming testcase, setting algorithm to 'SHA'");
					  algorithm = "SHA"; 
				}
		   	String passwordNew = StringUtil.encodePassword(sb.toString(),algorithm);
		   	//String passwordNew = StringUtil.encodePassword(portalId.replaceAll(" ", ""),algorithm);
		   	confirmPasswordNew=passwordNew;
		   	user.setPassword(passwordNew);
		   	user.setConfirmPassword(passwordNew);
		   	
		   	user.setPwdexpiryDate(DateUtils.addDays(getCurrentDate(), 90));
		   	user.setWorkStartTime("00:00");
			user.setWorkEndTime("00:00");
			user.setCreatedOn(new Date());
			user.setUpdatedOn(new Date());
			user.setCreatedBy("Partner Request");
			user.setUpdatedBy("Partner Request");
			user.setAccountExpired(false);
			user.setAccountLocked(false);
			user.setCredentialsExpired(false);
			user.setEnabled(false);
			user.setUserType("AGENT");
			user.setPasswordHintQues(" Default");
			user.setPasswordHint("Default");
			user.setPasswordReset(true);
			user.setSupervisor("");
			signUpService.saveUser(user);
			/*try {
				systemDefaultEmail="redsky@sscw.com";
				sysDefEmailList=signUpService.supportEmail(sessionCorpID);
				if(sysDefEmailList!=null && !(sysDefEmailList.isEmpty())){
	        		for (SystemDefault systemDefault : sysDefEmailList) {
	        	systemDefaultEmail=systemDefault.getAgentIdRegistrationDistribution();
	        	}
	        	}	
				
				String from = "support@redskymobility.com";
			
				String subject="User has Registered for partner portal";
				String messageText1 = "\nemail:- "+userEmail;
				messageText1 = messageText1 +"\nPartner code:- "+partnerCode;
				messageText1 = messageText1 +"\nLast Name:- "+userLastName;
				messageText1 = messageText1 +"\nFirst Name:- "+userFirstName;
				messageText1 = messageText1 +"\n"+phoneType1+":-"+userPhoneNumber;
				messageText1 = messageText1 +"\n"+phoneType2+":- "+userFaxNumber;
				messageText1 = messageText1 +"\nUser Name:- "+portalId.replaceAll(" ", "");
				messageText1 = messageText1 +"\nJob Function:- "+userJobType;
				messageText1 = messageText1 +"\nJob Title:- "+userTitle;
				
				 emailSetupManager.globalEmailSetupProcess(from, systemDefaultEmail, "", "", "", messageText1, subject, sessionCorpID,"","","");
				
			} catch (Exception ex) {

			}*/
			accountMessage="Request has been submitted successfully";
			saveMessage(accountMessage);
    }
   	 	return SUCCESS;
   	 	
    }
    @SkipValidation
    public String findStateServiceList(){
    	states=signUpService.findStateService( bucket2 , sessionCorpID);
    	return SUCCESS;	
    }
    private String rtNumber;
    private String  soId;
    private String notesAddForServices="";
    private String MMGServices="";
 
    @SkipValidation
    public String customerSurveyAccess(){
    	//System.out.print("Check portal");
    	if(surveyJob.equalsIgnoreCase("RLO")){
    	//CustomerFeedbackStatus.add("soId");
    		if(services!=null && !services.equals("") && services.equals("MMG")){
    			int counterMMG=0;
    			List CheckForRlo=signUpService.checkRLOCustomerFeedback(soId,sessionCorpID);
    			for (Object object : CheckForRlo) {
    			if(object.toString().trim().equalsIgnoreCase("MMG")){
    				counterMMG++;	
    				}
				}
    			if(MMGServices.equalsIgnoreCase("Destination") && counterMMG==1){
    				notesAddForServices=services;
    				
    			}else if(MMGServices.equalsIgnoreCase("Origin") && counterMMG==0){
    				notesAddForServices=services;
    			}else{
    				CustomerFeedbackStatus.addAll(CheckForRlo);
    			}
    		}else{
    		int counter=0;
    		  String CustomerFeedback="";
    	List CheckForRlo=signUpService.checkRLOCustomerFeedback(soId,sessionCorpID);
    	for (Object object : CheckForRlo) {
    		if(CustomerFeedback.equalsIgnoreCase("")){
    			CustomerFeedback=object.toString();
			}else{
				CustomerFeedback=CustomerFeedback+","+object.toString();
			}
    	}
    	  if(services!=null && !services.equals("")){
    		  String serviceTypeDetails[]=services.split(",");	 
    		  for (String serviceTemp: serviceTypeDetails) {
    		//int checkPosition= CustomerFeedback.indexOf(serviceTemp);
    		boolean checkPosition= CustomerFeedback.contains(serviceTemp);
    			if(!checkPosition){
    				counter++;
    				if(notesAddForServices.equalsIgnoreCase("")){
    					notesAddForServices=serviceTemp;
    				}else{
    					notesAddForServices=notesAddForServices+","+serviceTemp;
    				}
    			}
    		  }
    	  }
        if(counter!=0){
    	  
         }else{
        	 notesAddForServices=services;
    	  CustomerFeedbackStatus.addAll(CheckForRlo); 
         }
		
    		}
    		}else{
    	CustomerFeedbackStatus=signUpService.checkCustomerFeedback(soId,sessionCorpID);	
    	}    
    	return SUCCESS;
    }
    
    private String counselor;
    private String oaName;
    private String daName;
    private String customerName;
    private String evaluationDate;
    private String shipNumber;
    private String postFeedBack;
    @SkipValidation
    public String customerPostMoveSurveyAccess(){
    	List orderDetail = signUpService.getPostMoveOrderDetails(soId, sessionCorpID) ;
    	Iterator it = orderDetail.iterator();
    	while(it.hasNext()){
    		Object obj[] = (Object[]) it.next();
    		//System.out.println("Name>>"+obj[0]);
    		//System.out.println("Coord>>"+obj[1]);
    		//System.out.println("Order>>"+obj[2]);
    		//System.out.println("OA>>"+obj[3]);
    		//System.out.println("DA>>"+obj[4]);
    		//System.out.println("DA>>"+obj[6]);
    		String coordName = signUpService.getUserDetails(obj[1].toString(), sessionCorpID).get(0).toString().replace("#", " ");
    		//System.out.println("CoordName>>"+coordName);
    		customerName =obj[0].toString();
    		counselor = coordName;
    		oaName = obj[3].toString();
    		daName = obj[4].toString();
    		evaluationDate = obj[5].toString();
    		shipNumber=obj[2].toString();
    		if(obj[6]!=null){
    			postFeedBack="Y";
    		}else{
    			postFeedBack="N";
    		}
    	}
    	return SUCCESS;
    }
    private String custService;
    private String originAgent;
    private String destinAgent;
    private String overall;
    private String recommend;
    private String comment1;
    private String comment2;
    private String comment3;
    private String comment4;
    private String comment5;
    private String comment6;
    private String comment7;
    private String contactPhone;
    private String contactEmail;
    @SkipValidation
    public String saveCustomerPostMoveRequestAccess(){
		customerSurveyFeedback = new CustomerSurveyFeedback();
		customerSurveyFeedback.setCorpID(sessionCorpID);
		customerSurveyFeedback.setCreatedOn(new Date());
		customerSurveyFeedback.setCreatedBy("Customer Feedback");
		customerSurveyFeedback.setUpdatedOn(new Date());
		customerSurveyFeedback.setUpdatedBy("Customer Feedback");
		customerSurveyFeedback.setShipNumber(shipNumber);
		customerSurveyFeedback.setServiceOrderId(Long.parseLong(soId));
		
		if(custService!=null)
			customerSurveyFeedback.setCouselorRating(Integer.parseInt(custService));
		else customerSurveyFeedback.setCouselorRating(3);
		customerSurveyFeedback.setCouselorFeedBack(comment1);
		
		if(originAgent!=null)
			customerSurveyFeedback.setOaRating(Integer.parseInt(originAgent));
		else customerSurveyFeedback.setOaRating(3);
		customerSurveyFeedback.setOaFeedBack(comment2);
		
		if(destinAgent!=null)
			customerSurveyFeedback.setDaRating(Integer.parseInt(destinAgent));
		else customerSurveyFeedback.setDaRating(3);
		customerSurveyFeedback.setDaFeedBack(comment3);
		
		if(overall!=null)
			customerSurveyFeedback.setCustomerRating(Integer.parseInt(overall));
		else customerSurveyFeedback.setCustomerRating(3);
		customerSurveyFeedback.setFeedBack(comment4);		
		
		if(recommend!=null)
			customerSurveyFeedback.setRecommendation(Integer.parseInt(recommend));
		else customerSurveyFeedback.setRecommendation(3);
		customerSurveyFeedback.setRecommendationFeedBack(comment5);
		
		customerSurveyFeedback.setAdditionalFeedBack(comment6);
		customerSurveyFeedback.setAdditionalSecondFeedBack(comment7);
		
		customerSurveyFeedbackManager.save(customerSurveyFeedback);
		signUpService.updateEmailReveived(soId,contactPhone,contactEmail,sessionCorpID,"");
		return SUCCESS;
    }
    
    private Long sid;
	private Map<List, Map> surveyUserList;
	private String sequenceNumber;
	private String addressDetails;
	private String userName;
	private String welcomeMsg;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private GenericSurveyQuestionManager genericSurveyQuestionManager;
	private RefMasterManager refMasterManager;
	public String getSurveyUserData(){
		try{
			if(sid!=null){
				List findSurveyDoneOrNotList = surveyAnswerByUserManager.getRecordsBySOId(sid);
				if(findSurveyDoneOrNotList!=null && !findSurveyDoneOrNotList.isEmpty() && findSurveyDoneOrNotList.get(0)!=null){
					ServiceOrder soObj = serviceOrderManager.get(sid);
					CustomerFile cfObj = signUpService.getCustomer(soObj.getCustomerFileId());
					List surveyDoneMsgList = refMasterManager.getDescriptionBylanguage("GENERICSURVEYSUBMITTEDMSG", cfObj.getCustomerLanguagePreference(),cfObj.getCorpID());
					if(surveyDoneMsgList!=null && !surveyDoneMsgList.isEmpty() && surveyDoneMsgList.get(0)!=null){
						returnAjaxStringValue = surveyDoneMsgList.get(0).toString();
					}
				}else{
					ServiceOrder soObj = serviceOrderManager.get(sid);
					String joType = soObj.getJob();
					String routingType = soObj.getRouting();
					sequenceNumber = soObj.getShipNumber();
					addressDetails = soObj.getOriginCity()+","+soObj.getOriginCountry()+"-"+soObj.getDestinationCity()+","+soObj.getDestinationCountry();
					userName = soObj.getFirstName()+" "+soObj.getLastName()+",";
					CustomerFile cfObj = signUpService.getCustomer(soObj.getCustomerFileId());
					List welcomeMsgList = refMasterManager.getDescriptionBylanguage("GENERICSURVEYWELCOMEMSG", cfObj.getCustomerLanguagePreference(),cfObj.getCorpID());
					if(welcomeMsgList!=null && !welcomeMsgList.isEmpty() && welcomeMsgList.get(0)!=null){
						welcomeMsg = welcomeMsgList.get(0).toString();
					}
					boolean isBookingAgent = false;
					boolean isOriginAgent = false;
					boolean isdestinationAgent = false;
					String bookingAgentVal = genericSurveyQuestionManager.checkForBA(sid,cfObj.getCorpID());
					if(bookingAgentVal!=null && !bookingAgentVal.equalsIgnoreCase("")){
						isBookingAgent = true;
					}
					String originAgentVal = genericSurveyQuestionManager.checkForOA(sid,cfObj.getCorpID());
					if(originAgentVal!=null && !originAgentVal.equalsIgnoreCase("")){
						isOriginAgent = true;
					}
					String destinationAgentVal = genericSurveyQuestionManager.checkForDA(sid,cfObj.getCorpID());
					if(destinationAgentVal!=null && !destinationAgentVal.equalsIgnoreCase("")){
						isdestinationAgent = true;
					}
					surveyUserList = genericSurveyQuestionManager.getSurveyUserList(joType,routingType,cfObj.getCorpID(),sid,cid,isBookingAgent,isOriginAgent,isdestinationAgent,cfObj.getCustomerLanguagePreference());
				}
			}else{
				List findSurveyDoneOrNotList = surveyAnswerByUserManager.getRecordsByCId(cid);
				if(findSurveyDoneOrNotList!=null && !findSurveyDoneOrNotList.isEmpty() && findSurveyDoneOrNotList.get(0)!=null){
					CustomerFile cfObj = customerFileManager.get(cid);
					List surveyDoneMsgList = refMasterManager.getDescriptionBylanguage("GENERICSURVEYSUBMITTEDMSG", cfObj.getCustomerLanguagePreference(),cfObj.getCorpID());
					if(surveyDoneMsgList!=null && !surveyDoneMsgList.isEmpty() && surveyDoneMsgList.get(0)!=null){
						returnAjaxStringValue = surveyDoneMsgList.get(0).toString();
					}
				}else{
					CustomerFile cfObj = customerFileManager.get(cid);
					String joType = cfObj.getJob();
					sequenceNumber = cfObj.getSequenceNumber();
					addressDetails = cfObj.getOriginCity()+","+cfObj.getOriginCountry()+"-"+cfObj.getDestinationCity()+","+cfObj.getDestinationCountry();
					userName = cfObj.getFirstName()+" "+cfObj.getLastName()+",";
					List welcomeMsgList = refMasterManager.getDescriptionBylanguage("GENERICSURVEYWELCOMEMSG", cfObj.getCustomerLanguagePreference(),cfObj.getCorpID());
					if(welcomeMsgList!=null && !welcomeMsgList.isEmpty() && welcomeMsgList.get(0)!=null){
						welcomeMsg = welcomeMsgList.get(0).toString();
					}
					boolean isBookingAgent = false;
					String bookingAgentVal = genericSurveyQuestionManager.checkForBA(sid,cfObj.getCorpID());
					if(bookingAgentVal!=null && !bookingAgentVal.equalsIgnoreCase("")){
						isBookingAgent = true;
					}
					surveyUserList = genericSurveyQuestionManager.getSurveyUserList(joType,"",cfObj.getCorpID(),sid,cid,isBookingAgent,false,false,cfObj.getCustomerLanguagePreference());
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	private String surveyByUserVal;
	private String returnAjaxStringValue;
	private SurveyAnswerByUserManager surveyAnswerByUserManager;
	private SurveyAnswerByUser surveyAnswerByUser;
	private CustomerFileManager customerFileManager;
	@SkipValidation
	public String saveSurveyByUser(){
		if(surveyByUserVal!=null && !surveyByUserVal.equalsIgnoreCase("")){
			try{
				String []tempVal = surveyByUserVal.split("@");
				for(String surveyVal:tempVal){
					String[] surveyValTemp = surveyVal.split("~");
					String surveyByUserAnswerIdVal="";
					String surveyByUserIdVal="";
					List answerIdList = new ArrayList();
					if(surveyValTemp[0].toString().contains("chk")){
						String surveyValueId = surveyValTemp[0].toString().substring(3);
						answerIdList = surveyAnswerByUserManager.getRecordsByQuestionId(Long.parseLong(surveyValueId),sid,cid);
						if(answerIdList!=null && !answerIdList.isEmpty() && answerIdList.get(0)!=null){
							String answerIdVal = answerIdList.get(0).toString();
							String [] surveyByUserAnswerAndIdVal = answerIdVal.split("~");
							surveyByUserIdVal = surveyByUserAnswerAndIdVal[0];
							surveyByUserAnswerIdVal = surveyByUserAnswerAndIdVal[1];
						}
					}
					if(surveyByUserAnswerIdVal==null || surveyByUserAnswerIdVal.equalsIgnoreCase("")){
						surveyAnswerByUser = new SurveyAnswerByUser();
						surveyAnswerByUser.setServiceOrderId(sid);
						surveyAnswerByUser.setCustomerFileId(cid);
						surveyAnswerByUser.setQuestionId(Long.parseLong(surveyValTemp[0].toString().substring(3)));
						surveyAnswerByUser.setAnswerId(surveyValTemp[1].toString());
						surveyAnswerByUser.setCreatedOn(new Date());
						surveyAnswerByUserManager.save(surveyAnswerByUser);
					}else{
						surveyByUserAnswerIdVal = surveyByUserAnswerIdVal+"#"+surveyValTemp[1];
						surveyAnswerByUser = surveyAnswerByUserManager.get(Long.parseLong(surveyByUserIdVal));
						surveyAnswerByUser.setAnswerId(surveyByUserAnswerIdVal);
						surveyAnswerByUserManager.save(surveyAnswerByUser);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		try{
			if(sid!=null){
				ServiceOrder soObj = serviceOrderManager.get(sid);
				CustomerFile cfObj = customerFileManager.get(soObj.getCustomerFileId());
				List welcomeMsgList = refMasterManager.getDescriptionBylanguage("GENERICSURVEYTHANKSMSG", cfObj.getCustomerLanguagePreference(),cfObj.getCorpID());
				if(welcomeMsgList!=null && !welcomeMsgList.isEmpty() && welcomeMsgList.get(0)!=null){
					returnAjaxStringValue = welcomeMsgList.get(0).toString();
				}
			}else{
				CustomerFile cfObj = customerFileManager.get(cid);
				List welcomeMsgList = refMasterManager.getDescriptionBylanguage("GENERICSURVEYTHANKSMSG", cfObj.getCustomerLanguagePreference(),cfObj.getCorpID());
				if(welcomeMsgList!=null && !welcomeMsgList.isEmpty() && welcomeMsgList.get(0)!=null){
					returnAjaxStringValue = welcomeMsgList.get(0).toString();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
    
    @SkipValidation
    public String saveCustomerFeedBackAccess(){
		System.out.println("Success for feedback"+overall);
		if(overall==null) overall="Y";
		if(sessionCorpID.equals("STVF")){
			contactPhone ="";
			if(originAgent!=null){
				/*if(Integer.parseInt(originAgent)>2){
					contactPhone = originAgent;
				}else{
					contactPhone = "-1";
				}*/
				contactPhone = originAgent;
			}else{
				contactPhone = "5";
			}
			if(destinAgent!=null){
				/*if(Integer.parseInt(destinAgent)>2){
					contactPhone = contactPhone+"~"+"1";
				}else{
					contactPhone = contactPhone+"~"+"-1";
				}*/
				contactPhone = contactPhone+"~"+destinAgent;
			}else{
				contactPhone = contactPhone+"~"+"5";
			}
		}
		signUpService.updateEmailReveived(soId,contactPhone,comment2,sessionCorpID,overall);
		return SUCCESS;
    }
    
    
    private String surveyJob;
    private String customerFeedback;
    private String services;
    Map<String , String> reloServiceDetails=new HashMap<String, String>();
    @SkipValidation
    public String saveCustomerRequestAccess(){
    	String customerfirstName="";
    	String customerlastName="";
    	String salesPerson="";
    	String salesPersonName="";
    	String coordinator="";
    	String coordinatorUserName="";
    	String emailTo="";
    	String shipNumber="";
    	String crewNameDetails="";
    	String vanForeman="";
    	String deliveryForeman="";
    	String sequenceNumber="";
    	String venderNameDetailsForMail="";
        String agentnameDetailsForMail="";
        String contactPersonDetailsForMail="";
        String nameOfServiceForMail="";
        String serviceStartDateMailDetails="";
        String serviceEndDateMailDetails="";
    	
    List soDetails=signUpService.updateSoCustomerFeedback(soId,rtNumber,customerFeedback,services,surveyJob,sessionCorpID);
    if(soDetails!=null && !soDetails.isEmpty()){
    	  Iterator it= soDetails.iterator();
    		while(it.hasNext()){
    		Object  row[] =(Object[]) it.next();
    		customerfirstName= row[0].toString();
    		customerlastName=row[1].toString();
    		coordinator=row[2].toString();
    		salesPerson=row[3].toString();
    		shipNumber=row[4].toString();
    		sequenceNumber=row[5].toString();
    		}	
    }
          String venderNameDetails="";
          String venderCodeDetails="";
          String agentnameDetails="";
          String contactPersonDetails="";
          String serviceStartDateDetails="";
          String serviceEndDateDetails="";
          SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd/MM/yyyy");
         if(services!=null && !services.equals("")){
		  String serviceTypeDetails[]=services.split(",");	 
		  for (String serviceTemp: serviceTypeDetails) {
			  if(serviceStartDateDetails.equals("")){
				  serviceStartDateDetails=serviceTemp+"_serviceStartDate";
			  }else{
				  serviceStartDateDetails=serviceStartDateDetails+","+serviceTemp+"_serviceStartDate";
			  }
			  if(serviceEndDateDetails.equals("")){
				  serviceEndDateDetails=serviceTemp+"_serviceEndDate";
			  }else{
				  serviceEndDateDetails=serviceEndDateDetails+","+serviceTemp+"_serviceEndDate";
			  }
			  if(serviceTemp.equalsIgnoreCase("SCH")){
				 if(venderNameDetails.equals("")){
					 venderNameDetails="SCH_schoolName";
				 }else{
					 venderNameDetails=venderNameDetails+","+"SCH_schoolName"; 
				 }
				 if(contactPersonDetails.equals("")){
					 contactPersonDetails="SCH_contactPerson"; 
				 }else{
					 contactPersonDetails=contactPersonDetails+","+"SCH_contactPerson";
				 }
			  }else{
					 if(venderNameDetails.equals("")){
						 venderNameDetails=serviceTemp+"_vendorName";
					 }else{
						 venderNameDetails=venderNameDetails+","+serviceTemp+"_vendorName";
					 }
					if(serviceTemp.equalsIgnoreCase("HOM") || serviceTemp.equalsIgnoreCase("HSM")){
						if(agentnameDetails.equals("")){
							agentnameDetails=serviceTemp+"_agentName";
						}else{
							agentnameDetails=agentnameDetails+","+serviceTemp+"_agentName";
						}
					}
					if(serviceTemp.equalsIgnoreCase("SCH") || serviceTemp.equalsIgnoreCase("AIO") || serviceTemp.equalsIgnoreCase("TAC") || serviceTemp.equalsIgnoreCase("PRV") || serviceTemp.equalsIgnoreCase("SET") || serviceTemp.equalsIgnoreCase("RNT")){
					 if(serviceTemp.equalsIgnoreCase("SCH")){
						 if(contactPersonDetails.equals("")){
							 contactPersonDetails=serviceTemp+"_contactPerson"; 
						 }else{
							 contactPersonDetails=contactPersonDetails+","+serviceTemp+"_contactPerson";
						 } 
					 }else{
						 if(contactPersonDetails.equals("")){
							 contactPersonDetails=serviceTemp+"_vendorContact"; 
						 }else{
							 contactPersonDetails=contactPersonDetails+","+serviceTemp+"_vendorContact";
						 }
					 }
					
					}
			  }
		  }
		  }
         if(!venderNameDetails.equals("")){
           venderNameDetailsForMail=signUpService.getDspDetalsData(services,venderNameDetails,soId,sessionCorpID); 
         }
         if(!agentnameDetails.equals("")){
        	 agentnameDetailsForMail=signUpService.getDspDetalsData(services,agentnameDetails,soId,sessionCorpID); 
           }
         if(!contactPersonDetails.equals("")){
        	 contactPersonDetailsForMail=signUpService.getDspDetalsData(services,contactPersonDetails,soId,sessionCorpID); 
           }
         if(!serviceStartDateDetails.equals("")){
        	 
        	 String serviceStartDateMail=signUpService.getDspDetalsData(services,serviceStartDateDetails,soId,sessionCorpID); 
        	 TreeSet<Date> startDate =new TreeSet<Date>();
        	 if(serviceStartDateMail!=null && !serviceStartDateMail.equals("")){
       		  String serviceTypeDetails[]=serviceStartDateMail.split(",");	 
       		  for (String serviceTemp: serviceTypeDetails) {
       			startDate.add(CommonUtil.multiParse(serviceTemp));
       		  }       		
       		serviceStartDateMailDetails=dateformatYYYYMMDD1.format(startDate.pollFirst()).toString();
        	 }
        	 
         }
        
         if(!serviceEndDateDetails.equals("")){
             String serviceEndDateMail=signUpService.getDspDetalsData(services,serviceEndDateDetails,soId,sessionCorpID); 
        	 TreeSet<Date> endDate =new TreeSet<Date>();
        	 if(serviceEndDateMail!=null && !serviceEndDateMail.equals("")){
       		  String serviceTypeDetails[]=serviceEndDateMail.split(",");	 
       		  for (String serviceTemp: serviceTypeDetails) {
       			endDate.add(CommonUtil.multiParse(serviceTemp));
       		  } 
       		  serviceEndDateMailDetails=dateformatYYYYMMDD1.format(endDate.pollLast()).toString();
         }
        
         }
    if(surveyJob.equalsIgnoreCase("RLO")){
    	List reloDetailsList=signUpService.getReloDetailsList(sessionCorpID);
        if(reloDetailsList!=null && !reloDetailsList.isEmpty()){
     	   Iterator itRelo=reloDetailsList.iterator();
     	   while(itRelo.hasNext()){
     		   Object[] rowRelo=(Object[])itRelo.next();
     		   reloServiceDetails.put(rowRelo[0].toString(), rowRelo[1].toString());
     	   }
        }
        String venderCodeValue="";
        String vendorEmailValue="";
    	String serviceTypeDetails[]=notesAddForServices.split(",");	 
		for (String serviceTemp: serviceTypeDetails) {
		String venderDetails=signUpService.getVenderDspDetails(serviceTemp,soId,shipNumber,sessionCorpID);
		if(venderDetails!=null && !venderDetails.isEmpty()){
		String vendervcodeDspDetails[]=venderDetails.split("~");
		venderCodeValue=vendervcodeDspDetails[0];
		if(vendervcodeDspDetails[1].equals("A")){
			
		}else{
			vendorEmailValue=vendervcodeDspDetails[1];
		}
		}		
		String relocationServiceName = (String) reloServiceDetails.get(serviceTemp);
		if(nameOfServiceForMail.equals("")){
			nameOfServiceForMail=relocationServiceName;
		}else{
			nameOfServiceForMail=nameOfServiceForMail+","+relocationServiceName;
		}
		customerSurveyFeedback = new CustomerSurveyFeedback();
		customerSurveyFeedback.setCorpID(sessionCorpID);
		customerSurveyFeedback.setCreatedOn(new Date());
		customerSurveyFeedback.setCreatedBy("customer Feedback");
		customerSurveyFeedback.setUpdatedOn(new Date());
		customerSurveyFeedback.setUpdatedBy("customer Feedback");
		customerSurveyFeedback.setShipNumber(shipNumber);
		customerSurveyFeedback.setServiceOrderId(Long.parseLong(soId));
		customerSurveyFeedback.setCustomerRating(Integer.parseInt(rtNumber));
		customerSurveyFeedback.setFeedBack(customerFeedback);
		customerSurveyFeedback.setServiceName(serviceTemp);
		customerSurveyFeedback.setVendorCode(venderCodeValue);
		customerSurveyFeedback.setVendorContact(vendorEmailValue);
		if(serviceTemp.equalsIgnoreCase("MMG")){
		//List CheckForRlo=signUpService.checkRLOCustomerFeedback(soId,sessionCorpID);
		if(MMGServices.equalsIgnoreCase("destination")){
			customerSurveyFeedback.setServiceDetails("Destination");
			customerSurveyFeedback.setServiceForDestEmail(false);	
		}else if(MMGServices.equalsIgnoreCase("Origin")){
		    customerSurveyFeedback.setServiceDetails("Origin");
		    customerSurveyFeedback.setServiceForDestEmail(true);
		}else{
			 customerSurveyFeedback.setServiceDetails("");
			 customerSurveyFeedback.setServiceForDestEmail(false);
		}
		}else{
		    customerSurveyFeedback.setServiceDetails("");
		    customerSurveyFeedback.setServiceForDestEmail(false);	
		}
		customerSurveyFeedbackManager.save(customerSurveyFeedback);
		signUpService.getNPSCalculation(venderCodeValue,vendorEmailValue,soId,sessionCorpID);
		}
		
    }
/*    List userEmailDetails=signUpService.getUserEmail(rtNumber,sessionCorpID);  
    if(userEmailDetails!=null && !userEmailDetails.isEmpty() && userEmailDetails.get(0)!=null){
    	for (Object object : userEmailDetails) {
    		if(!emailTo.equals("")){
  				emailTo=emailTo+","+object.toString()+"";
  			}else{
  				emailTo=object.toString();
  			}
		}
    
    }*/
    emailTo="betterdays@hsrelo.com";
    
    if(!surveyJob.equalsIgnoreCase("RLO") && !surveyJob.equalsIgnoreCase("FINALRLO")){
  List crewName=signUpService.getCrewName(shipNumber,sessionCorpID);
    for (Object object : crewName) {
    	String crewDetails=object.toString();
    	 String[] crewType=crewDetails.split("#"); 
    	 String crewNameValue=crewType[0];
    	 String[] crewNameVal=crewNameValue.split(",");
    	if(!crewNameDetails.equals("")){
    		crewNameDetails=crewNameDetails+" : "+crewNameVal[1]+" , "+crewNameVal[0]+"";
    	}else{
    		crewNameDetails=""+crewNameVal[1]+" , "+crewNameVal[0]+"";
    	}
    	if(crewType[1]!=null && !crewType[1].equals("")){
    		if(crewType[1].equalsIgnoreCase("VF")){
        		if(!vanForeman.equals("")){
        			vanForeman=vanForeman+" : "+crewNameVal[1]+" , "+crewNameVal[0]+"";
        		}else{
        			vanForeman=""+crewNameVal[1]+" , "+crewNameVal[0]+"";
        		}
        		
        	}else if(crewType[1].equalsIgnoreCase("DF")){
        		if(!deliveryForeman.equals("")){
        			deliveryForeman=deliveryForeman+" : "+crewNameVal[1]+" , "+crewNameVal[0]+"";
        		}else{
        			deliveryForeman=""+crewNameVal[1]+" , "+crewNameVal[0]+"";
        		}
        	}
    	}
    
    	
	}
    }
    String from="support@redskymobility.com";
    List compnaySurveyEmail =signUpService.getSurveyEmail(sessionCorpID);
    if(compnaySurveyEmail!=null && !compnaySurveyEmail.isEmpty() && compnaySurveyEmail.get(0)!=null){
    	from=compnaySurveyEmail.get(0).toString();
    }
    
    if(!salesPerson.equals("")){
    List salesPersonDetails=signUpService.getUserDetails(salesPerson,sessionCorpID);
    for (Object object : salesPersonDetails) {
    	String salesDetails=object.toString();
   	    String[] salesname=salesDetails.split("#");
   	 salesPersonName=""+salesname[0]+" "+salesname[1]+"";
	}
    }
    if(!coordinator.equals("")){
    List coordinatorDetails=signUpService.getUserDetails(coordinator,sessionCorpID);
    for (Object object : coordinatorDetails) {
    	String coordinatorDetailsValue=object.toString();
   	    String[] coordinatorName=coordinatorDetailsValue.split("#");
   	 coordinatorUserName=""+coordinatorName[0]+" "+coordinatorName[1]+"";
	}
    }
    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss aa");
    Date date = new Date();
    String messageText1="<html><head></head><body>";
     messageText1=  messageText1+"At "+dateFormat.format(date)+" A submission  was Successfully sent from Survey Page "+shipNumber+" ";
    
    messageText1=messageText1+"<br><br><table width='100%' border='0' cellspacing='0' cellpadding='3' style='font-family:Arial, Helvetica, sans-serif; font-size:15px'><tr><td><b>COMMENT :&nbsp;</b>"+customerFeedback+" </td></tr></table>";
    messageText1=messageText1+"<br><b>VOTE:</b>&nbsp; "+rtNumber+"";
    messageText1=messageText1+"<br><br><b>Customer Name:</b>&nbsp;"+customerlastName+"&nbsp;,&nbsp;"+customerfirstName+"";
    if(!surveyJob.equalsIgnoreCase("RLO")){
      if(!vanForeman.equals("")){
    	messageText1=messageText1+"<br><b>Van Foreman:</b>&nbsp; "+vanForeman+"";	
    }
    if(!deliveryForeman.equals("")){
    	messageText1=messageText1+"<br><b>Delivery Foreman:</b>&nbsp; "+deliveryForeman+"<br>";	
    }
    }
    if(!salesPersonName.equals("")){
    messageText1=messageText1+"<br><b>Sales Person:</b>&nbsp; "+salesPersonName+"";	
    }
   if(!coordinatorUserName.equals("")){
   messageText1=messageText1+"<br><b>Coordinator:</b>&nbsp; "+coordinatorUserName+"";   
   }
   if(surveyJob.equalsIgnoreCase("RLO")){
	   if(!venderNameDetailsForMail.equals("")){
		   messageText1=messageText1+"<br><b>Vendor Name:</b>&nbsp; "+venderNameDetailsForMail+"";   
	   }
    if(!agentnameDetailsForMail.equals("")){
    	messageText1=messageText1+"<br><b>Agent Name:</b>&nbsp; "+agentnameDetailsForMail+""; 
	   }
   if(!contactPersonDetailsForMail.equals("")){
	   messageText1=messageText1+"<br><b>Contact Person:</b>&nbsp; "+contactPersonDetailsForMail+""; 
   }
   if(!nameOfServiceForMail.equals("")){
	   messageText1=messageText1+"<br><b>Name of service: </b>&nbsp;"+nameOfServiceForMail+"";
   }
   if(!serviceStartDateMailDetails.equals("") && !serviceEndDateMailDetails.equals("")){
	   messageText1=messageText1+"<br><b>Service Dates :</b>&nbsp;"+serviceStartDateMailDetails+" To "+serviceEndDateMailDetails+"";
   }
   }
   if(!surveyJob.equalsIgnoreCase("RLO") && !surveyJob.equalsIgnoreCase("FINALRLO")){
   if(!crewNameDetails.equals("")){
   messageText1=messageText1+"<br><br><b>CREW&nbsp;:</b> "+crewNameDetails+"";	
    }
   }
    messageText1=messageText1+"</body></html>";    
    try {
		String subject = "HSRG Customer Information";
		//String from = "highland@highlandmoving.com";
	/*	String smtpHost = "localhost";
		//String from = "highland@highlandmoving.com";
		Properties props = System.getProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.host", smtpHost);
		Session session = Session.getInstance(props, null);
		session.setDebug(true);
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(from));
		//InternetAddress[] address = { new InternetAddress(emailTo) };
		msg.setRecipients(Message.RecipientType.TO, emailTo);
		msg.setSubject(subject);
		MimeBodyPart mbp1 = new MimeBodyPart();
		//mbp1.setText(messageText1);
		Multipart mp = new MimeMultipart();
		mp.addBodyPart(mbp1);
		msg.setContent(mp);
		msg.setSentDate(new Date());
		msg.setContent(messageText1, "text/html; charset=ISO-8859-1");
		Transport.send(msg);*/
		
		emailSetupManager.globalEmailSetupProcess(from, emailTo, "", "", "", messageText1, subject, sessionCorpID,"",shipNumber,"");
	} catch (Exception e) {
	   e.printStackTrace();
      
	}
	
		return SUCCESS;
    }
    
    public String agentEmailForm(){
    	ocountry = signUpService.findCountry("COUNTRY");
    	states=signUpService.findStateService(bucket2 , sessionCorpID);
    	enbState = signUpService.serviceStateList(sessionCorpID);
    	return SUCCESS;
    }
    
    private String emailMessage;
	private String hitFlag;
	private String userAddress1;
	private String userAddress2;
	private String userAddress3;
	private String userAddress4;
	private String userFax;
	private String userPhone;
	private String userCity;
	private String userTelex;
	private String userZip;
	
    public String saveAgentEmailPage(){
    	ocountry = signUpService.findCountry("COUNTRY");
    	states=signUpService.findStateService( bucket2 , sessionCorpID);
    	String address=userAddress1;
    	
    	if(userAddress2!=null && !userAddress2.equals("")){
    		address = address+","+userAddress2;
    	}
    	if(userAddress3!=null && !userAddress3.equals("")){
    		address = address+","+userAddress3;
    	}
    	if(userAddress4!=null && !userAddress4.equals("")){
    		address = address+","+userAddress4;
    	}
    	try {
			
			
			String from = "support@redskymobility.com";
			String subject="User Query for Partner Access Request";
			systemDefaultEmail="redsky@sscw.com";
			sysDefEmailList=signUpService.supportEmail(sessionCorpID);
			if(sysDefEmailList!=null && !(sysDefEmailList.isEmpty())){
        		for (SystemDefault systemDefault : sysDefEmailList) {
        	systemDefaultEmail=systemDefault.getAgentIdRegistrationDistribution();
        	}
        	}
			
			if(userState==null){
				userState="";
			}
			String messageText1 = "\nCompany Name:- "+userBranch;
			messageText1 = messageText1 +"\nAddress:- "+address;
			messageText1 = messageText1 +"\nCountry:- "+userCountry;
			messageText1 = messageText1 +"\nState:- "+userState;
			messageText1 = messageText1 +"\nCity:- "+userCity;
			messageText1 = messageText1 +"\nZip:- "+userZip;
			messageText1 = messageText1 +"\nFax:- "+userFax;
			messageText1 = messageText1 +"\nPhone:- "+userPhone;
			messageText1 = messageText1 +"\nTelex:- "+userTelex;
			messageText1 = messageText1 +"\nEmail:- "+userEmail;
			messageText1 = messageText1 +"\n\nMessage:- "+emailMessage;
			
			emailSetupManager.globalEmailSetupProcess(from, systemDefaultEmail, "", "", "", messageText1, subject, sessionCorpID,"","","");
			hitFlag="1";
		} catch (Exception ex) {
			hitFlag="2";
		}
		return SUCCESS;
    }
    
    private java.util.Date getCurrentDate() {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = dfm.format(new Date());
		Date currentDate = null;
		try {
			currentDate = dfm.parse(dateStr);
			
		} catch (java.text.ParseException e) {
			
			e.printStackTrace();
		}
		return currentDate;
		
	}
    
    private String partnerType;
    private String sessionCorpID;
    private String stateCode;
	private List agents;
    @SkipValidation 
	public String agentPopupList(){
    	try{
		agents = signUpService.getAgentPopupList(partnerType, sessionCorpID, userCountry, stateCode, "", "", "");
    	}catch(Exception ex)
    	{
    		System.out.println("\n\n\n\n\n\n exception--->>"+ex);
    		ex.printStackTrace();
    	}
		return SUCCESS;
	}
    
    private String partnerLastName;
    private String partnerTerminalCountryCode;
    private String partnerTerminalCountry;
    private String partnerTerminalState;
    @SkipValidation 
    public String searchAgentPopup(){
    	agents= signUpService.searchAgents(partnerCode,partnerLastName,partnerTerminalCountryCode,partnerTerminalCountry,partnerTerminalState,sessionCorpID );
    	return SUCCESS;
    }
    @SkipValidation 
    public String requestAccess(){
    	ocountry = signUpService.findCountry("COUNTRY");
    	jobFunction=signUpService.getJobFunction("JOBFUNCTION");
    	ostates=signUpService.findState("USA", sessionCorpID);
    	user= new User();
    	getSession().setAttribute("availableJobTypes", jobFunction);
    	return SUCCESS;
    }
    @SkipValidation 
    public String saveRequestAccess(){

    	ocountry = signUpService.findCountry("COUNTRY");
    	jobFunction=signUpService.getJobFunction("JOBFUNCTION");
    	ostates=signUpService.findState("USA", sessionCorpID);
    	boolean isNew=(user.getId()==null);
    	
    	List checkUser=signUpService.checkUser(userEmail, sessionCorpID);
    	if(isNew && !checkUser.isEmpty())
    	{
    		accountMessage="User already exist with this email Id. Please change the email address.";
			saveMessage(accountMessage);
			return INPUT;
    	}
    	else{
    	partner=(Partner)signUpService.getPartner(partnerCode, sessionCorpID).get(0);
    	String[] job = getRequest().getParameterValues("userJobType");
    	StringBuilder result = new StringBuilder();
    	if (job.length > 0) {
            result.append(job[0]);
            for (int i=1; i < job.length; i++) {
                result.append(",");
                result.append(job[i]);
            }
        }
    	user.setFunctionalArea(result.toString()); 

    	Address address = new Address();
    	address.setAddress(partner.getTerminalAddress1());
    	address.setCity(partner.getTerminalCity());
		address.setPostalCode(partner.getTerminalZip());
		address.setProvince(partner.getTerminalState());
    	address.setCountry(userCountry);
		user.setAddress(address);
    	user.setCorpID(sessionCorpID);
    	user.setFirstName(userFirstName);
    	user.setLastName(userLastName);
    	user.setAlias(userAlias);
    	user.setBranch(partnerCode);
    	user.setParentAgent(partnerCode);
    	user.setWebsite(partner.getUrl());
    	if(phoneType1.equalsIgnoreCase("Work"))
    	{
    		user.setPhoneNumber(userPhoneNumber);
    	}
    	if(phoneType1.equalsIgnoreCase("Cell"))
    	{
    		user.setPhoneCell(userPhoneNumber);
    	}
    	if(phoneType1.equalsIgnoreCase("Home"))
    	{
    		user.setPhoneHome(userPhoneNumber);
    	}
    	if(phoneType1.equalsIgnoreCase("Fax"))
    	{
    		user.setFaxNumber(userPhoneNumber);
    	}
    	
    	if(phoneType2.equalsIgnoreCase("Work"))
    	{
    		user.setPhoneNumber(userFaxNumber);
    	}
    	if(phoneType2.equalsIgnoreCase("Cell"))
    	{
    		user.setPhoneCell(userFaxNumber);
    	}
    	if(phoneType2.equalsIgnoreCase("Home"))
    	{
    		user.setPhoneHome(userFaxNumber);
    	}
    	if(phoneType2.equalsIgnoreCase("Fax"))
    	{
    		user.setFaxNumber(userFaxNumber);
    	}
    	Role role=signUpService.getRole("ROLE_AGENT");
    	user.addRole(signUpService.getRole("ROLE_EMPLOYEE"));
    	user.addRole(signUpService.getRole("ROLE_ADMIN"));
    	user.addRole(signUpService.getRole("ROLE_SECURITY_ADMIN"));
    	user.addRole(signUpService.getRole("ROLE_USER"));
    	user.setFaxNumber(userFaxNumber);
    	user.setEmail(userEmail);
    	oldPortalId = "astwerat123";
		oldPortalId = oldPortalId.replace("'", "");
		newPortalId = signUpService.findPortalId(oldPortalId);
		portalIdCount = newPortalId.get(0).toString();
			if (portalIdCount.equals("0")) {
				portalIdCount = "";
			}
			portalId = oldPortalId + portalIdCount;
			boolean userExists = signUpService.findUser(portalId);
			
			if (userExists) {
				if (portalIdCount.equals("")) {
					portalIdCount = "0";
				}
				portalIdCount = (new Long(Long.parseLong(portalIdCount) + 1)).toString();
				portalId = oldPortalId + portalIdCount;
			}
		
		poratalIdCountList = signUpService.findCustomerPortalListDetails(oldPortalId);
		if(!poratalIdCountList.isEmpty()){
			  Iterator it= poratalIdCountList.iterator();
				while(it.hasNext()){
					Object  row =(Object)it.next();
					
					if(row.equals(portalId)){
						portalIdCount = (new Long(Long.parseLong(portalIdCount) + 1)).toString();
						userPortalId = oldPortalId + portalIdCount;
						portalId=userPortalId;
					}
				}
				
		}
		user.setUsername(portalId);
    	String str=new  String("QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR");
	     StringBuffer sb=new StringBuffer();
	     Random r = new Random();
	     int te=0;
	     for(int i=1;i<=8;i++){
	         te=r.nextInt(62);
	         sb.append(str.charAt(te));
	     }
		   	getConfiguration().get(Constants.ENCRYPT_PASSWORD);
		   	String algorithm = (String)
		   	getConfiguration().get(Constants.ENC_ALGORITHM);
		   	if (algorithm == null) { // should only happen for test case
					  log.debug("assuming testcase, setting algorithm to 'SHA'");
					  algorithm = "SHA"; 
				}
		   	String passwordNew = StringUtil.encodePassword("xyz",algorithm);
		   	//String passwordNew = StringUtil.encodePassword(portalId.replaceAll(" ", ""),algorithm);
		   	confirmPasswordNew=passwordNew;
		   	user.setPassword(passwordNew);
		   	user.setConfirmPassword(passwordNew);
		   	
		   	user.setPwdexpiryDate(DateUtils.addDays(getCurrentDate(), 90));
		   	user.setWorkStartTime("00:00");
			user.setWorkEndTime("00:00");
			user.setCreatedOn(new Date());
			user.setUpdatedOn(new Date());
			user.setCreatedBy("");
			user.setUpdatedBy("");
			user.setAccountExpired(false);
			user.setAccountLocked(false);
			user.setCredentialsExpired(false);
			user.setEnabled(true);
			user.setUserType("USER");
			user.setPasswordHintQues(" Default");
			user.setPasswordHint("Default");
			user.setPasswordReset(false);
			user.setSupervisor("");
			signUpService.saveUser(user);
			accountMessage="Request has been submitted successfully";
			saveMessage(accountMessage);
    }
   	 	return SUCCESS;
   	 	
    
    }
    
    private CustomerFile customerFile;
    private AccessInfo accessInfo;
    private Document dom;
    private Long cid;
    public Element addElement(String value,String tagName){
    	 Element ele = dom.createElement(tagName);
         Text nameText=dom.createTextNode(value);
         ele.appendChild(nameText);
         return ele;
    }
    public String sendXmlResponseToSurveyor(){
    	creatXMLFromUrl(cid);
    	return SUCCESS;
    }
    private Map<String ,Integer> loginHitCont = new LinkedHashMap<String ,Integer>();
	private String userNameStatus;
	private String loginMsg="";
	private String currentPassword;
	@SkipValidation
	public String updateSucFail(){ 
		return SUCCESS;
	}
   
	public String creatXMLFromUrl(Long id){
    	customerFile=signUpService.getCustomer(id);
    	try{
    	accessInfo=signUpService.getAccessinfo(id);
    	}catch (Exception e) {
    	}
    	if(accessInfo==null){
    		accessInfo=new AccessInfo();
    		return "N";
    	}
    	createDocument();
    	String surveyarEmail="";
    	String filePath="mf"+customerFile.getFirstName()+"_"+customerFile.getLastName()+"_"+customerFile.getSequenceNumber()+".xml";
    	File file = new File(filePath);
    	Element rootEle = dom.createElement("MovingData");
    	rootEle.setAttribute("ID",customerFile.getId().toString());
    	dom.appendChild(rootEle);
    	   Element generalInfoEle = dom.createElement("GeneralInfo");
    	        Element Name = dom.createElement("Name");
    	        Text nameText=dom.createTextNode(customerFile.getFirstName()+" "+customerFile.getLastName());
    	        Name.appendChild(nameText);
    	   generalInfoEle.appendChild(Name);
    	        if(customerFile.getEstimator()==null)customerFile.setEstimator("");
    	   generalInfoEle.appendChild(addElement(customerFile.getEstimator(),"EstimatorName")); 
    	        if(customerFile.getSource()==null)customerFile.setSource("");
    	   generalInfoEle.appendChild(addElement(customerFile.getSource(),"SourceOfRequest"));
    	        if(customerFile.getSurvey()==null)customerFile.setSurvey(new Date());
    	        SimpleDateFormat dateformat = new SimpleDateFormat("mm/dd/yy");
    	   generalInfoEle.appendChild(addElement(" ","EstimationDate"));
    	        if(customerFile.getSequenceNumber()==null)customerFile.setSequenceNumber("");
    	   generalInfoEle.appendChild(addElement(customerFile.getSequenceNumber(),"EMFID"));
    	            
    	           Element addressEle = dom.createElement("Address");
    	                   if(customerFile.getOriginAddress1()==null)customerFile.setOriginAddress1("");
    	                   addressEle.appendChild(addElement(" ","Company"));
    	                   if(customerFile.getOriginAddress2()==null)customerFile.setOriginAddress2("");
    	                   addressEle.appendChild(addElement(customerFile.getOriginAddress1()+" "+customerFile.getOriginAddress2(),"Street"));
    	                   if(customerFile.getOriginCity()==null)customerFile.setOriginCity("");
    	                   addressEle.appendChild(addElement(customerFile.getOriginCity(),"City"));
    	                   if(customerFile.getOriginState()==null)customerFile.setOriginState("");
    	                   addressEle.appendChild(addElement(customerFile.getOriginState(),"State"));
    	                   if(customerFile.getOriginCountry()==null)customerFile.setOriginCountry("");
    	                   addressEle.appendChild(addElement(customerFile.getOriginCountry(),"Country"));
    	                   if(customerFile.getOriginZip()==null)customerFile.setOriginZip("");
    	                   addressEle.appendChild(addElement(customerFile.getOriginZip(),"Zip"));
    	                   if(customerFile.getOriginDayPhone()==null)customerFile.setOriginDayPhone("");
    	                   addressEle.appendChild(addElement(customerFile.getOriginDayPhone(),"PrimaryPhone"));
    	                   if(customerFile.getOriginMobile()==null)customerFile.setOriginMobile("");
    	                   addressEle.appendChild(addElement(customerFile.getOriginMobile(),"SecondaryPhone"));
    	                   if(customerFile.getEmail()==null)customerFile.setEmail("");
    	                   addressEle.appendChild(addElement(customerFile.getEmail(),"email"));
    	                   if(customerFile.getOriginFax()==null)customerFile.setOriginFax("");
    	                   addressEle.appendChild(addElement(customerFile.getOriginFax(),"fax"));
    	                           
    	                       Element accessInfoEle = dom.createElement("AccessInfo");	                      
    	                       accessInfoEle.appendChild(addElement(accessInfo.getOriginfloor(),"Floor"));
    	                       if(accessInfo.getOriginElevator()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginElevator().toString(),"HasElevator")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement("false","HasElevator"));  
    	                       }
    	                       if(accessInfo.getOriginElevatorDetails()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginElevatorDetails().toString(),"ElevatorDetails")); 
    	                       }else{
    	                       accessInfoEle.appendChild(addElement(" ","ElevatorDetails"));
    	                       }
    	                       if(accessInfo.getOriginDistanceToParking()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginDistanceToParking().toString(),"DistanceToParking")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement("0","DistanceToParking"));
    	                       }
    	                       if(accessInfo.getOriginNeedCrane()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginNeedCrane().toString(),"NeedCrane")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement("false","NeedCrane"));
    	                       }
    	                       accessInfoEle.appendChild(addElement(" ","PictureFileName"));
    	                       if(accessInfo.getOriginResidenceType()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginResidenceType().toString(),"PropertyType")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement(" ","PropertyType"));
    	                       }
    	                       if(accessInfo.getOriginResidenceSize()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginResidenceSize().toString(),"PropertySize")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement(" ","PropertySize"));
    	                       }
    	                       if(accessInfo.getOriginReserveParking()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginReserveParking().toString(),"ParkingReservationRequired")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement("false","ParkingReservationRequired"));
    	                       }
    	                       if(accessInfo.getOriginParkingType()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginParkingType().toString(),"ParkingType")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement(" ","ParkingType"));
    	                       }
    	                       if(accessInfo.getOriginParkingSpots()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginParkingSpots().toString(),"NumOfParkingSpots")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement("0","NumOfParkingSpots"));
    	                       }
    	                       if(accessInfo.getOriginParkinglotsize()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginParkinglotsize().toString(),"ParkingSpotSize")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement("0","ParkingSpotSize"));
    	                       }
    	                       if(accessInfo.getOriginLongCarry()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginLongCarry().toString(),"CarryRequired")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement("false","CarryRequired"));
    	                       }
    	                       if(accessInfo.getOriginCarryDistance()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginCarryDistance().toString(),"CarryLength")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement("0","CarryLength"));
    	                       }
    	                       if(accessInfo.getOriginCarrydetails()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginCarrydetails().toString(),"CarryDescription")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement(" ","CarryDescription"));
    	                       }
    	                       if(accessInfo.getOriginElevatorType()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginElevatorType().toString(),"ExternalElevatorType")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement(" ","ExternalElevatorType"));
    	                       }
    	                       if(accessInfo.getOriginShuttleRequired()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginShuttleRequired().toString(),"shuttleCarryRequired")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement("false","shuttleCarryRequired"));
    	                       }
    	                       if(accessInfo.getOriginShuttleDistance()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginShuttleDistance().toString(),"ShuttleDistance")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement("0","ShuttleDistance"));
    	                       }
    	                       if(accessInfo.getOriginStairCarry()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginStairCarry().toString(),"StairCarryRequired")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement("false","StairCarryRequired"));
    	                       }
    	                       if(accessInfo.getOriginStairDistance()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginStairDistance().toString(),"StairCarryLength")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement("0","StairCarryLength"));
    	                       }
    	                       if(accessInfo.getOriginStairdetails()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginStairdetails().toString(),"StairCarryDescription")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement(" ","StairCarryDescription"));
    	                       }
    	                       if(accessInfo.getOriginAdditionalStopRequired()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginAdditionalStopRequired().toString(),"StairCarryDescription")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement("false","AdditionalStopRequired"));
    	                       }
    	                       if(accessInfo.getOriginAdditionalStopDetails()!=null){
    	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginAdditionalStopDetails().toString(),"AdditionalStop")); 
    	                       }else{
    	                    	   accessInfoEle.appendChild(addElement(" ","AdditionalStop"));
    	                       }
    	                      
    	                  
    	                  addressEle.appendChild(accessInfoEle);
    	                  addressEle.appendChild(addElement("","Comment"));
    	                  
    	                 
    	    generalInfoEle.appendChild(addressEle); 
    	       
    	                Element destinationEle = dom.createElement("Destination");
    	                      if(customerFile.getDestinationAddress1()==null)customerFile.setDestinationAddress1("");
    	                      destinationEle.appendChild(addElement("","Company"));
    	                      if(customerFile.getDestinationAddress2()==null)customerFile.setDestinationAddress2("");
    	                      destinationEle.appendChild(addElement( customerFile.getDestinationAddress1()+" "+customerFile.getDestinationAddress2(),"Street"));
    	                      if(customerFile.getDestinationCity()==null)customerFile.setDestinationCity("");
    	                      destinationEle.appendChild(addElement(customerFile.getDestinationCity(),"City"));
    	                      if(customerFile.getDestinationState()==null)customerFile.setDestinationState("");
    	                      destinationEle.appendChild(addElement(customerFile.getDestinationState(),"State"));
    	                      if(customerFile.getDestinationCountry()==null)customerFile.setDestinationCountry("");
    	                      destinationEle.appendChild(addElement(customerFile.getDestinationCountry(),"Country"));
    	                      if(customerFile.getDestinationZip()==null)customerFile.setDestinationZip("");
    	                      destinationEle.appendChild(addElement(customerFile.getDestinationZip(),"Zip"));
    	                      if(customerFile.getDestinationDayPhone()==null)customerFile.setDestinationDayPhone("");
    	                      destinationEle.appendChild(addElement(customerFile.getDestinationDayPhone(),"PrimaryPhone"));
    	                      if(customerFile.getDestinationMobile()==null)customerFile.setDestinationMobile("");
    	                      destinationEle.appendChild(addElement(customerFile.getDestinationMobile(),"SecondaryPhone"));
    	                      if(customerFile.getEmail()==null)customerFile.setEmail("");
    	                      destinationEle.appendChild(addElement(customerFile.getEmail(),"email"));
    	                      if(customerFile.getDestinationFax()==null)customerFile.setDestinationFax("");
    	                      destinationEle.appendChild(addElement(customerFile.getDestinationFax(),"fax"));
    	                         
    	                         Element accessInfoEle2 = dom.createElement("AccessInfo");
    	                         
    	                         
    	                         accessInfoEle2.appendChild(addElement(accessInfo.getDestinationfloor(),"Floor"));
    		                       if(accessInfo.getDestinationElevator()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationElevator().toString(),"HasElevator")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement("false","HasElevator"));  
    		                       }
    		                       if(accessInfo.getDestinationElevatorDetails()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationElevatorDetails().toString(),"ElevatorDetails")); 
    		                       }else{
    		                       accessInfoEle2.appendChild(addElement(" ","ElevatorDetails"));
    		                       }
    		                       if(accessInfo.getDestinationDistanceToParking()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationDistanceToParking().toString(),"DistanceToParking")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement("0","DistanceToParking"));
    		                       }
    		                       if(accessInfo.getDestinationNeedCrane()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationNeedCrane().toString(),"NeedCrane")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement("false","NeedCrane"));
    		                       }
    		                       accessInfoEle2.appendChild(addElement(" ","PictureFileName"));
    		                       if(accessInfo.getDestinationResidenceType()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationResidenceType().toString(),"PropertyType")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement(" ","PropertyType"));
    		                       }
    		                       if(accessInfo.getDestinationResidenceSize()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationResidenceSize().toString(),"PropertySize")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement(" ","PropertySize"));
    		                       }
    		                       if(accessInfo.getDestinationReserveParking()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationReserveParking().toString(),"ParkingReservationRequired")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement("false","ParkingReservationRequired"));
    		                       }
    		                       if(accessInfo.getDestinationParkingType()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationParkingType().toString(),"ParkingType")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement(" ","ParkingType"));
    		                       }
    		                       if(accessInfo.getDestinationParkingSpots()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationParkingSpots().toString(),"NumOfParkingSpots")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement("0","NumOfParkingSpots"));
    		                       }
    		                       if(accessInfo.getDestinationParkinglotsize()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationParkinglotsize().toString(),"ParkingSpotSize")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement("0","ParkingSpotSize"));
    		                       }
    		                       if(accessInfo.getDestinationLongCarry()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationLongCarry().toString(),"CarryRequired")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement("false","CarryRequired"));
    		                       }
    		                       if(accessInfo.getDestinationCarryDistance()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationCarryDistance().toString(),"CarryLength")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement("0","CarryLength"));
    		                       }
    		                       if(accessInfo.getDestinationCarrydetails()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationCarrydetails().toString(),"CarryDescription")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement(" ","CarryDescription"));
    		                       }
    		                       if(accessInfo.getDestinationElevatorType()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationElevatorType().toString(),"ExternalElevatorType")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement(" ","ExternalElevatorType"));
    		                       }
    		                       if(accessInfo.getDestinationShuttleRequired()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationShuttleRequired().toString(),"shuttleCarryRequired")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement("false","shuttleCarryRequired"));
    		                       }
    		                       if(accessInfo.getDestinationShuttleDistance()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationShuttleDistance().toString(),"ShuttleDistance")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement("0","ShuttleDistance"));
    		                       }
    		                       if(accessInfo.getDestinationStairCarry()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationStairCarry().toString(),"StairCarryRequired")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement("false","StairCarryRequired"));
    		                       }
    		                       if(accessInfo.getDestinationStairDistance()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationStairDistance().toString(),"StairCarryLength")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement("0","StairCarryLength"));
    		                       }
    		                       if(accessInfo.getDestinationStairdetails()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationStairdetails().toString(),"StairCarryDescription")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement(" ","StairCarryDescription"));
    		                       }
    		                       if(accessInfo.getDestinationAdditionalStopRequired()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationAdditionalStopRequired().toString(),"StairCarryDescription")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement("false","AdditionalStopRequired"));
    		                       }
    		                       if(accessInfo.getDestinationAdditionalStopDetails()!=null){
    		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationAdditionalStopDetails().toString(),"AdditionalStop")); 
    		                       }else{
    		                    	   accessInfoEle2.appendChild(addElement(" ","AdditionalStop"));
    		                       }
    	                        
    	                 destinationEle.appendChild(accessInfoEle2);
    	                 destinationEle.appendChild(addElement("","Comment"));
    	            generalInfoEle.appendChild(destinationEle);
    	            Element preferencesEle = dom.createElement("Preferences"); 
    	                 preferencesEle.appendChild(addElement(" ","PreferredLanguage"));
    	                 preferencesEle.appendChild(addElement(" ","PackingDate"));
    	                 preferencesEle.appendChild(addElement(" ","PackingTime"));	                
    	                 preferencesEle.appendChild(addElement(dateformat.format(customerFile.getSurvey())+" "+customerFile.getSurveyTime(),"VacationDate"));
    	                 preferencesEle.appendChild(addElement(" ","SurveyEndDate"));
    	                 preferencesEle.appendChild(addElement(" ","DepartureDate"));
    	                 preferencesEle.appendChild(addElement(" ","PackingFinishDate"));
    	                 preferencesEle.appendChild(addElement(" ","DeliveryFinishDate"));
    	                 preferencesEle.appendChild(addElement(" ","ExportCustomsDate"));
    	                 preferencesEle.appendChild(addElement(" ","OriginStorageInDate"));
    	                 preferencesEle.appendChild(addElement(" ","OriginStorageOutDate"));
    	                 preferencesEle.appendChild(addElement(" ","DestCustomsInDate"));
    	                 preferencesEle.appendChild(addElement(" ","DestCustomsOutDate"));
    	                 preferencesEle.appendChild(addElement(" ","DestStorageInDate"));
    	                 preferencesEle.appendChild(addElement(" ","DestStorageOutDate"));
    	                 preferencesEle.appendChild(addElement(" ","CompletionDate"));
    	                 preferencesEle.appendChild(addElement(" ","DeliveryDate"));
    	                 preferencesEle.appendChild(addElement(" ","RealArrivalDate"));
    	                 preferencesEle.appendChild(addElement(" ","ServiceType"));
    	                 preferencesEle.appendChild(addElement(" ","Comment"));
    	                 preferencesEle.appendChild(addElement(" ","AdditionalServices"));
    	         generalInfoEle.appendChild(preferencesEle);
    	         generalInfoEle.appendChild(addElement(" ","Comment"));
      rootEle.appendChild(generalInfoEle);  
      try{
     //TransformerFactory instance is used to create Transformer objects. 
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer = factory.newTransformer(); 
      transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
      // create string from xml tree
      StringWriter sw = new StringWriter();
      StreamResult result = new StreamResult(sw);
      DOMSource source = new DOMSource(dom);
      transformer.transform(source, result);
      String xmlString = sw.toString();
      BufferedWriter bw = new BufferedWriter
                    (new OutputStreamWriter(new FileOutputStream(file)));
      bw.write(xmlString);
      bw.flush();
      bw.close();
      message=xmlString;
      }catch(Exception e){e.printStackTrace();}      
      return SUCCESS;
    }

    private void createDocument() {
    	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    	try {
    	DocumentBuilder db = dbf.newDocumentBuilder();
           dom = db.newDocument();
    	}catch(ParserConfigurationException pce) {
    		System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
    	}
    }

    
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the emailList
	 */
	public List getEmailList() {
		return emailList;
	}

	/**
	 * @param emailList the emailList to set
	 */
	public void setEmailList(List emailList) {
		this.emailList = emailList;
	}

	/**
	 * @param userManager the userManager to set
	 */
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	/**
	 * @param signUpService the signUpService to set
	 */
	public void setSignUpService(SignUpService signUpService) {
		this.signUpService = signUpService;
	}

	/**
	 * @return the confirmPasswordNew
	 */
	public String getConfirmPasswordNew() {
		return confirmPasswordNew;
	}

	/**
	 * @param confirmPasswordNew the confirmPasswordNew to set
	 */
	public void setConfirmPasswordNew(String confirmPasswordNew) {
		this.confirmPasswordNew = confirmPasswordNew;
	}

	/**
	 * @return the passwordNew
	 */
	public String getPasswordNew() {
		return passwordNew;
	}

	/**
	 * @param passwordNew the passwordNew to set
	 */
	public void setPasswordNew(String passwordNew) {
		this.passwordNew = passwordNew;
	}

	/**
	 * @return the fName
	 */
	public String getFName() {
		return fName;
	}

	/**
	 * @param name the fName to set
	 */
	public void setFName(String name) {
		fName = name;
	}

	/**
	 * @return the lName
	 */
	public String getLName() {
		return lName;
	}

	/**
	 * @param name the lName to set
	 */
	public void setLName(String name) {
		lName = name;
	}

	
	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the hintAns
	 */
	public String getHintAns() {
		return hintAns;
	}

	/**
	 * @param hintAns the hintAns to set
	 */
	public void setHintAns(String hintAns) {
		this.hintAns = hintAns;
	}

	/**
	 * @return the hintQuestion
	 */
	public String getHintQuestion() {
		return hintQuestion;
	}

	/**
	 * @param hintQuestion the hintQuestion to set
	 */
	public void setHintQuestion(String hintQuestion) {
		this.hintQuestion = hintQuestion;
	}

	/**
	 * @return the target
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * @param target the target to set
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the eMail
	 */
	public String getEMail() {
		return eMail;
	}

	/**
	 * @param mail the eMail to set
	 */
	public void setEMail(String mail) {
		eMail = mail;
	}
	/**
	 * @return the usernameHintList
	 */
	public List getUsernameHintList() {
		return usernameHintList;
	}
	/**
	 * @param usernameHintList the usernameHintList to set
	 */
	public void setUsernameHintList(List usernameHintList) {
		this.usernameHintList = usernameHintList;
	}
	/**
	 * @return the hintQues
	 */
	public String getHintQues() {
		return hintQues;
	}
	/**
	 * @param hintQues the hintQues to set
	 */
	public void setHintQues(String hintQues) {
		this.hintQues = hintQues;
	}
	/**
	 * @return the usernamePersist
	 */
	public String getUsernamePersist() {
		return usernamePersist;
	}
	/**
	 * @param usernamePersist the usernamePersist to set
	 */
	public void setUsernamePersist(String usernamePersist) {
		this.usernamePersist = usernamePersist;
	}
	/**
	 * @return the hintQues1
	 */
	public String getHintQues1() {
		return hintQues1;
	}
	/**
	 * @param hintQues1 the hintQues1 to set
	 */
	public void setHintQues1(String hintQues1) {
		this.hintQues1 = hintQues1;
	}
	/**
	 * @return the hintQues1reset
	 */
	public String getHintQues1reset() {
		return hintQues1reset;
	}
	/**
	 * @param hintQues1reset the hintQues1reset to set
	 */
	public void setHintQues1reset(String hintQues1reset) {
		this.hintQues1reset = hintQues1reset;
	}
	/**
	 * @return the hintQuesreset
	 */
	public String getHintQuesreset() {
		return hintQuesreset;
	}
	/**
	 * @param hintQuesreset the hintQuesreset to set
	 */
	public void setHintQuesreset(String hintQuesreset) {
		this.hintQuesreset = hintQuesreset;
	}
	/**
	 * @return the hintQuestion1
	 */
	public String getHintQuestion1() {
		return hintQuestion1;
	}
	/**
	 * @param hintQuestion1 the hintQuestion1 to set
	 */
	public void setHintQuestion1(String hintQuestion1) {
		this.hintQuestion1 = hintQuestion1;
	}
	/**
	 * @return the corpIdList
	 */
	public List getCorpIdList() {
		return corpIdList;
	}
	/**
	 * @param corpIdList the corpIdList to set
	 */
	public void setCorpIdList(List corpIdList) {
		this.corpIdList = corpIdList;
	}
	/**
	 * @return the corpIdUserList
	 */
	public List getCorpIdUserList() {
		return corpIdUserList;
	}
	/**
	 * @param corpIdUserList the corpIdUserList to set
	 */
	public void setCorpIdUserList(List corpIdUserList) {
		this.corpIdUserList = corpIdUserList;
	}
	
	/**
	 * @return the systemDefaultEmail
	 */
	public String getSystemDefaultEmail() {
		return systemDefaultEmail;
	}
	/**
	 * @param systemDefaultEmail the systemDefaultEmail to set
	 */
	public void setSystemDefaultEmail(String systemDefaultEmail) {
		this.systemDefaultEmail = systemDefaultEmail;
	}
	/**
	 * @return the countVar
	 */
	public int getCountVar() {
		return countVar;
	}
	/**
	 * @param countVar the countVar to set
	 */
	public void setCountVar(int countVar) {
		this.countVar = countVar;
	}
	/**
	 * @return the nameOfCompany
	 */
	public String getNameOfCompany() {
		return nameOfCompany;
	}
	/**
	 * @param nameOfCompany the nameOfCompany to set
	 */
	public void setNameOfCompany(String nameOfCompany) {
		this.nameOfCompany = nameOfCompany;
	}
	/**
	 * @return the maxAttempt
	 */
	public int getMaxAttempt() {
		return maxAttempt;
	}
	/**
	 * @param maxAttempt the maxAttempt to set
	 */
	public void setMaxAttempt(int maxAttempt) {
		this.maxAttempt = maxAttempt;
	}
	/**
	 * @return the accountMessage
	 */
	public String getAccountMessage() {
		return accountMessage;
	}
	/**
	 * @param accountMessage the accountMessage to set
	 */
	public void setAccountMessage(String accountMessage) {
		this.accountMessage = accountMessage;
	}
	public Map<String, String> getOcountry() {
		return ocountry;
	}
	public void setOcountry(Map<String, String> ocountry) {
		this.ocountry = ocountry;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUserAlias() {
		return userAlias;
	}
	public void setUserAlias(String userAlias) {
		this.userAlias = userAlias;
	}
	public String getUserBranch() {
		return userBranch;
	}
	public void setUserBranch(String userBranch) {
		this.userBranch = userBranch;
	}
	public String getUserCountry() {
		return userCountry;
	}
	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserFaxNumber() {
		return userFaxNumber;
	}
	public void setUserFaxNumber(String userFaxNumber) {
		this.userFaxNumber = userFaxNumber;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserJobType() {
		return userJobType;
	}
	public void setUserJobType(String userJobType) {
		this.userJobType = userJobType;
	}
	public String getUserLastName() {
		return userLastName;
	}
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	public String getUserPhoneNumber() {
		return userPhoneNumber;
	}
	public void setUserPhoneNumber(String userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}
	public List getAgents() {
		return agents;
	}
	public void setAgents(List agents) {
		this.agents = agents;
	}
	public String getPhoneType1() {
		return phoneType1;
	}
	public void setPhoneType1(String phoneType1) {
		this.phoneType1 = phoneType1;
	}
	public String getPhoneType2() {
		return phoneType2;
	}
	public void setPhoneType2(String phoneType2) {
		this.phoneType2 = phoneType2;
	}
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public List getJobFunction() {
		return jobFunction;
	}
	public void setJobFunction(List jobFunction) {
		this.jobFunction = jobFunction;
	}
	public Partner getPartner() {
		return partner;
	}
	public void setPartner(Partner partner) {
		this.partner = partner;
	}
	public Map<String, String> getOstates() {
		return ostates;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getPartnerLastName() {
		return partnerLastName;
	}
	public void setPartnerLastName(String partnerLastName) {
		this.partnerLastName = partnerLastName;
	}
	public String getPartnerTerminalCountry() {
		return partnerTerminalCountry;
	}
	public void setPartnerTerminalCountry(String partnerTerminalCountry) {
		this.partnerTerminalCountry = partnerTerminalCountry;
	}
	public String getPartnerTerminalCountryCode() {
		return partnerTerminalCountryCode;
	}
	public void setPartnerTerminalCountryCode(String partnerTerminalCountryCode) {
		this.partnerTerminalCountryCode = partnerTerminalCountryCode;
	}
	public String getPartnerTerminalState() {
		return partnerTerminalState;
	}
	public void setPartnerTerminalState(String partnerTerminalState) {
		this.partnerTerminalState = partnerTerminalState;
	}
	/**
	 * @return the userState
	 */
	public String getUserState() {
		return userState;
	}
	/**
	 * @param userState the userState to set
	 */
	public void setUserState(String userState) {
		this.userState = userState;
	}
	/**
	 * @return the userJobType1
	 */
	public String getUserJobType1() {
		return userJobType1;
	}
	/**
	 * @param userJobType1 the userJobType1 to set
	 */
	public void setUserJobType1(String userJobType1) {
		this.userJobType1 = userJobType1;
	}
	public String getEmailMessage() {
		return emailMessage;
	}
	public void setEmailMessage(String emailMessage) {
		this.emailMessage = emailMessage;
	}
	public String getHitFlag() {
		return hitFlag;
	}
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	public String getUserAddress1() {
		return userAddress1;
	}
	public void setUserAddress1(String userAddress1) {
		this.userAddress1 = userAddress1;
	}
	public String getUserCity() {
		return userCity;
	}
	public void setUserCity(String userCity) {
		this.userCity = userCity;
	}
	public String getUserFax() {
		return userFax;
	}
	public void setUserFax(String userFax) {
		this.userFax = userFax;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getUserTelex() {
		return userTelex;
	}
	public void setUserTelex(String userTelex) {
		this.userTelex = userTelex;
	}
	public String getUserZip() {
		return userZip;
	}
	public void setUserZip(String userZip) {
		this.userZip = userZip;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public AccessInfo getAccessInfo() {
		return accessInfo;
	}
	public void setAccessInfo(AccessInfo accessInfo) {
		this.accessInfo = accessInfo;
	}
	public Long getCid() {
		return cid;
	}
	public void setCid(Long cid) {
		this.cid = cid;
	}
	public String getUserAddress2() {
		return userAddress2;
	}
	public void setUserAddress2(String userAddress2) {
		this.userAddress2 = userAddress2;
	}
	public String getUserAddress3() {
		return userAddress3;
	}
	public void setUserAddress3(String userAddress3) {
		this.userAddress3 = userAddress3;
	}
	public String getUserAddress4() {
		return userAddress4;
	}
	public void setUserAddress4(String userAddress4) {
		this.userAddress4 = userAddress4;
	}
	public String getEnbState() {
		return enbState;
	}
	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}
	public String getBucket2() {
		return bucket2;
	}
	public void setBucket2(String bucket2) {
		this.bucket2 = bucket2;
	}
	public List getStates() {
		return states;
	}
	public void setStates(List states) {
		this.states = states;
	}
	public String getUserTitle() {
		return userTitle;
	}
	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}


	public String getUserNameStatus() {
		return userNameStatus;
	}

	public void setUserNameStatus(String userNameStatus) {
		this.userNameStatus = userNameStatus;
	}
	public String getLoginMsg() {
		return loginMsg;
	}
	public void setLoginMsg(String loginMsg) {
		this.loginMsg = loginMsg;
	}
	public String getCustomerFeedback() {
		return customerFeedback;
	}
	public void setCustomerFeedback(String customerFeedback) {
		this.customerFeedback = customerFeedback;
	}
	public String getRtNumber() {
		return rtNumber;
	}
	public void setRtNumber(String rtNumber) {
		this.rtNumber = rtNumber;
	}
	public String getSoId() {
		return soId;
	}
	public void setSoId(String soId) {
		this.soId = soId;
	}
	public List getCustomerFeedbackStatus() {
		return CustomerFeedbackStatus;
	}
	public void setCustomerFeedbackStatus(List customerFeedbackStatus) {
		CustomerFeedbackStatus = customerFeedbackStatus;
	}
	public String getSurveyJob() {
		return surveyJob;
	}
	public void setSurveyJob(String surveyJob) {
		this.surveyJob = surveyJob;
	}
	public String getServices() {
		return services;
	}
	public void setServices(String services) {
		this.services = services;
	}
	public String getNotesAddForServices() {
		return notesAddForServices;
	}
	public void setNotesAddForServices(String notesAddForServices) {
		this.notesAddForServices = notesAddForServices;
	}
	public Map<String, String> getReloServiceDetails() {
		return reloServiceDetails;
	}
	public void setReloServiceDetails(Map<String, String> reloServiceDetails) {
		this.reloServiceDetails = reloServiceDetails;
	}
	public CustomerSurveyFeedback getCustomerSurveyFeedback() {
		return customerSurveyFeedback;
	}
	public void setCustomerSurveyFeedback(
			CustomerSurveyFeedback customerSurveyFeedback) {
		this.customerSurveyFeedback = customerSurveyFeedback;
	}
	public void setCustomerSurveyFeedbackManager(
			CustomerSurveyFeedbackManager customerSurveyFeedbackManager) {
		this.customerSurveyFeedbackManager = customerSurveyFeedbackManager;
	}
	public String getMMGServices() {
		return MMGServices;
	}
	public void setMMGServices(String mMGServices) {
		MMGServices = mMGServices;
	}
	public String getCounselor() {
		return counselor;
	}
	public void setCounselor(String counselor) {
		this.counselor = counselor;
	}
	public String getDaName() {
		return daName;
	}
	public void setDaName(String daName) {
		this.daName = daName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getOaName() {
		return oaName;
	}
	public void setOaName(String oaName) {
		this.oaName = oaName;
	}
	public String getEvaluationDate() {
		return evaluationDate;
	}
	public void setEvaluationDate(String evaluationDate) {
		this.evaluationDate = evaluationDate;
	}
	public String getCustService() {
		return custService;
	}
	public void setCustService(String custService) {
		this.custService = custService;
	}
	public String getComment1() {
		return comment1;
	}
	public void setComment1(String comment1) {
		this.comment1 = comment1;
	}
	public String getComment2() {
		return comment2;
	}
	public void setComment2(String comment2) {
		this.comment2 = comment2;
	}
	public String getComment3() {
		return comment3;
	}
	public void setComment3(String comment3) {
		this.comment3 = comment3;
	}
	public String getComment4() {
		return comment4;
	}
	public void setComment4(String comment4) {
		this.comment4 = comment4;
	}
	public String getComment5() {
		return comment5;
	}
	public void setComment5(String comment5) {
		this.comment5 = comment5;
	}
	public String getComment6() {
		return comment6;
	}
	public void setComment6(String comment6) {
		this.comment6 = comment6;
	}
	public String getComment7() {
		return comment7;
	}
	public void setComment7(String comment7) {
		this.comment7 = comment7;
	}
	public String getOriginAgent() {
		return originAgent;
	}
	public void setOriginAgent(String originAgent) {
		this.originAgent = originAgent;
	}
	public String getDestinAgent() {
		return destinAgent;
	}
	public void setDestinAgent(String destinAgent) {
		this.destinAgent = destinAgent;
	}
	public String getOverall() {
		return overall;
	}
	public void setOverall(String overall) {
		this.overall = overall;
	}
	public String getRecommend() {
		return recommend;
	}
	public void setRecommend(String recommend) {
		this.recommend = recommend;
	}
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	public String getPostFeedBack() {
		return postFeedBack;
	}
	public void setPostFeedBack(String postFeedBack) {
		this.postFeedBack = postFeedBack;
	}
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public String getCurrentPassword() {
		return currentPassword;
	}
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}
	public Long getSid() {
		return sid;
	}
	public void setSid(Long sid) {
		this.sid = sid;
	}
	public Map<List, Map> getSurveyUserList() {
		return surveyUserList;
	}
	public void setSurveyUserList(Map<List, Map> surveyUserList) {
		this.surveyUserList = surveyUserList;
	}
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getAddressDetails() {
		return addressDetails;
	}
	public void setAddressDetails(String addressDetails) {
		this.addressDetails = addressDetails;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getWelcomeMsg() {
		return welcomeMsg;
	}
	public void setWelcomeMsg(String welcomeMsg) {
		this.welcomeMsg = welcomeMsg;
	}
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	public void setGenericSurveyQuestionManager(
			GenericSurveyQuestionManager genericSurveyQuestionManager) {
		this.genericSurveyQuestionManager = genericSurveyQuestionManager;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public String getSurveyByUserVal() {
		return surveyByUserVal;
	}
	public void setSurveyByUserVal(String surveyByUserVal) {
		this.surveyByUserVal = surveyByUserVal;
	}
	public String getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}
	public void setReturnAjaxStringValue(String returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}
	public SurveyAnswerByUser getSurveyAnswerByUser() {
		return surveyAnswerByUser;
	}
	public void setSurveyAnswerByUser(SurveyAnswerByUser surveyAnswerByUser) {
		this.surveyAnswerByUser = surveyAnswerByUser;
	}
	public void setSurveyAnswerByUserManager(
			SurveyAnswerByUserManager surveyAnswerByUserManager) {
		this.surveyAnswerByUserManager = surveyAnswerByUserManager;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
    public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}


	
}
