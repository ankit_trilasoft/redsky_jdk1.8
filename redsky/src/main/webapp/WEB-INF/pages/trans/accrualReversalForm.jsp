<%@ include file="/common/taglibs.jsp"%> 

<head>

	<meta name="heading" content="<fmt:message key='accural.heading'/>"/> 
	<title><fmt:message key="accural.heading"/></title>
	
<style type="text/css">
		h2 {background-color: #FBBFFF}
		
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>

</head>
<body style="background-color:#444444;">
<s:form id="accrualFormNew" name="accrualFormNew" action="accrualReversalsList" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:70% " >
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Accrual Reversals</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">

 <table class="" cellspacing="1" cellpadding="1" border="0" style="width:550px" >
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  	<table class="detailTabLabel" border="0">
		  <tbody> 		  	
		  	
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	
		  	<tr>
			  	<td align="right" width="10px">
		  		<td align="right" width="70px" style="padding-bottom:5px">Select<font color="red" size="2">*</font></td>
		  				
				<td width="" class="listwhitetext" style="padding-bottom:5px;"><s:radio name="track" value="receivable" list="'Receivable'" onchange="changeStatus();"/></td>
		  	
		  					
				<td width="200px" class="listwhitetext" style="padding-bottom:5px"><s:radio name="track" value="payable" list="'Payable'" onchange="changeStatus();"/></td>
		  	</tr>
		  	
			<tr>	
				<td align="right" width="10px">			
				<td align="right" width="70px" style="padding-bottom:5px"><fmt:message key="accural.endingDate"/><font color="red" size="2">*</font></td>
				
				<td align="right" style="padding-bottom:5px; padding-left:7px;"><s:textfield cssClass="input-text" id="date" name="date" value="%{date}" size="8" maxlength="11" readonly="true" /> </td><td><img id="date_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  	</tr>		  	
		  	<tr>
		  				  	  		
	  			<td colspan="2"></td>			
				<td colspan="4" style="padding-left:6px;"><s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " align="top" method="listReversals" value="Search"  onclick="return valbutton(accrualFormNew);"/>	  		 				           	
				<s:reset cssClass="cssbutton" key="Clear" cssStyle="width:70px; height:25px "/>   
				</td>
        	</tr>
        	
        	</table>
        	<tr>
		  		<td align="left" height="20px"></td>
		  	</tr>
		</tbody>
	</table>
	        	      </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
</s:form>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="javascript" type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();	
</script>f
<script language="javascript" type="text/javascript">
		
		function valbutton(thisform) {
		myOption = -1;
		for (i=thisform.track.length-1; i > -1; i--) {
		if (thisform.track[i].checked) {
		myOption = i; i = -1;
		}
		}
		if (myOption == -1) {
		alert("You must select a radio button");
		return false;
		}
		
	return checkDateField();
		
}

function checkDateField(){

	if(document.forms['accrualFormNew'].elements['date'].value=='')
	{
		alert('Please enter the date');
		return false;
	}

}
		
		
</script>


		