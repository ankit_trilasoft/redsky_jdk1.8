
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MSSOrderCrate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MSSOrderCrate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OorD_Only" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Length" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Width" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Height" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="COD" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PlyWood" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="BugWood" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PendingAuthorization" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MSSOrderCrate", propOrder = {
    "oorDOnly",
    "description",
    "length",
    "width",
    "height",
    "cod",
    "plyWood",
    "bugWood",
    "pendingAuthorization"
})
public class MSSOrderCrate {

    @XmlElement(name = "OorD_Only")
    protected String oorDOnly;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "Length")
    protected double length;
    @XmlElement(name = "Width")
    protected double width;
    @XmlElement(name = "Height")
    protected double height;
    @XmlElement(name = "COD")
    protected boolean cod;
    @XmlElement(name = "PlyWood")
    protected boolean plyWood;
    @XmlElement(name = "BugWood")
    protected boolean bugWood;
    @XmlElement(name = "PendingAuthorization")
    protected boolean pendingAuthorization;

    /**
     * Gets the value of the oorDOnly property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOorDOnly() {
        return oorDOnly;
    }

    /**
     * Sets the value of the oorDOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOorDOnly(String value) {
        this.oorDOnly = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the length property.
     * 
     */
    public double getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     */
    public void setLength(double value) {
        this.length = value;
    }

    /**
     * Gets the value of the width property.
     * 
     */
    public double getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     */
    public void setWidth(double value) {
        this.width = value;
    }

    /**
     * Gets the value of the height property.
     * 
     */
    public double getHeight() {
        return height;
    }

    /**
     * Sets the value of the height property.
     * 
     */
    public void setHeight(double value) {
        this.height = value;
    }

    /**
     * Gets the value of the cod property.
     * 
     */
    public boolean isCOD() {
        return cod;
    }

    /**
     * Sets the value of the cod property.
     * 
     */
    public void setCOD(boolean value) {
        this.cod = value;
    }

    /**
     * Gets the value of the plyWood property.
     * 
     */
    public boolean isPlyWood() {
        return plyWood;
    }

    /**
     * Sets the value of the plyWood property.
     * 
     */
    public void setPlyWood(boolean value) {
        this.plyWood = value;
    }

    /**
     * Gets the value of the bugWood property.
     * 
     */
    public boolean isBugWood() {
        return bugWood;
    }

    /**
     * Sets the value of the bugWood property.
     * 
     */
    public void setBugWood(boolean value) {
        this.bugWood = value;
    }

    /**
     * Gets the value of the pendingAuthorization property.
     * 
     */
    public boolean isPendingAuthorization() {
        return pendingAuthorization;
    }

    /**
     * Sets the value of the pendingAuthorization property.
     * 
     */
    public void setPendingAuthorization(boolean value) {
        this.pendingAuthorization = value;
    }

}
