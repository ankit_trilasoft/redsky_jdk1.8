<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Maintain Network Partner List</title>
<meta name="heading" content="Maintain Network Partner List" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style type="text/css">
	.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:4px 3px 1px 5px; height:15px;width:598px; border:1px solid #99BBE8; border-right:none; border-left:none} 
	a.dsphead{text-decoration:none;color:#000000;}
	.dspchar2{padding-left:0px;}
</style>
<style>
	.input-textarea{
		border:1px solid #219DD1;
		color:#000000;
		font-family:arial,verdana;
		font-size:12px;
		height:45px;
		text-decoration:none;
	}


</style>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script>

<script language="javascript" type="text/javascript">

</script>

</head> 
<s:form name="maintainNetworkPartnerList" id="maintainNetworkPartnerList" action="" method="post" validate="true">
<s:hidden name="selectedCorpId" value="<%=request.getParameter("selectedCorpId") %>"/>
<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="dd-NNN-yy"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>

<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="dd-NNN-yy"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>
<div id="Layer1" style="width:100%;">
<div class="spnblk">&nbsp;</div>
<div id="Layer5" style="width:100%;">
	
	<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Maintain Network Partner List</span></a></li>
		  </ul>
	</div>
	<div class="spnblk">&nbsp;</div>
	<s:set name="networkPartnerList" value="networkPartnerList" scope="request" />
	<display:table name="networkPartnerList" class="table" requestURI="" id="networkPartnerList" export="true" pagesize="50" style="margin-top: 1px;!margin-top: -1px;">
	<display:column property="corpId" sortable="true" title="Corp Id" href="editNetworkPartner.html?selectedCorpId=${selectedCorpId}" paramId="id" paramProperty="id" style="width:80px"/>
    <display:column property="agentCorpId" sortable="true" title="Agent CorpId" style="width:80px"/>
    <display:column property="agentPartnerCode" sortable="true" title="Agent PartnerCode" style="width:80px"/>
    <display:column property="agentLocalPartnerCode" sortable="true" title="Agent Local PartnerCode" style="width:80px"/>
    <display:column property="agentCompanyDivision" sortable="true" title="Agent CompanyDivision" style="width:80px"/>
    <display:column property="agentName" sortable="true" title="Agent Name" style="width:180px"/>
    </display:table>
	<c:set var="buttons"/>
	<input type="button" class="cssbuttonA" value="Add"  style="width:65px; height:25px" onclick="location.href='<c:url value="/editNetworkPartner.html?selectedCorpId=${selectedCorpId}"/>'" /></td>  
	<c:out value="${buttons}" escapeXml="false" />
	</div>
</div>  

</s:form>

<script type="text/javascript">  
</script>

