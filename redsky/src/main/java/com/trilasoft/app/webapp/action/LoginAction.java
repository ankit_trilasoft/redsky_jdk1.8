package com.trilasoft.app.webapp.action;

import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID; 

import javax.servlet.ServletContext; 

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.ui.rememberme.TokenBasedRememberMeServices;
import org.apache.log4j.Logger;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.model.UserDevice;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction; 

import com.trilasoft.app.init.AppInitServlet; 
import com.trilasoft.app.model.CompanySystemDefault; 
import com.trilasoft.app.model.Userlogfile;
import com.trilasoft.app.service.ClaimManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CompanySystemDefaultManager;
import com.trilasoft.app.service.CustomerFileManager; 
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.UserDeviceManager;
import com.trilasoft.app.service.UserlogfileManager; 

import javax.servlet.http.Cookie;

public class LoginAction  extends BaseAction  {
	
	private User user;
	private String userType;
	String parentAgent;
	private String sessionCorpID; 
	private String sessionExp; 
	private String partnerDetailLocation1; 
	private String partnerCompanyDivision=""; 
	private String vanLineAffiliation;  
	private String utsNumber;  
	private String decorator; 
	private String companyNameString;
	private String landingPageWelcomeMassage;
	private String globalSessionCorp;
	private String soLastName;
	private String soTab;
	private String userPortalCheckType;
	private String soSortOrder;
	private String csoSortOrder;
	private String ticketSortOrder;
	private String orderForJob;
	private String orderForCso;
	private String orderForTicket;
	private String defaultCss; 
	private Boolean visibilityFornewAccountline=false;
	private boolean cportalAccessCompanyDivisionLevel=false;
	private boolean roleSecurityAdmin=true;
	private List userDeviceList = new ArrayList();
	private List accountImageList;
	private UserManager userManager;
	private CompanySystemDefaultManager companySystemDefaultManager;
	private CustomerFileManager customerFileManager; 
	private ClaimManager claimManager;
	private CompanyManager companyManager;
	private UserlogfileManager  userlogfileManager;
	private UserDeviceManager userDeviceManager;
	private RefMasterManager refMasterManager;
	private CompanySystemDefault companySystemDefault;
	private List <CompanySystemDefault> companySystemDefaultList ;
	private  Map<String, String> PWD_HINT;
	static final Logger logger = Logger.getLogger(LoginAction.class);
	Date currentdate = new Date(); 
	public LoginAction(){
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth != null){
			user = (User) auth.getPrincipal();
			userType = user.getUserType();
			this.sessionCorpID = user.getCorpID();
			logger.warn("USER AUDIT: ID: " + currentdate + " ## User: " + user.getUsername() + " sessionCorpID: "+sessionCorpID + " userType: "+userType);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	} 
	
	public String findUserDetails() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
		user = (User) userManager.findUserDetails(getRequest().getRemoteUser()).get(0);	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+user);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User corpid: "+user.getCorpID());
		companySystemDefault = companySystemDefaultManager.getByCorpid(user.getCorpID());
		System.out.println("\n\n\n\n companySystemDefault"+companySystemDefault);
		if(companySystemDefault !=null ){  
		if(companySystemDefault !=null && (!(companySystemDefault.getStatus()))){ 
			sessionExp="true";
			ServletContext sc =getRequest().getSession(true).getServletContext();
    		sc.setAttribute("restrictLoginMsg", "Your company is not using RedSky.");
			getRequest().getSession().setAttribute("sessionExp", sessionExp);
			return "rajeshlogin";
		}
		
		if(user.getUserType().equalsIgnoreCase("ACCOUNT")){
			
			accountImageList=claimManager.findUserByBasedAt(user.getBasedAt(),sessionCorpID);
			if(accountImageList!=null && (!(accountImageList.isEmpty())) && accountImageList.get(0)!=null){
			partnerDetailLocation1=(accountImageList.get(0)!=null?accountImageList.get(0).toString():"");
			}
			
			 
			if(companySystemDefault!=null){
				cportalAccessCompanyDivisionLevel=companySystemDefault.getCportalAccessCompanyDivisionLevel();
			}
			if(cportalAccessCompanyDivisionLevel){
			List accountCompanyDivision=claimManager.findUserCompanyDivision(user.getBasedAt(),sessionCorpID);	
			if(accountCompanyDivision!=null && (!(accountCompanyDivision.isEmpty())) && accountCompanyDivision.get(0)!=null){
				partnerCompanyDivision=accountCompanyDivision.get(0).toString();
			}
			}			
		}
		
	
		if(user.getUserType().equalsIgnoreCase("AGENT")){
			this.parentAgent=user.getParentAgent();
			if(parentAgent==null || parentAgent.equals("")){
				parentAgent=user.getBasedAt();
			}
			List partnerDetails=customerFileManager.getPartnerDetails(parentAgent,sessionCorpID);
			if(partnerDetails!=null && !partnerDetails.isEmpty()){
			Iterator it = partnerDetails.iterator();
			while (it.hasNext()) {
				Object[] object = (Object[]) it.next();
				if(object[0]!=null && object[0].toString().contains("United")){
					vanLineAffiliation="United";	
				}
				if(object[1]!=null){
					utsNumber=object[1].toString();	
				}
			}
			}	
		}
		getRequest().getSession().setAttribute("userType", user.getUserType());
		getRequest().getSession().setAttribute("newsUpdateFlag", user.isNewsUpdateFlag());		
		try{
			sessionExp="false";
			getRequest().getSession().setAttribute("sessionExp", sessionExp);
		Date passwordExp=user.getPwdexpiryDate();
		Date cal = new Date();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        formatter.setTimeZone(TimeZone.getTimeZone(companySystemDefault.getTimeZone())); 
        String pwdExpDate =formatter.format(cal);
		// need to check in java calculation
        
        List dateDiffExp=companyManager.getDateDiffExp(user.getUsername(), sessionCorpID, passwordExp);
		List dateDiffAlert=companyManager.getDateDiffAlert(user.getUsername(), sessionCorpID, passwordExp, pwdExpDate);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: dateDiffAlert  "+dateDiffAlert);
		if(Long.parseLong(dateDiffAlert.get(0).toString())<=10 && Long.parseLong(dateDiffAlert.get(0).toString())>=0){
			String clickHere = "window.open('resetAlertPassword.html?decorator=popup&popup=true','forms','height=300,width=530,top=120,left=150')";
			saveMessage("Your password will expire in "+dateDiffAlert.get(0).toString()+" days, please click <b><a style='text-decoration: underline; cursor: pointer;' onclick="+clickHere+">here</a></b> to change your password, to extend the expiry date.");
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: dateDiffExp  "+dateDiffExp);
		if(Long.parseLong(dateDiffExp.get(0).toString())>0)
		{
		String expAction="";
		 if((user.getUserType().toString().trim().equalsIgnoreCase("ACCOUNT"))  || (user.getUserType().toString().trim().equalsIgnoreCase("AGENT")) || (user.getUserType().toString().trim().equalsIgnoreCase("PARTNER")))
		 {       
			 expAction=companySystemDefault.getPartnerActionOnExpiry();
		}else
		{
			expAction=companySystemDefault.getUserActionOnExpiry();
		}
			
		if(expAction.equalsIgnoreCase("reset"))
		{
			PWD_HINT = refMasterManager.findByParameter(sessionCorpID, "PWD_HINT");
			saveMessage("Please reset your password as it has been expired per the company security policies.");
			return "rajesh";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: expAction  "+expAction);
		if(expAction.equalsIgnoreCase("expire"))
		{
			userManager.setExpirePassword(user.getUsername(), sessionCorpID);
			saveMessage("Your userid has been expired, please contact support@redskymobility.com for re-activation.");
			sessionExp="true";
			getRequest().getSession().setAttribute("sessionExp", sessionExp);
			return "rajeshlogin";
		}
		}
		if(companySystemDefault.getRestrictAccess() && user.getUserType().trim().equalsIgnoreCase("USER")){
			
			Set<Role> roles  = user.getRoles();
	        Role role=new Role();
	        Iterator it = roles.iterator();
	        while(it.hasNext()) {
	        	role=(Role)it.next();
	        	String userRole = role.getName();
	        	if(userRole.equalsIgnoreCase("ROLE_SECURITY_ADMIN")){
	        		roleSecurityAdmin=false;
	        		break;
	        	}
	        	
	        }
	        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: roleSecurityAdmin  "+roleSecurityAdmin);
	        if(roleSecurityAdmin){
			String dnsclient="";
				List nameservers = sun.net.dns.ResolverConfiguration.open().nameservers();
				try{
					for( Object dns : nameservers ) {
						dnsclient=dns.toString();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			String IPaddress=getRequest().getRemoteAddr(); 
			boolean loginFlag=false;
			nameservers.add(IPaddress);
			String excludeIpCheck = "";
			String [] includeIpData = companySystemDefault.getExcludeIPs().split(",");
			for (String includeIp:includeIpData){
				if(nameservers.contains(includeIp)){
					loginFlag=true;
					break;
				}
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: ExcludeIPs  "+companySystemDefault.getExcludeIPs());
			if((companySystemDefault.getExcludeIPs()!=null && !companySystemDefault.getExcludeIPs().equals("")) && (!loginFlag)){
			sessionExp="true";
			ServletContext sc =getRequest().getSession(true).getServletContext();
    		sc.setAttribute("restrictLoginMsg", companySystemDefault.getRestrictedMassage());
			getRequest().getSession().setAttribute("sessionExp", sessionExp);
			return "rajeshlogin";
			}else getRequest().getSession().removeAttribute(sessionExp);
		}
	        }
		}catch(Exception ex){
			ex.printStackTrace();
		} 
		decorator = "pwdreset"; 
		companyNameString=companySystemDefault.getCompanyName();
		landingPageWelcomeMassage=companySystemDefault.getLandingPageWelcomeMsg();
		getRequest().getSession().setAttribute("globalSessionCorp",user.getCorpID());
		globalSessionCorp = getRequest().getSession().getAttribute("globalSessionCorp").toString();
		getRequest().setAttribute("soLastName","");
		soLastName=getRequest().getAttribute("soLastName").toString();
		String permKey = sessionCorpID +"-"+"module.tab.serviceorder.newAccountingTab";
        visibilityFornewAccountline=AppInitServlet.roleBasedComponentPerms.containsKey(permKey);
        if(visibilityFornewAccountline && user.getDefaultSoURL()!=null && user.getDefaultSoURL().toString().equalsIgnoreCase("accountLineList.html?sid=")){
        	getRequest().getSession().setAttribute("soTab", "pricingList.html?sid=");
    		soTab="pricingList.html?sid=";
        }else{
		getRequest().getSession().setAttribute("soTab", user.getDefaultSoURL());
		soTab=user.getDefaultSoURL();
        }
		getRequest().getSession().setAttribute("userPortalCheckType", user.getUserType());
		userPortalCheckType=user.getUserType(); 
		getRequest().getSession().setAttribute("soSortOrder", user.getDefaultSortForJob());
		soSortOrder=user.getDefaultSortForJob();
		getRequest().getSession().setAttribute("csoSortOrder", user.getDefaultSortForCso());
		csoSortOrder=user.getDefaultSortForCso();
		getRequest().getSession().setAttribute("ticketSortOrder", user.getDefaultSortForTicket());
		ticketSortOrder=user.getDefaultSortForTicket();
		getRequest().getSession().setAttribute("orderForJob", user.getSortOrderForJob());
		orderForJob=user.getSortOrderForJob();
		getRequest().getSession().setAttribute("orderForCso", user.getSortOrderForCso());
		orderForCso=user.getSortOrderForCso();
		getRequest().getSession().setAttribute("orderForTicket", user.getSortOrderTicket());
		orderForTicket=user.getSortOrderTicket();
		getRequest().getSession().setAttribute("defaultCss", user.getDefaultCss());
		getRequest().getSession().setAttribute("newsUpdateFlag", user.isNewsUpdateFlag());
		if(user.getDefaultCss()!=null && (!(user.getDefaultCss().equalsIgnoreCase("")))){
			defaultCss=user.getDefaultCss();
		}else{
			defaultCss="blue";
		} 
		if (user.isPasswordReset()) {
			PWD_HINT = refMasterManager.findByParameter(sessionCorpID, "PWD_HINT");
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		} else {
			try{
				
			}
			catch(Exception e)
			{
				 e.printStackTrace();	
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return INPUT;
		}
		}else{ 
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: Login error, please try after some time");
			return "LoginError";
		}
		}catch(Exception e){
			 e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return INPUT;
	}
	
	public String  findUserLogOut(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		if (getRequest().getSession(false) != null) {
			int i=userlogfileManager.updateUserLogOut(getSession().getId());
			getSession().invalidate();
		}
		Cookie terminate = new Cookie(TokenBasedRememberMeServices.ACEGI_SECURITY_HASHED_REMEMBER_ME_COOKIE_KEY, null);
		String contextPath = getRequest().getContextPath();
		terminate.setPath(contextPath != null && contextPath.length() > 0 ? contextPath : "/");
		terminate.setMaxAge(0);
		getResponse().addCookie(terminate);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	
	public void userlogfileSave(User user) throws UnknownHostException{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
		String userAgentDetails="";
		try{ 
			userAgentDetails=getRequest().getHeader("User-Agent");
		}catch(Exception e){
			 e.printStackTrace();
		 }
		String dnsclient="";
	   
		 try{
			List nameservers = sun.net.dns.ResolverConfiguration.open().nameservers();
			for( Object dns : nameservers ) {
				dnsclient=dns.toString();
			}
		}catch(Exception e){
			 e.printStackTrace();
		 }
		String IPaddress=getRequest().getRemoteAddr(); 
		Userlogfile userlogfile=new Userlogfile();
		userlogfile.setUserName(user.getUsername());
		userlogfile.setUserType(user.getUserType());
		userlogfile.setCorpID(user.getCorpID()); 
		userlogfile.setIPaddress(IPaddress);
		userlogfile.setDNS(dnsclient);
		userlogfile.setLogin(new Date());
		userlogfile.setSessionId(getSession().getId());
		userlogfile.setUserAgentDetails(userAgentDetails);
		userlogfile = userlogfileManager.save(userlogfile);  
		getSession().getCreationTime();
		user.getUsername(); 
		
		try{
			String deviceIdVal="";
			Cookie[] cookies = getRequest().getCookies();
		        if (cookies != null) {
		            for (Cookie cookie : cookies) {
		            	if (cookie.getName().equals("d_id")) {
		            		deviceIdVal = cookie.getValue();
		            	}
		            }
		        }
		    if(!deviceIdVal.equalsIgnoreCase("")){
		    	userDeviceList = userDeviceManager.findRecordByLoginUser(user.getUsername(), deviceIdVal);
		    }
			if(userDeviceList==null || userDeviceList.isEmpty()){
				String uuid = UUID.randomUUID().toString();
				Cookie dIdCookie = new Cookie("d_id", uuid);
				String contextPath = getRequest().getContextPath();
				dIdCookie.setPath(contextPath != null && contextPath.length() > 0 ? contextPath : "/");
				dIdCookie.setMaxAge(60 * 60 * 24 * 365 * 1);
				getResponse().addCookie(dIdCookie);
				
				UserDevice usrDevice = new UserDevice();
				usrDevice.setUserName(user.getUsername());
				usrDevice.setDeviceId(uuid);
				usrDevice.setCreatedOn(new Date());
				usrDevice.setUserLogFileId(userlogfile.getId());
				usrDevice.setCorpID(user.getCorpID());
				userDeviceManager.save(usrDevice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}


	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}


	public void setCompanySystemDefaultManager(
			CompanySystemDefaultManager companySystemDefaultManager) {
		this.companySystemDefaultManager = companySystemDefaultManager;
	}


	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}


	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	} 

	public void setUserlogfileManager(UserlogfileManager userlogfileManager) {
		this.userlogfileManager = userlogfileManager;
	}

	public void setUserDeviceManager(UserDeviceManager userDeviceManager) {
		this.userDeviceManager = userDeviceManager;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getSessionExp() {
		return sessionExp;
	}

	public void setSessionExp(String sessionExp) {
		this.sessionExp = sessionExp;
	}

	public String getPartnerDetailLocation1() {
		return partnerDetailLocation1;
	}

	public void setPartnerDetailLocation1(String partnerDetailLocation1) {
		this.partnerDetailLocation1 = partnerDetailLocation1;
	}

	public String getPartnerCompanyDivision() {
		return partnerCompanyDivision;
	}

	public void setPartnerCompanyDivision(String partnerCompanyDivision) {
		this.partnerCompanyDivision = partnerCompanyDivision;
	}

	public String getVanLineAffiliation() {
		return vanLineAffiliation;
	}

	public void setVanLineAffiliation(String vanLineAffiliation) {
		this.vanLineAffiliation = vanLineAffiliation;
	}

	public String getUtsNumber() {
		return utsNumber;
	}

	public void setUtsNumber(String utsNumber) {
		this.utsNumber = utsNumber;
	}

	public String getCompanyNameString() {
		return companyNameString;
	}

	public void setCompanyNameString(String companyNameString) {
		this.companyNameString = companyNameString;
	}

	public String getLandingPageWelcomeMassage() {
		return landingPageWelcomeMassage;
	}

	public void setLandingPageWelcomeMassage(String landingPageWelcomeMassage) {
		this.landingPageWelcomeMassage = landingPageWelcomeMassage;
	}

	public String getGlobalSessionCorp() {
		return globalSessionCorp;
	}

	public void setGlobalSessionCorp(String globalSessionCorp) {
		this.globalSessionCorp = globalSessionCorp;
	}

	public String getSoLastName() {
		return soLastName;
	}

	public void setSoLastName(String soLastName) {
		this.soLastName = soLastName;
	}

	public String getSoTab() {
		return soTab;
	}

	public void setSoTab(String soTab) {
		this.soTab = soTab;
	}

	public String getSoSortOrder() {
		return soSortOrder;
	}

	public void setSoSortOrder(String soSortOrder) {
		this.soSortOrder = soSortOrder;
	}

	public String getCsoSortOrder() {
		return csoSortOrder;
	}

	public void setCsoSortOrder(String csoSortOrder) {
		this.csoSortOrder = csoSortOrder;
	}

	public String getTicketSortOrder() {
		return ticketSortOrder;
	}

	public void setTicketSortOrder(String ticketSortOrder) {
		this.ticketSortOrder = ticketSortOrder;
	}

	public String getOrderForJob() {
		return orderForJob;
	}

	public void setOrderForJob(String orderForJob) {
		this.orderForJob = orderForJob;
	}

	public String getOrderForCso() {
		return orderForCso;
	}

	public void setOrderForCso(String orderForCso) {
		this.orderForCso = orderForCso;
	}

	public String getOrderForTicket() {
		return orderForTicket;
	}

	public void setOrderForTicket(String orderForTicket) {
		this.orderForTicket = orderForTicket;
	}

	public String getDefaultCss() {
		return defaultCss;
	}

	public void setDefaultCss(String defaultCss) {
		this.defaultCss = defaultCss;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUserPortalCheckType() {
		return userPortalCheckType;
	}

	public void setUserPortalCheckType(String userPortalCheckType) {
		this.userPortalCheckType = userPortalCheckType;
	}

	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getPWD_HINT() {
		return PWD_HINT;
	}

	public void setPWD_HINT(Map<String, String> pWD_HINT) {
		PWD_HINT = pWD_HINT;
	}

	public String getDecorator() {
		return decorator;
	}

	public void setDecorator(String decorator) {
		this.decorator = decorator;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

}
