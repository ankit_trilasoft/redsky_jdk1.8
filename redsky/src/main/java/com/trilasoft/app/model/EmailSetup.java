package com.trilasoft.app.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name="emailsetup")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class EmailSetup extends BaseObject{
	
	private Long id;
	private Integer retryCount;
	private String recipientTo;
	private String recipientCc;
	private String recipientBcc;
	private String subject;
	private String body;
	private String signature;
	private String signaturePart;
	private String attchedFileLocation;
	private Date dateSent;
	private String emailStatus;
	private String corpId;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	private Date createdOn;
	private String fileNumber;
	private String module;
	private String saveAs;
	private String lastSentMailBody;
	private Date emailUploadedOn;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("retryCount", retryCount)
				.append("recipientTo", recipientTo)
				.append("recipientCc", recipientCc)
				.append("recipientBcc", recipientBcc)
				.append("subject", subject).append("body", body)
				.append("signature", signature)
				.append("signaturePart", signaturePart)
				.append("attchedFileLocation", attchedFileLocation)
				.append("dateSent", dateSent)
				.append("emailStatus", emailStatus).append("corpId", corpId)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("createdOn", createdOn)
				.append("fileNumber", fileNumber)
				.append("saveAs", saveAs)
				.append("lastSentMailBody", lastSentMailBody)
				.append("emailUploadedOn", emailUploadedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof EmailSetup))
			return false;
		EmailSetup castOther = (EmailSetup) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(retryCount, castOther.retryCount)
				.append(recipientTo, castOther.recipientTo)
				.append(recipientCc, castOther.recipientCc)
				.append(recipientBcc, castOther.recipientBcc)
				.append(subject, castOther.subject)
				.append(body, castOther.body)
				.append(signature, castOther.signature)
				.append(signaturePart, castOther.signaturePart)
				.append(attchedFileLocation, castOther.attchedFileLocation)
				.append(dateSent, castOther.dateSent)
				.append(emailStatus, castOther.emailStatus)
				.append(corpId, castOther.corpId)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(createdOn, castOther.createdOn)
				.append(fileNumber, castOther.fileNumber)
				.append(saveAs, castOther.saveAs)
				.append(lastSentMailBody, castOther.lastSentMailBody)
				.append(emailUploadedOn, castOther.emailUploadedOn)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(retryCount)
				.append(recipientTo).append(recipientCc).append(recipientBcc)
				.append(subject).append(body).append(signature)
				.append(signaturePart).append(attchedFileLocation)
				.append(dateSent).append(emailStatus).append(corpId)
				.append(createdBy).append(updatedBy).append(updatedOn)
				.append(createdOn).append(fileNumber).append(saveAs).append(lastSentMailBody)
				.append(emailUploadedOn)
				.toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=5)	
	public Integer getRetryCount() {
		return retryCount;
	}
	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}
	@Column(length=200)	
	public String getRecipientTo() {
		return recipientTo;
	}
	public void setRecipientTo(String recipientTo) {
		this.recipientTo = recipientTo;
	}
	@Column(length=200)	
	public String getRecipientCc() {
		return recipientCc;
	}
	public void setRecipientCc(String recipientCc) {
		this.recipientCc = recipientCc;
	}
	@Column(length=200)	
	public String getRecipientBcc() {
		return recipientBcc;
	}
	public void setRecipientBcc(String recipientBcc) {
		this.recipientBcc = recipientBcc;
	}
	@Column(length=200)	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	@Column(length=10000)	
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	@Column(length=200)	
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	@Column(length=45)	
	public String getAttchedFileLocation() {
		return attchedFileLocation;
	}
	public void setAttchedFileLocation(String attchedFileLocation) {
		this.attchedFileLocation = attchedFileLocation;
	}
	@Column
	public Date getDateSent() {
		return dateSent;
	}
	public void setDateSent(Date dateSent) {
		this.dateSent = dateSent;
	}
	@Column(length=500)	
	public String getEmailStatus() {
		return emailStatus;
	}
	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}
	@Column(length=30)	
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column(length=45)	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(length=45)	
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=200)	
	public String getSignaturePart() {
		return signaturePart;
	}
	public void setSignaturePart(String signaturePart) {
		this.signaturePart = signaturePart;
	}
	@Column( length=25, unique= true )
	public String getFileNumber() {
		return fileNumber;
	}
	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}
	@Column
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getSaveAs() {
		return saveAs;
	}
	public void setSaveAs(String saveAs) {
		this.saveAs = saveAs;
	}
	public String getLastSentMailBody() {
		return lastSentMailBody;
	}
	public void setLastSentMailBody(String lastSentMailBody) {
		this.lastSentMailBody = lastSentMailBody;
	}
	@Column
	public Date getEmailUploadedOn() {
		return emailUploadedOn;
	}
	public void setEmailUploadedOn(Date emailUploadedOn) {
		this.emailUploadedOn = emailUploadedOn;
	}
}
