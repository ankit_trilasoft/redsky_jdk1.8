<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="/common/tooltip.jsp"%>
<head>   
    <title>Event List</title>   
    <meta name="heading" content="Event List"/>   

<%-- Modified By Kunal Sharma at 13-Jan-2012 --%> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<%-- Modification closed here --%>	
<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:3px;
!margin-bottom:1px;
margin-top:-21px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>

	
	<script language="javascript" type="text/javascript">
	function clear_fields(){    
document.forms['eventListForm'].elements['driverCode'].value = "";
document.forms['eventListForm'].elements['driverFirstName'].value = "";
document.forms['eventListForm'].elements['driverLastName'].value = "";
document.forms['eventListForm'].elements['eventFrom'].value = "";
document.forms['eventListForm'].elements['eventTo'].value = "";
}
	
function autoPopulate_Date(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		//targetElement.form.elements['surveyListForm.surveyFrom'].value=datam;
		document.forms['eventListForm'].elements['eventFrom'].value=datam;
	}
	
	function autoPopulate_DateTo(targetElement) {
		var mydate=new Date();
        mydate.setDate(mydate.getDate() + 30);
		var daym;
		var year=mydate.getFullYear();
		var y = ""+year;
		if (year < 1000)
		year+=1900;
		var month=mydate.getMonth()+1;
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate();
		if (daym<10)
		daym="0"+daym;
		var datam = (daym)+"-"+month+"-"+y.substring(2,4);
		document.forms['eventListForm'].elements['eventTo'].value=datam;
	}
	
function copyFromDate(){
 var date1 = ''; 
 date1 = document.forms['eventListForm'].elements['date1'].value; 
 if(date1 !='')
 {
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   
   var finalDate = month+"-"+day+"-"+year;
  
   date1 = finalDate.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
   sDate.setDate(sDate.getDate() + 30);
   var df1 = sDate.format("dd-mmm-yy");
  document.forms['eventListForm'].elements['date2'].value=df1;
  }
 }
 	function isDate() {
	var surFrom = document.forms['eventListForm'].elements['eventFrom'].value;
	var surTo = document.forms['eventListForm'].elements['eventTo'].value;
	if (surFrom=='' || surTo==''){
	alert('Please select From Date and To Date');
	   return false;
	   } else {
	   return isValidDate();
	  }  
	}
function isValidDate(){
 var date1 = ''; 
 var date2 = '';
 var date1 = document.forms['eventListForm'].elements['eventFrom'].value;
 var date2 = document.forms['eventListForm'].elements['eventTo'].value; 
 if (date1 != ''){
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   
   var finalDate = month+"-"+day+"-"+year;
   }
   
   if (date2 != ''){
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
   } 
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
   sDate.setDate(sDate.getDate() + 30);
   var df1 = sDate.format("dd-mmm-yy");
  if(date1 !='' && date2 =='') {
  document.forms['eventListForm'].elements['eventTo'].value=df1;
  return true;
  } else if (date2 !='' && daysApart<0){
  alert("From Date should be less than To Date");
   return false;
  }  
 }
 
function editCalendar(targetElement2){
	
	document.forms['eventListForm'].action='editDriverEvent.html?from=list&id='+targetElement2;
	document.forms['eventListForm'].submit();
} 

function confirmSubmit(targetElement){

	var surveyFrom = document.forms['eventListForm'].elements['eventFrom'].value;
	var surveyTo = document.forms['eventListForm'].elements['eventTo'].value;
	var agree=confirm("Are you sure you want to Delete this Driver Event?");
	var did = targetElement;
	if (agree){
		location.href='driverDeleted.html?id='+did+'&eventFrom='+surveyFrom+'&eventTo='+surveyTo+'&from=Driver';
	}else{
		return false;
	}
}
function checkDate()
{
 var date1 = document.forms['eventListForm'].elements['eventFrom'].value;	 
 var date2 = document.forms['eventListForm'].elements['eventTo'].value; 
   if((date1=="")||(date2==""))
   {
   alert("Please enter date to search");
   return false;
   }
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')   {       month = "01";   }
   else if(month == 'Feb')   {       month = "02";   }
   else if(month == 'Mar')   {       month = "03"   }
   else if(month == 'Apr')   {       month = "04"   }
   else if(month == 'May')   {       month = "05"   }
   else if(month == 'Jun')   {       month = "06"   }
   else if(month == 'Jul')   {       month = "07"   }
   else if(month == 'Aug')   {       month = "08"   }
   else if(month == 'Sep')   {       month = "09"   }
   else if(month == 'Oct')   {       month = "10"   }
   else if(month == 'Nov')   {       month = "11"   }
   else if(month == 'Dec')   {       month = "12";   }   
   var finalDate = month+"-"+day+"-"+year;
   
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')   {       month2 = "01";   }
   else if(month2 == 'Feb')   {       month2 = "02";   }
   else if(month2 == 'Mar')   {       month2 = "03"   }
   else if(month2 == 'Apr')   {       month2 = "04"   }
   else if(month2 == 'May')   {       month2 = "05"   }
   else if(month2 == 'Jun')   {       month2 = "06"   }
   else if(month2 == 'Jul')   {       month2 = "07"   }
   else if(month2 == 'Aug')   {       month2 = "08"   }
   else if(month2 == 'Sep')   {       month2 = "09"   }
   else if(month2 == 'Oct')   {       month2 = "10"   }
   else if(month2 == 'Nov')   {       month2 = "11"   }
   else if(month2 == 'Dec')   {       month2 = "12";   }
   var finalDate2 = month2+"-"+day2+"-"+year2;

  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);  
  if(daysApart<0)
  {
      alert("From Date should be less than To Date");
    document.forms['eventListForm'].elements['eventTo'].value='';
    document.forms['eventListForm'].elements['eventFrom'].focus();
    return false;
  }
}
		</script>
	</head>
<s:form id="eventListForm" action="searchEventsList.html?from=search" method="post" onsubmit="return checkDate();">	
<c:set var="from" value="<%=request.getParameter("from") %>" />
<s:hidden name="from" value="<%=request.getParameter("from") %>" />
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:65px; height:25px" onclick="document.forms['eventListForm'].action='editDriverEvent.html';document.forms['eventListForm'].submit();" value="Add Event"/>   
</c:set>   
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" align="top" key="button.search" /> 
	 <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
</c:set>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>


<div id="Layer1" style="width:100%;">

<table class="" cellspacing="1" cellpadding="0" border="0" width="100%">
	<tbody>
		<tr>
			<td>   
				<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
				
						<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:-20px;!margin-top:-5px;"><span></span></div>
<div class="center-content">
				
				<table class="table" width="100%" >
				<thead>
				<tr>
				<th>Driver Code #</th>
				<th>First Name</th>
				<th>Last/Company Name</th>
				<th>From Date</th>
				<th>To Date</th>
				</tr>
				</thead>	
				<tbody>
				<tr>
					<td>
						    <s:textfield name="driverCode" cssClass="input-text" id="driverCode" cssStyle="width:75px" maxlength="8"/>
					</td>
					<td>
							<s:textfield name="driverFirstName" cssClass="input-text" id="driverFirstName" cssStyle="width:150px" maxlength="20" />
					</td>
							<td>
							    <s:textfield name="driverLastName" cssClass="input-text" id="driverLastName" cssStyle="width:150px" maxlength="11"/>
							</td>
							<td>
							    <s:textfield name="eventFrom" cssClass="input-text" id="date1" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/><img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['eventListForm'].date1.focus();  return false;" />
							</td>
							<td>
							    <s:textfield name="eventTo" cssClass="input-text" id="date2" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/><img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
							</td>
							</tr>
							<tr>
							<td colspan="4"></td>
							<td style="border-left: hidden;">
							    <c:out value="${searchbuttons}" escapeXml="false" />   
							</td>
						</tr>
						</tbody>
					</table>
						</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

	<s:set name="driverEventList" value="driverEventList" scope="request"/>  
		<div id="newmnav">
				  <ul>
				   	 <li id="newmnav1" style="background:#FFF "><a class="current"><span>Event List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>				   	 
					 <li><a href="driverLocationMap.html" ><span>Driver Map</span></a></li>					  	
					 <li><a href="driverLocationList.html" ><span>Driver List</span></a></li>					   	 
				  </ul>
		</div><div class="spn">&nbsp;</div>
		<div style="padding-bottom:3px;"></div>
				<display:table name="driverEventList" class="table" requestURI="" id="driverEventList" export="true" defaultsort="1" defaultorder="ascending" pagesize="10" style="width:100%;margin-top:0px; !margin-top:6px;" decorator='${!accountpopup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}'>   
				    <display:column property="surveyDate" sortable="true" title="Event Date" format="{0,date,dd-MMM-yyyy}"/>
				    <display:column property="personId" sortable="true" title="Driver" style="width:260px"/>
				    <display:column property="firstName" sortable="true" title="First Name" />
				    <display:column property="lastName" sortable="true" title="Last Name" />
				    <display:column sortable="true" title="Comment" >
				    				    		<a onclick="editCalendar(${driverEventList.id});" ><c:out value="${driverEventList.activityName}" /></a>
					</display:column>				    				    		
				    <display:column title="Remove" style="width: 15px;">
				    <a><img align="middle" title="Remove" onclick="confirmSubmit(${driverEventList.id});"  style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>	</a> 
     				</display:column>				    
				    <display:setProperty name="paging.banner.item_name" value="driver"/>   
				    <display:setProperty name="paging.banner.items_name" value="driver"/>   
				    <display:setProperty name="export.excel.filename" value="DriverDetail List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="DriverDetail List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="DiverDetail List.pdf"/>   
				</display:table> 
				<c:out value="${buttons}" escapeXml="false" />  
</td>
</tr>
</tbody>
</table>
</div>

</s:form>
<c:if test="${hitFlag == 1}" >
		<c:redirect url="/searchEventsList.html?from=search&eventFrom=${eventFrom}&eventTo=${eventTo}"/>				
	</c:if>
<script type="text/javascript">   
  Form.focusFirstElement($("eventListForm")); 
 try{
  if(document.forms['eventListForm'].elements['from'].value=='driver') { 
    autoPopulate_Date(this);
    autoPopulate_DateTo(this);
    copyFromDate();
   }
   }
   catch(e){}
 </script>
<script type="text/javascript">
	setOnSelectBasedMethods(["calcDays(),calculateDeliveryDate()"]);
	setCalendarFunctionality();
</script>