package com.trilasoft.app.webapp.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import org.appfuse.webapp.action.BaseAction;
import com.trilasoft.app.service.PartnerLocatorManager;

public class PartnerLocatorAction extends BaseAction {

	private PartnerLocatorManager partnerLocatorManager;

	private double weight;
	
	private double inputLatitute;

	private double inputLongitude;
	
	private String countryCode;
	
	private String tariffApplicability;
	
	private String packingMode;
	
	private String poe;

	public String getPackingMode() {
		return packingMode;
	}

	public void setPackingMode(String packingMode) {
		this.packingMode = packingMode;
	}

	public String getPoe() {
		return poe;
	}

	public void setPoe(String poe) {
		this.poe = poe;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	public String getTariffApplicability() {
		return tariffApplicability;
	}

	public void setTariffApplicability(String tariffApplicability) {
		this.tariffApplicability = tariffApplicability;
	}
	
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public void setPartnerLocatorManager(
			PartnerLocatorManager partnerLocatorManager) {
		this.partnerLocatorManager = partnerLocatorManager;
	}

	public void setInputLatitute(double inputLatitute) {
		this.inputLatitute = inputLatitute;
	}

	public void setInputLongitude(double inputLongitude) {
		this.inputLongitude = inputLongitude;
	}

	public String collectPartnerLocatorInput() {
		return SUCCESS;
	}

	public String locatePartners() {
		return SUCCESS;
	}
	
	public InputStream getPartnerList() {
		return new ByteArrayInputStream(partnerLocatorManager.locateAgents(weight, inputLatitute, inputLongitude, tariffApplicability, countryCode, packingMode, poe).getBytes());
	}

}
