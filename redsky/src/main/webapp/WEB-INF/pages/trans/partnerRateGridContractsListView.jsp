<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Partner Rate matrix Contract Rates	</title>   
    <meta name="heading" content="Partner Rate matrix Contract Rates"/>  
    
<style>
.tab{
border:1px solid #74B3DC;

}
</style> 
</head>  
<s:form id="partnerRateGridContractsList" action="" method="post" validate="true"> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    <s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
    <c:set var="btntype"  value="<%=request.getParameter("btntype") %>"/>
<div id="newmnav">
		 <ul> 
		 <li id="newmnav1" style="background:#FFF "><a class="current"><span>Contract Rates</span></a></li>
		<!--<li ><a  href="editPartnerRateGridView.html?partnerId=${partnerPublic.id}&id=${partnerRateGrid.id}"><span>Rate Matrix Detail</span></a></li> 
		--><c:if test="${byAdmin!=''}"> 
			<!--<li><a href="partnerRateGridsView.html?from=view&partnerId=${partnerPublic.id}"><span>Rate Matrix List</span></a></li>
		--></c:if> 
	</ul> 
		</div>
		<div style="width:100%;">
		<div class="spn">&nbsp;</div>
	
		</div>
<div id="Layer4" style="width:100%;">
<c:set var="buttons">  
    <input type="button" class="cssbutton" style="width:120px; height:25px" 
        onclick="location.href='<c:url value="/editPartnerRateGridContracts.html?partnerId=${partnerPublic.id}&partnerRateGridID=${partnerRateGrid.id}"/>'"  
        value="Add Contract Rates"/>  
</c:set>  

<table  width="100%" cellspacing="1" cellpadding="0" border="0" width="100%">
	<tbody>
		<tr>
			<td >
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content"> 
   <table  cellspacing="1" cellpadding="1" border="0" >
					<tbody>
						<tr>
							<td width="" height="30px"><s:label label="  "/></td>
							<td class="listwhitetext"  >Partner Rate matrix</td>
							<td width="50px"><s:hidden name="partnerPublic.id" />
							    <s:textfield cssClass="input-textUpper" name="partnerPublic.partnerCode" required="true" size="7" readonly="true"/>
							</td>
							<td>
							    <s:textfield cssClass="input-textUpper" name="partnerPublic.lastName" required="true" size="31" readonly="true"/>
							</td> 
							<td width="5px"></td>
						    <td align="right"  class="listwhitetext">Metro City</td>
						    <td align="left"><s:textfield cssClass="input-textUpper"  name="partnerRateGrid.metroCity" cssStyle="width:100px" maxlength="50"  readonly="true"/></td>
						    <td width="5px"></td>
						    <td align="right"  class="listwhitetext">Effective&nbsp;Date</td>
							 <c:if test="${not empty partnerRateGrid.effectiveDate}"> 
					           <s:text id="partnerRateGridEffectiveDate" name="${FormDateValue}"><s:param name="value" value="partnerRateGrid.effectiveDate"/></s:text>
				                 <td><s:textfield id="effectiveDate" name="partnerRateGrid.effectiveDate" value="%{partnerRateGridEffectiveDate}" readonly="true" cssClass="input-textUpper" size="7" tabindex="4"/>
				                </td> 
				             </c:if>
				             <c:if test="${empty partnerRateGrid.effectiveDate}"> 
					             <td><s:textfield id="effectiveDate" name="partnerRateGrid.effectiveDate"  readonly="true" cssClass="input-textUpper" size="7" tabindex="4"/>
					             </td>
					         </c:if>
					         <td width="5px"></td>
					         <td align="right" width="90px"  class="listwhitetext"><fmt:message key="partnerRateGrid.tariffApplicability"/></td>
							 <td><s:textfield cssClass="input-textUpper" name="partnerRateGrid.tariffApplicability" required="true" size="8" readonly="true"/></td>
							
					  </tr>
					  <tr><td style="!height:25px;" ></td></tr>
					</tbody>
				</table>
    </div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
<s:set name="partnerRatesGrids" value="partnerRatesGrids" scope="request"/>  
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>List</span></a></li>
		  </ul>
</div>
<div class="spnblk">&nbsp;</div>
<display:table name="partnerRateGridContractsList" class="table" requestURI="" id="partnerRateGridContractsList" export="true" defaultsort="2" defaultorder="descending" pagesize="10" >   
	
	 <display:column sortable="true" title="Company" href="editPartnerRateGridContractsView.html?partnerId=${partnerPublic.id}&partnerRateGridID=${partnerRateGrid.id}" paramId="id" paramProperty="id"  property="discountCompany"  />
	 <display:column sortable="true" title="Contract" property="contract" style="width: 100px;"/> 
    <display:column sortable="true" title="Eff.Date" property="effectiveDate" style="width: 110px;" format="{0,date,dd-MMM-yyyy}"/>
    <display:column sortable="true" title="Disc/Prem&nbsp;Base&nbsp;%" property="discountBaseRate" /> 
    <display:column sortable="true" title="Disc/Prem&nbsp;Addl&nbsp;%" property="discountAddlCharges" /> 
    <display:column sortable="true" title="Disc/Prem&nbsp;Hauling&nbsp;%" property="discountHaulingCharges" /> 
    <display:column sortable="true" title="Disc/Prem&nbsp;Auto&nbsp;%" property="discountAutoHandling" /> 
    <display:column sortable="true" title="Disc/Prem&nbsp;SIT&nbsp;%" property="discountSITCharges" /> 
   
    <display:setProperty name="paging.banner.item_name" value="partnerRates"/>   
    <display:setProperty name="paging.banner.items_name" value="partnerRatesGrids"/>  
    <display:setProperty name="export.excel.filename" value="PartnerRates List.xls"/>   
    <display:setProperty name="export.csv.filename" value="PartnerRates List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="PartnerRates List.pdf"/>   
</display:table>  
</td>
</tr>
</tbody>
</table> 
<table>

<tr><td style="height:60px; !height:100px"></td></tr></table>
</div>
<c:if test="${btntype=='yes'}"> 
<c:if test="${not empty partnerPublic.id && not empty partnerRateGrid.id}">
	<c:redirect url="/partnerRateGridContractsList.html?partnerId=${partnerPublic.id}&partnerRateGridID=${partnerRateGrid.id}"></c:redirect>
</c:if>
</c:if> 
</s:form>
<script type="text/javascript">   
   function confirmSubmit(targetElement)
	{
	var agree=confirm("Are you sure to delete this entry?");
	if (agree){
			location.href = 'deleteRateGrid.html?id='+encodeURI(targetElement)+'&partnerId='+${partnerPublic.id};
	   }else{
		return false;
	}
}
<c:if test="${hitFlag == 1}" >
		<c:redirect url="/partnerRateGrids.html">
		<c:param name="partnerId" value="${partnerId}"/>
		</c:redirect>
</c:if>
</script>