package com.trilasoft.app.service;


import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CustomerInformation;
import com.trilasoft.app.model.Quote;
import com.trilasoft.app.model.QuoteItem;
import com.trilasoft.app.model.QuoteRate;
import com.trilasoft.app.model.SecorQuote;

public interface QuoteManager extends GenericManager<Quote, Long> {

	public List<Quote> findBySequenceNumber(Long sequenceNumber);

	public List searchQuoteDetailList(String firstName, String lastName,Date createdOn, String originCity, String originCountry,
			String destinationCity, String destinationCountry, String sessionCorpID);

	public List<SecorQuote> findSecorQuoteByQuoteId(String sessionCorpID, Long quoteId);

	public List<CustomerInformation> findCustomerInformationByQuoteId(String sessionCorpID, Long quoteId);

	public List<QuoteItem> findQuoteItemByQuoteId(String sessionCorpID, Long quoteId);

	public List<QuoteRate> findQuoteRateByQuoteId(String sessionCorpID, Long quoteId);

}
