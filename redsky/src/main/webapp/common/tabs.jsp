		<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
		<c:if test="${validateFormNav == 'OK'}">
		<c:choose>
			<c:when test="${gotoPageString == 'gototab.serviceorder' }">
				<c:redirect url="/customerServiceOrders.html?id=${customerFile.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.accounting' }">
				<c:redirect url="/accountLineList.html?sid=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.newAccounting' }">
				<c:redirect url="/pricingList.html?sid=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.billing' }">
				<c:redirect url="/editBilling.html?id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.domestic' }">
				<c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.status' }">
			<c:if test="${serviceOrder.job =='RLO'}"> 
				 <c:redirect url="/editDspDetails.html?id=${serviceOrder.id}" />
			</c:if>
           <c:if test="${serviceOrder.job !='RLO'}"> 
				<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}" />
			</c:if>
			</c:when>
			<c:when test="${gotoPageString == 'gototab.ticket' }">
				<c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.claims' }">
				<c:redirect url="/claims.html?id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.cartons' }">
				<c:redirect url="/cartons.html?id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.vehicles' }">
				<c:redirect url="/vehicles.html?id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.containers' }">
				<c:redirect url="/containers.html?id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.servicepartners' }">
				<c:redirect url="/servicePartnerss.html?id=${serviceOrder.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.customerfile' }">
				<c:redirect url="/editCustomerFile.html?id=${customerFile.id}" />
			</c:when>			
			<c:when test="${gotoPageString == 'gototab.criticaldate' }">			
				<sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
					<c:redirect url="/soAdditionalDateDetails.html?sid=${serviceOrder.id}" />
				</sec-auth:authComponent>
			</c:when>			
			<c:otherwise>
			</c:otherwise>
		</c:choose>
	</c:if>
	<div id="newmnav" style="float: left;">
	<ul><c:if test="${not empty serviceOrder.id}">
		<sec-auth:authComponent componentId="module.tab.serviceorder.serviceorderTab">
		<li ><a onmouseover="return chkSelect();"
			onclick="forwardToMyMessage1();setReturnString('gototab.serviceorder');return saveAuto('none');"
			><span>S/O Details</span></a>
		</li>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.serviceorder.billingTab">
		<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.billing');return saveAuto('none');"><span>Billing</span></a></li>
		</sec-auth:authComponent>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		<c:choose>
			<%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			   <li><a onmouseover="return chkSelect();" onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			</c:when> --%>
			<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 		<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			</c:when>
			<c:otherwise> 
		       <li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.accounting');return saveAuto('none');"><span>Accounting</span></a>
		       </li>
		    </c:otherwise>
	  </c:choose>
	  </sec-auth:authComponent>
	  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		<c:choose> 
			<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 		<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			</c:when>
			<c:otherwise> 
		       <li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.newAccounting');return saveAuto('none');"><span>Accounting</span></a>
		       </li>
		    </c:otherwise>
	  </c:choose>
	  </sec-auth:authComponent>
	  
	  	<sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
         <li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
  	 </sec-auth:authComponent>
	  <sec-auth:authComponent componentId="module.tab.serviceorder.forwardingTab">
	  <c:if test="${usertype!='ACCOUNT'}">
	    <c:if test="${serviceOrder.job !='RLO'}"> 
		<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.containers'); return saveAuto('none');"><span>Forwarding</span></a></li>
	  </c:if>
	  </c:if>
	  <c:if test="${usertype=='ACCOUNT'}">
	  <c:if test="${serviceOrder.job !='RLO'}"> 
	  <li><li><a href="servicePartnerss.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li></li>
	  </c:if>
	  </c:if>
	  </sec-auth:authComponent>
	 <sec-auth:authComponent componentId="module.tab.serviceorder.domesticTab">
		<c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
		<c:if test="${serviceOrder.job !='RLO'}"> 
			<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.domestic');return saveAuto('none');"><span>Domestic</span></a></li>
		</c:if>
		</c:if>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
	<c:if test="${serviceOrder.job =='INT'}">
	<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.domestic');return saveAuto('none');"><span>Domestic</span></a></li>
	</c:if>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.statusTab">
		<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.status');return saveAuto('none');"><span>Status</span></a></li>
	</sec-auth:authComponent>	
	<sec-auth:authComponent componentId="module.tab.summary.summaryTab">
		<li><a href="findSummaryList.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
	</sec-auth:authComponent>
	
	<sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.ticketTab">
	<c:if test="${serviceOrder.job =='RLO'}"> 
		<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.ticket');return saveAuto('none');"><span>Ticket</span></a></li>
	</c:if>
	</sec-auth:authComponent>	
	<sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.claimsTab">
	<c:if test="${serviceOrder.job =='RLO'}"> 	
		<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.claims');return saveAuto('none');"><span>Claims</span></a></li>
	</c:if> 
	</sec-auth:authComponent>
	
	<sec-auth:authComponent componentId="module.tab.serviceorder.ticketTab">
	<c:if test="${serviceOrder.job !='RLO'}"> 
		<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.ticket');return saveAuto('none');"><span>Ticket</span></a></li>
	</c:if>
	</sec-auth:authComponent>	
	<sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
	<c:if test="${serviceOrder.job !='RLO'}"> 	
		<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.claims');return saveAuto('none');"><span>Claims</span></a></li>
	</c:if> 
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
	<c:if test="${voxmeIntergartionFlag=='true'}">
	<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
	</c:if>
	<configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 <li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 </configByCorp:fieldVisibility>
	<li id="newmnav1" style="background:#FFF"><a onmouseover="return chkSelect();"
			onclick="forwardToMyMessage1();setReturnString('gototab.serviceorder');return saveAuto('none');"
			class="current"><span>New S/O Details</span></a>
		</li>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.customerFileTab">	
		<li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.customerfile');return saveAuto('none');"><span>Customer File</span></a></li>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
	 <li><a onmouseover="return chkSelect();" onclick="forwardToMyMessage1();setReturnString('gototab.criticaldate');return saveAuto('none');"><span>Critical Dates</span></a></li>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.reportTab">	
			<li><a onmouseover="return chkSelect();" onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.auditTab">			
			<li><a onmouseover="return chkSelect();" onclick="window.open('auditList.html?id=${serviceOrder.id}&tableName=serviceorder,miscellaneous&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
	</sec-auth:authComponent>
	 <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
         <li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
 	 </sec-auth:authComponent>
      	</c:if>
		<c:if test="${empty serviceOrder.id}">
		<li id="newmnav1" style="background:#FFF"><a onmouseover="return chkSelect();" href="customerServiceOrders.html?id=${customerFile.id}"	class="current"><span>S/O Details</span></a></li>
		<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
			<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Billing</span></a></li>
		</sec-auth:authComponent>
		<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Accounting</span></a></li>
		<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Forwarding</span></a></li>
		<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Domestic</span></a></li>
		<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Status</span></a></li>
		<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Ticket</span></a></li>
		<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Claims</span></a></li>
		<li><a href="editCustomerFile.html?id=${customerFile.id}&from=list"><span>Customer File</span></a></li>
		<sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
		<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Critical Dates</span></a></li>
		</sec-auth:authComponent>
		<!--<li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.customerfile');return ContainerAutoSave('none');"><span>Customer File</span></a></li>
		--><li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Forms</span></a></li>
		<li><a onmouseover="return chkSelect();" onclick="notExists()"><span>Audit</span></a></li>
		</c:if>
	</ul>
	</div>
	<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;"><tr>
	<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<c:if test="${countShip != 1}" >
		<td width="20px" align="left" valign="top" style="vertical-align:top;!padding-top:1px;">		
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</td>
		</c:if>
		<c:if test="${countShip == 1}" >
		<td width="20px" align="left" style="!vertical-align:top;">
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</td>
  		</c:if>		
		</c:if>
		</tr></table>