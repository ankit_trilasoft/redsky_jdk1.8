package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.WorkTicketInventoryDataDao;
import com.trilasoft.app.model.MoversDTO;
import com.trilasoft.app.model.WorkTicketInventoryData;

public class WorkTicketInventoryDataDaoHibernate extends GenericDaoHibernate<WorkTicketInventoryData, Long> implements WorkTicketInventoryDataDao{

	public WorkTicketInventoryDataDaoHibernate() {
		super(WorkTicketInventoryData.class);
		// TODO Auto-generated constructor stub
	}

	public List checkInventoryData(String corpID, String barcode,String shipNumber) {
		return getHibernateTemplate().find(" from WorkTicketInventoryData where corpId='"+corpID+"' and barCode='"+barcode+"' and shipNumber='"+shipNumber+"' ");
	}

	public List getAllInventory(String sessionCorpID, String shipno) {
		List <MoversDTO> movValueList = new ArrayList<MoversDTO>(); 
		MoversDTO moverDto = null;
		List list = getSession()
		.createSQLQuery("select " +
				"i.barCode ," + //0
				"i.itemDesc ," + //1
				"l.workTicket," + //2
				"l.locationType ,"+ //3
				"if(i.oItemRoomId=r.id,r.roomDesc,'') as oRoom ," + //4
				"if(i.dItemRoomId=r.id,r.roomDesc,'') as dRoom ," + //5
				"i.oItemCondition , " + //6
				"i.dItemCondition , " + //7  
				"i.oItemComment , " + //8
				"i.dItemComment , " + //9
				"i.oPacker , " + //10
				"i.dPacker , " + //11
				"ip.workticketInventoryDataId , " + //12
				"i.dItemImageId  ," + //13
				"i.delivered ," + //14
				"i.quantity ," + //15
				"i.cartonContent ," + //16
				"i.packingCode  "+ //17
				"from workticketinventorydata i left outer join inventorypath ip on ip.workticketInventoryDataId=i.id and ip.corpid='"+sessionCorpID+"'" +
				" , room r , inventorylocation l where i.corpId='"+sessionCorpID+"' and  i.corpId='"+sessionCorpID+"' and i.shipNumber='"+shipno+"'" +
				" and  r.shipNumber='"+shipno+"' and l.shipNumber='"+shipno+"' and  l.id=r.locationId and  l.id=i.locationId  and r.locationId =i.locationId and (i.oItemRoomId=r.id or i.dItemRoomId=r.id ) order by 1 ") 
				.addScalar("i.barCode", Hibernate.STRING)
				.addScalar("i.itemDesc", Hibernate.STRING)
				.addScalar("l.workTicket", Hibernate.STRING)
				.addScalar("l.locationType", Hibernate.STRING)
				.addScalar("oRoom", Hibernate.STRING)
				.addScalar("dRoom", Hibernate.STRING)
				.addScalar("i.oItemCondition", Hibernate.STRING)
				.addScalar("i.dItemCondition", Hibernate.STRING)
				.addScalar("i.oItemComment", Hibernate.STRING)
				.addScalar("i.dItemComment", Hibernate.STRING)
				.addScalar("i.oPacker", Hibernate.STRING)
				.addScalar("i.dPacker", Hibernate.STRING)
				.addScalar("ip.workticketInventoryDataId", Hibernate.LONG)
				.addScalar("i.dItemImageId", Hibernate.LONG)
				.addScalar("i.delivered", Hibernate.STRING)
				.addScalar("i.quantity", Hibernate.INTEGER)
				.addScalar("i.cartonContent", Hibernate.STRING)
				.addScalar("i.packingCode", Hibernate.STRING)
				.list(); 
		Iterator it=list.iterator();
		  while(it.hasNext()) {
			  Object [] row=(Object[])it.next();
			  moverDto = new MoversDTO();
			  moverDto.setInventory(row[0]!=null  ? row[0].toString() :"" );
			  moverDto.setInventoryDesc(row[1]!=null  ? row[1].toString() :"" );
			  moverDto.setTicket(row[2]!=null  ? row[2].toString() :"");
			  moverDto.setLocation(row[3]!=null  ? row[3].toString() :"");
			  moverDto.setoRooms(row[4]!=null  ? row[4].toString() :"");
			  moverDto.setdRooms(row[5]!=null  ? row[5].toString() :"");
			  moverDto.setoItemCondition(row[6]!=null ? row[6].toString() :"");
			  moverDto.setdItemCondition(row[7]!=null ? row[7].toString() :"");
			  moverDto.setoComments(row[8]!=null ? row[8].toString() :"");
			  moverDto.setdComments(row[9]!=null ? row[9].toString() :"");
			  moverDto.setoPacker(row[10]!=null ? row[10].toString() :"");
			  moverDto.setdPacker(row[11]!=null ? row[11].toString() :"");
			  moverDto.setoPhotos(row[12]!=null ? row[12].toString() :"");
			  moverDto.setdPhotos(row[13]!=null ? row[13].toString() :"");
			  moverDto.setDelivered(row[14]!=null ? row[14].toString() :"");
			  moverDto.setQuantity(row[15]!=null ? row[15].toString() :"");
			  moverDto.setCartonContent(row[16]!=null ? row[16].toString() :"");
			  moverDto.setPackingCode(row[17]!=null ? row[17].toString() :"");
			  movValueList.add(moverDto);
		  }
		  return movValueList;
	
	}

}
