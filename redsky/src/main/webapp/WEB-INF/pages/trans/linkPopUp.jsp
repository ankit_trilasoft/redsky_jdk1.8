<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Link Pop Up</title>   
    <meta name="heading" content="Link up Notes"/> 
    <script type="text/javascript">
function selectCheckBox(){	
	try{
		var docIdListCF="";	
		var docIdListCFUnchecked ="";
		<c:forEach  var="list1" items="${documentListCF}">
		var aid="${list1.id}";	
		var fieldNameVal="${list1.fieldValue}";
		var status = document.getElementById(aid).checked;		 
			if(status == true){
				if(docIdListCF==''){
					docIdListCF = aid;	
				}else{
					docIdListCF = docIdListCF+','+aid;	
				}
		 }else{
			 if(docIdListCFUnchecked==''){
				 docIdListCFUnchecked = aid;	
				}else{
					docIdListCFUnchecked = docIdListCFUnchecked+','+aid;	
				}
		 }
		</c:forEach>
			document.forms['linkUPForm'].elements['documentIdListCF'].value=docIdListCF;
			document.forms['linkUPForm'].elements['documentIdListCFFalse'].value=docIdListCFUnchecked;
			document.forms['linkUPForm'].elements['myFileFieldName'].value=fieldNameVal;
		}catch(e){}	
}
function selectCheckBox1(){	
	try{
		var docIdListSO="";	
		var docIdListSOUnchecked ="";
		<c:forEach  var="list1" items="${documentListSO}">
		var aid="${list1.id}";
		var fieldNameVal="${list1.fieldValue}";
		var status = document.getElementById(aid).checked;		 
			if(status == true){
				if(docIdListSO==''){
					docIdListSO = aid;	
				}else{
					docIdListSO = docIdListSO+','+aid;	
				}
		 }else{
			 if(docIdListSOUnchecked==''){
				 docIdListSOUnchecked = aid;	
				}else{
					docIdListSOUnchecked = docIdListSOUnchecked+','+aid;	
				}
		 }
		</c:forEach>
			document.forms['linkUPForm'].elements['documentIdListSO'].value=docIdListSO;
			document.forms['linkUPForm'].elements['documentIdListSOFalse'].value=docIdListSOUnchecked;
			document.forms['linkUPForm'].elements['myFileFieldName'].value=fieldNameVal;
	}catch(e){}	
}

window.onbeforeunload = function (evt) {
	if(document.forms['linkUPForm'].elements['closeWindowFlag'].value!='1'){
	  var message = 'Are you sure you want to leave?';
	  if (typeof evt == 'undefined') {
		 
	    evt = window.event;
	  }
	  if (evt) {
		  
		  window.opener.closeBoxDetails();
	  }
	  
	  //return message;
	}
}
	
function findMyFileId(){
	var cfId = document.forms['linkUPForm'].elements['documentIdListCFFalse'].value;
	var soId = document.forms['linkUPForm'].elements['documentIdListSOFalse'].value;
	 var allIdOfMyFile = "";
	 if(cfId!=''){
		 
		 if(allIdOfMyFile==''){
			 allIdOfMyFile=cfId;	 
		 }else{
			 allIdOfMyFile=allIdOfMyFile+","+cfId;
		 }
	 }
	 if(soId!=''){
		 if(allIdOfMyFile==''){
			 allIdOfMyFile=soId;	 
		 }else{
			 allIdOfMyFile=allIdOfMyFile+","+soId;
		 }
	 }		 
	 document.forms['linkUPForm'].elements['allIdOfMyFile'].value=allIdOfMyFile;
	}
</script>
 </head>
 
 <c:set var="buttons">   
 <input type="button" class="cssbutton" style="width:125px; height:25px;" onclick="linkUpComplete();" value="Complete Link-Up"/>
 <input type="button" class="cssbutton" style="width:55px; height:25px;" onclick="closeWindow();" value="Cancel"/>
</c:set> 

<s:form name="linkUPForm" action="" method="post" validate="true" >
<s:hidden name="closeWindowFlag" value="" />
<s:hidden name="notes.notesKeyId" value="${trackingStatus.id }" />
<s:hidden name="notes.corpID" value="${trackingStatus.corpID}" />
<s:hidden name="notes.customerNumber" value="${trackingStatus.sequenceNumber }" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>  
<s:hidden name="documentIdListCF"/>
<s:hidden name="documentIdListSO"/>
<s:hidden name="documentIdListSOFalse"/>
<s:hidden name="documentIdListCFFalse"/>
<s:hidden name="allIdOfMyFile"/>
<s:hidden name="myFileFieldName"/>
<div>
	<table border="0" style="margin:0px 0px 5px 0px">
		<tr>
			<td class="listwhitetext" >Select Service Request from Agent<font color="red" size="2">*</font></td>
			<td><s:select  name="serviceType" list="%{service}" cssClass="list-menu" cssStyle="width:200px" tabindex="23" /></td>
		<td width="50"></td>
			<td ><s:checkbox id="sendNotes" name="sendNotes" onchange="notesVisibility();" /></td>
			<td class="listwhitetext" >Send Notes to Agent</td>
		</tr>		
	</table>
	<div>
		<div id="Layer1" style="display: none;">
			<jsp:include page="linkPopUpNotes.jsp"></jsp:include>
		</div>
	</div>
	
	
</div>
 <c:if test="${documentListCF!='[]'}">
 <div class="listwhitetext" style="padding-bottom:4px;color:#C35817;" ><b>Document Available For Customer File</b></div>
 
 <display:table name="documentListCF" class="table-fc" id="documentListCF" style="width:100%;margin-bottom:12px;"> 
   	<display:column title="" style="width:25px;">
   		<c:set var="ischeckedDoc" value="false"/>
			<c:if test="${documentListCF.agentType}">
				<c:set var="ischeckedDoc" value="true"/>
			</c:if>
		<s:checkbox key="documentList.agentType" id="${documentListCF.id}" value="${ischeckedDoc}" fieldValue="true" />   	
   	</display:column>
  	<display:column property="fileType" title="Document Type" ></display:column>
	<display:column property="fileDescription" title="Description" ></display:column>	
	<display:column property="fileSize" title="Size" ></display:column>
	<display:column property="updatedOn" title="Updated On" format="{0,date,dd-MMM-yyyy}" style="width:100px"></display:column>
	<display:column property="updatedBy" title="Updated By" ></display:column>
</display:table>
</c:if>

 <c:if test="${documentListSO!='[]'}">
 <div class="listwhitetext" style="padding-bottom:4px;color:#C35817;"><b>Document Available For Service Order</b></div>

 <display:table name="documentListSO" class="table-fc" id="documentListSO" style="width:100%;"> 
   	<display:column title="" style="width:25px;">
   		<c:set var="ischeckedDoc" value="false"/>
			<c:if test="${documentListSO.agentType}">
				<c:set var="ischeckedDoc" value="true"/>
			</c:if>
		<s:checkbox key="documentList.agentType" id="${documentListSO.id}" value="${ischeckedDoc}" fieldValue="true" />   	
   	</display:column>
  	<display:column property="fileType" title="Document Type" ></display:column>
	<display:column property="fileDescription" title="Description" ></display:column>	
	<display:column property="fileSize" title="Size" ></display:column>
	<display:column property="updatedOn" title="Updated On" format="{0,date,dd-MMM-yyyy}" style="width:100px"></display:column>
	<display:column property="updatedBy" title="Updated By" ></display:column>
</display:table>
</c:if>
<div>
<table width="" border="0" >
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${notes.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="notes.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td style="width:"><fmt:formatDate value="${notes.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdBy' /></td>						
						<c:if test="${not empty notes.id}">
								<s:hidden name="notes.createdBy"/>
								<td style="width:60px"><s:label name="createdBy" value="%{notes.createdBy}"/></td>
							</c:if>
							<c:if test="${empty notes.id}">
								<s:hidden name="notes.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:90px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${notes.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="notes.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td style="width:"><fmt:formatDate value="${notes.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty notes.id}">
							<s:hidden name="notes.updatedBy"/>
							<td style="width:90px"><s:label name="updatedBy" value="%{notes.updatedBy}"/></td>
						</c:if>
						<c:if test="${empty notes.id}">
							<s:hidden name="notes.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:90px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
						</c:if>
					</tr>
				</tbody>
			</table>
</div>
 <table>
		<tr><td colspan="2"><c:out value="${buttons}" escapeXml="false" /></td></tr> 
	</table>
</s:form>

<script type="text/javascript">
<c:if test="${linkUp == 'NOTES'}">
	var serviceType = document.forms['linkUPForm'].elements['serviceType'].value;
	window.opener.document.forms['trackingStatusForm'].elements['serviceType'].value = serviceType;
	window.opener.linkUP('${notesSubType}');
	document.forms['linkUPForm'].elements['closeWindowFlag'].value='1';
	window.close();
</c:if>

function linkUpComplete(){
	var serviceTypeValidate = document.forms['linkUPForm'].elements['serviceType'].value;
	serviceTypeValidate=serviceTypeValidate.trim();
	if(serviceTypeValidate==''){
	alert("To complete the Link-Up, please select Service requested from Agent.")	
	}else{
	var noteVisibility = document.getElementById("sendNotes");
	if(noteVisibility.checked){
		var url="saveNotesLinkUp.html?ajax=1&decorator=popup&popup=true&id="+'<%=request.getParameter("id") %>';
		document.forms['linkUPForm'].action = url;
		selectCheckBox();
		window.opener.document.forms['trackingStatusForm'].elements['documentIdListCF'].value = document.forms['linkUPForm'].elements['documentIdListCF'].value;
		selectCheckBox1();
		window.opener.document.forms['trackingStatusForm'].elements['documentIdListSO'].value = document.forms['linkUPForm'].elements['documentIdListSO'].value;
		findMyFileId();
		document.forms['linkUPForm'].elements['closeWindowFlag'].value='1';
		window.opener.document.forms['trackingStatusForm'].elements['allIdOfMyFile'].value = document.forms['linkUPForm'].elements['allIdOfMyFile'].value;
		window.opener.document.forms['trackingStatusForm'].elements['myFileFieldName'].value = document.forms['linkUPForm'].elements['myFileFieldName'].value;
		document.forms['linkUPForm'].submit();
	}else{
		var serviceType = document.forms['linkUPForm'].elements['serviceType'].value;
		window.opener.document.forms['trackingStatusForm'].elements['serviceType'].value = serviceType;
		selectCheckBox();
		window.opener.document.forms['trackingStatusForm'].elements['documentIdListCF'].value = document.forms['linkUPForm'].elements['documentIdListCF'].value;
		selectCheckBox1();
		window.opener.document.forms['trackingStatusForm'].elements['documentIdListSO'].value = document.forms['linkUPForm'].elements['documentIdListSO'].value;
		findMyFileId();
		window.opener.document.forms['trackingStatusForm'].elements['allIdOfMyFile'].value = document.forms['linkUPForm'].elements['allIdOfMyFile'].value;
		window.opener.document.forms['trackingStatusForm'].elements['myFileFieldName'].value = document.forms['linkUPForm'].elements['myFileFieldName'].value;
		window.opener.linkUP('<%=request.getParameter("linkUpSubType") %>');
		document.forms['linkUPForm'].elements['closeWindowFlag'].value='1'; 
		window.close();
	}
}
}

function closeWindow(){
	window.opener.closeBoxDetails();
	window.close();
}
function notesVisibility(){
	var noteVisibility = document.getElementById("sendNotes");
	var note = document.getElementById("Layer1")
	if(noteVisibility.checked){
		note.style.display = 'block';
	}else{
		note.style.display = 'none';
	}
	
	document.forms['linkUPForm'].elements['notes.noteStatus'].value = 'NEW';
	document.forms['linkUPForm'].elements['notes.noteSubType'].value = '<%=request.getParameter("linkUpSubType") %>';
	
}

String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}
document.forms['linkUPForm'].elements['<%=request.getParameter("checkId") %>'].checked = true;
document.forms['linkUPForm'].elements['<%=request.getParameter("checkId") %>'].disabled = false;
</script> 