package com.trilasoft.app.webapp.action;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.multipart.MultiPartRequestWrapper;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.EmailSetup;
import com.trilasoft.app.model.EmailSetupTemplate;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.Reports;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DocumentBundleManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.EmailSetupTemplateManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ReportsManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporterParameter;

public class EmailSetupTemplateAction extends BaseAction implements Preparable{
	private String sessionCorpID;
	private Long id;
	private Map<String, String> routingList;
	private Map<String, String> serviceList; 
	private Map<String, String> jobsList;
	private Map<String, String> modeList;
	private Map<String, String> moduleList;
	private Map<String, String> subModuleList;
	private Map<String, String> moveTypeList;
	private Map<String, String> languageList;
	private Map<String, String> distributionMethodList;
	private List companyDivisionList = new ArrayList();
	private Map<String, String> docsList;
	private String emailSetupTemplateURL;
	private String reportsId;
	private Map<String, String> tableWithColumnsNameList = new LinkedHashMap<String, String>();
	private String voxmeIntergartionFlag;
	private Long cid;
	private String mmValidation = "NO";
	private Long sid;
	
	private RefMasterManager refMasterManager;
	private EmailSetupTemplate emailSetupTemplate;
	private EmailSetupTemplateManager emailSetupTemplateManager;
	private CustomerFileManager customerFileManager;
	private ReportsManager reportsManager;
	private DocumentBundleManager documentBundleManager;
	private CustomerFile customerFile;
	private Company company;
	private User userNew;
	private PartnerPublic partnerPublicObj;
	private CompanyManager companyManager;
	private EmailSetupManager emailSetupManager;
	private ServiceOrder serviceOrder;
	private ServiceOrderManager serviceOrderManager;
	private PartnerPublicManager partnerPublicManager;
	private Reports reports;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private Miscellaneous miscellaneous;
	private MiscellaneousManager miscellaneousManager;
	private UserManager userManager;
	private MyFileManager myFileManager;
	private MyFile myFile;
    private String oiJobList;
    private String dashBoardHideJobsList;
	
	static final Logger logger = Logger.getLogger(EmailSetupTemplateAction.class);
	Date currentdate = new Date();
	
	public EmailSetupTemplateAction(){
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if(auth != null){
				User user = (User) auth.getPrincipal(); 
				this.sessionCorpID = user.getCorpID();
			}
		} catch (Exception e) {e.printStackTrace();} 
	}
	
	public void prepare() throws Exception {
		company= companyManager.findByCorpID(sessionCorpID).get(0);
        if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }

        if (company != null && company.getDashBoardHideJobs() != null) {
       					dashBoardHideJobsList = company.getDashBoardHideJobs();	 
       				}
	}
	
	public void getComboList() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" getComboList() Start");
		String parameters="'ROUTING','MODE','JOB','SERVICE','STANDARDEMAILMODULE','STANDARDEMAILSUBMODULE', 'LANGUAGE','DISTRIBUTIONMETHOD','DOCUMENT'" ;
		routingList = new LinkedHashMap<String, String>();
		modeList = new LinkedHashMap<String, String>();
		jobsList= new LinkedHashMap<String,String>();
		serviceList = new LinkedHashMap<String, String>();
		moduleList = new LinkedHashMap<String, String>();
		subModuleList = new LinkedHashMap<String, String>();
		languageList = new LinkedHashMap<String, String>();
		distributionMethodList = new LinkedHashMap<String, String>();
		docsList = new LinkedHashMap<String, String>();
		
		 List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(sessionCorpID,parameters);
    	 for (RefMasterDTO  refObj : allParamValue) {
    		 if (refObj.getParameter().trim().equals("ROUTING")){
    			 if(refObj.getCode()!=null && !refObj.getCode().equalsIgnoreCase("")){
    				 routingList.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
    			 }
    		 }else if(refObj.getParameter().trim().equals("MODE")){
    			 if(refObj.getCode()!=null && !refObj.getCode().equalsIgnoreCase("")){
    				 modeList.put(refObj.getDescription().trim()+"~"+refObj.getStatus().trim() ,refObj.getDescription().trim());
    			 }
    		 }else if(refObj.getParameter().trim().equals("JOB")){
    			 if(refObj.getCode()!=null && !refObj.getCode().equalsIgnoreCase("")){
    				 jobsList.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim() ,refObj.getDescription().trim());
    			 }
    		 }else if(refObj.getParameter().trim().equals("SERVICE") && (refObj.getFlex1()==null || refObj.getFlex1().trim().equals("") || !refObj.getFlex1().trim().equals("Relo"))){
    			 if(refObj.getCode()!=null && !refObj.getCode().equalsIgnoreCase("")){
    				 serviceList.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim() ,refObj.getDescription().trim());
    			 }
    		 }else if(refObj.getParameter().trim().equals("STANDARDEMAILMODULE")){
    			 if(refObj.getCode()!=null && !refObj.getCode().equalsIgnoreCase("")){
    				 moduleList.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim() ,refObj.getDescription().trim());
    			 }
    		 }else if(refObj.getParameter().trim().equals("STANDARDEMAILSUBMODULE")){
    			 if(refObj.getCode()!=null && !refObj.getCode().equalsIgnoreCase("")){
    				 subModuleList.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim() ,refObj.getDescription().trim());
    			 }
    		 }else if(refObj.getParameter().trim().equals("LANGUAGE")){
		 		 if(refObj.getFlex1()!=null && !refObj.getFlex1().trim().equals("")){
		 			 languageList.put(refObj.getFlex1().trim()+"~"+refObj.getStatus().trim(),refObj.getCode().trim());
		 		 }
		 	}else if(refObj.getParameter().trim().equals("DISTRIBUTIONMETHOD")){
		 		if(refObj.getCode()!=null && !refObj.getCode().equalsIgnoreCase("")){
		 			distributionMethodList.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim() ,refObj.getDescription().trim());
		 		}
		 	}else if(refObj.getParameter().trim().equals("DOCUMENT")){
		 		if(refObj.getCode()!=null && !refObj.getCode().equalsIgnoreCase("")){
		 			docsList.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim() ,refObj.getDescription().trim());
		 		}
		 	}
    		 
    	 }
		
		companyDivisionList = customerFileManager.findCompanyDivision(sessionCorpID);
		
		moveTypeList =new LinkedHashMap<String, String>();
		moveTypeList.put("Quote", "Quote");
		moveTypeList.put("BookedMove", "Booked Move");
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" getComboList() End");
	}
	private List emailSetupTemplateList = new ArrayList();
	@SkipValidation
	public String list() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" List() Start");
		getComboList();
		emailSetupTemplateList = emailSetupTemplateManager.getListByCorpId(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" List() End");
		return SUCCESS;
	}
	private List fileCabinetFileList = new ArrayList();
	private List emailSetupTemplateFormsList = new ArrayList();
	private List<String> attachedFileList = new ArrayList<String>();
	private Integer sizeOfEmailFrom=60;
	private Integer sizeOfEmailTo=250;
	private Integer sizeOfEmailBcc=250;
	private Integer sizeOfEmailCc=250;
	private Integer sizeOfEmailSubject=500;
	@SkipValidation
	public String edit() throws Exception {
		getComboList();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Edit() Start");
		String fileName = "";
		if (id != null) {
			emailSetupTemplate = emailSetupTemplateManager.get(id);
			if ((emailSetupTemplate.getAttachedFileLocation() != null) && (!emailSetupTemplate.getAttachedFileLocation().trim().equalsIgnoreCase(""))) {
				for(String str:emailSetupTemplate.getAttachedFileLocation().split("~")){
	        		if(fileName!=null && !fileName.equalsIgnoreCase("")){
	        			fileName = fileName+"^"+str.substring(str.lastIndexOf("/")+1);
	        		}else{
	        			fileName = str.substring(str.lastIndexOf("/")+1);
	        		}
	        	}
	        }
			attachedFileList = Arrays.asList(fileName.split("^"));
			if(emailSetupTemplate.getFormsId()!=null && !emailSetupTemplate.getFormsId().equals("")){
				String[] arr = emailSetupTemplate.getFormsId().split(",");
				int arrayLength = arr.length;
				String reportId="";
				for(int i=0;i<arrayLength;i++){
					if(reportId.equals("")){
						reportId = arr[i];
					}else{
						reportId = reportId+","+arr[i];
					}
				}
				emailSetupTemplateFormsList = documentBundleManager.findFormsByFormsId(reportId);
			}
			String filesFromFileCabinet="";
			if(emailSetupTemplate.getFileTypeOfFileCabinet()!=null && !emailSetupTemplate.getFileTypeOfFileCabinet().equalsIgnoreCase("")){
				if(cid!=null){
					customerFile = customerFileManager.get(cid);
					filesFromFileCabinet = emailSetupTemplateManager.findDocsFromFileCabinet(emailSetupTemplate.getFileTypeOfFileCabinet(),customerFile.getSequenceNumber(),sessionCorpID);
				}else if(sid!=null && emailSetupTemplate.getFileTypeOfFileCabinet()!=null && !emailSetupTemplate.getFileTypeOfFileCabinet().equalsIgnoreCase("")){
					serviceOrder = serviceOrderManager.get(sid);
					filesFromFileCabinet = emailSetupTemplateManager.findDocsFromFileCabinet(emailSetupTemplate.getFileTypeOfFileCabinet(),serviceOrder.getShipNumber(),sessionCorpID);
				}
			String fileCabinetFileName="";
				for(String str:filesFromFileCabinet.split("~")){
	        		if(fileCabinetFileName!=null && !fileCabinetFileName.equalsIgnoreCase("")){
	        			if(!str.contains("C:")){
	        				fileCabinetFileName = fileCabinetFileName+"^"+str.substring(str.lastIndexOf("/")+1);
	        			}else{
	        				fileCabinetFileName = fileCabinetFileName+"^"+str.substring(str.lastIndexOf("\\")+1);
	        			}
	        		}else{
	        			if(!str.contains("C:")){
	        				fileCabinetFileName = (str.substring(str.lastIndexOf("/")+1));
	        			}else{
	        				fileCabinetFileName = (str.substring(str.lastIndexOf("\\")+1));
	        			}
	        		}
	        	}
				fileCabinetFileList = Arrays.asList(fileCabinetFileName.split("^"));
			}
			if(emailSetupTemplate.getEmailFrom() != null && !emailSetupTemplate.getEmailFrom().equalsIgnoreCase("")){
				sizeOfEmailFrom	= sizeOfEmailFrom - emailSetupTemplate.getEmailFrom().length();
			}
			if(emailSetupTemplate.getEmailTo() != null && !emailSetupTemplate.getEmailTo().equalsIgnoreCase("")){
				sizeOfEmailTo	= sizeOfEmailTo - emailSetupTemplate.getEmailTo().length();
			}
			if(emailSetupTemplate.getEmailCc() != null && !emailSetupTemplate.getEmailCc().equalsIgnoreCase("")){
				sizeOfEmailCc	= sizeOfEmailCc - emailSetupTemplate.getEmailCc().length();
			}
			if(emailSetupTemplate.getEmailBcc() != null && !emailSetupTemplate.getEmailBcc().equalsIgnoreCase("")){
				sizeOfEmailBcc	= sizeOfEmailBcc - emailSetupTemplate.getEmailBcc().length();
			}
			if(emailSetupTemplate.getEmailSubject() != null && !emailSetupTemplate.getEmailSubject().equalsIgnoreCase("")){
				sizeOfEmailSubject	= sizeOfEmailSubject - emailSetupTemplate.getEmailSubject().length();
			}
		}else {
			emailSetupTemplate = new EmailSetupTemplate(); 
			emailSetupTemplate.setCreatedOn(new Date());
			emailSetupTemplate.setUpdatedOn(new Date());
			emailSetupTemplate.setLanguage("en");
			emailSetupTemplate.setEnable(true);
			int orderNumverValue = emailSetupTemplateManager.getMaxOrderNumber(sessionCorpID);
			emailSetupTemplate.setOrderNumber(orderNumverValue+1);
		}
		tableWithColumnsNameList = emailSetupTemplateManager.getTableWithColumnsNameMap();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Edit() End");
		return SUCCESS;
	}
	private File[] fileUpload;
    private String[] fileUploadFileName;
    private String[] fileUploadContentType;
	@SkipValidation
	public String save() throws Exception {
		getComboList();
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Save()  Start");
				
				boolean isNew = (emailSetupTemplate.getId() == null);
				if (isNew) { 
					emailSetupTemplate.setCreatedOn(new Date());
					emailSetupTemplate.setCreatedBy(getRequest().getRemoteUser());
		        }
				emailSetupTemplate.setCorpId(sessionCorpID);
				emailSetupTemplate.setUpdatedOn(new Date());
				emailSetupTemplate.setUpdatedBy(getRequest().getRemoteUser());
				emailSetupTemplate.setFormsId(reportsId);
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
				uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + "EmailSetupTemplate" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
				File dirPath = new File(uploadDir);
		        if (!dirPath.exists()) {
		            dirPath.mkdirs();
		        }
				try{
					for (int i = 0; i < fileUpload.length; i++) {
						String uploadFilePath="";
						String fileName = "";
			            fileName = fileUploadFileName[i];
			            int num = 1;
			            try {
			            	if(fileName != null && !fileName.equalsIgnoreCase("")){
			            		fileName = fileName.replaceAll("'", "");
			            		fileName = fileName.replaceAll("#", "");
			            		File uploadedFile = fileUpload[i];
			            		int pos =fileName.lastIndexOf(".");
			            		String fileExtension1 = fileName.substring(0, pos);
			            		String fileExtension2 = fileName.substring(pos);
			            		File fileToCreate = new File(uploadDir,fileName);
			            		while(fileToCreate.exists()) {
			            			fileName = fileExtension1 +" ("+ (num++)+")"+fileExtension2;
			            		    fileToCreate = new File(uploadDir,fileName); 
			            		}
			            		
			            		FileUtils.copyFile(uploadedFile, fileToCreate);
			            		if(uploadFilePath==null || uploadFilePath.equalsIgnoreCase("")){
									uploadFilePath =(uploadDir+fileName );
								}else{
									uploadFilePath =uploadFilePath+"~"+(uploadDir+fileName );
								}
								
								if (emailSetupTemplate.getAttachedFileLocation() != null && !emailSetupTemplate.getAttachedFileLocation().trim().equalsIgnoreCase("")) {
									emailSetupTemplate.setAttachedFileLocation(emailSetupTemplate.getAttachedFileLocation()+"~"+uploadFilePath);
								}else{
									emailSetupTemplate.setAttachedFileLocation(uploadFilePath);
								}
			            	}else{
								emailSetupTemplate.setAttachedFileLocation(emailSetupTemplate.getAttachedFileLocation());
							}
			            } catch (IOException ex) {
			                ex.printStackTrace();
			            }
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(emailSetupTemplate.getOthers()!=null && !emailSetupTemplate.getOthers().equalsIgnoreCase("")){
					returnAjaxStringValue =emailSetupTemplateManager.testEmailSetupTestConditionQuery(emailSetupTemplate.getModule(),emailSetupTemplate.getOthers(),sessionCorpID);
					emailSetupTemplate.setStatus(returnAjaxStringValue);
				}
				emailSetupTemplate = emailSetupTemplateManager.save(emailSetupTemplate);
				String key = (isNew) ? "E-Mail setup has been added Sucessfully" : "E-Mail setup has been updated Sucessfully";
			    saveMessage(getText(key));
			    emailSetupTemplateURL = "?id="+emailSetupTemplate.getId();
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Save() End");
			return SUCCESS;
	}
	private String languageName;
	private String descriptionName;
	@SkipValidation
	public String search() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" search() Start");
		getComboList();
		emailSetupTemplateList = emailSetupTemplateManager.getListBySearch(moduleName,languageName,descriptionName,sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" search() End");
		return SUCCESS;
	}
	private String moduleName;
	private String jrxmlName;
	private List formsAutoCompleteList = new ArrayList();
	@SkipValidation
	public String getFormsNameAutocomplete(){
		formsAutoCompleteList = reportsManager.findFormsForAutocomplete(moduleName,jrxmlName,sessionCorpID);
		return SUCCESS;
	}
	
	@SkipValidation
	public void deleteFormsIdInEmailSetupTemplate(){
		if(reportsId.contains(",")){
			reportsId = reportsId.substring(0, reportsId.lastIndexOf(','));
		}else{
			reportsId ="";
		}
		emailSetupTemplateManager.updateFormsIdInEmailSetupTemplate(reportsId,id);
	}
	
	private String fileName;
	@SkipValidation
	public void deleteAttachedFileNameInEmailSetupTemplate(){
		emailSetupTemplate = emailSetupTemplateManager.get(id);
		String tempFileName="";
		if ((emailSetupTemplate.getAttachedFileLocation() != null) && (!emailSetupTemplate.getAttachedFileLocation().trim().equalsIgnoreCase(""))) {
			for(String str:emailSetupTemplate.getAttachedFileLocation().split("~")){
        		if((fileName!=null && !fileName.equalsIgnoreCase("")) && (!str.contains(fileName))){
        			if(tempFileName!=null && !tempFileName.equalsIgnoreCase("")){
        				tempFileName = tempFileName+"~"+str;
        			}else{
        				tempFileName = str;
        			}
        		}
			}
		emailSetupTemplate.setAttachedFileLocation(tempFileName);
		emailSetupTemplateManager.save(emailSetupTemplate);
		
        }
	}
	
	@SkipValidation
	public String deleteFileInEmailSetupTemplatePopup(){
		String tempFileName="";
		String fileNameStr = Arrays.toString(attachedFileNameListEdit.toArray());
		fileNameStr = fileNameStr.replace("[", "").replace("]", "");
			for(String str:fileNameStr.split("\\^")){
				if(!str.equalsIgnoreCase(fileName)){
	        		if(tempFileName!=null && !tempFileName.equalsIgnoreCase("")){
	        			tempFileName = tempFileName+"^"+str;
	        		}else{
	        			tempFileName = str;
	        		}
				}
			}
		attachedFileNameListEdit = Arrays.asList(tempFileName.split("^"));
		emailSetupTemplate = emailSetupTemplateManager.get(id);
		return SUCCESS;
	}
	private String pageType;
	private String fileError="";
	private InputStream fileInputStream;
	public String downloadAttachedFile() throws FileNotFoundException {
		try{
			if(id!=null){
				emailSetupTemplate = emailSetupTemplateManager.get(id);
				for(String str:emailSetupTemplate.getAttachedFileLocation().split("~")){
	        		if(str!=null && !str.equalsIgnoreCase("") && str.contains(fileName)){
	        			File fileToDownload = new File(str);
	   	             	fileName = fileToDownload.getName();
	   	             	fileInputStream = new FileInputStream(fileToDownload);
	        		}
	        	}
			}
			/*String fileNameWithPath = "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + "EmailSetupTemplate" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/"+fileName;
			File fileToDownload = new File(fileNameWithPath);
        	fileName = fileToDownload.getName();
        	fileInputStream = new FileInputStream(fileToDownload);*/
		}catch (FileNotFoundException ex) {
			ex.printStackTrace();
			if(id!=null && (pageType==null || pageType.equalsIgnoreCase(""))){
				emailSetupTemplateURL = "editEmailSetupTemplate.html?id="+id+"&fileError=NoFile";
			}else if(cid!=null && pageType.equalsIgnoreCase("popUp")){
				emailSetupTemplateURL = "findEmailSetupTemplateByModuleNameCF.html?cid="+cid+"&fileError=NoFile";
			}else if(sid!=null && pageType.equalsIgnoreCase("popUp")){
				emailSetupTemplateURL = "findEmailSetupTemplateByModuleNameSO.html?sid="+sid+"&fileError=NoFile";
			}
			return ERROR;
		}
		 return SUCCESS; 
	}
	
	public String downloadFileCabinetFile() throws FileNotFoundException {
		try{
			if(id!=null){
				emailSetupTemplate = emailSetupTemplateManager.get(id);
				String filesFromFileCabinet="";
					if(cid!=null){
						customerFile = customerFileManager.get(cid);
						filesFromFileCabinet = emailSetupTemplateManager.findDocsFromFileCabinet(emailSetupTemplate.getFileTypeOfFileCabinet(),customerFile.getSequenceNumber(),sessionCorpID);
					}else if(sid!=null && emailSetupTemplate.getFileTypeOfFileCabinet()!=null && !emailSetupTemplate.getFileTypeOfFileCabinet().equalsIgnoreCase("")){
						serviceOrder = serviceOrderManager.get(sid);
						filesFromFileCabinet = emailSetupTemplateManager.findDocsFromFileCabinet(emailSetupTemplate.getFileTypeOfFileCabinet(),serviceOrder.getShipNumber(),sessionCorpID);
					}
				for(String str:filesFromFileCabinet.split("~")){
	        		if(str!=null && !str.equalsIgnoreCase("") && str.contains(fileName)){
	        			File fileToDownload = new File(str);
	   	             	fileName = fileToDownload.getName();
	   	             	fileInputStream = new FileInputStream(fileToDownload);
	        		}
	        	}
			}
			/*String fileNameWithPath = "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/"+fileName;
			File fileToDownload = new File(fileNameWithPath);
        	fileName = fileToDownload.getName();
        	fileInputStream = new FileInputStream(fileToDownload);*/
		}catch (FileNotFoundException ex) {
			ex.printStackTrace();
			if(cid!=null){
				emailSetupTemplateURL = "findEmailSetupTemplateByModuleNameCF.html?cid="+cid+"&fileError=NoFile";
			}else if(sid!=null){
				emailSetupTemplateURL = "findEmailSetupTemplateByModuleNameSO.html?sid="+sid+"&fileError=NoFile";
			}
			return ERROR;
		}
		 return SUCCESS; 
	}
	private Long fId;
	public String downloadFileCabinetFileFromEdit() throws FileNotFoundException {
		try{
			if(fId!=null){
				myFile=myFileManager.get(fId);
				String fileLocation = myFile.getLocation();
				File fileToDownload = new File(fileLocation);
             	fileName = myFile.getFileFileName();
             	fileInputStream = new FileInputStream(fileToDownload);
			}
		}catch (FileNotFoundException ex) {
			ex.printStackTrace();
			if(cid!=null){
				emailSetupTemplateURL = "findEmailSetupTemplateByModuleNameCF.html?cid="+cid+"&fileError=NoFile";
			}else if(sid!=null){
				emailSetupTemplateURL = "findEmailSetupTemplateByModuleNameSO.html?sid="+sid+"&fileError=NoFile";
			}
			return ERROR;
		}
		 return SUCCESS; 
	}
	
	public String downloadAttachedFileFromEdit() throws FileNotFoundException {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" downloadAttachedFileFromEdit() Start");
		try{
			if(id!=null){
				emailSetupTemplate = emailSetupTemplateManager.get(id);
				if(fileName.contains("File Template : ")){
					fileName = fileName.replaceAll("File Template : ", "");
					for(String str:emailSetupTemplate.getAttachedFileLocation().split("~")){
		        		if(str!=null && !str.equalsIgnoreCase("") && str.contains(fileName)){
		        			File fileToDownload = new File(str);
		   	             	fileName = fileToDownload.getName();
		   	             	fileInputStream = new FileInputStream(fileToDownload);
		        		}
		        	}
				}else if(fileName.contains("File Cabinet : ")){
					String fileNamePrefix = "File Cabinet : "+emailSetupTemplate.getFileTypeOfFileCabinet()+" - ";
					fileName = fileName.replaceAll(fileNamePrefix, "");
					String filesFromFileCabinet="";
						if(cid!=null){
							customerFile = customerFileManager.get(cid);
							filesFromFileCabinet = emailSetupTemplateManager.findDocsFromFileCabinet(emailSetupTemplate.getFileTypeOfFileCabinet(),customerFile.getSequenceNumber(),sessionCorpID);
						}else if(sid!=null && emailSetupTemplate.getFileTypeOfFileCabinet()!=null && !emailSetupTemplate.getFileTypeOfFileCabinet().equalsIgnoreCase("")){
							serviceOrder = serviceOrderManager.get(sid);
							filesFromFileCabinet = emailSetupTemplateManager.findDocsFromFileCabinet(emailSetupTemplate.getFileTypeOfFileCabinet(),serviceOrder.getShipNumber(),sessionCorpID);
						}
					for(String str:filesFromFileCabinet.split("~")){
		        		if(str!=null && !str.equalsIgnoreCase("") && str.contains(fileName)){
		        			File fileToDownload = new File(str);
		   	             	fileName = fileToDownload.getName();
		   	             	fileInputStream = new FileInputStream(fileToDownload);
		        		}
		        	}
				}else if(fileName.contains("JRXML : ")){
					fileName = fileName.replaceAll("JRXML : ", "");
					String fileNameWithPath = "";
					fileNameWithPath = "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + "EmailSetupTemplate" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/"+fileName;
					File fileToDownload = new File(fileNameWithPath);
					if(fileToDownload.exists()){
						fileName = fileToDownload.getName();
						fileInputStream = new FileInputStream(fileToDownload);
					}
				}else{
					List lastSentMailList = new ArrayList();
					if(cid!=null){
						customerFile = customerFileManager.get(cid);
						String fileNumber = customerFile.getSequenceNumber();
						String saveAsVal = emailSetupTemplate.getSaveAs();
						lastSentMailList = emailSetupTemplateManager.findLastSentMail(sessionCorpID,"CustomerFile",fileNumber,saveAsVal);
					}
					if(sid!=null){
						serviceOrder = serviceOrderManager.get(sid);
						String fileNumber = serviceOrder.getShipNumber();
						String saveAsVal = emailSetupTemplate.getSaveAs();
						lastSentMailList = emailSetupTemplateManager.findLastSentMail(sessionCorpID,"ServiceOrder",fileNumber,saveAsVal);
					}
					String lastSentMailAttachLocation="";
					if(lastSentMailList!=null && !lastSentMailList.isEmpty() && lastSentMailList.get(0)!=null){
						EmailSetup emailSetupObj = (EmailSetup)lastSentMailList.get(0);
						lastSentMailAttachLocation = emailSetupObj.getAttchedFileLocation();
						for(String str:lastSentMailAttachLocation.split("~")){
			        		if(str!=null && !str.equalsIgnoreCase("") && str.contains(fileName)){
			        			File fileToDownload = new File(str);
			   	             	fileName = fileToDownload.getName();
			   	             	fileInputStream = new FileInputStream(fileToDownload);
			        		}
			        	}
						
					}
				}
			}
			/*String fileNameWithPath = "";
			fileNameWithPath = "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + "EmailSetupTemplate" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/"+fileName;
			File fileToDownload = new File(fileNameWithPath);
			if(fileToDownload.exists()){
				fileName = fileToDownload.getName();
				fileInputStream = new FileInputStream(fileToDownload);
			}else if(!fileToDownload.exists()){
				fileNameWithPath = "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/"  + sessionCorpID + "/" + getRequest().getRemoteUser() + "/"+fileName;
				File fileToDownload1 = new File(fileNameWithPath);
				if(fileToDownload1.exists()){
					fileName = fileToDownload1.getName();
					fileInputStream = new FileInputStream(fileToDownload1);
				}else{
					if(cid!=null && pageType.equalsIgnoreCase("popUp")){
						emailSetupTemplateURL = "findEmailSetupTemplateByModuleNameCF.html?cid="+cid+"&fileError=NoFile";
						return ERROR;
					}else if(sid!=null && pageType.equalsIgnoreCase("popUp")){
						emailSetupTemplateURL = "findEmailSetupTemplateByModuleNameSO.html?sid="+sid+"&fileError=NoFile";
						return ERROR;
					}
				}
			}*/
		}catch (FileNotFoundException ex) {
			ex.printStackTrace();
			if(cid!=null && pageType.equalsIgnoreCase("popUp")){
				emailSetupTemplateURL = "findEmailSetupTemplateByModuleNameCF.html?cid="+cid+"&fileError=NoFile";
			}else if(sid!=null && pageType.equalsIgnoreCase("popUp")){
				emailSetupTemplateURL = "findEmailSetupTemplateByModuleNameSO.html?sid="+sid+"&fileError=NoFile";
			}
			return ERROR;
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" downloadAttachedFileFromEdit() End");
		return SUCCESS; 
	}
	
	public Date getZeroTimeDate(Date dt) {
	    Date res = dt;
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(dt);
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    res = calendar.getTime();
	    return res;
	}

	@SkipValidation
	public String findEmailSetupTemplateByModuleNameCF(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ListByModuleNameCF() Start");
		if(company!=null){
			if(company.getVoxmeIntegration()!=null){
				voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
			}
			if(company.getEnableMobileMover() && company.getMmStartDate()!=null && company.getMmStartDate().compareTo(getZeroTimeDate(new Date()))<=0 ){
				mmValidation= "Yes";
				if(company.getMmEndDate()!=null && company.getMmEndDate().compareTo(getZeroTimeDate(new Date()))<0 ){
					mmValidation= "No";
				}
			}
		}
		if(cid!=null){
			customerFile = customerFileManager.get(cid);
			String job = customerFile.getJob();
			String language = customerFile.getCustomerLanguagePreference();
			String moveType = customerFile.getMoveType();
			String companyDivision = customerFile.getCompanyDivision();
			String fileNumber = customerFile.getSequenceNumber();
			emailSetupTemplateList = emailSetupTemplateManager.getListByModuleName(sessionCorpID,"CustomerFile",job,language,moveType,companyDivision,"","","",fileNumber,cid);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ListByModuleNameCF() End");
		return SUCCESS;
	}
	@SkipValidation
	public String findEmailSetupTemplateByModuleNameSO(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ListByModuleNameSO() Start");
		if(company!=null){
			if(company.getVoxmeIntegration()!=null){
				voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
			}
		}
		if(sid!=null){
			serviceOrder = serviceOrderManager.get(sid);
			customerFile = customerFileManager.get(serviceOrder.getCustomerFileId());
			String language = customerFile.getCustomerLanguagePreference();
			String job = serviceOrder.getJob();
			String moveType = serviceOrder.getMoveType();
			String companyDivision = serviceOrder.getCompanyDivision();
			String mode = serviceOrder.getMode();
			String routing = serviceOrder.getRouting();
			String serviceType = serviceOrder.getServiceType();
			String fileNumber = serviceOrder.getShipNumber();
			emailSetupTemplateList = emailSetupTemplateManager.getListByModuleName(sessionCorpID,"ServiceOrder",job,language,moveType,companyDivision,mode,routing,serviceType,fileNumber,sid);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" ListByModuleNameSO() End");
		return SUCCESS;
	}
	private String imageInBodyAttachement;
	private EmailSetup emailSetup;
	@SkipValidation
	public String getLastSentMailTemplatePopupAjax(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" getLastSentMailTemplatePopupAjax() Start");
		List lastSentMailList = new ArrayList();	
			if(sid!=null){
				serviceOrder = serviceOrderManager.get(sid);
				String fileNumber = serviceOrder.getShipNumber();
				emailSetupTemplate = emailSetupTemplateManager.get(id);
				String saveAsVal = emailSetupTemplate.getSaveAs();
				lastSentMailList = emailSetupTemplateManager.findLastSentMail(sessionCorpID,"ServiceOrder",fileNumber,saveAsVal);
			}
			if(cid!=null){
				customerFile = customerFileManager.get(cid);
				String fileNumber = customerFile.getSequenceNumber();
				emailSetupTemplate = emailSetupTemplateManager.get(id);
				String saveAsVal = emailSetupTemplate.getSaveAs();
				lastSentMailList = emailSetupTemplateManager.findLastSentMail(sessionCorpID,"CustomerFile",fileNumber,saveAsVal);
			}
			if(lastSentMailList!=null && !lastSentMailList.isEmpty() && lastSentMailList.get(0)!=null){
				EmailSetup emailSetupObj = (EmailSetup)lastSentMailList.get(0);
				emailFromEditedValue = emailSetupObj.getSignature();
				emailToEditedValue = emailSetupObj.getRecipientTo();
				emailCcEditedValue = emailSetupObj.getRecipientCc();
				emailBccEditedValue = emailSetupObj.getRecipientBcc();
				emailSubjectEditedValue	= emailSetupObj.getSubject();
				//emailBodyEditedValue = emailSetupObj.getBody();
				emailBodyEditedValue = emailSetupObj.getLastSentMailBody();
				//lastSentMailId = emailSetupObj.getId();
				
				/*if(emailSetupObj.getAttchedFileLocation()!=null && !emailSetupObj.getAttchedFileLocation().equalsIgnoreCase("")) {
					for(String str:emailSetupObj.getAttchedFileLocation().split("~")){
						if((str!=null && !str.equalsIgnoreCase("")) && str.contains("#image")){
			        		if(imageInBodyAttachement!=null && !imageInBodyAttachement.equalsIgnoreCase("")){
			        			imageInBodyAttachement = imageInBodyAttachement+"~"+str.substring(str.lastIndexOf("/")+1);
			        		}else{
			        			imageInBodyAttachement = str.substring(str.lastIndexOf("/")+1);
			        		}
						}
		        	}
		        }*/
				
			}
			returnAjaxStringValue = emailFromEditedValue+"~"+emailToEditedValue+"~"+emailCcEditedValue+"~"+emailBccEditedValue+"~"+emailSubjectEditedValue+"~"+emailBodyEditedValue;
			
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" getLastSentMailTemplatePopupAjax() End");
		return SUCCESS;
	}
	@SkipValidation
	public String getLastSentMailAttachementAjax(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" getLastSentMailAttachementAjax() Start");
		List lastSentMailList = new ArrayList();
			if(sid!=null){
				serviceOrder = serviceOrderManager.get(sid);
				String fileNumber = serviceOrder.getShipNumber();
				emailSetupTemplate = emailSetupTemplateManager.get(id);
				String saveAsVal = emailSetupTemplate.getSaveAs();
				lastSentMailList = emailSetupTemplateManager.findLastSentMail(sessionCorpID,"ServiceOrder",fileNumber,saveAsVal);
			}
			if(cid!=null){
				customerFile = customerFileManager.get(cid);
				String fileNumber = customerFile.getSequenceNumber();
				emailSetupTemplate = emailSetupTemplateManager.get(id);
				String saveAsVal = emailSetupTemplate.getSaveAs();
				lastSentMailList = emailSetupTemplateManager.findLastSentMail(sessionCorpID,"CustomerFile",fileNumber,saveAsVal);
			}
			if(lastSentMailList!=null && !lastSentMailList.isEmpty() && lastSentMailList.get(0)!=null){
				EmailSetup emailSetupObj = (EmailSetup)lastSentMailList.get(0);
				lastSentMailId = emailSetupObj.getId();
				String fileName="";
				if(emailSetupObj.getAttchedFileLocation()!=null && !emailSetupObj.getAttchedFileLocation().equalsIgnoreCase("")) {
					for(String str:emailSetupObj.getAttchedFileLocation().split("~")){
						if((str!=null && !str.equalsIgnoreCase("")) && !str.contains("#image")){
							
							if(fileName!=null && !fileName.equalsIgnoreCase("")){
			        			if(!str.contains("C:")){
			        				fileName = fileName+"^"+str.substring(str.lastIndexOf("/")+1);
			        			}else{
			        				fileName = fileName+"^"+str.substring(str.lastIndexOf("\\")+1);
			        			}
			        		}else{
			        			if(!str.contains("C:")){
			        				fileName = str.substring(str.lastIndexOf("/")+1);
			        			}else{
			        				fileName = str.substring(str.lastIndexOf("\\")+1);
			        			}
			        		}
						}
		        	}
		        }
				attachedFileNameListEdit = Arrays.asList(fileName.split("^"));
			}
			
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" getLastSentMailAttachementAjax() End");
		return SUCCESS;
	}
	private String returnAjaxStringValue;
	@SkipValidation
	public String uploadImageForEmailSetupTemplateBody() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" UploadImageInEmailBody() Start");
		String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/" ;
		uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + "EmailSetupTemplateBody" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
		
		File dirPath = new File(uploadDir);
        if (!dirPath.exists()) {
            dirPath.mkdirs();
        }
        String fileNameTemp="";
        try {	
			MultiPartRequestWrapper multiWrapper = (MultiPartRequestWrapper) ServletActionContext.getRequest();
			String[] fileName = multiWrapper.getFileNames("file");
			for(String s : fileName) {
				fileNameTemp = s;
				fileNameTemp = fileNameTemp.replaceAll(" ", "_");
				//System.out.println(s);
			}
			// Get a Files[] object for the uploaded File
			File[] files = multiWrapper.getFiles("file");
			for(int i = 0; i < files.length; i++) {
		         File fileToCreate = new File(uploadDir,fileNameTemp);
		         FileUtils.copyFile(files[i], fileToCreate);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
            
		returnAjaxStringValue = dirPath.getAbsolutePath()+ Constants.FILE_SEP +fileNameTemp;
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" UploadImageInEmailBody() End");
		return SUCCESS;
	}
	private String moduleFieldValue;
	private String othersFieldValue;
	@SkipValidation
	public String testEmailSetupTestConditionQuery(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" testEmailSetupTestConditionQuery() Start");
		returnAjaxStringValue =emailSetupTemplateManager.testEmailSetupTestConditionQuery(moduleFieldValue,othersFieldValue,sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" testEmailSetupTestConditionQuery() End");
		return SUCCESS;
	}
	private EmailSetupTemplate copyEmailSetupTemplate;
	@SkipValidation 
    public String copyEmailSetupTemplate() throws Exception {
		getComboList();
    	try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" copyEmailSetupTemplate() Start");
			copyEmailSetupTemplate = emailSetupTemplateManager.get(id);
			emailSetupTemplate = new EmailSetupTemplate();
			BeanUtilsBean beanUtilsServiceOrder = BeanUtilsBean.getInstance();
		    beanUtilsServiceOrder.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
		    beanUtilsServiceOrder.copyProperties(emailSetupTemplate, copyEmailSetupTemplate);
		    emailSetupTemplate.setFileTypeOfFileCabinet("");
		    emailSetupTemplate.setAttachedFileLocation("");
		    emailSetupTemplate.setFormsId("");
		    emailSetupTemplate.setCreatedBy(getRequest().getRemoteUser());
			emailSetupTemplate.setUpdatedBy(getRequest().getRemoteUser());
			emailSetupTemplate.setCreatedOn(new Date());
			emailSetupTemplate.setUpdatedOn(new Date());
			emailSetupTemplate.setId(null);
			int orderNumverValue = emailSetupTemplateManager.getMaxOrderNumber(sessionCorpID);
			emailSetupTemplate.setOrderNumber(orderNumverValue+1);
			tableWithColumnsNameList = emailSetupTemplateManager.getTableWithColumnsNameMap();
			
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" copyEmailSetupTemplate() End");
    	} catch (Exception e) {
			e.printStackTrace();
		}
    	return SUCCESS;
	}
	private String emailBodyCurrentValue;
	private String emailToCurrentValue;
	private String emailSubjectCurrentValue;
	private String emailCcCurrentValue;
	private Long idCurrentValue;
	@SkipValidation
	public String sendEmailFromEmailTemplate() throws AddressException, MessagingException, Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" SendEmailFromEmailSetupTemplate() Start");
		if(id!=null){
			emailSetupTemplate = emailSetupTemplateManager.get(id);
		}else{
			emailSetupTemplate = emailSetupTemplateManager.get(idCurrentValue);
		}
		if(cid!=null){
			customerFile = customerFileManager.get(cid);
		}
		if(sid!=null){
			serviceOrder = serviceOrderManager.get(sid);
			trackingStatus = trackingStatusManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid);
			customerFile = customerFileManager.get(serviceOrder.getCustomerFileId());
		}
		String logMessage ="Mail has been scheduled succesfully.";
		String tempEmailFromVal="";
		String emailFrom = "";
		emailFrom = emailSetupTemplate.getEmailFrom();
		if(emailFrom!=null && !emailFrom.equalsIgnoreCase("")){
			String[] splittedEmailFromValue = emailFrom.split(",");
			for (String splittedEmailFrom : splittedEmailFromValue){
				if(splittedEmailFrom.contains("^$")){
					splittedEmailFrom = splittedEmailFrom.replaceAll("\\^\\$", "").replaceAll("\\$\\^", "");
		        	if(splittedEmailFrom.contains(".")){
		        		String [] tableFieldnameArr = splittedEmailFrom.split("\\.");
		        		String tableName = tableFieldnameArr[0];
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
		        			String fieldName = tableFieldnameArr[1];
		        			try{
			        			Class<?> cls = customerFile.getClass();
			        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
			        			modelFieldName.setAccessible(true);
								Object tempFieldValue =modelFieldName.get(customerFile);
								if(tempFieldValue!=null && !tempFieldValue.equals("")){
									if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("coordinator") && !fieldName.trim().equalsIgnoreCase("personPricing") && !fieldName.trim().equalsIgnoreCase("personBilling") && !fieldName.trim().equalsIgnoreCase("personPayable") && !fieldName.trim().equalsIgnoreCase("auditor")&& !fieldName.trim().equalsIgnoreCase("approvedBy")){
										if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
											tempEmailFromVal = tempFieldValue+"";
										}else{
											tempEmailFromVal = tempEmailFromVal+","+tempFieldValue+"";
										}
									}else{
										userNew = userManager.getUserByUsername(tempFieldValue+"");
					        			Class<?> cls1 = userNew.getClass();
					        			Field usrModelFieldName = cls1.getDeclaredField("email");
					        			usrModelFieldName.setAccessible(true);
										Object usrFieldValue =usrModelFieldName.get(userNew);
										if(usrFieldValue!=null && !usrFieldValue.equals("")){
											if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
												tempEmailFromVal = usrFieldValue+"";
											}else{
												tempEmailFromVal = tempEmailFromVal+","+usrFieldValue+"";
											}
										}
									}
								}
		        			}catch(IllegalAccessException e){
		        				e.printStackTrace();
		        				int pos =e.toString().lastIndexOf(":");
			            		String errorField = e.toString().substring(pos);
		        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
		        				returnAjaxStringValue = logMessage;
		        				return SUCCESS;
		        			} catch (NoSuchFieldException e) {
		        				e.printStackTrace();
		        				int pos =e.toString().lastIndexOf(":");
			            		String errorField = e.toString().substring(pos);
		        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email From.";
		        				returnAjaxStringValue = logMessage;
		        				return SUCCESS;
		        			}
		        		}
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
		        			if(sid==null){
		        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email From as module is CustomerFile";
		        				returnAjaxStringValue = logMessage;
		        				return SUCCESS;
	        				}else{
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = serviceOrder.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(serviceOrder);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("salesMan") && !fieldName.trim().equalsIgnoreCase("coordinator")){
											if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
												tempEmailFromVal = tempFieldValue+"";
											}else{
												tempEmailFromVal = tempEmailFromVal+","+tempFieldValue+"";
											}
										}else{
											userNew = userManager.getUserByUsername(tempFieldValue+"");
						        			Class<?> cls1 = userNew.getClass();
						        			Field usrModelFieldName = cls1.getDeclaredField("email");
						        			usrModelFieldName.setAccessible(true);
											Object usrFieldValue =usrModelFieldName.get(userNew);
											if(usrFieldValue!=null && !usrFieldValue.equals("")){
												if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
													tempEmailFromVal = usrFieldValue+"";
												}else{
													tempEmailFromVal = tempEmailFromVal+","+usrFieldValue+"";
												}
											}
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
			        				returnAjaxStringValue = logMessage;
			        				return SUCCESS;
			        			} catch (NoSuchFieldException e) {
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email From.";
			        				returnAjaxStringValue = logMessage;
			        				return SUCCESS;
			        			}
			        		}
		        		}
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
		        			if(sid==null){
		        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use TrackingStatus table in email From as module is CustomerFile";
		        				returnAjaxStringValue = logMessage;
		        				return SUCCESS;
	        				}else{
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = trackingStatus.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			Object fieldType = modelFieldName.getType();
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(trackingStatus);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
											tempEmailFromVal = tempFieldValue+"";
										}else{
											tempEmailFromVal = tempEmailFromVal+","+tempFieldValue+"";
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
			        				returnAjaxStringValue = logMessage;
			        				return SUCCESS;
			        			} catch (NoSuchFieldException e) {
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email From.";
			        				returnAjaxStringValue = logMessage;
			        				return SUCCESS;
			        			}
			        		}
		        		}
		        	}
				}else{
					if(splittedEmailFrom!=null && !splittedEmailFrom.equalsIgnoreCase("")){
						if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
							tempEmailFromVal  = splittedEmailFrom;
						}else{
							tempEmailFromVal  = tempEmailFromVal+","+splittedEmailFrom;
						}
					}
				}
			}
			emailFrom = tempEmailFromVal;
		}
		if(emailFrom==null || emailFrom.equalsIgnoreCase("")){
			logMessage =  "Mail can not be sent because <BR> ErrorIN : email From is blank.";
			returnAjaxStringValue = logMessage;
			return SUCCESS;
		}
		String tempEmailToVal = "";
		String emailTo = "";
		if(idCurrentValue!=null){
			emailTo = emailToCurrentValue;
		}else{
			emailTo = emailSetupTemplate.getEmailTo();
		}
		if(emailTo!=null && !emailTo.equalsIgnoreCase("")){
			String[] splittedEmailToValue = emailTo.split(",");
			for (String splittedEmailTo : splittedEmailToValue){
				if(splittedEmailTo.contains("^$")){
					splittedEmailTo = splittedEmailTo.replaceAll("\\^\\$", "").replaceAll("\\$\\^", "");
		        	if(splittedEmailTo.contains(".")){
		        		String [] tableFieldnameArr = splittedEmailTo.split("\\.");
		        		String tableName = tableFieldnameArr[0];
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
		        			String fieldName = tableFieldnameArr[1];
		        			try{
			        			Class<?> cls = customerFile.getClass();
			        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
			        			modelFieldName.setAccessible(true);
								Object tempFieldValue =modelFieldName.get(customerFile);
								if(tempFieldValue!=null && !tempFieldValue.equals("")){
									if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("coordinator") && !fieldName.trim().equalsIgnoreCase("personPricing") && !fieldName.trim().equalsIgnoreCase("personBilling") && !fieldName.trim().equalsIgnoreCase("personPayable") && !fieldName.trim().equalsIgnoreCase("auditor")&& !fieldName.trim().equalsIgnoreCase("approvedBy")){
										if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
											tempEmailToVal = tempFieldValue+"";
										}else{
											tempEmailToVal = tempEmailToVal+","+tempFieldValue+"";
										}
									}else{
										userNew = userManager.getUserByUsername(tempFieldValue+"");
					        			Class<?> cls1 = userNew.getClass();
					        			Field usrModelFieldName = cls1.getDeclaredField("email");
					        			usrModelFieldName.setAccessible(true);
										Object usrFieldValue =usrModelFieldName.get(userNew);
										if(usrFieldValue!=null && !usrFieldValue.equals("")){
											if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
												tempEmailToVal = usrFieldValue+"";
											}else{
												tempEmailToVal = tempEmailToVal+","+usrFieldValue+"";
											}
										}
									}
								}
		        			}catch(IllegalAccessException e){
		        				e.printStackTrace();
		        				int pos =e.toString().lastIndexOf(":");
			            		String errorField = e.toString().substring(pos);
		        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
		        				returnAjaxStringValue = logMessage;
		        				return SUCCESS;
		        			} catch (NoSuchFieldException e) {
		        				e.printStackTrace();
		        				int pos =e.toString().lastIndexOf(":");
			            		String errorField = e.toString().substring(pos);
		        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email To.";
		        				returnAjaxStringValue = logMessage;
		        				return SUCCESS;
		        			}
		        		}
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
		        			if(sid==null){
		        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email To as module is CustomerFile";
		        				returnAjaxStringValue = logMessage;
		        				return SUCCESS;
	        				}else{
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = serviceOrder.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(serviceOrder);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("salesMan") && !fieldName.trim().equalsIgnoreCase("coordinator")){
											if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
												tempEmailToVal = tempFieldValue+"";
											}else{
												tempEmailToVal = tempEmailToVal+","+tempFieldValue+"";
											}
										}else{
											userNew = userManager.getUserByUsername(tempFieldValue+"");
						        			Class<?> cls1 = userNew.getClass();
						        			Field usrModelFieldName = cls1.getDeclaredField("email");
						        			usrModelFieldName.setAccessible(true);
											Object usrFieldValue =usrModelFieldName.get(userNew);
											if(usrFieldValue!=null && !usrFieldValue.equals("")){
												if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
													tempEmailToVal = usrFieldValue+"";
												}else{
													tempEmailToVal = tempEmailToVal+","+usrFieldValue+"";
												}
											}
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
			        				returnAjaxStringValue = logMessage;
			        				return SUCCESS;
			        			} catch (NoSuchFieldException e) {
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email To.";
			        				returnAjaxStringValue = logMessage;
			        				return SUCCESS;
			        			}
			        		}
		        		}
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
		        			if(sid==null){
		        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use TrackingStatus table in email To as module is CustomerFile";
		        				returnAjaxStringValue = logMessage;
		        				return SUCCESS;
	        				}else{
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = trackingStatus.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(trackingStatus);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
											tempEmailToVal = tempFieldValue+"";
										}else{
											tempEmailToVal = tempEmailToVal+","+tempFieldValue+"";
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
			        				returnAjaxStringValue = logMessage;
			        				return SUCCESS;
			        			} catch (NoSuchFieldException e) {
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email To.";
			        				returnAjaxStringValue = logMessage;
			        				return SUCCESS;
			        			}
			        		}
		        		}
		        	}
				}else{
					if(splittedEmailTo!=null && !splittedEmailTo.equalsIgnoreCase("")){
						if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
							tempEmailToVal  = splittedEmailTo;
						}else{
							tempEmailToVal  = tempEmailToVal+","+splittedEmailTo;
						}
					}
				}
			}
			emailTo = tempEmailToVal;
		}
		if(emailTo==null || emailTo.equalsIgnoreCase("")){
			logMessage =  "Mail can not be sent because <BR> ErrorIN : email To is blank.";
			returnAjaxStringValue = logMessage;
			return SUCCESS;
		}
		String tempEmailCcVal="";
		String emailCc = "";
		if(idCurrentValue!=null){
			emailCc = emailCcCurrentValue;
		}else{
			emailCc = emailSetupTemplate.getEmailCc();
		}
		if(emailCc!=null && !emailCc.equalsIgnoreCase("")){
			String[] splittedEmailCcValue = emailCc.split(",");
			for (String splittedEmailCc : splittedEmailCcValue){
				if(splittedEmailCc.contains("^$")){
					splittedEmailCc = splittedEmailCc.replaceAll("\\^\\$", "").replaceAll("\\$\\^", "");
		        	if(splittedEmailCc.contains(".")){
		        		String [] tableFieldnameArr = splittedEmailCc.split("\\.");
		        		String tableName = tableFieldnameArr[0];
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
		        			String fieldName = tableFieldnameArr[1];
		        			try{
			        			Class<?> cls = customerFile.getClass();
			        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
			        			modelFieldName.setAccessible(true);
								Object tempFieldValue =modelFieldName.get(customerFile);
								if(tempFieldValue!=null && !tempFieldValue.equals("")){
									if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("coordinator") && !fieldName.trim().equalsIgnoreCase("personPricing") && !fieldName.trim().equalsIgnoreCase("personBilling") && !fieldName.trim().equalsIgnoreCase("personPayable") && !fieldName.trim().equalsIgnoreCase("auditor")&& !fieldName.trim().equalsIgnoreCase("approvedBy")){
										if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
											tempEmailCcVal = tempFieldValue+"";
										}else{
											tempEmailCcVal = tempEmailCcVal+","+tempFieldValue+"";
										}
									}else{
										userNew = userManager.getUserByUsername(tempFieldValue+"");
					        			Class<?> cls1 = userNew.getClass();
					        			Field usrModelFieldName = cls1.getDeclaredField("email");
					        			usrModelFieldName.setAccessible(true);
										Object usrFieldValue =usrModelFieldName.get(userNew);
										if(usrFieldValue!=null && !usrFieldValue.equals("")){
											if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
												tempEmailCcVal = usrFieldValue+"";
											}else{
												tempEmailCcVal = tempEmailCcVal+","+usrFieldValue+"";
											}
										}
									}
								}
		        			}catch(IllegalAccessException e){
		        				e.printStackTrace();
		        				int pos =e.toString().lastIndexOf(":");
			            		String errorField = e.toString().substring(pos);
		        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
		        				returnAjaxStringValue = logMessage;
		        				return SUCCESS;
		        			} catch (NoSuchFieldException e) {
		        				e.printStackTrace();
		        				int pos =e.toString().lastIndexOf(":");
			            		String errorField = e.toString().substring(pos);
		        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Cc.";
		        				returnAjaxStringValue = logMessage;
		        				return SUCCESS;
		        			}
		        		}
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
		        			if(sid==null){
		        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email Cc as module is CustomerFile";
		        				returnAjaxStringValue = logMessage;
		        				return SUCCESS;
	        				}else{
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = serviceOrder.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(serviceOrder);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("salesMan") && !fieldName.trim().equalsIgnoreCase("coordinator")){
											if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
												tempEmailCcVal = tempFieldValue+"";
											}else{
												tempEmailCcVal = tempEmailCcVal+","+tempFieldValue+"";
											}
										}else{
											userNew = userManager.getUserByUsername(tempFieldValue+"");
						        			Class<?> cls1 = userNew.getClass();
						        			Field usrModelFieldName = cls1.getDeclaredField("email");
						        			usrModelFieldName.setAccessible(true);
											Object usrFieldValue =usrModelFieldName.get(userNew);
											if(usrFieldValue!=null && !usrFieldValue.equals("")){
												if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
													tempEmailCcVal = usrFieldValue+"";
												}else{
													tempEmailCcVal = tempEmailCcVal+","+usrFieldValue+"";
												}
											}
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
			        				returnAjaxStringValue = logMessage;
			        				return SUCCESS;
			        			} catch (NoSuchFieldException e) {
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Cc.";
			        				returnAjaxStringValue = logMessage;
			        				return SUCCESS;
			        			}
			        		}
		        		}
		        	}
	        	}else{
					if(splittedEmailCc!=null && !splittedEmailCc.equalsIgnoreCase("")){
						if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
							tempEmailCcVal  = splittedEmailCc;
						}else{
							tempEmailCcVal  = tempEmailCcVal+","+splittedEmailCc;
						}
					}
				}
			}
			emailCc = tempEmailCcVal;
		}
		String tempEmailBccVal ="";
		String emailBcc = "";
		emailBcc = emailSetupTemplate.getEmailBcc();
		if(emailBcc!=null && !emailBcc.equalsIgnoreCase("")){
			String[] splittedEmailBccValue = emailBcc.split(",");
			for (String splittedEmailBcc : splittedEmailBccValue){
				if(splittedEmailBcc.contains("^$")){
					splittedEmailBcc = splittedEmailBcc.replaceAll("\\^\\$", "").replaceAll("\\$\\^", "");
		        	if(splittedEmailBcc.contains(".")){
		        		String [] tableFieldnameArr = splittedEmailBcc.split("\\.");
		        		String tableName = tableFieldnameArr[0];
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
		        			String fieldName = tableFieldnameArr[1];
		        			try{
			        			Class<?> cls = customerFile.getClass();
			        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
			        			modelFieldName.setAccessible(true);
								Object tempFieldValue =modelFieldName.get(customerFile);
								if(tempFieldValue!=null && !tempFieldValue.equals("")){
									if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("coordinator") && !fieldName.trim().equalsIgnoreCase("personPricing") && !fieldName.trim().equalsIgnoreCase("personBilling") && !fieldName.trim().equalsIgnoreCase("personPayable") && !fieldName.trim().equalsIgnoreCase("auditor")&& !fieldName.trim().equalsIgnoreCase("approvedBy")){
										if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
											tempEmailBccVal = tempFieldValue+"";
										}else{
											tempEmailBccVal = tempEmailBccVal+","+tempFieldValue+"";
										}
									}else{
										userNew = userManager.getUserByUsername(tempFieldValue+"");
					        			Class<?> cls1 = userNew.getClass();
					        			Field usrModelFieldName = cls1.getDeclaredField("email");
					        			usrModelFieldName.setAccessible(true);
										Object usrFieldValue =usrModelFieldName.get(userNew);
										if(usrFieldValue!=null && !usrFieldValue.equals("")){
											if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
												tempEmailBccVal = usrFieldValue+"";
											}else{
												tempEmailBccVal = tempEmailBccVal+","+usrFieldValue+"";
											}
										}
									}
								}
		        			}catch(IllegalAccessException e){
		        				e.printStackTrace();
		        				int pos =e.toString().lastIndexOf(":");
			            		String errorField = e.toString().substring(pos);
		        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
		        				returnAjaxStringValue = logMessage;
		        				return SUCCESS;
		        			} catch (NoSuchFieldException e) {
		        				e.printStackTrace();
		        				int pos =e.toString().lastIndexOf(":");
			            		String errorField = e.toString().substring(pos);
		        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Bcc.";
		        				returnAjaxStringValue = logMessage;
		        				return SUCCESS;
		        			}
		        		}
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
		        			if(sid==null){
		        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email Bcc as module is CustomerFile";
		        				returnAjaxStringValue = logMessage;
		        				return SUCCESS;
	        				}else{
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = serviceOrder.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(serviceOrder);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("salesMan") && !fieldName.trim().equalsIgnoreCase("coordinator")){
											if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
												tempEmailBccVal = tempFieldValue+"";
											}else{
												tempEmailBccVal = tempEmailBccVal+","+tempFieldValue+"";
											}
										}else{
											userNew = userManager.getUserByUsername(tempFieldValue+"");
						        			Class<?> cls1 = userNew.getClass();
						        			Field usrModelFieldName = cls1.getDeclaredField("email");
						        			usrModelFieldName.setAccessible(true);
											Object usrFieldValue =usrModelFieldName.get(userNew);
											if(usrFieldValue!=null && !usrFieldValue.equals("")){
												if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
													tempEmailBccVal = usrFieldValue+"";
												}else{
													tempEmailBccVal = tempEmailBccVal+","+usrFieldValue+"";
												}
											}
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
			        				returnAjaxStringValue = logMessage;
			        				return SUCCESS;
			        			} catch (NoSuchFieldException e) {
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Bcc.";
			        				returnAjaxStringValue = logMessage;
			        				return SUCCESS;
			        			}
			        		}
		        		}
		        	}
				}else{
					if(splittedEmailBcc!=null && !splittedEmailBcc.equalsIgnoreCase("")){
						if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
							tempEmailBccVal  = splittedEmailBcc;
						}else{
							tempEmailBccVal  = tempEmailBccVal+","+splittedEmailBcc;
						}
					}
				}
			}
			emailBcc = tempEmailBccVal;
		}
		
		String subject = "";
		String subjectTemp = "";
		if(idCurrentValue!=null){
			subjectTemp = emailSubjectCurrentValue;
		}else{
			subjectTemp = emailSetupTemplate.getEmailSubject();
		}
		String subjectEmptyTokenizer="";
		StringBuilder emailSubjectValue = new StringBuilder();
		if(subjectTemp.contains("^$")){
			StringTokenizer subjectTokenizer = new StringTokenizer(subjectTemp);
	        while (subjectTokenizer.hasMoreTokens()) {
	        	subjectEmptyTokenizer = subjectTokenizer.nextToken();
	        	if(subjectEmptyTokenizer.contains("^$")){
	        		String[] splittedValue = subjectEmptyTokenizer.split(Pattern.quote("^$"));
					for (String splittedWord : splittedValue){
						if(splittedWord.contains("$^") && splittedWord.contains(".")){
							String[] splittedValueNew = splittedWord.split(Pattern.quote("$^"));
							  for (String splittedWordNew : splittedValueNew){
									String tempTableAndFieldname = splittedWordNew.replaceAll("\\^\\$", "");
						        	if(tempTableAndFieldname.contains(".") && (tempTableAndFieldname.toLowerCase().contains("customerfile") || tempTableAndFieldname.toLowerCase().contains("serviceorder") || tempTableAndFieldname.toLowerCase().contains("trackingstatus"))){
						        		String [] tableFieldnameArr = tempTableAndFieldname.split("\\.");
						        		String tableName = tableFieldnameArr[0];
						        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
						        			String fieldName = tableFieldnameArr[1];
						        			try{
							        			Class<?> cls = customerFile.getClass();
							        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
							        			Object fieldType = modelFieldName.getType();
							        			modelFieldName.setAccessible(true);
												Object tempFieldValue =modelFieldName.get(customerFile);
												if(tempFieldValue!=null && !tempFieldValue.equals("")){
													if(fieldType.toString().equals("class java.util.Date")){
														SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
														Date tempDateValue = (Date) tempFieldValue;
											    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
											    		String dateValue = nowYYYYMMDD1.toString();
											    		emailSubjectValue.append(dateValue+" ");
													}else{
														emailSubjectValue.append(tempFieldValue+" ");
													}
												}
						        			}catch(IllegalAccessException e){
						        				e.printStackTrace();
						        				int pos =e.toString().lastIndexOf(":");
							            		String errorField = e.toString().substring(pos);
						        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+errorField+" in "+tableName;
						        				returnAjaxStringValue = logMessage;
						        				return SUCCESS;
						        			} catch (NoSuchFieldException e) {
						        				e.printStackTrace();
						        				int pos =e.toString().lastIndexOf(":");
							            		String errorField = e.toString().substring(pos);
						        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".\nIncorrect field name in email Subject.";
						        				returnAjaxStringValue = logMessage;
						        				return SUCCESS;
						        			}
						        		}
						        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
						        			if(sid==null){
						        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email Subject as module is CustomerFile";
						        				returnAjaxStringValue = logMessage;
						        				return SUCCESS;
					        				}else{
							        			String fieldName = tableFieldnameArr[1];
							        			try{
								        			Class<?> cls = serviceOrder.getClass();
								        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
								        			Object fieldType = modelFieldName.getType();
								        			modelFieldName.setAccessible(true);
													Object tempFieldValue =modelFieldName.get(serviceOrder);
													if(tempFieldValue!=null && !tempFieldValue.equals("")){
														if(fieldType.toString().equals("class java.util.Date")){
															SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
															Date tempDateValue = (Date) tempFieldValue;
												    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
												    		String dateValue = nowYYYYMMDD1.toString();
												    		emailSubjectValue.append(dateValue+" ");
														}else{
															emailSubjectValue.append(tempFieldValue+" ");
														}
													}
							        			}catch(IllegalAccessException e){
							        				e.printStackTrace();
							        				int pos =e.toString().lastIndexOf(":");
								            		String errorField = e.toString().substring(pos);
							        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+errorField+" in "+tableName;
							        				returnAjaxStringValue = logMessage;
							        				return SUCCESS;
							        			} catch (NoSuchFieldException e) {
							        				e.printStackTrace();
							        				int pos =e.toString().lastIndexOf(":");
								            		String errorField = e.toString().substring(pos);
							        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Subject.";
							        				returnAjaxStringValue = logMessage;
							        				return SUCCESS;
							        			}
							        		}
						        		}
						        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
						        			if(sid==null){
						        				logMessage = "Mail can not be sent because<BR> ErrorIN : You can,t use TrackingStatus table in email Subject as module is CustomerFile";
						        				returnAjaxStringValue = logMessage;
						        				return SUCCESS;
					        				}else{
							        			String fieldName = tableFieldnameArr[1];
							        			try{
								        			Class<?> cls = trackingStatus.getClass();
								        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
								        			Object fieldType = modelFieldName.getType();
								        			modelFieldName.setAccessible(true);
													Object tempFieldValue =modelFieldName.get(trackingStatus);
													if(tempFieldValue!=null && !tempFieldValue.equals("")){
														if(fieldType.toString().equals("class java.util.Date")){
															SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
															Date tempDateValue = (Date) tempFieldValue;
												    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
												    		String dateValue = nowYYYYMMDD1.toString();
												    		emailSubjectValue.append(dateValue+" ");
														}else{
															emailSubjectValue.append(tempFieldValue+" ");
														}
													}
							        			}catch(IllegalAccessException e){
							        				e.printStackTrace();
							        				int pos =e.toString().lastIndexOf(":");
								            		String errorField = e.toString().substring(pos);
							        				logMessage =  "Mail can not be sent because<BR> ErrorIN "+ errorField+" in "+tableName;
							        				returnAjaxStringValue = logMessage;
							        				return SUCCESS;
							        			} catch (NoSuchFieldException e) {
							        				e.printStackTrace();
							        				int pos =e.toString().lastIndexOf(":");
								            		String errorField = e.toString().substring(pos);
							        				logMessage =  "Mail can not be sent because<BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Subject.";
							        				returnAjaxStringValue = logMessage;
							        				return SUCCESS;
							        			}
							        		}
						        		}
						        	}else{
						        		emailSubjectValue.append(tempTableAndFieldname+" ");
						        	}
						  		}
							}else{
								if(splittedWord!=null && !splittedWord.equalsIgnoreCase("")){
									emailSubjectValue.append(splittedWord+" ");
								}
							}
						}
					}else{
			        	emailSubjectValue.append(subjectEmptyTokenizer+" ");
			        }
	        	}
		}else{
			emailSubjectValue.append(subjectTemp+" ");
		}
		subject = emailSubjectValue.toString();
		
		String fileLocation = "";
		//fileLocation = emailSetupTemplate.getAttachedFileLocation();
		if(emailSetupTemplate.getAttachedFileLocation()!=null && !emailSetupTemplate.getAttachedFileLocation().equalsIgnoreCase("")){
			for(String str1:emailSetupTemplate.getAttachedFileLocation().split("~")){
				if(str1!=null && !str1.equalsIgnoreCase("")){
					File f = new File(str1);
					String tempFileName = str1.substring(str1.lastIndexOf("/")+1);
					if(f.exists()){
						if(fileLocation!=null && !fileLocation.equalsIgnoreCase("") && (tempFileName!=null && !tempFileName.equalsIgnoreCase(""))){
							fileLocation = fileLocation+"~"+str1;
		        		}else{
		        			if(tempFileName!=null && !tempFileName.equalsIgnoreCase("")){
		        				fileLocation = str1;
		        			}
		        		}
					}else if(!f.exists()){
						String fileName = f.getName();
						String doubleQuote = "-";
						logMessage =  "ErrorIN : File template "+doubleQuote+" "+fileName+" "+doubleQuote+" not found.<BR>Use Edit to manually send this e-mail.";
        				returnAjaxStringValue = logMessage;
        				return SUCCESS;
					}
				}
			}
		}
		String emailBody ="";
		if(idCurrentValue!=null){
			emailBody = emailBodyCurrentValue;
		}else{
			emailBody = emailSetupTemplate.getEmailBody();
		}
		if(emailBody!=null && !emailBody.equalsIgnoreCase("")){
			emailBody = emailBody.replaceAll("<br />", "<br/>");
			StringBuilder emailBodyTempValue = new StringBuilder();
			String str;
			BufferedReader reader = new BufferedReader(new StringReader(emailBody));
			String bodyEmptyTokenizer="";
			try {       
				while((str = reader.readLine()) != null) {
					if(str.contains("^$")){
						  StringTokenizer bodyTokenizer = new StringTokenizer(str);
						  while (bodyTokenizer.hasMoreTokens()) {
							  bodyEmptyTokenizer = bodyTokenizer.nextToken();
							  if(bodyEmptyTokenizer.contains("^$")){
								String[] splittedValue = bodyEmptyTokenizer.split(Pattern.quote("^$"));
								for (String splittedWord : splittedValue){
								if(splittedWord.contains("$^") && splittedWord.contains(".") && splittedWord.contains("~")){
									String modelFieldValue = "";
									String[] splittedValueByTild = splittedWord.split(Pattern.quote("~"));
									for(String splittedWordByTild : splittedValueByTild){
										if(splittedWordByTild.contains("$^")){
											String[] splittWordArr = splittedWordByTild.split(Pattern.quote("$^"));
											for(String splittWordVal : splittWordArr){
												if(splittWordVal.contains(".")){
													String [] tableFieldNameArr = splittWordVal.split("\\.");
													if(tableFieldNameArr.length!=0){
									        		String tableName = tableFieldNameArr[0];
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("user") && !modelFieldValue.equalsIgnoreCase("")){
									        			String tempFieldName = tableFieldNameArr[1];
									        			try{
										        			userNew = userManager.getUserByUsername(modelFieldValue);
										        			Class<?> cls = userNew.getClass();
										        			Field modelFieldName = cls.getDeclaredField(tempFieldName.trim());
										        			Object fieldType = modelFieldName.getType();
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(userNew);
															if(tempFieldValue!=null && !tempFieldValue.equals("")){
																if(fieldType.toString().equals("class java.util.Date")){
																	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																	Date tempDateValue = (Date) tempFieldValue;
														    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
														    		String dateValue = nowYYYYMMDD1.toString();
																	emailBodyTempValue.append(dateValue+" ");
																}else{
																	emailBodyTempValue.append(tempFieldValue+" ");
																}
															}
										        		}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because<BR> ErrorIN "+ errorField+" in "+tableName;
									        				returnAjaxStringValue = logMessage;
									        				return SUCCESS;
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because<BR> ErrorIN"+ errorField+" in "+tableName+".Incorrect field name in email Subject.";
									        				returnAjaxStringValue = logMessage;
									        				return SUCCESS;
									        			}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("partnerPublic") && !modelFieldValue.equalsIgnoreCase("")){
									        			String tempFieldName = tableFieldNameArr[1];
									        			try{
										        			partnerPublicObj = partnerPublicManager.getPartnerByCode(modelFieldValue);
										        			Class<?> cls = partnerPublicObj.getClass();
										        			Field modelFieldName = cls.getDeclaredField(tempFieldName.trim());
										        			Object fieldType = modelFieldName.getType();
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(partnerPublicObj);
															if(tempFieldValue!=null && !tempFieldValue.equals("")){
																if(fieldType.toString().equals("class java.util.Date")){
																	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																	Date tempDateValue = (Date) tempFieldValue;
														    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
														    		String dateValue = nowYYYYMMDD1.toString();
																	emailBodyTempValue.append(dateValue+" ");
																}else{
																	emailBodyTempValue.append(tempFieldValue+" ");
																}
															}
										        		}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
									        				returnAjaxStringValue = logMessage;
									        				return SUCCESS;
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Subject.";
									        				returnAjaxStringValue = logMessage;
									        				return SUCCESS;
									        			}
									        		}
												}
												}else{
													emailBodyTempValue.append(splittWordVal+" ");
												}
											}
										}else{
											String tempTableAndFieldname = splittedWordByTild.replaceAll("\\^\\$", "");
								        	if(tempTableAndFieldname.contains(".")){
								        		String [] tableFieldnameArr = tempTableAndFieldname.split("\\.");
								        		if(tableFieldnameArr.length!=0){
								        		String tableName = tableFieldnameArr[0];
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
								        			String fieldName = tableFieldnameArr[1];
								        			try{
									        			Class<?> cls = customerFile.getClass();
									        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
									        			modelFieldName.setAccessible(true);
														Object tempFieldValue =modelFieldName.get(customerFile);
														modelFieldValue =tempFieldValue+""; 
								        			}catch(IllegalAccessException e){
								        				e.printStackTrace();
								        				int pos =e.toString().lastIndexOf(":");
									            		String errorField = e.toString().substring(pos);
								        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
								        				returnAjaxStringValue = logMessage;
								        				return SUCCESS;
								        			} catch (NoSuchFieldException e) {
								        				e.printStackTrace();
								        				int pos =e.toString().lastIndexOf(":");
									            		String errorField = e.toString().substring(pos);
								        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Body.";
								        				returnAjaxStringValue = logMessage;
								        				return SUCCESS;
								        			}
								        		}
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
								        			if(sid==null){
								        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email Body as module is CustomerFile";
								        				returnAjaxStringValue = logMessage;
								        				return SUCCESS;
							        				}else{
									        			String fieldName = tableFieldnameArr[1];
									        			try{
										        			Class<?> cls = serviceOrder.getClass();
										        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(serviceOrder);
															modelFieldValue =tempFieldValue+"";
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
									        				returnAjaxStringValue = logMessage;
									        				return SUCCESS;
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Body.";
									        				returnAjaxStringValue = logMessage;
									        				return SUCCESS;
									        			}
									        		}
								        		}
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
								        			if(sid==null){
								        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email Body as module is CustomerFile";
								        				returnAjaxStringValue = logMessage;
								        				return SUCCESS;
							        				}else{
									        			String fieldName = tableFieldnameArr[1];
									        			try{
										        			Class<?> cls = trackingStatus.getClass();
										        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(trackingStatus);
															modelFieldValue =tempFieldValue+""; 
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
									        				returnAjaxStringValue = logMessage;
									        				return SUCCESS;
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".\nIncorrect field name in email Body.";
									        				returnAjaxStringValue = logMessage;
									        				return SUCCESS;
									        			}
									        		}
								        		}
								        	}
								        	}else{
								        		emailBodyTempValue.append(tempTableAndFieldname+" ");
								        	}
										}
								}	
								}else if(splittedWord.contains("$^") && splittedWord.contains(".")){
									String[] splittedValueNew = splittedWord.split(Pattern.quote("$^"));
									  for (String splittedWordNew : splittedValueNew){
												String tempTableAndFieldname = splittedWordNew.replaceAll("\\^\\$", "");
									        	if(tempTableAndFieldname.contains(".")){
									        		String [] tableFieldnameArr = tempTableAndFieldname.split("\\.");
									        		if(tableFieldnameArr.length!=0){
									        		String tableName = tableFieldnameArr[0];
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
									        			String fieldName = tableFieldnameArr[1];
									        			try{
									        				if(!fieldName.trim().equalsIgnoreCase("prefix")){
											        			Class<?> cls = customerFile.getClass();
											        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
											        			Object fieldType = modelFieldName.getType();
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(customerFile);
																if(tempFieldValue!=null && !tempFieldValue.equals("")){
																	if(fieldType.toString().equals("class java.util.Date")){
																		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																		Date tempDateValue = (Date) tempFieldValue;
															    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
															    		String dateValue = nowYYYYMMDD1.toString();
																		emailBodyTempValue.append(dateValue+" ");
																	}else{
																		emailBodyTempValue.append(tempFieldValue+" ");
																	}
																}
									        				}else{
									        					String preffixValue ="";
									       					 	if((customerFile.getPrefix()!=null && !customerFile.getPrefix().equalsIgnoreCase("")) && (customerFile.getCustomerLanguagePreference()!=null && !customerFile.getCustomerLanguagePreference().equalsIgnoreCase(""))){
									       					 		preffixValue = customerFileManager.getPreffixFromRefmaster("PREFFIX",customerFile.getCustomerLanguagePreference(), customerFile.getPrefix());
									       							emailBodyTempValue.append(preffixValue+" ");
									       					 	}
									        				}
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
									        				returnAjaxStringValue = logMessage;
									        				return SUCCESS;
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".\nIncorrect field name in email Body.";
									        				returnAjaxStringValue = logMessage;
									        				return SUCCESS;
									        			}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
									        			if(sid==null){
									        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email Body as module is CustomerFile";
									        				returnAjaxStringValue = logMessage;
									        				return SUCCESS;
								        				}else{
										        			String fieldName = tableFieldnameArr[1];
										        			try{
										        				if(!fieldName.trim().equalsIgnoreCase("prefix")){
												        			Class<?> cls = serviceOrder.getClass();
												        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
												        			Object fieldType = modelFieldName.getType();
												        			modelFieldName.setAccessible(true);
																	Object tempFieldValue =modelFieldName.get(serviceOrder);
																	if(tempFieldValue!=null && !tempFieldValue.equals("")){
																		if(fieldType.toString().equals("class java.util.Date")){
																			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																			Date tempDateValue = (Date) tempFieldValue;
																    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
																    		String dateValue = nowYYYYMMDD1.toString();
																			emailBodyTempValue.append(dateValue+" ");
																		}else{
																			emailBodyTempValue.append(tempFieldValue+" ");
																		}
																	}
										        				}else{
										        					String preffixValue ="";
										       					 	if((customerFile.getPrefix()!=null && !customerFile.getPrefix().equalsIgnoreCase("")) && (customerFile.getCustomerLanguagePreference()!=null && !customerFile.getCustomerLanguagePreference().equalsIgnoreCase(""))){
										       					 		preffixValue = customerFileManager.getPreffixFromRefmaster("PREFFIX",customerFile.getCustomerLanguagePreference(), customerFile.getPrefix());
										       							emailBodyTempValue.append(preffixValue+" ");
										       					 	}
										        				}
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
										        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
										        				returnAjaxStringValue = logMessage;
										        				return SUCCESS;
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
										        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Subject.";
										        				returnAjaxStringValue = logMessage;
										        				return SUCCESS;
										        			}
										        		}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
									        			if(sid==null){
									        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use TrackingStatus table in email Body as module is CustomerFile";
									        				returnAjaxStringValue = logMessage;
									        				return SUCCESS;
								        				}else{
										        			String fieldName = tableFieldnameArr[1];
										        			try{
										        				Class<?> cls = trackingStatus.getClass();
										        				Field modelFieldName = cls.getDeclaredField(fieldName.trim());
										        				Object fieldType = modelFieldName.getType();
										        				modelFieldName.setAccessible(true);
										        					Object tempFieldValue =modelFieldName.get(trackingStatus);
										        					if(tempFieldValue!=null && !tempFieldValue.equals("")){
										        						if(fieldType.toString().equals("class java.util.Date")){
										        							SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
										        							Date tempDateValue = (Date) tempFieldValue;
										        						StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
										        						String dateValue = nowYYYYMMDD1.toString();
										        							emailBodyTempValue.append(dateValue+" ");
										        						}else{
										        							emailBodyTempValue.append(tempFieldValue+" ");
										        						}
										        					}
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
										        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
										        				returnAjaxStringValue = logMessage;
										        				return SUCCESS;
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
										        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Body.";
										        				returnAjaxStringValue = logMessage;
										        				return SUCCESS;
										        			}
										        		}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("miscellaneous")){
									        			if(sid==null){
									        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use Miscellaneous table in email Body as module is CustomerFile";
									        				returnAjaxStringValue = logMessage;
									        				return SUCCESS;
								        				}else{
										        			String fieldName = tableFieldnameArr[1];
										        			try{
											        			Class<?> cls = miscellaneous.getClass();
											        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
											        			Object fieldType = modelFieldName.getType();
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(miscellaneous);
																if(tempFieldValue!=null && !tempFieldValue.equals("")){
																	if(fieldType.toString().equals("class java.util.Date")){
																		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																		Date tempDateValue = (Date) tempFieldValue;
															    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
															    		String dateValue = nowYYYYMMDD1.toString();
																		emailBodyTempValue.append(dateValue+" ");
																	}else{
																		emailBodyTempValue.append(tempFieldValue+" ");
																	}
																}
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
										        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
										        				returnAjaxStringValue = logMessage;
										        				return SUCCESS;
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
										        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Body.";
										        				returnAjaxStringValue = logMessage;
										        				return SUCCESS;
										        			}
										        		}
									        		}
									        	}
								        	}else{
								        		emailBodyTempValue.append(tempTableAndFieldname+" ");
								        	}
									  }
									}else{
										emailBodyTempValue.append(splittedWord+"");
									}
								}
							  }else{
								  emailBodyTempValue.append(bodyEmptyTokenizer+" ");
							  }
						  }
					}else{
						if(str.contains("UserImage?location")){
							Random rand = new Random(); 
							int intValue = rand.nextInt(25);
							StringTokenizer bodyTokenizer = new StringTokenizer(str);
							  while (bodyTokenizer.hasMoreTokens()) {
								  bodyEmptyTokenizer = bodyTokenizer.nextToken();
								  if(bodyEmptyTokenizer.contains("UserImage?location")){
									  Pattern pImg = Pattern.compile("\"([^\"]*)\"");
									  Matcher mImg = pImg.matcher(bodyEmptyTokenizer);
									  String imageSource="";
									  String imagePath="";
									  while(mImg.find()){
										  imageSource = (String) mImg.group();
										  imagePath = imageSource.substring(20);
										  imagePath = imagePath.replaceAll("\"", "");
										  //imagePath = imagePath.replace("\\", "/");
										  imageSource = "cid:image"+intValue;
										  char quotes ='"';
										  emailBodyTempValue.append("src="+quotes+imageSource+quotes+" ");
										  if(fileLocation!=null && !fileLocation.equalsIgnoreCase("")){
											  fileLocation = fileLocation+"~"+imagePath+"#"+"image"+intValue;
										  }else{
											  fileLocation = imagePath+"#"+"image"+intValue; 
										  }
									  }
								  }else{
									  emailBodyTempValue.append(bodyEmptyTokenizer+" ");
								  }
							  }
						}else{
							emailBodyTempValue.append(str+" ");
						}
					}
				}
				//System.out.println(emailBodyTempValue);
			} catch(IOException e) {
				logMessage =  "Mail can not be sent because\nException:"+ e.toString();
				returnAjaxStringValue = logMessage;
				return SUCCESS;
			}
			emailBody = emailBodyTempValue.toString();
		}
		
		
		String lastSentEmailBody ="";
		if(idCurrentValue!=null){
			lastSentEmailBody = emailBodyCurrentValue;
		}else{
			lastSentEmailBody = emailSetupTemplate.getEmailBody();
		}
		if(lastSentEmailBody!=null && !lastSentEmailBody.equalsIgnoreCase("")){
			lastSentEmailBody = lastSentEmailBody.replaceAll("<br />", "<br/>");
			StringBuilder lastSentEmailBodyTempValue = new StringBuilder();
			String str;
			BufferedReader reader = new BufferedReader(new StringReader(lastSentEmailBody));
			String bodyEmptyTokenizer="";
			try {       
				while((str = reader.readLine()) != null) {
					if(str.contains("^$")){
						  StringTokenizer bodyTokenizer = new StringTokenizer(str);
						  while (bodyTokenizer.hasMoreTokens()) {
							  bodyEmptyTokenizer = bodyTokenizer.nextToken();
							  if(bodyEmptyTokenizer.contains("^$")){
								String[] splittedValue = bodyEmptyTokenizer.split(Pattern.quote("^$"));
								for (String splittedWord : splittedValue){
								if(splittedWord.contains("$^") && splittedWord.contains(".") && splittedWord.contains("~")){
									String modelFieldValue = "";
									String[] splittedValueByTild = splittedWord.split(Pattern.quote("~"));
									for(String splittedWordByTild : splittedValueByTild){
										if(splittedWordByTild.contains("$^")){
											String[] splittWordArr = splittedWordByTild.split(Pattern.quote("$^"));
											for(String splittWordVal : splittWordArr){
												if(splittWordVal.contains(".")){
													String [] tableFieldNameArr = splittWordVal.split("\\.");
													if(tableFieldNameArr.length!=0){
									        		String tableName = tableFieldNameArr[0];
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("user") && !modelFieldValue.equalsIgnoreCase("")){
									        			String tempFieldName = tableFieldNameArr[1];
									        			try{
										        			userNew = userManager.getUserByUsername(modelFieldValue);
										        			Class<?> cls = userNew.getClass();
										        			Field modelFieldName = cls.getDeclaredField(tempFieldName.trim());
										        			Object fieldType = modelFieldName.getType();
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(userNew);
															if(tempFieldValue!=null && !tempFieldValue.equals("")){
																if(fieldType.toString().equals("class java.util.Date")){
																	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																	Date tempDateValue = (Date) tempFieldValue;
														    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
														    		String dateValue = nowYYYYMMDD1.toString();
														    		lastSentEmailBodyTempValue.append(dateValue+" ");
																}else{
																	lastSentEmailBodyTempValue.append(tempFieldValue+" ");
																}
															}
										        		}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        			}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("partnerPublic") && !modelFieldValue.equalsIgnoreCase("")){
									        			String tempFieldName = tableFieldNameArr[1];
									        			try{
										        			partnerPublicObj = partnerPublicManager.getPartnerByCode(modelFieldValue);
										        			Class<?> cls = partnerPublicObj.getClass();
										        			Field modelFieldName = cls.getDeclaredField(tempFieldName.trim());
										        			Object fieldType = modelFieldName.getType();
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(partnerPublicObj);
															if(tempFieldValue!=null && !tempFieldValue.equals("")){
																if(fieldType.toString().equals("class java.util.Date")){
																	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																	Date tempDateValue = (Date) tempFieldValue;
														    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
														    		String dateValue = nowYYYYMMDD1.toString();
														    		lastSentEmailBodyTempValue.append(dateValue+" ");
																}else{
																	lastSentEmailBodyTempValue.append(tempFieldValue+" ");
																}
															}
										        		}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        			}
									        		}
												}
												}else{
													lastSentEmailBodyTempValue.append(splittWordVal+" ");
												}
											}
										}else{
											String tempTableAndFieldname = splittedWordByTild.replaceAll("\\^\\$", "");
								        	if(tempTableAndFieldname.contains(".")){
								        		String [] tableFieldnameArr = tempTableAndFieldname.split("\\.");
								        		if(tableFieldnameArr.length!=0){
								        		String tableName = tableFieldnameArr[0];
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
								        			String fieldName = tableFieldnameArr[1];
								        			try{
									        			Class<?> cls = customerFile.getClass();
									        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
									        			modelFieldName.setAccessible(true);
														Object tempFieldValue =modelFieldName.get(customerFile);
														modelFieldValue =tempFieldValue+""; 
								        			}catch(IllegalAccessException e){
								        				e.printStackTrace();
								        			} catch (NoSuchFieldException e) {
								        				e.printStackTrace();
								        			}
								        		}
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
								        			if(sid==null){
							        				}else{
									        			String fieldName = tableFieldnameArr[1];
									        			try{
										        			Class<?> cls = serviceOrder.getClass();
										        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(serviceOrder);
															modelFieldValue =tempFieldValue+"";
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        			}
									        		}
								        		}
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
								        			if(sid==null){
							        				}else{
									        			String fieldName = tableFieldnameArr[1];
									        			try{
										        			Class<?> cls = trackingStatus.getClass();
										        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(trackingStatus);
															modelFieldValue =tempFieldValue+""; 
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        			}
									        		}
								        		}
								        	}
								        	}else{
								        		lastSentEmailBodyTempValue.append(tempTableAndFieldname+" ");
								        	}
										}
								}	
								}else if(splittedWord.contains("$^") && splittedWord.contains(".")){
									String[] splittedValueNew = splittedWord.split(Pattern.quote("$^"));
									  for (String splittedWordNew : splittedValueNew){
												String tempTableAndFieldname = splittedWordNew.replaceAll("\\^\\$", "");
									        	if(tempTableAndFieldname.contains(".")){
									        		String [] tableFieldnameArr = tempTableAndFieldname.split("\\.");
									        		if(tableFieldnameArr.length!=0){
									        		String tableName = tableFieldnameArr[0];
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
									        			String fieldName = tableFieldnameArr[1];
									        			try{
									        				if(!fieldName.trim().equalsIgnoreCase("prefix")){
											        			Class<?> cls = customerFile.getClass();
											        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
											        			Object fieldType = modelFieldName.getType();
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(customerFile);
																if(tempFieldValue!=null && !tempFieldValue.equals("")){
																	if(fieldType.toString().equals("class java.util.Date")){
																		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																		Date tempDateValue = (Date) tempFieldValue;
															    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
															    		String dateValue = nowYYYYMMDD1.toString();
															    		lastSentEmailBodyTempValue.append(dateValue+" ");
																	}else{
																		lastSentEmailBodyTempValue.append(tempFieldValue+" ");
																	}
																}
									        				}else{
									        					String preffixValue ="";
									       					 	if((customerFile.getPrefix()!=null && !customerFile.getPrefix().equalsIgnoreCase("")) && (customerFile.getCustomerLanguagePreference()!=null && !customerFile.getCustomerLanguagePreference().equalsIgnoreCase(""))){
									       					 		preffixValue = customerFileManager.getPreffixFromRefmaster("PREFFIX",customerFile.getCustomerLanguagePreference(), customerFile.getPrefix());
									       					 		lastSentEmailBodyTempValue.append(preffixValue+" ");
									       					 	}
									        				}
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        			}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
									        			if(sid==null){
								        				}else{
										        			String fieldName = tableFieldnameArr[1];
										        			try{
										        				if(!fieldName.trim().equalsIgnoreCase("prefix")){
												        			Class<?> cls = serviceOrder.getClass();
												        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
												        			Object fieldType = modelFieldName.getType();
												        			modelFieldName.setAccessible(true);
																	Object tempFieldValue =modelFieldName.get(serviceOrder);
																	if(tempFieldValue!=null && !tempFieldValue.equals("")){
																		if(fieldType.toString().equals("class java.util.Date")){
																			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																			Date tempDateValue = (Date) tempFieldValue;
																    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
																    		String dateValue = nowYYYYMMDD1.toString();
																    		lastSentEmailBodyTempValue.append(dateValue+" ");
																		}else{
																			lastSentEmailBodyTempValue.append(tempFieldValue+" ");
																		}
																	}
										        				}else{
										        					String preffixValue ="";
										       					 	if((customerFile.getPrefix()!=null && !customerFile.getPrefix().equalsIgnoreCase("")) && (customerFile.getCustomerLanguagePreference()!=null && !customerFile.getCustomerLanguagePreference().equalsIgnoreCase(""))){
										       					 		preffixValue = customerFileManager.getPreffixFromRefmaster("PREFFIX",customerFile.getCustomerLanguagePreference(), customerFile.getPrefix());
										       					 	lastSentEmailBodyTempValue.append(preffixValue+" ");
										       					 	}
										        				}
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        			}
										        		}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
									        			if(sid==null){
								        				}else{
										        			String fieldName = tableFieldnameArr[1];
										        			try{
											        			Class<?> cls = trackingStatus.getClass();
											        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
											        			Object fieldType = modelFieldName.getType();
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(trackingStatus);
																if(tempFieldValue!=null && !tempFieldValue.equals("")){
																	if(fieldType.toString().equals("class java.util.Date")){
																		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																		Date tempDateValue = (Date) tempFieldValue;
															    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
															    		String dateValue = nowYYYYMMDD1.toString();
															    		lastSentEmailBodyTempValue.append(dateValue+" ");
																	}else{
																		lastSentEmailBodyTempValue.append(tempFieldValue+" ");
																	}
																}
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        			}
										        		}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("miscellaneous")){
									        			if(sid==null){
								        				}else{
										        			String fieldName = tableFieldnameArr[1];
										        			try{
											        			Class<?> cls = miscellaneous.getClass();
											        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
											        			Object fieldType = modelFieldName.getType();
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(miscellaneous);
																if(tempFieldValue!=null && !tempFieldValue.equals("")){
																	if(fieldType.toString().equals("class java.util.Date")){
																		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																		Date tempDateValue = (Date) tempFieldValue;
															    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
															    		String dateValue = nowYYYYMMDD1.toString();
															    		lastSentEmailBodyTempValue.append(dateValue+" ");
																	}else{
																		lastSentEmailBodyTempValue.append(tempFieldValue+" ");
																	}
																}
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        			}
										        		}
									        		}
									        	}
								        	}else{
								        		lastSentEmailBodyTempValue.append(tempTableAndFieldname+" ");
								        	}
									  }
									}else{
										lastSentEmailBodyTempValue.append(splittedWord+" ");
									}
								}
							  }else{
								  lastSentEmailBodyTempValue.append(bodyEmptyTokenizer+" ");
							  }
						  }
					}else{
							lastSentEmailBodyTempValue.append(str+" ");
					}
				}
			} catch(IOException e) {
				e.printStackTrace();
			}
			lastSentEmailBody = lastSentEmailBodyTempValue.toString();
		}
		
		
		String filesFromFileCabinet = "";
		if(cid!=null && emailSetupTemplate.getFileTypeOfFileCabinet()!=null && !emailSetupTemplate.getFileTypeOfFileCabinet().equalsIgnoreCase("")){
			filesFromFileCabinet = emailSetupTemplateManager.findDocsFromFileCabinet(emailSetupTemplate.getFileTypeOfFileCabinet(),customerFile.getSequenceNumber(),sessionCorpID);
		}else if(sid!=null && emailSetupTemplate.getFileTypeOfFileCabinet()!=null && !emailSetupTemplate.getFileTypeOfFileCabinet().equalsIgnoreCase("")){
			filesFromFileCabinet = emailSetupTemplateManager.findDocsFromFileCabinet(emailSetupTemplate.getFileTypeOfFileCabinet(),serviceOrder.getShipNumber(),sessionCorpID);
		}
		String tempFilesFromFileCabinet = "";
		if(filesFromFileCabinet!=null && !filesFromFileCabinet.equalsIgnoreCase("")){
			for(String str1:filesFromFileCabinet.split("~")){
				if(str1!=null && !str1.equalsIgnoreCase("")){
					File f = new File(str1);
					if(f.exists()){
						if(tempFilesFromFileCabinet!=null && !tempFilesFromFileCabinet.equalsIgnoreCase("")){
							tempFilesFromFileCabinet = tempFilesFromFileCabinet+"~"+str1;
		        		}else{
		        			tempFilesFromFileCabinet = str1;
		        		}
						filesFromFileCabinet = tempFilesFromFileCabinet;
					}else if(!f.exists()){
						String fileName = f.getName();
						String doubleQuote = "-";
						logMessage =  "ErrorIN : File cabinet File "+doubleQuote+" "+fileName+" "+doubleQuote+" not found.<BR>Use Edit to manually send this e-mail.";
        				returnAjaxStringValue = logMessage;
        				return SUCCESS;
					}
				}
			}
		}else{
			if(emailSetupTemplate.getFileTypeOfFileCabinet()!=null && !emailSetupTemplate.getFileTypeOfFileCabinet().equalsIgnoreCase("")){
				String doubleQuote = "-";
				logMessage =  "ErrorIN : No File found for Document Type "+doubleQuote+" "+emailSetupTemplate.getFileTypeOfFileCabinet()+".<BR>Use Edit to manually send this e-mail.";
				returnAjaxStringValue = logMessage;
				return SUCCESS;
			}
		}
		if((fileLocation!=null && !fileLocation.equalsIgnoreCase("")) && (filesFromFileCabinet!=null && !filesFromFileCabinet.equalsIgnoreCase(""))){
			fileLocation = fileLocation+"~"+filesFromFileCabinet;
		}else if(filesFromFileCabinet!=null && !filesFromFileCabinet.equalsIgnoreCase("")){
			fileLocation = filesFromFileCabinet;
		}
		
		if(emailSetupTemplate.getFormsId()!=null && !emailSetupTemplate.getFormsId().equalsIgnoreCase("")){
			String uploadFilePath="";
			Map parameters = new HashMap();
			if(sid!=null){
				parameters.put("Service Order Number", serviceOrder.getShipNumber());
			}
			if(cid!=null){
				parameters.put("Customer File Number", customerFile.getSequenceNumber());
			}
			parameters.put("Corporate ID", sessionCorpID);
			String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + "EmailSetupTemplate" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
	
			byte[] output;
			if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
				Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			} else {
				Locale locale = new Locale("en", "US");
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
			List<String> files = new ArrayList<String>();
			String filePath="";
			JasperPrint jasperPrint = new JasperPrint();
			HttpServletResponse response = getResponse();
			ServletOutputStream ouputStream = response.getOutputStream();
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			String[] reportId = emailSetupTemplate.getFormsId().split(",");
			
			for (int i=0;i<reportId.length;i++) {
				try{
					Random rand = new Random(); 
					//int intValue = rand.nextInt(1000);
					jasperPrint = reportsManager.generateReport(Long.parseLong(reportId[i]), parameters);
					reports = reportsManager.get(Long.parseLong(reportId[i]));
					String temp = reports.getReportName();
					String fileName = temp.substring(0, temp.indexOf("."));
					if(reports.getEnabled().equalsIgnoreCase("Yes")){
						if((reports.getPdf()!=null && !reports.getPdf().equals("")) && reports.getPdf().equalsIgnoreCase("true")){
							response.setContentType("application/pdf");
							response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
							output = JasperExportManager.exportReportToPdf(jasperPrint);
							
							File dirPath = new File(uploadDir);
							if (!dirPath.exists()) {
								dirPath.mkdirs();
							}			
							String tempFileFileName = fileName;
							String fileFileName = tempFileFileName+".pdf";
							files.add(uploadDir + fileFileName);
							File newFile = new File(uploadDir + fileFileName);
							filePath =(uploadDir+fileFileName );
							if (newFile.exists()) {
								newFile.delete();
							}
							if(fileLocation!=null && !fileLocation.equalsIgnoreCase("")){
								fileLocation=filePath+"~"+fileLocation;
					        }else{
					        	fileLocation=filePath;
					        }
							
							FileOutputStream fos = new FileOutputStream(newFile);
							DataOutputStream bos =  new DataOutputStream(fos);
							bos.write(output);
							bos.flush();
							bos.close();
						}
						if((reports.getDocx()!=null && !reports.getDocx().equals("")) && reports.getDocx().equalsIgnoreCase("true")){
							Random rand1 = new Random(); 
							//int intValue1 = rand1.nextInt(1000);
							response.setContentType("application/docx");
						    response.setHeader("Content-Disposition", "attachment; filename="+ fileName +".docx");
							
						    JRDocxExporter exporterDocx = new JRDocxExporter();
							exporterDocx.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
							exporterDocx.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
							exporterDocx.setParameter(JRDocxExporterParameter.FLEXIBLE_ROW_HEIGHT, Boolean.TRUE);
							exporterDocx.exportReport();
							output = byteArrayOutputStream.toByteArray();
							
							File dirPath = new File(uploadDir);
							if (!dirPath.exists()) {
								dirPath.mkdirs();
							}			
							String tempFileFileName = fileName;
							String fileFileName = tempFileFileName+".docx";
							files.add(uploadDir + fileFileName);
							File newFile = new File(uploadDir + fileFileName);
							filePath =(uploadDir+fileFileName );
							if (newFile.exists()) {
								newFile.delete();
							}
							if(fileLocation!=null && !fileLocation.equalsIgnoreCase("")){
								fileLocation=filePath+"~"+fileLocation;
					        }else{
					        	fileLocation=filePath;
					        }
							
							FileOutputStream fos = new FileOutputStream(newFile);
							fos.write(output);
							fos.flush();
							fos.close();
						}
						if((reports.getPdf()==null || reports.getPdf().equals("") || reports.getPdf().equalsIgnoreCase("false")) && (reports.getDocx()==null || reports.getDocx().equals("") || reports.getDocx().equalsIgnoreCase("false"))){
							String doubleQuote = "-";
							logMessage =  "ErrorIN : "+temp+" not found.<BR>Use Edit to manually send this e-mail.";
							returnAjaxStringValue = logMessage;
							return SUCCESS;
						}
					}else{
						String doubleQuote = "-";
						logMessage =  "ErrorIN : "+temp+" not found.<BR>Use Edit to manually send this e-mail.";
						returnAjaxStringValue = logMessage;
						return SUCCESS;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		String emailSignatureValue = "";
		if(emailSetupTemplate.getAddSignatureFromAppUser()!=null && emailSetupTemplate.getAddSignatureFromAppUser()){
			List userSignatureValue = customerFileManager.findUserSignature(emailFrom);
			if(userSignatureValue!=null && !userSignatureValue.isEmpty()){
				String signatureValue = userSignatureValue.get(0).toString();
				emailSignatureValue = signatureValue;
			}
		}
		if(cid!=null){
			emailSetupManager.insertMailFromEmailSetupTemplate(emailFrom, emailTo, emailCc, emailBcc, fileLocation, emailBody, subject, sessionCorpID,"CustomerFile",customerFile.getSequenceNumber(),emailSignatureValue,emailSetupTemplate.getSaveAs(),lastSentEmailBody);
		}
		if(sid!=null){
			emailSetupManager.insertMailFromEmailSetupTemplate(emailFrom, emailTo, emailCc, emailBcc, fileLocation, emailBody, subject, sessionCorpID,"ServiceOrder",serviceOrder.getShipNumber(),emailSignatureValue,emailSetupTemplate.getSaveAs(),lastSentEmailBody);
		}
		returnAjaxStringValue = logMessage;
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" SendEmailFromEmailSetupTemplate() End");
		return SUCCESS;
	}
	private String emailSetupTemplateEditURL;
	private String emailBccCurrentValue;
	private String emailFromCurrentValue;
	@SkipValidation
	public String sendEmailFromEmailTemplateEdit() throws AddressException, MessagingException, Exception{
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" sendEmailFromEmailTemplateEdit() Start");
		List lastSentMailList = new ArrayList();
		if(idCurrentValue!=null){
			emailSetupTemplate = emailSetupTemplateManager.get(idCurrentValue);
		}
		if(cid!=null){
			customerFile = customerFileManager.get(cid);
			String fileNumber = customerFile.getSequenceNumber();
			String saveAsVal = emailSetupTemplate.getSaveAs();
			lastSentMailList = emailSetupTemplateManager.findLastSentMail(sessionCorpID,"CustomerFile",fileNumber,saveAsVal);
		}
		if(sid!=null){
			serviceOrder = serviceOrderManager.get(sid);
			trackingStatus = trackingStatusManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid);
			customerFile = customerFileManager.get(serviceOrder.getCustomerFileId());
			String fileNumber = serviceOrder.getShipNumber();
			String saveAsVal = emailSetupTemplate.getSaveAs();
			lastSentMailList = emailSetupTemplateManager.findLastSentMail(sessionCorpID,"ServiceOrder",fileNumber,saveAsVal);
		}
		String lastSentMailAttachLocation="";
		if(lastSentMailList!=null && !lastSentMailList.isEmpty() && lastSentMailList.get(0)!=null){
			EmailSetup emailSetupObj = (EmailSetup)lastSentMailList.get(0);
			lastSentMailAttachLocation = emailSetupObj.getAttchedFileLocation();
		}
		
		String logMessage ="Mail has been scheduled succesfully.";
		String tempEmailFromVal="";
		String emailFrom = "";
		
		if(idCurrentValue!=null){
			emailFrom = emailFromCurrentValue;
		}else{
			emailFrom = emailSetupTemplate.getEmailFrom();
		}
		if(emailFrom!=null && !emailFrom.equalsIgnoreCase("")){
			String[] splittedEmailFromValue = emailFrom.split(",");
			for (String splittedEmailFrom : splittedEmailFromValue){
				if(splittedEmailFrom.contains("^$")){
					splittedEmailFrom = splittedEmailFrom.replaceAll("\\^\\$", "").replaceAll("\\$\\^", "");
		        	if(splittedEmailFrom.contains(".")){
		        		String [] tableFieldnameArr = splittedEmailFrom.split("\\.");
		        		String tableName = tableFieldnameArr[0];
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
		        			String fieldName = tableFieldnameArr[1];
		        			try{
			        			Class<?> cls = customerFile.getClass();
			        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
			        			modelFieldName.setAccessible(true);
								Object tempFieldValue =modelFieldName.get(customerFile);
								if(tempFieldValue!=null && !tempFieldValue.equals("")){
									if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("coordinator") && !fieldName.trim().equalsIgnoreCase("personPricing") && !fieldName.trim().equalsIgnoreCase("personBilling") && !fieldName.trim().equalsIgnoreCase("personPayable") && !fieldName.trim().equalsIgnoreCase("auditor")&& !fieldName.trim().equalsIgnoreCase("approvedBy")){
										if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
											tempEmailFromVal = tempFieldValue+"";
										}else{
											tempEmailFromVal = tempEmailFromVal+","+tempFieldValue+"";
										}
									}else{
										userNew = userManager.getUserByUsername(tempFieldValue+"");
					        			Class<?> cls1 = userNew.getClass();
					        			Field usrModelFieldName = cls1.getDeclaredField("email");
					        			usrModelFieldName.setAccessible(true);
										Object usrFieldValue =usrModelFieldName.get(userNew);
										if(usrFieldValue!=null && !usrFieldValue.equals("")){
											if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
												tempEmailFromVal = usrFieldValue+"";
											}else{
												tempEmailFromVal = tempEmailFromVal+","+usrFieldValue+"";
											}
										}
									}
								}
		        			}catch(IllegalAccessException e){
		        				e.printStackTrace();
		        				int pos =e.toString().lastIndexOf(":");
			            		String errorField = e.toString().substring(pos);
		        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
		        				errorMessage(getText(logMessage));
		        				return SUCCESS;
		        			} catch (NoSuchFieldException e) {
		        				int pos =e.toString().lastIndexOf(":");
			            		String errorField = e.toString().substring(pos);
		        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email From.";
		        				errorMessage(getText(logMessage));
		        				return SUCCESS;
		        			}
		        		}
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
		        			if(sid==null){
		        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email From as module is CustomerFile";
		        				errorMessage(getText(logMessage));
		        				return SUCCESS;
	        				}else{
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = serviceOrder.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(serviceOrder);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("salesMan") && !fieldName.trim().equalsIgnoreCase("coordinator")){
											if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
												tempEmailFromVal = tempFieldValue+"";
											}else{
												tempEmailFromVal = tempEmailFromVal+","+tempFieldValue+"";
											}
										}else{
											userNew = userManager.getUserByUsername(tempFieldValue+"");
						        			Class<?> cls1 = userNew.getClass();
						        			Field usrModelFieldName = cls1.getDeclaredField("email");
						        			usrModelFieldName.setAccessible(true);
											Object usrFieldValue =usrModelFieldName.get(userNew);
											if(usrFieldValue!=null && !usrFieldValue.equals("")){
												if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
													tempEmailFromVal = usrFieldValue+"";
												}else{
													tempEmailFromVal = tempEmailFromVal+","+usrFieldValue+"";
												}
											}
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
			        				errorMessage(getText(logMessage));
			        				return SUCCESS;
			        			} catch (NoSuchFieldException e) {
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email From.";
			        				errorMessage(getText(logMessage));
			        				return SUCCESS;
			        			}
			        		}
		        		}
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
		        			if(sid==null){
		        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use TrackingStatus table in email From as module is CustomerFile";
		        				errorMessage(getText(logMessage));
		        				return SUCCESS;
	        				}else{
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = trackingStatus.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			Object fieldType = modelFieldName.getType();
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(trackingStatus);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
											tempEmailFromVal = tempFieldValue+"";
										}else{
											tempEmailFromVal = tempEmailFromVal+","+tempFieldValue+"";
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
			        				errorMessage(getText(logMessage));
			        				return SUCCESS;
			        			} catch (NoSuchFieldException e) {
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email From.";
			        				errorMessage(getText(logMessage));
			        				return SUCCESS;
			        			}
			        		}
		        		}
		        	}
				}else{
					if(splittedEmailFrom!=null && !splittedEmailFrom.equalsIgnoreCase("")){
						if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
							tempEmailFromVal  = splittedEmailFrom;
						}else{
							tempEmailFromVal  = tempEmailFromVal+","+splittedEmailFrom;
						}
					}
				}
			}
			emailFrom = tempEmailFromVal;
		}
		String emailTo = "";
		if(idCurrentValue!=null){
			emailTo = emailToCurrentValue;
		}else{
			emailTo = emailSetupTemplate.getEmailTo();
		}
		String tempEmailToVal = "";
		if(emailTo!=null && !emailTo.equalsIgnoreCase("")){
			String[] splittedEmailToValue = emailTo.split(",");
				for (String splittedEmailTo : splittedEmailToValue){
					if(splittedEmailTo.contains("^$")){
						splittedEmailTo = splittedEmailTo.replaceAll("\\^\\$", "").replaceAll("\\$\\^", "");
			        	if(splittedEmailTo.contains(".")){
			        		String [] tableFieldnameArr = splittedEmailTo.split("\\.");
			        		String tableName = tableFieldnameArr[0];
			        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = customerFile.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(customerFile);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("coordinator") && !fieldName.trim().equalsIgnoreCase("personPricing") && !fieldName.trim().equalsIgnoreCase("personBilling") && !fieldName.trim().equalsIgnoreCase("personPayable") && !fieldName.trim().equalsIgnoreCase("auditor")&& !fieldName.trim().equalsIgnoreCase("approvedBy")){
											if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
												tempEmailToVal = tempFieldValue+"";
											}else{
												tempEmailToVal = tempEmailToVal+","+tempFieldValue+"";
											}
										}else{
											userNew = userManager.getUserByUsername(tempFieldValue+"");
						        			Class<?> cls1 = userNew.getClass();
						        			Field usrModelFieldName = cls1.getDeclaredField("email");
						        			usrModelFieldName.setAccessible(true);
											Object usrFieldValue =usrModelFieldName.get(userNew);
											if(usrFieldValue!=null && !usrFieldValue.equals("")){
												if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
													tempEmailToVal = usrFieldValue+"";
												}else{
													tempEmailToVal = tempEmailToVal+","+usrFieldValue+"";
												}
											}
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
			        				errorMessage(getText(logMessage));
			        				return SUCCESS;
			        			} catch (NoSuchFieldException e) {
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email To.";
			        				errorMessage(getText(logMessage));
			        				return SUCCESS;
			        			}
			        		}
			        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
			        			if(sid==null){
			        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email To as module is CustomerFile";
			        				errorMessage(getText(logMessage));
			        				return SUCCESS;
		        				}else{
				        			String fieldName = tableFieldnameArr[1];
				        			try{
					        			Class<?> cls = serviceOrder.getClass();
					        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
					        			modelFieldName.setAccessible(true);
										Object tempFieldValue =modelFieldName.get(serviceOrder);
										if(tempFieldValue!=null && !tempFieldValue.equals("")){
											if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("salesMan") && !fieldName.trim().equalsIgnoreCase("coordinator")){
												if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
													tempEmailToVal = tempFieldValue+"";
												}else{
													tempEmailToVal = tempEmailToVal+","+tempFieldValue+"";
												}
											}else{
												userNew = userManager.getUserByUsername(tempFieldValue+"");
							        			Class<?> cls1 = userNew.getClass();
							        			Field usrModelFieldName = cls1.getDeclaredField("email");
							        			usrModelFieldName.setAccessible(true);
												Object usrFieldValue =usrModelFieldName.get(userNew);
												if(usrFieldValue!=null && !usrFieldValue.equals("")){
													if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
														tempEmailToVal = usrFieldValue+"";
													}else{
														tempEmailToVal = tempEmailToVal+","+usrFieldValue+"";
													}
												}
											}
										}
				        			}catch(IllegalAccessException e){
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
				        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
				        				errorMessage(getText(logMessage));
				        				return SUCCESS;
				        			} catch (NoSuchFieldException e) {
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
				        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email To.";
				        				errorMessage(getText(logMessage));
				        				return SUCCESS;
				        			}
				        		}
			        		}
			        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
			        			if(sid==null){
			        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use TrackingStatus table in email To as module is CustomerFile";
			        				errorMessage(getText(logMessage));
			        				return SUCCESS;
		        				}else{
				        			String fieldName = tableFieldnameArr[1];
				        			try{
					        			Class<?> cls = trackingStatus.getClass();
					        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
					        			modelFieldName.setAccessible(true);
										Object tempFieldValue =modelFieldName.get(trackingStatus);
										if(tempFieldValue!=null && !tempFieldValue.equals("")){
											if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
												tempEmailToVal = tempFieldValue+"";
											}else{
												tempEmailToVal = tempEmailToVal+","+tempFieldValue+"";
											}
										}
				        			}catch(IllegalAccessException e){
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
				        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
				        				errorMessage(getText(logMessage));
				        				return SUCCESS;
				        			} catch (NoSuchFieldException e) {
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
				        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email To.";
				        				errorMessage(getText(logMessage));
				        				return SUCCESS;
				        			}
				        		}
				        	}
			        	}
					}else{
						if(splittedEmailTo!=null && !splittedEmailTo.equalsIgnoreCase("")){
							if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
								tempEmailToVal  = splittedEmailTo;
							}else{
								tempEmailToVal  = tempEmailToVal+","+splittedEmailTo;
							}
						}
					}
				}
			emailTo = tempEmailToVal;
		}
		
		String tempEmailCcVal="";
		String emailCc = "";
		if(idCurrentValue!=null){
			emailCc = emailCcCurrentValue;
		}else{
			emailCc = emailSetupTemplate.getEmailCc();
		}
		if(emailCc!=null && !emailCc.equalsIgnoreCase("")){
			String[] splittedEmailCcValue = emailCc.split(",");
			for (String splittedEmailCc : splittedEmailCcValue){
				if(splittedEmailCc.contains("^$")){
					splittedEmailCc = splittedEmailCc.replaceAll("\\^\\$", "").replaceAll("\\$\\^", "");
		        	if(splittedEmailCc.contains(".")){
		        		String [] tableFieldnameArr = splittedEmailCc.split("\\.");
		        		String tableName = tableFieldnameArr[0];
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
		        			String fieldName = tableFieldnameArr[1];
		        			try{
			        			Class<?> cls = customerFile.getClass();
			        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
			        			modelFieldName.setAccessible(true);
								Object tempFieldValue =modelFieldName.get(customerFile);
								if(tempFieldValue!=null && !tempFieldValue.equals("")){
									if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("coordinator") && !fieldName.trim().equalsIgnoreCase("personPricing") && !fieldName.trim().equalsIgnoreCase("personBilling") && !fieldName.trim().equalsIgnoreCase("personPayable") && !fieldName.trim().equalsIgnoreCase("auditor")&& !fieldName.trim().equalsIgnoreCase("approvedBy")){
										if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
											tempEmailCcVal = tempFieldValue+"";
										}else{
											tempEmailCcVal = tempEmailCcVal+","+tempFieldValue+"";
										}
									}else{
										userNew = userManager.getUserByUsername(tempFieldValue+"");
					        			Class<?> cls1 = userNew.getClass();
					        			Field usrModelFieldName = cls1.getDeclaredField("email");
					        			usrModelFieldName.setAccessible(true);
										Object usrFieldValue =usrModelFieldName.get(userNew);
										if(usrFieldValue!=null && !usrFieldValue.equals("")){
											if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
												tempEmailCcVal = usrFieldValue+"";
											}else{
												tempEmailCcVal = tempEmailCcVal+","+usrFieldValue+"";
											}
										}
									}
								}
		        			}catch(IllegalAccessException e){
		        				e.printStackTrace();
		        				int pos =e.toString().lastIndexOf(":");
			            		String errorField = e.toString().substring(pos);
		        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
		        				errorMessage(getText(logMessage));
		        				return SUCCESS;
		        			} catch (NoSuchFieldException e) {
		        				e.printStackTrace();
		        				int pos =e.toString().lastIndexOf(":");
			            		String errorField = e.toString().substring(pos);
		        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Cc.";
		        				errorMessage(getText(logMessage));
		        				return SUCCESS;
		        			}
		        		}
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
		        			if(sid==null){
		        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email Cc as module is CustomerFile";
		        				errorMessage(getText(logMessage));
		        				return SUCCESS;
	        				}else{
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = serviceOrder.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(serviceOrder);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("salesMan") && !fieldName.trim().equalsIgnoreCase("coordinator")){
											if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
												tempEmailCcVal = tempFieldValue+"";
											}else{
												tempEmailCcVal = tempEmailCcVal+","+tempFieldValue+"";
											}
										}else{
											userNew = userManager.getUserByUsername(tempFieldValue+"");
						        			Class<?> cls1 = userNew.getClass();
						        			Field usrModelFieldName = cls1.getDeclaredField("email");
						        			usrModelFieldName.setAccessible(true);
											Object usrFieldValue =usrModelFieldName.get(userNew);
											if(usrFieldValue!=null && !usrFieldValue.equals("")){
												if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
													tempEmailCcVal = usrFieldValue+"";
												}else{
													tempEmailCcVal = tempEmailCcVal+","+usrFieldValue+"";
												}
											}
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
			        				errorMessage(getText(logMessage));
			        				return SUCCESS;
			        			} catch (NoSuchFieldException e) {
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Cc.";
			        				errorMessage(getText(logMessage));
			        				return SUCCESS;
			        			}
			        		}
			        	}
		        	}
				}else{
					if(splittedEmailCc!=null && !splittedEmailCc.equalsIgnoreCase("")){
						if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
							tempEmailCcVal  = splittedEmailCc;
						}else{
							tempEmailCcVal  = tempEmailCcVal+","+splittedEmailCc;
						}
					}
				}
			}
			emailCc = tempEmailCcVal;
		}
		String tempEmailBccVal ="";
		String emailBcc = "";
		if(idCurrentValue!=null){
			emailBcc = emailBccCurrentValue;
		}else{
			emailBcc = emailSetupTemplate.getEmailBcc();
		}
		if(emailBcc!=null && !emailBcc.equalsIgnoreCase("")){
			String[] splittedEmailBccValue = emailBcc.split(",");
			for (String splittedEmailBcc : splittedEmailBccValue){
				if(splittedEmailBcc.contains("^$")){
					splittedEmailBcc = splittedEmailBcc.replaceAll("\\^\\$", "").replaceAll("\\$\\^", "");
		        	if(splittedEmailBcc.contains(".")){
		        		String [] tableFieldnameArr = splittedEmailBcc.split("\\.");
		        		String tableName = tableFieldnameArr[0];
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
		        			String fieldName = tableFieldnameArr[1];
		        			try{
			        			Class<?> cls = customerFile.getClass();
			        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
			        			modelFieldName.setAccessible(true);
								Object tempFieldValue =modelFieldName.get(customerFile);
								if(tempFieldValue!=null && !tempFieldValue.equals("")){
									if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("coordinator") && !fieldName.trim().equalsIgnoreCase("personPricing") && !fieldName.trim().equalsIgnoreCase("personBilling") && !fieldName.trim().equalsIgnoreCase("personPayable") && !fieldName.trim().equalsIgnoreCase("auditor")&& !fieldName.trim().equalsIgnoreCase("approvedBy")){
										if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
											tempEmailBccVal = tempFieldValue+"";
										}else{
											tempEmailBccVal = tempEmailBccVal+","+tempFieldValue+"";
										}
									}else{
										userNew = userManager.getUserByUsername(tempFieldValue+"");
					        			Class<?> cls1 = userNew.getClass();
					        			Field usrModelFieldName = cls1.getDeclaredField("email");
					        			usrModelFieldName.setAccessible(true);
										Object usrFieldValue =usrModelFieldName.get(userNew);
										if(usrFieldValue!=null && !usrFieldValue.equals("")){
											if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
												tempEmailBccVal = usrFieldValue+"";
											}else{
												tempEmailBccVal = tempEmailBccVal+","+usrFieldValue+"";
											}
										}
									}
								}
		        			}catch(IllegalAccessException e){
		        				e.printStackTrace();
		        				int pos =e.toString().lastIndexOf(":");
			            		String errorField = e.toString().substring(pos);
		        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
		        				errorMessage(getText(logMessage));
		        				return SUCCESS;
		        			} catch (NoSuchFieldException e) {
		        				e.printStackTrace();
		        				int pos =e.toString().lastIndexOf(":");
			            		String errorField = e.toString().substring(pos);
		        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Bcc.";
		        				errorMessage(getText(logMessage));
		        				return SUCCESS;
		        			}
		        		}
		        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
		        			if(sid==null){
		        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email Bcc as module is CustomerFile";
		        				errorMessage(getText(logMessage));
		        				return SUCCESS;
	        				}else{
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = serviceOrder.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(serviceOrder);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("salesMan") && !fieldName.trim().equalsIgnoreCase("coordinator")){
											if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
												tempEmailBccVal = tempFieldValue+"";
											}else{
												tempEmailBccVal = tempEmailBccVal+","+tempFieldValue+"";
											}
										}else{
											userNew = userManager.getUserByUsername(tempFieldValue+"");
						        			Class<?> cls1 = userNew.getClass();
						        			Field usrModelFieldName = cls1.getDeclaredField("email");
						        			usrModelFieldName.setAccessible(true);
											Object usrFieldValue =usrModelFieldName.get(userNew);
											if(usrFieldValue!=null && !usrFieldValue.equals("")){
												if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
													tempEmailBccVal = usrFieldValue+"";
												}else{
													tempEmailBccVal = tempEmailBccVal+","+usrFieldValue+"";
												}
											}
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
			        				errorMessage(getText(logMessage));
			        				return SUCCESS;
			        			} catch (NoSuchFieldException e) {
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
			        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Bcc.";
			        				errorMessage(getText(logMessage));
			        				return SUCCESS;
			        			}
			        		}
			        	}
		        	}
				}else{
					if(splittedEmailBcc!=null && !splittedEmailBcc.equalsIgnoreCase("")){
						if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
							tempEmailBccVal  = splittedEmailBcc;
						}else{
							tempEmailBccVal  = tempEmailBccVal+","+splittedEmailBcc;
						}
					}
				}
			}
			emailBcc = tempEmailBccVal;
		}
		
		String subject = "";
		String subjectTemp = "";
		if(idCurrentValue!=null){
			subjectTemp = emailSubjectCurrentValue;
		}else{
			subjectTemp = emailSetupTemplate.getEmailSubject();
		}
		String subjectEmptyTokenizer="";
		StringBuilder emailSubjectValue = new StringBuilder();
		if(subjectTemp.contains("^$")){
			StringTokenizer subjectTokenizer = new StringTokenizer(subjectTemp);
	        while (subjectTokenizer.hasMoreTokens()) {
	        	subjectEmptyTokenizer = subjectTokenizer.nextToken();
	        	if(subjectEmptyTokenizer.contains("^$")){
	        		String[] splittedValue = subjectEmptyTokenizer.split(Pattern.quote("^$"));
					for (String splittedWord : splittedValue){
						if(splittedWord.contains("$^") && splittedWord.contains(".")){
							String[] splittedValueNew = splittedWord.split(Pattern.quote("$^"));
							  for (String splittedWordNew : splittedValueNew){
									String tempTableAndFieldname = splittedWordNew.replaceAll("\\^\\$", "");
						        	if(tempTableAndFieldname.contains(".") && (tempTableAndFieldname.toLowerCase().contains("customerfile") || tempTableAndFieldname.toLowerCase().contains("serviceorder") || tempTableAndFieldname.toLowerCase().contains("trackingstatus"))){
						        		String [] tableFieldnameArr = tempTableAndFieldname.split("\\.");
						        		String tableName = tableFieldnameArr[0];
						        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
						        			String fieldName = tableFieldnameArr[1];
						        			try{
							        			Class<?> cls = customerFile.getClass();
							        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
							        			Object fieldType = modelFieldName.getType();
							        			modelFieldName.setAccessible(true);
												Object tempFieldValue =modelFieldName.get(customerFile);
												if(tempFieldValue!=null && !tempFieldValue.equals("")){
													if(fieldType.toString().equals("class java.util.Date")){
														SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
														Date tempDateValue = (Date) tempFieldValue;
											    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
											    		String dateValue = nowYYYYMMDD1.toString();
											    		emailSubjectValue.append(dateValue+" ");
													}else{
														emailSubjectValue.append(tempFieldValue+" ");
													}
												}
						        			}catch(IllegalAccessException e){
						        				e.printStackTrace();
						        				int pos =e.toString().lastIndexOf(":");
							            		String errorField = e.toString().substring(pos);
						        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
						        				errorMessage(getText(logMessage));
						        				return SUCCESS;
						        			} catch (NoSuchFieldException e) {
						        				e.printStackTrace();
						        				int pos =e.toString().lastIndexOf(":");
							            		String errorField = e.toString().substring(pos);
						        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Subject.";
						        				errorMessage(getText(logMessage));
						        				return SUCCESS;
						        			}
						        		}
						        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
						        			if(sid==null){
						        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email Subject as module is CustomerFile";
						        				errorMessage(getText(logMessage));
						        				return SUCCESS;
					        				}else{
							        			String fieldName = tableFieldnameArr[1];
							        			try{
								        			Class<?> cls = serviceOrder.getClass();
								        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
								        			Object fieldType = modelFieldName.getType();
								        			modelFieldName.setAccessible(true);
													Object tempFieldValue =modelFieldName.get(serviceOrder);
													if(tempFieldValue!=null && !tempFieldValue.equals("")){
														if(fieldType.toString().equals("class java.util.Date")){
															SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
															Date tempDateValue = (Date) tempFieldValue;
												    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
												    		String dateValue = nowYYYYMMDD1.toString();
												    		emailSubjectValue.append(dateValue+" ");
														}else{
															emailSubjectValue.append(tempFieldValue+" ");
														}
													}
							        			}catch(IllegalAccessException e){
							        				e.printStackTrace();
							        				int pos =e.toString().lastIndexOf(":");
								            		String errorField = e.toString().substring(pos);
							        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
							        				errorMessage(getText(logMessage));
							        				return SUCCESS;
							        			} catch (NoSuchFieldException e) {
							        				e.printStackTrace();
							        				int pos =e.toString().lastIndexOf(":");
								            		String errorField = e.toString().substring(pos);
							        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Subject.";
							        				errorMessage(getText(logMessage));
							        				return SUCCESS;
							        			}
							        		}
						        		}
						        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
						        			if(sid==null){
						        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use TrackingStatus table in email Subject as module is CustomerFile";
						        				errorMessage(getText(logMessage));
						        				return SUCCESS;
					        				}else{
							        			String fieldName = tableFieldnameArr[1];
							        			try{
								        			Class<?> cls = trackingStatus.getClass();
								        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
								        			Object fieldType = modelFieldName.getType();
								        			modelFieldName.setAccessible(true);
													Object tempFieldValue =modelFieldName.get(trackingStatus);
													if(tempFieldValue!=null && !tempFieldValue.equals("")){
														if(fieldType.toString().equals("class java.util.Date")){
															SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
															Date tempDateValue = (Date) tempFieldValue;
												    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
												    		String dateValue = nowYYYYMMDD1.toString();
												    		emailSubjectValue.append(dateValue+" ");
														}else{
															emailSubjectValue.append(tempFieldValue+" ");
														}
													}
							        			}catch(IllegalAccessException e){
							        				e.printStackTrace();
							        				int pos =e.toString().lastIndexOf(":");
								            		String errorField = e.toString().substring(pos);
							        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
							        				errorMessage(getText(logMessage));
							        				return SUCCESS;
							        			} catch (NoSuchFieldException e) {
							        				e.printStackTrace();
							        				int pos =e.toString().lastIndexOf(":");
								            		String errorField = e.toString().substring(pos);
							        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Subject.";
							        				errorMessage(getText(logMessage));
							        				return SUCCESS;
							        			}
							        		}
						        		}
						        	}else{
						        		emailSubjectValue.append(tempTableAndFieldname+" ");
						        	}
						  		}
							}else{
								if(splittedWord!=null && !splittedWord.equalsIgnoreCase("")){
									emailSubjectValue.append(splittedWord+" ");
								}
							}
						}
					}else{
			        	emailSubjectValue.append(subjectEmptyTokenizer+" ");
			        }
	        	}
		}else{
			emailSubjectValue.append(subjectTemp+" ");
		}
		subject = emailSubjectValue.toString();
		
		String fileLocation = "";
		
		String emailBody ="";
		if(idCurrentValue!=null){
			emailBody = emailBodyCurrentValue;
		}else{
			emailBody = emailSetupTemplate.getEmailBody();
		}
		if(emailBody!=null && !emailBody.equalsIgnoreCase("")){
			emailBody = emailBody.replaceAll("<br />", "<br/>");
			StringBuilder emailBodyTempValue = new StringBuilder();
			String str;
			BufferedReader reader = new BufferedReader(new StringReader(emailBody));
			String bodyEmptyTokenizer="";
			try {       
				while((str = reader.readLine()) != null) {
					if(str.contains("^$")){
						  StringTokenizer bodyTokenizer = new StringTokenizer(str);
						  while (bodyTokenizer.hasMoreTokens()) {
							  bodyEmptyTokenizer = bodyTokenizer.nextToken();
							  if(bodyEmptyTokenizer.contains("^$")){
								String[] splittedValue = bodyEmptyTokenizer.split(Pattern.quote("^$"));
								for (String splittedWord : splittedValue){
								if(splittedWord.contains("$^") && splittedWord.contains(".") && splittedWord.contains("~")){
									String modelFieldValue = "";
									String[] splittedValueByTild = splittedWord.split(Pattern.quote("~"));
									for(String splittedWordByTild : splittedValueByTild){
										if(splittedWordByTild.contains("$^")){
											String[] splittWordArr = splittedWordByTild.split(Pattern.quote("$^"));
											for(String splittWordVal : splittWordArr){
												if(splittWordVal.contains(".")){
													String [] tableFieldNameArr = splittWordVal.split("\\.");
													if(tableFieldNameArr.length!=0){
									        		String tableName = tableFieldNameArr[0];
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("user") && !modelFieldValue.equalsIgnoreCase("")){
									        			String tempFieldName = tableFieldNameArr[1];
									        			try{
										        			userNew = userManager.getUserByUsername(modelFieldValue);
										        			Class<?> cls = userNew.getClass();
										        			Field modelFieldName = cls.getDeclaredField(tempFieldName.trim());
										        			Object fieldType = modelFieldName.getType();
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(userNew);
															if(tempFieldValue!=null && !tempFieldValue.equals("")){
																if(fieldType.toString().equals("class java.util.Date")){
																	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																	Date tempDateValue = (Date) tempFieldValue;
														    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
														    		String dateValue = nowYYYYMMDD1.toString();
																	emailBodyTempValue.append(dateValue+" ");
																}else{
																	emailBodyTempValue.append(tempFieldValue+" ");
																}
															}
										        		}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
									        				errorMessage(getText(logMessage));
									        				return SUCCESS;
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Body.";
									        				errorMessage(getText(logMessage));
									        				return SUCCESS;
									        			}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("partnerPublic") && !modelFieldValue.equalsIgnoreCase("")){
									        			String tempFieldName = tableFieldNameArr[1];
									        			try{
										        			partnerPublicObj = partnerPublicManager.getPartnerByCode(modelFieldValue);
										        			Class<?> cls = partnerPublicObj.getClass();
										        			Field modelFieldName = cls.getDeclaredField(tempFieldName.trim());
										        			Object fieldType = modelFieldName.getType();
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(partnerPublicObj);
															if(tempFieldValue!=null && !tempFieldValue.equals("")){
																if(fieldType.toString().equals("class java.util.Date")){
																	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																	Date tempDateValue = (Date) tempFieldValue;
														    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
														    		String dateValue = nowYYYYMMDD1.toString();
																	emailBodyTempValue.append(dateValue+" ");
																}else{
																	emailBodyTempValue.append(tempFieldValue+" ");
																}
															}
										        		}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
									        				errorMessage(getText(logMessage));
									        				return SUCCESS;
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Body.";
									        				errorMessage(getText(logMessage));
									        				return SUCCESS;
									        			}
									        		}
												}
												}else{
													emailBodyTempValue.append(splittWordVal+" ");
												}
											}
										}else{
											String tempTableAndFieldname = splittedWordByTild.replaceAll("\\^\\$", "");
								        	if(tempTableAndFieldname.contains(".")){
								        		String [] tableFieldnameArr = tempTableAndFieldname.split("\\.");
								        		if(tableFieldnameArr.length!=0){
								        		String tableName = tableFieldnameArr[0];
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
								        			String fieldName = tableFieldnameArr[1];
								        			try{
									        			Class<?> cls = customerFile.getClass();
									        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
									        			modelFieldName.setAccessible(true);
														Object tempFieldValue =modelFieldName.get(customerFile);
														modelFieldValue =tempFieldValue+""; 
								        			}catch(IllegalAccessException e){
								        				e.printStackTrace();
								        				int pos =e.toString().lastIndexOf(":");
									            		String errorField = e.toString().substring(pos);
								        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
								        				errorMessage(getText(logMessage));
								        				return SUCCESS;
								        			} catch (NoSuchFieldException e) {
								        				e.printStackTrace();
								        				int pos =e.toString().lastIndexOf(":");
									            		String errorField = e.toString().substring(pos);
								        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Body.";
								        				errorMessage(getText(logMessage));
								        				return SUCCESS;
								        			}
								        		}
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
								        			if(sid==null){
								        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email Body as module is CustomerFile";
								        				errorMessage(getText(logMessage));
								        				return SUCCESS;
							        				}else{
									        			String fieldName = tableFieldnameArr[1];
									        			try{
										        			Class<?> cls = serviceOrder.getClass();
										        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(serviceOrder);
															modelFieldValue =tempFieldValue+"";
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
									        				errorMessage(getText(logMessage));
									        				return SUCCESS;
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Body.";
									        				errorMessage(getText(logMessage));
									        				return SUCCESS;
									        			}
									        		}
								        		}
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
								        			if(sid==null){
								        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use TrackingStatus table in email Body as module is CustomerFile";
								        				errorMessage(getText(logMessage));
								        				return SUCCESS;
							        				}else{
									        			String fieldName = tableFieldnameArr[1];
									        			try{
										        			Class<?> cls = trackingStatus.getClass();
										        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(trackingStatus);
															modelFieldValue =tempFieldValue+""; 
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
									        				errorMessage(getText(logMessage));
									        				return SUCCESS;
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Body.";
									        				errorMessage(getText(logMessage));
									        				return SUCCESS;
									        			}
									        		}
								        		}
								        	}
								        	}else{
								        		emailBodyTempValue.append(tempTableAndFieldname+" ");
								        	}
										}
								}	
								}else if(splittedWord.contains("$^") && splittedWord.contains(".")){
									String[] splittedValueNew = splittedWord.split(Pattern.quote("$^"));
									  for (String splittedWordNew : splittedValueNew){
												String tempTableAndFieldname = splittedWordNew.replaceAll("\\^\\$", "");
									        	if(tempTableAndFieldname.contains(".")){
									        		String [] tableFieldnameArr = tempTableAndFieldname.split("\\.");
									        		if(tableFieldnameArr.length!=0){
									        		String tableName = tableFieldnameArr[0];
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
									        			String fieldName = tableFieldnameArr[1];
									        			try{
									        				if(!fieldName.trim().equalsIgnoreCase("prefix")){
											        			Class<?> cls = customerFile.getClass();
											        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
											        			Object fieldType = modelFieldName.getType();
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(customerFile);
																if(tempFieldValue!=null && !tempFieldValue.equals("")){
																	if(fieldType.toString().equals("class java.util.Date")){
																		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																		Date tempDateValue = (Date) tempFieldValue;
															    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
															    		String dateValue = nowYYYYMMDD1.toString();
																		emailBodyTempValue.append(dateValue+" ");
																	}else{
																		emailBodyTempValue.append(tempFieldValue+" ");
																	}
																}
									        				}else{
									        					String preffixValue ="";
									       					 	if((customerFile.getPrefix()!=null && !customerFile.getPrefix().equalsIgnoreCase("")) && (customerFile.getCustomerLanguagePreference()!=null && !customerFile.getCustomerLanguagePreference().equalsIgnoreCase(""))){
									       					 		preffixValue = customerFileManager.getPreffixFromRefmaster("PREFFIX",customerFile.getCustomerLanguagePreference(), customerFile.getPrefix());
									       							emailBodyTempValue.append(preffixValue+" ");
									       					 	}
									        				}
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
									        				errorMessage(getText(logMessage));
									        				return SUCCESS;
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
									        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Body.";
									        				errorMessage(getText(logMessage));
									        				return SUCCESS;
									        			}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
									        			if(sid==null){
									        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use ServiceOrder table in email Body as module is CustomerFile";
									        				errorMessage(getText(logMessage));
									        				return SUCCESS;
								        				}else{
										        			String fieldName = tableFieldnameArr[1];
										        			try{
										        				if(!fieldName.trim().equalsIgnoreCase("prefix")){
												        			Class<?> cls = serviceOrder.getClass();
												        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
												        			Object fieldType = modelFieldName.getType();
												        			modelFieldName.setAccessible(true);
																	Object tempFieldValue =modelFieldName.get(serviceOrder);
																	if(tempFieldValue!=null && !tempFieldValue.equals("")){
																		if(fieldType.toString().equals("class java.util.Date")){
																			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																			Date tempDateValue = (Date) tempFieldValue;
																    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
																    		String dateValue = nowYYYYMMDD1.toString();
																			emailBodyTempValue.append(dateValue+" ");
																		}else{
																			emailBodyTempValue.append(tempFieldValue+" ");
																		}
																	}
										        				}else{
										        					String preffixValue ="";
										       					 	if((customerFile.getPrefix()!=null && !customerFile.getPrefix().equalsIgnoreCase("")) && (customerFile.getCustomerLanguagePreference()!=null && !customerFile.getCustomerLanguagePreference().equalsIgnoreCase(""))){
										       					 		preffixValue = customerFileManager.getPreffixFromRefmaster("PREFFIX",customerFile.getCustomerLanguagePreference(), customerFile.getPrefix());
										       							emailBodyTempValue.append(preffixValue+" ");
										       					 	}
										        				}
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
										        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
										        				errorMessage(getText(logMessage));
										        				return SUCCESS;
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
										        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Body.";
										        				errorMessage(getText(logMessage));
										        				return SUCCESS;
										        			}
										        		}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
									        			if(sid==null){
									        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use TrackingStatus table in email Body as module is CustomerFile";
									        				errorMessage(getText(logMessage));
									        				return SUCCESS;
								        				}else{
										        			String fieldName = tableFieldnameArr[1];
										        			try{
											        			Class<?> cls = trackingStatus.getClass();
											        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
											        			Object fieldType = modelFieldName.getType();
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(trackingStatus);
																if(tempFieldValue!=null && !tempFieldValue.equals("")){
																	if(fieldType.toString().equals("class java.util.Date")){
																		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																		Date tempDateValue = (Date) tempFieldValue;
															    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
															    		String dateValue = nowYYYYMMDD1.toString();
																		emailBodyTempValue.append(dateValue+" ");
																	}else{
																		emailBodyTempValue.append(tempFieldValue+" ");
																	}
																}
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
										        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
										        				errorMessage(getText(logMessage));
										        				return SUCCESS;
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
										        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Body.";
										        				errorMessage(getText(logMessage));
										        				return SUCCESS;
										        			}
										        		}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("miscellaneous")){
									        			if(sid==null){
									        				logMessage = "Mail can not be sent because <BR> ErrorIN : You can,t use Miscellaneous table in email Body as module is CustomerFile";
									        				errorMessage(getText(logMessage));
									        				return SUCCESS;
								        				}else{
										        			String fieldName = tableFieldnameArr[1];
										        			try{
											        			Class<?> cls = miscellaneous.getClass();
											        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
											        			Object fieldType = modelFieldName.getType();
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(miscellaneous);
																if(tempFieldValue!=null && !tempFieldValue.equals("")){
																	if(fieldType.toString().equals("class java.util.Date")){
																		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																		Date tempDateValue = (Date) tempFieldValue;
															    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
															    		String dateValue = nowYYYYMMDD1.toString();
																		emailBodyTempValue.append(dateValue+" ");
																	}else{
																		emailBodyTempValue.append(tempFieldValue+" ");
																	}
																}
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
										        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName;
										        				errorMessage(getText(logMessage));
										        				return SUCCESS;
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
										        				logMessage =  "Mail can not be sent because <BR> ErrorIN "+ errorField+" in "+tableName+".Incorrect field name in email Body.";
										        				errorMessage(getText(logMessage));
										        				return SUCCESS;
										        			}
										        		}
									        		}
									        	}
								        	}else{
								        		emailBodyTempValue.append(tempTableAndFieldname+" ");
								        	}
									  }
									}else{
										emailBodyTempValue.append(splittedWord+" ");
									}
								}
							  }else{
								  emailBodyTempValue.append(bodyEmptyTokenizer+" ");
							  }
						  }
					}else{
						if(str.contains("UserImage?location")){
							StringBuilder aa =new StringBuilder(str);
							Random rand = new Random(); 
							int intValue = rand.nextInt(25);
							try{
							StringTokenizer bodyTokenizer = new StringTokenizer(str);
							  while (bodyTokenizer.hasMoreTokens()) {
								  bodyEmptyTokenizer = bodyTokenizer.nextToken();
								  if(bodyEmptyTokenizer.contains("UserImage?location")){
									  Pattern pImg = Pattern.compile("\"([^\"]*)\"");
									  Matcher mImg = pImg.matcher(bodyEmptyTokenizer);
									  String imageSource="";
									  String imagePath="";
									  while(mImg.find()){
										  imageSource = (String) mImg.group();
										  imagePath = imageSource.substring(20);
										  imagePath = imagePath.replaceAll("\"", "");
										  //imagePath = imagePath.replace("\\", "/");
										  imageSource = "cid:image"+intValue;
										  char quotes ='"';
										  emailBodyTempValue.append("src="+quotes+imageSource+quotes+" ");
										  if(fileLocation!=null && !fileLocation.equalsIgnoreCase("")){
											  fileLocation = fileLocation+"~"+imagePath+"#"+"image"+intValue;
										  }else{
											  fileLocation = imagePath+"#"+"image"+intValue; 
										  }
									  }
								  }else{
									  emailBodyTempValue.append(bodyEmptyTokenizer+" ");
								  }
							  }
							}catch(Exception e){
								e.printStackTrace();
							}
						}else{
							emailBodyTempValue.append(str+" ");
						}
					}
				}
				//System.out.println(emailBodyTempValue);
			} catch(IOException e) {
				e.printStackTrace();
				logMessage =  "Mail can not be sent because\nException:"+ e.toString();
				errorMessage(getText(logMessage));
				return SUCCESS;
			}
			emailBody = emailBodyTempValue.toString();
		}
		
		
		String lastSentEmailBody ="";
		if(idCurrentValue!=null){
			lastSentEmailBody = emailBodyCurrentValue;
		}else{
			lastSentEmailBody = emailSetupTemplate.getEmailBody();
		}
		if(lastSentEmailBody!=null && !lastSentEmailBody.equalsIgnoreCase("")){
			lastSentEmailBody = lastSentEmailBody.replaceAll("<br />", "<br/>");
			StringBuilder lastSentEmailBodyTempValue = new StringBuilder();
			String str;
			BufferedReader reader = new BufferedReader(new StringReader(lastSentEmailBody));
			String bodyEmptyTokenizer="";
			try {       
				while((str = reader.readLine()) != null) {
					if(str.contains("^$")){
						  StringTokenizer bodyTokenizer = new StringTokenizer(str);
						  while (bodyTokenizer.hasMoreTokens()) {
							  bodyEmptyTokenizer = bodyTokenizer.nextToken();
							  if(bodyEmptyTokenizer.contains("^$")){
								String[] splittedValue = bodyEmptyTokenizer.split(Pattern.quote("^$"));
								for (String splittedWord : splittedValue){
								if(splittedWord.contains("$^") && splittedWord.contains(".") && splittedWord.contains("~")){
									String modelFieldValue = "";
									String[] splittedValueByTild = splittedWord.split(Pattern.quote("~"));
									for(String splittedWordByTild : splittedValueByTild){
										if(splittedWordByTild.contains("$^")){
											String[] splittWordArr = splittedWordByTild.split(Pattern.quote("$^"));
											for(String splittWordVal : splittWordArr){
												if(splittWordVal.contains(".")){
													String [] tableFieldNameArr = splittWordVal.split("\\.");
													if(tableFieldNameArr.length!=0){
									        		String tableName = tableFieldNameArr[0];
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("user") && !modelFieldValue.equalsIgnoreCase("")){
									        			String tempFieldName = tableFieldNameArr[1];
									        			try{
										        			userNew = userManager.getUserByUsername(modelFieldValue);
										        			Class<?> cls = userNew.getClass();
										        			Field modelFieldName = cls.getDeclaredField(tempFieldName.trim());
										        			Object fieldType = modelFieldName.getType();
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(userNew);
															if(tempFieldValue!=null && !tempFieldValue.equals("")){
																if(fieldType.toString().equals("class java.util.Date")){
																	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																	Date tempDateValue = (Date) tempFieldValue;
														    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
														    		String dateValue = nowYYYYMMDD1.toString();
														    		lastSentEmailBodyTempValue.append(dateValue+" ");
																}else{
																	lastSentEmailBodyTempValue.append(tempFieldValue+" ");
																}
															}
										        		}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        			}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("partnerPublic") && !modelFieldValue.equalsIgnoreCase("")){
									        			String tempFieldName = tableFieldNameArr[1];
									        			try{
										        			partnerPublicObj = partnerPublicManager.getPartnerByCode(modelFieldValue);
										        			Class<?> cls = partnerPublicObj.getClass();
										        			Field modelFieldName = cls.getDeclaredField(tempFieldName.trim());
										        			Object fieldType = modelFieldName.getType();
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(partnerPublicObj);
															if(tempFieldValue!=null && !tempFieldValue.equals("")){
																if(fieldType.toString().equals("class java.util.Date")){
																	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																	Date tempDateValue = (Date) tempFieldValue;
														    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
														    		String dateValue = nowYYYYMMDD1.toString();
														    		lastSentEmailBodyTempValue.append(dateValue+" ");
																}else{
																	lastSentEmailBodyTempValue.append(tempFieldValue+" ");
																}
															}
										        		}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        			}
									        		}
												}
												}else{
													lastSentEmailBodyTempValue.append(splittWordVal+" ");
												}
											}
										}else{
											String tempTableAndFieldname = splittedWordByTild.replaceAll("\\^\\$", "");
								        	if(tempTableAndFieldname.contains(".")){
								        		String [] tableFieldnameArr = tempTableAndFieldname.split("\\.");
								        		if(tableFieldnameArr.length!=0){
								        		String tableName = tableFieldnameArr[0];
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
								        			String fieldName = tableFieldnameArr[1];
								        			try{
									        			Class<?> cls = customerFile.getClass();
									        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
									        			modelFieldName.setAccessible(true);
														Object tempFieldValue =modelFieldName.get(customerFile);
														modelFieldValue =tempFieldValue+""; 
								        			}catch(IllegalAccessException e){
								        				e.printStackTrace();
								        			} catch (NoSuchFieldException e) {
								        				e.printStackTrace();
								        			}
								        		}
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
								        			if(sid==null){
							        				}else{
									        			String fieldName = tableFieldnameArr[1];
									        			try{
										        			Class<?> cls = serviceOrder.getClass();
										        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(serviceOrder);
															modelFieldValue =tempFieldValue+"";
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        			}
									        		}
								        		}
								        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
								        			if(sid==null){
							        				}else{
									        			String fieldName = tableFieldnameArr[1];
									        			try{
										        			Class<?> cls = trackingStatus.getClass();
										        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(trackingStatus);
															modelFieldValue =tempFieldValue+""; 
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        			}
									        		}
								        		}
								        	}
								        	}else{
								        		lastSentEmailBodyTempValue.append(tempTableAndFieldname+" ");
								        	}
										}
								}	
								}else if(splittedWord.contains("$^") && splittedWord.contains(".")){
									String[] splittedValueNew = splittedWord.split(Pattern.quote("$^"));
									  for (String splittedWordNew : splittedValueNew){
												String tempTableAndFieldname = splittedWordNew.replaceAll("\\^\\$", "");
									        	if(tempTableAndFieldname.contains(".")){
									        		String [] tableFieldnameArr = tempTableAndFieldname.split("\\.");
									        		if(tableFieldnameArr.length!=0){
									        		String tableName = tableFieldnameArr[0];
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
									        			String fieldName = tableFieldnameArr[1];
									        			try{
									        				if(!fieldName.trim().equalsIgnoreCase("prefix")){
											        			Class<?> cls = customerFile.getClass();
											        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
											        			Object fieldType = modelFieldName.getType();
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(customerFile);
																if(tempFieldValue!=null && !tempFieldValue.equals("")){
																	if(fieldType.toString().equals("class java.util.Date")){
																		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																		Date tempDateValue = (Date) tempFieldValue;
															    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
															    		String dateValue = nowYYYYMMDD1.toString();
															    		lastSentEmailBodyTempValue.append(dateValue+" ");
																	}else{
																		lastSentEmailBodyTempValue.append(tempFieldValue+" ");
																	}
																}
									        				}else{
									        					String preffixValue ="";
									       					 	if((customerFile.getPrefix()!=null && !customerFile.getPrefix().equalsIgnoreCase("")) && (customerFile.getCustomerLanguagePreference()!=null && !customerFile.getCustomerLanguagePreference().equalsIgnoreCase(""))){
									       					 		preffixValue = customerFileManager.getPreffixFromRefmaster("PREFFIX",customerFile.getCustomerLanguagePreference(), customerFile.getPrefix());
									       					 		lastSentEmailBodyTempValue.append(preffixValue+" ");
									       					 	}
									        				}
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        			}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
									        			if(sid==null){
								        				}else{
										        			String fieldName = tableFieldnameArr[1];
										        			try{
										        				if(!fieldName.trim().equalsIgnoreCase("prefix")){
												        			Class<?> cls = serviceOrder.getClass();
												        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
												        			Object fieldType = modelFieldName.getType();
												        			modelFieldName.setAccessible(true);
																	Object tempFieldValue =modelFieldName.get(serviceOrder);
																	if(tempFieldValue!=null && !tempFieldValue.equals("")){
																		if(fieldType.toString().equals("class java.util.Date")){
																			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																			Date tempDateValue = (Date) tempFieldValue;
																    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
																    		String dateValue = nowYYYYMMDD1.toString();
																    		lastSentEmailBodyTempValue.append(dateValue+" ");
																		}else{
																			lastSentEmailBodyTempValue.append(tempFieldValue+" ");
																		}
																	}
										        				}else{
										        					String preffixValue ="";
										       					 	if((customerFile.getPrefix()!=null && !customerFile.getPrefix().equalsIgnoreCase("")) && (customerFile.getCustomerLanguagePreference()!=null && !customerFile.getCustomerLanguagePreference().equalsIgnoreCase(""))){
										       					 		preffixValue = customerFileManager.getPreffixFromRefmaster("PREFFIX",customerFile.getCustomerLanguagePreference(), customerFile.getPrefix());
										       					 	lastSentEmailBodyTempValue.append(preffixValue+" ");
										       					 	}
										        				}
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        			}
										        		}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
									        			if(sid==null){
								        				}else{
										        			String fieldName = tableFieldnameArr[1];
										        			try{
											        			Class<?> cls = trackingStatus.getClass();
											        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
											        			Object fieldType = modelFieldName.getType();
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(trackingStatus);
																if(tempFieldValue!=null && !tempFieldValue.equals("")){
																	if(fieldType.toString().equals("class java.util.Date")){
																		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																		Date tempDateValue = (Date) tempFieldValue;
															    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
															    		String dateValue = nowYYYYMMDD1.toString();
															    		lastSentEmailBodyTempValue.append(dateValue+" ");
																	}else{
																		lastSentEmailBodyTempValue.append(tempFieldValue+" ");
																	}
																}
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        			}
										        		}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("miscellaneous")){
									        			if(sid==null){
								        				}else{
										        			String fieldName = tableFieldnameArr[1];
										        			try{
											        			Class<?> cls = miscellaneous.getClass();
											        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
											        			Object fieldType = modelFieldName.getType();
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(miscellaneous);
																if(tempFieldValue!=null && !tempFieldValue.equals("")){
																	if(fieldType.toString().equals("class java.util.Date")){
																		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																		Date tempDateValue = (Date) tempFieldValue;
															    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
															    		String dateValue = nowYYYYMMDD1.toString();
															    		lastSentEmailBodyTempValue.append(dateValue+" ");
																	}else{
																		lastSentEmailBodyTempValue.append(tempFieldValue+" ");
																	}
																}
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        			}
										        		}
									        		}
									        	}
								        	}else{
								        		lastSentEmailBodyTempValue.append(tempTableAndFieldname+" ");
								        	}
									  }
									}else{
										lastSentEmailBodyTempValue.append(splittedWord+" ");
									}
								}
							  }else{
								  lastSentEmailBodyTempValue.append(bodyEmptyTokenizer+" ");
							  }
						  }
					}else{
							lastSentEmailBodyTempValue.append(str+" ");
					}
				}
			} catch(IOException e) {
			}
			lastSentEmailBody = lastSentEmailBodyTempValue.toString();
		}
		
		//fileLocation = emailSetupTemplate.getAttachedFileLocation();
		String tempFileNameWithLocation="";
		//String fileNameWithPath = "";
		if((attachedFileNameListEditVal!=null && !attachedFileNameListEditVal.equalsIgnoreCase("")) && attachedFileNameListEditVal.equalsIgnoreCase("allDeleted")){
			tempFileNameWithLocation = "";
		}else if(attachedFileNameListEditVal!=null && !attachedFileNameListEditVal.equalsIgnoreCase("")){
			emailSetupTemplate = emailSetupTemplateManager.get(idCurrentValue);
			if(cid!=null && emailSetupTemplate.getFileTypeOfFileCabinet()!=null && !emailSetupTemplate.getFileTypeOfFileCabinet().equalsIgnoreCase("")){
				fileCabinetListByDocumentTpye = emailSetupTemplateManager.findDocsFromFileCabinet(emailSetupTemplate.getFileTypeOfFileCabinet(),customerFile.getSequenceNumber(),sessionCorpID);
			}else if(sid!=null && emailSetupTemplate.getFileTypeOfFileCabinet()!=null && !emailSetupTemplate.getFileTypeOfFileCabinet().equalsIgnoreCase("")){
				fileCabinetListByDocumentTpye = emailSetupTemplateManager.findDocsFromFileCabinet(emailSetupTemplate.getFileTypeOfFileCabinet(),serviceOrder.getShipNumber(),sessionCorpID);
			}
			String fileNameStr = attachedFileNameListEditVal.replace("[", "").replace("]", "");
				for(String str1:fileNameStr.split("\\^")){
					if(str1!=null && !str1.equalsIgnoreCase("")){
						
						if(fileCabinetIdListFromValue==null || fileCabinetIdListFromValue.equalsIgnoreCase("")){
							if(str1.contains("File Template : ")){
							str1 = str1.replaceAll("File Template : ", "");
								for(String str:emailSetupTemplate.getAttachedFileLocation().split("~")){
					        		if(str!=null && !str.equalsIgnoreCase("") && str.contains(str1)){
					        			if(tempFileNameWithLocation!=null && !tempFileNameWithLocation.equalsIgnoreCase("")){
						        			tempFileNameWithLocation = tempFileNameWithLocation+"~"+str;
						        		}else{
						        			tempFileNameWithLocation = str;
						        		}
					        		}
					        	}
							}else if(str1.contains("JRXML : ")){
							str1 = str1.replaceAll("JRXML : ", "");
							String fileNameWithPathJrxml = "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + "EmailSetupTemplate" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/"+str1;
								File fileToDownload = new File(fileNameWithPathJrxml);
								if(fileToDownload.exists()){
									if(tempFileNameWithLocation!=null && !tempFileNameWithLocation.equalsIgnoreCase("")){
					        			tempFileNameWithLocation = tempFileNameWithLocation+"~"+fileNameWithPathJrxml;
					        		}else{
					        			tempFileNameWithLocation = fileNameWithPathJrxml;
					        		}
								}
							}else if(str1.contains("File Cabinet : ")){
								for(String strFileCabinet:fileCabinetListByDocumentTpye.split("~")){
									String fileNamePrefix = "File Cabinet : "+emailSetupTemplate.getFileTypeOfFileCabinet()+" - ";
									str1 = str1.replaceAll(fileNamePrefix, "");
					        		if(strFileCabinet!=null && !strFileCabinet.equalsIgnoreCase("") && strFileCabinet.contains(str1)){
					        			if(tempFileNameWithLocation!=null && !tempFileNameWithLocation.equalsIgnoreCase("")){
						        			tempFileNameWithLocation = tempFileNameWithLocation+"~"+strFileCabinet;
						        		}else{
						        			tempFileNameWithLocation = strFileCabinet;
						        		}
					        			break;
					        		}
					        	}
							}
						}
						if((fileCabinetIdListFromValue!=null && !fileCabinetIdListFromValue.equalsIgnoreCase("") && fileCabinetIdListFromValue.equalsIgnoreCase("loadLastSent")) && (!lastSentMailAttachLocation.equalsIgnoreCase(""))){
							for(String lastSentMailLocation:lastSentMailAttachLocation.split("~")){
				        		if(lastSentMailLocation!=null && !lastSentMailLocation.equalsIgnoreCase("") && lastSentMailLocation.contains(str1)){
				        			if(tempFileNameWithLocation!=null && !tempFileNameWithLocation.equalsIgnoreCase("")){
					        			tempFileNameWithLocation = tempFileNameWithLocation+"~"+lastSentMailLocation;
					        		}else{
					        			tempFileNameWithLocation = lastSentMailLocation;
					        		}
				        			break;
				        		}
				        	}
						}
					}
				}
		}
        
		if((fileLocation!=null && !fileLocation.equalsIgnoreCase("")) && (tempFileNameWithLocation!=null && !tempFileNameWithLocation.equalsIgnoreCase(""))){
			fileLocation = fileLocation+"~"+tempFileNameWithLocation;
		}else if(tempFileNameWithLocation!=null && !tempFileNameWithLocation.equalsIgnoreCase("")){
			fileLocation = tempFileNameWithLocation;
		}
		
		if(fileCabinetFileIdListValue!=null && !fileCabinetFileIdListValue.equalsIgnoreCase("")){
			for(String fileId:fileCabinetFileIdListValue.split("\\^")){
				myFile=myFileManager.get(Long.parseLong(fileId));
				String myFileLocation = myFile.getLocation();
	    			if(fileLocation!=null && !fileLocation.equalsIgnoreCase("")){
	    				fileLocation = fileLocation+"~"+myFileLocation;
	        		}else{
	        			fileLocation = myFileLocation;
	        		}
	    		
			}
		}
		
		String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + "EmailSetupTemplate" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
		String uploadTempFilePath="";
		try{
			for (int i = 0; i < fileUpload.length; i++) {
				String fileName = "";
	            fileName = fileUploadFileName[i];
	            int num = 1;
	            try {
	            	if(fileName != null && !fileName.equalsIgnoreCase("")){
	            		fileName = fileName.replaceAll("'", "");
	            		fileName = fileName.replaceAll("#", "");
	            		File uploadedFile = fileUpload[i];
	            		int pos =fileName.lastIndexOf(".");
	            		String fileExtension1 = fileName.substring(0, pos);
	            		String fileExtension2 = fileName.substring(pos);
	            		File fileToCreate = new File(uploadDir,fileName);
	            		while(fileToCreate.exists()) {
	            			fileName = fileExtension1 +" ("+ (num++)+")"+fileExtension2;
	            		    fileToCreate = new File(uploadDir,fileName); 
	            		}
	            		
	            		FileUtils.copyFile(uploadedFile, fileToCreate);
	            		if(uploadTempFilePath==null || uploadTempFilePath.equalsIgnoreCase("")){
	            			uploadTempFilePath =(uploadDir+fileName );
						}else{
							uploadTempFilePath =uploadTempFilePath+"~"+(uploadDir+fileName );
						}
						
	            	}
	            } catch (IOException ex) {
	                ex.printStackTrace();
	            }
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if((fileLocation!=null && !fileLocation.equalsIgnoreCase("")) && (uploadTempFilePath!=null && !uploadTempFilePath.equalsIgnoreCase(""))){
			fileLocation = fileLocation+"~"+uploadTempFilePath;
		}else if(uploadTempFilePath!=null && !uploadTempFilePath.equalsIgnoreCase("")){
			fileLocation = uploadTempFilePath;
		}
		
		String emailSignatureValue = "";
		if(emailSetupTemplate.getAddSignatureFromAppUser()!=null && emailSetupTemplate.getAddSignatureFromAppUser()){
			List userSignatureValue = customerFileManager.findUserSignature(emailFrom);
			if(userSignatureValue!=null && !userSignatureValue.isEmpty()){
				String signatureValue = userSignatureValue.get(0).toString();
				emailSignatureValue = signatureValue;
			}
		}
		if(cid!=null){
			emailSetupManager.insertMailFromEmailSetupTemplate(emailFrom, emailTo, emailCc, emailBcc, fileLocation, emailBody, subject, sessionCorpID,"CustomerFile",customerFile.getSequenceNumber(),emailSignatureValue,emailSetupTemplate.getSaveAs(),lastSentEmailBody);
		}
		if(sid!=null){
			emailSetupManager.insertMailFromEmailSetupTemplate(emailFrom, emailTo, emailCc, emailBcc, fileLocation, emailBody, subject, sessionCorpID,"ServiceOrder",serviceOrder.getShipNumber(),emailSignatureValue,emailSetupTemplate.getSaveAs(),lastSentEmailBody);
		}
		saveMessage(getText(logMessage));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" sendEmailFromEmailTemplateEdit() End");
		return SUCCESS;
	}
	private String fileCabinetIdListFromValue;
	private String fileCabinetFileIdListValue;
	private boolean emailTemplateActive;
	@SkipValidation
	public String activeInactiveEmailTemplate(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" activeInactiveEmailTemplate() Start");
		if(id!=null){
			emailSetupTemplate = emailSetupTemplateManager.get(id);
			emailSetupTemplate.setEnable(emailTemplateActive);
			emailSetupTemplate.setUpdatedOn(new Date());
			emailSetupTemplate.setUpdatedBy(getRequest().getRemoteUser());
			emailSetupTemplateManager.save(emailSetupTemplate);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" activeInactiveEmailTemplate() End");
		return SUCCESS;
	}
	private Long lastSentMailId;
	private String attachedFileNameListEditVal;
	private String editModalId;
	private String emailFromEditedValue;
	private String emailToEditedValue;
	private String emailCcEditedValue;
	private String emailBccEditedValue;
	private String emailSubjectEditedValue;
	private String emailBodyEditedValue;
	@SkipValidation
	public String editEmailPopupFromEmailTemplate() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" editEmailPopupFromEmailTemplate() Start");
		List lastSentMailList = new ArrayList();
		
		if(id!=null){
			emailSetupTemplate = emailSetupTemplateManager.get(id);
			
			if(cid!=null){
				customerFile = customerFileManager.get(cid);
				String fileNumber = customerFile.getSequenceNumber();
				String saveAsVal = emailSetupTemplate.getSaveAs();
				lastSentMailList = emailSetupTemplateManager.findLastSentMail(sessionCorpID,"CustomerFile",fileNumber,saveAsVal);
			}
			if(sid!=null){
				serviceOrder = serviceOrderManager.get(sid);
				trackingStatus = trackingStatusManager.get(sid);
				miscellaneous = miscellaneousManager.get(sid);
				customerFile = customerFileManager.get(serviceOrder.getCustomerFileId());
				String fileNumber = serviceOrder.getShipNumber();
				String saveAsVal = emailSetupTemplate.getSaveAs();
				lastSentMailList = emailSetupTemplateManager.findLastSentMail(sessionCorpID,"ServiceOrder",fileNumber,saveAsVal);
			}
			
			if(lastSentMailList!=null && !lastSentMailList.isEmpty() && lastSentMailList.get(0)!=null){
				EmailSetup emailSetupObj = (EmailSetup)lastSentMailList.get(0);
				lastSentMailId = emailSetupObj.getId();
			}
			
			String tempEmailFromVal="";
			String emailFrom = "";
			emailFrom = emailSetupTemplate.getEmailFrom();
			if(emailFrom!=null && !emailFrom.equalsIgnoreCase("")){
				String[] splittedEmailFromValue = emailFrom.split(",");
				for (String splittedEmailFrom : splittedEmailFromValue){
					if(splittedEmailFrom.contains("^$")){
						splittedEmailFrom = splittedEmailFrom.replaceAll("\\^\\$", "").replaceAll("\\$\\^", "");
			        	if(splittedEmailFrom.contains(".")){
			        		String [] tableFieldnameArr = splittedEmailFrom.split("\\.");
			        		String tableName = tableFieldnameArr[0];
			        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = customerFile.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(customerFile);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("coordinator") && !fieldName.trim().equalsIgnoreCase("personPricing") && !fieldName.trim().equalsIgnoreCase("personBilling") && !fieldName.trim().equalsIgnoreCase("personPayable") && !fieldName.trim().equalsIgnoreCase("auditor")&& !fieldName.trim().equalsIgnoreCase("approvedBy")){
											if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
												tempEmailFromVal = tempFieldValue+"";
											}else{
												tempEmailFromVal = tempEmailFromVal+","+tempFieldValue+"";
											}
										}else{
											userNew = userManager.getUserByUsername(tempFieldValue+"");
						        			Class<?> cls1 = userNew.getClass();
						        			Field usrModelFieldName = cls1.getDeclaredField("email");
						        			usrModelFieldName.setAccessible(true);
											Object usrFieldValue =usrModelFieldName.get(userNew);
											if(usrFieldValue!=null && !usrFieldValue.equals("")){
												if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
													tempEmailFromVal = usrFieldValue+"";
												}else{
													tempEmailFromVal = tempEmailFromVal+","+usrFieldValue+"";
												}
											}
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
				            		tempEmailFromVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
			        			} catch (NoSuchFieldException e) {
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
				            		tempEmailFromVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
			        			}
			        		}
			        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
			        			if(sid==null){
			        				tempEmailFromVal = "{ ErrorIN : You can,t use ServiceOrder table in email From as module is CustomerFile }";
		        				}else{
				        			String fieldName = tableFieldnameArr[1];
				        			try{
					        			Class<?> cls = serviceOrder.getClass();
					        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
					        			modelFieldName.setAccessible(true);
										Object tempFieldValue =modelFieldName.get(serviceOrder);
										if(tempFieldValue!=null && !tempFieldValue.equals("")){
											if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("salesMan") && !fieldName.trim().equalsIgnoreCase("coordinator")){
												if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
													tempEmailFromVal = tempFieldValue+"";
												}else{
													tempEmailFromVal = tempEmailFromVal+","+tempFieldValue+"";
												}
											}else{
												userNew = userManager.getUserByUsername(tempFieldValue+"");
							        			Class<?> cls1 = userNew.getClass();
							        			Field usrModelFieldName = cls1.getDeclaredField("email");
							        			usrModelFieldName.setAccessible(true);
												Object usrFieldValue =usrModelFieldName.get(userNew);
												if(usrFieldValue!=null && !usrFieldValue.equals("")){
													if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
														tempEmailFromVal = usrFieldValue+"";
													}else{
														tempEmailFromVal = tempEmailFromVal+","+usrFieldValue+"";
													}
												}
											}
										}
				        			}catch(IllegalAccessException e){
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailFromVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
				        			} catch (NoSuchFieldException e) {
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailFromVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
				        			}
				        		}
			        		}
			        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
			        			if(sid==null){
			        				tempEmailFromVal = "{ ErrorIN : You can,t use TrackingStatus table in email From as module is CustomerFile }";
		        				}else{
				        			String fieldName = tableFieldnameArr[1];
				        			try{
					        			Class<?> cls = trackingStatus.getClass();
					        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
					        			Object fieldType = modelFieldName.getType();
					        			modelFieldName.setAccessible(true);
										Object tempFieldValue =modelFieldName.get(trackingStatus);
										if(tempFieldValue!=null && !tempFieldValue.equals("")){
											if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
												tempEmailFromVal = tempFieldValue+"";
											}else{
												tempEmailFromVal = tempEmailFromVal+","+tempFieldValue+"";
											}
										}
				        			}catch(IllegalAccessException e){
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailFromVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
				        			} catch (NoSuchFieldException e) {
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailFromVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
				        			}
				        		}
			        		}
			        	}
					}else{
						if(splittedEmailFrom!=null && !splittedEmailFrom.equalsIgnoreCase("")){
							if(tempEmailFromVal== null || tempEmailFromVal.equalsIgnoreCase("")){
								tempEmailFromVal  = splittedEmailFrom;
							}else{
								tempEmailFromVal  = tempEmailFromVal+","+splittedEmailFrom;
							}
						}
					}
				}
				emailFrom = tempEmailFromVal;
			}
			emailFromEditedValue = emailFrom;
			
			String emailTo = "";
			String tempEmailToVal="";
			emailTo = emailSetupTemplate.getEmailTo();
			if(emailTo!=null && !emailTo.equalsIgnoreCase("")){
				String[] splittedEmailToValue = emailTo.split(",");
					for (String splittedEmailTo : splittedEmailToValue){
						if(splittedEmailTo.contains("^$")){
							splittedEmailTo = splittedEmailTo.replaceAll("\\^\\$", "").replaceAll("\\$\\^", "");
				        	if(splittedEmailTo.contains(".")){
				        		String [] tableFieldnameArr = splittedEmailTo.split("\\.");
				        		String tableName = tableFieldnameArr[0];
				        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
				        			String fieldName = tableFieldnameArr[1];
				        			try{
					        			Class<?> cls = customerFile.getClass();
					        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
					        			modelFieldName.setAccessible(true);
										Object tempFieldValue =modelFieldName.get(customerFile);
										if(tempFieldValue!=null && !tempFieldValue.equals("")){
											if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("coordinator") && !fieldName.trim().equalsIgnoreCase("personPricing") && !fieldName.trim().equalsIgnoreCase("personBilling") && !fieldName.trim().equalsIgnoreCase("personPayable") && !fieldName.trim().equalsIgnoreCase("auditor")&& !fieldName.trim().equalsIgnoreCase("approvedBy")){
												if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
													tempEmailToVal = tempFieldValue+"";
												}else{
													tempEmailToVal = tempEmailToVal+","+tempFieldValue+"";
												}
											}else{
												userNew = userManager.getUserByUsername(tempFieldValue+"");
							        			Class<?> cls1 = userNew.getClass();
							        			Field usrModelFieldName = cls1.getDeclaredField("email");
							        			usrModelFieldName.setAccessible(true);
												Object usrFieldValue =usrModelFieldName.get(userNew);
												if(usrFieldValue!=null && !usrFieldValue.equals("")){
													if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
														tempEmailToVal = usrFieldValue+"";
													}else{
														tempEmailToVal = tempEmailToVal+","+usrFieldValue+"";
													}
												}
											}
										}
				        			}catch(IllegalAccessException e){
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailToVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
				        			} catch (NoSuchFieldException e) {
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailToVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
				        			}
				        		}
				        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
				        			if(sid==null){
				        				tempEmailToVal = "{ ErrorIN : You can,t use ServiceOrder table as module is CustomerFile }";
				        			}else{
					        			String fieldName = tableFieldnameArr[1];
					        			try{
						        			Class<?> cls = serviceOrder.getClass();
						        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
						        			modelFieldName.setAccessible(true);
											Object tempFieldValue =modelFieldName.get(serviceOrder);
											if(tempFieldValue!=null && !tempFieldValue.equals("")){
												if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("salesMan") && !fieldName.trim().equalsIgnoreCase("coordinator")){
													if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
														tempEmailToVal = tempFieldValue+"";
													}else{
														tempEmailToVal = tempEmailToVal+","+tempFieldValue+"";
													}
												}else{
													userNew = userManager.getUserByUsername(tempFieldValue+"");
								        			Class<?> cls1 = userNew.getClass();
								        			Field usrModelFieldName = cls1.getDeclaredField("email");
								        			usrModelFieldName.setAccessible(true);
													Object usrFieldValue =usrModelFieldName.get(userNew);
													if(usrFieldValue!=null && !usrFieldValue.equals("")){
														if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
															tempEmailToVal = usrFieldValue+"";
														}else{
															tempEmailToVal = tempEmailToVal+","+usrFieldValue+"";
														}
													}
												}
											}
					        			}catch(IllegalAccessException e){
					        				e.printStackTrace();
					        				int pos =e.toString().lastIndexOf(":");
						            		String errorField = e.toString().substring(pos);
						            		tempEmailToVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
					        			} catch (NoSuchFieldException e) {
					        				e.printStackTrace();
					        				int pos =e.toString().lastIndexOf(":");
						            		String errorField = e.toString().substring(pos);
						            		tempEmailToVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
					        			}
				        			}
				        		}
				        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
				        			if(sid==null){
				        				tempEmailToVal = "{ ErrorIN : You can,t use TrackingStatus table as module is CustomerFile }";
				        			}else{
					        			String fieldName = tableFieldnameArr[1];
					        			try{
						        			Class<?> cls = trackingStatus.getClass();
						        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
						        			Object fieldType = modelFieldName.getType();
						        			modelFieldName.setAccessible(true);
											Object tempFieldValue =modelFieldName.get(trackingStatus);
											if(tempFieldValue!=null && !tempFieldValue.equals("")){
												if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
													tempEmailToVal = tempFieldValue+"";
												}else{
													tempEmailToVal = tempEmailToVal+","+tempFieldValue+"";
												}
											}
					        			}catch(IllegalAccessException e){
					        				e.printStackTrace();
					        				int pos =e.toString().lastIndexOf(":");
						            		String errorField = e.toString().substring(pos);
						            		tempEmailToVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
					        			} catch (NoSuchFieldException e) {
					        				e.printStackTrace();
					        				int pos =e.toString().lastIndexOf(":");
						            		String errorField = e.toString().substring(pos);
						            		tempEmailToVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
					        			}
				        			}
				        		}
				        	}
						}else{
							if(splittedEmailTo!=null && !splittedEmailTo.equalsIgnoreCase("")){
								if(tempEmailToVal== null || tempEmailToVal.equalsIgnoreCase("")){
									tempEmailToVal  = splittedEmailTo;
								}else{
									tempEmailToVal  = tempEmailToVal+","+splittedEmailTo;
								}
							}
						}
				}
				emailTo = tempEmailToVal;
			}
			emailToEditedValue = emailTo;
			
			String tempEmailCcVal="";
			String emailCc = "";
			emailCc = emailSetupTemplate.getEmailCc();
			if(emailCc!=null && !emailCc.equalsIgnoreCase("")){
				String[] splittedEmailCcValue = emailCc.split(",");
				for (String splittedEmailCc : splittedEmailCcValue){
					if(splittedEmailCc.contains("^$")){
						splittedEmailCc = splittedEmailCc.replaceAll("\\^\\$", "").replaceAll("\\$\\^", "");
			        	if(splittedEmailCc.contains(".")){
			        		String [] tableFieldnameArr = splittedEmailCc.split("\\.");
			        		String tableName = tableFieldnameArr[0];
			        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = customerFile.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(customerFile);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("coordinator") && !fieldName.trim().equalsIgnoreCase("personPricing") && !fieldName.trim().equalsIgnoreCase("personBilling") && !fieldName.trim().equalsIgnoreCase("personPayable") && !fieldName.trim().equalsIgnoreCase("auditor")&& !fieldName.trim().equalsIgnoreCase("approvedBy")){
											if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
												tempEmailCcVal = tempFieldValue+"";
											}else{
												tempEmailCcVal = tempEmailCcVal+","+tempFieldValue+"";
											}
										}else{
											userNew = userManager.getUserByUsername(tempFieldValue+"");
						        			Class<?> cls1 = userNew.getClass();
						        			Field usrModelFieldName = cls1.getDeclaredField("email");
						        			usrModelFieldName.setAccessible(true);
											Object usrFieldValue =usrModelFieldName.get(userNew);
											if(usrFieldValue!=null && !usrFieldValue.equals("")){
												if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
													tempEmailCcVal = usrFieldValue+"";
												}else{
													tempEmailCcVal = tempEmailCcVal+","+usrFieldValue+"";
												}
											}
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
				            		tempEmailCcVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
			        			} catch (NoSuchFieldException e) {
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
				            		tempEmailCcVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
			        			}
			        		}
			        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
			        			if(sid==null){
			        				tempEmailCcVal = "{ ErrorIN : You can,t use ServiceOrder table as module is CustomerFile }";
		        				}else{
				        			String fieldName = tableFieldnameArr[1];
				        			try{
					        			Class<?> cls = serviceOrder.getClass();
					        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
					        			modelFieldName.setAccessible(true);
										Object tempFieldValue =modelFieldName.get(serviceOrder);
										if(tempFieldValue!=null && !tempFieldValue.equals("")){
											if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("salesMan") && !fieldName.trim().equalsIgnoreCase("coordinator")){
												if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
													tempEmailCcVal = tempFieldValue+"";
												}else{
													tempEmailCcVal = tempEmailCcVal+","+tempFieldValue+"";
												}
											}else{
												userNew = userManager.getUserByUsername(tempFieldValue+"");
							        			Class<?> cls1 = userNew.getClass();
							        			Field usrModelFieldName = cls1.getDeclaredField("email");
							        			usrModelFieldName.setAccessible(true);
												Object usrFieldValue =usrModelFieldName.get(userNew);
												if(usrFieldValue!=null && !usrFieldValue.equals("")){
													if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
														tempEmailCcVal = usrFieldValue+"";
													}else{
														tempEmailCcVal = tempEmailCcVal+","+usrFieldValue+"";
													}
												}
											}
										}
				        			}catch(IllegalAccessException e){
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailCcVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
				        			} catch (NoSuchFieldException e) {
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailCcVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
				        			}
				        		}
				        	}
			        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
			        			if(sid==null){
			        				tempEmailCcVal = "{ ErrorIN : You can,t use TrackingStatus table as module is CustomerFile }";
			        			}else{
				        			String fieldName = tableFieldnameArr[1];
				        			try{
					        			Class<?> cls = trackingStatus.getClass();
					        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
					        			Object fieldType = modelFieldName.getType();
					        			modelFieldName.setAccessible(true);
										Object tempFieldValue =modelFieldName.get(trackingStatus);
										if(tempFieldValue!=null && !tempFieldValue.equals("")){
											if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
												tempEmailCcVal = tempFieldValue+"";
											}else{
												tempEmailCcVal = tempEmailCcVal+","+tempFieldValue+"";
											}
										}
				        			}catch(IllegalAccessException e){
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailCcVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
				        			} catch (NoSuchFieldException e) {
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailCcVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
				        			}
			        			}
			        		}
			        	}
					}else{
						if(splittedEmailCc!=null && !splittedEmailCc.equalsIgnoreCase("")){
							if(tempEmailCcVal== null || tempEmailCcVal.equalsIgnoreCase("")){
								tempEmailCcVal  = splittedEmailCc;
							}else{
								tempEmailCcVal  = tempEmailCcVal+","+splittedEmailCc;
							}
						}
					}
				}
				emailCc = tempEmailCcVal;
			}
			emailCcEditedValue = emailCc;
			
			String tempEmailBccVal ="";
			String emailBcc = "";
			emailBcc = emailSetupTemplate.getEmailBcc();
			if(emailBcc!=null && !emailBcc.equalsIgnoreCase("")){
				String[] splittedEmailBccValue = emailBcc.split(",");
				for (String splittedEmailBcc : splittedEmailBccValue){
					if(splittedEmailBcc.contains("^$")){
						splittedEmailBcc = splittedEmailBcc.replaceAll("\\^\\$", "").replaceAll("\\$\\^", "");
			        	if(splittedEmailBcc.contains(".")){
			        		String [] tableFieldnameArr = splittedEmailBcc.split("\\.");
			        		String tableName = tableFieldnameArr[0];
			        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
			        			String fieldName = tableFieldnameArr[1];
			        			try{
				        			Class<?> cls = customerFile.getClass();
				        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
				        			modelFieldName.setAccessible(true);
									Object tempFieldValue =modelFieldName.get(customerFile);
									if(tempFieldValue!=null && !tempFieldValue.equals("")){
										if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("coordinator") && !fieldName.trim().equalsIgnoreCase("personPricing") && !fieldName.trim().equalsIgnoreCase("personBilling") && !fieldName.trim().equalsIgnoreCase("personPayable") && !fieldName.trim().equalsIgnoreCase("auditor")&& !fieldName.trim().equalsIgnoreCase("approvedBy")){
											if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
												tempEmailBccVal = tempFieldValue+"";
											}else{
												tempEmailBccVal = tempEmailBccVal+","+tempFieldValue+"";
											}
										}else{
											userNew = userManager.getUserByUsername(tempFieldValue+"");
						        			Class<?> cls1 = userNew.getClass();
						        			Field usrModelFieldName = cls1.getDeclaredField("email");
						        			usrModelFieldName.setAccessible(true);
											Object usrFieldValue =usrModelFieldName.get(userNew);
											if(usrFieldValue!=null && !usrFieldValue.equals("")){
												if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
													tempEmailBccVal = usrFieldValue+"";
												}else{
													tempEmailBccVal = tempEmailBccVal+","+usrFieldValue+"";
												}
											}
										}
									}
			        			}catch(IllegalAccessException e){
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
				            		tempEmailBccVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
			        			} catch (NoSuchFieldException e) {
			        				e.printStackTrace();
			        				int pos =e.toString().lastIndexOf(":");
				            		String errorField = e.toString().substring(pos);
				            		tempEmailBccVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
			        			}
			        		}
			        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
			        			if(sid==null){
			        				tempEmailBccVal =  "{ ErrorIN : You can,t use ServiceOrder table in email Bcc as module is CustomerFile }";
		        				}else{
				        			String fieldName = tableFieldnameArr[1];
				        			try{
					        			Class<?> cls = serviceOrder.getClass();
					        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
					        			modelFieldName.setAccessible(true);
										Object tempFieldValue =modelFieldName.get(serviceOrder);
										if(tempFieldValue!=null && !tempFieldValue.equals("")){
											if(!fieldName.trim().equalsIgnoreCase("estimator") && !fieldName.trim().equalsIgnoreCase("salesMan") && !fieldName.trim().equalsIgnoreCase("coordinator")){
												if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
													tempEmailBccVal = tempFieldValue+"";
												}else{
													tempEmailBccVal = tempEmailBccVal+","+tempFieldValue+"";
												}
											}else{
												userNew = userManager.getUserByUsername(tempFieldValue+"");
							        			Class<?> cls1 = userNew.getClass();
							        			Field usrModelFieldName = cls1.getDeclaredField("email");
							        			usrModelFieldName.setAccessible(true);
												Object usrFieldValue =usrModelFieldName.get(userNew);
												if(usrFieldValue!=null && !usrFieldValue.equals("")){
													if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
														tempEmailBccVal = usrFieldValue+"";
													}else{
														tempEmailBccVal = tempEmailBccVal+","+usrFieldValue+"";
													}
												}
											}
										}
				        			}catch(IllegalAccessException e){
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailBccVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
				        			} catch (NoSuchFieldException e) {
				        				e.printStackTrace();
				        				int pos =e.toString().lastIndexOf(":");
					            		String errorField = e.toString().substring(pos);
					            		tempEmailBccVal =  "{ ErrorIN "+ errorField+" in "+tableName+" }";
				        			}
				        		}
				        	}
			        	}
					}else{
						if(splittedEmailBcc!=null && !splittedEmailBcc.equalsIgnoreCase("")){
							if(tempEmailBccVal== null || tempEmailBccVal.equalsIgnoreCase("")){
								tempEmailBccVal  = splittedEmailBcc;
							}else{
								tempEmailBccVal  = tempEmailBccVal+","+splittedEmailBcc;
							}
						}
					}
				}
				emailBcc = tempEmailBccVal;
			}
			emailBccEditedValue = emailBcc;
			
			String subject = "";
			String subjectTemp = "";
			subjectTemp = emailSetupTemplate.getEmailSubject();
			String subjectEmptyTokenizer="";
			StringBuilder emailSubjectValue = new StringBuilder();
			if(subjectTemp.contains("^$")){
				StringTokenizer subjectTokenizer = new StringTokenizer(subjectTemp);
		        while (subjectTokenizer.hasMoreTokens()) {
		        	subjectEmptyTokenizer = subjectTokenizer.nextToken();
		        	if(subjectEmptyTokenizer.contains("^$")){
		        		String[] splittedValue = subjectEmptyTokenizer.split(Pattern.quote("^$"));
						for (String splittedWord : splittedValue){
							if(splittedWord.contains("$^") && splittedWord.contains(".")){
								String[] splittedValueNew = splittedWord.split(Pattern.quote("$^"));
								  for (String splittedWordNew : splittedValueNew){
										String tempTableAndFieldname = splittedWordNew.replaceAll("\\^\\$", "");
							        	if(tempTableAndFieldname.contains(".") && (tempTableAndFieldname.toLowerCase().contains("customerfile") || tempTableAndFieldname.toLowerCase().contains("serviceorder") || tempTableAndFieldname.toLowerCase().contains("trackingstatus"))){
							        		String [] tableFieldnameArr = tempTableAndFieldname.split("\\.");
							        		String tableName = tableFieldnameArr[0];
							        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
							        			String fieldName = tableFieldnameArr[1];
							        			try{
								        			Class<?> cls = customerFile.getClass();
								        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
								        			Object fieldType = modelFieldName.getType();
								        			modelFieldName.setAccessible(true);
													Object tempFieldValue =modelFieldName.get(customerFile);
													if(tempFieldValue!=null && !tempFieldValue.equals("")){
														if(fieldType.toString().equals("class java.util.Date")){
															SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
															Date tempDateValue = (Date) tempFieldValue;
												    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
												    		String dateValue = nowYYYYMMDD1.toString();
												    		emailSubjectValue.append(dateValue+" ");
														}else{
															emailSubjectValue.append(tempFieldValue+" ");
														}
													}
							        			}catch(IllegalAccessException e){
							        				e.printStackTrace();
							        				int pos =e.toString().lastIndexOf(":");
								            		String errorField = e.toString().substring(pos);
								            		emailSubjectValue.append("{ ErrorIN "+ errorField+" in "+tableName+" }");
							        			} catch (NoSuchFieldException e) {
							        				e.printStackTrace();
							        				int pos =e.toString().lastIndexOf(":");
								            		String errorField = e.toString().substring(pos);
								            		emailSubjectValue.append("{ ErrorIN "+ errorField+" in "+tableName+" }");
							        			}
							        		}
							        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
							        			if(sid==null){
							        				emailSubjectValue.append( "{ ErrorIN : You can,t use ServiceOrder table as module is CustomerFile }" );
							        			}else{
								        			String fieldName = tableFieldnameArr[1];
								        			try{
									        			Class<?> cls = serviceOrder.getClass();
									        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
									        			Object fieldType = modelFieldName.getType();
									        			modelFieldName.setAccessible(true);
														Object tempFieldValue =modelFieldName.get(serviceOrder);
														if(tempFieldValue!=null && !tempFieldValue.equals("")){
															if(fieldType.toString().equals("class java.util.Date")){
																SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																Date tempDateValue = (Date) tempFieldValue;
													    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
													    		String dateValue = nowYYYYMMDD1.toString();
													    		emailSubjectValue.append(dateValue+" ");
															}else{
																emailSubjectValue.append(tempFieldValue+" ");
															}
														}
								        			}catch(IllegalAccessException e){
								        				e.printStackTrace();
								        				int pos =e.toString().lastIndexOf(":");
									            		String errorField = e.toString().substring(pos);
									            		emailSubjectValue.append("{ ErrorIN "+ errorField+" in "+tableName+" }");
								        			} catch (NoSuchFieldException e) {
								        				e.printStackTrace();
								        				int pos =e.toString().lastIndexOf(":");
									            		String errorField = e.toString().substring(pos);
									            		emailSubjectValue.append("{ ErrorIN "+ errorField+" in "+tableName+" }");
								        			}
							        			}
							        		}
							        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
							        			if(sid==null){
							        				emailSubjectValue.append( "{ ErrorIN : You can,t use TrackingStatus table as module is CustomerFile }" );
							        			}else{
								        			String fieldName = tableFieldnameArr[1];
								        			try{
									        			Class<?> cls = trackingStatus.getClass();
									        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
									        			Object fieldType = modelFieldName.getType();
									        			modelFieldName.setAccessible(true);
														Object tempFieldValue =modelFieldName.get(trackingStatus);
														if(tempFieldValue!=null && !tempFieldValue.equals("")){
															if(fieldType.toString().equals("class java.util.Date")){
																SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																Date tempDateValue = (Date) tempFieldValue;
													    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
													    		String dateValue = nowYYYYMMDD1.toString();
													    		emailSubjectValue.append(dateValue+" ");
															}else{
																emailSubjectValue.append(tempFieldValue+" ");
															}
														}
								        			}catch(IllegalAccessException e){
								        				e.printStackTrace();
								        				int pos =e.toString().lastIndexOf(":");
									            		String errorField = e.toString().substring(pos);
									            		emailSubjectValue.append("{ ErrorIN "+ errorField+" in "+tableName+" }");
								        			} catch (NoSuchFieldException e) {
								        				e.printStackTrace();
								        				int pos =e.toString().lastIndexOf(":");
									            		String errorField = e.toString().substring(pos);
									            		emailSubjectValue.append("{ ErrorIN "+ errorField+" in "+tableName+" }");
								        			}
							        			}
							        		}
							        	}else{
							        		emailSubjectValue.append(tempTableAndFieldname+" ");
							        	}
							  		}
								}else{
									if(splittedWord!=null && !splittedWord.equalsIgnoreCase("")){
										emailSubjectValue.append(splittedWord+" ");
									}
								}
							}
						}else{
				        	emailSubjectValue.append(subjectEmptyTokenizer+" ");
				        }
		        	}
			}else{
				emailSubjectValue.append(subjectTemp+" ");
			}
			subject = emailSubjectValue.toString();
			emailSubjectEditedValue = subject;
			
			String emailBody ="";
			emailBody = emailSetupTemplate.getEmailBody();
			if(emailBody!=null && !emailBody.equalsIgnoreCase("")){
				emailBody = emailBody.replaceAll("<br />", "<br/>");
				StringBuilder emailBodyTempValue = new StringBuilder();
				String str;
				BufferedReader reader = new BufferedReader(new StringReader(emailBody));
				String bodyEmptyTokenizer="";
				try {       
					while((str = reader.readLine()) != null) {
						if(str.contains("^$")){
							  StringTokenizer bodyTokenizer = new StringTokenizer(str);
							  while (bodyTokenizer.hasMoreTokens()) {
								  bodyEmptyTokenizer = bodyTokenizer.nextToken();
								  if(bodyEmptyTokenizer.contains("^$")){
									String[] splittedValue = bodyEmptyTokenizer.split(Pattern.quote("^$"));
									for (String splittedWord : splittedValue){
									if(splittedWord.contains("$^") && splittedWord.contains(".") && splittedWord.contains("~")){
										String modelFieldValue = "";
										String[] splittedValueByTild = splittedWord.split(Pattern.quote("~"));
										for(String splittedWordByTild : splittedValueByTild){
											if(splittedWordByTild.contains("$^")){
												String[] splittWordArr = splittedWordByTild.split(Pattern.quote("$^"));
												for(String splittWordVal : splittWordArr){
													if(splittWordVal.contains(".")){
														String [] tableFieldNameArr = splittWordVal.split("\\.");
														if(tableFieldNameArr.length!=0){
										        		String tableName = tableFieldNameArr[0];
										        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("user") && !modelFieldValue.equalsIgnoreCase("")){
										        			String tempFieldName = tableFieldNameArr[1];
										        			try{
											        			userNew = userManager.getUserByUsername(modelFieldValue);
											        			Class<?> cls = userNew.getClass();
											        			Field modelFieldName = cls.getDeclaredField(tempFieldName.trim());
											        			Object fieldType = modelFieldName.getType();
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(userNew);
																if(tempFieldValue!=null && !tempFieldValue.equals("")){
																	if(fieldType.toString().equals("class java.util.Date")){
																		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																		Date tempDateValue = (Date) tempFieldValue;
															    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
															    		String dateValue = nowYYYYMMDD1.toString();
																		emailBodyTempValue.append(dateValue+" ");
																	}else{
																		emailBodyTempValue.append(tempFieldValue+" ");
																	}
																}
											        		}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
											            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
											            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
										        			}
										        		}
										        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("partnerPublic") && !modelFieldValue.equalsIgnoreCase("")){
										        			String tempFieldName = tableFieldNameArr[1];
										        			try{
											        			partnerPublicObj = partnerPublicManager.getPartnerByCode(modelFieldValue);
											        			Class<?> cls = partnerPublicObj.getClass();
											        			Field modelFieldName = cls.getDeclaredField(tempFieldName.trim());
											        			Object fieldType = modelFieldName.getType();
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(partnerPublicObj);
																if(tempFieldValue!=null && !tempFieldValue.equals("")){
																	if(fieldType.toString().equals("class java.util.Date")){
																		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																		Date tempDateValue = (Date) tempFieldValue;
															    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
															    		String dateValue = nowYYYYMMDD1.toString();
																		emailBodyTempValue.append(dateValue+" ");
																	}else{
																		emailBodyTempValue.append(tempFieldValue+" ");
																	}
																}
											        		}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
											            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
											            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
										        			}
										        		}
													}
													}else{
														emailBodyTempValue.append(splittWordVal+" ");
													}
												}
											}else{
												String tempTableAndFieldname = splittedWordByTild.replaceAll("\\^\\$", "");
									        	if(tempTableAndFieldname.contains(".")){
									        		String [] tableFieldnameArr = tempTableAndFieldname.split("\\.");
									        		if(tableFieldnameArr.length!=0){
									        		String tableName = tableFieldnameArr[0];
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
									        			String fieldName = tableFieldnameArr[1];
									        			try{
										        			Class<?> cls = customerFile.getClass();
										        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
										        			modelFieldName.setAccessible(true);
															Object tempFieldValue =modelFieldName.get(customerFile);
															modelFieldValue =tempFieldValue+""; 
									        			}catch(IllegalAccessException e){
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
										            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
									        			} catch (NoSuchFieldException e) {
									        				e.printStackTrace();
									        				int pos =e.toString().lastIndexOf(":");
										            		String errorField = e.toString().substring(pos);
										            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
									        			}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
									        			if(sid==null){
									        				emailBodyTempValue.append( "<strong><font color=#ff0000>{ ErrorIN : You can,t use ServiceOrder table as module is CustomerFile }</font></strong>" );
									        			}else{
										        			String fieldName = tableFieldnameArr[1];
										        			try{
											        			Class<?> cls = serviceOrder.getClass();
											        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(serviceOrder);
																modelFieldValue =tempFieldValue+"";
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
											            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
											            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
										        			}
									        			}
									        		}
									        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
									        			if(sid==null){
									        				emailBodyTempValue.append( "<strong><font color=#ff0000>{ ErrorIN : You can,t use TrackingStatus table as module is CustomerFile }</font></strong>" );
									        			}else{
										        			String fieldName = tableFieldnameArr[1];
										        			try{
											        			Class<?> cls = trackingStatus.getClass();
											        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
											        			modelFieldName.setAccessible(true);
																Object tempFieldValue =modelFieldName.get(trackingStatus);
																modelFieldValue =tempFieldValue+""; 
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
											            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
											            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
										        			}
									        			}
									        		}
									        	}
									        	}else{
									        		emailBodyTempValue.append(tempTableAndFieldname+" ");
									        	}
											}
									}	
									}else if(splittedWord.contains("$^") && splittedWord.contains(".")){
										String[] splittedValueNew = splittedWord.split(Pattern.quote("$^"));
										  for (String splittedWordNew : splittedValueNew){
													String tempTableAndFieldname = splittedWordNew.replaceAll("\\^\\$", "");
										        	if(tempTableAndFieldname.contains(".")){
										        		String [] tableFieldnameArr = tempTableAndFieldname.split("\\.");
										        		if(tableFieldnameArr.length!=0){
										        		String tableName = tableFieldnameArr[0];
										        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("customerFile")){
										        			String fieldName = tableFieldnameArr[1];
										        			try{
										        				if(!fieldName.trim().equalsIgnoreCase("prefix")){
												        			Class<?> cls = customerFile.getClass();
												        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
												        			Object fieldType = modelFieldName.getType();
												        			modelFieldName.setAccessible(true);
																	Object tempFieldValue =modelFieldName.get(customerFile);
																	if(tempFieldValue!=null && !tempFieldValue.equals("")){
																		if(fieldType.toString().equals("class java.util.Date")){
																			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																			Date tempDateValue = (Date) tempFieldValue;
																    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
																    		String dateValue = nowYYYYMMDD1.toString();
																			emailBodyTempValue.append(dateValue+" ");
																		}else{
																			emailBodyTempValue.append(tempFieldValue+" ");
																		}
																	}
										        				}else{
										        					String preffixValue ="";
										       					 	if((customerFile.getPrefix()!=null && !customerFile.getPrefix().equalsIgnoreCase("")) && (customerFile.getCustomerLanguagePreference()!=null && !customerFile.getCustomerLanguagePreference().equalsIgnoreCase(""))){
										       					 		preffixValue = customerFileManager.getPreffixFromRefmaster("PREFFIX",customerFile.getCustomerLanguagePreference(), customerFile.getPrefix());
										       							emailBodyTempValue.append(preffixValue+" ");
										       					 	}
										        				}
										        			}catch(IllegalAccessException e){
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
											            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
										        			} catch (NoSuchFieldException e) {
										        				e.printStackTrace();
										        				int pos =e.toString().lastIndexOf(":");
											            		String errorField = e.toString().substring(pos);
											            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
										        			}
										        		}
										        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("serviceOrder")){
										        			if(sid==null){
										        				emailBodyTempValue.append( "<strong><font color=#ff0000>{ ErrorIN : You can,t use ServiceOrder table as module is CustomerFile }</font></strong>" );
										        			}else{
											        			String fieldName = tableFieldnameArr[1];
											        			try{
											        				if(!fieldName.trim().equalsIgnoreCase("prefix")){
													        			Class<?> cls = serviceOrder.getClass();
													        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
													        			Object fieldType = modelFieldName.getType();
													        			modelFieldName.setAccessible(true);
																		Object tempFieldValue =modelFieldName.get(serviceOrder);
																		if(tempFieldValue!=null && !tempFieldValue.equals("")){
																			if(fieldType.toString().equals("class java.util.Date")){
																				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																				Date tempDateValue = (Date) tempFieldValue;
																	    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
																	    		String dateValue = nowYYYYMMDD1.toString();
																				emailBodyTempValue.append(dateValue+" ");
																			}else{
																				emailBodyTempValue.append(tempFieldValue+" ");
																			}
																		}
											        				}else{
											        					String preffixValue ="";
											       					 	if((customerFile.getPrefix()!=null && !customerFile.getPrefix().equalsIgnoreCase("")) && (customerFile.getCustomerLanguagePreference()!=null && !customerFile.getCustomerLanguagePreference().equalsIgnoreCase(""))){
											       					 		preffixValue = customerFileManager.getPreffixFromRefmaster("PREFFIX",customerFile.getCustomerLanguagePreference(), customerFile.getPrefix());
											       							emailBodyTempValue.append(preffixValue+" ");
											       					 	}
											        				}
											        			}catch(IllegalAccessException e){
											        				e.printStackTrace();
											        				int pos =e.toString().lastIndexOf(":");
												            		String errorField = e.toString().substring(pos);
												            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
											        			} catch (NoSuchFieldException e) {
											        				e.printStackTrace();
											        				int pos =e.toString().lastIndexOf(":");
												            		String errorField = e.toString().substring(pos);
												            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
											        			}
											        		}
										        		}
										        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("trackingStatus")){
										        			if(sid==null){
										        				emailBodyTempValue.append( "<strong><font color=#ff0000>{ ErrorIN : You can,t use TrackingStatus table as module is CustomerFile }</font></strong>" );
										        			}else{
											        			String fieldName = tableFieldnameArr[1];
											        			try{
												        			Class<?> cls = trackingStatus.getClass();
												        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
												        			Object fieldType = modelFieldName.getType();
												        			modelFieldName.setAccessible(true);
																	Object tempFieldValue =modelFieldName.get(trackingStatus);
																	if(tempFieldValue!=null && !tempFieldValue.equals("")){
																		if(fieldType.toString().equals("class java.util.Date")){
																			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																			Date tempDateValue = (Date) tempFieldValue;
																    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
																    		String dateValue = nowYYYYMMDD1.toString();
																			emailBodyTempValue.append(dateValue+" ");
																		}else{
																			emailBodyTempValue.append(tempFieldValue+" ");
																		}
																	}
											        			}catch(IllegalAccessException e){
											        				e.printStackTrace();
											        				int pos =e.toString().lastIndexOf(":");
												            		String errorField = e.toString().substring(pos);
												            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
											        			} catch (NoSuchFieldException e) {
											        				e.printStackTrace();
											        				int pos =e.toString().lastIndexOf(":");
												            		String errorField = e.toString().substring(pos);
												            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
											        			}
											        		}
										        		}
										        		if((tableName!=null && !tableName.equalsIgnoreCase("")) && tableName.equalsIgnoreCase("miscellaneous")){
										        			if(sid==null){
										        				emailBodyTempValue.append( "<strong><font color=#ff0000>{ ErrorIN : You can,t use Miscellaneous table as module is CustomerFile }</font></strong>" );
										        			}else{
											        			String fieldName = tableFieldnameArr[1];
											        			try{
												        			Class<?> cls = miscellaneous.getClass();
												        			Field modelFieldName = cls.getDeclaredField(fieldName.trim());
												        			Object fieldType = modelFieldName.getType();
												        			modelFieldName.setAccessible(true);
																	Object tempFieldValue =modelFieldName.get(miscellaneous);
																	if(tempFieldValue!=null && !tempFieldValue.equals("")){
																		if(fieldType.toString().equals("class java.util.Date")){
																			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
																			Date tempDateValue = (Date) tempFieldValue;
																    		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(tempDateValue));
																    		String dateValue = nowYYYYMMDD1.toString();
																			emailBodyTempValue.append(dateValue+" ");
																		}else{
																			emailBodyTempValue.append(tempFieldValue+" ");
																		}
																	}
											        			}catch(IllegalAccessException e){
											        				e.printStackTrace();
											        				int pos =e.toString().lastIndexOf(":");
												            		String errorField = e.toString().substring(pos);
												            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
											        			} catch (NoSuchFieldException e) {
											        				e.printStackTrace();
											        				int pos =e.toString().lastIndexOf(":");
												            		String errorField = e.toString().substring(pos);
												            		emailBodyTempValue.append("<strong><font color=#ff0000>{ ErrorIN "+ errorField+" in "+tableName+" }</font></strong>");
											        			}
											        		}
										        		}
										        	}
									        	}else{
									        		emailBodyTempValue.append(tempTableAndFieldname+" ");
									        	}
										  }
										}else{
											emailBodyTempValue.append(splittedWord+"");
										}
									}
								  }else{
									  emailBodyTempValue.append(bodyEmptyTokenizer+" ");
								  }
							  }
						}else{
							emailBodyTempValue.append(str+" ");
						}
					}
				} catch(IOException e) {
					e.printStackTrace();
				}
				emailBodyEditedValue = emailBodyTempValue+"";
			}else{
				emailBodyEditedValue = emailBody;
			}
			Long StartTime = System.currentTimeMillis();
			String noFileFoundList = "";
			String fileName="";
			if ((emailSetupTemplate.getAttachedFileLocation() != null) && (!emailSetupTemplate.getAttachedFileLocation().trim().equalsIgnoreCase(""))) {
				for(String str:emailSetupTemplate.getAttachedFileLocation().split("~")){
					File attachedFile = new File(str);
					String tempFileName = str.substring(str.lastIndexOf("/")+1);
					if(attachedFile.exists()){
						if(fileName!=null && !fileName.equalsIgnoreCase("") && (tempFileName!=null && !tempFileName.equalsIgnoreCase(""))){
		        			fileName = fileName+"^"+"File Template : "+str.substring(str.lastIndexOf("/")+1);
		        		}else{
		        			if(tempFileName!=null && !tempFileName.equalsIgnoreCase("")){
		        				fileName ="File Template : "+ str.substring(str.lastIndexOf("/")+1);
		        			}
		        		}
					}else if(!attachedFile.exists()){
						if(noFileFoundList!=null && !noFileFoundList.equalsIgnoreCase("")){
							noFileFoundList = noFileFoundList+"^"+"File Template : "+(str.substring(str.lastIndexOf("/")+1));
		        		}else{
		        			noFileFoundList = "File Template : "+(str.substring(str.lastIndexOf("/")+1));
		        		}
					}
	        	}
	        }
			
			//String filesFromFileCabinet = "";
			if(cid!=null && emailSetupTemplate.getFileTypeOfFileCabinet()!=null && !emailSetupTemplate.getFileTypeOfFileCabinet().equalsIgnoreCase("")){
				fileCabinetListByDocumentTpye = emailSetupTemplateManager.findDocsFromFileCabinet(emailSetupTemplate.getFileTypeOfFileCabinet(),customerFile.getSequenceNumber(),sessionCorpID);
			}else if(sid!=null && emailSetupTemplate.getFileTypeOfFileCabinet()!=null && !emailSetupTemplate.getFileTypeOfFileCabinet().equalsIgnoreCase("")){
				fileCabinetListByDocumentTpye = emailSetupTemplateManager.findDocsFromFileCabinet(emailSetupTemplate.getFileTypeOfFileCabinet(),serviceOrder.getShipNumber(),sessionCorpID);
			}
			
			String fileCabinetFile = "";
			if(fileCabinetListByDocumentTpye!=null && !fileCabinetListByDocumentTpye.equalsIgnoreCase("")){
				for(String str:fileCabinetListByDocumentTpye.split("~")){
					File fileCabinetF = new File(str);
					if(fileCabinetF.exists()){
		        		if(fileCabinetFile!=null && !fileCabinetFile.equalsIgnoreCase("")){
			        		if(!str.contains("C:")){
			        			fileCabinetFile = fileCabinetFile+"^"+"File Cabinet : "+emailSetupTemplate.getFileTypeOfFileCabinet()+" - "+str.substring(str.lastIndexOf("/")+1);
		        			}else{
		        				fileCabinetFile = fileCabinetFile+"^"+"File Cabinet : "+emailSetupTemplate.getFileTypeOfFileCabinet()+" - "+str.substring(str.lastIndexOf("\\")+1);
		        			}
		        		}else{
		        			if(!str.contains("C:")){
		        				fileCabinetFile ="File Cabinet : "+emailSetupTemplate.getFileTypeOfFileCabinet()+" - "+(str.substring(str.lastIndexOf("/")+1));
		        			}else{
		        				fileCabinetFile ="File Cabinet : "+emailSetupTemplate.getFileTypeOfFileCabinet()+" - "+(str.substring(str.lastIndexOf("\\")+1));
		        			}
						}
					}else if(!fileCabinetF.exists()){
						if(noFileFoundList!=null && !noFileFoundList.equalsIgnoreCase("")){
			        		if(!str.contains("C:")){
			        			noFileFoundList = noFileFoundList+"^"+"File Cabinet : "+emailSetupTemplate.getFileTypeOfFileCabinet()+" - "+(str.substring(str.lastIndexOf("/")+1));
		        			}else{
		        				noFileFoundList = noFileFoundList+"^"+"File Cabinet : "+emailSetupTemplate.getFileTypeOfFileCabinet()+" - "+(str.substring(str.lastIndexOf("\\")+1));
		        			}
		        		}else{
		        			if(!str.contains("C:")){
		        				noFileFoundList ="File Cabinet : "+emailSetupTemplate.getFileTypeOfFileCabinet()+" - "+(str.substring(str.lastIndexOf("/")+1));
		        			}else{
		        				noFileFoundList ="File Cabinet : "+emailSetupTemplate.getFileTypeOfFileCabinet()+" - "+(str.substring(str.lastIndexOf("\\")+1));
		        			}
						}
					}
				}
			}else{
				if(emailSetupTemplate.getFileTypeOfFileCabinet()!=null && !emailSetupTemplate.getFileTypeOfFileCabinet().equalsIgnoreCase("")){
					String doubleQuote = "-";
					if(noFileFoundList!=null && !noFileFoundList.equalsIgnoreCase("")){
						noFileFoundList = noFileFoundList+"^"+"No File found for Document Type "+doubleQuote+" "+emailSetupTemplate.getFileTypeOfFileCabinet();
	        		}else{
	        			noFileFoundList =  "No File found for Document Type "+doubleQuote+" "+emailSetupTemplate.getFileTypeOfFileCabinet();
	        		}
				}
			}
			if(!fileCabinetFile.equalsIgnoreCase("")){
				fileName = fileName +"^"+ fileCabinetFile;
			}
			String jrxmlFileNameList = "";
			if(emailSetupTemplate.getFormsId()!=null && !emailSetupTemplate.getFormsId().equalsIgnoreCase("")){
				String uploadFilePath="";
				Map parameters = new HashMap();
				if(sid!=null){
					parameters.put("Service Order Number", serviceOrder.getShipNumber());
				}
				if(cid!=null){
					parameters.put("Customer File Number", customerFile.getSequenceNumber());
				}
				parameters.put("Corporate ID", sessionCorpID);
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
				uploadDir = uploadDir.replace(uploadDir,"/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + "EmailSetupTemplate" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
		
				byte[] output;
				if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
					Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
					parameters.put(JRParameter.REPORT_LOCALE, locale);
				} else {
					Locale locale = new Locale("en", "US");
					parameters.put(JRParameter.REPORT_LOCALE, locale);
				}
				List<String> files = new ArrayList<String>();
				String filePath="";
				JasperPrint jasperPrint = new JasperPrint();
				HttpServletResponse response = getResponse();
				ServletOutputStream ouputStream = response.getOutputStream();
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				String[] reportId = emailSetupTemplate.getFormsId().split(",");
				
				for (int i=0;i<reportId.length;i++) {
					try{
						Random rand = new Random(); 
						jasperPrint = reportsManager.generateReport(Long.parseLong(reportId[i]), parameters);
						reports = reportsManager.get(Long.parseLong(reportId[i]));
						String temp = reports.getReportName();
						String jrxmlFileName = temp.substring(0, temp.indexOf("."));
						if(reports.getEnabled().equalsIgnoreCase("Yes")){
							if((reports.getPdf()!=null && !reports.getPdf().equals("")) && reports.getPdf().equalsIgnoreCase("true")){
									response.setContentType("application/pdf");
									response.setHeader("Content-Disposition", "attachment; filename=" + jrxmlFileName + ".pdf");
									output = JasperExportManager.exportReportToPdf(jasperPrint);
									
									File dirPath = new File(uploadDir);
									if (!dirPath.exists()) {
										dirPath.mkdirs();
									}			
									String tempFileFileName = jrxmlFileName;
									String fileFileName = tempFileFileName+".pdf";
									files.add(uploadDir + fileFileName);
									File newFile = new File(uploadDir + fileFileName);
									filePath =(uploadDir+fileFileName );
									if (newFile.exists()) {
										newFile.delete();
									}
									if(jrxmlFileNameList!=null && !jrxmlFileNameList.equalsIgnoreCase("")){
										jrxmlFileNameList=jrxmlFileNameList+"^"+"JRXML : "+filePath.substring(filePath.lastIndexOf("/")+1);
							        }else{
							        	jrxmlFileNameList="JRXML : "+filePath.substring(filePath.lastIndexOf("/")+1);
							        }
									
									FileOutputStream fos = new FileOutputStream(newFile);
									DataOutputStream bos =  new DataOutputStream(fos);
									bos.write(output);
									bos.flush();
									bos.close();
							}
							if((reports.getDocx()!=null && !reports.getDocx().equals("")) && reports.getDocx().equalsIgnoreCase("true")){
									Random rand1 = new Random(); 
									response.setContentType("application/docx");
								    response.setHeader("Content-Disposition", "attachment; filename="+ jrxmlFileName +".docx");
									
								    JRDocxExporter exporterDocx = new JRDocxExporter();
									exporterDocx.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
									exporterDocx.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
									exporterDocx.setParameter(JRDocxExporterParameter.FLEXIBLE_ROW_HEIGHT, Boolean.TRUE);
									exporterDocx.exportReport();
									output = byteArrayOutputStream.toByteArray();
									
									File dirPath = new File(uploadDir);
									if (!dirPath.exists()) {
										dirPath.mkdirs();
									}			
									String tempFileFileName = jrxmlFileName;
									String fileFileName = tempFileFileName+".docx";
									files.add(uploadDir + fileFileName);
									File newFile = new File(uploadDir + fileFileName);
									filePath =(uploadDir+fileFileName );
									if (newFile.exists()) {
										newFile.delete();
									}
									if(jrxmlFileNameList!=null && !jrxmlFileNameList.equalsIgnoreCase("")){
										jrxmlFileNameList=jrxmlFileNameList+"^"+"JRXML : "+filePath.substring(filePath.lastIndexOf("/")+1);
							        }else{
							        	jrxmlFileNameList="JRXML : "+filePath.substring(filePath.lastIndexOf("/")+1);
							        }
									
									FileOutputStream fos = new FileOutputStream(newFile);
									fos.write(output);
									fos.flush();
									fos.close();
							}
							if((reports.getPdf()==null || reports.getPdf().equals("") || reports.getPdf().equalsIgnoreCase("false")) && (reports.getDocx()==null || reports.getDocx().equals("") || reports.getDocx().equalsIgnoreCase("false"))){
								if(noFileFoundList!=null && !noFileFoundList.equalsIgnoreCase("")){
									noFileFoundList = noFileFoundList+"^"+"JRXML : "+temp;
				        		}else{
				        			noFileFoundList = "JRXML : "+temp;
				        		}
							}
						}else{
							if(noFileFoundList!=null && !noFileFoundList.equalsIgnoreCase("")){
								noFileFoundList = noFileFoundList+"^"+"JRXML : "+temp;
			        		}else{
			        			noFileFoundList = "JRXML : "+temp;
			        		}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			if(!jrxmlFileNameList.equalsIgnoreCase("")){
				fileName = fileName +"^"+ jrxmlFileNameList;
			}
			attachedFileNameListEdit = Arrays.asList(fileName.split("^"));
			noFileFoundListEdit = Arrays.asList(noFileFoundList.split("^"));
			if(cid!=null){
				allFileFromFileCabinetList = emailSetupTemplateManager.findAllDocsFromFileCabinet(emailSetupTemplate.getFileTypeOfFileCabinet(),customerFile.getSequenceNumber(),sessionCorpID);
			}else if(sid!=null){
				allFileFromFileCabinetList = emailSetupTemplateManager.findAllDocsFromFileCabinet(emailSetupTemplate.getFileTypeOfFileCabinet(),serviceOrder.getShipNumber(),sessionCorpID);
			}
			Long timeTaken =  System.currentTimeMillis() - StartTime;
			logger.warn("\n\n\n\n\nTime taken For Adding All Attachements in EmailSetupTemplateEdit() "+timeTaken+"\n\n\n\n\n");
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" editEmailPopupFromEmailTemplate() End");
		return SUCCESS;
	}
	private String fileCabinetFilesByDocumentType; 
	private String fileCabinetListByDocumentTpye;
	private List allFileFromFileCabinetList = new ArrayList();
	private List noFileFoundListEdit = new ArrayList();
	private List attachedFileNameListEdit = new ArrayList();
	private String fileTypeName;
	public String generateFileFromJrxml() throws Exception{
		
		Map parameters = new HashMap();
		if(sid!=null){
			serviceOrder = serviceOrderManager.get(sid);
			parameters.put("Service Order Number", serviceOrder.getShipNumber());
		}
		if(cid!=null){
			customerFile = customerFileManager.get(cid);
			parameters.put("Customer File Number", customerFile.getSequenceNumber());
		}
		parameters.put("Corporate ID", sessionCorpID);
		if(!(company.getLocaleLanguage()==null || company.getLocaleLanguage().equalsIgnoreCase("")) && !(company.getLocaleCountry()==null || company.getLocaleCountry().equalsIgnoreCase(""))) {
			Locale locale = new Locale(company.getLocaleLanguage(), company.getLocaleCountry());
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		} else {
			Locale locale = new Locale("en", "US");
			parameters.put(JRParameter.REPORT_LOCALE, locale);
		}
			
	 	byte[] output;
	 	JasperPrint jasperPrint = new JasperPrint();
		HttpServletResponse response = getResponse();
		ServletOutputStream ouputStream = response.getOutputStream();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		
		jasperPrint = reportsManager.generateReport(Long.parseLong(reportsId), parameters);
		reports = reportsManager.get(Long.parseLong(reportsId));
		String temp = reports.getReportName();
		String fileName = temp.substring(0, temp.indexOf("."));
		try{
			if(fileTypeName!=null && !fileTypeName.equals("") && fileTypeName.equalsIgnoreCase("PDF")){
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", "attachment; filename="+fileName+".pdf");
				
				output = JasperExportManager.exportReportToPdf(jasperPrint);
				ouputStream.write(output);
			}
			if(fileTypeName!=null && !fileTypeName.equals("") && fileTypeName.equalsIgnoreCase("DOCX")){
				response.setContentType("application/docx");
			    response.setHeader("Content-Disposition", "attachment; filename="+ fileName +".docx");
				
			    JRDocxExporter exporterDocx = new JRDocxExporter();
				exporterDocx.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporterDocx.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
				exporterDocx.setParameter(JRDocxExporterParameter.FLEXIBLE_ROW_HEIGHT, Boolean.TRUE);
				exporterDocx.exportReport();
				ouputStream.write(byteArrayOutputStream.toByteArray());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		ouputStream.flush();
		ouputStream.close();
		return null;
	}
	
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setEmailSetupTemplateManager(
			EmailSetupTemplateManager emailSetupTemplateManager) {
		this.emailSetupTemplateManager = emailSetupTemplateManager;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EmailSetupTemplate getEmailSetupTemplate() {
		return emailSetupTemplate;
	}

	public void setEmailSetupTemplate(EmailSetupTemplate emailSetupTemplate) {
		this.emailSetupTemplate = emailSetupTemplate;
	}

	public Map<String, String> getRoutingList() {
		return routingList;
	}

	public void setRoutingList(Map<String, String> routingList) {
		this.routingList = routingList;
	}

	public Map<String, String> getServiceList() {
		return serviceList;
	}

	public void setServiceList(Map<String, String> serviceList) {
		this.serviceList = serviceList;
	}

	public Map<String, String> getJobsList() {
		return jobsList;
	}

	public void setJobsList(Map<String, String> jobsList) {
		this.jobsList = jobsList;
	}

	public Map<String, String> getModeList() {
		return modeList;
	}

	public void setModeList(Map<String, String> modeList) {
		this.modeList = modeList;
	}

	public Map<String, String> getModuleList() {
		return moduleList;
	}

	public void setModuleList(Map<String, String> moduleList) {
		this.moduleList = moduleList;
	}

	public Map<String, String> getSubModuleList() {
		return subModuleList;
	}

	public void setSubModuleList(Map<String, String> subModuleList) {
		this.subModuleList = subModuleList;
	}

	public Map<String, String> getMoveTypeList() {
		return moveTypeList;
	}

	public void setMoveTypeList(Map<String, String> moveTypeList) {
		this.moveTypeList = moveTypeList;
	}

	public Map<String, String> getLanguageList() {
		return languageList;
	}

	public void setLanguageList(Map<String, String> languageList) {
		this.languageList = languageList;
	}

	public List getCompanyDivisionList() {
		return companyDivisionList;
	}

	public void setCompanyDivisionList(List companyDivisionList) {
		this.companyDivisionList = companyDivisionList;
	}

	public List getEmailSetupTemplateList() {
		return emailSetupTemplateList;
	}

	public void setEmailSetupTemplateList(List emailSetupTemplateList) {
		this.emailSetupTemplateList = emailSetupTemplateList;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public Map<String, String> getDistributionMethodList() {
		return distributionMethodList;
	}

	public void setDistributionMethodList(Map<String, String> distributionMethodList) {
		this.distributionMethodList = distributionMethodList;
	}

	public File[] getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(File[] fileUpload) {
		this.fileUpload = fileUpload;
	}

	public String[] getFileUploadFileName() {
		return fileUploadFileName;
	}

	public void setFileUploadFileName(String[] fileUploadFileName) {
		this.fileUploadFileName = fileUploadFileName;
	}

	public String[] getFileUploadContentType() {
		return fileUploadContentType;
	}

	public void setFileUploadContentType(String[] fileUploadContentType) {
		this.fileUploadContentType = fileUploadContentType;
	}

	public String getEmailSetupTemplateURL() {
		return emailSetupTemplateURL;
	}

	public void setEmailSetupTemplateURL(String emailSetupTemplateURL) {
		this.emailSetupTemplateURL = emailSetupTemplateURL;
	}

	public String getJrxmlName() {
		return jrxmlName;
	}

	public void setJrxmlName(String jrxmlName) {
		this.jrxmlName = jrxmlName;
	}

	public List getFormsAutoCompleteList() {
		return formsAutoCompleteList;
	}

	public void setFormsAutoCompleteList(List formsAutoCompleteList) {
		this.formsAutoCompleteList = formsAutoCompleteList;
	}

	public void setReportsManager(ReportsManager reportsManager) {
		this.reportsManager = reportsManager;
	}

	public String getReportsId() {
		return reportsId;
	}

	public void setReportsId(String reportsId) {
		this.reportsId = reportsId;
	}

	public List getEmailSetupTemplateFormsList() {
		return emailSetupTemplateFormsList;
	}

	public void setEmailSetupTemplateFormsList(List emailSetupTemplateFormsList) {
		this.emailSetupTemplateFormsList = emailSetupTemplateFormsList;
	}

	public void setDocumentBundleManager(DocumentBundleManager documentBundleManager) {
		this.documentBundleManager = documentBundleManager;
	}

	public List<String> getAttachedFileList() {
		return attachedFileList;
	}

	public void setAttachedFileList(List<String> attachedFileList) {
		this.attachedFileList = attachedFileList;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Map<String, String> getDocsList() {
		return docsList;
	}

	public void setDocsList(Map<String, String> docsList) {
		this.docsList = docsList;
	}

	public Map<String, String> getTableWithColumnsNameList() {
		return tableWithColumnsNameList;
	}

	public void setTableWithColumnsNameList(
			Map<String, String> tableWithColumnsNameList) {
		this.tableWithColumnsNameList = tableWithColumnsNameList;
	}

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}

	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public String getMmValidation() {
		return mmValidation;
	}

	public void setMmValidation(String mmValidation) {
		this.mmValidation = mmValidation;
	}

	public String getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}

	public void setReturnAjaxStringValue(String returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}

	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

	public boolean isEmailTemplateActive() {
		return emailTemplateActive;
	}

	public void setEmailTemplateActive(boolean emailTemplateActive) {
		this.emailTemplateActive = emailTemplateActive;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public String getEmailBodyCurrentValue() {
		return emailBodyCurrentValue;
	}

	public void setEmailBodyCurrentValue(String emailBodyCurrentValue) {
		this.emailBodyCurrentValue = emailBodyCurrentValue;
	}

	public String getEmailToCurrentValue() {
		return emailToCurrentValue;
	}

	public void setEmailToCurrentValue(String emailToCurrentValue) {
		this.emailToCurrentValue = emailToCurrentValue;
	}

	public String getEmailSubjectCurrentValue() {
		return emailSubjectCurrentValue;
	}

	public void setEmailSubjectCurrentValue(String emailSubjectCurrentValue) {
		this.emailSubjectCurrentValue = emailSubjectCurrentValue;
	}

	public Long getIdCurrentValue() {
		return idCurrentValue;
	}

	public void setIdCurrentValue(Long idCurrentValue) {
		this.idCurrentValue = idCurrentValue;
	}

	public User getUserNew() {
		return userNew;
	}

	public void setUserNew(User userNew) {
		this.userNew = userNew;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public PartnerPublic getPartnerPublicObj() {
		return partnerPublicObj;
	}

	public void setPartnerPublicObj(PartnerPublic partnerPublicObj) {
		this.partnerPublicObj = partnerPublicObj;
	}

	public String getEmailSetupTemplateEditURL() {
		return emailSetupTemplateEditURL;
	}

	public void setEmailSetupTemplateEditURL(String emailSetupTemplateEditURL) {
		this.emailSetupTemplateEditURL = emailSetupTemplateEditURL;
	}

	public String getEmailFromEditedValue() {
		return emailFromEditedValue;
	}

	public void setEmailFromEditedValue(String emailFromEditedValue) {
		this.emailFromEditedValue = emailFromEditedValue;
	}

	public String getEmailToEditedValue() {
		return emailToEditedValue;
	}

	public void setEmailToEditedValue(String emailToEditedValue) {
		this.emailToEditedValue = emailToEditedValue;
	}

	public String getEmailCcEditedValue() {
		return emailCcEditedValue;
	}

	public void setEmailCcEditedValue(String emailCcEditedValue) {
		this.emailCcEditedValue = emailCcEditedValue;
	}

	public String getEmailBccEditedValue() {
		return emailBccEditedValue;
	}

	public void setEmailBccEditedValue(String emailBccEditedValue) {
		this.emailBccEditedValue = emailBccEditedValue;
	}

	public String getEmailSubjectEditedValue() {
		return emailSubjectEditedValue;
	}

	public void setEmailSubjectEditedValue(String emailSubjectEditedValue) {
		this.emailSubjectEditedValue = emailSubjectEditedValue;
	}

	public String getEmailBodyEditedValue() {
		return emailBodyEditedValue;
	}

	public void setEmailBodyEditedValue(String emailBodyEditedValue) {
		this.emailBodyEditedValue = emailBodyEditedValue;
	}

	public String getEditModalId() {
		return editModalId;
	}

	public void setEditModalId(String editModalId) {
		this.editModalId = editModalId;
	}

	public Integer getSizeOfEmailFrom() {
		return sizeOfEmailFrom;
	}

	public void setSizeOfEmailFrom(Integer sizeOfEmailFrom) {
		this.sizeOfEmailFrom = sizeOfEmailFrom;
	}

	public Integer getSizeOfEmailTo() {
		return sizeOfEmailTo;
	}

	public void setSizeOfEmailTo(Integer sizeOfEmailTo) {
		this.sizeOfEmailTo = sizeOfEmailTo;
	}

	public Integer getSizeOfEmailBcc() {
		return sizeOfEmailBcc;
	}

	public void setSizeOfEmailBcc(Integer sizeOfEmailBcc) {
		this.sizeOfEmailBcc = sizeOfEmailBcc;
	}

	public Integer getSizeOfEmailCc() {
		return sizeOfEmailCc;
	}

	public void setSizeOfEmailCc(Integer sizeOfEmailCc) {
		this.sizeOfEmailCc = sizeOfEmailCc;
	}

	public Integer getSizeOfEmailSubject() {
		return sizeOfEmailSubject;
	}

	public void setSizeOfEmailSubject(Integer sizeOfEmailSubject) {
		this.sizeOfEmailSubject = sizeOfEmailSubject;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public String getFileError() {
		return fileError;
	}

	public void setFileError(String fileError) {
		this.fileError = fileError;
	}

	public String getModuleFieldValue() {
		return moduleFieldValue;
	}

	public void setModuleFieldValue(String moduleFieldValue) {
		this.moduleFieldValue = moduleFieldValue;
	}

	public String getOthersFieldValue() {
		return othersFieldValue;
	}

	public void setOthersFieldValue(String othersFieldValue) {
		this.othersFieldValue = othersFieldValue;
	}

	public Reports getReports() {
		return reports;
	}

	public void setReports(Reports reports) {
		this.reports = reports;
	}

	public String getFileTypeName() {
		return fileTypeName;
	}

	public void setFileTypeName(String fileTypeName) {
		this.fileTypeName = fileTypeName;
	}

	public List getFileCabinetFileList() {
		return fileCabinetFileList;
	}

	public void setFileCabinetFileList(List fileCabinetFileList) {
		this.fileCabinetFileList = fileCabinetFileList;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getPageType() {
		return pageType;
	}

	public void setPageType(String pageType) {
		this.pageType = pageType;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public String getDescriptionName() {
		return descriptionName;
	}

	public void setDescriptionName(String descriptionName) {
		this.descriptionName = descriptionName;
	}

	public EmailSetupTemplate getCopyEmailSetupTemplate() {
		return copyEmailSetupTemplate;
	}

	public void setCopyEmailSetupTemplate(EmailSetupTemplate copyEmailSetupTemplate) {
		this.copyEmailSetupTemplate = copyEmailSetupTemplate;
	}

	public List getAttachedFileNameListEdit() {
		return attachedFileNameListEdit;
	}

	public void setAttachedFileNameListEdit(List attachedFileNameListEdit) {
		this.attachedFileNameListEdit = attachedFileNameListEdit;
	}

	public String getAttachedFileNameListEditVal() {
		return attachedFileNameListEditVal;
	}

	public void setAttachedFileNameListEditVal(String attachedFileNameListEditVal) {
		this.attachedFileNameListEditVal = attachedFileNameListEditVal;
	}

	public String getEmailCcCurrentValue() {
		return emailCcCurrentValue;
	}

	public void setEmailCcCurrentValue(String emailCcCurrentValue) {
		this.emailCcCurrentValue = emailCcCurrentValue;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public EmailSetup getEmailSetup() {
		return emailSetup;
	}

	public void setEmailSetup(EmailSetup emailSetup) {
		this.emailSetup = emailSetup;
	}

	public Long getLastSentMailId() {
		return lastSentMailId;
	}

	public void setLastSentMailId(Long lastSentMailId) {
		this.lastSentMailId = lastSentMailId;
	}

	public String getImageInBodyAttachement() {
		return imageInBodyAttachement;
	}

	public void setImageInBodyAttachement(String imageInBodyAttachement) {
		this.imageInBodyAttachement = imageInBodyAttachement;
	}

	public String getEmailBccCurrentValue() {
		return emailBccCurrentValue;
	}

	public void setEmailBccCurrentValue(String emailBccCurrentValue) {
		this.emailBccCurrentValue = emailBccCurrentValue;
	}

	public String getEmailFromCurrentValue() {
		return emailFromCurrentValue;
	}

	public void setEmailFromCurrentValue(String emailFromCurrentValue) {
		this.emailFromCurrentValue = emailFromCurrentValue;
	}

	public List getNoFileFoundListEdit() {
		return noFileFoundListEdit;
	}

	public void setNoFileFoundListEdit(List noFileFoundListEdit) {
		this.noFileFoundListEdit = noFileFoundListEdit;
	}

	public List getAllFileFromFileCabinetList() {
		return allFileFromFileCabinetList;
	}

	public void setAllFileFromFileCabinetList(List allFileFromFileCabinetList) {
		this.allFileFromFileCabinetList = allFileFromFileCabinetList;
	}

	public Long getfId() {
		return fId;
	}

	public void setfId(Long fId) {
		this.fId = fId;
	}

	public void setMyFileManager(MyFileManager myFileManager) {
		this.myFileManager = myFileManager;
	}

	public MyFile getMyFile() {
		return myFile;
	}

	public void setMyFile(MyFile myFile) {
		this.myFile = myFile;
	}

	public String getFileCabinetListByDocumentTpye() {
		return fileCabinetListByDocumentTpye;
	}

	public void setFileCabinetListByDocumentTpye(
			String fileCabinetListByDocumentTpye) {
		this.fileCabinetListByDocumentTpye = fileCabinetListByDocumentTpye;
	}

	public String getFileCabinetFilesByDocumentType() {
		return fileCabinetFilesByDocumentType;
	}

	public void setFileCabinetFilesByDocumentType(
			String fileCabinetFilesByDocumentType) {
		this.fileCabinetFilesByDocumentType = fileCabinetFilesByDocumentType;
	}

	public String getFileCabinetFileIdListValue() {
		return fileCabinetFileIdListValue;
	}

	public void setFileCabinetFileIdListValue(String fileCabinetFileIdListValue) {
		this.fileCabinetFileIdListValue = fileCabinetFileIdListValue;
	}

	public String getFileCabinetIdListFromValue() {
		return fileCabinetIdListFromValue;
	}

	public void setFileCabinetIdListFromValue(String fileCabinetIdListFromValue) {
		this.fileCabinetIdListFromValue = fileCabinetIdListFromValue;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}

	public String getDashBoardHideJobsList() {
		return dashBoardHideJobsList;
	}

	public void setDashBoardHideJobsList(String dashBoardHideJobsList) {
		this.dashBoardHideJobsList = dashBoardHideJobsList;
	}  

}
