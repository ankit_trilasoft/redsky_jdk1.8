<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>RefJob Document Type List</title>   
    <meta name="heading" content="RefJobDocumentTypeList"/>  
    
<style>
.tab{
	border:1px solid #74B3DC;
}
span.pagelinks {
display:block;
font-size:0.90em;
margin-bottom:2px;
!margin-bottom:2px;
margin-top:-16px;
!margin-top:0px;
padding:2px 0px;
text-align:right;
width:100%;
}
</style> 

<script type="text/javascript"> 

function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove it?");
	var id = targetElement;
	if (agree){
		location.href="deleteRefJobDocumentType.html?id="+id;
	}else{
		return false;
	}
}

</script>

</head>

<c:set var="searchbuttons">   
	   <s:submit cssStyle="width:55px; height:25px" cssClass="cssbutton"  method="search" key="button.search"/>   
</c:set>   


<s:form id="refJobDocumentTypeList" name="refJobDocumentTypeList" action="" method="post" validate="true">	
<div id="Layer1" style="width: 100%;">
<div id="newmnav">
	  <ul>
	  	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Ref Job Document Type List</span></a></li>	  	
				<li><a href="editRefJobDocumentType.html" ><span>Backup Docs Details</span></a></li>					  	
	  </ul>
</div>
<div class="spn">&nbsp;</div>
<s:set name="refJobDocumentTypes" value="refJobDocumentTypes" scope="request"/> 
<s:hidden name="refJobDocumentType.corpID"/>
<s:hidden name="refJobDocumentType.id"/>
<s:hidden name="id" value="refJobDocumentTypeList.id"/>
<s:hidden name="refJobDocumentType.jobType"/>

<table  width="100%" cellspacing="1" cellpadding="0" border="0" style="!margin-top:5px;">
	<tbody>
		<tr>
			<td >				
				<s:set name="jobType" value="jobType" scope="request"/>  


<display:table name="refJobDocumentTypes" class="table" id="refJobDocumentTypes" export="true" requestURI="" pagesize="10" style="width:100%;"> 
	<display:column property="jobType" sortable="true" url="/editRefJobDocumentType.html"  paramId="id" paramProperty="id" style="width:110px"/>		
	<display:column property="refJobDocs" sortable="true" title="Doc List"  maxLength="15" style="width:110px"/>		
	<display:column property="corpID" sortable="true" title="CorpID" maxLength="15" style="width:110px"/>				
	
		<display:column title="Remove" style="text-align:center; width:20px;">
				<a><img align="middle" onclick="confirmSubmit(${refJobDocumentTypes.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
		</display:column>	
					
					
					<display:setProperty name="paging.banner.item_name" value="Ref Job Type"/>   
				    <display:setProperty name="paging.banner.items_name" value="Ref Job Type"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Ref Job Type List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Ref Job Type List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Ref Job Type List.pdf"/>   
	
</display:table>
			</td>
		</tr>
	</tbody>
</table> 

<c:set var="buttons"> 
    <input type="button" style="margin-right:5px;width:50px;" class="cssbutton" onclick="location.href='<c:url value="/editRefJobDocumentType.html"/>'" value="<fmt:message key="button.add"/>"/> 
</c:set> 
<c:out value="${buttons}" escapeXml="false" /> 
</div>
</s:form>