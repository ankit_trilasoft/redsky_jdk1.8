package com.trilasoft.app.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "extractqueryfile")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")

public class ExtractQueryFile extends BaseObject {
	private Long id;
	private Date createdon;
	private String createdby;
	private Date modifiedon;
	private String modifiedby;
	private String userId;
	private String queryName;
	private String publicPrivateFlag;
	private String queryCondition;
	private String corpID;
	private String extractType;
	private String selectQuery;
	private String selectColumnId;
	private boolean autoGenerateReoprt;
	private String queryScheduler;
	private String perWeekScheduler;
	private String perMonthScheduler;
	private String perQuarterScheduler;
	private String email;
	private String lastEmailSent;
	private Date emailDateTime;
	private String emailStatus;
	private String emailMessage;
	private String datePeriod;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("createdon",
				createdon).append("createdby", createdby).append("modifiedon",
				modifiedon).append("modifiedby", modifiedby).append("userId",
				userId).append("queryName", queryName).append(
				"publicPrivateFlag", publicPrivateFlag).append(
				"queryCondition", queryCondition).append("corpID", corpID)
				.append("extractType", extractType).append("selectQuery",
						selectQuery).append("selectColumnId", selectColumnId)
				.append("autoGenerateReoprt", autoGenerateReoprt).append(
						"queryScheduler", queryScheduler).append(
						"perWeekScheduler", perWeekScheduler).append(
						"perMonthScheduler", perMonthScheduler).append(
						"perQuarterScheduler", perQuarterScheduler).append(
						"email", email).append("lastEmailSent", lastEmailSent)
				.append("emailDateTime", emailDateTime).append("emailStatus",
						emailStatus).append("emailMessage", emailMessage)
				.append("datePeriod", datePeriod).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ExtractQueryFile))
			return false;
		ExtractQueryFile castOther = (ExtractQueryFile) other;
		return new EqualsBuilder().append(id, castOther.id).append(createdon,
				castOther.createdon).append(createdby, castOther.createdby)
				.append(modifiedon, castOther.modifiedon).append(modifiedby,
						castOther.modifiedby).append(userId, castOther.userId)
				.append(queryName, castOther.queryName).append(
						publicPrivateFlag, castOther.publicPrivateFlag).append(
						queryCondition, castOther.queryCondition).append(
						corpID, castOther.corpID).append(extractType,
						castOther.extractType).append(selectQuery,
						castOther.selectQuery).append(selectColumnId,
						castOther.selectColumnId).append(autoGenerateReoprt,
						castOther.autoGenerateReoprt).append(queryScheduler,
						castOther.queryScheduler).append(perWeekScheduler,
						castOther.perWeekScheduler).append(perMonthScheduler,
						castOther.perMonthScheduler).append(
						perQuarterScheduler, castOther.perQuarterScheduler)
				.append(email, castOther.email).append(lastEmailSent,
						castOther.lastEmailSent).append(emailDateTime,
						castOther.emailDateTime).append(emailStatus,
						castOther.emailStatus).append(emailMessage,
						castOther.emailMessage).append(datePeriod,
						castOther.datePeriod).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(createdon).append(
				createdby).append(modifiedon).append(modifiedby).append(userId)
				.append(queryName).append(publicPrivateFlag).append(
						queryCondition).append(corpID).append(extractType)
				.append(selectQuery).append(selectColumnId).append(
						autoGenerateReoprt).append(queryScheduler).append(
						perWeekScheduler).append(perMonthScheduler).append(
						perQuarterScheduler).append(email)
				.append(lastEmailSent).append(emailDateTime)
				.append(emailStatus).append(emailMessage).append(datePeriod)
				.toHashCode();
	}
	@Column(length=30)
	public String getExtractType() {
		return extractType;
	}
	public void setExtractType(String extractType) {
		this.extractType = extractType;
	}
	
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=30)
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length=82)
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	@Column(length=82)
	public String getModifiedby() {
		return modifiedby;
	}
	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}
	public ExtractQueryFile() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Column(length=30)
	public Date getModifiedon() {
		return modifiedon;
	}
	public void setModifiedon(Date modifiedon) {
		this.modifiedon = modifiedon;
	}
	@Column(length=30)
	public String getPublicPrivateFlag() {
		return publicPrivateFlag;
	}
	public void setPublicPrivateFlag(String publicPrivateFlag) {
		this.publicPrivateFlag = publicPrivateFlag;
	}
	@Column(length=2500)
	public String getQueryCondition() {
		return queryCondition;
	}
	public void setQueryCondition(String queryCondition) {
		this.queryCondition = queryCondition;
	}
	@Column(length=30)
	public String getQueryName() {
		return queryName;
	}
	public void setQueryName(String queryName) {
		this.queryName = queryName;
	}
	@Column(length=10)
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	@Column(length=2500)
	public String getSelectQuery() {
		return selectQuery;
	}
	public void setSelectQuery(String selectQuery) {
		this.selectQuery = selectQuery;
	}
	@Column(length=1000)
	public String getSelectColumnId() {
		return selectColumnId;
	}
	public void setSelectColumnId(String selectColumnId) {
		this.selectColumnId = selectColumnId;
	}
	@Column
	public boolean isAutoGenerateReoprt() {
		return autoGenerateReoprt;
	}
	public void setAutoGenerateReoprt(boolean autoGenerateReoprt) {
		this.autoGenerateReoprt = autoGenerateReoprt;
	}
	@Column(length=10)
	public String getPerMonthScheduler() {
		return perMonthScheduler;
	}
	public void setPerMonthScheduler(String perMonthScheduler) {
		this.perMonthScheduler = perMonthScheduler;
	}

	@Column(length=10)
	public String getPerWeekScheduler() {
		return perWeekScheduler;
	}
	public void setPerWeekScheduler(String perWeekScheduler) {
		this.perWeekScheduler = perWeekScheduler;
	}
	
	@Column(length=10)
	public String getQueryScheduler() {
		return queryScheduler;
	}
	public void setQueryScheduler(String queryScheduler) {
		this.queryScheduler = queryScheduler;
	}
	@Column(length=10)
	public String getPerQuarterScheduler() {
		return perQuarterScheduler;
	}
	public void setPerQuarterScheduler(String perQuarterScheduler) {
		this.perQuarterScheduler = perQuarterScheduler;
	}
	
	@Column(length=225)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column
	public Date getEmailDateTime() {
		return emailDateTime;
	}
	public void setEmailDateTime(Date emailDateTime) {
		this.emailDateTime = emailDateTime;
	}
	
	@Column(length=225)
	public String getEmailMessage() {
		return emailMessage;
	}
	public void setEmailMessage(String emailMessage) {
		this.emailMessage = emailMessage;
	}
	
	@Column(length=50)
	public String getEmailStatus() {
		return emailStatus;
	}
	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}
	
	@Column(length=225)
	public String getLastEmailSent() {
		return lastEmailSent;
	}
	public void setLastEmailSent(String lastEmailSent) {
		this.lastEmailSent = lastEmailSent;
	}
	
	@Column(length=2500)
	public String getDatePeriod() {
		return datePeriod;
	}
	public void setDatePeriod(String datePeriod) {
		this.datePeriod = datePeriod;
	}
	
	
	


	
	
}
