package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="companydivision")
@FilterDefs({
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")}),
@FilterDef(name = "serviceOrderCompanyDivisionFilter", parameters = { @ParamDef(name = "companydivision", type = "string")})
})
@Filters( {
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)"),
@Filter(name = "serviceOrderCompanyDivisionFilter", condition = " companyCode in (:companydivision)  ")
} )
public class CompanyDivision extends BaseObject {
	private String corpID;
	private Long id;
	private String companyCode;
	private String description;
	private String accountingCode;
	private String bookingAgentCode;
	private String vanLineCode;
	private String vanlinedefaultJobtype;
	private String billingAddress1;
	private String billingAddress2;
	private String billingAddress3;
	private String billingAddress4;
	private String billingCity;
	private String billingCountry;
	private String billingCountryCode;
	private String billingEmail;
	private String billingFax;
	private String billingPhone;
	private String billingState;
	private String billingTelex;
	private String billingZip;
	private String logoName;
	private String bankName;
	private String bankAccountNumber;
	private String wireTransferAccountNumber;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String vanlineseq;
	private String vanlineCoordinator;
	private String vanLineCodeGen;
	private String EIN;
	private String SCAC;
	private String internalCostVendorCode;
	private String operationsHub;
	private String baseCurrency;
	private String resourceID;
	private String resourcePassword;
	private String textMessage;
	private String vanlineJob;
	private String swiftcode;
	private String bankAddress;
	private String beneficiaryName;
	private String mssCustomerID;
	private String mssPassword;
	private String ugwwAgentCodes;
	private String childAgentCode;
	private Date lastUpdatedLeave;
	private String cportalBrandingURL;
	private String companyWebsite;
	private String eoriNo;
	private Date inactiveEffectiveDate;
	private Boolean closedDivision = new Boolean(false);
	private String GSTRegnNo;
	private int lastInvoiceNumber=0;
	private int lastCreditInvoice=0;



	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id).append("companyCode", companyCode)
				.append("description", description)
				.append("accountingCode", accountingCode)
				.append("bookingAgentCode", bookingAgentCode)
				.append("vanLineCode", vanLineCode)
				.append("vanlinedefaultJobtype", vanlinedefaultJobtype)
				.append("billingAddress1", billingAddress1)
				.append("billingAddress2", billingAddress2)
				.append("billingAddress3", billingAddress3)
				.append("billingAddress4", billingAddress4)
				.append("billingCity", billingCity)
				.append("billingCountry", billingCountry)
				.append("billingCountryCode", billingCountryCode)
				.append("billingEmail", billingEmail)
				.append("billingFax", billingFax)
				.append("billingPhone", billingPhone)
				.append("billingState", billingState)
				.append("billingTelex", billingTelex)
				.append("billingZip", billingZip).append("logoName", logoName)
				.append("bankName", bankName)
				.append("bankAccountNumber", bankAccountNumber)
				.append("wireTransferAccountNumber", wireTransferAccountNumber)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("vanlineseq", vanlineseq)
				.append("vanlineCoordinator", vanlineCoordinator)
				.append("vanLineCodeGen", vanLineCodeGen).append("EIN", EIN)
				.append("SCAC", SCAC)
				.append("internalCostVendorCode", internalCostVendorCode)
				.append("operationsHub", operationsHub)
				.append("baseCurrency", baseCurrency)
				.append("resourceID", resourceID)
				.append("resourcePassword", resourcePassword)
				.append("textMessage", textMessage)
				.append("vanlineJob", vanlineJob)
				.append("swiftcode", swiftcode)
				.append("bankAddress", bankAddress)
				.append("beneficiaryName", beneficiaryName)
				.append("mssCustomerID", mssCustomerID)
				.append("mssPassword", mssPassword)
				.append("ugwwAgentCodes", ugwwAgentCodes)
				.append("childAgentCode",childAgentCode)
		        .append("lastUpdatedLeave",lastUpdatedLeave)
		        .append("cportalBrandingURL",cportalBrandingURL)
		        .append("companyWebsite",companyWebsite)
		        .append("eoriNo",eoriNo).append("inactiveEffectiveDate",inactiveEffectiveDate)
		        .append("closedDivision",closedDivision)
		        .append("GSTRegnNo",GSTRegnNo)
		        .append("lastInvoiceNumber",lastInvoiceNumber)
		        .append("lastCreditInvoice",lastCreditInvoice)
		        .toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CompanyDivision))
			return false;
		CompanyDivision castOther = (CompanyDivision) other;
		return new EqualsBuilder()
				.append(corpID, castOther.corpID)
				.append(id, castOther.id)
				.append(companyCode, castOther.companyCode)
				.append(description, castOther.description)
				.append(accountingCode, castOther.accountingCode)
				.append(bookingAgentCode, castOther.bookingAgentCode)
				.append(vanLineCode, castOther.vanLineCode)
				.append(vanlinedefaultJobtype, castOther.vanlinedefaultJobtype)
				.append(billingAddress1, castOther.billingAddress1)
				.append(billingAddress2, castOther.billingAddress2)
				.append(billingAddress3, castOther.billingAddress3)
				.append(billingAddress4, castOther.billingAddress4)
				.append(billingCity, castOther.billingCity)
				.append(billingCountry, castOther.billingCountry)
				.append(billingCountryCode, castOther.billingCountryCode)
				.append(billingEmail, castOther.billingEmail)
				.append(billingFax, castOther.billingFax)
				.append(billingPhone, castOther.billingPhone)
				.append(billingState, castOther.billingState)
				.append(billingTelex, castOther.billingTelex)
				.append(billingZip, castOther.billingZip)
				.append(logoName, castOther.logoName)
				.append(bankName, castOther.bankName)
				.append(bankAccountNumber, castOther.bankAccountNumber)
				.append(wireTransferAccountNumber,
						castOther.wireTransferAccountNumber)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(vanlineseq, castOther.vanlineseq)
				.append(vanlineCoordinator, castOther.vanlineCoordinator)
				.append(vanLineCodeGen, castOther.vanLineCodeGen)
				.append(EIN, castOther.EIN)
				.append(SCAC, castOther.SCAC)
				.append(internalCostVendorCode,
						castOther.internalCostVendorCode)
				.append(operationsHub, castOther.operationsHub)
				.append(baseCurrency, castOther.baseCurrency)
				.append(resourceID, castOther.resourceID)
				.append(resourcePassword, castOther.resourcePassword)
				.append(textMessage, castOther.textMessage)
				.append(vanlineJob, castOther.vanlineJob)
				.append(swiftcode, castOther.swiftcode)
				.append(bankAddress, castOther.bankAddress)
				.append(beneficiaryName, castOther.beneficiaryName)
				.append(mssCustomerID, castOther.mssCustomerID)
				.append(mssPassword, castOther.mssPassword)
				.append(ugwwAgentCodes, castOther.ugwwAgentCodes)
				.append(childAgentCode, castOther.childAgentCode)
		        .append(lastUpdatedLeave, castOther.lastUpdatedLeave)
		        .append(cportalBrandingURL, castOther.cportalBrandingURL)
		        .append(companyWebsite, castOther.companyWebsite)
		        .append(eoriNo, castOther.eoriNo).append(inactiveEffectiveDate,castOther.inactiveEffectiveDate)
		        .append(closedDivision,castOther.closedDivision)
		        .append(GSTRegnNo,castOther.GSTRegnNo) 
		        .append(lastInvoiceNumber,castOther.lastInvoiceNumber)
		        .append(lastCreditInvoice,castOther.lastCreditInvoice)
		        .isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id)
				.append(companyCode).append(description).append(accountingCode)
				.append(bookingAgentCode).append(vanLineCode)
				.append(vanlinedefaultJobtype).append(billingAddress1)
				.append(billingAddress2).append(billingAddress3)
				.append(billingAddress4).append(billingCity)
				.append(billingCountry).append(billingCountryCode)
				.append(billingEmail).append(billingFax).append(billingPhone)
				.append(billingState).append(billingTelex).append(billingZip)
				.append(logoName).append(bankName).append(bankAccountNumber)
				.append(wireTransferAccountNumber).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(vanlineseq).append(vanlineCoordinator)
				.append(vanLineCodeGen).append(EIN).append(SCAC)
				.append(internalCostVendorCode).append(operationsHub)
				.append(baseCurrency).append(resourceID)
				.append(resourcePassword).append(textMessage)
				.append(vanlineJob).append(swiftcode).append(bankAddress)
				.append(beneficiaryName).append(mssCustomerID)
				.append(mssPassword).append(ugwwAgentCodes)
				.append(childAgentCode)
		        .append(lastUpdatedLeave)
		        .append(cportalBrandingURL)
		        .append(companyWebsite)
		        .append(eoriNo).append(inactiveEffectiveDate)
		        .append(closedDivision).append(GSTRegnNo)
		        .append(lastInvoiceNumber)
		        .append(lastCreditInvoice)
		        .toHashCode();
	}

	@Column(length=20)
	public String getAccountingCode() {
		return accountingCode;
	}
	public void setAccountingCode(String accountingCode) {
		this.accountingCode = accountingCode;
	}
	@Column(length=20)
	public String getBookingAgentCode() {
		return bookingAgentCode;
	}
	public void setBookingAgentCode(String bookingAgentCode) {
		this.bookingAgentCode = bookingAgentCode;
	}
	@Column(length=10)
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	@Column(length=20)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=100)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Id@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=20)
	public String getVanLineCode() {
		return vanLineCode;
	}
	public void setVanLineCode(String vanLineCode) {
		this.vanLineCode = vanLineCode;
	}
	@Column(length=5)
	public String getVanlinedefaultJobtype() {
		return vanlinedefaultJobtype;
	}
	public void setVanlinedefaultJobtype(String vanlinedefaultJobtype) {
		this.vanlinedefaultJobtype = vanlinedefaultJobtype;
	}
	@Column(length=50)
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	@Column(length=100)
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	@Column
	public String getBillingAddress1() {
		return billingAddress1;
	}
	public void setBillingAddress1(String billingAddress1) {
		this.billingAddress1 = billingAddress1;
	}
	@Column
	public String getBillingAddress2() {
		return billingAddress2;
	}
	public void setBillingAddress2(String billingAddress2) {
		this.billingAddress2 = billingAddress2;
	}
	@Column
	public String getBillingAddress3() {
		return billingAddress3;
	}
	public void setBillingAddress3(String billingAddress3) {
		this.billingAddress3 = billingAddress3;
	}
	@Column
	public String getBillingAddress4() {
		return billingAddress4;
	}
	public void setBillingAddress4(String billingAddress4) {
		this.billingAddress4 = billingAddress4;
	}
	@Column(length=20)
	public String getBillingCity() {
		return billingCity;
	}
	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}
	@Column(length=45)
	public String getBillingCountry() {
		return billingCountry;
	}
	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}
	@Column(length=70)
	public String getBillingEmail() {
		return billingEmail;
	}
	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}
	@Column(length=20)
	public String getBillingFax() {
		return billingFax;
	}
	public void setBillingFax(String billingFax) {
		this.billingFax = billingFax;
	}
	@Column(length=100)
	public String getBillingPhone() {
		return billingPhone;
	}
	public void setBillingPhone(String billingPhone) {
		this.billingPhone = billingPhone;
	}
	@Column(length=2)
	public String getBillingState() {
		return billingState;
	}
	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}
	@Column(length=20)
	public String getBillingTelex() {
		return billingTelex;
	}
	public void setBillingTelex(String billingTelex) {
		this.billingTelex = billingTelex;
	}
	@Column(length=50)
	public String getBillingZip() {
		return billingZip;
	}
	public void setBillingZip(String billingZip) {
		this.billingZip = billingZip;
	}
	@Column(length=50)
	public String getLogoName() {
		return logoName;
	}
	public void setLogoName(String logoName) {
		this.logoName = logoName;
	}
	@Column(length=50)
	public String getWireTransferAccountNumber() {
		return wireTransferAccountNumber;
	}
	public void setWireTransferAccountNumber(String wireTransferAccountNumber) {
		this.wireTransferAccountNumber = wireTransferAccountNumber;
	}
	@Column(length=3)
	public String getBillingCountryCode() {
		return billingCountryCode;
	}
	public void setBillingCountryCode(String billingCountryCode) {
		this.billingCountryCode = billingCountryCode;
	}
	@Column
	public String getVanlineseq() {
		return vanlineseq;
	}
	public void setVanlineseq(String vanlineseq) {
		this.vanlineseq = vanlineseq;
	}
	@Column( length=82)
	public String getVanlineCoordinator() {
		return vanlineCoordinator;
	}
	public void setVanlineCoordinator(String vanlineCoordinator) {
		this.vanlineCoordinator = vanlineCoordinator;
	}
	@Column(length=20)
	public String getVanLineCodeGen() {
		return vanLineCodeGen;
	}
	public void setVanLineCodeGen(String vanLineCodeGen) {
		this.vanLineCodeGen = vanLineCodeGen;
	}
	@Column(length=20)
	public String getEIN() {
		return EIN;
	}
	public void setEIN(String ein) {
		EIN = ein;
	}
	public String getSCAC() {
		return SCAC;
	}
	public void setSCAC(String scac) {
		SCAC = scac;
	}
	
	@Column(length=20)
	public String getInternalCostVendorCode() {
		return internalCostVendorCode;
	}
	public void setInternalCostVendorCode(String internalCostVendorCode) {
		this.internalCostVendorCode = internalCostVendorCode;
	}
	public String getOperationsHub() {
		return operationsHub;
	}
	public void setOperationsHub(String operationsHub) {
		this.operationsHub = operationsHub;
	}
	@Column(length=25)	
	public String getBaseCurrency() {
		return baseCurrency;
	}
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	public String getResourceID() {
		return resourceID;
	}
	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}
	public String getResourcePassword() {
		return resourcePassword;
	}
	public void setResourcePassword(String resourcePassword) {
		this.resourcePassword = resourcePassword;
	}
	public String getTextMessage() {
		return textMessage;
	}
	public void setTextMessage(String textMessage) {
		this.textMessage = textMessage;
	}
	
	@Column
	public String getVanlineJob() {
		return vanlineJob;
	}
	public void setVanlineJob(String vanlineJob) {
		this.vanlineJob = vanlineJob;
	}
	@Column(length=50)
	public String getSwiftcode() {
	    return swiftcode;
	}
	public void setSwiftcode(String swiftcode) {
	    this.swiftcode = swiftcode;
	}
	@Column(length=150)
	public String getBankAddress() {
		return bankAddress;
	}
	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}
	@Column
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	@Column
	public String getMssCustomerID() {
		return mssCustomerID;
	}
	public void setMssCustomerID(String mssCustomerID) {
		this.mssCustomerID = mssCustomerID;
	}
	@Column
	public String getMssPassword() {
		return mssPassword;
	}
	public void setMssPassword(String mssPassword) {
		this.mssPassword = mssPassword;
	}
	public String getUgwwAgentCodes() {
		return ugwwAgentCodes;
	}
	public void setUgwwAgentCodes(String ugwwAgentCodes) {
		this.ugwwAgentCodes = ugwwAgentCodes;
	}
	public String getChildAgentCode() {
		return childAgentCode;
	}
	public void setChildAgentCode(String childAgentCode) {
		this.childAgentCode = childAgentCode;
	}
	@Column
	public Date getLastUpdatedLeave() {
		return lastUpdatedLeave;
	}
	public void setLastUpdatedLeave(Date lastUpdatedLeave) {
		this.lastUpdatedLeave = lastUpdatedLeave;
	}
	@Column
	public String getCportalBrandingURL() {
		return cportalBrandingURL;
	}
	public void setCportalBrandingURL(String cportalBrandingURL) {
		this.cportalBrandingURL = cportalBrandingURL;
	}
	@Column
	public String getCompanyWebsite() {
		return companyWebsite;
	}
	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}
	@Column 
	public String getEoriNo() {
		return eoriNo;
	}
	public void setEoriNo(String eoriNo) {
		this.eoriNo = eoriNo;
	}
	@Column
	public Date getInactiveEffectiveDate() {
		return inactiveEffectiveDate;
	}
	public void setInactiveEffectiveDate(Date inactiveEffectiveDate) {
		this.inactiveEffectiveDate = inactiveEffectiveDate;
	}
	public Boolean getClosedDivision() {
		return closedDivision;
	}
	public void setClosedDivision(Boolean closedDivision) {
		this.closedDivision = closedDivision;
	}
	
	@Column
	public String getGSTRegnNo() {
		return GSTRegnNo;
	}
	public void setGSTRegnNo(String gSTRegnNo) {
		GSTRegnNo = gSTRegnNo;
	}
	
	@Column
	public int getLastInvoiceNumber() {
		return lastInvoiceNumber;
	}
	public void setLastInvoiceNumber(int lastInvoiceNumber) {
		this.lastInvoiceNumber = lastInvoiceNumber;
	}
	
	@Column
	public int getLastCreditInvoice() {
		return lastCreditInvoice;
	}
	public void setLastCreditInvoice(int lastCreditInvoice) {
		this.lastCreditInvoice = lastCreditInvoice;
	}
	
}
