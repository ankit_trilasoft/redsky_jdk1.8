<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Reciprocity Analysis</title>   
    <meta name="heading" content="Reciprocity Analysis"/> 
    <style type="text/css"> 

.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

.table tfoot td {
border:1px solid #E0E0E0;
}
.table2 thead th, table2HeaderTable td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0 0;
border-color:#99BBE8 #99BBE8 #99BBE8 -moz-use-text-color;
border-style:solid;
border-right:medium;
color:#99BBE8;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

</style> 
<style type="text/css">h2 {background-color: #FBBFFF}</style> 
	
  
</head>
<s:form id="reciprocityAnalysisDetailList" action="" method="post">  
 

<s:set name="reciprocityAnalysisDetailList" value="reciprocityAnalysisDetailList" scope="request"/> 
<display:table name="reciprocityAnalysisDetailList" class="table" requestURI="" id="reciprocityAnalysisDetailList" style="width:100%"  defaultsort="0" pagesize="100" >   
 		 <display:column property="shipnumber" sortable="true" title="So#" style="width:5%" />
		 <display:column property="shipper" sortable="true" title="Shipper"  style="width:4%"/> 
		 <display:column property="mode" sortable="true" title="Mode" style="width:2%"/> 
		 <display:column property="loadDelivery" sortable="true" title="Load/Delivery" style="width:9%"/> 
		 <display:column property="booker" sortable="true" title="Booker"   style="width:15%"/>  
		 <display:column property="origin" sortable="true" title="Origin"  style="width:3%"/> 
		 <display:column property="oA" sortable="true" title="OA"   style="width:15%"/> 
		 <configByCorp:fieldVisibility componentId="component.tab.ReciprocityAnalysis.subOADA">
		 <display:column property="subOA" sortable="true" title="Sub OA"   style="width:15%"/> 
		 </configByCorp:fieldVisibility>  
		 <display:column property="dest" sortable="true" title="Dest."  style="width:3%"/> 
		 <display:column property="dA" sortable="true" title="DA"  style="width:15%"/>
		 <configByCorp:fieldVisibility componentId="component.tab.ReciprocityAnalysis.subOADA"> 
		 <display:column property="subDA" sortable="true" title="Sub DA"   style="width:15%"/> 
		 </configByCorp:fieldVisibility> 
		 <display:column property="accountCode" sortable="true" title="Acc&nbsp;Code"  style="width:4%"/>
		 <display:column property="accountName" sortable="true" title="Acc&nbsp;Name"  style="width:15%"/>
		 <display:column sortable="true" title="Lbs"  style="width:3%">
		 <fmt:formatNumber type="number" maxFractionDigits="0"
                     value="${reciprocityAnalysisDetailList.tonnage }" />
		 </display:column> 
		 <display:column sortable="true" title="Kgs"  style="width:3%">
		 <fmt:formatNumber type="number" maxFractionDigits="0"
                     value="${reciprocityAnalysisDetailList.tonnageKg }" />
		 </display:column> 
		 
		 <display:column sortable="true" title="Rev Given"  style="width:3%">
		 <fmt:formatNumber type="number" maxFractionDigits="0"
                     value="${reciprocityAnalysisDetailList.rev }" />
		 </display:column> 
</display:table> 
<c:if test="${includeChildCorpids}"> 
<c:forEach items="${corpidCount}" var="myMap">
  <c:out value="${myMap.key}"></c:out>
  <c:out value="${myMap.value}"></c:out>
</c:forEach>
</c:if>
<table> 
 <tr>
 <td>
<input type="button" name="Submit"  value="Close" class="cssbuttonA" style="width:60px; height:25px" onclick="window.close();">
</td>
 </tr>
</table>
 
</s:form>
<script type="text/javascript">   

 
</script>  		  	