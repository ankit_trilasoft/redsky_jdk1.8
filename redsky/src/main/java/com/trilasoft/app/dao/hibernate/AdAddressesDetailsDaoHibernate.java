package com.trilasoft.app.dao.hibernate;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.AdAddressesDetailsDao;
import com.trilasoft.app.model.AdAddressesDetails;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.StandardAddresses;


public class AdAddressesDetailsDaoHibernate extends GenericDaoHibernate<AdAddressesDetails, Long>implements AdAddressesDetailsDao {

	public AdAddressesDetailsDaoHibernate() {   
        super(AdAddressesDetails.class);   
    }

	public List getAdAddressesDetailsList(Long id, String sessionCorpID) {
		return getHibernateTemplate().find("from AdAddressesDetails where corpID='"+sessionCorpID+"' and customerFileId=?", id); 
	}
	public Map<String, String> getAdAddressesDropDown(Long id, String sessionCorpID,String temp,String jobType) {
		List<AdAddressesDetails> list;
		list = getHibernateTemplate().find("from AdAddressesDetails where corpID='"+sessionCorpID+"' and customerFileId=?", id);

		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		/*if(temp.equalsIgnoreCase("Origin"))
		{*/
			parameterMap.put("Origin Address", "Default Origin");
		//}else{
			parameterMap.put("Destination Address", "Default Destination");
		//}
		for (AdAddressesDetails adAddressesDetails : list) {
			parameterMap.put(adAddressesDetails.getId()+"^add", adAddressesDetails.getDescription());
		}
		List<StandardAddresses> list2 ;
		list2 = getHibernateTemplate().find("from StandardAddresses where job='"+jobType+"'");
		for(StandardAddresses standardAddresses : list2){
			parameterMap.put(standardAddresses.getId()+"^std", standardAddresses.getAddressLine1());
		}
		return parameterMap;
	}

	public Long findRemoteAdAddressesDetails(Long networkId, Long id) {
		return Long.parseLong(this.getSession().createSQLQuery("select id from address where networkId="+networkId+" and customerFileId="+id+"").list().get(0).toString());
		 
	}

	public int updateAdAddressesDetailsNetworkId(Long id) {
		int i=getSession().createSQLQuery("update address set networkId="+id+" where id="+id).executeUpdate();
		return i;
	}
	public AdAddressesDetails getFirstAddressDetail(String sessionCorpID,Long id, String adType){
		List<AdAddressesDetails> list;
		list = getHibernateTemplate().find("from AdAddressesDetails where corpID='"+sessionCorpID+"' and addressType='"+adType+"' and customerFileId=?", id);
		if(list!=null && !list.isEmpty() && list.get(0)!=null){
			AdAddressesDetails adNew = list.get(0);
			return adNew;
		}
		return null;
	}
}
