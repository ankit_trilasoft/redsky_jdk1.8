package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CorpComponentPermission;

public interface CorpComponentPermissionManager extends GenericManager<CorpComponentPermission, Long>{
	
	public boolean getFieldVisibilityForComponent(String userCorpID, String componentId);

	public List findMaximumId();

	public List searchcompanyPermission(String componentId, String description,String corpId);
	public List findPermissionsByCorpId(String corpId);
	public List findCorpID();

	public Map<String, Integer> getComponentVisibilityAttrbuteDetail(String userCorpID, String componentId);

	public void updateCurrentFieldVisibilityMap();
}
