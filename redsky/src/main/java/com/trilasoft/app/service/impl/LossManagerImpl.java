package com.trilasoft.app.service.impl;

import java.util.List;

import com.trilasoft.app.dao.LossDao;
import com.trilasoft.app.model.Loss;
import com.trilasoft.app.model.LossPicture;
import com.trilasoft.app.service.LossManager;

import org.appfuse.service.impl.GenericManagerImpl;

public class LossManagerImpl extends GenericManagerImpl<Loss, Long> implements LossManager {
	LossDao lossDao;

	public LossManagerImpl(LossDao lossDao) {
		super(lossDao);
		this.lossDao = lossDao;
	}

	public List findMaximum() {
		return lossDao.findMaximum();
	}

	public List<Loss> findByShipNumber(String shipNumber) {
		return lossDao.findByShipNumber(shipNumber);
	}

	public List<Loss> findByfindClaimLossTickets(String lossNumber) {
		return lossDao.findByfindClaimLossTickets(lossNumber);
	}

	public List findMaximumId() {
		return lossDao.findMaximumId();
	}

	public List checkById(Long id) {
		return lossDao.checkById(id);
	}

	public List findLossByIdNumber(int idNum) {
		return lossDao.findLossByIdNumber(idNum);
	}

	/*public Boolean ticketExists(Long ticket,String lossNumber){
	 return lossDao.ticketExists(ticket,lossNumber);
	 }
	 public Boolean checkTicket(Long ticket,String lossNumber){
	 return lossDao.checkTicket(ticket,lossNumber);

	 }*/
	public List<Loss> findLossList() {
		return lossDao.findLossList();
	}

	public List findWH(Long claimNumber, String corpID) {
		return lossDao.findWH(claimNumber, corpID);
	}

	public List findCountLoss(Long claimNumber, String sessionCorpID) {
		return lossDao.findCountLoss(claimNumber, sessionCorpID);
	}

	public List findMaximumLoss(Long claimNumber, String sessionCorpID) {
		return lossDao.findMaximumLoss(claimNumber, sessionCorpID);
	}

	public List findMinimumLoss(Long claimNumber, String sessionCorpID) {
		return lossDao.findMinimumLoss(claimNumber, sessionCorpID);
	}

	public List goNextLossChild(Long claimNumber, Long id, String sessionCorpID) {
		return lossDao.goNextLossChild(claimNumber, id, sessionCorpID);
	}

	public List goPrevLossChild(Long claimNumber, Long id, String sessionCorpID) {
		return lossDao.goPrevLossChild(claimNumber, id, sessionCorpID);
	}
	
	public List goLossChild(Long claimNum, Long id, String sessionCorpID){
		return lossDao.goLossChild(claimNum, id, sessionCorpID);
	}
	public int updateLossChequeNumberCustmer(String chequeNumberCustmer,Long claimNumber, String shipNumber)
	{
		return lossDao.updateLossChequeNumberCustmer(chequeNumberCustmer,claimNumber, shipNumber);
	}
	public int updateLossChequeNumber3Party(String chequeNumber3Party,Long claimNumber, String shipNumber)
	{
		return lossDao.updateLossChequeNumber3Party(chequeNumber3Party,claimNumber, shipNumber);
	}

	public List lossList(Long claimId) {
		return lossDao.lossList(claimId);
	}

	public List findMaximumIdNumber1(String corpID) {
		return lossDao.findMaximumIdNumber1(corpID);
	}

	public List findMaximumIdNumber(Long id) {
		return lossDao.findMaximumIdNumber(id);
	}

	public Long findRemoteLoss(String idNumber, Long id) {
		return lossDao.findRemoteLoss(idNumber, id);
	}
	public List lossForOtherCorpId(Long id,String corpId){
		return lossDao.lossForOtherCorpId(id, corpId);
	}
	public List getpertaininglossvalue(Long claimNumber, String itmClmsItem, String sessionCorpID){
		return lossDao.getpertaininglossvalue(claimNumber, itmClmsItem, sessionCorpID);
	}
	public List<LossPicture> getLossPictureList(String lossId,String claimId){
		return lossDao.getLossPictureList(lossId, claimId);
	};
	
	public List getLossPictureData(Long id, String corpID){
		return lossDao.getLossPictureData(id, corpID);
	}
	
	public List getLossDocumentImageServletAction(Long id, String sessionCorpID){
		return lossDao.getLossDocumentImageServletAction(id, sessionCorpID);
	}
}