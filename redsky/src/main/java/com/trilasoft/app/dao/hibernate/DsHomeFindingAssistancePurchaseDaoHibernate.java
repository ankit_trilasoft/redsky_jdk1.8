package com.trilasoft.app.dao.hibernate;

import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.DsHomeFindingAssistancePurchaseDao;
import com.trilasoft.app.model.DsHomeFindingAssistancePurchase;

public class DsHomeFindingAssistancePurchaseDaoHibernate extends GenericDaoHibernate<DsHomeFindingAssistancePurchase, Long> implements DsHomeFindingAssistancePurchaseDao {	
	public DsHomeFindingAssistancePurchaseDaoHibernate(){
		super(DsHomeFindingAssistancePurchase.class);
	}
	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsHomeFindingAssistancePurchase where id = "+id+" ");
	}

	
}
