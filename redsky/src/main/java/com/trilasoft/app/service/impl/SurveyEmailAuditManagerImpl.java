package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.SurveyEmailAuditDao;
import com.trilasoft.app.model.SurveyEmailAudit;
import com.trilasoft.app.service.SurveyEmailAuditManager;

public class SurveyEmailAuditManagerImpl extends GenericManagerImpl<SurveyEmailAudit, Long> implements SurveyEmailAuditManager{
	SurveyEmailAuditDao surveyEmailAuditDao;
	public SurveyEmailAuditManagerImpl(SurveyEmailAuditDao surveyEmailAuditDao)
	{
		super(surveyEmailAuditDao);
		this.surveyEmailAuditDao=surveyEmailAuditDao;
	}
	public Long getSurveyEmailAuditSession(String shipNumber,String accountId,String corpId){
		return surveyEmailAuditDao.getSurveyEmailAuditSession(shipNumber,accountId, corpId);
	}
}
