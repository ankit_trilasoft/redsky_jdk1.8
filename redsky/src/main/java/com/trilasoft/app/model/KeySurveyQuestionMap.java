package com.trilasoft.app.model;

import org.appfuse.model.BaseObject;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table ( name="keysurveyquestionmap") 
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class KeySurveyQuestionMap extends BaseObject{
	private Long id;
	private Long surveyQuestionId;
	private String questionText;
	private String type;
	private String corpId;	
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("surveyQuestionId", surveyQuestionId)
				.append("questionText", questionText).append("type", type)
				.append("corpId", corpId).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof KeySurveyQuestionMap))
			return false;
		KeySurveyQuestionMap castOther = (KeySurveyQuestionMap) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(surveyQuestionId, castOther.surveyQuestionId)
				.append(questionText, castOther.questionText)
				.append(type, castOther.type).append(corpId, castOther.corpId)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(surveyQuestionId)
				.append(questionText).append(type).append(corpId)
				.append(createdBy).append(createdOn).append(updatedBy)
				.append(updatedOn).toHashCode();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSurveyQuestionId() {
		return surveyQuestionId;
	}
	public void setSurveyQuestionId(Long surveyQuestionId) {
		this.surveyQuestionId = surveyQuestionId;
	}
	@Column(length=20)
	public String getQuestionText() {
		return questionText;
	}
	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}
	@Column(length=20)
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Column(length=15)
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
}
