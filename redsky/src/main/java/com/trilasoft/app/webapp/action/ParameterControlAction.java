package com.trilasoft.app.webapp.action;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.ParameterControl;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.service.ParameterControlManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;
import com.trilasoft.app.webapp.helper.SearchCriterion;

import org.acegisecurity.Authentication;
import org.appfuse.model.User;
import org.displaytag.properties.SortOrderEnum;
public class ParameterControlAction extends BaseAction{
	private Long id;
	private ParameterControlManager parameterControlManager;
	private List<ParameterControl> parameterControls;
	private ParameterControl parameterControl;
	private String sessionCorpID;
	private List useCorpId;
	 private PagingLookupManager pagingLookupManager;
	 private PaginateListFactory paginateListFactory;
	private ExtendedPaginatedList parameterControlsExt;
	private List childParameter;
	public List getUseCorpId() {
		return useCorpId;
	}
	public void setUseCorpId(List useCorpId) {
		this.useCorpId = useCorpId;
	}
	ParameterControlAction(){
		Authentication auth=SecurityContextHolder.getContext().getAuthentication();
		User user=(User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
 public String list(){
	 parameterControls=parameterControlManager.getAll();	
	 useCorpId=parameterControlManager.findUseCorpId();
	
	 
	 return SUCCESS;
 }
 @SkipValidation
 public String searchOrder(){ 
	 	parameterControls=parameterControlManager.findByFields(parameterControl.getParameter(),parameterControl.getCustomType(),parameterControl.getActive(),parameterControl.getTsftFlag(),parameterControl.getHybridFlag()); 
	 return SUCCESS;     
   } 
 public String searchList(){
	 parameterControls=parameterControlManager.findByFields("","",true,true,true); 
	 parameterControl=new ParameterControl();
	 parameterControl.setActive(true);
	 parameterControl.setHybridFlag(true);
	 parameterControl.setTsftFlag(true);
	 return SUCCESS; 
 }
 
 @SkipValidation
 public String search(){ 
	    useCorpId=parameterControlManager.findUseCorpId();
	  		boolean activeStatus= true;
		    boolean parameter = (parameterControl.getParameter() == null);
		    boolean fieldLength = (parameterControl.getFieldLength() == null);
		   
		    getRequest().getSession().setAttribute("activeStatusSession", activeStatus);
		       
	       if(!parameter || !fieldLength) 
	       {
	    	   parameterControlsExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
	    	  List<SearchCriterion> searchCriteria = new ArrayList();
	               searchCriteria.add(new SearchCriterion("parameter",parameterControl.getParameter(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
				   searchCriteria.add(new SearchCriterion("fieldLength",parameterControl.getFieldLength(),SearchCriterion.FIELD_TYPE_INTEGER, SearchCriterion.OPERATOR_LIKE));
				  
		          
           pagingLookupManager.getAllRecordsPage(ParameterControl.class, parameterControlsExt, searchCriteria);
	     }
	   
   return SUCCESS;     
   } 
 
 public String edit(){
	 if(id!=null){
		 
		 
		 parameterControl= parameterControlManager.get(id);
		 useCorpId=parameterControlManager.findUseCorpId();
	 }
	 else{
			 parameterControl=new ParameterControl();
			 useCorpId=parameterControlManager.findUseCorpId();
			 parameterControl.setCreatedOn(new Date());
			 parameterControl.setTsftFlag(false);
			 parameterControl.setHybridFlag(false);
			
	 }
	 String parameter=parameterControl.getParameter();
	 if(parameter==null){
		 parameter="";
	 }
	 childParameter=parameterControlManager.getAllParameter(parameter);
	 parameterControl.setUpdatedOn(new Date());
	 parameterControl.setUpdatedBy(getRequest().getRemoteUser());
	 return SUCCESS;
	 
 }
 public String save() throws Exception{
	 boolean isNew=(parameterControl.getId()==null);
	 if (isNew) { 
		 parameterControl.setCreatedOn(new Date());
     } 
	 parameterControl.setCorpID(sessionCorpID);
	 parameterControl.setUpdatedOn(new Date());
	 parameterControl.setUpdatedBy(getRequest().getRemoteUser());
	 parameterControlManager.save(parameterControl);
	 String key = (isNew) ? "ParameterControl has been added successfully" : "ParameterControl has been updated successfully";
	 saveMessage(getText(key));
	 return SUCCESS;
	 
 }
 public String delete(){
	 parameterControlManager.remove(id);
	 return SUCCESS;
	 
 }
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public ParameterControl getParameterControl() {
	return parameterControl;
}
public void setParameterControl(ParameterControl parameterControl) {
	this.parameterControl = parameterControl;
}
public List<ParameterControl> getParameterControls() {
	return parameterControls;
}
public void setParameterControls(List<ParameterControl> parameterControls) {
	this.parameterControls = parameterControls;
}
public String getSessionCorpID() {
	return sessionCorpID;
}
public void setSessionCorpID(String sessionCorpID) {
	this.sessionCorpID = sessionCorpID;
}
public void setParameterControlManager(
		ParameterControlManager parameterControlManager) {
	this.parameterControlManager = parameterControlManager;
}
public ExtendedPaginatedList getParameterControlsExt() {
	return parameterControlsExt;
}
public void setParameterControlsExt(ExtendedPaginatedList parameterControlsExt) {
	this.parameterControlsExt = parameterControlsExt;
}
public PaginateListFactory getPaginateListFactory() {
	return paginateListFactory;
}
public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
	this.paginateListFactory = paginateListFactory;
}
public void setPagingLookupManager(PagingLookupManager pagingLookupManager) {
	this.pagingLookupManager = pagingLookupManager;
}
public List getChildParameter() {
	return childParameter;
}
public void setChildParameter(List childParameter) {
	this.childParameter = childParameter;
}

}
