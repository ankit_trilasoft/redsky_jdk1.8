package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SoAdditionalDate;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.SoAdditionalDateManager;
import com.trilasoft.app.service.TrackingStatusManager;

public class SoAdditionalDateAction extends BaseAction{
	private Long id;
	private String sessionCorpID;
	private SoAdditionalDateManager soAdditionalDateManager;
	private SoAdditionalDate soAdditionalDate;
	private ServiceOrder serviceOrder;
	private ServiceOrderManager serviceOrderManager;
	private CustomerFile customerFile;
	private MiscellaneousManager miscellaneousManager;
	private BillingManager billingManager;
	private Miscellaneous miscellaneous;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private Billing billing;
	private CustomerFileManager customerFileManager;
	
	public SoAdditionalDateAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 		
	}
	
	private String shipSize;
	private String minShip;
	private String countShip;
	private Long sid;
	public String edit() {
		serviceOrder = serviceOrderManager.get(sid);
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus=trackingStatusManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		List soAddDateList = soAdditionalDateManager.findId(sid,sessionCorpID);
		if(soAddDateList!=null && !soAddDateList.isEmpty()&& soAddDateList.get(0)!=null && !soAddDateList.get(0).toString().equalsIgnoreCase("")){
			id = Long.parseLong(soAddDateList.get(0).toString());
		}else{
			id = null;
		}
		if (id != null) {
			soAdditionalDate = soAdditionalDateManager.get(id);
		} else {			
			soAdditionalDate = new SoAdditionalDate(); 
			soAdditionalDate.setCreatedOn(new Date());
			soAdditionalDate.setUpdatedOn(new Date());
			soAdditionalDate.setLoadTimeFrom("00:00");
			soAdditionalDate.setLoadTimeTo("00:00");
			soAdditionalDate.setDeliveryTimeFrom("00:00");
			soAdditionalDate.setDeliveryTimeTo("00:00");
			soAdditionalDate.setPossessionTime("00:00");
		} 
		List shipSizeList= customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",serviceOrder.getCorpID());
		if(shipSizeList!=null && (!(shipSizeList.isEmpty())) && shipSizeList.get(0)!=null){
			shipSize = shipSizeList.get(0).toString();
		}
		List minShipList=customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID());
		if(minShipList!=null && (!(minShipList.isEmpty())) && minShipList.get(0)!=null){
			minShip =  minShipList.get(0).toString();
		}
		List countShipList=customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()); 
		if(countShipList!=null && (!(countShipList.isEmpty())) && countShipList.get(0)!=null){
			countShip=countShipList.get(0).toString();
		}
		return SUCCESS;
	}
	private String additionalDateURL;
	public String save() throws Exception {
		serviceOrder = serviceOrderManager.get(sid);
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus=trackingStatusManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		additionalDateURL = "?sid="+sid;
		boolean isNew = (soAdditionalDate.getId() == null);  
				if (isNew) { 
					soAdditionalDate.setCreatedOn(new Date());
					soAdditionalDate.setCreatedBy(getRequest().getRemoteUser());
		        } 
				soAdditionalDate.setServiceOrderId(sid);
				soAdditionalDate.setCorpID(sessionCorpID);
				soAdditionalDate.setUpdatedOn(new Date());
				soAdditionalDate.setUpdatedBy(getRequest().getRemoteUser()); 
				soAdditionalDateManager.save(soAdditionalDate);  
			    String key = (isNew) ?"Additional Dates have been saved." :"Additional Dates have been updated." ;
			    saveMessage(getText(key)); 
				return SUCCESS;			
	}

	public void setSoAdditionalDateManager(
			SoAdditionalDateManager soAdditionalDateManager) {
		this.soAdditionalDateManager = soAdditionalDateManager;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public SoAdditionalDate getSoAdditionalDate() {
		return soAdditionalDate;
	}

	public void setSoAdditionalDate(SoAdditionalDate soAdditionalDate) {
		this.soAdditionalDate = soAdditionalDate;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}
	public String getAdditionalDateURL() {
		return additionalDateURL;
	}
	public void setAdditionalDateURL(String additionalDateURL) {
		this.additionalDateURL = additionalDateURL;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public String getShipSize() {
		return shipSize;
	}

	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}

	public String getMinShip() {
		return minShip;
	}

	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}

	public String getCountShip() {
		return countShip;
	}

	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}

}
