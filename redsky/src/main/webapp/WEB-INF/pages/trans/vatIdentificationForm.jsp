<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<head>
<title>Vat Identification Details</title>
</head>
<s:form id="vatIdentificationForm" action="saveVatIdentification" method="post" validate="true">
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="vatIdentification.id" value="%{vatIdentification.id}"/>
<div id="layer1" style="width:100%; margin-top: 10px;">
<div id="newmnav">
			<ul>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Vat Identification Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
             <li><a href="vatIdentificationList.html" ><span>Vat Identification List</span></a></li>
           </ul>
       </div><div class="spn" >&nbsp;</div>
  <div id="Layer5" style="width:100%" onkeydown="changeStatus();">
  
  <div id="content" align="center">
<div id="liquid-round">
<div class="top" style="!margin-top:4px;"><span></span></div>
   <div class="center-content">  
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="3" width="95%" >
<tr>
<td align="right" class="listwhitetext" width="70px">Country</td>
<td align="left" ><s:select cssClass="list-menu" name="vatIdentification.countryCode"  list="%{country}" cssStyle="width:155px"  headerKey="" headerValue="" tabindex="6" /></td>
 <td></td>
 <td align="right" class="listwhitetext" width="70px">Type</td> 
 <td align="left" class="listwhitetext" width="157px"><s:textfield name="vatIdentification.dataType" maxlength="200"  required="true" cssClass="input-text" size="25" tabindex="5"  /></td>
 <td></td>
 <td align="right" class="listwhitetext" width="105px">Number Of Block</td> 
 <td align="left" class="listwhitetext" width="280px"><s:textfield name="vatIdentification.numberOfBlock" maxlength="15"  required="true" cssClass="input-text" size="25" tabindex="5"  />
 <font size="1px" color="#808080" >Separate&nbsp;by&nbsp;comma(,).</font>
 </td>
 <td></td>
 </tr>
 <tr>
 <td align="right" class="listwhitetext" >Start With</td> 
 <td align="left" class="listwhitetext" ><s:textfield name="vatIdentification.startWith" maxlength="2"  required="true" cssClass="input-text" size="25" tabindex="5"  /></td>
  <td></td>
 <td align="right" class="listwhitetext" >End With</td> 
 <td align="left" class="listwhitetext" ><s:textfield name="vatIdentification.endWith" maxlength="2"  required="true" cssClass="input-text" size="25" tabindex="5"  /></td>
<td></td>
<td align="right" class="listwhitetext" >Message</td> 
 <td align="left" class="listwhitetext" valign="top" rowspan="4"><s:textarea name="vatIdentification.message" rows="2" cols="44"  required="true" cssClass="textarea"  tabindex="5"  /></td>
 <td></td>
 </tr>
 <tr>
 <td align="right" class="listwhitetext" >Each Block</td> 
 <td align="left" class="listwhitetext" ><s:textfield name="vatIdentification.eachBlock" maxlength="2"  required="true" cssClass="input-text" size="25" tabindex="5"  /></td>
 <td></td>
 <td align="right" class="listwhitetext" >Block Value</td> 
  <td align="left" class="listwhitetext" ><s:textfield name="vatIdentification.blockValue" maxlength="20"  required="true" cssClass="input-text" size="25" tabindex="5"  /></td>
   </tr>
   <tr>
   <td colspan="4"></td>
   <td colspan="5"><font size="1px" color="#808080" >Block separate by slash(/) and block value separate by comma(,).</font>
  </td>
   </tr>
 </table>
 <div style="height:20px;"></div>
 </div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>
<table width="700px">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${vatIdentification.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="vatIdentification.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${vatIdentification.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>						
						<c:if test="${not empty vatIdentification.id}">
								<s:hidden name="vatIdentification.createdBy"/>
								<td ><s:label name="createdBy" value="%{vatIdentification.createdBy}"/></td>
							</c:if>
							<c:if test="${empty vatIdentification.id}">
								<s:hidden name="vatIdentification.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${vatIdentification.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="vatIdentification.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${vatIdentification.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty vatIdentification.id}">
							<s:hidden name="vatIdentification.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{vatIdentification.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty vatIdentification.id}">
							<s:hidden name="vatIdentification.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
			</div> 
			<table><tr><td>   
        <s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" method="save" key="button.save" theme="simple" />
        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" onclick="setState();"/>
	</td></tr></table>
	</div>
</s:form>
<script type="text/javascript">
<c:if test="${flage=='1'}">
<c:redirect url="/vatIdentificationList.html"/>
</c:if>
</script>
