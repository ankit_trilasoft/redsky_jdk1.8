<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<script type="text/javascript">
<!--

//-->
</script>

<script>
function getContract(targetElement) {
	var tableNames = targetElement.value;
	// alert(tableNames);
	var url="findfieldList.html?ajax=1&decorator=simple&popup=true&tableNames=" + encodeURI(tableNames);
    // alert(url);
    	http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse3;
     http2.send(null);
	
	}


function handleHttpResponse3()
        {
            
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                // alert(results);
                res = results.split("@");
                
                targetElement = document.forms['systemConfigurationForm'].elements['systemConfiguration.fieldName'];
				targetElement.length = res.length;
				
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					
					document.forms['systemConfigurationForm'].elements['systemConfiguration.fieldName'].options[i].text = '';
					document.forms['systemConfigurationForm'].elements['systemConfiguration.fieldName'].options[i].value = '';
					}else{
					
					// contractVal = res[i].split("#");
					document.forms['systemConfigurationForm'].elements['systemConfiguration.fieldName'].options[i].text = res[i];
					document.forms['systemConfigurationForm'].elements['systemConfiguration.fieldName'].options[i].value = res[i];
					}
					}
					
             }
           
        }
        
        
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();   
</script>
<head>   
    <title><fmt:message key="systemConfigurationForm.title"/></title>   
    <meta name="heading" content="<fmt:message key='systemConfigurationForm.heading'/>"/>   
</head>

<s:form id="systemConfigurationForm" name="systemConfigurationForm" action="systemsave" method="post" validate="true">
<s:hidden name="systemConfiguration.id" value="%{systemConfiguration.id}"/>
<s:hidden name="parameter" value="<%=request.getParameter("parameter")%>" />
<c:set var="systemConfiguration.id" value="%{systemConfiguration.id}" scope="session"/>

<div id="Layer1" style="width:374px">
		<div id="newmnav">
		  <ul>
		    <li style="background:#FFF "><a class="current"><span>System Configuration<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a href="systemlist.html"><span>System Configuration List</span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div><br>
<!--  
<div id="newmnav">
  <ul>
    <li style="background:#FFF "><a href="editsystem.html" class="current" ><span>System Configuration Details<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
    <li><a href="systemlist.html"><span>System Configuration</span></a></li>
</ul>
</div><div class="spn">&nbsp;</div><br>
-->
<table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
	<tr>
	<td height="10" align="left" class="listwhitetext">
		<table  class="detailTabLabel" border="0">
 		  <tbody>
 	
 			<tr><td align="left" height="15px"></td></tr>
			<tr>
				<td align="right" style="width:100px"><fmt:message key="systemConfiguration.corpID"/></td>
				<td align="left" colspan="2"><s:textfield name="systemConfiguration.corpID" readonly="true" onkeydown="return onlyCharsAllowed(event)" value="${sessionCorpID}" required="true" cssClass="input-text" size="16" maxlength="17"/></td>
		    </tr>
			<tr>
				<td align="right" style="width:100px"><fmt:message key="systemConfiguration.tableName"/></td>
				<td align="left" colspan="2"><s:select list="%{tableList}" onchange="getContract(this);" cssStyle="width:110px" headerKey=" " headerValue=" "name="systemConfiguration.tableName" /></td>
		    </tr>
		    <tr>	
		    	<td align="right"><fmt:message key="systemConfiguration.fieldName"/></td>
		    	<td align="left" colspan="5"><s:select list="%{fieldList}"  cssStyle="width:110px" headerKey=" " headerValue=" " name="systemConfiguration.fieldName" /></td>
		   	</tr>
		      
			</tbody>
		</table>
		</td></tr>
		
		<tr>
		<table class="detailTabLabel" border="0" style="width:550px">
		   <tbody>
					
					<tr>
						<td align="right"  class="listwhitetext"><fmt:message key='customerFile.createdOn'/></td>
						<s:hidden name="systemConfiguration.createdOn" />
						<td  align="left" ><s:date name="systemConfiguration.createdOn" format="dd-MMM-yyyy HH:mm"/></td>		
						<td align="right" class="listwhitetext"><fmt:message key='customerFile.createdBy' /></td>
						<c:if test="${not empty systemConfiguration.id}">
										<s:hidden name="systemConfiguration.createdBy" />
										<td><s:label name="createdBy"
											value="%{systemConfiguration.createdBy}" /></td>
									</c:if>
									<c:if test="${empty systemConfiguration.id}">
										<s:hidden name="systemConfiguration.createdBy"
											value="${pageContext.request.remoteUser}" />
										<td><s:label name="createdBy"
											value="${pageContext.request.remoteUser}" /></td>
									
									</c:if>
						
						
						<td align="right" class="listwhitetext"><fmt:message key='customerFile.updatedOn'/></td>
						<s:hidden name="systemConfiguration.updatedOn"/>
						<td><s:date name="systemConfiguration.updatedOn" format="dd-MMM-yyyy HH:mm"/></td>
						<td align="right" class="listwhitetext"><fmt:message key='customerFile.updatedBy' /></td>
						<s:hidden name="systemConfiguration.updatedBy" value="${pageContext.request.remoteUser}"/>
						<td><s:label name="customerFile.updatedBy" value="${pageContext.request.remoteUser}"/></td>
					</tr>
				</tbody>
			</table>
			
		<table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>
				<td></td>
				<td align="left">
				<s:submit cssClass="cssbuttonA" cssStyle="width:55px; height:25px" method="save" key="button.save"/>
				<input type="button" class="cssbutton1" style="width:55px; height:25px" value="Add" style="width:87px; height:26px" onclick="location.href='<c:url value="/editsystem.html"/>'" /></td>
			
			   <c:if test="${not empty systemConfiguration.id}">
				<td align="left"><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="delete" key="button.delete" /></td>
			</c:if>
			</tr>
		</tr>
		
		</tr>		  	
		  </tbody>
		  </table>
		
		</tbody></table>
		
		
																 
<s:hidden name="tableNames"/>
	</div>	    
</s:form>  

<script type="text/javascript">   
    highlightTableRows("systemConfigurationForm");   
</script>