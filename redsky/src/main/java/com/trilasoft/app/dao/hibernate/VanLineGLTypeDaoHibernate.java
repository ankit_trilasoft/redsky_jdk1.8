package com.trilasoft.app.dao.hibernate;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.VanLineGLTypeDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.VanLineGLType;


public class VanLineGLTypeDaoHibernate extends GenericDaoHibernate<VanLineGLType, Long> implements VanLineGLTypeDao {

	private HibernateUtil hibernateUtil;
	
	public VanLineGLTypeDaoHibernate() {   
        super(VanLineGLType.class);   
    }
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
    }
	
	static String replaceWord(String original, String find, String replacement) {
		int i = original.indexOf(find);
		if (i < 0) {
			return original;
		}
		String partBefore = original.substring(0, i);
		String partAfter = original.substring(i + find.length());
		return partBefore + replacement + partAfter;
	}

	public List<VanLineGLType> vanLineGLTypeList(){
		List vanLineGLTypeList = null;
        String columns = "id,glType, description, glCode, discrepancy, shortHaul, bucket";
        vanLineGLTypeList = getHibernateTemplate().find("select " + columns + " from VanLineGLType");
        try {
        	vanLineGLTypeList = hibernateUtil.convertScalarToComponent(columns, vanLineGLTypeList, VanLineGLType.class);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error executing query"+ e,e);
        }
		return vanLineGLTypeList;
	}
	
	public List<VanLineGLType> searchVanLineGLType(String glType, String description, String glCode){
		List vanLineGLTypeList = null;		
        String columns = "id,glType, description, glCode, discrepancy, shortHaul, bucket";
        vanLineGLTypeList = getHibernateTemplate().find("select " + columns + " from VanLineGLType where trim(glType) like '" + glType.replaceAll("'", "''").replaceAll(":", "''").trim() + "%' AND trim(description) like '" +description.replaceAll("'", "''").replaceAll(":", "''").trim() + "%' AND trim(glCode) like '" +glCode.replaceAll("'", "''").replaceAll(":", "''").trim() + "%'");
        try {
        	vanLineGLTypeList = hibernateUtil.convertScalarToComponent(columns, vanLineGLTypeList, VanLineGLType.class);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error executing query"+ e,e);
        }
		return vanLineGLTypeList;
		
	}
	
	public Map<String, String> findByParameter(String corpId) {
		String[] params = {corpId};
		
		List<VanLineGLType> list =  getHibernateTemplate().find("from VanLineGLType where corpID in ('" + corpId + "','XXXX','TSFT','NULL') ORDER BY description");
		
		Map<String, String> parameterMap = new TreeMap<String, String>();
		
		for(VanLineGLType vanLineGLType : list){
			parameterMap.put(vanLineGLType.getGlType(), vanLineGLType.getGlType()+ " : " +vanLineGLType.getDescription());
			
		}
		return parameterMap;
	} 
	
	public Map<String, String> findGlCode(String corpId) {
		String[] params = {corpId};
		try{
		List<VanLineGLType> list =  getHibernateTemplate().find("from VanLineGLType where corpID in ('" + corpId + "','XXXX','TSFT','NULL') ORDER BY description");
		
		Map<String, String> parameterMap = new TreeMap<String, String>();
		
		for(VanLineGLType vanLineGLType : list){
			parameterMap.put(vanLineGLType.getGlCode(), vanLineGLType.getGlCode());
			
		}
		return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	} 
	
	public List isExisted(String code, String corpId) {
		try{
		return getHibernateTemplate().find("from VanLineGLType where lower(glType) = lower('"+code+"') and corpID='"+corpId+"'");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
  
	public List<VanLineGLType> getGLPopupList(String chargesGl){
		try{
			return getHibernateTemplate().find("from VanLineGLType where glCode=?",chargesGl);
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
		
	}


}
