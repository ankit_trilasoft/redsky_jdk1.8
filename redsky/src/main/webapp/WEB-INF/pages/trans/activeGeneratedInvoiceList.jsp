<%@ include file="/common/taglibs.jsp"%>
<%@page import="java.util.*" %>
<%@page import="java.text.*" %>
<title>Active SO List</title>
<meta name="heading" content="Active SO List" />
<% 
	Date date11 = new Date();
	SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd");
	String dt11=sdfDestination.format(date11);
%>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
#loader {filter:alpha(opacity=70);-moz-opacity:0.7;-khtml-opacity: 0.7;opacity: 0.7;position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
	</style>
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>	

</head>
<s:form id="activeWorkTicketList" name="activeWorkTicketList" action="" onsubmit="" method="post">
<configByCorp:fieldVisibility componentId="component.field.BatchPayable.VoerCMMDMM"> 
<s:hidden name="batchInvoiceNonCMMDMMFlag" value="YES" />
</configByCorp:fieldVisibility>
<s:hidden name="sidGeneratedInvoice" />
<s:hidden name="pir"/>
<s:hidden name="billingCurrency"/>
<s:hidden name="invoiceFlag"/>
<s:hidden name="sumActualRevenue"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="billToCodeForTicket"/>
<s:hidden name="companyDivisionForTicket"/>
<s:hidden name="assignInvoiceButton" value="<%=request.getParameter("assignInvoiceButton")%>"/>
<s:hidden name="assignInvoiceNumber"/>
<s:hidden name="userCheck"/>
<s:hidden name="userUncheck"/>
<s:hidden name="collectiveInvoiceNumber"/>
<s:hidden name="shipNumberForTickets" value="<%=request.getParameter("shipNumberForTickets")%>"/>  
<s:hidden name="accountInterface" value="${accountInterface}"/>
<configByCorp:fieldVisibility componentId="component.accountLine.receivedInvoiceDate"> 
<s:hidden name="recInvoiceDateToFormat1"/>
</configByCorp:fieldVisibility>
<s:hidden name="recAccDateForm"/>  
<div id="mydiv" style="position:absolute"></div>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="bookingAgent" style="align:center;">

<table border="0" class="mainDetailTable" cellpadding="0" cellspacing="0" style="width:510px;margin-bottom:2px;"> 
	<tr>
	<td>
	<table  border="0" align="left"  cellpadding="3" cellspacing="0" class="detailTabLabel" width="">
	<tr>
	<td align="left" colspan="3" class="listwhitetext"><b>This Service Order will be Processed for the following information: </b></td>  
	</tr>
	<tr><td height="10px"></td></tr>	
	<tr>
	<td align="left" class="listwhitetext" colspan=""><b>The invoice lines company division is/are:</b></td>
	<td width="100px"  class="listwhitetext" ><s:textfield cssClass="input-text" cssStyle="text-align:left" name="companyDivisionList" size="25" tabindex="9" readonly="true"/></td>  
	</tr>
	<tr><td height="10px"></td></tr>
	<tr>
	<td align="left" class="listwhitetext" width=""><b>The BillTocode Code for these SO List is/are: </b></td>  
	<td width="100px"><s:textfield cssClass="input-text" cssStyle="text-align:left" name="billCodeList" size="25" tabindex="9" readonly="true"/></td>
	</tr>
	<tr><td height="10px"></td></tr>
	<c:if test="${checkFieldVisibilityOnGenerateInvoice ==true}">
    <c:forEach var="reportParameter" items="${reportParameters}">
	<tr>
		<td  align="left"  class="listwhitetext">&nbsp;&nbsp;&nbsp;<b><c:out value="${reportParameter.name}"/></b> :</td>
		
	 	<td  align="left"  class="listwhitetext">
	 		<s:hidden cssClass="input-textUpper"  name="reportParameterName" value="${reportParameter.name}"  tabindex="" />
	 		<s:select cssClass="list-menu" name="reportParameterValue" list="%{inputParameters}" value="%{printOptionsValue}" headerKey="" headerValue="" cssStyle="width:150px" >
	 		</s:select>
	 	</td>
	</tr>
	</c:forEach>
	</c:if>
	<tr><td height="10px"></td></tr>	
	<tr>	
	<td align="left" class="listwhitetext" width=""><b>Is the above information correct?</b></td>	
	</tr></table> 
	</td></tr>	
	</table>
	<table style="margin:0px;padding:0px;">
	<tr>	
	<td align="left" class="listwhitetext" width=""></td>
	<td> <input type="button" class="cssbutton1"  value="Yes" name="yes" onclick="cheackBookingAgent();"/></td> 
     <td><input type="button" class="cssbutton1" value="No" onclick="companyMessage();"/>   
     </td>
	</tr>
	</table>
		</div>	
	<div id="totalhid" style="margin-left:100px;">
	<table  border="0" align="center" style="margin:0px;padding:0px;" cellpadding="2" cellspacing="2"  >
	<tbody>
	<tr> 
		<td align="right"  class="listwhitebox" width="">Posting&nbsp;Date </td>		
		<c:if test="${recAccDateToFormat!=''&& recAccDateToFormat!=null}">
		<s:text id="recAccDateToFormat" name="${FormDateValue}"><s:param name="value" value="recAccDateToFormat"/></s:text>
		<td align="left"><s:textfield cssClass="input-textUpper" id="recAccDateToFormat"  name="recAccDateToFormat" value="%{recAccDateToFormat}"  size="8" maxlength="11"  readonly="true"/><img id="recAccDateToFormat_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<c:if test="${recAccDateToFormat==''|| recAccDateToFormat==null}">
		<td align="left"><s:textfield cssClass="input-textUpper" id="recAccDateToFormat" name="recAccDateToFormat" size="8" maxlength="11"  readonly="true"/><img id="recAccDateToFormat_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.accountLine.receivedInvoiceDate"> 
		<td align="right"  class="listwhitebox" width="64px">Invoice&nbsp;Date</td>
		<c:if test="${recInvoiceDateToFormat!=''&& recInvoiceDateToFormat!=null}">
		<s:text id="recInvoiceDateToFormat" name="${FormDateValue}"><s:param name="value" value="recInvoiceDateToFormat"/></s:text>
		<td width="130px" ><s:textfield cssClass="input-textUpper" id="recInvoiceDateToFormat"  name="recInvoiceDateToFormat" value="%{recInvoiceDateToFormat}"  size="8" maxlength="11"  readonly="true"/><img id="recInvoiceDateToFormat_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<c:if test="${recInvoiceDateToFormat==''||recInvoiceDateToFormat==null}">
		<td width="130px"><s:textfield cssClass="input-textUpper" id="recInvoiceDateToFormat" name="recInvoiceDateToFormat" size="8" maxlength="11"  readonly="true"/><img id="recInvoiceDateToFormat_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
	</tr>
		<tr><td height="5px" colspan="5"></td></tr>	
	<tr>
		<c:if test="${sumActualRevenue >= 0}">
        	<s:hidden name="creditInvoiceNumber"value=""/>
        </c:if>	
		<configByCorp:fieldVisibility componentId="component.accountLine.utsi.collectiveInvoice">
		<td align="left">
		 <c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
		 <input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:120px; height:25px;margin-left:10px;" value="Batch Invoicing" onclick="checkFormDate('generate');" onmouseover="calcDateFlexibility();" />
		 </c:if>
		  <c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
		 <input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:120px; height:25px;margin-left:10px;" value="Batch Invoicing" onclick="checkFormDate('generate');" onmouseover="calcDate();" />
		</c:if>
		</td>
		
		<td align="left">		
			<c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
	        <input type="button" class="cssbutton1" id="addLine1" name="addLine1" style="width:120px; height:25px;margin-left:10px;" value="Collective Invoice" onclick="checkFormDate('collective');" onmouseover="calcDateFlexibility();" />
			</c:if>
			  <c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
			<input type="button" class="cssbutton1" id="addLine1" name="addLine1" style="width:120px; height:25px;margin-left:10px;" value="Collective Invoice" onclick="checkFormDate('collective');" onmouseover="calcDate();" />
			</c:if>		
		</td>
		<td align="left" colspan="2"><input type="button" class="cssbutton1" id="assignLinestoExistingInvoice" style="width:200px;margin-left:10px;" value="Assign Lines to Existing Invoice(s)" name="Assign Lines to Existing Invoice(s)" onclick="findAssignInvoice();"/></td>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.accountLine.voer.collectiveInvoice">
			<td align="center" colspan="4">
			 <c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
			 <input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:120px; height:25px;margin-left:10px;" value="Batch Invoice" onclick="checkFormDate('generate');" onmouseover="calcDateFlexibility();" />
			 </c:if>
			  <c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
			 <input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:120px; height:25px;margin-left:10px;" value="Batch Invoice" onclick="checkFormDate('generate');" onmouseover="calcDate();" />
			</c:if>
			</td>		
		</configByCorp:fieldVisibility>
		</tr>		
		</tbody>
		</table>
	</div>
		<s:hidden  id="recAccDateDammy"  name="recAccDateDammy" value="%{recAccDateToFormat}" />
		<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
		<s:set name="btntype" value="<%=request.getParameter("btntype") %>"/>
		
	<div id="assignInvoiceDiv" style="align:center" >
		<table  border="0"><tr><td width="20%"></td><td>
		<display:table name="assignAllInvoiceList" class="table" requestURI="" id="assignAllInvoiceList" style="width:100%;margin-top:10px;"  pagesize="100" >   
		 		<display:column property="recInvoiceNumber" sortable="true" title="Invoice #" style="width:70px" />
				<display:column property="receivedInvoiceDate" sortable="true" title="Dated" style="width:70px" />
				<display:column property="billtocode" sortable="true" title="BilltoCode" style="width:70px" />
				<display:column property="billedAmount" sortable="true" title="Billed Amount" style="width:70px" />
				<display:column property="unbilledAmount" sortable="true" title="Unbilled Amount" style="width:70px" /> 
				<display:column   title="" style="width:10px" >
				<c:if test="${assignAllInvoiceList.recInvoiceNumber!=''}">
		 		<input style="vertical-align:bottom;" type="radio" name="radiobilling" id=${assignAllInvoiceList.recInvoiceNumber} value=${assignAllInvoiceList.recInvoiceNumber} onchange="setInvoiceNumber('${assignAllInvoiceList.recInvoiceNumber}')"/>
		 		</c:if>
		 		</display:column> 
		</display:table>  
		</td></tr>
		<tr><td width="20%"></td><td>
		<!-- <input type="button" name="Submit" onclick="return fillAssignInvoice();" value="Add Lines To Invoice" class="cssbuttonA" style="width:170px; height:25px" > -->
		<input type="button" class="cssbuttonA"  value="Add Lines To Collective" style="width:170px; height:25px" onclick="checkFormDate1('collective');"/>  </td>
		</table>
	</div>
	
	<div id="loader" style="text-align:center; display:none">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
		<tr>
			<td align="center">
				<table cellspacing="0" cellpadding="3" align="center">
					<tr><td height="200px"></td></tr>
					<tr>
				       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
				           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
				       </td>
				    </tr>
				    <tr>
				      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
				           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
				       </td>
				    </tr>
		       </table>
		     </td>
	  	</tr>
	</table>
</div>
</s:form>
<%-- Script Shifted from Top to Botton on 10-Sep-2012 By Kunal --%>

<%-- Modified By Kunal Sharma at 13-Jan-2012 --%> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<%-- Modification closed here --%>
<SCRIPT LANGUAGE="JavaScript">
function populateParentData(){
	  document.forms['activeWorkTicketList'].elements['sidGeneratedInvoice'].value=window.opener.document.forms['collectiveBilling'].elements['collectiveInvoiceSoId'].value;
}
populateParentData();
function autoPopulate_recievedDate() {
	try{
		<configByCorp:fieldVisibility componentId="component.accountLine.receivedInvoiceDate">
			var mydate=new Date();
			var daym;
			var year=mydate.getFullYear()
			var y=""+year;
			if (year < 1000)
			year+=1900
			var day=mydate.getDay()
			var month=mydate.getMonth()+1
			if(month == 1)month="Jan";
			if(month == 2)month="Feb";
			if(month == 3)month="Mar";
			if(month == 4)month="Apr";
			if(month == 5)month="May";
			if(month == 6)month="Jun";
			if(month == 7)month="Jul";
			if(month == 8)month="Aug";
			if(month == 9)month="Sep";
			if(month == 10)month="Oct";
			if(month == 11)month="Nov";
			if(month == 12)month="Dec";
			
			var daym=mydate.getDate()
			if (daym<10)
				daym="0"+daym;
			var datam = daym+"-"+month+"-"+y.substring(2,4);	
			document.forms['activeWorkTicketList'].elements['recInvoiceDateToFormat'].value=datam;
		</configByCorp:fieldVisibility>
	}catch(e){}
}
function showHide(action){
	document.getElementById("loader").style.display = action;
}
function checkFormDate(invoiceFlag)
{ 
  var fdate = document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value;
  document.forms['activeWorkTicketList'].elements['recAccDateForm'].value=fdate;
  if(fdate=='')
  {
 	  document.forms['activeWorkTicketList'].elements['recAccDateForm'].value=''; 
  }
  <configByCorp:fieldVisibility componentId="component.accountLine.receivedInvoiceDate"> 
	  var fdate1 = document.forms['activeWorkTicketList'].elements['recInvoiceDateToFormat'].value;
	  document.forms['activeWorkTicketList'].elements['recInvoiceDateToFormat1'].value=fdate1;
	  if(fdate1==''){
	  	 document.forms['activeWorkTicketList'].elements['recInvoiceDateToFormat1'].value=''; 
	  } 
  </configByCorp:fieldVisibility>
  showHide("block");
    document.forms['activeWorkTicketList'].elements['invoiceFlag'].value=invoiceFlag;
    document.forms['activeWorkTicketList'].action ='generateInvoiveForServiceOrderList.html?btntype=yes&decorator=popup&popup=true';
    document.forms['activeWorkTicketList'].submit();
}


function calcDateFlexibility(){
	  if(document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value==''||document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value=='') {
	  }else{
		     var date4 = document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value; 
			 var date1 = document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value; 
			 var date1SplitResult = date1.split("-");
			 var day1 = date1SplitResult[0];
			 var month1 = date1SplitResult[1];
			 var year1 = date1SplitResult[2];
			   	 year1 = '20'+year1;
				 if(month1 == 'Jan'){ month1 = "01";  }
		    else if(month1 == 'Feb'){ month1 = "02";  }
			else if(month1 == 'Mar'){ month1 = "03";  }
			else if(month1 == 'Apr'){ month1 = "04";  }
			else if(month1 == 'May'){ month1 = "05";  }
			else if(month1 == 'Jun'){ month1 = "06";  }
			else if(month1 == 'Jul'){ month1 = "07";  }
			else if(month1 == 'Aug'){ month1 = "08";  }
			else if(month1 == 'Sep'){ month1 = "09";  }
			else if(month1 == 'Oct'){ month1 = "10";  }																					
			else if(month1 == 'Nov'){ month1 = "11";  }
			else if(month1 == 'Dec'){ month1 = "12";  }
					
			 var date2 = document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value; 
			 var date2SplitResult = date2.split("-");
			 var day2 = date2SplitResult[0];
			 var month2 = date2SplitResult[1];
			 var year2 = date2SplitResult[2];
			   	 year2 = '20'+year2;
				 if(month2 == 'Jan'){ month2 = "01";  }
		    else if(month2 == 'Feb'){ month2 = "02";  }
			else if(month2 == 'Mar'){ month2 = "03";  }
			else if(month2 == 'Apr'){ month2 = "04";  }
			else if(month2 == 'May'){ month2 = "05";  }
			else if(month2 == 'Jun'){ month2 = "06";  }
			else if(month2 == 'Jul'){ month2 = "07";  }
			else if(month2 == 'Aug'){ month2 = "08";  }
			else if(month2 == 'Sep'){ month2 = "09";  }
			else if(month2 == 'Oct'){ month2 = "10";  }																					
			else if(month2 == 'Nov'){ month2 = "11";  }
			else if(month2 == 'Dec'){ month2 = "12";  }

		    var currentD = '<%= dt11 %>';
		    var date3SplitResult = currentD.split("-");
	 		var day3 = date3SplitResult[2];
	 		var month3 = date3SplitResult[1];
	 		var year3 = date3SplitResult[0];

	 		var selectedDate = new Date(month1+"/"+day1+"/"+year1);
	 		var sysDefaultDate = new Date(month2+"/"+day2+"/"+year2);		
	 		var currentDate = new Date(month3+"/"+day3+"/"+year3);
	 		  var daysApart = Math.round((selectedDate-currentDate)/86400000);
	 		  var daysApartNew = Math.round((sysDefaultDate-currentDate)/86400000);
	 			var daysApartCurrent = Math.round((sysDefaultDate-selectedDate)/86400000);
		 		  if(daysApartNew<=0){
			 		  if((daysApart<0)&&(daysApartCurrent!=0)){
				 			 document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value=date4;
				 			document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].focus();
				 		    alert("You cannot enter posting date less than the current date."); 
				 		  }
		 		  }else{
			 		  if(daysApart<0){
			 				 document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value=date4;
		 					document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].focus();
		 			    	alert("You cannot enter posting date less than the current date."); 
		 		  			}
		 		  }
	 		  /*else{
	 	 		  if((parseInt(month1)>=parseInt(month2))&&(parseInt(year1)>=parseInt(year2))){
	 	 		  }else{
	 	  		    alert("You can not enter Posting Date less than System Default current month"); 
	 	 		    document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value=date4;
	 	 		  }  
	 		  } */
	  }
}
function calcDate()
{  
	
if(document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value==''||document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value=='')
{
	alert(document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value);
	alert(document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value);
		
//alert('in if date blank')
}
else
{ 

var date2 = document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value; 
var date1 = document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value; 
var date3 = document.forms['activeWorkTicketList'].elements['recAccDateDammy'].value;
 var mySplitResult = date1.split("-");
 var day = mySplitResult[0];
 var month = mySplitResult[1];
 var month1 = 0;
 var month2 = month;
 var year = mySplitResult[2];
	
if(month == 'Jan')
 {
     month = "01";
     month1 = 1;
 }
 else if(month == 'Feb')
 {
     month = "02";
     month1 = 2;
 }
 else if(month == 'Mar')
 {
     month = "03"
     month1 = 3;
 }
 else if(month == 'Apr')
 {
     month = "04"
     month1 = 4;
 }
 else if(month == 'May')
 {
     month = "05"
     month1 = 5;
 }
 else if(month == 'Jun')
 {
     month = "06"
     month1 = 6;
 }
 else if(month == 'Jul')
 {
     month = "07"
     month1 = 7;
 }
 else if(month == 'Aug')
 {
     month = "08"
     month1 = 8;
 }
 else if(month == 'Sep')
 {
     month = "09"
     month1 = 9;
 }
 else if(month == 'Oct')
 {
     month = "10"
     month1 = 10;
 }
 else if(month == 'Nov')
 {
     month = "11"
     month1 = 11;
 }
 else if(month == 'Dec')
 {
     month = "12";
     month1 = 12;
 }
 var year1 = '20'+year;
 var day1 = getLastDayOfMonth(month1,year1);

 var date4 = day1+"-"+month2+"-"+year; 
 var finalDate = month+"-"+day+"-"+year;
 var mySplitResult2 = date2.split("-");
 var day2 = mySplitResult2[0];
 var month2 = mySplitResult2[1];
 var year2 = mySplitResult2[2];
 if(month2 == 'Jan')
 {
     month2 = "01";
 }
 else if(month2 == 'Feb')
 {
     month2 = "02";
 }
 else if(month2 == 'Mar')
 {
     month2 = "03"
 }
 else if(month2 == 'Apr')
 {
     month2 = "04"
 }
 else if(month2 == 'May')
 {
     month2 = "05"
 }
 else if(month2 == 'Jun')
 {
     month2 = "06"
 }
 else if(month2 == 'Jul')
 {
     month2 = "07"
 }
 else if(month2 == 'Aug')
 {
     month2 = "08"
 }
 else if(month2 == 'Sep')
 {
     month2 = "09"
 }
 else if(month2 == 'Oct')
 {
     month2 = "10"
 }
 else if(month2 == 'Nov')
 {
     month2 = "11"
 }
 else if(month2 == 'Dec')
 {
     month2 = "12";
 }
var finalDate2 = month2+"-"+day2+"-"+year2;
date1 = finalDate.split("-");
date2 = finalDate2.split("-");
var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);

var daysApart = Math.round((sDate-eDate)/86400000);
var flag = 0;
if(daysApart<0)
{
  alert("You can not enter Posting Date less than system default Posting Date "); 
  flag = 1;
  document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value=date3;
}
if(flag == 0)
{
var corpid='${accCorpID}' 
if(corpid=='SSCW'){
 if(day1 != day)
 {
  alert("Posting date should be last day of the month.\n\nSystem will change the posting date as "+date4); 
 	document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value = date4;
 }
 }
} 
}
}

function getLastDayOfMonth(month,year)
{
  var day;
  switch(month)
  {
      case 1 :
      case 3 :
      case 5 :
      case 7 :
      case 8 :
      case 10 :
      case 12 :
          day = 31;
          break;
      case 4 :
      case 6 :
      case 9 :
      case 11 :
             day = 30;
          break;
      case 2 :
          if( ( (year % 4 == 0) && ( year % 100 != 0) )
                         || (year % 400 == 0) )
              day = 29;
          else
              day = 28;
          break;

  }
  return day;

}
function companyMessage(){ 
	alert('Please review Company Division in Service Order');
	window.close(); 
}
function cheackBookingAgent()
{
    var serviceOrderId = document.forms['activeWorkTicketList'].elements['sidGeneratedInvoice'].value;
    var url="updateBookingAgentForCollective.html?ajax=1&decorator=simple&popup=true&sidGeneratedInvoice=" + encodeURI(serviceOrderId); 
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponse;
    http2.send(null); 
    document.getElementById("totalhid").style.display="block"; 
}
function handleHttpResponse() { 
     if (http2.readyState == 4)
         {
             var result= http2.responseText         
          }
 } 
function getHTTPObject()
{
  var xmlhttp;
  if(window.XMLHttpRequest)
  {
      xmlhttp = new XMLHttpRequest();
  }
  else if (window.ActiveXObject)
  {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      if (!xmlhttp)
      {
          xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
      }
  }
  return xmlhttp;
}
  var http2 = getHTTPObject();
  function pick() {
		//parent.window.opener.document.location.reload();
		window.opener.document.forms[0].submit();
		window.close();
	} 
  function trap(){
		 if(document.images)
		    {
		      var totalImages = document.images.length;
		      	for (var i=0;i<totalImages;i++) {
			      	try{
			      		if(document.images[i].src.indexOf('calender.png')>0 && document.getElementById(document.images[i].id).id !='recInvoiceDateToFormat_trigger'){ 
							document.images[i].src = 'images/navarrow.gif';
							var el = document.getElementById(document.images[i].id);
							if((el.getAttribute("id")).indexOf('trigger')>0){
								el.removeAttribute("id");
							} } }
		      	catch(e){}
					} }  }
  function findAssignInvoice() {
		var sidList = document.forms['activeWorkTicketList'].elements['sidGeneratedInvoice'].value;
		sidList = sidList.split("~");
	   	var serviceOrderId = sidList[0];
	    var billToCode = sidList[1]; 
	    var companyDivisionForTicket = sidList[2];
	    var billingCurrency=sidList[5]
	    document.forms['activeWorkTicketList'].elements['sid'].value = serviceOrderId;
	    document.forms['activeWorkTicketList'].elements['billToCodeForTicket'].value = billToCode;
	    document.forms['activeWorkTicketList'].elements['companyDivisionForTicket'].value = companyDivisionForTicket;
	    document.forms['activeWorkTicketList'].elements['billingCurrency'].value = billingCurrency;
	    var url="findAssignInvoice.html?ajax=1&decorator=simple&popup=true&collectivePage=true&billingCurrencyForCollective="+encodeURI(billingCurrency)+"&sid=" + encodeURI(serviceOrderId)+"&companyDivisionForTicket="+encodeURI(companyDivisionForTicket)+"&billToCodeForTicket="+encodeURI(billToCode); 
	    http4.open("GET", url, true);
	    http4.onreadystatechange = handleHttpInvoiceResponse;
	    http4.send(null); 
	  }

function handleHttpInvoiceResponse() {
             if (http4.readyState == 4) {
                var results = http4.responseText 
                results = results.trim();
                if(results.length>2){
                	var billingCurrency = document.forms['activeWorkTicketList'].elements['billingCurrency'].value;
                	document.forms['activeWorkTicketList'].action ='assignCollectiveInvoiceList.html?assignInvoiceButton=yes&collectivePage=true&billingCurrencyForCollective='+encodeURI(billingCurrency)+'&decorator=popup&popup=true';
                	document.forms['activeWorkTicketList'].submit(); 
                }else if(results.length<=4){
                 	alert("No Available Invoice to Add lines to");
                }
            }
		}
var http4 = getHTTPObject();

function setInvoiceNumber(invNumber){
	document.forms['activeWorkTicketList'].elements['collectiveInvoiceNumber'].value = invNumber;
}
function checkFormDate1(invoiceFlag){
  var invNumber = document.forms['activeWorkTicketList'].elements['collectiveInvoiceNumber'].value;
  if (invNumber == null || invNumber=='')  {
      alert("Please select any radio button"); 
   	  return false;
   }else{
	  var fdate = document.forms['activeWorkTicketList'].elements['recAccDateToFormat'].value;
	  document.forms['activeWorkTicketList'].elements['recAccDateForm'].value=fdate;
	  if(fdate==''){
	 	  document.forms['activeWorkTicketList'].elements['recAccDateForm'].value=''; 
	  }
	  <configByCorp:fieldVisibility componentId="component.accountLine.receivedInvoiceDate"> 
		  var fdate1 = document.forms['activeWorkTicketList'].elements['recInvoiceDateToFormat'].value;
		  document.forms['activeWorkTicketList'].elements['recInvoiceDateToFormat1'].value=fdate1;
		  if(fdate1==''){
		  	 document.forms['activeWorkTicketList'].elements['recInvoiceDateToFormat1'].value=''; 
		  } 
	  </configByCorp:fieldVisibility>
	  	showHide("block");
	    document.forms['activeWorkTicketList'].elements['invoiceFlag'].value=invoiceFlag;
	    document.forms['activeWorkTicketList'].action ='generateInvoiveForServiceOrderList.html?btntype=yes&decorator=popup&popup=true';
	    document.forms['activeWorkTicketList'].submit();
   }
}
function fillAssignInvoice(){ 
    var invoiceNumber = valButton(document.forms['activeWorkTicketList'].elements['radiobilling']);   
	    if (invoiceNumber == null)  {
	          alert("Please select any radio button"); 
	       	  return false;
	       }else{ 
	        invoiceNumber = invoiceNumber.replace('/',''); 
	        document.forms['activeWorkTicketList'].elements['assignInvoiceNumber'].value=invoiceNumber; 
	        var agree = confirm("Press OK to proceed, or press Cancel.");
         if(agree)  { 
            document.forms['activeWorkTicketList'].action ='generateInvoiveForBatchInvoicing.html?&buttonType=yes&btntype=yes&decorator=popup&popup=true';
            document.forms['activeWorkTicketList'].submit(); 
          }  else {
          document.forms['activeWorkTicketList'].elements['assignInvoiceNumber'].value='';
          return false; 
          } 
        }  
   }
function valButton(btn) {
    var cnt = -1; 
    var len = btn.length; 
    if(len >1){
    for (var i=btn.length-1; i > -1; i--) {
	        if (btn[i].checked) {cnt = i; i = -1;}
	    }
	    if (cnt > -1) return btn[cnt].value;
	    else return null;
    	
    }else{ 
    	return document.forms['activeWorkTicketList'].elements['radiobilling'].value; 
    } 
  }
  
</SCRIPT>
<%-- Shifting Closed Here --%>

<script type="text/javascript">
try{
	 <c:if test="${postingDateStop}"> 
	 trap();
	 </c:if>
	 }
catch(e){}
try{
	var sidList=document.forms['activeWorkTicketList'].elements['sidGeneratedInvoice'].value;
	var arr=sidList.split(",");
	var billCodeList="";
	var companyDivList="";
	for(t=0;t<arr.length;t++){
		    var arr1=arr[t].split("~");
			if(billCodeList.trim()==""){
				billCodeList=arr1[1];
			}else{
				if(billCodeList.indexOf(arr1[1])<0)
				{
				billCodeList=billCodeList+","+arr1[1];
				}
			}
			if(companyDivList.trim()==""){
				companyDivList=arr1[2];
			}else{
				if(companyDivList.indexOf(arr1[2])<0)
				{				
				companyDivList=companyDivList+","+arr1[2];
				}
			}
	}	
	document.forms['activeWorkTicketList'].elements['billCodeList'].value=billCodeList;
	document.forms['activeWorkTicketList'].elements['companyDivisionList'].value=companyDivList;
	var disableButton = sidList.indexOf(",");
	if(disableButton>0){
		document.getElementById("assignLinestoExistingInvoice").disabled = true;
	}
}catch(e){} 
try{/*
	<c:if test="${accountInterface!='Y'}"> 
	      document.getElementById("totalhid").style.display="none";
	</c:if>
	<c:if test="${accountInterface=='Y'}"> 
	    document.getElementById("totalhid").style.display="block";
	</c:if>*/
	document.getElementById("totalhid").style.display="none";
	if(document.forms['activeWorkTicketList'].elements['assignInvoiceButton'].value=='yes'){ 
		document.getElementById("assignInvoiceDiv").style.display="block";
		document.getElementById("totalhid").style.display="block";
	}
	if(document.forms['activeWorkTicketList'].elements['assignInvoiceButton'].value!='yes'){
		document.getElementById("assignInvoiceDiv").style.display="none"; 
	}
}catch(e){} 
  try{
	  if(document.forms['activeWorkTicketList'].elements['btntype'].value=='yes'){
	  pick();
	  }
	  }
	  catch(e){}
	  <c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
	  setOnSelectBasedMethods(["calcDateFlexibility()"]);
	  </c:if>
	  <c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
	  setOnSelectBasedMethods(["calcDate()"]);
	  </c:if>

	  autoPopulate_recievedDate();
	  setCalendarFunctionality();	  
</script>
