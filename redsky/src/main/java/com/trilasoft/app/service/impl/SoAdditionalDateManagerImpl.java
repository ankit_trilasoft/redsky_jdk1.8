package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.SoAdditionalDateDao;
import com.trilasoft.app.model.SoAdditionalDate;
import com.trilasoft.app.service.SoAdditionalDateManager;

public class SoAdditionalDateManagerImpl extends GenericManagerImpl<SoAdditionalDate, Long> implements SoAdditionalDateManager{	
	SoAdditionalDateDao soAdditionalDateDao;
	
	public SoAdditionalDateManagerImpl(SoAdditionalDateDao soAdditionalDateDao) {
		super(soAdditionalDateDao);
		this.soAdditionalDateDao = soAdditionalDateDao;
	}
	public List findId(Long sid, String sessionCorpID){
		return soAdditionalDateDao.findId(sid, sessionCorpID);
	}
}
