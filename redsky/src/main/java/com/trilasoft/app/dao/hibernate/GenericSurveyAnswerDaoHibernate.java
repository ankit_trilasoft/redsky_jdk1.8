package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.GenericSurveyAnswerDao;
import com.trilasoft.app.model.GenericSurveyAnswer;


public class GenericSurveyAnswerDaoHibernate extends GenericDaoHibernate<GenericSurveyAnswer, Long>implements GenericSurveyAnswerDao{

	public GenericSurveyAnswerDaoHibernate() {
		super(GenericSurveyAnswer.class);
	}
	
	public List getSurveyAnswerList(Long parentId, String sessionCorpID){
		String query="from GenericSurveyAnswer where surveyQuestionId='"+parentId+"' and corpId='"+sessionCorpID+"'";
		List list = getHibernateTemplate().find(query);
		return list;
	}
}
