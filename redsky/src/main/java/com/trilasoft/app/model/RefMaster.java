package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="refmaster")
@FilterDefs({
	@FilterDef(name = "refMasterCorpID", parameters = { @ParamDef(name = "partnerCorpID", type = "string") })
})

@Filters( {
	@Filter(name = "refMasterCorpID", condition = "corpID in (:partnerCorpID) ")
} )


@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    

public class RefMaster extends BaseObject implements Comparable<RefMaster> {
	private Long id;
	
	private String corpID;
	//private int idNum;
	//private String what;
	private String code;
	private int fieldLength;
	private Double number;
	private String description;
	private String parameter;
	private String bucket;
	private String bucket2;
	private Date stopDate;
	private String billCrew;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String lastReset;
	
	private String branch;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String flex1;
	private String flex2;
	private String flex3;
	private String flex4;
	private String label1;
	private String label2;
	private String label3;
	private String label4;
	private String language;
	private String flex5;
	private String flex6;
	private String label5;
	private String label6;
	private String status;
	private String flex7;
	private String label7;
	private String flex8;
	private String label8;
	private Integer sequenceNo;
	private String childParameterValues;
	private String globallyUniqueIdentifier;
	private String serviceWarehouse;
	//private String documentCategory;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("code", code)
				.append("fieldLength", fieldLength).append("number", number)
				.append("description", description)
				.append("parameter", parameter).append("bucket", bucket)
				.append("bucket2", bucket2).append("stopDate", stopDate)
				.append("billCrew", billCrew).append("address1", address1)
				.append("address2", address2).append("city", city)
				.append("state", state).append("zip", zip)
				.append("lastReset", lastReset).append("branch", branch)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("flex1", flex1).append("flex2", flex2)
				.append("flex3", flex3).append("flex4", flex4)
				.append("label1", label1).append("label2", label2)
				.append("label3", label3).append("label4", label4)
				.append("language", language)
				.append("flex5", flex5)
				.append("flex6", flex6)
				.append("label5", label5)
				.append("label6", label6).append("status", status)
				.append("flex7", flex7).append("label7", label7)
				.append("flex8", flex8).append("label8", label8)
				.append("sequenceNo", sequenceNo)
				.append("childParameterValues", childParameterValues)
				.append("globallyUniqueIdentifier", globallyUniqueIdentifier)
				.append("serviceWarehouse", serviceWarehouse)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof RefMaster))
			return false;
		RefMaster castOther = (RefMaster) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID).append(code, castOther.code)
				.append(fieldLength, castOther.fieldLength)
				.append(number, castOther.number)
				.append(description, castOther.description)
				.append(parameter, castOther.parameter)
				.append(bucket, castOther.bucket)
				.append(bucket2, castOther.bucket2)
				.append(stopDate, castOther.stopDate)
				.append(billCrew, castOther.billCrew)
				.append(address1, castOther.address1)
				.append(address2, castOther.address2)
				.append(city, castOther.city).append(state, castOther.state)
				.append(zip, castOther.zip)
				.append(lastReset, castOther.lastReset)
				.append(branch, castOther.branch)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(flex1, castOther.flex1).append(flex2, castOther.flex2)
				.append(flex3, castOther.flex3).append(flex4, castOther.flex4)
				.append(label1, castOther.label1)
				.append(label2, castOther.label2)
				.append(label3, castOther.label3)
				.append(label4, castOther.label4)
				.append(language, castOther.language)
				.append(flex5, castOther.flex5)
				.append(flex6, castOther.flex6)
				.append(label5, castOther.label5)
				.append(label6, castOther.label6)
				.append(status, castOther.status)
				.append(flex7, castOther.flex7)
				.append(label7, castOther.label7)
				.append(flex8, castOther.flex8)
				.append(label8, castOther.label8)
				.append(sequenceNo, castOther.sequenceNo)
				.append(childParameterValues, castOther.childParameterValues)
				.append(globallyUniqueIdentifier, castOther.globallyUniqueIdentifier)
				.append(serviceWarehouse, castOther.serviceWarehouse)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(code)
				.append(fieldLength).append(number).append(description)
				.append(parameter).append(bucket).append(bucket2)
				.append(stopDate).append(billCrew).append(address1)
				.append(address2).append(city).append(state).append(zip)
				.append(lastReset).append(branch).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(flex1).append(flex2).append(flex3).append(flex4)
				.append(label1).append(label2).append(label3).append(label4)
				.append(language)
				.append(flex5)
				.append(flex6)
				.append(label5)
				.append(label6).append(status).append(flex7).append(flex8).append(label7).append(label8)
				.append(sequenceNo)
				.append(childParameterValues)
				.append(globallyUniqueIdentifier)
				.append(serviceWarehouse)
				.toHashCode();
	}

	public int compareTo(final RefMaster other) {
		return new CompareToBuilder().append(id, other.id).append(corpID,
				other.corpID).append(code, other.code).append(fieldLength,
				other.fieldLength).append(number, other.number).append(
				description, other.description).append(parameter,
				other.parameter).append(bucket, other.bucket).append(bucket2,
				other.bucket2).append(stopDate, other.stopDate).append(
				billCrew, other.billCrew).append(address1, other.address1)
				.append(address2, other.address2).append(city, other.city)
				.append(state, other.state).append(zip, other.zip).append(
						lastReset, other.lastReset)
				.append(branch, other.branch)
				.append(createdBy, other.createdBy).append(updatedBy,
						other.updatedBy).append(createdOn, other.createdOn)
				.append(updatedOn, other.updatedOn).append(flex1, other.flex1)
				.append(flex2, other.flex2).append(flex3, other.flex3).append(
						flex4, other.flex4).append(label1, other.label1)
				.append(label2, other.label2).append(label3, other.label3)
				.append(label4, other.label4).append(flex7, other.flex7)
				.append(label7, other.label7).append(flex8, other.flex8)
				.append(label8, other.label8).append(sequenceNo, other.sequenceNo)
				.append(childParameterValues, other.childParameterValues)
				.append(globallyUniqueIdentifier, other.globallyUniqueIdentifier).append(serviceWarehouse, other.serviceWarehouse).toComparison();
	}

	@Column(length=40)
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	@Column(length=40)
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	@Column(length=2)
	public String getBillCrew() {
		return billCrew;
	}
	public void setBillCrew(String billCrew) {
		this.billCrew = billCrew;
	}
	@Column(length=8)
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	@Column(length=50)
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	@Column(length=20)
	public String getBucket2() {
		return bucket2;
	}
	public void setBucket2(String bucket2) {
		this.bucket2 = bucket2;
	}
	@Column(length=20)
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Column(length=100)
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=5000)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column
	public int getFieldLength() {
		return fieldLength;
	}
	public void setFieldLength(int fieldLength) {
		this.fieldLength = fieldLength;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=26)
	public String getLastReset() {
		return lastReset;
	}
	public void setLastReset(String lastReset) {
		this.lastReset = lastReset;
	}
	@Column
	public Double getNumber() {
		return number;
	}
	public void setNumber(Double number) {
		this.number = number;
	}
	@Column(length=2)
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column
	public Date getStopDate() {
		return stopDate;
	}
	public void setStopDate(Date stopDate) {
		this.stopDate = stopDate;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/*
	@Column(length=8)
	public String getWhat() {
		return what;
	}
	public void setWhat(String what) {
		this.what = what;
	}
	*/
	@Column(length=10)
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	@Column(length=50)
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	@Column(length=100)
	public String getflex1() {
		return flex1;
	}
	public void setflex1(String flex1) {
		this.flex1 = flex1;
	}
	@Column(length=100)
	public String getflex2() {
		return flex2;
	}
	public void setflex2(String flex2) {
		this.flex2 = flex2;
	}
	@Column(length=100)
	public String getflex3() {
		return flex3;
	}
	public void setflex3(String flex3) {
		this.flex3 = flex3;
	}
	@Column(length=100)
	public String getflex4() {
		return flex4;
	}
	public void setflex4(String flex4) {
		this.flex4 = flex4;
	}
	@Column(length=50)
	public String getLabel1() {
		return label1;
	}

	public void setLabel1(String label1) {
		this.label1 = label1;
	}
	@Column(length=50)
	public String getLabel2() {
		return label2;
	}

	public void setLabel2(String label2) {
		this.label2 = label2;
	}
	@Column(length=50)
	public String getLabel3() {
		return label3;
	}

	public void setLabel3(String label3) {
		this.label3 = label3;
	}
	@Column(length=50)
	public String getLabel4() {
		return label4;
	}

	public void setLabel4(String label4) {
		this.label4 = label4;
	}
	@Column(length=5)
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	/*@Column(length=35)
	public String getDocumentCategory() {
		return documentCategory;
	}

	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}*/
	@Column(length=100)
	public String getFlex5() {
		return flex5;
	}

	public void setFlex5(String flex5) {
		this.flex5 = flex5;
	}
	@Column(length=100)
	public String getFlex6() {
		return flex6;
	}

	public void setFlex6(String flex6) {
		this.flex6 = flex6;
	}
	@Column(length=50)
	public String getLabel5() {
		return label5;
	}

	public void setLabel5(String label5) {
		this.label5 = label5;
	}
	@Column(length=50)
	public String getLabel6() {
		return label6;
	}

	public void setLabel6(String label6) {
		this.label6 = label6;
	}
	@Column
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	@Column
	public String getFlex7() {
		return flex7;
	}

	public void setFlex7(String flex7) {
		this.flex7 = flex7;
	}
	@Column
	public String getLabel7() {
		return label7;
	}
	@Column
	public void setLabel7(String label7) {
		this.label7 = label7;
	}
	@Column
	public String getFlex8() {
		return flex8;
	}
	
	public void setFlex8(String flex8) {
		this.flex8 = flex8;
	}
	@Column
	public String getLabel8() {
		return label8;
	}

	public void setLabel8(String label8) {
		this.label8 = label8;
	}
	@Column
	public Integer getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	@Column
	public String getChildParameterValues() {
		return childParameterValues;
	}

	public void setChildParameterValues(String childParameterValues) {
		this.childParameterValues = childParameterValues;
	}
	@Column
	public String getGloballyUniqueIdentifier() {
		return globallyUniqueIdentifier;
	}

	public void setGloballyUniqueIdentifier(String globallyUniqueIdentifier) {
		this.globallyUniqueIdentifier = globallyUniqueIdentifier;
	}

	public String getServiceWarehouse() {
		return serviceWarehouse;
	}

	public void setServiceWarehouse(String serviceWarehouse) {
		this.serviceWarehouse = serviceWarehouse;
	}
	
}
