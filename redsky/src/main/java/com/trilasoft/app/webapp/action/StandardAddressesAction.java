package com.trilasoft.app.webapp.action;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.StandardAddresses;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.StandardAddressesManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class StandardAddressesAction extends BaseAction implements Preparable {

	private List standardaddresses;
	
	private StandardAddressesManager standardAddressesManager;
	
	private StandardAddresses standardAddresses;
	
	private RefMasterManager refMasterManager;
	
	private Map<String, String> jobs;

	private Long id;

	private String sessionCorpID;

	private Map<String, String> states;

	private Map<String, String> country;

	private CustomerFileManager customerFileManager;
	private String addressLine1;
	private String countryCode;
	private String state;
	private String job;
	private String enbState;
    private Map<String, String> countryCod;
    private  Map<String, String> payType;
    private  Map<String, String> residenceType;
    private  Map<String, String> residenceDeposit;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(StandardAddressesAction.class);
	
    public void prepare() throws Exception {
		enbState = customerFileManager.enableStateList(sessionCorpID);
	}

	public StandardAddressesAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	private String popup="";
	private String jobType;
	public String standardAddList() {	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		jobs = refMasterManager.findByParameter(sessionCorpID, "JOB");
		country = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		countryCod = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
		//country = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
		if(popup.equalsIgnoreCase("true")){
			standardaddresses = standardAddressesManager.getByJobType(jobType);
		}else{
		standardaddresses = standardAddressesManager.getAll();
		}
		states = customerFileManager.findDefaultStateList("", sessionCorpID);
		payType = refMasterManager.findByParameter(sessionCorpID, "PAYTYPE");
		residenceType = refMasterManager.findByParameter(sessionCorpID, "RESIDENCETYPE");
		residenceDeposit = refMasterManager.findByParameter(sessionCorpID, "RESIDENCEDEPOSITTYPE");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String addNewAddressDetails() {	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		country = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		countryCod = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
		//country = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
		jobs = refMasterManager.findByParameter(sessionCorpID, "JOB");
		payType = refMasterManager.findByParameter(sessionCorpID, "PAYTYPE");
		residenceType = refMasterManager.findByParameter(sessionCorpID, "RESIDENCETYPE");
		residenceDeposit = refMasterManager.findByParameter(sessionCorpID, "RESIDENCEDEPOSITTYPE");
		//System.out.println("id>>>>>>>>"+id);
		if (id != null) {			
			standardAddresses = standardAddressesManager.get(id);			
			states = customerFileManager.findDefaultStateList(standardAddresses.getCountryCode(), sessionCorpID);
		}else{			
			//System.out.println("ONE--------------\n");
			standardAddresses = new StandardAddresses();
			//System.out.println("TWO--------------\n");
			states = customerFileManager.findDefaultStateList(standardAddresses.getCountryCode(), sessionCorpID);
			//System.out.println("THREE--------------\n");
			standardAddresses.setCreatedBy(getRequest().getRemoteUser());
			standardAddresses.setCreatedOn(new Date());
			standardAddresses.setUpdatedBy(getRequest().getRemoteUser());
			standardAddresses.setUpdatedOn(new Date());
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String save() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//System.out.println("id>>>>>>>>"+id);
		country = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		countryCod = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
		//country = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
		jobs = refMasterManager.findByParameter(sessionCorpID, "JOB");
		payType = refMasterManager.findByParameter(sessionCorpID, "PAYTYPE");
		residenceType = refMasterManager.findByParameter(sessionCorpID, "RESIDENCETYPE");
		residenceDeposit = refMasterManager.findByParameter(sessionCorpID, "RESIDENCEDEPOSITTYPE");
		standardAddresses.setCorpID(sessionCorpID);
		boolean isNew = (standardAddresses.getId() == null);
	
		if (standardAddresses.getId()==null) {			
			standardAddresses.setCreatedBy(getRequest().getRemoteUser());
			standardAddresses.setCreatedOn(new Date());
			standardAddresses.setUpdatedBy(getRequest().getRemoteUser());
			standardAddresses.setUpdatedOn(new Date());
			//states = customerFileManager.findDefaultStateList("", sessionCorpID);
		}else {			
			//states = customerFileManager.findDefaultStateList(standardAddresses.getCountryCode(), sessionCorpID);
			standardAddresses.setUpdatedBy(getRequest().getRemoteUser());
			standardAddresses.setUpdatedOn(new Date());
		}		
		
		standardAddresses = standardAddressesManager.save(standardAddresses);
		states = customerFileManager.findDefaultStateList(standardAddresses.getCountryCode(), sessionCorpID);	
		String key=(isNew)?"Address added successfully":"Address updated successfully";
		saveMessage(getText(key)); 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String searchAddRecord(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(popup.equalsIgnoreCase("true")){
			if(standardAddresses.getState()==null){
				standardaddresses=standardAddressesManager.searchByDetailForPopup(jobType, standardAddresses.getAddressLine1(),standardAddresses.getCity(), standardAddresses.getCountryCode(), "");
			}else{
			standardaddresses=standardAddressesManager.searchByDetailForPopup(jobType, standardAddresses.getAddressLine1(), standardAddresses.getCity(),standardAddresses.getCountryCode(), standardAddresses.getState());
			}	
		}else{
			if(standardAddresses.getState()==null ){
				standardaddresses=standardAddressesManager.searchByDetail(standardAddresses.getJob(), standardAddresses.getAddressLine1(),standardAddresses.getCity(), standardAddresses.getCountryCode(), "");
			}else{
			standardaddresses=standardAddressesManager.searchByDetail(standardAddresses.getJob(), standardAddresses.getAddressLine1(),standardAddresses.getCity(), standardAddresses.getCountryCode(), standardAddresses.getState());
			}
		}
		
		if (standardaddresses != null) {			
			states = customerFileManager.findDefaultStateList(standardAddresses.getCountryCode(), sessionCorpID);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: states "+states);
		}else{
			states = customerFileManager.findDefaultStateList("", sessionCorpID);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: states "+states);
		}
		country = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		countryCod = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
		//country = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
		jobs = refMasterManager.findByParameter(sessionCorpID, "JOB");
		payType = refMasterManager.findByParameter(sessionCorpID, "PAYTYPE");
		residenceType = refMasterManager.findByParameter(sessionCorpID, "RESIDENCETYPE");
		residenceDeposit = refMasterManager.findByParameter(sessionCorpID, "RESIDENCEDEPOSITTYPE");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	
	public String delete() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		standardAddressesManager.remove(id);
		saveMessage(getText("Address Detail is deleted sucessfully"));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public List getStandardaddresses() {
		return standardaddresses;
	}

	public void setStandardaddresses(List standardaddresses) {
		this.standardaddresses = standardaddresses;
	}

	public StandardAddresses getStandardAddresses() {
		return standardAddresses;
	}

	public void setStandardAddresses(StandardAddresses standardAddresses) {
		this.standardAddresses = standardAddresses;
	}

	public void setStandardAddressesManager(StandardAddressesManager standardAddressesManager) {
		this.standardAddressesManager = standardAddressesManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Map<String, String> getJobs() {
		return jobs;
	}

	public void setJobs(Map<String, String> jobs) {
		this.jobs = jobs;
	}

	public Map<String, String> getCountry() {
		return country;
	}

	public void setCountry(Map<String, String> country) {
		this.country = country;
	}

	public Map<String, String> getStates() {
		return states;
	}

	public void setStates(Map<String, String> states) {
		this.states = states;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressline1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPopup() {
		return popup;
	}

	public void setPopup(String popup) {
		this.popup = popup;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getEnbState() {
		return enbState;
	}

	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}

	public Map<String, String> getCountryCod() {
		return countryCod;
	}

	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}

	public Map<String, String> getPayType() {
		return payType;
	}

	public void setPayType(Map<String, String> payType) {
		this.payType = payType;
	}

	public Map<String, String> getResidenceType() {
		return residenceType;
	}

	public void setResidenceType(Map<String, String> residenceType) {
		this.residenceType = residenceType;
	}

	public Map<String, String> getResidenceDeposit() {
		return residenceDeposit;
	}

	public void setResidenceDeposit(Map<String, String> residenceDeposit) {
		this.residenceDeposit = residenceDeposit;
	}
}
