<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>   
    <title><fmt:message key="zipcodeList.title"/></title>   
    <meta name="heading" content="<fmt:message key='zipcodeList.heading' />"/>   
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>Zip Code List </b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
	<display:table name="zipCodeLists" class="table" requestURI="" id="zipCodeLists" export="false" defaultsort="3" >
	<display:column title="City" ><a onclick="goToUrlZip('${zipCodeLists.city}','${zipType}'),ajax_hideTooltip();" >${fn:replace(zipCodeLists.city,"\\","")}</a></display:column>      
	<display:column property="primaryRecord" title="Primary" style="width:10px" />
 	<display:column property="phoneAreaCode" title="Phone Area Code" style="width:10px" />
 	</display:table>
