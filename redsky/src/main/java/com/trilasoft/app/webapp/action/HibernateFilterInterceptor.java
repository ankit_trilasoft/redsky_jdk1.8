package com.trilasoft.app.webapp.action;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.service.SessionCorpIDInterceptor;



public class HibernateFilterInterceptor implements Interceptor {

	
private SessionCorpIDInterceptor sessionCorpIDInterceptor;
	
	private String result = null;
	public ResourceBundle resourceBundle;	
	private String byPassUrls=(String)AppInitServlet.redskyConfigMap.get("BY_PASS_URL");
	public void setSessionCorpIDInterceptor(
			SessionCorpIDInterceptor sessionCorpIDInterceptor) {
		this.sessionCorpIDInterceptor = sessionCorpIDInterceptor;
	}
	
	public void prepare() throws Exception {

/*	       	
			try {
				//resourceBundle = ResourceBundle.getBundle("redsky-configuration");
				//byPassUrls = resourceBundle.getString("BY_PASS_URL");
			} catch (Exception e) {
				e.printStackTrace();
			}*/
	       HttpServletRequest request = ServletActionContext.getRequest();
	       String url = (request.getRequestURI()).substring(request

	                           .getRequestURI().lastIndexOf("/"), request.getRequestURI()

	                           .length());
	       if (url.indexOf("!") > -1) {

	                     url = url.replaceAll(

	                                  url.substring(url.indexOf("!"), url.indexOf(".")), "");

	              }
	       if (byPassUrls.indexOf(url.replaceAll(

	                           url.substring(url.indexOf("."), url.length()), "")

	                           .trim()) > -1){

	              if(ServletActionContext.getRequest().getParameter("bypass_sessionfilter") != null 

	                            && ServletActionContext.getRequest().getParameter("bypass_sessionfilter").equalsIgnoreCase("YES") ){

	                     return;
	              }
	       }
	       sessionCorpIDInterceptor.enableFilters(); 
	       }
	
	public String intercept(ActionInvocation invocation) throws Exception {
		// TODO Auto-generated method stub
		
		prepare();
		
		result = invocation.invoke();
		
		return result;
	}

	public void destroy() {
	}

	public void init() {
	}
	
	

}
