package com.trilasoft.app.webapp.tags;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.util.ExpressionEvaluationUtils;

import com.trilasoft.app.webapp.action.ComponentConfigByCorpUtil;

public class TimeZoneByCorpTag extends TagSupport {

	// ~ Instance fields
	// ================================================================================================

	

	private ComponentConfigByCorpUtil componentConfigByCorpUtil = null;

	private String userCorpID;
	
	public TimeZoneByCorpTag(){
	}

	// ~ Methods
	// ========================================================================================================

	public int doStartTag() throws JspException {
		//get timezone if already available use it
		//String timeZone = (String)pageContext.getSession("sessionTimeZone");
		if(pageContext.getSession()==null){
			return Tag.EVAL_BODY_INCLUDE;
		}
		String timeZone = (String)pageContext.getSession().getAttribute("sessionTimeZone");
		if (timeZone != null ) return Tag.EVAL_BODY_INCLUDE;

		ServletContext context = pageContext.getServletContext();
		XmlWebApplicationContext ctx = (XmlWebApplicationContext) context
				.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);

		componentConfigByCorpUtil = (ComponentConfigByCorpUtil) ctx.getBean("componentConfigByCorpUtil");	
		Authentication currentUser = SecurityContextHolder.getContext()
		.getAuthentication();
		if (currentUser.getPrincipal() instanceof String && currentUser.getPrincipal().equals("anonymous")  ) return Tag.EVAL_BODY_INCLUDE;;
		User user = (User) currentUser.getPrincipal();
		String userCorpID = user.getCorpID();
				
		timeZone = componentConfigByCorpUtil.getTimeZone(userCorpID);
		//pageContext.getSession("sessionTimeZone", timeZone);
		pageContext.getSession().setAttribute("sessionTimeZone", timeZone);
		return Tag.EVAL_BODY_INCLUDE;
	}

	
}
