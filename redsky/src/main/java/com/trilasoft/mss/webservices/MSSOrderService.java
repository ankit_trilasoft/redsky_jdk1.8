
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MSSOrderService complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MSSOrderService">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ServiceNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="COD" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PendingAuthorization" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MSSOrderService", propOrder = {
    "serviceNumber",
    "cod",
    "pendingAuthorization"
})
public class MSSOrderService {

    @XmlElement(name = "ServiceNumber")
    protected int serviceNumber;
    @XmlElement(name = "COD")
    protected boolean cod;
    @XmlElement(name = "PendingAuthorization")
    protected boolean pendingAuthorization;

    /**
     * Gets the value of the serviceNumber property.
     * 
     */
    public int getServiceNumber() {
        return serviceNumber;
    }

    /**
     * Sets the value of the serviceNumber property.
     * 
     */
    public void setServiceNumber(int value) {
        this.serviceNumber = value;
    }

    /**
     * Gets the value of the cod property.
     * 
     */
    public boolean isCOD() {
        return cod;
    }

    /**
     * Sets the value of the cod property.
     * 
     */
    public void setCOD(boolean value) {
        this.cod = value;
    }

    /**
     * Gets the value of the pendingAuthorization property.
     * 
     */
    public boolean isPendingAuthorization() {
        return pendingAuthorization;
    }

    /**
     * Sets the value of the pendingAuthorization property.
     * 
     */
    public void setPendingAuthorization(boolean value) {
        this.pendingAuthorization = value;
    }

}
