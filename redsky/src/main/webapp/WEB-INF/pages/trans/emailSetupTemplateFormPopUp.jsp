<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>  
<html lang="en">
<head>
</head>
<style type="text/css">
h4 {color: #444;font-size: 14px;line-height: 1.3em; margin: 0 0 0.25em;padding: 0 0 0 15px;font-weight:bold;}
.modal-body {padding: 0 5px; max-height: calc(100vh - 100px); overflow-y: auto; margin-top: 10px }
.modal-header { border-bottom: 1px solid #e5e5e5; min-height: 5.43px; padding: 10px;}
.modal-footer { border-top: 1px solid #e5e5e5; padding: 10px; text-align: right;}
.modal-header .close {margin-right:5px;margin-top: -2px; text-align: right;}
.hide{display:none;}
.show{display:block;}
.list-menu {height: 19px;!height: 20px;}
.header-bdr{text-align:left !important;color:#444;padding-bottom:5px !important;margin-top:-10px;padding-top: 5px;border-top-left-radius: 4px;border-top-right-radius: 4px;}
@media screen and (min-width: 768px) {
    .custom-class {width: 70%;       
    }
}
</style>
	
<script type="text/javascript">
var emailId = '${emailSetupTemplate.id}';
	tinymce.init({
			    selector: '#emailBody'+emailId,
			    theme: 'modern',
			    width: 702,
			    height: 225,
			    branding: false,
			    menubar: false,
			    readonly : 1,
			    dialog_type : "modal",
			    content_style: ".mce-content-body {font-size:10px;font-family:Arial,sans-serif;}",
			    plugins: [
			      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
			      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
			      'save table contextmenu directionality template paste textcolor'
			    ],
			    //content_css:'redskyditor/css/content.css',
			    toolbar: 'fontselect fontsizeselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview | forecolor backcolor ',
			    font_formats: "Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Open Sans=Open Sans,helvetica,sans-serif;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva",
				fontsize_formats: "8px 10px 12px 14px 16px 18px 20px",
		});
</script>
<div class="modal-dialog" style="width:1200px">
 <div class="modal-content">
 <div class="modal-header bg-info header-bdr">
        <button type="button" class="close" style="text-align:right !important;" onclick="closeModal('${emailSetupTemplate.id}');" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="line-height:1.32 !important">Email Setup Template Details</h4>
      </div>
<div class="modal-body" style="padding:0px 20px;">
<s:form id="emailTemplateFormPopUp" name="emailTemplateFormPopUp" action="" method="post" validate="true" >
<s:hidden name="fileTypeName" />
<s:hidden name="reportsId" />
<s:hidden name="cid" value="<%=request.getParameter("cid") %>"/>
<s:hidden name="sid" value="<%=request.getParameter("sid") %>"/>	
   		<div style="width:100%;">				
				<table class="table-cmodal" style="margin:0px;padding:0px;width:auto !important;" cellspacing="0" cellpadding="3" border="0">
				<tbody>
					<tr>
						<td colspan="10">
						<table style="margin:0px;padding:0px;"  cellspacing="0" cellpadding="3" border="0">
							<tr>
								<td align="right" style="width:53px;text-align:right!important;" class="listwhitetext">Description</td>
								<td align="left" class="listwhitetext"><s:textfield name="emailSetupTemplate.saveAs" cssStyle="width:307px;" maxlength="100" cssClass="input-text" /></td>
								
								<td align="right" style="width:42px;text-align:right!important;" class="listwhitetext">Module </td>
								<td align="left">
									<configByCorp:customDropDown listType="map" list="${moduleList}" fieldValue="${emailSetupTemplate.module}" attribute="class=list-menu name=emailSetupTemplate.module  style=width:110px headerKey='' headerValue='' "/>
								</td>
							
								<td align="right" style="width:84px;text-align:right!important;" class="listwhitetext">Sub-Module </td>
								<td align="left">
									<configByCorp:customDropDown listType="map" list="${subModuleList}" fieldValue="${emailSetupTemplate.subModule}" attribute="class=list-menu name=emailSetupTemplate.subModule onchange='checkParentModule();' style=width:110px headerKey='' headerValue='' "/>
								</td>
								
								<td align="right" style="width:55px;text-align:right!important;" class="listwhitetext">Order# </td>
								<td align="left" class="listwhitetext"><s:textfield name="emailSetupTemplate.orderNumber" cssClass="input-text" cssStyle="width:25px;text-align:right" maxlength="3" onkeydown="return onlyNumsAllowed(event)"/></td>
								
								<c:set var="isSignatureFlag" value="false"/>
								<c:if test="${emailSetupTemplate.addSignatureFromAppUser}">
									<c:set var="isSignatureFlag" value="true"/>
								</c:if>
								<td>&nbsp;</td>
								<td align="left" ><s:checkbox name="emailSetupTemplate.addSignatureFromAppUser" value="${isSignatureFlag}" fieldValue="true" onclick="" cssStyle="margin:0px;" /></td>
								<td align="left" class="listwhitetext">Add Signature From App User</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="15">
						<fieldset style="margin-bottom:0px;">
						<legend>Filters</legend>
						<table style="margin:0px;padding:0px;"  cellspacing="0" cellpadding="3" border="0">									
						<tr>
							<td align="right" class="listwhitetext">Job&nbsp;Type </td>
							<td align="left">
								<configByCorp:customDropDown listType="map" list="${jobsList}" fieldValue="${emailSetupTemplate.jobType}" attribute="class=list-menu name=emailSetupTemplate.jobType  style=width:180px headerKey='' headerValue='' "/>
							</td>
						
							<td align="right" class="listwhitetext" style="width:45px;text-align:right!important;">Mode </td>
							<td align="left">
								<configByCorp:customDropDown listType="map" list="${modeList}" fieldValue="${emailSetupTemplate.mode}" attribute="class=list-menu name=emailSetupTemplate.mode  style=width:81px headerKey='' headerValue='' "/>
							</td>
						
							<td align="right" class="listwhitetext" style="text-align:right!important;">&nbsp;Routing </td>
							<td align="left">
								<configByCorp:customDropDown listType="map" list="${routingList}" fieldValue="${emailSetupTemplate.routing}" attribute="class=list-menu name=emailSetupTemplate.routing  style=width:100px headerKey='' headerValue='' "/>
							</td>
							
							<td align="right" class="listwhitetext" style="text-align:right!important;">Move&nbsp;Type </td>
							<td align="left">
							<s:select cssClass="list-menu" name="emailSetupTemplate.moveType" list="%{moveTypeList}" cssStyle="width:100px" headerKey="" headerValue=""/>
							</td>
							
							<td style="text-align:right;" class="listwhitetext">Service </td>
							<td align="left" colspan="3">
								<configByCorp:customDropDown listType="map" list="${serviceList}" fieldValue="${emailSetupTemplate.service}" attribute="class=list-menu name=emailSetupTemplate.service  style=width:240px headerKey='' headerValue='' "/>
							</td>
							
							
						</tr>
						<tr>
							<td colspan="5">
						    <table style="margin:0px;padding:0px;"  cellspacing="0" cellpadding="3" border="0">
						    <tbody>
						    <tr> 
							<td align="right" valign="top" style="width:41px;text-align:right!important;" class="listwhitetext">Others</td>
	    							<td align="left" class="listwhitetext"><s:textarea  cssClass="textarea" name="emailSetupTemplate.others" cssStyle="width:312px;height:65px;" onkeypress="" /></td>
							</tr>
							</tbody>
							</table>
							</td>
							<td align="right"><input type="button" id="testCondition" class="cssbutton" method="" name="testCondition" value="Test Condition" style="width:100px; height:23px;" disabled/></td>
							<td style="text-align:right;" class="listwhitetext" width="">Status</td>
							<c:if test="${emailSetupTemplate.status!='Error'}">
								<td align="left"><s:textfield name="emailSetupTemplate.status" id="status" cssStyle="width:97px;" readonly="true" cssClass="input-textUpper" /></td>
					  		</c:if>
					  		<c:if test="${emailSetupTemplate.status=='Error'}">
					  			<td align="left"><s:textfield name="emailSetupTemplate.status" id="status" cssStyle="width:97px;" readonly="true" cssClass="rules-textUpper" /></td>
					  		</c:if>
							
							<td align="right" class="listwhitetext" style="text-align:right;">Company&nbsp;Division </td>
							<td align="left"><s:select cssClass="list-menu" name="emailSetupTemplate.companyDivision" list="%{companyDivisionList}" cssStyle="width:90px" headerKey="" headerValue=""/></td>
							
							<td align="right" class="listwhitetext txt-right">Language</td>
							<td align="left">
								<configByCorp:customDropDown listType="map" list="${languageList}" fieldValue="${emailSetupTemplate.language}" attribute="class=list-menu name=emailSetupTemplate.language  style=width:88px headerKey='' headerValue='' "/>
							</td>
							<%-- <td align="right" class="listwhitetext" style="text-align:right;">Trigger&nbsp;Field </td>
							<td align="left" class="listwhitetext" colspan="3"><s:textfield name="emailSetupTemplate.triggerField" cssStyle="width:290px" maxlength="50" cssClass="input-text" /></td> --%>
						</tr>
						</table>
						</fieldset>
						</td>
					</tr>
					
					<tr>
						<td colspan="15">								
						<fieldset style="margin-bottom:0px;padding-top:5px;">
						<legend>Attachment Type</legend>
						<table style="margin:0px;padding:0px;width:100%;"  cellspacing="0" cellpadding="3" border="0">	
						<tr>
						<td valign="top" width="36%">							
							<table style="margin:0px;padding:0px;width:100%;margin-top:0px;"  cellspacing="0" cellpadding="3" border="0">
							<tr>
							<td colspan="2" style="padding-right:15px;">								
			   					<div id="para1" style="clear: both;">
									<table class="table" style="width:100%; margin:2px 0px 5px 0px;">
										<thead>
											<tr>
												<!-- <th style="height:17px;">#</th> -->
												<th style="min-width: 150px;height:17px;">Attached File Name</th>
												<th style="width:50px; height:17px;" class="centeralign">File</th>
											</tr>
										</thead>
										<tbody>
											<c:if test="${not empty attachedFileList}">
												<c:set var = "replacedAttachedFileList" value = "${attachedFileList}"/>
												<c:set var="replacedAttachedFileList" value="${fn:replace(replacedAttachedFileList,'[', '')}" />
												<c:set var="replacedAttachedFileList" value="${fn:replace(replacedAttachedFileList,']', '')}" />
												<c:forTokens items="${replacedAttachedFileList}" delims="^" var="attachedFileItem" varStatus="rowCounter">
													<tr>
													<%-- <td style="border:1px solid #e0e0e0 !important;">${rowCounter.count}</td> --%>
														<td style="border:1px solid #e0e0e0 !important;"><s:textfield cssClass="input-textUpper"	name="fileName" value="${attachedFileItem}" readonly="true" cssStyle="width:95%;background:none;border:none;" />
														</td>
														<td style="text-align:center;border:1px solid #e0e0e0 !important;">
															<i class='fa fa-download' style="color:#0a84ff;" onclick="downloadAttachedFile('${attachedFileItem}','${emailSetupTemplate.id}');"></i>
														</td>
													</tr>
												</c:forTokens>
											</c:if>
										</tbody>
									</table>
								</div>
		   					</td>
							</tr>
							<tr>
							<td style="text-align:right;width:80px;" class="listwhitetext">Document&nbsp;Type</td>
							<td align="left" style="width:100px;">
								<configByCorp:customDropDown listType="map" list="${docsList}" fieldValue="${emailSetupTemplate.fileTypeOfFileCabinet}" attribute="class=list-menu name=emailSetupTemplate.fileTypeOfFileCabinet  style=width:275px headerKey='' headerValue='' "/>
							</td>
							</tr>
							<tr>
							<td colspan="2" style="padding-right:15px;">								
			   					<div id="para1" style="clear: both;">
									<table class="table" style="width:100%; margin:2px 0px 0px 0px;">
										<thead>
											<tr>
												<!-- <th style="height:17px;">#</th> -->
												<th style="min-width: 150px;height:17px;"> File From File Cabinet</th>
												<th style="width:50px; height:17px;" class="centeralign">File</th>
											</tr>
										</thead>
										<tbody>
											<c:if test="${not empty fileCabinetFileList}">
												<c:set var = "replacedAttachedFileList" value = "${fileCabinetFileList}"/>
												<c:set var="replacedAttachedFileList" value="${fn:replace(replacedAttachedFileList,'[', '')}" />
												<c:set var="replacedAttachedFileList" value="${fn:replace(replacedAttachedFileList,']', '')}" />
												<c:forTokens items="${replacedAttachedFileList}" delims="^" var="attachedFileItem" varStatus="rowCounter">
													<tr>
													<%-- <td style="border:1px solid #e0e0e0 !important;">${rowCounter.count}</td> --%>
														<td style="border:1px solid #e0e0e0 !important;"><s:textfield cssClass="input-textUpper"	name="fileName" value="${attachedFileItem}" readonly="true" cssStyle="width:95%;background:none;border:none;" />
														</td>
														<td style="text-align:center;border:1px solid #e0e0e0 !important;">
															<i class='fa fa-download' style="color:#0a84ff;" onclick="downloadFileCabinetFile('${attachedFileItem}','${emailSetupTemplate.id}');"></i>
														</td>
													</tr>
												</c:forTokens>
											</c:if>
										</tbody>
									</table>
								</div>
		   					</td>
							</tr>
							
							</table>
							
						</td>
						<td valign="top">
						<table style="margin:0px;padding:0px;width:100%;"  cellspacing="0" cellpadding="3" border="0">
						<tr>
						<td align="right" style="padding-left:5px;">
						<div id="para1" style="clear: both;">
							<table class="table" id="dataTable"
								style="width:100%; margin:2px 0px 5px 0px;">
								<thead>
									<tr>
										<!-- <th style="height:17px;">#</th> -->
										<th style="min-width: 200px;height:17px;">Jrxml Name</th>
										<th style="min-width: 200px;height:17px;">Form Description</th>
										<th width="50px" style="height:17px;" class="centeralign">PDF</th>
										<th width="50px" style="height:17px;" class="centeralign">Docx</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${not empty emailSetupTemplateFormsList}">
										<c:forEach var="individualItem"
											items="${emailSetupTemplateFormsList}" varStatus="rowCounter">
											<tr>
												<%-- <td style="border:1px solid #e0e0e0 !important;">${rowCounter.count}</td> --%>
												<td style="border:1px solid #e0e0e0 !important;"><s:textfield cssClass="input-textUpper"
														name="reportName" value="${individualItem.reportName}"
														id="rep${rowCounter.count}" readonly="true"
														onkeyup="autoCompleterAjaxCall(this.value,'${rowCounter.count}','choices');"
														cssStyle="width:95%;background:none;border:none;" />
													<div id="choices${rowCounter.count}" class="listwhitetext"></div>
												</td>
												<td style="border:1px solid #e0e0e0 !important;"><s:textfield cssClass="input-textUpper"
														name="reportDescription" readonly="true"
														value="${individualItem.description}"
														id="des${rowCounter.count}" cssStyle="width:95%;background:none;border:none;" />
												</td>
												<td style="border:1px solid #e0e0e0 !important;">
													<c:if test="${individualItem.pdf=='true' }">
														<div align="center">
															<i class='fa fa-download' style="color:#0a84ff;" onclick="generateFile('${individualItem.id}','PDF');" />
														</div>
													</c:if>
													<c:if test="${individualItem.pdf=='false' || individualItem.pdf==''}">
														<div align="center">
															<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
														</div>
													</c:if>
												</td>
												<td style="border:1px solid #e0e0e0 !important;">
													<c:if test="${individualItem.docx=='true' }">
														<div align="center">
															<i class='fa fa-download' style="color:#0a84ff;" onclick="generateFile('${individualItem.id}','DOCX')"  />
														</div>
													</c:if>
													<c:if test="${individualItem.docx=='false' || individualItem.docx==''}">
														<div align="center">
															<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
														</div>
													</c:if>
												</td>
											</tr>
										</c:forEach>
									</c:if>
								</tbody>
	
							</table>
						</div>
						</td>
						</tr>
						</table>
						</td>
						</tr>
						</table>
					   </fieldset>
						</td>
					</tr>
					
					<tr>
						<td colspan="0" valign="top" class="no-mp">
						
						<table style="margin:0px;padding:0px;"  cellspacing="3" cellpadding="3" border="0">
						<tr>
						<td class="listwhitetext" width="45" style="text-align:right!important;">From </td>
						<td align="left" ><s:textfield name="emailSetupTemplate.emailFrom" id="emailFrom" onchange="validateFieldsName(this);" cssStyle="width:320px;" maxlength="50" cssClass="input-text" /></td>
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:right!important;">To </td>
						<td align="left" ><s:textfield name="emailSetupTemplate.emailTo" id="emailTo" cssStyle="width:320px;" onchange="validateFieldsName(this);" maxlength="50" cssClass="input-text" /></td>
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:right!important;">Cc </td>
						<td align="left" ><s:textfield name="emailSetupTemplate.emailCc" cssStyle="width:320px;" maxlength="50" cssClass="input-text" /></td>
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:right!important;">Bcc </td>
						<td align="left" ><s:textfield name="emailSetupTemplate.emailBcc" cssStyle="width:320px;" maxlength="50" cssClass="input-text" /></td>
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:right!important;">Subject </td>
						<td align="left" ><s:textfield name="emailSetupTemplate.emailSubject" id="emailSubject" cssStyle="width:320px;" maxlength="200" cssClass="input-text" /></td>
						</tr>								
						</table>								
														
						<table style="margin:0px;padding:0px;"  cellspacing="0" cellpadding="3" border="0">
						<tr>
						<td align="right" class="listwhitetext" width="50">Distribution Method </td>
							<td align="left">
							<configByCorp:customDropDown listType="map" list="${distributionMethodList}" fieldValue="${emailSetupTemplate.distributionMethod}" attribute="class=list-menu name=emailSetupTemplate.distributionMethod  style=width:90px;margin-left:-2px headerKey='' headerValue='' "/>
							</td>
						</tr>								
						</table>								
						</td>								
						<td colspan="0" style="margin:0px;padding:0px;"  valign="top">
					    <table style="margin:0px;padding:0px;padding-top:2px;margin-bottom:10px;"  cellspacing="0" cellpadding="3" border="0">
					    <tbody>
					    <tr> 
	   						<td align="left"><s:textarea  cssClass="textarea" id="emailBody${emailSetupTemplate.id}" name="emailSetupTemplate.emailBody" cols="122" rows="10" onkeypress="" /></td>
						</tr>
						</tbody>
						</table>								
						</td>									 
					</tr>								
					
					
				</tbody>
				</table>
		</div>
		
</s:form>
</div></div></div>
<script language="javascript" type="text/javascript">
function downloadAttachedFile(fileName,emailId){
	var cid = document.forms['emailTemplateFormPopUp'].elements['cid'].value;
	var sid = document.forms['emailTemplateFormPopUp'].elements['sid'].value;
	var pageType = "popUp";
	var url="downloadAttachedFileFromEmailSetupTemplate.html?fileName="+encodeURI(fileName)+"&id="+emailId+"&cid="+cid+"&sid="+sid+"&pageType="+pageType;
	location.href=url;
}
function downloadFileCabinetFile(fileName,emailId){
	var cid = document.forms['emailTemplateFormPopUp'].elements['cid'].value;
	var sid = document.forms['emailTemplateFormPopUp'].elements['sid'].value;
	var url="downloadFileCabinetFileFromEmailSetupTemplate.html?fileName="+encodeURI(fileName)+"&id="+emailId+"&cid="+cid+"&sid="+sid;
	 location.href=url;
}
function generateFile(id,type){
	document.forms['emailTemplateFormPopUp'].elements['reportsId'].value=id;
	 if(type=='PDF'){
		 document.forms['emailTemplateFormPopUp'].elements['fileTypeName'].value=type; 
	 }else{
		 document.forms['emailTemplateFormPopUp'].elements['fileTypeName'].value=type;
	 }
	 
	 var url = 'generateFileFromJrxmlEmailSetupTemplate.html?decorator=popup&popup=true';
		document.forms['emailTemplateFormPopUp'].action =url;
		document.forms['emailTemplateFormPopUp'].submit();
		
}
</script>