package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.VatIdentificationDao;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.VatIdentification;

public class VatIdentificationDaoHibernate extends GenericDaoHibernate<VatIdentification, Long> implements VatIdentificationDao {

	public VatIdentificationDaoHibernate() {
		 super(VatIdentification.class);
		// TODO Auto-generated constructor stub
	}
	public List findAllVat() {
		try{
		return getHibernateTemplate().find("from VatIdentification");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
	
	public List isCountryExisted(String Country ,String corpid){
		try{
		return getHibernateTemplate().find("from VatIdentification where countryCode='"+Country+"' and corpID='"+corpid+"' ");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
	public List searchAll(String vatCountryCode){
		try{
		List exCurrency = getHibernateTemplate().find("from VatIdentification where countryCode like '" +vatCountryCode+"%' ");		
		return exCurrency;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
	public String getIdVatCode(String vatCountryCode){
		try{
		String vatId="";
		List tempId=getHibernateTemplate().find("select id from VatIdentification where countryCode like '" +vatCountryCode+"%' ");
		try{
			if((tempId!=null) && (!(tempId.isEmpty()))&& (tempId.get(0)!=null))
			{
				vatId=tempId.get(0).toString();
			}
			}catch(Exception e){
				vatId="";
			}
		return vatId;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
}
