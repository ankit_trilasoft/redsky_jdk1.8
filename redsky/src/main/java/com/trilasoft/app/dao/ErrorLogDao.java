package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ErrorLog;



public interface ErrorLogDao extends GenericDao<ErrorLog,Long> {
	 public List findDistinct();
	 public List searchErrorLogFile(String corpid,String module,String createdBy,Date createdOn);
}
