<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<head>
	<title>Pricing Engine</title>
	<meta name="heading" content="Pricing Engine"/>
    <meta name="menu" content="UserIDHelp"/>
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />
<style>

#content-containertable.override{
width:190px;
}

#content-containertable.override div{
height:460px;
overflow:scroll;
}

#content-containertable.override1{
width:190px;
}

#content-containertable.override1 div{
height:150px;
overflow:scroll;
}

#content-containertable.override2{
width:100%;
}

#content-containertable.override2 div{
height:165px;
overflow:scroll;
}

.listwhitetext-head {
color:#FFFFFF;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
text-decoration:none;
}

#priceinfotab_bg{background:#ffffff url(images/price_tab_bg.png) repeat-x;height:26px; !width:70%; width:90%; color: #3A8AB3;font:13px Arial, Helvetica, sans-serif ; font-weight:bold; cursor: pointer;padding:0px; border:1px solid #5FA0C0; border-bottom:none;}

.inner-tab a, a:link a:active {
color:#FFFFFF;text-decoration:none;font-weight:bold;font-family:arial,verdana; font-size:11px;
}
.price_tleft{float:left;padding:3px 2px 0px 8px;!margin-top:0px;}

.filter-head{color: #484848;font:11px arial,verdana ; font-weight:bold; cursor: pointer; }

</style>

</head>
<%--
<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w" type="text/javascript"></script>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghRtKhpDtLX6R5MagO3ZOu0_GHeYthTZmolg4KuvVTJhWVj_M3YuGSUoeA" type="text/javascript"></script>
--%>

<style type="text/css">

</style>

<s:form id="agentPricing" name="agentPricing" action='locatePartners1.html' method="post" validate="true">
<s:hidden key="pricingControl.weight"/>
<s:hidden key="pricingControl.mode"/>
<s:hidden key="pricingControl.packing"/>
<s:hidden key="pricingControl.containerSize"/>
<s:hidden key="pricingControl.isOrigin"/>
<s:hidden key="pricingControl.isDestination"/>
<s:hidden key="pricingControl.isFreight"/>
<s:hidden key="pricingControl.destinationLatitude"/>
<s:hidden key="pricingControl.destinationLongitude"/>
<s:hidden key="pricingControl.destinationCountry"/>
<s:hidden key="pricingControl.destinationPOE"/>
<s:hidden key="pricingControl.destinationPOEName"/>
<s:hidden key="pricingControl.id"/>
<s:hidden key="pricingControl.pricingID"/>
<s:hidden key="latitude"/>
<s:hidden key="longitude"/>
<s:hidden key="pricingControl.sequenceNumber"/>
 <sec-auth:authComponent componentId="module.hiddenCheckBox.pricingWizard.markUp">
 <s:hidden key="markUp" value="true"/>
 </sec-auth:authComponent>
<div id="newmnav" style="!margin-bottom:5px;float:left;">
		  <ul>
		  <c:if test="${!param.popup}">
		 	 	<li><a href="agentPricingBasicInfo.html?id=${pricingControl.id}"><span>Basic Info</span></a></li>
			  	<li><a onclick="return checkOriginScope()" href="agentPricingOriginInfo.html?id=${pricingControl.id}"><span>Origin Options</span></a></li>
			  	<li><a onclick="return checkFreightScope()" href="agentPricingFreightInfo.html?id=${pricingControl.id}"><span>Freight Options</span></a></li>
			  	<li id="newmnav1" ><a class="current"><span>Destination Options<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  	<li><a href="agentPricingSummaryInfo.html?id=${pricingControl.id}"><span>Pricing Summary</span></a></li>
			  	<li><a href="agentPricingAllPricingInfo.html?id=${pricingControl.id}"><span>Pricing Options</span></a></li>
			  	 <li><a href="agentPricingList.html"><span>Quote List</span></a></li>
			  	<sec-auth:authComponent componentId="module.tab.pricingWizard.accountingTab">
			  	 <c:if test="${not empty pricingControl.serviceOrderId}">
			  	<li><a onclick="return checkControlFlag();"><span>Accounting</span></a></li>
		   		</c:if>	
		  	 </sec-auth:authComponent>
		  </c:if>	
		   <c:if test="${param.popup}">
		  	<li><a href="agentPricingList.html?serviceOrderId=${serviceOrderId}&decorator=popup&popup=true"><span>Pricing Engine List</span></a></li>
		  </c:if>
		  	
		  </ul>
		</div>
		<!--<div style="float: left; font-size: 13px; margin-left: 50px;margin-top:-3px;">
		<a href="http://www.sscw.com/movingservices/international_move.html" target="_blank" style="text-decoration:underline;">
		
		<a href="#"  style="color:#330000;" onclick="javascript:window.open('http://www.sscw.com/redsky/agentportal/','help','width=1004,height=500,scrollbars=yes,left=100,top=50');">
		<img src="images/wizard-button.png" border="0"/></a></div>
		--><div class="spn">&nbsp;</div>
<table class="mainDetailTable" cellspacing="0" cellpadding="0" border="0"  style="width: 100%;" >
  <tbody>
  <tr>
  <td colspan="2" align="left" valign="top" height="82" class="price-bg">
  <div class="filter-bg">
  <table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;margin-top:2px;!margin-top:0px;">
  <tr>  
  <td rowspan="4" width="180" align="center"> <table cellspacing="0" cellpadding="0" border="0" style="margin:0px;padding:0px;">
  
   <tr>
  <td>
  <input type="button" class="cssbutton1" value="Reset Filters" style="width:85px;" onclick="reset_fields();"/>
  </td>
  
  </tr>
  <tr>
  <td height="2px"></td>  
  </tr>
   <tr>
   <td align="center" class="listwhitetext" valign="middle" style="font-size:13px;">DTHC Included</td>	
   <td class="listwhitetext" valign="bottom"><s:checkbox key="DTHC"  fieldValue="true" onclick="updateDTHCFlag();locatePartners()" /></td>
  </tr>
   <tr>
   <sec-auth:authComponent componentId="module.checkBox.pricingWizard.markUp">
   <td align="center" class="listwhitetext" valign="middle" style="font-size:13px;">Price Markup</td>	
   <td class="listwhitetext" valign="bottom"><s:checkbox key="markUp"  fieldValue="true" onclick="locatePartners()"/></td>
 	</sec-auth:authComponent>
  </tr>
  
  
  	</table>
  
  </td>
  
  
  <td align="left" valign="top">
  <table border="0" style="margin:0px;padding:0px;margin-top:2px;!margin-top:0px;">
  <tr> 
   <td><img src="images/arrow-filter.png" width="20" height="12" border="0" /></td>
  <td align="left" class="filter-head">Filters:&nbsp;</td>
  
  <td align="right" class="listwhitetext">FIDI</td>
  <td align="left" class="listwhitetext"><s:checkbox key="FIDI" fieldValue="true" onclick="locatePartners()"/></td> 
  
  <td align="right" class="listwhitetext">OMNI</td>
  <td align="left" class="listwhitetext"><s:checkbox key="OMNI" fieldValue="true" onclick="locatePartners()"/></td>
  
   <td align="left" colspan="6">
   <table cellspacing="0" cellpadding="0" border="0" style="margin:0px;padding:0px;">
   <tr>
   <td align="left" class="listwhitetext">Minimum Feedback Rating&nbsp;</td>
   <td><s:select cssClass="list-menu" name="minimumFeedbackRating" list="{'50','60','70','80','90','100'}" headerKey="" headerValue="" cssStyle="width:50px" onchange="locatePartners();"/></td>
  <td width="10px"></td>
   <td><input type="button" class="cssbutton1" value="Refresh" style="width:85px;" onclick="refreshWindow()"/> </td>
   </tr>
   </table>
  
   </td>
   
   </tr>
   
   <tr>  
  <td width="20"></td>
  <td align="left" class="filter-head">Certifications:&nbsp;</td>
  
  <td align="right" class="listwhitetext">FAIM ISO</td>
  <td class="listwhitetext"><s:checkbox key="FAIM"  fieldValue="true" onclick="locatePartners()"/></td>
  
  <td align="right" class="listwhitetext">ISO 9002</td>
  <td class="listwhitetext"><s:checkbox key="ISO9002"  fieldValue="true" onclick="locatePartners()"/></td>
  
  <td align="right" class="listwhitetext">ISO 27001</td>
  <td class="listwhitetext"><s:checkbox key="ISO27001" fieldValue="true" onclick="locatePartners()"/></td>
  
  <td align="right" class="listwhitetext">ISO 14000</td>
  <td class="listwhitetext"><s:checkbox key="ISO14000"  fieldValue="true" onclick="locatePartners()"/></td>
  
  <td align="right" class="listwhitetext">RIM</td>
  <td class="listwhitetext"><s:checkbox key="RIM"  fieldValue="true" onclick="locatePartners()"/></td>
  
   </tr>
   
   <!--<tr>  
  <td width="20"></td>
  <td align="left" class="filter-head">Service Lines:&nbsp;</td>
  
  <td align="right" class="listwhitetext">DOS</td> 
  <td class="listwhitetext"><s:checkbox key="DOS"  fieldValue="true" onclick="locatePartners()"/></td>
  
  <td align="right" class="listwhitetext">GSA</td>
  <td class="listwhitetext"><s:checkbox key="GSA" fieldValue="true" onclick="locatePartners()"/></td>
  
  <td align="right" class="listwhitetext">Military</td>
  <td class="listwhitetext"><s:checkbox key="Military"  fieldValue="true" onclick="locatePartners()"/></td>
  </tr>
  --></table>
  </td>
  
  
  
 
  
   </tr>
   
  </table>
	</div>
  </td>
  </tr>
  <tr>
  <td valign="top" align="left" style="margin:0px;padding: 0px; background-color:#F6F9FA;">
  
  <table style="margin:0px;padding:0px;width:100%;!width:89%;" border="0" cellpadding="0" cellspacing="0" >    
    <tr>
    <td style="padding-left:1px;">
      <div id="priceinfotab_bg"><div class="price_tleft">Destination Results:</div></div>
      </td>
	</tr>
	  <tr>
      <td align="left" colspan="2">
       <div id="content-containertable" class="override">
			 <div id="partnerList"></div>
			 </div>
      </td>
    </tr><!--
    
    <tr>
    <td align="left">
    <table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:100%;">
        <tr>
          <td align="left" style="padding-left:1px;">
      <div id="priceinfotab_bg"><div class="price_tleft">Distances:</div></div>
      </td>
        </tr>
      </table></td>
    </tr>
      --><tr>
    
      <td align="left" colspan="2">
         
			 <div id="content-containertable" class="override1"><!--
			 <div id="distanceList"></div>
			 --></div>
		
	 </td>
    </tr>
    
    
    <tr>
      <td colspan="2" rowspan="" class="listwhitetext">&nbsp;</td>
    </tr>
    
  </table></td>
        
     <td width="81%" valign="top" style="margin:0px;padding: 0px;">
     	<table border="0" cellpadding="2" cellspacing="1" width="100%" style="margin:0px;padding: 0px;">
		  <tbody>  	
		 
		 <tr>     
        	<td valign="top" style="margin:0px;padding: 0px;width:100%;border:1px solid #969694;">
			<div id="content-containertable" class="override2">
        	 <div>
				<display:table name="agentMasterList" class="table" requestURI="" id="agentMasterListId" export="true"  defaultsort="7" defaultorder="ascending" pagesize="200" style="width:100%" >   
					<display:column sortable="true" title="#">
						<c:out value="${agentMasterListId_rowNum}" />
					</display:column>
					<display:column sortable="true" title="Agent">
						<a  onclick="openPartnerProfile('${agentMasterListId.destinationPartnerId}');"><c:out value="${agentMasterListId.destinationAgent}" /></a>
					</display:column>
					<display:column sortable="true" title="Activity/ Feedback%">
				   		<c:out value="${agentMasterListId.destinationAgentActivity}" />/
				   		<c:out value="${agentMasterListId.destinationAgentFeedback}" />%
				   	</display:column>
				   	
				   	<display:column property="destinationPort"  sortable="true" title="Port"/>
				   	<display:column property="haulingDistance"  sortable="true" title="Hauling Dist."/>
				   	<display:column property="addressDistance"  sortable="true" title="Address Dist."/>
				   	<display:column property="destinationCost"  sortable="true" title="Quote" />
				   	
				  <%--<c:if test="${pricingControl.isFreight!='true'}">						   	
				   	<display:column   title="Select" style="width:10px">
				   		<c:if test="${agentMasterListId.destinationSeletion==true }">
				 			<input style="vertical-align:bottom;" type="radio" name="radioFreight" checked="checked" onclick="selectRespectiveRecords('${agentMasterListId.destinationAgentId}')"/>
				 		</c:if>
				 		<c:if test="${agentMasterListId.destinationSeletion!=true }">
				 			<input style="vertical-align:bottom;" type="radio" name="radioFreight" onclick="selectRespectiveRecords('${agentMasterListId.destinationAgentId}')"/>
				 		</c:if>
			 		</display:column>
			 	</c:if> 
			--%></display:table>
			</div>
			</div>
            </td>
        </tr>
		 
		 <tr>      
        	<td valign="top" style="margin:0px;padding: 0px;width:74%;border:1px solid #969694;">
				<div id="map" style="width: 100%; height: 500px;margin:0px;padding: 0px;">
            </td>
        </tr>
        
        <tr>
        <td height="15"></td>
        </tr>
         <tr>
        <td colspan="">
        
        </td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        </tbody>
        </table>

</s:form>

<%-- Script Shifted from Top to Botton on 11-Sep-2012 By Kunal --%>

<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>"djConfig="parseOnLoad:true, isDebug:false"></script>
<style><%@ include file="/common/calenderStyle.css"%></style>
<script language="javascript" type="text/javascript"><%@ include file="/common/formCalender.js"%></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>

<script language="javascript" type="text/javascript">
function initLoader() { 
	 var script = document.createElement("script");
     script.type = "text/javascript";
     script.src = "http://maps.google.com/maps?file=api&v=2&key="+googlekey+"&async=2&callback=loadGoogleGeoCode";
     //alert(script.src);
     document.body.appendChild(script);
     setTimeout('',6000);
     }

var geocoder;
var map;
 function loadGoogleGeoCode () {
document.forms['agentPricing'].elements['markUp'].checked = true;
if(!'${pricingControl.dthcFlag}'){
	document.forms['agentPricing'].elements['DTHC'].checked = true;
}

      // Create new map object
      map = new GMap2(document.getElementById("map"));

      // Create new geocoding object
      geocoder = new GClientGeocoder();
		addToMapDestination();	
		getPortsPoint();
	locatePartners();
	 
       if(document.forms['agentPricing'].elements['pricingControl.sequenceNumber'].value!=''){
		 var elementsLen=document.forms['agentPricing'].elements.length;
		 for(i=0;i<=elementsLen-1;i++)
			{
					if(document.forms['agentPricing'].elements[i].type=='text')
					{
						document.forms['agentPricing'].elements[i].readOnly =true;
						document.forms['agentPricing'].elements[i].className = 'input-textUpper';
						
					}
					else
					{
						document.forms['agentPricing'].elements[i].disabled=true;
					}
			}
			
		 }
   }
   
   function getPortsPoint(){
		dojo.xhrPost({
	       form: "agentPricing",
	       url:"portsPointPerDestination.html",
	       timeout: 9000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){
	        		//var jsonData = dojo.toJson(data)
	        	
	           for(var i=0; i < jsonData.ports.length; i++){
	           	           	    
	        	   console.log("lattitude: " + jsonData.ports[i].latitude + ", longitude: " +  jsonData.ports[i].longitude);
	        	   point = new GLatLng(jsonData.ports[i].latitude, jsonData.ports[i].longitude);
	        	   addPortsCoordsToMap(point, jsonData.ports[i]);
	        	   var delme = 0;
	        	}
	       }	       
	   });
	
	
	//var cord1=document.forms['agentPricing'].elements['latitude'].value;
	//var cord2=document.forms['agentPricing'].elements['longitude'].value;
  	//point = new GLatLng(cord1,cord2);
  	
	 //addPortsCordToMap(point, null); 
	 // map.setUIToDefault();
  }
  
  function addPortsCoordsToMap(point, portDetails){
	//portDetails=document.forms['agentPricing'].elements['pricingControl.destinationPOE'].value;
	//portDetails1=document.forms['agentPricing'].elements['pricingControl.destinationPOEName'].value;
	      // Create a marker
	      // marker = new GMarker(point);
		// Create our "tiny" marker icon
		var yellowIcon = new GIcon(G_DEFAULT_ICON);
		yellowIcon.image = "${pageContext.request.contextPath}/images/markerP-yellow.png";
		                
		// Set up our GMarkerOptions object
		markerOptions = { icon:yellowIcon };
	      var marker = new GMarker(point, markerOptions); 
		 map.addOverlay(marker, markerOptions);
	    	var fn = markerClickFnPorts(point, portDetails);
	      GEvent.addListener(marker, "click", fn);
	  }
function markerClickFnPorts(point, portDetails) {
	   return function() {
		   if (!portDetails) return;
	      
	       var infoHtml = '<div style="width:100%;"><h3>'+portDetails.portcode+', '+portDetails.name+'</h3>';
	            
	       infoHtml += '</div></div>';
	       map.openInfoWindowHtml(point, infoHtml);

	   }
   }
   function reset_fields(){
   	document.forms['agentPricing'].elements['FIDI'].checked = false;
	document.forms['agentPricing'].elements['OMNI'].checked = false;
	document.forms['agentPricing'].elements['minimumFeedbackRating'].value = "";
	document.forms['agentPricing'].elements['FAIM'].checked = false;
	document.forms['agentPricing'].elements['ISO9002'].checked = false;
	document.forms['agentPricing'].elements['ISO27001'].checked = false;
	document.forms['agentPricing'].elements['ISO14000'].checked = false;
	document.forms['agentPricing'].elements['RIM'].checked = false;
	document.forms['agentPricing'].elements['DOS'].checked = false;
	document.forms['agentPricing'].elements['GSA'].checked = false;
	document.forms['agentPricing'].elements['Military'].checked = false;
   locatePartners();
   } 
 function addToMapDestination()
   {
      // Retrieve the object
      //place = response.Placemark[0];
	var coordinates1=document.forms['agentPricing'].elements['pricingControl.destinationLatitude'].value;
	var coordinates2=document.forms['agentPricing'].elements['pricingControl.destinationLongitude'].value;
      point = new GLatLng(coordinates1,coordinates2);
	  addBasicInfoToMap(point, null); 
	  map.setUIToDefault();
	   map.disableScrollWheelZoom();
  }
  
    function addBasicInfoToMap(point, partnerDetails){

	      // Create a marker
	      marker = new GMarker(point);
		// Create our "tiny" marker icon
		var yellowIcon = new GIcon(G_DEFAULT_ICON);
		yellowIcon.image = "${pageContext.request.contextPath}/images/target.gif";
		                
		// Set up our GMarkerOptions object
		markerOptions = { icon:yellowIcon };
	      
		 map.addOverlay(new GMarker(point, markerOptions));
	  		 map.setCenter(point, 8);
		  map.setUIToDefault();
	  }
 
function getPricing(){
		locatePartners();
} 

function selectRespectiveRecords(target){
	var id= target;
	var pricingControlID='${pricingControl.id}';
	var url="updatePricingDetail.html?ajax=1&decorator=simple&popup=true&tarrifApplicability=Destination&pricingDetailsId=" + encodeURI(id)+'&pricingControlID='+encodeURI(pricingControlID);
	http2.open("GET", url, true);
  	//http2.onreadystatechange = handleHttpResponseFieldList;
   	http2.send(null);
	locatePartners();
}

function openPartnerProfile(target)
{
	window.open('findPartnerProfileList.html?id='+target+'&decorator=popup&popup=true','profile','scrollbars=1,left=50,top=100,resizable=yes,width=900,height=450');
	//findPartnerProfileList.html?code=${partner.partnerCode}&partnerType=${partnerType}&id=${partner.id}"
}

String.prototype.trim = function() {
	    return this.replace(/^\s+|\s+$/g,"");
	}
	String.prototype.ltrim = function() {
	    return this.replace(/^\s+/,"");
	}
	String.prototype.rtrim = function() {
	    return this.replace(/\s+$/,"");
	}
	String.prototype.lntrim = function() {
	    return this.replace(/^\s+/,"","\n");
	}
	
	
	function getHTTPObject()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
	}
    var http2 = getHTTPObject();

function locatePartners(){
	var markUp=document.forms['agentPricing'].elements['markUp'].checked;
	var DTHC=document.forms['agentPricing'].elements['DTHC'].checked;
	// post some data, ignore the response:
	   dojo.xhrPost({
	       form: "agentPricing", // read the url: from the action="" of the <form>
	       timeout: 3000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){
	        reinitMarkers();
	         var d=document.getElementById("partnerList");
	         // var d1=document.getElementById("distanceList");
	         //  var d2=document.getElementById("masterList");
	          // d1.innerHTML='';
	           d.innerHTML='';	
	         //  d2.innerHTML='';    
	         // d1.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;color:#FFFFFF;"><tr><td valign="top" width="20px">#</td><td width="20px" align="center" title="Distance between hauling ports">Hauling</td><td width="65px" align="center" title="Distance between Origin and destination address">Address</td></tr>' ;
	         // d2.innerHTML+='<table width="100%" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;color:#FFFFFF;"><tr>'+
	         // '<td valign="top" width="5%">#</td>' +
	         // '<td width="20%" align="center" title="">Partner</td>'+
	        //  '<td width="10%" align="center" title="">Activity%/Feedback</td>'+
	         // '<td width="10%" align="center" title="">Port Code</td>'+
	         // '<td width="10%" align="center" title="">Quote</td>'+
	         // '<td width="10%" align="center" title="">Base Rate</td>'+
	         // '<td width="10%" align="center" title="">Hauling</td>'+
	        //  '<td width="10%" align="center" title=""></td>'+
	        //  '<td width="10%" align="center" title="">Select</td>'+
	       //   '</tr>' ;
	      
	               for(var i=0; i < jsonData.partners.length; i++){
	              // var letter = String.fromCharCode("A".charCodeAt(0) + i);
	                 var letter = i+1;
	           		var ii=i+1;
	           		var exchangeRate=jsonData.partners[i].exchangeRate;
	           		var chargeDetail=jsonData.partners[i].chargeDetail;
	           		var weightDetail=chargeDetail.substring(7,chargeDetail.indexOf(","));
	           		var weightDetailRound=weightDetail;
	           		var charges=chargeDetail.substring(chargeDetail.indexOf(",")+1,chargeDetail.indexOf("Hauling")-1);
	           		if(charges==',')
	           		{
	           			charges='';
	           		}
	           		var chargeArray=new Array();
	           		chargeArray = charges.split(",");
	           		
	           		var chargeDetailMarkup=jsonData.partners[i].chargeDetailMarkUp;
	           		var chargeDetailMarkUpWithoutTHC=jsonData.partners[i].chargeDetailMarkUpWithoutTHC;
	           		var forwarding=chargeDetailMarkup.substring(chargeDetailMarkup.indexOf(":")+1,chargeDetail.length);
	           		var forwardingWithoutTHC=chargeDetailMarkUpWithoutTHC.substring(chargeDetailMarkUpWithoutTHC.indexOf(":")+1,chargeDetail.length);
	           		// var weightDetailMarkup=chargeDetailMarkup.substring(7,chargeDetailMarkup.indexOf(","));
	           		//var weightDetailRoundMarkup=weightDetailMarkup;
	           		//var chargesMarkUp=chargeDetailMarkup.substring(chargeDetailMarkup.indexOf(",")+1,chargeDetailMarkup.indexOf("Hauling")-1);
	           		//if(chargesMarkUp==',')
	           		//{
	           		//	chargesMarkUp='';
	           		//}
	           		//var chargeArrayMarkUp=new Array();
	           		//chargeArrayMarkUp = chargesMarkUp.split(",");
	           		
	           		// 0 THC:250.00 Flat 
	           		// 1 Customs Clearance:200.00 Flat 
	           		
	           		var hauling=chargeDetail.substring(chargeDetail.indexOf("Hauling:")+8,chargeDetail.length);
	           		var haulingRound=hauling;
	           		
	           		//var haulingMarkup=chargeDetailMarkup.substring(chargeDetailMarkup.indexOf("Hauling:")+8,chargeDetailMarkup.indexOf("Forwarding")-1);
	           		//var haulingMarkupRound=haulingMarkup;
	           		
	           		//var forwardingMarkup=chargeDetailMarkup.substring(chargeDetailMarkup.indexOf("Forwarding")+11,chargeDetailMarkup.length);
	           		var haulingDistance=jsonData.partners[i].haulingDistance;
	           		var addressDistance=jsonData.partners[i].addressDistance;
	           		
	           		if(haulingDistance=='null')
	           		{
	           			haulingDistance=00;
	           		}
	           		if(addressDistance=='null')
	           		{
	           			addressDistance=00;
	           		}
	           		if(haulingDistance.length==1){
	           			haulingDistance='0' +haulingDistance;
	           		}
	           		if(addressDistance.length==1){
	           			addressDistance='0' +addressDistance;
	           		}
	           		
	           		var quoteBasisValue='';
	           		if(jsonData.partners[i].basis=='Flat'){
	           			quoteBasisValue='Flat'
	           		}else{
	           			quoteBasisValue=(Math.round(jsonData.partners[i].basisWithoutMarkUp*exchangeRate*100)/100).toFixed(2)+' cwt';
	           		}
	           		
	           		var THCValue=00;
	           		var totalQuoteWithoutTHC=00;
	           		//d1.innerHTML+= '<table width="100%" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"></table></td>' +
	           		//'<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td valign="top" class="listwhitetext" width="35px" style="color:#003366;">'+letter+'</td><td class="listwhitetext" width="40px" align="center">'+haulingDistance+'</td><td class="listwhitetext" align="center">'+addressDistance+'</td></tr></table></td>';
	           		
	           		//d2.innerHTML+= '<table width="100%" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"></table></td>' +
	           		//'<table width="100%" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr>'+
	           		//'<td valign="top" class="listwhitetext" width="5%" style="color:#003366;">'+letter+'</td>'+
	           		//'<td class="listwhitetext" width="20%" align="left">'+jsonData.partners[i].name+'</td>'+
	           		//'<td valign="top" class="listwhitetext" width="10%" style="color:#003366;">'+jsonData.partners[i].feedbackRating+'% / '+jsonData.partners[i].shipmentActivity+'</td>'+
	           		//'<td class="listwhitetext" align="left" width="10%">'+jsonData.partners[i].destinationPortCode+'</td>'+
	           		//'<td class="listwhitetext" align="left" width="10%">'+(Math.round(jsonData.partners[i].rateMarkUp*exchangeRate*100)/100).toFixed(2)+'</td>'+
	           		//'<td class="listwhitetext" align="left" width="8%">'+(Math.round(weightDetailRoundMarkup*exchangeRate*100)/100).toFixed(2)+'</td>'+
	           		//'<td class="listwhitetext" align="left" width="10%">'+(Math.round(haulingMarkupRound*exchangeRate*100)/100).toFixed(2)+'</td>'+
	           		//'<td class="listwhitetext" align="left" width="10%"></td>'+
	           		//'<td class="listwhitetext" valign="top"><input style="" type="radio" name="rgroup2" id="rgroup2" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" /></td>'+
	           		//'</tr></table></td>';
	           		
	           		if(jsonData.partners[i].destinationSelection=="true"){
	           		
	           		
	           		if(markUp==true && DTHC==true)
	           		{
	           			if(jsonData.partners[i].basis=='Flat'){
	           			quoteBasisValue='Flat'
	           		}else{
	           			quoteBasisValue=(Math.round(jsonData.partners[i].basisWithoutMarkUp*exchangeRate*100)/100).toFixed(2)+' cwt';
	           		}
					if(jsonData.partners[i].destinationSelection=="true")
					{
					
					if(document.forms['agentPricing'].elements['pricingControl.sequenceNumber'].value!=''){
					d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+(Math.round(jsonData.partners[i].rateMarkUp*exchangeRate*100)/100).toFixed(2)+'</td><td class="listwhitetext" valign="top">'+
					'<input style="" type="radio" name="rgroup1" id="rgroup1" checked="checked" disabled="true" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />'+
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Port: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].destinationPortCode+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Forwarding: </td><td class="listwhitetext" valign="top">'+(Math.round(forwarding*exchangeRate*100)/100).toFixed(2)+' Flat</td></table>';
					}else{
					d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+(Math.round(jsonData.partners[i].rateMarkUp*exchangeRate*100)/100).toFixed(2)+'</td><td class="listwhitetext" valign="top">'+
					'<input style="" type="radio" name="rgroup1" id="rgroup1" checked="checked" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />'+
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Port: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].destinationPortCode+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Forwarding: </td><td class="listwhitetext" valign="top">'+(Math.round(forwarding*exchangeRate*100)/100).toFixed(2)+' Flat</td></table>';
					}
					
					
					for(var k =0; k<= chargeArray.length-2; k++)
					{
						var str=chargeArray[k];
					    var st1 = str.substring('0',str.indexOf(":")+1);
					    var st22 = str.substring(str.indexOf(":")+1,str.length)
					    var st2 = st22.substring('0',st22.indexOf(" "));
					    var st33 = st22.substring(st22.indexOf(" "),st22.length);
					    var st3 = st33.substring('0',st33.length);

						
						if(st1=='Customs Clearance:')
						{
							st1='Cus.Clear:';
						}
												
						d.innerHTML+= '<table width="100%" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td class="listwhitetext" valign="top" width="57px">'+st1+'</td><td class="listwhitetext" valign="top">'+(Math.round(st2*exchangeRate*100)/100).toFixed(2)+ ' '+st3+'</td>'  ;
					}
					}
					else
					{
					
					if(document.forms['agentPricing'].elements['pricingControl.sequenceNumber'].value!=''){
						d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top" width="75px">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+(Math.round(jsonData.partners[i].rateMarkUp*exchangeRate*100)/100).toFixed(2)+'</td>'  +
					'<td class="listwhitetext" valign="top">'+
					'<input style="margin-top:-2px;" type="radio" name="rgroup1" id="radio" disabled="true" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />' + 
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td> ' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Port: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].destinationPortCode+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Forwarding: </td><td class="listwhitetext" valign="top">'+(Math.round(forwarding*exchangeRate*100)/100).toFixed(2)+' Flat</td></table>';
					}else{
					d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top" width="75px">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+(Math.round(jsonData.partners[i].rateMarkUp*exchangeRate*100)/100).toFixed(2)+'</td>'  +
					'<td class="listwhitetext" valign="top">'+
					'<input style="margin-top:-2px;" type="radio" name="rgroup1" id="radio" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />' + 
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Port: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].destinationPortCode+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Forwarding: </td><td class="listwhitetext" valign="top">'+(Math.round(forwarding*exchangeRate*100)/100).toFixed(2)+' Flat</td></table>';
					}
						
					
					for(var k =0; k<= chargeArray.length-2; k++)
					{
						
						var str=chargeArray[k];
					    var st1 = str.substring('0',str.indexOf(":")+1);
					    var st22 = str.substring(str.indexOf(":")+1,str.length)
					    var st2 = st22.substring('0',st22.indexOf(" "));
					    var st33 = st22.substring(st22.indexOf(" "),st22.length);
					    var st3 = st33.substring('0',st33.length);

						
						if(st1=='Customs Clearance:')
						{
							st1='Cus.Clear:';
						}
												
						d.innerHTML+= '<table width="100%" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td class="listwhitetext" valign="top" width="57px">'+st1+'</td><td class="listwhitetext" valign="top">'+(Math.round(st2*exchangeRate*100)/100).toFixed(2)+ ' '+st3+'</td>'  ;
					}
					 
					}
					}
					
					
					if(markUp==true && DTHC==false)
					{
					
						if(jsonData.partners[i].basis=='Flat'){
	           			quoteBasisValue='Flat'
	           		}else{
	           			quoteBasisValue=(Math.round(jsonData.partners[i].basisWithoutMarkUp*exchangeRate*100)/100).toFixed(2)+' cwt';
	           		}
	           		
						if(jsonData.partners[i].destinationSelection=="true")
					{
					
					//for(var k =0; k<= chargeArray.length-2; k++)
					//{
					//	var str=chargeArray[k];
					//    var st1 = str.substring('0',str.indexOf(":")+1);
					//    var st22 = str.substring(str.indexOf(":")+1,str.length)
					//    var st2 = st22.substring('0',st22.indexOf(" "));
					//    var st33 = st22.substring(st22.indexOf(" "),st22.length);
					//    var st3 = st33.substring('0',st33.length);
						
					//	if(st1=='THC:')
					//	{
					//		THCValue=st2;
					//	}
				//	}
					
					totalQuoteWithoutTHC=((Math.round(jsonData.partners[i].quoteMarkupWithoutTHC*exchangeRate*100)/100)).toFixed(2);
					
					if(document.forms['agentPricing'].elements['pricingControl.sequenceNumber'].value!=''){
						d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+totalQuoteWithoutTHC+'</td>' +
					'<td class="listwhitetext" valign="top">'+
					'<input style="margin-top:-2px;" type="radio" name="rgroup1" id="radio" checked="checked" disabled="true" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />' +
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Port: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].destinationPortCode+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Forwarding: </td><td class="listwhitetext" valign="top">'+(Math.round(forwardingWithoutTHC*exchangeRate*100)/100).toFixed(2)+' Flat</td></table>';
					}else{
					d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+totalQuoteWithoutTHC+'</td>' +
					'<td class="listwhitetext" valign="top">'+
					'<input style="margin-top:-2px;" type="radio" name="rgroup1" id="radio" checked="checked" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />' +
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Port: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].destinationPortCode+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Forwarding: </td><td class="listwhitetext" valign="top">'+(Math.round(forwardingWithoutTHC*exchangeRate*100)/100).toFixed(2)+' Flat</td></table>';
					
					}
					
					
					for(var k =0; k<= chargeArray.length-2; k++)
					{
						var str=chargeArray[k];
					    var st1 = str.substring('0',str.indexOf(":")+1);
					    var st22 = str.substring(str.indexOf(":")+1,str.length)
					    var st2 = st22.substring('0',st22.indexOf(" "));
					    var st33 = st22.substring(st22.indexOf(" "),st22.length);
					    var st3 = st33.substring('0',st33.length);

						
						if(st1=='Customs Clearance:')
						{
							st1='Cus.Clear:';
						}
						if(st1!='THC:')
						{						
							d.innerHTML+= '<table width="100%" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td class="listwhitetext" valign="top" width="57px">'+st1+'</td><td class="listwhitetext" valign="top">'+(Math.round(st2*exchangeRate*100)/100).toFixed(2)+ ' '+st3+'</td>'  ;
						}
					}
					}
					else
					{
					
						//for(var k =0; k<= chargeArray.length-2; k++)
					//{
					//	var str=chargeArray[k];
					  //  var st1 = str.substring('0',str.indexOf(":")+1);
					  //  var st22 = str.substring(str.indexOf(":")+1,str.length)
					  //  var st2 = st22.substring('0',st22.indexOf(" "));
					 //   var st33 = st22.substring(st22.indexOf(" "),st22.length);
					  //  var st3 = st33.substring('0',st33.length);
						
					//	if(st1=='THC:')
					//	{
					//		THCValue=st2;
						//}
				//	}
					
					totalQuoteWithoutTHC=((Math.round(jsonData.partners[i].quoteMarkupWithoutTHC*exchangeRate*100)/100) - THCValue).toFixed(2);
					if(document.forms['agentPricing'].elements['pricingControl.sequenceNumber'].value!=''){
						d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top" width="75px">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+totalQuoteWithoutTHC+'</td>'  +
					'<td class="listwhitetext" valign="top">'+
					'<input style="margin-top:-2px;" type="radio" name="rgroup1" disabled="true" id="radio" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />' + 
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Port: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].destinationPortCode+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Forwarding: </td><td class="listwhitetext" valign="top">'+(Math.round(forwardingWithoutTHC*exchangeRate*100)/100).toFixed(2)+' Flat</td></table>';
					}else{
					d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top" width="75px">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+totalQuoteWithoutTHC+'</td>'  +
					'<td class="listwhitetext" valign="top">'+
					'<input style="margin-top:-2px;" type="radio" name="rgroup1" id="radio" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />' + 
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td> ' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].jsonData.partners[i].haulingBasis+'</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Port: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].destinationPortCode+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Forwarding: </td><td class="listwhitetext" valign="top">'+(Math.round(forwardingWithoutTHC*exchangeRate*100)/100).toFixed(2)+' Flat</td></table>';
					}
					
					
					for(var k =0; k<= chargeArray.length-2; k++)
					{
						
						var str=chargeArray[k];
					    var st1 = str.substring('0',str.indexOf(":")+1);
					    var st22 = str.substring(str.indexOf(":")+1,str.length)
					    var st2 = st22.substring('0',st22.indexOf(" "));
					    var st33 = st22.substring(st22.indexOf(" "),st22.length);
					    var st3 = st33.substring('0',st33.length);

						
						if(st1=='Customs Clearance:')
						{
							st1='Cus.Clear:';
						}
						if(st1!='THC:')
						{						
						d.innerHTML+= '<table width="100%" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td class="listwhitetext" valign="top" width="57px">'+st1+'</td><td class="listwhitetext" valign="top">'+(Math.round(st2*exchangeRate*100)/100).toFixed(2)+ ' '+st3+'</td>'  ;
						}
					}
					 
					}
					
					}
					
					
					if(markUp==false && DTHC==true)
					{
					
						if(jsonData.partners[i].basisWithoutMarkUp=='Flat'){
	           			quoteBasisValue='Flat'
	           		}else{
	           			quoteBasisValue=(Math.round(jsonData.partners[i].basisWithoutMarkUp*exchangeRate*100)/100).toFixed(2)+' cwt';
	           		}
					
						if(jsonData.partners[i].destinationSelection=="true")
					{
					
					
					totalQuoteWithoutTHC=((Math.round(jsonData.partners[i].rate*exchangeRate*100)/100)).toFixed(2);
					if(document.forms['agentPricing'].elements['pricingControl.sequenceNumber'].value!=''){
					d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+totalQuoteWithoutTHC+'</td>' +
					'<td class="listwhitetext" valign="top">'+
					'<input style="margin-top:-2px;" type="radio" name="rgroup1" disabled="true" id="radio" checked="checked" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />' +
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td></table>';
					
					}else{
					d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+totalQuoteWithoutTHC+'</td>' +
					'<td class="listwhitetext" valign="top">'+
					'<input style="margin-top:-2px;" type="radio" name="rgroup1" id="radio" checked="checked" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />' +
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td></table>';
					
					
					}
					
					
					for(var k =0; k<= chargeArray.length-2; k++)
					{
						var str=chargeArray[k];
					    var st1 = str.substring('0',str.indexOf(":")+1);
					    var st22 = str.substring(str.indexOf(":")+1,str.length)
					    var st2 = st22.substring('0',st22.indexOf(" "));
					    var st33 = st22.substring(st22.indexOf(" "),st22.length);
					    var st3 = st33.substring('0',st33.length);

						
						if(st1=='Customs Clearance:')
						{
							st1='Cus.Clear:';
						}
												
						d.innerHTML+= '<table width="100%" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td class="listwhitetext" valign="top" width="57px">'+st1+'</td><td class="listwhitetext" valign="top">'+(Math.round(st2*exchangeRate*100)/100).toFixed(2)+ ' '+st3+'</td>'  ;
						
					}
					}
					else
					{
						
					
					totalQuoteWithoutTHC=((Math.round(jsonData.partners[i].rate*exchangeRate*100)/100)).toFixed(2);
					if(document.forms['agentPricing'].elements['pricingControl.sequenceNumber'].value!=''){
					d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top" width="75px">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+totalQuoteWithoutTHC+'</td>'  +
					'<td class="listwhitetext" valign="top">'+
					'<input style="margin-top:-2px;" type="radio" name="rgroup1" disabled="true" id="radio" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />' + 
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td> ' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td></table>';
					
					}else{
					d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top" width="75px">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+totalQuoteWithoutTHC+'</td>'  +
					'<td class="listwhitetext" valign="top">'+
					'<input style="margin-top:-2px;" type="radio" name="rgroup1" id="radio" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />' + 
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td> ' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td></table>';
					
					}
						
					
					for(var k =0; k<= chargeArray.length-2; k++)
					{
						
						var str=chargeArray[k];
					    var st1 = str.substring('0',str.indexOf(":")+1);
					    var st22 = str.substring(str.indexOf(":")+1,str.length)
					    var st2 = st22.substring('0',st22.indexOf(" "));
					    var st33 = st22.substring(st22.indexOf(" "),st22.length);
					    var st3 = st33.substring('0',st33.length);

						
						if(st1=='Customs Clearance:')
						{
							st1='Cus.Clear:';
						}
												
						d.innerHTML+= '<table width="100%" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td class="listwhitetext" valign="top" width="57px">'+st1+'</td><td class="listwhitetext" valign="top">'+(Math.round(st2*exchangeRate*100)/100).toFixed(2)+ ' '+st3+'</td>'  ;
						
					 }
					}
					
					}
					
					
					if(markUp==false && DTHC==false){
					
					if(jsonData.partners[i].basisWithoutMarkUp=='Flat'){
	           			quoteBasisValue='Flat'
	           		}else{
	           			quoteBasisValue=(Math.round(jsonData.partners[i].basisWithoutMarkUp*exchangeRate*100)/100).toFixed(2)+' cwt';
	           		}
					
					if(jsonData.partners[i].destinationSelection=="true")
					{
					
						//for(var k =0; k<= chargeArray.length-2; k++)
					//{
					//	var str=chargeArray[k];
					 //   var st1 = str.substring('0',str.indexOf(":")+1);
					 //   var st22 = str.substring(str.indexOf(":")+1,str.length)
					 //   var st2 = st22.substring('0',st22.indexOf(" "));
					 //   var st33 = st22.substring(st22.indexOf(" "),st22.length);
					 //   var st3 = st33.substring('0',st33.length);
						
					//	if(st1=='THC:')
					//	{
					//		THCValue=st2;
						//}
					//}
					totalQuoteWithoutTHC=((Math.round(jsonData.partners[i].quoteWithoutTHC*exchangeRate*100)/100) - THCValue).toFixed(2);
					if(document.forms['agentPricing'].elements['pricingControl.sequenceNumber'].value!=''){
					d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+totalQuoteWithoutTHC+'</td>' +
					'<td class="listwhitetext" valign="top">'+
					'<input style="margin-top:-2px;" type="radio" name="rgroup1" disabled="true" id="radio" checked="checked" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />' +
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td></table>';
					
					}else{
					d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+totalQuoteWithoutTHC+'</td>' +
					'<td class="listwhitetext" valign="top">'+
					'<input style="margin-top:-2px;" type="radio" name="rgroup1" id="radio" checked="checked" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />' +
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td></table>';
					
					}
					
					for(var j =0; j<= chargeArray.length-2; j++)
					{
						var str=chargeArray[j];
					    var st1 = str.substring('0',str.indexOf(":")+1);
					    var st22 = str.substring(str.indexOf(":")+1,str.length)
					    var st2 = st22.substring('0',st22.indexOf(" "));
					    var st33 = st22.substring(st22.indexOf(" "),st22.length);
					    var st3 = st33.substring('0',st33.length);

						
						if(st1=='Customs Clearance:')
						{
							st1='Cus.Clear:';
						}
						if(st1!='THC:')
						{
						d.innerHTML+= '<table width="100%" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td class="listwhitetext" valign="top" width="57px">'+st1+'</td><td class="listwhitetext" valign="top">'+(Math.round(st2*exchangeRate*100)/100).toFixed(2)+ ' '+st3+'</td>'  ;
						}
					}
					}
					else
					{
					
						//for(var k =0; k<= chargeArray.length-2; k++)
					//{
					//	var str=chargeArray[k];
					  //  var st1 = str.substring('0',str.indexOf(":")+1);
					 //   var st22 = str.substring(str.indexOf(":")+1,str.length)
					 //   var st2 = st22.substring('0',st22.indexOf(" "));
					 //   var st33 = st22.substring(st22.indexOf(" "),st22.length);
					  //  var st3 = st33.substring('0',st33.length);
						
					//	if(st1=='THC:')
						//{
						//	THCValue=st2;
						//}
				//	}
					totalQuoteWithoutTHC=((Math.round(jsonData.partners[i].quoteWithoutTHC*exchangeRate*100)/100) - THCValue).toFixed(2);
					if(document.forms['agentPricing'].elements['pricingControl.sequenceNumber'].value!=''){
					d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+totalQuoteWithoutTHC+'</td>' +
					'<td class="listwhitetext" valign="top">'+
					'<input style="margin-top:-2px;" type="radio" name="rgroup1" disabled="true" id="radio" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />' + 
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td> ' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td></table>';
					
					}else{
					d.innerHTML+='<table width="174" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;">'+letter+'</td><td><a style="text-decoration: underline; cursor: pointer;" onclick="openPartnerProfile('+jsonData.partners[i].id+')">'+jsonData.partners[i].name+'</a></td></tr></table></td>'+
					'</tr><tr><td class="listwhitetext"><table border="0"  style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td valign="top" class="listwhitetext" >Feedback: </td><td valign="top" class="listwhitetext" width="80px" >'+jsonData.partners[i].feedbackRating+'%</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Activity: </td><td class="listwhitetext">'+jsonData.partners[i].shipmentActivity+'</td></tr>'+
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Quote: </td><td class="listwhitetext" valign="top">'+jsonData.partners[i].currency+' '+totalQuoteWithoutTHC+'</td>' +
					'<td class="listwhitetext" valign="top">'+
					'<input style="margin-top:-2px;" type="radio" name="rgroup1" id="radio" onClick="selectedValue('+jsonData.partners[i].detailID+','+jsonData.partners[i].pricingControlID+')" />' + 
					'</td></tr>' +
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Details:</td>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Base Rate: </td><td class="listwhitetext" valign="top">'+(Math.round(weightDetailRound*exchangeRate*100)/100).toFixed(2)+'</td> ' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Basis: </td><td class="listwhitetext" valign="top">'+quoteBasisValue+'</td>'+
					'<tr><td class="vertlinedata" align="center" colspan="4"/></tr>' +
					'<tr><td valign="top"></td><td class="listwhitetext" valign="top">Hauling: </td><td class="listwhitetext" valign="top">'+(Math.round(haulingRound*exchangeRate*100)/100).toFixed(2)+' '+jsonData.partners[i].haulingBasis+'</td></table>';
					
					}
					
					
					for(var j =0; j<= chargeArray.length-2; j++)
					{
						var str=chargeArray[j];
					    var st1 = str.substring('0',str.indexOf(":")+1);
					    var st22 = str.substring(str.indexOf(":")+1,str.length)
					    var st2 = st22.substring('0',st22.indexOf(" "));
					    var st33 = st22.substring(st22.indexOf(" "),st22.length);
					    var st3 = st33.substring('0',st33.length);

						
						if(st1=='Customs Clearance:')
						{
							st1='Cus.Clear:';
						}
						// Customs Clearnace:100 Flat  
						// THC:20 Included
						if(st1!='THC:')
						{
						d.innerHTML+= '<table width="100%" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;"><tr><td valign="top" width="8"></td><td class="listwhitetext" valign="top" width="57px">'+st1+'</td><td class="listwhitetext" valign="top">'+(Math.round(st2*exchangeRate*100)/100).toFixed(2)+ ' '+st3+'</td>'  ;
						}
					}					 
					}
					
					}				
	           		
	           		console.log("lat: " + jsonData.partners[i].latitude + ", long: " +  jsonData.partners[i].longitude);
	        	   point = new GLatLng(jsonData.partners[i].latitude, jsonData.partners[i].longitude);
	        	   // addCoordsToMap(point, jsonData.partners[i]);
	        	   addPartnersCoordsToMap(point, jsonData.partners[i], i);
	        	   var delme = 0;
	        	   }
	        	}
	       }	       
	   });
   }
   
   
   function addPartnersCoordsToMap(point, partnerDetails, recordNumber){
    			//marker = new GMarker(point);
    			var baseIcon = new GIcon(G_DEFAULT_ICON);
				baseIcon.shadow = "http://www.google.com/mapfiles/shadow50.png";
				baseIcon.iconSize = new GSize(20, 34);
				baseIcon.shadowSize = new GSize(37, 34);
				baseIcon.iconAnchor = new GPoint(9, 34);
				baseIcon.infoWindowAnchor = new GPoint(9, 2)

    			var letter = String.fromCharCode("A".charCodeAt(0) + recordNumber);
			  var letteredIcon = new GIcon(baseIcon);
			  recordNumber=recordNumber*1+1;
			  letteredIcon.image = "${pageContext.request.contextPath}/images/markerImage/marker"+recordNumber+".png";
			
			  // Set up our GMarkerOptions object
			  markerOptions = { icon:letteredIcon };
			  marker = new GMarker(point, markerOptions);
			    	
	     // Create a marker
	      
			 
		   map.addOverlay(marker);
	      // Add the marker to map
	      // map.addOverlay(marker);
	 	  map.setCenter(point, 8);
		  map.setUIToDefault();
		   map.disableScrollWheelZoom();
	      // Add address information to marker
	      //marker.openInfoWindowHtml(place.address);
	    var fn = markerClickFn(point, partnerDetails);
	      GEvent.addListener(marker, "click", fn);
	}
   
   function reinitMarkers(){

	   	  map.clearOverlays();
	   	  
	      // Retrieve the base latitude and longitude
	     addToMapDestination();
	     getPortsPoint();

   }
   
   
   function addCoordsToMap1(point, partnerDetails){

	    //alert(point);

	      // Create a marker
	      marker = new GMarker(point);

	      // Add the marker to map
	      map.addOverlay(marker);
	 	  map.setCenter(point, 8);
		  map.setUIToDefault();
	      // Add address information to marker
	      //marker.openInfoWindowHtml(place.address);
	   
	}
   
   function addCoordsToMap(point, partnerDetails){
			 // alert(point);
	      // Create a marker
	      marker = new GMarker(point);

	      // Add the marker to map
	      map.addOverlay(marker);
	 	  map.setCenter(point, 8);
		  map.setUIToDefault();
	      // Add address information to marker
	      //marker.openInfoWindowHtml(place.address);
	     var fn = markerClickFn(point, partnerDetails);
	      GEvent.addListener(marker, "click", fn);
	}
   
    function markerClickFn(point, partnerDetails) {
	   return function() {
		   if (!partnerDetails) return;
	       var title = partnerDetails.name;
	       var url = partnerDetails.url;
	       var fileurl = partnerDetails.locationPhotoUrl;
	       var exchageRate=partnerDetails.exchangeRate;
	        if(document.forms['agentPricing'].elements['markUp'].checked==true)
	       {
	       
	       	var rate=(Math.round(partnerDetails.rateMarkUp*exchageRate*100)/100).toFixed(2);
	       }
	       if(document.forms['agentPricing'].elements['markUp'].checked==false)
	       {
	       	var rate=(Math.round(partnerDetails.rate*exchageRate*100)/100).toFixed(2);
	       }
	       
	       var infoHtml = '<div style="width:210px;"><h3>' + title
	         + '</h3>'
	         + '<h5>'+partnerDetails.address1+'</h6>'
	         + '<h5>'+partnerDetails.address2+'</h6>'
	         + '<h4 style="padding-left:120px">Quote: $'+rate+'</h4>';
	       
	       infoHtml += '</div></div>';
	       map.openInfoWindowHtml(point, infoHtml);

	   }
   }
   
     
   function adjustImage(img, maxwidth, maxheight) {
	     var wid = img.width;
	     var hei = img.height;
	     var newwid = wid;
	     var newhei = hei;
	     if(wid / maxwidth > hei / maxheight){
	       if(wid > maxwidth){
	         newwid = maxwidth;
	         newhei = parseInt(hei * newwid / wid);
	       }
	     } else {
	       if(hei > maxheight) {
	         newhei = maxheight;
	         newwid = parseInt(wid * newhei / hei);
	       }
	     }
	     var src = img.src;
	     img = document.createElement("img");
	     img.src = src;
	     img.width = newwid;
	     img.height = newhei;
	     return img;
	   }
</script>

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('address', 'fade=1,persist=0,hide=0')
animatedcollapse.addDiv('info', 'fade=1,persist=0,hide=1')
animatedcollapse.addDiv('pricing', 'fade=1,persist=0,hide=1')



animatedcollapse.init()
</script>
<script language="JavaScript" type="text/JavaScript">
var namesVec = new Array("130.png", "129.png");
var root='images/';
function swapImg(ima){
// divides the path
nr = ima.getAttribute('src').split('/');
// gets the last part of path, ie name
nr = nr[nr.length-1]
// former was .split('.')[0];
 
if(nr==namesVec[0]){ima.setAttribute('src',root+namesVec[1]);}
else{ima.setAttribute('src',root+namesVec[0]);}
 
}

function checkOriginScope(){
var destinationScope=document.forms['agentPricing'].elements['pricingControl.isOrigin'].value;
if(destinationScope!='true')
{
	alert('Origin information not provided.');
	return false;
}

}
function checkFreightScope(){
var freightScope=document.forms['agentPricing'].elements['pricingControl.isFreight'].value;
if(freightScope!='true')
{
	alert('Origin and/or Destination Ports information not provided.');
	return false;
}

}

function updateDTHCFlag(){
	var DTHCFlag=document.forms['agentPricing'].elements['DTHC'].checked;
	var id= '${pricingControl.id}';
	var url="updateDTHCFlag.html?ajax=1&decorator=simple&popup=true&tarrifApplicability=Destination&dthcFlag=" +DTHCFlag+'&pricingControlID='+encodeURI(id);
	http2.open("GET", url, true);
  	//http2.onreadystatechange = handleHttpResponseFieldList;
   	http2.send(null);


}
function checkControlFlag(){
	var id= '${pricingControl.id}';
	var sequenceNumber='${pricingControl.sequenceNumber}';
	var url="checkControlFlag.html?ajax=1&decorator=simple&popup=true&sequenceNumber=" +sequenceNumber;
	http2.open("GET", url, true);
  	http2.onreadystatechange = handleHttpResponseCheckControlFlag;
   	http2.send(null);
	
}

function handleHttpResponseCheckControlFlag(){
	if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results=='C'){
                	var sid='${pricingControl.serviceOrderId}';
                	location.href='accountLineList.html?sid='+sid;
                	return true;
                }else{
                	alert('Quote is still not accepted');
                	return false;
                }
             }
}

function refreshWindow(){
var id= '${pricingControl.id}';
	location.href="agentPricingOriginInfo.html?id="+id;
}
</script>
<%-- Shifting Closed Here --%>


<script type="text/javascript">
try{
initLoader();	
}catch(e){}
</script>
