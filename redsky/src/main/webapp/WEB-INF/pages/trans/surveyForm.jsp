<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<head>

<title><fmt:message key="customerFileDetail.title" /></title>
<meta name="heading"
	content="<fmt:message key='customerFileDetail.heading'/>" />

<style type="text/css">
		h2 {background-color: #FBBFFF}

input.newmnavbutton {
background:transparent url(../images/tabn1.gif) no-repeat scroll 0%;
color:#FFF;
cursor:pointer;
float:left;
padding:2pt 0pt 0pt 9px;
text-decoration:none;
}	


</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<script language="JavaScript" type="text/javascript">

function focusDate(target)
{
	document.forms['customerFileForm'].elements[target].focus();
}
		function setTimeMask() {
		//alert();
		oTimeMask1 = new Mask("##:##", "surveyTime");
		
		oTimeMask1.attach(document.forms['customerFileForm'].surveyTime);
		//alert("gg");
		oTimeMask2 = new Mask("##:##", "surveyTime2");
		oTimeMask2.attach(document.forms['customerFileForm'].surveyTime2);
		}
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

	<script language="JavaScript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>
	

		
<script language="javascript" type="text/javascript">

function myDate() {
	var HH2;
	var HH1;
	var MM2;
	var MM1;
	var tim1=document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
	var tim2=document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
	//if(tim1.length == 5 || tim2.length == 5 ) {
		tim1=tim1.replace(":","");
		tim2=tim2.replace(":","");
	//}
	if((tim1.substring(0,tim1.length-2)).length == 1){
		HH1 = '0'+tim1.substring(0,tim1.length-2);
	}else {
		HH1 = tim1.substring(0,tim1.length-2);
	}
	MM1 = tim1.substring(tim1.length-2,tim1.length);
	if((tim2.substring(0,tim1.length-2)).length == 1){
		HH2 = '0'+tim2.substring(0,tim2.length-2);
	}else {
		HH2 = tim2.substring(0,tim2.length-2);
	}
	MM2 = tim2.substring(tim2.length-2,tim2.length);
	document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = HH1 + ':' + MM1;
	document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = HH2 + ':' + MM2;

	//if (!document.getElementById) return false; 
	var f = document.getElementById('customerFileForm'); 
	//var u = f.elements[0]; 
	f.setAttribute("autocomplete", "off"); 
	//u.focus(); 
	targetElementOrigin = document.forms['customerFileForm'].elements['customerFile.originCountry'];
	targetElementDestin = document.forms['customerFileForm'].elements['customerFile.destinationCountry'];
	
	document.forms['customerFileForm'].elements['oCountry'].value = document.forms['customerFileForm'].elements['customerFile.originCountry'].options[targetElementOrigin.selectedIndex].text;
	document.forms['customerFileForm'].elements['dCountry'].value = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].options[targetElementDestin.selectedIndex].text;
	if(document.forms['customerFileForm'].elements['customerFile.originCountry'].value == 'USA' || document.forms['customerFileForm'].elements['customerFile.originCountry'].value == 'CAN' || document.forms['customerFileForm'].elements['customerFile.originCountry'].value == 'IND' ){
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled = false;
	}else{
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
	}
	if(document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value == 'USA' || document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value == 'CAN' || document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value == 'IND' ){
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = false;
	}else{
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
	}
}
</script>
<script type="text/javascript">
function validate_email(field)
{
with (field)
{
apos=value.indexOf("@")
dotpos=value.lastIndexOf(".")
if (apos<1||dotpos-apos<2) 
  {alert("Not a valid e-mail address!");
  field.value = "";
  return false}
else {return true}
}
}
</script>


<script language="JavaScript">
		
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
	
	function copyCompanyToDestination(targetElement){
		document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value = targetElement.value;
	}
</script>

<script type="text/javascript">

// Declaring required variables
var digits = "0123456789";
// non-digit characters which are allowed in phone numbers
var phoneNumberDelimiters = "()- ";
// characters which are allowed in international phone numbers
// (a leading + is OK)
var validWorldPhoneChars = phoneNumberDelimiters + "+";
// Minimum no of digits in an international phone no.
var minDigitsInIPhoneNumber = 10;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function checkInternationalPhone(strPhone){
var s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}

function validatePhone(targetElelemnt){
	var Phone=targetElelemnt.value;
	
	if (checkInternationalPhone(Phone)==false){
		alert("Please enter a valid phone number")
		targetElelemnt.value="";
		return false;
	}
	return true;
 }
</script>

<script type="text/javascript">
	function autoPopulate_customerFile_originCountry(targetElement) {
		//var originCountryCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value=targetElement.options[targetElement.selectedIndex].value;
		document.forms['customerFileForm'].elements['oCountry'].value=targetElement.options[targetElement.selectedIndex].text;
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled = false;
		}else{
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.originState'].value = '';
			autoPopulate_customerFile_originCityCode(targetElement,'special');
		}
		//targetElement.form.elements['customerFile.originCountry'].value=originCountryCode.substring(originCountryCode.indexOf(":")+2,originCountryCode.length);
	}
</script>
<script type="text/javascript">
	function autoPopulate_customerFile_destinationCountry(targetElement) {
		//var destinationCountryCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value=targetElement.options[targetElement.selectedIndex].value;
		document.forms['customerFileForm'].elements['dCountry'].value=targetElement.options[targetElement.selectedIndex].text;
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = false;
		}else{
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.destinationState'].value = '';
		}
		autoPopulate_customerFile_destinationCityCode(targetElement,'special');
		//targetElement.form.elements['customerFile.destinationCountry'].value=destinationCountryCode.substring(destinationCountryCode.indexOf(":")+2,destinationCountryCode.length);
	}
</script>
<script type="text/javascript">
	function autoPopulate_customerFile_destinationCityCode(targetElement, w) {
	var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};

 		targetElement.value = targetElement.value.replace(r[w],'');
	
		if(document.forms['customerFileForm'].elements['customerFile.destinationCity'].value != ''){
			if(document.forms['customerFileForm'].elements['customerFile.destinationState'].value == ''){
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
			}else{
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value+','+document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
			}
		}
		//var sourceCode=targetElement.options[targetElement.selectedIndex].value;
	    //document.forms['customerFileForm'].elements['customerFile.sourceCode'].value=sourceCode.substring(0,sourceCode.indexOf(":")-1);
		//targetElement.form.elements['customerFile.source'].value=sourceCode.substring(sourceCode.indexOf(":")+2,sourceCode.length);
	}
</script>
<script type="text/javascript">
	function autoPopulate_customerFile_originCityCode(targetElement, w) {
		var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};

 		targetElement.value = targetElement.value.replace(r[w],'');
		if(document.forms['customerFileForm'].elements['customerFile.originCity'].value != ''){
			if(document.forms['customerFileForm'].elements['customerFile.originState'].value != ''){
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value+','+document.forms['customerFileForm'].elements['customerFile.originState'].value;
			}else{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value;
			}
		}
		//var sourceCode=targetElement.options[targetElement.selectedIndex].value;
	    //document.forms['customerFileForm'].elements['customerFile.sourceCode'].value=sourceCode.substring(0,sourceCode.indexOf(":")-1);
		//targetElement.form.elements['customerFile.source'].value=sourceCode.substring(sourceCode.indexOf(":")+2,sourceCode.length);
	}
</script>
<script type="text/javascript">
	function autoPopulate_customerFile_statusDate(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['customerFile.statusDate'].value=datam;
		
	}
	function autoPopulate_customerFile_approvedDate(targetElement, w) {
		
		var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};

 		targetElement.value = targetElement.value.replace(r[w],'');
		
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['customerFile.billApprovedDate'].value=datam;
		
	}	
</script>

<script type="text/javascript">
	function dateValidation(targetElement) {
	alert(targetElement.value);
		var openDate = document.forms['customerFileForm'].elements['customerFile.createdOn'].value;
		var survey = targetElement.value;
		var openDateArray = openDate.split("/");
		var surveyArray = survey.split("/");
		var openDate_Date = openDateArray[1];
		var openDate_Month = openDateArray[0];
		var openDate_Year = openDateArray[2];
		var survey_Date = surveyArray[1];
		var survey_Month = surveyArray[0];
		var survey_Year = surveyArray[2];
		
		alert(targetElement.value);
	} 
</script>
<script type="text/javascript">
	function openOriginLocation() {
		var city = document.forms['customerFileForm'].elements['customerFile.originCity'].value;
		var country = document.forms['customerFileForm'].elements['oCountry'].value;
		var zip = document.forms['customerFileForm'].elements['customerFile.originZip'].value;
		var address = document.forms['customerFileForm'].elements['customerFile.originAddress1'].value;
		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+country);
}
	function openDestinationLocation() {
		var city = document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
		var country = document.forms['customerFileForm'].elements['dCountry'].value;
		var zip = document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
		var address = document.forms['customerFileForm'].elements['customerFile.destinationAddress1'].value;
		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+country);
}
</script>
<script type="text/javascript">
	function generatePortalId() {
		//document.forms['customerFileForm'].elements['customerFile.customerPortalId'].value = document.forms['customerFileForm'].elements['customerFile.corpID'].value + document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
		document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked = true ;
		var daReferrer = document.referrer; 
		var email = document.forms['customerFileForm'].elements['customerFile.email'].value + ',' +document.forms['customerFileForm'].elements['customerFile.email2'].value; 
		var errorMsg = "here here here is the error error error error"; 
		var subject = ""; 
		var name = ""; 
		var formNotes = ""; 
		var body_message = "%0D%0DHi "+name+"%0D%0D "+formNotes; 
		
		var mailto_link = 'mailto:'+email+'?subject='+subject+'&body='+body_message; 
		
		win = window.open(mailto_link,'emailWindow'); 
		if (win && win.open &&!win.closed) win.close(); 
	} 
</script>

<SCRIPT LANGUAGE="JavaScript">
	function completeTimeString() {
	
		stime1 = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
		stime2 = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;;
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = "0" + stime1;
			}
		}
		if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
			if(stime2.length==1 || stime2.length==2){
				if(stime2.length==2){
					document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = stime2 + ":00";
				}
				if(stime2.length==1){
					document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = "0" + stime2 + ":00";
				}
			}else{
				document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = stime2 + "00";
			}
		}else{
			if(stime2.indexOf(":") == -1 && stime2.length==3){
				document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
			}
			if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
				document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
			}
			if(stime2.indexOf(":") == 1){
				document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = "0" + stime2;
			}
		}
	}

</SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
function IsValidTime(clickType) {

var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("Time is not in a valid format. Please use HH:MM format");
//document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("'Pre Move Survey' time must between 0 to 23 (Hrs)");
//document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}
if (minute<0 || minute > 59) {
alert ("'Pre Move Survey' time must between 0 to 59 (Min)");
//document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Pre Move Survey' time must between 0 to 59 (Sec)");
//document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}

// **************Check for Survey Time2*************************

var time2Str = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
var matchTime2Array = time2Str.match(timePat);
if (matchTime2Array == null) {
alert("Time is not in a valid format. please Use HH:MM format");
//document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}
hourTime2 = matchTime2Array[1];
minuteTime2 = matchTime2Array[2];
secondTime2 = matchTime2Array[4];
ampmTime2 = matchTime2Array[6];

if (hourTime2 < 0  || hourTime2 > 23) {
alert("'Pre Move Survey' time must between 0 to 23 (Hrs)");
//document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'Pre Move Survey' time must between 0 to 59 (Min)");
//document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'Pre Move Survey' time must between 0 to 59 (Sec)");
//document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = '00:00';
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}
if(!(clickType == 'save')){
	var id1 = document.forms['customerFileForm'].elements['customerFile.id'].value;
	var billCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	var shipNumber = document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
	
	if (document.forms['customerFileForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Do you want to save the customer file and continue? press OK to continue with save OR press CANCEL");
		if(agree){
			document.forms['customerFileForm'].action = 'saveCustomerFile!saveOnTabChange.html';
			document.forms['customerFileForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				location.href = 'customerServiceOrders.html?id='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.raterequest'){
				location.href = 'customerRateOrders.html?id='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.surveys'){
				location.href = 'surveysList.html?id1='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.accountpolicy'){
				location.href = 'showAccountPolicy.html?id='+id1+'&code='+billCode;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.docs'){
				//location.href = 'myFiles.html?id='+id1+'&myFileFor=CF';
				openWindow('myFiles.html?id='+id1+'&myFileFor=CF&decorator=popup&popup=true',740,400);
				}
		}
		}
	}else{
	if(id1 != ''){
		if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				location.href = 'customerServiceOrders.html?id='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.raterequest'){
				location.href = 'customerRateOrders.html?id='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.surveys'){
				location.href = 'surveysList.html?id1='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.accountpolicy'){
				location.href = 'showAccountPolicy.html?id='+id1+'&code='+billCode;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.docs'){
				openWindow('myFiles.html?id='+id1+'&myFileFor=CF&decorator=popup&popup=true',740,400);
				}
	}
	}
}
}
function checkDate(){
		var str =  document.forms['customerFileForm'].elements['customerFile.survey'].value;
		var str_array = str.split("/");
		surveydate = new Date(str_array[2],str_array[0]*1-1,str_array[1]);
		var str1 =  document.forms['customerFileForm'].elements['customerFile.moveDate'].value;
		var str1_array = str1.split("/");
		movedate = new Date(str1_array[2],str1_array[0]*1-1,str1_array[1]);
		var str2 =  document.forms['customerFileForm'].elements['customerFile.createdOn'].value;
		var str2_array = str2.split("/");
		var stre = str2_array[2].split(" ");
		createdate = new Date("20"+stre[0],str2_array[0]*1-1,str2_array[1]);
		if(!(str == '')	|| !(str1 == '')){
		if(!(str == '') && surveydate < createdate){
			agree = confirm("Survey date is less than create date, do you want to continue ?");
			return agree;
		}
		if(!(str1 == '') && movedate < createdate){
			agree = confirm("Move date is less than create date, do you want to continue ?");
			return agree;
		}
		}
		return true;
	
}
function checkTime()
{
	var tim1=document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
	var tim2=document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
	time1=tim1.replace(":","");
	time2=tim2.replace(":","");
	if(time1 > time2){
		alert("'Pre Move Survey' To time cannot be less than from time");
		//document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = '00:00';
		document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
		return false;
	}else{
		if(document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked == false){
			/*
			var sendmail = confirm("Do you want to activate the customer ? press OK to activate or CANCEL to proceed without activation");
			if(sendmail){
				//alert("yes");
				document.forms['customerFileForm'].elements['sendEmail'].value="yes";
				generatePortalId();
			}else{
				//alert("No");			
				document.forms['customerFileForm'].elements['sendEmail'].value="no";
			}
			*/
			document.forms['customerFileForm'].elements['submitCnt'].value = 0;
			document.forms['customerFileForm'].elements['submitType'].value = '';
		
			document.forms['customerFileForm'].submit();
		}
	}
	
}
function checkBillToCode(){
		//var x = document.getElementById("contactName");
		//x.style.visibility = 'hidden';
		if(document.forms['customerFileForm'].elements['customerFile.billToCode'].value == '500270'){
			document.forms['customerFileForm'].elements['customerFile.contactName'].disabled = false ;
			document.forms['customerFileForm'].elements['customerFile.contactPhone'].disabled = false ;
			document.forms['customerFileForm'].elements['customerFile.contactFax'].disabled = false ;
			document.forms['customerFileForm'].elements['customerFile.contactEmail'].disabled = false ;
			document.forms['customerFileForm'].elements['customerFile.organization'].disabled = false ;
			document.forms['customerFileForm'].elements['customerFile.destinationContactName'].disabled = false ;
			document.forms['customerFileForm'].elements['customerFile.destinationContactPhone'].disabled = false ;
			document.forms['customerFileForm'].elements['customerFile.destinationContactFax'].disabled = false ;
			document.forms['customerFileForm'].elements['customerFile.destinationContactEmail'].disabled = false ;
			document.forms['customerFileForm'].elements['customerFile.destinationOrganization'].disabled = false ;
		}else{
			document.forms['customerFileForm'].elements['customerFile.contactName'].disabled = true ;
			document.forms['customerFileForm'].elements['customerFile.contactPhone'].disabled = true ;
			document.forms['customerFileForm'].elements['customerFile.contactFax'].disabled = true ;
			document.forms['customerFileForm'].elements['customerFile.contactEmail'].disabled = true ;
			document.forms['customerFileForm'].elements['customerFile.organization'].disabled = true ;
			document.forms['customerFileForm'].elements['customerFile.destinationContactName'].disabled = true ;
			document.forms['customerFileForm'].elements['customerFile.destinationContactPhone'].disabled = true ;
			document.forms['customerFileForm'].elements['customerFile.destinationContactFax'].disabled = true ;
			document.forms['customerFileForm'].elements['customerFile.destinationContactEmail'].disabled = true ;
			document.forms['customerFileForm'].elements['customerFile.destinationOrganization'].disabled = true ;
		}
}
function notExists(){
	alert("The customer information has not been saved yet, please save customer information to continue");
}

function resetAfterSubmit(){
	var goCnt = '-'+document.forms['customerFileForm'].elements['submitCnt'].value * 1;
	history.go(goCnt);
}

function openPopWindow(){
var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
javascript:openWindow('partnersPopup.html?partnerType=PP&firstName='+first+'&lastName='+last+'&decorator=popup&popup=true&fld_sixthDescription=customerFile.customerEmployer&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.billToName&fld_code=customerFile.billToCode');
}
function openBookingAgentPopWindow(){
var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
javascript:openWindow('bookingAgentPopup.html?partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.bookingAgentName&fld_code=customerFile.bookingAgentCode');
}
</script>

<SCRIPT LANGUAGE="JavaScript">


function findBillToName()
{
    var billToCode = 
document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    //alert(vendorId);
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + 
encodeURI(billToCode);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
}
function getContract(targetElement) {
	var custJobType = targetElement.value;
	var url="findContractbyJob.html?ajax=1&decorator=simple&popup=true&custJobType=" + encodeURI(custJobType);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse3;
     http2.send(null);
	
	}

function findBookingAgentName(){
    var bookingAgentCode = document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(bookingAgentCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4;
     http2.send(null);
}

function getState(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse5;
     http2.send(null);
	
	}
	
function getDestinationState(targetElement) {

	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse6;
     http3.send(null);
	
	}

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

function handleHttpResponse2()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					document.forms['customerFileForm'].elements['customerFile.billToName'].value = results;
 					document.forms['customerFileForm'].elements['customerFile.customerEmployer'].value = results;
 					
                 }
                 else
                 {
                     alert("Invalid Bill To code, please select another");
                     document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
					 document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
					 document.forms['customerFileForm'].elements['customerFile.billToCode'].select();
                 }
             }
        }
function handleHttpResponse3()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.contract'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value = '';
					}else{
					contractVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text = contractVal[1];
					document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value = contractVal[0];
					}
					}
             }
        }
function handleHttpResponse4()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].value = results;
 				}
                 else
                 {
                     alert("Invalid Booking Agent code, please select another");
                     document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value="";
					 document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].value="";
					 document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].select();
                 }
             }
        }   
 function handleHttpResponse5()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("originState").value = '${customerFile.originState}';
             }
        }  
        
function handleHttpResponse6()
        {

             if (http3.readyState == 4)
             {
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("destinationState").value = '${customerFile.destinationState}';
             }
        }  
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();

function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject1();
    
</script>

<script>
function changeStatus(){
	document.forms['customerFileForm'].elements['formStatus'].value = '1';
}
</script>

<style type="text/css">


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;}

</style>

</head>

<s:form id="customerFileForm" name="customerFileForm" action="saveCustomerFile" method="post" validate="true" >

		<s:hidden name="customerFile.id" value="%{customerFile.id}" />
		<s:hidden name="custJobType"/>
		<s:hidden name="custStatus"/>
		<s:hidden name="customerFile.corpID" />
		<s:hidden name="id" value="<%=request.getParameter("id") %>" />

		<s:hidden name="gotoPageString" id="gotoPageString" value="" />
	
		<s:hidden id="countNotes" name="countNotes" value="<%=request.getParameter("countNotes") %>"/>
		<s:hidden id="countOriginNotes" name="countOriginNotes" value="<%=request.getParameter("countOriginNotes") %>"/>
		<s:hidden id="countDestinationNotes" name="countDestinationNotes" value="<%=request.getParameter("countDestinationNotes") %>"/>
		<s:hidden id="countSurveyNotes" name="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>"/>
		<s:hidden id="countVipNotes" name="countVipNotes" value="<%=request.getParameter("countVipNotes") %>"/>
		<s:hidden id="countBillingNotes" name="countBillingNotes" value="<%=request.getParameter("countBillingNotes") %>"/>
		<s:hidden id="countSpouseNotes" name="countSpouseNotes" value="<%=request.getParameter("countSpouseNotes") %>"/>
		<s:hidden id="countContactNotes" name="countContactNotes" value="<%=request.getParameter("countContactNotes") %>"/>
		<s:hidden id="countCportalNotes" name="countCportalNotes" value="<%=request.getParameter("countCportalNotes") %>"/>
		<s:hidden id="countEntitlementNotes" name="countEntitlementNotes" value="<%=request.getParameter("countEntitlementNotes") %>"/>

		<c:set var="countNotes" value="<%=request.getParameter("countNotes") %>" />
		<c:set var="countOriginNotes" value="<%=request.getParameter("countOriginNotes") %>" />
		<c:set var="countDestinationNotes" value="<%=request.getParameter("countDestinationNotes") %>" />
		<c:set var="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>" />
		<c:set var="countVipNotes" value="<%=request.getParameter("countVipNotes") %>" />
		<c:set var="countBillingNotes" value="<%=request.getParameter("countBillingNotes") %>"/>
		<c:set var="countSpouseNotes" value="<%=request.getParameter("countSpouseNotes") %>"/>
		<c:set var="countContactNotes" value="<%=request.getParameter("countContactNotes") %>"/>
		<c:set var="countCportalNotes" value="<%=request.getParameter("countCportalNotes") %>"/>
		<c:set var="countEntitlementNotes" value="<%=request.getParameter("countEntitlementNotes") %>"/>
		<s:hidden name="dCountry" />
		<s:hidden name="oCountry" />
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    <s:hidden name="formStatus" value=""/>
<div id="Layer1" onkeydown="changeStatus();" >		
	<%-- <c:if test="${empty customerFile.id}">		  --%>
		<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a href="customerFiles.html" class="current"><span>Customer File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <!--<li><s:submit cssClass="newmnavbutton" method="saveOnTabChange"  value="ServiceOrders" onmouseover="completeTimeString();" onclick="return setReturnString('gototab.serviceorder');return IsValidTime();"/></li>
		    --><li><a onmouseover="completeTimeString();" onclick="setReturnString('gototab.serviceorder');return IsValidTime('none');"><span>Service Order</span></a></li>
		    <li><a onmouseover="completeTimeString();" onclick="setReturnString('gototab.raterequest');return IsValidTime();"><span>Rate Request</span></a></li>
		    <li><a onmouseover="completeTimeString();" onclick="setReturnString('gototab.surveys');return IsValidTime();"><span>Surveys</span></a></li>
		    <li><a onmouseover="completeTimeString();" onclick="setReturnString('gototab.accountpolicy');return IsValidTime();"><span>Account Policy</span></a></li>
		    <li><a onclick="openWindow('subModuleReports.html?id='+id1+'&&jobNumber='+shipNumber+'&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=customerFile&reportSubModule=customerFile&decorator=popup&popup=true',750,400)""><span>Forms</span></a></li>
		    <li><a><span>Docs</span></a></li>
		    <!--<li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
		    <li><a href="surveysList.html?id=${customerFile.id} "><span>Surveys</span></a></li>
		    <li><a href="showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}" ><span>Account Policy</span></a></li> 
		  	<li><a href="sortCustomerReportss.html"><span>Forms</span></a></li>   
			<li><a href="myFiles.html?id=${customerFile.id }&myFileFor=CF" ><span>Docs</span></a></li>-->
		  </ul>
		</div><div class="spn">&nbsp;</div><br>
	<%-- </c:if>	
	<c:if test="${not empty customerFile.id}">
		<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a href="customerFiles.html" class="current"><span>Customer File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a onclick="notExists()"><span>Service Orders</span></a></li>
		    <li><a onclick="notExists()"><span>Rate Request</span></a></li>
		    <li><a onclick="notExists()"><span>Surveys</span></a></li>
		    <li><a onclick="notExists()"><span>Account Policy</span></a></li> 
		  	<li><a onclick="notExists()"><span>Forms</span></a></li>  
			<li><a onclick="notExists()"><span>Docs</span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div><br>
	</c:if>
	--%>


<table class="mainDetailTable" cellspacing="1" cellpadding="0"
		border="0">
		<tbody>
			<tr>
				<td>
				<div class="subcontent-tab"><a class="dsphead" >Customer Details&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></div>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
						<tr></tr>
						<tr></tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:50px"></td>
							<td align="left" class="listwhitetext" valign="bottom">
							<a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.sequenceNumber',this);return false" onmouseout="ajax_hideTooltip()">
							<fmt:message key='customerFile.sequenceNumber'/></a>
							</td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="bottom">
							<a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.status',this);return false" onmouseout="ajax_hideTooltip()">
								<fmt:message key='customerFile.status'/>
							</a>
							</td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.statusDate'/></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:50px">&nbsp;</td>
							<td align="left" class="listwhitetext" valign="top"> <s:textfield cssClass="input-textUpper" key="customerFile.sequenceNumber"
							required="true" cssStyle="width:90px" maxlength="15" readonly="true" onfocus = "myDate();"/> </td>
							<td align="right" class="listwhitetext" style="width:320px"></td>
							<td align="left" class="listwhitetext" valign="top"> <s:select cssClass="list-menu" key="customerFile.status"
							list="%{custStatus}" cssStyle="width:105px" onchange="changeStatus();autoPopulate_customerFile_statusDate(this);" onfocus = "myDate();"/> </td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<c:if test="${not empty customerFile.statusDate}">
							<s:text id="customerFileStatusDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.statusDate" /></s:text>
							<td valign="top"><s:textfield cssClass="input-textUpper" name="customerFile.statusDate" value="%{customerFileStatusDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
							</c:if>
							<c:if test="${empty customerFile.statusDate}">
							<td valign="top"><s:textfield cssClass="input-textUpper"  name="customerFile.statusDate" cssStyle="width:65px" readonly="true" /></td>
							</c:if>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr><td class="listwhitetext" style="width:50px; height:10px;"></td></tr>
						<tr></tr>
						<tr></tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:50px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.prefix'/></td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="bottom">Title</td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.firstName' /></td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.middleInitial'/></td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.lastName'/><font color="red" size="2">*</font></td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext" style="width:67px"><configByCorp:fieldVisibility componentId="customerfile.socialSecurityNumber"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.socialSecurityNumber',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='customerFile.socialSecurityNumber'/></a></configByCorp:fieldVisibility></td>
							<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.vip',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='serviceOrder.vip'/></a></td>
						</tr>
						<tr><c:set var="isVipFlag" value="false"/>
								<c:if test="${customerFile.vip}">
									<c:set var="isVipFlag" value="true"/>
								</c:if>
							<td align="right" class="listwhitetext" style="width:50px">&nbsp;</td>
							<td align="left" class="listwhitetext" valign="top"> <s:select cssClass="list-menu" name="customerFile.prefix" cssStyle="width:47px" list="%{preffix}" headerKey="" headerValue="" onchange="changeStatus();" onfocus = "myDate();"/></td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="customerFile.rank" size="7" maxlength="7" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/> </td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="top" > <s:textfield cssClass="input-text" name="customerFile.firstName" required="true"
							cssStyle="width:115px" maxlength="15" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')" onfocus = "myDate();"/> </td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="top"> <s:textfield cssClass="input-text" key="customerFile.middleInitial"
							required="true" cssStyle="width:13px" maxlength="1" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')" /> </td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="top" > <s:textfield cssClass="input-text" key="customerFile.lastName" required="true"
							size="32" maxlength="80" onkeydown="return onlyCharsAllowed(event, this, 'special')" onfocus = "myDate();"/> </td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext" ><configByCorp:fieldVisibility componentId="customerfile.socialSecurityNumber"><s:textfield cssClass="input-text" key="customerFile.socialSecurityNumber" size="7" maxlength="9" onkeydown="return onlyAlphaNumericAllowed(event, this, 'special')"/></configByCorp:fieldVisibility> </td>
							<td align="left" width="40px" valign="bottom"><s:checkbox key="customerFile.vip" value="${isVipFlag}" fieldValue="true"/></td>
							<c:if test="${empty customerFile.id}">
							<td align="right" style="width:60px;"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty customerFile.id}">
							<c:choose>
								<c:when test="${countVipNotes == '0' || countVipNotes == '' || countVipNotes == null}">
								<td align="right" style="width:60px;"><img id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',740,400);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="right" style="width:60px;"><img id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',740,400);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr><td class="listwhitetext" style="width:50px; height:10px;"></td></tr>
						<tr></tr>
						<tr></tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:50px"></td>
							<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.originCountry',this);return false" onmouseout="ajax_hideTooltip()">Origin</a></td>
							<td align="right" class="listwhitetext" style="width:3px"></td>
							<td align="left" class="listwhitetext" valign="bottom"></td>
							<td align="right" class="listwhitetext" style="width:15px"></td>
							<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.destinationCountry',this);return false" onmouseout="ajax_hideTooltip()">Destination</a></td>
							<td align="right" class="listwhitetext" style="width:3px"></td>
							<td align="left" class="listwhitetext" valign="bottom"></td>
							<td align="right" class="listwhitetext" style="width:15px"></td>
							<td align="left" class="listwhitetext">
							<a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.customerEmployer',this);return false" onmouseout="ajax_hideTooltip()">
							<fmt:message key='customerFile.customerEmployer'/>
							</a>
							</td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:50px">&nbsp;</td>
							<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" name="customerFile.originCityCode" size="20" maxlength="30" readonly="true"/></td>
							<td align="right" class="listwhitetext" style="width:3px"></td>
							<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper"  name="customerFile.originCountryCode" cssStyle="width:37px" maxlength="30" readonly="true"/></td>
							<td align="right" class="listwhitetext" style="width:15px"></td>
							<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" name="customerFile.destinationCityCode" size="20" maxlength="30" readonly="true"/></td>
							<td align="right" class="listwhitetext" style="width:3px"></td>
							<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" name="customerFile.destinationCountryCode" cssStyle="width:37px" maxlength="30" readonly="true"/></td>
							<td align="right" class="listwhitetext" style="width:15px"></td>
							<td align="left" class="listwhitetext" >
							
							<s:textfield cssClass="input-textUpper" key="customerFile.customerEmployer" size="35" readonly="true"/>
						
							 </td>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr><td class="listwhitetext" style="width:50px; height:10px;"></td></tr>
						<tr></tr>
						<tr></tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:50px"></td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.job'/><font color="red" size="2">*</font></td>
							<td align="right" class="listwhitetext" style="width:15px"></td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.coordinator'/></td>
							<td align="right" class="listwhitetext" style="width:15px"></td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.salesMan'/></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:50px">&nbsp;</td>
							<td align="left"><s:select cssClass="list-menu" name="customerFile.job"  list="%{job}" cssStyle="width:250px" headerKey="" headerValue="" onchange="changeStatus();getContract(this);"/></td>
							<td align="right" class="listwhitetext" style="width:15px"></td>
							<td align="left"><s:select cssClass="list-menu" name="customerFile.coordinator" list="%{coord}" cssStyle="width:100px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
							<td align="right" class="listwhitetext" style="width:15px"></td>
							<td align="left"><s:select cssClass="list-menu" name="customerFile.salesMan" list="%{sale}"  cssStyle="width:100px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr><td class="listwhitetext" style="width:50px; height:10px;"></td></tr>
						<tr></tr>
						<tr></tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:50px"></td>
							<td align="left" class="listwhitetext">
							<a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.comptetive',this);return false" onmouseout="ajax_hideTooltip()">
							<fmt:message key='customerFile.comptetive'/>
							</a>
							</td>
							<td align="right" class="listwhitetext" style="width:15px"></td>
							<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.sourceCode',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='customerFile.sourceCode'/></a></td>
							<td align="right" class="listwhitetext" style="width:15px"></td>
							<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.moveDate',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='customerFile.moveDate'/></a></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:50px"></td>
							<td align="left"><s:select cssClass="list-menu" name="customerFile.comptetive" onchange="changeStatus();" list="{'Y','N'}"/></td>
							<td align="right" class="listwhitetext" style="width:15px"></td>
							<td><s:select cssClass="list-menu" name="customerFile.source" list="%{lead}" cssStyle="width:240px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
							<td align="right" class="listwhitetext" style="width:15px"></td>
							<c:if test="${not empty customerFile.moveDate}">
							<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.moveDate"/></s:text>
							<td><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" value="%{customerFileMoveDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/></td><td align="left" width="100px"><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.moveDate'),cal.select(document.forms['customerFileForm'].moveDate,'calender',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<c:if test="${empty customerFile.moveDate}">
							<td><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/></td>
							<td width="100px"><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.moveDate'),cal.select(document.forms['customerFileForm'].moveDate,'calender',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<td align="left" class="listwhitetext" ></td>
							<td align="left" class="listwhitetext" ></td>
							<td align="left" class="listwhitetext" ></td>
							<td align="left" class="listwhitetext" ></td>
							<c:if test="${empty customerFile.id}">
										<td align="right" width="152px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty customerFile.id}">
							<c:choose>
								<c:when test="${countNotes == '0' || countNotes == '' || countNotes == null}">
									<td align="right" class="listwhitetext" width="152px"><img id="countNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Customer&imageId=countNotesImage&fieldId=countNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Customer&imageId=countNotesImage&fieldId=countNotes&decorator=popup&popup=true',740,400);" ></a></td>
								</c:when>
								<c:otherwise>
									<td align="right" class="listwhitetext" width="152px"><img id="countNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Customer&imageId=countNotesImage&fieldId=countNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Customer&imageId=countNotesImage&fieldId=countNotes&decorator=popup&popup=true',740,400);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
						<tr></tr>
						<tr></tr>
						<tr>
							<td align="center" colspan="15"><img height="2px" width="710px" src="<c:url value='/images/vertline.jpg'/>"/></td>
						</tr>
					</tbody>
				</table>
				<table>
					<tbody>
						<tr>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td align="right" class="listwhitetext" style="width:90px"></td>
							<td align="left" class="listwhitetext" >Code<font color="red" size="2">*</font></td>
							<td></td>
							<td align="left" class="listwhitetext" style="width:100px">Name</td>
							<td align="left" class="listwhitetext" style="width:15px"></td>
							<td align="right" class="listwhitetext" style="width:65px"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.bookingDate',this);return false" onmouseout="ajax_hideTooltip()">Booking&nbsp;Date</a></td>
							<td></td>
							<td align="left" class="listwhitetext" ></td>
							<td align="left" class="listwhitetext" style="width:15px"></td>
							<td align="right" class="listwhitetext"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.initialContactDate',this);return false" onmouseout="ajax_hideTooltip()">Initial&nbsp;Contact</a></td>
							<td></td>
							<td align="left" class="listwhitetext"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:100px">Booking Agent:&nbsp;</td>
							<td><s:textfield cssClass="input-text" name="customerFile.bookingAgentCode" size="10" maxlength="8" onchange="changeStatus();findBookingAgentName()"/></td>
							<td><img class="openpopup" width="17" height="20" onclick="openBookingAgentPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
							<td align="left"><s:textfield cssClass="input-text" name="customerFile.bookingAgentName" size="35" maxlength="250"/></td>
							<td align="left" class="listwhitetext" style="width:15px"></td>
							<c:if test="${not empty customerFile.bookingDate}">
								<s:text id="customerFileBookingFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.bookingDate"/></s:text>
								<td><s:textfield id="bookingDate" cssClass="input-text" name="customerFile.bookingDate" value="%{customerFileBookingFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender7" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.bookingDate'),cal.select(document.forms['customerFileForm'].bookingDate,'calender6',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<c:if test="${empty customerFile.bookingDate}">
								<td><s:textfield id="bookingDate" cssClass="input-text" name="customerFile.bookingDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender7" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.bookingDate'),cal.select(document.forms['customerFileForm'].bookingDate,'calender7',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<td align="left" class="listwhitetext" ></td>
							<td align="left" class="listwhitetext" style="width:15px"></td>
							<c:if test="${not empty customerFile.initialContactDate}">
							<s:text id="customerFileInitialFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.initialContactDate"/></s:text>
							<td><s:textfield id="initialContactDate" cssClass="input-text" name="customerFile.initialContactDate" value="%{customerFileInitialFormattedValue}" cssStyle="width:65px" maxlength="11" onfocus="changeStatus();" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="calender6" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.initialContactDate'),cal.select(document.forms['customerFileForm'].initialContactDate,'calender6',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<c:if test="${empty customerFile.initialContactDate}">
							<td><s:textfield id="initialContactDate" cssClass="input-text" name="customerFile.initialContactDate" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/></td><td><img id="calender6" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.initialContactDate'),cal.select(document.forms['customerFileForm'].initialContactDate,'calender6',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<td align="left" class="listwhitetext"></td>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr><td class="listwhitetext" style="width:100px; height:10px;" colspan="10"></td></tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:100px">&nbsp;</td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.estimator'/></td>
							<td align="left" class="listwhitetext" style="width:25px"></td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.survey'/></td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="right" class="listwhitetext" style="width:10px" colspan="8"></td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext" style="width:3px">Time&nbsp;Zone</td>
							
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:100px">Pre Move Survey:&nbsp;</td>
							<td><s:select cssClass="list-menu" name="customerFile.estimator" list="%{sale}" cssStyle="width:110px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
							<td align="left" class="listwhitetext" style="width:25px"></td>
							<c:if test="${not empty customerFile.survey}">
							<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.survey"/></s:text>
							<td><s:textfield cssClass="input-text" id="survey" name="customerFile.survey" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.survey'),cal.select(document.forms['customerFileForm'].survey,'calender2',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<c:if test="${empty customerFile.survey}">
							<td><s:textfield cssClass="input-text" id="survey" name="customerFile.survey" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.survey'),cal.select(document.forms['customerFileForm'].survey,'calender2',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='customerFile.surveyTime'/></td>
							<td align="right" class="listwhitetext" style="width:3px"></td>
							<td><s:textfield cssClass="input-text" name="customerFile.surveyTime" id="surveyTime" size="3" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event,this,'special')" onchange = "changeStatus();completeTimeString();"/> </td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='customerFile.surveyTime2'/></td>
							<td align="right" class="listwhitetext" style="width:3px"></td>
							<td><s:textfield cssClass="input-text" name="customerFile.surveyTime2" id="surveyTime2" size="3" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event,this,'special')" onchange = "changeStatus();completeTimeString();"/> <font color="blue">(hh:mm)</font></td>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<td><s:textfield name="timeZone" value="${sessionTimeZone}" readonly="true" size="15" cssClass="input-textUpper"/></td>
						</tr>
						<tr><td class="listwhitetext" style="width:100px; height:10px;" colspan="10"></td></tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td align="right" class="listwhitetext" style="width:100px">&nbsp;</td>
							<td align="right" class="listwhitetext"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.priceSubmissionToAccDate',this);return false" onmouseout="ajax_hideTooltip()">To Account</a></td>
							<td></td>
							<td align="left" class="listwhitetext" ></td>
							<td align="left" class="listwhitetext" style="width:50px"></td>
							<td align="right" class="listwhitetext"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.priceSubmissionToTranfDate',this);return false" onmouseout="ajax_hideTooltip()">To Transferee</a></td>
							<td></td>
							<td align="left" class="listwhitetext" ></td>
							<td align="left" class="listwhitetext" style="width:50px"></td>
							<td align="right" class="listwhitetext" colspan="2"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.quoteAcceptenceDate',this);return false" onmouseout="ajax_hideTooltip()">Quote Acceptance</a></td>
							<td align="left" class="listwhitetext"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:100px">Price Submission:&nbsp;</td>
							<c:if test="${not empty customerFile.priceSubmissionToAccDate}">
							<s:text id="customerFileSubmissionToAccFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToAccDate"/></s:text>
							<td><s:textfield id="priceSubmissionToAccDate" cssClass="input-text" name="customerFile.priceSubmissionToAccDate" value="%{customerFileSubmissionToAccFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender8" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.priceSubmissionToAccDate'),cal.select(document.forms['customerFileForm'].priceSubmissionToAccDate,'calender8',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<c:if test="${empty customerFile.priceSubmissionToAccDate}">
							<td><s:textfield id="priceSubmissionToAccDate" cssClass="input-text" name="customerFile.priceSubmissionToAccDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender8" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.priceSubmissionToAccDate'),cal.select(document.forms['customerFileForm'].priceSubmissionToAccDate,'calender8',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<td align="left" class="listwhitetext" ></td>
							<td align="left" class="listwhitetext" style="width:50px"></td>
							<c:if test="${not empty customerFile.priceSubmissionToTranfDate}">
							<s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToTranfDate"/></s:text>
							<td><s:textfield id="priceSubmissionToTranfDate" cssClass="input-text" name="customerFile.priceSubmissionToTranfDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender9" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.priceSubmissionToTranfDate'),cal.select(document.forms['customerFileForm'].priceSubmissionToTranfDate,'calender9',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<c:if test="${empty customerFile.priceSubmissionToTranfDate}">
							<td><s:textfield id="priceSubmissionToTranfDate" cssClass="input-text" name="customerFile.priceSubmissionToTranfDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender9" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.priceSubmissionToTranfDate'),cal.select(document.forms['customerFileForm'].priceSubmissionToTranfDate,'calender9',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<td align="left" class="listwhitetext" ></td>
							<td align="left" class="listwhitetext" style="width:50px"></td>
							<c:if test="${not empty customerFile.quoteAcceptenceDate}">
							<s:text id="customerFileAcceptenceFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.quoteAcceptenceDate"/></s:text>
							<td><s:textfield id="quoteAcceptenceDate" cssClass="input-text" name="customerFile.quoteAcceptenceDate" value="%{customerFileAcceptenceFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender10" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.quoteAcceptenceDate'),cal.select(document.forms['customerFileForm'].quoteAcceptenceDate,'calender10',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<c:if test="${empty customerFile.quoteAcceptenceDate}">
							<td><s:textfield id="quoteAcceptenceDate" cssClass="input-text" name="customerFile.quoteAcceptenceDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender10" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.quoteAcceptenceDate'),cal.select(document.forms['customerFileForm'].quoteAcceptenceDate,'calender10',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
							</c:if>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<c:if test="${empty customerFile.id}">
							<td align="right" style="width:225px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty customerFile.id}">
							<c:choose>
								<c:when test="${countSurveyNotes == '0' || countSurveyNotes == '' || countSurveyNotes == null}">
								<td align="right" style="width:225px"><img id="countSurveyNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',740,400);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="right" style="width:225px"><img id="countSurveyNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',740,400);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>
						</tr>
						<tr><td class="listwhitetext" style="width:100px; height:5px;" colspan="7"></td></tr>
					</tbody>
				</table>
				</td>
				</tr>
				
									<tr>
										<td height="10" width="100%" align="left" >
										<div class="subcontent-tab"><a href="javascript:void(0)"
											class="dsphead" onclick="dsp(this);"><span
											class="dspchar2"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" align="absmiddle" border="0"/></span>&nbsp;&nbsp;Address Details</a></div>
										
										<div class="dspcont" id="Layer1">
										<table class="detailTabLabel" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td valign="top" align="left" class="listwhitetext">
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr><font color="black"><b>&nbsp;Origin</b></font>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originAddress1'/></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" style="width:35px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originCompany'/></td>
															</tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.originAddress1" size="35" maxlength="100"/></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" style="width:35px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.originCompany" size="45" maxlength="70" onblur="copyCompanyToDestination(this)"/></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.originAddress2" size="35" maxlength="100"/></td>
																<td align="left" class="listwhitetext"><font color="gray">Address2</font></td>
																<td align="left" class="listwhitetext" style="width:35px"></td>
																<td align="left" class="listwhitetext"></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.originAddress3" size="100" maxlength="30"/></td>
																<td align="left" class="listwhitetext"><font color="gray">Address3</font></td>
																<td align="left" class="listwhitetext" style="width:35px"></td>
																<td align="left" class="listwhitetext"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originCountry'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originState'/></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																<td align="left" class="listwhitetext" onmouseover ="gettooltip('customerFile.originCity');" onmouseout="hideddrivetip();"><fmt:message key='customerFile.originCity'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originZip' /></td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px">&nbsp;</td>
																<td><s:select cssClass="list-menu" name="customerFile.originCountry" list="%{ocountry}" cssStyle="width:195px" onchange="changeStatus();autoPopulate_customerFile_originCountry(this);getState(this)"  headerKey="" headerValue=""/></td><td><img src="${pageContext.request.contextPath}/images/globe.jpg" HEIGHT=20 WIDTH=20 onclick="openOriginLocation();"/></td>
																<td><s:select cssClass="list-menu" id="originState" name="customerFile.originState" list="%{ostates}" cssStyle="width:130px" onchange="changeStatus();autoPopulate_customerFile_originCityCode(this);"/></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originCity" size="15" maxlength="30" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="autoPopulate_customerFile_originCityCode(this,'special');" /></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originZip" size="9" maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originDayPhone'/></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext"></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originHomePhone'/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originMobile'/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originFax'/></td>
															</tr>
															<tr>	
																<td align="right" class="listwhitetext" style="width:90px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.originDayPhone" size="15" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
																<td align="right" class="listwhitetext" style="width:25px"><fmt:message key='customerFile.originDayPhoneExt'/></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originDayPhoneExt" size="4" maxlength="3" onkeydown="return onlyNumsAllowed(event,this,'special')" onblur="valid(this,'special')" /></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originHomePhone" size="15" maxlength="16" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originMobile" size="15" maxlength="25" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originFax" size="15" maxlength="16" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.email'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.email2'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originSkypeId'/></td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px">&nbsp;</td>
																<td ><s:textfield cssClass="input-text" name="customerFile.email" size="30" maxlength="65" /></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.email2" size="30" maxlength="65" /></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.originSkypeId" size="24" maxlength="65" /></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<c:if test="${empty customerFile.id}">
																<td style="width:0px" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countOriginNotes == '0' || countOriginNotes == '' || countOriginNotes == null}">
																		<td style="width:0px" align="right"><img id="countOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',740,400);" ></a></td>
																	</c:when>
																	<c:otherwise>
																		<td style="width:0px" align="right"><img id="countOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',740,400);" ></a></td>
																	</c:otherwise>
																</c:choose>
																</c:if>
															</tr>
														</tbody>
													</table>
													</td>
													
												</tr>
												<tr><td class="listwhitetext" style="width:100px; height:10px;" colspan="7"></td></tr>
												<tr>
													<td align="left" ><img height="2px" width="710px" src="<c:url value='/images/vertline.jpg'/>"/></td>
												</tr>
												<tr>
													<td valign="top" align="left" class="listwhitetext" >
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr><font color="black"><b>&nbsp;Destination</b></font>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationAddress1'/></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" style="width:35px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCompany'/></td>
															</tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.destinationAddress1" size="35" maxlength="100"/></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" style="width:35px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.destinationCompany" size="45" maxlength="70"  /></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.destinationAddress2" size="35" maxlength="100"/></td>
																<td align="left" class="listwhitetext"><font color="gray">Address2</font></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" style="width:35px"></td>
																<td align="left" class="listwhitetext"></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:90px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.destinationAddress3" size="35" maxlength="100"/></td>
																<td align="left" class="listwhitetext"><font color="gray">Address3</font></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext" style="width:35px"></td>
																<td align="left" class="listwhitetext"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCountry'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationState'/></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCity'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationZip' /></td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px">&nbsp;</td>
																<td><s:select cssClass="list-menu" name="customerFile.destinationCountry" list="%{dcountry}" cssStyle="width:195px" onchange="changeStatus();autoPopulate_customerFile_destinationCountry(this);getDestinationState(this);"  headerKey="" headerValue=""/></td><td><img src="${pageContext.request.contextPath}/images/globe.jpg" HEIGHT=20 WIDTH=20 onclick="openDestinationLocation();"/></td>
																<td><s:select cssClass="list-menu" id="destinationState" name="customerFile.destinationState" list="%{dstates}" cssStyle="width:130px" value="%{customerFile.destinationState}" onchange="changeStatus();autoPopulate_customerFile_destinationCityCode(this);"/></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationCity" size="15" maxlength="30" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="autoPopulate_customerFile_destinationCityCode(this,'special');" /></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationZip" size="9" maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationDayPhone'/></td>
																<td align="left" class="listwhitetext"></td>
																<td align="left" class="listwhitetext"></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.destinationHomePhone'/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.destinationMobile'/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationFax'/></td>
															</tr>
															<tr>	
																<td align="right" class="listwhitetext" style="width:90px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationDayPhone" size="15" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
																<td align="right" class="listwhitetext" style="width:25px"><fmt:message key='customerFile.destinationDayPhoneExt'/></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationDayPhoneExt" size="4" maxlength="3" onkeydown="return onlyNumsAllowed(event,this,'special')" onblur="valid(this,'special')" /></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationHomePhone" size="15" maxlength="16" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationMobile" size="15" maxlength="25" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationFax" size="15" maxlength="16" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationEmail'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationEmail2'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationSkypeId'/></td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:90px"></td>
																<td align="left"><s:textfield cssClass="input-text" name="customerFile.destinationEmail" size="30" maxlength="65" /></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.destinationEmail2" size="30" maxlength="65" /></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.destinationSkypeId" size="24" maxlength="65" /></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<c:if test="${empty customerFile.id}">
																<td style="width:0px" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countDestinationNotes == '0' || countDestinationNotes == '' || countDestinationNotes == null}">
																		<td style="width:0px" align="right"><img id="countDestinationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',740,400);" ></a></td>
																	</c:when>
																	<c:otherwise>
																		<td style="width:0px" align="right"><img id="countDestinationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',740,400);" ></a></td>
																	</c:otherwise>
																</c:choose>
																</c:if>
															</tr>
															<tr><td class="listwhitetext" style="width:100px; height:10px;" colspan="7"></td></tr>
														</tbody>
													</table>
													</td>
												</tr>
											</tbody>
										</table>
										</div>
										</td>
									</tr>
									<%-- <c:if test="${(customerFile.billToCode == '500270') || (empty customerFile.id)}"> --%>
									<tr>
										<td height="10" align="left" class="listwhitetext">
										<div class="subcontent-tab"><a href="javascript:void(0)"
											class="dsphead" onclick="dsp(this);"><span
											class="dspchar2"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" border="0" align="absmiddle" /></span>&nbsp;&nbsp;Alternative&nbsp;/&nbsp;Spouse&nbsp;Contact</a></div>
										
										<div class="dspcont" id="Layer1" >
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:100px; height:10px;"></td></tr>
															<tr></tr>
															<tr></tr>
															<tr><font color="black"><b>&nbsp;At Origin</b></font>
																<td align="left" valign="top" class="listwhitetext" style="width:108px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactName'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.organization'/></td>
															</tr>
															<tr>
																<td align="right" valign="top" class="listwhitetext" style="width:108px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" id="contactName" name="customerFile.contactName"  size="33" maxlength="35" onkeydown="return onlyCharsAllowed(event, this, 'special')" onblur="valid(this,'special')"/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:select cssClass="list-menu" name="customerFile.organization" list="%{organiza}" onchange="changeStatus();"/></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" valign="top" class="listwhitetext" style="width:108px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactEmail'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactPhone'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactFax'/></td>
															</tr>
															<tr>
																<td align="right" valign="top" class="listwhitetext" style="width:108px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.contactEmail"  size="33" maxlength="65" /></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.contactPhone"  size="15" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.contactFax" size="15" maxlength="20" onkeydown="return onlyNumsAllowed(event,this,'special')" onblur="valid(this,'special')" /></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
															<tr></tr>
															<tr></tr>
															<tr>
																<td align="center" colspan="10"><img height="2px" width="710px" src="<c:url value='/images/vertline.jpg'/>"/></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
															<tr></tr>
															<tr></tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><font color="black"><b>&nbsp;At Destination</b></font>
																<td align="left" valign="top" class="listwhitetext" style="width:160px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactName'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.organization'/></td>
															</tr>
															<tr>
																<td align="right" valign="top" class="listwhitetext" style="width:160px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationContactName"  size="33" maxlength="35" onkeydown="return onlyCharsAllowed(event, this, 'special')" onblur="valid(this,'special')"/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:select cssClass="list-menu" name="customerFile.destinationOrganization" list="%{organiza}" onchange="changeStatus();"/></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" valign="top" class="listwhitetext" style="width:160px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactEmail'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactPhone'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactFax'/></td>
															</tr>
															<tr>
																<td align="right" valign="top" class="listwhitetext" style="width:160px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationContactEmail"  size="33" maxlength="65" /></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationContactPhone"  size="15" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationContactFax" size="15" maxlength="20" onkeydown="return onlyNumsAllowed(event,this,'special')" onblur="valid(this,'special')" /></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
																<c:if test="${empty customerFile.id}">
																<td align="right" style="width:215px" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countContactNotes == '0' || countContactNotes == '' || countContactNotes == null}">
																	<td align="right" style="width:215px" valign="bottom"><img id="countContactNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',740,400);" ></a></td>
																	</c:when>
																	<c:otherwise>
																	<td align="right" style="width:215px" valign="bottom"><img id="countContactNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',740,400);" ></a></td>
																	</c:otherwise>
																</c:choose> 
																</c:if>
															</tr>
															<tr><td class="listwhitetext" style="width:100px; height:10px;"></td></tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
															<tr></tr>
															<tr></tr>
															<tr>
																<td align="center" colspan="10"><img height="2px" width="710px" src="<c:url value='/images/vertline.jpg'/>"/></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
															<tr></tr>
															<tr></tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><font color="black"><b>&nbsp;Spouse Contact</b></font>
															<c:set var="privilegeFlag" value="false"/>
															<c:if test="${customerFile.privileges}">
												 				<c:set var="privilegeFlag" value="true"/>
															</c:if>
															<td class="listwhitetext" style="width:50px; height:10px;"></td></tr>
															<tr></tr>
															<tr></tr>
															<tr>
																<td align="right" valign="top" class="listwhitetext" style="width:108px">&nbsp;</td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.custPartnerPrefix'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerFirstName'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerLastName'/></td>
															</tr>
															<tr>
																<td align="right" valign="top" class="listwhitetext" style="width:108px">&nbsp;</td>
																<td align="left" class="listwhitetext"> <s:select cssClass="list-menu" name="customerFile.custPartnerPrefix" cssStyle="width:47px" list="%{preffix}" headerKey="" headerValue="" onchange="changeStatus();"/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"> <s:textfield cssClass="input-text" name="customerFile.custPartnerFirstName" required="true"
																cssStyle="width:167px" maxlength="80" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')"/> </td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"> <s:textfield cssClass="input-text" key="customerFile.custPartnerLastName" required="true"
																size="36" maxlength="80" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')"/> </td>
																<td align="right">&nbsp;&nbsp;<s:checkbox key="customerFile.privileges" value="${privilegeFlag}" fieldValue="true"/></td>
																<td align="left" class="listwhitetext" style="width:65px"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.privileges',this);return false" onmouseout="ajax_hideTooltip()">Joint&nbsp;Account</a></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" valign="top" class="listwhitetext" style="width:160px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerEmail'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerContact'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.custPartnerMobile'/></td>
															</tr>
															<tr>
																<td align="right" valign="top" class="listwhitetext" style="width:160px">&nbsp;</td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.custPartnerEmail"  size="39" maxlength="65" /></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext" > <s:textfield cssClass="input-text" name="customerFile.custPartnerContact"size="15" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/> </td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext" > <s:textfield cssClass="input-text" name="customerFile.custPartnerMobile"size="15" maxlength="25" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/> </td>
																<c:if test="${empty customerFile.id}">
																<td align="right" style="width:180px" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countSpouseNotes == '0' || countSpouseNotes == '' || countSpouseNotes == null}">
																	<td align="right" style="width:180px" valign="bottom"><img id="countSpouseNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',740,400);" ></a></td>
																	</c:when>
																	<c:otherwise>
																	<td align="right" style="width:180px" valign="bottom"><img id="countSpouseNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',740,400);" ></a></td>
																	</c:otherwise>
																</c:choose> 
																</c:if>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:10px;" colspan="7"></td></tr>
														</tbody>
													</table>
										</div>
										</td>
									</tr>
									<%-- </c:if> --%>	
									<tr>
										<td height="10" align="left" class="listwhitetext" colspan="2">
										<div class="subcontent-tab"><a href="javascript:void(0)"
											class="dsphead" onclick="dsp(this)"><span
											class="dspchar2" ><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" border="0" align="absmiddle" /></span>&nbsp;&nbsp;Billing Details</a></div>
				
										<div class="dspcont" id="Layer1">
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:10px;"></td></tr>
															<tr></tr>
															<tr></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:50px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.billToCode'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.billToAuthorization',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='customerFile.billToAuthorization'/></a></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.contract'/></td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:110px">Billing:&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.billToCode" size="10" maxlength="8" onchange="changeStatus();findBillToName()"/></td>
																<td><img class="openpopup" width="17" height="20" onclick="openPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
																<td align="left"><s:textfield cssClass="input-text" name="customerFile.billToName" size="20"  maxlength="250"/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.billToAuthorization"  size="14" maxlength="50" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td ><s:select cssClass="list-menu" name="customerFile.contract" list="%{contract}" cssStyle="width:190px"  headerKey="" headerValue="" onchange="changeStatus();"/></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<c:set var="noChargeFlag" value="false"/>
																<c:if test="${customerFile.noCharge}">
									 								<c:set var="noChargeFlag" value="true"/>
																</c:if>
																<td align="right" class="listwhitetext" style="width:50px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.billPayMethod'/></td>
																	<td align="right" class="listwhitetext" style="width:15px">&nbsp;</td>
																<td align="left" class="listwhitetext">Account&nbsp;Reference&nbsp;#</td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:110px">&nbsp;</td>
																<td><s:select cssClass="list-menu" name="customerFile.billPayMethod" list="%{paytype}" cssStyle="width:228px"  headerKey="" headerValue="" onchange="changeStatus();"/></td>
																<td align="right" class="listwhitetext" style="width:15px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.billToReference"  size="14" maxlength="15" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
															<tr></tr>
															<tr></tr>
															<tr>
																<td align="center" colspan="10"><img height="2px" width="710px" src="<c:url value='/images/vertline.jpg'/>"/></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
															<tr></tr>
															<tr></tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:50px">&nbsp;</td>
																<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.orderBy',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='customerFile.orderBy'/></a></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.billApprovedDate'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.orderPhone'/></td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:110px">Authorization:&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.orderBy" size="25" maxlength="30" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="autoPopulate_customerFile_approvedDate(this,'special');"/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<c:if test="${not empty customerFile.billApprovedDate}">
																<s:text id="customerFileApprovedDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.billApprovedDate"/></s:text>
																<td><s:textfield cssClass="input-text" id="billApprovedDate" name="customerFile.billApprovedDate" value="%{customerFileApprovedDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender5" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.billApprovedDate'),cal.select(document.forms['customerFileForm'].billApprovedDate,'calender5',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
																</c:if>
																<c:if test="${empty customerFile.billApprovedDate}">
																<td><s:textfield cssClass="input-text" id="billApprovedDate" name="customerFile.billApprovedDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender5" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.billApprovedDate'),cal.select(document.forms['customerFileForm'].billApprovedDate,'calender5',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
																</c:if>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.orderPhone"  size="15" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')"/></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:110px">&nbsp;</td>
																<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.personPricing',this);return false" onmouseout="ajax_hideTooltip()">Pricing</a></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.personBilling',this);return false" onmouseout="ajax_hideTooltip()">Billing</a></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.personPayable',this);return false" onmouseout="ajax_hideTooltip()">Payable</a></td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:110px">Responsible Person:&nbsp;</td>
																<td><s:select name="customerFile.personPricing" cssClass="list-menu" list="%{pricing}" cssStyle="width:100px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:select name="customerFile.personBilling" cssClass="list-menu" list="%{billing}" cssStyle="width:100px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
																<td align="right" class="listwhitetext" style="width:15px"></td>
																<td><s:select cssClass="list-menu" name="customerFile.personPayable" list="%{payable}" cssStyle="width:100px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
																<td align="right" class="listwhitetext" style="width:10px"></td>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:10px;"></td></tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="right" width="110px"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.noCharge',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='customerFile.noCharge'/></td>
																<td align="left"><s:checkbox key="customerFile.noCharge" value="${noChargeFlag}" fieldValue="true"/></td>
																<td align="right" width="70px">Approved&nbsp;By&nbsp;</td>
																<td align="left" colspan="2">
																<s:select cssClass="list-menu" name="customerFile.approvedBy" list="%{exec}"  cssStyle="width:100px" headerKey="" headerValue="" onchange="changeStatus();"/>
																</td>
																<td align="right" width="100px">Approved&nbsp;On&nbsp;</td>
																
														    	
														    	<c:if test="${not empty customerFile.approvedOn}">
																	<td align="left"><s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="customerFile.approvedOn"/></s:text>
																    <s:textfield id="dateApprovedOn" name="customerFile.approvedOn" value="%{FormatedInvoiceDate}" onkeydown="return onlyDel(event,this)"  onfocus="changeStatus();" readonly="true" cssClass="input-text" size="7"/></td>
																    <td><img id="calender12" class="openpopup" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.approvedOn'),cal.select(document.forms['customerFileForm'].dateApprovedOn,'calender12',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
																</c:if>
																<c:if test="${empty customerFile.approvedOn}">
																	<td align="left"><s:textfield id="dateApprovedOn" name="customerFile.approvedOn" onkeydown="return onlyDel(event,this)"  onfocus="changeStatus();" readonly="true" cssClass="input-text" size="7"/></td>
																	<td><img align="top" id="calender12" class="openpopup" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="focusDate('customerFile.approvedOn'),cal.select(document.forms['customerFileForm'].dateApprovedOn,'calender12',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>
																</c:if>
																<c:if test="${empty customerFile.id}">
																<td align="right" style="width:210px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countBillingNotes == '0' || countBillingNotes == '' || countBillingNotes == null}">
																	<td align="right" style="width:210px"><img id="countBillingNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Billing&imageId=countBillingNotesImage&fieldId=countBillingNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Billing&imageId=countBillingNotesImage&fieldId=countBillingNotes&decorator=popup&popup=true',740,400);" ></a></td>
																	</c:when>
																	<c:otherwise>
																	<td align="right" style="width:210px"><img id="countBillingNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Billing&imageId=countBillingNotesImage&fieldId=countBillingNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Billing&imageId=countBillingNotesImage&fieldId=countBillingNotes&decorator=popup&popup=true',740,400);" ></a></td>
																	</c:otherwise>
																</c:choose> 
																</c:if>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:10px;"></td></tr>
														</tbody>
													</table>
													
										</div>
										</td>
									</tr>
 						<tr>
							<td height="10" align="left" class="listwhitetext">
							<div class="subcontent-tab"><a href="javascript:void(0)"
								class="dsphead" onclick="dsp(this)"><span
								class="dspchar2" ><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" border="0" align="absmiddle" /></span>&nbsp;&nbsp;Entitlement</a></div>
	
							<div class="dspcont" id="Layer1">
										<table class="detailTabLabel" cellspacing="0" cellpadding="0">
											<tbody>
												<tr><td class="listwhitetext" style="width:100px; height:10px;"></td></tr>
												<tr></tr>
												<tr></tr>
												<tr>
													<td align="right" valign="top" class="listwhitetext" style="width:160px">Entitlement:&nbsp;</td>
													<td><s:textarea name="customerFile.entitled" cols="80" rows="8" cssClass="textarea"/></td>
													<td class="listwhitetext" style="width:5px;"></td>
													<td valign="bottom"><!-- input type="button" class="cssbuttonB" value="Add Entitlement" /--></td>
													<td align="right" class="listwhitetext" style="width:10px"></td>
													<c:if test="${empty customerFile.id}">
													<td align="right" valign="bottom" style="width:405px" ><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
													</c:if>
													<c:if test="${not empty customerFile.id}">
													<c:choose>
														<c:when test="${countEntitlementNotes == '0' || countEntitlementNotes == '' || countEntitlementNotes == null}">
														<td align="right" style="width:405px" valign="bottom"><img id="countEntitlementNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',740,400);" ></a></td>
														</c:when>
														<c:otherwise>
														<td align="right" style="width:405px" valign="bottom"><img id="countEntitlementNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',740,400);" ></a></td>
														</c:otherwise>
													</c:choose> 
													</c:if>
													</tr>
													<tr><td class="listwhitetext" align="right" >Assignment&nbsp;Type:&nbsp;</td>
													<td class="listwhitetext"><s:radio name="customerFile.assignmentType" list="%{assignmentTypes}" /></td></tr>
													<tr><td class="listwhitetext" style="width:50px; height:10px;"></td></tr>
												
												<tr><td class="listwhitetext" style="width:100px; height:10px;"></td></tr>
											</tbody>
										</table>
							</div>
							</td>
						</tr>
						<%-- 
						<tr>
							<td height="10" align="left" class="listwhitetext">
							<div class="subcontent-tab"><a href="javascript:void(0)"
								class="dsphead" onclick="dsp(this)"><span
								class="dspchar2"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" border="0" align="absmiddle" /></span>&nbsp;&nbsp;Spouse Details</a></div>
	
							<div class="dspcont" id="Layer1">
										<table class="detailTabLabel" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
												<c:set var="privilegeFlag" value="false"/>
												<c:if test="${customerFile.privileges}">
									 				<c:set var="privilegeFlag" value="true"/>
												</c:if>
												<td class="listwhitetext" style="width:50px; height:10px;"></td></tr>
												<tr></tr>
												<tr></tr>
												<tr>
													<td align="right" valign="top" class="listwhitetext" style="width:108px">&nbsp;</td>
													<td align="left" class="listwhitetext" ><fmt:message key='customerFile.custPartnerPrefix'/></td>
													<td align="left" class="listwhitetext" style="width:15px"></td>
													<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerFirstName'/></td>
													<td align="left" class="listwhitetext" style="width:15px"></td>
													<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerLastName'/></td>
												</tr>
												<tr>
													<td align="right" valign="top" class="listwhitetext" style="width:108px">&nbsp;</td>
													<td align="left" class="listwhitetext"> <s:textfield cssClass="input-text" name="customerFile.custPartnerPrefix" cssStyle="width:47px" /></td>
													<td align="right" class="listwhitetext" style="width:15px"></td>
													<td align="left" class="listwhitetext"> <s:textfield cssClass="input-text" name="customerFile.custPartnerFirstName" required="true"
													cssStyle="width:167px" maxlength="15" onkeydown="return onlyCharsAllowed(event)"/> </td>
													<td align="right" class="listwhitetext" style="width:15px"></td>
													<td align="left" class="listwhitetext"> <s:textfield cssClass="input-text" key="customerFile.custPartnerLastName" required="true"
													size="36" maxlength="80" onkeydown="return onlyCharsAllowed(event)"/> </td>
													<td align="right">&nbsp;&nbsp;<s:checkbox key="customerFile.privileges" value="${privilegeFlag}" fieldValue="true"/></td>
													<td align="left" class="listwhitetext" style="width:65px"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?decorator=simple&popup=true&tableFieldName=customerFile.privileges',this);return false" onmouseout="ajax_hideTooltip()">Joint&nbsp;Account</a></td>
												</tr>
											</tbody>
										</table>
										<table class="detailTabLabel" cellspacing="0" cellpadding="0">
											<tbody>
												<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
												<tr>
													<td align="right" valign="top" class="listwhitetext" style="width:160px">&nbsp;</td>
													<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerEmail'/></td>
													<td align="left" class="listwhitetext" style="width:15px"></td>
													<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerContact'/></td>
													<td align="left" class="listwhitetext" style="width:15px"></td>
													<td align="left" class="listwhitetext" ><fmt:message key='customerFile.custPartnerMobile'/></td>
												</tr>
												<tr>
													<td align="right" valign="top" class="listwhitetext" style="width:160px">&nbsp;</td>
													<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.custPartnerEmail"  size="39" maxlength="65" /></td>
													<td align="right" class="listwhitetext" style="width:15px"></td>
													<td align="left" class="listwhitetext" > <s:textfield cssClass="input-text" name="customerFile.custPartnerContact"size="15" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event)"/> </td>
													<td align="right" class="listwhitetext" style="width:15px"></td>
													<td align="left" class="listwhitetext" > <s:textfield cssClass="input-text" name="customerFile.custPartnerMobile"size="15" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event)"/> </td>
													<c:if test="${empty customerFile.id}">
													<td align="right" style="width:180px" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
													</c:if>
													<c:if test="${not empty customerFile.id}">
													<c:choose>
														<c:when test="${countSpouseNotes == '0' || countSpouseNotes == '' || countSpouseNotes == null}">
														<td align="right" style="width:180px" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&decorator=popup&popup=true',740,400);" ></a></td>
														</c:when>
														<c:otherwise>
														<td align="right" style="width:180px" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&decorator=popup&popup=true',740,400);" ></a></td>
														</c:otherwise>
													</c:choose> 
													</c:if>
												</tr>
												<tr><td class="listwhitetext" style="width:50px; height:10px;" colspan="7"></td></tr>
											</tbody>
										</table>
										
							</div>
							</td>
						</tr>
						--%>
						<tr>
							<td height="10" align="left" class="listwhitetext" colspan="2">
							<div class="subcontent-tab"><a href="javascript:void(0)"
							class="dsphead" onclick="dsp(this)"><span
							class="dspchar2" ><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" border="0" align="absmiddle" /></span>&nbsp;&nbsp;Customer Portal Detail</a></div>
				
							<div class="dspcont" id="Layer1">
										<table class="detailTabLabel" cellspacing="0" cellpadding="1">
											<tbody>
												<tr><td class="listwhitetext" style="width:100px; height:5px;"></td></tr>
												<tr></tr>
												<tr></tr>
												<tr>
													<c:set var="isActiveFlag" value="false"/>
													<c:if test="${customerFile.portalIdActive}">
														<c:set var="isActiveFlag" value="true"/>
													</c:if>
													<td align="right" valign="top" class="listwhitetext" style="width:50px">Customer&nbsp;Portal:&nbsp;</td>
													<td><s:checkbox key="customerFile.portalIdActive" value="${isActiveFlag}" fieldValue="true"/></td>
													<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onmouseover="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.portalIdActive',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='customerFile.portalIdActive'/></a></td>
												</tr>
												<tr><td class="listwhitetext" style="width:100px; height:5px;" colspan="7"></td></tr>
												<tr>
													<td align="right" valign="top" class="listwhitetext" style="width:50px">&nbsp;</td>
													<td align="left" class="listwhitetext"><fmt:message key='customerFile.customerPortalId'/></td>
													<td><s:textfield cssClass="input-textUpper" name="customerFile.customerPortalId" required="true" size="15" maxlength="15" readonly="true"/></td>
													<td></td>
													<c:if test="${not empty customerFile.id}">
													<td><input type="button" value="Email Password" class="cssbuttonB" onclick="generatePortalId();" /></td>
													</c:if>
													<c:if test="${empty customerFile.id}">
													<td><input type="button" disabled value="Email Password" class="cssbuttonB" onclick="generatePortalId();"/></td>
													</c:if>
													<c:if test="${empty customerFile.id}">
													<td align="right" style="width:605px" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
													</c:if>
													<c:if test="${not empty customerFile.id}">
													<c:choose>
														<c:when test="${countCportalNotes == '0' || countCportalNotes == '' || countCportalNotes == null}">
														<td align="right" style="width:605px" valign="bottom"><img id="countCportalNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',740,400);" ></a></td>
														</c:when>
														<c:otherwise>
														<td align="right" style="width:605px" valign="bottom"><img id="countCportalNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',740,400);" ></a></td>
														</c:otherwise>
													</c:choose> 
													</c:if>
												</tr>
												<tr><td class="listwhitetext" style="width:100px; height:15px;" colspan="7"></td></tr>
										</tbody>
										</table>
						</div>
						</td>
						</tr>
		</tbody>
	</table>
	</div>
				<table>
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${customerFile.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="customerFile.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${customerFile.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty customerFile.id}">
								<s:hidden name="customerFile.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{customerFile.createdBy}"/></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<s:hidden name="customerFile.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${customerFile.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="customerFile.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${customerFile.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<s:hidden name="customerFile.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:85px"><s:label name="customerFile.updatedBy" value="${pageContext.request.remoteUser}" /></td>
						</tr>
					</tbody>
				</table>
	
<s:submit cssClass="cssbuttonA" method="save" key="button.save" onmouseover="completeTimeString();" onclick="return IsValidTime('save');"/>
				<c:if test="${not empty customerFile.id}">
		    <input type="button" class="cssbutton1" 
	        onclick="location.href='<c:url   value="/editCustomerFile.html"/>'" 
	        value="<fmt:message  key="button.add"/>"/> 
	    </c:if>  
<!-- 		<input type="submit" name="saveBtn" value="Save" style="width:87px; height:30px" />  -->
<s:hidden name="submitCnt" value="<%=request.getParameter("submitCnt") %>"/>
<s:hidden name="submitType" value="<%=request.getParameter("submitType") %>"/>
<c:set var="submitType" value="<%=request.getParameter("submitType") %>"/>
	<c:choose>
		<c:when test="${submitType == 'job'}">
			<s:reset type="button" cssClass="cssbutton1" key="Reset" />
		</c:when>
		<c:otherwise>
			<s:reset type="button" cssClass="cssbutton1" key="Reset" />
		</c:otherwise>
	 </c:choose> 
		
								
<s:hidden name="customerFile.statusNumber" />	
<s:hidden name="customerFile.nextCheck" />	
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="sourceCodeTemp" />
<s:hidden name="customerFile.quotationStatus" />	
<s:hidden name="customerFile.controlFlag" />	


<s:text id="customerFileSystemDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.systemDate" /></s:text>
<td valign="top"><s:hidden name="customerFile.systemDate" value="%{customerFileSystemDateFormattedValue}" /></td>
							
<s:hidden name="sendEmail" />

<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>
<c:if test="${empty customerFile.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty customerFile.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<input type="hidden" name="encryptPass" value="true" />

<s:hidden key="user.id"/>
<s:hidden key="user.version"/>

</s:form>
<script type="text/javascript">
	 try{
	 getState(document.forms['customerFileForm'].elements['customerFile.originCountry']);
	}
	catch(e){}
	 try{
	 getDestinationState(document.forms['customerFileForm'].elements['customerFile.destinationCountry']);
	}
	catch(e){}
</script>
<script type="text/javascript">  
 
    Form.focusFirstElement($("customerFileForm"));   
</script>