package com.trilasoft.app.webapp.helper;

import java.text.SimpleDateFormat;
import java.util.Map;

public class SearchCriterion {

	private String fieldName;

	private Object fieldValue;

	private int fieldType;

	private int operator;

	public static int FIELD_TYPE_BIT = -7;

	public static int FIELD_TYPE_TINYINT = -6;

	public static int FIELD_TYPE_BIGINT = -5;

	public static int FIELD_TYPE_LONGVARBINARY = -4;

	public static int FIELD_TYPE_VARBINARY = -3;

	public static int FIELD_TYPE_BINARY = -2;

	public static int FIELD_TYPE_LONGVARCHAR = -1;

	public static int FIELD_TYPE_NULL = 0;

	public static int FIELD_TYPE_CHAR = 1;

	public static int FIELD_TYPE_NUMERIC = 2;

	public static int FIELD_TYPE_DECIMAL = 3;

	public static int FIELD_TYPE_INTEGER = 4;

	public static int FIELD_TYPE_SMALLINT = 5;

	public static int FIELD_TYPE_FLOAT = 6;

	public static int FIELD_TYPE_REAL = 7;

	public static int FIELD_TYPE_DOUBLE = 8;

	public static int FIELD_TYPE_STRING = 12;

	public static int FIELD_TYPE_DATE = 91;

	public static int FIELD_TYPE_TIME = 92;

	public static int FIELD_TYPE_TIMESTAMP = 93;

	public static int FIELD_TYPE_OTHER = 1111;
	
	public static int FIELD_TYPE_SQL = 2222;

	public static int OPERATOR_EQUALS = 0;

	public static int OPERATOR_NOT_EQUALS = 1;

	public static int OPERATOR_BETWEEN = 2;

	public static int OPERATOR_LIKE = 3;
	public static int OPERATOR_IN = 14;
	public static int OPERATOR_START_LIKE = 9;
	public static int OPERATOR_END_LIKE = 11;
	
	public static int OPERATOR_GE = 4;
	
	public static int OPERATOR_NONE = -1;
	
	public SearchCriterion(String fieldName, Object fieldValue, int fieldType,
			int operator) {
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
		this.fieldType = fieldType;
		this.operator = operator;
		
		if (fieldType == FIELD_TYPE_STRING){
			if(fieldValue!=null){
				fieldValue= formatStringValue(fieldValue.toString());	
			}
			
		}
		if (fieldType == FIELD_TYPE_DATE){
			if(fieldValue!=null){
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder formatedDate1 = new StringBuilder(dateformatYYYYMMDD1.format(fieldValue));
				fieldValue= formatedDate1;	
			}
			
		}
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Object getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(Object fieldValue) {
		this.fieldValue = fieldValue;
	}

	public static String formatStringValue(Object value) {
		return (value==null)?"":value.toString().replaceAll("'", "''").replaceAll(":", "''");
	}

	public int getFieldType() {
		return fieldType;
	}

	public void setFieldType(int fieldType) {
		this.fieldType = fieldType;
	}

	public int getOperator() {
		return operator;
	}

	public void setOperator(int operator) {
		this.operator = operator;
	}

}
