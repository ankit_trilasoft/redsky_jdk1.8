<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<head>
    <title>Work Plan List</title>
    <meta name="heading" content="Work Plan List"/>
   
<style> 
   span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
!margin-bottom:2px;
margin-top:-22px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
   
</style>
<script type="text/javascript">
function reportOpen(ticket){
	//var url='viewReportWithParam.html?id=1992&reportName=Revenue for Bill To By Job&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Bill To=500130&reportParameter_Corporate ID=SSCW&fileType=EXTRACT';
	/* new Ajax.Request('viewReportWithParam.html?id=145&fileType=PDF&reportParameter_Work Ticket Number='+ticket+'&reportParameter_Corporate ID=SSCW&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			    //alert(response);
			      var container = document.getElementById(id);
			     // alert(container);
			      container.innerHTML = response;
			      //container.update(response);
			      container.show();
			      
			    },
			    
			    onLoading: function()
			       {
			    	 var loading = document.getElementById("loading");
			    	 loading.innerHTML ;
			    },
			    
			    onFailure: function(){ 
				    //alert('Something went wrong...')
				     }
			  }); */
	document.forms['workPlanListForm'].elements['reportParameter_Work Ticket Number'].value = ticket;
			  //alert(document.forms['workPlanListForm'].elements['reportParameter_Work Ticket Number'].value);
	var url = 'viewReceipt.html?decorator=popup&popup=true';
	document.forms['workPlanListForm'].action =url;
	document.forms['workPlanListForm'].submit();
}
</script>

</head>
<s:form id="workPlanListForm" action="" method="post" validate="true">
<s:hidden name="reportParameter_Work Ticket Number" value=""></s:hidden>
<s:hidden name="reportParameter_Corporate ID" value="${sessionCorpID}"></s:hidden>
<s:hidden name="fileType" value="PDF"></s:hidden>
<s:hidden name="jasperName" value="TktByTktNumber.jrxml"></s:hidden>
<div id="newmnav">
		  <ul>
		    <li  id="newmnav1" style="background:#FFF "><a class="current" style="margin-bottom:-1px;"><span>Work Summary<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
</div>
<div class="spn">&nbsp;</div>
<div style="!padding-bottom:5px;"></div>
<s:set name="driverTicketSchedule" value="driverTicketSchedule" scope="request"/>
<c:if test="${driverTicketSchedule!='[]'}">
<display:table name="driverTicketSchedule" class="table" requestURI="" id="driverTicketSchedule" export="true" pagesize="200" defaultsort="1">
	    <display:column property="dateDisplay" group="1" sortable="true" title="Date" format="{0,date,dd-MMM-yyyy}"/> 
	    <display:column sortable="true" title="Ticket">
	    <a href="javascript:;" onclick="window.open('editWorkTicketUpdate.html?id=${driverTicketSchedule.id}')"><c:out value="${driverTicketSchedule.ticket}"></c:out> </a>
	    </display:column>
	     <display:column property="warehouse" sortable="true" title="Warehouse"/>
	     <display:column property="service" sortable="true" title="Service"/>
	     <display:column property="estimatedweight" sortable="true" title="Est. Weight"/>
	     <display:column property="estimatedCartoons" sortable="true" title="Est. Cartons"/>
	     <display:column property="trucks" sortable="true" title="Trucks"/>
	     <display:column property="shipper" sortable="true" title="Shipper"/>
	     <display:column title="Arrival Time" property="aTime" > </display:column>
		<display:column title="Departure Time" property="dTime" > </display:column>
	     <display:column property="originCity" sortable="true" title="Origin City"/>
	     <display:column property="destinationCity" sortable="true" title="Destination City"/>
	     <display:column   title="Print">
	     <a><img align="middle"  onclick="reportOpen('${driverTicketSchedule.ticket}');"  style="margin: 0px 0px 0px 8px;" src="images/print-blue.gif"/></a>
	     </display:column>
</display:table>
</c:if>
<c:if test="${driverTicketSchedule=='[]'}">
<display:table name="driverTicketSchedule" class="table" requestURI="" id="driverTicketSchedule" export="true" pagesize="200" defaultsort="1">
<display:column  group="1"  title="Date" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column  title="Ticket"/>
	     <display:column   title="Warehouse"/>
	     <display:column title="Service"/>
	     <display:column title="Est. Weight"/>
	      <display:column title="Est. Cartons"/>
	     <display:column   title="Trucks"/>
	     <display:column title="Shipper"/>
	     <display:column  title="Origin City"/>
	     <display:column  title="Destination City"/>
	      <display:column   title="Print"/>
</display:table>
</c:if>
</s:form>