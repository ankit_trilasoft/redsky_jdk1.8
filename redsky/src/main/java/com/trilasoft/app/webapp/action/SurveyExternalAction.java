package com.trilasoft.app.webapp.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.CustomerFileService;
import com.trilasoft.app.webapp.helper.SearchCriterion;

public class SurveyExternalAction extends BaseAction {
	
private CustomerFileManager customerFileManager;
private CustomerFile customerFile;
private CustomerFileService customerFileService;

public void setCustomerFileManager(CustomerFileManager customerFileManager) {
	this.customerFileManager = customerFileManager;
}
public CustomerFile getCustomerFile() {
	return customerFile;
}
public void setCustomerFile(CustomerFile customerFile) {
	this.customerFile = customerFile;
}
public SurveyExternalAction() { 
	this.sessionCorpID = "SSCW";
}
    private List maxSequenceNumber;
    private Long autoSequenceNumber;
    private String sequenceNumber;
	private String lead_name_first;
	private String lead_name_last;
	private String lead_from_add1;
	private String lead_from_add2;
	private String lead_from_city;
	private String lead_from_state;
	private String lead_from_country;
	private String lead_from_postal;
	private String lead_to_add1;
	private String lead_to_add2;
	private String lead_to_city;
	private String lead_to_state;
	private String lead_to_country; 
	private String lead_to_postal;
	private String lead_email;
	private String lead_phone1;
	private String lead_phone2;
	private String lead_phone3;
	private String lead_altphone1;
	private String lead_altphone2;
	private String lead_altphone3;
	private String lead_busphone1;
	private String lead_busphone2;
	private String lead_busphone3;
	private String massage;
	private Long id;
	private String sessionCorpID=new String("SSCW");
	
   public String createCustomerFile() throws Exception {
	   return SUCCESS; 
   }
   public InputStream getExternalSurveyCreation(){
	   String result = "";
	   String description ="";
	   String UserIPaddress=getRequest().getRemoteAddr(); 
	   List activeIpsList=customerFileService.getSurveyExternalIPsList("SSCW"); 
	   try{
			if(activeIpsList!=null && (!(activeIpsList.isEmpty()))){
				String surveyExternalIPList="";
				surveyExternalIPList=activeIpsList.get(0).toString();
			  if(!surveyExternalIPList.equals("")){
				  surveyExternalIPList=surveyExternalIPList.trim();
			   if(surveyExternalIPList.indexOf("(")==0)
			    {
				   surveyExternalIPList=surveyExternalIPList.substring(1);
			    }
			  if(surveyExternalIPList.lastIndexOf(")")==surveyExternalIPList.length()-1)
			    {
				  surveyExternalIPList=surveyExternalIPList.substring(0, surveyExternalIPList.length()-1);
			    }
			  if(surveyExternalIPList.indexOf(",")==0)
				{
				  surveyExternalIPList=surveyExternalIPList.substring(1);
				}
			 if(surveyExternalIPList.lastIndexOf(",")==surveyExternalIPList.length()-1)
				{
				 surveyExternalIPList=surveyExternalIPList.substring(0, surveyExternalIPList.length()-1);
				}
			    String[] arrayexcludeIPs=surveyExternalIPList.split(",");
			    int arrayLength = arrayexcludeIPs.length;
				for(int i=0;i<arrayLength;)
				 {
					String IPaddress=arrayexcludeIPs[i];
					if(IPaddress.indexOf("'")==0)
					 {
						IPaddress=IPaddress.substring(1);
					 }
					if(IPaddress.lastIndexOf("'")==IPaddress.length()-1)
					 {
						IPaddress=IPaddress.substring(0, IPaddress.length()-1);
					 }
					if(UserIPaddress.equals(IPaddress)){
						customerFile=new CustomerFile();
						  customerFile.setCorpID("SSCW");
						  customerFile.setFirstName(lead_name_first);
						  customerFile.setLastName(lead_name_last);
						  customerFile.setOriginAddress1(lead_from_add1);
						  customerFile.setOriginAddress2(lead_from_add2);
						  customerFile.setOriginCity(lead_from_city);
						  customerFile.setOriginState(lead_from_state); 
						  customerFile.setOriginCountryCode(lead_from_country);
						  customerFile.setOriginZip(lead_from_postal);
						  customerFile.setDestinationAddress1(lead_to_add1);
						  customerFile.setDestinationAddress2(lead_to_add2);
						  customerFile.setDestinationCity(lead_to_city);
						  customerFile.setDestinationState(lead_to_state);
						  customerFile.setDestinationCountryCode(lead_to_country);
						  customerFile.setDestinationZip(lead_to_postal); 
						  customerFile.setEmail(lead_email); 
						  customerFile.setOriginMobile(lead_phone1+"-"+lead_phone2+"-"+lead_phone3);
						  customerFile.setOriginHomePhone(lead_altphone1+"-"+lead_altphone2+"-"+lead_altphone3);
						  customerFile.setOriginDayPhone(lead_busphone1+"-"+lead_busphone2+"-"+lead_busphone3);
						  customerFile.setControlFlag("C");
						  customerFile.setStatus("NEW");
						  customerFile.setCreatedBy(UserIPaddress);
						  customerFile.setCreatedOn(new Date());
						  customerFile.setUpdatedBy(UserIPaddress);
						  customerFile.setUpdatedOn(new Date());
						  maxSequenceNumber = customerFileService.findMaximum(sessionCorpID);
						  if (maxSequenceNumber.get(0) == null) {
							  customerFile.setSequenceNumber(sessionCorpID + "1000001");
						  } else {
							  autoSequenceNumber = Long.parseLong((maxSequenceNumber.get(0).toString()).replace(sessionCorpID, "")) + 1;
							  sequenceNumber = sessionCorpID + autoSequenceNumber.toString();
							  customerFile.setSequenceNumber(sequenceNumber);
							} 
						  massage= customerFileService.saveCustomerFile(customerFile); 
						  if(massage.equals("")){
							  result="1";
							  description="customer file has been added successfully.";
						  }else {
							  result="0";
							  description="Invalid data.";
						  }
						  break;
					} else{
						result="0";
						description="Un-authorized access";
					}
					i++; 
				 } 
				
			} else{
				 result="0";
				 description="Un-authorized access";
				}
			}else{
				result="0";
				 description="Un-authorized access";
			}
			}catch(Exception ex){
				  result="0";
				  description="Invalid data.";
			}
			result=createResponseXml(result,description); 
			//byte[] result1 = new byte[1];
			//result1[0]=Byte.parseByte(result);
			byte[] resultArray = result.getBytes();
			return new ByteArrayInputStream(resultArray);
   }
   
   private String createResponseXml(String messagecode, String messageDescription){
       return "<result><message_code>"+messagecode+"</message_code><message_description>"+messageDescription+"</message_description></result>";
   }
   
public String getLead_from_add1() {
	return lead_from_add1;
}
public void setLead_from_add1(String lead_from_add1) {
	this.lead_from_add1 = lead_from_add1;
}
public String getLead_from_add2() {
	return lead_from_add2;
}
public void setLead_from_add2(String lead_from_add2) {
	this.lead_from_add2 = lead_from_add2;
}
public String getLead_from_city() {
	return lead_from_city;
}
public void setLead_from_city(String lead_from_city) {
	this.lead_from_city = lead_from_city;
}
public String getLead_from_state() {
	return lead_from_state;
}
public void setLead_from_state(String lead_from_state) {
	this.lead_from_state = lead_from_state;
}
public String getLead_name_first() {
	return lead_name_first;
}
public void setLead_name_first(String lead_name_first) {
	this.lead_name_first = lead_name_first;
}
public String getLead_name_last() {
	return lead_name_last;
}
public void setLead_name_last(String lead_name_last) {
	this.lead_name_last = lead_name_last;
}

public String getSessionCorpID() {
	return sessionCorpID;
}
public void setSessionCorpID(String sessionCorpID) {
	this.sessionCorpID = sessionCorpID;
}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public void setCustomerFileService(CustomerFileService customerFileService) {
	this.customerFileService = customerFileService;
}
public String getLead_from_country() {
	return lead_from_country;
}
public void setLead_from_country(String lead_from_country) {
	this.lead_from_country = lead_from_country;
}
public String getLead_from_postal() {
	return lead_from_postal;
}
public void setLead_from_postal(String lead_from_postal) {
	this.lead_from_postal = lead_from_postal;
}
public String getLead_to_add1() {
	return lead_to_add1;
}
public void setLead_to_add1(String lead_to_add1) {
	this.lead_to_add1 = lead_to_add1;
}
public String getLead_to_city() {
	return lead_to_city;
}
public void setLead_to_city(String lead_to_city) {
	this.lead_to_city = lead_to_city;
}
public String getLead_to_state() {
	return lead_to_state;
}
public void setLead_to_state(String lead_to_state) {
	this.lead_to_state = lead_to_state;
}
public String getLead_to_country() {
	return lead_to_country;
}
public void setLead_to_country(String lead_to_country) {
	this.lead_to_country = lead_to_country;
}
public String getLead_to_postal() {
	return lead_to_postal;
}
public void setLead_to_postal(String lead_to_postal) {
	this.lead_to_postal = lead_to_postal;
}
public String getLead_email() {
	return lead_email;
}
public void setLead_email(String lead_email) {
	this.lead_email = lead_email;
}
public String getMassage() {
	return massage;
}
public void setMassage(String massage) {
	this.massage = massage;
}
public String getLead_altphone1() {
	return lead_altphone1;
}
public void setLead_altphone1(String lead_altphone1) {
	this.lead_altphone1 = lead_altphone1;
}
public String getLead_altphone2() {
	return lead_altphone2;
}
public void setLead_altphone2(String lead_altphone2) {
	this.lead_altphone2 = lead_altphone2;
}
public String getLead_altphone3() {
	return lead_altphone3;
}
public void setLead_altphone3(String lead_altphone3) {
	this.lead_altphone3 = lead_altphone3;
}
public String getLead_busphone1() {
	return lead_busphone1;
}
public void setLead_busphone1(String lead_busphone1) {
	this.lead_busphone1 = lead_busphone1;
}
public String getLead_busphone2() {
	return lead_busphone2;
}
public void setLead_busphone2(String lead_busphone2) {
	this.lead_busphone2 = lead_busphone2;
}
public String getLead_busphone3() {
	return lead_busphone3;
}
public void setLead_busphone3(String lead_busphone3) {
	this.lead_busphone3 = lead_busphone3;
}
public String getLead_phone1() {
	return lead_phone1;
}
public void setLead_phone1(String lead_phone1) {
	this.lead_phone1 = lead_phone1;
}
public String getLead_phone2() {
	return lead_phone2;
}
public void setLead_phone2(String lead_phone2) {
	this.lead_phone2 = lead_phone2;
}
public String getLead_phone3() {
	return lead_phone3;
}
public void setLead_phone3(String lead_phone3) {
	this.lead_phone3 = lead_phone3;
}
public String getLead_to_add2() {
	return lead_to_add2;
}
public void setLead_to_add2(String lead_to_add2) {
	this.lead_to_add2 = lead_to_add2;
}

}
