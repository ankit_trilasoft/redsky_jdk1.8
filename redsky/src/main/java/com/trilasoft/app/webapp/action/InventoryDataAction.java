package com.trilasoft.app.webapp.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap; 

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.TrackingStatusManager;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import com.trilasoft.app.dao.hibernate.InventoryDataDaoHibernate.InvDTO;
import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.AdAddressesDetails;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CountryDeviation;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.EmailSetup;
import com.trilasoft.app.model.IntegrationLogInfo;
import com.trilasoft.app.model.InventoryData;
import com.trilasoft.app.model.InventoryPacking;
import com.trilasoft.app.model.InventoryPath;
import com.trilasoft.app.model.ItemsJbkEquip;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.Vehicle;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.AccessInfoManager;
import com.trilasoft.app.service.AdAddressesDetailsManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.IntegrationLogInfoManager;
import com.trilasoft.app.service.InventoryArticleManager;
import com.trilasoft.app.service.InventoryDataManager;
import com.trilasoft.app.service.InventoryLocationManager;
import com.trilasoft.app.service.InventoryPackingManager;
import com.trilasoft.app.service.InventoryPathManager;
import com.trilasoft.app.service.ItemsJbkEquipManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.RoomManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.VehicleManager;
import com.trilasoft.app.service.WorkTicketInventoryDataManager;
import com.trilasoft.app.service.WorkTicketManager;

public class InventoryDataAction extends BaseAction implements Preparable{
private InventoryDataManager inventoryDataManager;
private InventoryData inventoryData;
private InventoryPath inventoryPath;
private CustomerFile customerFile;
private ServiceOrder serviceOrder;
private WorkTicket workTicket;
private WorkTicketManager workTicketManager;
private CustomerFileManager  customerFileManager;
private ServiceOrderManager serviceOrderManager;
private RefMasterManager refMasterManager;
private AccessInfoManager accessInfoManager;
private ItemsJbkEquipManager itemsJbkEquipManager;
private InventoryPackingManager  inventoryPackingManager;
private UserManager userManager;
private Long id; 
private Long cid;
private Long sid;
private Long aid;
private User user;
private String sessionCorpID;
private List articlesList;
private Map<String, String> mode; 
private Map<String, String> relocationServices;
private List upLoadedImages = new ArrayList();
private List upLoadedImages2 = new ArrayList();
private InventoryPathManager inventoryPathManager;
private InventoryArticleManager inventoryArticleManager;
private VehicleManager vehicleManager;
private String idNumber;
private File attachment; 
private String attachmentFileName; 
private String attachmentContentType; 
private String hitflag;
private String  SOflag;
private AccessInfo accessInfo;
private  SortedMap modeShipMap = new TreeMap();  
private String shipAndModeList;
private String modeWithOutSO="";
private String modeWithOneSO="";
private String modeSORelation;
private Map elevatorType;
private Map parkingType;
private Map residenseType;
private Map<String, String> roomType;
private String message="";
private String minShip;
private String countShip;
private String shipSize;
private String accessInfoType;
private List workTicketList  = new ArrayList();
private List ticketList = new ArrayList();
private List artileList;
private String inventoryTransferflag;
private String Quote;
private InventoryPacking inventoryPacking;
private List inventoryPackingList;
private String article;
private String room;
private List inventoryDataListFromPieceId;
private String pieceID;
private String materialTransferFlag;
private String chkSave;
private String code;
private String crewMailID;
private List inventoryPackingitems;
private Map conditionType;
public String results;
public String mailId;
public Long wrkTckId;
private Billing billing;
private BillingManager billingManager; 
private List itemInsuranceList;
private List invSumList;
private List vehicleList;
private String checkData;
private Vehicle vehicle; 
private String checkDataVehicle;
private String commodity;
private String newField;
private String fieldValue;
private Map<String,String> vehicleTypeList;
private String invListflag;
private BigDecimal quatSum;
private BigDecimal volSum;
private BigDecimal totSum;
private List soList;
private List resultSOList;
private List preConditionList;
private InventoryLocationManager inventoryLocationManager;
private RoomManager roomManager;
private WorkTicketInventoryDataManager workTicketInventoryDataManager;
private TrackingStatus trackingStatus; 
private MiscellaneousManager miscellaneousManager;
private String vehicleStatus;

private TrackingStatusManager trackingStatusManager;
private String shipno;
private String page;
private List inventoryRoomsList;
private List ticketInvData;
private String fieldName;
public String idURL;
private SystemDefault systemDefault;
private SystemDefaultManager systemDefaultManager;
private ErrorLogManager errorLogManager ;
private CompanyManager companyManager;
private Company company;
private String usertype;
private List serviceOrders;
private AdAddressesDetails adAddressesDetails;
private AdAddressesDetailsManager adAddressesDetailsManager;
private EmailSetupManager emailSetupManager;
private String oiJobList;
private String dashBoardHideJobsList;
private String  accCorpID;

static final Logger logger = Logger.getLogger(InventoryDataAction.class);
public String getCountShip() {
	return countShip;
}
public void setCountShip(String countShip) {
	this.countShip = countShip;
}
public String getMinShip() {
	return minShip;
}
public void setMinShip(String minShip) {
	this.minShip = minShip;
}
public String getShipSize() {
	return shipSize;
}
public void setShipSize(String shipSize) {
	this.shipSize = shipSize;
}
public String getComboList(String corpID){	
	mode = refMasterManager.findByParameter(corpID, "MODE");
	roomType=refMasterManager.findByParameter(corpID, "ROOMTYPE");
	relocationServices=refMasterManager.findByRelocationServices(corpID, "RELOCATIONSERVICES");
	vehicleTypeList = refMasterManager.findByParameter(corpID, "VEHICLETYPE");
	return SUCCESS;
}
public String refDataForList(){
	elevatorType= refMasterManager.findByParameter(sessionCorpID, "ELEVATORTYPE");
	parkingType=refMasterManager.findByParameter(sessionCorpID, "PARKINGTYPE");
	residenseType=refMasterManager.findByParameter(sessionCorpID, "RESIDENCETYPE");
	return SUCCESS;
}
public InventoryDataAction() {
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	User user = (User) auth.getPrincipal(); 
	this.sessionCorpID = user.getCorpID();
}
public void prepare() throws Exception {
	accCorpID=sessionCorpID;
	User usr = userManager.getUserByUsername(getRequest().getRemoteUser());
	usertype = usr.getUserType();
	company=companyManager.findByCorpID(sessionCorpID).get(0);
     if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }
     if (company != null && company.getDashBoardHideJobs() != null) {
			dashBoardHideJobsList = company.getDashBoardHideJobs();	 
		}
     
}
public String articleAutocomplete(){
	if(!inventoryData.getAtricle().equals("")){
		artileList=inventoryArticleManager.getArticleAutoComplete(sessionCorpID,inventoryData.getAtricle());	
	}
	return SUCCESS;	
}
public String inventoryDetails(){
	roomType=refMasterManager.findByParameter(sessionCorpID, "ROOMTYPE");
	inventoryPackingList=inventoryPackingManager.searchCriteria(sid,"","",sessionCorpID);
	return SUCCESS;	
}
public String editInventoryDetails(){
	roomType=refMasterManager.findByParameter(sessionCorpID, "ROOMTYPE");
	conditionType= refMasterManager.findByParameter(sessionCorpID, "ItemCondition");
	inventoryPacking=inventoryPackingManager.get(id);
	return SUCCESS;	
}
public String inventoryDataListfromPieceId(){
	inventoryDataListFromPieceId=inventoryPackingManager.seachInventoryThroughPieceId(sid,pieceID,sessionCorpID);
	inventoryPackingList= inventoryPackingManager.findArticleByPieceId(sid,pieceID,sessionCorpID);
	roomType=refMasterManager.findByParameter(sessionCorpID, "ROOMTYPE");
	return SUCCESS;	
}
public String readXmlInventory(){
	fetchXmlforInventoryData();
	inventoryDetails();
	hitflag="1";
	return SUCCESS;
}

public String sendCrewMailMethod(){
	return SUCCESS;
}

public String searchInventoryDetails(){
	roomType=refMasterManager.findByParameter(sessionCorpID, "ROOMTYPE");
	inventoryPackingList=inventoryPackingManager.searchCriteria(sid,room,article,sessionCorpID);
	return SUCCESS;	
}
public String list(){
	refDataForList();
	inventoryTransferflag=inventoryDataManager.getSOTransferStatus("A",cid,sessionCorpID);
	List list = new ArrayList<AccessInfo>();
	articlesList=inventoryDataManager.getListByType("A",cid);
 	invSumList=inventoryDataManager.getSumOfListByType("M",cid,null);
 	if(articlesList!=null && !articlesList.isEmpty()){
 		invListflag="1";
 		
 	}
 	if(invSumList!=null && !invSumList.isEmpty() && invSumList.get(0)!=null){
 		//invListflag="1";
 		InvDTO invdata=(InvDTO)invSumList.get(0);
 		if(invdata.getQuantitySum()!=null){
 		 quatSum=new BigDecimal(invdata.getQuantitySum());
 		}else{
 			quatSum=new BigDecimal("0.00");;	
 		}
 		if(invdata.getVolSum()!=null){
 		 volSum=new BigDecimal(invdata.getVolSum());
 		}else{
 			volSum=new BigDecimal("0.00");
 		}
 		if(invdata.getTotalSum()!=null){
 		 totSum=new BigDecimal(invdata.getTotalSum());
 		}else{
 			totSum=new BigDecimal("0.00");
 		}
 		
 	}
	customerFile=customerFileManager.get(cid);
	getRequest().setAttribute("soLastName", customerFile.getLastName());
        list=accessInfoManager.getAccessInfo(cid);
		if(!list.isEmpty() && list.get(0)!=""){
			accessInfo=(AccessInfo)list.get(0);
			accessInfo.setCreatedOn(accessInfo.getCreatedOn());
			accessInfo.setCreatedBy(accessInfo.getCreatedBy());
			List signImagelist=inventoryPathManager.getListByAccessInfoId(cid,"sign");
			  if(signImagelist!=null && !signImagelist.isEmpty() && signImagelist.get(0)!=null){
				inventoryPath=(InventoryPath) signImagelist.get(0);
			  }
		}else{
			accessInfo= new AccessInfo();
			accessInfo.setCreatedOn(new Date());
			accessInfo.setUpdatedOn(new Date());
			accessInfo.setCreatedBy(getRequest().getRemoteUser());
			accessInfo.setUpdatedBy(getRequest().getRemoteUser());
			
		}
		if(message!=null){
			if(!message.equals("done") && !message.equals("")&& !message.equals("Access Info has been updated")&& !message.equals("Access Info has been added")){
				String message=this.message;
				errorMessage(getText(message));
			}else{
				if(message.equals("done")){
					if(Quote.equalsIgnoreCase("y")){
						message="Survey Data Transfered To Quotes";
					}else{
				     message="Survey Data Transfered To S/O";
					}
				saveMessage(getText(message));
				}
			}
		}
		SOflag="CF";
	return SUCCESS;	
}
public String saveInventoryPacking(){
	inventoryPacking.setUpdatedBy(getRequest().getRemoteUser());
	inventoryPacking.setUpdatedOn(new Date());
	inventoryPacking.setCorpID(sessionCorpID);
	inventoryPacking =inventoryPackingManager.save(inventoryPacking);
	message="Inventory data updated";
    saveMessage(getText(message));
    hitflag="1";
	return SUCCESS;
}
public String saveAccessInfo(){
	/*if(accessInfo.getId()!=null){
		accessInfo=accessInfoManager.save(accessInfo);
		message="Access Info has been updated";
		saveMessage(getText(message));
	}else{
		accessInfo=accessInfoManager.save(accessInfo);
		message="Access Info has been added";
		saveMessage(getText(message));
	}*/
	List list = companyManager.findByCorpID(sessionCorpID);
	if(list!=null && !list.isEmpty() && list.get(0)!=null){
		company = (Company) list.get(0);
	}
	systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
	boolean isNew = false;
	CustomerFile customerFile = customerFileManager.get(cid);
	if(aid!=null){
		accessInfo.setNetworkSynchedId(null);
		accessInfo.setCreatedOn(accessInfo.getCreatedOn());
		accessInfo.setCreatedBy(accessInfo.getCreatedBy());
		accessInfo.setUpdatedOn(new Date());
		accessInfo.setUpdatedBy(getRequest().getRemoteUser());
		accessInfo=accessInfoManager.save(accessInfo);
		message="Access Info has been updated";
		saveMessage(getText(message));
	}else{
		isNew = true;
		accessInfo.setNetworkSynchedId(null);
		accessInfo.setCreatedOn(new Date());
		accessInfo.setUpdatedOn(new Date());
		accessInfo.setCreatedBy(getRequest().getRemoteUser());
		accessInfo.setUpdatedBy(getRequest().getRemoteUser());
		accessInfo=accessInfoManager.save(accessInfo);
		message="Access Info has been added";
		saveMessage(getText(message));
	}
	if(customerFile.getIsNetworkRecord()!=null && customerFile.getIsNetworkRecord() && ((customerFile.getContractType()!=null && (customerFile.getContractType().equals("CMM") ||customerFile.getContractType().equals("DMM"))) || company.getSurveyLinking())){
		List linkedSequenceNumber=findLinkerSequenceNumber(customerFile);
		Set set = new HashSet(linkedSequenceNumber);
		List linkedSeqNum = new ArrayList(set);
		List<Object> records=findRecords(linkedSeqNum,customerFile);
		accessInfo.setNetworkSynchedId(accessInfo.getId());
		accessInfo.setCreatedOn(accessInfo.getCreatedOn());
		accessInfo.setCreatedBy(accessInfo.getCreatedBy());
		accessInfo=accessInfoManager.save(accessInfo);
		synchornizeAccessInfoCF(records,accessInfo,isNew);
	}
	 //synchornizeAccessInfo(serviceOrderRecords,accessInfo,isNew,serviceOrder);
	list();
	return SUCCESS;	
}


public void synchornizeInventoryData(List<Object> records,	InventoryData inventoryDataOld, ServiceOrder serviceOrder,TrackingStatus trackingStatus, boolean isNew,String flag) {
	
	Long invId= new Long(0);
	Date createdOn = new Date();
	String cratedBy = "";
	Iterator  it=records.iterator();
	
	
	
	if(flag.equals("CF")){
		while(it.hasNext()){
			String surveryLink ="No";
			CustomerFile customerFileToRecods=(CustomerFile)it.next();
			if(company.getSurveyLinking()!=null && company.getSurveyLinking()){
				surveryLink ="Yes";
			}
			if(surveryLink.equalsIgnoreCase("NO") && customerFileToRecods.getContractType()!=null && (customerFileToRecods.getContractType().equals("CMM") || customerFileToRecods.getContractType().equals("DMM"))){
				surveryLink ="Yes";
			}
			if(surveryLink.equalsIgnoreCase("Yes")){
			InventoryData inventoryDataNew = new InventoryData();
			if(!isNew){
				inventoryDataNew =  (InventoryData) inventoryDataManager.getLinkedInventoryData(inventoryDataOld.getId(),customerFileToRecods.getId(),null);
				invId = inventoryDataNew.getId();
				cratedBy = inventoryDataNew.getCreatedBy();
				createdOn = inventoryDataNew.getCreatedOn();
			}
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(inventoryDataNew, inventoryDataOld);
				 if(isNew){
					 inventoryDataNew.setId(null);
					 inventoryDataNew.setCreatedBy("Networking");
					 inventoryDataNew.setCreatedOn(new Date());
				 }else{
					 inventoryDataNew.setId(invId);
					 inventoryDataNew.setCreatedBy(cratedBy);
					 inventoryDataNew.setCreatedOn(createdOn);
				 }
				 inventoryDataNew.setCustomerFileID(customerFileToRecods.getId());
				
				 inventoryDataNew.setCorpID(customerFileToRecods.getCorpID());
				 inventoryDataNew.setServiceOrderID(null); 
				 inventoryDataNew.setUpdatedBy("Networking");
				 inventoryDataNew.setUpdatedOn(new Date());
				 String unitW = systemDefaultManager.getCorpIdUnit(inventoryDataNew.getCorpID(),"weightUnit");
				 if(!unitW.equals("") && !unitW.equals(systemDefault.getWeightUnit())){
					 if(unitW.equals("Kgs")){
						 double temp = (Float.parseFloat(inventoryDataOld.getWeight())) * 0.4536;
						 temp =  temp*10000;
						 int tempIntValue = (int) temp;
						 temp = tempIntValue;
						 temp = temp/10000;
				    		//temp= Math.round(temp*10000)/10000;
				    		inventoryDataNew.setWeight(""+temp);
				    		String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
				    		inventoryDataNew.setTotalWeight(total);
					 }else{
						 double temp = (Float.parseFloat(inventoryDataOld.getWeight())) * 2.2046;
						 temp =  temp*10000;
						 int tempIntValue = (int) temp;
						 temp = tempIntValue;
						 temp = temp/10000;
				    		inventoryDataNew.setWeight(""+temp);
				    		String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
				    		inventoryDataNew.setTotalWeight(total);
					 }
				 }
				 String unitVol = systemDefaultManager.getCorpIdUnit(inventoryDataNew.getCorpID(),"volumeUnit");
				 if(!unitVol.equals("") &&!unitVol.equals(systemDefault.getVolumeUnit())){
					 if(!unitVol.equalsIgnoreCase("Cft")){
							double temp = (Float.parseFloat(inventoryDataOld.getCft())) * 0.0283;
							 temp =  temp*10000;
							 int tempIntValue = (int) temp;
							 temp = tempIntValue;
							 temp = temp/10000;
				    		inventoryDataNew.setCft(""+temp);
				    		String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
				    		inventoryDataNew.setTotal(total);
						}else{
							double temp = (Float.parseFloat(inventoryDataOld.getCft())) * 35.3147;
							 temp =  temp*10000;
							 int tempIntValue = (int) temp;
							 temp = tempIntValue;
							 temp = temp/10000;
				    		inventoryDataNew.setCft(""+temp);
				    		String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
				    		inventoryDataNew.setTotal(total);
						}
				 }
				 inventoryDataNew.setNetworkSynchedId(inventoryDataOld.getId());
				 inventoryDataNew= inventoryDataManager.save(inventoryDataNew);
				 List oldInventoryPath = inventoryPathManager.getListByInventoryId(inventoryDataOld.getId());
					if(oldInventoryPath!=null && !oldInventoryPath.isEmpty()){
						Iterator itrInventoryPathId = oldInventoryPath.iterator();
						while(itrInventoryPathId.hasNext()){
							Long tempInvPathId = Long.parseLong(itrInventoryPathId.next().toString());
							InventoryPath oldInvPath = inventoryPathManager.get(tempInvPathId);
							InventoryPath newInvPath = new InventoryPath();
							try{
								BeanUtilsBean beanUtilsInvPath = BeanUtilsBean.getInstance();
								beanUtilsInvPath.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
								beanUtilsInvPath.copyProperties(newInvPath, oldInvPath);
								newInvPath.setId(null);
								newInvPath.setInventoryDataId(inventoryDataNew.getId());
								newInvPath.setAccessInfoId(null);
								inventoryPathManager.save(newInvPath);
							}catch(Exception ex){
								ex.printStackTrace();
							}
						}
					}
			}catch(Exception ex){
				String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
		    	  String logMethod =   ex.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , ex.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 ex.printStackTrace();
			}
		}
		}
		
	}else if(flag.equals("SO")){

		while(it.hasNext()){
			String surveryLink ="No";
			ServiceOrder so=(ServiceOrder)it.next();
			TrackingStatus trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(so.getId());
			//if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup() && serviceOrder.getShipNumber()!=null && so.getShipNumber()!=null && (!(serviceOrder.getShipNumber().toString().trim().equalsIgnoreCase(so.getShipNumber().toString().trim()))) ){
			CustomerFile customerFileToRecods=so.getCustomerFile();
			if(company.getSurveyLinking()!=null && company.getSurveyLinking()){
				surveryLink ="Yes";
			}
			if(surveryLink.equalsIgnoreCase("NO") && trackingStatusToRecods.getSoNetworkGroup()){
				surveryLink ="Yes";
			}
			if(surveryLink.equalsIgnoreCase("Yes")){
			InventoryData inventoryDataNew = new InventoryData();
			if(!isNew){
			/*	if(inventoryDataOld.getServiceOrderID()!=null){
					inventoryDataNew =  (InventoryData) inventoryDataManager.getLinkedInventoryData(inventoryDataOld.getId(),customerFileToRecods.getId(),so.getId());
				}else{*/
					inventoryDataNew =  (InventoryData) inventoryDataManager.getLinkedInventoryData(inventoryDataOld.getId(),customerFileToRecods.getId(),null);
				//}
				invId = inventoryDataNew.getId();
				cratedBy = inventoryDataNew.getCreatedBy();
				createdOn = inventoryDataNew.getCreatedOn();
			}
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(inventoryDataNew, inventoryDataOld);
				 if(isNew){
					 inventoryDataNew.setId(null);
					 inventoryDataNew.setCreatedBy("Networking");
					 inventoryDataNew.setCreatedOn(new Date());
				 }else{
					 inventoryDataNew.setId(invId);
					 inventoryDataNew.setCreatedBy(cratedBy);
					 inventoryDataNew.setCreatedOn(createdOn);
				 }
				 inventoryDataNew.setCustomerFileID(customerFileToRecods.getId());
				
				 inventoryDataNew.setCorpID(customerFileToRecods.getCorpID());
				 inventoryDataNew.setServiceOrderID(so.getId()); 
				 inventoryDataNew.setUpdatedBy("Networking");
				 inventoryDataNew.setUpdatedOn(new Date());
				 String unitW = systemDefaultManager.getCorpIdUnit(inventoryDataNew.getCorpID(),"weightUnit");
				 if(!unitW.equals("") && !unitW.equals(systemDefault.getWeightUnit())){
					 if(unitW.equals("Kgs")){
						 double temp = (Float.parseFloat(inventoryDataOld.getWeight())) * 0.4536;
						 temp =  temp*10000;
						 int tempIntValue = (int) temp;
						 temp = tempIntValue;
						 temp = temp/10000;
				    		inventoryDataNew.setWeight(""+temp);
				    		String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
				    		inventoryDataNew.setTotalWeight(total);
					 }else{
						 double temp = (Float.parseFloat(inventoryDataOld.getWeight())) * 2.2046;
						 temp =  temp*10000;
						 int tempIntValue = (int) temp;
						 temp = tempIntValue;
						 temp = temp/10000;
				    		inventoryDataNew.setWeight(""+temp);
				    		String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
				    		inventoryDataNew.setTotalWeight(total);
					 }
				 }
				 String unitVol = systemDefaultManager.getCorpIdUnit(inventoryDataNew.getCorpID(),"volumeUnit");
				 if(!unitVol.equals("") &&!unitVol.equals(systemDefault.getVolumeUnit())){
					 if(!unitVol.equalsIgnoreCase("Cft")){
							double temp = (Float.parseFloat(inventoryDataOld.getCft())) * 0.0283;
							 temp =  temp*10000;
							 int tempIntValue = (int) temp;
							 temp = tempIntValue;
							 temp = temp/10000;
				    		inventoryDataNew.setCft(""+temp);
				    		String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
				    		inventoryDataNew.setTotal(total);
						}else{
							double temp = (Float.parseFloat(inventoryDataOld.getCft())) * 35.3147;
							 temp =  temp*10000;
							 int tempIntValue = (int) temp;
							 temp = tempIntValue;
							 temp = temp/10000;
				    		inventoryDataNew.setCft(""+temp);
				    		String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
				    		inventoryDataNew.setTotal(total);
						}
				 }
				 inventoryDataNew.setNetworkSynchedId(inventoryDataOld.getId());
				 inventoryDataNew= inventoryDataManager.save(inventoryDataNew);
				 List oldInventoryPath = inventoryPathManager.getListByInventoryId(inventoryDataOld.getId());
					if(oldInventoryPath!=null && !oldInventoryPath.isEmpty()){
						Iterator itrInventoryPathId = oldInventoryPath.iterator();
						while(itrInventoryPathId.hasNext()){
							Long tempInvPathId = Long.parseLong(itrInventoryPathId.next().toString());
							InventoryPath oldInvPath = inventoryPathManager.get(tempInvPathId);
							InventoryPath newInvPath = new InventoryPath();
							try{
								BeanUtilsBean beanUtilsInvPath = BeanUtilsBean.getInstance();
								beanUtilsInvPath.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
								beanUtilsInvPath.copyProperties(newInvPath, oldInvPath);
								newInvPath.setId(null);
								newInvPath.setInventoryDataId(inventoryDataNew.getId());
								newInvPath.setAccessInfoId(null);
								inventoryPathManager.save(newInvPath);
							}catch(Exception ex){
								ex.printStackTrace();
							}
						}
					}
			}catch(Exception ex){
				String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
		    	  String logMethod =   ex.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , ex.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 ex.printStackTrace();
			}
		}
	
		}
	}

}


private void synchornizeVehicle(List<Object> serviceOrderRecords,Vehicle vehicle, boolean isNew ,ServiceOrder serviceOrderold,TrackingStatus trackingStatus) {
	
	Iterator  it=serviceOrderRecords.iterator();
	while(it.hasNext()){
		ServiceOrder serviceOrder=(ServiceOrder)it.next();
		TrackingStatus trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrder.getId());
		try{
			if((!(serviceOrder.getShipNumber().equals(serviceOrderold.getShipNumber())))){	
			if(isNew){
				Vehicle vehicleNew= new Vehicle();
				BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtilsBean.copyProperties(vehicleNew, vehicle);
				
				vehicleNew.setId(null);
				vehicleNew.setCorpID(serviceOrder.getCorpID());
				vehicleNew.setShipNumber(serviceOrder.getShipNumber());
				vehicleNew.setSequenceNumber(serviceOrder.getSequenceNumber());
				vehicleNew.setServiceOrder(serviceOrder);
				vehicleNew.setServiceOrderId(serviceOrder.getId());
				vehicleNew.setCreatedBy("Networking");
				vehicleNew.setUpdatedBy("Networking");
				vehicleNew.setCreatedOn(new Date());
				vehicleNew.setUpdatedOn(new Date()); 
				// added to fixed issue of merge functionality in 2 step linking
				try{
				 List maxIdNumber = vehicleManager.findMaximumIdNumber(serviceOrder.getShipNumber());
			       if ( maxIdNumber.get(0) == null ) 
			        {          
			      	 idNumber = "01";
		         }else {
			          	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
			        	if((autoId.toString()).length() == 1) 
			        		{
			        			idNumber = "0"+(autoId.toString());
			        		}else {
			        			 idNumber=autoId.toString();
			        			        
			        			}
			        	} 
			    }catch(Exception e){
					e.printStackTrace();
				}
			    vehicleNew.setIdNumber(idNumber);
				vehicleNew = vehicleManager.save(vehicleNew);
				
				 if(vehicleNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
					 vehicleNew.setUgwIntId(vehicleNew.getId().toString());
					 vehicleNew= vehicleManager.save(vehicleNew);
					}
				
				 
			}else{
				Vehicle vehicleTo= vehicleManager.getForOtherCorpid(vehicleManager.findRemoteVehicle(vehicle.getIdNumber(),serviceOrder.getId()));  
    			Long id=vehicleTo.getId();
    			String createdBy=vehicleTo.getCreatedBy();
    			Date createdOn=vehicleTo.getCreatedOn();
    			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtilsBean.copyProperties(vehicleTo, vehicle);
				
				vehicleTo.setId(id);
				vehicleTo.setCreatedBy(createdBy);
				vehicleTo.setCreatedOn(createdOn);
				vehicleTo.setCorpID(serviceOrder.getCorpID());
				vehicleTo.setShipNumber(serviceOrder.getShipNumber());
				vehicleTo.setSequenceNumber(serviceOrder.getSequenceNumber());
				vehicleTo.setServiceOrder(serviceOrder);
				vehicleTo.setServiceOrderId(serviceOrder.getId());
				vehicleTo.setUpdatedBy(vehicle.getCorpID()+":"+getRequest().getRemoteUser());
				vehicleTo.setUpdatedOn(new Date()); 
				/*// added to fixed issue of merge functionality in 2 step linking
				try{
				 List maxIdNumber = vehicleManager.findMaximumIdNumber(serviceOrder.getShipNumber());
			       if ( maxIdNumber.get(0) == null ) 
			        {          
			      	 idNumber = "01";
		         }else {
			          	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
			        	if((autoId.toString()).length() == 1) 
			        		{
			        			idNumber = "0"+(autoId.toString());
			        		}else {
			        			 idNumber=autoId.toString();
			        			        
			        			}
			        	} 
			    }catch(Exception e){
					System.out.println("Error in id number");
				}
			    vehicleTo.setIdNumber(idNumber);*/
				vehicleTo =vehicleManager.save(vehicleTo);
				 if(vehicleTo.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
					 vehicleTo.setUgwIntId(vehicleTo.getId().toString());
					 vehicleTo= vehicleManager.save(vehicleTo);
				}
				
			}
			}
		}catch(Exception ex){
			 ex.printStackTrace();
		}
	}
	
	}


public void synchornizeAccessInfoCF(List<Object> records,	AccessInfo accessInfoOld, boolean isNew) {
	Long oldId = new Long(0);
	String createdBy="";
	Iterator  it=records.iterator();
	while(it.hasNext()){
		String surveryLink ="No";
		CustomerFile customerFileToRecods=(CustomerFile)it.next();
		if(company.getSurveyLinking()!=null && company.getSurveyLinking()){
			surveryLink ="Yes";
		}
		if(surveryLink.equalsIgnoreCase("NO") && customerFileToRecods.getContractType()!=null && (customerFileToRecods.getContractType().equals("CMM") || customerFileToRecods.getContractType().equals("DMM"))){
			surveryLink ="Yes";
		}
		if(surveryLink.equalsIgnoreCase("Yes")){
		AccessInfo accessInfoNew = new AccessInfo();
		if(!isNew){
			accessInfoNew =  (AccessInfo) accessInfoManager.getAccessInfo(customerFileToRecods.getId()).get(0);
			oldId = accessInfoNew.getId();
		}
		try{
			 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
			 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			 beanUtilsBean2.copyProperties(accessInfoNew, accessInfoOld);
			 if(isNew){
				 accessInfoNew.setId(null); 
			 }else{
				 accessInfoNew.setId(oldId);
			 }
			 accessInfoNew.setNetworkSynchedId(accessInfoOld.getId());
			 accessInfoNew.setCorpID(customerFileToRecods.getCorpID());
			 accessInfoNew.setCustomerFileId(customerFileToRecods.getId());
			 accessInfoNew.setUpdatedBy(getRequest().getRemoteUser());
			 accessInfoNew.setUpdatedOn(new Date());
			 accessInfoNew= accessInfoManager.save(accessInfoNew);
			// Copying accessinfo images
				linkSyncInventpryPath(accessInfoOld, isNew, accessInfoNew);
		}catch(Exception ex){
			String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
	    	  String logMethod =   ex.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , ex.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 ex.printStackTrace();
		}
	}
	}
}
public List<Object> findRecords(List linkedSequenceNumber,CustomerFile customerFileLocal) {
	List<Object> recordList= new ArrayList();
	Iterator it =linkedSequenceNumber.iterator();
	while(it.hasNext()){
		String sequenceNumber= it.next().toString();
		CustomerFile customerFileRemote=customerFileManager.getForOtherCorpid(Long.parseLong(customerFileManager.findRemoteCustomerFile(sequenceNumber).toString()));
		recordList.add(customerFileRemote);
	}
	return recordList;
}

public List findLinkerSequenceNumber(CustomerFile customerFile) {
	List linkedSequnceNumberList= new ArrayList();
	if(customerFile.getBookingAgentSequenceNumber()==null || customerFile.getBookingAgentSequenceNumber().equals("") ){
		linkedSequnceNumberList=customerFileManager.getLinkedSequenceNumber(customerFile.getSequenceNumber(), "Primary");
	}else{
		linkedSequnceNumberList=customerFileManager.getLinkedSequenceNumber(customerFile.getBookingAgentSequenceNumber(), "Secondary");
	}
	return linkedSequnceNumberList;
}

public String findInventoryDetailsMethods(){
	systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
	soList = new ArrayList();
	resultSOList = new ArrayList();
	List <ServiceOrder> tempSoList = inventoryDataManager.getSOListbyCF(cid);
	for (ServiceOrder serviceOrder : tempSoList) {
		soList.add(serviceOrder.getShipNumber());
	}
	Collections.sort(soList);
	resultSOList.addAll(soList);
	customerFile=customerFileManager.get(cid);
	getRequest().setAttribute("soLastName", customerFile.getLastName());
    
	return SUCCESS;	
}

public String saveAccessInfoForSo(){
	/*if(accessInfo.getId()!=null){
		accessInfo=accessInfoManager.save(accessInfo);
		message="Access Info has been updated";
		saveMessage(getText(message));
	}else{
		accessInfo=accessInfoManager.save(accessInfo);
		message="Access Info has been added";
		saveMessage(getText(message));
	}*/
	List list = companyManager.findByCorpID(sessionCorpID);
	if(list!=null && !list.isEmpty() && list.get(0)!=null){
		company = (Company) list.get(0);
	}
	systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
	boolean isNew = false;
	ServiceOrder soObj = serviceOrderManager.get(sid);
	TrackingStatus ts = trackingStatusManager.get(sid);
	if(aid!=null){
		accessInfo.setNetworkSynchedId(null);
		accessInfo.setCreatedOn(accessInfo.getCreatedOn());
		accessInfo.setCreatedBy(accessInfo.getCreatedBy());
		accessInfo.setUpdatedOn(new Date());
		accessInfo.setUpdatedBy(getRequest().getRemoteUser());
		accessInfo=accessInfoManager.save(accessInfo);
		message="Access Info has been updated";
		saveMessage(getText(message));
	}else{
		isNew = true;
		accessInfo.setCreatedOn(new Date());
		accessInfo.setUpdatedOn(new Date());
		accessInfo.setCreatedBy(getRequest().getRemoteUser());
		accessInfo.setUpdatedBy(getRequest().getRemoteUser());
		accessInfo.setNetworkSynchedId(null);
		accessInfo=accessInfoManager.save(accessInfo);
		message="Access Info has been added";
		saveMessage(getText(message));
	}
	if(soObj.getIsNetworkRecord() && (ts.getSoNetworkGroup() || company.getSurveyLinking())){
		List linkedShipNumber=findLinkerShipNumber(soObj);
		List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,soObj);
		accessInfo.setNetworkSynchedId(accessInfo.getId());
		accessInfo.setCreatedOn(accessInfo.getCreatedOn());
		accessInfo.setCreatedBy(accessInfo.getCreatedBy());
		accessInfo=accessInfoManager.save(accessInfo);
		synchornizeAccessInfoSO(serviceOrderRecords,accessInfo,isNew);
	}
	inventorySOList();
	return SUCCESS;	
}


public void synchornizeAccessInfoSO(List<Object> serviceOrderRecords,AccessInfo accessInfoOld, boolean isNew) {
	Long accid= new Long(0);
	
	Iterator  it=serviceOrderRecords.iterator();
	while(it.hasNext()){
		String surveryLink ="No";
		ServiceOrder so=(ServiceOrder)it.next();
		TrackingStatus ts = trackingStatusManager.getForOtherCorpid(so.getId());
		if(company.getSurveyLinking()!=null && company.getSurveyLinking()){
			surveryLink ="Yes";
		}
		if(surveryLink.equalsIgnoreCase("NO") && ts.getSoNetworkGroup()){
			surveryLink ="Yes";
		}
		if(surveryLink.equalsIgnoreCase("Yes")){
		AccessInfo accessInfoNew = new AccessInfo();
		if(!isNew){
			accessInfoNew =  (AccessInfo) accessInfoManager.getAccessInfoForSO(so.getId()).get(0);
			accid  = accessInfoNew.getId();
		}
		try{
			 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
			 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			 beanUtilsBean2.copyProperties(accessInfoNew, accessInfoOld);
			 if(isNew){
				 accessInfoNew.setId(null);
			 }else{
				 accessInfoNew.setId(accid);
			 }
			 accessInfoNew.setCorpID(so.getCorpID());
			 accessInfoNew.setNetworkSynchedId(accessInfoOld.getId());
			 accessInfoNew.setServiceOrderId(so.getId());
			 accessInfoNew.setUpdatedBy(getRequest().getRemoteUser());
			 accessInfoNew.setUpdatedOn(new Date());
			 accessInfoNew= accessInfoManager.save(accessInfoNew);
			// Copying accessinfo images
				linkSyncInventpryPath(accessInfoOld, isNew, accessInfoNew);
		}catch(Exception ex){
			String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
	    	  String logMethod =   ex.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , ex.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 ex.printStackTrace();
		}
		}
	}

	
}
public void linkSyncInventpryPath(AccessInfo accessInfoOld, boolean isNew,
		AccessInfo accessInfoNew) {
	inventoryPathManager.deleteLInkedImgs(accessInfoOld.getId()); 
	List localAccessinfo = inventoryPathManager.getListByLocId(accessInfoOld.getId(), "accessInfoId");
	try {
		Iterator<InventoryPath> itr=localAccessinfo.iterator();
		while(itr.hasNext()){
			Long invPathId= new Long(0);
			InventoryPath invPathOld = itr.next();
			invPathOld.setNetworkSynchedId(invPathOld.getId());
			invPathOld = inventoryPathManager.save(invPathOld);
			//List l1 = inventoryPathManager.getListByAccessInfoId(accessInfoNew.getId(), invPathOld.getAccessInfoType());
			InventoryPath invPathNew= new InventoryPath();
			/*if(l1!=null && !l1.isEmpty() ){
				invPathNew = (InventoryPath) l1.get(0);
				invPathId = invPathNew.getId();
				
			}*/
			 
			BeanUtilsBean beanUtilsBean22 = BeanUtilsBean.getInstance();
			 beanUtilsBean22.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			 beanUtilsBean22.copyProperties(invPathNew, invPathOld);
			 if(isNew){
				 invPathNew.setId(null);
			 }else{
				 invPathNew.setId(invPathId);
			 }
			 invPathNew.setCorpID(accessInfoNew.getCorpID());
			 invPathNew.setAccessInfoId(accessInfoNew.getId());
			 invPathNew.setNetworkSynchedId(invPathOld.getId());
			 inventoryPathManager.save(invPathNew);
		}
		
	} catch (Exception e) {
		e.printStackTrace();
	}
}
@SkipValidation
public List<Object> findServiceOrderRecords(List linkedShipNumber,	ServiceOrder serviceOrderLocal) {
	List<Object> recordList= new ArrayList();
	Iterator it =linkedShipNumber.iterator();
	while(it.hasNext()){
		String shipNumber= it.next().toString();
		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(shipNumber));
		recordList.add(serviceOrderRemote);
	}
	return recordList;
	}

@SkipValidation
public List findLinkerShipNumber(ServiceOrder serviceOrder) {
	List linkedShipNumberList= new ArrayList();
	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
	}else{
		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
	}
	return linkedShipNumberList;
}

public String saveMaterialsAccessInfo(){

	/*if(accessInfo.getId()!=null){
		accessInfo=accessInfoManager.save(accessInfo);
		message="Access Info has been updated";
		saveMessage(getText(message));
	}else{
		accessInfo=accessInfoManager.save(accessInfo);
		message="Access Info has been added";
		saveMessage(getText(message));
	}*/
	List list = companyManager.findByCorpID(sessionCorpID);
	if(list!=null && !list.isEmpty() && list.get(0)!=null){
		company = (Company) list.get(0);
	}
	systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
	boolean isNew = false;
	CustomerFile customerFile = customerFileManager.get(cid);
	if(aid!=null){
		accessInfo.setNetworkSynchedId(null);
		accessInfo=accessInfoManager.save(accessInfo);
		message="Access Info has been updated";
		saveMessage(getText(message));
	}else{
		isNew = true;
		accessInfo.setNetworkSynchedId(null);
		accessInfo=accessInfoManager.save(accessInfo);
		message="Access Info has been added";
		saveMessage(getText(message));
	}
	if(customerFile.getIsNetworkRecord()!=null && customerFile.getIsNetworkRecord() && ((customerFile.getContractType()!=null && (customerFile.getContractType().equals("CMM") ||customerFile.getContractType().equals("DMM"))) || company.getSurveyLinking())){
		List linkedSequenceNumber=findLinkerSequenceNumber(customerFile);
		Set set = new HashSet(linkedSequenceNumber);
		List linkedSeqNum = new ArrayList(set);
		List<Object> records=findRecords(linkedSeqNum,customerFile);
		accessInfo.setNetworkSynchedId(accessInfo.getId());
		accessInfo=accessInfoManager.save(accessInfo);
		synchornizeAccessInfoCF(records,accessInfo,isNew);
	}
	 //synchornizeAccessInfo(serviceOrderRecords,accessInfo,isNew,serviceOrder);
	listMaterials();
	return SUCCESS; 
}
public String listMaterials(){
	refDataForList();
	inventoryTransferflag=inventoryDataManager.getSOTransferStatus("M",cid,sessionCorpID);
	List list = new ArrayList<AccessInfo>();
 	articlesList=inventoryDataManager.getListByType("M",cid);
 	invSumList=inventoryDataManager.getSumOfListByType("A",cid,null);
 	if(articlesList!=null && !articlesList.isEmpty()){
 		invListflag="1";
 	}
 	if(invSumList!=null && !invSumList.isEmpty() && invSumList.get(0)!=null){
 		//invListflag="1";
 		InvDTO invdata=(InvDTO)invSumList.get(0);
 		if(invdata.getQuantitySum()!=null){
 		 quatSum=new BigDecimal(invdata.getQuantitySum());
 		}else{
 			quatSum=new BigDecimal("0.00");;	
 		}
 		if(invdata.getVolSum()!=null){
 		 volSum=new BigDecimal(invdata.getVolSum());
 		}else{
 			volSum=new BigDecimal("0.00");
 		}
 		if(invdata.getTotalSum()!=null){
 		 totSum=new BigDecimal(invdata.getTotalSum());
 		}else{
 			totSum=new BigDecimal("0.00");
 		}
 		
 	}
	customerFile=customerFileManager.get(cid);
	getRequest().setAttribute("soLastName", customerFile.getLastName());
        list=accessInfoManager.getAccessInfo(cid);
		if(!list.isEmpty() && list.get(0)!=""){
			accessInfo=(AccessInfo)list.get(0);
			List signImagelist=inventoryPathManager.getListByAccessInfoId(cid,"sign");
			  if(signImagelist!=null && !signImagelist.isEmpty() && signImagelist.get(0)!=null){
				inventoryPath=(InventoryPath) signImagelist.get(0);
			  }
		}else{
			accessInfo= new AccessInfo();
		}
		if(message!=null){
			if(!message.equals("done") && !message.equals("")&& !message.equals("Access Info has been updated")){
				String message=this.message;
				errorMessage(getText(message));
			}else{
				if(message.equals("done")){
					if(Quote.equalsIgnoreCase("y")){
						message="Survey Data Transfered To Quotes";
					}else{
				     message="Survey Data Transfered To S/O";
					}
				saveMessage(getText(message));
				}
			}
		}
		SOflag="CF";
	return SUCCESS;	
}

public String inventorySOList(){
	refDataForList();
	articlesList=inventoryDataManager.getInventoryListForSOByType(id,cid,"A");
	if(articlesList!=null && !articlesList.isEmpty()){
		invListflag="1";
	}
	invSumList=inventoryDataManager.getSumOfListByType("M",cid,id);
	if(invSumList!=null && !invSumList.isEmpty() && invSumList.get(0)!=null){
 		//invListflag="1";
 		InvDTO invdata=(InvDTO)invSumList.get(0);
 		if(invdata.getQuantitySum()!=null){
 		 quatSum=new BigDecimal(invdata.getQuantitySum());
 		}else{
 			quatSum=new BigDecimal("0.00");;	
 		}
 		if(invdata.getVolSum()!=null){
 		 volSum=new BigDecimal(invdata.getVolSum());
 		}else{
 			volSum=new BigDecimal("0.00");
 		}
 		if(invdata.getTotalSum()!=null){
 		 totSum=new BigDecimal(invdata.getTotalSum());
 		}else{
 			totSum=new BigDecimal("0.00");
 		}
 		
 	}
	List l=inventoryDataManager.getInventoryListForSOByType(id,cid,"M");
	List list = new ArrayList<AccessInfo>();
	 list=accessInfoManager.getAccessInfoForSO(id);
		if(!list.isEmpty() && list.get(0)!=""){
			accessInfo=(AccessInfo)list.get(0);
			List signImagelist=inventoryPathManager.getListByAccessInfoId(id,"sign");
			  if(signImagelist!=null && !signImagelist.isEmpty() && signImagelist.get(0)!=null){
				inventoryPath=(InventoryPath) signImagelist.get(0);
			  }
		}else{
			accessInfo= new AccessInfo();
			accessInfo.setCreatedOn(new Date());
			accessInfo.setUpdatedOn(new Date());
			accessInfo.setCreatedBy(getRequest().getRemoteUser());
			accessInfo.setUpdatedBy(getRequest().getRemoteUser());
		}
	if(!(l.isEmpty()) && !(l.get(0)==null)){
	  materialTransferFlag="Y";
	}
	customerFile=customerFileManager.get(cid);
	getRequest().setAttribute("soLastName", customerFile.getLastName());
	serviceOrder=serviceOrderManager.get(id);
	billing=billingManager.get(id);
	//inventoryDataManager.updateWeghtAndVol(customerFile.getId(),serviceOrder.getId(),sessionCorpID);
	shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
	minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
    countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
	SOflag="SO";
	return SUCCESS;	
}
public String inventorySOMaterialsList(){
	refDataForList();
	List list = new ArrayList<AccessInfo>();
	 list=accessInfoManager.getAccessInfoForSO(id);
		if(!list.isEmpty() && list.get(0)!=""){
			accessInfo=(AccessInfo)list.get(0);
			List signImagelist=inventoryPathManager.getListByAccessInfoId(id,"sign");
			  if(signImagelist!=null && !signImagelist.isEmpty() && signImagelist.get(0)!=null){
				inventoryPath=(InventoryPath) signImagelist.get(0);
			  }
		}else{
			accessInfo= new AccessInfo();
		}
	articlesList=inventoryDataManager.getInventoryListForSOByType(id,cid,"M");
	 if(!(articlesList.isEmpty()) && !(articlesList.get(0)==null)){
		  materialTransferFlag="Y";
		}
	 invSumList=inventoryDataManager.getSumOfListByType("A",cid,id);
	 	if(articlesList!=null && !articlesList.isEmpty()){
	 		invListflag="1";
	 		
	 	}
	 	if(invSumList!=null && !invSumList.isEmpty() && invSumList.get(0)!=null){
	 		//invListflag="1";
	 		InvDTO invdata=(InvDTO)invSumList.get(0);
	 		if(invdata.getQuantitySum()!=null){
	 		 quatSum=new BigDecimal(invdata.getQuantitySum());
	 		}else{
	 			quatSum=new BigDecimal("0.00");;	
	 		}
	 		if(invdata.getVolSum()!=null){
	 		 volSum=new BigDecimal(invdata.getVolSum());
	 		}else{
	 			volSum=new BigDecimal("0.00");
	 		}
	 		if(invdata.getTotalSum()!=null){
	 		 totSum=new BigDecimal(invdata.getTotalSum());
	 		}else{
	 			totSum=new BigDecimal("0.00");
	 		}
	 		
	 	}
	customerFile=customerFileManager.get(cid);
	getRequest().setAttribute("soLastName", customerFile.getLastName());
	serviceOrder=serviceOrderManager.get(id);
	billing=billingManager.get(id);
	inventoryDataManager.updateWeghtAndVol(customerFile.getId(),serviceOrder.getId(),sessionCorpID);
	shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
	minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
    countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
	SOflag="SO";
	return SUCCESS;	
}
public String delete(){
	inventoryDataManager.remove(id);
	inventoryDataManager.removeLinkedInventories(id); 
	hitflag="1";
	if(SOflag.equals("SO")){
		serviceOrder=serviceOrderManager.get(sid);
		billing=billingManager.get(sid);
		list();
		return INPUT;
	}else{
		list();
		return SUCCESS;
	}
}
public String edit(){
	getComboList(sessionCorpID);
	List list = companyManager.findByCorpID(sessionCorpID);
	if(list!=null && !list.isEmpty() && list.get(0)!=null){
		company = (Company) list.get(0);
	}
	systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
	if(id==null){
		inventoryData=new InventoryData();
		inventoryData.setCustomerFileID(cid);
		inventoryData.setCorpID(sessionCorpID);
		inventoryData.setCreatedOn(new Date());
		inventoryData.setCreatedBy(getRequest().getRemoteUser());
		inventoryData.setUpdatedBy(getRequest().getRemoteUser());
		inventoryData.setUpdatedOn(new Date());	
		if(SOflag.equals("SO")){
			articlesList=inventoryDataManager.getInventoryListForSOByType(sid,cid,"A");
			 if(!(articlesList.isEmpty()) && !(articlesList.get(0)==null)){
				 InventoryData inventoryData2=(InventoryData) articlesList.get(0);
				 if(inventoryData2.getMode().equalsIgnoreCase("other")){
					 inventoryData.setMode(inventoryData2.getMode());
					 inventoryData.setOther(inventoryData.getOther());
				 }else{
					 inventoryData.setMode(inventoryData2.getMode());
				 }
				 
			 }else{		
                    serviceOrder=serviceOrderManager.get(sid);
                    getRequest().setAttribute("soLastName", serviceOrder.getLastName());
                	billing=billingManager.get(sid);
				 if(serviceOrder.getJob().equalsIgnoreCase("sto")){
					 inventoryData.setMode("STORAGE");
				 }else{
					 inventoryData.setMode(serviceOrder.getMode().toUpperCase());
				 }				
			 }
		}		
		}else{
			inventoryData=inventoryDataManager.get(id);
			if(inventoryData.getServiceOrderID()!=null){
				if(!inventoryData.getServiceOrderID().equals("")){
					sid=inventoryData.getServiceOrderID();
				}
			}
	}
	
	return SUCCESS;		
}

public String save(){
	getComboList(sessionCorpID);
	List list = companyManager.findByCorpID(sessionCorpID);
	if(list!=null && !list.isEmpty() && list.get(0)!=null){
		company = (Company) list.get(0);
	}
	systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
	boolean isNew = (inventoryData.getId()==null);
	inventoryData.setUpdatedBy(getRequest().getRemoteUser());
	inventoryData.setUpdatedOn(new Date());
	inventoryData.setNetworkSynchedId(null);
	inventoryData=inventoryDataManager.save(inventoryData);
	
	id=inventoryData.getId();
		if(inventoryData.getServiceOrderID()!=null && !inventoryData.getServiceOrderID().equals("")){
			sid=inventoryData.getServiceOrderID();
			ServiceOrder so = serviceOrderManager.get(sid); 
			CustomerFile cf= customerFileManager.get(so.getCustomerFileId());
			if(so!=null){
				TrackingStatus ts = trackingStatusManager.get(sid);
				inventoryDataManager.updateWeghtAndVol(so.getCustomerFileId(),so.getId(),sessionCorpID);
				if(so.getIsNetworkRecord()){
					
					List linkedShipNumber=findLinkerShipNumber(so);
					 List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,so);
					WebApplicationContext  ctx=WebApplicationContextUtils.getWebApplicationContext(ServletActionContext.getServletContext()); 
					 ServiceOrderAction serviceOrderAction = (ServiceOrderAction) ctx.getBean("serviceOrderAction");
					List<Object> miscellaneousRecords=serviceOrderAction.findMiscellaneousRecords(linkedShipNumber,so);
					serviceOrderAction.synchornizeServiceOrder(serviceOrderRecords,so,ts,cf);
					Miscellaneous mis = miscellaneousManager.get(sid);
					serviceOrderAction.synchornizeMiscellaneous(miscellaneousRecords,mis,ts);
				if(so.getIsNetworkRecord() && (company.getSurveyLinking() || ts.getSoNetworkGroup())){
						inventoryData.setNetworkSynchedId(inventoryData.getId());
						inventoryData = inventoryDataManager.save(inventoryData);
						//List linkedShipNumber=findLinkerShipNumber(so);
						//List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,so);
						serviceOrderRecords.remove(so);
						synchornizeInventoryData(serviceOrderRecords,inventoryData,so,ts ,isNew,"SO");

				}
			}
			}
		}else{
			CustomerFile cf = customerFileManager.get(inventoryData.getCustomerFileID());
			if(cf.getIsNetworkRecord()!=null && cf.getIsNetworkRecord() && ((cf.getContractType()!=null && (cf.getContractType().equals("CMM") ||cf.getContractType().equals("DMM"))) || company.getSurveyLinking())){
				inventoryData.setNetworkSynchedId(inventoryData.getId());
				inventoryData = inventoryDataManager.save(inventoryData);
				sid=inventoryData.getServiceOrderID();
				ServiceOrder so = new ServiceOrder();
				TrackingStatus ts = new TrackingStatus();
					List linkedSequenceNumber=findLinkerSequenceNumber(cf);
					Set set = new HashSet(linkedSequenceNumber);
					List linkedSeqNum = new ArrayList(set);
					List<Object> records=findRecords(linkedSeqNum,cf);
					records.remove(cf);
					synchornizeInventoryData(records,inventoryData,so,ts ,isNew,"CF");

			}	
		}

	String message=inventoryData.getId()==null?"Data Saved":"Data Updated";
	saveMessage(getText(message));
	 hitflag="1";
	return SUCCESS;
}
public String materialsTransferPopUp(){
 List list=inventoryDataManager.getWorkTicket(id,sessionCorpID);
 if(!list.isEmpty()){
	 Iterator itr=list.iterator();
	 while(itr.hasNext()){
		 WorkTicket workTicket= (WorkTicket) itr.next();
		 workTicketList.add(workTicket);
	 }
 }
 return SUCCESS;
}
public String selectedMaterialsTransfer(){
	 String ticket="";
	 String ticketMessageList="";
	 
	 AccessInfo accInfo = inventoryDataManager.getAccessinfo(cid,sessionCorpID,id);
	   if(!ticketList.isEmpty()){
		   Iterator itr= ticketList.listIterator();
		   while(itr.hasNext()){
			   ticket= (String)itr.next();
			   if(!ticket.equals("[]")){
				   List inventoryList=inventoryDataManager.getDistinctArticleInventoryListByType(id,cid,"M"); 
					 if(!inventoryList.isEmpty()){
					 Iterator itr2= inventoryList.listIterator();
					 while(itr2.hasNext()){
						  Object[]  obj=(Object[]) itr2.next();
						 ItemsJbkEquip itemsJbkEquip = new ItemsJbkEquip();
						 itemsJbkEquip.setDescript(obj[0].toString());
						 if(!obj[1].toString().equals("")){
							 itemsJbkEquip.setQty(Integer.parseInt(obj[1].toString())); 
						 }else{
							 itemsJbkEquip.setQty(1); 
						 }
						 itemsJbkEquip.setTicket(Long.parseLong(ticket));
						// itemsJbkEquip.setWorkTicketID(workTicket.getId());
						 itemsJbkEquip.setType("M");
						 itemsJbkEquip.setCorpID(sessionCorpID);
						 itemsJbkEquipManager.save(itemsJbkEquip);
					     }					    
					}
					 if(!ticketMessageList.equals("")){
						 ticketMessageList=ticketMessageList+" , "+ticket;
					 }else{
						 ticketMessageList=ticket;
					 }
					 workTicket = inventoryDataManager.getWorkTicketByTicket(ticket,sessionCorpID); 
					 if(workTicket!=null){
						 updateWorkTicketInfo(workTicket,accInfo);
					 }
					
			   }
		   }
		    String message="Packing Materials transferred to Tickets:"+ticketMessageList;
			saveMessage(getText(message));
	   }	
		hitflag="1";
	   return SUCCESS;
}


private void updateWorkTicketInfo(WorkTicket workTicket, AccessInfo accInfo) {
	 if(accInfo !=null ){
		 workTicket.setSiteS(accInfo.getOriginResidenceType());
		 workTicket.setOrigMeters(accInfo.getOriginfloor());
		 workTicket.setOriginElevator(accInfo.getOriginElevatorType());
		 workTicket.setParkingS(accInfo.getOriginParkingType());
		 workTicket.setOriginShuttle(accInfo.getOriginShuttleRequired());
		 workTicket.setDest_farInMeters(accInfo.getOriginShuttleDistance());
		 workTicketManager.save(workTicket);
	 }
	
}
public String transferMaterialsAll(){
	 List list=inventoryDataManager.getWorkTicket(id,sessionCorpID);
	 AccessInfo accInfo = inventoryDataManager.getAccessinfo(cid,sessionCorpID,id);
	 String ticketList="";
	 if(!list.isEmpty() && list.get(0)!=null ){
		 Iterator itr= list.listIterator();
		 while(itr.hasNext()){
			 WorkTicket workTicket=(WorkTicket) itr.next();
			 List inventoryList=inventoryDataManager.getDistinctArticleInventoryListByType(id,cid,"M"); 
			 if(!inventoryList.isEmpty()){
			 Iterator itr2= inventoryList.listIterator();
			 while(itr2.hasNext()){
				 Object[]  obj=(Object[]) itr2.next();
				 ItemsJbkEquip itemsJbkEquip = new ItemsJbkEquip();
				 itemsJbkEquip.setDescript(obj[0].toString());
				 if(!obj[1].toString().equals("")){
					 itemsJbkEquip.setQty(Integer.parseInt(obj[1].toString())); 
				 }else{
					 itemsJbkEquip.setQty(1); 
				 }				 
				 itemsJbkEquip.setTicket(workTicket.getTicket());
				 itemsJbkEquip.setWorkTicketID(workTicket.getId());
				 itemsJbkEquip.setType("M");
				 itemsJbkEquip.setCorpID(sessionCorpID);
				 itemsJbkEquipManager.save(itemsJbkEquip);
			     }
			}
			 if(ticketList.equals("")){
				 ticketList=""+workTicket.getTicket();
			 }else{
				 ticketList=ticketList+","+workTicket.getTicket();
			 }
			 updateWorkTicketInfo(workTicket,accInfo);
		 }
		    String message="Packing Materials transferred to Tickets:"+ticketList;
			saveMessage(getText(message));
	}
	 hitflag="1";
	 return SUCCESS;
}
public String transferMaterials(){
	inventorySOList();
	int count= inventoryDataManager.workTicketCount(id,sessionCorpID);
	if(count==0){
		String message="No Packing Ticket available for Transfer. Kindly create one.";
		saveMessage(getText(message));
		 hitflag="1";
		return SUCCESS;
	}else if(count>=1){
		 List inventoryList=inventoryDataManager.getInventoryListByType(id,cid,"M"); 
		 if(inventoryList.isEmpty()){
			    String message="No Materials to Transfer";
				errorMessage(getText(message));
			 hitflag="1";
		 }else{
		    message="popup";
		 }		 
		return SUCCESS;
	}else if(count==1){
		 List list=inventoryDataManager.getWorkTicket(id,sessionCorpID); 
		 if(!list.isEmpty() && list.get(0)!=null ){
			 WorkTicket workTicket=(WorkTicket) list.get(0);
		 List inventoryList=inventoryDataManager.getInventoryListByType(id,cid,"M"); 
		 if(!inventoryList.isEmpty()){
		 Iterator itr= inventoryList.listIterator();
		 while(itr.hasNext()){
			 InventoryData inventoryData=(InventoryData) itr.next();
			 ItemsJbkEquip itemsJbkEquip = new ItemsJbkEquip();
			 itemsJbkEquip.setDescript(inventoryData.getAtricle());
			 itemsJbkEquip.setQty(Integer.parseInt(inventoryData.getQuantity()));
			 itemsJbkEquip.setTicket(workTicket.getTicket());
			 itemsJbkEquip.setWorkTicketID(workTicket.getId());
			 itemsJbkEquip.setType(inventoryData.getType());
			 itemsJbkEquip.setCorpID(sessionCorpID);
			 itemsJbkEquipManager.save(itemsJbkEquip);
		 }
		    String message="Packing Materials transferred to Ticket:"+workTicket.getTicket();
			saveMessage(getText(message));
		}else{
			String message="No Materials to Transfer";
			errorMessage(getText(message));
			 hitflag="1";
			return SUCCESS;
		}
	  }
	}
	 hitflag="1";
	return SUCCESS;
}

public String inventoryTransfer(){
	 List soList = inventoryDataManager.getSOListbyCF(cid);
	 List articlesList=inventoryDataManager.getDistinctMode(cid);
         for(int i=0;i<soList.size();i++){
        	 ServiceOrder serviceorder =(ServiceOrder)soList.get(i);
        	 if(i==0){ 
        		 if(serviceorder.getJob().equalsIgnoreCase("sto")){  
        			 shipAndModeList="storage"+":"+serviceorder.getShip(); 
        		 }else{
        			 shipAndModeList=serviceorder.getMode()+":"+serviceorder.getShip(); 
        		   }        		 
        		 } 
        	 else{
        		 if(serviceorder.getJob().equalsIgnoreCase("sto")){  
        			 shipAndModeList=shipAndModeList+","+serviceorder.getJob()+":"+serviceorder.getShip();
        		 }else{
        			 shipAndModeList=shipAndModeList+","+serviceorder.getMode()+":"+serviceorder.getShip();
        		   }        		
        		 }
         }
         for(int i=0;i<soList.size();i++){
        	 ServiceOrder serviceorder =(ServiceOrder)soList.get(i);
        	 int breakFlag=0;
        	 for(int k=0;k<articlesList.size();k++){
        		 if((serviceorder.getMode()!= null) && (serviceorder.getMode().equals(""))){
        		 if(serviceorder.getJob().equalsIgnoreCase(articlesList.get(k).toString().substring(0, 3))){       		
            			 modeShipMap.put(articlesList.get(k).toString(), serviceorder);
            			 articlesList.remove(k);
            			 k=k-1;
            			 soList.remove(i);
            			 i=i-1; 
            			 breakFlag=1;
            			 break;
        		 	}
        		 }
        	 }
        		 
        	 if(breakFlag==1){
        		 continue;
        	 }
        	 for(int j=0;j<articlesList.size();j++){ 
        		 if((serviceorder.getMode()!= null) && (!(serviceorder.getMode().equals("")))){
        		 if(articlesList.get(j).toString().length()>3){
        			 if(serviceorder.getMode().substring(0, 3).equalsIgnoreCase(articlesList.get(j).toString().substring(0,3))){            			 
            			 int count = inventoryDataManager.countByIndividualMode(cid,articlesList.get(j).toString().substring(0,3));
            			 if(count>=1){
            			 modeShipMap.put(articlesList.get(j).toString(), serviceorder);
            			 articlesList.remove(j);
            			 j=j-1;
            			 soList.remove(i);
            			 i=i-1;
            			 }            			 
            			 break;
            		 }  
        		 }else{
        		   if(serviceorder.getMode().equalsIgnoreCase(articlesList.get(j).toString().substring(0, 3))){            			 
        			 int count = inventoryDataManager.countByIndividualMode(cid,articlesList.get(j).toString().substring(0, 3));
        			 if(count>=1){
        			 modeShipMap.put(articlesList.get(j).toString(), serviceorder);
        			 articlesList.remove(j);
        			 j=j-1;
        			 soList.remove(i);
        			 i=i-1;
        			 }            			 
        			 break;
        		 } }           		        		   
        	 }
         }
         }
         for(int i=0;i<articlesList.size();i++){ 
        	 int count = inventoryDataManager.countByIndividualMode(cid,articlesList.get(i).toString().substring(0, 3));
        	 if(count==0){
			        if(modeWithOutSO.equals("")){ 
				        modeWithOutSO=articlesList.get(i).toString();
			        }else{
				     modeWithOutSO=modeWithOutSO+","+articlesList.get(i).toString();
			          }          				  
		           }
        	/*if(count==1){
        		 if(modeWithOneSO.equals("")){ 
        			 modeWithOneSO=articlesList.get(i).toString();
			        }else{
			        	modeWithOneSO=modeWithOneSO+","+articlesList.get(i).toString();
			          }            		 
        	 }*/
        	 }
          list();
          SOflag="CF";
         return SUCCESS;
} 
public String modeMessage;
public String inventryTransferToSO(){
	Boolean status = inventoryDataManager.getCountForSameModeTypeSO(cid);
	 List soList = inventoryDataManager.getSOListbyCF(cid);
     List articlesList=inventoryDataManager.getDistinctMode(cid);
   	String otherModeItemList="";
	modeSORelation="";
	modeWithOutSO="";
	if(!status){
	     String tempMode="";
        Set modeSet = new HashSet();
         Iterator  it =soList.iterator();
         while(it.hasNext()){
             serviceOrder = (ServiceOrder) it.next();
             if((articlesList != null) && (!(articlesList.isEmpty()))){
             for(int i=0; i<articlesList.size();i++){
            	 String temp = articlesList.get(i).toString();
            	 if(!(temp.equalsIgnoreCase("not going"))){
                     if(serviceOrder.getJob().equalsIgnoreCase("STO")){
                    	 if(!(temp.equalsIgnoreCase("STORAGE"))){
                  	  		 modeSet.add(temp.toUpperCase());
                                             		
                 }
                 }
                 else{
                	 if(temp.length()>7){
                	 String sub =  temp.substring(0, 3).toUpperCase();
                   	 	if(!(sub.equalsIgnoreCase(serviceOrder.getMode()))){
                	 			modeSet.add(sub.toUpperCase());
                	 		}
                	 }
                	 else{
                		 if(!(temp.equalsIgnoreCase(serviceOrder.getMode()))){
             	 			modeSet.add(temp.toUpperCase());
             	 		}
                	 }
               }
             }
         }
           
            /* if((articlesList.size())<=checkList){
                 String message="Service Order Not Created for Mode "+tempMode;
                 errorMessage(message);
           } */
         }
         }
         Iterator  it1 =soList.iterator();
         while(it1.hasNext()){
             serviceOrder = (ServiceOrder) it1.next();
             String soMode=serviceOrder.getMode();
             Iterator itSet= modeSet.iterator();
             boolean checkMade=true; 
             while((itSet.hasNext()) && (checkMade)){
            	 String modes=itSet.next().toString();
            	 if(serviceOrder.getJob().equalsIgnoreCase("STO")){
            		// System.out.print("list of service orders-------------"+modeSet);
            		 checkMade=false;
            	 } 	 else{
            		 if(modes.length()>7){
                      	 String sub =  modes.substring(0,3).toUpperCase();
                         	 	if((sub.equalsIgnoreCase(serviceOrder.getMode()))){
                         	 		modeSet.remove(modes);
                         	 		//System.out.print("list of service orders-------------"+modeSet);
                           		 checkMade=false;
                      	 		}
                      	 }
                      	 else{
                      		 if(modes.equalsIgnoreCase(soMode)){
                      			modeSet.remove(modes);
                      			//System.out.print("list of service orders-------------"+modeSet);
                       		 checkMade=false;
                   	 		}
            		 
            		/* if(modes.equalsIgnoreCase(soMode)){
            		 modeSet.remove(modes);
            		 System.out.print("list of service orders-------------"+modeSet);
            		 checkMade=false;
            		 }*/
            	 }
               }
             }
         }
       //  System.out.print("list of service orders-------------"+modeSet);
         Iterator itSet1= modeSet.iterator();
         while(itSet1.hasNext()){
        	 tempMode= tempMode+", "+itSet1.next().toString();
         }
         if(!(tempMode.equals("")) && !(tempMode.equals(" ,"))){
        	 modeMessage=tempMode.substring(1,tempMode.length());
         }
		this.list();
		message="popup";
	  return SUCCESS;
	}else{
       
        if(soList.isEmpty()){
               String message="No Service Order To Update";
               saveMessage(getText(message));
                list();
               return SUCCESS;
        }
        else{
          /* int checkList =0;
           String tempMode="";
           Iterator  it =soList.iterator();
           while(it.hasNext()){
               serviceOrder = (ServiceOrder) it.next();
               if((articlesList != null) && (!(articlesList.isEmpty()))){
               for(int i=0; i<articlesList.size();i++){
                   String temp = articlesList.get(i).toString();
                   if(!(temp.equalsIgnoreCase("not going"))){
                       if(serviceOrder.getJob().equalsIgnoreCase("STO")){
                       if(temp!= serviceOrder.getJob()){
                    	   if(checkList==0){
                    		   tempMode=temp;
                    		 }
                    	   else{
                    		   tempMode=tempMode+","+temp;
                    	   }
                           checkList++;
                   }
                   }
                   else{
                   if(!(temp.substring(0,3).equalsIgnoreCase(serviceOrder.getMode()))){
                	   if(checkList==0){
                		   tempMode=temp;
                		 }
                	   else{
                		   tempMode=tempMode+","+temp;
                	   }
                       checkList++;
                     }
                   }
                   }
               }
           }
             
               if((articlesList.size())<=checkList){
                   String message="Service Order Not Created for Mode "+tempMode;
                   errorMessage(message);
                   list();
                   return SUCCESS;
               }
   }*/
        	  String tempMode="";
              Set modeSet = new HashSet();
               Iterator  it =soList.iterator();
               while(it.hasNext()){
                   serviceOrder = (ServiceOrder) it.next();
                   if((articlesList != null) && (!(articlesList.isEmpty()))){
                   for(int i=0; i<articlesList.size();i++){
                  	 String temp = articlesList.get(i).toString();
                  	 if(!(temp.equalsIgnoreCase("not going"))){
                           if(serviceOrder.getJob().equalsIgnoreCase("STO")){
                           if(!(temp.equalsIgnoreCase("STORAGE"))){
                        	  		 modeSet.add(temp.toUpperCase());
                                                   		
                       }
                       }
                       else{
                      	 if(temp.length()>7){
                      	 String sub =  temp.substring(0, 3).toUpperCase();
                         	 	if(!(sub.equalsIgnoreCase(serviceOrder.getMode()))){
                      	 			modeSet.add(sub.toUpperCase());
                      	 		}
                      	 }
                      	 else{
                      		 if(!(temp.equalsIgnoreCase(serviceOrder.getMode()))){
                   	 			modeSet.add(temp.toUpperCase());
                   	 		}
                      	 }
                     }
                   }
               }
                 
                  /* if((articlesList.size())<=checkList){
                       String message="Service Order Not Created for Mode "+tempMode;
                       errorMessage(message);
                 } */
               }
               }
               Iterator  it1 =soList.iterator();
               while(it1.hasNext()){
                   serviceOrder = (ServiceOrder) it1.next();
                   String soMode=serviceOrder.getMode();
                   Iterator itSet= modeSet.iterator();
                   boolean checkMade=true; 
                   while((itSet.hasNext()) && (checkMade)){
                  	 String modes=itSet.next().toString();
                  	 if(serviceOrder.getJob().equalsIgnoreCase("STO")){
                		 modeSet.remove("STORAGE");
                		// System.out.print("list of service orders-------------"+modeSet);
                		 checkMade=false;
                	 } 	 else{
                		 
                		 if(modes.length()>5){
                          	 String sub =  modes.substring(0, 3).toUpperCase();
                             	 	if((sub.equalsIgnoreCase(serviceOrder.getMode()))){
                             	 		modeSet.remove(modes);
                             	 		//System.out.print("list of service orders-------------"+modeSet);
                               		 checkMade=false;
                          	 		}
                          	 }
                          	 else{
                          		 if(modes.equalsIgnoreCase(soMode)){
                          			modeSet.remove(modes);
                          			//System.out.print("list of service orders-------------"+modeSet);
                           		 checkMade=false;
                       	 		}
                		 
                		 
                		/* if(modes.equalsIgnoreCase(soMode)){
                		 modeSet.remove(modes);
                		 System.out.print("list of service orders-------------"+modeSet);
                		 checkMade=false;
                		 } */
                	 }
                  	 
                   }
                   }
               }
              // System.out.print("list of service orders-------------"+modeSet);
               Iterator itSet1= modeSet.iterator();
               while(itSet1.hasNext()){
              	 tempMode= tempMode+", "+itSet1.next().toString();
               }
               if(!(tempMode.equals("")) && !(tempMode.equals(" ,"))){
		               modeMessage=tempMode.substring(1,tempMode.length());
		               String message1="";
		               if(Quote.equalsIgnoreCase("y")){
		               message1="Quote Not Created for Mode - "+modeMessage+".";
		               }else{
		               message1="Service Order Not Created for Mode - "+modeMessage+".";		            	   
		               }
		               errorMessage(message1);
               }
               list();
               return SUCCESS;
           
   }
		/* 
		 * 
		 * else{
		 List soList = inventoryDataManager.getSOListbyCF(cid);
		 List articlesList=inventoryDataManager.getDistinctMode(cid);
		 if(soList.isEmpty()){
			    String message="No ServiceOrder To Update";
				saveMessage(getText(message));
				 list();
				return SUCCESS;
		 }
		 * for(int i=0;i<soList.size();i++){
        	 ServiceOrder serviceorder =(ServiceOrder)soList.get(i);
        	 for(int j=0;j<articlesList.size();j++){        		 
        		 if(serviceorder.getMode().equalsIgnoreCase(articlesList.get(j).toString())){            			 
        			 int count = inventoryDataManager.countByIndividualMode(cid,articlesList.get(j).toString());
        			 if(count==1){
        			  if(modeSORelation.equals("")){ 
        				  modeSORelation=articlesList.get(j).toString()+":"+serviceorder.getShipNumber();
				        }else{
				        	modeSORelation=modeSORelation+","+articlesList.get(j).toString()+":"+serviceorder.getShipNumber();
				          }  
        			 articlesList.remove(j);
        			 j=j-1;
        			 soList.remove(i);
        			 i=i-1;
        			 }            			 
        			 break;
        		 }            		        		   
        	 }
         }*/
		/* for(int i=0;i<articlesList.size();i++){ 
	        	 int count = inventoryDataManager.countByIndividualMode(cid,articlesList.get(i).toString());
	        	 if(count==0){
				        if(modeWithOutSO.equals("")){ 					       
					        if(articlesList.get(i).toString().trim().length()>3){
				        		inventoryDataManager.updateOtherModeList(cid,articlesList.get(i).toString());
				        		if(otherModeItemList.equals("")){
				        			otherModeItemList=articlesList.get(i).toString();
				        		} else{
				        			otherModeItemList=otherModeItemList+","+articlesList.get(i).toString();	
				        		} 				        		
				        	}else{
				        		 modeWithOutSO=articlesList.get(i).toString();
				        	}
				        }else{
				        	if(articlesList.get(i).toString().trim().length()>3){
				        		inventoryDataManager.updateOtherModeList(cid,articlesList.get(i).toString());
				        		if(otherModeItemList.equals("")){
				        			otherModeItemList=articlesList.get(i).toString();
				        		} else{
				        			otherModeItemList=otherModeItemList+","+articlesList.get(i).toString();	
				        		} 				        		
				        	}else{
				        		modeWithOutSO=modeWithOutSO+","+articlesList.get(i).toString();
				        	}					     
				          }          				  
			           }	        
	        	 }*/
		/* inventoryDataManager.InventorySOMapping(cid,modeSORelation,"");
		 if(modeWithOutSO.equals("")){
			 if(!otherModeItemList.equals("")){
					inventoryDataManager.setOtherModeInventorySOid(cid,otherModeItemList);				
				    String	message="Data Transfered To S/O";
					saveMessage(getText(message)); 
				}else{
				    String	message="Data Transfered To S/O";
					saveMessage(getText(message)); 
				}
		 }else{
			if(!otherModeItemList.equals("")){
				inventoryDataManager.setOtherModeInventorySOid(cid,otherModeItemList);				
				String key = "Service Order Not Created for Mode �"+modeWithOutSO;
				errorMessage(getText(key));
			}else{
				String key = "Service Order Not Created for Mode �"+modeWithOutSO;
				errorMessage(getText(key));
			}
		 }*/
		// list();
		// return SUCCESS;
		/*
		int volumeSea=0;
		int volumeAir=0;
		int volumeStorage=0;
		int volumeRoad=0;
		articlesList=inventoryDataManager.getListByType("",cid);
		if(articlesList.isEmpty()){
			String message="No record to Update";
			saveMessage(getText(message));
		}else{ 
		 	Iterator iterator= articlesList.iterator();
		 	while(iterator.hasNext()){
		 		inventoryData = (InventoryData) iterator.next();
		 		if(inventoryData.getMode().equalsIgnoreCase("Air")){
		 			volumeAir=volumeAir+Integer.parseInt(inventoryData.getTotal());
		 		}
		 		if(inventoryData.getMode().equalsIgnoreCase("Sea")){
		 			volumeSea=volumeSea+Integer.parseInt(inventoryData.getTotal());
		 		}
		 		if(inventoryData.getMode().equalsIgnoreCase("TRU")){
		 			volumeRoad=volumeRoad+Integer.parseInt(inventoryData.getTotal());
		 		}
		 		if(inventoryData.getMode().equalsIgnoreCase("STO")){
		 			volumeStorage=volumeStorage+Integer.parseInt(inventoryData.getTotal());
		 		}
		 	}
		 	String result = inventoryDataManager.updateEstimatedVolumeCft(volumeAir,volumeSea,volumeRoad,volumeStorage,cid);
			if(result.equals("false")){String message="No Records To Update";
			saveMessage(getText(message));}else{
				if(!(result.equals(""))){
				//String[] result2= result.split(",");
				//String r2="";
				//for(int i=0;i<result2.length-1;i++){
					//if((r2.equals(""))){
				//		r2=result2[i];
					//}else{r2=r2+","+result2[i];}
				//}
				//r2=r2+","+result2[result2.length-1];
				String key = "Service Order Not Created for Mode �"+result;
				errorMessage(getText(key));
				}else{
					String	message="Data Transfered To S/O";
					saveMessage(getText(message)); 
		      		}
				}
		 	}
		list();
		return SUCCESS;
	*/}
		
}



public String inventoryFromSOCongormation(){
	List list1 = companyManager.findByCorpID(sessionCorpID);
	if(list1!=null && !list1.isEmpty() && list1.get(0)!=null){
		company = (Company) list1.get(0);
	}
	systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
	if(modeWithOutSO.equals("")){		
	    message="done";
		}else{
			//message = "Service Order Not Created for Mode "+modeWithOutSO;
      		} 
	inventoryDataManager.InventorySOMapping(cid,modeSORelation,modeWithOneSO,sessionCorpID,getRequest().getRemoteUser());
	
	
//	accessInfo=accessInfo
	String[]modeship=modeSORelation.split(",");
	String shipNum="";
	Long soid=null;
	Long sacessinfoid=null;
	if(modeship!=null && modeship.length>0){
	for(int i=0;i<modeship.length;i++){
		String []modeandship=modeship[i].split(":");
		if(modeandship!=null && modeandship.length>1){
			if(modeandship[1]!=null )
			 shipNum=modeandship[1];
			if(shipNum!=null && !shipNum.equals("")){
				List lids=serviceOrderManager.findIdByShipNumber(shipNum);
				if(lids!=null && !lids.isEmpty()){
					if(lids.get(0)!=null && !lids.get(0).toString().equals("")){
						soid=Long.parseLong(lids.get(0).toString());
						ServiceOrder soObj = serviceOrderManager.get(soid);
						TrackingStatus ts = trackingStatusManager.get(soid);
						Miscellaneous mis = miscellaneousManager.get(soid);
						CustomerFile cf= customerFileManager.get(soObj.getCustomerFileId());
						if(soObj.getIsNetworkRecord() ){
							List linkedShipNumber=findLinkerShipNumber(soObj);
							 List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,soObj);
							WebApplicationContext  ctx=WebApplicationContextUtils.getWebApplicationContext(ServletActionContext.getServletContext()); 
							 ServiceOrderAction serviceOrderAction = (ServiceOrderAction) ctx.getBean("serviceOrderAction");
							List<Object> miscellaneousRecords=serviceOrderAction.findMiscellaneousRecords(linkedShipNumber,soObj);
							serviceOrderAction.synchornizeServiceOrder(serviceOrderRecords,soObj,ts,cf);
							serviceOrderAction.synchornizeMiscellaneous(miscellaneousRecords,mis,ts);
							if(soObj.getIsNetworkRecord() && (ts.getSoNetworkGroup() || company.getSurveyLinking())){
							List l =  inventoryDataManager.getItemList(soid, sessionCorpID);
							for (Object object : l) {
								boolean isNew = false;
								inventoryData = (InventoryData) object;
								if(inventoryData.getNetworkSynchedId()==null){
									isNew= true;
									inventoryData.setNetworkSynchedId(inventoryData.getId());
									inventoryData = inventoryDataManager.save(inventoryData);
								}
								 serviceOrderRecords.remove(soObj);
								synchornizeInventoryData(serviceOrderRecords,inventoryData,soObj,ts,isNew,"SO");
							
							}
							}
						}
					}
					if(soid!=null){
						List list = new ArrayList<AccessInfo>();
						AccessInfo sourceaccessInfo=new AccessInfo();
						AccessInfo destaccessInfo=new AccessInfo();
						 list=accessInfoManager.getAccessInfo(cid);
							if(list!=null && !list.isEmpty() && list.get(0)!=""){
								sourceaccessInfo=(AccessInfo)list.get(0);
								List signImagelist=inventoryPathManager.getListByAccessInfoId(soid,"sign");
								  if(signImagelist!=null && !signImagelist.isEmpty() && signImagelist.get(0)!=null){
									inventoryPath=(InventoryPath) signImagelist.get(0);
								  }
							}else{
								sourceaccessInfo= new AccessInfo();
							}
							BeanUtilsBean beanUtilsContract = BeanUtilsBean.getInstance();
							  beanUtilsContract.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
							  try{
							  beanUtilsContract.copyProperties(destaccessInfo, sourceaccessInfo);
							  }catch(Exception e){
								  e.printStackTrace();
							  }
							 // newContract.setCorpID(agentsCorpId[i]);
							  if(sourceaccessInfo.getNetworkSynchedId()!=null){
								  destaccessInfo.setNetworkSynchedId(sourceaccessInfo.getNetworkSynchedId());
							  }else{
								  destaccessInfo.setNetworkSynchedId(null);
							  }
							  destaccessInfo.setCorpID(sessionCorpID);
							 // newContract.setOrigContract(contract.getId());
							 // newContract.setCompanyDivision("");
							 // newContract.setId(null);
							 // contractManager.save(newContract);
							  destaccessInfo.setCustomerFileId(null);
							  destaccessInfo.setServiceOrderId(soid);
							  
							  List tempidacessList=accessInfoManager.getIdBySoID(destaccessInfo.getServiceOrderId());
							  if(tempidacessList!=null && !tempidacessList.isEmpty()){
								  if(tempidacessList.get(0)!=null){
							  sacessinfoid=(Long)tempidacessList.get(0);
								  }
							  }
							  if(sacessinfoid!=null){
								  destaccessInfo.setId(sacessinfoid);
							  }else{
								  destaccessInfo.setId(null);  
							  }
							  /*if(sacessinfoid!=null){
								  List listtemp = new ArrayList<AccessInfo>();
								 // List listtemp = new ArrayList<AccessInfo>();
								  listtemp=accessInfoManager.getAccessInfoForSO(destaccessInfo.getServiceOrderId());
								  if(listtemp!=null && !listtemp.isEmpty() && listtemp.get(0)!=""){
									  AccessInfo tempinfo=(AccessInfo)list.get(0);
										List signImagelist=inventoryPathManager.getListByAccessInfoId(soid,"sign");
										  if(signImagelist!=null && !signImagelist.isEmpty() && signImagelist.get(0)!=null){
											inventoryPath=(InventoryPath) signImagelist.get(0);
										  }
										  tempinfo.setCustomerFileId(null);
										  if(destaccessInfo.getDestinationResidenceType()!=null){
											  tempinfo.setDestinationResidenceType(destaccessInfo.getDestinationResidenceType());
											  }
										  if(destaccessInfo.getDestinationResidenceSize()!=null){
											  tempinfo.setDestinationResidenceSize(destaccessInfo.getDestinationResidenceSize());
											  }
										  if(destaccessInfo.getDestinationLongCarry()!=null){
											  tempinfo.setDestinationLongCarry(destaccessInfo.getDestinationLongCarry());
											  }
										  if(destaccessInfo.getDestinationCarryDistance()!=null){
											  tempinfo.setDestinationCarryDistance(destaccessInfo.getDestinationCarryDistance());
											  }
										  if(destaccessInfo.getDestinationCarrydetails()!=null){
											  tempinfo.setDestinationCarrydetails(destaccessInfo.getDestinationCarrydetails());
											  }
										  if(destaccessInfo.getDestinationStairCarry()!=null){
											  tempinfo.setDestinationStairCarry(destaccessInfo.getDestinationStairCarry());
											  }
										  if(destaccessInfo.getDestinationStairDistance()!=null){
											  tempinfo.setDestinationStairDistance(destaccessInfo.getDestinationStairDistance());
											  }
										  if(destaccessInfo.getDestinationStairdetails()!=null){
											  tempinfo.setDestinationStairdetails(destaccessInfo.getDestinationStairdetails());
											  }
										  if(destaccessInfo.getDestinationfloor()!=null){
											  tempinfo.setDestinationfloor(destaccessInfo.getDestinationfloor());
											  }
										  if(destaccessInfo.getDestinationNeedCrane()!=null){
											  tempinfo.setDestinationNeedCrane(destaccessInfo.getDestinationNeedCrane());
											  }
										  if(destaccessInfo.getDestinationElevator()!=null){
											  tempinfo.setDestinationElevator(destaccessInfo.getDestinationElevator());
											  }
										  if(destaccessInfo.getDestinationElevatorDetails()!=null){
											  tempinfo.setDestinationElevatorDetails(destaccessInfo.getDestinationElevatorDetails());
											  }
										  if(destaccessInfo.getDestinationExternalElevator()!=null){
											  tempinfo.setDestinationExternalElevator(destaccessInfo.getDestinationExternalElevator());
											  }
										  if(destaccessInfo.getDestinationElevatorType()!=null){
											  tempinfo.setDestinationElevatorType(destaccessInfo.getDestinationElevatorType());
											  }
										  if(destaccessInfo.getDestinationParkingType()!=null){
											  tempinfo.setDestinationParkingType(destaccessInfo.getDestinationParkingType());
											  }
										  if(destaccessInfo.getDestinationParkinglotsize()!=null){
											  tempinfo.setDestinationParkinglotsize(destaccessInfo.getDestinationParkinglotsize());
											  }
										  if(destaccessInfo.getDestinationParkingSpots()!=null){
											  tempinfo.setDestinationParkingSpots(destaccessInfo.getDestinationParkingSpots());
											  }
										  if(destaccessInfo.getDestinationDistanceToParking()!=null){
											  tempinfo.setDestinationDistanceToParking(destaccessInfo.getDestinationDistanceToParking());
											  }
										  if(destaccessInfo.getDestinationShuttleDistance()!=null){
											  tempinfo.setDestinationShuttleDistance(destaccessInfo.getDestinationShuttleDistance());
											  }
										  if(destaccessInfo.getDestinationReserveParking()!=null){
											  tempinfo.setDestinationReserveParking(destaccessInfo.getDestinationReserveParking());
											  }
										  if(destaccessInfo.getDestinationAdditionalStopRequired()!=null){
											  tempinfo.setDestinationAdditionalStopRequired(destaccessInfo.getDestinationAdditionalStopRequired());
											  }
										  if(destaccessInfo.getDestinationShuttleRequired()!=null){
											  tempinfo.setDestinationShuttleRequired(destaccessInfo.getDestinationShuttleRequired());
											  }
										  if(destaccessInfo.getDestinationAdditionalStopDetails()!=null){
										  tempinfo.setDestinationAdditionalStopDetails(destaccessInfo.getDestinationAdditionalStopDetails());
										  }
										  if(destaccessInfo.getOriginResidenceType()!=null){
											  tempinfo.setOriginResidenceType(destaccessInfo.getOriginResidenceType());
											  }
										  if(destaccessInfo.getOriginResidenceSize()!=null){
											  tempinfo.setOriginResidenceSize(destaccessInfo.getOriginResidenceSize());
											  }
										  if(destaccessInfo.getOriginLongCarry()!=null){
											  tempinfo.setOriginLongCarry(destaccessInfo.getOriginLongCarry());
											  }
										  if(destaccessInfo.getOriginCarryDistance()!=null){
											  tempinfo.setOriginCarryDistance(destaccessInfo.getOriginCarryDistance());
											  }
										  if(destaccessInfo.getOriginCarrydetails()!=null){
											  tempinfo.setOriginCarrydetails(destaccessInfo.getOriginCarrydetails());
											  }
										  if(destaccessInfo.getOriginStairCarry()!=null){
											  tempinfo.setOriginStairCarry(destaccessInfo.getOriginStairCarry());
											  }
										  if(destaccessInfo.getOriginStairDistance()!=null){
											  tempinfo.setOriginStairDistance(destaccessInfo.getOriginStairDistance());
											  }
										  if(destaccessInfo.getOriginStairdetails()!=null){
											  tempinfo.setOriginStairdetails(destaccessInfo.getOriginStairdetails());
											  }
										  if(destaccessInfo.getOriginfloor()!=null){
											  tempinfo.setOriginfloor(destaccessInfo.getOriginfloor());
											  }
										  if(destaccessInfo.getOriginNeedCrane()!=null){
											  tempinfo.setOriginNeedCrane(destaccessInfo.getOriginNeedCrane());
											  }
										  if(destaccessInfo.getOriginElevator()!=null){
											  tempinfo.setOriginElevator(destaccessInfo.getOriginElevator());
											  }
										  if(destaccessInfo.getOriginElevatorDetails()!=null){
											  tempinfo.setOriginElevatorDetails(destaccessInfo.getOriginElevatorDetails());
											  }
										  if(destaccessInfo.getOriginExternalElevator()!=null){
											  tempinfo.setOriginExternalElevator(destaccessInfo.getOriginExternalElevator());
											  }
										  if(destaccessInfo.getOriginElevatorType()!=null){
											  tempinfo.setOriginElevatorType(destaccessInfo.getOriginElevatorType());
											  }
										  if(destaccessInfo.getOriginParkingType()!=null){
											  tempinfo.setOriginParkingType(destaccessInfo.getOriginParkingType());
											  }
										  if(destaccessInfo.getOriginParkinglotsize()!=null){
											  tempinfo.setOriginParkinglotsize(destaccessInfo.getOriginParkinglotsize());
											  }
										  if(destaccessInfo.getOriginParkingSpots()!=null){
											  tempinfo.setOriginParkingSpots(destaccessInfo.getOriginParkingSpots());
											  }
										  if(destaccessInfo.getOriginDistanceToParking()!=null){
											  tempinfo.setOriginDistanceToParking(destaccessInfo.getOriginDistanceToParking());
											  }
										  if(destaccessInfo.getOriginShuttleDistance()!=null){
											  tempinfo.setOriginShuttleDistance(destaccessInfo.getOriginShuttleDistance());
											  }
										  if(destaccessInfo.getOriginReserveParking()!=null){
											  tempinfo.setOriginReserveParking(destaccessInfo.getOriginReserveParking());
											  }
										  if(destaccessInfo.getOriginAdditionalStopRequired()!=null){
											  tempinfo.setOriginAdditionalStopRequired(destaccessInfo.getOriginAdditionalStopRequired());
											  }
										  if(destaccessInfo.getOriginShuttleRequired()!=null){
											  tempinfo.setOriginShuttleRequired(destaccessInfo.getOriginShuttleRequired());
											  }
										  if(destaccessInfo.getOriginAdditionalStopDetails()!=null){
											  tempinfo.setOriginAdditionalStopDetails(destaccessInfo.getOriginAdditionalStopDetails());
											  }
										 // tempinfo.setDestinationAdditionalStopDetails();
										  tempinfo.setCorpID(sessionCorpID);
											 // newContract.setOrigContract(contract.getId());
											 // newContract.setCompanyDivision("");
											 // newContract.setId(null);
											 // contractManager.save(newContract);
										  tempinfo.setCustomerFileId(null);
										  tempinfo.setServiceOrderId(destaccessInfo.getServiceOrderId());
										  tempinfo.setId(sacessinfoid);
										  if(destaccessInfo.getNetworkSynchedId()==null){
											  tempinfo.setNetworkSynchedId(null);
										  }else{
											  tempinfo.setNetworkSynchedId(destaccessInfo.getNetworkSynchedId());
										  }
										  tempinfo=accessInfoManager.save(tempinfo);
										  destaccessInfo = tempinfo;
										  
										   ServiceOrder soObj = serviceOrderManager.get(destaccessInfo.getServiceOrderId());
										   TrackingStatus ts = trackingStatusManager.get(destaccessInfo.getServiceOrderId());
										   if(soObj.getIsNetworkRecord() && (ts.getSoNetworkGroup() || company.getSurveyLinking())){
												List linkedShipNumber=findLinkerShipNumber(soObj);
												List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,soObj);
												 destaccessInfo.setNetworkSynchedId(destaccessInfo.getId());
												   destaccessInfo=accessInfoManager.save(destaccessInfo);
												synchornizeAccessInfoSO(serviceOrderRecords,destaccessInfo,true);
											}
							  }
								  
							  }
								  else{
									  if(destaccessInfo.getNetworkSynchedId()==null){
										  destaccessInfo.setNetworkSynchedId(null);
									  }else{
										  destaccessInfo.setNetworkSynchedId(destaccessInfo.getNetworkSynchedId());
									  }
									  destaccessInfo=accessInfoManager.save(destaccessInfo);  
									 
									  
								  }*/
							  destaccessInfo=accessInfoManager.save(destaccessInfo);
							  ServiceOrder soObj = serviceOrderManager.get(destaccessInfo.getServiceOrderId());
							   TrackingStatus ts = trackingStatusManager.get(destaccessInfo.getServiceOrderId());
							   if(soObj.getIsNetworkRecord() && (ts.getSoNetworkGroup() || company.getSurveyLinking())){
									List linkedShipNumber=findLinkerShipNumber(soObj);
									linkedShipNumber.remove(soObj.getShipNumber());
									List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,soObj);
									 destaccessInfo.setNetworkSynchedId(destaccessInfo.getId());
									   destaccessInfo=accessInfoManager.save(destaccessInfo);
									   if(sacessinfoid!=null){
										   synchornizeAccessInfoSO(serviceOrderRecords,destaccessInfo,false);
									   }else{
										   synchornizeAccessInfoSO(serviceOrderRecords,destaccessInfo,true);
									   }
								}
							 // destaccessInfo=accessInfoManager.save(destaccessInfo);	
							  
							 List inventoryPathList = inventoryPathManager.getListByLocId( sourceaccessInfo.getId(),"accessInfoId");
							 if(inventoryPathList!=null && !inventoryPathList.isEmpty() && inventoryPathList.get(0)!=null){
								inventoryPathManager.deleteAllWorkTicketInventory("accessInfoId", sessionCorpID, destaccessInfo.getId());
							 }
							 for (Object object : inventoryPathList) {
								try {
									InventoryPath invPath = (InventoryPath) object;
									InventoryPath invNew = new InventoryPath();
									BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
									beanUtils.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
									beanUtils.copyProperties(invNew, invPath);
									invNew.setId(null);
									invNew.setAccessInfoId(destaccessInfo.getId());
									invNew = inventoryPathManager.save(invNew);
									customerFile = customerFileManager.get(cid);
									if(customerFile.getIsNetworkRecord()!=null && customerFile.getIsNetworkRecord() && ((customerFile.getContractType()!=null && (customerFile.getContractType().equals("CMM") ||customerFile.getContractType().equals("DMM"))) || company.getSurveyLinking())){
										invNew.setNetworkSynchedId(invNew.getId());
										invNew = inventoryPathManager.save(invNew);
										List linkedSequenceNumber=findLinkerSequenceNumber(customerFile);
										Set set = new HashSet(linkedSequenceNumber);
										List linkedSeqNum = new ArrayList(set);
										List<Object> records=findRecords(linkedSeqNum,customerFile);
										records.remove(customerFile);
										synchornizeInventoryPathData(records,invNew,true,"CF","AccInfo",id);
									}
									
								} catch (IllegalAccessException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						
					}
				}
			}
		}
	}
	}
	
	
	hitflag="1";	
return SUCCESS;
}

public String getUploadedImagesAndUpload(){
	inventoryData = inventoryDataManager.get(id);
 List	upLoadedImages=inventoryPathManager.getListByInventoryId(id);
	int size=upLoadedImages.size();
	if(size>1){
	   	if((size%2==0)){
	   		 for(int i=0;i<size/2;i++){
	   			this.upLoadedImages.add(upLoadedImages.get(i));
	   		 }
	   		for(int i=size/2;i<size;i++){
	   			this.upLoadedImages2.add(upLoadedImages.get(i));
	   		    }
	   	   }else{
	   		   int y=size/2;
	   		for(int i=0;i<y;i++){
	   			this.upLoadedImages.add(upLoadedImages.get(i));
	   		 }
	   		for(int i=y;i<size;i++){
	   			this.upLoadedImages2.add(upLoadedImages.get(i));
	   		    }
	   	       }
	 }else{
		 this.upLoadedImages=upLoadedImages;
	 }
	return SUCCESS;
}


public String getUploadedInvImagesAndUpload(){
	inventoryData = inventoryDataManager.get(id);
	 List	upLoadedImages=inventoryPathManager.getListByLocId(id,fieldName);
		int size=upLoadedImages.size();
		if(size>1){
		   	if((size%2==0)){
		   		 for(int i=0;i<size/2;i++){
		   			this.upLoadedImages.add(upLoadedImages.get(i));
		   		 }
		   		for(int i=size/2;i<size;i++){
		   			this.upLoadedImages2.add(upLoadedImages.get(i));
		   		    }
		   	   }else{
		   		   int y=size/2;
		   		for(int i=0;i<y;i++){
		   			this.upLoadedImages.add(upLoadedImages.get(i));
		   		 }
		   		for(int i=y;i<size;i++){
		   			this.upLoadedImages2.add(upLoadedImages.get(i));
		   		    }
		   	       }
		 }else{
			 this.upLoadedImages=upLoadedImages;
		 }
		return SUCCESS;
	}

public String getPackingUploadedImagesAndUpload(){
	InventoryData inventoryData = inventoryDataManager.get(id);
	 List	upLoadedImages=inventoryPathManager.getInventoryPackingImg(id);
		int size=upLoadedImages.size();
		if(size>1){
		   	if((size%2==0)){
		   		 for(int i=0;i<size/2;i++){
		   			this.upLoadedImages.add(upLoadedImages.get(i));
		   		 }
		   		for(int i=size/2;i<size;i++){
		   			this.upLoadedImages2.add(upLoadedImages.get(i));
		   		    }
		   	   }else{
		   		   int y=size/2;
		   		for(int i=0;i<y;i++){
		   			this.upLoadedImages.add(upLoadedImages.get(i));
		   		 }
		   		for(int i=y;i<size;i++){
		   			this.upLoadedImages2.add(upLoadedImages.get(i));
		   		    }
		   	       }
		 }else{
			 this.upLoadedImages=upLoadedImages;
		 }
		return SUCCESS;
	}

public String getAccessInfoImages(){
	 accessInfo = inventoryDataManager.getAccessinfoByCid(cid, sessionCorpID);
	 List	upLoadedImages=inventoryPathManager.getListByAccessInfoId(id,accessInfoType);
	 if(upLoadedImages.isEmpty()){
		 upLoadedImages=inventoryPathManager.getListByAccessInfoId(cid,accessInfoType);
	 }
		int size=upLoadedImages.size();
		if(size>1){
		   	if((size%2==0)){
		   		 for(int i=0;i<size/2;i++){
		   			this.upLoadedImages.add(upLoadedImages.get(i));
		   		 }
		   		for(int i=size/2;i<size;i++){
		   			this.upLoadedImages2.add(upLoadedImages.get(i));
		   		    }
		   	   }else{
		   		   int y=size/2;
		   		for(int i=0;i<y;i++){
		   			this.upLoadedImages.add(upLoadedImages.get(i));
		   		 }
		   		for(int i=y;i<size;i++){
		   			this.upLoadedImages2.add(upLoadedImages.get(i));
		   		    }
		   	       }
		 }else{
			 this.upLoadedImages=upLoadedImages;
		 }
	
		return SUCCESS;
	}

public String imageSaveAccessInfo(){
	List list1 = companyManager.findByCorpID(sessionCorpID);
	if(list1!=null && !list1.isEmpty() && list1.get(0)!=null){
		company = (Company) list1.get(0);
	}
	 accessInfo =  inventoryDataManager.getAccessinfoByCid(cid, sessionCorpID);
	customerFile=customerFileManager.get(cid);
	getRequest().setAttribute("soLastName", customerFile.getLastName());
	//ServletContext servletContext = ServletActionContext.getServletContext(); 
	String dataDir ="/usr/local/redskydoc/voxme/images";
	File savedFile = new File(dataDir+"/"+customerFile.getSequenceNumber());
	 if (!savedFile.exists()) {
		 savedFile.mkdirs();
     }
	 if(attachment.exists()){
		 try{
	        InputStream stream = new FileInputStream(attachment);
	        
	        OutputStream bos = new FileOutputStream(savedFile.getAbsolutePath() +"/"+ attachmentFileName);
	        int bytesRead = 0;
	        byte[] buffer = new byte[8192];

	        while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
	            bos.write(buffer, 0, bytesRead);
	        }

	        bos.close();  
	        stream.close();
	        inventoryPath =new InventoryPath();
	        inventoryPath.setCorpID(sessionCorpID);
	        inventoryPath.setAccessInfoId(id);
	        inventoryPath.setAccessInfoType(accessInfoType);
	        inventoryPath.setPath(savedFile.getAbsolutePath() +"/" + attachmentFileName);
	        inventoryPath= inventoryPathManager.save(inventoryPath);
	        if(customerFile.getIsNetworkRecord()!=null && customerFile.getIsNetworkRecord() && ((customerFile.getContractType()!=null && (customerFile.getContractType().equals("CMM") ||customerFile.getContractType().equals("DMM"))) || company.getSurveyLinking())){
	        	inventoryPath.setNetworkSynchedId(inventoryPath.getId());
	        	 inventoryPath= inventoryPathManager.save(inventoryPath);
					List linkedSequenceNumber=findLinkerSequenceNumber(customerFile);
					Set set = new HashSet(linkedSequenceNumber);
					List linkedSeqNum = new ArrayList(set);
					List<Object> records=findRecords(linkedSeqNum,customerFile);
					records.remove(customerFile);
					synchornizeInventoryPathData(records,inventoryPath,true,"CF","AccInfo",id);
			}
		         }catch (Exception e) {
		        	 e.printStackTrace();
		        	 logger.warn("error is :"+e);
		       }
	        }
	attachment.renameTo(savedFile);
	hitflag="1";
	return SUCCESS;
}


private void synchornizeInventoryPathData(List<Object> records,	InventoryPath inventoryPathOld, boolean isNew,String flag,String module,Long modelId) {
	inventoryPathOld.setNetworkSynchedId(inventoryPathOld.getNetworkSynchedId());
	inventoryPathOld =  inventoryPathManager.save(inventoryPathOld);
	Iterator  it=records.iterator();
	if(module.equals("AccInfo")){
		if(flag.equals("CF")){
		while(it.hasNext()){
			CustomerFile customerFileToRecods=(CustomerFile)it.next();
			InventoryPath inventoryPathNew = new InventoryPath();
			inventoryPathManager.deleteLInkedImgs(inventoryPathOld.getId()); 
			//AccessInfo accInfo = inventoryDataManager.getAccessinfoByCid(customerFileToRecods.getId(),customerFileToRecods.getCorpID());
			AccessInfo accInfo = inventoryDataManager.getAccessinfoByNetworkSyncId(inventoryPathOld.getAccessInfoId(),customerFileToRecods.getCorpID());
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(inventoryPathNew, inventoryPathOld);
				 inventoryPathNew.setId(null);
				 inventoryPathNew.setAccessInfoId(accInfo.getId());
				 inventoryPathNew.setNetworkSynchedId(inventoryPathOld.getId());
				 inventoryPathNew.setCorpID(customerFileToRecods.getCorpID());
				 inventoryPathManager.save(inventoryPathNew);
				 
			}catch(Exception ex){
				String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
		    	  String logMethod =   ex.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , ex.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 ex.printStackTrace();
			}
		}
		
	}else if(flag.equals("SO")){/*
		while(it.hasNext()){
			ServiceOrder so=(ServiceOrder)it.next();
			InventoryPath inventoryPathNew = new InventoryPath();
			AccessInfo accInfo = inventoryDataManager.getAccessinfoByCid(customerFileToRecods.getId(),customerFileToRecods.getCorpID());
			
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(inventoryPathNew, inventoryPathOld);
				 inventoryPathNew.setId(null);
				 inventoryPathNew.setAccessInfoId(accInfo.getId());
				 inventoryPathNew.setCorpID(customerFileToRecods.getCorpID());
				 inventoryPathManager.save(inventoryPathNew);
				 
			}catch(Exception ex){
				String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
		    	  String logMethod =   ex.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , ex.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 ex.printStackTrace();
			}
		}
	*/}
}else if(module.equals("InvData")){
		if(flag.equals("CF")){
		while(it.hasNext()){
			CustomerFile customerFileToRecods=(CustomerFile)it.next();
			InventoryPath inventoryPathNew = new InventoryPath();
			InventoryData inventoryDataNew =  (InventoryData) inventoryDataManager.getLinkedInventoryData(modelId,customerFileToRecods.getId(),null);
			
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(inventoryPathNew, inventoryPathOld);
				 inventoryPathNew.setId(null);
				 inventoryPathNew.setInventoryDataId(inventoryDataNew.getId());
				 inventoryPathNew.setCorpID(customerFileToRecods.getCorpID());
				 inventoryPathNew.setNetworkSynchedId(inventoryPathOld.getId());
				 inventoryPathManager.save(inventoryPathNew);
				 
			}catch(Exception ex){
				String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
		    	  String logMethod =   ex.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , ex.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 ex.printStackTrace();
			}
		}
		
	}else if(flag.equals("SO")){
		while(it.hasNext()){
			ServiceOrder so=(ServiceOrder)it.next();
			InventoryPath inventoryPathNew = new InventoryPath();
			InventoryData inventoryDataNew =  (InventoryData) inventoryDataManager.getLinkedInventoryData(modelId,so.getCustomerFileId(),so.getId());
			
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(inventoryPathNew, inventoryPathOld);
				 inventoryPathNew.setId(null);
				 inventoryPathNew.setInventoryDataId(inventoryDataNew.getId());
				 inventoryPathNew.setNetworkSynchedId(inventoryPathOld.getId());
				 inventoryPathNew.setCorpID(so.getCorpID());
				 inventoryPathManager.save(inventoryPathNew);
				 
			}catch(Exception ex){
				String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
		    	  String logMethod =   ex.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , ex.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 ex.printStackTrace();
			}
		}
	}
}
}




@SkipValidation
public String syncSoWithMobileMoverMethod(){
	 // ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("/WEB-INF/applicationContext.xml");
	 WebApplicationContext  ctx=WebApplicationContextUtils.getWebApplicationContext(ServletActionContext.getServletContext()); 
	 MobileMoverAction mobileMoverAction = (MobileMoverAction) ctx.getBean("mobileMoverAction");
	 customerFile  = customerFileManager.get(cid);
	 List<ServiceOrder> SoObjList = customerFileManager.getServiceOrderList(cid);
	 for (ServiceOrder serviceOrder : SoObjList) {
	   List tempList = serviceOrderManager.findShipNumber(serviceOrder.getShipNumber());
	   if(tempList!=null && !tempList.isEmpty()){
	    try {
	     mobileMoverAction.setSid(serviceOrder.getId());
	     List <Long> workTicketIdList = inventoryDataManager.getWorkTicketIdList(serviceOrder.getShipNumber(),sessionCorpID);
	     for (Long wktId : workTicketIdList) {
	      mobileMoverAction.setWktId(wktId);
	      mobileMoverAction.updateRedSkyByMobileMover();
	      customerFile.setLastRunMMTime(new Date());
	      customerFile = customerFileManager.save(customerFile);
	    }
	   } catch (Exception e) {
	    e.printStackTrace();
	   }
	    
	   }
	 }
	 
	  idURL ="?cid="+cid;
	 return SUCCESS ;}

public String packingImageSave(){
	List list1 = companyManager.findByCorpID(sessionCorpID);
	if(list1!=null && !list1.isEmpty() && list1.get(0)!=null){
		company = (Company) list1.get(0);
	}
	System.out.print(sid+"-------------------+"+cid);
	if(sid!= null && cid== null){
		serviceOrder = serviceOrderManager.get(sid);
		cid = serviceOrder.getCustomerFileId();
	}
	customerFile=customerFileManager.get(cid);
	getRequest().setAttribute("soLastName", customerFile.getLastName());
	//ServletContext servletContext = ServletActionContext.getServletContext(); 
	String dataDir ="/usr/local/redskydoc/voxme/images";
	File savedFile = new File(dataDir+"/"+customerFile.getSequenceNumber());
	 if (!savedFile.exists()) {
		 savedFile.mkdirs();
     }
	 if(attachment.exists()){
		 try{
	        InputStream stream = new FileInputStream(attachment);
	        
	        OutputStream bos = new FileOutputStream(savedFile.getAbsolutePath() +"/"+ attachmentFileName);
	        int bytesRead = 0;
	        byte[] buffer = new byte[8192];

	        while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
	            bos.write(buffer, 0, bytesRead);
	        }

	        bos.close();  
	        stream.close();
	        inventoryPath =new InventoryPath();
	        inventoryPath.setCorpID(sessionCorpID);
	        inventoryPath.setInventoryPackingId(id);
	        inventoryPath.setPath(savedFile.getAbsolutePath() +"/" + attachmentFileName);	        
	        inventoryPathManager.save(inventoryPath);
	        inventoryPacking=inventoryPackingManager.get(id);
	        inventoryPacking.setImagesAvailablityFlag("1000");
	        inventoryPackingManager.save(inventoryPacking);
	        
		         }catch (Exception e) {
		        	 e.printStackTrace();
		       }
	        }
	attachment.renameTo(savedFile);
	chkSave="saved";
	hitflag="1";
	return SUCCESS;
}


public String imageSave(){
	List list1 = companyManager.findByCorpID(sessionCorpID);
	if(list1!=null && !list1.isEmpty() && list1.get(0)!=null){
		company = (Company) list1.get(0);
	}
	System.out.print(sid+"-------------------+"+cid);
	if(sid!= null && cid== null){
		serviceOrder = serviceOrderManager.get(sid);
		cid = serviceOrder.getCustomerFileId();
	}
	customerFile=customerFileManager.get(cid);
	getRequest().setAttribute("soLastName", customerFile.getLastName());
	//ServletContext servletContext = ServletActionContext.getServletContext(); 
	String dataDir ="/usr/local/redskydoc/voxme/images";
	File savedFile = new File(dataDir+"/"+customerFile.getSequenceNumber());
	 if (!savedFile.exists()) {
		 savedFile.mkdirs();
     }
	 if(attachment.exists()){
		 try{
	        InputStream stream = new FileInputStream(attachment);
	        
	        OutputStream bos = new FileOutputStream(savedFile.getAbsolutePath() +"/"+ attachmentFileName);
	        int bytesRead = 0;
	        byte[] buffer = new byte[8192];

	        while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
	            bos.write(buffer, 0, bytesRead);
	        }

	        bos.close();  
	        stream.close();
	        inventoryPath =new InventoryPath();
	        inventoryPath.setCorpID(sessionCorpID);
	        inventoryPath.setInventoryDataId(id);
	        inventoryPath.setPath(savedFile.getAbsolutePath() +"/" + attachmentFileName);	        
	        inventoryPath = inventoryPathManager.save(inventoryPath);
	        inventoryData=inventoryDataManager.get(id);
	        inventoryData.setImagesAvailablityFlag(true);
	        inventoryDataManager.save(inventoryData);
	        if(serviceOrder!= null ){
	        	TrackingStatus ts  = trackingStatusManager.get(sid);
	        if( serviceOrder.getIsNetworkRecord() && (company.getSurveyLinking() || ts.getSoNetworkGroup())){
	        	inventoryPath.setNetworkSynchedId(inventoryPath.getId());
	        	 inventoryPath= inventoryPathManager.save(inventoryPath);
				List linkedShipNumber=findLinkerShipNumber(serviceOrder);
				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
				serviceOrderRecords.remove(serviceOrder);
				synchornizeInventoryPathData(serviceOrderRecords,inventoryPath,true,"SO","InvData",id);
		}
	        }else{
	        	if(customerFile.getIsNetworkRecord()!=null && customerFile.getIsNetworkRecord() && ((customerFile.getContractType()!=null && (customerFile.getContractType().equals("CMM") ||customerFile.getContractType().equals("DMM"))) || company.getSurveyLinking())){
		        	inventoryPath.setNetworkSynchedId(inventoryPath.getId());
		        	 inventoryPath= inventoryPathManager.save(inventoryPath);
						List linkedSequenceNumber=findLinkerSequenceNumber(customerFile);
						Set set = new HashSet(linkedSequenceNumber);
						List linkedSeqNum = new ArrayList(set);
						List<Object> records=findRecords(linkedSeqNum,customerFile);
						records.remove(customerFile);
						synchornizeInventoryPathData(records,inventoryPath,true,"CF","InvData",id);
				}
	        }
	        
		         }catch (Exception e) {
		        	 e.printStackTrace();
		       }
	        }
	attachment.renameTo(savedFile);
	chkSave="saved";
	hitflag="1";
	return SUCCESS;
}


@SkipValidation
public String inventoryPreConditionMethod(){
	try {
		preConditionList = inventoryLocationManager.getAllLocation(sessionCorpID,shipno);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return SUCCESS;
}

@SkipValidation
public String roomConditionsMethod(){
	try {
		inventoryRoomsList = roomManager.getAllRoom(sessionCorpID,shipno);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return SUCCESS;
}

@SkipValidation
public String ticketInventoryDataMethod(){
	try {
		ticketInvData = workTicketInventoryDataManager.getAllInventory(sessionCorpID,shipno);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return SUCCESS;	
}
private String getTextValue(Element ele, String tagName) {
	String textVal = null;
	NodeList nl = ele.getElementsByTagName(tagName);
	if(nl != null && nl.getLength() > 0) {
		Element el = (Element)nl.item(0);
		if(el.getFirstChild()!=null){
			textVal = el.getFirstChild().getNodeValue();
		}	
	}

	return textVal;
}
private String getBoxAttributeValue(Element ele, String tagName) {
	String textVal = null;
	NodeList nl = ele.getElementsByTagName(tagName);
	if(nl != null && nl.getLength() > 0) {
		Element el = (Element)nl.item(0);
		if(el.getAttribute("name")!=null){
			textVal = el.getAttribute("name");
		}	
	}

	return textVal;
}
public String fetchXmlforInventoryData(){
	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	String files;
	String dataDir = "/home/redskyuser/redint/voxmeInventory/survey_email/";
	File voxmeInventoryFolder = new File(dataDir);
	File voxmeInventoryFetchedFolder= new File(dataDir.replace("voxmeInventory", "voxmeInventoryFetched"));
	 if (!voxmeInventoryFolder.exists()) {
		 voxmeInventoryFolder.mkdirs();
     }
	 if (!voxmeInventoryFetchedFolder.exists()) {
		 voxmeInventoryFetchedFolder.mkdirs();
     }
     File[] listOfDirectory = voxmeInventoryFolder.listFiles(); 
     for (int i = 0; i < listOfDirectory.length; i++) 
	  {
    	 if (listOfDirectory[i].isDirectory()) 
		   {
    		 File[] listOfFiles=listOfDirectory[i].listFiles();			  
			  for (int z = 0; z < listOfFiles.length; z++) 
			  {
				
				  if (listOfFiles[z].isFile()) 
				   {
					  files = listOfFiles[z].getName();
				       if (files.endsWith(".xml"))
				       {
				    	  try{
				    		    DocumentBuilder db = dbf.newDocumentBuilder();
					      		Document dom = db.parse(listOfFiles[z].getAbsolutePath());	
					    		Element docEle = dom.getDocumentElement();
					    		NodeList nl = docEle.getElementsByTagName("InventoryData");
					    		if(nl != null && nl.getLength() > 0) {
					    			Element elePiece = (Element)nl.item(0);
					    			String tempOrgId = (((Element)dom.getElementsByTagName("OrgID").item(0)).getFirstChild().getNodeValue()).toString().trim().toUpperCase();
					    			String tempEmfId = (((Element)dom.getElementsByTagName("EMFID").item(0)).getFirstChild().getNodeValue()).toString().trim();
					    			String tempSID = inventoryDataManager.findServiceOrderId(tempOrgId,tempEmfId);
					    			if(tempSID!=null && !(tempSID.equals(""))){
					    			NodeList inventoryList=elePiece.getElementsByTagName("Piece");
						    		if(inventoryList != null && inventoryList.getLength() > 0) {
						    			for(int p = 0 ; p < inventoryList.getLength();p++) {
						    				Element eleInventoryPacking = (Element)inventoryList.item(p);
						    				NodeList itemList=eleInventoryPacking.getElementsByTagName("Item");						    				
						    				if(itemList != null && itemList.getLength() > 0) {
						    					for(int pp = 0 ; pp < itemList.getLength();pp++) {
						    					Element elItem=(Element)itemList.item(pp);
						    					inventoryPacking = new InventoryPacking();
						    					inventoryPacking.setItem(elItem.getAttribute("name"));
						    					inventoryPacking.setItemQuantity(getTextValue(elItem,"Quantity"));
						    					inventoryPacking.setItemType(getTextValue(elItem,"Type"));
						    					try{
						    						inventoryPacking.setValue((new Double(getTextValue(elItem,"Value"))).intValue());
								    				}catch(Exception e){
								    					e.printStackTrace();
								    					inventoryPacking.setValue(0);
								    				}
								    			inventoryPacking.setComment(getTextValue(elItem,"Comment"));
								    			inventoryPacking.setConditon(getTextValue(elItem,"Condition"));
								    			inventoryPacking.setDismantling(new Boolean(getTextValue(elItem,"Dismantling")));
								    			inventoryPacking.setAssembling(new Boolean(getTextValue(elItem,"Assembling")));
								    			inventoryPacking.setSpecialContainer(new Boolean(getTextValue(elItem,"SpecialContainer")));
								    			inventoryPacking.setMake(getTextValue(elItem,"Make"));
								    			inventoryPacking.setModel(getTextValue(elItem,"Model"));
								    			inventoryPacking.setYear(getTextValue(elItem,"Year"));
								    			inventoryPacking.setSerialnumber(getTextValue(elItem,"SerialNumber"));
								    			
								    			NodeList sizeList=elItem.getElementsByTagName("Size");						    				
									    		if(sizeList != null && sizeList.getLength() > 0) {
									    		   Element elsize=(Element)sizeList.item(0);
									    		   try{
							    						inventoryPacking.setWidth((new Double(getTextValue(elsize,"Width"))).intValue());
									    				}catch(Exception e){
									    					inventoryPacking.setWidth(0);
									    					e.printStackTrace();
									    				}
									    				 try{
									    					inventoryPacking.setLength((new Double(getTextValue(elsize,"Length"))).intValue());
											    			}catch(Exception e){
											    				inventoryPacking.setLength(0);
											    				e.printStackTrace();
											    			}
											    			try{
										    					inventoryPacking.setHeight((new Double(getTextValue(elsize,"Height"))).intValue());
												    			}catch(Exception e){
												    				inventoryPacking.setHeight(0);
												    			}		
									    		}
						    						    				
						    				//inventoryPacking.setServiceOrderID(new Long(((Element)dom.getElementsByTagName("MovingData").item(0)).getAttribute("ID")));
						    				//inventoryPacking.setCorpID(((Element)dom.getElementsByTagName("OrgID").item(0)).getFirstChild().getNodeValue());
									    	inventoryPacking.setServiceOrderID(new Long(tempSID));
									    	inventoryPacking.setCorpID(tempOrgId.substring(0, 4));
						    			    inventoryPacking.setCreatedOn(new Date());
						    				inventoryPacking.setUpdatedOn(new Date());
						    				try{
						    				inventoryPacking.setPieceID(new Integer(eleInventoryPacking.getAttribute("id")));
						    				}catch (Exception e) {	
						    					e.printStackTrace();
											}
						    				inventoryPacking.setCreatedBy("Crew-XML");
						    				inventoryPacking.setUpdatedBy("Crew-XML");
						    				inventoryPacking.setLocation(getTextValue(eleInventoryPacking,"Location"));
						    				inventoryPacking.setVolume(getTextValue(eleInventoryPacking,"Volume"));						    				
						    				try{
						    					inventoryPacking.setWeight((new Double(getTextValue(eleInventoryPacking,"Weight"))).intValue());
							    				}catch(Exception e){
							    					inventoryPacking.setWeight(0);
							    					e.printStackTrace();
							    				}
							    		    inventoryPacking.setMode(getTextValue(eleInventoryPacking,"Destination"));
							    		    NodeList boxList=eleInventoryPacking.getElementsByTagName("Box");						    				
						    				if(boxList != null && boxList.getLength() > 0) {
						    					Element elBox=(Element)boxList.item(0);
						    					inventoryPacking.setQuantity(getTextValue(elBox,"Quantity"));
						    					inventoryPacking.setBoxName(elBox.getAttribute("name"));
						    				}
						    				 				
							    				inventoryPackingManager.save(inventoryPacking);
						    				}
						    				}
						    			}
					    			  }
					    			}else{
					    				//String message="Sorry! Service order is present.";
					    				//errorMessage(getText(message));
					    			}
					    		}
					    		
				    	  }catch(ParserConfigurationException pce) {
					      		pce.printStackTrace();
				      	    }catch(SAXException se) {
				      		se.printStackTrace();
				      	   }catch(IOException ioe) {
				      		ioe.printStackTrace();
				         	  }	
				       }
					  
				   }
			  }  
			    String str=  listOfDirectory[i].getAbsolutePath();
				str=str.replace("voxmeInventory", "voxmeInventoryFetched");
				listOfDirectory[i].renameTo(new File(str));
		   }
    	 
	  }
	return SUCCESS;
}

public String fetchAccesInfoFromXML(){
	inventoryData= new InventoryData();
	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	String files;
	//ServletContext servletContext = ServletActionContext.getServletContext(); 
	String dataDir = "/home/redskyuser/redint/voxme/survey_email/";
	//dataDir=dataDir.replace("redsky","userData").replace("images","voxmeIntegrationXML");
	File voxmeFolder = new File(dataDir);
	File voxmeFetchedFolder= new File(dataDir.replace("voxme", "voxmeFetched"));
	 if (!voxmeFolder.exists()) {
		 voxmeFolder.mkdirs();
     }
	 if (!voxmeFetchedFolder.exists()) {
		 voxmeFetchedFolder.mkdirs();
     }
     File[] listOfDirectory = voxmeFolder.listFiles(); 	 
	  for (int i = 0; i < listOfDirectory.length; i++) 
	  {	 
		  if (listOfDirectory[i].isDirectory()) 
		   {
			  File[] listOfFiles=listOfDirectory[i].listFiles();
			  
			  for (int z = 0; z < listOfFiles.length; z++) 
			  {
				
				  if (listOfFiles[z].isFile()) 
				   {
				   files = listOfFiles[z].getName();
				       if (files.endsWith(".xml"))
				       {
				          try {
				      		//Using factory get an instance of document builder
				      		DocumentBuilder db = dbf.newDocumentBuilder();
				      		//parse using builder to get DOM representation of the XML file
				      		Document dom = db.parse(listOfFiles[z].getAbsolutePath());	
				    		Element docEle = dom.getDocumentElement();
				    		NodeList nl = docEle.getElementsByTagName("AccessInfo");
				    		NodeList nl2 = docEle.getElementsByTagName("MovingData");
				    		accessInfo= new AccessInfo();
				    		accessInfo.setId(new Long(((Element)dom.getElementsByTagName("MovingData").item(0)).getAttribute("ID")));
				    		//accessInfo.setCorpID(docEle.getElementsByTagName("EMFID").item(0).getFirstChild().getNodeValue().substring(0,4));
				    		//accessInfo.setCorpID("SSCW");
				    		String EstimatorName = "";
				    		String signatureImagePath="";
				    		NodeList nlGeneralInfo = docEle.getElementsByTagName("GeneralInfo");
				    		if(nlGeneralInfo != null && nlGeneralInfo.getLength() > 0) {
			    				Element el = (Element)nlGeneralInfo.item(0);
			    				EstimatorName=getTextValue(el,"EstimatorName");
			    				signatureImagePath=getTextValue(el,"SurveySignature");
			    				}
				    		
				    		try{
				    		//accessInfo.setCorpID(((Element)dom.getElementsByTagName("OrgID").item(0)).getFirstChild().getNodeValue());
				    			accessInfo.setCorpID("SSCW");
				    		}catch(Exception e){
				    		accessInfo.setCorpID(sessionCorpID);
				    		e.printStackTrace();
				    		}
				    		if(nl != null && nl.getLength() > 0) {
				    				Element el = (Element)nl.item(0);
				    				Element el2 = (Element)nl.item(1);
				    				accessInfo.setOriginResidenceType(getTextValue(el,"PropertyType"));
				    				accessInfo.setOriginResidenceSize(getTextValue(el,"PropertySize"));
				    				accessInfo.setOriginLongCarry(new Boolean(getTextValue(el,"CarryRequired")));
				    				accessInfo.setOriginCarryDistance(getTextValue(el,"CarryLength"));
				    				accessInfo.setOriginCarrydetails(getTextValue(el,"CarryDescription"));
				    				accessInfo.setOriginStairCarry(new Boolean(getTextValue(el,"StairCarryRequired")));
				    				accessInfo.setOriginStairDistance(getTextValue(el,"StairCarryLength"));
				    				accessInfo.setOriginStairdetails(getTextValue(el,"StairCarryDescription"));
				    				accessInfo.setOriginfloor(getTextValue(el,"Floor"));
				    				accessInfo.setOriginNeedCrane(new Boolean(getTextValue(el,"NeedCrane")));
				    				accessInfo.setOriginElevator(new Boolean(getTextValue(el,"HasElevator")));
				    				accessInfo.setOriginElevatorDetails(getTextValue(el,"ElevatorDetails"));
				    				accessInfo.setOriginElevatorType(getTextValue(el,"ExternalElevatorType"));
				    				accessInfo.setOriginReserveParking(new Boolean(getTextValue(el,"ParkingReservationRequired")));
				    				accessInfo.setOriginParkingType(getTextValue(el,"ParkingType"));
				    				accessInfo.setOriginParkinglotsize(getTextValue(el,"ParkingSpotSize"));
				    				accessInfo.setOriginParkingSpots(getTextValue(el,"NumOfParkingSpots"));
				    				accessInfo.setOriginDistanceToParking(getTextValue(el,"DistanceToParking"));
				    				accessInfo.setOriginShuttleRequired(new Boolean(getTextValue(el,"shuttleCarryRequired")));
				    				accessInfo.setOriginShuttleDistance(getTextValue(el,"ShuttleDistance"));
				    				accessInfo.setOriginAdditionalStopRequired(new Boolean(getTextValue(el,"AdditionalStopRequired")));
				    				accessInfo.setOriginAdditionalStopDetails(getTextValue(el,"AdditionalStop"));
				    				//destination
				    				accessInfo.setDestinationResidenceType(getTextValue(el2,"PropertyType"));
				    				accessInfo.setDestinationResidenceSize(getTextValue(el2,"PropertySize"));
				    				accessInfo.setDestinationLongCarry(new Boolean(getTextValue(el2,"CarryRequired")));
				    				accessInfo.setDestinationCarryDistance(getTextValue(el2,"CarryLength"));
				    				accessInfo.setDestinationCarrydetails(getTextValue(el2,"CarryDescription"));
				    				accessInfo.setDestinationStairCarry(new Boolean(getTextValue(el2,"StairCarryRequired")));
				    				accessInfo.setDestinationStairDistance(getTextValue(el2,"StairCarryLength"));
				    				accessInfo.setDestinationStairdetails(getTextValue(el2,"StairCarryDescription"));
				    				accessInfo.setDestinationfloor(getTextValue(el2,"Floor"));
				    				accessInfo.setDestinationNeedCrane(new Boolean(getTextValue(el2,"NeedCrane")));
				    				accessInfo.setDestinationElevator(new Boolean(getTextValue(el2,"HasElevator")));
				    				accessInfo.setDestinationElevatorDetails(getTextValue(el2,"ElevatorDetails"));
				    				accessInfo.setDestinationElevatorType(getTextValue(el2,"ExternalElevatorType"));
				    				accessInfo.setDestinationReserveParking(new Boolean(getTextValue(el2,"ParkingReservationRequired")));
				    				accessInfo.setDestinationParkingType(getTextValue(el2,"ParkingType"));
				    				accessInfo.setDestinationParkinglotsize(getTextValue(el2,"ParkingSpotSize"));
				    				accessInfo.setDestinationParkingSpots(getTextValue(el2,"NumOfParkingSpots"));
				    				accessInfo.setDestinationDistanceToParking(getTextValue(el2,"DistanceToParking"));
				    				accessInfo.setDestinationShuttleRequired(new Boolean(getTextValue(el2,"shuttleCarryRequired")));
				    				accessInfo.setDestinationShuttleDistance(getTextValue(el2,"ShuttleDistance"));
				    				accessInfo.setDestinationAdditionalStopRequired(new Boolean(getTextValue(el2,"AdditionalStopRequired")));
				    				accessInfo.setDestinationAdditionalStopDetails(getTextValue(el2,"AdditionalStop"));
				    				accessInfo=accessInfoManager.save(accessInfo);	
				    				if(getTextValue(el,"PictureFileName")!=null){
				    					String imageName=getTextValue(el,"PictureFileName");
				    					if((imageName!=null)&&!imageName.equals("")){
				    						String imagesDir ="/usr/local/redskydoc/voxme/images";
				    						customerFile = customerFileManager.get(accessInfo.getId());
				    						File   savedFile = new File(imagesDir+"/"+customerFile.getSequenceNumber());	    						   					   
				    						 if (!savedFile.exists()) {
				    							 savedFile.mkdirs();
				    						} 
				    						String[] imageNameList=imageName.split(";");
				    						if(imageNameList.length>=1){
				    							for(int p=0;p<imageNameList.length;p++){
				    						    inventoryPath =new InventoryPath();
				    					        inventoryPath.setCorpID(accessInfo.getCorpID());
				    					        inventoryPath.setAccessInfoId(accessInfo.getId());
				    					        inventoryPath.setAccessInfoType("origin");
				    					        inventoryPath.setPath(listOfDirectory[i].getAbsolutePath().replace("voxme", "voxmeFetched")+"/"+ imageNameList[p]);
				    					        inventoryPathManager.save(inventoryPath);
				    						 }
				    						}
				    					}
				    				}
				    				if(getTextValue(el2,"PictureFileName")!=null){
				    					String imageName=getTextValue(el2,"PictureFileName ");
				    					if((imageName!=null)&&!imageName.equals("")){
				    						String imagesDir ="/usr/local/redskydoc/voxme/images";
				    						customerFile = customerFileManager.get(accessInfo.getId());
				    						File   savedFile = new File(imagesDir+"/"+customerFile.getSequenceNumber());	    						   					   
				    						 if (!savedFile.exists()) {
				    							 savedFile.mkdirs();
				    					     }
				    						String[] imageNameList=imageName.split(";");
				    						if(imageNameList.length>=1){
				    							for(int p=0;p<imageNameList.length;p++){
				    						    inventoryPath =new InventoryPath();
				    					        inventoryPath.setCorpID(accessInfo.getCorpID());
				    					        inventoryPath.setAccessInfoId(accessInfo.getId());
				    					        inventoryPath.setAccessInfoType("dest");
				    					        inventoryPath.setPath(listOfDirectory[i].getAbsolutePath().replace("voxme", "voxmeFetched")+"/" + imageNameList[p]);
				    					        inventoryPathManager.save(inventoryPath);
				    							}
				    						}
				    					}
				    				}
				    			
				    					if((signatureImagePath!=null)&&!signatureImagePath.equals("")){				    						
				    						String[] imageNameList=signatureImagePath.split(";");
				    						if(imageNameList.length>=1){
				    							for(int p=0;p<imageNameList.length;p++){
				    						    inventoryPath =new InventoryPath();
				    					        inventoryPath.setCorpID(accessInfo.getCorpID());
				    					        inventoryPath.setAccessInfoId(accessInfo.getId());
				    					        inventoryPath.setAccessInfoType("sign");
				    					        inventoryPath.setPath(listOfDirectory[i].getAbsolutePath().replace("voxme", "voxmeFetched")+"/" + imageNameList[p]);
				    					        inventoryPathManager.save(inventoryPath);
				    							}
				    						}
				    					}	
				    		}	
				    		NodeList inventoryList = docEle.getElementsByTagName("Piece");
				    		if(inventoryList != null && inventoryList.getLength() > 0) {
				    			for(int p = 0 ; p < inventoryList.getLength();p++) {
				    				Element el = (Element)inventoryList.item(p);
				    				inventoryData= new InventoryData();
				    				inventoryData.setCustomerFileID(new Long(((Element)dom.getElementsByTagName("MovingData").item(0)).getAttribute("ID")));
				    				inventoryData.setRoom(getTextValue(el,"Location"));
				    				inventoryData.setWeight(getTextValue(el,"Weight"));
					    			inventoryData.setCft(getTextValue(el,"Volume"));
				    				if(getTextValue(el,"Destination")!=null){
				    					String mode=getTextValue(el,"Destination");
				    					if(!mode.equals("")){
				    						if(mode.length()>3&&!(mode.substring(0, 3).equalsIgnoreCase("sto"))){
				    							inventoryData.setMode("other");
				    							inventoryData.setOther(mode);
				    						}else{
				    							inventoryData.setMode(mode);
				    						}
				    					}else{
				    						inventoryData.setMode(mode);
				    					}
				    				}
				    				//inventoryData.setMode(getTextValue(el,"Destination"));	    				
				    				NodeList itemList=el.getElementsByTagName("Item");
				    				Element el2=(Element)itemList.item(0);
				    				inventoryData.setAtricle(el2.getAttribute("name"));
				    				inventoryData.setQuantity(getTextValue(el2,"Quantity"));
				    				try{ 
				    					Integer total=(new Integer(inventoryData.getQuantity()))*(new Integer(inventoryData.getCft()));
				    					inventoryData.setTotal(total.toString());
					    				}catch(Exception e){
					    					e.printStackTrace();
					    					inventoryData.setTotal("0");
					    				}
				    				try{
				    					inventoryData.setValuation((new Double(getTextValue(el2,"Value"))).intValue());
					    				}catch(Exception e){	
					    					e.printStackTrace();
					    				}
				    				//inventoryData.setValuation(new Integer(getTextValue(el2,"Value")));
				    				inventoryData.setArticleCondition(getTextValue(el2,"Condition"));
				    				if(getTextValue(el2,"Comment")!=null){
				    					inventoryData.setComment(getTextValue(el2,"Comment"));
				    				}else{
				    					inventoryData.setComment("");
				    					}	    				
				    				NodeList sizeList=el2.getElementsByTagName("Size");
				    				Element el3=(Element)sizeList.item(0);
				    				try{
				    				inventoryData.setWidth((new Double(getTextValue(el3,"Width"))).intValue());
				    				}catch(Exception e){
				    					e.printStackTrace();
				    					inventoryData.setWidth(0);
				    				}
				    				try{
				    					inventoryData.setHeight((new Double(getTextValue(el3,"Height"))).intValue());
					    				}catch(Exception e){
					    					e.printStackTrace();
					    					inventoryData.setHeight(0);
					    				}
					    			 try{
					    				 inventoryData.setLength((new Double(getTextValue(el3,"Length"))).intValue());
						    				}catch(Exception e){
						    					e.printStackTrace();
						    					inventoryData.setLength(0);
						    				}
						    				
						    	if( getBoxAttributeValue(el,"Box")!=null){
						    			if(! getBoxAttributeValue(el,"Box").equals("")){
						    					inventoryData.setType("M");
						    			}else{
						    				inventoryData.setType("A");
						    				}
						    	}else{
					    				inventoryData.setType("A");
					    			}		
				    				//inventoryData.setHeight(new Integer(getTextValue(el3,"Height")));
				    				//inventoryData.setLength(new Integer(getTextValue(el3,"Length")));
				    				inventoryData.setDismantling(new Boolean(getTextValue(el2,"Dismantling")));
				    				inventoryData.setIsValuable(new Boolean(getTextValue(el2,"IsValuable")));
				    				inventoryData.setAssembling(new Boolean(getTextValue(el2,"Assembling")));
				    				inventoryData.setSpecialContainer(new Boolean(getTextValue(el2,"SpecialContainer")));
				    				//inventoryData.setCorpID(docEle.getElementsByTagName("EMFID").item(0).getFirstChild().getNodeValue().substring(0,4));
				    				try{
				    					//inventoryData.setCorpID(((Element)dom.getElementsByTagName("OrgID").item(0)).getFirstChild().getNodeValue());
				    					inventoryData.setCorpID("SSCW");
				    				}catch(Exception e){
				    					e.printStackTrace();
				    		    		inventoryData.setCorpID(sessionCorpID);
				    		    		}
				    				inventoryData.setCreatedOn(new Date());
				    				inventoryData.setCreatedBy(EstimatorName+" - XML");
				    				inventoryData=inventoryDataManager.save(inventoryData);
				    				
				    				if(getTextValue(el2,"PictureFileName")!=null){
				    					String imageName=getTextValue(el2,"PictureFileName");
				    					if((imageName!=null)&&!imageName.equals("")){
				    						String imagesDir ="/usr/local/redskydoc/voxme/images";
				    						customerFile = customerFileManager.get(inventoryData.getCustomerFileID());
				    						File   savedFile = new File(imagesDir+"/"+customerFile.getSequenceNumber());	    						   					   
				    						 if (!savedFile.exists()) {
				    							 savedFile.mkdirs();
				    					     }
				    						String[] imageNameList=imageName.split(";");
				    						if(imageNameList.length>=1){
				    							for(int s=0;s<imageNameList.length;s++){
				    						    inventoryPath =new InventoryPath();
				    					        inventoryPath.setCorpID(inventoryData.getCorpID());
				    					        inventoryPath.setInventoryDataId(inventoryData.getId());
				    					        inventoryPath.setPath(listOfDirectory[i].getAbsolutePath().replace("voxme", "voxmeFetched")+"/" + imageNameList[s]);
				    					        inventoryPathManager.save(inventoryPath);
				    							}
				    							inventoryData.setImagesAvailablityFlag(true);
				    						}
				    					}
				    				}
				    				inventoryData=inventoryDataManager.save(inventoryData);
				    			}
				    		}
				          	}catch(ParserConfigurationException pce) {
				      		pce.printStackTrace();
				      	    }catch(SAXException se) {
				      		se.printStackTrace();
				      	   }catch(IOException ioe) {
				      		ioe.printStackTrace();
				         	  }				      	 
				       }
				     }
				  								  
			  }
			   String str=  listOfDirectory[i].getAbsolutePath();
				str=str.replace("voxme", "voxmeFetched");
				listOfDirectory[i].renameTo(new File(str));
			 }
	   }
   
	  list();
	return SUCCESS;
}



private Document dom;

public Element addElement(String value,String tagName){
	 Element ele = dom.createElement(tagName);
     Text nameText=dom.createTextNode(value);
     ele.appendChild(nameText);
     return ele;
}
public String sendXmlFromCrew(){
	message=creatXMLFormWorkTicketCrew(cid);
	return SUCCESS;
}

public String creatXMLFormWorkTicketCrew(Long id){
	customerFile=customerFileManager.get(id);
	try{
	accessInfo=accessInfoManager.get(id);
	}catch (Exception e) {
		e.printStackTrace();
	}
	if(accessInfo==null){
		accessInfo=new AccessInfo();
		return "N";
	}
	createDocument();
	String surveyarEmail="";
	//User user = userManager.getUserByUsername(estimatorCustomer);
	//if(user!=null){
	//surveyarEmail = (user.getEmail()!=null)?user.getEmail():"";
	//}
	String filePath="mf"+customerFile.getFirstName()+"_"+customerFile.getLastName()+"_"+customerFile.getSequenceNumber()+".xml";
	File file = new File(filePath);
	Element rootEle = dom.createElement("MovingData");
	rootEle.setAttribute("ID",customerFile.getId().toString());
	dom.appendChild(rootEle);
	   Element generalInfoEle = dom.createElement("GeneralInfo");
	        Element Name = dom.createElement("Name");
	        Text nameText=dom.createTextNode(customerFile.getFirstName()+" "+customerFile.getLastName());
	        Name.appendChild(nameText);
	   generalInfoEle.appendChild(Name);
	        if(customerFile.getEstimator()==null)customerFile.setEstimator("");
	   generalInfoEle.appendChild(addElement(customerFile.getEstimator(),"EstimatorName")); 
	        if(customerFile.getSource()==null)customerFile.setSource("");
	   generalInfoEle.appendChild(addElement(customerFile.getSource(),"SourceOfRequest"));
	        if(customerFile.getSurvey()==null)customerFile.setSurvey(new Date());
	        SimpleDateFormat dateformat = new SimpleDateFormat("mm/dd/yy");
	   generalInfoEle.appendChild(addElement(" ","EstimationDate"));
	        if(customerFile.getSequenceNumber()==null)customerFile.setSequenceNumber("");
	   generalInfoEle.appendChild(addElement(customerFile.getSequenceNumber(),"EMFID"));
	            
	           Element addressEle = dom.createElement("Address");
	                   if(customerFile.getOriginAddress1()==null)customerFile.setOriginAddress1("");
	                   addressEle.appendChild(addElement(" ","Company"));
	                   if(customerFile.getOriginAddress2()==null)customerFile.setOriginAddress2("");
	                   addressEle.appendChild(addElement(customerFile.getOriginAddress1()+" "+customerFile.getOriginAddress2(),"Street"));
	                   if(customerFile.getOriginCity()==null)customerFile.setOriginCity("");
	                   addressEle.appendChild(addElement(customerFile.getOriginCity(),"City"));
	                   if(customerFile.getOriginState()==null)customerFile.setOriginState("");
	                   addressEle.appendChild(addElement(customerFile.getOriginState(),"State"));
	                   if(customerFile.getOriginCountry()==null)customerFile.setOriginCountry("");
	                   addressEle.appendChild(addElement(customerFile.getOriginCountry(),"Country"));
	                   if(customerFile.getOriginZip()==null)customerFile.setOriginZip("");
	                   addressEle.appendChild(addElement(customerFile.getOriginZip(),"Zip"));
	                   if(customerFile.getOriginDayPhone()==null)customerFile.setOriginDayPhone("");
	                   addressEle.appendChild(addElement(customerFile.getOriginDayPhone(),"PrimaryPhone"));
	                   if(customerFile.getOriginMobile()==null)customerFile.setOriginMobile("");
	                   addressEle.appendChild(addElement(customerFile.getOriginMobile(),"SecondaryPhone"));
	                   if(customerFile.getEmail()==null)customerFile.setEmail("");
	                   addressEle.appendChild(addElement(customerFile.getEmail(),"email"));
	                   if(customerFile.getOriginFax()==null)customerFile.setOriginFax("");
	                   addressEle.appendChild(addElement(customerFile.getOriginFax(),"fax"));
	                           
	                       Element accessInfoEle = dom.createElement("AccessInfo");	                      
	                       accessInfoEle.appendChild(addElement(accessInfo.getOriginfloor(),"Floor"));
	                       if(accessInfo.getOriginElevator()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginElevator().toString(),"HasElevator")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement("false","HasElevator"));  
	                       }
	                       if(accessInfo.getOriginElevatorDetails()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginElevatorDetails().toString(),"ElevatorDetails")); 
	                       }else{
	                       accessInfoEle.appendChild(addElement(" ","ElevatorDetails"));
	                       }
	                       if(accessInfo.getOriginDistanceToParking()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginDistanceToParking().toString(),"DistanceToParking")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement("0","DistanceToParking"));
	                       }
	                       if(accessInfo.getOriginNeedCrane()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginNeedCrane().toString(),"NeedCrane")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement("false","NeedCrane"));
	                       }
	                       accessInfoEle.appendChild(addElement(" ","PictureFileName"));
	                       if(accessInfo.getOriginResidenceType()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginResidenceType().toString(),"PropertyType")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement(" ","PropertyType"));
	                       }
	                       if(accessInfo.getOriginResidenceSize()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginResidenceSize().toString(),"PropertySize")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement(" ","PropertySize"));
	                       }
	                       if(accessInfo.getOriginReserveParking()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginReserveParking().toString(),"ParkingReservationRequired")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement("false","ParkingReservationRequired"));
	                       }
	                       if(accessInfo.getOriginParkingType()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginParkingType().toString(),"ParkingType")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement(" ","ParkingType"));
	                       }
	                       if(accessInfo.getOriginParkingSpots()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginParkingSpots().toString(),"NumOfParkingSpots")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement("0","NumOfParkingSpots"));
	                       }
	                       if(accessInfo.getOriginParkinglotsize()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginParkinglotsize().toString(),"ParkingSpotSize")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement("0","ParkingSpotSize"));
	                       }
	                       if(accessInfo.getOriginLongCarry()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginLongCarry().toString(),"CarryRequired")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement("false","CarryRequired"));
	                       }
	                       if(accessInfo.getOriginCarryDistance()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginCarryDistance().toString(),"CarryLength")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement("0","CarryLength"));
	                       }
	                       if(accessInfo.getOriginCarrydetails()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginCarrydetails().toString(),"CarryDescription")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement(" ","CarryDescription"));
	                       }
	                       if(accessInfo.getOriginElevatorType()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginElevatorType().toString(),"ExternalElevatorType")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement(" ","ExternalElevatorType"));
	                       }
	                       if(accessInfo.getOriginShuttleRequired()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginShuttleRequired().toString(),"shuttleCarryRequired")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement("false","shuttleCarryRequired"));
	                       }
	                       if(accessInfo.getOriginShuttleDistance()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginShuttleDistance().toString(),"ShuttleDistance")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement("0","ShuttleDistance"));
	                       }
	                       if(accessInfo.getOriginStairCarry()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginStairCarry().toString(),"StairCarryRequired")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement("false","StairCarryRequired"));
	                       }
	                       if(accessInfo.getOriginStairDistance()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginStairDistance().toString(),"StairCarryLength")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement("0","StairCarryLength"));
	                       }
	                       if(accessInfo.getOriginStairdetails()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginStairdetails().toString(),"StairCarryDescription")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement(" ","StairCarryDescription"));
	                       }
	                       if(accessInfo.getOriginAdditionalStopRequired()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginAdditionalStopRequired().toString(),"StairCarryDescription")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement("false","AdditionalStopRequired"));
	                       }
	                       if(accessInfo.getOriginAdditionalStopDetails()!=null){
	                    	   accessInfoEle.appendChild(addElement(accessInfo.getOriginAdditionalStopDetails().toString(),"AdditionalStop")); 
	                       }else{
	                    	   accessInfoEle.appendChild(addElement(" ","AdditionalStop"));
	                       }
	                      
	                  
	                  addressEle.appendChild(accessInfoEle);
	                  addressEle.appendChild(addElement("","Comment"));
	                      
	                      // Element roomsEle = dom.createElement("Rooms");
	                          // Element roomEle = dom.createElement("Room"); 
	                         //  roomEle.setAttribute("name","XXX");
	                             //  roomEle.appendChild(addElement(" ","Nickname"));
	                             //  roomEle.appendChild(addElement("0","NumOfPeople"));
	                             //  roomEle.appendChild(addElement("0","EstimatedVolume"));
	                          // roomsEle.appendChild(roomEle);
	                // addressEle.appendChild(roomsEle); 
	                 
	    generalInfoEle.appendChild(addressEle); 
	       
	                Element destinationEle = dom.createElement("Destination");
	                      if(customerFile.getDestinationAddress1()==null)customerFile.setDestinationAddress1("");
	                      destinationEle.appendChild(addElement("","Company"));
	                      if(customerFile.getDestinationAddress2()==null)customerFile.setDestinationAddress2("");
	                      destinationEle.appendChild(addElement( customerFile.getDestinationAddress1()+" "+customerFile.getDestinationAddress2(),"Street"));
	                      if(customerFile.getDestinationCity()==null)customerFile.setDestinationCity("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationCity(),"City"));
	                      if(customerFile.getDestinationState()==null)customerFile.setDestinationState("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationState(),"State"));
	                      if(customerFile.getDestinationCountry()==null)customerFile.setDestinationCountry("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationCountry(),"Country"));
	                      if(customerFile.getDestinationZip()==null)customerFile.setDestinationZip("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationZip(),"Zip"));
	                      if(customerFile.getDestinationDayPhone()==null)customerFile.setDestinationDayPhone("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationDayPhone(),"PrimaryPhone"));
	                      if(customerFile.getDestinationMobile()==null)customerFile.setDestinationMobile("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationMobile(),"SecondaryPhone"));
	                      if(customerFile.getEmail()==null)customerFile.setEmail("");
	                      destinationEle.appendChild(addElement(customerFile.getEmail(),"email"));
	                      if(customerFile.getDestinationFax()==null)customerFile.setDestinationFax("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationFax(),"fax"));
	                         
	                         Element accessInfoEle2 = dom.createElement("AccessInfo");
	                         
	                         
	                         accessInfoEle2.appendChild(addElement(accessInfo.getDestinationfloor(),"Floor"));
		                       if(accessInfo.getDestinationElevator()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationElevator().toString(),"HasElevator")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement("false","HasElevator"));  
		                       }
		                       if(accessInfo.getDestinationElevatorDetails()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationElevatorDetails().toString(),"ElevatorDetails")); 
		                       }else{
		                       accessInfoEle2.appendChild(addElement(" ","ElevatorDetails"));
		                       }
		                       if(accessInfo.getDestinationDistanceToParking()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationDistanceToParking().toString(),"DistanceToParking")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement("0","DistanceToParking"));
		                       }
		                       if(accessInfo.getDestinationNeedCrane()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationNeedCrane().toString(),"NeedCrane")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement("false","NeedCrane"));
		                       }
		                       accessInfoEle2.appendChild(addElement(" ","PictureFileName"));
		                       if(accessInfo.getDestinationResidenceType()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationResidenceType().toString(),"PropertyType")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement(" ","PropertyType"));
		                       }
		                       if(accessInfo.getDestinationResidenceSize()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationResidenceSize().toString(),"PropertySize")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement(" ","PropertySize"));
		                       }
		                       if(accessInfo.getDestinationReserveParking()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationReserveParking().toString(),"ParkingReservationRequired")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement("false","ParkingReservationRequired"));
		                       }
		                       if(accessInfo.getDestinationParkingType()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationParkingType().toString(),"ParkingType")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement(" ","ParkingType"));
		                       }
		                       if(accessInfo.getDestinationParkingSpots()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationParkingSpots().toString(),"NumOfParkingSpots")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement("0","NumOfParkingSpots"));
		                       }
		                       if(accessInfo.getDestinationParkinglotsize()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationParkinglotsize().toString(),"ParkingSpotSize")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement("0","ParkingSpotSize"));
		                       }
		                       if(accessInfo.getDestinationLongCarry()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationLongCarry().toString(),"CarryRequired")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement("false","CarryRequired"));
		                       }
		                       if(accessInfo.getDestinationCarryDistance()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationCarryDistance().toString(),"CarryLength")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement("0","CarryLength"));
		                       }
		                       if(accessInfo.getDestinationCarrydetails()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationCarrydetails().toString(),"CarryDescription")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement(" ","CarryDescription"));
		                       }
		                       if(accessInfo.getDestinationElevatorType()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationElevatorType().toString(),"ExternalElevatorType")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement(" ","ExternalElevatorType"));
		                       }
		                       if(accessInfo.getDestinationShuttleRequired()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationShuttleRequired().toString(),"shuttleCarryRequired")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement("false","shuttleCarryRequired"));
		                       }
		                       if(accessInfo.getDestinationShuttleDistance()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationShuttleDistance().toString(),"ShuttleDistance")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement("0","ShuttleDistance"));
		                       }
		                       if(accessInfo.getDestinationStairCarry()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationStairCarry().toString(),"StairCarryRequired")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement("false","StairCarryRequired"));
		                       }
		                       if(accessInfo.getDestinationStairDistance()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationStairDistance().toString(),"StairCarryLength")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement("0","StairCarryLength"));
		                       }
		                       if(accessInfo.getDestinationStairdetails()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationStairdetails().toString(),"StairCarryDescription")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement(" ","StairCarryDescription"));
		                       }
		                       if(accessInfo.getDestinationAdditionalStopRequired()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationAdditionalStopRequired().toString(),"StairCarryDescription")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement("false","AdditionalStopRequired"));
		                       }
		                       if(accessInfo.getDestinationAdditionalStopDetails()!=null){
		                    	   accessInfoEle2.appendChild(addElement(accessInfo.getDestinationAdditionalStopDetails().toString(),"AdditionalStop")); 
		                       }else{
		                    	   accessInfoEle2.appendChild(addElement(" ","AdditionalStop"));
		                       }
	                         
	                         
	                        /* accessInfoEle2.appendChild(addElement("0","Floor"));
	                         accessInfoEle2.appendChild(addElement("false","HasElevator"));
	                         accessInfoEle2.appendChild(addElement(" ","ElevatorDetails"));
	                         accessInfoEle2.appendChild(addElement("0","DistanceToParking"));
	                         accessInfoEle2.appendChild(addElement("false","NeedCrane"));
	                         accessInfoEle2.appendChild(addElement(" ","PictureFileName"));
	                         accessInfoEle2.appendChild(addElement(" ","PropertyType"));
	                         accessInfoEle2.appendChild(addElement(" ","PropertySize"));
	                         accessInfoEle2.appendChild(addElement("false","ParkingReservationRequired"));
	                         accessInfoEle2.appendChild(addElement(" ","ParkingType"));
	                         accessInfoEle2.appendChild(addElement("0","NumOfParkingSpots"));
	                         accessInfoEle2.appendChild(addElement("0","ParkingSpotSize"));
	                         accessInfoEle2.appendChild(addElement("false","CarryRequired"));
	                         accessInfoEle2.appendChild(addElement("0","CarryLength"));
	                         accessInfoEle2.appendChild(addElement(" ","CarryDescription"));
	                         accessInfoEle2.appendChild(addElement(" ","ExternalElevatorType"));
	                         accessInfoEle2.appendChild(addElement("false","ShuttleRequired"));
	                         accessInfoEle2.appendChild(addElement("0","ShuttleDistance"));
	                         accessInfoEle.appendChild(addElement("false","StairCarryRequired"));
		                     accessInfoEle.appendChild(addElement("0","StairCarryLength"));
		                     accessInfoEle.appendChild(addElement(" ","StairCarryDescription"));
		                     accessInfoEle.appendChild(addElement("false","AdditionalStopRequired"));
		                     accessInfoEle.appendChild(addElement(" ","AdditionalStop"));*/
	                 destinationEle.appendChild(accessInfoEle2);
	                 destinationEle.appendChild(addElement("","Comment"));
	            generalInfoEle.appendChild(destinationEle);
	            Element preferencesEle = dom.createElement("Preferences"); 
	                 preferencesEle.appendChild(addElement(" ","PreferredLanguage"));
	                 preferencesEle.appendChild(addElement(" ","PackingDate"));
	                 preferencesEle.appendChild(addElement(" ","PackingTime"));	                
	                 preferencesEle.appendChild(addElement(dateformat.format(customerFile.getSurvey())+" "+customerFile.getSurveyTime(),"VacationDate"));
	                 preferencesEle.appendChild(addElement(" ","SurveyEndDate"));
	                 preferencesEle.appendChild(addElement(" ","DepartureDate"));
	                 preferencesEle.appendChild(addElement(" ","PackingFinishDate"));
	                 preferencesEle.appendChild(addElement(" ","DeliveryFinishDate"));
	                 preferencesEle.appendChild(addElement(" ","ExportCustomsDate"));
	                 preferencesEle.appendChild(addElement(" ","OriginStorageInDate"));
	                 preferencesEle.appendChild(addElement(" ","OriginStorageOutDate"));
	                 preferencesEle.appendChild(addElement(" ","DestCustomsInDate"));
	                 preferencesEle.appendChild(addElement(" ","DestCustomsOutDate"));
	                 preferencesEle.appendChild(addElement(" ","DestStorageInDate"));
	                 preferencesEle.appendChild(addElement(" ","DestStorageOutDate"));
	                 preferencesEle.appendChild(addElement(" ","CompletionDate"));
	                 preferencesEle.appendChild(addElement(" ","DeliveryDate"));
	                 preferencesEle.appendChild(addElement(" ","RealArrivalDate"));
	                 preferencesEle.appendChild(addElement(" ","ServiceType"));
	                 preferencesEle.appendChild(addElement(" ","Comment"));
	                 preferencesEle.appendChild(addElement(" ","AdditionalServices"));
	         generalInfoEle.appendChild(preferencesEle);
	         generalInfoEle.appendChild(addElement(" ","Comment"));
  rootEle.appendChild(generalInfoEle);  
  try{
 //TransformerFactory instance is used to create Transformer objects. 
  TransformerFactory factory = TransformerFactory.newInstance();
  Transformer transformer = factory.newTransformer(); 
  transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
  // create string from xml tree
  StringWriter sw = new StringWriter();
  StreamResult result = new StreamResult(sw);
  DOMSource source = new DOMSource(dom);
  transformer.transform(source, result);
  String xmlString = sw.toString();
  //String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources");
	//uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/XML" + "/");
  BufferedWriter bw = new BufferedWriter
                (new OutputStreamWriter(new FileOutputStream(file)));
  bw.write(xmlString);
  bw.flush();
  bw.close();
	  /*TransformerFactory transformerFactory = TransformerFactory.newInstance();
      Transformer transformer = transformerFactory.newTransformer();
      DOMSource source = new DOMSource(dom);
      StreamResult result =  new StreamResult(System.out);
      transformer.transform(source, result);*/
  }catch(Exception e){e.printStackTrace();}
  
  try {
	    surveyarEmail="nsingh@redskymobility.com";
	    String msgText1="";
	    String subject = "Voxme XML of "+customerFile.getFirstName()+"_"+customerFile.getLastName()+" & survey done on "+dateformat.format(customerFile.getSurvey());
	 
		String from = "info@sscw.com";
		
		String tempRecipient="";
		String tempRecipientArr[]=surveyarEmail.split(",");
		for(String str1:tempRecipientArr){
			if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
				if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
				tempRecipient += str1;
			}
		}
		surveyarEmail=tempRecipient;
		EmailSetUpUtil.sendMail(surveyarEmail, "", "", subject, msgText1, filePath, from);
		
  } catch (Exception mex) {
		mex.printStackTrace();
		
	}
  return "Y";
}

private void createDocument() {
	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	try {
	DocumentBuilder db = dbf.newDocumentBuilder();
       dom = db.newDocument();
	}catch(ParserConfigurationException pce) {
		System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
	}
}

/**
 * Crew data needs to sent move cloud
 * @return
 */
public String sendDataMoveToCloudCrew(){
	message=createDataCrewForMoveCloud(cid);
	return SUCCESS;
}
public String createDataCrewForMoveCloud(Long cid)
{
	workTicket = workTicketManager.get(wrkTckId);
	customerFile=customerFileManager.get(cid);
	String crewEmail = workTicketManager.findCrewEmail(workTicket.getWarehouse(), workTicket.getDate1(), sessionCorpID, workTicket.getTicket());
	IntegrationLogInfo inloginfo= new IntegrationLogInfo();
    inloginfo.setSource("RedSky");
    inloginfo.setDestination("MoveCloud");
    inloginfo.setIntegrationId(cid.toString());
    inloginfo.setSourceOrderNum(customerFile.getSequenceNumber());
    inloginfo.setDestOrderNum(workTicket.getService());
    inloginfo.setOperation("CREATE");
    inloginfo.setOrderComplete("N");
    inloginfo.setStatusCode("S");
    inloginfo.setEffectiveDate(workTicket.getDate1());
    inloginfo.setDetailedStatus("Ready For Sending");
    inloginfo.setCreatedBy(crewEmail);
    inloginfo.setCreatedOn(new Date());
    inloginfo.setUpdatedBy(crewEmail);
    inloginfo.setUpdatedOn(new Date());
    inloginfo.setCorpID(sessionCorpID);
    inloginfo =  integrationLogInfoManager.save(inloginfo);
	
	return "Y";
}
private  IntegrationLogInfoManager  integrationLogInfoManager;
public void setIntegrationLogInfoManager(
		IntegrationLogInfoManager integrationLogInfoManager) {
	this.integrationLogInfoManager = integrationLogInfoManager;
}

/**
 * End Method
 * @return
 */


public String sendXmlFromVoxmeToCrew(){
	message=createXMLFormVoxmeCrew(cid,crewMailID,sid);
	return SUCCESS;
}

@SkipValidation
public String checkCrewMailIDMethod(){
    List tempList = refMasterManager.getTimetypeFlex1List(code,"Crew-Device",sessionCorpID);
    message = tempList.get(0).toString();
	return SUCCESS;
}
public String createXMLFormVoxmeCrew(Long cid, String crewMailID,Long sid){
	workTicket = workTicketManager.get(wrkTckId);
	customerFile=customerFileManager.get(cid);
	/*serviceOrders = new ArrayList( customerFile.getServiceOrders() );
	if (serviceOrders != null) {

		Iterator it = serviceOrders.iterator();
		while (it.hasNext()) { 
			serviceOrder = (ServiceOrder) it.next();
			
			if(serviceOrder.getId()!=null ){
				sid=serviceOrder.getId();
			}
		}
	}*/
	serviceOrder=serviceOrderManager.get(sid);
	createDocument();
	String surveyarEmail="";
	/*List tempEmailId=customerFileManager.findCoordEmailAddress(customerFile.getEstimator());
	if(tempEmailId!= null && !tempEmailId.isEmpty() && tempEmailId.get(0)!= null){
		surveyarEmail=tempEmailId.get(0).toString();
	}*/
	surveyarEmail = crewMailID;
	String uploadDir="";
	String filePath="mf"+customerFile.getFirstName()+"_"+customerFile.getLastName()+"_"+serviceOrder.getShipNumber()+".xml";
	uploadDir = ServletActionContext.getServletContext().getRealPath("/resources")+"/" ;
    String rightUploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/redskydoc" + "/redskySendVoxmeXML"+"/");
    String strFilePath="";
    try{
	        File dirPath = new File(rightUploadDir);
	        if (!dirPath.exists()) {
	            dirPath.mkdirs();
	        }
			strFilePath=rightUploadDir+""+filePath;
    }catch(Exception e){e.printStackTrace();}		
    filePath=strFilePath;
	File file = new File(filePath);
	Element rootEle = dom.createElement("MovingData");
	rootEle.setAttribute("ID",serviceOrder.getId().toString());
	dom.appendChild(rootEle);
	  Element orgId = dom.createElement("OrgID");
	  rootEle.appendChild(orgId);
	  Text orgIdText=dom.createTextNode(customerFile.getCorpID());
	     orgId.appendChild(orgIdText);
	   Element generalInfoEle = dom.createElement("GeneralInfo");
	        Element Name = dom.createElement("Name");
	        Text nameText=dom.createTextNode(customerFile.getFirstName()+" "+customerFile.getLastName());
	        Name.appendChild(nameText);
	   generalInfoEle.appendChild(Name);
	        if(customerFile.getEstimator()==null)customerFile.setEstimator("");
	   generalInfoEle.appendChild(addElement(customerFile.getEstimator(),"EstimatorName")); 
	        if(customerFile.getSource()==null)customerFile.setSource("");
	   generalInfoEle.appendChild(addElement(customerFile.getSource(),"SourceOfRequest"));
	        if(customerFile.getSurvey()==null)customerFile.setSurvey(new Date());
	       	SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yy");
	   generalInfoEle.appendChild(addElement(" ","EstimationDate"));
	        if(serviceOrder.getShipNumber()==null)serviceOrder.setShipNumber("");
	   generalInfoEle.appendChild(addElement(serviceOrder.getShipNumber(),"EMFID"));
	            
	           Element addressEle = dom.createElement("Address");
	                   if(customerFile.getOriginAddress1()==null)customerFile.setOriginAddress1("");
	                   addressEle.appendChild(addElement(" ","Company"));
	                   if(customerFile.getOriginAddress2()==null)customerFile.setOriginAddress2("");
	                   addressEle.appendChild(addElement(customerFile.getOriginAddress1()+" "+customerFile.getOriginAddress2(),"Street"));
	                   if(customerFile.getOriginCity()==null)customerFile.setOriginCity("");
	                   addressEle.appendChild(addElement(customerFile.getOriginCity(),"City"));
	                   if(customerFile.getOriginState()==null)customerFile.setOriginState("");
	                   addressEle.appendChild(addElement(customerFile.getOriginState(),"State"));
	                   if(customerFile.getOriginCountry()==null)customerFile.setOriginCountry("");
	                   addressEle.appendChild(addElement(customerFile.getOriginCountry(),"Country"));
	                   if(customerFile.getOriginZip()==null)customerFile.setOriginZip("");
	                   addressEle.appendChild(addElement(customerFile.getOriginZip(),"Zip"));
	                   if(customerFile.getOriginDayPhone()==null)customerFile.setOriginDayPhone("");
	                   addressEle.appendChild(addElement(customerFile.getOriginDayPhone(),"PrimaryPhone"));
	                   if(customerFile.getOriginMobile()==null)customerFile.setOriginMobile("");
	                   addressEle.appendChild(addElement(customerFile.getOriginMobile(),"SecondaryPhone"));
	                   if(customerFile.getEmail()==null)customerFile.setEmail("");
	                   addressEle.appendChild(addElement(customerFile.getEmail(),"email"));
	                   if(customerFile.getOriginFax()==null)customerFile.setOriginFax("");
	                   addressEle.appendChild(addElement(customerFile.getOriginFax(),"fax"));
	                           
	                       Element accessInfoEle = dom.createElement("AccessInfo");
	                       accessInfoEle.appendChild(addElement(workTicket.getOrigMeters()==null ?"0" :workTicket.getOrigMeters(),"Floor"));
	                       accessInfoEle.appendChild(addElement(workTicket.getOriginElevator()==null ?"false" :workTicket.getOriginElevator(),"HasElevator"));
	                       accessInfoEle.appendChild(addElement(workTicket.getOriginElevator()==null ?"false" :workTicket.getOriginElevator(),"ElevatorDetails"));
	                       accessInfoEle.appendChild(addElement(workTicket.getFarInMeters()==null ?"0" :workTicket.getFarInMeters(),"DistanceToParking"));
	                       accessInfoEle.appendChild(addElement(workTicket.getOriginElevator()==null ?"false" :workTicket.getOriginElevator(),"NeedCrane"));
	                       accessInfoEle.appendChild(addElement(" ","PictureFileName"));
	                       accessInfoEle.appendChild(addElement(" ","PropertyType"));
	                       accessInfoEle.appendChild(addElement(" ","PropertySize"));
	                       accessInfoEle.appendChild(addElement(workTicket.getParking()==null ?"false" :workTicket.getParking(),"ParkingReservationRequired"));
	                       accessInfoEle.appendChild(addElement(" ","ParkingType"));
	                       accessInfoEle.appendChild(addElement("0","NumOfParkingSpots"));
	                       accessInfoEle.appendChild(addElement("0","ParkingSpotSize"));
	                       accessInfoEle.appendChild(addElement(workTicket.getOriginLongCarry()==null ?"false" :workTicket.getOriginLongCarry(),"CarryRequired"));
	                       accessInfoEle.appendChild(addElement("0","CarryLength"));
	                       accessInfoEle.appendChild(addElement(" ","CarryDescription"));
	                       accessInfoEle.appendChild(addElement(" ","ExternalElevatorType"));
	                       accessInfoEle.appendChild(addElement(workTicket.getOriginShuttle().toString()==null ?"false" :workTicket.getOriginShuttle().toString(),"ShuttleRequired"));
	                       accessInfoEle.appendChild(addElement("0","ShuttleDistance"));
	                       accessInfoEle.appendChild(addElement("false","StairCarryRequired"));
	                       accessInfoEle.appendChild(addElement("0","StairCarryLength"));
	                       accessInfoEle.appendChild(addElement(" ","StairCarryDescription"));
	                       accessInfoEle.appendChild(addElement("false","AdditionalStopRequired"));
	                       accessInfoEle.appendChild(addElement(" ","AdditionalStop"));
	                  
	                  addressEle.appendChild(accessInfoEle);
	                  addressEle.appendChild(addElement("","Comment"));
	                      
	                      // Element roomsEle = dom.createElement("Rooms");
	                          // Element roomEle = dom.createElement("Room"); 
	                         //  roomEle.setAttribute("name","XXX");
	                             //  roomEle.appendChild(addElement(" ","Nickname"));
	                             //  roomEle.appendChild(addElement("0","NumOfPeople"));
	                             //  roomEle.appendChild(addElement("0","EstimatedVolume"));
	                          // roomsEle.appendChild(roomEle);
	                // addressEle.appendChild(roomsEle); 
	                 
	    generalInfoEle.appendChild(addressEle); 
	       
	                Element destinationEle = dom.createElement("Destination");
	                      if(customerFile.getDestinationAddress1()==null)customerFile.setDestinationAddress1("");
	                      destinationEle.appendChild(addElement("","Company"));
	                      if(customerFile.getDestinationAddress2()==null)customerFile.setDestinationAddress2("");
	                      destinationEle.appendChild(addElement( customerFile.getDestinationAddress1()+" "+customerFile.getDestinationAddress2(),"Street"));
	                      if(customerFile.getDestinationCity()==null)customerFile.setDestinationCity("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationCity(),"City"));
	                      if(customerFile.getDestinationState()==null)customerFile.setDestinationState("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationState(),"State"));
	                      if(customerFile.getDestinationCountry()==null)customerFile.setDestinationCountry("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationCountry(),"Country"));
	                      if(customerFile.getDestinationZip()==null)customerFile.setDestinationZip("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationZip(),"Zip"));
	                      if(customerFile.getDestinationDayPhone()==null)customerFile.setDestinationDayPhone("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationDayPhone(),"PrimaryPhone"));
	                      if(customerFile.getDestinationMobile()==null)customerFile.setDestinationMobile("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationMobile(),"SecondaryPhone"));
	                      if(customerFile.getEmail()==null)customerFile.setEmail("");
	                      destinationEle.appendChild(addElement(customerFile.getEmail(),"email"));
	                      if(customerFile.getDestinationFax()==null)customerFile.setDestinationFax("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationFax(),"fax"));
	                         
	                         Element accessInfoEle2 = dom.createElement("AccessInfo");
	                         accessInfoEle2.appendChild(addElement(workTicket.getDestMeters()==null ?"0" :workTicket.getDestMeters(),"Floor"));
	                         accessInfoEle2.appendChild(addElement(workTicket.getDestinationElevator()==null ?"false" :workTicket.getDestinationElevator(),"HasElevator"));
	                         accessInfoEle2.appendChild(addElement(workTicket.getDestinationElevator()==null ?"false" :workTicket.getDestinationElevator(),"ElevatorDetails"));
	                         accessInfoEle2.appendChild(addElement(workTicket.getDest_farInMeters()==null ?"0" :workTicket.getDest_farInMeters(),"DistanceToParking"));
	                         accessInfoEle2.appendChild(addElement(workTicket.getDestinationElevator()==null ?"false" :workTicket.getDestinationElevator(),"NeedCrane"));
	                         accessInfoEle2.appendChild(addElement(" ","PictureFileName"));
	                         accessInfoEle2.appendChild(addElement(" ","PropertyType"));
	                         accessInfoEle2.appendChild(addElement(" ","PropertySize"));
	                         accessInfoEle2.appendChild(addElement(workTicket.getDest_park()==null ?"false" :workTicket.getDest_park(),"ParkingReservationRequired"));
	                         accessInfoEle2.appendChild(addElement(" ","ParkingType"));
	                         accessInfoEle2.appendChild(addElement("0","NumOfParkingSpots"));
	                         accessInfoEle2.appendChild(addElement("0","ParkingSpotSize"));
	                         accessInfoEle2.appendChild(addElement(workTicket.getDestinationLongCarry()==null ?"false" :workTicket.getDestinationLongCarry(),"CarryRequired"));
	                         accessInfoEle2.appendChild(addElement("0","CarryLength"));
	                         accessInfoEle2.appendChild(addElement(" ","CarryDescription"));
	                         accessInfoEle2.appendChild(addElement(" ","ExternalElevatorType"));
	                         accessInfoEle2.appendChild(addElement(workTicket.getDestinationShuttle().toString()==null ?"false" :workTicket.getDestinationShuttle().toString(),"ShuttleRequired"));
	                         accessInfoEle2.appendChild(addElement("0","ShuttleDistance"));
	                         accessInfoEle2.appendChild(addElement("false","StairCarryRequired"));
	                         accessInfoEle2.appendChild(addElement("0","StairCarryLength"));
	                         accessInfoEle2.appendChild(addElement(" ","StairCarryDescription"));
	                         accessInfoEle2.appendChild(addElement("false","AdditionalStopRequired"));
	                         accessInfoEle2.appendChild(addElement(" ","AdditionalStop"));
	                 destinationEle.appendChild(accessInfoEle2);
	                 destinationEle.appendChild(addElement("","Comment"));
	            generalInfoEle.appendChild(destinationEle);
	            Element preferencesEle = dom.createElement("Preferences"); 
	                 preferencesEle.appendChild(addElement(" ","PreferredLanguage"));
	                 preferencesEle.appendChild(addElement(" ","PackingDate"));
	                 preferencesEle.appendChild(addElement(" ","PackingTime"));
	                 preferencesEle.appendChild(addElement(dateformat.format(customerFile.getSurvey())+" "+customerFile.getSurveyTime(),"VacationDate"));
	                 preferencesEle.appendChild(addElement(" ","SurveyEndDate"));
	                 preferencesEle.appendChild(addElement(" ","DepartureDate"));
	                 preferencesEle.appendChild(addElement(" ","PackingFinishDate"));
	                 preferencesEle.appendChild(addElement(" ","DeliveryFinishDate"));
	                 preferencesEle.appendChild(addElement(" ","ExportCustomsDate"));
	                 preferencesEle.appendChild(addElement(" ","OriginStorageInDate"));
	                 preferencesEle.appendChild(addElement(" ","OriginStorageOutDate"));
	                 preferencesEle.appendChild(addElement(" ","DestCustomsInDate"));
	                 preferencesEle.appendChild(addElement(" ","DestCustomsOutDate"));
	                 preferencesEle.appendChild(addElement(" ","DestStorageInDate"));
	                 preferencesEle.appendChild(addElement(" ","DestStorageOutDate"));
	                 preferencesEle.appendChild(addElement(" ","CompletionDate"));
	                 preferencesEle.appendChild(addElement(" ","DeliveryDate"));
	                 preferencesEle.appendChild(addElement(" ","RealArrivalDate"));
	                 preferencesEle.appendChild(addElement(" ","ServiceType"));
	                 preferencesEle.appendChild(addElement(" ","Comment"));
	                 preferencesEle.appendChild(addElement(" ","AdditionalServices"));
	         generalInfoEle.appendChild(preferencesEle);
	         generalInfoEle.appendChild(addElement(" ","Comment"));
	        rootEle.appendChild(generalInfoEle); 
	        

	  Element invtoryDataEle = dom.createElement("InventoryData");
	  invtoryDataEle.setAttribute("UMO", "");
	  invtoryDataEle.setAttribute("Offset", "0");
	  Element proprties = dom.createElement("Properties");
	  
	  Element property = dom.createElement("Property");
	  property.appendChild(addElement(" ","Category"));
	  property.appendChild(addElement("Allowances","Type"));
	  property.appendChild(addElement(customerFile.getEntitled()==null ?"" :customerFile.getEntitled(),"Description"));
	  property.appendChild(addElement("1","QtyTaken"));
	  property.appendChild(addElement("1","QtyReturned"));
	  property.appendChild(addElement("","Value"));
	  proprties.appendChild(property);
	  
	  
	  Element propertyNew = dom.createElement("Property");
	  propertyNew.appendChild(addElement(" ","Category"));
	  propertyNew.appendChild(addElement("Prefix","Type"));
	  propertyNew.appendChild(addElement(customerFile.getPrefix()==null ?"" :customerFile.getPrefix(),"Description"));
	  propertyNew.appendChild(addElement("1","QtyTaken"));
	  propertyNew.appendChild(addElement("1","QtyReturned"));
	  propertyNew.appendChild(addElement("1","Value"));
	  proprties.appendChild(propertyNew);
	 
	 
	  Element property2 = dom.createElement("Property");
	  property2.appendChild(addElement(" ","Category"));
	  property2.appendChild(addElement("Booking Agent","Type"));
	  property2.appendChild(addElement(customerFile.getBookingAgentCode()==null ?"" :customerFile.getBookingAgentCode(),"Description"));
	  property2.appendChild(addElement("1","QtyTaken"));
	  property2.appendChild(addElement("1","QtyReturned"));
	  property2.appendChild(addElement("","Value"));
	  proprties.appendChild(property2);
	  
	  Element property3 = dom.createElement("Property");
	  property3.appendChild(addElement(" ","Category"));
	  property3.appendChild(addElement("Booking Agent Name","Type"));
	  property3.appendChild(addElement(customerFile.getBookingAgentName()==null ?"" :customerFile.getBookingAgentName(),"Description"));
	  property3.appendChild(addElement("1","QtyTaken"));
	  property3.appendChild(addElement("1","QtyReturned"));
	  property3.appendChild(addElement("","Value"));
	  proprties.appendChild(property3);
	  
	  Element property4 = dom.createElement("Property");
	  property4.appendChild(addElement(" ","Category"));
	  property4.appendChild(addElement("Company","Type"));
	  property4.appendChild(addElement(customerFile.getCompanyDivision()==null ?"" :customerFile.getCompanyDivision(),"Description"));
	  property4.appendChild(addElement("1","QtyTaken"));
	  property4.appendChild(addElement("1","QtyReturned"));
	  property4.appendChild(addElement("","Value"));
	  proprties.appendChild(property4);
	  
	  Element property5 = dom.createElement("Property");
	  property5.appendChild(addElement(" ","Category"));
	  property5.appendChild(addElement("Job","Type"));
	  property5.appendChild(addElement(customerFile.getJob()==null ?"" :customerFile.getJob(),"Description"));
	  property5.appendChild(addElement("1","QtyTaken"));
	  property5.appendChild(addElement("1","QtyReturned"));
	  property5.appendChild(addElement("","Value"));
	  proprties.appendChild(property5);
	  
	  Element property6 = dom.createElement("Property");
	  property6.appendChild(addElement(" ","Category"));
	  property6.appendChild(addElement("Coordinator","Type"));
	  property6.appendChild(addElement(customerFile.getCoordinatorName()==null ?"" :customerFile.getCoordinatorName(),"Description"));
	  property6.appendChild(addElement("1","QtyTaken"));
	  property6.appendChild(addElement("1","QtyReturned"));
	  property6.appendChild(addElement("","Value"));
	  proprties.appendChild(property6);
	  
	  Element property7 = dom.createElement("Property");
	  property7.appendChild(addElement(" ","Category"));
	  property7.appendChild(addElement("Move By Date","Type"));
	  try{
		  SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("MM/dd/yy");
		  StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(customerFile.getMoveDate()));
	  	  String tempMoveDate = nowYYYYMMDD2.toString();
	  	  property7.appendChild(addElement(tempMoveDate,"Description"));
	  }catch(Exception e){
		  e.printStackTrace();
		  property7.appendChild(addElement("","Description"));
	  }
	  property7.appendChild(addElement("1","QtyTaken"));
	  property7.appendChild(addElement("1","QtyReturned"));
	  property7.appendChild(addElement("","Value"));
	  proprties.appendChild(property7);
	  
	  Element property8 = dom.createElement("Property");
	  property8.appendChild(addElement(" ","Category"));
	  property8.appendChild(addElement("Employee Name","Type"));
	  property8.appendChild(addElement(customerFile.getOriginCompany()==null ?"" :customerFile.getOriginCompany(),"Description"));
	  property8.appendChild(addElement("1","QtyTaken"));
	  property8.appendChild(addElement("1","QtyReturned"));
	  property8.appendChild(addElement("","Value"));
	  proprties.appendChild(property8);
	  
	  Element property9 = dom.createElement("Property");
	  property9.appendChild(addElement(" ","Category"));
	  property9.appendChild(addElement("Employee Name","Type"));
	  property9.appendChild(addElement(customerFile.getDestinationCompany()==null ?"" :customerFile.getDestinationCompany(),"Description"));
	  property9.appendChild(addElement("1","QtyTaken"));
	  property9.appendChild(addElement("1","QtyReturned"));
	  property9.appendChild(addElement("","Value"));
	  proprties.appendChild(property9);
	  
	  String add="";
	  AdAddressesDetails adAddressesDetails = adAddressesDetailsManager.getFirstAddressDetail(sessionCorpID,customerFile.getId(),"Additional Origin"); 
	  if(adAddressesDetails!=null){
		  add = adAddressesDetails.getDescription() +" , "+adAddressesDetails.getAddress1() +" "+adAddressesDetails.getAddress2()+" "+adAddressesDetails.getAddress3()+" " +
		  		", "+adAddressesDetails.getCity()+", "+adAddressesDetails.getState()+", "+adAddressesDetails.getCountry()+" ,"+adAddressesDetails.getZipCode()+" ,"+
		  		"Ph. : "+adAddressesDetails.getPhone();
	  }
	  Element property10 = dom.createElement("Property");
	  property10.appendChild(addElement(" ","Category"));
	  property10.appendChild(addElement("Origin Additional Address","Type"));
	  property10.appendChild(addElement(add==null ?"" :add,"Description"));
	  property10.appendChild(addElement("1","QtyTaken"));
	  property10.appendChild(addElement("1","QtyReturned"));
	  property10.appendChild(addElement("","Value"));
	  proprties.appendChild(property10);
	  add="";
	  adAddressesDetails = adAddressesDetailsManager.getFirstAddressDetail(sessionCorpID,customerFile.getId(),"Additional Destination"); 
	  if(adAddressesDetails!=null){
		  add = adAddressesDetails.getDescription() +" , "+adAddressesDetails.getAddress1() +" "+adAddressesDetails.getAddress2()+" "+adAddressesDetails.getAddress3()+" " +
		  		", "+adAddressesDetails.getCity()+", "+adAddressesDetails.getState()+", "+adAddressesDetails.getCountry()+" ,"+adAddressesDetails.getZipCode()+" ,"+
		  		"Ph. : "+adAddressesDetails.getPhone();
	  }
	  Element property11 = dom.createElement("Property");
	  property11.appendChild(addElement(" ","Category"));
	  property11.appendChild(addElement("Destination Additional Address","Type"));
	  property11.appendChild(addElement(add==null ?"" :add,"Description"));
	  property11.appendChild(addElement("1","QtyTaken"));
	  property11.appendChild(addElement("1","QtyReturned"));
	  property11.appendChild(addElement("","Value"));
	  proprties.appendChild(property11);

	  Element property12 = dom.createElement("Property");
	  property12.appendChild(addElement(" ","Category"));
	  property12.appendChild(addElement("Entitlement","Type"));
	  property12.appendChild(addElement(customerFile.getOrderComment()== null ? "" : customerFile.getOrderComment(),"Description"));
	  property12.appendChild(addElement("1","QtyTaken"));
	  property12.appendChild(addElement("1","QtyReturned"));
	  property12.appendChild(addElement("","Value"));
	  proprties.appendChild(property12);

	  Element property13 = dom.createElement("Property");
	  property13.appendChild(addElement(" ","Category"));
	  property13.appendChild(addElement("Bill to code","Type"));
	  property13.appendChild(addElement(customerFile.getBillToCode()== null ? "" : customerFile.getBillToCode(),"Description"));
	  property13.appendChild(addElement("1","QtyTaken"));
	  property13.appendChild(addElement("1","QtyReturned"));
	  property13.appendChild(addElement("","Value"));
	  proprties.appendChild(property13);
	  
	  Element property14 = dom.createElement("Property");
	  property14.appendChild(addElement(" ","Category"));
	  property14.appendChild(addElement("Bill to name","Type"));
	  property14.appendChild(addElement(customerFile.getBillToName()== null ? "" : customerFile.getBillToName(),"Description"));
	  property14.appendChild(addElement("1","QtyTaken"));
	  property14.appendChild(addElement("1","QtyReturned"));
	  property14.appendChild(addElement("","Value"));
	  proprties.appendChild(property14);
	  
	  invtoryDataEle.appendChild(proprties);
	  rootEle.appendChild(invtoryDataEle);
	  
	  /*try {
		  System.out.println("\n\n Voxme XML:- \n\n\n");
		OutputFormat format = new OutputFormat(dom);
		    format.setIndenting(true);
		    XMLSerializer serializer = new XMLSerializer(System.out, format);
		    serializer.serialize(dom);
		    System.out.println("\n\n Voxme XML:- \n\n\n");
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}*/
	
	/*String filePath="mf"+customerFile.getFirstName()+"_"+customerFile.getLastName()+"_"+customerFile.getSequenceNumber()+".xml";
	File file = new File(filePath);
	Element rootEle = dom.createElement("MovingData");
	rootEle.setAttribute("ID",customerFile.getId().toString());
	dom.appendChild(rootEle);
	  Element orgId = dom.createElement("OrgID");
	  rootEle.appendChild(orgId);
	  Text orgIdText=dom.createTextNode(customerFile.getCorpID());
	     orgId.appendChild(orgIdText);
	   Element generalInfoEle = dom.createElement("GeneralInfo");
	        Element Name = dom.createElement("Name");
	        Text nameText=dom.createTextNode(customerFile.getFirstName()+" "+customerFile.getLastName());
	        Name.appendChild(nameText);
	   generalInfoEle.appendChild(Name);
	        if(customerFile.getEstimator()==null)customerFile.setEstimator("");
	   generalInfoEle.appendChild(addElement(customerFile.getEstimator(),"EstimatorName")); 
	        if(customerFile.getSource()==null)customerFile.setSource("");
	   generalInfoEle.appendChild(addElement(customerFile.getSource(),"SourceOfRequest"));
	        if(customerFile.getSurvey()==null)customerFile.setSurvey(new Date());
	       	SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yy");
	   generalInfoEle.appendChild(addElement(" ","EstimationDate"));
	        if(customerFile.getSequenceNumber()==null)customerFile.setSequenceNumber("");
	   generalInfoEle.appendChild(addElement(customerFile.getSequenceNumber(),"EMFID"));
	            
	           Element addressEle = dom.createElement("Address");
	                   if(customerFile.getOriginAddress1()==null)customerFile.setOriginAddress1("");
	                   addressEle.appendChild(addElement(" ","Company"));
	                   if(customerFile.getOriginAddress2()==null)customerFile.setOriginAddress2("");
	                   addressEle.appendChild(addElement(customerFile.getOriginAddress1()+" "+customerFile.getOriginAddress2(),"Street"));
	                   if(customerFile.getOriginCity()==null)customerFile.setOriginCity("");
	                   addressEle.appendChild(addElement(customerFile.getOriginCity(),"City"));
	                   if(customerFile.getOriginState()==null)customerFile.setOriginState("");
	                   addressEle.appendChild(addElement(customerFile.getOriginState(),"State"));
	                   if(customerFile.getOriginCountry()==null)customerFile.setOriginCountry("");
	                   addressEle.appendChild(addElement(customerFile.getOriginCountry(),"Country"));
	                   if(customerFile.getOriginZip()==null)customerFile.setOriginZip("");
	                   addressEle.appendChild(addElement(customerFile.getOriginZip(),"Zip"));
	                   if(customerFile.getOriginDayPhone()==null)customerFile.setOriginDayPhone("");
	                   addressEle.appendChild(addElement(customerFile.getOriginDayPhone(),"PrimaryPhone"));
	                   if(customerFile.getOriginMobile()==null)customerFile.setOriginMobile("");
	                   addressEle.appendChild(addElement(customerFile.getOriginMobile(),"SecondaryPhone"));
	                   if(customerFile.getEmail()==null)customerFile.setEmail("");
	                   addressEle.appendChild(addElement(customerFile.getEmail(),"email"));
	                   if(customerFile.getOriginFax()==null)customerFile.setOriginFax("");
	                   addressEle.appendChild(addElement(customerFile.getOriginFax(),"fax"));
	                           
	                       Element accessInfoEle = dom.createElement("AccessInfo");
	                       accessInfoEle.appendChild(addElement("0","Floor"));
	                       accessInfoEle.appendChild(addElement("false","HasElevator"));
	                       accessInfoEle.appendChild(addElement(" ","ElevatorDetails"));
	                       accessInfoEle.appendChild(addElement("0","DistanceToParking"));
	                       accessInfoEle.appendChild(addElement("false","NeedCrane"));
	                       accessInfoEle.appendChild(addElement(" ","PictureFileName"));
	                       accessInfoEle.appendChild(addElement(" ","PropertyType"));
	                       accessInfoEle.appendChild(addElement(" ","PropertySize"));
	                       accessInfoEle.appendChild(addElement("false","ParkingReservationRequired"));
	                       accessInfoEle.appendChild(addElement(" ","ParkingType"));
	                       accessInfoEle.appendChild(addElement("0","NumOfParkingSpots"));
	                       accessInfoEle.appendChild(addElement("0","ParkingSpotSize"));
	                       accessInfoEle.appendChild(addElement("false","CarryRequired"));
	                       accessInfoEle.appendChild(addElement("0","CarryLength"));
	                       accessInfoEle.appendChild(addElement(" ","CarryDescription"));
	                       accessInfoEle.appendChild(addElement(" ","ExternalElevatorType"));
	                       accessInfoEle.appendChild(addElement("false","ShuttleRequired"));
	                       accessInfoEle.appendChild(addElement("0","ShuttleDistance"));
	                       accessInfoEle.appendChild(addElement("false","StairCarryRequired"));
	                       accessInfoEle.appendChild(addElement("0","StairCarryLength"));
	                       accessInfoEle.appendChild(addElement(" ","StairCarryDescription"));
	                       accessInfoEle.appendChild(addElement("false","AdditionalStopRequired"));
	                       accessInfoEle.appendChild(addElement(" ","AdditionalStop"));
	                  
	                  addressEle.appendChild(accessInfoEle);
	                  addressEle.appendChild(addElement("","Comment"));
	                      
	                      // Element roomsEle = dom.createElement("Rooms");
	                          // Element roomEle = dom.createElement("Room"); 
	                         //  roomEle.setAttribute("name","XXX");
	                             //  roomEle.appendChild(addElement(" ","Nickname"));
	                             //  roomEle.appendChild(addElement("0","NumOfPeople"));
	                             //  roomEle.appendChild(addElement("0","EstimatedVolume"));
	                          // roomsEle.appendChild(roomEle);
	                // addressEle.appendChild(roomsEle); 
	                 
	    generalInfoEle.appendChild(addressEle); 
	       
	                Element destinationEle = dom.createElement("Destination");
	                      if(customerFile.getDestinationAddress1()==null)customerFile.setDestinationAddress1("");
	                      destinationEle.appendChild(addElement("","Company"));
	                      if(customerFile.getDestinationAddress2()==null)customerFile.setDestinationAddress2("");
	                      destinationEle.appendChild(addElement( customerFile.getDestinationAddress1()+" "+customerFile.getDestinationAddress2(),"Street"));
	                      if(customerFile.getDestinationCity()==null)customerFile.setDestinationCity("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationCity(),"City"));
	                      if(customerFile.getDestinationState()==null)customerFile.setDestinationState("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationState(),"State"));
	                      if(customerFile.getDestinationCountry()==null)customerFile.setDestinationCountry("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationCountry(),"Country"));
	                      if(customerFile.getDestinationZip()==null)customerFile.setDestinationZip("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationZip(),"Zip"));
	                      if(customerFile.getDestinationDayPhone()==null)customerFile.setDestinationDayPhone("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationDayPhone(),"PrimaryPhone"));
	                      if(customerFile.getDestinationMobile()==null)customerFile.setDestinationMobile("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationMobile(),"SecondaryPhone"));
	                      if(customerFile.getEmail()==null)customerFile.setEmail("");
	                      destinationEle.appendChild(addElement(customerFile.getEmail(),"email"));
	                      if(customerFile.getDestinationFax()==null)customerFile.setDestinationFax("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationFax(),"fax"));
	                         
	                         Element accessInfoEle2 = dom.createElement("AccessInfo");
	                         accessInfoEle2.appendChild(addElement("0","Floor"));
	                         accessInfoEle2.appendChild(addElement("false","HasElevator"));
	                         accessInfoEle2.appendChild(addElement(" ","ElevatorDetails"));
	                         accessInfoEle2.appendChild(addElement("0","DistanceToParking"));
	                         accessInfoEle2.appendChild(addElement("false","NeedCrane"));
	                         accessInfoEle2.appendChild(addElement(" ","PictureFileName"));
	                         accessInfoEle2.appendChild(addElement(" ","PropertyType"));
	                         accessInfoEle2.appendChild(addElement(" ","PropertySize"));
	                         accessInfoEle2.appendChild(addElement("false","ParkingReservationRequired"));
	                         accessInfoEle2.appendChild(addElement(" ","ParkingType"));
	                         accessInfoEle2.appendChild(addElement("0","NumOfParkingSpots"));
	                         accessInfoEle2.appendChild(addElement("0","ParkingSpotSize"));
	                         accessInfoEle2.appendChild(addElement("false","CarryRequired"));
	                         accessInfoEle2.appendChild(addElement("0","CarryLength"));
	                         accessInfoEle2.appendChild(addElement(" ","CarryDescription"));
	                         accessInfoEle2.appendChild(addElement(" ","ExternalElevatorType"));
	                         accessInfoEle2.appendChild(addElement("false","ShuttleRequired"));
	                         accessInfoEle2.appendChild(addElement("0","ShuttleDistance"));
	                         accessInfoEle.appendChild(addElement("false","StairCarryRequired"));
		                     accessInfoEle.appendChild(addElement("0","StairCarryLength"));
		                     accessInfoEle.appendChild(addElement(" ","StairCarryDescription"));
		                     accessInfoEle.appendChild(addElement("false","AdditionalStopRequired"));
		                     accessInfoEle.appendChild(addElement(" ","AdditionalStop"));
	                 destinationEle.appendChild(accessInfoEle2);
	                 destinationEle.appendChild(addElement("","Comment"));
	            generalInfoEle.appendChild(destinationEle);
	            Element preferencesEle = dom.createElement("Preferences"); 
	                 preferencesEle.appendChild(addElement(" ","PreferredLanguage"));
	                 preferencesEle.appendChild(addElement(" ","PackingDate"));
	                 preferencesEle.appendChild(addElement(" ","PackingTime"));
	                 preferencesEle.appendChild(addElement(dateformat.format(customerFile.getSurvey())+" "+customerFile.getSurveyTime(),"VacationDate"));
	                 preferencesEle.appendChild(addElement(" ","SurveyEndDate"));
	                 preferencesEle.appendChild(addElement(" ","DepartureDate"));
	                 preferencesEle.appendChild(addElement(" ","PackingFinishDate"));
	                 preferencesEle.appendChild(addElement(" ","DeliveryFinishDate"));
	                 preferencesEle.appendChild(addElement(" ","ExportCustomsDate"));
	                 preferencesEle.appendChild(addElement(" ","OriginStorageInDate"));
	                 preferencesEle.appendChild(addElement(" ","OriginStorageOutDate"));
	                 preferencesEle.appendChild(addElement(" ","DestCustomsInDate"));
	                 preferencesEle.appendChild(addElement(" ","DestCustomsOutDate"));
	                 preferencesEle.appendChild(addElement(" ","DestStorageInDate"));
	                 preferencesEle.appendChild(addElement(" ","DestStorageOutDate"));
	                 preferencesEle.appendChild(addElement(" ","CompletionDate"));
	                 preferencesEle.appendChild(addElement(" ","DeliveryDate"));
	                 preferencesEle.appendChild(addElement(" ","RealArrivalDate"));
	                 preferencesEle.appendChild(addElement(" ","ServiceType"));
	                 preferencesEle.appendChild(addElement(" ","Comment"));
	                 preferencesEle.appendChild(addElement(" ","AdditionalServices"));
	         generalInfoEle.appendChild(preferencesEle);
	         generalInfoEle.appendChild(addElement(" ","Comment"));
rootEle.appendChild(generalInfoEle);  */
try{
//TransformerFactory instance is used to create Transformer objects. 
TransformerFactory factory = TransformerFactory.newInstance();
Transformer transformer = factory.newTransformer(); 
transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
// create string from xml tree
StringWriter sw = new StringWriter();
StreamResult result = new StreamResult(sw);
DOMSource source = new DOMSource(dom);
transformer.transform(source, result);
//System.out.println("SWWW--"+sw);
String xmlString = sw.toString();
//String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources");
	//uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/XML" + "/");
BufferedWriter bw = new BufferedWriter
              (new OutputStreamWriter(new FileOutputStream(file)));
bw.write(xmlString);
bw.flush();
bw.close();
	  /*TransformerFactory transformerFactory = TransformerFactory.newInstance();
    Transformer transformer = transformerFactory.newTransformer();
    DOMSource source = new DOMSource(dom);
    StreamResult result =  new StreamResult(System.out);
    transformer.transform(source, result);*/
}catch(Exception e){e.printStackTrace();}


try {
	    String msgText1="";
	    String subject = "Voxme XML of "+customerFile.getFirstName()+"_"+customerFile.getLastName()+" for Inventory on  "+dateformat.format(workTicket.getDate1())+" - "+dateformat.format(workTicket.getDate2());
		//String from = "info@redsky.com";
		String from = "support@redskymobility.com";
		String tempRecipient="";
		String tempRecipientArr[]=surveyarEmail.split(",");
		for(String str1:tempRecipientArr){
			if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
				if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
				tempRecipient += str1;
			}
		}
		surveyarEmail=tempRecipient;
		
		try{
			  emailSetupManager.globalEmailSetupProcess(from, surveyarEmail, "", "", filePath, msgText1, subject, sessionCorpID,"",serviceOrder.getShipNumber(),"");
				  
		  }
		  catch(Exception sfe)
		   {
			  sfe.printStackTrace();  
			 
		   }
		
		//EmailSetUpUtil.sendMail(surveyarEmail, "", "", subject, msgText1, filePath, from);
		return "Y";
} catch (Exception mex) {
		mex.printStackTrace();
		
		return "N"; 
	}
}

public String insuranceList(){
	getComboList(sessionCorpID);
	itemInsuranceList=inventoryDataManager.getItemList(sid,sessionCorpID);
	vehicleList=inventoryDataManager.getVehicle(sid,sessionCorpID);
	if(itemInsuranceList!=null && !itemInsuranceList.isEmpty()){
 		invListflag="1";
 	}
	return SUCCESS;
}
@SkipValidation
public String addRowToInsurance() throws Exception{
	serviceOrder=serviceOrderManager.get(sid);
	trackingStatus = trackingStatusManager.get(sid);
	 getComboList(sessionCorpID);
	 inventoryData=new InventoryData();
	 inventoryData.setQuantity("1");
	 inventoryData.setCft("0");
	 inventoryData.setTotal("0");
	 inventoryData.setWeight("0");
	 inventoryData.setCreatedOn(new Date());
	 inventoryData.setCreatedBy(getRequest().getRemoteUser());
	 inventoryData.setUpdatedOn(new Date());
	 inventoryData.setUpdatedBy(getRequest().getRemoteUser());
	 inventoryData.setCorpID(sessionCorpID);
	 inventoryData.setCustomerFileID(serviceOrder.getCustomerFileId());
	 inventoryData.setServiceOrderID(sid);
	 inventoryData=inventoryDataManager.save(inventoryData);
	itemInsuranceList=inventoryDataManager.getItemList(sid,sessionCorpID);
	vehicleList=inventoryDataManager.getVehicle(sid,sessionCorpID);
	/*inventoryData=new InventoryData();
	itemInsuranceList.add(inventoryData);*/
	try{
	      if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup()){
	    	  inventoryData.setNetworkSynchedId(inventoryData.getId());
			  inventoryData = inventoryDataManager.save(inventoryData);
	    	  List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			  List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder); 
			  serviceOrderRecords.remove(serviceOrder);
			  synchornizeInventoryDataForInsurance(serviceOrderRecords,inventoryData,serviceOrder,trackingStatus ,true,"SO");

	      }
	 }catch(Exception e){
		 e.printStackTrace();
		 
	 }
	if(itemInsuranceList!=null && !itemInsuranceList.isEmpty()){
 		invListflag="1";
 	}
	return SUCCESS;
}
private void synchornizeInventoryDataForInsurance(List<Object> records,	InventoryData inventoryDataOld, ServiceOrder serviceOrder,TrackingStatus trackingStatus, boolean isNew,String flag) {
	systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
	Long invId= new Long(0);
	Date createdOn = new Date();
	String cratedBy = "";
	Iterator  it=records.iterator();
	
	
	
	if(flag.equals("CF")){
		
	}else if(flag.equals("SO")){

		while(it.hasNext()){
			ServiceOrder so=(ServiceOrder)it.next();
			TrackingStatus trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(so.getId());
			if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup() && serviceOrder.getShipNumber()!=null && so.getShipNumber()!=null && (!(serviceOrder.getShipNumber().toString().trim().equalsIgnoreCase(so.getShipNumber().toString().trim()))) ){
			CustomerFile customerFileToRecods=so.getCustomerFile();
			InventoryData inventoryDataNew = new InventoryData();
			if(!isNew){
			/*	if(inventoryDataOld.getServiceOrderID()!=null){
					inventoryDataNew =  (InventoryData) inventoryDataManager.getLinkedInventoryData(inventoryDataOld.getId(),customerFileToRecods.getId(),so.getId());
				}else{*/
					inventoryDataNew =  (InventoryData) inventoryDataManager.getLinkedInventoryData(inventoryDataOld.getId(),customerFileToRecods.getId(),null);
				//}
				invId = inventoryDataNew.getId();
				cratedBy = inventoryDataNew.getCreatedBy();
				createdOn = inventoryDataNew.getCreatedOn();
			}
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(inventoryDataNew, inventoryDataOld);
				 if(isNew){
					 inventoryDataNew.setId(null);
					 inventoryDataNew.setCreatedBy("Networking");
					 inventoryDataNew.setCreatedOn(new Date());
				 }else{
					 inventoryDataNew.setId(invId);
					 inventoryDataNew.setCreatedBy(cratedBy);
					 inventoryDataNew.setCreatedOn(createdOn);
				 }
				 inventoryDataNew.setCustomerFileID(customerFileToRecods.getId());
				
				 inventoryDataNew.setCorpID(customerFileToRecods.getCorpID());
				 inventoryDataNew.setServiceOrderID(so.getId()); 
				 inventoryDataNew.setUpdatedBy("Networking");
				 inventoryDataNew.setUpdatedOn(new Date());
				 String unitW = systemDefaultManager.getCorpIdUnit(inventoryDataNew.getCorpID(),"weightUnit");
				 if(!unitW.equals("") && !unitW.equals(systemDefault.getWeightUnit())){
					 if(unitW.equals("Kgs")){
						 double temp = (Float.parseFloat(inventoryDataOld.getWeight())) * 0.4536;
						 temp =  temp*10000;
						 int tempIntValue = (int) temp;
						 temp = tempIntValue;
						 temp = temp/10000;
				    		inventoryDataNew.setWeight(""+temp);
				    		String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
				    		inventoryDataNew.setTotalWeight(total);
					 }else{
						 double temp = (Float.parseFloat(inventoryDataOld.getWeight())) * 2.2046;
						 temp =  temp*10000;
						 int tempIntValue = (int) temp;
						 temp = tempIntValue;
						 temp = temp/10000;
				    		inventoryDataNew.setWeight(""+temp);
				    		String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
				    		inventoryDataNew.setTotalWeight(total);
					 }
				 }
				 String unitVol = systemDefaultManager.getCorpIdUnit(inventoryDataNew.getCorpID(),"volumeUnit");
				 if(!unitVol.equals("") &&!unitVol.equals(systemDefault.getVolumeUnit())){
					 if(!unitVol.equalsIgnoreCase("Cft")){
							double temp = (Float.parseFloat(inventoryDataOld.getCft())) * 0.0283;
							 temp =  temp*10000;
							 int tempIntValue = (int) temp;
							 temp = tempIntValue;
							 temp = temp/10000;
				    		inventoryDataNew.setCft(""+temp);
				    		String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
				    		inventoryDataNew.setTotal(total);
						}else{
							double temp = (Float.parseFloat(inventoryDataOld.getCft())) * 35.3147;
							 temp =  temp*10000;
							 int tempIntValue = (int) temp;
							 temp = tempIntValue;
							 temp = temp/10000;
				    		inventoryDataNew.setCft(""+temp);
				    		String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
				    		inventoryDataNew.setTotal(total);
						}
				 }
				 inventoryDataNew.setNetworkSynchedId(inventoryDataOld.getId());
				 inventoryDataNew= inventoryDataManager.save(inventoryDataNew);
				 List oldInventoryPath = inventoryPathManager.getListByInventoryId(inventoryDataOld.getId());
					if(oldInventoryPath!=null && !oldInventoryPath.isEmpty()){
						Iterator itrInventoryPathId = oldInventoryPath.iterator();
						while(itrInventoryPathId.hasNext()){
							Long tempInvPathId = Long.parseLong(itrInventoryPathId.next().toString());
							InventoryPath oldInvPath = inventoryPathManager.get(tempInvPathId);
							InventoryPath newInvPath = new InventoryPath();
							try{
								BeanUtilsBean beanUtilsInvPath = BeanUtilsBean.getInstance();
								beanUtilsInvPath.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
								beanUtilsInvPath.copyProperties(newInvPath, oldInvPath);
								newInvPath.setId(null);
								newInvPath.setInventoryDataId(inventoryDataNew.getId());
								newInvPath.setAccessInfoId(null);
								inventoryPathManager.save(newInvPath);
							}catch(Exception ex){
								ex.printStackTrace();
							}
						}
					}
			}catch(Exception ex){
				String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
		    	  String logMethod =   ex.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , ex.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 ex.printStackTrace();
			}
		}
	
		}
	}

}
/*@SkipValidation
public String saveInsurance(){
	if(!checkData.equalsIgnoreCase("")){
		String[] str = checkData.split(",");
		String atricle="";
		int valuation=0;
		for(int i=0;i<str.length;i++){ 
			 String	idIns=str[i];	
			 if(!((idIns.equals("new")))){
				 inventoryData=inventoryDataManager.get(Long.parseLong(idIns)); 
				 atricle=getRequest().getParameter("atricle"+idIns);
				 inventoryData.setAtricle(atricle);
				 valuation=Integer.parseInt(getRequest().getParameter("valuation"+idIns));
				 inventoryData.setValuation(valuation);
				 inventoryData.setUpdatedBy(getRequest().getRemoteUser());
				 inventoryData.setUpdatedOn(new Date());
				 inventoryData.setServiceOrderID(sid);
				 inventoryDataManager.save(inventoryData);
			 }else{
				 inventoryData=new InventoryData(); 
				 inventoryData.setCreatedOn(new Date());
				 inventoryData.setCreatedBy(getRequest().getRemoteUser());
				 inventoryData.setUpdatedOn(new Date());
				 inventoryData.setUpdatedBy(getRequest().getRemoteUser());
				 inventoryData.setCorpID(sessionCorpID);
				 atricle=getRequest().getParameter("newDescription");
				 inventoryData.setAtricle(atricle);
				 inventoryData.setServiceOrderID(sid);
				 valuation=Integer.parseInt(getRequest().getParameter("newValuation"));
				 inventoryData.setValuation(valuation);
				 inventoryDataManager.save(inventoryData);
			 }
		}
	}	
	return SUCCESS;
}*/
private String vehicleIdNumber;
private Long autoId;
@SkipValidation
public String addRowToVehicle() throws Exception{
	serviceOrder=serviceOrderManager.get(sid);
	trackingStatus = trackingStatusManager.get(sid);
	getComboList(sessionCorpID);
	/*vehicle=new Vehicle();
	vehicleList.add(vehicle);*/
	 vehicle=new Vehicle();
	 vehicle.setCreatedOn(new Date());
	 vehicle.setCreatedBy(getRequest().getRemoteUser());
	 vehicle.setUpdatedOn(new Date());
	 vehicle.setUpdatedBy(getRequest().getRemoteUser());
	 vehicle.setCorpID(sessionCorpID);
	 vehicle.setServiceOrderId(sid);
	 vehicle.setShipNumber(serviceOrder.getShipNumber());
	 List maxIdNumber = vehicleManager.findMaximumIdNumber(serviceOrder.getShipNumber());
     if ( maxIdNumber.get(0) == null ) 
      {          
    	 vehicleIdNumber = "01";
   }else {
        	autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
      	if((autoId.toString()).length() == 1) 
      		{
      		vehicleIdNumber = "0"+(autoId.toString());
      		}else {
      			vehicleIdNumber=autoId.toString();
      			        
      			}
      	} 
     vehicle.setIdNumber(vehicleIdNumber);
     vehicle.setServiceOrder(serviceOrder);
     vehicle=vehicleManager.save(vehicle);
	itemInsuranceList=inventoryDataManager.getItemList(sid,sessionCorpID);
	vehicleList=inventoryDataManager.getVehicle(sid,sessionCorpID);
	if(itemInsuranceList!=null && !itemInsuranceList.isEmpty()){
 		invListflag="1";
 	}
	try{
	      if(serviceOrder.getIsNetworkRecord()){
	    	  List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			  List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder); 
			  synchornizeVehicle(serviceOrderRecords,vehicle,true,serviceOrder,trackingStatus);
	      }
	 }catch(Exception e){
		 e.printStackTrace();
	 }
	return SUCCESS;	
}
public String documentFileDelete(){
	vehicle=vehicleManager.get(id);
	//vehicleManager.remove(id);
	vehicleManager.updateStatus(id,vehicleStatus,getRequest().getRemoteUser());
	try{
    	
    	 Long serviceId=    vehicle.getServiceOrderId();
         serviceOrder=serviceOrderManager.get(serviceId);
         trackingStatus = trackingStatusManager.get(serviceId);
        if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup()){
        	List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder); 
			deleteVehicle(serviceOrderRecords,vehicle.getIdNumber(),vehicleStatus);
      }
      }catch(Exception ex){
    	 ex.printStackTrace();
      }
	getComboList(sessionCorpID);
	itemInsuranceList=inventoryDataManager.getItemList(sid,sessionCorpID);
	vehicleList=inventoryDataManager.getVehicle(sid,sessionCorpID);
	if(itemInsuranceList!=null && !itemInsuranceList.isEmpty()){
 		invListflag="1";
 	}
	return SUCCESS;	
}
public String documentFileArticle(){
	inventoryData=inventoryDataManager.get(id);
	inventoryDataManager.remove(id);
	try{
		
        Long serviceId=    inventoryData.getServiceOrderID();
        serviceOrder=serviceOrderManager.get(serviceId);
        trackingStatus = trackingStatusManager.get(serviceId);
        if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup()){
        	List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder); 
        	
			deleteInventoryData(serviceOrderRecords,inventoryData.getNetworkSynchedId());
      }
      }catch(Exception ex){
    	 ex.printStackTrace();
      }
	getComboList(sessionCorpID);
	itemInsuranceList=inventoryDataManager.getItemList(sid,sessionCorpID);
	vehicleList=inventoryDataManager.getVehicle(sid,sessionCorpID);
	if(itemInsuranceList!=null && !itemInsuranceList.isEmpty()){
 		invListflag="1";
 	}
	return SUCCESS;	
}
private void deleteInventoryData(List<Object> serviceOrderRecords,Long networkSynchedId) {
					
		Iterator  it=serviceOrderRecords.iterator();
	    while(it.hasNext()){
		ServiceOrder serviceOrder=(ServiceOrder)it.next();
		inventoryDataManager.deleteInventoryData(serviceOrder.getId(),networkSynchedId);
	}
	
}
private void deleteVehicle(List<Object> serviceOrderRecords,String idNumber,String vehicleStatus) { 
	Iterator  it=serviceOrderRecords.iterator();
	while(it.hasNext()){
		ServiceOrder serviceOrder=(ServiceOrder)it.next();
		vehicleManager.deleteNetworkVehicle(serviceOrder.getId(),idNumber,vehicleStatus);
	}
		
}
/*@SkipValidation
public String saveVehicle(){
	if(!checkDataVehicle.equalsIgnoreCase("")){
		String[] str = checkDataVehicle.split(",");
		String vehicleType="";
		String model="";
		int year=0;
		int valueEUR=0;
		
		for(int i=0;i<str.length;i++){ 
			 String	idIns=str[i];	
			 if(!((idIns.equals("new")))){
				 vehicle=vehicleManager.get(Long.parseLong(idIns)); 
				 vehicleType=getRequest().getParameter("vehicleType"+idIns);
				 vehicle.setVehicleType(vehicleType);
			     model=getRequest().getParameter("model"+idIns);
			     vehicle.setModel(model);
			     year=Integer.parseInt(getRequest().getParameter("year"+idIns));
			     vehicle.setYear(year);
			     valueEUR=Integer.parseInt(getRequest().getParameter("value"+idIns));
			     vehicle.setValuation(valueEUR);
				 vehicle.setUpdatedBy(getRequest().getRemoteUser());
				 vehicle.setUpdatedOn(new Date());
				 vehicle.setServiceOrderId(sid);
				 vehicleManager.save(vehicle);
			 }else{
				 vehicle=new Vehicle();
				 vehicle.setCreatedOn(new Date());
				 vehicle.setCreatedBy(getRequest().getRemoteUser());
				 vehicle.setUpdatedOn(new Date());
				 vehicle.setUpdatedBy(getRequest().getRemoteUser());
				 vehicle.setCorpID(sessionCorpID);
				
				 vehicle.setServiceOrderId(sid);
				 vehicleType=getRequest().getParameter("newVehicleType");
				 vehicle.setVehicleType(vehicleType);
			     model=getRequest().getParameter("newModel");
			     vehicle.setModel(model);
			     year=Integer.parseInt(getRequest().getParameter("newYear"));
			     vehicle.setYear(year);
			     valueEUR=Integer.parseInt(getRequest().getParameter("newValueEUR"));
			     vehicle.setValuation(valueEUR);
				 vehicleManager.save(vehicle);
			 }
		}
	}
	return SUCCESS;		
}*/
public String insuranceDetailedList(){
	getComboList(sessionCorpID);
	itemInsuranceList=inventoryDataManager.getItemList(sid,sessionCorpID);
	if(itemInsuranceList!=null && !itemInsuranceList.isEmpty()){
 		invListflag="1";
 	}
	vehicleList=inventoryDataManager.getVehicle(sid,sessionCorpID);
	return SUCCESS;
}
public void updateInsurance(){
	serviceOrder=serviceOrderManager.get(sid);
	inventoryDataManager.updateInsurance(newField,fieldValue,id,sid,serviceOrder.getCustomerFileId(),sessionCorpID);
	try{
		trackingStatus = trackingStatusManager.get(sid);
		inventoryData=inventoryDataManager.get(id); 
        if(serviceOrder.getIsNetworkRecord()  && trackingStatus.getSoNetworkGroup()){
        	List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder); 
        	
			updateLinkedInsuranceData(serviceOrderRecords,newField,fieldValue,inventoryData.getNetworkSynchedId());
      }
      }catch(Exception ex){
    	  ex.printStackTrace();
      }
	
}
private void updateLinkedInsuranceData(List<Object> serviceOrderRecords,String newField,String fieldValue,Long networkSynchedId) {
	
	Iterator  it=serviceOrderRecords.iterator();
    while(it.hasNext()){
	ServiceOrder serviceOrder=(ServiceOrder)it.next();
	inventoryDataManager.updateLinkedInsurance (newField,fieldValue,networkSynchedId,serviceOrder.getId(),serviceOrder.getCustomerFileId());
}

}
public void updateVehicle(){
	serviceOrder=serviceOrderManager.get(sid);
	inventoryDataManager.updateVehicle(newField,fieldValue,id,sid,vehicleIdNumber,sessionCorpID);
	try{
		trackingStatus = trackingStatusManager.get(sid);
		vehicle=vehicleManager.get(id); 
        if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup()){
        	List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder); 
        	
			updateLinkedVehicleData(serviceOrderRecords,newField,fieldValue,vehicle.getIdNumber());
      }
      }catch(Exception ex){
    	 ex.printStackTrace();
      }
}
private void updateLinkedVehicleData(List<Object> serviceOrderRecords,String newField,String fieldValue,String IdNumber) {
	
	Iterator  it=serviceOrderRecords.iterator();
    while(it.hasNext()){
	ServiceOrder serviceOrder=(ServiceOrder)it.next();
	inventoryDataManager.updateLinkedVehicleData (newField,fieldValue,serviceOrder.getId(),IdNumber);
}

}

@SkipValidation
public String inventoryListId(){
	//inventoryDataList=inventoryDataManager.getInventoryDataList(pieceID,sid,sessionCorpID);
	inventoryPackingList= inventoryPackingManager.findArticleByPieceId(sid,pieceID,sessionCorpID);
	return SUCCESS;
}

public List getArticlesList() {
	return articlesList;
}
public void setArticlesList(List articlesList) {
	this.articlesList = articlesList;
}
public CustomerFile getCustomerFile() {
	return customerFile;
}
public void setCustomerFile(CustomerFile customerFile) {
	this.customerFile = customerFile;
}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}


public String getSessionCorpID() {
	return sessionCorpID;
}
public void setSessionCorpID(String sessionCorpID) {
	this.sessionCorpID = sessionCorpID;
}
public void setCustomerFileManager(CustomerFileManager customerFileManager) {
	this.customerFileManager = customerFileManager;
}
public Long getCid() {
	return cid;
}
public void setCid(Long cid) {
	this.cid = cid;
}
public InventoryData getInventoryData() {
	return inventoryData;
}
public void setInventoryData(InventoryData inventoryData) {
	this.inventoryData = inventoryData;
}
public void setInventoryDataManager(InventoryDataManager inventoryDataManager) {
	this.inventoryDataManager = inventoryDataManager;
}
public Map<String, String> getMode() {
	return mode;
}
public void setMode(Map<String, String> mode) {
	this.mode = mode;
}
public void setRefMasterManager(RefMasterManager refMasterManager) {
	this.refMasterManager = refMasterManager;
}
public ServiceOrder getServiceOrder() {
	return serviceOrder;
}
public void setServiceOrder(ServiceOrder serviceOrder) {
	this.serviceOrder = serviceOrder;
}
public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
	this.serviceOrderManager = serviceOrderManager;
}
public Map<String, String> getRelocationServices() {
	return relocationServices;
}
public void setRelocationServices(Map<String, String> relocationServices) {
	this.relocationServices = relocationServices;
}
public Long getSid() {
	return sid;
}
public void setSid(Long sid) {
	this.sid = sid;
}
public List getUpLoadedImages() {
	return upLoadedImages;
}
public void setUpLoadedImages(List upLoadedImages) {
	this.upLoadedImages = upLoadedImages;
}
public void setInventoryPathManager(InventoryPathManager inventoryPathManager) {
	this.inventoryPathManager = inventoryPathManager;
}
public String getAttachmentContentType() {
	return attachmentContentType;
}
public void setAttachmentContentType(String attachmentContentType) {
	this.attachmentContentType = attachmentContentType;
}
public String getAttachmentFileName() {
	return attachmentFileName;
}
public void setAttachmentFileName(String attachmentFileName) {
	this.attachmentFileName = attachmentFileName;
}
public File getAttachment() {
	return attachment;
}
public void setAttachment(File attachment) {
	this.attachment = attachment;
}
public String getHitflag() {
	return hitflag;
}
public void setHitflag(String hitflag) {
	this.hitflag = hitflag;
}
public InventoryPath getInventoryPath() {
	return inventoryPath;
}
public void setInventoryPath(InventoryPath inventoryPath) {
	this.inventoryPath = inventoryPath;
}
public List getUpLoadedImages2() {
	return upLoadedImages2;
}
public void setUpLoadedImages2(List upLoadedImages2) {
	this.upLoadedImages2 = upLoadedImages2;
}
public String getSOflag() {
	return SOflag;
}
public void setSOflag(String oflag) {
	SOflag = oflag;
}
public void setAccessInfoManager(AccessInfoManager accessInfoManager) {
	this.accessInfoManager = accessInfoManager;
}
public AccessInfo getAccessInfo() {
	return accessInfo;
}
public void setAccessInfo(AccessInfo accessInfo) {
	this.accessInfo = accessInfo;
}

public String getShipAndModeList() {
	return shipAndModeList;
}
public void setShipAndModeList(String shipAndModeList) {
	this.shipAndModeList = shipAndModeList;
}
public String getModeWithOutSO() {
	return modeWithOutSO;
}
public void setModeWithOutSO(String modeWithOutSO) {
	this.modeWithOutSO = modeWithOutSO;
}
public String getModeWithOneSO() {
	return modeWithOneSO;
}
public void setModeWithOneSO(String modeWithOneSO) {
	this.modeWithOneSO = modeWithOneSO;
}
public String getModeSORelation() {
	return modeSORelation;
}
public void setModeSORelation(String modeSORelation) {
	this.modeSORelation = modeSORelation;
}
public Map getElevatorType() {
	return elevatorType;
}
public void setElevatorType(Map elevatorType) {
	this.elevatorType = elevatorType;
}
public Map getParkingType() {
	return parkingType;
}
public void setParkingType(Map parkingType) {
	this.parkingType = parkingType;
}
public Map getResidenseType() {
	return residenseType;
}
public void setResidenseType(Map residenseType) {
	this.residenseType = residenseType;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}

public SortedMap getModeShipMap() {
	return modeShipMap;
}
public void setModeShipMap(SortedMap modeShipMap) {
	this.modeShipMap = modeShipMap;
}
public String getAccessInfoType() {
	return accessInfoType;
}
public void setAccessInfoType(String accessInfoType) {
	this.accessInfoType = accessInfoType;
}
public void setItemsJbkEquipManager(ItemsJbkEquipManager itemsJbkEquipManager) {
	this.itemsJbkEquipManager = itemsJbkEquipManager;
}
public List getWorkTicketList() {
	return workTicketList;
}
public void setWorkTicketList(List workTicketList) {
	this.workTicketList = workTicketList;
}
public List getTicketList() {
	return ticketList;
}
public void setTicketList(List ticketList) {
	this.ticketList = ticketList;
}
public void setInventoryArticleManager(
		InventoryArticleManager inventoryArticleManager) {
	this.inventoryArticleManager = inventoryArticleManager;
}
public List getArtileList() {
	return artileList;
}
public void setArtileList(List artileList) {
	this.artileList = artileList;
}
public String getInventoryTransferflag() {
	return inventoryTransferflag;
}
public void setInventoryTransferflag(String inventoryTransferflag) {
	this.inventoryTransferflag = inventoryTransferflag;
}
public String getQuote() {
	return Quote;
}
public void setQuote(String quote) {
	Quote = quote;
}
public void setInventoryPackingManager(
		InventoryPackingManager inventoryPackingManager) {
	this.inventoryPackingManager = inventoryPackingManager;
}
public InventoryPacking getInventoryPacking() {
	return inventoryPacking;
}
public void setInventoryPacking(InventoryPacking inventoryPacking) {
	this.inventoryPacking = inventoryPacking;
}
public List getInventoryPackingList() {
	return inventoryPackingList;
}
public void setInventoryPackingList(List inventoryPackingList) {
	this.inventoryPackingList = inventoryPackingList;
}
public String getRoom() {
	return room;
}
public void setRoom(String room) {
	this.room = room;
}
public String getArticle() {
	return article;
}
public void setArticle(String article) {
	this.article = article;
}
public List getInventoryDataListFromPieceId() {
	return inventoryDataListFromPieceId;
}
public void setInventoryDataListFromPieceId(List inventoryDataListFromPieceId) {
	this.inventoryDataListFromPieceId = inventoryDataListFromPieceId;
}
public String getPieceID() {
	return pieceID;
}
public void setPieceID(String pieceID) {
	this.pieceID = pieceID;
}
public String getMaterialTransferFlag() {
	return materialTransferFlag;
}
public void setMaterialTransferFlag(String materialTransferFlag) {
	this.materialTransferFlag = materialTransferFlag;
}
public String getModeMessage() {
	return modeMessage;
}
public void setModeMessage(String modeMessage) {
	this.modeMessage = modeMessage;
}
public String getChkSave() {
	return chkSave;
}
public void setChkSave(String chkSave) {
	this.chkSave = chkSave;
}
public String getCode() {
	return code;
}
public void setCode(String code) {
	this.code = code;
}
public String getCrewMailID() {
	return crewMailID;
}
public void setCrewMailID(String crewMailID) {
	this.crewMailID = crewMailID;
}
public List getInventoryPackingitems() {
	return inventoryPackingitems;
}
public void setInventoryPackingitems(List inventoryPackingitems) {
	this.inventoryPackingitems = inventoryPackingitems;
}
public Map getConditionType() {
	return conditionType;
}
public void setConditionType(Map conditionType) {
	this.conditionType = conditionType;
}
public String getResults() {
	return results;
}
public void setResults(String results) {
	this.results = results;
}
public String getMailId() {
	return mailId;
}
public void setMailId(String mailId) {
	this.mailId = mailId;
}
public Long getWrkTckId() {
	return wrkTckId;
}
public void setWrkTckId(Long wrkTckId) {
	this.wrkTckId = wrkTckId;
}
public WorkTicket getWorkTicket() {
	return workTicket;
}
public void setWorkTicket(WorkTicket workTicket) {
	this.workTicket = workTicket;
}
public void setWorkTicketManager(WorkTicketManager workTicketManager) {
	this.workTicketManager = workTicketManager;
}
public Billing getBilling() {
	return billing;
}
public void setBilling(Billing billing) {
	this.billing = billing;
}
public void setBillingManager(BillingManager billingManager) {
	this.billingManager = billingManager;
}

public List getVehicleList() {
	return vehicleList;
}
public void setVehicleList(List vehicleList) {
	this.vehicleList = vehicleList;
}
public List getItemInsuranceList() {
	return itemInsuranceList;
}
public void setItemInsuranceList(List itemInsuranceList) {
	this.itemInsuranceList = itemInsuranceList;
}
public String getCheckData() {
	return checkData;
}
public void setCheckData(String checkData) {
	this.checkData = checkData;
}
public void setVehicleManager(VehicleManager vehicleManager) {
	this.vehicleManager = vehicleManager;
}
public Vehicle getVehicle() {
	return vehicle;
}
public void setVehicle(Vehicle vehicle) {
	this.vehicle = vehicle;
}
public String getCheckDataVehicle() {
	return checkDataVehicle;
}
public void setCheckDataVehicle(String checkDataVehicle) {
	this.checkDataVehicle = checkDataVehicle;
}
public String getCommodity() {
	return commodity;
}
public void setCommodity(String commodity) {
	this.commodity = commodity;
}

public Long getAutoId() {
	return autoId;
}
public void setAutoId(Long autoId) {
	this.autoId = autoId;
}
public String getVehicleIdNumber() {
	return vehicleIdNumber;
}
public void setVehicleIdNumber(String vehicleIdNumber) {
	this.vehicleIdNumber = vehicleIdNumber;
}
public String getNewField() {
	return newField;
}
public void setNewField(String newField) {
	this.newField = newField;
}
public String getFieldValue() {
	return fieldValue;
}
public void setFieldValue(String fieldValue) {
	this.fieldValue = fieldValue;
}
public Map<String, String> getVehicleTypeList() {
	return vehicleTypeList;
}
public void setVehicleTypeList(Map<String, String> vehicleTypeList) {
	this.vehicleTypeList = vehicleTypeList;
}
public String getInvListflag() {
	return invListflag;
}
public void setInvListflag(String invListflag) {
	this.invListflag = invListflag;
}
public Long getAid() {
	return aid;
}
public void setAid(Long aid) {
	this.aid = aid;
}
public Map<String, String> getRoomType() {
	return roomType;
}
public void setRoomType(Map<String, String> roomType) {
	this.roomType = roomType;
}
public List getInvSumList() {
	return invSumList;
}
public void setInvSumList(List invSumList) {
	this.invSumList = invSumList;
}
public BigDecimal getQuatSum() {
	return quatSum;
}
public void setQuatSum(BigDecimal quatSum) {
	this.quatSum = quatSum;
}
public BigDecimal getVolSum() {
	return volSum;
}
public void setVolSum(BigDecimal volSum) {
	this.volSum = volSum;
}
public BigDecimal getTotSum() {
	return totSum;
}
public void setTotSum(BigDecimal totSum) {
	this.totSum = totSum;
}
public List getSoList() {
	return soList;
}
public void setSoList(List soList) {
	this.soList = soList;
}
public List getResultSOList() {
	return resultSOList;
}
public void setResultSOList(List resultSOList) {
	this.resultSOList = resultSOList;
}
/**
 * @return the preConditionList
 */
public List getPreConditionList() {
	return preConditionList;
}
/**
 * @param preConditionList the preConditionList to set
 */
public void setPreConditionList(List preConditionList) {
	this.preConditionList = preConditionList;
}
/**
 * @param inventoryLocationManager the inventoryLocationManager to set
 */
public void setInventoryLocationManager(
		InventoryLocationManager inventoryLocationManager) {
	this.inventoryLocationManager = inventoryLocationManager;
}
public String getShipno() {
	return shipno;
}
public void setShipno(String shipno) {
	this.shipno = shipno;
}
public String getPage() {
	return page;
}
public void setPage(String page) {
	this.page = page;
}
public List getInventoryRoomsList() {
	return inventoryRoomsList;
}
public void setInventoryRoomsList(List inventoryRoomsList) {
	this.inventoryRoomsList = inventoryRoomsList;
}
public void setRoomManager(RoomManager roomManager) {
	this.roomManager = roomManager;
}
public List getTicketInvData() {
	return ticketInvData;
}
public void setTicketInvData(List ticketInvData) {
	this.ticketInvData = ticketInvData;
}
public void setWorkTicketInventoryDataManager(
		WorkTicketInventoryDataManager workTicketInventoryDataManager) {
	this.workTicketInventoryDataManager = workTicketInventoryDataManager;
}
public String getFieldName() {
	return fieldName;
}
public void setFieldName(String fieldName) {
	this.fieldName = fieldName;
}
public String getIdURL() {
	return idURL;
}
public void setIdURL(String idURL) {
	this.idURL = idURL;
}
public SystemDefault getSystemDefault() {
	return systemDefault;
}
public void setSystemDefault(SystemDefault systemDefault) {
	this.systemDefault = systemDefault;
}
public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
	this.systemDefaultManager = systemDefaultManager;
}
public void setErrorLogManager(ErrorLogManager errorLogManager) {
	this.errorLogManager = errorLogManager;
}
public TrackingStatus getTrackingStatus() {
	return trackingStatus;
}
public void setTrackingStatus(TrackingStatus trackingStatus) {
	this.trackingStatus = trackingStatus;
}
public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
	this.trackingStatusManager = trackingStatusManager;
}
public Company getCompany() {
	return company;
}
public void setCompany(Company company) {
	this.company = company;
}
public void setCompanyManager(CompanyManager companyManager) {
	this.companyManager = companyManager;
}
public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
	this.miscellaneousManager = miscellaneousManager;
}
public String getUsertype() {
	return usertype;
}
public void setUsertype(String usertype) {
	this.usertype = usertype;
}
public void setUserManager(UserManager userManager) {
	this.userManager = userManager;
}
public List getServiceOrders() {
	return serviceOrders;
}
public void setServiceOrders(List serviceOrders) {
	this.serviceOrders = serviceOrders;
}
public AdAddressesDetails getAdAddressesDetails() {
	return adAddressesDetails;
}
public void setAdAddressesDetails(AdAddressesDetails adAddressesDetails) {
	this.adAddressesDetails = adAddressesDetails;
}
public AdAddressesDetailsManager getAdAddressesDetailsManager() {
	return adAddressesDetailsManager;
}
public void setAdAddressesDetailsManager(
		AdAddressesDetailsManager adAddressesDetailsManager) {
	this.adAddressesDetailsManager = adAddressesDetailsManager;
}
public EmailSetupManager getEmailSetupManager() {
	return emailSetupManager;
}
public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
	this.emailSetupManager = emailSetupManager;
}
public String getVehicleStatus() {
	return vehicleStatus;
}
public void setVehicleStatus(String vehicleStatus) {
	this.vehicleStatus = vehicleStatus;
}
public String getOiJobList() {
	return oiJobList;
}
public void setOiJobList(String oiJobList) {
	this.oiJobList = oiJobList;
}
public String getDashBoardHideJobsList() {
	return dashBoardHideJobsList;
}
public void setDashBoardHideJobsList(String dashBoardHideJobsList) {
	this.dashBoardHideJobsList = dashBoardHideJobsList;
}
public String getAccCorpID() {
	return accCorpID;
}
public void setAccCorpID(String accCorpID) {
	this.accCorpID = accCorpID;
}
}
