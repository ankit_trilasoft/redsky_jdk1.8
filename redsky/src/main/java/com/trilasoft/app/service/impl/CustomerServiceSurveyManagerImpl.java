package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CustomerServiceSurveyDao;
import com.trilasoft.app.dao.MiscellaneousDao;
import com.trilasoft.app.model.CustomerServiceSurvey;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.service.CustomerServiceSurveyManager;
import com.trilasoft.app.service.MiscellaneousManager;

public class CustomerServiceSurveyManagerImpl extends GenericManagerImpl<CustomerServiceSurvey, Long> implements CustomerServiceSurveyManager{

	CustomerServiceSurveyDao customerServiceSurveyDao; 
    public CustomerServiceSurveyManagerImpl(CustomerServiceSurveyDao customerServiceSurveyDao)
    { 
        super(customerServiceSurveyDao); 
        this.customerServiceSurveyDao = customerServiceSurveyDao; 
    } 
    public Map<String,String> getCustomerServiceList(Long id){
    	return customerServiceSurveyDao.getCustomerServiceList(id);
    }
}
