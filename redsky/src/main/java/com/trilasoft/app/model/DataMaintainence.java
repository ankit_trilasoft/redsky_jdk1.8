package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="datamaintainence")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class DataMaintainence extends BaseObject{
	
	private Long id;
	private String corpID;
	private String description;
	private String query;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("description", description).append("query",
				query).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DataMaintainence))
			return false;
		DataMaintainence castOther = (DataMaintainence) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(description, castOther.description)
				.append(query, castOther.query).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(
				description).append(query).toHashCode();
	}
	@Column 
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	

}
