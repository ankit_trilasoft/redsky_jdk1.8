package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.SystemDefaultManager;

public class SystemDefaultAction extends BaseAction implements Preparable{

	private String sessionCorpID;

	private List ls;

	private List refMasters;

	private RefMasterManager refMasterManager;

	private Map<String, String> baseCurrency;

	private Map<String, String> invcewh;

	private Map<String, String> state;

	private Map<String, String> forworkTickets;

	private Map<String, String> paid;

	private Map<String, String> typeMiles;
	private  Map<String, String> claimsPerson = new LinkedHashMap<String, String>();
	private  Map<String, String> coordinatorList = new LinkedHashMap<String, String>();
	
	
	private static List weightunits;

	private static List volumeunits;

	private static List filterCoordinator;

	private Map<String, String> accountingInterface;

	private Map<String, String> accountingSystem;

	private SystemDefaultManager systemDefaultManager;

	private SystemDefault systemDefault;

	public List systemDefaults;

	private Long id;

	private Long maxId;
	private String weightType;

	private Map<String, String> country;

	private List statess;

	private CustomerFileManager customerFileManager;
	private String bucket2;
	private Company company;
	private CompanyManager companyManager;
	private String enbState;
	private Map<String, String> countryCod;
	private static List unitVariable;
	private List operationMgmtBy;
	private String cportalActivation;
	private Map<String, String> recVatList;
	private Map<String, String> payVatList;
	private Map<String, String> tcktservc;
	private Map<String, String> house;
	
		public void prepare() throws Exception {
		enbState = customerFileManager.enableStateList1(sessionCorpID);
		
	}
	
	public SystemDefaultAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	public String List() {
		systemDefaults = systemDefaultManager.getAll();
		return SUCCESS;
	}
	// Setting time zero
	public Date getZeroTimeDate(Date dt) {
	    Date res = dt;
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(dt);
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    res = calendar.getTime();
	    return res;
	}
	@SkipValidation
	public String findStateList() {
		statess = customerFileManager.findStateList(bucket2, sessionCorpID);
		return SUCCESS;
	}

	public String edit() {
		if ((systemDefaultManager.findByCorpID(sessionCorpID)).isEmpty()) {

		} else {
			id = Long.parseLong(systemDefaultManager.findIdByCorpID(sessionCorpID).get(0).toString());
		}
		if (id != null) {
			systemDefault = systemDefaultManager.get(id);
			getComboList(sessionCorpID);
			if(systemDefault.getWeightUnit().equalsIgnoreCase("Lbs")){
    			weightType="lbscft";
    		}
    		else{
    			weightType="kgscbm";
    		}
		} else {
			getComboList(sessionCorpID);
			systemDefault = new SystemDefault();
			systemDefault.setCreatedOn(new Date());
			systemDefault.setFilterCoordinator("No");
		}
		systemDefault.setCorpID(sessionCorpID);
		return SUCCESS;
	}

	public String save() throws Exception {
		boolean isNew = (systemDefault.getId() == null);
		if (isNew) {
			systemDefault.setCreatedOn(new Date());
		}
		getComboList(sessionCorpID);
		systemDefault.setCorpID(sessionCorpID);
		if(cportalActivation==null || cportalActivation.equals("")){
			systemDefault.setMinInsurancePerUnit("");
		}
		systemDefault = systemDefaultManager.save(systemDefault);
		systemDefault.setUpdatedOn(new Date());
		systemDefault.setUpdatedBy(getRequest().getRemoteUser());
		String key = (isNew) ? "systemDefault.added" : "systemDefault.updated";
		saveMessage(getText(key));
		return SUCCESS;

	}
	
	public String saveDailyOps() throws Exception {
		if(systemDefault != null){
			if(systemDefault.getDailyOperationalLimit()!= null && systemDefault.getDailyOpLimitPC() != null && systemDefault.getMinServiceDay() != null){
				systemDefaultManager.updateDailyOpsLimit(systemDefault.getDailyOperationalLimit(), systemDefault.getDailyOpLimitPC(), systemDefault.getMinServiceDay(), sessionCorpID);
				saveMessage("Operational management control has been updated");
			}
		}else{
			if ((systemDefaultManager.findByCorpID(sessionCorpID)).isEmpty()) {

			} else {
				id = Long.parseLong(systemDefaultManager.findIdByCorpID(sessionCorpID).get(0).toString());
			}
			if (id != null) {
				systemDefault = systemDefaultManager.get(id);
			} 
		}
		return SUCCESS;
	}

	public void getComboList(String corpId) {
		baseCurrency = refMasterManager.findCodeOnleByParameter(corpId, "CURRENCY");
		invcewh = refMasterManager.findByParameter(corpId, "INVCEWH");
		state = refMasterManager.findByParameter(corpId, "STATE");
		forworkTickets = refMasterManager.findByParameter(corpId, "FORWORKTICKETS");
		paid = refMasterManager.findByParameter(corpId, "PAID");
		typeMiles = refMasterManager.findByParameter(corpId, "TYPEMILES");
		accountingSystem = refMasterManager.findByParameter(corpId, "ACC_SYS");
		accountingInterface = refMasterManager.findByParameter(corpId, "ACC_INTF");
		claimsPerson=refMasterManager.findUser(corpId, "ROLE_EMPLOYEE");
		country = refMasterManager.findByParameter(corpId, "COUNTRY");
		coordinatorList = refMasterManager.findUser(sessionCorpID,"ROLE_COORD");
		countryCod = refMasterManager.findByParameter(corpId, "COUNTRY");
		recVatList=refMasterManager.findByParameterWithoutParent(corpId, "EUVAT");
		payVatList=refMasterManager.findByParameterWithoutParent (corpId, "PAYVATDESC");
		weightunits = new ArrayList();
		weightunits.add("Lbs");
		weightunits.add("Kgs");

		volumeunits = new ArrayList();
		volumeunits.add("Cft");
		volumeunits.add("Cbm");

		filterCoordinator = new ArrayList();
		filterCoordinator.add("Yes");
		filterCoordinator.add("No");
		company= companyManager.findByCorpID(sessionCorpID).get(0);		
		
		unitVariable = new ArrayList();
		unitVariable.add("Yes");
		unitVariable.add("No");
		
		operationMgmtBy = new ArrayList();
		operationMgmtBy.add("By Hub");
		operationMgmtBy.add("By Warehouse");
		tcktservc= refMasterManager.findByParameter(sessionCorpID, "TCKTSERVC");
		house =	refMasterManager.findByParameter(sessionCorpID, "HOUSE");
	}

	public Map<String, String> getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(Map<String, String> baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public List getRefMasters() {
		return refMasters;
	}

	public void setRefMasters(List refMasters) {
		this.refMasters = refMasters;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public List getSystemDefaults() {
		return systemDefaults;
	}

	public void setSystemDefaults(List systemDefaults) {
		this.systemDefaults = systemDefaults;
	}

	public List getLs() {
		return ls;
	}

	public void setLs(List ls) {
		this.ls = ls;
	}

	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

	public Long getMaxId() {
		return maxId;
	}

	public void setMaxId(Long maxId) {
		this.maxId = maxId;
	}

	public List getCompanies() {
		return systemDefaults;
	}

	public void setCompanies(List systemDefaults) {
		this.systemDefaults = systemDefaults;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Map<String, String> getInvcewh() {
		return invcewh;
	}

	public void setInvcewh(Map<String, String> invcewh) {
		this.invcewh = invcewh;
	}

	public Map<String, String> getState() {
		return state;
	}

	public void setState(Map<String, String> state) {
		this.state = state;
	}

	public Map<String, String> getForworkTickets() {
		return forworkTickets;
	}

	public void setForworkTickets(Map<String, String> forworkTickets) {
		this.forworkTickets = forworkTickets;
	}

	public Map<String, String> getPaid() {
		return paid;
	}

	public void setPaid(Map<String, String> paid) {
		this.paid = paid;
	}

	public Map<String, String> getTypeMiles() {
		return typeMiles;
	}

	public void setTypeMiles(Map<String, String> typeMiles) {
		this.typeMiles = typeMiles;
	}

	public List getWeightunits() {
		return weightunits;
	}

	public List getVolumeunits() {
		return volumeunits;
	}

	public Map<String, String> getAccountingSystem() {
		return accountingSystem;
	}

	public void setAccountingSystem(Map<String, String> accountingSystem) {
		this.accountingSystem = accountingSystem;
	}

	public Map<String, String> getAccountingInterface() {
		return accountingInterface;
	}

	public void setAccountingInterface(Map<String, String> accountingInterface) {
		this.accountingInterface = accountingInterface;
	}

	public static List getFilterCoordinator() {
		return filterCoordinator;
	}

	public static void setFilterCoordinator(List filterCoordinator) {
		SystemDefaultAction.filterCoordinator = filterCoordinator;
	}

	/**
	 * @return the weightType
	 */
	public String getWeightType() {
		return weightType;
	}

	/**
	 * @param weightType the weightType to set
	 */
	public void setWeightType(String weightType) {
		this.weightType = weightType;
	}

	public Map<String, String> getClaimsPerson() {
		return claimsPerson;
	}

	public Map<String, String> getCountry() {
		return country;
	}

	public void setCountry(Map<String, String> country) {
		this.country = country;
	}

	public List getStatess() {
		return statess;
	}

	public void setStatess(List statess) {
		this.statess = statess;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public String getBucket2() {
		return bucket2;
	}

	public void setBucket2(String bucket2) {
		this.bucket2 = bucket2;
	}

	public Map<String, String> getCoordinatorList() {
		return coordinatorList;
	}

	public void setCoordinatorList(Map<String, String> coordinatorList) {
		this.coordinatorList = coordinatorList;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public String getEnbState() {
		return enbState;
	}

	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}

	public Map<String, String> getCountryCod() {
		return countryCod;
	}

	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}

	public static List getUnitVariable() {
		return unitVariable;
	}

	public static void setUnitVariable(List unitVariable) {
		SystemDefaultAction.unitVariable = unitVariable;
	}

	public List getOperationMgmtBy() {
		return operationMgmtBy;
	}

	public void setOperationMgmtBy(List operationMgmtBy) {
		this.operationMgmtBy = operationMgmtBy;
	}

	public String getCportalActivation() {
		return cportalActivation;
	}

	public void setCportalActivation(String cportalActivation) {
		this.cportalActivation = cportalActivation;
	}

	public Map<String, String> getRecVatList() {
		return recVatList;
	}

	public void setRecVatList(Map<String, String> recVatList) {
		this.recVatList = recVatList;
	}

	public Map<String, String> getPayVatList() {
		return payVatList;
	}

	public void setPayVatList(Map<String, String> payVatList) {
		this.payVatList = payVatList;
	}

	public Map<String, String> getTcktservc() {
		return tcktservc;
	}

	public void setTcktservc(Map<String, String> tcktservc) {
		this.tcktservc = tcktservc;
	}

	public Map<String, String> getHouse() {
		return house;
	}

	public void setHouse(Map<String, String> house) {
		this.house = house;
	}
}