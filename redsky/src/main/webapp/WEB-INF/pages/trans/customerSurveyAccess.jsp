<%@ include file="/common/taglibs.jsp"%>
<head>
	<title>Customer Response Access</title>
	<meta name="heading" content="Customer Request Access"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<s:form id="customerRequestAccess" name="customerRequestAccess" action="saveCustomerRequestAccess.html?decorator=popup&popup=true&bypass_sessionfilter=YES&soId=${soId}&sessionCorpID=${sessionCorpID}&rtNumber=${rtNumber}&surveyJob=${surveyJob}&services=${services}&MMGServices=${MMGServices}&notesAddForServices=${notesAddForServices}" method="post" validate="true">

<!--<c:set var="soId" value="<%=request.getParameter("soId") %>" />
<s:textfield name="soId" value="<%=request.getParameter("soId") %>" />
<c:set var="sessionCorpID" value="<%=request.getParameter("sessionCorpID") %>" />
<s:hidden name="sessionCorpID" value="<%=request.getParameter("sessionCorpID") %>" />
<c:set var="rtNumber" value="<%=request.getParameter("rtNumber") %>" />
<s:hidden name="rtNumber" value="<%=request.getParameter("rtNumber") %>" />
-->
<c:set var="surveyJob" value="<%=request.getParameter("surveyJob") %>" />
<body>
<table border="0"  cellpadding="0" cellspacing="0" style="width: 780px; border: 2px solid rgb(116, 179, 220); padding: 3px;">
  <tr>
  <c:if test="${surveyJob=='INT'}">
    <td><a href="http://www.highlandmoving.com/"><img src="<c:url value='/images/Highland-main.jpg'/>" width="899" height="296" /></a></td>
    </c:if>
    <c:if test="${surveyJob=='RLO'}">
    <td><a href="http://www.hsrelo.com//"><img src="<c:url value='/images/highstar-logo-SMALL.jpg'/>"  /></a></td>
    </c:if>
      <c:if test="${surveyJob=='FINALRLO'}">
    <td><a href="http://www.hsrelo.com//"> <img src="<c:url value='/images/highstar-logo-SMALL.jpg'/>"  /></a></td>
    </c:if>
  </tr>
  <tr><td height="40"></td></tr>
  <c:if test="${CustomerFeedbackStatus=='[]'}">
  <tr>
    <td valign="middle" align="left" style="padding-left:10%"><label>
    <h3 class="listwhitetext" onkeyup="" style="color: rgb(114, 183, 229); font-size: 28px; font-family: verdana;line-height:0.6em;">What key factors influenced your response?</h3>
    <s:textarea id="customerFeedback" name="customerFeedback" cssStyle="width:580px;height:100px;" cssClass="textarea" ></s:textarea><!--
      <textarea name="textarea" id="customerFeedback" n cols="45" rows="5" style="border:1px solid #219DD1;"></textarea>
    --></label></td>
  </tr>
    <tr><td height="10"></td></tr>
  <tr>
    <td align="center" valign="middle"><label>
    <s:submit  cssClass="cssbutton1" type="button" cssStyle="width:65px;"  value="Submit" ></s:submit>
    </label></td>
  </tr>
  </c:if>
  <c:if test="${CustomerFeedbackStatus!='[]'}">
   <tr>
    <td valign="middle" align="left" style="padding-left:24%"><label>
    <h3 class="listwhitetext" style="font-size:15px;"><b>Sorry, your survey has already been submitted.<br>Please click our Logo on the left to return to our home page.</b></h3>
    </td>
  </tr>
  </c:if>
  <tr><td height="10"></td></tr>
</table>
</body>
</s:form>