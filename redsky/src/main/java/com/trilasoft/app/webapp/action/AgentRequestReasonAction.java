package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.AgentRequest;
import com.trilasoft.app.model.AgentRequestReason;
import com.trilasoft.app.service.AgentRequestManager;
import com.trilasoft.app.service.AgentRequestReasonManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.RefMasterManager;
import org.appfuse.service.UserManager;

import org.apache.log4j.Logger;

	public class AgentRequestReasonAction extends BaseAction implements Preparable{
		
		private Long id;
		private String sessionCorpID;
		private AgentRequestReason agentRequestReason;
		private AgentRequestReasonManager agentRequestReasonManager;
		private AgentRequest agentRequest;
		private AgentRequestManager agentRequestManager;
		private Long partnerPrivateId;
		private String partnerCode;
		private String partnerPrivateURL;
		private EmailSetupManager emailSetupManager;
		 private String agentStatus;
		 private UserManager userManager;
		static final Logger logger = Logger.getLogger(AgentRequestReasonAction.class);
		
	public void prepare() throws Exception {		
	}
	 private RefMasterManager refMasterManager;
	 private Map<String, String> agentRequestStatus;
	@SkipValidation
	public String getComboList(String corpId) {
		
		
		reason=refMasterManager.findByParameter(corpId, "AGENTREQUESTREASON");
		agentRequestStatus = refMasterManager.findByParameter(corpId, "AGENTREQUESTSTATUS");
		
		return SUCCESS;
	}
	
	public AgentRequestReasonAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
	}
	private Long agentId;
	public String editAgentRequestReason()
	{
		
		System.out.println("hello--------------------"+agentId);
	
		 
    	try {
			getComboList(sessionCorpID);
			if(agentId!=null)
			  {
				  agentRequest = agentRequestManager.get(agentId);
			  }
			  
			    agentRequestReason = new AgentRequestReason();
			    agentRequestReason.setCreatedOn(new Date());
			    agentRequestReason.setCreatedBy(getRequest().getRemoteUser());
				agentRequestReason.setUpdatedOn(new Date());
				agentRequestReason.setComment(comment);
				agentRequestReason.setReason(agentReason);
				agentRequestReason.setAgentRequestId(agentRequest.getId());
				agentRequestReason.setCorpID(sessionCorpID);
			
			
		 }catch (NumberFormatException e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	 // errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	 return CANCEL;
		}  
      
        return SUCCESS;   
    
	}
	private String comment;
	private Map<String, String> reason;
	private String agentReason;
	private String createdBy;
	
	
	  private List donotMergelist;
	public String edit() {
		getComboList(sessionCorpID);
		agentRequestReason=agentRequestReasonManager.getByID(agentRequestId);
		if (agentRequestReason.getId()!= null) {
			agentRequestReason = agentRequestReasonManager.get(agentRequestReason.getId());
			
		} else {
			agentRequestReason = new AgentRequestReason(); 
			agentRequestReason.setCreatedOn(new Date());
			agentRequestReason.setUpdatedOn(new Date());
			agentRequestReason.setComment(comment);
			agentRequestReason.setReason(agentReason);
			agentRequestReason.setAgentRequestId(agentRequestId);
			agentRequestReason.setCorpID(sessionCorpID);
			
			
		} 
		return SUCCESS;
	}
	private String hitFlag;
	private Long agentRequestId;
	private String corpId;
	public String save() throws Exception {	
		logger.warn("USER AUDIT: ID:start-----"+agentId+"---comment----"+comment+"---reason----"+agentReason);
		if(agentId!=null)
		  {
			  agentRequest = agentRequestManager.get(agentId);
		  }
		agentRequestReason = new AgentRequestReason();
		agentRequestReason.setCreatedOn(new Date());
		agentRequestReason.setCreatedBy(getRequest().getRemoteUser());
		agentRequestReason.setUpdatedOn(new Date());
		agentRequestReason.setUpdatedBy(getRequest().getRemoteUser()); 
		agentRequestReason.setCorpID(sessionCorpID);
		agentRequestReason.setAgentRequestId(agentRequest.getId());
		agentRequestReason.setComment(comment);
		agentRequestReason.setReason(agentReason);
		agentRequestReason=agentRequestReasonManager.save(agentRequestReason);	
		//agentRequest=agentRequestManager.get(agentRequest.getId());
		agentRequest.setCounter("1");
		if(sessionCorpID.equals("TSFT"))
		{
			agentRequest.setCounter("0");
			
		}
		agentRequest.setStatus("Rejected");
		agentRequest.setUpdatedBy(getRequest().getRemoteUser());
		agentRequest.setUpdatedOn(new Date());
		agentRequest=agentRequestManager.save(agentRequest);
		hitFlag="1";
		String key="";
		if(sessionCorpID.equals("TSFT"))
		{
		
		 key = "The selected Agent has been Rejected successfully.";
		}
		else
		{
	      key = "Message has been Send successfully.";
				
		}
	    saveMessage(getText(key));
	    //agentRequestManager.updateAgentRecord(sessionCorpID,agentRequest.getLastName(),agentRequest.getAliasName(),agentRequest.getBillingCountry(), agentRequest.getBillingCity(),agentRequest.getBillingState(),agentStatus,agentRequest.getBillingEmail(),true,agentRequest.getBillingZip(),agentRequest.getBillingAddress1(),getRequest().getRemoteUser(),agentRequest.getId(),agentRequest.getCreatedBy());
		User user1 = userManager.getUserByUsername(agentRequest.getUpdatedBy());	
        //agentRequestReason = agentRequestReasonManager.getByID(agentRequest.getId());
		    String msgText1="Dear "+user1.getFirstName()+  ", \n\nYou request to add/edit agent  "+agentRequest.getLastName()+" has been Rejected with reason "+agentRequestReason.getComment()+". \n\nIf this is an error, you may use the resend button on the Agent Request List tab under Move Management->Partner to modify or resend the request.  \n\n Best regards,\nRedsky Team";
		    String subject = "Request Reject for Agent "+agentRequest.getLastName();
			String smtpHost = "localhost"; 
			String from = "support@redskymobility.com";
		if(sessionCorpID.equalsIgnoreCase("TSFT"))
		{
			  emailSetupManager.globalEmailSetupProcess(from,user1.getEmail(), "", "", "", msgText1, subject, agentRequest.getCorpID(),"","","");
		} 
	    logger.warn("USER AUDIT: ID:User: "+getRequest().getRemoteUser()+"---End----"+agentRequestReason.getComment());
		return SUCCESS;

	     
				
	
	     }
	private String flag;
	private List accountAssignmentTypeList;
	public String list(){ 
		//accountAssignmentTypeList=accountAssignmentTypeManager.getAll();
		return SUCCESS;
	}
	
	public String delete(){		
		//accountAssignmentTypeManager.remove(id);  
		partnerPrivateURL = "?partnerId="+partnerPrivateId+"&partnerCode="+partnerCode+"&partnerType=AC";
	      return SUCCESS;		    
	}
	private List findAgentFeedbackList;
	  @SkipValidation
      public String getAgentInfo() {
     	 try {
     		reason=refMasterManager.findByParameter(corpId, "AGENTREQUESTREASON");	
     		agentRequestReason=agentRequestReasonManager.getByID(agentRequestId);
				findAgentFeedbackList= agentRequestReasonManager.findAgentFeedbackList(agentRequestReason.getAgentRequestId());
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  e.printStackTrace();
		    	 return CANCEL;
			}
  		
  		return SUCCESS;   
      }
  	
	private List assignmentList;
	private String checkDup;
	private String assignment;
	@SkipValidation
	 public String checkAssignmentDuplicate(){
		if(partnerCode!=null && !"".equals(partnerCode.trim())){
	      /* // assignmentList = accountAssignmentTypeManager.checkAssignmentDuplicate(partnerCode,sessionCorpID,assignment);
	        if(assignmentList != null && assignmentList.size()>0){
	        	checkDup = "YES";
	        }else{
	        	checkDup = "NO";
	        }*/
		}
		return SUCCESS; 
	  }
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}


	public Long getPartnerPrivateId() {
		return partnerPrivateId;
	}

	public void setPartnerPrivateId(Long partnerPrivateId) {
		this.partnerPrivateId = partnerPrivateId;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public List getAccountAssignmentTypeList() {
		return accountAssignmentTypeList;
	}

	public void setAccountAssignmentTypeList(List accountAssignmentTypeList) {
		this.accountAssignmentTypeList = accountAssignmentTypeList;
	}

	

	public String getPartnerPrivateURL() {
		return partnerPrivateURL;
	}

	public void setPartnerPrivateURL(String partnerPrivateURL) {
		this.partnerPrivateURL = partnerPrivateURL;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public String getAssignment() {
		return assignment;
	}

	public void setAssignment(String assignment) {
		this.assignment = assignment;
	}

	public List getAssignmentList() {
		return assignmentList;
	}

	public void setAssignmentList(List assignmentList) {
		this.assignmentList = assignmentList;
	}

	public String getCheckDup() {
		return checkDup;
	}

	public void setCheckDup(String checkDup) {
		this.checkDup = checkDup;
	}

	public AgentRequestReason getAgentRequestReason() {
		return agentRequestReason;
	}

	public void setAgentRequestReason(AgentRequestReason agentRequestReason) {
		this.agentRequestReason = agentRequestReason;
	}

	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	

	public void setAgentRequestReasonManager(
			AgentRequestReasonManager agentRequestReasonManager) {
		this.agentRequestReasonManager = agentRequestReasonManager;
	}


	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getAgentReason() {
		return agentReason;
	}

	public void setAgentReason(String agentReason) {
		this.agentReason = agentReason;
	}

	public void setReason(Map<String, String> reason) {
		this.reason = reason;
	}

	public Map<String, String> getReason() {
		return reason;
	}

	public long getAgentRequestId() {
		return agentRequestId;
	}

	public void setAgentRequestId(long agentRequestId) {
		this.agentRequestId = agentRequestId;
	}

	public void setAgentRequestId(Long agentRequestId) {
		this.agentRequestId = agentRequestId;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public List getFindAgentFeedbackList() {
		return findAgentFeedbackList;
	}

	public void setFindAgentFeedbackList(List findAgentFeedbackList) {
		this.findAgentFeedbackList = findAgentFeedbackList;
	}

	public AgentRequest getAgentRequest() {
		return agentRequest;
	}

	public void setAgentRequest(AgentRequest agentRequest) {
		this.agentRequest = agentRequest;
	}


	public void setAgentRequestManager(AgentRequestManager agentRequestManager) {
		this.agentRequestManager = agentRequestManager;
	}

	public Map<String, String> getAgentRequestStatus() {
		return agentRequestStatus;
	}

	public void setAgentRequestStatus(Map<String, String> agentRequestStatus) {
		this.agentRequestStatus = agentRequestStatus;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}


	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public String getAgentStatus() {
		return agentStatus;
	}

	public void setAgentStatus(String agentStatus) {
		this.agentStatus = agentStatus;
	}

	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	
}
