package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CategoryRevenueDao;
import com.trilasoft.app.model.CategoryRevenue;
import com.trilasoft.app.service.CategoryRevenueManager;

public class CategoryRevenueManagerImpl extends GenericManagerImpl <CategoryRevenue,Long> implements CategoryRevenueManager{

	CategoryRevenueDao categoryRevenueDao;
	
	public CategoryRevenueManagerImpl(CategoryRevenueDao categoryRevenueDao){
		super(categoryRevenueDao);
		this.categoryRevenueDao=categoryRevenueDao;
		
	}

	public List findMaximumId(){
	    return categoryRevenueDao.findMaximumId(); 
	}
	public List findById(Long id) {
		return categoryRevenueDao.findById(id);
	}

	public List searchCategoryRevenue(String secondSet, String totalCrew,  String numberOfCrew1, String numberOfCrew2, String numberOfCrew3, String numberOfCrew4) {
		return categoryRevenueDao.searchCategoryRevenue(secondSet, totalCrew, numberOfCrew1, numberOfCrew2, numberOfCrew3, numberOfCrew4);
	}
}
