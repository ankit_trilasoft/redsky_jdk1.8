package com.trilasoft.app.webapp.action;

import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.JobStatus;
import com.trilasoft.app.service.JobStatusManager;

public class JobStatusAction extends BaseAction{
	
	private String sessionCorpID;
	private JobStatus jobStatus;
	private JobStatusManager jobStatusManager;
	private List jobStatusList;
	
	public JobStatusAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	
	@SkipValidation
	public String list(){
		jobStatusList=jobStatusManager.getList(sessionCorpID);
		return SUCCESS;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public JobStatus getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(JobStatus jobStatus) {
		this.jobStatus = jobStatus;
	}
	public List getJobStatusList() {
		return jobStatusList;
	}
	public void setJobStatusList(List jobStatusList) {
		this.jobStatusList = jobStatusList;
	}
	public void setJobStatusManager(JobStatusManager jobStatusManager) {
		this.jobStatusManager = jobStatusManager;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

}
