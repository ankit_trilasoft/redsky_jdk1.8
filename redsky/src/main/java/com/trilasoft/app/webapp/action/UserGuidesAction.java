package com.trilasoft.app.webapp.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.UserGuides;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.UserGuidesManager;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.service.CompanyManager;


public class UserGuidesAction extends BaseAction implements Preparable{

	/**
	 * 
	 */
//	private static final long serialVersionUID = 1L;
	private Company company;
	private User user;
	private String sessionCorpID;
	private Long id;
	private String updatedBy;
	private String createdBy;
	private Date createdOn;
	private Date updatedOn;
	private UserGuides userGuides;
	private UserGuidesManager userGuidesManager;
	private RefMasterManager refMasterManager;
	private Map<String,String> userGuidesModule;
	private Map<String,String> corpidChecks;
	private List distinctCorpId;
	private List userGuidesList;
	private Integer displayOrder;
	private String module;
	private Boolean visible ;
	private String urlValue;
	private String fileLocation;
	private String fileFileName;
	private File file;
	private String location1;
    private String fileFileName1;
    private String documentName;
    private List multiplCorpId;
    private Boolean activeStatus;
    private String userGuideModule;
    private String userType;
	private CompanyManager companyManager;
    private List<UserGuides> userModuleGuidesList;
    private Integer prevDisplayOrder;
    private String imageTypes;
    private String mySearchText;
    private Boolean videoFlag;
    private List<String> userModuleList;
    
    static final Logger logger = Logger.getLogger(UserGuidesAction.class);
    Date currentdate = new Date();
    
	public UserGuidesAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		 if(user.isDefaultSearchStatus()){
	 		   activeStatus=true;
	 	   }
	 	   else{
	 		   activeStatus=false;
	 	   }
		
	}

	@SkipValidation
	public String getComboList(String corpId) {
	    	userGuidesModule=refMasterManager.findByParameterUserGuideModule(sessionCorpID, "Training_Doc");
	    	String permKey = sessionCorpID +"-"+"component.userGuide.showAdminModule";
	    	boolean checkFieldVisibility=false;
	    	checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
			if(checkFieldVisibility){
				ServletContext context = getRequest().getSession().getServletContext();
				XmlWebApplicationContext ctx = (XmlWebApplicationContext) context.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
				RoleBasedComponentPermissionUtil roleBasedComponentPermissionUtil = (RoleBasedComponentPermissionUtil) ctx.getBean("componentConfigByRoleUtil");
				int permission = roleBasedComponentPermissionUtil.getComponentPermission("module.userGuide.showAdminModule");
				if(permission!=14){
					userGuidesModule.remove("Admin");
				}
			}
	    	corpidChecks=refMasterManager.findByParameter(sessionCorpID, "CorpID_Checks");
	    	distinctCorpId=userGuidesManager.findDistinct();
	    	  	
	    	return SUCCESS; 
	}
	@SkipValidation
	public String distinctCorpId(){
	    distinctCorpId=userGuidesManager.findDistinct();
		return SUCCESS;
	}
	
	@SkipValidation
	public String list(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
	   		userGuidesList = userGuidesManager.getAll();
	   		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   		return SUCCESS;
	}
	@SkipValidation
	public String listByModuleName(){ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		userModuleList = userGuidesManager.findListByDistinctModule(sessionCorpID);
		List<String> li = new ArrayList<String>();
		
		for (Map.Entry<String, String> entry : userGuidesModule.entrySet()) {
			if(!userModuleList.contains(entry.getKey().toString().trim())){
				li.add(entry.getKey().toString().trim());
			}
		}
		for (String str : li) {
			userGuidesModule.remove(str);
		}
		List companyTemp =companyManager.findByCorpID(sessionCorpID);
		if((companyTemp!=null)&&(!companyTemp.isEmpty())&&(companyTemp.get(0)!=null)){
		company= (Company) companyTemp.get(0);
		}
		List<UserGuides> userModuleGuidesList1;
		userModuleGuidesList1 = userGuidesManager.findListByModule(userGuideModule,sessionCorpID);
		userModuleGuidesList=new LinkedList<UserGuides>();
		for(UserGuides users: userModuleGuidesList1){
			if(users.getCorpidChecks().equalsIgnoreCase("All")){
				if((user.getUserType().equalsIgnoreCase("USER")) && (!(user.getUserType().equalsIgnoreCase("ACCOUNT"))) && (!(user.getUserType().equalsIgnoreCase("AGENT")))&&(!(user.getUserType().equalsIgnoreCase("CUSTOMER"))) && (!(user.getUserType().equalsIgnoreCase("PARTNER"))) ){
					userModuleGuidesList.add(users);
				}
			}
			if(users.getCorpidChecks().equalsIgnoreCase("Partner Portal")){
				if(user.getUserType().equalsIgnoreCase("PARTNER")){
					userModuleGuidesList.add(users);
				}
			}else if(users.getCorpidChecks().equalsIgnoreCase("Account Portal")){
				if(user.getUserType().equalsIgnoreCase("ACCOUNT")){
					userModuleGuidesList.add(users);
				}
			}else if(users.getCorpidChecks().equalsIgnoreCase("Agent Portal")){
				if(user.getUserType().equalsIgnoreCase("AGENT")){
					userModuleGuidesList.add(users);
				}
			}else if(users.getCorpidChecks().equalsIgnoreCase("Customer Portal")){
				if(user.getUserType().equalsIgnoreCase("CUSTOMER")){
					userModuleGuidesList.add(users);
				}
			}else if(users.getCorpidChecks().equalsIgnoreCase("CMM/DMM Agent")){
				if((user.getUserType().equalsIgnoreCase("USER")) && (company.getCmmdmmAgent()!=null && company.getCmmdmmAgent())){
					userModuleGuidesList.add(users);
				}
			}else if(users.getCorpidChecks().equalsIgnoreCase("Vanline")){
				if((user.getUserType().equalsIgnoreCase("USER")) && (company.getVanlineEnabled()!=null && company.getVanlineEnabled())){
					userModuleGuidesList.add(users);
				}
			}
			else if((users.getCorpidChecks().indexOf("Vanline")>-1) && (users.getCorpidChecks().indexOf("CMM/DMM Agent")>-1)){
				if((user.getUserType().equalsIgnoreCase("USER")) && ((company.getVanlineEnabled()!=null && company.getVanlineEnabled()) || (company.getCmmdmmAgent()!=null && company.getCmmdmmAgent()))){
					userModuleGuidesList.add(users);
				}
			}
			if((users.getCorpidChecks().indexOf("All")>-1) && (users.getCorpidChecks().indexOf("Vanline")>-1)){
				if((user.getUserType().equalsIgnoreCase("USER")) && (company.getVanlineEnabled()!=null && company.getVanlineEnabled())){
					userModuleGuidesList.add(users);
				}
			}
			if((users.getCorpidChecks().indexOf("All")>-1) && (users.getCorpidChecks().indexOf("CMM/DMM Agent")>-1)){
				if((user.getUserType().equalsIgnoreCase("USER")) && (company.getCmmdmmAgent()!=null && company.getCmmdmmAgent())){
					userModuleGuidesList.add(users);
				}
			}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String showUserGuideList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		userGuidesList=userGuidesManager.getAll();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   		return SUCCESS;
	}
	
	
	@SkipValidation
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		distinctCorpId();
        if (id != null) {
        	userGuides = userGuidesManager.get(id);
        	 multiplCorpId = new ArrayList();
 			if((userGuides.getCorpidChecks()!=null) && (!userGuides.getCorpidChecks().equals(""))){
 			String[] serviceType = userGuides.getCorpidChecks().split(",");
 			for (String sType : serviceType) {
 				multiplCorpId.add(sType.trim());
 			    }
 			}
        } else {
        	userGuides = new UserGuides();
        	userGuides.setCreatedOn(new Date());
        	userGuides.setUpdatedOn(new Date());
        }
        userGuidesModule=refMasterManager.findByParameterUserGuideModule(sessionCorpID, "Training_Doc");
        corpidChecks=refMasterManager.findByParameter(sessionCorpID, "CorpID_Checks");
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
    }
	
	
	 public String save() throws Exception {
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	   userGuidesModule=refMasterManager.findByParameterUserGuideModule(sessionCorpID, "Training_Doc");
	   corpidChecks=refMasterManager.findByParameter(sessionCorpID, "CorpID_Checks");
	   distinctCorpId();
	   boolean isNew = (userGuides.getId() == null);
	     if(isNew){
	    	 userGuides.setCreatedOn(new Date());
	        }
	   		//updateDisplayOrder(userGuides.getModule(),userGuides.getDisplayOrder(),prevDisplayOrder);
	        String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "UserGuides" +  "/");
			File dirPath = new File(uploadDir);
			if (!dirPath.exists()) {
				dirPath.mkdirs();
			}
			Long kid;
			{
				kid = 1L;
			}
			try{					
				InputStream stream = new FileInputStream(file);

				//write the file to the file specified
				OutputStream bos = new FileOutputStream(uploadDir + kid + "_" + fileFileName);
				int bytesRead = 0;
				byte[] buffer = new byte[8192];

				while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
					bos.write(buffer, 0, bytesRead);
				}

				bos.close();
				stream.close();
	         	InputStream copyStream = new FileInputStream(file);

				OutputStream boscopy = new FileOutputStream(uploadDir + fileFileName);
				int bytesReadCopy = 0;
				byte[] bufferCopy = new byte[8192];

				while ((bytesReadCopy = copyStream.read(bufferCopy, 0, 8192)) != -1) {
					boscopy.write(bufferCopy, 0, bytesReadCopy);
				}

				boscopy.close();
				copyStream.close();
				getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
				userGuides.setLocation(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName);
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
				if(!fileFileName.equals("")){
					userGuides.setFileName(userGuides.getFileName());
					userGuides.setFileName(fileFileName);
				}else{
					userGuides.setFileName(fileFileName1);
					userGuides.setLocation(location1);
				}
	    
	  // 	userGuides.setCorpId(sessionCorpID);
		//userGuides.setCreatedOn(new Date());
		userGuides.setCreatedBy(getRequest().getRemoteUser());
		userGuides.setUpdatedOn(new Date());
		userGuides.setUpdatedBy(getRequest().getRemoteUser());
		
		userGuides = userGuidesManager.save(userGuides);
		
		String key = (isNew) ? "userGuides.added" : "userGuides.updated";
		saveMessage(getText(key));
	       String str=list();
	       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	       return str;
	}
	 public void updateDisplayOrder(String module,int displayOrder,Integer prevDisplayOrder){
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 List<UserGuides> guideList = userGuidesManager.findListByModuleAndDisplayOrder(module, displayOrder,prevDisplayOrder);
		 for (UserGuides userGuides : guideList) {
			 userGuides.setDisplayOrder(userGuides.getDisplayOrder()+1);
			 userGuidesManager.save(userGuides);
			  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		}
	 }
	
	 public String delete() {
		    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	userGuidesManager.remove(userGuides.getId());
	        saveMessage(getText("userGuides.deleted"));
	        userGuidesModule=refMasterManager.findByParameterUserGuideModule(sessionCorpID, "Training_Doc");
	        corpidChecks=refMasterManager.findByParameter(sessionCorpID, "CorpID_Checks");
	        distinctCorpId();
	        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS;
	    }
	 
	 @SkipValidation
	    public String deleteUserGuidesDoc() {
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	userGuidesManager.remove(id);
	    	saveMessage(getText("userGuidesDetail.deleted"));
	    	 	 urlValue="true&corpID=${sessionCorpID}";
	    	   list();
	    	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	 return SUCCESS;
	    	}
	 
	 
	//  Method to search module
	    @SkipValidation
	    public String searchUserGuides() {
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	getComboList(sessionCorpID);
			boolean module = (userGuides.getModule() == null) || ((userGuides.getModule()).equals(""));
			boolean corpid = (userGuides.getCorpId() == null) || ((userGuides.getCorpId()).equals(""));
					
			userGuidesList = userGuidesManager.findListByModuleDocStatus(userGuides.getModule(),userGuides.getDocumentName(),activeStatus);
			   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
	    		return SUCCESS;
		}
	    @SkipValidation
	    public String searchUserGuidesDocumentType() {
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	   	getComboList(sessionCorpID);
	   	
	   	List companyTemp =companyManager.findByCorpID(sessionCorpID);
		if((companyTemp!=null)&&(!companyTemp.isEmpty())&&(companyTemp.get(0)!=null)){
	   	company= (Company) companyTemp.get(0);
		}
	    userModuleList = userGuidesManager.findListByDistinctModule(sessionCorpID);
		userModuleGuidesList = userGuidesManager.findListByDocumentName(mySearchText,user.getUserType(),company.getCmmdmmAgent(),company.getVanlineEnabled(),sessionCorpID);
	   	List<String> li = new ArrayList<String>();
		
		for (Map.Entry<String, String> entry : userGuidesModule.entrySet()) {
			if(!userModuleList.contains(entry.getKey().toString().trim())){
				li.add(entry.getKey().toString().trim());
			}
		}
		for (String str : li) {
			userGuidesModule.remove(str);
		}
		   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    		return SUCCESS;
		}
	    
	    public String importDocData() {
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	    	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return SUCCESS;
	    }
	    
	public String getSessionCorpID() {
		return sessionCorpID;
	}

	
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	/**
	 * @return the userGuides
	 */
	public UserGuides getUserGuides() {
		return userGuides;
	}
	
	/**
	 * @param userGuides the userGuides to set
	 */
	public void setUserGuides(UserGuides userGuides) {
		this.userGuides = userGuides;
	}

	/**
	 * @param userGuidesManager the userGuidesManager to set
	 */
	public void setUserGuidesManager(UserGuidesManager userGuidesManager) {
		this.userGuidesManager = userGuidesManager;
	}

	public Map<String, String> getUserGuidesModule() {
		return userGuidesModule;
	}

	public Map<String, String> getCorpidChecks() {
		return corpidChecks;
	}

	public void setUserGuidesModule(Map<String, String> userGuidesModule) {
		this.userGuidesModule = userGuidesModule;
	}

	public void setCorpidChecks(Map<String, String> corpidChecks) {
		this.corpidChecks = corpidChecks;
	}

	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public List getUserGuidesList() {
		return userGuidesList;
	}

	public void setUserGuidesList(List userGuidesList) {
		this.userGuidesList = userGuidesList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public String getUrlValue() {
		return urlValue;
	}

	public void setUrlValue(String urlValue) {
		this.urlValue = urlValue;
	}

	public String getFileLocation() {
		return fileLocation;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public File getFile() {
		return file;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getLocation1() {
		return location1;
	}

	public String getFileFileName1() {
		return fileFileName1;
	}

	public void setLocation1(String location1) {
		this.location1 = location1;
	}

	public void setFileFileName1(String fileFileName1) {
		this.fileFileName1 = fileFileName1;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getUserGuideModule() {
		return userGuideModule;
	}

	public void setUserGuideModule(String userGuideModule) {
		this.userGuideModule = userGuideModule;
	}

	public List<UserGuides> getUserModuleGuidesList() {
		return userModuleGuidesList;
	}

	public void setUserModuleGuidesList(List<UserGuides> userModuleGuidesList) {
		this.userModuleGuidesList = userModuleGuidesList;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Company getCompany() {
		return company;
	}

	public User getUser() {
		return user;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public CompanyManager getCompanyManager() {
		return companyManager;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public Integer getPrevDisplayOrder() {
		return prevDisplayOrder;
	}

	public void setPrevDisplayOrder(Integer prevDisplayOrder) {
		this.prevDisplayOrder = prevDisplayOrder;
	}

	public String getImageTypes() {
		return imageTypes;
	}

	public void setImageTypes(String imageTypes) {
		this.imageTypes = imageTypes;
	}

	public String getMySearchText() {
		return mySearchText;
	}

	public void setMySearchText(String mySearchText) {
		this.mySearchText = mySearchText;
	}

	public List<String> getUserModuleList() {
		return userModuleList;
	}

	public void setUserModuleList(List<String> userModuleList) {
		this.userModuleList = userModuleList;
	}
	public Boolean getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(Boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public List getMultiplCorpId() {
		return multiplCorpId;
	}

	public void setMultiplCorpId(List multiplCorpId) {
		this.multiplCorpId = multiplCorpId;
	}

	public void prepare() throws Exception { 
		
	}

	public List getDistinctCorpId() {
		return distinctCorpId;
	}

	public void setDistinctCorpId(List distinctCorpId) {
		this.distinctCorpId = distinctCorpId;
	}
	public UserGuidesManager getUserGuidesManager() {
		return userGuidesManager;
	}

	public Boolean getVideoFlag() {
		return videoFlag;
	}

	public void setVideoFlag(Boolean videoFlag) {
		this.videoFlag = videoFlag;
	}

}
